<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

global $config;
$db2 = new sqldb();
$db3 = new sqldb();
$l_r =ROW_PER_PAGE;

if(!empty($in['start_date_js'])){
	$in['start_date'] =strtotime($in['start_date_js']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['stop_date_js'])){
	$in['stop_date'] =strtotime($in['stop_date_js']);
	$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}
if(!empty($in['d_start_date_js'])){
	$in['d_start_date'] =strtotime($in['d_start_date_js']);
	$in['d_start_date'] = mktime(0,0,0,date('n',$in['d_start_date']),date('j',$in['d_start_date']),date('y',$in['d_start_date']));
}
if(!empty($in['d_stop_date_js'])){
	$in['d_stop_date'] =strtotime($in['d_stop_date_js']);
	$in['d_stop_date'] = mktime(23,59,59,date('n',$in['d_stop_date']),date('j',$in['d_stop_date']),date('y',$in['d_stop_date']));
}

if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}
$order_by_array = array('serial_number','t_date','del_date','our_ref','customer_name','amount');
$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$arguments = '';
$filter = 'WHERE 1=1 ';
$filter_link = 'block';
$filter_article='';
$order_by = " ORDER BY t_date DESC, iii DESC ";

if(!$in['archived']){
	$filter.= " AND pim_orders.active=1";
}else{
	$filter.= " AND pim_orders.active=0";
	$arguments.="&archived=".$in['archived'];
}
if($in['customer_id']){
	$filter.= " AND pim_orders.customer_id='".$in['customer_id']."'";
	$arguments_c .= '&customer_id='.$in['customer_id'];
}
if($in['filter']){
	$arguments.="&filter=".$in['filter'];
}
if($in['search']){
	$filter.=" and (pim_orders.serial_number like '%".$in['search']."%' OR pim_orders.customer_name like '%".$in['search']."%' OR pim_orders.our_ref like '%".$in['search']."%' )";
	$arguments.="&search=".$in['search'];
}

if($in['article_name_search']){
	$filter_article.=" INNER JOIN pim_order_articles ON pim_order_articles.order_id=pim_orders.order_id
                       INNER JOIN pim_articles ON pim_articles.article_id=pim_order_articles.article_id AND (pim_articles.internal_name like '%".$in['article_name_search']."%' OR pim_order_articles.article_code like '%".$in['article_name_search']."%')";

	$arguments.="&article_name_search=".$in['article_name_search'];
}
if($in['order_by']){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == '1'){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		/*$view_list->assign(array(
			'on_'.$in['order_by'] 	=> $in['desc'] ? 'on_asc' : 'on_desc',
		));*/
	}
}

if(!isset($in['view'])){
	$in['view'] = 4;
}
$show_only_my = true;
if(defined('SHOW_MY_ORDERS_ONLY') && SHOW_MY_ORDERS_ONLY == 1){
	if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])){
		$show_only_my = false;
		if($in['view'] == 0){
			$in['view'] = 4;
		}
		$filter.=" and ( pim_orders.created_by = '".$_SESSION['u_id']."' OR pim_orders.author_id='".$_SESSION['u_id']."' ) ";
	}
}
	switch ($in['view']) {
		case '1':
			$filter.=" and pim_orders.rdy_invoice = '0' and pim_orders.sent='0' ";
			$arguments.="&view=1";
			break;
		case '2':
			$filter.=" and pim_orders.rdy_invoice != '1' and pim_orders.sent='1'";
			$arguments.="&view=2";
			break;
		case '3':
			$filter.=" and pim_orders.rdy_invoice = '1' ";
			$arguments.="&view=3";
			break;
		case '4':
			if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])) {
				$filter.=" and ( pim_orders.created_by = '".$_SESSION['u_id']."' OR pim_orders.author_id='".$_SESSION['u_id']."' ) ";
			}
			$arguments.="&view=4";
			break;
		default:
			break;
	}

if($in['start_date'] && $in['stop_date']){
	$filter.=" and pim_orders.date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
	$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}
else if($in['start_date']){
	$filter.=" and cast(pim_orders.date as signed) > ".$in['start_date']." ";
	$arguments.="&start_date=".$in['start_date'];
}
else if($in['stop_date']){
	$filter.=" and cast(pim_orders.date as signed) < ".$in['stop_date']." ";
	$arguments.="&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}

if($in['d_start_date'] && $in['d_stop_date']){
	$filter.=" and pim_orders.del_date BETWEEN '".$in['d_start_date']."' and '".$in['d_stop_date']."' ";
	$arguments.="&d_start_date=".$in['d_start_date']."&d_stop_date=".$in['d_stop_date'];
	$filter_link = 'none';
}
else if($in['d_start_date']){
	$filter.=" and cast(pim_orders.del_date as signed) > ".$in['d_start_date']." ";
	$arguments.="&d_start_date=".$in['d_start_date'];
}
else if($in['d_stop_date']){
	$filter.=" and cast(pim_orders.start_date as signed) < ".$in['d_stop_date']." ";
	$arguments.="&d_stop_date=".$in['d_stop_date'];
	$filter_link = 'none';
}

if($in['new_orders']==1) {
	$days_30 = time() - 30*3600*24;
	$now = time();
	$filter .= " AND pim_orders.rdy_invoice!=1 AND date BETWEEN ".$days_30." AND ".$now." ";
}

$arguments = $arguments.$arguments_s;
$max_rows = $db->field("SELECT count(pim_orders.order_id)
			FROM pim_orders
			".$filter_article."
			 ".$filter."  ");
$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;
/*$orders = $db->query("SELECT SUM( pim_orders_delivery.invoiced ) AS invoiced_del, pim_orders.*, serial_number as iii, cast( FROM_UNIXTIME( `date` ) AS DATE ) AS t_date
			FROM pim_orders
			LEFT JOIN pim_orders_delivery ON pim_orders_delivery.order_id = pim_orders.order_id
			".$filter_article."
			 ".$filter." GROUP BY pim_orders.order_id ".$order_by );*/
$orders = $db->query("SELECT SUM( pim_orders_delivery.invoiced ) AS invoiced_del, pim_orders.order_id,pim_orders.author_id,pim_orders.buyer_ref,pim_orders.serial_number,pim_orders.del_date,
					pim_orders.date,pim_orders.amount,pim_orders.customer_name,pim_orders.rdy_invoice,pim_orders.sent,pim_orders.our_ref,pim_orders.invoiced, pim_orders.postgreen_id,serial_number as iii, cast( FROM_UNIXTIME( `date` ) AS DATE ) AS t_date
			FROM pim_orders
			LEFT JOIN pim_orders_delivery ON pim_orders_delivery.order_id = pim_orders.order_id
			".$filter_article."
			 ".$filter." GROUP BY pim_orders.order_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r);


// $max_rows=$orders->records_count();
// $orders->move_to($offset*$l_r);
$j=0;
$color = '';

$result = array('query'=>array(),'max_rows'=>$max_rows);
while($orders->next()){

	$is_admin = getItemPerm(array('module'=>'order','item'=>$orders->f('author_id')));

	$ref = '';
	if(ACCOUNT_ORDER_REF && $orders->f('buyer_ref')){
		$ref = $orders->f('buyer_ref');

	}

	if($orders->f('sent')==0 ){
		$status = gm('Draft');
	}
	if($orders->f('invoiced')){
		if($orders->f('rdy_invoice') == 1){
			$status = gm('Delivered');
		}else{
			$status = gm('Final');
		}
	}else{
		if($orders->f('rdy_invoice') == 2){
			$status = gm('Final');
		}else{
			if($orders->f('rdy_invoice') == 1){
				$status = gm('Delivered');
			}else{
				if($orders->f('sent') == 1){
					$status = gm('Final');
				}else{
					$status = gm('Draft');
				}
			}
		}
	}

  $order_email=$db->field("SELECT order_email FROM pim_orders WHERE order_id='".$orders->f('order_id')."'");
	$inv_ord = $orders->f('invoiced_del');
	$item =array(
		'info_link'    	 				=> 'index.php?do=order-order&order_id='.$orders->f('order_id').$arguments,
		'edit_link'     				=> 'index.php?do=order-norder&order_id='.$orders->f('order_id').$arguments,
		// 'delete_link'  					=> 'index.php?do=order-orders_list-order-delete_order&order_id='.$orders->f('order_id').$arguments,
		'delete_link'  					=> array('do'=>'order-orders-order-delete_order','order_id'=>$orders->f('order_id')),
		// 'undo_link' 	 					=> 'index.php?do=order-orders_list-order-activate_order&order_id='.$orders->f('order_id').$arguments,
		'undo_link' 	 					=> array('do'=>'order-orders-order-activate_order','order_id'=>$orders->f('order_id')),
		// 'archice_link'  				=> 'index.php?do=order-orders_list-order-archive_order&order_id='.$orders->f('order_id').$arguments ,
		'archive_link'  				=> array('do'=>'order-orders-order-archive_order','order_id'=>$orders->f('order_id')),
		// 'view_delete_link2'			=> $_SESSION['access_level'] == 1 ? ($in['archived'] ? '' : 'hide') : 'hide',
		// 'hide_on_a'							=> $in['archived'] == 1 ? 'hide' : '',
		'id'              	  	=> $orders->f('order_id'),
		'serial_number'   	  	=> $orders->f('serial_number') ? $ref.$orders->f('serial_number') : '',
		'created'         	  	=> date(ACCOUNT_DATE_FORMAT,$orders->f('date')),
		'delivery_date'   	   	=> $orders->f('del_date')?date(ACCOUNT_DATE_FORMAT,$orders->f('del_date')):'',
		'amount'								=> place_currency(display_number($orders->f('amount'))),
		'buyer_name'      	  	=> $orders->f('customer_name'),
		// 'is_editable'				=> $orders->f('rdy_invoice') ? false : true,
		'is_editable'						=> $orders->f('rdy_invoice') ? false : ($orders->f('sent') == 1 ? false : true),
		'is_archived'     	  	=> $in['archived'] ? true : false,
		//'path'								=> $config['site_url'],
		'status'								=> $orders->f('rdy_invoice') ? ($orders->f('rdy_invoice')==1 ? 'green' : 'orange') : ($orders->f('sent') == 1 ? 'orange' : 'gray'),
		'status2'								=> $status,
		'our_ref'       	 			=> $orders->f('our_ref'),
		// 'alt_for_delete_icon'		=> $in['archived'] == 1 ? 'Delete' : 'Archive',
		'order_id'							=> $orders->f('order_id'),
		'check_add_to_product'	=> $_SESSION['add_to_pdf_o'][$orders->f('order_id')] == 1 ? true : false,
		// 'is_invoice'						=> empty($inv_ord) ? false : true,
		'is_invoice'						=> (!empty($inv_ord) || $orders->f('invoiced') == 1 ) ? true : false,
		'invAll'								=> $orders->f('rdy_invoice')==1 ? 'inv_ordGreenIcons' : '',
		'invAll_info'						=> $orders->f('rdy_invoice')==1 ? gm('Invoiced') : gm('Partially Invoiced'),
		'is_admin'							=> $is_admin,
		'is_front'							=> $order_email?true:false,
		'is_partial_deliver'    => $orders->f('rdy_invoice')==2?true:false,
		'confirm'				=> gm('Confirm'),
		'ok'					=> gm('Ok'),
		'cancel'				=> gm('Cancel'),
		'partialDEL_info'       => gm('Partially Delivered'),
		'post_order'		    => $orders->f('postgreen_id')!='' ? true : false,
		// 'status'								=> $orders->f('rdy_invoice') ? ($orders->f('rdy_invoice')==1 ? 'green' : 'orange') : ($orders->f('sent') == 1 ? 'orange' : 'gray'),
	);
	/*if(ORDER_DELIVERY_STEPS==2){
		$count_del = $db3->field("SELECT COUNT(delivery_id) FROM pim_order_deliveries WHERE order_id = '".$orders->f('order_id')."' AND delivery_done = '0' ");
		$count_done_del = $db3->field("SELECT COUNT(delivery_id) FROM pim_order_deliveries WHERE order_id = '".$orders->f('order_id')."' AND delivery_done = '1' ");
		/*$view_list->assign(array(
			'status'				=> $orders->f('rdy_invoice') ? ($orders->f('rdy_invoice')==1 ? ($count_del == 0 ? 'green' : 'orange' ) : 'orange') : ($orders->f('sent') == 1 ? 'orange' : 'gray'),
		),'order_row');* /
	}*/
	array_push($result['query'], $item);
	// $view_list->loop('order_row');
	$j++;
}

$post_active=$db->field("SELECT active FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");

$default_lang_id = $db->field("SELECT lang_id FROM pim_lang WHERE default_lang = '1' ");
$default_pdf_type = ACCOUNT_ORDER_PDF_FORMAT;

$result['lid']				= $default_lang_id;
$result['pdf_type']		= $default_pdf_type;
$result['lr']		= $l_r;
$result['post_active']= !$post_active || $post_active==0 ? false : true;


json_out($result);
