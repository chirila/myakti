<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}

global $config;

// $view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.$config['cache_version']);
$db2 = new sqldb();
$db3 = new sqldb();
$db_lines=new sqldb();
$o = array(
	'main_comp_info'=>array(
		'name'		=> ACCOUNT_COMPANY,
		'address'	=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
		'zip'		=> ACCOUNT_DELIVERY_ZIP,
		'city'		=> ACCOUNT_DELIVERY_CITY,
		'country'	=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
		'phone'		=> ACCOUNT_PHONE,
		'fax'		=> ACCOUNT_FAX,
		'email'		=> ACCOUNT_EMAIL,
		'url'		=> ACCOUNT_URL,
		'logo'		=> '../'.ACCOUNT_LOGO_ORDER,
		)
);
$in['main_comp_info']=array(
	'name'		=> ACCOUNT_COMPANY,
	'address'	=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
	'zip'		=> ACCOUNT_DELIVERY_ZIP,
	'city'		=> ACCOUNT_DELIVERY_CITY,
	'country'	=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
	'phone'		=> ACCOUNT_PHONE,
	'fax'		=> ACCOUNT_FAX,
	'email'		=> ACCOUNT_EMAIL,
	'url'		=> ACCOUNT_URL,
	'logo'		=> '../'.ACCOUNT_LOGO_ORDER,
	);

$o['default_currency']     	= ACCOUNT_CURRENCY_TYPE;
$o['default_currency_name']    = build_currency_name_list(ACCOUNT_CURRENCY_TYPE);

$o['is_admin']			=   $_SESSION['access_level'] == 1 ? true : false;
$in['is_admin']     	=   $_SESSION['access_level'] == 1 ? true : false;

$o['search_by']=$in['search_by'];
if(!$in['search_by']){
	$o['search_by']=1;
}
// $view->assign(array(
$o['search_by_name_check']		        = $in['search_by'] == 1 ? 'CHECKED' : '';
$o['search_by_company_number_check']	= $in['search_by'] == 2 ? 'CHECKED' : '';
// ));

$o['payment_term']=$in['payment_term'];
if(!$in['payment_term']){
	$o['payment_term']=15;
}
$cancel_link = "index.php?do=order-orders";

if((!$in['order_id']  ||  $in['c_quote_id'] ) || $in['quote_id'] || $in['order_id'] == 'tmp'){
	$o['do_request'] = 'order-norder-order-updateCustomerData';

	if($in['buyer_id'] == 'tmp'){
		$in['add_customer']				= true;
		$in['cc']						= get_cc($in,true,false);
		$in['country_dd']				= build_country_list();
		$in['language_dd']				= build_language_dd();
		$in['gender_dd']				= gender_dd();
		$in['title_dd']					= build_contact_title_type_dd(0);
		$in['title_id']					= '0';
		$in['gender_id']				= '0';
		$in['language_id']				= '0';
		$in['main_country_id']			= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';
		$in['email_language'] 			= DEFAULT_LANG_ID;
		$in['APPLY_DISCOUNT_LIST']   	= build_apply_discount_list($in['apply_discount']);
		$in['apply_discount'] 			= '0';
		$in['payment_type_dd']     		= build_payment_type_dd($in['payment_type']);
		$in['payment_type']				= '0';
		$in['language_dd'] 				= build_language_dd_new($in['email_language']);
		$in['currency_id']				= ACCOUNT_CURRENCY_TYPE;
		$in['identity_id'] 				= '0';
		$in['articles_list']			= get_articles_list($in,true,false);
		$in['authors']					= get_author($in,true,false);
		$in['accmanager']				= get_accmanager($in,true,false);
		$in['contacts']					= get_contacts($in,true,false);
		$in['customers']				= get_customers($in,true,false);
		$in['addresses']				= get_addresses($in,true,false);
		$in['is_vat']					= $in['remove_vat'] == 1 ? false : true;
		$in['languages']				= $in['email_language'];
		$in['do_request']				= 'order-norder-order-updateCustomerData';
		$in['remove_vat'] ? $vat_column = 0 : $vat_column = 1 ;
		if(!ALLOW_ARTICLE_PACKING){
			$in['allow_article_packing']=0;
		}else{
			$in['allow_article_packing']=1;
		}
		if(!ALLOW_ARTICLE_SALE_UNIT){
			$in['allow_article_sale_unit']=0;
		}else{
			$in['allow_article_sale_unit']=1;
		}
		$colspan_code = $vat_column.'-'.$in['allow_article_packing'].'-'.$in['apply_discount'];
		switch ($colspan_code) {
			case '0-0-0':
			case '0-0-2':
			$cols = 6;
			$col = 6;
				break;
			case '1-0-0':
			case '0-0-1':
			case '0-0-3':
			case '1-0-2':
			case '0-1-0':
			case '0-1-2':
				$cols = 7;
				$col = 5;
				break;
			case '1-0-1':
			case '1-0-3':
			case '0-1-1':
			case '0-1-3':
			case '1-1-0':
			case '1-1-2':
				$cols = 8;
				$col = 4;
				break;
			case '1-1-1':
			case '1-1-3':
				$cols = 9;
				$col = 3;
				break;
		}
		$in['colum'] = $col;
		//print_r($in);
		json_out($in);
	}elseif ($in['buyer_id'] && !is_numeric($in['buyer_id'])) {
		$o = array('redirect'=>'orders');
		json_out($o);
		
	}
	// console::log('ba da2 '.DEFAULT_LANG_ID);
	$in['email_language'] 		= DEFAULT_LANG_ID;
	$in['languages']			= $in['email_language'];
	$in['currency_id']			= ACCOUNT_CURRENCY_TYPE;
	$in['identity_id']			= $in['identity_id'] ? $in['identity_id'] : '0';
	//ADD
	$cancel_link = "index.php?do=order-orders";
    // $page_title = gm('Add Order');
  	if($in['quote_id']){ // we ignore
  		$view->assign('quote_id',$in['quote_id']);
  	}
  	$do_next = 'order-norder-order-add_order';

  	$apply_discount = 0;

  	if($in['c_quote_id'] ){ // we ignore
  		// $view->assign('quote_id',$in['c_quote_id']);
  		$o['c_quote_id'] = $in['c_quote_id'];
  		$o['quote_id'] = $in['c_quote_id'];
  		$in['our_ref'] = $in['a_quote_id'];

		if(isset($in['version_id']) && is_numeric($in['version_id'])){
			$quote_filter = 'AND tblquote_version.version_id = '.$in['version_id'];
		}else{
			$quote_filter = 'AND tblquote_version.active = 1';
		}
        $quote = $db->query("SELECT tblquote.*,tblquote_version.active,tblquote_version.version_code,tblquote_version.version_id,tblquote_version.active AS version_active, tblquote_version.version_date
                    FROM tblquote
                    INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
                    WHERE tblquote.id='".$in['c_quote_id']."' AND 1=1 ".$quote_filter);
		/*if(!$db->move_next()){
		    exit();
		}*/
		if($quote->f('delivery_address')){
			$in['delivery_address']=$quote->f('delivery_address');
		}
		$version_id =   $quote->f('version_id');

		$q_line_discount = $quote->f('discount_line_gen');
		$in['apply_discount']= $quote->gf('apply_discount');
		$o['apply_discount'] = (string)$in['apply_discount'];
        $in['discount']= display_number($quote->gf('discount'));
        $in['your_ref']= $quote->gf('own_reference');
        $in['currency_id'] = $quote->f('currency_type') ? $quote->f('currency_type') : ACCOUNT_CURRENCY_TYPE;
        $in['delivery_address_id'] = $quote->f('delivery_address_id');
        $o['main_address_id'] = $quote->f('main_address_id');
		//$q_apply_fix_disc = $db->f('apply_fix_disc');
		//$q_apply_line_disc = $db->f('apply_line_disc');
  	}

  	if(!$in['date_h']){
		$in['date'] = date(ACCOUNT_DATE_FORMAT, mktime(0, 0, 0, date("n"), date("j"), date("Y")));
		$in['date_h'] = time()*1000;
	}
	$o['date'] = $in['date'];
	$o['date_h'] = $in['date_h'];


	if(!$in['del_date']){
		$in['delivery_date'] = date(ACCOUNT_DATE_FORMAT, mktime(0, 0, 0, date("n"), date("j"), date("Y")));
		$in['del_date'] = time()*1000;
	}
	$o['delivery_date'] = $in['delivery_date'];
	$o['del_date'] = $in['del_date'];

	if (!$in['serial_number']){
		$in['serial_number'] = generate_order_serial_number(DATABASE_NAME);
	}
	$o['serial_number'] = $in['serial_number'];


	if(!ALLOW_ARTICLE_PACKING){
		$in['allow_article_packing']=0;
	}else{
		$in['allow_article_packing']=1;
	}
	if(!ALLOW_ARTICLE_SALE_UNIT){
		$in['allow_article_sale_unit']=0;
	}else{
		$in['allow_article_sale_unit']=1;
	}
	$o['buyer_id'] = $in['buyer_id'];
	$o['customer_id'] = $in['buyer_id'];
	$o['contact_id'] = $in['contact_id'] ? $in['contact_id'] : '';

	$details = array('table' 		=> $in['buyer_id'] ? 'customer_addresses' : 'customer_contact_address' ,
					 'field'		=> $in['buyer_id'] ? 'customer_id' : 'contact_id',
					 'value'		=> $in['buyer_id'] ? $in['buyer_id'] : $in['contact_id'],
					 'name'			=> $in['buyer_id'] ? stripslashes($in['s_buyer_id']) : stripslashes(substr($in['s_customer_id'], 0, strpos($in['s_customer_id'],'>'))) ,
	);

	$ref = '';
	$o['customer_ref'] = '';
	
	if(!$in['buyer_id']){
		$details['cat'] = '0';

		$o['is_company']=false;
		$buyer_info = $db->query("SELECT phone, cell, email FROM customer_contacts WHERE ".$details['field']."='".$details['value']."' ");
		$buyer_info->next();
	    if(!$details['name']){
	    	$contact_name = $db2->query("SELECT firstname,lastname,contact_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
	    	$details['name'] = $contact_name->f('firstname').' '.$contact_name->f('lastname');
	    }
	}else {
		$customer_id = $in['buyer_id'];
		$o['is_company']=true;
		$buyer_info = $db->query("SELECT deliv_disp_note, cat_id, c_email, our_reference ,comp_phone,name, customers.no_vat,customers.btw_nr, customers.internal_language,
								customers.line_discount, customers.apply_fix_disc,customers.currency_id, customers.apply_line_disc, customers.identity_id
								FROM customers WHERE ".$details['field']."='".$details['value']."' ");
		$buyer_info->next();

		$o['discount_line_gen']= display_number($buyer_info->f('line_discount'));

		if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
			$apply_discount = 3;
		}else{
			if($buyer_info->f('apply_fix_disc')){
				$apply_discount = 2;
			}
			if($buyer_info->f('apply_line_disc')){
				$apply_discount = 1;
			}
		}
		if(!$in['c_quote_id']){
		   	$in['apply_discount'] = $apply_discount;
		   	$o['apply_discount'] = (string)$apply_discount;
	    }
			if(!$details['name'] ){
	    	$details['name']=$buyer_info->f('name');
	    }
	    $details['cat'] = $buyer_info->f('cat_id');
		$in['remove_vat'] = $buyer_info->f('no_vat');
		$o['customer_ref']=$buyer_info->f('our_reference');
		if(ACCOUNT_ORDER_REF && $buyer_info->f('our_reference')){
			$ref = $buyer_info->f('our_reference').ACCOUNT_ORDER_DEL;
		}
		// var_dump($buyer_info->f('internal_language'));

		 $in['email_language'] 		= $buyer_info->f('internal_language') ? $buyer_info->f('internal_language') : $in['email_language'];
		 $in['currency_id']			= $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
		 $in['identity_id']			= $buyer_info->gf('identity_id');
		 $in['del_note']			= $in['del_note'] ? $in['del_note'] : $buyer_info->f('deliv_disp_note');
		 $in['languages']			= $in['email_language'];
		 $buyer_details = $db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND is_primary='1' ");
			$main_address_id			= $buyer_details->f('address_id');
			$sameAddress = false;
			if($buyer_details->f('billing')==1){
				$sameAddress = true;
			}
			
				
			


	}
	$contact_title = $db->field("SELECT customer_contact_title.name FROM customer_contact_title
								INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
								WHERE customer_contacts.contact_id='".$in['contact_id']."'");
	if($contact_title)
	{
		$contact_title .= " ";
	}


	$contact_info=$db->query("SELECT firstname,lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."'");
	$contact_info->next();
	if($contact_info->f('lastname')) {
	   $contact_last_name = " ".$contact_info->f('lastname');
	}
	$in['contact_name']= $contact_title.$contact_info->f('firstname').$contact_last_name;
	if(!$in['apply_discount']){
		$in['apply_discount']=0;
	}

	$buyer = $db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ORDER BY is_primary DESC");
	$buyer->next();
	if($in['buyer_id'] && $buyer->f('delivery') == 0){
		$buyer_del_address = $db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' AND delivery=1 limit 1 ");
		if($buyer_del_address){
				$o['delivery_address'] = $buyer_del_address->f('address').'
'.$buyer_del_address->f('zip').'  '.$buyer_del_address->f('city').'
'.get_country_name($buyer_del_address->f('country_id'));
		}
	}
	if(!$in['c_quote_id']){ // we don't care
	  	$discount=0;
	  	if($in['buyer_id']){
      		$discount= $db2->field('SELECT fixed_discount FROM customers WHERE customer_id ='.$in['buyer_id']);
    	}
    	$in['discount']	    		= display_number($discount);
  	}
	$in['customer_id'] 		= $details['value'];
	$in['field'] 			= $details['field'];
	$o['field'] 			= $details['field'];
	$o['price_category_id'] = $details['cat'];
	$in['price_category_id'] = $details['cat'];

  	$o['buyer_ref']	    	= $ref;
  	$o['currency_name']    	= build_currency_name_list($in['currency_type']);
  	$o['currency_rate']    	= $in['currency_rate'];


  	$o['show_vat'] = $in['show_vat'];
  	if(!isset($in['show_vat'])){
  		$in['show_vat']=1;
  		$o['show_vat']=1;
  	}
    if($in['buyer_id']){
$in['address_info'] = $buyer->f('address').'
'.$buyer->f('zip').'  '.$buyer->f('city').'
'.get_country_name($buyer->f('country_id'));

    }else{
$in['address_info'] = $buyer->f('address').'
'.$buyer->f('zip').'  '.$buyer->f('city').'
'.get_country_name($buyer->f('country_id'));

    }

	$existIdentity = $db2->field("SELECT COUNT(identity_id) FROM multiple_identity ");

  	$acc_manager=$db->field("SELECT acc_manager_name FROM customers WHERE customer_id = '".$in['buyer_id']."' ");
	$acc_manager = explode(',',$acc_manager);
	$acc_manager_id=$db->field("SELECT user_id FROM customers WHERE customer_id = '".$in['buyer_id']."' ");
	$acc_manager_id = explode(',',$acc_manager_id);
	if(!$in['discount']){
		$in['discount'] = '0';
	}

	$in['remove_vat'] ? $vat_column = 0 : $vat_column = 1 ;
	$colspan_code = $vat_column.'-'.$in['allow_article_packing'].'-'.$in['apply_discount'];
	switch ($colspan_code) {
		case '0-0-0':
		case '0-0-2':
		$cols = 6;
		$col = 6;
			break;
		case '1-0-0':
		case '0-0-1':
		case '0-0-3':
		case '1-0-2':
		case '0-1-0':
		case '0-1-2':
			$cols = 7;
			$col = 5;
			break;
		case '1-0-1':
		case '1-0-3':
		case '0-1-1':
		case '0-1-3':
		case '1-1-0':
		case '1-1-2':
			$cols = 8;
			$col = 4;
			break;
		case '1-1-1':
		case '1-1-3':
			$cols = 9;
			$col = 3;
			break;
	}
	$o['colum'] = $col;
	$o['sameAddress'] = $sameAddress;
	
    
  $buyer_adr=$db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' and address_id='".$in['main_address_id']."'");
if($in['delivery_address_id']){
	$buyer_adr=$db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' and address_id='".$in['delivery_address_id']."'");
}
 
    $in['address_info']=$buyer_adr->f('address').'
'.$buyer_adr->f('zip').'  '.$buyer_adr->f('city').'
'.get_country_name($buyer_adr->f('country_id'));

	if($in['c_quote_id']){
		$in['address_info'] = $quote->f('free_field');
		$main_address_id = $quote->f('main_address_id');
		if($in['delivery_address_id']){
			$buyer_adr=$db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' and address_id='".$in['delivery_address_id']."'");
			$in['address_info']=$buyer_adr->f('address')."\n".$buyer_adr->f('zip').'  '.$buyer_adr->f('city')."\n".get_country_name($buyer_adr->f('country_id'));
		}
	}
   	if($in['currency_id'] != ACCOUNT_CURRENCY_TYPE){
   		
    	$params = array(
    		'currency' 					=> currency::get_currency($in['currency_id'],'code'),
			'acc' 						=> ACCOUNT_NUMBER_FORMAT,
			'into' 						=> currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code')
		);
		
    	$o['currency_rate'] 			= get_currency_convertor($params);
    }



  $o['main_address_id'] = $main_address_id;

	$o['our_ref'] = $in['our_ref'];
	$o['your_ref'] = $in['your_ref'];
	$o['add_customer']							= false;
	// $view->assign(array(
	$o['form_mode']                    			= 0; //0-add 1-edi;
	$o['customer_vat_dd']        				= build_vat_dd($in['customer_vat_id']);
	$o['customer_vat_id']        				= $in['customer_vat_id'];
	$o['payment_type_dd']      				 	= build_payment_type_dd($in['payment_type']);
	$o['payment_type_txt']       				= build_payment_type_dd($in['payment_type'],true);
	$o['payment_type']							= $db->f('payment_type') ? (string)$db->f('payment_type') : '';
	$o['vat_total']	                    		= number_format(0,2);
	$o['discount_percent']      				= '( '.$in['discount'].'% )';
	$o['del_note']           					= $in['del_note'];

	$o['discount_total']						= number_format(0,2);
	$o['total']									= number_format(0,2);
	$o['total_vat']								= number_format(0,2);
	$o['hide_currency2']						= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
	$o['hide_currency1']						= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
	$o['currency_type']							= get_commission_type_list($in['currency_type']);#ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$';
	$o['default_currency_val']  				= get_commission_type_list(ACCOUNT_CURRENCY_TYPE);
	$o['currency_type_val']						= $in['currency_id'];
	$o['checked']								= $in['rdy_invoice'] ? '"checked"':'';
	$o['name_txt']								= !$in['buyer_id'] ? 'Name:' : 'Company Name:';
	$o['customer_name']							= stripslashes($details['name']);
	$o['contact_name']							= $in['contact_name'];
	$o['ref_serial_number']						= $ref.$in['serial_number'];
	$o['ref_var']								= $ref ? " var ref= '".$ref."';" : " var ref;";
	$o['delivery_address']      				= $in['delivery_address'];
	$o['delivery_address_txt']     				= nl2br($in['delivery_address']);
	$o['delivery_address_id']     				= $in['delivery_address_id'];
	$o['customer_address']						= $buyer->f('address');
	$o['customer_zip']							= $buyer->f('zip');
	$o['customer_vat_number']					= $buyer_info->f('btw_nr');
	$o['customer_city']							= $buyer->f('city');
	$o['customer_country_id']					= $buyer->f('country_id');
	$o['customer_cell']							= !$in['buyer_id'] ? $buyer_info->f('cell') : '';
	$o['customer_phone']						= !$in['buyer_id'] ? $buyer_info->f('phone') : $buyer_info->f('comp_phone');
	$o['customer_email']						= !$in['buyer_id'] ? $buyer_info->f('email') : $buyer_info->f('c_email');
	$o['buyer_country']							= get_country_name($buyer->f('country_id'));
	$o['comp_name']								= $in['s_buyer_id'];
	$o['is_buyer_id']							= $in['s_buyer_id'] ? true : false;
	$o['remove_vat']							= $in['remove_vat'];
	$o['is_vat']								= $in['remove_vat'] == 1 ? false : true;
	$o['is_buyer']			    				= $in['buyer_id'] ? true : false;
	$o['show_vat_2']							= $in['remove_vat'] == 1 ? false : true;
	$o['show_vat']				      			= $in['show_vat'] == 1 ? true : false;
	$o['show_vat_txt']							= $in['show_vat'] == 1 ? gm('Yes') : gm('No');
	$o['address_info_txt']						= nl2br(trim($in['address_info']));
	$o['address_info']							= $in['address_info'];
	$o['APPLY_DISCOUNT_LIST']   				= build_apply_discount_list($in['apply_discount']);
	$o['APPLY_DISCOUNT_LIST_TXT']   			= build_apply_discount_list($in['apply_discount'],true);
	$o['DISCOUNT']              				= display_number($in['discount']);
	$o['discount']              				= display_number($in['discount']);
	$o['hide_global_discount']					= $in['apply_discount'] < 2 ? false : true;
	$o['hide_line_global_discount']				= $in['apply_discount'] == 1 ? '' : 'hide';
	$o['hide_disc']								= $in['apply_discount'] ==0 || $in['apply_discount'] == 2 ? false : true;
	$o['total_currency_hide']					= 'hide';
	$o['disc_line']								= display_number($buyer_info->f('line_discount'));
	$o['author_id']								= isset($in['author_id']) ? $in['author_id'] : $_SESSION['u_id'];
	$o['acc_manager']							= isset($in['author_id']) ? get_user_name($db->gf('author_id')) : get_user_name($_SESSION['u_id']);

	$o['acc_manager_txt']    					= $acc_manager[0];
  	$o['acc_manager_id']     					= $acc_manager_id[0];
  	$o['acc_manager_name']   					= $acc_manager[0];

	$o['multiple_identity_txt'] 				= $db->field("SELECT identity_name from multiple_identity WHERE identity_id='".$in['identity_id']."'");
	$o['multiple_identity_dd'] 					= build_identity_dd($in['identity_id']);
	$o['identity_id'] 							= $in['identity_id'] ? $in['identity_id'] : '0';
	$o['existIdentity']							= $existIdentity >0 ? true : false;
	$o['sameAddress']							= isset($in['sameAddress']) ? ($in['sameAddress'] ? true :false ) : ($in['delivery_address'] ? false : true);
	// ));
	if($o['identity_id'] != '0'){
		$mm = $db->query("SELECT * FROM multiple_identity WHERE identity_id='".$o['identity_id']."'")->getAll();
		$value = $mm[0];
		$template = $db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$o['identity_id']."' and module='order'");
		
		$o['main_comp_info']=array(
				'name'		=> $value['identity_name'],
				'address'	=> nl2br($value['company_address']),
				'zip'		=> $value['company_zip'],
				'city'		=> $value['city_name'],
				'country'	=> get_country_name($value['country_id']),
				'phone'		=> $value['company_phone'],
				'fax'		=> $value['company_fax'],
				'email'		=> $value['company_email'],
				'url'		=> $value['company_url'],
				'logo'		=> '../pim/admin/'.$value['company_logo'],
				);
		
	}

  	if($in['c_quote_id']){ //we don't care
	    
	    $in['purchase_price']=array();$in['article']=array();	 $in['article_code']=array(); $in['article_id']=array(); $in['quantity']=array();$in['quantity_old']=array();$in['is_tax']=array();$in['disc']=array();$in['content']=array();
	    $in['packing']=array();  $in['sale_unit']=array(); $in['vat_percent']=array();$in['price']=array();$in['price_vat']=array();$in['vat_value']=array();

	    $db_lines->query("SELECT tblquote_line.*, tblquote_version.active FROM tblquote_line
				            INNER JOIN tblquote_version ON tblquote_line.version_id=tblquote_version.version_id
				           WHERE tblquote_line.quote_id='".$in['c_quote_id']."'  AND tblquote_line.article_code!='' AND tblquote_version.active='1' ORDER BY id ASC");

	    while($db_lines->move_next()){
			array_push($in['article'],  (NOT_COPY_ARTICLE_ORD==1 && $db_lines->f('article_id')) ? get_article_label($db_lines->f('article_id'),$in['languages'],'order'):$db_lines->f('name'));
			array_push($in['article_code'],  $db_lines->f('article_code'));
			array_push($in['quantity'],  $db_lines->f('quantity'));
			array_push($in['quantity_old'],  $db_lines->f('quantity'));
			array_push($in['disc'],  $db_lines->f('line_discount'));

			array_push($in['sale_unit'],  $db_lines->f('sale_unit'));
			array_push($in['packing'],  $db_lines->f('package'));
			array_push($in['is_tax'],  0);
			array_push($in['content'],  0);
			$price=$db_lines->f('price');
			$db2->query("SELECT  pim_articles.article_id ,vat_id FROM  pim_articles WHERE article_id='".addslashes($db_lines->f('article_id'))." ' ");
			$db2->move_next();

				$purchase_price= $db3->field("SELECT pim_article_prices.purchase_price FROM pim_article_prices WHERE pim_article_prices.base_price=1  AND  pim_article_prices.article_id='".$db2->f('article_id')."'");
			array_push($in['purchase_price'], $purchase_price);

			array_push($in['article_id'],  $db2->f('article_id'));
			array_push($in['vat_percent'],$db_lines->f('vat'));
				array_push($in['price'], $price);

			if($in['remove_vat']) {
				array_push($in['price_vat'], $price);
			}else{
				array_push($in['price_vat'], $price + ($price * get_vat_val($db2->f('vat_id'))/100));
			}
			array_push($in['vat_value'],$price * (get_vat_val($db2->f('vat_id'))/100));
	    }

	    $in['discount_line_gen']= display_number($q_line_discount);
	    $view->assign(array(
	      'disc_line'				=> display_number($q_line_discount),
	    ));
	}

	$i=0;
	$total = 0;
	$total_with_vat = 0;
	$discount_total = 0;
	$vat_total = 0;
	$o['tr_id']=array();
	$global_disc = $in['discount'];
	// console::log($in['article']);
	if($in['article']){
		$in['remove_vat'] ? $vat_column = 0 : $vat_column = 1 ;
		$colspan_code = $vat_column.'-'.$use_package.'-'.$in['apply_discount'];
		switch ($colspan_code) {
			case '0-0-0':
			case '0-0-2':
			$cols = 6;
			$col = 6;
				break;
			case '1-0-0':
			case '0-0-1':
			case '0-0-3':
			case '1-0-2':
			case '0-1-0':
			case '0-1-2':
				$cols = 7;
				$col = 5;
				break;
			case '1-0-1':
			case '1-0-3':
			case '0-1-1':
			case '0-1-3':
			case '1-1-0':
			case '1-1-2':
				$cols = 8;
				$col = 4;
				break;
			case '1-1-1':
			case '1-1-3':
				$cols = 9;
				$col = 3;
				break;
		}
		foreach ($in['article'] as $row){
			$row_id = 'tmp'.$i;
			if(!ALLOW_ARTICLE_SALE_UNIT){
				$in['sale_unit'][$i]=1;
			}

			if(!ALLOW_ARTICLE_PACKING){
				$in['packing'][$i]=1;
				$in['sale_unit'][$i]=1;
			}

			$discount_line = $in['disc'][$i];
			if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
				$discount_line = 0;
			}
			if($in['apply_discount'] < 2){
				$global_disc = 0;
			}
			$q = $in['quantity'][$i] * $in['packing'][$i] / $in['sale_unit'][$i];

			$price_line = $in['price'][$i] - ($in['price'][$i] * $discount_line /100);

			$discount_total += $price_line * $global_disc / 100 * $q;
			
			$vat_total += ($price_line - $price_line * $global_disc /100) * $in['vat_percent'][$i] / 100 * $q;
			$line_total=$price_line * $q;

			$total += $line_total;

			if(!($in['is_tax'][$i])){
				 $in['is_tax'][$i]=0;
			}
			if(!$in['packing'][$i]){
				$in['packing'][$i]=1;
			}
			if(!$in['packing'][$i]){
				$in['sale_unit'][$i]=1;
			}
			$stock=$db2->field("SELECT stock from pim_articles WHERE article_id='".$in['article_id'][$i]."' ");
			$hide_stock=$db2->field("SELECT hide_stock from pim_articles WHERE article_id='".$in['article_id'][$i]."' ");
		    $threshold_value=$db2->field("SELECT article_threshold_value from pim_articles WHERE article_id='".$in['article_id'][$i]."' ");
		 	$pending_articles=$db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$in['article_id'][$i]."' AND delivered=1");

			$linie= array(
			'tr_id'         			=> $row_id,
			'article'  			    	=> $in['article'][$i],
			'content'  			    	=> $in['content'][$i],
			'colspan'					=> $in['content'][$i] ==1 ? 'colspan="8" class="last"' : '',
			'article_id'				=> $in['article_id'][$i],
			'tax_id'				    => $in['tax_id'][$i],
			'tax_for_article_id'	    => $in['is_tax'][$i]==1?$in['tax_for_article_id'][$i]:'',
			'is_tax'      	            => $in['is_tax'][$i],
			'quantity_old'      	    => $in['quantity_old'][$i],
			'quantity'      			=> display_number($in['quantity'][$i]),
			'price_vat'      			=> display_number($in['price_vat'][$i]),
			'price'         			=> display_number_var_dec($in['price'][$i]),
			'purchase_price'         	=> $in['purchase_price'][$i],
			'percent'					=> $in['vat_percent'][$i],
			'vat_value'					=> $in['vat_value'][$i],
			'percent_x'					=> display_number($in['vat_percent'][$i]),
			'vat_value_x'			    => display_number($in['vat_value'][$i]),
			'sale_unit_x'				=> $in['sale_unit'][$i],
			'packing_x'					=> remove_zero_decimals($in['packing'][$i]),
			'sale_unit'				    => $in['sale_unit'][$i],
			'packing'					=> remove_zero_decimals($in['packing'][$i]),
			'stock'				    	=> $stock,
			'pending_articles'          => intval($pending_articles),
			'threshold_value'           => intval($threshold_value),
			'hide_stock'           		=>  $hide_stock,
			'disc'				        => display_number($in['disc'][$i]),
			'line_total'				=> display_number($line_total),
			'td_width'					=> ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px',
			'input_width'				=> ALLOW_ARTICLE_PACKING ? 'width:164px' : 'width:223px',
			'article_code'				=> $in['article_code'][$i],
			'is_article_code'			=> $in['article_code'][$i] ? true : false,
			'colum'						=> $col
			);
			// $row['colum'] = $o['colum'];
			// $linie = $row;

			array_push($o['tr_id'], $linie);
			$i++;
		}
	}

	$total_with_vat=$vat_total+$total-$discount_total;
	if($in['remove_vat']){
		$total_with_vat = $total-$discount_total;
	}
	if($discount_total){
		$show_discount_total = true;
	}
	$total_currency = $total_with_vat * return_value($o['currency_rate']);
	// $view->assign(array(
	$o['discount_total']        					= display_number($discount_total);
	$o['vat_total']									= display_number($vat_total);
	$o['total']				    					= display_number($total);
	$o['total_vat']									= display_number($total_with_vat);
	$o['total_default_c']							= display_number($total_currency);
	$o['allow_article_packing']     				= ALLOW_ARTICLE_PACKING == 1 ? true : false;
  	$o['allow_article_sale_unit']   				= ALLOW_ARTICLE_SALE_UNIT == 1 ? true : false;
	$o['hide_add']				 					= 'hide';
	//$o['view_delivery']          					= trim($in['delivery_address']) ? true : false;
	/*$o['th_width']									= ALLOW_ARTICLE_PACKING ? 'width:208px' : 'width:267px';
	$o['td_width']									= ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px';
	$o['input_width']								= ALLOW_ARTICLE_PACKING ? 'width:164px' : 'width:223px';*/
	$o['shipping_price']							= display_number($shipping_price);
	$o['is_delivery_cost']							= $shipping_price > 0 ? true : false;
	// ));

}else {
	//EDIT
	$page_title = gm('Edit Order');

 	$do_next = 'order-norder-order-update_order';
	if($in['duplicate_order_id']){
		$in['order_id'] = $in['duplicate_order_id'];
		$page_title = gm('Duplicate Order');
		$do_next = 'order-norder-order-add_order';
	}
	$o['order_id'] = $in['order_id'];
  	$order_query = $db->query("SELECT pim_orders.* ,customer_contacts.firstname,customer_contacts.lastname, customer_contacts.customer_id as cust_id
	            FROM pim_orders
	            LEFT JOIN customer_contacts ON customer_contacts.contact_id=pim_orders.customer_id
	            WHERE pim_orders.order_id='".$in['order_id']."'
	           ");
  	$cancel_link = "index.php?do=order-order&order_id=".$in['order_id'];
  	if($order_query->f('shipping_price')){
		$shipping_price = $order_query->f('shipping_price');
	}else{
		$shipping_price = 0;
	}

	if(!$order_query->move_next()){
		$o = array('redirect'=>'orders');
		json_out($o);
		// return ark::run('order-orders');
	}
	if(!getItemPerm(array('module'=>'order','item'=>$order_query->f('author_id')))){
		$o = array('redirect'=>'orders');
		json_out($o);
		// return ark::run('order-orders');
	}

	if($order_query->f('field') == 'customer_id'){
		$customer_id = $order_query->f('customer_id');
		$o['is_company']=true;
		$o['is_buyer']=true;
		$in['buyer_id']      	= $order_query->f('customer_id');
		$o['buyer_id'] = $in['buyer_id'];
	}else{
		$o['is_company']=false;
		$o['is_buyer']=false;
	}

	if ($order_query->f('customer_id'))
	{
		$price_category_id = $db2->field('SELECT cat_id FROM customers WHERE customer_id ='.$order_query->f('customer_id'));
	}
	else
	{
		$price_category_id =0;
	}

	$ref='';
	if(ACCOUNT_ORDER_REF && $order_query->gf('buyer_ref')){
		$ref = $order_query->gf('buyer_ref');
	}

	if($order_query->f('use_package')){
		$in['allow_article_packing']=1;
	}else{
		$in['allow_article_packing']=0;
		$in['allow_article_sale_unit']=0;
	}
	if($order_query->f('use_sale_unit')){
	   $in['allow_article_sale_unit']=1;
	}else{
	   $in['allow_article_sale_unit']=0;
	}
	$o['main_address_id']      				= $order_query->gf('main_address_id');
	$in['contact_id']      				= $order_query->gf('contact_id');
	$o['contact_id']      				= $order_query->gf('contact_id');
  	$in['delivery_id']      			= $order_query->gf('delivery_id');
  	$o['delivery_id']      				= $order_query->gf('delivery_id');
	$in['contact_name']      			= $order_query->gf('contact_name');
	$in['customer_id']      			= $order_query->gf('customer_id');
	$o['customer_id']      				= $order_query->gf('customer_id');
	$in['price_category_id']  			= $price_category_id;
	$o['price_category_id']   			= $price_category_id;
	$in['s_customer_name']  			= $order_query->f('firstname')." ".$order_query->f('lastname');
	$o['s_customer_name']  				= $order_query->f('firstname')." ".$order_query->f('lastname');
	$in['serial_number']    			= $order_query->gf('serial_number');

	$in['payment_term']     			= $order_query->gf('payment_term');
	$o['payment_term']     				= $order_query->gf('payment_term');
	$in['date']             			= date(ACCOUNT_DATE_FORMAT,$order_query->f('date'));
	$o['date']             				= date(ACCOUNT_DATE_FORMAT,$order_query->f('date'));
	$in['date_h']           			= $order_query->f('date');
	$o['date_h']           				= $order_query->f('date')*1000;
	$in['delivery_date']        		= $order_query->f('del_date') ? date(ACCOUNT_DATE_FORMAT,$order_query->f('del_date')) : "";
	$o['delivery_date']        			= $order_query->f('del_date') ? date(ACCOUNT_DATE_FORMAT,$order_query->f('del_date')) : "";
	$in['del_date']           			= $order_query->f('del_date') ? $order_query->f('del_date') : '';
	$o['del_date']           			= $order_query->f('del_date') ? $order_query->f('del_date')*1000 : '';
	$in['del_note']           			= $order_query->f('del_note');
	$o['del_note']           			= $order_query->f('del_note');
	$in['field']	           			= $order_query->f('field');
	$o['field']	           				= $order_query->f('field');
	$in['discount']	           			= display_number($order_query->f('discount'));
	$o['discount']	           			= display_number($order_query->f('discount'));
	$in['currency_name']    			= build_currency_name_list($order_query->f('currency_type'));
	$o['currency_name']    				= build_currency_name_list($order_query->f('currency_type'));
	$in['buyer_ref']					= $order_query->gf('buyer_ref');
	$o['buyer_ref']						= $order_query->gf('buyer_ref');
	$in['currency_rate']    			= $order_query->f('currency_rate');
	$o['currency_rate']    				= $order_query->f('currency_rate');
	$o['currency_type_val']				= $order_query->f('currency_type');
	// $in['notes']		        = $order_query->f('notes');
	$in['our_ref']								= $order_query->f('our_ref');
	$o['our_ref']									= $order_query->f('our_ref');
	$in['your_ref']								= $order_query->f('your_ref');
	$o['your_ref']								= $order_query->f('your_ref');
	$in['remove_vat']							= $order_query->f('remove_vat');
	$o['remove_vat']							= $order_query->f('remove_vat');
	$in['languages']            	= $order_query->f('email_language');
	$o['languages']            		= $order_query->f('email_language');

	$in['apply_discount']					= $order_query->f('apply_discount');
	$o['apply_discount']					= $order_query->f('apply_discount');
	$in['discount_line_gen']			= display_number($order_query->gf('discount_line_gen'));
	$o['discount_line_gen']			=	 display_number($order_query->gf('discount_line_gen'));
	if($in['duplicate_order_id']){
		$in['serial_number'] = generate_order_serial_number(DATABASE_NAME);
	}
	$o['serial_number']    				= $order_query->gf('serial_number');

	$existIdentity = $db2->field("SELECT COUNT(identity_id) FROM multiple_identity ");

	// $view->assign(array(
	$o['form_mode']             				= 1; //0-add 1-edi;
	$o['customer_vat_dd']       				= build_vat_dd($order_query->f('customer_vat_id'));
	$o['payment_type_dd']       				= build_payment_type_dd($order_query->f('payment_type'));
	$o['payment_type_txt']       				= build_payment_type_dd($order_query->f('payment_type'),true);
	$o['payment_type']							= $order_query->f('payment_type');
	$o['hide_currency2']						= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
	$o['hide_currency1']						= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
	$o['currency_type']							= get_commission_type_list($order_query->f('currency_type'));#ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$';
	$o['default_currency_val']  				= get_commission_type_list(ACCOUNT_CURRENCY_TYPE);
	$o['checked']								= $order_query->f('rdy_invoice') ? 'checked="checked"':'';

	$o['customer_name']							= $order_query->f('customer_name');
	$o['customer_email']     					= $order_query->f('customer_email');
	$o['contact_name']							= $in['contact_id'] ? $db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$in['contact_id']}' ") : $order_query->f('contact_name');
	$o['customer_city']     					= $order_query->f('customer_city');
	$o['customer_zip']      					= $order_query->f('customer_zip');
	$o['customer_address']  					= $order_query->f('customer_address');
	$o['customer_phone']    					= $order_query->f('customer_phone');
	$o['customer_cell']     	 				= $order_query->f('customer_cell');
	$o['buyer_country']								= get_country_name($order_query->f('customer_country_id'));
	$o['customer_country_id']       	= $order_query->f('customer_country_id');
	$o['delivery_address_txt']     		= nl2br($order_query->f('delivery_address'));
	$o['delivery_address']     				= $order_query->f('delivery_address');
	//$o['view_delivery']             	= trim($order_query->gf('delivery_address')) ? true : false;
	$o['customer_vat_number']					= $order_query->gf('customer_vat_number');
	$o['name_txt']										= 'Name:';
	$o['ref_serial_number']						= $ref.$order_query->gf('serial_number');
	$o['ref_var']											= $order_query->gf('buyer_ref') ? " var ref= '".$order_query->gf('buyer_ref')."';" : " var ref;";
	$o['discount_percent']         		= '( '.$in['discount'].'% )';
	// 'sh_discount'		        => ($order_query->f('discount')==0.00)?'hide':'',
	$o['allow_article_packing']     	= $order_query->f('use_package') == 1 ? true :false ;
	$o['allow_article_sale_unit']   	= $order_query->f('use_sale_unit') == 1 ? true : false;
	$o['is_vat']											= $in['remove_vat'] == 1 ? false : true;
	$o['show_vat_2']									= $in['remove_vat'] == 1 ? 'hide' : '';
	$o['show_vat']										= $order_query->gf('show_vat') == 1 ? true : false;
	$o['show_vat_checked']          	= $order_query->gf('show_vat') == 1 ? 'CHECKED' : '';
	$o['show_vat_txt']								= $order_query->gf('show_vat') == 1 ? gm('Yes') : gm('No');
	// $o['address_info']          			= nl2br($order_query->gf('address_info'));
	 $address_info = $order_query->f('buyer_address').'
		'.$order_query->f('buyer_zip').' '.$order_query->f('buyer_city').'
		'.get_country_name($order_query->f('buyer_country_id'));

	if($in['contact_id'] && $in['change']){
		$contact_title = $db->field("SELECT customer_contact_title.name FROM customer_contact_title
								INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
								WHERE customer_contacts.contact_id='".$in['contact_id']."'");
		if($contact_title)
		{
			$contact_title .= " ";
		}
		$buyer = $db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' ORDER BY is_primary DESC");
		$buyer->next();
		$address_info  = $buyer->f('address')."\n".$buyer->f('zip').'  '.$buyer->f('city')."\n".get_country_name($buyer->f('country_id'));
		
    }

		if($order_query->f('apply_discount')==0){
			$discount_line=0;
			$discount_global=0;
		}elseif($order_query->f('apply_discount')==1){
			$discount_line=1;
			$discount_global=0;
		}elseif($order_query->f('apply_discount')==2){
			$discount_line=0;
			$discount_global=1;
		}elseif($order_query->f('apply_discount')==3){
			$discount_line=1;
			$discount_global=1;
		}
   
		

	$o['address_info_txt']						= $order_query->f('address_info') ? nl2br($order_query->f('address_info')) : nl2br($address_info); 
	$o['address_info']							= trim($order_query->f('address_info')) ? nl2br($order_query->f('address_info')) : nl2br($address_info);

	$o['apply_discount_line']      						= $discount_line;
	$o['apply_discount_global']     					= $discount_global;
	$o['th_width']										= $in['allow_article_sale_unit'] ? 'width:208px' : 'width:267px';
	$o['td_width']										= $in['allow_article_sale_unit'] ? 'width:218px' : 'width:277px';
	$o['input_width']									= $in['allow_article_sale_unit'] ? 'width:164px' : 'width:223px';
	$o['author_id']										= $order_query->gf('author_id') ? $order_query->gf('author_id') : '';
	$o['acc_manager']									= $order_query->gf('author_id') ? get_user_name($order_query->gf('author_id')) : '';
	$o['APPLY_DISCOUNT_LIST']       					= build_apply_discount_list($in['apply_discount']);
	$o['APPLY_DISCOUNT_LIST_TXT']     					= build_apply_discount_list($in['apply_discount'],true);
	$o['DISCOUNT']              						= display_number($in['discount']);
	$o['hide_disc']										= $in['apply_discount'] ==0 || $in['apply_discount'] == 2 ? false : true;
	$o['hide_global_discount']							= $in['apply_discount'] < 2 ? false : true;
	$o['hide_line_global_discount']						= $in['apply_discount'] == 1 ? '' : 'hide';
	$o['total_currency_hide']							= $in['currency_rate'] ? true : false;
	$o['disc_line']										= display_number($order_query->f('discount_line_gen'));
	$o['is_duplicate']			 						= false;
	$o['acc_manager_id']     							= $order_query->gf('acc_manager_id');
	$o['acc_manager_name']   							= $order_query->gf('acc_manager_name');
	$o['multiple_identity_txt'] 						= $db2->field("SELECT identity_name from multiple_identity WHERE identity_id='".$order_query->gf('identity_id')."'");
	$o['multiple_identity_dd'] 							= build_identity_dd($order_query->gf('identity_id'));
	$o['existIdentity']									= $existIdentity >0 ? true : false;
  	$o['identity_id'] 									= $order_query->gf('identity_id') ? $order_query->gf('identity_id') : '0';
  	
  	$o['sameAddress']			= !$order_query->f('same_address') ? true : false;
  	$in['delivery_address_id']							= $order_query->gf('delivery_address_id');
  	$o['delivery_address_id']							= $order_query->gf('delivery_address_id');

  	if($o['identity_id'] != '0'){
		$mm = $db->query("SELECT * FROM multiple_identity WHERE identity_id='".$o['identity_id']."'")->getAll();
		$value = $mm[0];
		$template = $db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$o['identity_id']."' and module='order'");
		
		$o['main_comp_info']=array(
				'name'		=> $value['identity_name'],
				'address'	=> nl2br($value['company_address']),
				'zip'		=> $value['company_zip'],
				'city'		=> $value['city_name'],
				'country'	=> get_country_name($value['country_id']),
				'phone'		=> $value['company_phone'],
				'fax'		=> $value['company_fax'],
				'email'		=> $value['company_email'],
				'url'		=> $value['company_url'],
				'logo'		=> '../pim/admin/'.$value['company_logo'],
				);
		
	}

	// ));
	$use_package = $order_query->f('use_package');

	$show_shipping_price = $order_query->gf('show_shipping_price');

	$i=0;
	$discount_total = 0;
	$show_discount_total = false;
	$vat_value = 0;
	$global_disc = $in['discount'];
	$old_currency_type = $in['currency_type'];

	// $use_package 			= $order_query->f('use_package');

	$in['remove_vat'] ? $vat_column = 0 : $vat_column = 1 ;
	$colspan_code = $vat_column.'-'.$use_package.'-'.$in['apply_discount'];
	switch ($colspan_code) {
		case '0-0-0':
		case '0-0-2':
		$cols = 6;
		$col = 6;
			break;
		case '1-0-0':
		case '0-0-1':
		case '0-0-3':
		case '1-0-2':
		case '0-1-0':
		case '0-1-2':
			$cols = 7;
			$col = 5;
			break;
		case '1-0-1':
		case '1-0-3':
		case '0-1-1':
		case '0-1-3':
		case '1-1-0':
		case '1-1-2':
			$cols = 8;
			$col = 4;
			break;
		case '1-1-1':
		case '1-1-3':
			$cols = 9;
			$col = 3;
			break;
	}

	$o['colum'] = $col;

	$o['tr_id']=array();
	$last_tr = '';
	$db->query("SELECT pim_order_articles.* FROM pim_order_articles WHERE order_id='".$in['order_id']."' ORDER BY sort_order ASC");
	while ($db->move_next())
	{
		if($in['duplicate_order_id']){

			if($in['change']){
				$in['buyer_id'] = $in['customer_id'];
				$customer_id = $in['buyer_id'];
			}
			// echo $db->f('article_id').' '.$customer_id.' '.$contact_id;
			$contact_id = $in['contact_id'];


			$article_data = get_customer_article_data($db->f('article_id'),$customer_id,$contact_id,$old_currency_type);

			$in['languages']			= $article_data['languages'];
			$in['currency_rate']    	= $article_data['currency_rate'];
			// $in['currency_type']		= $article_data['currency_type'];
			$in['remove_vat'] 		= $article_data['remove_vat'];


			$global_disc = $article_data['global_discount'];
			if(!$show_discount_total && $article_data['line_discount']){
				$show_discount_total = true;
			}
			$discount_line = $article_data['line_discount'];

			$in['apply_discount'] = $article_data['apply_discount'];

			if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
				$discount_line = 0;
			}
			if($in['apply_discount'] < 2){
				$global_disc = 0;
			}

			$in['discount_line_gen'] = display_number($article_data['line_discount']);
			$in['discount'] = display_number($article_data['global_discount']);

			$q = $db->f('quantity') * $article_data['packing'] / $article_data['sale_unit'];
			$row_id = 'tmp'.$i;
			$price_line = $article_data['price'] - ($article_data['price'] * $discount_line /100);
			$discount_total += $price_line * $global_disc / 100 * $q;

			$vat_total += ($price_line - $price_line * $global_disc /100) *$article_data['vat_value'] / 100 * $q;
			$line_total=$price_line * $q;
			// echo $price_line.'<br>';
			$total += $line_total;
		}else{

			if(!$show_discount_total && $db->f('discount') !=0){
				$show_discount_total = true;
			}
			$discount_line = $db->f('discount');
			if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
				$discount_line = 0;
			}
			if($in['apply_discount'] < 2){
				$global_disc = 0;
			}
			$q = @($db->f('quantity') * $db->f('packing') / $db->f('sale_unit'));
			$row_id = 'tmp'.$i;
			$price_line = $db->f('price') - ($db->f('price') * $discount_line /100);
			$discount_total += $price_line * $global_disc / 100 * $q;

			$vat_total += ($price_line - $price_line * $global_disc /100) *$db->f('vat_percent') / 100 * $q;
			$line_total=$price_line * $q;


			$total += $line_total;
		}

		if($db->f('article_id')){
			$stock = $db2->field("SELECT stock from pim_articles WHERE article_id='".$db->f('article_id')."' ");
	 		$pending_articles = $db2->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$db->f('article_id')."' AND delivered=0");
	    	$threshold_value = $db2->field("SELECT article_threshold_value from pim_articles WHERE article_id='".$db->f('article_id')."'");
	    	$hide_stock = $db2->field("SELECT hide_stock from pim_articles WHERE article_id='".$db->f('article_id')."'");
		}
		// echo $in['remove_vat'].','.$use_package.'<br>';

		if(!$db->f('tax_id')){
			$last_tr = $row_id;
		}

		$linie = array(
			'tr_id'         					=> $row_id,
			'article'  			    			=> html_entity_decode($db->f('article')),
	    	'article_code'						=> $db->f('article_code'),
	    	'is_article_code'					=> $db->f('article_code') ? true : false,
			'article_id'							=> $db->f('article_id')?$db->f('article_id'):$db->f('tax_id'),
			'tax_for_article_id'	    => $db->f('tax_for_article_id'),
			'is_article'      	      => $db->f('article_id') ? true : false,
			'is_article1'      	      => $db->f('article_id') ? true : false,
			'is_tax'      	          => $db->f('tax_id')?1:0,
			'for_article'							=> $db->f('tax_id') ? $last_tr : '',
			'quantity_old'      	    => $db->f('quantity'),
			'quantity'      					=> display_number($db->f('quantity')),
			'price_vat'      					=> display_number($db->f('price_with_vat')),
			'price'         					=> display_number_var_dec($db->f('price')),
			'purchase_price'         	=> $db->f('purchase_price'),
			'percent_x'								=> display_number($db->f('vat_percent')),
			'percent'									=> $db->f('vat_percent'),
			'vat_value_x'							=> display_number($db->f('vat_value')),
			'vat_value'								=> $db->f('vat_value'),
		  	'sale_unit_x'							=> $db->f('sale_unit'),
			'packing_x'								=> remove_zero_decimals($db->f('packing')),
			'sale_unit'				    		=> $db->f('sale_unit'),
			'stock'				    				=> $stock,
			'pending_articles'        => intval($pending_articles),
			'threshold_value'         => intval($threshold_value),
			'hide_stock'              => $hide_stock,
			'packing'									=> remove_zero_decimals($db->f('packing')),
			'content'									=> $db->f('content') ? true : false,
			'colspan'									=> $db->f('content') ? ' colspan='.$cols.' class="last" ' : '',
			'disc'										=> display_number($db->f('discount')),
			'content_class'						=> $db->f('content') ? ' type_content ' : '',
	    	'line_total'							=> display_number($line_total),
	    	'th_width'								=> $in['allow_article_sale_unit'] ? 'width:208px' : 'width:267px',
			'td_width'								=> $in['allow_article_sale_unit'] ? 'width:218px' : 'width:277px',
			'input_width'							=> $in['allow_article_sale_unit'] ? 'width:164px' : 'width:223px',
			'colum'										=> $col,
		);
		//reset prices for duplicate order depending of new article prices and price category of the company
		/*if($in['duplicate_order_id']){
			$view->assign(array(
				'tr_id'         			=> $row_id,
				'article'  			    	=> $article_data['content'] == 1 ? htmlspecialchars(html_entity_decode($db->f('article'))) : htmlspecialchars(html_entity_decode($article_data['article'])),
				'colspan'					=> $article_data['content'] == 1 ? ' colspan='.$cols.' class="last" ' : '',
		        'article_code'				=> $article_data['article_code'],
		        'is_article_code'			=> $article_data['article'] ? true : false,
				'price_vat'      			=> display_number($article_data['price_with_vat']),
				'price'         			=> display_number_var_dec($article_data['price']),
				'vat_value_x'				=> display_number($article_data['vat_value']),
				'vat_value'					=> $article_data['vat_value'],
			    'sale_unit_x'				=> $article_data['sale_unit'],
				'packing_x'					=> remove_zero_decimals($article_data['packing']),
				'sale_unit'					=> $article_data['sale_unit'],
				'stock'						=> $article_data['stock'],
				'pending_articles'         	=> intval($article_data['pending_articles']),
				'hide_stock'               	=> $article_data['hide_stock'],
				'packing'					=> remove_zero_decimals($article_data['packing']),
				'line_total'				=> display_number($line_total),
				'threshold_value'          	=> intval($article_data['threshold_value']),
				'disc'						=> display_number($article_data['line_discount']),
			),'article_line');

			$view->assign(array(
					'discount_percent'      => '( '.$article_data['global_discount'].'% )',
					'disc_line'				=> display_number($article_data['line_discount']),
					'is_duplicate'			=> true,
					'currency_type'			=> get_commission_type_list($in['currency_type']),
					'total_currency_hide'		=> $in['currency_rate'] ? '' : 'hide',
					'customer_name'			=> $article_data['customer_name'],
					'address_info'          	=> $article_data['address_info'],
					'APPLY_DISCOUNT_LIST'       	=> build_apply_discount_list($article_data['apply_discount']),
					'APPLY_DISCOUNT_LIST_TXT'       	=> build_apply_discount_list($article_data['apply_discount'],true),
					'DISCOUNT'              	=> display_number($in['discount']),
					'discount_line_gen'		=> $article_data['line_discount'],
					'is_vat'				=> $in['remove_vat'] == 1 ? false : true,
					'hide_disc'				=> $in['apply_discount'] ==0 || $in['apply_discount'] == 2 ? 'hide' : '',
					'show_vat'					=> $in['remove_vat'] == 1 ? 'hide' : '',
			));
		}*/
		// $view->loop('article_line');
		array_push($o['tr_id'], $linie);
		$i++;

	}
	$total = round($total,2);
	$vat_total = round($vat_total,2);
	$total_with_vat=$vat_total+$total-$discount_total+$shipping_price;

	$total_currency = $total_with_vat * return_value($in['currency_rate']);
	// $view->assign(array(
	$o['discount_total']        	= display_number($discount_total);
	$o['vat_total']					= display_number($vat_total);
	$o['total']				    	= display_number($total);
	$o['total_vat']					= $in['remove_vat'] ? display_number($total-$discount_total+$shipping_price) : display_number($total_with_vat);
	$o['total_default_c']			= display_number($total_currency);
	$o['is_delivery_cost']			= $shipping_price > 0 ? true : false;
	$o['shipping_price']			= display_number($shipping_price);
	$o['show_shipping_price']		= $show_shipping_price;
	$o['shipping_price_txt'] 		= $show_shipping_price == 1 ? display_number($shipping_price) : '-';
	$o['show_if_1']					= $show_shipping_price == 2 ? 'hide' : '';
	$o['hide_shipping']				= $show_shipping_price == 2 ? 'hide' : '';
	$o['show_shipping']				= $show_shipping_price == 1 ? 'hide' : '';
	// ));

}
$default_note = $db->field("SELECT value FROM default_data WHERE type='order_note'");

$is_extra = false;
if($customer_id){
	$c_details = $db->query("SELECT * FROM customers WHERE customer_id ='".$customer_id."' ");

	if($c_details->f('bank_name') || $c_details->f('bank_bic_code') || $c_details->f('bank_iban') ){
		$extra_info .= '<h4 >'.gm('Bank Details').'</h4>
	<p><strong>'.gm('Name').': </strong> <span>'.$c_details->f('bank_name').'</span></p>
	<p><strong>'.gm('BIC Code').': </strong> <span>'.$c_details->f('bank_bic_code').'</span></p>
	<p><strong>'.gm('IBAN').': </strong> <span>'.$c_details->f('bank_iban').'</span></p>
	';
	}
	if($extra_info){
		$is_extra = true;
		$extra_info = '<div style="width: 924px;" >'.$extra_info.'</div>';
	}
}

// $view->assign(array(
	$o['notes']                     = $in['notes'] ? $in['notes'] : ($default_note ? $default_note : '');
	$o['pick_date_format']          = pick_date_format(ACCOUNT_DATE_FORMAT);
	$o['style']                     = ACCOUNT_NUMBER_FORMAT;
	$o['page_title']			    = $page_title;
	$o['do_next']	    		    = $do_next;
	// 'total_currency_hide'		=> 'hide',
	$o['extra_info']				= $extra_info;
	$o['is_extra']					= $is_extra;
	$o['allow_stock']               = ALLOW_STOCK;
	$o['stock_multiple_location']   = STOCK_MULTIPLE_LOCATIONS;
	$o['show_stock_warning']        = SHOW_STOCK_WARNING;
// ));

////////// multilanguage for order note ////////////////

$transl_lang_id_active 		= $in['languages'];
if(!$transl_lang_id_active){
	$transl_lang_id_active = $in['email_language'];
}
// console::log('lang_d2 '.$transl_lang_id_active);
$o['translate_loop'] = array();
$langs = $db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");
while($langs->next()){
	if((!$in['order_id']  ||  $in['c_quote_id']) || $in['quote_id'] || $in['order_id'] =='tmp'){
		if($langs->f('lang_id')==1){
			$note_type = 'order_note';
		}else{
			$note_type = 'order_note_'.$langs->f('lang_id');
		}
		$order_note = $db3->field("SELECT value FROM default_data
								   WHERE default_name = 'order note'
								   AND type = '".$note_type."' ");
		if($in['c_quote_id']){
			$transl_lang_id_active = $db3->field("SELECT lang_id FROM note_fields WHERE item_id = '".$in['c_quote_id']."' AND version_id = '".$version_id."' AND active = 1 AND item_type = 'quote' ");
		}
	}else{
			$order_note = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type 	= 'order'
								   AND 	item_name 	= 'notes'
								   AND 	item_id 	= '".$in['order_id']."'
								   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

			$transl_lang_id_active = $db3->field("SELECT lang_id FROM note_fields
								   WHERE item_type 	= 'order'
								   AND 	item_name 	= 'notes'
								   AND 	active 		= '1'
								   AND 	item_id 	= '".$in['order_id']."' ");
			if($in['duplicate_order_id']){
				$transl_lang_id_active = $in['languages'];
			}
			if(!$order_note && !$transl_lang_id_active){
				$order_from_front = $db3->field("SELECT order_email FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
			}
			if($order_from_front){
				if($langs->f('lang_id')==1){
					$note_type = 'order_note';
				}else{
					$note_type = 'order_note_'.$langs->f('lang_id');
				}
				$order_note = $db3->field("SELECT value FROM default_data
										   WHERE default_name = 'order note'
										   AND type = '".$note_type."' ");
				$transl_lang_id_active = $db3->field("SELECT lang_id FROM pim_lang WHERE sort_order = 1");
				$in['order_from_front'] = 1;

			}


	}
	$translate_lang = 'form-language-'.$langs->f('code');
	if($langs->f('code')=='du'){
		$translate_lang = 'form-language-nl';
	}

	if($transl_lang_id_active != $langs->f('lang_id')){
		$translate_lang = $translate_lang.' hidden';
	}

	array_push($o['translate_loop'], array(
		'translate_cls' 	=> 		$translate_lang,
		'lang_id' 			=> 		$langs->f('lang_id'),
		'notes'				=> 		$order_note,
	));

}
$transl_lang_id_active_custom 		= $in['languages'];
if(!$transl_lang_id_active_custom){
	$transl_lang_id_active_custom = $in['email_language'];
}
$o['translate_loop_custom'] = array();
#custom extra languages
$custom_langs = $db->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
while($custom_langs->next()) {
	if(!$in['order_id'] || $in['c_quote_id'] || $in['quote_id'] || $in['order_id'] =='tmp') {
		$custom_note_type = 'order_note_'.$custom_langs->f('lang_id');
		$order_note_custom = $db3->field("SELECT value FROM default_data WHERE default_name = 'order note' AND type = '".$custom_note_type."' ");
		if($in['c_quote_id']) {
			$transl_lang_id_active_custom = $db3->field("SELECT lang_id FROM note_fields WHERE item_id = '".$in['c_quote_id']."' AND version_id = '".$version_id."' AND active = 1 AND item_type = 'quote'");
		}
	} else {
		$order_note_custom = $db3->field("SELECT item_value FROM note_fields
										WHERE item_type 	= 'order'
										AND item_name 		= 'notes'
										AND item_id			= '".$in['order_id']."'
										AND lang_id 		= '".$custom_langs->f('lang_id')."'");
		$transl_lang_id_active_custom = $db3->field("SELECT lang_id FROM note_fields
													WHERE item_type 	= 'order'
													AND item_name 		= 'notes'
													AND active 			= '1'
													AND item_id 		= '".$in['order_id']."' ");
		if($in['duplicate_order_id']) {
			$transl_lang_id_active_custom = $in['languages'];
		}

		if(!$order_note_custom && $transl_lang_id_active_custom) {
			$order_from_front_cust = $db3->field("SELECT order_email FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
		}
		if($order_from_front_cust) {
			$custom_note_type = 'order_note_'.$custom_langs->f('lang_id');
			$order_note_custom = $db3->field("SELECT value FROM default_data WHERE default_name = 'order note' AND type = '".$custom_note_type."' ");
			if(!$transl_lang_id_active_custom){
				$transl_lang_id_active_custom = $db3->field("SELECT lang_id FROM pim_custom_lang WHERE sort_order = 1");
			}
			$in['order_from_front'] = 1;
		}
	}
	$translate_lang_custom = 'form-language-'.$custom_langs->f('code');

	if($transl_lang_id_active_custom != $custom_langs->f('lang_id')) {
		$translate_lang_custom = $translate_lang_custom.' hidden';
	}

	array_push($o['translate_loop_custom'], array(
		'translate_cls'		=> $translate_lang_custom,
		'lang_id'			=> $custom_langs->f('lang_id'),
		'notes'				=> $order_note_custom
	));
}

/*$translate_lang_active = $db->field("SELECT code FROM pim_lang WHERE lang_id = '".$transl_lang_id_active."'");
$translate_lang_active = 'form-language-'.$translate_lang_active;
if($translate_lang_active=='form-language-du'){
	$translate_lang_active = 'form-language-nl';
}*/

if($transl_lang_id_active>=1000) {
	$lang_code = $db->field("SELECT language FROM pim_custom_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
	$code = $db->field("SELECT code FROM pim_custom_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
} else {
	$lang_code = $db->field("SELECT language FROM pim_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
	$code = $db->field("SELECT code FROM pim_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
}

$in['only_contact_name'] = 1;
$in['title'] = 1;
// console::log('lang_d '.$transl_lang_id_active);
$o['translate_lang_active']							= 'form-language-'.$code;
$o['language_dd'] 									= build_language_dd_new($transl_lang_id_active);
$o['email_language'] 								= $transl_lang_id_active;
$o['langs'] 										= $transl_lang_id_active;
$o['order_from_front']								= $in['order_from_front'];
$o['nr_decimals'] 									= ARTICLE_PRICE_COMMA_DIGITS;
$o['cancel_link']									= $cancel_link;
$o['language_txt']									= gm($lang_code);
$o['style']  										= ACCOUNT_NUMBER_FORMAT;
$o['authors']										= get_author($in,true,false);
$o['accmanager']									= get_accmanager($in,true,false);
$o['contacts']										= get_contacts($in,true,false);
$o['customers']										= get_customers($in,true,false);
$o['addresses']										= get_addresses($in,true,false);
$o['cc']											= get_cc($in,true,false);
$o['articles_list']									= get_articles_list($in,true,false);
$o['country_dd']									= build_country_list();
$o['main_country_id']								= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';



json_out($o);

function get_author($in,$showin=true,$exit=true){

	$q = strtolower($in["term"]);

	global $database_config;

	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$db_users = new sqldb($database_users);

	$filter = '';

	if($q){
	  $filter .=" AND users.first_name LIKE '".$q."%'";
	}
	$db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role
									FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
									WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ");

	$items = array();
	while($db_users->move_next()){
		array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>$db_users->f('first_name').' '.$db_users->f('last_name') ) );
	}
	return json_out($items, $showin,$exit);

}
function get_accmanager($in,$showin=true,$exit=true){

	$q = strtolower($in["term"]);

	global $database_config;

	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$db_users = new sqldb($database_users);

	$filter = '';

	if($q){
	  $filter .=" AND users.first_name LIKE '".$q."%'";
	}

	$db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type
										FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
										WHERE database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY first_name ");

	$items = array();
	while($db_users->move_next()){
		array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>$db_users->f('first_name').' '.$db_users->f('last_name') ) );
	}
	return json_out($items, $showin,$exit);

}
function get_contacts($in,$showin=true,$exit=true){

	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db2 = new sqldb();
	$db_user = new sqldb($database_users);
	$filter .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";

	if($in['current_id']){
		$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
	}

	if($in['customer_id']){
		$filter .= " AND customer_contacts.customer_id='".$in['customer_id']."'";
	}
	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){

		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$items = array();
	$db= new sqldb();

	$title = array();
	$titles = $db2->query("SELECT * FROM customer_contact_title ")->getAll();
	foreach ($titles as $key => $value) {
		$title[$value['id']] = $value['name'];
	}

	$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
				customers.name, customers.currency_id,customers.internal_language,customer_contacts.email,customer_contacts.title,country.name AS country_name, customer_contacts.position_n
				FROM customer_contacts
				LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id
				LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
				LEFT JOIN country ON country.country_id=customer_contact_address.country_id
				WHERE customer_contacts.active=1 $filter ORDER BY lastname limit 5")->getAll();
	$result = array();
	foreach ($contacts as $key => $value) {
		$contact_title = $title[$value['title']];
    	if($contact_title){
			$contact_title .= " ";
    	}		

	    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
	    $price_category = "1";
	    if ($value['customer_id']){
	  		$price_category = $value['cat_id'];
	  	}

		$result[]=array(
			'symbol'				=> '',
			"id"					=> $value['contact_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
			"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
			"email"					=> $value['email'],
			"price_category_id"		=> $price_category, 
			'customer_id'	 		=> $value['customer_id'], 
			'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
			'currency_id' 			=> $value['currency_id'], 
			"lang_id" 				=> $value['internal_language'], 
			'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
			'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
			'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
			'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
			'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']) 

		);

	}

	$added = false;
	if(count($result)==5){
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999'));
		}
		$added = true;
	}
	if($in['contact_id']){
		$cust = $db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
				customers.name, customers.currency_id,customers.internal_language,customer_contacts.email,customer_contacts.title,country.name AS country_name, customer_contacts.position_n
				FROM customer_contacts
				LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id
				LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
				LEFT JOIN country ON country.country_id=customer_contact_address.country_id
				WHERE customer_contacts.active=1 AND customer_contacts.contact_id='".$in['contact_id']."' ORDER BY lastname ")->getAll();
		$value = $cust[0];
		$contact_title = $title[$value['title']];
    	if($contact_title){
			$contact_title .= " ";
    	}		

	    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
	  	$price_category = "1";
		if ($value['customer_id']){
	  		$price_category = $value['cat_id'];
	  	}
	  	
		$result[]=array(
			"id"					=> $value['contact_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
			"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
			"email"					=> $value['email'],
			"price_category_id"		=> $price_category, 
			'customer_id'	 		=> $value['customer_id'], 
			'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
			'currency_id' 			=> $value['currency_id'], 
			"lang_id" 				=> $value['internal_language'], 
			'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
			'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
			'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
			'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
			'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']) 
		);
	}
	if(!$added){
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999'));
		}
	}
	
	return json_out($result, $showin,$exit);
}

function  get_notes($in,$showin=true,$exit=true){
	$db=new sqldb();
	$o = array();
	$transl_lang_id_active 		= $in['languages'];
	if(!$transl_lang_id_active){
		$transl_lang_id_active = $in['email_language'];
	}
	$o['translate_loop'] = array();
	$langs = $db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");
	while($langs->next()){
		if((!$in['order_id']  ||  $in['c_quote_id']) || $in['quote_id']){
			if($langs->f('lang_id')==1){
				$note_type = 'order_note';
			}else{
				$note_type = 'order_note_'.$langs->f('lang_id');
			}
			$order_note = $db->field("SELECT value FROM default_data
									   WHERE default_name = 'order note'
									   AND type = '".$note_type."' ");
			if($in['c_quote_id']){
				$transl_lang_id_active = $db->field("SELECT lang_id FROM note_fields WHERE item_id = '".$in['c_quote_id']."' AND version_id = '".$version_id."' AND active = 1 AND item_type = 'quote' ");
			}
		}else{
				$order_note = $db->field("SELECT item_value FROM note_fields
									   WHERE item_type 	= 'order'
									   AND 	item_name 	= 'notes'
									   AND 	item_id 	= '".$in['order_id']."'
									   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

				/*$transl_lang_id_active = $db->field("SELECT lang_id FROM note_fields
									   WHERE item_type 	= 'order'
									   AND 	item_name 	= 'notes'
									   AND 	active 		= '1'
									   AND 	item_id 	= '".$in['order_id']."' ");*/
				if($in['duplicate_order_id']){
					$transl_lang_id_active = $in['languages'];
				}
				if(!$order_note && !$transl_lang_id_active){
					$order_from_front = $db->field("SELECT order_email FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
				}
				if($order_from_front){
					if($langs->f('lang_id')==1){
						$note_type = 'order_note';
					}else{
						$note_type = 'order_note_'.$langs->f('lang_id');
					}
					$order_note = $db->field("SELECT value FROM default_data
											   WHERE default_name = 'order note'
											   AND type = '".$note_type."' ");
					// $transl_lang_id_active = $db->field("SELECT lang_id FROM pim_lang WHERE sort_order = 1");
					$in['order_from_front'] = 1;

				}
		}
		$translate_lang = 'form-language-'.$langs->f('code');
		if($langs->f('code')=='du'){
			$translate_lang = 'form-language-nl';
		}

		if($transl_lang_id_active != $langs->f('lang_id')){
			$translate_lang = $translate_lang.' hidden';
		}

		array_push($o['translate_loop'], array(
			'translate_cls' 	=> 		$translate_lang,
			'lang_id' 			=> 		$langs->f('lang_id'),
			'notes'				=> 		$order_note,
			));

	}
	$transl_lang_id_active_custom 		= $in['languages'];
	if(!$transl_lang_id_active_custom){
		$transl_lang_id_active_custom = $in['email_language'];
	}
	$o['translate_loop_custom'] = array();
	#custom extra languages
	$custom_langs = $db->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
	while($custom_langs->next()) {
		if(!$in['order_id'] || $in['c_quote_id'] || $in['quote_id']) {
			$custom_note_type = 'order_note_'.$custom_langs->f('lang_id');
			$order_note_custom = $db->field("SELECT value FROM default_data WHERE default_name = 'order note' AND type = '".$custom_note_type."' ");
			if($in['c_quote_id']) {
				$transl_lang_id_active_custom = $db->field("SELECT lang_id FROM note_fields WHERE item_id = '".$in['c_quote_id']."' AND version_id = '".$version_id."' AND active = 1 AND item_type = 'quote'");
			}
		} else {
			$order_note_custom = $db->field("SELECT item_value FROM note_fields
											WHERE item_type 	= 'order'
											AND item_name 		= 'notes'
											AND item_id			= '".$in['order_id']."'
											AND lang_id 		= '".$custom_langs->f('lang_id')."'");
			/*$transl_lang_id_active_custom = $db->field("SELECT lang_id FROM note_fields
														WHERE item_type 	= 'order'
														AND item_name 		= 'notes'
														AND active 			= '1'
														AND item_id 		= '".$in['order_id']."' ");*/
			if($in['duplicate_order_id']) {
				$transl_lang_id_active_custom = $in['languages'];
			}

			if(!$order_note_custom && $transl_lang_id_active_custom) {
				$order_from_front_cust = $db->field("SELECT order_email FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
			}
			if($order_from_front_cust) {
				$custom_note_type = 'order_note_'.$custom_langs->f('lang_id');
				$order_note_custom = $db->field("SELECT value FROM default_data WHERE default_name = 'order note' AND type = '".$custom_note_type."' ");
				if(!$transl_lang_id_active_custom){
					// $transl_lang_id_active_custom = $db->field("SELECT lang_id FROM pim_custom_lang WHERE sort_order = 1");
				}
				$in['order_from_front'] = 1;
			}
		}
		$translate_lang_custom = 'form-language-'.$custom_langs->f('code');

		if($transl_lang_id_active_custom != $custom_langs->f('lang_id')) {
			$translate_lang_custom = $translate_lang_custom.' hidden';
		}

		array_push($o['translate_loop_custom'], array(
			'translate_cls'		=> $translate_lang_custom,
			'lang_id'			=> $custom_langs->f('lang_id'),
			'notes'				=> $order_note_custom
		));
	}

	/*$translate_lang_active = $db->field("SELECT code FROM pim_lang WHERE lang_id = '".$transl_lang_id_active."'");
	$translate_lang_active = 'translate_'.$translate_lang_active;
	if($translate_lang_active=='translate_du'){
		$translate_lang_active = 'translate_nl';
	}*/

	if($transl_lang_id_active >= 1000) {
		$lang_code = $db->field("SELECT language FROM pim_custom_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
		$code = $db->field("SELECT code FROM pim_custom_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
	} else {
		$lang_code = $db->field("SELECT language FROM pim_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
		$code = $db->field("SELECT code FROM pim_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
	}

	$in['only_contact_name'] = 1;
	$in['title'] = 1;

	$o['translate_lang_active']				= 'form-language-'.$code;
	$o['language_txt']								= gm($lang_code);

	return json_out($o, $showin,$exit);
}
function get_customers($in,$showin=true,$exit=true){
	
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_user = new sqldb($database_users);
	$filter =" AND is_admin='0' ";

	if($q){
		$filter .=" AND name LIKE '%".addslashes($q)."%'";
	}

	if($is_admin){
		$filter =" AND is_admin='".$is_admin."' ";
	}

	if($in['current_id']){
		$filter .= " AND customer_id !='".$in['current_id']."'";
	}
	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$db= new sqldb();

	$cust = $db->query("SELECT customers.name, customers.customer_id, customers.active, our_reference, currency_id, internal_language, identity_id, 
					acc_manager_name, customers.country_name
					FROM customers 
					WHERE customers.active=1 $filter GROUP BY customers.customer_id ORDER BY customers.name limit 5 ")->getAll();

	# foreach e 2x mai rapid decat while-ul
	$result = array();

	foreach ($cust as $key => $value) {
		$cname = trim($value['name']);
		$result[]=array(
			"id"					=> $value['customer_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"ref" 					=> $value['our_reference'],
			"currency_id"			=> $value['currency_id'],
			"lang_id" 				=> $value['internal_language'],
			"identity_id"	 		=> $value['identity_id'],
			'contact_name'			=> $value['acc_manager_name'],
			'country'				=> $value['country_name']
		);
		
	}
	$added = false;
	if(count($result)==5){
		array_push($result,array('id'=>'99999999999'));
		$added = true;
	}
	
	if($in['buyer_id']){
		$cust = $db->query("SELECT customers.name, customers.customer_id, customers.active, our_reference, currency_id, internal_language, identity_id, 
					acc_manager_name, customers.country_name
					FROM customers 
					WHERE customers.active=1 AND customer_id='".$in['buyer_id']."' GROUP BY customers.customer_id ORDER BY customers.name ")->getAll();
		$value = $cust[0];
		$cname = trim($value['name']);
		$result[]=array(
			"id"					=> $value['customer_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"ref" 					=> $value['our_reference'],
			"currency_id"			=> $value['currency_id'],
			"lang_id" 				=> $value['internal_language'],
			"identity_id"	 		=> $value['identity_id'],
			'contact_name'			=> $value['acc_manager_name'],
			'country'				=> $value['country_name']
		);
	}
	
	if(!$added){
		array_push($result,array('id'=>'99999999999'));		
	}
	return json_out($result, $showin,$exit);

}
function get_cc($in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_user = new sqldb($database_users);
	$filter =" is_admin='0' AND customers.active=1 ";
	// $filter_contact = ' 1=1 ';
	if($q){
		$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
		// $filter_contact .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
	}

	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$db= new sqldb();
// UNION 
// 		SELECT customer_contacts.customer_id, CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) AS name, customer_contacts.contact_id, position_n as acc_manager_name, country.name AS country_name, customer_contact_address.zip AS zip_name, customer_contact_address.city AS city_name
// 		FROM customer_contacts
// 		LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
// 		LEFT JOIN country ON country.country_id=customer_contact_address.country_id
// 		WHERE $filter_contact
// 		ORDER BY name
	$cust = $db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type
			FROM customers
			LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter
			GROUP BY customers.customer_id			
			ORDER BY name
			LIMIT 5")->getAll();

	$result = array();
	foreach ($cust as $key => $value) {
		$cname = trim($value['name']);
		if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}
		$result[]=array(
				"id"					=> $value['cust_id'].'-'.$value['contact_id'],
				'symbol'				=> $symbol,
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']
		);
		/*if (count($result) > 100)
			break;*/
	}
	if($q){
		array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
	}else{
		array_push($result,array('id'=>'99999999999','value'=>''));
	}
	array_push($result,array('id'=>'99999999999','value'=>''));
	
	return json_out($result, $showin,$exit);

}
function get_addresses($in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);
	if($q){
		$filter .=" AND (customer_addresses.address LIKE '%".addslashes($q)."%' OR customer_addresses.zip LIKE '%".addslashes($q)."%' OR customer_addresses.city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
	}
	
	// array_push($result,array('id'=>'99999999999','value'=>''));
	if($in['field'] == 'customer_id'){
		$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
								 FROM customer_addresses
								 LEFT JOIN country ON country.country_id=customer_addresses.country_id
								 LEFT JOIN state ON state.state_id=customer_addresses.state_id
								 WHERE customer_addresses.customer_id='".$in['customer_id']."'
								 ORDER BY customer_addresses.address_id limit 5");
	}
	else if($in['field'] == 'contact_id'){
	   $address= $db->query("SELECT customer_contact_address.*,country.name AS country 
							 FROM customer_contact_address 
							 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
							 WHERE customer_contact_address.contact_id='".$in['contact_id']."' 
							 AND customer_contact_address.delivery='1' limit 5");
	}

	// $max_rows=$db->records_count();
	// $db->move_to($offset*$l_r);
	// $j=0;
	$addresses=array();
	if($address){
		while($address->next()){
		  	$a = array(
		  		'symbol'				=> '',
			  	'address_id'	    => $address->f('address_id'),
			  	'id'			    => $address->f('address_id'),
			  	'address'			=> nl2br($address->f('address')),
			  	'top'				=> nl2br($address->f('address')),
			  	'zip'			    => $address->f('zip'),
			  	'city'			    => $address->f('city'),
			  	'state'			    => $address->f('state'),
			  	'country'			=> $address->f('country'),
			  	'right'				=> $address->f('country'),
			  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
		  	);
			array_push($addresses, $a);
		}
	}
	$added = false;
	if(count($addresses)==5){
		if($q){
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
		}
		$added = true;
	}
	if($in['delivery_address_id']){
		if($in['field'] == 'customer_id'){
			$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['customer_id']."' AND customer_addresses.address_id='".$in['delivery_address_id']."'
									 ORDER BY customer_addresses.address_id ");
		}
		else if($in['field'] == 'contact_id'){
		   $address= $db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  AND customer_contact_address.address_id='".$in['delivery_address_id']."'
								 AND customer_contact_address.delivery='1' ");
		}
		$a = array(
		  		'address_id'	    => $address->f('address_id'),
			  	'id'			    => $address->f('address_id'),
			  	'address'			=> nl2br($address->f('address')),
			  	'top'				=> nl2br($address->f('address')),
			  	'zip'			    => $address->f('zip'),
			  	'city'			    => $address->f('city'),
			  	'state'			    => $address->f('state'),
			  	'country'			=> $address->f('country'),
			  	'right'				=> $address->f('country'),
			  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
		  	);
			array_push($addresses, $a);
	}
	if(!$added){
		if($q){
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
		}
	}
	
	return json_out($addresses, $showin,$exit);

}
function get_articles_list(&$in,$showin=true,$exit=true){
	$in['lang_id'] = $in['languages'];
	$in['cat_id'] = $in['price_category_id'];
	$in['from_order_page'] = true;
	// return false;
	return ark::run('order-addArticle');
}

function get_currency_convertor($in)
{
	$currency = $in['currency'];
	if(empty($currency)){
		$currency = "USD";
	}
	$separator = $in['acc'];
	$into = $in['into'];
	return currency::getCurrency($currency, $into, 1,$separator);
}
