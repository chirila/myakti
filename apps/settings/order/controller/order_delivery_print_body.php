<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/*if($in['type']){
	$html='order_print_'.$in['type'].'.html';
}else{
	$html='order_print_1.html';
}*/
if($in['type']){
	$html='order_print_'.$in['type'].'.html';
}elseif ($in['custom_type']) {
	$path = INSTALLPATH.'upload/'.DATABASE_NAME.'/custom_pdf/';
	$html='custom_invoice_print-'.$in['custom_type'].'.html';
}elseif ($in['body']){
	$html='customizable_print_'.$in['layout'].'.html';
}elseif($in['body_id']){
	$html='customizable_body_print_'.$in['body_id'].'.html';
}elseif($in['header']){
	$html='customizable_print_header_'.$in['layout'].'.html';
}elseif($in['layout'] || $in['footer_id']){
	$html='customizable_print_footer_'.$in['layout'].'.html';
}else{
	$html='order_print_1.html';
}
$db=new sqldb();
$db2=new sqldb();
$db3 = new sqldb();
$db4 = new sqldb();
$db5 = new sqldb();
$db_date=new sqldb();
$view = new at(ark::$viewpath.$html);
$in['deliv_preview']  = 1;
if($in['type']){
	$def_header = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_HEADER_PDF_FORMAT' ");
	$def_body = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_BODY_PDF_FORMAT' ");
	$def_footer = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_FOOTER_PDF_FORMAT' ");
	$in['header_id']=$def_header;
	$in['footer_id']=$def_footer;

		$view_html->assign(array(
		'head'			  => true,
		'foot'			  => true,
		));
}

$labels_query = $db->query("SELECT * FROM label_language WHERE label_language_id='".$in['lid']."'")->getAll();
$labels = array();
$j=1;

foreach ($labels_query['0'] as $key => $value) {
	$labels[$key] = $value;
	$j++;
}
$labels_query = $db->query("SELECT * FROM label_language_order WHERE label_language_id='".$in['lid']."'")->getAll();
$j=1;

foreach ($labels_query['0'] as $key => $value) {
	$labels[$key] = $value;
	$j++;
}
if($in['lid']<=4){
	$labels_query_custom = $db->query("SELECT * FROM label_language_order WHERE lang_code='".$in['lid']."'")->getAll();
	if($labels_query_custom){
		foreach ($labels_query_custom['0'] as $key => $value) {
			$labels[$key] = $value;
			$j++;
		}
	}
}

if(!$in['order_id']){ //sample

	if($in['header']){
		$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='order' AND type='header' AND initial='1' AND layout='".$in['layout']."' ");
		if($data_exist1){
			$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
		}else{
			$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='order' AND type='header' AND initial='0' AND layout='".$in['layout']."' ");
		}
		$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='order' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
		if($data_exist2){
			$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
		}else{
			$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='order' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
		}

	}elseif($in['header_id']){

			$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='order' AND type='header' AND initial='1' AND layout='".$in['header_id']."' ");
			if($data_exist1){
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
			}else{
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='order' AND type='header' AND initial='0' AND layout='".$in['header_id']."' ");
			}
			$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='order' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
			if($data_exist2){
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
			}else{
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='order' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
			}
			/*var_dump($content1);
			exit();*/
	}elseif($in['footer_id']){

			$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='order' AND type='header' AND initial='1' AND layout='".$in['header_id']."' ");
			if($data_exist1){
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
			}else{
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='order' AND type='header' AND initial='0' AND layout='".$in['header_id']."' ");
			}
			$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='order' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
			if($data_exist2){
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
			}else{
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='order' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
			}
			/*var_dump($content1);
			exit();*/
	}
//header1
	$needle = "[label]";
	$needle_end = "[/label]";
	$positions = array();
	$raw_data=array();
	$final_data=array();
	$initPos = 0;
	while (($initPos = strpos($content1, $needle, $initPos))!== false) {
	 $lastPos=strpos($content1, $needle_end, $initPos);
	    $positions[$initPos + strlen($needle)] = $lastPos;
	    $initPos = $initPos + strlen($needle);
	}

	foreach ($positions as $key=>$value) {	 
	    $raw_data[]=substr($content1,$key,$value-$key);
	}
	foreach($raw_data as $value){
	 if($labels[$value]){
			$final_data[$value]=$labels[$value];		
		}else{
	 		$final_data[$value]=$labels_q[$value];
		}
	}
	foreach($final_data as $key=>$value){
	 	$content1=str_replace("[label]".$key."[/label]",$value,$content1);
	}

//header1

//footer1
	$needle2 = "[label]";
	$needle_end2 = "[/label]";
	$positions2 = array();
	$raw_data2=array();
	$final_data2=array();
	$initPos2 = 0;
	while (($initPos2 = strpos($content2, $needle2, $initPos2))!== false) {
	 $lastPos2=strpos($content2, $needle_end2, $initPos2);
	    $positions2[$initPos2 + strlen($needle2)] = $lastPos2;
	    $initPos2 = $initPos2 + strlen($needle2);
	}

	foreach ($positions2 as $key=>$value) {	 
	    $raw_data2[]=substr($content2,$key,$value-$key);
	}
	foreach($raw_data2 as $value){
		if($labels[$value]){
			$final_data2[$value]=$labels[$value];		
		}else{
	 		$final_data2[$value]=$labels_q[$value];
		}
	}
	
	foreach($final_data2 as $key=>$value){
	 	$content2=str_replace("[label]".$key."[/label]",$value,$content2);
	}
//footer1w

	//assign dummy data
$height = 251;
	$factur_date = date(ACCOUNT_DATE_FORMAT,  strtotime('now'));

	$pdf->setQuotePageLabel($labels['page']);

	$view->assign(array(
	'account_logo'        => 'images/no-logo.png',
	'billing_address_txt' => $labels['billing_address'],
	'order_note_txt'      => $labels['order_note'],
	'order_txt'           => $in['deliv_preview'] ? $labels['delivery_note'] : $labels['order'],
	'invoice_txt'         => $labels['invoice'],
	'date_txt'            => $labels['date'],
	'customer_txt'        => $labels['customer'],
	'article_txt'         => $labels['article'],
	'unitmeasure_txt'     => $labels['unitmeasure'],
	'quantity_txt'        => $labels['quantity'],
	'unit_price_txt'      => $labels['unit_price'],
	'amount_txt'          => $labels['amount'],
	'subtotal_txt'        => $labels['subtotal'],
	'discount_txt'        => $labels['discount'],
	'vat_txt'             => $labels['vat'],
	'payments_txt'        => $labels['payments'],
	'amount_due_txt'      => $labels['amount_due'],
	'notes_txt'           => $labels['notes'],
	//	'duedate'			  => $labels['duedate'],
	'bankd_txt'			  => $labels['bank_details'],
	'bank_txt'			  => $labels['bank_name'],
	'bic_txt'			  => $labels['bic_code'],
	'iban_txt'			  => $labels['iban'],
	'our_ref_txt'		  => $labels['our_ref'],
	'your_ref_txt'		  => $labels['your_ref'],
	'phone_txt'			  => $labels['phone'],
	'fax_txt'			  => $labels['fax'],
	'url_txt'			  => $labels['url'],
	'email_txt'			  => $labels['email'],
	'vat_number_txt' 	  => $labels['vat_number'],
	'vat_number_txt' 	  => $labels['vat_number'],
	'shipping_price_txt'  => $labels['shipping_price'],
	'article_code_txt'    => $labels['article_code'],
	'delivery_note_txt'    => $labels['delivery_note'],
	'delivery_note'		=> '[Delivery note]',
	'hide_our_ref'		  => '',
	'hide_your_ref'		  => '',

	'seller_name'         => '[Account Name]',
	'seller_d_country'    => '[Country]',
	'seller_d_state'      => '[State]',
	'seller_d_CITY'       => '[City]',
	'seller_d_zip'        => '[Zip]',
	'seller_d_address'    => '[Address]',
	'seller_b_country'    => '[Billing Country]',
	'seller_b_state'      => '[Billing State]',
	'seller_b_city'       => '[Billing City]',
	'seller_b_zip'        => '[Billing Zip]',
	'seller_b_address'    => '[Billing Address]',
	'serial_number'       => '####',
	'order_date'          => $factur_date,
	'order_vat'           => '##',
	'order_vat_no'		  => '[Vat Number]',
	'buyer_name'       	  => '[Customer Name]',
	'buyer_country'    	  => '[Customer Country]<br>',
	'buyer_state'      	  => '[Customer State]',
	'buyer_city'       	  => '[Customer City]<br>',
	'buyer_zip'           => '[Customer ZIP]',
	'buyer_address'    	  => '[Customer Address]<br>',
	'seller_d_country'    => '[Country]',
	'seller_d_state'      => '[State]',
	'seller_d_city'       => '[City]',
	'seller_d_zip'        => '[Zip]',
	'seller_d_address'    => '[Address]',
	'delivery_address'	  => '[Delivery Address]',
	'your_ref'       	  => '[Your Reference]',
	'our_ref' 			  => '[Our reference]',
	'notes'            	  => '[NOTES]',
	'bank'				  => '[Bank name]',
	'bic'				  => '[BIC]',
	'iban'				  => '[IBAN]',
	//'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	'seller_b_fax'        => '[Account Fax]',
	'seller_b_email'      => '[Account Email]',
	'seller_b_phone'      => '[Account Phone]',
	'seller_b_url'        => '[Account URL]',

	'hide_f'			  => true,
	'hide_e'			  => true,
	'hide_p'			  => true,
	'hide_u'			  => true,
	'is_delivery'		  => true,
	'sh_discount'		  => 'hide',
	'is_delivery_date'	  => true,
	'delivery_date' 		  => date(ACCOUNT_DATE_FORMAT,  (time()+(3600*24*30))),
	'delivery_date_txt'   	  => $labels['delivery_date'],
	'width_if_del'		  => 40,
	'address_info'	      => '[Address]<br>[Zip][City]<br>[Country]<br>',
	'buyer_email'		=> '[Email]<br>',
	'buyer_phone'		=> '[Phone]<br>',
	'buyer_fax'			=> '[Fax]',
	));
	$i=0;
	while ($i<3)
	{
		$shipping_price = 9;
		$vat_total = 5;
		$total = 63;
		$total_with_vat = 68+$shipping_price;
		$view->assign(array(
			'article'  			    	=> 'Article Name',
			'quantity'      			=> display_number(2),
			'price_vat'      			=> display_number(21),
			'price'         			=> display_number_var_dec(20),
			'percent'					=> display_number(5),
			'content'					=> true,
			'vat_value'					=> display_number(1),
			'is_article_code'	  		=> true,
			'delivery'				=> $in['deliv_preview'] ? false : true,
			'q_class'			  => $in['deliv_preview'] ? 'last right' : '',

		),'order_row');

		if(in_array($in['type'], $pdf_type)){
			if($in['deliv_preview']){
				$view->assign('article_width',68,'order_row');
			}else{
				$view->assign('article_width',41,'order_row');
			}
		}

		$view->loop('order_row');
		$i++;

	}
	$view->assign(array(
		'vat_total'				=> display_number($vat_total),
		'total'					=> display_number($total),
		'total_vat'				=> display_number($total_with_vat),
		'delivery'				=> $in['deliv_preview'] ? false : true,
		'hide_currency2'		=> ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide',
		'hide_currency1'		=> ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide',
		'currency_type'			=> ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
		'hide_default_total'	=> hide,
		'is_free_delivery'		=> true,
		'shipping_price'		=> display_number($shipping_price),
	));


}else{

	if($in['header']){
		$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice' AND type='header' AND initial='1' AND layout='".$in['layout']."' ");
		if($data_exist1){
			$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
		}else{
			$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice' AND type='header' AND initial='0' AND layout='".$in['layout']."' ");
		}
		$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
		if($data_exist2){
			$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
		}else{
			$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
		}

	}elseif($in['header_id']){

			$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice' AND type='header' AND initial='1' AND layout='".$in['header_id']."' ");
			if($data_exist1){
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
			}else{
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice' AND type='header' AND initial='0' AND layout='".$in['header_id']."' ");
			}
			$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='invoice' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
			if($data_exist2){
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
			}else{
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='invoice' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
			}
			/*var_dump($content1);
			exit();*/
	}elseif($in['footer_id']){

			$data_exist1=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='quote' AND type='header' AND initial='1' AND layout='".$in['header_id']."' ");
			if($data_exist1){
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist1."' ");
			}else{
				$content1=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='quote' AND type='header' AND initial='0' AND layout='".$in['header_id']."' ");
			}
			$data_exist2=$db5->field("SELECT id FROM tblquote_pdf_data WHERE master='quote' AND type='footer' AND initial='1' AND layout='".$in['footer_id']."' ");
			if($data_exist2){
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist2."' ");
			}else{
				$content2=$db5->field("SELECT content FROM tblquote_pdf_data WHERE master='quote' AND type='footer' AND initial='0' AND layout='".$in['footer_id']."' ");
			}
			/*
			exit();*/
	}

//header1
	$needle = "[label]";
	$needle_end = "[/label]";
	$positions = array();
	$raw_data=array();
	$final_data=array();
	$initPos = 0;
	while (($initPos = strpos($content1, $needle, $initPos))!== false) {
	 $lastPos=strpos($content1, $needle_end, $initPos);
	    $positions[$initPos + strlen($needle)] = $lastPos;
	    $initPos = $initPos + strlen($needle);
	}

	foreach ($positions as $key=>$value) {	 
	    $raw_data[]=substr($content1,$key,$value-$key);
	}
	foreach($raw_data as $value){
	 	if($labels[$value]){
			$final_data[$value]=$labels[$value];		
		}else{
	 		$final_data[$value]=$labels_q[$value];
		}
	}
	
	foreach($final_data as $key=>$value){
	 	$content1=str_replace("[label]".$key."[/label]",$value,$content1);
	}

//header1

//footer1
	$needle2 = "[label]";
	$needle_end2 = "[/label]";
	$positions2 = array();
	$raw_data2=array();
	$final_data2=array();
	$initPos2 = 0;
	while (($initPos2 = strpos($content2, $needle2, $initPos2))!== false) {
	 $lastPos2=strpos($content2, $needle_end2, $initPos2);
	    $positions2[$initPos2 + strlen($needle2)] = $lastPos2;
	    $initPos2 = $initPos2 + strlen($needle2);
	}

	foreach ($positions2 as $key=>$value) {	 
	    $raw_data2[]=substr($content2,$key,$value-$key);
	}

	foreach($raw_data2 as $value){
	 	if($labels[$value]){
			$final_data2[$value]=$labels[$value];		
		}else{
	 		$final_data2[$value]=$labels_q[$value];
		}
	}

	foreach($final_data2 as $key=>$value){
	 	$content2=str_replace("[label]".$key."[/label]",$value,$content2);
	}

//footer1w

	$db = new sqldb();
	$db->query("SELECT pim_orders.*, multiple_identity.*,pim_order_articles.vat_percent FROM pim_orders
			LEFT JOIN multiple_identity ON multiple_identity.identity_id=pim_orders.identity_id
			LEFT JOIN pim_order_articles ON pim_orders.order_id=pim_order_articles.order_id
			WHERE pim_orders.order_id='".$in['order_id']."'");
	$db->move_next();
	$serial_number = $db->f('serial_number');
    $allow_article_packing=$db->f('use_package');
    $allow_article_sale_unit=$db->f('use_sale_unit');
    $remove_vat = $db->f('remove_vat');
    $apply_discount = $db->f('apply_discount');
	$currency = get_commission_type_list($db->f('currency_type'));
	$vat = $db->f('vat_percent');
	$global_disc = $db->f('discount');
	if($apply_discount < 2){
		$global_disc = 0;
	}
    if($db->f('show_vat')){
       $show_vat = true;
    }else{
       $show_vat = false;
    }
    if($remove_vat == 1){
    	$show_vat = false;
    }

	$currency_type = $db->f('currency_type');
	$currency_rate = $db->f('currency_rate');

	$factur_date = date(ACCOUNT_DATE_FORMAT,  $db->f('date'));
	//	$due_date = date(ACCOUNT_DATE_FORMAT,  $db->f('due_date'));
	$delivery_date = date(ACCOUNT_DATE_FORMAT,  $db->f('del_date'));
	$deliv_date = $db_date->field("SELECT date FROM pim_order_deliveries WHERE order_id = '".$in['order_id']."' AND delivery_id = '".$in['delivery_id']."' ");
	$db_date->query("SELECT minute,hour,pm FROM pim_order_deliveries WHERE order_id = '".$in['order_id']."' AND delivery_id = '".$in['delivery_id']."' ");

	$delivery_del_date = date(ACCOUNT_DATE_FORMAT,  $deliv_date);
	if($db_date->f('hour')){
		$delivery_del_date.= ' ('.$db_date->f('hour').':'.$db_date->f('minute').' '.$db_date->f('pm').')';
	}


	$img = 'images/no-logo.png';
	$attr = '';
	if($in['logo']) {
		$print_logo = $in['logo'];
	}else {
		$print_logo=ACCOUNT_LOGO_ORDER;
	}
	if($db->f('company_logo')){
		$print_logo_idnt=$db->f('company_logo');
	}
	if($print_logo){
		$img = '../'.$print_logo;
		if($print_logo_idnt){
			$img=$print_logo_idnt;
		}
		$size = getimagesize($img);
		$ratio = 250 / 77;
		if(@($size[0]/$size[1]) > $ratio ){
			$attr = 'width="250"';
		}else{
			$attr = 'height="77"';
		}
	}

	$buyer_country = get_country_name($db->f('customer_country_id'));
	if($db->f('contact_id'))
	{
		$contact_title = $db2->field("SELECT customer_contact_title.name FROM customer_contact_title
									INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
									WHERE customer_contacts.contact_id='".$db->f('contact_id')."'");
	}
	if($contact_title)
	{
		$contact_title .= " ";
	}

	$contact_title2 = $contact_title .= " ";

	if($db->f('field')=='customer_id'){
		$contact_title2 = '';
	}

	$shipping_price = $db->f('shipping_price');

	$notes = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'order'
								   AND item_name = 'notes'
								   AND active = '1'
								   AND item_id = '".$in['order_id']."' ");

	$pdf->setQuotePageLabel($labels['page']);

	$view->assign(array(
	'account_logo'        => $img,
	'attr'				  			=> $attr,
	'billing_address_txt' => $labels['billing_address'],
	'order_note_txt'      => $labels['order_note'],
	'order_txt'           => $in['delivery_id'] ? $labels['delivery_note'] : $labels['order'],
	'date_txt'            => $labels['date'],
	'customer_txt'        => $labels['customer'],
	'article_txt'         => $labels['article'],
	'unitmeasure_txt'     => $labels['unitmeasure'],
	'quantity_txt'        => $labels['quantity'],
	'unit_price_txt'      => $labels['unit_price'],
	'amount_txt'          => $labels['amount'],
	'subtotal_txt'        => $labels['subtotal'],
	'discount_txt'        => $labels['discount'],
	'vat_txt'             => $labels['vat'],
	'payments_txt'        => $labels['payments'],
	'total_txt'      	  	=> $labels['total'],
	'notes_txt'           => $notes ? $labels['notes'] : '',
	'duedate_txt'		  => $labels['duedate'],
	'bankd_txt'			  => $labels['bank_details'],
	'bank_txt'			  => $labels['bank_name'],
	'bic_txt'			  => $labels['bic_code'],
	'iban_txt'			  => $labels['iban'],
	'gov_taxes_txt'       => $labels['gov_taxes'],
	'gov_taxes_code_txt'  => $labels['gov_taxes_code'],
	'gov_taxes_type_txt'  => $labels['gov_taxes_type'],
	'sale_unit_txt'		  => wrap($labels['sale_unit'], 9, ' '),
	'package_txt'		  => wrap($labels['package'], 9, ' '),
	'our_ref_txt'		  => $labels['our_ref'],
	'your_ref_txt'		  => $labels['your_ref'],
	'phone_txt'			  => $labels['phone'],
	'fax_txt'			  => $labels['fax'],
	'url_txt'			  => $labels['url'],
	'email_txt'			  => $labels['email'],
	'vat_number_txt' 	  => $labels['vat_number'],
	'delivery_note_txt'	  => $labels['delivery_note'],
	'shipping_price_txt'  => $labels['shipping_price'],
	'article_code_txt'    => $labels['article_code'],
	'delivery_date_txt'   => $labels['delivery_date'],
	'pick_up_from_store_txt' => $labels['pick_up_from_store'],
	// 'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	'serial_number'		  => $serial_number,
	'order_date'          => $factur_date,
	'buyer_name'       	  => $contact_title2.$db->f('customer_name'),
	'buyer_country'    	  => $buyer_country? $buyer_country."<br/>":'',
	'buyer_state'      	  => $db->f('customer_state'),
	'buyer_city'       	  => $db->f('customer_city')? $db->f('customer_city')."<br/>":'',
	'buyer_zip'        	  => $db->f('customer_zip'),
	'buyer_address'    	  => $db->f('customer_address')? $db->f('customer_address')."<br/>":'',
	'buyer_email'		  => $db->f('customer_email')? $db->f('customer_email')."<br/>":'',
	'buyer_fax'           => $db->f('customer_fax')? $db->f('customer_fax')."<br/>":'',
	'buyer_phone'         => $db->f('customer_phone')? $db->f('customer_phone')."<br/>":'',
	'seller_name'         => $db->f('identity_id')? $db->f('company_name') : $db->f('seller_name'),
	'seller_d_country'    => $db->f('identity_id')? get_country_name($db->f('country_id')) : get_country_name($db->f('seller_d_country_id')),
	// 'seller_d_state'      => get_state_name($db->f('seller_d_state_id')),
	'seller_d_city'       => $db->f('identity_id')? $db->f('city_name') : utf8_decode($db->f('seller_d_city')),
	'seller_d_zip'        => $db->f('identity_id')? $db->f('company_zip') : $db->f('seller_d_zip'),
	'seller_d_address'    => $db->f('identity_id')? nl2br($db->f('company_address')) : nl2br(utf8_decode($db->f('seller_d_address'))),
	'order_contact_name'  => $contact_title.$db->f('contact_name'),
	'field_customer_id'   => $db->f('field')=='customer_id'?true:false,
	//	'INVOICE_BUYER_VAT'   => ACCOUNT_VAT_NUMBER,
	'bank'				  => $db->f('identity_id')? $db->f('bank_name_identity') : ACCOUNT_BANK_NAME,
	'bic'				  => $db->f('identity_id')? $db->f('bank_bic_code_identity') : ACCOUNT_BIC_CODE,
	'iban'				  => $db->f('identity_id')? $db->f('bank_iban_identity') : ACCOUNT_IBAN,
	// 'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	'sh_discount'		  => $apply_discount < 2 ? false :true,
	'discount_percent'    => ' ( '.$db->f('discount').'% )',
	'notes'               =>  $notes ? nl2br($notes) : '',
	'view_notes'          => $notes ? '' : 'hide' ,
	'hide_default_total'  => 'hide',//$currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? 'hide' : '') : 'hide',
	'def_currency'		  => build_currency_name_list(ACCOUNT_CURRENCY_TYPE),
	'order_vat_no'		  => $db->f('seller_bwt_nr'),
	'your_ref'       	  => $db->f('your_ref'),
	'our_ref' 			  => $db->f('our_ref'),
	'hide_our_ref'		  => $db->f('our_ref')? '':'hide',
	'hide_your_ref'		  => $db->f('your_ref')? '':'hide',

	'seller_b_fax'        => $db->f('identity_id')? $db->f('company_fax') : ACCOUNT_FAX,
	'seller_b_email'      => $db->f('identity_id')? $db->f('company_email') : ACCOUNT_EMAIL,
	'seller_b_phone'      => $db->f('identity_id')? $db->f('company_phone') : ACCOUNT_PHONE,
	'seller_b_url'        => $db->f('identity_id')? $db->f('company_url') : ACCOUNT_URL,

	'hide_f'			  => defined('ACCOUNT_FAX') && !ACCOUNT_FAX ? false : true,
	'hide_e'			  => defined('ACCOUNT_EMAIL') && !ACCOUNT_EMAIL ? false : true,
	'hide_p'			  => defined('ACCOUNT_PHONE') && !ACCOUNT_PHONE ? false : true,
	'hide_u'			  => defined('ACCOUNT_URL') && !ACCOUNT_URL ? false : true,
	'address_info'	        => nl2br($db->f('address_info'))."<br/>",
	'delivery_date'		  => $delivery_date,
	'is_delivery_date'	  => ($in['delivery_id'] && $delivery_date) ? false : true,
	'delivery_del_date'	  => $delivery_del_date,
	'is_delivery_del_date'=> ($in['delivery_id'] && $delivery_del_date) ? true : false,
	'is_pickup'           => $db->f('delivery_type')==2 ? true : false,

	));
	$height = 251;
	$discount_percent= $db->f('discount');
	if($db->f('discount') == 0){
		$discount_procent=0;
	} else {
		$discount_procent = $db->f('discount');
		$view->assign('total_discount_procent',$db->f('discount'));
	}



	$delivery_address = $db_date->field("SELECT delivery_address  FROM pim_order_deliveries WHERE order_id = '".$in['order_id']."' AND delivery_id = '".$in['delivery_id']."' AND TRIM(delivery_address) <>' '");

    if(trim($delivery_address)){
       $view->assign(array(
			'delivery_address'		=>nl2br($delivery_address),
			'is_delivery'			=>true
		));
    }else{
     if($db->f('delivery_address')){
		$view->assign(array(
			'delivery_address'		=>nl2br($db->f('delivery_address')),
			'is_delivery'			=>true
		));
	}else{
		$view->assign(array(
			'delivery_address'=>'',
			'is_delivery'=>false
		));
	}


    }



	$i=0;
	$discount_total = 0;

	$show_discount_total = false;

	$vat_total = 0;
	$total = 0;
	$total_with_vat = 0;

	if($in['delivery_id']){

		$first_line_content_data = $db4->query("SELECT article, content FROM pim_order_articles WHERE order_id = '".$in['order_id']."' ORDER BY order_articles_id ASC LIMIT 1");
		$first_line_content_data->next();
		$is_first_content_line = false;
		if($first_line_content_data->f('content')=='1'){
			$is_first_content_line = true;
			$view->assign(array(
			'is_content_line2'			=> true,
			'content_line2'				=> $first_line_content_data->f('article'),
			'is_first_content_line'		=> $is_first_content_line,
			));

		}
		$db->query("SELECT pim_order_articles.*, pim_orders_delivery.* FROM pim_order_articles
					INNER JOIN pim_orders_delivery ON pim_orders_delivery.order_articles_id=pim_order_articles.order_articles_id
					WHERE pim_orders_delivery.order_id='".$in['order_id']."' AND pim_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC");
		// echo "SELECT pim_order_articles.*, pim_orders_delivery.* FROM pim_order_articles
		// 			INNER JOIN pim_orders_delivery ON pim_orders_delivery.order_articles_id=pim_order_articles.order_articles_id
		// 			WHERE pim_orders_delivery.order_id='".$in['order_id']."' AND pim_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC";
		$content_line_data = $db4->query("SELECT order_articles_id, article FROM pim_order_articles WHERE order_id = '".$in['order_id']."' AND content = '1' ");
		$is_content_line = false;
		$content_line = array();
		while($content_line_data->next()){
			$content_line[$content_line_data->f('order_articles_id')] = $content_line_data->f('article');
			$is_content_line = true;
		}
	}else{
		$db->query("SELECT pim_order_articles.* FROM pim_order_articles WHERE order_id='".$in['order_id']."' ORDER BY sort_order ASC");

		$is_content_line = false;
	}
	while ($db->move_next())
	{
		if($apply_discount == 1 || $apply_discount == 3){
			$show_discount_total = true;
		}
		if (in_array($in['type'], $pdf_type)&& $in['delivery_id']){
			if($delivery_date || $delivery_del_date){
				$view->assign('width_if_del','38');
			}else{
				$view->assign('width_if_del','68');
			}
			$view->assign('article_width','68','order_row');
		}
		elseif(in_array($in['type'], $pdf_type) && !$in['delivery_id'] && $db->f('content')){
			$view->assign('article_width','94','order_row');
		}
		else{
			$view->assign('article_width','18','order_row');

			if($delivery_date || $delivery_del_date){
				$view->assign('width_if_del','40');
			}else{
				$view->assign('width_if_del','68');
			}
		}

		if($is_content_line == true){
			foreach ($content_line as $key => $value) {
				$after_order_articles_id = $db->f('order_articles_id')+1;
				// echo $after_order_articles_id; echo " / "; echo $key; echo "<br>";
				if($key == $after_order_articles_id){
					$is_spec_content_line = true;
					$spec_content_line = $value;
					goto found;
				}else{
					$is_spec_content_line = false;
				}
			}
		}
		found:
		// var_dump($is_content_line);
		// var_dump($is_spec_content_line);
		$discount_line = $db->f('discount');
		if($apply_discount == 0 || $apply_discount == 2){
			$discount_line = 0;
		}
		if($apply_discount < 2){
			$global_disc = 0;
		}

		$q = $db->f('quantity') * $db->f('packing') / $db->f('sale_unit');

		$price_line = $db->f('price') - ($db->f('price') * $discount_line /100);
		$discount_total += $price_line * $global_disc / 100 * $q;

		$vat_total += ($price_line - $price_line * $global_disc /100) *$db->f('vat_percent') / 100 * $q;
		$line_total=$price_line * $q;

		$total += $line_total;


		// $discount_total += ($db->f('price')*$db->f('discount')/100)*$db->f('quantity')*($db->f('packing')/$db->f('sale_unit'));
		// $price_line = $db->f('price') - ($db->f('price')*$db->f('discount')/100);
		// $vat_total += ($price_line*$db->f('vat_percent')/100)*$db->f('quantity')* ($db->f('packing')/$db->f('sale_unit'));
		// $total += $db->f('price')*$db->f('quantity') * ($db->f('packing')/$db->f('sale_unit'));
		// $total_with_vat += $db->f('price_with_vat')*$db->f('quantity') * ($db->f('packing')/$db->f('sale_unit'));

		$delivery_article = $db->f('article');
		//if show serial numbers
		if($in['delivery_id'] && $is_content_line == false && (SHOW_ART_S_N_ORDER_PDF==1)){
			$art_serial_number_list = '';
			$use_serial_no = $db3->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$db->f('article_id')."' ");
			if($use_serial_no){
				$serial_number_data = $db3->query("SELECT serial_number FROM serial_numbers WHERE delivery_id = '".$in['delivery_id']."' AND article_id = '".$db->f('article_id')."' ");
				$k=0;
				while($serial_number_data->next()){
					$art_serial_number_list .= "<br/>".$serial_number_data->f('serial_number');
					$k++;
				}
				$art_serial_number_list = "<br/>".gm("Serial Numbers").':'.$art_serial_number_list;
				$view->assign(array(
					'is_serial_number'		=> $k > 0 ? true : false,
					// 'count_s_n'				=> $k+1,
					// 'serial_nr_txt'			=> gm('Serial Numbers'),
					'art_serial_number_list'	=> $art_serial_number_list,
				),'order_row');
			}
		}

		//if show batch numbers
		if($in['delivery_id'] && $is_content_line == false && (SHOW_ART_B_N_ORDER_PDF==1)){
			$art_batch_number_list = '';
			$use_batch_no = $db3->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$db->f('article_id')."' ");
			if($use_batch_no){
				$batch_number_data = $db3->query("SELECT DISTINCT batch_id FROM batches_from_orders WHERE delivery_id = '".$in['delivery_id']."' AND article_id = '".$db->f('article_id')."' AND order_articles_id = '".$db->f('order_articles_id')."' ");
				$k=0;
				while($batch_number_data->next()){
					$batch_number = $db3->field("SELECT batch_number FROM batches WHERE id = '".$batch_number_data->f('batch_id')."' ");
					$art_batch_number_list .= "<br/>".$batch_number;
					$k++;
				}
				$art_batch_number_list = "<br/>".gm("Batches").':'.$art_batch_number_list;
				$view->assign(array(
					'is_batch_number'		=> $k > 0 ? true : false,
					'art_batch_number_list'	=> $art_batch_number_list,
				),'order_row');
			}
		}
		$view->assign(array(
		// 'article'  			    	=> wrap($db->f('article'), 9, ' '),
		'article'  			    	=> $in['delivery_id'] ? nl2br($delivery_article) : nl2br($db->f('article')),
		// 'article'  			    	=> $db->f('article'),
		'quantity'      			=> display_number($db->f('quantity')),
		'price_vat'      			=> display_number($db->f('quantity') * $price_line * ($db->f('packing') / $db->f('sale_unit'))),
		'price'         			=> display_number_var_dec($db->f('price')),
		'percent'					=> display_number($db->f('vat_percent')),
		'vat_value'					=> display_number($db->f('vat_value')),
		'discount'					=> display_number($db->f('discount')),
		'sale_unit'					=> $db->f('sale_unit'),
		'price_with_vat'            => display_number($db->f('price')+$db->f('vat_value')),
		'packing'					=> remove_zero_decimals($db->f('packing')),
		'q_class'					=> $in['delivery_id'] ? 'last right' : '',
		'colspan'					=> $db->f('content') ? ' colspan="8" ' : '',
		'content'					=> $db->f('content') ? false : true,
		'content_class'				=> $db->f('content') ? ' last_nocenter ' : '',
		'is_article_code'			=> $db->f('article_code') ? true : false,
		// 'article_code'				=> wrap($db->f('article_code'), 8, ' '),
		'article_code'				=> $db->f('article_code'),
		'is_content_line'			=> ($is_content_line && $is_spec_content_line) ? true : false,
		'content_line'				=> $spec_content_line,
		'allow_article_packing'     => $allow_article_packing,
		'allow_article_sale_unit'   => $allow_article_sale_unit,
		),'order_row');

		if( $in['type']== '5' || $in['type'] == '4' ){
			$view->assign(array(
				'allow_article_packing'		=> false,
			),'order_row');
		}

		if($in['delivery_id'] && ($in['type'] == 4 || $in['type'] == 5) ){
			$view->assign(array(
				'article_width'		=> $allow_article_packing ? 57 : 69,
			),'order_row');
		}

		$view->loop('order_row');

		$view->assign(array('delivery_note'	=> nl2br($db->f('delivery_note'))));
		$delivery_note = $db->f('delivery_note');

		$i++;

	}

	$view->assign('delivery',$in['delivery_id'] ? false : true);

	// $total=$total-$discount_total;
	// $total += $shipping_price;
	if(!$show_vat){
		$vat_total	= 0;
	}
	$total = round($total,2);
	$vat_total = round($vat_total,2);
	$total_with_vat=$vat_total+$total-$discount_total+$shipping_price;
	// $total_with_vat=$vat_total+$total+$shipping_price;
	if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
		$total_default = $total_with_vat*return_value($currency_rate);
	}

	$view->assign(array(
	'discount_total'        		=> place_currency(display_number($discount_total),get_commission_type_list($currency_type)),
	'vat_total'						=> place_currency(display_number($vat_total),get_commission_type_list($currency_type)),
	'total'							=> place_currency(display_number($total),get_commission_type_list($currency_type)),
	'total_vat'						=> place_currency(display_number($total_with_vat + $total_gov_taxes),get_commission_type_list($currency_type),'','helvetica'),
	'gov_taxes_value'       		=> place_currency(display_number($total_gov_taxes),get_commission_type_list($currency_type)),
	'total_amount_due_default'		=> place_currency(display_number($total_default)),
	'hide_currency2'				=> ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide',
	'hide_currency1'				=> ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide',
	'currency'						=> ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
	'discount_percent'				=> $show_discount ? ' ( '.$discount_percent.'% )' : "",
	'sh_discount_total'				=> $apply_discount == 1 || $apply_discount == 3 ? true : false,
	'show_vat'				        => $show_vat,
	'is_free_delivery'				=> $shipping_price > 0 ? true : false,
	'shipping_price'				=> place_currency(display_number($shipping_price),get_commission_type_list($currency_type)),
	'hide_if_no_vat_no_disc'		=> true,

	));

	if($vat_total == 0 || !$show_vat){

		$view->assign(array(
			'sh_vat' 			=>	'hide',
		));
	}

	if(!$show_vat){
		$view->assign(array(

			'subtotal_txt'       		=> $apply_discount < 2 ? "<strong>".$labels['amount']."</strong>" : $labels['subtotal'],
			'total'				=> "<strong>".place_currency(display_number($total+$shipping_price),get_commission_type_list($currency_type))."</strong>",
			'hide_if_no_vat_no_disc'	=> $apply_discount < 2 ? false : true,
		));

	}

}
if($allow_article_sale_unit && $allow_article_packing ){
	$article_width=11;
	//$view->assign('article_width',27);
}elseif($allow_article_packing && !$allow_article_sale_unit){
	$article_width=11;
	//$view->assign('article_width',27);
}else{
	$article_width=24;
	//$view->assign('article_width',40);
}


if($show_discount_total && $show_vat){
   $view->assign('article_width',$article_width);
}elseif(!$show_discount_total && $show_vat){
   $view->assign('article_width',$article_width+12);
}
elseif($show_discount_total && !$show_vat){
   $view->assign('article_width',$article_width+12);
}else{
   $view->assign('article_width',$article_width+24);
}

if($in['delivery_id']){
	$view->assign(array(
		'article_width'			=> $allow_article_packing ? 61 : 74,
		'show_delivery_note'	=> $delivery_note ? true : false ));
}else{
	$view->assign(array(
		'show_delivery_note'	=> false ));

}



$view->assign(array(
    'allow_article_packing'     => $allow_article_packing,
    'allow_article_sale_unit'   => $allow_article_sale_unit,

));
if($in['deliv_preview']){
	$view->assign('article_width'	, 74 );
}
if($in['deliv_preview']){
	$view->assign('show_delivery_note'	, $in['deliv_preview'] ? true : false);
}

$is_table = true;
$is_data = false;

#for layout nr 4.
if($hide_all == 1){
	// $view->assign('hide_all','hide');
	$is_table = false;
	$is_data = true;
}else if($hide_all == 2){
	$is_table = true;
	$is_data = false;
	// $view->assign('hide_t','hide');
}

$view->assign(array(
	'is_table'=> $is_table,
	'is_data'=>$is_data,
));

$vars=$view->return_vars();
	$needle1 = "[!";
	$needle_end1 = "!]";
	$positions1 = array();
	$raw_data1=array();
	$final_data1=array();
	$initPos1 = 0;
	// var_dump($content1);	
	while (($initPos1 = strpos($content1, $needle1, $initPos1))!== false) {
	 	$lastPos1=strpos($content1, $needle_end1, $initPos1);
	    $positions1[$initPos1 + strlen($needle1)] = $lastPos1;
	    $initPos1 = $initPos1 + strlen($needle1);
	}

	foreach ($positions1 as $key=>$value) {	 
	    $raw_data1[]=substr($content1,$key,$value-$key);
	}

	foreach($raw_data1 as $value){
 		$final_data1[$value]=$vars->$value;
	}
// var_dump($final_data1);
	foreach($final_data1 as $key=>$value){
		if($key=='ACCOUNT_LOGO'){
			$value=$img;
		}
/*		if(!$value){
			$value = '-';
		}*/
			
	 	$content1=str_replace("[!".$key."!]",$value,$content1);
	 	// if(!$value){
	 	// 	$content1=preg_replace('/\[if\:'.$key.'\!\](.*)\[end:'.$key.'\!\]/', '', $content1);
	 	// }
	}
//header1

//footer1
	$vars3=$view->return_vars();
	$needle3 = "[!";
	$needle_end3 = "!]";
	$positions3 = array();
	$raw_data3=array();
	$final_data3=array();
	$initPos3 = 0;
	while (($initPos3 = strpos($content2, $needle3, $initPos3))!== false) {
	 	$lastPos3=strpos($content2, $needle_end3, $initPos3);
	    $positions3[$initPos3 + strlen($needle3)] = $lastPos3;
	    $initPos3 = $initPos3 + strlen($needle3);
	}

	foreach ($positions3 as $key=>$value) {	 
	    $raw_data3[]=substr($content2,$key,$value-$key);
	}

	foreach($raw_data3 as $value){
 		$final_data3[$value]=$vars->$value;
	}
// var_dump($final_data3);
	foreach($final_data3 as $key=>$value){
		if($key=='ACCOUNT_LOGO'){
			$value=$img;
		}
/*		if(!$value){
			$value = '-';
		}*/
		
	 	$content2=str_replace("[!".$key."!]",$value,$content2);
	}

//footer1
	if($in['header_id']){
		$pdf->setQuotePageLabel('salesassist_2');
		$pdf->Custom_footer($content2);
	}elseif($in['footer_id']){
		$pdf->setQuotePageLabel('salesassist_2');
		$pdf->Custom_footer($content2);

	}

	$view->assign(array(
		'HEADER'			  => $content1,
	));

return $view->fetch();