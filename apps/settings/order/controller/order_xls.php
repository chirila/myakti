<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');


require_once INSTALLPATH.'libraries/PHPExcel.php';
$filename ="order.xls";

if(!$in['lid']){
  $in['lid']=1;
}

$labels = array();

$labels_query = $db->query("SELECT * FROM label_language_order WHERE label_language_id='".$in['lid']."'")->getAll();
$j=1;

foreach ($labels_query['0'] as $key => $value) {
  $labels[$key] = $value;
  $j++;
}
if($in['lid']<=4){
  $labels_query_custom = $db->query("SELECT * FROM label_language_order WHERE lang_code='".$in['lid']."'")->getAll();
  if($labels_query_custom){
    foreach ($labels_query_custom['0'] as $key => $value) {
      $labels[$key] = $value;
      $j++;
    }
  }
}

$db2=new sqldb();
$db_date=new sqldb();
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
               ->setLastModifiedBy("Akti")
               ->setTitle("Order")
               ->setSubject("Order")
               ->setDescription("Order")
               ->setKeywords("office PHPExcel php")
               ->setCategory("Order");
$db = new sqldb();

$filter = '1=1';
$notes = $db->field("SELECT item_value FROM note_fields
                   WHERE item_type = 'order'
                   AND item_name = 'notes'
                   AND active = '1'
                   AND lang_id=  '".$in['lid']."'
                   AND item_id = '".$in['order_id']."'");

if(!$notes){
    if($in['lid']==1){
          $note_type = 'order_note';
        }else{
          $note_type = 'order_note_'.$in['lid'];
        }
        $notes = $db->field("SELECT value FROM default_data
                       WHERE default_name = 'order note'
                       AND type = '".$note_type."' ");
}

$db->query("SELECT pim_orders.*,pim_order_articles.vat_percent FROM pim_orders
      LEFT JOIN pim_order_articles ON pim_orders.order_id=pim_order_articles.order_id
      WHERE pim_orders.order_id='".$in['order_id']."'");
  $db->move_next();




  if($db->f('contact_id'))
  {
    $contact_title = $db2->field("SELECT customer_contact_title.name FROM customer_contact_title
                  INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
                  WHERE customer_contacts.contact_id='".$db->f('contact_id')."'");
  }
  if($contact_title)
  {
    $contact_title .= " ";
  }

  $contact_title2 = $contact_title .= " ";

  if($db->f('field')=='customer_id'){
    $contact_title2 = '';
  }

 $comments = $db->f('comments');
  $serial_number = $db->f('serial_number');
    $allow_article_packing=$db->f('use_package');
    $allow_article_sale_unit=$db->f('use_sale_unit');
    $remove_vat = $db->f('remove_vat');
    $apply_discount = $db->f('apply_discount');
  $currency = get_commission_type_list($db->f('currency_type'));
  $vat = $db->f('vat_percent');
  $global_disc = $db->f('discount');
$factur_date = date(ACCOUNT_DATE_FORMAT,  $db->f('date'));
  //  $due_date = date(ACCOUNT_DATE_FORMAT,  $db->f('due_date'));
  $delivery_date = date(ACCOUNT_DATE_FORMAT,  $db->f('del_date'));


  if($apply_discount < 2){
    $global_disc = 0;
  }
    if($db->f('show_vat')){
       $show_vat = true;
    }else{
       $show_vat = false;
    }
    if($remove_vat == 1){
      $show_vat = false;
    }
    if($apply_discount == 1 || $apply_discount == 3){
      $show_discount_total = true;
    }
     $sh_discount=true;
    if($apply_discount < 2){
       $sh_discount=false;
    }

  $currency_type = $db->f('currency_type');
  $currency_rate = $db->f('currency_rate');
  $shipping_price=$db->f('shipping_price');

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('E2', $labels['order_note']);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('F2', $serial_number);
$objPHPExcel->getActiveSheet(0)->getStyle('F2:F4')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet(0)->getStyle('D2:D5')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet(0)->getStyle('B2:B11')->getFont()->setBold(true);

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('E3', $labels['date']);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('F3', $factur_date);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('E4', $labels['delivery_date']);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('F4', $delivery_date);

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('C2', $labels['vat_number']);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('D2', $db->f('customer_vat_number'));
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('C3', $labels['phone']);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('D3', $db->f('customer_phone'));
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('C4', $labels['email']);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('D4',  $db->f('customer_email'));
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('C5', $labels['CUSTOMER_REF']);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('D5',  $db->f('customer_reference'));

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2',  $labels['customer']);

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B2',  $contact_title2.$db->f('customer_name'));
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A7',  $labels['CUSTOMER_REF']);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B7',  $db->f('customer_ref'));



$objPHPExcel->setActiveSheetIndex(0)->mergeCells( 'B3:B6' );
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B3',  $db->f('address_info'));
$objPHPExcel->setActiveSheetIndex(0)->getStyle('B3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(80);
if($db->f('delivery_address')){
  $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A7',  $labels['billing_address']);
  $objPHPExcel->setActiveSheetIndex(0)->mergeCells( 'B7:B10' );
 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B7',  $db->f('delivery_address'));
 $objPHPExcel->setActiveSheetIndex(0)->getStyle('B7')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
     $xlsRow=13;

  }else{
        $xlsRow=13;
  }



$db->query("SELECT pim_order_articles.* FROM pim_order_articles WHERE order_id='".$in['order_id']."' ORDER BY sort_order ASC");

$supplier_reference='Référence Fournisseur';

if($in['lid']==3){
  $supplier_reference='Referentie leverancier';
}

if(DATABASE_NAME=='65086f10_4ab4_b171_0d0c6f1c5604' ||  $_SESSION['front']['theme']['path']== 'ui/001-07/' ){
  $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A12', $labels['article_code'])
      ->setCellValue('B12',$supplier_reference)
      ->setCellValue('C12',$labels['article'])
      ->setCellValue('D12',$labels['quantity'])
      ->setCellValue('E12',$labels['unit_price']);

         $next_column= array('F','G','H','I','J');

}else{
  $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A12', $labels['article_code'])
      ->setCellValue('B12',$labels['article'])
      ->setCellValue('C12',$labels['quantity'])
      ->setCellValue('D12',$labels['unit_price']);
       $next_column= array('E','F','G','H','I');
}



$i=0;

if($show_vat){
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].'12',$labels['vat']);
       $i++;
}

if($allow_article_packing){
       $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].'12',$labels['package']);
         $i++;

}
if($allow_article_sale_unit){
       $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].'12',$labels['sale_unit']);
         $i++;

}

if($show_discount_total){
       $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].'12',$labels['discount']);
         $i++;

}

$objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].'12',$labels['amount']);

//some style
$objPHPExcel->getActiveSheet(0)->getStyle('A12:'.$next_column[$i].'12')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet(0)->getDefaultRowDimension()->setRowHeight(20);
$objPHPExcel->getActiveSheet(0)->getStyle('A12:'.$next_column[$i].'12')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setRGB('A3A3A3');
foreach(range('A','I') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
}


$discount_total = 0;



  $vat_total = 0;
  $total = 0;
  $total_with_vat = 0;


while ($db->move_next())
{
  if($db->f('content')==0){

 $discount_line = $db->f('discount');
    if($apply_discount == 0 || $apply_discount == 2){
      $discount_line = 0;
    }
    if($apply_discount < 2){
      $global_disc = 0;
    }

    $q = $db->f('quantity') * $db->f('packing') / $db->f('sale_unit');

    $price_line = $db->f('price') - ($db->f('price') * $discount_line /100);
    $discount_total += $price_line * $global_disc / 100 * $q;

    $vat_total += ($price_line - $price_line * $global_disc /100) *$db->f('vat_percent') / 100 * $q;
    $line_total=$price_line * $q;

    $total += $line_total;

if(DATABASE_NAME=='65086f10_4ab4_b171_0d0c6f1c5604' || $_SESSION['front']['theme']['path']== 'ui/001-07/' ){
$supplier_reference=$db2->field("SELECT supplier_reference FROM pim_articles WHERE article_id='".$db->f('article_id')."'");
$objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A'.$xlsRow, ' '.$db->f('article_code'))
       ->setCellValue('B'.$xlsRow, $supplier_reference)
      ->setCellValue('C'.$xlsRow, $db->f('article'))
      ->setCellValue('D'.$xlsRow, display_number($db->f('quantity')))
      ->setCellValue('E'.$xlsRow, display_number_var_dec($db->f('price')));
  $next_column= array('F','G','H','I','J');
}else{

$objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A'.$xlsRow, ' '.$db->f('article_code'))
      ->setCellValue('B'.$xlsRow, $db->f('article'))
      ->setCellValue('C'.$xlsRow, display_number($db->f('quantity')))
      ->setCellValue('D'.$xlsRow, display_number_var_dec($db->f('price')));
 $next_column= array('E','F','G','H','I');
 }

 $i=0;
if($show_vat){
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].$xlsRow,display_number($db->f('vat_percent')). "%");
        $i++;
 }

if($allow_article_packing){
       $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].$xlsRow,$db->f('packing'));
         $i++;

}
if($allow_article_sale_unit){
       $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].$xlsRow,$db->f('sale_unit'));
         $i++;

}

if($show_discount_total){
       $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].$xlsRow,display_number($db->f('discount')));
          $i++;

}

     $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].$xlsRow,display_number($db->f('quantity') * $price_line * ($db->f('packing') / $db->f('sale_unit'))));



  $xlsRow++;
}
}


$total_with_vat=$vat_total+$total-$discount_total+$shipping_price;
  // $total_with_vat=$vat_total+$total+$shipping_price;
  if(($currency_type != ACCOUNT_CURRENCY_TYPE) && $currency_rate){
    $total_default = $total_with_vat*return_value($currency_rate);
  }

if($shipping_price > 0){

$objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i-1].$xlsRow++,$labels['shipping_price']);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].($xlsRow-1),display_number($shipping_price),get_commission_type_list($currency_type));
}

$objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i-1].$xlsRow++,$labels['subtotal']);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].($xlsRow-1),display_number($total),get_commission_type_list($currency_type));


if($show_vat){
    if($sh_discount){
       $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i-1].$xlsRow++,$labels['discount']);
       $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].($xlsRow-1),'- '.display_number(number_format($discount_total,2)),get_commission_type_list($currency_type));
      }

    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i-1].$xlsRow++,$labels['vat']);
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].($xlsRow-1),display_number($vat_total),get_commission_type_list($currency_type));

    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i-1].$xlsRow++,$labels['amount']);
     $objPHPExcel->setActiveSheetIndex(0)->setCellValue($next_column[$i].($xlsRow-1),display_number($total_with_vat + $total_gov_taxes),get_commission_type_list($currency_type));
}

$objPHPExcel->setActiveSheetIndex(0)->getRowDimension('11')->setRowHeight(100);
$objPHPExcel->setActiveSheetIndex(0)->getStyle('A11')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A11', $labels['notes']);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells( 'B11:'.$next_column[$i].'11' );
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B11', $comments);
$objPHPExcel->setActiveSheetIndex(0)->getStyle('B11')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);


$objPHPExcel->getActiveSheet()->setTitle('Order');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save(INSTALLPATH.'upload/'.$filename);





define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',
  'csv' => 'text/csv',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload', $fname, $file_path);

if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}

// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
if(!$in['attach']){
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);


// download
//@readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);

      die();
    }
  }
  @fclose($file);
}
}

?>