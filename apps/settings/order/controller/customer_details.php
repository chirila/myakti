<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
$result = array('customer'=>array(),'contact'=>array());

$filter = "";
if($in['invoice']){
	$filter = " AND billing='1' ";
}
else{
	$filter = " AND is_primary='1' ";
}
if(!$in['customer_id']){
	json_out($result);
}
$customer_details = $db->query("SELECT customers.*, customer_addresses.*, country.name as country_n, customer_contact_language.name as c_lang
								FROM customers
								LEFT JOIN customer_addresses ON (customer_addresses.customer_id=customers.customer_id) $filter
								LEFT JOIN country ON (country.country_id=customer_addresses.country_id)
								LEFT JOIN customer_contact_language ON customers.language = customer_contact_language.id
								WHERE customers.customer_id=".$in['customer_id']." ");

$contacts = $db->query("SELECT customer_contacts.*, customer_contact_language.name as c_lang FROM customer_contacts
						LEFT JOIN customer_contact_language ON customer_contacts.language = customer_contact_language.id
						WHERE customer_id=".$in['customer_id']." AND contact_id='".$in['contact_id']."' ");

if($customer_details->next()){
	$result['customer']= array(
		'customer_name'			=> $customer_details->f('name'),
		'customer_id'				=> $customer_details->f('customer_id'),
		'country'						=> $customer_details->f('country_n'),
		'state_dd'					=> $customer_details->f('state_id'),
		'city'							=> $customer_details->f('city'),
		'zip'								=> $customer_details->f('zip'),
		'address'						=> nl2br($customer_details->f('address')),
		// 'vat_dd'						=> build_vat_dd($customer_details->f('vat_id')),
		'btw_nr'						=> $in['bwt_nr'],
		'comp_start'				=> true,
		'language'					=> $customer_details->f('c_lang'),
		'bank_name'					=> $customer_details->f('bank_name'),
		'bank_bic_code'			=> $customer_details->f('bank_bic_code'),
		'bank_iban'					=> $customer_details->f('bank_iban'),
		'is_del_note'       => $in['del_note']?true:false,
		'del_note'					=> $customer_details->f('deliv_disp_note'),
	);
}
if($contacts->next()){
	$result['contact']=array(
		/* contact details */
		'first_name'				=> $contacts->f('firstname'),
		'last_name'					=> $contacts->f('lastname'),
		'contact_id'				=> $contacts->f('contact_id'),
		'email'							=> $contacts->f('email'),
		'phone'							=> $contacts->f('phone'),
		'c_start'						=> $in['customer_id'] && $in['invoice'] ? false : true,
		'c_language'				=> $contacts->f('c_lang'),
	);
}

json_out($result);
?>