<?php
if (!defined('INSTALLPATH')) {
	define('INSTALLPATH', '../../../');
}
if (!defined('BASEPATH')) {
	define('BASEPATH', INSTALLPATH . 'core/');
}
session_start();
include_once (INSTALLPATH . 'config/config.php');
include_once (BASEPATH . '/library/sqldb.php');
include_once (BASEPATH . '/library/sqldbResult.php');
include_once (INSTALLPATH . 'config/database.php');
include_once (BASEPATH . 'startup.php');


$q = strtolower($in["term"]);

global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($database_users);
if($q){
	$filter .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
}
if($in['current_id']){
	$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
}

if($in['customer_id']){
	$filter .= " AND customer_contacts.customer_id='".$in['customer_id']."'";
}
//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){

	$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
}
$items = array();
$db= new sqldb();

$title = array();
$titles = $db->query("SELECT * FROM customer_contact_title ")->getAll();
foreach ($titles as $key => $value) {
	$title[$value['id']] = $value['name'];
}

$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, 
				customers.name, customers.currency_id,customers.internal_language,customer_contacts.email,customer_contacts.title,country.name AS country_name,customer_contacts.position_n
				FROM customer_contacts
				LEFT JOIN customers ON customer_contacts.customer_id = customers.customer_id
				LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
				LEFT JOIN country ON country.country_id=customer_contact_address.country_id
				WHERE customer_contacts.active=1 $filter ORDER BY lastname limit 5")->getAll();


$result = array();
foreach ($contacts as $key => $value) {
	$contact_title = $title[$value['title']];
	if($contact_title){
		$contact_title .= " ";
	}		
    
    $name = $contact_title.$value['firstname'].' '.$value['lastname'];

  	$price_category = "1";
    if ($value['customer_id']){
  		$price_category = $value['cat_id'];
  	}

	$result[]=array(
		"id"					=> $value['contact_id'],
		"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
		"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
		"email"					=> $value['email'],
		"price_category_id"		=> $price_category, 
		'customer_id'	 		=> $value['customer_id'], 
		'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
		'currency_id' 			=> $value['currency_id'], 
		"lang_id" 				=> $value['internal_language'], 
		'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
		'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
		'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']) 

	);

}
array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
json_out($result);exit();

?>