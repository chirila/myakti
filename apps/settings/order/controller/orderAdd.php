<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $cfg,$database_config, $config;

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}

$user_lang = $_SESSION['l'];
if ($user_lang=='nl')
{
	$user_lang='du';
}
switch ($user_lang) {
	case 'en':
		$user_lang_id='1';
		break;
	case 'du':
		$user_lang_id='3';
		break;
	case 'fr':
		$user_lang_id='2';
		break;
}
if(!$in['languages']){
	$in['languages'] = $user_lang_id;
}
$in['language_dd2'] = build_language_dd_new($in['languages']);
$in['currency_type_list']	= build_currency_list(ACCOUNT_CURRENCY_TYPE);
$in['currency_id'] = ACCOUNT_CURRENCY_TYPE;
$in['multiple_identity'] = build_identity_dd($selected);

$result = array(

);

json_out($result);



?>