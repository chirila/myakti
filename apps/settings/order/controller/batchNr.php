<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$batch = array('b_n_row'=>array());

$is_data = false;

if(($in['order_id'] && $in['order_articles_id']) && ($in['count_quantity_nr'] > 0) ){
	$article_id = $db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '".$in['order_articles_id']."' ");
	if($in['delivery_approved']==1){
		$batch_number_data = $db->query("SELECT * FROM batches WHERE article_id = '".$article_id."' ORDER BY in_stock ASC ");
	}else{
		$batch_number_data = $db->query("SELECT * FROM batches WHERE article_id = '".$article_id."'  AND status_id = '1' AND in_stock > '0' ORDER BY in_stock ASC ");

	}

	$i = 0;

	$b_n_q_selected = intval('0.00');
	$rest = $in['count_quantity_nr'];

	$selected_quantity = 0;
	$selected_batches = 0;

	while($batch_number_data->next()){

		if( $rest > $batch_number_data->f('in_stock') ){
			$b_n_q_selected = intval($batch_number_data->f('in_stock'));
			$rest = $rest - $batch_number_data->f('in_stock');
		}elseif($rest<=$batch_number_data->f('in_stock') && $rest != 0 ){
			$b_n_q_selected = intval($rest);
			$rest = intval('0.00');
		}else{
			$b_n_q_selected = intval($rest);
			$rest = intval('0.00');
		}

		$selected_quantity += $b_n_q_selected;
		if($b_n_q_selected>0){
			$selected_batches++;
		}
		if($in['delivery_approved']){
			if($in['selected_items']){
				$quantity_sel = 0;
				foreach ($in['selected_items'] as $key => $value) {
					if($value['order_articles_id'] == $in['order_articles_id'] && $value['batch_id'] == $batch_number_data->f('id')){
						$quantity_sel = $value['batch_q'];
					}
				}
				$linie=array(
					'article_id'				=> $article_id,
					'order_articles_id'			=> $in['order_articles_id'],
					'batch_no_checked'			=> $quantity_sel > 0 ? true : false,
					'batch_checked'				=> $quantity_sel > 0 ? 'active' : '',
					'batch_number'				=> $batch_number_data->f('batch_number'),
					'in_stock'					=> intval($batch_number_data->f('in_stock')+$quantity_sel),
					'b_n_q_selected'				=> $quantity_sel > 0 ? $quantity_sel : 0 ,
					'b_n_id'					=> $batch_number_data->f('id'),
					'red_row'					=> (intval($batch_number_data->f('in_stock'))+$quantity_sel) < $quantity_sel ? 'red' : '',
				);
			}else{
				$linie=array(
					'article_id'				=> $article_id,
					'order_articles_id'			=> $in['order_articles_id'],
					'batch_no_checked'			=> $b_n_q_selected > 0 ? true : false,
					'batch_checked'				=> $b_n_q_selected > 0 ? 'active' : '',
					'batch_number'				=> $batch_number_data->f('batch_number'),
					'in_stock'					=> intval($batch_number_data->f('in_stock')),
					'b_n_q_selected'				=> $b_n_q_selected,
					'b_n_id'					=> $batch_number_data->f('id'),
				);
			}
		}else{
			if($in['selected_items']){
				$quantity_sel = 0;
				foreach ($in['selected_items'] as $key => $value) {
					if($value['order_articles_id'] == $in['order_articles_id'] && $value['batch_id'] == $batch_number_data->f('id')){
						$quantity_sel = $value['batch_q'];
					}
				}
				$linie=array(
					'article_id'				=> $article_id,
					'order_articles_id'			=> $in['order_articles_id'],
					'batch_no_checked'			=> $quantity_sel > 0 ? true : false,
					'batch_checked'				=> $quantity_sel > 0 ? 'active' : '',
					'batch_number'				=> $batch_number_data->f('batch_number'),
					'in_stock'					=> intval($batch_number_data->f('in_stock')),
					'b_n_q_selected'				=> $quantity_sel > 0 ? $quantity_sel : 0 ,
					'b_n_id'					=> $batch_number_data->f('id'),
					'red_row'					=> intval($batch_number_data->f('in_stock')) < $quantity_sel ? 'red' : '',
				);
			}else{
				$linie=array(
					'article_id'				=> $article_id,
					'order_articles_id'			=> $in['order_articles_id'],
					'batch_no_checked'			=> $b_n_q_selected > 0 ? true : false,
					'batch_checked'				=> $b_n_q_selected > 0 ? 'active' : '',
					'batch_number'				=> $batch_number_data->f('batch_number'),
					'in_stock'					=> intval($batch_number_data->f('in_stock')),
					'b_n_q_selected'				=> $b_n_q_selected,
					'b_n_id'					=> $batch_number_data->f('id'),
				);
			}
		}
		array_push($batch['b_n_row'],$linie);
		// $view_b_n->loop('b_n_row');
		$i++;
	}

// $view_b_n->assign(array(
	$batch['is_delivery_id']			= true;
	// ));

}else{//only view files
	$i = 0;
	$article_id = $db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '".$in['order_articles_id']."' ");
	$batch_number_data = $db->query("SELECT  batches_from_orders.*, batches.batch_number,batches.in_stock FROM batches_from_orders
		LEFT JOIN batches
		ON batches_from_orders.batch_id = batches.id
		WHERE 	batches_from_orders.article_id =	'".$article_id."'
		AND 		batches_from_orders.delivery_id 		= 	'".$in['delivery_id']."'
		AND 		order_articles_id 	= 	'".$in['order_articles_id']."' ");
	while($batch_number_data->next()){
		$quantity_sel = 0;
		if($in['selected_items']){
			foreach ($in['selected_items'] as $key => $value) {
				if($value['order_articles_id'] == $in['order_articles_id'] && $value['batch_id'] == $batch_number_data->f('id')){
					$quantity_sel = $value['batch_q'];
				}
			}
		}
		$linie=array(
			'article_id'				=> $article_id,
			'order_articles_id'			=> $in['order_articles_id'],
			'batch_no_checked'			=> true,
			'batch_checked'				=> 'active',
			'batch_number'				=> $batch_number_data->f('batch_number'),
			'in_stock'					=> intval($batch_number_data->f('in_stock'))+intval($batch_number_data->f('quantity')),
			'b_n_q_selected'				=> intval($batch_number_data->f('quantity')),
			'b_n_id'					=> $batch_number_data->f('id'),
			'red_row'					=> intval($batch_number_data->f('in_stock')) < $quantity_sel ? 'red' : '',
			'disabled_on_view'			=> true,
		);

		array_push($batch['b_n_row'],$linie);

		$i++;
	}
	// $view_b_n->assign(array(
		$batch['is_delivery_id']			= false;
	// ));

}
if($i>0){
	$is_data = true;
}
// $view_b_n->assign(array(
$batch['is_data']							= $is_data;
$batch['order_articles_id']		= $in['order_articles_id'];
$batch['total_b_n']						= $selected_quantity;
$batch['selected_b_n']				= $in['count_quantity_nr'];
$batch['green']								= $selected_quantity == $in['count_quantity_nr'] ? 'green' : '';
$batch['selected_batches']		= $selected_batches;
// ));
if($in['selected_items']){
	$selected_quantity = 0;
	$selected_batches = 0;
	foreach ($in['selected_items'] as $key => $value) {
		$selected_quantity+=$value['batch_q'];
		$selected_batches++;
	}
	// $view_b_n->assign(array(
	$batch['total_b_n']						= $selected_quantity;
	$batch['selected_b_n']				= $in['count_quantity_nr'];
	$batch['green']								= $selected_quantity == $in['count_quantity_nr'] ? 'green' : '';
	$batch['selected_batches']		= $selected_batches;
	// ));
}


json_out($batch);
?>