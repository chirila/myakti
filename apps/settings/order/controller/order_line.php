<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$o=array('recipients'=>array(),'lines'=>array(),'order_articles_id' =>array());

if(ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS){
	$o['view_location']=true;
}else{
	$o['view_location']=false;
}

if($in['order_id'] && !$in['delivery_id']){
	$o['order_id'] =$in['order_id'];
	$o['is_add'] = true;
	$order =  $db->query("SELECT pim_orders.* FROM pim_orders WHERE pim_orders.order_id='".$in['order_id']."'");
	$order->move_next();
	$o['delivery_note'] = $in['delivery_note'];
	if($order->f('field') == 'customer_id'){
		$customer_id = $order->f('customer_id');
		$c_details = $db->query("SELECT * FROM customers WHERE customer_id ='".$customer_id."' ");
		$o['delivery_note'] = $c_details->f('deliv_disp_note');
	}

	$allow_packing = $order->f('use_package');
	$allow_sale_unit = $order->f('use_sale_unit');

	$o['allow_article_packing']       = $allow_packing;
	$o['allow_article_sale_unit']     = $allow_sale_unit;


	if($order->f('delivery_address')){
		$o['delivery_address']=$order->gf('delivery_address');
	}else{
		$o['delivery_address']=$order->gf('address_info');
	}
	$customers_dep = $db->query("SELECT  dispatch_stock_address.* FROM  dispatch_stock_address order by is_default DESC")->getAll();
	$un_query = $db->query("SELECT  dispatch_stock.*,pim_stock_disp.to_customer_id,pim_stock_disp.to_naming,pim_stock_disp.to_address_id,pim_stock_disp.to_address,pim_stock_disp.to_zip,pim_stock_disp.to_city,pim_stock_disp.to_country
           FROM  dispatch_stock
           INNER JOIN pim_stock_disp ON pim_stock_disp.to_customer_id=dispatch_stock.customer_id and pim_stock_disp.to_address_id=dispatch_stock.address_id
           WHERE dispatch_stock.customer_id!=0
           GROUp BY dispatch_stock.customer_id,dispatch_stock.address_id")->getAll();

	$i = 0;
	$j = 0;
	$k = 0;
	$o['location_dd']=build_dispach_location_dd($in['select_main_location'],$article_ids);
	$o['select_main_location'] = $in['select_main_location'] ? $in['select_main_location'] : $o['location_dd'][0]['id'];
	$line = $db->query("SELECT * FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND article_id!=0 AND delivered='0' AND content='0' ORDER BY sort_order ASC ");
	$article_ids="";
	while ($line->next()) {
		$delivered = 0;
		$left = 0;
		$delivered = $db->field("SELECT SUM(quantity) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$line->f('order_articles_id')."' ");

		$use_serial_no = $db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
		if($use_serial_no==1){
			$j++;
		}

		$use_batch_no = $db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
		if($use_batch_no==1){
			$k++;
		}

		$hide_stock = $db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$line->f('article_id')."' ");
		$left = $line->f('quantity') - $delivered;

		$packing = $db->field("SELECT packing FROM pim_articles WHERE article_id='".$line->f('article_id')."' ");
		$sale_unit = $db->field("SELECT sale_unit FROM pim_articles WHERE article_id='".$line->f('article_id')."' ");
		$array_disp = array();
		$nr = 0;
		$new_qty=0;
		foreach ($customers_dep as $key => $value) {
		  $qty=$db->field("SELECT dispatch_stock.stock FROM dispatch_stock WHERE article_id = '".$line->f('article_id')."' and address_id='".$value['address_id']."' and main_address=1");
		  array_push($array_disp, array(
         'quantity'                  => display_number($qty),
         'qty'                  		 => $qty,
         'quantity_delivered'        => display_number($view_quantity),
         'order_articles_id'         => $line->f('order_articles_id'),
         'customer_name'           	 =>  $value['naming'],
         'customer_id'               =>  0,
         'address_id'                =>  $value['address_id'],
         'order_buyer_country'       => get_country_name($value['country_id']),
         'order_buyer_state'         => $value['state'],
         'order_buyer_city'          => $value['city'],
         'order_buyer_zip'           => $value['zip'],
         'order_buyer_address'       => $value['address'],
         'cust_addr_id'							 => '0-'.$value['address_id'],
         'is_error'									 => $nr == 0 && ($left > $qty) ? true : false,
      ));
      if($nr==0){
      	$new_qty=$qty;
      }
		  $nr++;
		}
		foreach ($un_query as $key => $value) {

		  $qty=$db->field("SELECT dispatch_stock.stock FROM dispatch_stock WHERE article_id = '".$line->f('article_id')."' and address_id='".$value['to_address_id']."' and customer_id='".$value['customer_id']."'");
		  array_push($array_disp, array(
				'quantity'								 	=>  display_number($qty),
				'qty'                  		 	=> $qty,
				'quantity_delivered'        => display_number($in['quantity_delivered']),
				'order_articles_id'					=> $line->f('order_articles_id'),
				'customer_name'						 	=> $value['to_naming'],
				'customer_id'   						=> $value['to_customer_id'],
				'address_id'    						=> $value['to_address_id'],
				'type'          						=> 2,
				'order_buyer_country'     	=> $value['to_country'],
				'order_buyer_city'         	=> $value['to_city'],
				'order_buyer_zip'          	=> $value['to_zip'],
				'order_buyer_address'      	=> $value['to_address'],
				'cust_addr_id'							=> $value['customer_id'].'-'.$value['address_id'],
				'is_error'									=> false,
		  ));

		}

		array_push($o['lines'], array(
			'article'											=> $line->f('article'),
			'article_id'									=> $line->f('article_id'),
      'quantity_value'							=> $left > $new_qty ? $new_qty : $left,
			'quantity'										=> $hide_stock ? display_number($left) : ($left > $new_qty ? display_number($new_qty) : display_number($left)),
			'hide_stock'									=> $hide_stock ,
			'quantity_value2'							=> $left,
			'quantity2'										=> display_number($left),
			'order_articles_id' 					=> $line->f('order_articles_id'),
			'show_stock'           				=> $hide_stock == 0 ? true : false,
			'use_serial_no_art'						=> $use_serial_no == 1 ? true : false,
			'count_selected_s_n'					=> 0,
			'selected_s_n'								=> '',
			'use_batch_no_art'						=> $use_batch_no == 1 ? true : false,
			'count_selected_b_n'					=> 0,
			'allow_article_packing'       => $allow_packing,
			'allow_article_sale_unit'     => $allow_sale_unit,
			'packing'											=> remove_zero_decimals($packing),
			'sale_unit'										=> $sale_unit,
			'view_location'								=> ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS ? true : false,
			'disp_addres_info' 						=> $array_disp,
			'is_error'										=> $hide_stock ? false : ($left > $new_qty ? true : false),
		));
		array_push($o['order_articles_id'] , $line->f('order_articles_id'));
		$i++;
	}

	$o['use_serial_no'] 	= $j > 0 && ALLOW_STOCK ? true : false;
	$o['use_batch_no'] 		= $k > 0 && ALLOW_STOCK ? true : false;



}elseif($in['delivery_id']){
	$o['order_id'] = $in['order_id'];
	$o['delivery_id'] = $in['delivery_id'];
	$order =  $db->query("SELECT pim_orders.* FROM pim_orders WHERE pim_orders.order_id='".$in['order_id']."'");
	$order->next();

	$allow_packing = $order->f('use_package');
	$allow_sale_unit = $order->f('use_sale_unit');

	$in['edit_note']=1;
  $del = $db->query("SELECT pim_order_deliveries.*,pim_orders_delivery.delivery_note
  	FROM  pim_order_deliveries
  	INNER JOIN pim_orders_delivery ON pim_orders_delivery.delivery_id = pim_order_deliveries.delivery_id
  	WHERE pim_order_deliveries.order_id='".$in['order_id']."' AND pim_order_deliveries.delivery_id='".$in['delivery_id']."'");

  $del->next();
  /*$o['date_del_line_h']  	= $del->gf('date');
  $o['date_del_line']   	= date(ACCOUNT_DATE_FORMAT,$del->gf('date'));
  $o['contact_name'] 			= $del->gf('contact_name');
  $o['contact_id'] 				= $del->gf('contact_id');*/

  /*$view_list->assign(array(
		'hour_dd'      => hourmin('hour','minute','pm', $db->gf('hour'),$db->gf('minute'),$db->gf('pm')),*/
	$o['delivery_note']	= $del->gf('delivery_note');
	$o['is_add']       	=  false;
		// 'is_edit'      =>  1,
	$o['order_info']   	= array();

###### view order line ######
	/*$delis = $db->query("SELECT pim_order_deliveries.*,pim_orders_delivery.delivery_note
      FROM  pim_order_deliveries
      INNER JOIN pim_orders_delivery ON pim_orders_delivery.delivery_id = pim_order_deliveries.delivery_id
      WHERE pim_order_deliveries.order_id='".$in['order_id']."' AND pim_order_deliveries.delivery_id='".$in['delivery_id']."'");
*/

	$o['delivery_date'] 					= date(ACCOUNT_DATE_FORMAT,$del->gf('date'));
	$o['delivery_date_js'] 				= $del->gf('date')*1000;
	$o['hour']          					= $del->f('hour')?$del->f('hour'):'';
	$o['minute']        					= $del->f('minute')?$del->f('minute'):'';
	$o['pm']            					= $del->f('pm')?$del->f('pm'):'';
	$o['contact_name']  					= $del->f('contact_name')?$del->f('contact_name'):'-';
	$o['contact_id']  						= $del->f('contact_id')?$del->f('contact_id'):'';
	$o['delivery_address_txt']  	= nl2br($del->f('delivery_address'));
	$o['lines'] 		= array();
	$i = 0;
	$line = $db->query("SELECT pim_order_articles.*, pim_orders_delivery.* FROM pim_order_articles
					INNER JOIN pim_orders_delivery ON pim_orders_delivery.order_articles_id=pim_order_articles.order_articles_id
					WHERE pim_orders_delivery.order_id='".$in['order_id']."' AND pim_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC");
	while ($line->next()) {
		$delivered = 0;
		$left = 0;
		$delivered = $db->field("SELECT SUM(quantity) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$line->f('order_articles_id')."' ");
		$left = $line->f('quantity');

    $packing = $line->f('packing');
    $sale_unit = $line->f('sale_unit');

		$linie = array(
			'article'							=> $line->f('article'),
			'quantity'						=> display_number($left),
			'order_articles_id' 	=> $line->f('order_articles_id'),
      'packing'       			=> remove_zero_decimals($packing),
      'sale_unit'       		=> $sale_unit,
		);

    //article serial numbers
    $use_serial_nr = $db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
    if( $use_serial_nr == 1 ){
      $article_s_n_data = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$line->f('article_id')."' AND delivery_id = '".$in['delivery_id']."' ");
      $k = 0;
      $selected_s_n ='';
      while($article_s_n_data->next()){
          if($k==0){
            $selected_s_n .= $article_s_n_data->f('id');
          }else{
            $selected_s_n .= ','.$article_s_n_data->f('id');
          }
        $k++;
      }
      if($k>0){
        $use_serial_no = true;
        $linie['use_serial_no_art']     = true;
      }
      $count_selected_s_n = $k;
      $linie['selected_s_n']          = $selected_s_n;
      $linie['count_selected_s_n']    = $count_selected_s_n;
    }

    // article batch numbers
    $use_batch_nr = $db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
    if( $use_batch_nr == 1 ){
      $article_b_n_data = $db->query("SELECT * FROM batches WHERE article_id = '".$line->f('article_id')."' AND delivery_id = '".$in['delivery_id']."' ");
      $m = 0;
      while($article_b_n_data->next()){
        $m++;
      }
      if($m > 0){
        $use_batch_no = true;
        $linie['use_batch_no_art']     = true;
      }
    }

	  if(ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS){
      $linie['view_location']=true;
      $j = 0;

      $locations=$db->field("SELECT stock_movements.location_info  FROM stock_movements
        WHERE stock_movements.article_id='".$line->f('article_id')."' AND stock_movements.delivery_id='".$in['delivery_id']."'");
      $locations=unserialize($locations);
      $linie['locations']=array();
      if(!empty($locations)){
	      foreach ($locations as $customer => $value) {
					if(return_value($value)!='0.00'){
	          $customeres=explode('-', $customer);
	          if($customeres[0]==0){
	            $customer_info=$db->query("SELECT dispatch_stock_address.* FROM dispatch_stock_address WHERE address_id='".$customeres['1']."'");
	          }else{
	            $customer_info=$db->query("SELECT  dispatch_stock.*,pim_stock_disp.to_naming,pim_stock_disp.to_address,pim_stock_disp.to_zip,pim_stock_disp.to_city,pim_stock_disp.to_country
	         		FROM  dispatch_stock
	         		INNER JOIN pim_stock_disp ON pim_stock_disp.to_customer_id=dispatch_stock.customer_id
	         		WHERE dispatch_stock.customer_id='".$customeres['0']."' and dispatch_stock.address_id='".$customeres['1']."' ");
	          }
						array_push($linie['locations'],array(
							'quantity_loc'=> remove_zero_decimals($value),
							'depo'   => $customer_info->f('naming')?$customer_info->f('naming'):$customer_info->f('to_naming'),
							'address'   =>$customer_info->f('address')?$customer_info->f('address'):$customer_info->f('to_address'),
							'city'     =>$customer_info->f('city')?$customer_info->f('city'):$customer_info->f('to_city'),
							'zip'   =>$customer_info->f('zip')?$customer_info->f('zip'):$customer_info->f('to_zip'),
							'country'   =>get_country_name($customer_info->f('country_id'))?get_country_name($customer_info->f('country_id')):get_country_name($customer_info->f('to)country_id')),
						));
	        }
	      }
	    }
      $j++;
	  }else{
	  	$linie['view_location']=false;
	  }

		array_push($o['lines'],$linie);
		array_push($o['order_articles_id'] , $linie['order_articles_id']);
		$o['order_info']['delivery_note']=$line->f('delivery_note');
		$i++;

	}
	if($i>0){
		$data = true;
	}

  $o['allow_article_packing']       = $allow_packing;
  $o['allow_article_sale_unit']     = $allow_sale_unit;
  $o['use_serial_no']         			= $use_serial_no;
  $o['use_batch_no']         				= $use_batch_no;

###### view order line ######

  if($del->f('delivery_address')){
   	$o['delivery_address']=$del->f('delivery_address');
  }
  else{
  	$order =  $db->query("SELECT pim_orders.* FROM pim_orders WHERE pim_orders.order_id='".$in['order_id']."'");

		if($order->f('delivery_address')){
			$o['delivery_address']=$order->gf('delivery_address');
		}else{
			$o['delivery_address']=$order->f('address_info');
		}
  }

}
$tblinvoice_customer_contacts = $db->query("SELECT customer_contacts.*,pim_orders.order_id,pim_orders.customer_id,pim_orders.serial_number
	             FROM pim_orders
	             INNER JOIN customer_contacts ON customer_contacts.customer_id=pim_orders.customer_id
	             WHERE pim_orders.order_id='".$in['order_id']."' AND customer_contacts.active ='1'");

while ($tblinvoice_customer_contacts->move_next()){
	$recipient = array(
		'recipient_name'  			=> $tblinvoice_customer_contacts->f('firstname').' '.$tblinvoice_customer_contacts->f('lastname'),
		'recipient_email' 			=> $tblinvoice_customer_contacts->f('email'),
		'recipient_id'    			=> $tblinvoice_customer_contacts->f('contact_id'),
	);
	array_push($o['recipients'], $recipient);
}

json_out($o);