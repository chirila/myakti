<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$is_data = false;
$serial = array('serial_nr_row'=>array(),'sel_serial_nr_row'=>array(),'view_sel_serial_nr_row'=>array());

if(($in['order_id'] && $in['order_articles_id']) && ($in['count_serial_nr'] > 0)){
	$article_id = $db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '".$in['order_articles_id']."' ");
	if($in['selected_s_n']){
		$is_data = true;
		$selected_s_n = count(explode(',', $in['selected_s_n']));
		$serial_numbers = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' AND id NOT IN(".$in['selected_s_n'].") ORDER BY serial_number ASC ");
		$sel_serial_no = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' AND id IN(".$in['selected_s_n'].") ORDER BY serial_number ASC ");
		if($in['delivery_approved']){
			$sel_serial_no = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND id IN(".$in['selected_s_n'].") ORDER BY serial_number ASC ");
		}
	}else{
		$selected_s_n = 0;
		$serial_numbers = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' ORDER BY serial_number ASC ");
		$sel_serial_no = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' AND id < '0' ");
	}
	$i=0;
	while($serial_numbers->next()){
		array_push($serial['serial_nr_row'],array(
			'serial_number'		=> $serial_numbers->f('serial_number'),
			'serial_nr_id'		=> $serial_numbers->f('id'),
		));
		$i++;
	}


	while($sel_serial_no->next()){
		array_push($serial['sel_serial_nr_row'],array(
			'sel_serial_number'		=> $sel_serial_no->f('serial_number'),
			'sel_serial_nr_id'		=> $sel_serial_no->f('id'),
		));
		$j++;
	}

	$serial['order_articles_id']		= $in['order_articles_id'];
	$serial['overflow_y']						= $i > 9 ?  'style="overflow-y:scroll;"' : '';
	$serial['sel_overflow_y']				= $j > 9 ?  'style="overflow-y:scroll;"' : '';
	$serial['all_green']						= $selected_s_n == $in['count_serial_nr'] ? 'all_green' : '';
	$serial['sel_s_n']							= $selected_s_n ? $selected_s_n : 0;
	$serial['total_s_n']						= $in['count_serial_nr'];
	$serial['not_delivery_id']			= true;
	$serial['hide_if_delivery']			= true;
	$serial['column']								= '6';
	if($in['delivery_id']){
		if($in['delivery_approved'] !=1){
			$serial['hide_if_delivery']		= false;
			$serial['column']							= '12';
			$serial['view_sn']						= 'view_sn';
			$serial['not_delivery_id']		= false;
			$sel_serial_numbers = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND delivery_id = '".$in['delivery_id']."' ORDER BY serial_number ASC ");
			while($sel_serial_numbers->next()){
				array_push($serial['sel_serial_nr_row'],array(
					'serial_nr_id'	=> $sel_serial_numbers->f('id'),
					'serial_number'	=> $sel_serial_numbers->f('serial_number'),
				));
			}
		}
	}
}
if($i>0){
	$is_data = true;
}

$serial['is_data']				= $is_data;

json_out($serial);
?>