<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
$db6= new sqldb();
$o=array('recipients'=>array(),'lines'=>array(),'order_articles_id' =>array());

if(ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS){
	$o['view_location']=true;
}else{
	$o['view_location']=false;
	$o['quantity_last']='last';
}

//delivery data without articles
$in['edit_note']=1;
$db->query("SELECT pim_order_deliveries.*,pim_orders_delivery.delivery_note
		FROM  pim_order_deliveries
		INNER JOIN pim_orders_delivery ON pim_orders_delivery.delivery_id = pim_order_deliveries.delivery_id
		WHERE pim_order_deliveries.order_id='".$in['order_id']."' AND pim_order_deliveries.delivery_id='".$in['delivery_id']."'");
$db->move_next();
$in['date_del_line_h']  = $db->gf('date');
$o['date_del_line_h']		= $db->gf('date')*1000;
$in['date_del_line'] 		= date(ACCOUNT_DATE_FORMAT,$db->gf('date'));
$o['date_del_line'] 		= date(ACCOUNT_DATE_FORMAT,$db->gf('date'));
$in['contact_name'] 		= $db->gf('contact_name');
$o['contact_name'] 			= $db->gf('contact_name');
$in['contact_id'] 			= $db->gf('contact_id');
$o['contact_id'] 				= $db->gf('contact_id');
$o['hour']          		= $db->f('hour')?$db->f('hour'):'';
$o['minute']        		= $db->f('minute')?$db->f('minute'):'';
$o['pm']            		= $db->f('pm')?$db->f('pm'):'';

$order =  $db->query("SELECT pim_orders.* FROM pim_orders WHERE pim_orders.order_id='".$in['order_id']."'");
$order->move_next();
$allow_packing = $order->f('use_package');
$allow_sale_unit = $order->f('use_sale_unit');

// $view_list->assign(array(
$o['allow_article_packing']       = $allow_packing;
$o['allow_article_sale_unit']     = $allow_sale_unit;
// ));

// $view_list->assign(array(
	// 'hour_dd'      => hourmin('hour','minute','pm', $db->gf('hour'),$db->gf('minute'),$db->gf('pm')),
$o['delivery_note']= $db->gf('delivery_note');
// ));

if($db->f('delivery_address')){
	$o['delivery_address']=$db->f('delivery_address');
}else{
	$order =  $db->query("SELECT pim_orders.* FROM pim_orders WHERE pim_orders.order_id='".$in['order_id']."'");
	if($order->f('delivery_address')){
		// $view_list->assign(array(
		$o['delivery_address']=$order->gf('delivery_address');
		// ));
	}else{
		// $view_list->assign(array(
		$o['delivery_address']=$order->f('address_info');
		// ));
	}
}

//articles data
$i = 0;
$j = 0;
$m = 0;
$article_ids="";
$customers_dep = $db->query("SELECT  dispatch_stock_address.* FROM  dispatch_stock_address order by is_default DESC")->getAll();
$un_query = $db->query("SELECT  dispatch_stock.*,pim_stock_disp.to_customer_id,pim_stock_disp.to_naming,pim_stock_disp.to_address_id,pim_stock_disp.to_address,pim_stock_disp.to_zip,pim_stock_disp.to_city,pim_stock_disp.to_country
           FROM  dispatch_stock
           INNER JOIN pim_stock_disp ON pim_stock_disp.to_customer_id=dispatch_stock.customer_id and pim_stock_disp.to_address_id=dispatch_stock.address_id
           WHERE dispatch_stock.customer_id!=0
           GROUp BY dispatch_stock.customer_id,dispatch_stock.address_id")->getAll();
$line = $db->query("	SELECT pim_order_articles.*, pim_orders_delivery.* FROM pim_order_articles
				INNER JOIN pim_orders_delivery ON pim_orders_delivery.order_articles_id=pim_order_articles.order_articles_id
				WHERE pim_orders_delivery.order_id='".$in['order_id']."' AND pim_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC");
while($line->next()) {
	$array_disp = array();
	foreach ($customers_dep as $key => $value) {
	  $qty=$db->field("SELECT dispatch_stock.stock FROM dispatch_stock WHERE article_id = '".$line->f('article_id')."' and address_id='".$value['address_id']."' and main_address=1");
	  array_push($array_disp, array(
       'quantity'                  => display_number($qty),
       'qty'                  		 => $qty,
       'quantity_delivered'        => display_number($view_quantity),
       'order_articles_id'         => $line->f('order_articles_id'),
       'customer_name'           	 =>  $value['naming'],
       'customer_id'               =>  0,
       'address_id'                =>  $value['address_id'],
       'order_buyer_country'       => get_country_name($value['country_id']),
       'order_buyer_state'         => $value['state'],
       'order_buyer_city'          => $value['city'],
       'order_buyer_zip'           => $value['zip'],
       'order_buyer_address'       => $value['address'],
       'cust_addr_id'							 => '0-'.$value['address_id'],
       'is_error'									 => $nr == 0 && ($left > $qty) ? true : false,
    ));
    if($nr==0){
    	$new_qty=$qty;
    }
	  $nr++;
	}
	foreach ($un_query as $key => $value) {

		  $qty=$db->field("SELECT dispatch_stock.stock FROM dispatch_stock WHERE article_id = '".$line->f('article_id')."' and address_id='".$value['to_address_id']."' and customer_id='".$value['customer_id']."'");
		  array_push($array_disp, array(
				'quantity'								 	=>  display_number($qty),
				'qty'                  		 	=> $qty,
				'quantity_delivered'        => display_number($in['quantity_delivered']),
				'order_articles_id'					=> $line->f('order_articles_id'),
				'customer_name'						 	=> $value['to_naming'],
				'customer_id'   						=> $value['to_customer_id'],
				'address_id'    						=> $value['to_address_id'],
				'type'          						=> 2,
				'order_buyer_country'     	=> $value['to_country'],
				'order_buyer_city'         	=> $value['to_city'],
				'order_buyer_zip'          	=> $value['to_zip'],
				'order_buyer_address'      	=> $value['to_address'],
				'cust_addr_id'							=> $value['customer_id'].'-'.$value['address_id'],
				'is_error'									=> false,
		  ));

		}
	$linie = array();
	$delivered = 0;
	$left = 0;
	$delivered = $db->field("SELECT SUM(quantity) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$line->f('order_articles_id')."' ");
	$left = $line->f('quantity');
	$hide_stock = $db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$line->f('article_id')."' ");

	// article serial numbers
	$use_serial_no = $db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
	if($use_serial_no==1){
		$j++;
		$article_s_n_data = $db6->query("SELECT id FROM serial_numbers WHERE article_id = '".$line->f('article_id')."' AND delivery_id = '".$in['delivery_id']."' ");
		$k = 0;
		$selected_s_n =array();
		while($article_s_n_data->next()){
		    /*if($k==0){
		      $selected_s_n .= $article_s_n_data->f('id');
		    }else{
		      $selected_s_n .= ','.$article_s_n_data->f('id');
		    }*/
		    array_push($selected_s_n, array('serial_nr_id'=>$article_s_n_data->f('id')));
		  $k++;
		}
		if($k>0){
		  $use_serial_no = true;
		  // $view_list->assign(array(
		  $linie['use_serial_no_art']     = true;
		  // ),'view_order_line');
		}
		$count_selected_s_n = $k;
		// $view_list->assign(array(
		 $linie['selected_s_n']          = $selected_s_n;
		 $linie['count_selected_s_n']    = $count_selected_s_n;
		// ),'view_order_line');
	}

	$packing = $db->field("SELECT packing FROM pim_articles WHERE article_id='".$line->f('article_id')."' ");
	$sale_unit = $db->field("SELECT sale_unit FROM pim_articles WHERE article_id='".$line->f('article_id')."' ");

	// $view_list->assign(array(
	$linie['article']											= $line->f('article');
	$linie['quantity']										= display_number($left);
	$linie['order_articles_id'] 					= $line->f('order_articles_id');
	$linie['article_id']									= $line->f('article_id');
	$linie['show_stock']           				= $hide_stock == 0 ? true : false;
	$linie['quantity_value']							= $left;
	$linie['use_serial_no_art']						= $use_serial_no == 1 ? true : false;
	$linie['serialOk']										= true;
	$linie['allow_article_packing']       = $allow_packing;
	$linie['allow_article_sale_unit']     = $allow_sale_unit;
	$linie['packing']											= remove_zero_decimals($packing);
	$linie['sale_unit']										= $sale_unit;
	$linie['disp_addres_info'] 						= $array_disp;
	// ),'view_order_line');


	// article batch numbers
	$use_batch_no = $db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
	if($use_batch_no==1){
		$batches_orders_data = $db6->query("SELECT * FROM batches_from_orders WHERE order_articles_id = '".$line->f('order_articles_id')."' AND delivery_id = '".$in['delivery_id']."' ");
		$batches_orders_data = $batches_orders_data->getAll();
		$linie['edit_batches']=array();
		$count_selected_b_n =0;
		$selected_b_n = array();
		foreach ($batches_orders_data as $key => $value) {
			array_push($linie['edit_batches'],array(
				'batch_id'			=> 		$value['batch_id'],
				'batch_q'			=> 		$value['quantity'],
				'order_articles_id'	=> 		$value['order_articles_id'],
			));
			array_push($selected_b_n, array('b_n_id'=>$value['batch_id'],'b_n_q_selected'=>$value['quantity']));
			$count_selected_b_n +=$value['quantity'];
			// ),'edit_batches');
			$m++;
			// $view_list->loop('edit_batches','view_order_line');
		}
		if($m>0){
		  $use_batch_no = true;
		  // $view_list->assign(array(
		  $linie['use_batch_no_art']     = true;
		  $linie['selected_b_n']				 = $selected_b_n;
		  $linie['count_selected_b_n']	 = $count_selected_b_n;
		  $linie['batchOk']							 = true;
		  $linie['green_b'] 		         = 'green_b';
		  // ),'view_order_line');
		}
	}


	if(ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS){
		$linie['view_location']=true;
		$j = 0;
		$linie['view_location_line']=array();
		$dispatch_stock_movements=$db->query("	SELECT dispatch_stock_movements.*  FROM dispatch_stock_movements
									WHERE dispatch_stock_movements.order_articles_id='".$line->f('order_articles_id')."'
									AND dispatch_stock_movements.delivery_id='".$in['delivery_id']."'");
		while($dispatch_stock_movements->next()) {
			$linie2 = array();
			if( $dispatch_stock_movements->f('dispatch_customer_id')==0 && $dispatch_stock_movements->f('stock_disp_delivery_id')==0 ){ /*base loc*/
				$customers_dep = $db->query("SELECT account_address.* FROM account_address WHERE account_address_id=1");
				// $view_list->assign(array(
				$linie2['customer_name']						= ACCOUNT_COMPANY;
				$linie2['depo']                   	= '/ '.$customers_dep->f('naming');
				$linie2['order_buyer_country']      = get_country_name($customers_dep->f('country_id'));
				$linie2['order_buyer_state']        = $customers_dep->f('state');
				$linie2['order_buyer_city']         = $customers_dep->f('city');
				$linie2['order_buyer_zip']          = $customers_dep->f('zip');
				$linie2['order_buyer_address']      = $customers_dep->f('address');
				// ),'view_location_line');
			}elseif($dispatch_stock_movements->f('stock_disp_delivery_id')){
				$stock_disp_id=0;
				$stock_disp_id=$db->field("SELECT stock_disp_id FROM pim_stock_disp_delivery WHERE stock_disp_delivery_id='".$dispatch_stock_movements->f('stock_disp_delivery_id')."'");
				$pim_stock_disp=$db->query("SELECT * FROM pim_stock_disp WHERE stock_disp_id='".$stock_disp_id."'");
				/*
				if($pim_stock_disp->move_next()){
					$view_list->assign(array(
						'customer_name'=>  $pim_stock_disp->f('customer_name'),
						'depo'=>    '',
						'order_buyer_state'        => $pim_stock_disp->f('customer_state'),
						'order_buyer_city'         => $pim_stock_disp->f('customer_city'),
						'order_buyer_zip'          => $pim_stock_disp->f('customer_zip'),
						'order_buyer_address'      => $pim_stock_disp->f('customer_address'),
						'order_buyer_country'      => get_country_name($pim_stock_disp->f('customer_country_id')),
					),'view_location_line');
					if($pim_stock_disp->f('delivery_address')){
						$view_list->assign(array(
							'delivery_address'=>nl2br($pim_stock_disp->f('delivery_address')),
							'is_delivery'=>true
						),'view_location_line');
					}else{
						$view_list->assign(array(
							'delivery_address'=>'',
							'is_delivery'=>false
						),'view_location_line');
					}
				}
				*/
			}
			// $view_list->assign(array(
				$linie2['quantity_loc']				= display_number($dispatch_stock_movements->f('quantity'));
			// ),'view_location_line');
			// $view_list->loop('view_location_line','view_order_line');
			array_push($linie['view_location_line'], $linie2);
		}
	}else{
		$linie['view_location']=false;
	}
	$article_ids.= $line->f('article_id').',';
	// $view_list->loop('view_order_line');
	array_push($o['lines'],$linie);
	array_push($o['order_articles_id'] , $linie['order_articles_id']);
	$o['delivery_note']=$line->f('delivery_note');
	$i++;
}
if($i>0){
	$data = true;
	$article_ids=rtrim($article_ids,',');
	// $view_list->assign('location_dd',build_dispach_location_dd($in['select_main_location'],$article_ids));
		$o['location_dd']=build_dispach_location_dd($in['select_main_location'],$article_ids);
		$o['select_main_location'] = $in['select_main_location'] ? $in['select_main_location'] : $o['location_dd'][0]['id'];
}

$o['style']     		= ACCOUNT_NUMBER_FORMAT;
$o['is_data']				= $data;
$o['edit_note'] 		= $in['edit_note'] ? true : false;


// $view_list->assign(array(
	$o['use_serial_no'] 		= $j > 0 && ALLOW_STOCK ? true : false;
	$o['use_batch_no'] 			= $m > 0 && ALLOW_STOCK ? true : false;
// ));



//order data
$del_nr = $db->field("SELECT count(delivery_id) FROM pim_order_deliveries WHERE order_id='".$in['order_id']."' ");

$in['del_nr'] = $del_nr;

$order =  $db->query("SELECT pim_orders.* ,customer_contacts.firstname,customer_contacts.lastname, customer_contacts.customer_id as cust_id, customer_contacts.contact_id
      FROM pim_orders
      LEFT JOIN customer_contacts ON customer_contacts.contact_id=pim_orders.customer_id
      WHERE pim_orders.order_id='".$in['order_id']."'
     ");
if($order->f('field') == 'customer_id'){

	$o['is_buyer']=true;
	$in['buyer_id']      	= $order->f('customer_id');
	$o['buyer_id']      	= $order->f('customer_id');

}else{
	$o['is_buyer']=false;
}

if(!$in['contact_name']){
    	$in['contact_name']= $order->f('contact_name');
    	$o['contact_name']= $order->f('contact_name');
}

// $view_list->assign(array(
$o['do_me']									= $in['delivery_id'] ? 'order--order-edit_delivery' : 'order--order-make_delivery';
$o['style']     						= ACCOUNT_NUMBER_FORMAT;
$o['is_data']								= $data;
$o['pick_date_format']      = pick_date_format(ACCOUNT_DATE_FORMAT);
$o['mark_as_delivered']			= $order->f('rdy_invoice') == 1 ? 1 : 0;
$o['is_add']								= true;
$o['order_id'] 							= $in['order_id'];
$o['delivery_id'] 					= $in['delivery_id'];
$o['delivery_approved']			= "1";
// ));

json_out($o);
?>