<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
global $config;

$in['message'] = stripslashes($in['message']);
$order = $db->query("SELECT * FROM pim_orders WHERE order_id='".$in['order_id']."'");
$contact = $db->query("SELECT firstname,lastname,title FROM customer_contacts WHERE contact_id='".$order->f('contact_id')."'");
$title_cont = $db->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'");
$customer = $db->field("SELECT name FROM customers WHERE customer_id='".$order->f('buyer_id')."'");
$in['message']=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_ORDER."\" alt=\"\">",$in['message']);
$in['message']=str_replace('[!CONTACT_FIRST_NAME!]',"".$contact->f('firstname')."",$in['message']);
$in['message']=str_replace('[!CONTACT_LAST_NAME!]',"".$contact->f('lastname')."",$in['message']);
$in['message']=str_replace('[!SALUTATION!]',"".$title_cont."",$in['message']);
$in['message']=str_replace('[!SERIAL_NUMBER!]',"".$order->f('serial_number')."",$in['message']);
$in['message']=str_replace('[!DATE!]',"".date('m/d/Y', $order->f('date'))."",$in['message']);
$in['message']=str_replace('[!SUBJECT!]',"".$contact->f('del_note')."",$in['message']);
$in['message']=str_replace('[!DUE_DATE!]',"".date('m/d/Y', $order->f('del_date'))."",$in['message']);
$in['message']=str_replace('[!YOUR_REFERENCE!]',"".$order->f('your_ref')."",$in['message']);

$result = array(
  'e_message' => nl2br($in['message'])
);
json_out($result);
?>