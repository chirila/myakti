<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}

	function get_codelabels($in,$showin=true,$exit=true){
		$db = new sqldb();
		
		$content='';
		if($in['header']=='header'){
			$data_exist=$db->field("SELECT id FROM tblquote_pdf_data WHERE master='order' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' ");
			if($data_exist){
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist."' ");
			}else{
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE master='order' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."' ");
			}
		}else{

			$data_exist=$db->field("SELECT id FROM tblquote_pdf_data WHERE master='order' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' ");

			if($data_exist){
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE id='".$data_exist."' ");
			}else{
				$content=$db->field("SELECT content FROM tblquote_pdf_data WHERE master='order' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."' ");
			}
		}
		$data['header']=$in['header'];
		$data['footer']=$in['footer'];
		$data['variable_data'] = $content;
		$data['layout']=$in['layout'];
			
		return json_out($data, $showin,$exit);
	}

	function get_selectlabels($in,$showin=true,$exit=true){
		$db = new sqldb();
		/*$data = array( 'custom_variable'=>array(), 'light'=>array());*/
		$quote_columns=$db->query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'label_language_order' AND table_schema ='".DATABASE_NAME."' AND column_name not in ('label_language_id') AND column_name not in ('name') AND column_name not in ('lang_code') AND column_name not in ('active') ");
		$quote=$db->field("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'label_language_order' AND table_schema ='".DATABASE_NAME."' AND column_name not in ('label_language_id') AND column_name not in ('name') AND column_name not in ('lang_code') AND column_name not in ('active') ");

		$columns=array();
		$make_out=array();
		if(!$in['make_id']){
			$in['make_id']='0';
		}
		while($quote_columns->next()){
			$temp_description=$db->field("SELECT `{$quote_columns->f('COLUMN_NAME')}` FROM label_language_order WHERE lang_code='".$_SESSION['l']."' ");
			if(!$temp_description){
				continue;
			}
			array_push($make_out, array(
            	'id_value'=>$quote_columns->f('COLUMN_NAME'),
            	'name'=>ucfirst($temp_description),
        	));
		}

		$data['make_id']=$in['make_id'];
		$data['make']=$make_out;

		asort($columns);
		

		$final_select='';
		foreach($columns as $key=>$value){
			array_push($data['labels'], array(
				'code'		=> $key,
				'value'		=> $value,
			));
		}
		
		$quote_colum=$db->field("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'label_language_order' AND table_schema ='".DATABASE_NAME."' AND column_name not in ('label_language_id') AND column_name not in ('name') AND column_name not in ('lang_code') AND column_name not in ('active')  AND COLUMN_NAME='".$in['make_id']."'");

		if($data['make_id']!='0'){
			$data['selected_make']='[label]'.$quote_colum.'[/label]';
		}
		return json_out($data, $showin,$exit);
	}
	function get_selectdetails($in,$showin=true,$exit=true){
		$db = new sqldb();
		/*$data = array( 'custom_variable'=>array(), 'light'=>array());*/
		$array_values=array();
		$array_values['invoice_contact_name']=gm("Contact name");
		$array_values['buyer_reference']=gm("Buyer reference");
		$array_values['buyer_name']=gm("Buyer name");
		$array_values['buyer_country']=gm("Buyer country");
		$array_values['buyer_city']=gm("Buyer city");
		$array_values['buyer_zip']=gm("Buyer zip");
		$array_values['buyer_address']=gm("Buyer address");
		$array_values['serial_number']=gm("Serial number");
		$array_values['order_date']=gm("Order date");
		$array_values['delivery_date']=gm("Delivery date");
		$array_values['delivery_datedelivery_del_date']=gm("Delivery Final date");
		$array_values['our_ref']=gm("Your own reference");
		$array_values['your_ref']=gm("Your reference");
		$array_values['seller_name']=gm("Company name");
		$array_values['seller_d_address']=gm("Company address");
		$array_values['seller_d_zip']=gm("Company zip");
		$array_values['seller_d_city']=gm("Company city");
		$array_values['seller_b_phone']=gm("Company phone");
		$array_values['seller_b_fax']=gm("Company fax");
		$array_values['seller_name']=gm("Company name");
		$array_values['seller_d_country']=gm("Company country");
		$array_values['seller_b_email']=gm("Company email");
		$array_values['seller_b_url']=gm("Company url");
		$array_values['notes']=gm("Notes");
		$array_values['free_text_content']=gm("General conditions");
		$array_values['delivery_address']=gm("Adrress delivery");
		$array_values['attention_of']=gm("Attention of");
		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id_value'     => $key,
				'name'	   => $value,
			));
		}


		$data['detail_id']=$in['detail_id'];
		$data['detail']=$detail;
		if($data['detail_id']!='0'){
			$data['selected_detail']='[!'.$in['detail_id'].'!]';
		}
		return json_out($data, $showin,$exit);
	}

	$result = array(
		'codelabels'		=> get_codelabels($in,true,false),
		'selectlabels'		=> get_selectlabels($in,true,false),
		'selectdetails'		=> get_selectdetails($in,true,false),
	);


json_out($result);
?>