<?php
/************************************************************************
* @Author: MedeeaWeb Works                                                   *
************************************************************************/
class payment
{
	// var $db;
	var $db_gaetan;
	var $db_users;

	function payment()
	{
		ark::loadLibraries(array('stripe/init'));
   		$this->db = new sqldb();

   		global $database_config;
		$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$this->db_users =  new sqldb($database_2);
		$database_3 = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_gaetan =new sqldb($database_3);

		$database_4 = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d',
		);
		$this->db_new_gaetan =new sqldb($database_4);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function pay(&$in)
	{
		//$plan = $this->db_users->query("SELECT * FROM plan WHERE plan_id='".$in['plan']."' ");
		$plan = $this->db_users->query("SELECT * FROM plan WHERE plan_id= :plan_id ",['plan_id'=>$in['plan']]);
		if(!$plan->next()){
			return false;
		}
		if ($in['plan'] == 1) {
			$in['users'] = 1;
			$in['user_time_amount'] = 0;
		}
		if ($in['plan']==4)
		{
			$in['user_time_amount']=0;
		}

		switch ($in['web_size']) {
			case '1':
				$web_amount = 40;
				break;
			case '2':
				$web_amount = 80;
				break;
			case '3':
				$web_amount = 120;
				break;
			case '4':
				$web_amount = 150;
				break;
			default:
				$web_amount = '0';
				break;
		}

		if($in['plan'] < 3){
			$in['new_plan_webshop'] = 0;
		}
		if(defined('HOW_MANY')){
	    	$this->db->query("UPDATE settings SET value='".$in['how_many']."' WHERE constant_name='HOW_MANY' AND module='payment' ");
	    }else{
		    $this->db->query("INSERT INTO settings SET constant_name='HOW_MANY', value='".$in['how_many']."', module='payment' ");
		    define('HOW_MANY', $in['how_many']);
		}
		if(defined('PERIOD')){
	    	$this->db->query("UPDATE settings SET value='".$in['period']."' WHERE constant_name='PERIOD' AND module='payment' ");
	    }else{
		    $this->db->query("INSERT INTO settings SET constant_name='PERIOD', value='".$in['period']."', module='payment' ");
		    define('PERIOD', $in['period']);
		}
		if(defined('NEW_PLAN_WEBSHOP')){
	    	$this->db->query("UPDATE settings SET value='".$in['new_plan_webshop']."' WHERE constant_name='NEW_PLAN_WEBSHOP' AND module='payment' ");
	    }else{
		    $this->db->query("INSERT INTO settings SET constant_name='NEW_PLAN_WEBSHOP', value='".$in['new_plan_webshop']."', module='payment' ");
		    define('NEW_PLAN_WEBSHOP', $in['new_plan_webshop']);
		}

		if(defined('ACCOUNT_PLAN')){
	    	$this->db->query("UPDATE settings SET value='".$in['plan']."' WHERE constant_name='ACCOUNT_PLAN' AND module='payment' ");
	    }else{
		    $this->db->query("INSERT INTO settings SET constant_name='ACCOUNT_PLAN', value='".$in['plan']."', module='payment' ");
		    define('ACCOUNT_PLAN', $in['plan']);
		}
		if($in['sub_new']){
			if(!defined('PAYMILL_CLIENT_ID')){
				$this->db->query("INSERT INTO settings SET constant_name='PAYMILL_CLIENT_ID', module='payment' ");
		    	define('PAYMILL_CLIENT_ID', '');
			}
		}

		if($in['how_many'] == 0 && ($in['plan'] == 4 || $in['plan'] == 3) ){
			$this->db->query("UPDATE settings SET value='' WHERE constant_name='ALLOW_STOCK' ");
		}

		if($in['how_many'] == 0){
			$this->db->query("UPDATE apps SET active='0' WHERE name='Google Calendar' AND main_app_id='0' AND type='main' ");
			$this->db->query("UPDATE apps SET api='', active='0' WHERE name='Dropbox' AND main_app_id='0' AND type='main' ");
			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_QUOTE_DROP_BOX' ");
			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_ORDER_DROP_BOX' ");
			$this->db->query("UPDATE apps SET active='0' WHERE name='Mail chimp' AND main_app_id='0' AND type='main' ");
			$this->db->query("UPDATE apps SET active='0' WHERE name='Mail chimp' AND main_app_id='0' AND type='main' ");
			$this->db->query("UPDATE apps SET active='0' WHERE name='FTP Import' AND app_type='ftp_import'");
			$this->db->query("UPDATE apps SET active=0 WHERE name='WinBooks' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
		}

		/*$this->db_users->query("UPDATE users SET plan 				= '".$in['plan']."',
												 total 				= '".($in['amount']/100)."',
												 new_plan_users 	= '".$in['how_many']."',
												 period 			= '".$in['period']."',
												 new_plan_webshop   = '".$in['new_plan_webshop']."'
												 WHERE database_name='".DATABASE_NAME."' ");*/
		$this->db_users->query("UPDATE users SET plan 				= :plan,
												 total 				= :total,
												 new_plan_users 	= :new_plan_users,
												 period 			= :period,
												 new_plan_webshop   = :new_plan_webshop
												 WHERE database_name= :d ",
											[	 'plan' 				=> $in['plan'],
												 'total' 				=> ($in['amount']/100),
												 'new_plan_users' 	=> $in['how_many'],
												 'period' 			=> $in['period'],
												 'new_plan_webshop'   => $in['new_plan_webshop'],
												 'database_name'=> DATABASE_NAME]
										);												 

		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='PIM_ACTIVE' ");
		if($in['new_plan_webshop'] > 0){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name='PIM_ACTIVE' ");
		}



		$this->db_users->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' ");
		while ($this->db_users->move_next()) {
			$users_ids .= $this->db_users->f('user_id').',';
		}
		$users_ids .= $_SESSION['u_id'];
		 // total 				= '".$in['total']."',
		$this->db_users->query("UPDATE users SET plan 				= '".$in['plan']."',
												 users 				= '".$in['users']."',
												 time_users 		= '".$in['user_time_amount']."',
												 webshop_yes		= '".$in['webshop']."',
												 webshop_amount		= '".$web_amount."',
												 user_amount 		= '".$plan->f('users_amount')."',
												 user_time_amount 	= '".$plan->f('time_users_amount')."',

												 valid_vat			= '".$in['valid_vat']."',
												 order_id			= '".$in['order_id']."'
												 WHERE user_id		IN (".$users_ids.") ");
		// if($in['payed'] == '0'){
		//$this->db_users->query("UPDATE settings SET value='".$in['order_id']."' WHERE constant_name='ORDER_ID' ");
		$this->db_users->query("UPDATE settings SET value= :value WHERE constant_name= :constant_name ",['value'=>$in['order_id'],'constant_name'=>'ORDER_ID']);
		// }
		// $this->db->query("UPDATE settings SET value='0' WHERE constant_name='PIM_ACTIVE' ")
		if($in['web_size']){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name='PIM_ACTIVE' ");
		}

		if($in['plan'] == 1){
			$this->db_users->query("UPDATE user_info SET is_trial='1' WHERE user_id IN (".$users_ids.") ");
			$this->db_users->query("UPDATE users SET active='0' WHERE main_user_id<>'0' AND database_name='".DATABASE_NAME."' ");
			$this->db->query("UPDATE settings SET value='free' WHERE constant_name='ACCOUNT_TYPE'");
			$this->db->query("DELETE FROM settings WHERE constant_name='PAYMILL_CLIENT_ID'");
		}
		else if($in['plan']==2 || $in['plan']==5)
		{
			$this->db->query("UPDATE settings SET value='services' WHERE constant_name='ACCOUNT_TYPE'");
			doManageLog('Set plan.',DATABASE_NAME,array( array("property"=>'plan',"value"=>'Services') ));
		}
		else if ($in['plan']==3)
		{
			$this->db->query("UPDATE settings SET value='both' WHERE constant_name='ACCOUNT_TYPE'");
			doManageLog('Set plan.',DATABASE_NAME,array( array("property"=>'plan',"value"=>'Services and Products') ));
		}
		else if ($in['plan']==4)
		{
			$this->db->query("UPDATE settings SET value='goods' WHERE constant_name='ACCOUNT_TYPE'");
			doManageLog('Set plan.',DATABASE_NAME,array( array("property"=>'plan',"value"=>'Products') ));
		}
		$total =($in['amount']/100);
		if(defined('PAYMILL_CLIENT_ID')) {
			$subscription = array('1'=>array(
									'20'=>array(15,59,99,159,199),
									'50'=>array(29,59,99,159,199),
									'40'=>array(49,89,129,189,249),
									'41'=>array(89,129,169,229,289),
									'42'=>array(149,189,229,289,349),
									'30'=>array(59,119,169,239,309),
									'31'=>array(99,159,209,279,349),
									'32'=>array(159,219,269,339,409)
									),
								'2'=>array(
									'20'=>array(162,637,1069,1717,2149),
									'50'=>array(313,637,1069,1717,2149),
									'40'=>array(529,961,1393,2041,2689),
									'41'=>array(961,1393,1825,2473,3121),
									'42'=>array(1609,2041,2473,3121,3769),
									'30'=>array(637,1285,1825,2581,3337),
									'31'=>array(1069,1717,2257,3013,3769),
									'32'=>array(1717,2365,2905,3661,4417)
									)
								);
				$feature = NEW_PLAN_WEBSHOP;
				if(!$feature){ $feature = '0'; }
				$total = $subscription[PERIOD][ACCOUNT_PLAN.$feature][HOW_MANY];
		}
		doManageLog('Set mrr.',DATABASE_NAME,array( array("property"=>'mrr',"value"=> $total) ));

		if($in['change_card']){
		//	$this->db_users->query("UPDATE settings SET value='".$in['order_id']."' WHERE constant_name='ORDER_ID' ");
			$this->db->query("INSERT INTO settings SET value='1', constant_name='CHANGE_CARD' ");
		}else{
			$this->db->query("DELETE FROM settings WHERE constant_name='CHANGE_CARD' ");
		}
		updateUsersInfo();
		$in['pay'] = 'ok';
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function pay_new(&$in)
	{
		/*$this->db_users->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' ");
		while ($this->db_users->move_next()) {
			$users_ids .= $this->db_users->f('user_id').',';
		}
		$users_ids .= $_SESSION['u_id'];*/
		$usr_amount=$in['period']=='1' ? 10 : 12;
		/*$this->db_users->query("UPDATE users SET 				plan                    = '0',
												users 			= '".$in['users']."',
												 time_users 		= '".$in['users_t']."',
												 user_amount 		= '".$usr_amount."',
												 user_time_amount 	= '5',											
												 valid_vat			= '".$in['VALID_VAT']."',
												 credentials_serial	= '".serialize($in['aditional'])."'
												 WHERE `database_name`	='".DATABASE_NAME."' ");*/
		$this->db_users->query("UPDATE users SET 				plan                    = :plan,
												users 			= :users,
												 time_users 		= :time_users,
												 user_amount 		= :user_amount,
												 user_time_amount 	= :user_time_amount,											
												 valid_vat			= :valid_vat,
												 credentials_serial	= :credentials_serial
												 WHERE `database_name`	= :d ",
											[	'plan'              => '0',
												'users' 			=> $in['users'],
												 'time_users' 		=> $in['users_t'],
												 'user_amount' 		=> $usr_amount,
												 'user_time_amount' 	=> '5',											
												 'valid_vat'			=> $in['VALID_VAT'],
												 'credentials_serial'	=> serialize($in['aditional']),
												 'd'	=> DATABASE_NAME]
										);												 
		$total =($in['amount']/100);
		
		doManageLog('Set mrr.',DATABASE_NAME,array( array("property"=>'mrr',"value"=> $total) ));

		if($in['change_card']){
			$this->db->query("INSERT INTO settings SET value='1', constant_name='CHANGE_CARD' ");
		}else{
			$this->db->query("DELETE FROM settings WHERE constant_name='CHANGE_CARD' ");
		}

		//$in['period']=$in['period']=='1' ? 2 : 1; 
		$period = $this->db->query("SELECT value FROM settings WHERE constant_name='PERIOD' AND module='payment' ");
		if($period->next()){
			$this->db->query("UPDATE settings SET value='".$in['period']."' WHERE constant_name='PERIOD' AND module='payment' ");
		}else{
			$this->db->query("INSERT INTO settings SET value='".$in['period']."', constant_name='PERIOD', module='payment' ");
		}

		$acc_type=$this->db->field("SELECT value FROM settings WHERE constant_name='SUBSCRIPTION_TYPE'");
		$adv_features=array('crm_adv'=>'CRM','quotes_adv'=>'QUOTE','products'=>'PRODUCT','stocks'=>'STOCK','sepa'=>'SEPA','report_adv'=>'REPORT');
		//$credentials='1;4;5;10;12;';
		$credentials=$this->db_users->field("SELECT credentials FROM users WHERE `database_name`='".DATABASE_NAME."' AND user_type='3' ");
		$credentials.=';';
		$cred = explode(';', $credentials);
		foreach($in['aditional'] as $key=>$value){
			if($in['selected_plan']!=$in['pricing_plan_id']){
				if(!$value[$value['model']] && in_array( $key, $in['pricing_plans'][$in['pricing_plan_id']-1]['additionals'])){
					$value[$value['model']] =true;
				}
			}
			if(array_key_exists($value['model'], $adv_features)){
				$const_name='ADV_'.$adv_features[$value['model']];
				if(defined($const_name)){				
					$this->db->query("UPDATE settings SET value='".$value[$value['model']]."' WHERE constant_name='".$const_name."' ");
				}else{
					$this->db->query("INSERT INTO settings SET constant_name='".$const_name."', value='".$value[$value['model']]."' ");
				}
				if($value['model']=='stocks'){
					$this->db->query("UPDATE settings SET value='".$value[$value['model']]."' WHERE constant_name='ALLOW_STOCK' ");
				}
			}
			if($value[$value['model']] && $value['credential']){
				if(!in_array($value['credential'], $cred)){
					$credentials.=$value['credential'].';';
					array_push($cred, $value['credential']);
				}
				if($value['credential'] == '16' && !in_array('14', $cred)){
					$credentials.='14;';
					array_push($cred, '14');
				}
				if($value['credential'] == '13' && !in_array('17', $cred)){
					$credentials.='17;';
					array_push($cred, '17');
				}
				if($value['credential'] == '3' && $acc_type=='1' && !in_array('19', $cred)){
					$credentials.='19;';
					array_push($cred, '17');
				}
				
			}		
		}

		if(!in_array('18', $cred)){
			$credentials.='18;';
			array_push($cred, '18');
		}
		$credentials=rtrim($credentials,";");

		$credentials_general=explode(";",$credentials);
		$other_users=$this->db_users->query("SELECT user_id,credentials FROM users WHERE `database_name`='".DATABASE_NAME."' AND user_type!='3' ");
		while($other_users->next()){
			$credentials_old=explode(";",$other_users->f('credentials'));
			if(in_array('13', $credentials_old) && !in_array('17', $credentials_old)){
				array_push($credentials_old, '17');
			}
			if(!in_array('18', $credentials_old)){
				array_push($credentials_old, '18');
			}
			if(in_array('3', $credentials_old) && !in_array('19', $credentials_old) && $acc_type=='1'){
				array_push($credentials_old, '19');
			}
			$credentials_new=array_intersect($credentials_general, $credentials_old);
			$string_credentials='';
			foreach($credentials_new as $key=>$value){
				$string_credentials.=$value.';';
			}
			$string_credentials=rtrim($string_credentials,";");
			$this->db_users->query("UPDATE users SET credentials='".$string_credentials."' WHERE user_id='".$other_users->f('user_id')."' ");
		}	
		# we add 7 for the general settings and only for admins
		if(!in_array('7', $cred)){
			$credentials.='7;';
			array_push($cred, '7');
		}
		$this->db_users->query("UPDATE users SET credentials='".$credentials."' WHERE `database_name`='".DATABASE_NAME."' AND user_type='3' ");

		# we add 7 for the general settings and only for admins
		//$credentials .= ';7';
		$this->db_users->query("UPDATE users SET credentials='".$credentials."' WHERE `database_name`='".DATABASE_NAME."' AND group_id='1' ");
		$is_new_plan = $this->db->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION'");
		if(is_null($is_new_plan)){
			$this->db->query("INSERT INTO settings SET constant_name='NEW_SUBSCRIPTION', value='1'");
		}
		updateUsersInfo();
		updateCustomersInfo(); //update customers created in akti database
		return true;
	}

	function articles_size(&$in)
	{
		switch ($in['web_size']) {
			case '1':
				$web_size = 50;
				break;
			case '2':
				$web_size = 500;
				break;
			case '3':
				$web_size = 5000;
				break;
			case '4':
				$web_size = 6001;
				break;
			default:
				$web_size = '0';
				break;
		}
		if ($web_size!='0')
		{
			$web_size = $web_size-1;
			if ($web_size!=6000)
			{
				$nr_show = $this->db->query("SELECT article_id  FROM pim_articles WHERE show_front='1' LIMIT $web_size,1");
				while ($nr_show->next())
				{
					// echo $nr_show->f('article_id')."</br>";
					// $this->db->query("UPDATE pim_articles SET show_front='0' WHERE article_id>".$nr_show->f('article_id'));
				}
			}
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function payment_invoices($users_id,$start='')
	{
		$user_id = explode(',', $users_id);
		$this->db_gaetan->query("SET names utf8");
    	$this->db_gaetan->query("SET sql_mode= ''");
    	$this->db_gaetan->query("SELECT constant_name, value, long_value FROM settings");
		$const_u = array();
	    while($this->db_gaetan->move_next()){
	        if($this->db_gaetan->f('constant_name')){
	            $const_u[$this->db_gaetan->f('constant_name')] = $this->db_gaetan->f('value') ? utf8_decode($this->db_gaetan->f('value')) : utf8_decode($this->db_gaetan->f('long_value'));
	        }
	    }

		$our_ref = $start;

		foreach ($user_id as $key) {
			if($our_ref){
				$exists = $this->db_gaetan->field("SELECT id FROM tblinvoice WHERE our_ref='".$our_ref."' ");
				if($exists){
					continue;
				}
			}
			$user_info = $this->db_users->query("SELECT * FROM users WHERE user_id='".$key."' ");
			$user_info->next();
			global $database_config;
			$db_conf = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $user_info->f('database_name'),
			);
			$db = new sqldb($db_conf);
			// $this->db->database = $user_info->f('database_name');
			$db->query("SET names utf8");
    		$db->query("SET sql_mode= ''");
			$db->query("SELECT constant_name, value, long_value,type FROM settings");
			$const = array();
		    while($db->move_next()){
		        if($db->f('constant_name')){
		            $const[$db->f('constant_name')] = $db->f('type') == '1' ? utf8_decode($db->f('value')) : utf8_decode($db->f('long_value'));
		        }
		    }

		    $database_name = $this->db_users->field("SELECT database_name FROM users WHERE user_id='".$key."'");
		    $customer_id = $this->db_users->field("SELECT customer_id FROM customers WHERE database_name='".$database_name."' ");
		    $notes2 = $this->db_users->field("SELECT invoice_note2 FROM customers WHERE customer_id='".$customer_id."'");
	        $buyer_details = $this->db_users->query("SELECT customers.customer_id as buyer_id, customers.name, customers.payment_term, customer_addresses . *, customers.btw_nr, customers.c_email, customer_legal_type.name AS l_name
	            FROM customers
	            LEFT JOIN customer_addresses ON customer_addresses.customer_id = customers.customer_id
	            LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
	            AND customer_addresses.billing =1
	            WHERE customers.customer_id = '".$customer_id."'");
	        $buyer_details->next();

			$serial_number=generate_invoice_number($database_config['user_db']);
			$time = time();

			$price = $user_info->f('total');
			$vat_line = 0.00;
			$addition = 0;
			if($user_info->f('plan') == 3){
				$addition = 15;
			}
			$amount = ($user_info->f('users')*$user_info->f('user_amount'))+$addition+($user_info->f('time_users')*$user_info->f('user_time_amount')) + $user_info->f('webshop_amount');
			if($amount != $user_info->f('total')){
				$price = $amount;
				$vat_line = 21.00;
			}
			$amount_vat = $amount * (1 + $vat_line/100);
			if(isset($const['PAYMILL_CLIENT_ID'])){
				$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');
				$vat_added = false;
				$c_code = get_country_code($user_info->f('country_id'));

				$subscription = array('1'=>array(
									'20'=>array(15,59,99,159,199),
									'50'=>array(29,59,99,159,199),
									'40'=>array(49,89,129,189,249),
									'41'=>array(89,129,169,229,289),
									'42'=>array(149,189,229,289,349),
									'30'=>array(59,119,169,239,309),
									'31'=>array(99,159,209,279,349),
									'32'=>array(159,219,269,339,409)
									),
								'2'=>array(
									'20'=>array(162,637,1069,1717,2149),
									'50'=>array(313,637,1069,1717,2149),
									'40'=>array(529,961,1393,2041,2689),
									'41'=>array(961,1393,1825,2473,3121),
									'42'=>array(1609,2041,2473,3121,3769),
									'30'=>array(637,1285,1825,2581,3337),
									'31'=>array(1069,1717,2257,3013,3769),
									'32'=>array(1717,2365,2905,3661,4417)
									)
								);
				$feature = $const['NEW_PLAN_WEBSHOP'];
				if(!$feature){ $feature = '0'; }
				$total = $subscription[$const['PERIOD']][$const['ACCOUNT_PLAN'].$feature][$const['HOW_MANY']];
				$total_vat = $total;
				if( in_array($c_code, $eu_countries) && !$user_info->f("valid_vat")) {
					$total_vat = $total + ($total*(21/100));
					$vat_added = true;
					$vat_line = 21.00;
				}
				if( array_search($c_code, $eu_countries) == 1 && !$vat_added ) {
					$total_vat = $total + ($total*(21/100));
					$vat_added = true;
					$vat_line = 21.00;
				}
				$amount = $total;
				$amount_vat = $total_vat;
				$price = $amount;
			}

			if($user_info->f('resseler_id')){
				if($user_info->f('period') ==2){
					$invoice_code ='YI';
				}else{
					$invoice_code ='MI';
				}
				
				
			}elseif ($user_info->f('accountant_id')) {
				if($user_info->f('period') ==2){
					$invoice_code ='YI';
				}else{
					$invoice_code ='MI';
				}
			}else{
				if($user_info->f('period') ==2){
					$invoice_code ='YD';
				}else{
					$invoice_code ='MD';
				}
			}
			$seller_bwt_nr = $user_info->f('valid_vat') ? $user_info->f('valid_vat'): $user_info->f('ACCOUNT_VAT_NUMBER');

			$inv_id = $this->db_gaetan->insert("INSERT INTO tblinvoice SET
                                                serial_number       		=   '".$serial_number."',
                                                invoice_date       			=   '".$time."',
                                                due_date					=	'".$time."',
                                                discount      	    		=   '',
                                                currency_type       		=   '1',
                                                c_invoice_id            	=   '',
                                                notes                 		=   '',
                                                type                		=   '0',
                                                contact_id					=   '',
                                                status              		=   '0',
                                                f_archived          		=   '0',
                                                paid          				=   '0',
                                                buyer_id       	        	=   '".$key."',
                                                buyer_name       			=   '".addslashes($buyer_details->f('name').' '.$buyer_details->f('l_name'))."',
                                                buyer_address       		=   '".addslashes($buyer_details->f('address'))."',
                                                buyer_email       			=   '".$buyer_details->f('c_email')."',
                                                buyer_phone     	  		=   '".$buyer_details->f('comp_phone')."',
                                                buyer_fax		       		=   '".$buyer_details->f('comp_fax')."',
                                                buyer_zip       			=   '".$buyer_details->f('zip')."',
                                                buyer_city       			=   '".addslashes($buyer_details->f('city'))."',
                                                buyer_country_id       		=   '".$buyer_details->f('country_id')."',
                                                buyer_state_id       		=   '".addslashes($buyer_details->f('state_id'))."',
                                                seller_id       			=   '0',
                                                seller_name       			=   'AKTI S.A./N.V.',
                                                seller_d_address       		=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_ADDRESS']))."',
                                                seller_d_zip       			=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_ZIP']))."',
                                                seller_d_city           	=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_CITY']))."',
                                                seller_d_country_id     	=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_COUNTRY_ID']))."',
                                                seller_d_state_id       	=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_STATE_ID']))."',
                                                seller_b_address       		=   'Sinter-Goedelevoorplein 5 
Kantersteen 10',
                                                seller_b_zip       			=   '1000',
                                                seller_b_city       		=   'Brussels',
                                                seller_b_country_id     	=   '26',
                                                seller_b_state_id       	=   '".utf8_encode(addslashes($const_u['ACCOUNT_BILLING_STATE_ID']))."',
                                                seller_bwt_nr           	=   '".$seller_bwt_nr."',
                                                created             		=   '".$time."',
                                                created_by          		=   'Payment Gateway',
                                                last_upd            		=   '".$time."',
                                                last_upd_by         		=   'Payment Gateway',
                                                vat							= 	'',
                                                our_ref						= 	'".$our_ref."',
                                                notes2						=	'".utf8_encode(addslashes($notes2))."',
                                                your_ref					= 	'".$invoice_code."',
                                                remove_vat					=   '',
                                                currency_rate				=	'',
                                                payment 					=	'1',
												amount 						=   '".$amount."',
												amount_vat					=	'".$amount_vat."' ");

			$next_date = $user_info->f('next_date');
			if(!$user_info->f('next_date')){
				$next_date = mktime(0,0,1,date('n')+1,date('j'),date('Y'));
			}
			switch ($const['HOW_MANY']) {
				case '0':
					$planns = 'Personal';
					break;
				case '1':
					$planns = 'Startup';
					break;
				case '2':
					$planns = 'Small';
					break;
				case '3':
					$planns = 'Business';
					break;
				case '4':
					$planns = 'Entreprise';
					break;
				default:
					$planns = 'Personal';
					break;
			}
			switch ($user_info->f('plan')) {
				case '2':
					$hub_plan = "Services";
					$plan_text = 'Akti Subscription:
type Services - '.$planns.'
Next charge: '.date('d/m/Y',$next_date);
					$plan_text_nl = 'Akti abonnement:
type Services - '.$planns.'
Volgende afrekening: '.date('d/m/Y',$next_date);
					$plan_text_fr = 'Abonnement Akti:
type Services - '.$planns.'
Prochain décompte: '.date('d/m/Y',$next_date);
					break;
				case '3':
					$hub_plan = "Services and Products";
					$plan_text = 'Akti Subscription:
type Services and Products - '.$planns;

					$plan_text_nl = 'Akti abonnement:
type Services and Products - '.$planns;

					$plan_text_fr = 'Abonnement Akti:
type Services and Products - '.$planns;

					if($user_info->f('webshop_yes') > 0){
						$plan_text .= "\nWebshop: ".$user_info->f('webshop_amount').' &euro;';
						$plan_text_nl .= "\nWebshop: ".$user_info->f('webshop_amount').' &euro;';
						$plan_text_fr .= "\nWebshop: ".$user_info->f('webshop_amount').' &euro;';
					}
					$plan_text .= "\nNext charge: ".date('d/m/Y',$next_date);
					$plan_text_nl .= "\nVolgende afrekening: ".date('d/m/Y',$next_date);
					$plan_text_fr .= "\nProchain décompte: ".date('d/m/Y',$next_date);
					break;
				case '4':
					$hub_plan = "Products";
					$plan_text = 'Akti Subscription:
type Products - '.$planns;

					$plan_text_nl = 'Akti abonnement:
type Products - '.$planns;

					$plan_text_fr = 'Abonnement Akti:
type Products - '.$planns;

					if($user_info->f('webshop_yes') > 0){
						$plan_text .= "\nWebshop: ".$user_info->f('webshop_amount').' &euro;';
						$plan_text_nl .= "\nWebshop: ".$user_info->f('webshop_amount').' &euro;';
						$plan_text_fr .= "\nWebshop: ".$user_info->f('webshop_amount').' &euro;';
					}
					$plan_text .= "\nNext charge: ".date('d/m/Y',$next_date);
					$plan_text_nl .= "\nVolgende afrekening: ".date('d/m/Y',$next_date);
					$plan_text_fr .= "\nProchain décompte: ".date('d/m/Y',$next_date);
					break;
				default:
					$plan_text = 'Payment for Akti';
					$plan_text_nl = 'Payment voor Akti';
					$plan_text_fr = 'Payment pour Akti';
					break;
			}

			switch ($user_info->f('lang_id')) {
				case '1':
					$text = $plan_text_nl;
					break;
				case '2':
					$text = $plan_text;
					break;
				case '3':
					$text = $plan_text_fr;
					break;
				default:
					$text = $plan_text;
					break;
			}

			$this->db_gaetan->query("INSERT INTO tblinvoice_line SET invoice_id='".$inv_id."',
																	name='".$text."',
																	quantity='1',
																	price='".$price."',
																	amount='".$price."',
																	vat = '".$vat_line."',
																	created='".$time."',
																	created_by='Payment Gateway' ");
			$this->db_users->query("UPDATE users SET last_payment='".$user_info->f('total')."' WHERE database_name='".$user_info->f('database_name')."' ");
			$this->db_users->query("INSERT INTO `licences_log` SET `month`='".date('n')."',`year`='".date('Y')."',`type`='".$user_info->f('plan')."',
									`users`='".$user_info->f('users')."', `time_users`='".$user_info->f('time_users')."', `webshop`='".$user_info->f('webshop_yes')."' ");
			$this->db_users->query("INSERT INTO `clients_pay_log` SET `month`='".date('n')."',`year`='".date('Y')."',`type`='".$user_info->f('plan')."', `users`='1' ");
			// echo 'invoice created<br>';
			doManageLog('First payment.',$user_info->f('database_name'),array( array("property"=>'first_payment',"value"=>time()*1000) ));
			// doManageLog('Set plan.',$user_info->f('database_name'),array( array("property"=>'plan',"value"=>$hub_plan) ));
			// doManageLog('Set mmr.',$user_info->f('database_name'),array( array("property"=>'plan',"value"=>$amount) ));
			$our_ref++;
		}
		return true;
	}

	function payment_invoice_stripe($users_id,$invoice_ref){
		global $database_config,$config;
		\Stripe\Stripe::setApiKey($config['stripe_api_key']);
		$stripe_inv=\Stripe\Invoice::retrieve($invoice_ref);
		$stripe_inv_lines=\Stripe\Invoice::retrieve($invoice_ref)->lines->all(array('limit'=>20));

		$user_id = explode(',', $users_id);
		$this->db_new_gaetan->query("SET names utf8");
    	$this->db_new_gaetan->query("SET sql_mode= ''");
		$this->db_gaetan->query("SET names utf8");
    	$this->db_gaetan->query("SET sql_mode= ''");
    	$this->db_gaetan->query("SELECT constant_name, value, long_value FROM settings");
		$const_u = array();
	    while($this->db_gaetan->move_next()){
	        if($this->db_gaetan->f('constant_name')){
	            $const_u[$this->db_gaetan->f('constant_name')] = $this->db_gaetan->f('value') ? utf8_decode($this->db_gaetan->f('value')) : utf8_decode($this->db_gaetan->f('long_value'));
	        }
	    }

		$our_ref = $invoice_ref;

		foreach ($user_id as $key) {
			$user_info = $this->db_users->query("SELECT * FROM users WHERE user_id='".$key."' ");
			$user_info->next();
			if($user_info->f('user_id')>=10000){

				$db_conf = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => $user_info->f('database_name'),
				);
			}else{
				$db_conf = array(
					'hostname' => 'salesassist.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
					'username' => 'admin',
					'password' => 'OilSz3ANoVRzKA',
					'database' => $user_info->f('database_name'),
				);
			}
			$db = new sqldb($db_conf);
			// $this->db->database = $user_info->f('database_name');
			$db->query("SET names utf8");
    		$db->query("SET sql_mode= ''");
			$db->query("SELECT constant_name, value, long_value,type FROM settings");
			$const = array();
		    while($db->move_next()){
		        if($db->f('constant_name')){
		            $const[$db->f('constant_name')] = $db->f('type') == '1' ? utf8_decode($db->f('value')) : utf8_decode($db->f('long_value'));
		        }
		    }

		    $database_name = $this->db_users->field("SELECT database_name FROM users WHERE user_id='".$key."'");
		    $customer_id = $this->db_users->field("SELECT customer_id FROM customers WHERE database_name='".$database_name."' ");
		    $notes2 = $this->db_users->field("SELECT invoice_note2 FROM customers WHERE customer_id='".$customer_id."'");
		    $bill_data=$this->db_users->query("SELECT * FROM users WHERE database_name='".$database_name."' AND default_admin='1'");

			$serial_number=generate_invoice_number($database_config['user_db']);
			$serial_number_new=generate_invoice_number('04ed5b47_e424_5dd4_ad024bcf2e1d');
			$time = time();

			$cu=\Stripe\Customer::retrieve($user_info->f('stripe_cust_id'));
			$amount=$stripe_inv['subtotal']/100;
			$amount_no_discount=$stripe_inv['subtotal']/100;
			$amount_vat=$stripe_inv['tax']/100;
			$discount=0;
			if($stripe_inv['starting_balance']>0){
				if($cu['discount'] != null){
					$user_discount=\Stripe\Coupon::retrieve($user_info->f('stripe_discount_id'));
					if($user_discount['amount_off']==null){
						$amount_init=$amount+($stripe_inv['starting_balance']*100/($stripe_inv['tax_percent']*100-$stripe_inv['tax_percent']*$user_discount['percent_off']+10000-100*$user_discount['percent_off']));
						$amount_no_discount=$amount_init;
						$amount=$amount_init-($amount_init*$user_discount['percent_off']/100);
						$amount_vat=$amount*$stripe_inv['tax_percent']/100;						
						$discount=$user_discount['percent_off'];
					}else{
						if(($stripe_inv['subtotal']/100-$user_discount['amount_off']/100) < 0){
							$amount=$stripe_inv['starting_balance']/(100+$stripe_inv['tax_percent']);
							
						}else{
							$stripe_account_with_discount=($stripe_inv['subtotal']/100-$user_discount['amount_off']/100)*$stripe_inv['tax_percent']/100+($stripe_inv['subtotal']/100-$user_discount['amount_off']/100);
							$amount_init=$stripe_inv['starting_balance']/100+$stripe_account_with_discount;
							$amount=$amount_init*100/(100+$stripe_inv['tax_percent']);
						}
						$amount_no_discount=$amount+$user_discount['amount_off']/100;
						$amount_vat=$amount*$stripe_inv['tax_percent']/100;
						$discount=$user_discount['amount_off']/$amount_no_discount;
					}
				}else{
					$amount+=$stripe_inv['starting_balance']/(100+$stripe_inv['tax_percent']);
					$amount_no_discount=$amount;
					$amount_vat+=(($stripe_inv['starting_balance']/(100+$stripe_inv['tax_percent']))*$stripe_inv['tax_percent']/100);
				}	
			}else{
				if($cu['discount'] != null){
					$user_discount=\Stripe\Coupon::retrieve($user_info->f('stripe_discount_id'));
					if($user_discount['amount_off']==null){
						/*if(count($stripe_inv_lines['data'])>1){
							$amount=$amount-($stripe_inv_lines['data'][2]['amount']/100*$user_discount['percent_off']/100);
						}else{*/
							$amount=$amount-($amount*$user_discount['percent_off']/100);
						//}
						$amount_vat=$amount*$stripe_inv['tax_percent']/100;
						$discount=$user_discount['percent_off'];
					}else{
						$amount=$amount-$user_discount['amount_off']/100;
						$amount_vat=$amount*$stripe_inv['tax_percent']/100;
						$discount=$user_discount['amount_off']*100/$stripe_inv['subtotal'];
					}
				}
			}
			$amount_vat_percent=$stripe_inv['tax_percent'];
			if(!is_null($amount_vat_percent)){
				$vat_reg_data=$this->db_new_gaetan->query("SELECT vat_new.id,vats.value FROM vat_new
					INNER JOIN vats ON vat_new.vat_id=vats.vat_id
					WHERE vat_new.regime_type='1' AND vats.value='".$amount_vat_percent."' ");
				$vat_reg_id=$vat_reg_data->f('id');
				$vat_reg=$vat_reg_data->f('value');
			}else{
				$vat_reg_id=$this->db_new_gaetan->field("SELECT id FROM vat_new
					WHERE regime_type='2' ");
				$vat_reg='0';
			}	

			if($user_info->f('resseler_id')){
				if($user_info->f('period') ==2){
					$invoice_code ='YI';
				}else{
					$invoice_code ='MI';
				}
				
				
			}elseif ($user_info->f('accountant_id')) {
				if($user_info->f('period') ==2){
					$invoice_code ='YI';
				}else{
					$invoice_code ='MI';
				}
			}else{
				if($user_info->f('period') ==2){
					$invoice_code ='YD';
				}else{
					$invoice_code ='MD';
				}
			}
			switch ($user_info->f('lang_id')) {
				case '2':
					$lid = 1;
					break;
				case '3':
					$lid = 2;
					break;
				case '4':
					$lid = 4;
					break;
				default:
					$lid =3;
					break;
			}
			$seller_bwt_nr = trim($user_info->f('valid_vat')) ? $user_info->f('valid_vat'): $user_info->f('ACCOUNT_VAT_NUMBER');
			/*$inv_id = $this->db_gaetan->insert("INSERT INTO tblinvoice SET
                                                serial_number       		=   '".$serial_number."',
                                                invoice_date       			=   '".$time."',
                                                due_date					=	'".$time."',
                                                discount      	    		=   '".$discount."',
                                                currency_type       		=   '1',
                                                c_invoice_id            	=   '',
                                                notes                 		=   '',
                                                type                		=   '0',
                                                contact_id					=   '',
                                                status              		=   '1',
                                                f_archived          		=   '0',
                                                paid          				=   '1',
                                                sent                          =      '1',
                                                buyer_id       	        	=   '".$key."',
                                                buyer_name       			=   '".addslashes( ( $user_info->f('ACCOUNT_COMPANY') ? $user_info->f('ACCOUNT_COMPANY') : $user_info->f('last_name').' '.$user_info->f('first_name') ) )."',
                                                buyer_address       		=   '".addslashes($user_info->f('ACCOUNT_BILLING_ADDRESS'))."',
                                                buyer_email       			=   '".$user_info->f('email')."',
                                                buyer_phone     	  		=   '".$user_info->f('ACCOUNT_PHONE')."',
                                                buyer_fax		       		=   '".$user_info->f('ACCOUNT_FAX')."',
                                                buyer_zip       			=   '".$user_info->f('	ACCOUNT_BILLING_ZIP')."',
                                                buyer_city       			=   '".addslashes($user_info->f('ACCOUNT_BILLING_CITY'))."',
                                                buyer_country_id       		=   '".$user_info->f('ACCOUNT_BILLING_COUNTRY_ID')."',
                                                buyer_state_id       		=   '".addslashes($user_info->f('ACCOUNT_BILLING_STATE_ID'))."',
                                                seller_id       			=   '0',
                                                seller_name       			=   'AKTI S.A./N.V.',
                                                seller_d_address       		=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_ADDRESS']))."',
                                                seller_d_zip       			=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_ZIP']))."',
                                                seller_d_city           	=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_CITY']))."',
                                                seller_d_country_id     	=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_COUNTRY_ID']))."',
                                                seller_d_state_id       	=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_STATE_ID']))."',
                                                seller_b_address       		=   'Sinter-Goedelevoorplein 5 
Parvis Sainte-Gudule 5 ',
                                                seller_b_zip       			=   '1000',
                                                seller_b_city       		=   'Brussels',
                                                seller_b_country_id     	=   '26',
                                                seller_b_state_id       	=   '".utf8_encode(addslashes($const_u['ACCOUNT_BILLING_STATE_ID']))."',
                                                seller_bwt_nr           	=   '".$seller_bwt_nr."',
                                                created             		=   '".$time."',
                                                created_by          		=   'Payment Gateway',
                                                last_upd            		=   '".$time."',
                                                last_upd_by         		=   'Payment Gateway',
                                                vat							= 	'',
                                                our_ref						= 	'".$our_ref."',
                                                notes2						=	'".utf8_encode(addslashes($notes2))."',
                                                your_ref					= 	'".$invoice_code."',
                                                remove_vat					=   '',
                                                currency_rate				=	'',
                                                payment 					=	'1',
												amount 						=   '".$amount."',
												amount_vat					=	'".($amount+$amount_vat)."' ");*/
			$free_field = $bill_data->f('ACCOUNT_BILLING_ADDRESS').'
		'.$bill_data->f('ACCOUNT_BILLING_ZIP').' '.$bill_data->f('ACCOUNT_BILLING_CITY').'
		'.get_country_name($bill_data->f('ACCOUNT_BILLING_COUNTRY_ID'));
		$c_id=$this->db_new_gaetan->field("SELECT customer_contactsIds.customer_id FROM contact_field 
			INNER JOIN customer_contactsIds ON contact_field.customer_id=customer_contactsIds.contact_id 
			WHERE contact_field.field_id='1' AND contact_field.value='".$key."'");
		if($c_id){
			$this->db_new_gaetan->query("UPDATE customers SET is_customer='1' WHERE customer_id='".$c_id."' ");
			$c_billing=$this->db_new_gaetan->field("SELECT customer_id FROM customer_addresses WHERE customer_id='".$c_id."' AND billing='1'");
			if(!$c_billing){
				$this->db_new_gaetan->query("INSERT INTO customer_addresses SET
	                        address='".addslashes($bill_data->f('ACCOUNT_BILLING_ADDRESS'))."',
	                        zip='".$bill_data->f('ACCOUNT_BILLING_ZIP')."',
	                        city='".addslashes($bill_data->f('ACCOUNT_BILLING_CITY'))."',
	                        country_id='".$bill_data->f('ACCOUNT_BILLING_COUNTRY_ID')."',
	                        customer_id='".$c_id."',
	                        is_primary='1',
	                        billing='1' ");
			}
			$notes2 = $this->db_new_gaetan->field("SELECT invoice_note2 FROM customers WHERE customer_id='".$c_id."'");
			if($vat_reg_id && $vat_reg_id>=10000 && $lid){
				$acc_langs=array('en','fr','nl','de');
				$lang_c=$this->db_new_gaetan->field("SELECT code FROM pim_lang WHERE lang_id='".$lid."' ");
				if($lang_c){
					if($lang_c=='du'){
						$lang_c='nl';
					}
					if(in_array($lang_c, $acc_langs)){
						$vat_notes=stripslashes($this->db_new_gaetan->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$vat_reg_id."' "));
						if($vat_notes){
							if($notes2){
								$notes2=$notes2."\n".$vat_notes;
							}else{
								$notes2=$vat_notes;
							}
						}
					}
				}
			}
		}

			$inv_id_new = $this->db_new_gaetan->insert("INSERT INTO tblinvoice SET
                                                serial_number       		=   '".$serial_number_new."',
                                                invoice_date       			=   '".$time."',
                                                due_date					=	'".$time."',
                                                discount      	    		=   '".$discount."',
                                                currency_type       		=   '1',
                                                c_invoice_id            	=   '',
                                                notes                 		=   '',
                                                type                		=   '0',
                                                contact_id					=   '',
                                                status              		=   '1',
                                                f_archived          		=   '0',
                                                paid          				=   '1',
                                                sent                          =      '1',
                                                buyer_id       	        	=   '".$c_id."',
                                                buyer_name       			=   '".addslashes( ( $bill_data->f('ACCOUNT_COMPANY') ? $bill_data->f('ACCOUNT_COMPANY') : $user_info->f('last_name').' '.$user_info->f('first_name') ) )."',
                                                buyer_address       		=   '".addslashes($bill_data->f('ACCOUNT_BILLING_ADDRESS'))."',
                                                buyer_email       			=   '".$user_info->f('email')."',
                                                buyer_phone     	  		=   '".$user_info->f('ACCOUNT_PHONE')."',
                                                buyer_fax		       		=   '".$user_info->f('ACCOUNT_FAX')."',
                                                buyer_zip       			=   '".$bill_data->f('ACCOUNT_BILLING_ZIP')."',
                                                buyer_city       			=   '".addslashes($bill_data->f('ACCOUNT_BILLING_CITY'))."',
                                                buyer_country_id       		=   '".$bill_data->f('ACCOUNT_BILLING_COUNTRY_ID')."',
                                                buyer_state_id       		=   '".addslashes($user_info->f('ACCOUNT_BILLING_STATE_ID'))."',
                                                seller_id       			=   '0',
                                                seller_name       			=   'AKTI S.A./N.V.',
                                                seller_d_address       		=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_ADDRESS']))."',
                                                seller_d_zip       			=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_ZIP']))."',
                                                seller_d_city           	=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_CITY']))."',
                                                seller_d_country_id     	=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_COUNTRY_ID']))."',
                                                seller_d_state_id       	=   '".utf8_encode(addslashes($const_u['ACCOUNT_DELIVERY_STATE_ID']))."',
                                                seller_b_address       		=   'Sinter-Goedelevoorplein 5 
Kantersteen 10 ',
                                                seller_b_zip       			=   '1000',
                                                seller_b_city       		=   'Brussels',
                                                seller_b_country_id     	=   '26',
                                                seller_b_state_id       	=   '".utf8_encode(addslashes($const_u['ACCOUNT_BILLING_STATE_ID']))."',
                                                seller_bwt_nr           	=   '".$seller_bwt_nr."',
                                                created             		=   '".$time."',
                                                created_by          		=   'Payment Gateway',
                                                last_upd            		=   '".$time."',
                                                last_upd_by         		=   'Payment Gateway',
                                                vat					= 	'".$vat_reg."',
                                                vat_regime_id 			= '".$vat_reg_id."',
                                                our_ref						= 	'".$our_ref."',
                                                notes2						=	'".utf8_encode(addslashes($notes2))."',
                                                your_ref					= 	'".$invoice_code."',
                                                remove_vat					=   '',
                                                currency_rate				=	'',
                                                payment 					=	'1',
												amount 						=   '".$amount."',
												amount_vat					=	'".($amount+$amount_vat)."',
												email_language 				=	'".$lid."',
												free_field          		= 	'".addslashes($free_field)."' ");

			$this->db_new_gaetan->query("INSERT INTO note_fields SET
                            item_id         = '".$inv_id_new."',
                            lang_id         =   '".$lid."',
                            active          =   '1',
                            item_type         =   'invoice',
                            item_name         =   'notes'
            ");

			$next_date = $user_info->f('next_date');
			/*if(!$user_info->f('next_date')){
				$next_date = mktime(0,0,1,date('n')+1,date('j'),date('Y'));
			}*/
			switch ($const['HOW_MANY']) {
				case '0':
					$planns = 'Personal';
					break;
				case '1':
					$planns = 'Startup';
					break;
				case '2':
					$planns = 'Small';
					break;
				case '3':
					$planns = 'Business';
					break;
				case '4':
					$planns = 'Entreprise';
					break;
				default:
					$planns = 'Personal';
					break;
			}
			switch ($user_info->f('plan')) {
				case '2':
				case '5':
					$hub_plan = "Services";
					$plan_text = 'Akti Subscription:
type Services - '.$planns.'
Next charge: '.date('d/m/Y',$next_date);
					$plan_text_nl = 'Akti abonnement:
type Services - '.$planns.'
Volgende afrekening: '.date('d/m/Y',$next_date);
					$plan_text_fr = 'Abonnement Akti:
type Services - '.$planns.'
Prochain décompte: '.date('d/m/Y',$next_date);
					break;
				case '3':
					$hub_plan = "Services and Products";
					$plan_text = 'Akti Subscription:
type Services and Products - '.$planns;

					$plan_text_nl = 'Akti abonnement:
type Services and Products - '.$planns;

					$plan_text_fr = 'Abonnement Akti:
type Services and Products - '.$planns;

					if($user_info->f('webshop_yes') > 0){
						$plan_text .= "\nWebshop: ".$user_info->f('webshop_amount').' &euro;';
						$plan_text_nl .= "\nWebshop: ".$user_info->f('webshop_amount').' &euro;';
						$plan_text_fr .= "\nWebshop: ".$user_info->f('webshop_amount').' &euro;';
					}
					$plan_text .= "\nNext charge: ".date('d/m/Y',$next_date);
					$plan_text_nl .= "\nVolgende afrekening: ".date('d/m/Y',$next_date);
					$plan_text_fr .= "\nProchain décompte: ".date('d/m/Y',$next_date);
					break;
				case '4':
					$hub_plan = "Products";
					$plan_text = 'Akti Subscription:
type Products - '.$planns;

					$plan_text_nl = 'Akti abonnement:
type Products - '.$planns;

					$plan_text_fr = 'Abonnement Akti:
type Products - '.$planns;

					if($user_info->f('webshop_yes') > 0){
						$plan_text .= "\nWebshop: ".$user_info->f('webshop_amount').' &euro;';
						$plan_text_nl .= "\nWebshop: ".$user_info->f('webshop_amount').' &euro;';
						$plan_text_fr .= "\nWebshop: ".$user_info->f('webshop_amount').' &euro;';
					}
					$plan_text .= "\nNext charge: ".date('d/m/Y',$next_date);
					$plan_text_nl .= "\nVolgende afrekening: ".date('d/m/Y',$next_date);
					$plan_text_fr .= "\nProchain décompte: ".date('d/m/Y',$next_date);
					break;
				default:
					$plan_text = 'Payment for Akti';
					$plan_text_nl = 'Payment voor Akti';
					$plan_text_fr = 'Payment pour Akti';
					break;
			}

			switch ($user_info->f('lang_id')) {
				case '1':
					$text = $plan_text_nl;
					break;
				case '2':
					$text = $plan_text;
					break;
				case '3':
					$text = $plan_text_fr;
					break;
				default:
					$text = $plan_text;
					break;
			}

			$period = '1';
			$period_value = $db->query("SELECT value FROM settings WHERE constant_name='PERIOD' AND module='payment' ");
			if($period_value->next()){
				$period = $period_value->f('value');
			}
			if($period=='1'){
				$stripe_ids=array(
					'98'				=> 'New Subscription Users',
					'99'				=> 'New Subscription TimeUsers',
					'100'				=> 'New Subscription Basic',
					'101'				=> 'New Subscription Basic+',
					'102'				=> 'CRM (advanced)',
					'103'				=> 'Quotes (advanced)',
					'104'				=> 'Products (advanced)',
					'105'				=> 'Stocks',
					'106'				=> 'SEPA',
					'107'				=> 'E-commerce Site',
					'108'				=> 'Contracts',
					'109'				=> 'Interventions',
					'110'				=> 'Projects',
					'111'				=> 'Report (advanced)',
					'112'				=> 'Orders',
					'113'				=> 'Cashregister',
					'114'				=> 'New Subscription Base+',
					'115'				=> 'Timetracker');
			}else{
				$stripe_ids=array(
					'298'				=> 'New Subscription Users Year',
					'300'				=> 'New Subscription Basic Year',
					'301'				=> 'New Subscription Basic+ Year',
					'302'				=> 'CRM (advanced) Year',
					'303'				=> 'Quotes (advanced) Year',
					'304'				=> 'Products (advanced) Year',
					'305'				=> 'Stocks Year',
					'306'				=> 'SEPA Year',
					'307'				=> 'E-commerce Site Year',
					'308'				=> 'Contracts Year',
					'309'				=> 'Interventions Year',
					'310'				=> 'Projects Year',
					'311'				=> 'Report (advanced) Year',
					'312'				=> 'Orders Year',
					'313'				=> 'Cashregister Year',
					'314'				=> 'New Subscription Base+ Year',
					'315'				=> 'Timetracker Year');
			}			

			$multiple_plans=false;
			$subscription = $cu->subscriptions->retrieve($user_info->f('stripe_subscr_id'));
			if(count($subscription['items']['data'])>1){
				$multiple_plans=true;
			}

			if(count($stripe_inv_lines['data'])==1){
				if(array_key_exists($stripe_inv_lines['data'][0]['plan']['id'],$stripe_ids)){
					$text = 'Payment for Akti'.': '.$stripe_ids[$stripe_inv_lines['data'][0]['plan']['id']];
				}
				/*$this->db_gaetan->query("INSERT INTO tblinvoice_line SET invoice_id='".$inv_id."',
																	name='".$text."',
																	quantity='1',
																	price='".$amount_no_discount."',
																	amount='".$amount_no_discount."',
																	vat = '".$amount_vat_percent."',
																	created='".$time."',
																	created_by='Payment Gateway' ");*/

				$this->db_new_gaetan->query("INSERT INTO tblinvoice_line SET invoice_id='".$inv_id_new."',
																	name='".$text."',
																	quantity='1',
																	price='".$amount_no_discount."',
																	amount='".$amount_no_discount."',
																	vat = '".$amount_vat_percent."',
																	created='".$time."',
																	created_by='Payment Gateway' ");
			}else{
				/*$stripe_plan_values[0]-personal
				stripe_plan_values[0]-startup
				stripe_plan_values[0]-small
				stripe_plan_values[0]-business
				stripe_plan_values[0]-entreprise
				$stripe_plan_type[0]-services
				$stripe_plan_type[1]-articles
				$stripe_plan_type[2]-services&articles
				*/
				$stripe_plan_values=array(array('1','2','5','20','20.2','20.4','30','30.3','30.6','10','40','40.4','40.8','60','60.6','61.2'),
					array('6','21','21.21','21.42','31','31.31','31.62','11','41','41.41','41.82','61','61.61','62.22'),
					array('7','22','22.22','22.44','32','32.32','32.64','12','42','42.42','42.84','62','62.62','63.24'),
					array('8','23','23.23','23.46','33','33.33','33.66','13','43','43.43','43.86','63','63.63','64.26'),
					array('9','24','24.24','24.48','34','34.34','34.68','14','44','44.44','44.88','1000','64','64.64','65.28','2000'));
				$stripe_plan_type=array(array('5','6','7','8','9','10','11','12','13','14'),
					array('1','20','20.2','20.4','21','21.21','21.42','22','22.22','22.44','23','23.23','23.46','24','24.24','24.48','40','40.4','40.8','41','41.41','41.82','42','42.42','42.84','43','43.43','43.86','44','44.44','44.88','1000'),
					array('2','30','30.3','30.6','31','31.31','31.62','32','32.32','32.64','33','33.33','33.66','34','34.34','34.68','60','60.6','61.2','61','61.61','62.22','62','62.62','63.24','63','63.63','64.26','64','64.64','65.28','2000'));	
				foreach($stripe_inv_lines['data'] as $stripe_key => $stripe_value){
					switch ($user_info->f('lang_id')) {
						case '1':
							$stripe_plan='Abonnement Akti:';
							$between='tussen';
							$and='en';
							$unused='Ongebruikte tijd op';
							$remaining='Resterende tijd op';
							break;
						case '2':
							$stripe_plan='Akti Subscription:';
							$between='between';
							$and='and';
							$unused='Unused time on';
							$remaining='Remaining time on';
							break;
						case '3':
							$stripe_plan='Akti Abonnement:';
							$between='entre';
							$and='et';
							$unused='Temps non utilisé sur';
							$remaining='Temps restant sur';
							break;
						default:
							$stripe_plan='Akti Abonnement:';
							$between='zwischen';
							$and='und';
							$unused='Ungenutzte Zeit auf';
							$remaining='Restzeit auf';
							break;
					}
					if(in_array($stripe_value['plan']['id'],$stripe_plan_type[0])){
						$stripe_plan.='type Services - ';
					}else if(in_array($stripe_value['plan']['id'],$stripe_plan_type[1])){
						$stripe_plan.='type Products - ';
					}else if(in_array($stripe_value['plan']['id'],$stripe_plan_type[2])){
						$stripe_plan.='type Services and Products - ';
					}else{
						//here is new plan
					}

					if(in_array($stripe_value['plan']['id'],$stripe_plan_values[0])){
						$stripe_plan.='Personal';
					}else if(in_array($stripe_value['plan']['id'],$stripe_plan_values[1])){
						$stripe_plan.='Startup';
					}else if(in_array($stripe_value['plan']['id'],$stripe_plan_values[2])){
						$stripe_plan.='Small';
					}else if(in_array($stripe_value['plan']['id'],$stripe_plan_values[3])){
						$stripe_plan.='Business';
					}else if(in_array($stripe_value['plan']['id'],$stripe_plan_values[4])){
						$stripe_plan.='Entreprise';
					}else{
						//new plan
					}

					if($multiple_plans){
						$stripe_text=$stripe_plan.' '.$stripe_ids[$stripe_value['plan']['id']];
						/*$this->db_gaetan->query("INSERT INTO tblinvoice_line SET invoice_id='".$inv_id."',
																		name='".$stripe_text."',
																		quantity='".$stripe_value['quantity']."',
																		price='".($stripe_value['amount']/100/$stripe_value['quantity'])."',
																		amount='".($stripe_value['amount']/100)."',
																		vat = '".$amount_vat_percent."',
																		created='".$time."',
																		created_by='Payment Gateway' ");*/

						$this->db_new_gaetan->query("INSERT INTO tblinvoice_line SET invoice_id='".$inv_id_new."',
																		name='".$stripe_text."',
																		quantity='".$stripe_value['quantity']."',
																		price='".($stripe_value['amount']/100/$stripe_value['quantity'])."',
																		amount='".($stripe_value['amount']/100)."',
																		vat = '".$amount_vat_percent."',
																		created='".$time."',
																		created_by='Payment Gateway' ");
					}else{
						if($stripe_key==0){
							$stripe_text=$unused.' '.$stripe_plan.' '.$between.' '.date(ACCOUNT_DATE_FORMAT,$stripe_value['period']['start']).' '.$and.' '.date(ACCOUNT_DATE_FORMAT,$stripe_value['period']['end']);
							/*$this->db_gaetan->query("INSERT INTO tblinvoice_line SET invoice_id='".$inv_id."',
																		name='".$stripe_text."',
																		quantity='1',
																		price='".($stripe_value['amount']/100)."',
																		amount='".($stripe_value['amount']/100)."',
																		vat = '".$amount_vat_percent."',
																		created='".$time."',
																		created_by='Payment Gateway' ");*/

							$this->db_new_gaetan->query("INSERT INTO tblinvoice_line SET invoice_id='".$inv_id_new."',
																		name='".$stripe_text."',
																		quantity='1',
																		price='".($stripe_value['amount']/100)."',
																		amount='".($stripe_value['amount']/100)."',
																		vat = '".$amount_vat_percent."',
																		created='".$time."',
																		created_by='Payment Gateway' ");
						}else if($stripe_key==1){
							$stripe_text=$remaining.' '.$stripe_plan.' '.$between.' '.date(ACCOUNT_DATE_FORMAT,$stripe_value['period']['start']).' '.$and.' '.date(ACCOUNT_DATE_FORMAT,$stripe_value['period']['end']);
							/*$this->db_gaetan->query("INSERT INTO tblinvoice_line SET invoice_id='".$inv_id."',
																		name='".$stripe_text."',
																		quantity='1',
																		price='".($stripe_value['amount']/100)."',
																		amount='".($stripe_value['amount']/100)."',
																		vat = '".$amount_vat_percent."',
																		created='".$time."',
																		created_by='Payment Gateway' ");*/
							$this->db_new_gaetan->query("INSERT INTO tblinvoice_line SET invoice_id='".$inv_id_new."',
																		name='".$stripe_text."',
																		quantity='1',
																		price='".($stripe_value['amount']/100)."',
																		amount='".($stripe_value['amount']/100)."',
																		vat = '".$amount_vat_percent."',
																		created='".$time."',
																		created_by='Payment Gateway' ");
						}else{
							/*$single_line=$this->db_gaetan->insert("INSERT INTO tblinvoice_line SET invoice_id='".$inv_id."',
																		name='".$text."',
																		quantity='1',
																		price='".($stripe_value['amount']/100)."',
																		amount='".($stripe_value['amount']/100)."',
																		vat = '".$amount_vat_percent."',
																		created='".$time."',
																		created_by='Payment Gateway' ");*/
							$single_line_new=$this->db_new_gaetan->insert("INSERT INTO tblinvoice_line SET invoice_id='".$inv_id_new."',
																		name='".$text."',
																		quantity='1',
																		price='".($stripe_value['amount']/100)."',
																		amount='".($stripe_value['amount']/100)."',
																		vat = '".$amount_vat_percent."',
																		created='".$time."',
																		created_by='Payment Gateway' ");

							if($cu['discount'] != null){
								$user_discount=\Stripe\Coupon::retrieve($user_info->f('stripe_discount_id'));
								if($user_discount['amount_off']==null){
									/*$this->db_gaetan->query("UPDATE tblinvoice_line SET discount='".$user_discount['percent_off']."' WHERE id='".$single_line."' ");
									$this->db_gaetan->query("UPDATE tblinvoice SET discount='0' WHERE id='".$inv_id."' ");*/

									$this->db_new_gaetan->query("UPDATE tblinvoice_line SET discount='".$user_discount['percent_off']."' WHERE id='".$single_line_new."' ");
									$this->db_new_gaetan->query("UPDATE tblinvoice SET discount='0' WHERE id='".$inv_id_new."' ");
								}
							}
						}
					}
				}			
			}
			
			/*$this->db_users->query("INSERT INTO tblinvoice_payments SET
                                                    invoice_id  = '".$inv_id."',
                                                    date        = '".date("Y-m-d",$time)."',
                                                    amount      = '".($amount+$amount_vat)."',
                                                    info        = '".(date(ACCOUNT_DATE_FORMAT,$time).'  -  '.html_entity_decode('&euro;', ENT_COMPAT, 'UTF-8').' '.display_number($amount+$amount_vat))."' ");*/
			$this->db_new_gaetan->query("INSERT INTO tblinvoice_payments SET
                                                    invoice_id  = '".$inv_id_new."',
                                                    date        = '".date("Y-m-d",$time)."',
                                                    amount      = '".($amount+$amount_vat)."',
                                                    info        = '".(date(ACCOUNT_DATE_FORMAT,$time).'  -  '.html_entity_decode('&euro;', ENT_COMPAT, 'UTF-8').' '.display_number($amount+$amount_vat))."' ");
			$trace_id=$this->db_new_gaetan->insert("INSERT INTO tracking SET
                                origin_buyer_id  ='".$c_id."',
                                target_id        ='".$inv_id_new."',             
                                target_type      ='1',
                                target_buyer_id  ='".$c_id."',
                                creation_date    ='".time()."',
                                archived         ='0' ");

			$this->db_new_gaetan->query("UPDATE tblinvoice SET trace_id='".$trace_id."' WHERE id='".$inv_id_new."' ");

			$this->db_users->query("INSERT INTO `user_logs` SET `database_name`='".$user_info->f('database_name')."', `action`='Payment', `date`='".$time."' ");
			$this->db_users->query("UPDATE users SET last_payment='".($amount+$amount_vat)."', payment_type='3' WHERE database_name='".$user_info->f('database_name')."' ");
			$this->db_users->query("INSERT INTO `licences_log` SET `month`='".date('n')."',`year`='".date('Y')."',`type`='".$user_info->f('plan')."',
									`users`='".$user_info->f('users')."', `time_users`='".$user_info->f('time_users')."', `webshop`='".$user_info->f('webshop_yes')."' ");
			$this->db_users->query("INSERT INTO `clients_pay_log` SET `month`='".date('n')."',`year`='".date('Y')."',`type`='".$user_info->f('plan')."', `users`='1' ");
			// echo 'invoice created<br>';
			doManageLog('Payment.',$user_info->f('database_name'),array( array("property"=>'payment',"value"=>time()*1000) ));
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function cancel(&$in)
	{
		global $config;
		//$stripe_user=$this->db_users->field("SELECT stripe_cust_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$stripe_user=$this->db_users->field("SELECT stripe_cust_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
		if($stripe_user){
			\Stripe\Stripe::setApiKey($config['stripe_api_key']);
			//$stripe_subsc=$this->db_users->field("SELECT stripe_subscr_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
			$stripe_subsc=$this->db_users->field("SELECT stripe_subscr_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
			$cu = \Stripe\Customer::retrieve($stripe_user);
			$cu->subscriptions->retrieve($stripe_subsc)->cancel();
			//$this->db_users->field("UPDATE users SET stripe_subscr_id='' WHERE user_id='".$_SESSION['u_id']."'");
			$this->db_users->field("UPDATE users SET stripe_subscr_id= :stripe_subscr_id WHERE user_id= :user_id",['stripe_subscr_id'=>'','user_id'=>$_SESSION['u_id']]);
		}

		$this->db_users->query("UPDATE users SET payed='2', active='2',payment_fail_next='0',payment_fail='0' WHERE database_name='".DATABASE_NAME."' AND active!='1000' ");
		$this->db_users->query("UPDATE users SET payed='2', payment_fail_next='0',payment_fail='0' WHERE database_name='".DATABASE_NAME."' AND active='1000' ");
		$time = time();
		$this->db_users->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account canceled by customer', `date`='".$time."' ");
		msg::success(gm("Subscription canceled"),"success");

		$mail = new PHPMailer();
		$mail->WordWrap = 50;
		$fromMail='notification@akti.com';
		$mail->SetFrom($fromMail, $fromMail);
		//$this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
        $this->db_users->move_next();
        $body ="Dear Gaëtan,

		        ".utf8_encode($this->db_users->f('first_name'))." ".utf8_encode($this->db_users->f('last_name'))." has canceled subsription to Akti.

		        Your can see acount info <a href=\"http://manage.akti.com/index.php?pag=user&user_id=".$_SESSION['u_id']."\"> here</a>

				Best regards,
				Akti Customer Care
				<a href=\"http://akti.com\" >www.akti.com</a>";
				$subject= 'A user has canceled subscription to Akti';

		$mail->Subject = $subject;
        $mail->MsgHTML(nl2br($body));
		$mail->AddAddress('g.loriot@thinkweb.be');

		$mail->Send();
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function save_vat(&$in)
	{
		//$this->db_users->query("UPDATE users SET valid_vat='".$in['vat']."' WHERE database_name='".DATABASE_NAME."' ");
		$this->db_users->query("UPDATE users SET valid_vat= :valid_vat WHERE database_name= :d ",['valid_vat'=>$in['vat'],'d'=>DATABASE_NAME]);
		return true;
	}
}