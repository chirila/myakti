<?php
class ftp{

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function ftp()
	{
		$this->db = new sqldb();
	}


	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function ftp_import(&$in){
		$this->db->query("UPDATE apps SET active='".$in['activate']."' WHERE name='FTP Import' AND app_type='".$in['app_id']."'");
		if($in['activate']=="0"){
			$in['xget']='apps';
			return true;
		}
		$in['ftp_host']=str_replace('FTP://', '', $in['ftp_host']);
		$in['ftp_host']=trim ( $in['ftp_host'] ,'/' );
		$ftp = new ftp($in['ftp_host'],$in['ftp_user'],$in['ftp_password']);
/*		if(!$ftp->is_connect){
			$this->db->query("UPDATE apps SET active='0' WHERE name='FTP Import' AND app_type='".$in['app_id']."'");
			msg::error ( gm("The info provided is not valid!"),'error');
			return false;
		}*/
		$app_id=$this->db->field("SELECT app_id FROM apps WHERE name='FTP Import' AND app_type='".$in['app_id']."'");
		$this->db->query("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='user' ");

    		if(!$this->db->move_next()){
    			$this->db->query("INSERT INTO apps SET api='".$in['ftp_user']."', main_app_id='".$app_id."', type='user' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".$in['ftp_user']."' WHERE main_app_id='".$app_id."' AND type='user' ");
			}
		$this->db->query("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='password' ");
    		if(!$this->db->move_next()){
    			$this->db->query("INSERT INTO apps SET api='".$in['ftp_password']."', main_app_id='".$app_id."', type='password' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".$in['ftp_password']."' WHERE main_app_id='".$app_id."' AND type='password' ");
			}
		$this->db->query("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='host' ");
    		if(!$this->db->move_next()){
    			$this->db->query("INSERT INTO apps SET api='".$in['ftp_host']."', main_app_id='".$app_id."', type='host' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".$in['ftp_host']."' WHERE main_app_id='".$app_id."' AND type='host' ");
			}

			$adveo=$this->db->field("SELECT customer_id FROM customers WHERE  name regexp 'a[^a-zA-Z0-9]?d[^a-zA-Z0-9]?v[^a-zA-Z0-9]?e[^a-zA-Z0-9]?o[^a-zA-Z0-9]?'");
			if(!$adveo){
				$this->db->query("INSERT INTO customers SET name = 'Adveo',
															commercial_name = '',
															active	= '5',
															legal_type	= '',
															user_id	= '',
															c_type	= '',
															website	= '',
															language	= '',
															c_email	= '',
															sector	= '',
															comp_phone	= '',
															comp_fax	= '',
															activity	= '',
															comp_size	= '',
															lead_source	= '',
															various1	= '',
															various2	= '',
															c_type_name	= '',
															zip_name	= '',
															our_reference	= '',
															acc_manager_name	= '',
															sales_rep	= '',
															city_name = '',
															country_name = 'Belgium',
															payment_term = '',
															creation_date	= '".time()."'");
			}
			$this->db->query("SELECT * FROM account_address WHERE naming='Adveo' ");
			if(!$this->db->next()){
				$this->db->insert("INSERT INTO account_address SET
                           naming  			='Adveo',
                           country_id  	='26',
                           address 			='Europalaan 69',
                           zip 					='9800',
                           city 				='Deinze',
                           is_delivery  ='1'
	        								");
			}


			if(!defined('FTP_IMPORT_LAST_UPDATE')){
				$this->db->query("INSERT INTO settings (`constant_name` ,`value` ,`long_value` ,`module` ,`type`)VALUES ('FTP_IMPORT_LAST_UPDATE', '1', NULL , '', '1')");
				$this->db->query("CREATE TABLE IF NOT EXISTS `imported_orders_log` (
								  `id` int(11) NOT NULL AUTO_INCREMENT,
								  `filename` varchar(255) NOT NULL,
								  `process_date` varchar(255) NOT NULL,
								  `in` text NOT NULL,
								  `xml_object` text NOT NULL,
								  PRIMARY KEY (`id`)
								) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ");
				$this->db->query("CREATE TABLE IF NOT EXISTS `imported_order_files` (
								  `id` int(11) NOT NULL AUTO_INCREMENT,
								  `filename` varchar(255) NOT NULL,
								  `file_timestamp` varchar(255) NOT NULL,
								  PRIMARY KEY (`id`)
								) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ");
			}
			$in['xget']='apps';

		return true;
	}
	function winbooks(&$in)
	{
		$this->db->query("UPDATE apps SET active=0 WHERE name='WinBooks' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
		if(!$in['activate']){
			$in['xget']='apps';
			return true;
		}
		$app = $this->db->field("SELECT app_id FROM apps WHERE name='WinBooks' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
		if(!$app){
			return;
		}
		$actions = array('host','push_user','ap_push_key','ar_push_key','pull_user','ap_pull_key','ar_pull_key');
		foreach ($actions as $key => $value) {
			$this->db->query("SELECT api FROM apps WHERE  main_app_id='".$app."' AND type='".$value."' ");
	 		if(!$this->db->next()){
	  		$this->db->query("INSERT INTO apps SET api='".$in[$value]."', main_app_id='".$app."', type='".$value."' ");
	  	}else{
	  		$this->db->query("UPDATE apps SET api='".$in[$value]."' WHERE main_app_id='".$app."' AND type='".$value."' ");
			}
		}

		$in['is_activated'] = 0;
		$in['ap_push_key2']=str_replace(array('FTP://','ftp://'), '', $in['ap_push_key']);
		$in['ap_push_key2']=trim ( $in['ap_push_key2'] ,'/' );
		$ftp = new ftp($in['ap_push_key2'],$in['push_user'],$in['ar_push_key']);
/*		if(!$ftp->is_connect){
			msg::error ( gm("The info provided is not valid!"),'success');
			return false;
		}*/
		if($in['activate']){
			$this->db->query("UPDATE apps SET active=1 WHERE name='WinBooks' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
			$in['is_activated'] = 1;
		}
		$in['xget']='apps';
		return true;
	}

}

?>