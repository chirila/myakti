<?php
use \Dropbox as dbx;
class dropbox{

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function dropbox()
	{
		$this->db = new sqldb();
	}

	function step1(&$in){
		$json_cred = array( "key" => DROPBOX_KEY,  "secret" => DROPBOX_SECRET );
		$appInfo = dbx\AppInfo::loadFromJson($json_cred);
		$webAuth = new dbx\WebAuthNoRedirect($appInfo, "PHP-Example/1.0");
		$in['access_url'] = $webAuth->start(); # create the url to allow the application access your dropbox account
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function step2(&$in)
	{
		global $config;
		if(!$in['authCode']){
			msg::error(gm("Please fill all the fields"),"error");
			json_out($in);
		}
		/*$json_cred = array( "key" => DROPBOX_KEY,  "secret" => DROPBOX_SECRET );
		$appInfo = dbx\AppInfo::loadFromJson($json_cred);
		$webAuth = new dbx\WebAuthNoRedirect($appInfo, "PHP-Example/1.0");
		list($accessToken, $dropboxUserId) = $webAuth->finish($in['authCode']);
		$this->db->query("UPDATE apps SET api='".$accessToken."', active='1' WHERE name='Dropbox' AND main_app_id='0' AND type='main' ");
		$dbxClient = new dbx\Client($accessToken, "PHP-Example/1.0");
		$accountInfo = $dbxClient->getAccountInfo();
		if(is_array($accountInfo)){
		    $c = $dbxClient->createFolder('/customers');
		    $p = $dbxClient->createFolder('/orders');
		    $q = $dbxClient->createFolder('/quotes');
		    $o = $dbxClient->createFolder('/projects');
		    $s = $dbxClient->createFolder('/upload_invoices');
		    // $s = $dbxClient->createFolder('/contracts');
		    msg::success(gm("Success"),"success");
		    $const = 'ALLOW_QUOTE_DROP_BOX';
		    $this->db->query("SELECT * FROM settings WHERE constant_name='".$const."' ");
			if($this->db->next()){
				$up = $this->db->update("UPDATE settings SET value='1' WHERE constant_name='".$const."' ");
			}else{
				$this->db->query("INSERT INTO settings SET value='1',constant_name='".$const."' ");
			}
		}else{
			$const = 'ALLOW_QUOTE_DROP_BOX';
			$this->db->query("SELECT * FROM settings WHERE constant_name='".$const."' ");
			if($this->db->next()){
				$up = $this->db->update("UPDATE settings SET value='0' WHERE constant_name='".$const."' ");
			}else{
				$this->db->query("INSERT INTO settings SET value='0',constant_name='".$const."' ");
			}
			msg::error($accountInfo,"error");
			json_out($in);
		}*/
		$ch = curl_init();
		$drop_data=array(
			'code'			=> $in['authCode'],
			'grant_type'	=> 'authorization_code'
			);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
	    curl_setopt($ch, CURLOPT_USERPWD, DROPBOX_KEY.":".DROPBOX_SECRET);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $drop_data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, 'https://api.dropboxapi.com/oauth2/token');

        $put = curl_exec($ch);
    	$info = curl_getinfo($ch);

    	if($info['http_code']>300 || $info['http_code']==0){
    		$resp_error=json_decode($put);
    		msg::error($resp_error->error_description,'error');
    		json_out($in);
    	}else{
    		$data_c=json_decode($put);
    		$this->db->query("UPDATE apps SET api='".$data_c->access_token."', active='1' WHERE name='Dropbox' AND main_app_id='0' AND type='main' ");

    		$app = $this->db->query("SELECT * FROM apps WHERE name='Dropbox' AND type='main' AND main_app_id='0' ");
			if($app->next()){

	    		$access_token = $this->db->field("SELECT app_id FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='access_token' ");
				if(!$access_token){
					$this->db->query("INSERT INTO apps SET main_app_id='".$app->f('app_id')."', type='access_token', api='".$data_c->access_token."' ");
				}else{
					$this->db->query("UPDATE apps SET api ='".$data_c->access_token."'
	    			 								 WHERE main_app_id='".$app->f('app_id')."' AND type='access_token'");	
				}

				$refresh_token = $this->db->field("SELECT app_id FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='refresh_token' ");
				if(!$refresh_token){
					$this->db->query("INSERT INTO apps SET main_app_id='".$app->f('app_id')."', type='refresh_token', api='".$data_c->refresh_token."' ");
				}else{
					$this->db->query("UPDATE apps SET api ='".$data_c->refresh_token."'
	    			 								 WHERE main_app_id='".$app->f('app_id')."' AND type='refresh_token'");	
				}

				$authorizationcode = $this->db->field("SELECT app_id FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='authorizationcode' ");
				if(!$authorizationcode){
					$this->db->query("INSERT INTO apps SET main_app_id='".$app->f('app_id')."', type='authorizationcode', api='".$auth_code."' ");
				}else{
					$this->db->query("UPDATE apps SET api ='".$auth_code."'
	    			 								 WHERE main_app_id='".$app->f('app_id')."' AND type='authorizationcode'");	
				}

				$expires_in = $this->db->field("SELECT app_id FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='expires_in' ");
				$expires_time = time() + $data_c->expires_in;
				if(!$expires_in){
					$this->db->query("INSERT INTO apps SET main_app_id='".$app->f('app_id')."', type='expires_in', api='".$expires_time."' ");
				}else{
					$this->db->query("UPDATE apps SET api ='".$expires_time."'
	    			 								 WHERE main_app_id='".$app->f('app_id')."' AND type='expires_in'");	
				}
			}

    		$const = 'ALLOW_QUOTE_DROP_BOX';
		    $this->db->query("SELECT * FROM settings WHERE constant_name='".$const."' ");
			if($this->db->next()){
				$up = $this->db->update("UPDATE settings SET value='1' WHERE constant_name='".$const."' ");
			}else{
				$this->db->query("INSERT INTO settings SET value='1',constant_name='".$const."' ");
			}
			if(strpos($config['site_url'],'my.akti')!==false && $_SESSION['u_id']<10000){
				$basic_folder="";
			}else{
				$basic_folder='/APP/Akti';
			}		
			$headers=array("Content-Type: application/json","Authorization: Bearer ".$data_c->access_token);
		   	$folders=array('/customers','/orders','/quotes','/projects','/upload_invoices','/contracts');
	    	foreach($folders as $key=>$value){
				$drop_data=array(
						"path" 				=> $basic_folder.$value,
	    				"autorename"		=> false
				);
				$drop_data=json_encode($drop_data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    curl_setopt($ch, CURLOPT_POST, true);
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $drop_data);
			    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    curl_setopt($ch, CURLOPT_URL, 'https://api.dropboxapi.com/2/files/create_folder');

			    $put = curl_exec($ch);
			    $info = curl_getinfo($ch);
	    	}
	    	msg::success ( gm("Changes have been saved."),'success');
	    	return true;	
    	}		
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function deactivateDrop(&$in)
	{
		$this->db->query("UPDATE apps SET api='', active='0' WHERE name='Dropbox' AND main_app_id='0' AND type='main' ");
		$const = 'ALLOW_QUOTE_DROP_BOX';
		$this->db->query("SELECT * FROM settings WHERE constant_name='".$const."' ");
		if($this->db->next()){
			$up = $this->db->update("UPDATE settings SET value='0' WHERE constant_name='".$const."' ");
		}else{
			$this->db->query("INSERT INTO settings SET value='0',constant_name='".$const."' ");
		}
		$this->db->query("UPDATE settings SET value='' WHERE constant_name='AUTOMATIC_XML_PROCESS_P_INVOICE'");
		msg::success ( gm("Success"),'success');
		return true;
	}

	function getFolder(&$in){
		$ch = curl_init();
		$access_token=$this->db->field("SELECT api FROM apps WHERE name='Dropbox' AND type='main' AND main_app_id='0' ");
		$headers=array("Content-Type: application/json","Authorization: Bearer ".$access_token);
		$drop_data=array(
			"path" 				=> $in['path'],
		);
		$drop_data=json_encode($drop_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $drop_data);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_URL, 'https://api.dropboxapi.com/2/files/list_folder');

		$put = curl_exec($ch);
		$info = curl_getinfo($ch);

		if($info['http_code']>300 || $info['http_code']==0){
			msg::error(gm('Error'),'error');
	    	json_out($in);
	   	}else{
	   		$data_e=json_decode($put);
	   		$result=array();
	   		foreach($data_e->entries as $key=>$value){
	   			if($value->{'.tag'}=='folder'){
	   				$item=array(
		   				'name'	=> $value->name,
		   				'path'	=> $value->path_lower
		   				);
		   			array_push($result,$item);
	   			}			
	   		}
	   		json_out($result);
	    }
	}

}

?>