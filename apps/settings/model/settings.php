<?php

/**
* 
*/
class settings
{
	
	private $db;
	protected $pag = 'settings'; 						# used for log messages
	protected $field_n = 'settings_id';					# used for log messages

	function __construct()
	{
		$this->db = new sqldb();
		global $database_config;
		//ark::loadLibraries(array('stripe/init'));
/*		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($db_config);*/
		$this->database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$this->db_users =  new sqldb($this->database_2);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
		/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/

	function add(&$in)
	{
		global $config;
		$this->db_users->query("select user_id,database_name, migrated from users where database_name='".DATABASE_NAME."' and main_user_id=0");
		$this->db_users->move_next();
		$main_user_id=$this->db_users->f('user_id');
		$migrated = $this->db_users->f('migrated');
		if($in['group_id']==1){
			$in['user_type'] = 3;
		}


		$u_role = '2';
		//$cred = $this->db->field("SELECT credentials FROM groups WHERE group_id='".$in['group_id']."' ");
		$pr_admin = '0';
		if($in['user_type'] == 3){
			$u_role = '1';
			$pr_admin = '1';
			$in['group_id'] = 1;
			//$cred = $this->db->field("SELECT credentials FROM groups WHERE group_id='".$in['group_id']."' ");
			$cred = $this->db_users->field("SELECT credentials FROM users WHERE user_id='".$main_user_id."' ");
		}
		if($in['user_type']==2){
			$cred = 8;
			$in['group_id'] = 2;
		}
		if($in['user_type']==1){
			/*$in['group_id'] = '2';*/
			$cred = $this->db->field("SELECT credentials FROM groups WHERE group_id='".$in['group_id']."' ");
			$u_role = '1';
			$pr_admin = '1';
		}
		if(!$this->add_validate_user($in)){
			return false;
		}
		if(!$in['group_id']){
			$cred = '1;5;12';
			$pr_admin = '0';
		}
		// $u_role = '2';
		// $cred = $this->db->field("SELECT credentials FROM groups WHERE group_id='".$in['group_id']."' ");
		/*if($in['group_id'] == 1){
			$u_role = '1';
		}*/
		// if($in['user_type']==2){
		// 	$in['group_id'] = 1;
		// }elseif($in['user_type']==1){
		// 	$in['group_id'] = 2;
		// }

		//No need to update the settings
		   //  $this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_INVOICE_PDF' ");
     // 		$this->db->query("UPDATE settings set value='1' WHERE constant_name='ACCOUNT_INVOICE_HEADER_PDF_FORMAT'");
     //  		$this->db->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
     // 		$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_INVOICE_PDF_MODULE'");
	  		// $this->db->query("UPDATE settings set value='1' WHERE constant_name='ACCOUNT_INVOICE_BODY_PDF_FORMAT'");
     //  		$this->db->query("UPDATE settings set value='1' where constant_name='ACCOUNT_INVOICE_FOOTER_PDF_FORMAT'");
     //  		$this->db->query("UPDATE settings set value='1' where constant_name='ACCOUNT_QUOTE_HEADER_PDF_FORMAT'");
     //  		$this->db->query("UPDATE settings set value='1' where constant_name='ACCOUNT_QUOTE_BODY_PDF_FORMAT'");
     //  		$this->db->query("UPDATE settings set value='1' where constant_name='ACCOUNT_QUOTE_FOOTER_PDF_FORMAT'");

		$payment_type = $this->db_users->field("SELECT payment_type FROM users WHERE main_user_id='0' AND database_name='".DATABASE_NAME."' limit 1");
		/*$in['user_id'] = $this->db_users->insert("INSERT INTO users SET
                                                    main_user_id  	= '".$main_user_id."',
                                                    user_role     	= '".$u_role."',
                                                    database_name 	= '".DATABASE_NAME."',
                                                    first_name 		= '".addslashes($in['first_name'])."',
                                                    last_name 		= '".addslashes($in['last_name'])."',
                                                    payment_type	= '".$payment_type."',
													username  		= '".$in['username']."',
													password 	    = '".md5($in['password'])."',
													email 	    	= '".$in['email']."',
													country_id 		= '".$in['country_id']."',
													lang_id 		= '".$in['lang_id']."',
													group_id		= '".$in['group_id']."',
													credentials		= '".$cred."',
													user_type		= '".$in['user_type']."',
													h_rate			= '".return_value($in['h_rate'])."',
													h_cost 			= '".return_value($in['h_cost'])."',
													daily_rate			= '".return_value($in['daily_rate'])."',
													daily_cost 			= '".return_value($in['daily_cost'])."',
													migrated		= '".$migrated."' ");*/
		$in['user_id'] = $this->db_users->insert("INSERT INTO users SET
                                                    main_user_id  	= :main_user_id,
                                                    user_role     	= :user_role,
                                                    database_name 	= :database_name,
                                                    first_name 		= :first_name,
                                                    last_name 		= :last_name,
                                                    payment_type	= :payment_type,
													username  		= :username,
													password 	    = :password,
													email 	    	= :email,
													country_id 		= :country_id,
													lang_id 		= :lang_id,
													group_id		= :group_id,
													credentials		= :credentials,
													user_type		= :user_type,
													h_rate			= :h_rate,
													h_cost 			= :h_cost,
													daily_rate		= :daily_rate,
													daily_cost 		= :daily_cost,
													migrated		= :migrated ",
												[	'main_user_id'  => $main_user_id,
                                                    'user_role'     => $u_role,
                                                    'database_name' => DATABASE_NAME,
                                                    'first_name' 	=> addslashes($in['first_name']),
                                                    'last_name' 	=> addslashes($in['last_name']),
                                                    'payment_type'	=> $payment_type,
													'username'  	=> $in['username'],
													'password' 	    => md5($in['password']),
													'email' 	    => $in['email'],
													'country_id' 	=> $in['country_id'],
													'lang_id' 		=> $in['lang_id'],
													'group_id'		=> $in['group_id'],
													'credentials'	=> $cred,
													'user_type'		=> $in['user_type'],
													'h_rate'		=> return_value($in['h_rate']),
													'h_cost' 		=> return_value($in['h_cost']),
													'daily_rate'	=> return_value($in['daily_rate']),
													'daily_cost' 	=> return_value($in['daily_cost']),
													'migrated'		=> $migrated]
											);

		//$this->db_users->query("INSERT INTO users_settings SET user_id= '".$in['user_id']."' ");
		$this->db_users->insert("INSERT INTO users_settings SET user_id= :user_id ",['user_id'=>$in['user_id']]);
		//$this->db_users->query("INSERT INTO user_meta SET name='project_admin', value='".$pr_admin."', user_id='".$in['user_id']."' ");
		$this->db_users->insert("INSERT INTO user_meta SET name= :name, value= :value, user_id= :user_id ",['name'=>'project_admin','value'=>$pr_admin,'user_id'=>$in['user_id']]);
		$globfo = $this->db_users->query("select * from user_info where user_id='".$main_user_id."' ");
		$globfo->next();
		//$this->db_users->query("INSERT INTO user_info SET user_id='".$in['user_id']."', start_date='".time()."', is_trial='".$globfo->f('is_trial')."', pricing_type='".$globfo->f('pricing_type')."', end_date='".$globfo->f('end_date')."' ");
		$this->db_users->insert("INSERT INTO user_info SET user_id= :user_id, start_date= :start_date, is_trial= :is_trial, pricing_type= :pricing_type, end_date= :end_date, default_identity_id= :default_identity_id ",['user_id'=>$in['user_id'],'start_date'=>time(),'is_trial'=>$globfo->f('is_trial'),'pricing_type'=>$globfo->f('pricing_type'),'end_date'=>$globfo->f('end_date'),'default_identity_id'=>$in['identity_id']]);

		if($in['send_mail'] && $in['send_mail']==1){
				$this->send_invite_mail($in);
		}

			//Values inserted into Gaetan account
		if(strpos($config['site_url'],'my.akti')){
			global $database_config;
			$database_new = array(
					'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
					'username' => 'admin',
					'password' => 'hU2Qrk2JE9auQA',
					'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
			);
				
			$db_user_new = new sqldb($database_new);

			$country_name = $db_user_new->field("SELECT name FROM country WHERE country_id='".$country_id."'");

			$select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$_SESSION['u_id']."' AND field_id='1' ");
			if($select_contact){
				$select_customer = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$select_contact."' AND `primary`='1'");
				if($select_customer){
					$in['contact_new']=$db_user_new->insert("INSERT INTO customer_contacts SET 
																customer_id='".$select_customer."',
																firstname='".utf8_decode($in['first_name'])."',
																lastname='".utf8_decode($in['last_name'])."',
																is_primary='0',
																active='1'
					");
					$in['contact_newids']=$db_user_new->insert("INSERT INTO customer_contactsIds SET 
																customer_id='".$select_customer."',
																contact_id='".$in['contact_new']."',
																email='".$in['email']."',
																`primary`='0'
					");

					$custome_contact_field_snap = $db_user_new->insert("INSERT INTO contact_field SET
														field_id='1',
														customer_id='".$in['contact_new']."',
														value='".$in['user_id']."'
					");
					$custome_contact_field_username = $db_user_new->insert("INSERT INTO contact_field SET
																field_id='2',
																customer_id='".$in['contact_new']."',
																value='".$in['username']."'
					");
				}

				
			}
			
		}
			//end values inserted


		//msg::success ( gm("User added"),'success');
		return true;
	}
	// function add_validate_user(&$in)
	// {
	// 	$user_role = $this->db_users->field("SELECT main_user_id FROM users WHERE user_id='".$in['user_id']."' ");
	// 	if($user_role == 0 && $in['do'] == 'settings-user-user-update' ){
	// 		$in['group_id']	 = 1;
	// 	}
	// 	$v = new validation($in);
	// 	$is_ok = true;
	// 	$v->field('first_name',gm('First Name'),'required');
	// 	$v->field('last_name',gm('Last Name'),'required');
	// 	$v->field('email',gm('Email'),'required');
	// 	$v->field('country_id',gm('Country'),'required');
	// 	$v->field('lang_id',gm('Language'),'required',"Please select a 'Language'");
	// 	$v->field('username', gm("Username"), "required:min_length[6]:unique[users.username.( user_id!='".$in['user_id']."')]",false,$this->database_2);
	// 	$v->field('password',gm("Password"),'required');
	// 	$v->field('password1',gm("Confirm Password"),'required');
	// 	$is_ok = $v->run();

	// 	/*$trial = $this->db_users->field("SELECT is_trial FROM user_info WHERE user_id=(SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' AND u_type !=1) ");*/
	// 	$in['is_trial'] = $trial;
	// 	if($trial == 1){
			
	// 		switch (true) {
	// 			case defined('PAYMILL_CLIENT_ID'):
	// 				if(!$this->check_add_user2($in)){
	// 					return false;
	// 				}
	// 				break;
	// 			default:
	// 				if(!$this->check_add_user($in)){
	// 					return false;
	// 				}
	// 				break;
	// 		}
	// 	}
	// 	return $is_ok;
	// }
	function send_invite_mail(&$in){
		//require_once ('class.phpmailer.php');
       // include_once ("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

        $mail = new PHPMailer();
        $mail->WordWrap = 50;
        /*$mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPDebug = 1; // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth = true; // enable SMTP authentication
        $mail->Host = SMTP_HOST; // sets the SMTP server
        $mail->Port = SMTP_PORT; // set the SMTP port for the GMAIL server
        $mail->Username = SMTP_USERNAME; // SMTP account username
        $mail->Password = SMTP_PASSWORD; // SMTP account password*/

        $fromMail='notification@akti.com';
        $mail->SetFrom($fromMail, $fromMail);
        //$this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
        $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
        $this->db_users->move_next();
        //$username = get_user_name($_SESSION['u_id']);
        $username = htmlspecialchars_decode(utf8_decode(stripslashes($this->db_users->f('first_name')))).' '.htmlspecialchars_decode(utf8_decode(stripslashes($this->db_users->f('last_name'))));
        switch ($in['lang_id']) {
        	case '1':
        		$body ="Beste ".$in['first_name']." ".$in['last_name'].",

		        ".$username." Nodigt u uit om eveneens Akti te gebruiken.

		        Uw gebruikersnaam en paswoord zijn:

		      	Gebruikersnaam: ".$in['username']."
				Paswoord: ".$in['password']."

				We raden u ten stelligste aan om uw paswoord te wijzigen, zodra u bent aangemeld.

				U kan zich <a href=\"https://my.akti.com\"> hier </a> aanmelden.

				Vriendelijke groeten,
				Akti klanten ondersteuning
				<a href=\"http://akti.com\" >www.akti.com</a>";
				$subject= 'U bent uitgenodigd voor Akti';
        		break;
        	case '2':
        		$body ="Dear ".$in['first_name']." ".$in['last_name'].",

		        ".$username." invited you to join Akti.

		        Your username and password are:

		      	Username: ".$in['username']."
				Password: ".$in['password']."

				It is strongly recommended to change your password the first time you log in.

				You can log in <a href=\"https://my.akti.com\"> here </a>

				Best regards,
				Akti Customer Care
				<a href=\"http://akti.com\" >www.akti.com</a>";
				$subject= 'You have been invited to Akti';
        		break;
        	case '3':
        		$body ="Cher(e) ".utf8_decode($in['first_name'])." ".utf8_decode($in['last_name']).",

		        ".$username." vous invite ".utf8_decode('à')." rejoindre Akti.

		        Votre nom d'utilisateur et mot de passe sont:

		      	Nom d'utilisateur: ".$in['username']."
				Mot de passe: ".$in['password']."

				Il est fortement ".utf8_decode('conseillé')." de modifier votre mot de passe ".utf8_decode('dès')." que vous ".utf8_decode('êtes')." ".utf8_decode('connecté').".

				Vous pouvez vous connecter <a href=\"https://my.akti.com\"> ici </a>.

				Cordialement,
				Akti suivi ".utf8_decode('clientèle')."
				<a href=\"http://akti.com\" >www.akti.com</a>";
				$subject= 'Vous avez '.utf8_decode('été').' '.utf8_decode('invité').' pour Akti';
        		break;
        }

        $mail->Subject = $subject;
        $mail->MsgHTML(nl2br($body));
		$mail->AddAddress(trim($in['email']));

		$mail->Send();
		return true;
	}	
	function update_user(&$in)
	{
		global $config;
		$u_role = '2';
		$cred = $this->db->field("SELECT credentials FROM groups WHERE group_id='".$in['group_id']."' ");
		$pr_admin = '0';
		if($in['group_id'] == 1){
			$u_role = '1';
			$pr_admin = '1';
			$in['user_type']=3;
		}
		if($in['user_type']==2){
			$cred = 8;
			$in['group_id'] = 2;
		}
		if($in['user_type']==3){
			$in['group_id'] = 1;
			$cred = $this->db->field("SELECT credentials FROM groups WHERE group_id='".$in['group_id']."' ");
			$u_role = '1';
			$pr_admin = '1';
		}
		//$group = $this->db_users->field("SELECT group_id FROM users WHERE user_id='".$in['user_id']."' ");
		$group = $this->db_users->field("SELECT group_id FROM users WHERE user_id= :user_id ",['user_id'=>$in['user_id']]);
		$add = '';
		if($group != $in['group_id']){
			$add = "credentials		= '".$cred."',";
		}
		if(!$this->update_user_validate($in))
		{
			return false;
		}

		//create an array with pairs of credentials and admin credentials
		$credential_pairs = array(	'1'	=>	'crm_admin',
									'6' => 	'order_admin',
									'5'	=>	'quote_admin',
									'3'	=> 	'project_admin',
									'11'=>	'contract_admin',
									'4'	=>	'invoice_admin',
									);
		$cred_ids = explode(';', $cred);
		foreach ($credential_pairs as $cred_id => $value) {
			if(!in_array($cred_id, $cred_ids)){
				//$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$in['user_id']."' AND name='admin_".$cred_id."' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'','user_id'=>$in['user_id'],'name'=>'admin_'.$cred_id]);
			}
		}
		$this->db_users->query("UPDATE users SET	first_name 	    = '".addslashes($in['first_name'])."',
													user_role     	= '".$u_role."',
													last_name 	    = '".addslashes($in['last_name'])."',
													username  	    = '".$in['username']."',
													email 			= '".$in['email']."',
													country_id 	    = '".$in['country_id']."',
							        				lang_id 		= '".$in['lang_id']."',
							        				group_id		= '".$in['group_id']."',
							        				".$add."
							        				user_type		= '".$in['user_type']."',
							        				h_rate			= '".return_value($in['h_rate'])."',
							        				h_cost 			= '".return_value($in['h_cost'])."',
							        				daily_rate			= '".return_value($in['d_rate'])."',
							        				daily_cost 			= '".return_value($in['d_cost'])."'
					    	   					WHERE user_id='".$in['user_id']."' ");

		$this->db_users->query("UPDATE user_info SET default_identity_id	    = '".$in['identity_id']."'
					    	   					WHERE user_id='".$in['user_id']."' ");

			//Values inserted into Gaetan account
		if(strpos($config['site_url'],'my.akti')){
			global $database_config;
			$database_new = array(
					'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
					'username' => 'admin',
					'password' => 'hU2Qrk2JE9auQA',
					'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
			);
				
			$db_user_new = new sqldb($database_new);

			$country_name = $db_user_new->field("SELECT name FROM country WHERE country_id='".$country_id."'");

			$select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$in['user_id']."' AND field_id='1' ");
			$select_customer = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$select_contact."' AND `primary`='1'");

			if($select_contact){
				$in['contact_new']=$db_user_new->query("UPDATE customer_contacts SET 
														firstname='".utf8_decode($in['first_name'])."',
														lastname='".utf8_decode($in['last_name'])."'
														WHERE contact_id='".$select_contact."'
				");
				$in['contact_newids']=$db_user_new->query("UPDATE customer_contactsIds SET email='".$in['email']."' WHERE contact_id='".$select_contact."'");
			}
			
		}
			//end values inserted



		//$this->db_users->query("UPDATE user_meta SET value='".$pr_admin."' WHERE user_id='".$in['user_id']."' AND name='project_admin' ");
		$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>$pr_admin,'user_id'=>$in['user_id'],'name'=>'project_admin']);
		//$contact_id = $this->db_users->field("SELECT contact_id FROM customer_contacts WHERE table_user_id='".$in['user_id']."'");
		$contact_id = $this->db_users->field("SELECT contact_id FROM customer_contacts WHERE table_user_id= :table_user_id",['table_user_id'=>$in['user_id']]);
		if ($contact_id)
		{
			/*$this->db_users->query("UPDATE customer_contacts SET
					firstname 	    = '".$in['first_name']."',
					lastname 		= '".$in['last_name']."',
					username  	    = '".$in['username']."',
					email 			= '".$in['email']."'
					WHERE contact_id='".$contact_id."'
					");*/
			$this->db_users->query("UPDATE customer_contacts SET
					firstname 	    = :firstname,
					lastname 		= :lastname,
					username  	    = :username,
					email 			= :email
					WHERE contact_id= :contact_id",
				[	'firstname' 	=> $in['first_name'],
					'lastname' 		=> $in['last_name'],
					'username'  	=> $in['username'],
					'email' 		=> $in['email'],
					'contact_id'	=> $contact_id]
			);
		}

		if($in['password']){
			/*$this->db_users->query("UPDATE users SET 
					password  = '".md5($in['password'])."'
					WHERE user_id='".$in['user_id']."'
					");*/
			$this->db_users->query("UPDATE users SET 
					password  = :password
					WHERE user_id= :user_id",['password'=>md5($in['password']),'user_id'=>$in['user_id']]);
		}
		//$this->db->query("DELETE FROM user_function WHERE user_id='".$in['user_id']."' ");
		if($in['unit_prices']){
			foreach ($in['unit_prices'] as $key => $value) {
				$this->db->query("INSERT INTO user_function SET func_id='".$key."', user_id='".$in['user_id']."' ");
			}
		}

		$this->db->query("UPDATE project_user SET user_name='".$in['first_name']." ".$in['last_name']."' WHERE user_id='".$in['user_id']."' ");

		$acc_manager_name = $in['first_name'].' '.$in['last_name'];
		$this->db->query("UPDATE customers SET acc_manager_name = '".$acc_manager_name."' WHERE user_id = '".$in['user_id']."' ");

		//msg::success ( gm("User updated"),'success');

		if($_SESSION['u_id'] == $in['user_id'] && empty($_SERVER['HTTP_X_REQUESTED_WITH'])){
			//$this->db_users->query("SELECT code FROM lang WHERE lang_id='".$in['lang_id']."'");
			$this->db_users->query("SELECT code FROM lang WHERE lang_id= :lang_id",['lang_id'=>$in['lang_id']]);
			$this->db_users->move_next();
			$lang = $this->db_users->f('code');

			$_SESSION['l'] = $lang;
			header('Location: index.php?do=settings-user&user_id='.$in['user_id'].'&success='.msg::$success);
		}

		return true;
	}
	/*function update_user_validate(&$in)
	{
		$is_ok = true;

		if(is_int($in['user_id']) && $in['group_id'] != 1) {
			$g_id = $this->db_users->field("SELECT group_id FROM users WHERE user_id='".$in['user_id']."' ");
			if($g_id == 1){
				$c = $this->db_users->field("SELECT count(user_id) FROM users WHERE database_name='".DATABASE_NAME."' AND group_id='1' ");
				if($c < 2){
					return false;
				}
			}
		}
		$v = new validation($in);
		$v->field('first_name', gm('first_name'), "required");
		$v->field('last_name', gm('last_name'), "required");
		$v->field('email', gm('email'), "required");
		$v->field('country_id', gm('country_id'), "required");
		$v->field('lang_id', gm('lang_id'), "required");
		$v->field('username', gm('username'), "required");
		$v->field('password', gm('password'), "required");
		$v->field('password1', gm('password1'), "required");
		$v->field('user_id', gm('user_id'), "required");
		$is_ok = $v->run();

		if(!$in['user_type']){
			return false;
		}
		if(!$this->add_validate_user($in)){
			$is_ok = false;
		}
		return $is_ok;
	}*/

	function update_user_validate(&$in)
	{
		$is_ok = true;

		if(is_int($in['user_id']) && $in['group_id'] != 1) {
			//$g_id = $this->db_users->field("SELECT group_id FROM users WHERE user_id='".$in['user_id']."' ");
			$g_id = $this->db_users->field("SELECT group_id FROM users WHERE user_id= :user_id ",['user_id'=>$in['user_id']]);
			if($g_id == 1){
				$c = $this->db_users->field("SELECT count(user_id) FROM users WHERE database_name='".DATABASE_NAME."' AND group_id='1' ");
				if($c < 2){
					return false;
				}
			}
		}
		$v = new validation($in);
		$v->field('user_id', gm('ID'), "required:exist[users.user_id.( database_name='".DATABASE_NAME."')]", gm("Invalid Id"),$this->database_2);
		$is_ok = $v->run();
		if(!$in['user_type']){
			return false;
		}
		if($is_ok && !$this->add_validate_user($in)){
			$is_ok = false;
		}
		return $is_ok;
	}

	function CompanyInfo(&$in)
	{

		if(!$this->account_main_details_validate($in))
		{
			return false;
		}
		global $config;
		
		$this->db->query("UPDATE settings set value='".$in['ACCOUNT_EMAIL']."' where constant_name='ACCOUNT_EMAIL'");
		$this->db->query("UPDATE settings set value='".$in['ACCOUNT_COMPANY']."' where constant_name='ACCOUNT_COMPANY'");
		$this->db->query("SELECT * FROM customers WHERE is_admin='1'");
		if($this->db->next()){
			$this->db->query("UPDATE customers SET name='".$in['ACCOUNT_COMPANY']."' WHERE is_admin='1' ");
		}else{
			$this->db->query("INSERT INTO customers SET name='".$in['ACCOUNT_COMPANY']."', is_admin='1' ");
		}
		$this->db->query("UPDATE settings set long_value='".$in['ACCOUNT_DESCRIPTION']."' where constant_name='ACCOUNT_DESCRIPTION'");
		$this->db->query("UPDATE account_address SET country_id='".$in['ACCOUNT_DELIVERY_COUNTRY_ID']."',
			                                         zip  ='".$in['ACCOUNT_DELIVERY_ZIP']."',
			                                         city  ='".$in['ACCOUNT_DELIVERY_CITY']."',
			                                         address ='".$in['ACCOUNT_DELIVERY_ADDRESS']."'

			              WHERE is_delivery=1 AND is_default=1");

		//Values for Gaetan account
		if(strpos($config['site_url'],'my.akti')){
				global $database_config;
				$database_new = array(
						'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
						'username' => 'admin',
						'password' => 'hU2Qrk2JE9auQA',
						'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
				);
					
				$db_user_new = new sqldb($database_new);

				$select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$_SESSION['u_id']."' AND field_id='1' ");
				if($select_contact){
					$select_customer = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$select_contact."' AND `primary`='1'");
				}
				
				if($select_customer){
					$customer_name_update = $db_user_new->query("UPDATE customers SET name='".utf8_decode($in['ACCOUNT_COMPANY'])."' WHERE customer_id='".$select_customer."'");
					$address_company = $db_user_new->query("UPDATE customer_addresses SET
												country_id='".$in['ACCOUNT_DELIVERY_COUNTRY_ID']."',
												city='".addslashes($in['ACCOUNT_DELIVERY_CITY'])."',
												address='".addslashes($in['ACCOUNT_DELIVERY_ADDRESS'])."',
												zip='".$in['ACCOUNT_DELIVERY_ZIP']."' WHERE customer_id='".$select_customer."'
												");
				}
			}
		//

		if(!$this->default_values_details($in)){
			return false;
		}

		$this->manage_customer_details($in);
		// $this->set_default_language($in);
		updateUsersInfo();
		if(!msg::$error && !msg::$notice){
			// if(!msg::$error){
			msg::success ( gm("Changes have been saved."),'success');

		}
		$stmp_data_array=array(
    		'MAIL_SETTINGS_PREFERRED_OPTION',
    		'ACCOUNT_SEND_EMAIL'
    	);
		foreach($stmp_data_array as $value){
			$email_data_exist=$this->db->field("SELECT value FROM settings WHERE constant_name='".$value."' ");
			if(is_null($email_data_exist)){
				$this->db->query("INSERT INTO settings SET constant_name='".$value."',`value`='".$in[$value]."' ");
			}else{
				$this->db->query("UPDATE settings SET `value`='".$in[$value]."' WHERE constant_name='".$value."' ");
			}
		}
		return true;

	}
	function account_main_details_validate(&$in)
	{
		$v = new validation($in);
		$v->field('ACCOUNT_DELIVERY_COUNTRY_ID', gm('ID'), "required");
		/*$v->field('ACCOUNT_BILLING_COUNTRY_ID', gm('ID'), "required");*/
		return $v->run();
	}
	function default_values_details(&$in)
	{
		if(!$this->account_setting_update_validate($in))
		{
			return false;
		}

		$this->db->query("update settings set long_value='".$in['ACCOUNT_DELIVERY_ADDRESS']."' where constant_name='ACCOUNT_DELIVERY_ADDRESS'");
		$this->db->query("update settings set value='".$in['ACCOUNT_DELIVERY_ZIP']."' where constant_name='ACCOUNT_DELIVERY_ZIP'");
		$this->db->query("update settings set value='".$in['ACCOUNT_DELIVERY_CITY']."' where constant_name='ACCOUNT_DELIVERY_CITY'");
		$this->db->query("update settings set long_value='".$in['ACCOUNT_DELIVERY_STATE_ID']."', type=2 where constant_name='ACCOUNT_DELIVERY_STATE_ID'");
		$this->db->query("update settings set value='".$in['ACCOUNT_DELIVERY_COUNTRY_ID']."' where constant_name='ACCOUNT_DELIVERY_COUNTRY_ID'");
		if(!$in['INVOICE_ADDRESS']){
			$this->db->query("update settings set long_value='".$in['ACCOUNT_DELIVERY_ADDRESS']."' where constant_name='ACCOUNT_BILLING_ADDRESS'");
			$this->db->query("update settings set value='".$in['ACCOUNT_DELIVERY_ZIP']."' where constant_name='ACCOUNT_BILLING_ZIP'");
			$this->db->query("update settings set value='".$in['ACCOUNT_DELIVERY_CITY']."' where constant_name='ACCOUNT_BILLING_CITY'");
			$this->db->query("update settings set long_value='".$in['ACCOUNT_DELIVERY_STATE_ID']."', type=2  where constant_name='ACCOUNT_BILLING_STATE_ID'");
			$this->db->query("update settings set value='".$in['ACCOUNT_DELIVERY_COUNTRY_ID']."' where constant_name='ACCOUNT_BILLING_COUNTRY_ID'");
		}else{
			$this->db->query("update settings set long_value='".$in['ACCOUNT_BILLING_ADDRESS']."' where constant_name='ACCOUNT_BILLING_ADDRESS'");
			$this->db->query("update settings set value='".$in['ACCOUNT_BILLING_ZIP']."' where constant_name='ACCOUNT_BILLING_ZIP'");
			$this->db->query("update settings set value='".$in['ACCOUNT_BILLING_CITY']."' where constant_name='ACCOUNT_BILLING_CITY'");
			$this->db->query("update settings set long_value='".$in['ACCOUNT_BILLING_STATE_ID']."', type=2  where constant_name='ACCOUNT_BILLING_STATE_ID'");
			$this->db->query("update settings set value='".$in['ACCOUNT_BILLING_COUNTRY_ID']."' where constant_name='ACCOUNT_BILLING_COUNTRY_ID'");
			}

		$this->db->query("update settings set value='".$in['ACCOUNT_VAT_NUMBER']."' where constant_name='ACCOUNT_VAT_NUMBER'");
		$this->db->query("update settings set value='".$in['ACCOUNT_REG_NUMBER']."' where constant_name='ACCOUNT_REG_NUMBER'");
		//fields for specific countries
		$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_SIRET']."' WHERE constant_name='ACCOUNT_SIRET'");
		$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_RPR']."' WHERE constant_name='ACCOUNT_RPR'");
		$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_DEN_SOCIALE']."' WHERE constant_name='ACCOUNT_DEN_SOCIALE'");
		$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_REG_COMMERCE']."' WHERE constant_name='ACCOUNT_REG_COMMERCE'");
		$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_CAP_SOCIAL']."' WHERE constant_name='ACCOUNT_CAP_SOCIAL'");
		// $this->db->query("UPDATE settings SET value='".$in['account_no_matricule']."' WHERE constant_name='ACCOUNT_NO_MATRICULE'");
		$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_CODE_NAF']."' WHERE constant_name='ACCOUNT_CODE_NAF'");
		$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_JURIDICAL_FORM']."' WHERE constant_name='ACCOUNT_JURIDICAL_FORM'");
		/*$account_send_email=$this->db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_SEND_EMAIL' ");
		if(is_null($account_send_email)){
			$this->db->query("INSERT INTO settings SET constant_name='ACCOUNT_SEND_EMAIL', value='".$in['ACCOUNT_SEND_EMAIL']."'");
		}else{
			$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_SEND_EMAIL']."' WHERE constant_name='ACCOUNT_SEND_EMAIL'");
		}*/


		// prepare array with query`s to send on front
		$commands = array();
		array_push($commands, "UPDATE settings set long_value='".$in['ACCOUNT_DELIVERY_ADDRESS']."' WHERE constant_name='ACCOUNT_DELIVERY_ADDRESS'");
		array_push($commands, "UPDATE settings set value='".$in['ACCOUNT_DELIVERY_ZIP']."' WHERE constant_name='ACCOUNT_DELIVERY_ZIP'");
		array_push($commands, "UPDATE settings set value='".$in['ACCOUNT_DELIVERY_CITY']."' WHERE constant_name='ACCOUNT_DELIVERY_CITY'");
		array_push($commands, "UPDATE settings set long_value='".$in['ACCOUNT_DELIVERY_STATE_ID']."', type=2 WHERE constant_name='ACCOUNT_DELIVERY_STATE_ID'");
		array_push($commands, "UPDATE settings set value='".$in['ACCOUNT_DELIVERY_COUNTRY_ID']."' WHERE constant_name='ACCOUNT_DELIVERY_COUNTRY_ID'");
		$packet = array(
			'command' => base64_encode(serialize($commands)),
			'command_id' => 1
		);
		//send array on front
       	Sync::send(WEB_URL, $packet);


		//Bank details
		$this->db->query("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_BANK_NAME' ");
		if($this->db->move_next()){
			$this->db->query("update settings set value='".$in['ACCOUNT_BANK_NAME']."' where constant_name='ACCOUNT_BANK_NAME'");
		}else{
			$this->db->query("INSERT INTO settings SET value='".$in['ACCOUNT_BANK_NAME']."', constant_name='ACCOUNT_BANK_NAME' ");
		}

		$this->db->query("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_BIC_CODE' ");
		if($this->db->move_next()){
			$this->db->query("update settings set value='".$in['ACCOUNT_BIC_CODE']."' where constant_name='ACCOUNT_BIC_CODE' ");
		}else{
			$this->db->query("INSERT INTO settings set value='".$in['ACCOUNT_BIC_CODE']."', constant_name='ACCOUNT_BIC_CODE' ");
		}
		$this->db->query("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_IBAN' ");
		if($this->db->move_next()){
			$this->db->query("update settings set value='".$in['ACCOUNT_IBAN']."' where constant_name='ACCOUNT_IBAN' ");
		}else{
			$this->db->query("INSERT INTO settings set value='".$in['ACCOUNT_IBAN']."', constant_name='ACCOUNT_IBAN' ");
		}
		#account url
		$this->db->query("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_URL' ");
		if($this->db->move_next()){
			$this->db->query("update settings set value='".$in['ACCOUNT_URL']."' where constant_name='ACCOUNT_URL' ");
		}else{
			$this->db->query("INSERT INTO settings set value='".$in['ACCOUNT_URL']."', constant_name='ACCOUNT_URL' ");
		}
		#account phone
		$this->db->query("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_PHONE' ");
		if($this->db->move_next()){
			$this->db->query("update settings set value='".$in['ACCOUNT_PHONE']."' where constant_name='ACCOUNT_PHONE' ");
		}else{
			$this->db->query("INSERT INTO settings set value='".$in['ACCOUNT_PHONE']."', constant_name='ACCOUNT_PHONE' ");
		}
		#account fax
		$this->db->query("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_FAX' ");
		if($this->db->move_next()){
			$this->db->query("update settings set value='".$in['ACCOUNT_FAX']."' where constant_name='ACCOUNT_FAX' ");
		}else{
			$this->db->query("INSERT INTO settings set value='".$in['ACCOUNT_FAX']."', constant_name='ACCOUNT_FAX' ");
		}

//		$in['success'].= gm("Default values has been successfully updated");
		updateUsersInfo();
		return true;
	}
	function manage_customer_details(&$in){
		// btw_nr='".$in['account_vat_number']."',

		$customer_id = $this->db_users->field("SELECT customer_id FROM customers WHERE database_name='".DATABASE_NAME."'");
		/*$this->db_users->query("UPDATE customers SET
										name='".$in['ACCOUNT_COMPANY']."',
										bank_name='".$in['BANK_NAME']."',
										bank_bic_code='".$in['BANK_BIC_CODE']."',
										bank_iban='".$in['BANK_IBAN']."',
										c_email='".$in['ACCOUNT_EMAIL']."'
,										comp_phone='".$in['ACCOUNT_PHONE']."',
										website = '".$in['ACCOUNT_URL']."',
										city_name='".$in['ACCOUNT_BILLING_CITY']."',
										country_name='".get_country_name($in['ACCOUNT_BILLING_COUNTRY_ID'])."',
										comp_fax='".$in['ACCOUNT_FAX']."'
								WHERE customer_id = '".$customer_id."'");*/
		$this->db_users->query("UPDATE customers SET
										name= :name,
										bank_name= :bank_name,
										bank_bic_code= :bank_bic_code,
										bank_iban= :bank_iban,
										c_email= :c_email,
										comp_phone= :comp_phone,
										website = :website,
										city_name= :city_name,
										country_name= :country_name,
										comp_fax= :comp_fax
								WHERE customer_id = :customer_id",
							[			'name'=>$in['ACCOUNT_COMPANY'],
										'bank_name'=>$in['BANK_NAME'],
										'bank_bic_code'=>$in['BANK_BIC_CODE'],
										'bank_iban'=>$in['BANK_IBAN'],
										'c_email'=>$in['ACCOUNT_EMAIL'],
										'comp_phone'=>$in['ACCOUNT_PHONE'],
										'website' => $in['ACCOUNT_URL'],
										'city_name'=>$in['ACCOUNT_BILLING_CITY'],
										'country_name'=>get_country_name($in['ACCOUNT_BILLING_COUNTRY_ID']),
										'comp_fax'=>$in['ACCOUNT_FAX'],
										'customer_id' => $customer_id]
						);
		$delivery_add = $this->db_users->field("SELECT address_id FROM customer_addresses WHERE customer_id='".$customer_id."' AND delivery=1");
		if ($delivery_add)
		{
			/*$this->db_users->query("UPDATE customer_addresses SET
											country_id='".$in['ACCOUNT_DELIVERY_COUNTRY_ID']."',
											city='".$in['ACCOUNT_DELIVERY_CITY']."',
											zip='".$in['ACCOUNT_DELIVERY_ZIP']."',
											address='".$in['ACCOUNT_DELIVERY_ADDRESS']."'
									WHERE address_id='".$delivery_add."'");*/
			$this->db_users->query("UPDATE customer_addresses SET
											country_id= :country_id,
											city= :city,
											zip= :zip,
											address= :address
									WHERE address_id= :address_id",
								['country_id'=>$in['ACCOUNT_DELIVERY_COUNTRY_ID'],
								 'city'=>$in['ACCOUNT_DELIVERY_CITY'],
								 'zip'=>$in['ACCOUNT_DELIVERY_ZIP'],
								 'address'=>$in['ACCOUNT_DELIVERY_ADDRESS'],
								 'address_id'=>$delivery_add]
							);									
		}else
		{
			/*$this->db_users->query("INSERT INTO customer_addresses SET
											customer_id='".$customer_id."',
											country_id='".$in['ACCOUNT_DELIVERY_COUNTRY_ID']."',
											city='".$in['ACCOUNT_DELIVERY_CITY']."',
											zip='".$in['ACCOUNT_DELIVERY_ZIP']."',
											delivery='1',
											address='".$in['ACCOUNT_DELIVERY_ADDRESS']."'");*/
			$this->db_users->insert("INSERT INTO customer_addresses SET
											customer_id= :customer_id,
											country_id= :country_id,
											city= :city,
											zip= :zip,
											delivery= :delivery,
											address= :address",
										['customer_id'=>$customer_id,
										 'country_id'=>$in['ACCOUNT_DELIVERY_COUNTRY_ID'],
										 'city'=>$in['ACCOUNT_DELIVERY_CITY'],
										 'zip'=>$in['ACCOUNT_DELIVERY_ZIP'],
										 'delivery'=>'1',
										 'address'=>$in['ACCOUNT_DELIVERY_ADDRESS']]
									);
		}
		$billing_add = $this->db_users->field("SELECT address_id FROM customer_addresses WHERE customer_id='".$customer_id."' AND billing=1");
		if ($billing_add)
		{
			/*$this->db_users->query("UPDATE customer_addresses SET
											country_id='".$in['ACCOUNT_BILLING_COUNTRY_ID']."',
											city='".$in['ACCOUNT_BILLING_CITY']."',
											zip='".$in['ACCOUNT_BILLING_ZIP']."',
											address='".$in['ACCOUNT_BILLING_ADDRESS']."'
									WHERE address_id='".$billing_add."'");*/
			$this->db_users->query("UPDATE customer_addresses SET
											country_id= :country_id,
											city= :city,
											zip= :zip,
											address= :address
									WHERE address_id= :address_id",
								['country_id'=>$in['ACCOUNT_BILLING_COUNTRY_ID'],
								 'city'=>$in['ACCOUNT_BILLING_CITY'],
								 'zip'=>$in['ACCOUNT_BILLING_ZIP'],
								 'address'=>$in['ACCOUNT_BILLING_ADDRESS'],
								 'address_id'=>$billing_add]
							);									
		}else
		{
			/*$this->db_users->query("INSERT INTO customer_addresses SET
											customer_id='".$customer_id."',
											country_id='".$in['ACCOUNT_BILLING_COUNTRY_ID']."',
											city='".$in['ACCOUNT_BILLING_CITY']."',
											zip='".$in['ACCOUNT_BILLING_ZIP']."',
											billing='1',
											is_primary='1',
											address='".$in['ACCOUNT_BILLING_ADDRESS']."'");*/
			$this->db_users->insert("INSERT INTO customer_addresses SET
											customer_id= :customer_id,
											country_id= :country_id,
											city= :city,
											zip= :zip,
											billing= :billing,
											is_primary= :is_primary,
											address= :address",
										[	'customer_id'=>$customer_id,
											'country_id'=>$in['ACCOUNT_BILLING_COUNTRY_ID'],
											'city'=>$in['ACCOUNT_BILLING_CITY'],
											'zip'=>$in['ACCOUNT_BILLING_ZIP'],
											'billing'=>'1',
											'is_primary'=>'1',
											'address'=>$in['ACCOUNT_BILLING_ADDRESS']]
									);											
		}
		$contacts = $this->db_users->query("SELECT contact_id FROM customer_contacts WHERE customer_id='".$customer_id."'");
		while($contacts->next())
		{
			//$this->db_users->query("UPDATE customer_contacts SET company_name='".$in['ACCOUNT_COMPANY']."', phone='".$in['ACCOUNT_PHONE']."' WHERE contact_id='".$contacts->f('contact_id')."'");
			$this->db_users->query("UPDATE customer_contacts SET company_name= :company_name, phone= :phone WHERE contact_id=:contact_id",['company_name'=>$in['ACCOUNT_COMPANY'],'phone'=>$in['ACCOUNT_PHONE'],'contact_id'=>$contacts->f('contact_id')]);
		}
		return true;
	}
	function Regional(&$in)
	{
		if(!$this->account_setting_update_validate($in))
		{
			return false;
		}
		if($in['CHECK_FORMAT'] == '1'){
			$check_format=',.';
		}else if($in['CHECK_FORMAT'] == '2'){
			$check_format='.,';
		}else{
			$check_format=' ,';
		}
		$cal_first_week_id = $in['CAL_FIRST_WEEK_ID'];
		if($in['CAL_FIRST_WEEK_ID'] =='7'){
			$cal_first_week_id ='0';
		}
	    $this->db->query("UPDATE settings SET value = '".$in['CURRENCY_TYPE_ID']."' WHERE constant_name='ACCOUNT_CURRENCY_TYPE' ");
	    $this->db->query("UPDATE settings SET value = '".$in['CHECK_CUR1']."' WHERE constant_name='ACCOUNT_CURRENCY_FORMAT' ");
	    $this->db->query("UPDATE settings SET value = '".$check_format."' WHERE constant_name='ACCOUNT_NUMBER_FORMAT' ");
	    $this->db->query("UPDATE settings SET value = '".$in['TIME_ZONE_ID']."' WHERE constant_name='ACCOUNT_TIME_ZONE' ");
	    $this->db->query("UPDATE settings SET value = '".$in['DATE_FORMAT_ID']."' WHERE constant_name='ACCOUNT_DATE_FORMAT' ");
	    $this->db->query("UPDATE settings SET value = '".$cal_first_week_id."' WHERE constant_name='CALENDAR_FIRST_WEEK_DAY' ");

		$acc_time_zone = $in['account_timezone']*60*60;
		$_SESSION['user_timezone_offset'] = $acc_time_zone-7200;
		updateUsersInfo();
		msg::success ( gm("Changes have been saved."),'success');
	}
	function account_setting_update_validate(&$in)
	{
		$is_ok = true;

		return $is_ok;
	}

	function add_validate(&$in)
	{
		$v = new validation($in);
		$v->field('VALUE',gm('Value'),'required:numeric:unique[vats.value]');
		if($in['act'] = 'vat-update'){
			$v->field('value',gm('Value'),"required:numeric:unique[vats.value.( vat_id!='".$in['ID']."')]");
		}
		return $v->run();
	}

	function add_validate_user(&$in)
	{
		//$user_role = $this->db_users->field("SELECT main_user_id FROM users WHERE user_id='".$in['user_id']."' ");
		$user_role = $this->db_users->field("SELECT main_user_id FROM users WHERE user_id= :user_id ",['user_id'=>$in['user_id']]);
		if($user_role == 0 && $in['do'] == 'settings-user-user-update' ){
			$in['group_id']	 = 1;
		}
		$v = new validation($in);
		$is_ok = true;
		$v->field('first_name',gm('First Name'),'required');
		$v->field('last_name',gm('Last Name'),'required');
		$v->field('email',gm('Email'),'required:email');
		$v->field('lang_id',gm('Language'),'required',"Please select a 'Language'");
		if($in['act']=='user-add'){
			$v->field('username', gm('Username'), 'required:min_length[6]:unique[users.username]',false,$this->database_2);
		}else{
			$v->field('username', gm("Username"), "required:min_length[6]:unique[users.username.( user_id!='".$in['user_id']."')]",false,$this->database_2);
		}
/*		if($in['do'] != 'settings-user-user-update_account' ){
			$v->field('group_id',gm("Group"),'required',"Please select a group.");
		}*/
		if($in['do']=='settings-user-user-add'){
				$v->field('password',gm("Password"),'required');
		}
		$v->field('password',gm("Password"),'strange_chars:min_length[6]');
		if($in['do']=='settings-user-user-add'){
				$v->field('password1',gm("Confirm Password"),'required');
		}
		$v->field('password1',gm("Confirm Password"),'strange_chars:min_length[6]:match[password]');
		$is_ok = $v->run();

		$trial = $this->db_users->field("SELECT is_trial FROM user_info WHERE user_id=(SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' AND u_type !=1 limit 1) ");
		$in['is_trial'] = $trial;
		if($trial == 1 && ark::$method != 'update_user'){
			switch (true) {
				case (defined('NEW_SUBSCRIPTION') && NEW_SUBSCRIPTION==1):
					if(!$this->check_add_user3($in)){
						return false;
					}
					break;
				
				default:
					if(!$this->check_add_user2($in)){
						return false;
					}
					break;

				/*case defined('PAYMILL_CLIENT_ID'):
					if(!$this->check_add_user2($in)){
						return false;
					}
					break;
				default:
					if(!$this->check_add_user($in)){
						return false;
					}
					break;*/
			}
		}
		return $is_ok;
	}

	function check_add_user3(&$in){
		$user_p = $this->db_users->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' AND ( user_meta.value !=  '1' OR user_meta.value IS NULL ) ");
		$user_total = $this->db_users->field("SELECT users FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' ");
		if($user_p>=$user_total){
			msg::notice(gm("You cannot add more users. <br>Please change your subscription too add more users."),"notice");
			return false;
		}
		return true;
	}
	function vat_change(&$in){

			if(!$this->update_validate($in))
			{
				return false;
			}
			if($in['DEFAULT']=="true"){
				$this->db->query("UPDATE settings SET value='".return_value($in['VALUE'])."' WHERE constant_name='ACCOUNT_VAT' ");
				updateUsersInfo();
			}
			$this->db->query("UPDATE vats SET value= '".return_value($in['VALUE'])."'
							  WHERE vat_id='".$in['ID']."' ");

			#update total_price for articles that have modified vat value
			$article_ids = $this->db->query("	SELECT 		pim_articles.article_id, pim_article_prices.price
												FROM  		pim_articles
												INNER JOIN 	pim_article_prices
												ON 			pim_articles.article_id=pim_article_prices.article_id
												WHERE 		pim_articles.vat_id = '".$in['ID']."' ");
			while($article_ids->next()){
				$price = $article_ids->f('price');
				$vat_value = $price*$in['VALUE']/100;
				$total_price = $price+$vat_value;
				$this->db->query("	UPDATE 	pim_article_prices
									SET 	total_price 		= '".$total_price."'
									WHERE 	article_id 			= '".$article_ids->f('article_id')."'
									AND 	price 				= '".$price."' ");
				update_articles($article_ids->f('article_id'),DATABASE_NAME);
			}
		    msg::success ( gm("Changes have been saved."),'success');
		    return true;


  	}
	function vat_changenew(&$in){
			if(!$this->addlabelnew_validate($in)){
	  			json_out($in);
	  		}		
			$this->db->query("UPDATE vat_new SET
				name_id='".addslashes($in['NAME_ID'])."',
				description='".addslashes($in['DESCRIPTION'])."',
				vat_id='".$in['vat_id']."',
				no_vat='".$in['NO_VAT_PLUS']."',
				regime_type='".$in['regime_type_id']."'
				WHERE id='".$in['ID']."'");	
		    msg::success ( gm("Changes have been saved."),'success');
		    return true;
  	}
  	function update_validate(&$in)
	{
			$is_ok=true;
			$v = new validation($in);
			$v->field('ID', gm('ID'), 'required:exist[vats.vat_id]', "[!L!]Invalid Id[!/L!]");
			$is_ok = $v->run();
/*			if (!$this->add_validate($in))
			{
				$is_ok = false;
			}*/
			return $is_ok;
	}
	function updatenew_validate(&$in)
	{
			$is_ok=true;
			$v = new validation($in);
			//$v->field('ID', gm('ID'), 'required:exist[vats.vat_id]', "[!L!]Invalid Id[!/L!]");
			$v->field('ID', gm('ID'), 'required:exist[vat_new.id]', "[!L!]Invalid Id[!/L!]");
			$is_ok = $v->run();
/*			if (!$this->add_validate($in))
			{
				$is_ok = false;
			}*/
			return $is_ok;
	}
	function deletevat(&$in){

  		$exist_vat = $this->db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_VAT'");

  		if($exist_vat==$in['VALUE']){
  			msg::error ( gm('VAT is set as default '),'error');
  			return false;
  		}else{
			$this->db->query("DELETE FROM vats WHERE vat_id='".$in['ID']."' ");
	    	msg::success ( gm("Changes have been saved."),'success');
	    	return true;
		}
  	}
  	function deletevatnew(&$in){

  		$is_default=$this->db->field("SELECT `default` FROM vat_new WHERE id='".$in['ID']."' ");
  		if($is_default){
  			msg::error ( gm('VAT Regime is set as default '),'error');
  			json_out($in);
  		}else{
  			/*$old_vat=$this->db->field("SELECT vat_id FROM vat_new WHERE id='".$in['ID']."'");
  			if($old_vat){
  				$this->db->query("DELETE FROM vats WHERE vat_id='".$old_vat."' ");
  			}*/
  			$this->db->query("DELETE FROM vat_new WHERE id='".$in['ID']."' ");
	    	msg::success ( gm("Changes have been saved."),'success');
	    	return true;
  		}
  	}
  	function defaultvat(&$in){
		$this->db->query("UPDATE settings SET value='".$in['VALUE']."' WHERE constant_name='ACCOUNT_VAT'");    
    	msg::success ( gm("Changes have been saved."),'success');
    	return true;
  	}
  	function defaultvatnew(&$in){
		$no_vat=$this->db->field("SELECT no_vat FROM vat_new WHERE id='".$in['id']."' ");
		if($no_vat){
			$value='';
		}else{
			$value=$in['VALUE'];
		}
		$this->db->query("UPDATE vat_new SET `default`='0' ");
		$this->db->query("UPDATE vat_new SET `default`='1' WHERE id='".$in['id']."' ");
    	msg::success ( gm("Changes have been saved."),'success');
    	return true;
  	}
  	function novat(&$in){
		if($in['value']){
			$vat_id='0';
		}else{
			$vat_id=$in['value'];
		}
		$this->db->query("UPDATE vat_new SET `no_vat`='".$in['value']."',vat_id='".$vat_id."' WHERE id='".$in['id']."' ");
    	msg::success ( gm("Changes have been saved."),'success');
    	return true;
  	}

  	function addlabel(&$in){
  		$this->db->insert("INSERT INTO vats SET value='".return_value($in['VALUE'])."'");
  		msg::success ( gm("Changes have been saved."),'success');
    	return true;
  	}
  	function addlabelnew(&$in){
  		if(!$this->addlabelnew_validate($in)){
  			json_out($in);
  		}
  		$this->db->query("INSERT INTO vat_new SET
				name_id='".addslashes($in['NAME_ID'])."',
				description='".addslashes($in['DESCRIPTION'])."',
				vat_id='".$in['vat_id']."',
				no_vat='".$in['NO_VAT_PLUS']."',
				regime_type='".$in['regime_type_id']."' ");	
  		msg::success ( gm("Changes have been saved."),'success');
    	return true;
  	}
  	function addlabelnew_validate(&$in){
  		$v=new validation($in);
  		$v->field('NAME_ID',gm('Id'),'required');
  		$v->field('regime_type_id',gm('Vat Regime'),'required');
  		if(!$in['NO_VAT_PLUS']){
  			$v->field('vat_id',gm('Vat Id'),'required');
  		}	
  		return $v->run();
  	}


	function activate_lang(&$in){
		$this->db->query("UPDATE pim_lang SET pdf_default=0");
		$this->db->query("UPDATE pim_lang SET pdf_default=1,  sort_order='1', active='1' WHERE lang_id='".$in['VALUE']."' ");
		/*$old_def = $this->db->query("SELECT * FROM pim_lang WHERE sort_order='1'");
		$old_def->next();
		$new_def = $this->db->query("SELECT * FROM pim_lang WHERE lang_id='".$in['VALUE']."'");
		$new_def->next();
		$this->db->query("UPDATE pim_lang SET sort_order='".$new_def->f('sort_order')."' WHERE lang_id='".$old_def->f('lang_id')."'");*/
		/*$this->db->query("UPDATE pim_lang SET sort_order='1', active='1' WHERE lang_id='".$in['VALUE']."'");*/
		$langs = $this->db->query("SELECT * FROM pim_lang WHERE pdf_default=0 ORDER BY lang_id");
		$i=2;
		while ($langs->next()) {
			$this->db->query("UPDATE pim_lang SET  sort_order='".$i."' WHERE lang_id='".$langs->f("lang_id")."' ");
			$i++;
		}

		msg::success ( gm("Changes have been saved."),'success');

		return true;
	}

	function change_language_name(&$in){		
		$i=2;
		// $in['sort'] = rtrim($in['sort'],",");
		// $in['sort'] = explode(",", $in['sort']);
		foreach ($in['sort'] as $key=>$val ){
			$this->db->query("UPDATE pim_lang SET sort_order='".$i."', active='1' WHERE lang_id='".$val['ID']."' ");
			$i++;
		}
		$j=1;
		// $in['sort_custom'] = rtrim($in['sort_custom'],",");
		// $in['sort_custom'] = explode(",", $in['sort_custom']);
		foreach ($in['sort_custom'] as $key=>$val ){
			$this->db->query("UPDATE pim_custom_lang SET sort_order='".$j."', active='1' WHERE lang_id='".$val."' ");
			$j++;
		}
		$in['sort_custom_Extra'] = rtrim($in['sort_custom_Extra'],",");
		$in['sort_custom_Extra'] = explode(",", $in['sort_custom_Extra']);
		$k = 1;
		foreach ($in['sort_custom_Extra'] as $key => $value) {
			$this->db->query("UPDATE pim_custom_lang SET sort_order = '".$k."', active='1' WHERE lang_id = '".$value."' ");
			$k++;
		}

		msg::$success.= gm("Changes have been saved.");
		return true;
	}

	function deactivate_lang(&$in){
		if(!is_numeric($in['ID'])){
			msg::error ( gm('Invalid ID'),'error');
			return false;
		}

		$this->db->query("UPDATE pim_lang SET active='0' WHERE lang_id='".$in['ID']."' ");

		msg::success ( gm("Changes have been saved."),'success');

		return true;
	}
	function activate_language(&$in){
	
		$this->db->query("UPDATE pim_lang SET active='1' WHERE lang_id='".$in['ID']."' ");

		msg::success ( gm("Changes have been saved."),'success');

		return true;
	}
	function deactivate_custom_lang(&$in){
		if(!is_numeric($in['ID'])){
			msg::error ( gm('Invalid ID'),'success');
			return false;
		}

		$this->db->query("UPDATE pim_custom_lang SET active='0' WHERE lang_id='".$in['ID']."' ");

		msg::success ( gm("Changes have been saved."),'success');

		return true;
	}
	function activate_lang_custom(&$in){
		$this->db->query("UPDATE pim_custom_lang SET active='1' WHERE lang_id='".$in['ID']."' ");

		msg::success ( gm("Changes have been saved."),'success');

		return true;
	}
	function add_lang_custom(&$in){
		if($in['NAME_CUSTOM'])
		{
			$custom_lang_id = $this->db->insert("INSERT INTO pim_custom_lang SET language='".$in['NAME_CUSTOM']."', active='1'");
			$this->db->query("UPDATE pim_custom_lang SET code='".$custom_lang_id."' WHERE lang_id='".$custom_lang_id."'");
			$this->db->query("INSERT INTO label_language SET name='".$in['NAME_CUSTOM']."', active='1', lang_code='".$custom_lang_id."'");
			$this->db->query("INSERT INTO label_language_quote SET name='".$in['NAME_CUSTOM']."', active='1', lang_code='".$custom_lang_id."'");
			$this->db->query("INSERT INTO label_language_order SET name='".$in['NAME_CUSTOM']."', active='1', lang_code='".$custom_lang_id."'");
			$this->db->query("INSERT INTO label_language_time SET name='".$in['NAME_CUSTOM']."', active='1', lang_code='".$custom_lang_id."'");
			$this->db->query("INSERT INTO label_language_p_order SET name='".$in['NAME_CUSTOM']."', active='1', lang_code='".$custom_lang_id."'");
		}

		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function edit_lang_custom(&$in)
	{
		if($in['NAME_CUSTOM'])
		{
			$this->db->query("UPDATE pim_custom_lang SET language='".$in['NAME_CUSTOM']."', code='".$in['ID_CUSTOM']."' WHERE lang_id='".$in['ID_CUSTOM']."'");
			$this->db->query("UPDATE label_language SET name='".$in['NAME_CUSTOM']."' WHERE lang_code='".$in['ID_CUSTOM']."'");
			$this->db->query("UPDATE label_language_quote SET name='".$in['NAME_CUSTOM']."' WHERE lang_code='".$in['ID_CUSTOM']."'");
			$this->db->query("UPDATE label_language_order SET name='".$in['NAME_CUSTOM']."' WHERE lang_code='".$in['ID_CUSTOM']."'");
			$this->db->query("UPDATE label_language_time SET name='".$in['NAME_CUSTOM']."' WHERE lang_code='".$in['ID_CUSTOM']."'");
			$this->db->query("UPDATE label_language_p_order SET name='".$in['NAME_CUSTOM']."' WHERE lang_code='".$in['ID_CUSTOM']."'");
		}

		msg::success ( gm("Changes have been saved."),'success');

		return true;
	}
	function delete_lang_custom(&$in)
	{
		$v=new validation($in);
		$v->field('lang_id',"Id","required:exist[pim_custom_lang.lang_id.( code='".$in['lang_id']."')]");
		if(!$v->run()){
			json_out($in);
		}
		$this->db->query("DELETE FROM pim_custom_lang WHERE lang_id='".$in['lang_id']."' ");
		$this->db->query("DELETE FROM label_language WHERE lang_code='".$in['lang_id']."'");
		$this->db->query("DELETE FROM label_language_quote WHERE lang_code='".$in['lang_id']."'");
		$this->db->query("DELETE FROM label_language_order WHERE lang_code='".$in['lang_id']."'");
		$this->db->query("DELETE FROM label_language_time WHERE lang_code='".$in['lang_id']."'");
		$this->db->query("DELETE FROM label_language_p_order WHERE lang_code='".$in['lang_id']."'");

		msg::success ( gm("Changes have been saved."),'success');

		return true;
	}
	function change_main_user(&$in){
		if(!$this->change_main_user_validate($in)){
			return false;
		}		

		$stripe_user=$this->db_users->query("SELECT * FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' ");

		//no need to update the already main user
		if($in['user_id'] == $stripe_user->f('user_id')){
			return false;
		}

		$next_date = $this->db_users->field("SELECT next_date FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' ");
		//$this->db_users->query("UPDATE users SET  default_admin='0', main_user_id='".$in['user_id']."', next_date='' WHERE database_name='".DATABASE_NAME."' ");
		$this->db_users->query("UPDATE users SET  default_admin= :default_admin, main_user_id= :main_user_id, next_date= :next_date WHERE database_name= :database_name ",['default_admin'=>'0','main_user_id'=>$in['user_id'],'next_date'=>'','database_name'=>DATABASE_NAME]);
		$group_id = $this->db->query("SELECT group_id, credentials FROM groups WHERE name='Administrator' ");
		$group_id->next();

		//select credentials from main user of the account
		$credentials=$stripe_user->f('credentials');

		/*$this->db_users->query("UPDATE users SET
									   main_user_id 	= '0',
									   user_role		= '1',
									   group_id			= '".$group_id->f('group_id')."',
									   credentials		= '".$group_id->f('credentials')."',
									   default_admin 	= '1',
									   next_date		= '".$next_date."',
									   stripe_cust_id		='".$stripe_user->f('stripe_cust_id')."',
									   stripe_subscr_id		='".$stripe_user->f('stripe_subscr_id')."',
									   user_type		= '3',
									   accountant_id		='".$stripe_user->f('accountant_id')."'
								WHERE user_id='".$in['user_id']."' ");*/
		$this->db_users->query("UPDATE users SET
									   main_user_id 	= :main_user_id,
									   user_role		= :user_role,
									   group_id			= :group_id,
									   credentials  	= :credentials,
									   default_admin 	= :default_admin,
									   next_date		= :next_date,
									   stripe_cust_id	= :stripe_cust_id,
									   stripe_subscr_id	= :stripe_subscr_id,
									   user_type		= :user_type,
									   accountant_id	= :accountant_id,
									   pricing_plan_id	= :pricing_plan_id,
									   base_plan_id		= :base_plan_id,
									   users 			= :users,
									   ACCOUNT_BILLING_COUNTRY_ID = :ACCOUNT_BILLING_COUNTRY_ID,
									   valid_vat 		= :valid_vat,
									   time_users 		= :time_users,
									   payed 			= :payed,
									   plan 			= :plan,
									   last_payment 	= :last_payment,
									   ACCOUNT_COMPANY 	= :ACCOUNT_COMPANY,
									   user_amount 		= :user_amount,
									   user_time_amount = :user_time_amount
								WHERE user_id= :user_id ",
							[		   'main_user_id' 	=> '0',
									   'user_role'		=> '1',
									   'group_id'		=> $group_id->f('group_id'),
									   'credentials'	=> $credentials,
									   'default_admin' 	=> '1',
									   'next_date'		=> $next_date,
									   'stripe_cust_id'	=> $stripe_user->f('stripe_cust_id'),
									   'stripe_subscr_id'	=> $stripe_user->f('stripe_subscr_id'),
									   'user_type'		=> '3',
									   'accountant_id'	=> $stripe_user->f('accountant_id'),
									   'pricing_plan_id'	=> $stripe_user->f('pricing_plan_id'),
									   'base_plan_id'		=> $stripe_user->f('base_plan_id'),
									   'users'			=> $stripe_user->f('users'),
									   'ACCOUNT_BILLING_COUNTRY_ID'	=>$stripe_user->f('ACCOUNT_BILLING_COUNTRY_ID'),
									   'valid_vat'			=> $stripe_user->f('valid_vat'),
									   'time_users'		=> $stripe_user->f('time_users'),
									   'payed'			=> $stripe_user->f('payed'),
									   'plan'			=> $stripe_user->f('plan'),
									   'last_payment'	=> $stripe_user->f('last_payment'),
									   'ACCOUNT_COMPANY'	=>$stripe_user->f('ACCOUNT_COMPANY'),
									   'user_amount'		=> $stripe_user->f('user_amount'),
									   'user_time_amount'	=> $stripe_user->f('user_time_amount'),
									   'user_id'		=> $in['user_id']]
						);
		//$user = $this->db_users->query("SELECT last_name, first_name,email FROM users WHERE user_id='".$in['user_id']."'");
		$user = $this->db_users->query("SELECT last_name, first_name,email FROM users WHERE user_id= :user_id",['user_id'=>$in['user_id']]);
		$user->next();
		$this->db->query("UPDATE settings SET value='".$user->f('email')."' WHERE constant_name='ACCOUNT_EMAIL' ");
		$this->manage_contacts($in);

		if(strpos($config['site_url'],'my.akti')){
			global $database_config;
			$database_new = array(
					'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
					'username' => 'admin',
					'password' => 'hU2Qrk2JE9auQA',
					'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
			);
				
			$db_user_new = new sqldb($database_new);

			$select_customer = $db_user_new->field("SELECT customer_id FROM customer_field WHERE value='".$stripe_user->f('user_id')."' AND field_id='7' ");	
			if($select_customer){
				$db_user_new->query("UPDATE customer_field SET value='".$in['user_id']."' WHERE customer_id='".$select_customer."' AND field_id='7' ");
			}			
			
		}
		msg::success ( utf8_encode($user->f('last_name')).' - '.utf8_encode($user->f('first_name'))." ".gm("has been set as Administrator"),'success');
		return true;
	}
	function change_main_user_validate(&$in){
		$is_ok = true;


		//$user_id = $this->db_users->query("SELECT user_id,group_id FROM users WHERE user_id='".$in['user_id']."' ");
		$user_id = $this->db_users->query("SELECT user_id,group_id FROM users WHERE user_id= :user_id AND database_name= :db_name",['user_id'=>$in['user_id'],'db_name'=>DATABASE_NAME]);
		if(!$user_id->next()){
			msg::$error .= gm("Invalid Id");
			$is_ok = false;
		}else{
			if($user_id->f('group_id') != 1){
				msg::$error .= gm("Only users from the Administrator group can be set as Administrator")."<br>";
				$is_ok = false;
			}
		}
		//$user_meta = $this->db_users->field("SELECT name FROM user_meta WHERE user_id='".$in['user_id']."' AND name='active' ");
		$user_meta = $this->db_users->field("SELECT name FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'active']);
		if($user_meta=='active'){
			msg::error ( gm("User is inactive"),'error');
			$is_ok = false;
		}

		$group_id = $this->db->field("SELECT group_id FROM groups WHERE name='Administrator' ");
		//$u_group_id = $this->db_users->field("SELECT group_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$u_group_id = $this->db_users->field("SELECT group_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		if($u_group_id != $group_id ){
			msg::$error .= gm("You don't have enough privileges");
			$is_ok = false;
		}

		return $is_ok;
	}
	function manage_contacts(&$in)
	{

		//$contact_id = $this->db_users->field("SELECT contact_id FROM customer_contacts WHERE table_user_id='".$in['user_id']."' AND table_user_id!='0'");
		$contact_id = $this->db_users->field("SELECT contact_id FROM customer_contacts WHERE table_user_id= :t AND table_user_id!= :tb",['t'=>$in['user_id'],'tb'=>'0']);
		$customer_id= $this->db_users->field("SELECT customer_id FROM customers WHERE name='".addslashes(ACCOUNT_COMPANY)."'");
		//$user_info = $this->db_users->query("SELECT * FROM users WHERE user_id='".$in['user_id']."'");
		$user_info = $this->db_users->query("SELECT * FROM users WHERE user_id= :user_id",['user_id'=>$in['user_id']]);
		$user_info->next();
		if($contact_id)
		{
			$this->db_users->query("UPDATE customer_contacts SET
											username='".addslashes($user_info->f('username'))."',
											firstname='".addslashes($user_info->f('first_name'))."',
											lastname='".addslashes($user_info->f('last_name'))."',
											language='".$user_info->f('lang_id')."',
											email='".$user_info->f('email')."',
											company_name='".addslashes(ACCOUNT_COMPANY)."'
									WHERE contact_id='".$contact_id."'");
		}else
		{
			$customer_id = $this->db_users->field("SELECT customer_id FROM customers WHERE name='".addslashes(ACCOUNT_COMPANY)."'");
			/*$this->db_users->query("INSERT INTO customer_contacts SET
													customer_id='".$customer_id."',
													username='".$user_info->f('username')."',
													firstname='".addslashes($user_info->f('first_name'))."',
													lastname='".addslashes($user_info->f('last_name'))."',
													email='".$user_info->f('email')."',
													s_email 	=	'1',
													language='".$user_info->f('lang_id')."',
													company_name='".addslashes(ACCOUNT_COMPANY)."',
													phone='".addslashes(ACCOUNT_PHONE)."',
													table_user_id='".$in['user_id']."'");*/
			$this->db_users->insert("INSERT INTO customer_contacts SET
													customer_id= :customer_id,
													username= :username,
													firstname= :firstname,
													lastname= :lastname,
													email= :email,
													s_email 	=	:s_email,
													language= :language,
													company_name= :company_name,
													phone= :phone,
													table_user_id= :table_user_id",
												[	'customer_id'=>$customer_id,
													'username'=>$user_info->f('username'),
													'firstname'=>addslashes($user_info->f('first_name')),
													'lastname'=>addslashes($user_info->f('last_name')),
													'email'=>$user_info->f('email'),
													's_email' =>'1',
													'language'=>$user_info->f('lang_id'),
													'company_name'=>addslashes(ACCOUNT_COMPANY),
													'phone'=>addslashes(ACCOUNT_PHONE),
													'table_user_id'=>$in['user_id']]
											);													
		}
		$this->db_users->query("UPDATE customers SET c_email='".$user_info->f('email')."'");
		return true;
	}
	function deactivate(&$in){
		if(!$this->delete_validate($in)){
			return false;
		}

		//$this->db_users->query("UPDATE users SET active='0' WHERE user_id='".$in['user_id']."' ");
		$this->db_users->query("UPDATE users SET active= :active WHERE user_id= :user_id ",['active'=>'0','user_id'=>$in['user_id']]);
		msg::success ( gm("User archived"),'success');
		return true;
	}
	function delete_validate(&$in)
	{
		$v = new validation($in);
		$v->field('user_id', gm('ID'), "required:exist[users.user_id.( database_name='".DATABASE_NAME."' AND main_user_id!=0)]", gm("Invalid ID"),$this->database_2);

		return $v->run();
	}
	function deactivate2(&$in){
		$v = new validation($in);
		$v->field('user_id', gm('ID'), "required:exist[users.user_id.( database_name='".DATABASE_NAME."' AND main_user_id!=0)]", gm("Invalid ID"),$this->database_2);
		if(!$v->run()){
			return false;
		}
		//$this->db_users->query("DELETE FROM user_meta WHERE user_id='".$in['user_id']."' AND name='active' ");
		$this->db_users->query("DELETE FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'active']);
		//$this->db_users->query("INSERT INTO user_meta SET name='active', value='1', user_id='".$in['user_id']."' ");
		$this->db_users->insert("INSERT INTO user_meta SET name= :name, value= :value, user_id= :user_id ",['name'=>'active','value'=>'1','user_id'=>$in['user_id']]);
		//$this->db_users->query("UPDATE user_activity SET expired='1' WHERE user_id='".$in['user_id']."' ");
		$this->db_users->query("UPDATE user_activity SET expired= :expired WHERE user_id= :user_id ",['expired'=>'1','user_id'=>$in['user_id']]);
		msg::success ( gm("User deactivated"),'success');
		return true;
	}
	function activate(&$in){
		if(!$this->validate_activate($in)){
			return false;
		}

		//$this->db_users->query("UPDATE users SET active='1' WHERE user_id='".$in['user_id']."' ");
		$this->db_users->query("UPDATE users SET active= :active WHERE user_id= :user_id ",['active'=>'1','user_id'=>$in['user_id']]);
		msg::success ( gm("User activated"),'success');
		return true;
	}
	function ressend(&$in){
		$has_licences=true;
		if(!$this->checkLicense($in)){
			$has_licences=false;
		}
		if(!$has_licences){
			json_out($in);
		}
		//global $config
		$in['time'] =mktime();
		$timestamp = strtotime("+48 hours",$in['time']);

		//$this->db_users->query("UPDATE user_email SET time_email='".$timestamp."' WHERE crypt='".$in['crypt']."' ");
		$this->db_users->query("UPDATE user_email SET time_email= :time_email WHERE crypt= :crypt ",['time_email'=>$timestamp,'crypt'=>$in['crypt']]);

			//$name_user = $this->db_users->query("SELECT first_name,last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
			$name_user = $this->db_users->query("SELECT first_name,last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
			//$in['lang_id'] = $this->db_users->field("SELECT lang_id FROM users WHERE user_id='".$_SESSION['u_id']."'  ");
			$in['lang_id'] = $this->db_users->field("SELECT lang_id FROM users WHERE user_id= :user_id  ",['user_id'=>$_SESSION['u_id']]);
			$lang_code=$_SESSION['l'];
			$mail = new PHPMailer();
			$mail->WordWrap = 50;
		  	$fromMail='noreply@akti.com';
	 	 	$mail->SetFrom($fromMail, 'Akti');	

		 	$forgot_email = include(ark::$controllerpath . 'multi_email1.php');
			$body=$forgot_email;
			$body=str_replace(',[!USERNAME!]',"",$body);
         	$body=str_replace('[!FIRSTNAME!]',"".$name_user->f('first_name')."",$body);
	        $body=str_replace('[!LASTNAME!]',"".$name_user->f('last_name')."",$body);
            $body=str_replace('[!LINK_VALIDATE!]',"<a href='".$site."'>'".$site."'</a>",$body);
			$mail->Subject = $mail_subject;
		 	$mail->MsgHTML($body);
		  	$mail->AddAddress($in['email']);
		 	$mail->Send();
		msg::success ( gm("Email has been sent."),'success');
		return true;
	}

	function cancelInvitation(&$in){
		//$this->db_users->query("DELETE FROM user_email WHERE crypt='".$in['crypt']."'");
		$this->db_users->query("DELETE FROM user_email WHERE crypt= :crypt",['crypt'=>$in['crypt']]);
		msg::success(gm('Invitation cancelled'),'success');
		return true;
	}

	function activate2(&$in){
		if(!$this->validate_activate($in)){
			return false;
		}

		//$this->db_users->query("DELETE FROM user_meta WHERE user_id='".$in['user_id']."' AND name='active' ");
		$this->db_users->query("DELETE FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'active']);
		msg::success ( gm("User activated"),'success');
		return true;
	}
	function validate_activate(&$in){

		$trial = $this->db_users->field("SELECT is_trial FROM user_info WHERE user_id=(SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' limit 1) ");
		$in['is_trial'] = $trial;
		if($trial == 1){
			switch (true) {
				case (defined('PAYMILL_CLIENT_ID') && PAYMILL_CLIENT_ID==1):
					if(!$this->check_add_user2($in)){
						return false;
					}
					break;
				default:
					if(!$this->check_add_user($in)){
						return false;
					}
					break;
			}
		}
		return $this->delete_validate($in);
	}
	function check_add_user2(&$in){

		$u = $this->db_users->field("SELECT u_type FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' ");
		if($u == 2){
			return true;
		}
		$user_p = $this->db_users->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE credentials!='8' AND database_name='".DATABASE_NAME."' AND users.active='1' AND ( user_meta.value !=  '1' OR user_meta.value IS NULL ) ");
		$user_t = $this->db_users->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE credentials='8' AND database_name='".DATABASE_NAME."' AND users.active='1'  AND ( user_meta.value !=  '1' OR user_meta.value IS NULL )  ");
		if(ark::$method == 'add'){
			switch(HOW_MANY){
				case 0:
					if($in['group_id'] == 2){
						// if(ACCOUNT_PLAN == 4){
						// 	msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
						// 	return false;
						// }elseif ((ACCOUNT_PLAN == 5 || ACCOUNT_PLAN == 3) && $user_t>=1) {
						// 	msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
						// 	return false;
						// }
					}elseif($user_p > 1){
						msg::notice(gm("You cannot add more users. <br>Please change your subscription too add more users."),"notice");
						return false;
					}
					break;
				case 1:
					if($in['group_id'] == 2){

						//msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
						//return false;
						// if(ACCOUNT_PLAN == 4){
						// 	msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
						// 	return false;
						// }elseif ((ACCOUNT_PLAN == 5 || ACCOUNT_PLAN == 3) && $user_t>=8) {
						// 	msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
						// 	return false;
						// }

					}elseif($user_p > 3){
						msg::notice(gm("You cannot add more users. <br>Please change your subscription too add more users."),"notice");
						return false;
					}
					break;
				case 2:
					if($user_p > 7){
						msg::notice(gm("You cannot add more users. <br>Please change your subscription too add more users."),"notice");
						return false;
					}
					break;
				case 3:
					if($user_p > 15){
						msg::notice(gm("You cannot add more users. <br>Please change your subscription too add more users."),"notice");
						return false;
					}
					break;
			}
		}else{
			//$group_id = $this->db_users->field("SELECT group_id FROM users WHERE user_id='".$in['user_id']."'");
			$group_id = $this->db_users->field("SELECT group_id FROM users WHERE user_id= :user_id",['user_id'=>$in['user_id']]);
			if($in['group_id'] != $group_id && $in['group_id'] == 2){

				switch(HOW_MANY){
					case 0:
						// if(ACCOUNT_PLAN == 4){
						// 	msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
						// 	return false;
						// }elseif ((ACCOUNT_PLAN == 5 || ACCOUNT_PLAN == 3) && $user_t>=1) {
						// 	msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
						// 	return false;
						// }
						// msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
						// return false;
					case 1:
						if(ACCOUNT_PLAN == 4){
							msg::notice(gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users."),"notice");
							return false;
						}elseif ((ACCOUNT_PLAN == 5 || ACCOUNT_PLAN == 3) && $user_t>=8) {
							msg::notice(gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users."),"notice");
							return false;
						}
						// msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
						// return false;
				}
			}else{
				switch(HOW_MANY){
					case 0:
						if($group_id == 2){
							// if(ACCOUNT_PLAN == 4){
							// 	msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
							// 	return false;
							// }elseif ((ACCOUNT_PLAN == 5 || ACCOUNT_PLAN == 3) && $user_t>=1) {
							// 	msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
							// 	return false;
							// }
							// msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
							// return false;
						}elseif($user_p > 1){
							msg::notice(gm("You cannot add more users. <br>Please change your subscription too add more users."),"notice");
							return false;
						}
						break;
					case 1:

						if($group_id == 2){
							// msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
							// return false;
							// if(ACCOUNT_PLAN == 4){
							// 	msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
							// 	return false;
							// }elseif ((ACCOUNT_PLAN == 5 || ACCOUNT_PLAN == 3) && $user_t>=8) {
							// 	msg::$notice = "<a href='index.php?do=settings-subscription'>".gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users.")."</a>";
							// 	return false;
							// }
						}elseif($user_p > 3){
							msg::notice(gm("You cannot add more users. <br>Please change your subscription too add more users."),"notice");
							return false;
						}
						break;
					case 2:
						if($user_p > 7){
							msg::notice(gm("You cannot add more users. <br>Please change your subscription too add more users."),"notice");
							return false;
						}
						break;
					case 3:
						if($user_p > 15){
							msg::notice(gm("You cannot add more users. <br>Please change your subscription too add more users."),"notice");
							return false;
						}
						break;
				}
			}
		}
		return true;
	}
	function check_add_user(&$in){

		# get the max users for this account
		$globfo = $this->db_users->query("SELECT * FROM users WHERE user_id=(SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' AND user_role='1' ) ");
		$globfo->next();
		if($globfo->f('u_type') == 2){
			return true;
		}
		$max_users = $globfo->f('users');			#users
		$time_users = $globfo->f('time_users');	#time_users

		# get the groups credentials
		$cred_grp = $this->db->field("SELECT credentials FROM groups WHERE group_id='".$in['group_id']."' ");
		$cred_grp = explode(';',$cred_grp);

		# get the number of users for this account
		$user_p = $this->db_users->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE credentials!='8' AND database_name='".DATABASE_NAME."' AND users.active='1' AND ( user_meta.value !=  '1' OR user_meta.value IS NULL ) ");
		// $user_l = $this->db_users->field("SELECT COUNT(user_id) FROM users WHERE credentials!='8' AND credentials NOT LIKE '%3%' AND database_name='".DATABASE_NAME."' AND active='1' ");
		$user_t = $this->db_users->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE credentials='8' AND database_name='".DATABASE_NAME."' AND users.active='1'  AND ( user_meta.value !=  '1' OR user_meta.value IS NULL )  ");
		// console::log($max_users,$time_users,$user_p,$user_t);
		if(ark::$method == 'add'){
			if($in['group_id'] != '2'){
				if($user_p >= $max_users){
					msg::notice(gm("You cannot add more users. <br>Please change your subscription too add more users."),"notice");
					return false;
				}
			}else{
				if($user_t >= $time_users){
					msg::notice(gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users."),"notice");
					return false;
				}
			}
		}else{
			//$group_id = $this->db_users->field("SELECT group_id FROM users WHERE user_id='".$in['user_id']."'");
			$group_id = $this->db_users->field("SELECT group_id FROM users WHERE user_id= :user_id",['user_id'=>$in['user_id']]);
			if($in['group_id'] != $group_id ){
				if(isset($in['group_id']) && $in['group_id'] != '2' && $group_id == '2'){
					if($user_p >= $max_users){
						msg::notice(gm("You cannot add more users. <br>Please change your subscription too add more users."),"notice");
						return false;
					}
				}else if($in['group_id'] == '2'){
					if($user_t >= $time_users){
						msg::notice(gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users."),"notice");
						return false;
					}
				}else if(ark::$method != 'update'){
					if($user_p >= $max_users && $group_id != '2'){
						msg::notice(gm("You cannot add more users. <br>Please change your subscription too add more users."),"notice");
						return false;
					}
					if($user_t >= $time_users && $group_id == '2'){
						msg::notice(gm("You cannot add more Timesheet users. <br>Please change your subscription too add more Timesheet users."),"notice");
						return false;
					}
				}
			}
		}


		return true;
	}
	function delete(&$in)
	{
		
		if(!$this->delete_validate($in)){
			return false;
		}
		if($in['crypt']!=''){
			//$this->db_users->query("DELETE FROM user_email WHERE crypt='".$in['crypt']."'");
			$this->db_users->query("DELETE FROM user_email WHERE crypt= :crypt",['crypt'=>$in['crypt']]);
		}

		//$this->db_users->query("DELETE FROM users WHERE user_id='".$in['user_id']."'");
		$this->db_users->query("DELETE FROM users WHERE user_id= :user_id",['user_id'=>$in['user_id']]);
		//$this->db_users->query("DELETE FROM user_meta WHERE user_id='".$in['user_id']."'");
		$this->db_users->query("DELETE FROM user_meta WHERE user_id= :user_id",['user_id'=>$in['user_id']]);
		//$this->db_users->query("DELETE FROM user_info WHERE user_id='".$in['user_id']."'");
		$this->db_users->query("DELETE FROM user_info WHERE user_id= :user_id",['user_id'=>$in['user_id']]);
		//$this->db_users->query("DELETE FROM users_settings WHERE user_id='".$in['user_id']."'");
		$this->db_users->query("DELETE FROM users_settings WHERE user_id= :user_id",['user_id'=>$in['user_id']]);
		if(strpos($config['site_url'],'my.akti')){
			//Values inserted into Gaetan account
			global $database_config;
			$database_new = array(
					'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
					'username' => 'admin',
					'password' => 'hU2Qrk2JE9auQA',
					'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
			);
				
			$db_user_new = new sqldb($database_new);	
			$select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$in['user_id']."' AND field_id='1' ");
			if($select_contact){
				$db_user_new->query("DELETE FROM customer_contacts WHERE contact_id='".$select_contact."'");
				$db_user_new->query("DELETE FROM customer_contactsIds WHERE contact_id='".$select_contact."'");
				$db_user_new->query("DELETE FROM contact_field WHERE customer_id='".$select_contact."'");
			}		
			//end values inserted
		}

		msg::success ( gm("User deleted."),'success');
		return true;
	}
	function credentials(&$in){
		if(!$this->delete_validate($in)){
			return false;
        }
		//$old_group=$this->db_users->field("SELECT group_id FROM users WHERE user_id='".$in['user_id']."' ");
		$old_group=$this->db_users->field("SELECT group_id FROM users WHERE user_id= :user_id ",['user_id'=>$in['user_id']]);
		if($old_group==2){
			msg::error("Credentials not allowed to be changed","error");
			return false;
		}
		$arr = array('project_admin'=>false,'intervention_admin'=>false,'timesheet_admin'=>false);

		foreach ($in['credential'] as $key => $value ){
			if(!empty($value))
			{
				if($_SESSION['acc_type']=='services' && $value==12){
					continue;
				}
				$credential .= $value.';';
				/*if($value == '13'){
					$credential.='17;';
				}*/
				if($value == '3'){
					$arr['project_admin'] = true;
				}
				if($value == '13'){
					$arr['intervention_admin'] = true;
				}
				if($value == '8'){
					$arr['timesheet_admin'] = true;
				}
			}
		}

		# this is for third credentials o.O , two weren't enough
		foreach ($arr as $key => $value) {
			//$this->db_users->query("SELECT value FROM user_meta WHERE user_id='".$in['user_id']."' AND name='".$key."' ");
			$this->db_users->query("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>$key]);
			if($this->db_users->move_next()){
				//$this->db_users->query("UPDATE user_meta SET value='0' WHERE user_id='".$in['user_id']."' AND name='".$key."' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'0','user_id'=>$in['user_id'],'name'=>$key]);
			}else{
				//$this->db_users->query("INSERT INTO user_meta SET value='0', user_id='".$in['user_id']."', name='".$key."' ");
				$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>'0','user_id'=>$in['user_id'],'name'=>$key]);
			}
			if($value === true && $in['project_manage']){
				//$this->db_users->query("UPDATE user_meta SET value='1' WHERE user_id='".$in['user_id']."' AND name='project_admin' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'1','user_id'=>$in['user_id'],'name'=>'project_admin']);
			}
			if($value === true && $in['intervention_manage']){
				//$this->db_users->query("UPDATE user_meta SET value='1' WHERE user_id='".$in['user_id']."' AND name='intervention_admin' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'1','user_id'=>$in['user_id'],'name'=>'intervention_admin']);
			}
			if($value === true && $in['timesheet_manage']){
				//$timesheet_adm=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$in['user_id']."' AND name='timesheet_admin' ");
				$timesheet_adm=$this->db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'timesheet_admin']);
				if(is_null($timesheet_adm)){
					//$this->db_users->query("INSERT INTO user_meta SET value='1',user_id='".$in['user_id']."',name='timesheet_admin' ");
					$this->db_users->insert("INSERT INTO user_meta SET value= :value,user_id= :user_id,name= :name ",['value'=>'1','user_id'=>$in['user_id'],'name'=>'timesheet_admin']);
				}else{
					//$this->db_users->query("UPDATE user_meta SET value='1' WHERE user_id='".$in['user_id']."' AND name='timesheet_admin' ");
					$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'1','user_id'=>$in['user_id'],'name'=>'timesheet_admin']);
				}
			}
			if($value === true && $in['timetracker_manage']){
				//$timetracker_adm=$this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$in['user_id']."' AND name='timetracker_admin' ");
				$timetracker_adm=$this->db_users->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'timetracker_admin']);
				if(is_null($timetracker_adm)){
					//$this->db_users->query("INSERT INTO user_meta SET value='1',user_id='".$in['user_id']."',name='timetracker_admin' ");
					$this->db_users->insert("INSERT INTO user_meta SET value= :value,user_id= :user_id,name= :name ",['value'=>'1','user_id'=>$in['user_id'],'name'=>'timetracker_admin']);
				}else{
					//$this->db_users->query("UPDATE user_meta SET value='1' WHERE user_id='".$in['user_id']."' AND name='timetracker_admin' ");
					$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'1','user_id'=>$in['user_id'],'name'=>'timetracker_admin']);
				}
			}
		}

		//$group = $this->db_users->field("SELECT group_id FROM users WHERE user_id='".$in['user_id']."' ");
		$group = $this->db_users->field("SELECT group_id FROM users WHERE user_id= :user_id ",['user_id'=>$in['user_id']]);
		if($group == '1'){
			$credential .= '7;';
		}

		$credential = trim($credential,';');
		if($in['is_accountant']){
			//$this->db_users->query("UPDATE users SET is_accountant='".$in['is_accountant']."' WHERE  user_id='".$in['user_id']."'");
			$this->db_users->query("UPDATE users SET is_accountant= :is_accountant WHERE  user_id= :user_id",['is_accountant'=>$in['is_accountant'],'user_id'=>$in['user_id']]);
		}

		//$this->db_users->query("UPDATE users SET credentials='".$credential."' WHERE  user_id='".$in['user_id']."'");
		$this->db_users->query("UPDATE users SET credentials= :credentials WHERE  user_id= :user_id",['credentials'=>$credential,'user_id'=>$in['user_id']]);

		$this->set_settings_admin($in);

		//msg::success ( gm("User updated"),'success');
		return true;
	}
		function set_settings_admin(&$in)
	{
		//$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$in['user_id']."' AND name='admin_1' ");
		$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'','user_id'=>$in['user_id'],'name'=>'admin_1']);
		//$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$in['user_id']."' AND name='admin_12' ");
		$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'','user_id'=>$in['user_id'],'name'=>'admin_12']);
		//$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$in['user_id']."' AND name='admin_6' ");
		$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'','user_id'=>$in['user_id'],'name'=>'admin_6']);
		//$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$in['user_id']."' AND name='admin_5' ");
		$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'','user_id'=>$in['user_id'],'name'=>'admin_5']);
		//$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$in['user_id']."' AND name='admin_4' ");
		$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'','user_id'=>$in['user_id'],'name'=>'admin_4']);
		//$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$in['user_id']."' AND name='admin_3' ");
		$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'','user_id'=>$in['user_id'],'name'=>'admin_3']);
		//$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$in['user_id']."' AND name='admin_11' ");
		$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'','user_id'=>$in['user_id'],'name'=>'admin_11']);
		//$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$in['user_id']."' AND name='admin_13' ");
		$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'','user_id'=>$in['user_id'],'name'=>'admin_13']);
		//$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$in['user_id']."' AND name='admin_14' ");
		$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'','user_id'=>$in['user_id'],'name'=>'admin_14']);
		$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'','user_id'=>$in['user_id'],'name'=>'admin_16']);
		//$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$in['user_id']."' AND name='admin_17' ");
		$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'','user_id'=>$in['user_id'],'name'=>'admin_17']);
		//$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$in['user_id']."' AND name='admin_19' ");
		$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'','user_id'=>$in['user_id'],'name'=>'admin_19']);
		if($in['crm_admin']){
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['user_id']."' AND name='admin_1' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'admin_1']);
			if($this->db_users->move_next()){
				//$this->db_users->query("UPDATE user_meta SET value='company' WHERE user_id='".$in['user_id']."' AND name='admin_1' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'company','user_id'=>$in['user_id'],'name'=>'admin_1']);
			}else{
				//$this->db_users->query("INSERT INTO user_meta SET value='company', user_id='".$in['user_id']."', name='admin_1' ");
				$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>'company','user_id'=>$in['user_id'],'name'=>'admin_1']);
			}
		}
		if($in['article_admin']){
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['user_id']."' AND name='admin_12' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'admin_12']);
			if($this->db_users->move_next()){
				//$this->db_users->query("UPDATE user_meta SET value='article' WHERE user_id='".$in['user_id']."' AND name='admin_12' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'article','user_id'=>$in['user_id'],'name'=>'admin_12']);
			}else{
				//$this->db_users->query("INSERT INTO user_meta SET value='article', user_id='".$in['user_id']."', name='admin_12' ");
				$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>'article','user_id'=>$in['user_id'],'name'=>'admin_12']);
			}
		}
		if($in['order_admin']){
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['user_id']."' AND name='admin_6' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'admin_6']);
			if($this->db_users->move_next()){
				//$this->db_users->query("UPDATE user_meta SET value='order' WHERE user_id='".$in['user_id']."' AND name='admin_6' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'order','user_id'=>$in['user_id'],'name'=>'admin_6']);
			}else{
				//$this->db_users->query("INSERT INTO user_meta SET value='order', user_id='".$in['user_id']."', name='admin_6' ");
				$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>'order','user_id'=>$in['user_id'],'name'=>'admin_6']);
			}
		}
		if($in['po_order_admin']){
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['user_id']."' AND name='admin_14' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'admin_14']);
			if($this->db_users->move_next()){
				//$this->db_users->query("UPDATE user_meta SET value='po_order' WHERE user_id='".$in['user_id']."' AND name='admin_14' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'po_order','user_id'=>$in['user_id'],'name'=>'admin_14']);
			}else{
				//$this->db_users->query("INSERT INTO user_meta SET value='po_order', user_id='".$in['user_id']."', name='admin_14' ");
				$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>'po_order','user_id'=>$in['user_id'],'name'=>'admin_14']);
			}
		}
		if($in['stock_admin']){
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['user_id']."' AND name='admin_16' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'admin_16']);
			if($this->db_users->move_next()){
				//$this->db_users->query("UPDATE user_meta SET value='stock' WHERE user_id='".$in['user_id']."' AND name='admin_16' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'stock','user_id'=>$in['user_id'],'name'=>'admin_16']);
			}else{
				//$this->db_users->query("INSERT INTO user_meta SET value='stock', user_id='".$in['user_id']."', name='admin_16' ");
				$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>'stock','user_id'=>$in['user_id'],'name'=>'admin_16']);
			}
		}
		if($in['quote_admin']){
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['user_id']."' AND name='admin_5' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'admin_5']);
			if($this->db_users->move_next()){
				//$this->db_users->query("UPDATE user_meta SET value='quote' WHERE user_id='".$in['user_id']."' AND name='admin_5' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'quote','user_id'=>$in['user_id'],'name'=>'admin_5']);
			}else{
				//$this->db_users->query("INSERT INTO user_meta SET value='quote', user_id='".$in['user_id']."', name='admin_5' ");
				$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>'quote','user_id'=>$in['user_id'],'name'=>'admin_5']);
			}
		}
		if($in['contract_admin']){
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['user_id']."' AND name='admin_11' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'admin_11']);
			if($this->db_users->move_next()){
				//$this->db_users->query("UPDATE user_meta SET value='contract' WHERE user_id='".$in['user_id']."' AND name='admin_11' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'contract','user_id'=>$in['user_id'],'name'=>'admin_11']);
			}else{
				//$this->db_users->query("INSERT INTO user_meta SET value='contract', user_id='".$in['user_id']."', name='admin_11' ");
				$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>'contract','user_id'=>$in['user_id'],'name'=>'admin_11']);
			}
		}
		if($in['project_admin']){
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['user_id']."' AND name='admin_3' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'admin_3']);
			if($this->db_users->move_next()){
				//$this->db_users->query("UPDATE user_meta SET value='project' WHERE user_id='".$in['user_id']."' AND name='admin_3' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'project','user_id'=>$in['user_id'],'name'=>'admin_3']);
			}else{
				//$this->db_users->query("INSERT INTO user_meta SET value='project', user_id='".$in['user_id']."', name='admin_3' ");
				$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>'project','user_id'=>$in['user_id'],'name'=>'admin_3']);
			}
		}
		if($in['invoice_admin']){
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['user_id']."' AND name='admin_4' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'admin_4']);
			if($this->db_users->move_next()){
				//$this->db_users->query("UPDATE user_meta SET value='invoice' WHERE user_id='".$in['user_id']."' AND name='admin_4' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'invoice','user_id'=>$in['user_id'],'name'=>'admin_4']);
			}else{
				//$this->db_users->query("INSERT INTO user_meta SET value='invoice', user_id='".$in['user_id']."', name='admin_4' ");
				$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>'invoice','user_id'=>$in['user_id'],'name'=>'admin_4']);
			}
		}
		if($in['maintenance_admin']){
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['user_id']."' AND name='admin_13' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'admin_13']);
			if($this->db_users->move_next()){
				//$this->db_users->query("UPDATE user_meta SET value='service' WHERE user_id='".$in['user_id']."' AND name='admin_13' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'service','user_id'=>$in['user_id'],'name'=>'admin_13']);
			}else{
				//$this->db_users->query("INSERT INTO user_meta SET value='service', user_id='".$in['user_id']."', name='admin_13' ");
				$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>'service','user_id'=>$in['user_id'],'name'=>'admin_13']);
			}
		}
		if($in['installation_admin']){
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['user_id']."' AND name='admin_17' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'admin_17']);
			if($this->db_users->move_next()){
				//$this->db_users->query("UPDATE user_meta SET value='installation' WHERE user_id='".$in['user_id']."' AND name='admin_17' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'installation','user_id'=>$in['user_id'],'name'=>'admin_17']);
			}else{
				//$this->db_users->query("INSERT INTO user_meta SET value='installation', user_id='".$in['user_id']."', name='admin_17' ");
				$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>'installation','user_id'=>$in['user_id'],'name'=>'admin_17']);
			}
		}
		if($in['timetracker_admin']){
			//$this->db_users->query("SELECT * FROM user_meta WHERE user_id='".$in['user_id']."' AND name='admin_19' ");
			$this->db_users->query("SELECT * FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'admin_19']);
			if($this->db_users->move_next()){
				//$this->db_users->query("UPDATE user_meta SET value='timetracker' WHERE user_id='".$in['user_id']."' AND name='admin_19' ");
				$this->db_users->query("UPDATE user_meta SET value= :value WHERE user_id= :user_id AND name= :name ",['value'=>'timetracker','user_id'=>$in['user_id'],'name'=>'admin_19']);
			}else{
				//$this->db_users->query("INSERT INTO user_meta SET value='timetracker', user_id='".$in['user_id']."', name='admin_19' ");
				$this->db_users->insert("INSERT INTO user_meta SET value= :value, user_id= :user_id, name= :name ",['value'=>'timetracker','user_id'=>$in['user_id'],'name'=>'admin_19']);
			}
		}
	}
	function group_delete(&$in){

		if(!$this->group_delete_validate($in)){
			return false;
		}
		$this->db->query("DELETE FROM groups WHERE group_id='".$in['group_id']."'");
		msg::success ( gm('Group deleted!'),'success');
		return true;
	}
	function group_delete_validate(&$in){
		//$this->db_users->query("SELECT * FROM users WHERE group_id='".$in['group_id']."' ");
		$this->db_users->query("SELECT * FROM users WHERE group_id= :group_id ",['group_id'=>$in['group_id']]);
		if($this->db_users->move_next()){
			msg::error ( gm('There are users that are in this group. Remove the users first and then try to delete the group.'),'error');
			return false;
		}
		return true;
	}
	function add_group(&$in){
		if(!$this->add_group_validate($in)){
			return false;
		}
		$in['group_id'] = $this->db->insert("INSERT INTO groups SET name='".$in['name']."' ");
		//		$this->db_users->query("UPDATE users SET group_id='".$in['group_id']."' WHERE user_id='".$in['admin_id']."'");
		if($in['credential']){
			foreach ($in['credential'] as $key => $value ){
				if($value){
					$credential .= $value.';';
					// If value equals Purchase Orders than add Stock to menu
					if($value == '14'){
						$credential .= '16;';
					}
				}			
			}
			$credential = trim($credential,';');
			$this->db->query("UPDATE groups SET credentials='".$credential."' WHERE group_id='".$in['group_id']."'");
		}
		msg::success ( gm('Group added'),'success');
		return true;
	}
	function add_group_validate(&$in){
		$is_ok = true;
		$v = new validation($in);
		$v->field('name',gm("Group Name"),"required:unique[groups.name.( group_id!='".$in['group_id']."')]");
		// $v->field('admin_id',gm("Group Administrator"),"required");
		return $v->run();
	}
	function update_group(&$in){
		if($in['group_id']==1 || $in['group_id']==2){
			return true;
		}

		$this->db->insert("UPDATE groups SET name='".$in['name']."' WHERE group_id='".$in['group_id']."' ");

		$credential = '';
		if($in['credential']){
			foreach ($in['credential'] as $key => $value ){

		if($key==1 && $value==''){
			//$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id='".$in['group_id']."' AND database_name='".DATABASE_NAME."'");
			$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id= :group_id AND database_name= :database_name",['group_id'=>$in['group_id'],'database_name'=>DATABASE_NAME]);
			while ($users_all->move_next()) {
				$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$users_all->f('user_id')."' AND name='admin_1' ");
			}
		}
		if($key==3 && $value==''){
			//$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id='".$in['group_id']."' AND database_name='".DATABASE_NAME."'");
			$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id= :group_id AND database_name= :database_name",['group_id'=>$in['group_id'],'database_name'=>DATABASE_NAME]);
			while ($users_all->move_next()) {
				$this->db_users->query("UPDATE user_meta SET value=0 WHERE user_id='".$users_all->f('user_id')."' AND name='project_admin' ");
				$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$users_all->f('user_id')."' AND name='admin_3' ");
			}
		}
		if($key==4 && $value==''){
			//$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id='".$in['group_id']."' AND database_name='".DATABASE_NAME."'");
			$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id= :group_id AND database_name= :database_name",['group_id'=>$in['group_id'],'database_name'=>DATABASE_NAME]);
			while ($users_all->move_next()) {
				$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$users_all->f('user_id')."' AND name='admin_4' ");
			}
		}
		if($key==5 && $value==''){
			//$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id='".$in['group_id']."' AND database_name='".DATABASE_NAME."'");
			$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id= :group_id AND database_name= :database_name",['group_id'=>$in['group_id'],'database_name'=>DATABASE_NAME]);
			while ($users_all->move_next()) {
				$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$users_all->f('user_id')."' AND name='admin_5' ");
			}
		}
		if($key==6 && $value==''){
			//$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id='".$in['group_id']."' AND database_name='".DATABASE_NAME."'");
			$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id= :group_id AND database_name= :database_name",['group_id'=>$in['group_id'],'database_name'=>DATABASE_NAME]);
			while ($users_all->move_next()) {
				$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$users_all->f('user_id')."' AND name='admin_6' ");
			}
		}
		if($key==9 && $value==''){
			//$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id='".$in['group_id']."' AND database_name='".DATABASE_NAME."'");
			$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id= :group_id AND database_name= :database_name",['group_id'=>$in['group_id'],'database_name'=>DATABASE_NAME]);
			while ($users_all->move_next()) {
				$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$users_all->f('user_id')."' AND name='admin_9' ");
			}
		}
		if($key==11 && $value==''){
			//$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id='".$in['group_id']."' AND database_name='".DATABASE_NAME."'");
			$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id= :group_id AND database_name= :database_name",['group_id'=>$in['group_id'],'database_name'=>DATABASE_NAME]);
			while ($users_all->move_next()) {
				$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$users_all->f('user_id')."' AND name='admin_11' ");
			}
		}
		if($key==12 && $value==''){
			//$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id='".$in['group_id']."' AND database_name='".DATABASE_NAME."'");
			$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id= :group_id AND database_name= :database_name",['group_id'=>$in['group_id'],'database_name'=>DATABASE_NAME]);
			while ($users_all->move_next()) {
				$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$users_all->f('user_id')."' AND name='admin_12' ");
			}
		}
		if($key==13 && $value==''){
			//$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id='".$in['group_id']."' AND database_name='".DATABASE_NAME."'");
			$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id= :group_id AND database_name= :database_name",['group_id'=>$in['group_id'],'database_name'=>DATABASE_NAME]);
			while ($users_all->move_next()) {
				$this->db_users->query("UPDATE user_meta SET value=0 WHERE user_id='".$users_all->f('user_id')."' AND name='intervention_admin' ");
				$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$users_all->f('user_id')."' AND name='admin_13' ");
			}
		}
		if($key==14 && $value==''){
			//$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id='".$in['group_id']."' AND database_name='".DATABASE_NAME."'");
			$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id= :group_id AND database_name= :database_name",['group_id'=>$in['group_id'],'database_name'=>DATABASE_NAME]);
			while ($users_all->move_next()) {
				$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$users_all->f('user_id')."' AND name='admin_14' ");
			}
		}
		if($key==18 && $value==''){
			//$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id='".$in['group_id']."' AND database_name='".DATABASE_NAME."'");
			$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id= :group_id AND database_name= :database_name",['group_id'=>$in['group_id'],'database_name'=>DATABASE_NAME]);
			while ($users_all->move_next()) {
				$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$users_all->f('user_id')."' AND name='admin_18' ");
			}
		}
		if($key==19 && $value==''){
			//$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id='".$in['group_id']."' AND database_name='".DATABASE_NAME."'");
			$users_all = $this->db_users->query("SELECT * FROM users WHERE group_id= :group_id AND database_name= :database_name",['group_id'=>$in['group_id'],'database_name'=>DATABASE_NAME]);
			while ($users_all->move_next()) {
				$this->db_users->query("UPDATE user_meta SET value='' WHERE user_id='".$users_all->f('user_id')."' AND name='admin_19' ");
			}
		}
				if($value){
					$credential .= $value.';';
				}
			}
			$credential = trim($credential,';');
		}

		$this->db->query("UPDATE groups SET credentials='".$credential."' WHERE group_id='".$in['group_id']."' ");
		//$this->db_users->query("UPDATE users SET credentials='".$credential."' WHERE group_id='".$in['group_id']."' AND database_name='".DATABASE_NAME."' ");
		$this->db_users->query("UPDATE users SET credentials= :credentials WHERE group_id= :group_id AND database_name= :database_name ",['credentials'=>$credential,'group_id'=>$in['group_id'],'database_name'=>DATABASE_NAME]);
		
		

		msg::success ( gm('Group updated'),'success');
		return true;
	}

	function campaign_monitor(&$in){
		
		require_once 'libraries/campaignmonitor/campaign_all.php';

		$wrap = new CS_REST_General(NULL);

		$result = $wrap->get_apikey($in['campaign_user'], $in['campaign_password'], $in['campaign_site'].'.createsend.com');

		if($result->was_successful()) {
		  $in['campaign_monitor_apy'] = $result->response->ApiKey;
		  $this->db->query("UPDATE settings SET value='".$in['campaign_monitor_apy']."' WHERE constant_name='ACCOUNT_CAMPAIGN_APY' ");
		  $app = $this->db->query("SELECT name, app_id FROM apps WHERE name='Campaign monitor' AND type='main' ");
		  if($app->next()){
		  	$this->db->query("UPDATE apps SET api='".$in['campaign_monitor_apy']."', active='1' WHERE name='Campaign monitor' AND main_app_id='0' AND type='main' ");
		   	$app_id = $app->f('app_id');
		  }else{
		   	$app_id = $this->db->insert("INSERT INTO apps SET api='".$in['campaign_monitor_apy']."', name='Campaign monitor', type='main', template_file='camp' ");
		  }

			$wrap1 = new CS_REST_General($in['campaign_monitor_apy']);

			$result = $wrap1->get_clients();

			if($result->was_successful()) {
    		$in['campaign_client'] = $result->response[0]->ClientID;
    		$this->db->query("SELECT value FROM settings WHERE constant_name='ACCOUNT_CAMPAIGN_CLIENT'");
    		if(!$this->db->move_next()){
    			$this->db->query("INSERT INTO settings SET value='".$in['campaign_client']."', constant_name='ACCOUNT_CAMPAIGN_CLIENT'");
    			$this->db->query("INSERT INTO apps SET api='".$in['campaign_client']."', main_app_id='".$app_id."', type='client_api' ");
    		}else{
    			$this->db->query("UPDATE settings SET value='".$in['campaign_client']."' WHERE constant_name='ACCOUNT_CAMPAIGN_CLIENT'");
    			$this->db->query("UPDATE apps SET api='".$in['campaign_client']."' WHERE main_app_id='".$app_id."' AND type='client_api' ");
			}
				$this->db->query("DELETE FROM settings WHERE constant_name='ACCOUNT_CAMPAIGN_CLOSE' ");
			} else {
			  msg::$error = $result->response;
			  return false;
			}

		  msg::success ( gm("Success"),'success');
		  $in['xget']='apps';
		  return true;

		} else {
		    msg::error ( $result->response->Message,'error');
		    return false;
		}
	}
	function deactivate_camp(&$in){
		$this->db->query("UPDATE apps SET active='0' WHERE name='Campaign monitor' AND main_app_id='0' AND type='main' ");
		$in['xget']='apps';
		return true;
	}
/*	function chimp(){
		$this->db = new sqldb();
		// include(INSTALLPATH.'libraries/MCAPI.class.php');
		ark::loadLibraries(array('Mailchimp'));

	}*/
	function activate_chimp(&$in){
		ark::loadLibraries(array('Mailchimp'));

		if(!$this->activate_chimp_validate($in)){
			return  false;
		}
		$this->db->query("UPDATE apps SET api='".$in['api_key']."', active='1' WHERE name='Mail chimp' AND main_app_id='0' AND type='main' ");
		msg::success ( gm('App activated'),'success');
		return true;

	}

	/**
	 * Validation for activate
	 * we check to see if we have all the fields necesery
	 * we need user, password and api_key fields to be filled
	 * if we have all we need we check to see if the credentials are ok
	 *
	 * @param array() $in
	 * @return true for no error(s) or false for error(s)
	 */
	function activate_chimp_validate(&$in){
		$v = new validation($in);
		$v->field('api_key', gm('ID'), "required");
		if(!$v->run()){
			return false;
		}
		$api = new Mailchimp($in['api_key']);
		$result = $api->users->logins();
		if(empty($result['error'])){
			return true;
		}
		$this->db->query("UPDATE apps SET active='2' WHERE  name='Mail chimp' AND main_app_id='0' AND type='main' ");
		msg::error ( $result['error'],'success');
		return false;
	}

	function deactivate_chimp(&$in){
		$this->db->query("UPDATE apps SET active='0' WHERE name='Mail chimp' AND main_app_id='0' AND type='main' ");
		return true;
	}

	function coda_tok(&$in){
		global $config;
		$ch = curl_init();
		$headers=array('Content-Type: application/json','X-Software-Company: '.$config['codabox_api']);
		$coda_data=array("vat"=>$in['vat_nr']);
		$coda_data = json_encode($coda_data);
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        	curl_setopt($ch, CURLOPT_POST, true);
        	curl_setopt($ch, CURLOPT_POSTFIELDS, $coda_data);
        	curl_setopt($ch, CURLOPT_URL, $config['codabox_api_prefix'].'/request-access-token/invoicing-software/');

		$put = curl_exec($ch);
 		$info = curl_getinfo($ch);

 		if($info['http_code']>300 || $info['http_code']==0){
 			$response=json_decode($put);
 			$err='';
 			foreach($response as $key=>$value){
 				if(is_array($value)){
 					foreach($value as $key1=>$value1){
 						$err.=$value1.',';
 					}
 				}else{
 					$err.=$value.',';
 				}			
 			}
 			$err=rtrim($err,",");
 			msg::error ($err,'error');
 			json_out($in);
 			return false;
 		}else{
 			$coda_id=$this->db->field("SELECT app_id FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
 			$coda_step_id=$this->db->field("SELECT app_id FROM apps WHERE main_app_id='".$coda_id."' AND type='step' ");
 			$this->db->query("UPDATE apps SET api='".$config['codabox_api']."' WHERE app_id='".$coda_id."' ");
 			$this->db->query("UPDATE apps SET api='2' WHERE app_id='".$coda_step_id."' ");
 			$vat_nr=$this->db->field("SELECT app_id FROM apps WHERE type='vat' AND main_app_id='".$coda_id."' ");
 			if($vat_nr){
 				$this->db->query("UPDATE apps SET api='".$in['vat_nr']."' WHERE app_id='".$vat_nr."' ");
 			}else{
 				$this->db->query("INSERT INTO apps SET api='".$in['vat_nr']."', type='vat', main_app_id='".$coda_id."' ");
 			}
 			msg::success ( gm("Data validated"),'success');
 		}
 		return true;
	}
	function coda_cred(&$in){
		global $config;
		$ch = curl_init();
		$coda_data=$this->db->query("SELECT api,app_id FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
		$headers=array('Content-Type: application/json','X-Software-Company: '.$coda_data->f('api'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_URL, $config['codabox_api_prefix'].'/get-credentials/'.$in['token_id'].'/');

        $put = curl_exec($ch);
 		$info = curl_getinfo($ch);
 		if($info['http_code']>300 || $info['http_code']==0){
 			$response=json_decode($put);
 			$err='';
 			foreach($response as $key=>$value){
 				if(is_array($value)){
 					foreach($value as $key1=>$value1){
 						$err.=$value1.',';
 					}
 				}else{
 					$err.=$value.',';
 				}			
 			}
 			$err=rtrim($err,",");
 			msg::error ($err,'error');
 			json_out($in);
 			return false;
 		}else{
 			$data=json_decode($put);
 			$token_entry=$this->db->field("SELECT app_id FROM apps WHERE type='token' AND main_app_id='".$coda_data->f('app_id')."' ");
 			if($token_entry){
 				$this->db->query("UPDATE apps SET api='".$in['token_id']."' WHERE app_id='".$token_entry."' ");
 			}else{
 				$this->db->query("INSERT INTO apps SET api='".$in['token_id']."', type='token', main_app_id='".$coda_data->f('app_id')."' ");
 			}
 			$username_entry=$this->db->field("SELECT app_id FROM apps WHERE type='username' AND main_app_id='".$coda_data->f('app_id')."' ");
 			if($username_entry){
 				$this->db->query("UPDATE apps SET api='".$data->username."' WHERE app_id='".$username_entry."' ");
 			}else{
 				$this->db->query("INSERT INTO apps SET api='".$data->username."', type='username', main_app_id='".$coda_data->f('app_id')."' ");
 			}
 			$pass_entry=$this->db->field("SELECT app_id FROM apps WHERE type='password' AND main_app_id='".$coda_data->f('app_id')."' ");
 			if($pass_entry){
 				$this->db->query("UPDATE apps SET api='".$data->password."' WHERE app_id='".$pass_entry."' ");
 			}else{
 				$this->db->query("INSERT INTO apps SET api='".$data->password."', type='password', main_app_id='".$coda_data->f('app_id')."' ");
 			}
 			$sales_e=$this->db->field("SELECT app_id FROM apps WHERE type='sales' AND main_app_id='".$coda_data->f('app_id')."' ");
 			if($sales_e){
 				$this->db->query("UPDATE apps SET api='".$in['sales_check']."' WHERE app_id='".$sales_e."' ");
 			}else{
 				$this->db->query("INSERT INTO apps SET api='".$in['sales_check']."', type='sales', main_app_id='".$coda_data->f('app_id')."' ");
 			}
 			$payments_e=$this->db->field("SELECT app_id FROM apps WHERE type='payments' AND main_app_id='".$coda_data->f('app_id')."' ");
 			if($payments_e){
 				$this->db->query("UPDATE apps SET api='".$in['payments_check']."' WHERE app_id='".$payments_e."' ");
 			}else{
 				$this->db->query("INSERT INTO apps SET api='".$in['payments_check']."', type='payments', main_app_id='".$coda_data->f('app_id')."' ");
 			}
 			$coda_step_id=$this->db->field("SELECT app_id FROM apps WHERE main_app_id='".$coda_data->f('app_id')."' AND type='step' ");
 			$this->db->query("UPDATE apps SET api='3' WHERE app_id='".$coda_step_id."' ");
 			$this->db->query("UPDATE apps SET active='1' WHERE app_id='".$coda_data->f('app_id')."' ");
 			msg::success ( gm("Account on Codabox activated"),'success');
 		}
 		return true;
	}
	function coda_connect(&$in){
		global $config;
		$ch = curl_init();
		$coda_api=$this->db->query("SELECT api,app_id FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
		$coda_user=$this->db->field("SELECT api FROM apps WHERE type='username' AND main_app_id='".$coda_api->f('app_id')."' ");
		$coda_pass=$this->db->field("SELECT api FROM apps WHERE type='password' AND main_app_id='".$coda_api->f('app_id')."' ");

		$headers=array('Content-Type: application/json','X-Software-Company: '.$coda_api->f('api'));

    	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
    	curl_setopt($ch, CURLOPT_USERPWD, $coda_user.":".$coda_pass);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
       	curl_setopt($ch, CURLOPT_URL, $config['codabox_api_prefix'].'/clients/');
	  	$put = curl_exec($ch);
 		$info = curl_getinfo($ch);

 		if($info['http_code']>300 || $info['http_code']==0){
 			$response=json_decode($put);
 			$err='';
 			foreach($response as $key=>$value){
 				if(is_array($value)){
 					foreach($value as $key1=>$value1){
 						$err.=$value1.',';
 					}
 				}else{
 					$err.=$value.',';
 				}			
 			}
 			$err=rtrim($err,",");
 			msg::error ($err,'error');
 			json_out($in);
 			return false;
 		}else{
 			$sales_e=$this->db->field("SELECT app_id FROM apps WHERE type='sales' AND main_app_id='".$coda_api->f('app_id')."' ");
 			if($sales_e){
 				$this->db->query("UPDATE apps SET api='".$in['sales_check']."' WHERE app_id='".$sales_e."' ");
 			}else{
 				$this->db->query("INSERT INTO apps SET api='".$in['sales_check']."', type='sales', main_app_id='".$coda_api->f('app_id')."' ");
 			}
 			$payments_e=$this->db->field("SELECT app_id FROM apps WHERE type='payments' AND main_app_id='".$coda_api->f('app_id')."' ");
 			if($payments_e){
 				$this->db->query("UPDATE apps SET api='".$in['payments_check']."' WHERE app_id='".$payments_e."' ");
 			}else{
 				$this->db->query("INSERT INTO apps SET api='".$in['payments_check']."', type='payments', main_app_id='".$coda_api->f('app_id')."' ");
 			}
 			$this->db->query("UPDATE apps SET active='1' WHERE app_id='".$coda_api->f('app_id')."' ");
 			msg::success ( gm("Account on Codabox activated"),'success');
 			$in['xget']='apps';
 			mark_invoices_as_exported('codabox', false);
 		}
 		$in['done']=1;
		return true;
	}
	function coda_disconnect(&$in){
		$coda_api=$this->db->field("SELECT app_id FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
		$this->db->query("UPDATE apps SET active='0' WHERE app_id='".$coda_api."' ");
		msg::success ( gm("Account deactivated"),'success');
		$in['done']=0;
		return true;
	}
	function coda_back(&$in){
		$coda_api=$this->db->field("SELECT app_id FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
		$this->db->query("UPDATE apps SET api='1' WHERE type='step' AND main_app_id='".$coda_api."' ");
		return true;
	}
	function toggleActivate(&$in)
	{
		$this->db->query("UPDATE apps SET active='".$in['value']."' WHERE name='Google Calendar' AND main_app_id='0' AND type='main' ");
		$in['xget']='apps';
		return true;
	}
	function yuki(&$in)
	{
		$app = $this->db->field("SELECT app_id FROM apps WHERE name='Yuki' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
		if(!$app){
			msg::error(gm('Error'),'error');
			json_out($in);
		}
		if($in['eff_drop']=='1'){
			$value=$in['api_key'].';'.$in['admin_key'].';'.$in['yuki_drop'].';'.$in['yuki_drop_sale'];
			$activated=2;
		}else{
			$v=new validation($in);
			$v->field('api_key',gm('Api key'),'required');
			$v->field('admin_key',gm('Admin key'),'required');
			if(!$v->run()){
				json_out($in);
			}
			$value=$in['api_key'].';'.$in['admin_key'].';'.$in['yuki_drop'];
			$activated=1;
		}
		$this->db->query("UPDATE apps SET api='".$value."' WHERE app_id='".$app."' ");
		$this->db->query("UPDATE apps SET active=0 WHERE name='Yuki' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");


	      mark_invoices_as_exported('yuki', true);

  		$yuki_projects_enabled = $this->db->field("SELECT value FROM settings WHERE constant_name='YUKI_PROJECTS' ");
        if(is_null($yuki_projects_enabled)){
          $this->db->query("INSERT INTO settings SET constant_name='YUKI_PROJECTS', value='".$in['yuki_projects_enabled']."' ");
        }else{
          $this->db->query("UPDATE settings SET value='".$in['yuki_projects_enabled']."' WHERE constant_name='YUKI_PROJECTS'");
        } 
	  

		if($in['is_activated']){
			$this->db->query("UPDATE apps SET active='".$activated."' WHERE name='Yuki' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
			$in['is_activated'] = 1;
			require_once(__DIR__.'/ExportAuto.php');
			$exportAuto=new ExportAuto();
			$in['module']='yuki';
			$in['only_return']=true;
			$in['invoice']=$in['exportAutoInvoice'];
			$in['pinvoice']=$in['exportAutoPInvoice'];
			$exportAuto->updateExportAutoInvoice($in);
		}
		$in['xget']='apps';
		return true;
	}
	function stripe_disconnect(&$in){
		$stripe_id=$this->db->field("SELECT app_id FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' ");
		$this->db->query("UPDATE apps SET active='0' WHERE app_id='".$stripe_id."' ");
		msg::success ( gm("Account deactivated"),'success');
		$in['done']=0;
		$in['xget']='apps';
		return true;
	}
	function stripe_connect(&$in){

		if(substr($in['api_key'],3,4)=='test' || substr($in['publish_key'],3,4)=='test'){
			msg::error(gm('Please insert a valid live secret key or publishable key'),"error");
		}else{
			ark::loadLibraries(array('stripe/init'));
			try{
				\Stripe\Stripe::setApiKey($in['api_key']);
				$customers=\Stripe\Customer::all(array("limit" => 1));
				$in['done']=1;
				$stripe_id=$this->db->field("SELECT app_id FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' ");
				$this->db->query("UPDATE apps SET api='".$in['api_key']."', active='1' WHERE app_id='".$stripe_id."' ");
				$publish_id=$this->db->field("SELECT app_id FROM apps WHERE main_app_id='".$stripe_id."' AND type='publish_key' ");
				if($publish_id){
					$this->db->query("UPDATE apps SET api='".$in['publish_key']."' WHERE app_id='".$publish_id."' ");
				}else{
					$this->db->query("INSERT INTO apps SET api='".$in['publish_key']."', main_app_id='".$stripe_id."', type='publish_key' ");
				}
				msg::success ( gm('Account on Stripe activated'),'success');
				
			}catch(Exception $e){
				msg::error( gm('Please enter a valid key'),'error');
			}
		}
		$in['xget']='apps';
		return true;
	}

	function billtobox_activate_export(&$in){
		$app = $this->db->query("SELECT * FROM apps WHERE name='BillToBox' AND type='main' AND main_app_id='0' ");


	      if(!$in['from_connect']){
	      	if($in['api_key'] ){
		        $vat_number = $this->db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_VAT_NUMBER' ");
		          if($this->check_api_key($in['api_key'], $vat_number)){

		             $this->db->query("UPDATE apps SET api='".$in['api_key']."' WHERE name='BillToBox' AND type='main' AND main_app_id='0'");
		                 
		        }
	       
	      	}else{
	      		msg::error(gm('This field is required'),"api_key");
	      
	      	}
	      }

	     if(!$in['export_pinv_to_billtobox'] && !$in['export_billtobox_to_dropbox']){
	     	$export_billtobox_to_dropbox = $this->db->field("SELECT app_id FROM apps WHERE type='export_billtobox_to_dropbox' AND main_app_id='".$app->f('app_id')."' ");
	     	if($export_billtobox_to_dropbox){
	     		 $this->db->query("UPDATE apps SET active='0', api='' WHERE app_id='".$export_billtobox_to_dropbox."'");
	     	}
	     	
	     	 $export_pinv_to_billtobox = $this->db->field("SELECT app_id FROM apps WHERE type='export_pinv_to_billtobox' AND main_app_id='".$app->f('app_id')."' ");
	     	 if($export_pinv_to_billtobox){
	     	 	 $this->db->query("UPDATE apps SET active='0', api='' WHERE app_id='".$export_pinv_to_billtobox."'");
	     	 }
	     	 
	     }

		if($in['access_key'] && $in['export_billtobox_to_dropbox']){
	        $vat_number = $this->db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_VAT_NUMBER' ");
	          if($this->check_access_key($in['access_key'], $vat_number)){

	            $export_billtobox_to_dropbox = $this->db->field("SELECT app_id FROM apps WHERE type='export_billtobox_to_dropbox' AND main_app_id='".$app->f('app_id')."' ");
	            if(is_null($export_billtobox_to_dropbox)){
	              $this->db->query("INSERT INTO apps SET type='export_billtobox_to_dropbox', main_app_id='".$app->f('app_id')."', active='".$in['export_billtobox_to_dropbox']."', api='".$in['access_key']."' ");
	            }else{
	              $this->db->query("UPDATE apps SET active='".$in['export_billtobox_to_dropbox']."', api='".$in['access_key']."' WHERE app_id='".$export_billtobox_to_dropbox."'");
	            } 
	      
	             msg::success ( gm("Changes Saved."),'success');  
	        }else{
	        	if($in['from_connect']){
	      			return false;
	      		}
	        }
	       
	      }elseif(!$in['access_key'] && $in['export_billtobox_to_dropbox']){
	      		msg::error(gm('This field is required'),"access_key");

	      		if($in['from_connect']){
	      			return false;
	      		}
	      	
	      }elseif($in['access_key_pinv'] && $in['export_pinv_to_billtobox']){
	        $vat_number = $this->db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_VAT_NUMBER' ");
	          if($this->check_access_key_pinv($in['access_key_pinv'], $vat_number)){
	
	             $export_pinv_to_billtobox = $this->db->field("SELECT app_id FROM apps WHERE type='export_pinv_to_billtobox' AND main_app_id='".$app->f('app_id')."' ");
	            if(is_null($export_pinv_to_billtobox)){
	              $this->db->query("INSERT INTO apps SET type='export_pinv_to_billtobox', main_app_id='".$app->f('app_id')."', active='".$in['export_pinv_to_billtobox']."', api='".$in['access_key_pinv']."' ");
	            }else{
	              $this->db->query("UPDATE apps SET active='".$in['export_pinv_to_billtobox']."', api='".$in['access_key_pinv']."' WHERE app_id='".$export_pinv_to_billtobox."'");
	            } 
	           msg::success ( gm("Changes Saved."),'success');  
	        }else{
	        
	      		if($in['from_connect']){
	      			return false;
	      		}
	      		
	        }
	       
	      }elseif(!$in['access_key_pinv'] && $in['export_pinv_to_billtobox']){
	      		msg::error(gm('This field is required'),"access_key_pinv");
	      
	      		if($in['from_connect']){
	      			return false;
	      		}
	      		
	      }

	      if(empty(msg::$errors)){
	      	 msg::success ( gm("Changes Saved."),'success');  
	      }


		$in['done']=0;
		$in['xget']='apps';
		return true;
	} 

	function check_access_key($access_key, $owner){
      global $config;

      //$billtobox_ar_out_url = $config['billtobox_ar_out_url_test'];
      $billtobox_ar_out_url = $config['billtobox_ar_out_url'];
      $in['username'] = $config['billtobox_ar_out_user'];
          if(DATABASE_NAME =='4a5f9937_4f34_21bb_631eb43c4876' || DATABASE_NAME =='salesassist_2'){
            $billtobox_ar_out_url = $config['billtobox_ar_out_url_test'];
            $in['username'] ='aktiTest';
      }

      $ch = curl_init();

      $data=array(
        'owner'                 => $owner,
        'owner_id_type'         => 'VAT',
        'access_key'            => $access_key,
        'action_type'           => 'all', 
        'limit'                 => 1    
      );
    
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_USERPWD, $in['username'].":".$access_key);
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      curl_setopt($ch, CURLOPT_URL, $billtobox_ar_out_url);

      $put = curl_exec($ch);
      $info = curl_getinfo($ch);


      if($put){
	      	if($info['http_code']==401){
	        $err=json_decode($put);
	        msg::error(gm('The authentication failed. Check API key.'),"access_key");
	        return false;

	      }elseif($info['http_code']==404){
	        //$err=json_decode($put);
	        $err = $put;
	        msg::error(gm('Error').' - '.$err,"access_key");
	        return false;

	      }else{	         
	        return true;
	      }
      }else{
      	msg::error(gm('The authentication failed. Check API key.'),"access_key");
	    return false;
      }
     
    }

    function check_access_key_pinv($access_key, $owner){
      global $config;

      //$billtobox_ar_out_url = $config['billtobox_ar_out_url_test'];
      $billtobox_base_url_ap = $config['billtobox_base_url_ap'];
      $in['username'] = $config['billtobox_ar_out_user'];
          if(DATABASE_NAME =='4a5f9937_4f34_21bb_631eb43c4876' || DATABASE_NAME =='salesassist_2'){
            $billtobox_base_url_ap = $config['billtobox_base_url_ap_test'];
            $in['username'] ='aktiTest';
      }

      $ch = curl_init();
    
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_USERPWD, $in['username'].":".$access_key);
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      curl_setopt($ch, CURLOPT_URL, $billtobox_base_url_ap);

      //var_dump($ch, $billtobox_base_url_ap, $access_key, $in['username']);exit();
      $put = curl_exec($ch);
      $info = curl_getinfo($ch);


      if($put){
	      	if($info['http_code']==401){
	        $err=json_decode($put);
	        msg::error(gm('The authentication failed. Check API key.'),"access_key_pinv");
	        return false;

	      }elseif($info['http_code']==404){
	        //$err=json_decode($put);
	        $err = $put;
	        msg::error(gm('Error').' - '.$err,"access_key_pinv");
	        return false;

	      }else{	         
	        return true;
	      }
      }else{
      	msg::error(gm('The authentication failed. Check API key.'),"access_key_pinv");
	    return false;
      }
     
    }

    function check_api_key($access_key, $owner){
      global $config;

      $billtobox_base_url = $config['billtobox_base_url'];
      $in['username'] = $config['billtobox_ar_out_user'];
          if(DATABASE_NAME =='4a5f9937_4f34_21bb_631eb43c4876' || DATABASE_NAME =='salesassist_2'){
            $billtobox_base_url = $config['billtobox_base_url_test'];
            $in['username'] ='aktiTest';
      }

      $ch = curl_init();

      $headers=array('Content-Type: multipart/form-data');
		
    	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);   	
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($ch, CURLOPT_USERPWD, $in['username'].":".$access_key);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_URL, $billtobox_base_url);

      $put = curl_exec($ch);
      $info = curl_getinfo($ch);


      if($put){
	      	if($info['http_code']==401){
	        $err=json_decode($put);
	        msg::error(gm('The authentication failed. Check API key.'),"api_key");
	        return false;

	      }elseif($info['http_code']==404){
	        //$err=json_decode($put);
	        $err = $put;
	        msg::error(gm('Error').' - '.$err,"api_key");
	        return false;

	      }else{	         
	        return true;
	      }
      }else{
      	msg::error(gm('Error'),"api_key");
	    return false;
      }
     
    }


	function billtobox_disconnect(&$in){
		$billtobox_id=$this->db->field("SELECT app_id FROM apps WHERE name='BillToBox' AND type='main' AND main_app_id='0' ");
		$this->db->query("UPDATE apps SET active='0', api ='' WHERE app_id='".$billtobox_id."' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name = 'EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE' ");
		$this->db->query("UPDATE settings SET value='' WHERE constant_name = 'EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE_KEY' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name = 'EXPORT_P_INVOICE_TO_BILLTOBOX' ");
		$this->db->query("UPDATE settings SET value='' WHERE constant_name = 'EXPORT_P_INVOICE_TO_BILLTOBOX_KEY' ");

		$this->db->query("UPDATE apps SET api='', active='0' WHERE main_app_id='".$billtobox_id."' AND type='export_billtobox_to_dropbox'  ");
		$this->db->query("UPDATE apps SET api='', active='0' WHERE main_app_id='".$billtobox_id."' AND type='export_pinv_to_billtobox'  ");

		msg::success ( gm("Account deactivated"),'success');
		$in['done']=0;
		$in['xget']='apps';
		return true;
	}

	function billtobox_connect_validate(&$in){

		$app = $this->db->field("SELECT app_id FROM apps WHERE name='BillToBox' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
		if(!$app){
			msg::error(gm('Error'),'error');
			json_out($in);
		}

		$v=new validation($in);
		//$v->field('username',gm('Username'),'required');
		if($in['sendSalesInvoice']){
			$v->field('active_sales_api_key',gm('Api key'),'required');
		}else{
			$v->field('api_key',gm('Api key'),'required');
		}	
		if($in['export_billtobox_to_dropbox'] || $in['exportPurchaseInvoices'] =='1'){
			$v->field('access_key',gm('Api key'),'required');
		}elseif($in['export_pinv_to_billtobox'] || $in['exportPurchaseInvoices'] =='2'){
			$v->field('access_key_pinv',gm('Api key'),'required');
		}
		return $v->run();
	}

	function billtobox_connect(&$in){
		global $config;
		if(!$this->billtobox_connect_validate($in)){
			json_out($in);
			return false;
		}

		$billtobox_base_url = $in['sendSalesInvoice'] == '1' ? $config['billtobox_base_url_active'] : $config['billtobox_base_url'];
		$in['username'] ='aktiProd';
      	if(DATABASE_NAME =='4a5f9937_4f34_21bb_631eb43c4876' || DATABASE_NAME =='salesassist_2' || DATABASE_NAME =='SalesAssist_11'){
      		$billtobox_base_url = $config['billtobox_base_url_test'];
      		$in['username'] ='aktiTest';
		}
		
        $data=array(
			'file'				=> ''
			);

		$ch = curl_init();
    	$headers=array('Content-Type: multipart/form-data');
		$api_key=$in['sendSalesInvoice'] == '1' ? $in['active_sales_api_key'] : $in['api_key'];
    	
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);   	
    	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($ch, CURLOPT_USERPWD, $in['username'].":".$api_key);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_URL, $billtobox_base_url);

    	$put = curl_exec($ch);
    	$info = curl_getinfo($ch);
 
    	if($info['http_code']==401){
    		$err=json_decode($put);
    		msg::error(gm('The authentication failed. Check API key.'),"api_key");
    		$in['from_connect']=1;
			$this->billtobox_activate_export($in);
    		json_out($in);
    	}elseif($info['http_code']==404){
    		$err=json_decode($put);
    		msg::error(gm('Error'),"error");
    		json_out($in);
    	}else{
    		$in['from_connect']=1;
    		if(!$this->billtobox_activate_export($in)){
    			json_out($in);
    		}
    		$sendSalesInvoice=$this->db->field("SELECT value FROM settings WHERE constant_name='EXPORT_SEND_INVOICE_BILLTOBOX' ");
			if(is_null($sendSalesInvoice)){
				$this->db->query("INSERT INTO settings SET constant_name='EXPORT_SEND_INVOICE_BILLTOBOX', value='".$in['sendSalesInvoice']."' ");
			}else{
				$this->db->query("UPDATE settings SET value='".$in['sendSalesInvoice']."' WHERE constant_name='EXPORT_SEND_INVOICE_BILLTOBOX'");
			}
    		$billtobox_id=$this->db->field("SELECT app_id FROM apps WHERE name='BillToBox' AND type='main' AND main_app_id='0' ");
    		$this->db->query("UPDATE apps SET active='1', api='".trim($in['api_key'])."' WHERE app_id='".$billtobox_id."' ");
    		$user=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$billtobox_id."' AND type='username' ");
    		if(!$user){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$billtobox_id."', api='".trim($in['username'])."', type='username' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".trim($in['username'])."' WHERE main_app_id='".$billtobox_id."' AND type='username' ");
    		}
    		$pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$billtobox_id."' AND type='password' ");
    		if(!$pass){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$billtobox_id."', api='".trim($in['api_key'])."', type='password' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".trim($in['api_key'])."' WHERE main_app_id='".$billtobox_id."' AND type='password' ");
    		}
    		$activeSalesApiKey=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$billtobox_id."' AND type='active_sales_api_key' ");
    		if(!$activeSalesApiKey){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$billtobox_id."', api='".trim($in['active_sales_api_key'])."', type='active_sales_api_key' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".trim($in['active_sales_api_key'])."' WHERE main_app_id='".$billtobox_id."' AND type='active_sales_api_key' ");
    		}
    		require_once(__DIR__.'/ExportAuto.php');
			$exportAuto=new ExportAuto();
			$in['module']='billtobox';
			$in['only_return']=true;
			$in['invoice']=$in['exportAutoInvoice'];
			$in['pinvoice']=$in['exportAutoPInvoice'];
			$exportAuto->updateExportAutoInvoice($in);	

	      	mark_invoices_as_exported('billtobox', true);

    		msg::success ( gm('BillToBox activated'),'success');
    		return true;
    	}
    	
		
		$in['xget']='apps';
		return true;
	}
	function icepay_connect(&$in){
		global $cfg;
		$time1=date("Y-m-d",time());
		$time2=date("H:i:s",time());
		$time=$time1."ETC".$time2."+7200";
		$ch = curl_init();

		$data=array("Timestamp"=>$time);
		$data = json_encode($data);
		$anot=utf8_encode($cfg["icepay_base_url"]."/payment/getmypaymentmethods/POST".$in['merchant_id'].$in['api_key'].$data);
		$basestring=hash('sha256',utf8_encode($cfg["icepay_base_url"]."/payment/getmypaymentmethods/POST".$in['merchant_id'].$in['api_key'].$data));
		$headers=array("Content-Type: application/json", "MerchantID: ".$in['merchant_id'], "Checksum: ".$basestring);

        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        	curl_setopt($ch, CURLOPT_POST, true);
        	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
       	curl_setopt($ch, CURLOPT_URL, $cfg["icepay_base_url"]."/payment/getmypaymentmethods/");

	  	$put = curl_exec($ch);
 		$info = curl_getinfo($ch);
 		if($info['http_code']>300 || $info['http_code']==0){
 			msg::error ( gm("Incorrect data submitted"),'error');
 		}else{
 			$in['done']=1;
 			$icepay_id=$this->db->field("SELECT app_id FROM apps WHERE name='Icepay' AND type='main' AND main_app_id='0' ");
			$this->db->query("UPDATE apps SET api='".$in['api_key']."', active='1' WHERE app_id='".$icepay_id."' ");
			$merchant_id=$this->db->field("SELECT app_id FROM apps WHERE main_app_id='".$icepay_id."' AND type='merchant_id' ");
			if($merchant_id){
				$this->db->query("UPDATE apps SET api='".$in['merchant_id']."' WHERE app_id='".$merchant_id."' ");
			}else{
				$this->db->query("INSERT INTO apps SET api='".$in['merchant_id']."', main_app_id='".$icepay_id."', type='merchant_id' ");
			}
			//update in settings for webshop
			$settings_ice_secret=$this->db->field("SELECT value FROM settings WHERE constant_name='ICEPAY_SECRETCODE' ");
			if($settings_ice_secret){
				$this->db->query("UPDATE settings SET value='".$in['api_key']."' WHERE constant_name='ICEPAY_SECRETCODE' ");
			}else{
				$this->db->query("INSERT INTO settings SET constant_name='ICEPAY_SECRETCODE', value='".$in['api_key']."', type='1' ");
			}
			$settings_ice_merchant=$this->db->field("SELECT value FROM settings WHERE constant_name='ICEPAY_MERCHANTID' ");
			if($settings_ice_merchant){
				$this->db->query("UPDATE settings SET value='".$in['merchant_id']."' WHERE constant_name='ICEPAY_MERCHANTID' ");
			}else{
				$this->db->query("INSERT INTO settings SET constant_name='ICEPAY_MERCHANTID', value='".$in['merchant_id']."', type='1' ");
			}
			//update in settings for weblink
			$settings_ice_secret2=$this->db->field("SELECT value FROM settings WHERE constant_name='ICEPAY_SECRETCODE2' ");
			if($settings_ice_secret2){
				$this->db->query("UPDATE settings SET value='".$in['api_key']."' WHERE constant_name='ICEPAY_SECRETCODE2' ");
			}else{
				$this->db->query("INSERT INTO settings SET constant_name='ICEPAY_SECRETCODE2', value='".$in['api_key']."', type='1' ");
			}
			$settings_ice_merchant2=$this->db->field("SELECT value FROM settings WHERE constant_name='ICEPAY_MERCHANTID2' ");
			if($settings_ice_merchant2){
				$this->db->query("UPDATE settings SET value='".$in['merchant_id']."' WHERE constant_name='ICEPAY_MERCHANTID2' ");
			}else{
				$this->db->query("INSERT INTO settings SET constant_name='ICEPAY_MERCHANTID2', value='".$in['merchant_id']."', type='1' ");
			}
 			msg::success ( gm("Account on Icepay activated"),'success');
 		}
 		$in['xget']='apps';
		return true;
	}
	function icepay_disconnect(&$in){
		$icepay_id=$this->db->field("SELECT app_id FROM apps WHERE name='Icepay' AND type='main' AND main_app_id='0' ");
		$this->db->query("UPDATE apps SET active='0' WHERE app_id='".$icepay_id."' ");
		msg::success ( gm("Account deactivated"),'success');
		$in['done']=0;
		$in['xget']='apps';
		return true;
	}
	function lengow_connect(&$in){
		global $cfg;
		$time1=date("Y-m-d",time());
		$time2=date("H:i:s",time());
		$time=$time1."ETC".$time2."+7200";
                
            $headers = array();
			$ch = curl_init();
            $data = array('access_token'=>$in['access_token'],'secret'=>$in['secret']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_URL, 'http://api.lengow.io/access/get_token');
            $put = curl_exec($ch);
	 		$info = curl_getinfo($ch);
            $in['done']=1;
 			$value=$in['access_token'].';'.$in['secret'].';';
 			$lengow=$this->db->field("SELECT app_id FROM apps WHERE name='Lengow' AND type='main' AND main_app_id='0' ");
			$this->db->query("UPDATE apps SET api='".$value."', active='1' WHERE app_id='".$lengow."' ");
			
		msg::success ( gm("Account on Lengow activated"),'success');
		$in['xget']='apps';
		return true;
	}
	function lengow_disconnect(&$in){
		$lengow=$this->db->field("SELECT app_id FROM apps WHERE name='Lengow' AND type='main' AND main_app_id='0' ");
		$this->db->query("UPDATE apps SET active='0' WHERE app_id='".$lengow."' ");
		msg::success ( gm("Account deactivated"),'success');
		$in['done']=0;
		$in['xget']='apps';
		return true;
	}
	function user_accees_if_acc_manag(&$in)
	{
/*		var_dump($in['only_if_acc_manag']);
		var_dump($in['only_if_acc_manag2']);
		var_dump($in['only_if_acc_manag3']);*/
		$only_if_acc_manag=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ONLY_IF_ACC_MANAG'");
	      if(!is_null($only_if_acc_manag)){
	        $this->db->query("UPDATE settings SET value='0' WHERE `constant_name`='ONLY_IF_ACC_MANAG'");
	      }else{
	        $this->db->query("INSERT INTO settings SET `constant_name`='ONLY_IF_ACC_MANAG',value='0', type='1' ");
	      }

	    $only_if_acc_manag2=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ONLY_IF_ACC_MANAG2'");
	      if(!is_null($only_if_acc_manag2)){
	        $this->db->query("UPDATE settings SET value='0' WHERE `constant_name`='ONLY_IF_ACC_MANAG2'");
	      }else{
	        $this->db->query("INSERT INTO settings SET `constant_name`='ONLY_IF_ACC_MANAG2',value='0', type='1' ");
	      }

	    $only_if_acc_manag3=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ONLY_IF_ACC_MANAG3'");
	      if(!is_null($only_if_acc_manag3)){
	        $this->db->query("UPDATE settings SET value='0' WHERE `constant_name`='ONLY_IF_ACC_MANAG3'");
	      }else{
	        $this->db->query("INSERT INTO settings SET `constant_name`='ONLY_IF_ACC_MANAG3',value='0', type='1' ");
	      }

		/*$this->db->query("UPDATE settings SET value='0' WHERE constant_name = 'ONLY_IF_ACC_MANAG' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name = 'ONLY_IF_ACC_MANAG2' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name = 'ONLY_IF_ACC_MANAG3' ");*/

		$no_access_view_article=$this->db->field("SELECT value FROM settings WHERE `constant_name`='NO_ACCESS_VIEW_ARTICLE'");
	      if(!is_null($no_access_view_article)){
	        $this->db->query("UPDATE settings SET value='0' WHERE `constant_name`='NO_ACCESS_VIEW_ARTICLE'");
	      }else{
	        $this->db->query("INSERT INTO settings SET `constant_name`='NO_ACCESS_VIEW_ARTICLE',value='0', type='1' ");
	      }

	    $hide_profit_margin=$this->db->field("SELECT value FROM settings WHERE `constant_name`='HIDE_PROFIT_MARGIN'");
	      if(!is_null($hide_profit_margin)){
	        $this->db->query("UPDATE settings SET value='0' WHERE `constant_name`='HIDE_PROFIT_MARGIN'");
	      }else{
	        $this->db->query("INSERT INTO settings SET `constant_name`='HIDE_PROFIT_MARGIN',value='0', type='1' ");
	      }

	    $only_if_int_user=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ONLY_IF_INT_USER'");
	      if(!is_null($only_if_int_user)){
	        $this->db->query("UPDATE settings SET value='0' WHERE `constant_name`='ONLY_IF_INT_USER'");
	      }else{
	        $this->db->query("INSERT INTO settings SET `constant_name`='ONLY_IF_INT_USER',value='0', type='1' ");
	      }

	      $NO_ACCESS_PRICE_ARTICLE_INT_REG_USER=$this->db->field("SELECT value FROM settings WHERE `constant_name`='NO_ACCESS_PRICE_ARTICLE_INT_REG_USER'");
	      if(!is_null($NO_ACCESS_PRICE_ARTICLE_INT_REG_USER)){
	        $this->db->query("UPDATE settings SET value='0' WHERE `constant_name`='NO_ACCESS_PRICE_ARTICLE_INT_REG_USER'");
	      }else{
	        $this->db->query("INSERT INTO settings SET `constant_name`='NO_ACCESS_PRICE_ARTICLE_INT_REG_USER',value='0', type='1' ");
	      }

		if($in['only_if_acc_manag']==1){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ONLY_IF_ACC_MANAG' ");
		}
		if($in['only_if_acc_manag2']==1){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ONLY_IF_ACC_MANAG2' ");
		}
		if($in['only_if_acc_manag3']==1){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ONLY_IF_ACC_MANAG3' ");
		}
		if($in['no_access_view_article']==1){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'NO_ACCESS_VIEW_ARTICLE' ");
		}
		if($in['hide_profit_margin']==1){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'HIDE_PROFIT_MARGIN' ");
		}
		if($in['only_if_int_user']==1){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ONLY_IF_INT_USER' ");
		}
		if($in['no_access_price_article_int_reg_user']==1){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'NO_ACCESS_PRICE_ARTICLE_INT_REG_USER' ");
		}

		msg::success ( gm("Setting updated"),'success');
		return true;
	}
	function multiple_id_delete(&$in)
	{
		if(!$in['identity_id']){
			msg::$error = gm("Invalid ID.");
			return false;
		}

		$logo = $this->db->field("SELECT company_logo FROM multiple_identity WHERE identity_id='".$in['identity_id']."' ");

		if($logo){
			unlink($logo);
		}
		$this->db->query("DELETE FROM multiple_identity WHERE identity_id='".$in['identity_id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function add_identity(&$in){
		if(!$this->add_identity_validate($in)){
			return false;
		}
		$in['identity_id'] = $this->db->insert("INSERT INTO multiple_identity SET
                                                    identity_name  			= '".$in['identity_name']."',
                                                    company_name    		= '".$in['company_name']."',
                                                    company_email 			= '".$in['company_email']."',
                                                    company_url 			= '".$in['company_url']."',
                                                    company_description 	= '".$in['company_description']."',
													bank_name_identity  	= '".$in['bank_name_identity']."',
													bank_bic_code_identity 	= '".$in['bank_bic_code_identity']."',
													bank_iban_identity 	    = '".$in['bank_iban_identity']."',
													company_reg_number 		= '".$in['company_reg_number']."',
													company_phone 			= '".$in['company_phone']."',
													company_fax				= '".$in['company_fax']."',
													company_siret			= '".$in['company_siret']."',
													company_rpr				= '".$in['company_rpr']."',
													company_den_sociale		= '".$in['company_den_sociale']."',
													company_reg_commerce 	= '".$in['company_reg_commerce']."',
													company_cap_social 		= '".$in['company_cap_social']."',
													company_code_naf 		= '".$in['company_code_naf']."',
													company_juridical_form 	= '".$in['company_juridical_form']."',
													company_logo			= '".$in['image']."',
													country_id 				= '".$in['country_id']."',
													company_address 		= '".$in['company_address']."',
													company_zip 			= '".$in['company_zip']."',
													city_name 				= '".$in['city_name']."' ");
		

		if($in['image']!='images/no-logo.png'){
			$in['temp_filename'] = $in['image'];
		
			$ext= substr($in['image'],strripos($in['image'],'.'));
			$dbname=DATABASE_NAME;
			$pos = strlen($dbname) + 26;
			$in['image'] = substr_replace($in['image'],$in['identity_id'],$pos);
			$in['image'] .= $ext;
			
			$this->db->query("UPDATE multiple_identity SET company_logo			= '".$in['image']."'
									WHERE identity_id = '".$in['identity_id']."'");

			rename($in['temp_filename'],$in['image']);
		}
		
		msg::success ( gm("Changes have been saved."),'success');
		if($in['return_data']){
			json_out($in);
		}else{
			return true;	
		}
	
	}
	
	function add_identity_validate(&$in)
	{
		$v = new validation($in);

		$v->field('country_id', gm('ID'), "required");
		$v->field('identity_name', gm('Identity name'), "required");
		return $v->run();
	}
	function update_identity(&$in){
		if(!$this->update_identity_validate($in)){
			return false;
		}

		$this->db->query("UPDATE multiple_identity SET
 										identity_name  			= '".$in['identity_name']."',
                                        company_name    		= '".$in['company_name']."',
                                        company_email 			= '".$in['company_email']."',
                                        company_url 			= '".$in['company_url']."',
                                        company_description 	= '".$in['company_description']."',
										bank_name_identity  	= '".$in['bank_name_identity']."',
										bank_bic_code_identity 	= '".$in['bank_bic_code_identity']."',
										bank_iban_identity 	    = '".$in['bank_iban_identity']."',
										company_reg_number 		= '".$in['company_reg_number']."',
										company_phone 			= '".$in['company_phone']."',
										company_fax				= '".$in['company_fax']."',
										company_siret			= '".$in['company_siret']."',
										company_rpr				= '".$in['company_rpr']."',
										company_den_sociale		= '".$in['company_den_sociale']."',
										company_reg_commerce 	= '".$in['company_reg_commerce']."',
										company_cap_social 		= '".$in['company_cap_social']."',
										company_code_naf 		= '".$in['company_code_naf']."',
										company_juridical_form 	= '".$in['company_juridical_form']."',
										company_address 		= '".$in['company_address']."',
										company_zip 			= '".$in['company_zip']."',
										city_name 				= '".$in['city_name']."',
										country_id 				= '".$in['country_id']."'

								WHERE identity_id = '".$in['identity_id']."'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;

	}
	function update_identity_validate(&$in)
	{
		$v = new validation($in);
		$v->field('country_id', gm('ID'), "required");
		$v->field('identity_name', gm('Identity name'), "required");
		return $v->run();
	}
	function uploadify(&$in)
	{
		$response=array();
		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];

			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif','png'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			
			$targetPath = 'upload/'.DATABASE_NAME.'/identity/';
			$in['name'] = 'identity_'.$in['identity_id'].'.'.$fileParts['extension'];
			if($in['identity_id']=='tmp'){
				$in['name'] = 'identity_tmp_'.time().'.'.$fileParts['extension'];
			}
			
			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];

			@mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {

				move_uploaded_file($tempFile,$targetFile);
				global $database_config;

				$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => DATABASE_NAME,
				);

				$db_upload = new sqldb($database_2);
				$logo = $db_upload->field("SELECT company_logo FROM multiple_identity WHERE identity_id='".$in['identity_id']."' ");
		
					if($in['identity_id']!='tmp'){
						$db_upload->query("UPDATE multiple_identity SET
			                           company_logo = '".$targetFile."'
			                           WHERE identity_id='".$in['identity_id']."'");
					}
				$response['file_name'] = $in['name'];
				$response['success'] = 'success';
        			echo json_encode($response);
				// ob_clean();
			} else {
				$response['error'] = gm('Invalid file type.');
        			echo json_encode($response);
				// echo gm('Invalid file type.');
			}
		}
	}
	function uploadify1(&$in)
	{
		$response=array();
		if (!empty($_FILES)) {
			$tempFile = $_FILES['file']['tmp_name'][0];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
			$targetPath = 'upload/'.DATABASE_NAME.'/identity/';
			$in['name'] = 'identity_'.$in['identity_id'].'.jpg';
			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif'); // File extensions
			$fileParts = pathinfo($_FILES['file']['name'][0]);
			@mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {

				move_uploaded_file($tempFile,$targetFile);
				global $database_config;

				$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => DATABASE_NAME,
				);

				$db_upload = new sqldb($database_2);
				$logo = $db_upload->field("SELECT company_logo FROM multiple_identity WHERE identity_id='".$in['identity_id']."' ");
		
					if($in['identity_id']){
						$db_upload->query("UPDATE multiple_identity SET
			                           company_logo = '".$targetFile."'
			                           WHERE identity_id='".$in['identity_id']."'");
					}else{
						
						$db_upload->query("INSERT INTO multiple_identity SET
		                           company_logo = 'upload/".DATABASE_NAME."/identity/".$in['name']."',
		                           identity_id='".$in['identity_id']."' ");
					}
				$response['success'] = 'success';
        			/*echo json_encode($response);*/
				// ob_clean();
			} else {
				$response['error'] = gm('Invalid file type.');
        			/*echo json_encode($response);*/
				// echo gm('Invalid file type.');
			}
		}
		return true;
	}
	function upload1(&$in)
	{

		$response=array();
		if (!empty($_FILES)) {
			$tempFile = $_FILES['file']['tmp_name'][0];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
			$targetPath = 'upload/'.DATABASE_NAME.'/';
			$in['name'] = 'logo_img_'.DATABASE_NAME.'.jpg';
			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
			// Validate the file type

			$fileTypes = array('jpg','jpeg','gif','png'); // File extensions
			$fileParts = pathinfo($_FILES['file']['name'][0]);
			mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
				move_uploaded_file($tempFile,$targetFile);
					global $database_config;
					$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => DATABASE_NAME,
					);
					$db_upload = new sqldb($database_2);

				foreach (array('ACCOUNT_LOGO','ACCOUNT_LOGO_QUOTE','ACCOUNT_LOGO_ORDER','ACCOUNT_LOGO_WEBSHOP','ACCOUNT_LOGO_PO_ORDER','ACCOUNT_LOGO_TIMESHEET','ACCOUNT_MAINTENANCE_LOGO') as $constant){
					$db_upload->query("UPDATE settings SET
			                           value = 'upload/".DATABASE_NAME."/".$in['name']."',
			                           long_value=1
			                           WHERE constant_name='".$constant."'");
				}
				$size = getimagesize("../../upload/".$in['folder']."/".$in['name']);
		    	$ratio = 250 / 77;

				if($size[0]/$size[1] > $ratio ){
					$attr = 'width="250"';
				}else{
					$attr = 'height="77"';
				}
				$response['success'] = $attr;
				msg::success($attr,'success');
				// echo json_encode($response);
			} else {
				$response['error'] = gm('Invalid file type.');
				// echo json_encode($response);
				msg::error($attr,'error');
			}
		}
		return true;
	}

	function upload(&$in)
	{
		$response=array();
		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
			$targetPath = 'upload/'.DATABASE_NAME.'/';
			$in['name'] = 'logo_img_'.DATABASE_NAME.'.jpg';
			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
			// Validate the file type

			$fileTypes = array('jpg','jpeg','gif','png'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
				move_uploaded_file($tempFile,$targetFile);
					global $database_config;
					$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => DATABASE_NAME,
					);
					$db_upload = new sqldb($database_2);

				foreach (array('ACCOUNT_LOGO','ACCOUNT_LOGO_QUOTE','ACCOUNT_LOGO_ORDER','ACCOUNT_LOGO_WEBSHOP','ACCOUNT_LOGO_PO_ORDER','ACCOUNT_LOGO_TIMESHEET','ACCOUNT_MAINTENANCE_LOGO') as $constant){
					$db_upload->query("UPDATE settings SET
			                           value = 'upload/".DATABASE_NAME."/".$in['name']."',
			                           long_value=1
			                           WHERE constant_name='".$constant."'");
				}
				$size = getimagesize("../../upload/".$in['folder']."/".$in['name']);
		    	$ratio = 250 / 77;

				if($size[0]/$size[1] > $ratio ){
					$attr = 'width="250"';
				}else{
					$attr = 'height="77"';
				}
				$response['success'] = $attr;
				msg::success($attr,'success');
				// echo json_encode($response);
			} else {
				$response['error'] = gm('Invalid file type.');
				// echo json_encode($response);
				msg::error($attr,'error');
			}
		}
		return true;
	}
	function financial_settings(&$in)
	{  
		if($in['ARTICLE_PRICE_COMMA_DIGITS']>4){
			msg::error ( gm("Number of digits after comma for unit price (max.4)"),'error');
			return false;
		}else{
			$this->db->query("UPDATE settings SET value='".$in['ARTICLE_PRICE_COMMA_DIGITS']."' WHERE constant_name='ARTICLE_PRICE_COMMA_DIGITS'");
		}
		$this->db->query("UPDATE settings SET value = '".$in['MONTH_ID']."' WHERE constant_name='ACCOUNT_FISCAL_YEAR_START' ");
		$this->db->query("UPDATE settings SET value = '".$in['CURRENCY_TYPE_ID']."' WHERE constant_name='ACCOUNT_CURRENCY_TYPE' ");
		$this->db->query("UPDATE settings SET value='".$in['payment_term']."' WHERE constant_name='PAYMENT_TERM'");
		$this->db->query("UPDATE settings SET value='".$in['choose_payment_term_type']."' WHERE constant_name = 'PAYMENT_TERM_TYPE' ");

		$profit_margin_type=$this->db->field("SELECT value FROM settings WHERE `constant_name`='PROFIT_MARGIN_TYPE'");
	      if(!is_null($profit_margin_type)){
	        $this->db->query("UPDATE settings SET value='".$in['profit_margin_type']."' WHERE `constant_name`='PROFIT_MARGIN_TYPE'");
	      }else{
	        $this->db->query("INSERT INTO settings SET `constant_name`='PROFIT_MARGIN_TYPE',value='0', type='1' ");
	      }

	    $qrcode_purchaseinv=$this->db->field("SELECT value FROM settings WHERE `constant_name`='QRCODE_PURCHASE_INVOICE'");
	      if(!is_null($qrcode_purchaseinv)){
	        $this->db->query("UPDATE settings SET value='".$in['qrcode_purchaseinv']."' WHERE `constant_name`='QRCODE_PURCHASE_INVOICE'");
	      }else{
	        $this->db->query("INSERT INTO settings SET `constant_name`='QRCODE_PURCHASE_INVOICE',value='".$in['qrcode_purchaseinv']."', type='1' ");
	      }

	     if($in['loopfinancial']){
	     	foreach($in['loopfinancial'] as $key => $value){
				if(return_value($value['FINANCIAL_NR']) != return_value('1,00')){
				
					if($value['currency_id'] && $in['CURRENCY_TYPE_ID']){

						$rate=$this->db->field("SELECT currency_rate_id FROM currency_rates WHERE currency_id_base='".$in['CURRENCY_TYPE_ID']."' AND currency_id_to_change='".$value['currency_id']."' ");
					      if(!is_null($rate)){
					        $this->db->query("UPDATE currency_rates SET rate='".return_value($value['FINANCIAL_NR'])."' WHERE `currency_rate_id`='".$rate."' ");
					      }else{
					       $this->db->query("INSERT INTO currency_rates SET `currency_id_base`='".$in['CURRENCY_TYPE_ID']."', currency_id_to_change='".$value['currency_id']."', rate='".return_value($value['FINANCIAL_NR'])."', last_update='".time()."' ");
					      }

					}

				}
			}

	     }

		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function edebex_connect(){
		$this->db->query("UPDATE apps SET active='1' WHERE name='Edebex' AND type='main' AND main_app_id='0'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function edebex_disconnect(){
		$this->db->query("UPDATE apps SET active='0' WHERE name='Edebex' AND type='main' AND main_app_id='0'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function instapaid_connect(){
		$this->db->query("UPDATE apps SET active='1' WHERE name='Instapaid' AND type='main' AND main_app_id='0'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function instapaid_disconnect(){
		$this->db->query("UPDATE apps SET active='0' WHERE name='Instapaid' AND type='main' AND main_app_id='0'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function mollie_connect(&$in){
		if(strpos($in['api_key'],'test') === false){
			global $config;
    		$ch = curl_init();

    		$headers=array('Content-Type: application/json','Authorization: Bearer '.$in['api_key']);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
          	//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          	curl_setopt($ch, CURLOPT_URL, $config['mollie_base_url'].'/payments');

    		$put = curl_exec($ch);
    		$info = curl_getinfo($ch);

    		if($info['http_code']>300 || $info['http_code']==0){
    			msg::error(gm("Invalid key"),"error");
    			json_out($in);
    		}else{
    			$this->db->query("UPDATE apps SET active='1', api='".$in['api_key']."' WHERE name='Mollie' AND type='main' AND main_app_id='0'");
    			/*msg::success(gm("Mollie application activated"),"success");*/
    			msg::success ( gm("Changes have been saved."),'success');
    			return true;
    		}
		}else{
			/*msg::error(gm('Please insert a valid live API key'),"error");*/
			msg::error(gm('Invalid key'),"error");
			json_out($in);
		}
	}

	function mollie_disconnect(){
		$this->db->query("UPDATE apps SET active='0' WHERE name='Mollie' AND type='main' AND main_app_id='0'");
		/*msg::success(gm("Mollie application deactivated"),"success");*/
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function smart_connect(&$in){
		global $config;
    	$ch = curl_init();

    	$headers=array('Content-Type: application/json','Authorization: Bearer '.$in['token_id']);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $config['smart_base_url'].'/sheets');

    	$put = curl_exec($ch);
    	$info = curl_getinfo($ch);

    	if($info['http_code']>300 || $info['http_code']==0){
    		$err=json_decode($put);
    		msg::error($err->message,"error");
    		json_out($in);
    	}else{
    		$this->db->query("UPDATE apps SET active='1', api='".$in['token_id']."' WHERE name='Smartsheet' AND type='main' AND main_app_id='0'");
    		/*msg::success(gm("Smartsheet application activated"),"success");*/
    		msg::success ( gm("Changes have been saved."),'success');
    		return true;
    	}
	}

	function smart_disconnect(){
		$this->db->query("UPDATE apps SET active='0' WHERE name='Smartsheet' AND type='main' AND main_app_id='0'");
		/*msg::success(gm("Smartsheet application deactivated"),"success");*/
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function save_payment_details(&$in)
	{
		if(!$in['account_billing_country_id']){
			msg::error(gm('Please select a country'),"error");
			json_out($in);
		}
		$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

		if(in_array(get_country_code($in['account_billing_country_id']), $eu_countries) ===false){
			$this->db_users->query("UPDATE users SET valid_vat='' WHERE `database_name`='".DATABASE_NAME."' ");
			//$this->db_users->query("UPDATE customers SET btw_nr='' WHERE customer_id='".$in['customer_id']."' ");
		}
		if($in['name']){$filter_name= "name='".$in['name']."',";}
			$filter_city= "city_name='".$in['account_billing_city']."',";
			$filter_country= "country_name='".get_country_name($in['account_billing_country_id'])."'";

			if(!$in['account_billing_city']&&!$in['account_billing_country_id']){$filter_name=rtrim($filter_name,',');}
			if(!$in['account_billing_country_id']){$filter_city=rtrim($filter_city,',');}

			if($filter_name||$filter_city||$filter_country){
				/*$this->db_users->query("UPDATE users SET
						ACCOUNT_COMPANY='".$in['name']."',
						ACCOUNT_BILLING_CITY='".$in['account_billing_city']."',
						ACCOUNT_BILLING_COUNTRY_ID='".$in['account_billing_country_id']."',
						ACCOUNT_BILLING_ADDRESS='".utf8_decode($in['account_billing_address'])."',
						ACCOUNT_BILLING_ZIP='".$in['account_billing_zip']."'
						WHERE `database_name`='".DATABASE_NAME."' ");*/
				$this->db_users->query("UPDATE users SET
						ACCOUNT_COMPANY= :ACCOUNT_COMPANY,
						ACCOUNT_BILLING_CITY= :ACCOUNT_BILLING_CITY,
						ACCOUNT_BILLING_COUNTRY_ID= :ACCOUNT_BILLING_COUNTRY_ID,
						ACCOUNT_BILLING_ADDRESS= :ACCOUNT_BILLING_ADDRESS,
						ACCOUNT_BILLING_ZIP= :ACCOUNT_BILLING_ZIP
						WHERE `database_name`= :database_name ",
					['ACCOUNT_COMPANY'=>$in['name'],
					 'ACCOUNT_BILLING_CITY'=>$in['account_billing_city'],
					 'ACCOUNT_BILLING_COUNTRY_ID'=>$in['account_billing_country_id'],
					 'ACCOUNT_BILLING_ADDRESS'=>utf8_decode($in['account_billing_address']),
					 'ACCOUNT_BILLING_ZIP'=>$in['account_billing_zip'],
					 'database_name'=>DATABASE_NAME]
				);
				//$this->db_users->query("UPDATE customers SET ".$filter_name.$filter_city.$filter_country." WHERE customer_id='".$in['customer_id']."' ");
			}

		/*$billing_add = $this->db_users->field("SELECT address_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND billing=1");
		if($billing_add){
			$filter_country_id= "country_id='".$in['account_billing_country_id']."',";
			$filter_city= "city='".$in['account_billing_city']."',";
			$filter_zip= "zip='".$in['account_billing_zip']."',";
			$filter_address= "address='".utf8_decode($in['account_billing_address'])."'";

			if($filter_country_id||$filter_city||$filter_zip||$filter_address){
			$this->db_users->query("UPDATE customer_addresses SET ".$filter_country_id.$filter_city.$filter_zip.$filter_address." WHERE address_id='".$billing_add."'");
			}
		}*/

		msg::success(gm('Details updated'),"success");
		json_out($in);
	}

	function save_valid_vat(&$in){
		global $config;
		ark::loadLibraries(array('stripe/init'));
		\Stripe\Stripe::setApiKey($config['stripe_api_key']);

		$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');
		$c_code=get_country_code($in['account_billing_country_id']);
		if($c_code == 'BE'){
			//$this->db_users->query("UPDATE users SET valid_vat='".trim($in['account_vat_number'])."' WHERE database_name='".DATABASE_NAME."' ");
			$this->db_users->query("UPDATE users SET valid_vat= :valid_vat WHERE database_name= :database_name ",['valid_vat'=>trim($in['account_vat_number']),'database_name'=>DATABASE_NAME]);
			//$this->db_users->query("UPDATE customers SET btw_nr='".trim($in['account_vat_number'])."'	WHERE customer_id='".$in['customer_id']."' ");
			$this->db_users->query("UPDATE customers SET btw_nr= :btw_nr	WHERE customer_id= :customer_id ",['btw_nr'=>trim($in['account_vat_number']),'customer_id'=>$in['customer_id']]);
			if(strpos($config['site_url'],'my.akti')){
				global $database_config;
				$database_new = array(
						'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
						'username' => 'admin',
						'password' => 'hU2Qrk2JE9auQA',
						'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
				);
					
				$db_user_new = new sqldb($database_new);

				$select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$_SESSION['u_id']."' AND field_id='1' ");
				if($select_contact){
					$select_customer = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$select_contact."' AND `primary`='1'");
					if($select_customer){
						$db_user_new->query("UPDATE customers SET check_vat_number='".trim($in['account_vat_number'])."',
																btw_nr='".trim($in['account_vat_number'])."'
																WHERE customer_id='".$select_customer."'
						");
					}
				}		
			}
		}else{
			if(in_array($c_code, $eu_countries) && $in['account_vat_number']){
				$search   = array(" ", ".");
				$vat = str_replace($search, "", $in['account_vat_number']);
				$_GET['a'] = substr($vat,0,2);
				$_GET['b'] = substr($vat,2);
				$_GET['c'] = '1';
				$dd = include('valid_vat.php');
			}
			$this->db_users->query("UPDATE users SET valid_vat='' WHERE database_name='".DATABASE_NAME."' ");
			//$this->db_users->query("UPDATE customers SET btw_nr=''	WHERE customer_id='".$in['customer_id']."' ");
			$this->db_users->query("UPDATE customers SET btw_nr= :btw_nr	WHERE customer_id= :customer_id ",['btw_nr'=>'','customer_id'=>$in['customer_id']]);
	
			if(isset($response) && $response == 'invalid'){
				msg::error(gm('Incorrect VAT number or format'),"error");
				json_out($in);
			}else if(isset($response) && $response == 'error'){
				msg::error(gm('Sorry, but we could not verify your VAT'),"error");
				json_out($in);
			}else if(isset($response) && $response == 'valid'){
				//$this->db_users->query("UPDATE users SET valid_vat='".trim($in['account_vat_number'])."' WHERE database_name='".DATABASE_NAME."' ");
				$this->db_users->query("UPDATE users SET valid_vat= :valid_vat WHERE database_name= :database_name ",['valid_vat'=>trim($in['account_vat_number']),'database_name'=>DATABASE_NAME]);
				//$this->db_users->query("UPDATE customers SET btw_nr='".trim($in['account_vat_number'])."'	WHERE customer_id='".$in['customer_id']."' ");
				$this->db_users->query("UPDATE customers SET btw_nr= :btw_nr	WHERE customer_id= :customer_id ",['btw_nr'=>trim($in['account_vat_number']),'customer_id'=>$in['customer_id']]);
				//$to_pay = $this->db_users->query("SELECT * FROM users WHERE user_id='{$_SESSION['u_id']}' ");
				$to_pay = $this->db_users->query("SELECT * FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
				$to_pay->next();
				$total = $to_pay->f('users')*$to_pay->f('user_amount')+$to_pay->f('time_users')*$to_pay->f('user_time_amount');
				if($to_pay->f('webshop_yes') != 0 && $to_pay->f('plan') > 2){
					$total += $to_pay->f('webshop_amount');
				}
				if($to_pay->f('plan') == 3){
					$total += 15;
				}
				$this->db_users->query("UPDATE users SET total='".$total."' WHERE database_name='".DATABASE_NAME."' ");
				if(strpos($config['site_url'],'my.akti')){
					global $database_config;
					$database_new = array(
							'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
							'username' => 'admin',
							'password' => 'hU2Qrk2JE9auQA',
							'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
					);
						
					$db_user_new = new sqldb($database_new);

					$select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$_SESSION['u_id']."' AND field_id='1' ");
					if($select_contact){
						$select_customer = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$select_contact."' AND `primary`='1'");
						if($select_customer){
							$db_user_new->query("UPDATE customers SET check_vat_number='".trim($in['account_vat_number'])."',
																	btw_nr='".trim($in['account_vat_number'])."'
																	WHERE customer_id='".$select_customer."'
							");
						}
					}		
				}
			}
		}

		//$user_stripe=$this->db_users->field("SELECT stripe_cust_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$user_stripe=$this->db_users->field("SELECT stripe_cust_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		//$user_stripe_subscr=$this->db_users->field("SELECT stripe_subscr_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$user_stripe_subscr=$this->db_users->field("SELECT stripe_subscr_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		if($user_stripe!='' && $user_stripe_subscr!=''){
			$vat_added=0;
			if( in_array($c_code, $eu_countries) && !$in['account_vat_number']) {
				$vat_added = 21;
			}
			if($c_code == 'BE') {
				$vat_added = 21;
			}
			$cu = \Stripe\Customer::retrieve($user_stripe);
			$subscription = $cu->subscriptions->retrieve($user_stripe_subscr);
			$subscription->tax_percent = $vat_added;
			$subscription->save();
		}

		msg::success(gm('Details updated'),"success");
		json_out($in);
	}

	function cancel(&$in)
	{
		global $config;
		//$stripe_user=$this->db_users->field("SELECT stripe_cust_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$stripe_user=$this->db_users->field("SELECT stripe_cust_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
		if($stripe_user){
			ark::loadLibraries(array('stripe/init'));
			\Stripe\Stripe::setApiKey($config['stripe_api_key']);
			//$stripe_subsc=$this->db_users->field("SELECT stripe_subscr_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
			$stripe_subsc=$this->db_users->field("SELECT stripe_subscr_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
			$cu = \Stripe\Customer::retrieve($stripe_user);
			$cu->subscriptions->retrieve($stripe_subsc)->cancel();
			//$this->db_users->field("UPDATE users SET stripe_subscr_id='' WHERE user_id='".$_SESSION['u_id']."'");
			$this->db_users->field("UPDATE users SET stripe_subscr_id= :stripe_subscr_id WHERE user_id= :user_id",['stripe_subscr_id'=>'','user_id'=>$_SESSION['u_id']]);
		}
		
		$time = time();
		$this->db_users->query("UPDATE users SET payed='2', active='2' WHERE database_name='".DATABASE_NAME."' ");
		$this->db_users->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account canceled by customer', `date`='".$time."' ");
		msg::success(gm("Subscription canceled"),"success");
		$this->db_users->query("UPDATE users SET cancel='".$time."'
															WHERE database_name='".DATABASE_NAME."'");

		$mail = new PHPMailer();
		$mail->WordWrap = 50;
		$fromMail='notification@akti.com';
		$mail->SetFrom($fromMail, $fromMail);
		//$this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
        $this->db_users->move_next();
        $body ="Dear Gaëtan,

		        ".utf8_encode($this->db_users->f('first_name'))." ".utf8_encode($this->db_users->f('last_name'))." has canceled subsription to Akti.

		        Your can see acount info <a href=\"http://manage.akti.com/index.php?pag=user&user_id=".$_SESSION['u_id']."\"> here</a>

				Best regards,
				Akti Customer Care
				<a href=\"http://akti.com\" >www.akti.com</a>";
				$subject= 'A user has canceled subscription to Akti';

		$mail->Subject = $subject;
        $mail->MsgHTML(nl2br($body));
		$mail->AddAddress('g.loriot@thinkweb.be');

		$mail->Send();
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function UpdateAccountant($in)
	{
		$in['user_id'] = $this->db_users->field("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' AND active=1000  ");
		/*$this->db_users->query("UPDATE users SET	first_name 	    = '".utf8_decode($in['first_name'])."',													
							        				lang_id 		= '".$in['lang_id']."'
					    	   					WHERE user_id='".$in['user_id']."' ");*/
		$this->db_users->query("UPDATE users SET	first_name 	    = :first_name,													
							        				lang_id 		= :lang_id
					    	   					WHERE user_id= :user_id ",
					    	   				['first_name' 	=> utf8_decode($in['first_name']),	
							        		 'lang_id' 		=> $in['lang_id'],
					    	   				 'user_id'      => $in['user_id']]
					    	   			);					    	   					
		msg::success ( gm("User updated"),'success');
	}

	function tact_connect(&$in){
		global $config;
    	$ch = curl_init();

    	$headers=array('Content-Type: application/json','x-api-key:'.$in['api_key']);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $config['tact_base_url'].'/account/account');

    	$put = curl_exec($ch);
    	$info = curl_getinfo($ch);

    	if($info['http_code']>300 || $info['http_code']==0){
    		$err=json_decode($put);
    		msg::error($err->message,"error");
    		json_out($in);
    	}else{
    		$tact_data=json_decode($put);
    		$tact_id=$this->db->field("SELECT app_id FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
    		$this->db->query("UPDATE apps SET active='1', api='".$in['api_key']."' WHERE app_id='".$tact_id."' ");
    		$node_exist=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_id."' AND type='node' ");
    		if(!$node_exist){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$tact_id."', api='".$tact_data->nodes[0]."', type='node' ");
    		}
    		$comp_exist=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_id."' AND type='company' ");
    		if(!$comp_exist){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$tact_id."', api='".$tact_data->companies[0]."', type='company' ");
    		}
    		$shop_exist=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$tact_id."' AND type='shop' ");
    		if(!$shop_exist){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$tact_id."', api='".$tact_data->shops[0]."', type='shop' ");
    		}
    		msg::success ( gm("Changes have been saved."),'success');
    		return true;
    	}
	}

	function tact_disconnect(){
		$this->db->query("UPDATE apps SET active='0' WHERE name='Tactill' AND type='main' AND main_app_id='0'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function nylas_connect(){
		$this->db->query("UPDATE apps SET active='1' WHERE name='Nylas' AND main_app_id='0' AND type='main'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function nylas_disconnect(){
		$this->db->query("UPDATE apps SET active='0' WHERE name='Nylas' AND main_app_id='0' AND type='main'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;	
	}

	function zendesk_connect(&$in){
		if(!$this->zendesk_connect_validate($in)){
			json_out($in);
			return false;
		}
    	$ch = curl_init();

    	$headers=array('Content-Type: application/json');
    	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
	    curl_setopt($ch, CURLOPT_USERPWD, trim($in['email'])."/token:".trim($in['password']));
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $in['url'].'/api/v2/tickets.json');

    	$put = curl_exec($ch);
    	$info = curl_getinfo($ch);

    	if($info['http_code']>300 || $info['http_code']==0){
    		if(!$put){
    			msg::error("name",gm('This field is required'));
    		}else{
    			$err=json_decode($put);
    			msg::error($err->error,"error");
    		}
    		json_out($in);
    	}else{
    		$zen_id=$this->db->field("SELECT app_id FROM apps WHERE name='Zendesk' AND type='main' AND main_app_id='0' ");
    		$this->db->query("UPDATE apps SET active='1', api='".trim($in['url'])."' WHERE app_id='".$zen_id."' ");
    		$email=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_id."' AND type='email' ");
    		if(!$email){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$zen_id."', api='".trim($in['email'])."', type='email' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".trim($in['email'])."' WHERE main_app_id='".$zen_id."' AND type='email' ");
    		}
    		$pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$zen_id."' AND type='password' ");
    		if(!$pass){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$zen_id."', api='".trim($in['password'])."', type='password' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".trim($in['password'])."' WHERE main_app_id='".$zen_id."' AND type='password' ");
    		}
    		msg::success ( gm("Changes have been saved."),'success');
    		return true;
    	}
	}

	function zendesk_connect_validate(&$in){
		$v=new validation($in);
		$v->f('url', 'Url', 'required:url');
		$v->f('email', 'Email', 'required:email');
		$v->f('password', 'Password', 'required');
		return $v->run();
	}

	function zendesk_disconnect(){
		$this->db->query("UPDATE apps SET active='0' WHERE name='Zendesk' AND type='main' AND main_app_id='0'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	
	function address_invoice(&$in){
		$val = $this->db->field("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_ADDRESS_INVOICE'");
		if($val){
			$this->db->query("UPDATE settings SET value='".$in['INVOICE_ADDRESS']."' WHERE constant_name='ACCOUNT_ADDRESS_INVOICE'");
		}else{
			$this->db->query("INSERT INTO `settings` (`constant_name` ,`value` ,`long_value` ,`module` ,`type`) VALUES ('ACCOUNT_ADDRESS_INVOICE',  '".$in['INVOICE_ADDRESS']."', NULL ,  'billing',  '1') ");
		}
		
		if($in['INVOICE_ADDRESS']==''){
	    	$this->db->query("UPDATE settings SET long_value='".$in['ACCOUNT_DELIVERY_ADDRESS']."'  WHERE constant_name='ACCOUNT_BILLING_ADDRESS'");
	    	$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_DELIVERY_COUNTRY_ID']."'  WHERE constant_name='ACCOUNT_BILLING_COUNTRY_ID'");
	    	$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_DELIVERY_ZIP']."'  WHERE constant_name='ACCOUNT_BILLING_ZIP'");
	    	$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_DELIVERY_CITY']."' WHERE constant_name='ACCOUNT_BILLING_CITY'");
		}
		
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function add_user_email(&$in){
		global $config;
		$in['address'] = explode( ';', $in['emails'] );
		$in['total_invited']=count($in['address']);
		$has_licences=true;
		foreach($in['address'] as $key => $value){
			if(!$this->checkLicense($in)){
				$has_licences=false;
				break;
			}
		}
		if(!$has_licences){
			json_out($in);
		}

		$in['database'] = DATABASE_NAME;
		//$name_user = $this->db_users->query("SELECT first_name,last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$name_user = $this->db_users->query("SELECT first_name,last_name FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);

		foreach ($in['address'] as $key => $value) {
			$in['username']=$value;
			if(!$this->add_validate_create($in)){
				msg::error(gm('The username '.$in['username'].' exist'),'error');
				json_out($in);
				return false;

			}
			
			$in['email'] = $value;
			$lang_code=$_SESSION['l'];
			$mail = new PHPMailer();
			$mail->WordWrap = 50;
		  	$fromMail='noreply@akti.com';
	 	 	$mail->SetFrom($fromMail, 'Akti');	

		 	$forgot_email = include(ark::$controllerpath . 'multi_email.php');
			$body=$forgot_email;
			$body=str_replace(',[!USERNAME!]',"",$body);
         	$body=str_replace('[!FIRSTNAME!]',"".utf8_decode($name_user->f('first_name'))."",$body);
	        $body=str_replace('[!LASTNAME!]',"".utf8_decode($name_user->f('last_name'))."",$body);
            $body=str_replace('[!LINK_VALIDATE!]',"<a href='".$site."'>".$site."</a>",$body);
			$mail->Subject = $mail_subject;
		 	$mail->MsgHTML($body);
		  	$mail->AddAddress($value);
		 	$mail->Send();
		 	msg::success ( gm("Email has been sent."),'success');
			//json_out($in);
			return true;
		}
	}
	function checkLicense(&$in){
		$is_ok=true;
		$trial = $this->db_users->field("SELECT is_trial FROM user_info WHERE user_id=(SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' AND u_type !=1 limit 1) ");
		if($trial == 1){
			switch (true) {
				case (defined('NEW_SUBSCRIPTION') && NEW_SUBSCRIPTION==1):
					$user_p = $this->db_users->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' AND ( user_meta.value !=  '1' OR user_meta.value IS NULL ) ");
					$total_invited=$this->db_users->field("SELECT COUNT(user_id) FROM user_email WHERE user_id IN (SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."') ");
					if(!$total_invited){
						$total_invited=0;
					}
					$user_p+=$total_invited;
					$user_total = $this->db_users->field("SELECT users FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' ");
					if($in['total_invited']){
						$user_p+=$in['total_invited'];
					}
					if($user_p>$user_total){					
						msg::error(gm("You cannot add more users. <br>Please change your subscription too add more users."),"error");
						$is_ok=false;
					}
					break;			
				default:
					$u = $this->db_users->field("SELECT u_type FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' ");
					if($u == 2){
						return true;
					}
					$user_p = $this->db_users->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE credentials!='8' AND database_name='".DATABASE_NAME."' AND users.active='1' AND ( user_meta.value !=  '1' OR user_meta.value IS NULL ) ");
					$total_invited=$this->db_users->field("SELECT COUNT(user_id) FROM user_email WHERE user_id IN (SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."') ");
					if(!$total_invited){
						$total_invited=0;
					}
					$user_p+=$total_invited;
					if($in['total_invited']){
						$user_p+=$in['total_invited'];
					}
					$user_t = $this->db_users->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE credentials='8' AND database_name='".DATABASE_NAME."' AND users.active='1'  AND ( user_meta.value !=  '1' OR user_meta.value IS NULL )  ");
					switch(HOW_MANY){
						case 0:
							if($in['group_id'] == 2){
								//
							}else if($user_p > 1){
								msg::error(gm("You cannot add more users. <br>Please change your subscription too add more users."),"error");
								$is_ok=false;
							}
							break;
						case 1:
							if($in['group_id'] == 2){
								//
							}else if($user_p > 3){
								msg::error(gm("You cannot add more users. <br>Please change your subscription too add more users."),"error");
								$is_ok=false;
							}
							break;
						case 2:
							if($user_p > 7){
								msg::error(gm("You cannot add more users. <br>Please change your subscription too add more users."),"error");
								$is_ok=false;
							}
							break;
						case 3:
							if($user_p > 15){
								msg::error(gm("You cannot add more users. <br>Please change your subscription too add more users."),"error");
								$is_ok=false;
							}
							break;
					}
					break;
			}
		}
		return $is_ok;
	}

	function add_validate_create(&$in)
	{
		$v = new validation($in);
		$is_ok = true;
		$v->field('username', gm('Username'), 'required:min_length[6]:unique[users.email]',false,$this->database_2);
		$is_ok = $v->run();
		return $is_ok;
	}
	function clearfacts_connect(&$in){
		global $config;
		if(!$this->clearfacts_connect_validate($in)){
			json_out($in);
			return false;
		}
    	/*$ch = curl_init();
    	$headers=array('Content-Type: application/json');
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($ch, CURLOPT_USERPWD, $config['clearfacts_app_secret'].':');
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $config['clearfacts_base_url'].'/oauth2-server/authorize?'.$config['clearfacts_app_id']);

    	$put = curl_exec($ch);
    	$info = curl_getinfo($ch);

    	if($info['http_code']>300 || $info['http_code']==0){
    		$err=json_decode($put);
    		msg::error($err->error,"error");
    		json_out($in);
    	}else{
    		$cfact_id=$this->db->field("SELECT app_id FROM apps WHERE name='ClearFacts' AND type='main' AND main_app_id='0' ");
    		$this->db->query("UPDATE apps SET active='1', api='".trim($in['api_key'])."' WHERE app_id='".$cfact_id."' ");
    		$user=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$cfact_id."' AND type='username' ");
    		if(!$user){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$cfact_id."', api='".trim($in['username'])."', type='username' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".trim($in['username'])."' WHERE main_app_id='".$cfact_id."' AND type='usermane' ");
    		}
    		$pass=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$cfact_id."' AND type='password' ");
    		if(!$pass){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$cfact_id."', api='".trim($in['password'])."', type='password' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".trim($in['password'])."' WHERE main_app_id='".$cfact_id."' AND type='password' ");
    		}
    		msg::success ( gm("Changes have been saved."),'success');
    		return true;
    	}
    	$this->db->query("UPDATE apps SET active='1' WHERE name='ClearFacts' AND type='main' AND main_app_id='0'");*/
    	msg::success ( gm("Changes have been saved."),'success');
    	return true;
	}

	function clearfacts_connect_validate(&$in){
		$v=new validation($in);
		/*$v->f('api_key', 'X Software Company', 'required');
		$v->f('username', 'Username', 'required');
		$v->f('password', 'Password', 'required');*/
		return $v->run();
	}

	function clearfacts_disconnect(){
		$this->db->query("UPDATE apps SET active='0' WHERE name='ClearFacts' AND type='main' AND main_app_id='0'");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='CLEARFACTS_SYNC_STATUSES'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function allo_connect(&$in){
		if(!$this->allo_connect_validate($in)){
			json_out($in);
			return false;
		}
    	$ch = curl_init();
    	$headers=array('Content-Type: application/json');
    	$c_data=array('data'=>array(
    		'username'		=> $in['username'],
    		'api_key'		=> $in['api_key']
    		));
    	$c_data=json_encode($c_data);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $c_data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $in['url'].'/auth/user');

    	$put = curl_exec($ch);
    	$info = curl_getinfo($ch);

    	if($info['http_code']>300 || $info['http_code']==0){
    		$err=json_decode($put);
    		msg::error($err->error->message,"error");
    		json_out($in);
    	}else{
    		$data_c=json_decode($put);
    		$acloud_id=$this->db->field("SELECT app_id FROM apps WHERE name='Allocloud' AND type='main' AND main_app_id='0' ");
    		$this->db->query("UPDATE apps SET active='1', api='".trim($in['api_key'])."' WHERE app_id='".$acloud_id."' ");
    		$user=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$acloud_id."' AND type='username' ");
    		if(!$user){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$acloud_id."', api='".trim($in['username'])."', type='username' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".trim($in['username'])."' WHERE main_app_id='".$acloud_id."' AND type='username' ");
    		}
    		$token=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$acloud_id."' AND type='token' ");
    		if(!$token){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$acloud_id."', api='".$data_c->auth_token."', type='token' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".$data_c->auth_token."' WHERE main_app_id='".$acloud_id."' AND type='token' ");
    		}
    		$url=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$acloud_id."' AND type='url' ");
    		if(!$url){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$acloud_id."', api='".$in['url']."', type='url' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".$in['url']."' WHERE main_app_id='".$acloud_id."' AND type='url' ");
    		}
    		$expire=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$acloud_id."' AND type='expire' ");
    		if(!$expire){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$acloud_id."', api='".(time()+3600)."', type='expire' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".(time()+3600)."' WHERE main_app_id='".$acloud_id."' AND type='expire' ");
    		}
    		msg::success ( gm("Changes have been saved."),'success');
    		return true;
    	}
	}

	function allo_connect_validate(&$in){
		$v=new validation($in);
		$v->f('api_key', 'Api key', 'required');
		$v->f('username', 'Username', 'required');
		$v->f('url', 'Base Url', 'required');
		return $v->run();
	}

	function allo_disconnect(){
		$this->db->query("UPDATE apps SET active='0' WHERE name='Allocloud' AND type='main' AND main_app_id='0'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function importDevice(&$in){
		$i=0;
		foreach ($in['list'] as $key => $value) {
			$this->db->query("INSERT INTO allo_devices SET
					device_id 	= '".$value['device_id']."',
					device_name = '".addslashes($value['name'])."',
					last_name 	= '".addslashes($value['last_name'])."',
					first_name  = '".addslashes($value['first_name'])."',
					owner_id    = '".$value['owner_id']."',
					user_id 	= '".$in['user_id']."'
				");
			$i++;
		}
		if($i==0){
			msg::error(gm('No selection made'),'error');
			json_out($in);
		}else{
			msg::success(gm("Data imported"),"success");
			return true;
		}
	}

	function deleteDevice(&$in){
		$this->db->query("DELETE FROM allo_devices WHERE id='".$in['id']."' ");
		msg::success(gm('Changes saved'),'success');
		json_out($in);
	}

	function setZenField(&$in){
		if($in['id']=='-1'){
			$ZEN_ACCOUNT_PRIMARY_ADD=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ZEN_ACCOUNT_PRIMARY_ADD' ");
			if(is_null($ZEN_ACCOUNT_PRIMARY_ADD)){
				$this->db->query("INSERT INTO settings SET `constant_name`='ZEN_ACCOUNT_PRIMARY_ADD',value='".$in['checked']."' ");
			}else{
				$this->db->query("UPDATE settings SET value='".$in['checked']."' WHERE `constant_name`='ZEN_ACCOUNT_PRIMARY_ADD' ");
			}
		}else{
			$this->db->query("UPDATE import_labels SET is_zen_field='".$in['checked']."' WHERE import_label_id='".$in['id']."' ");
		}
		json_out($in);
	}

	function setZenDocument(&$in){
		$zen_doc=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ZEN_DOC_".$in['id']."' ");
			if(is_null($zen_doc)){
				$this->db->query("INSERT INTO settings SET `constant_name`='ZEN_DOC_".$in['id']."',value='".$in['checked']."' ");
			}else{
				$this->db->query("UPDATE settings SET value='".$in['checked']."' WHERE `constant_name`='ZEN_DOC_".$in['id']."' ");
			}
		json_out($in);
	}

	function setZenTimeDocument(&$in){
		$zen_doc=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ZEN_TIME_DOC_".$in['id']."' ");
			if(is_null($zen_doc)){
				$this->db->query("INSERT INTO settings SET `constant_name`='ZEN_TIME_DOC_".$in['id']."',value='".$in['checked']."' ");
			}else{
				$this->db->query("UPDATE settings SET value='".$in['checked']."' WHERE `constant_name`='ZEN_TIME_DOC_".$in['id']."' ");
			}
		json_out($in);
	}

	function generateToken(){
		//$user_mail=$this->db_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$user_mail=$this->db_users->field("SELECT email FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		$is_token=true;
		while($is_token){
			$in['token'] = md5(uniqid(mt_rand(),true).$user_mail);
			//$user_token=$this->db_users->field("SELECT user_id FROM users WHERE zen_token='".$in['token']."' AND `database_name`!='".DATABASE_NAME."'");
			$user_token=$this->db_users->field("SELECT user_id FROM users WHERE zen_token= :zen_token AND `database_name`!= :database_name",['zen_token'=>$in['token'],'database_name'=>DATABASE_NAME]);
			if(!$user_token){
				//$this->db_users->query("UPDATE users SET zen_token='".$in['token']."' WHERE `database_name`='".DATABASE_NAME."' ");
				$this->db_users->query("UPDATE users SET zen_token= :zen_token WHERE `database_name`= :database_name ",['zen_token'=>$in['token'],'database_name'=>DATABASE_NAME]);
				$is_token=false;
			}
		}
		json_out($in);
	}

	function setCodaOptions(&$in){
		$coda_api=$this->db->query("SELECT api,app_id FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
		if($in['type']=='sales_check'){
			$sales_e=$this->db->field("SELECT app_id FROM apps WHERE type='sales' AND main_app_id='".$coda_api->f('app_id')."' ");
 			if($sales_e){
 				$this->db->query("UPDATE apps SET api='".$in['value']."' WHERE app_id='".$sales_e."' ");
 			}else{
 				$this->db->query("INSERT INTO apps SET api='".$in['value']."', type='sales', main_app_id='".$coda_api->f('app_id')."' ");
 			}
		}
		if($in['type']=='payments_check'){
			$payments_e=$this->db->field("SELECT app_id FROM apps WHERE type='payments' AND main_app_id='".$coda_api->f('app_id')."' ");
 			if($payments_e){
 				$this->db->query("UPDATE apps SET api='".$in['value']."' WHERE app_id='".$payments_e."' ");
 			}else{
 				$this->db->query("INSERT INTO apps SET api='".$in['value']."', type='payments', main_app_id='".$coda_api->f('app_id')."' ");
 			}
		}
		json_out($in);		
	}

	function setLangTranslation(&$in){
		$word_id=$this->db->field("SELECT word_id FROM vat_lang WHERE id='".$in['id']."' ");
		if($word_id){
			$this->db->query("UPDATE vat_lang SET en='".addslashes($in['en'])."', fr='".addslashes($in['fr'])."', 
			nl='".addslashes($in['nl'])."', de='".addslashes($in['de'])."' WHERE word_id='".$word_id."' ");
		}else{
			$this->db->query("INSERT INTO vat_lang SET id='".$in['id']."',
										en='".addslashes($in['en'])."', 
										fr='".addslashes($in['fr'])."', 
										nl='".addslashes($in['nl'])."', 
										de='".addslashes($in['de'])."' ");
		}
		msg::success('Changes saved','success');
		json_out($in);
		return true;
	}

	function DeleteField(&$in){

		$this->db->query("DELETE FROM tblquote_".$in['table']." WHERE id='".$in['field_id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	/**
	 * add new custom field
	 *
	 * @return bool
	 * @author Mp
	 **/
	function add_field(&$in)
	{
		$in['field_id'] = $this->db->insert("INSERT INTO tblquote_".$in['table']." SET name='".$in['name']."' ");
		msg::success(gm('Field added.'),'success');
		return true;
	}

	 function UpdateField(&$in){
	 	$this->db->insert("UPDATE tblquote_".$in['table']." SET name='".$in['value']."',`code`='".$in['code']."' WHERE id='".$in['id']."' ");
		msg::success(gm('Field updated.'),'success');
		return true;
	 }

	 function saveCodes(&$in){
	 	if (!empty($_FILES)) {
	 		$fileTypes = array('xls','xlsx'); 
	 		$fileParts = pathinfo($_FILES['Filedata']['name']);
	 		if(in_array(strtolower($fileParts['extension']),$fileTypes)){
	 			ini_set('memory_limit', '2048M');
				ini_set('max_execution_time', 0);
	 			$targetFolder = 'upload/'.DATABASE_NAME.'/'; // Relative to the root
				@mkdir($targetFolder,0755,true);
	 			$tempFile = $_FILES['Filedata']['tmp_name'];
	 			$targetPath = $targetFolder;
				$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];
				$ok=move_uploaded_file($tempFile,$targetFile);
				if($ok===true){
					require_once 'libraries/PHPExcel.php';		
					$inputFileName = INSTALLPATH.UPLOAD_PATH.DATABASE_NAME."/".$_FILES['Filedata']['name'];
					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objReader->setReadDataOnly(true);
					$objPHPExcel = $objReader->load($inputFileName);
					$objWorksheet = $objPHPExcel->getActiveSheet();
					$highestRow = $objWorksheet->getHighestRow(); // e.g. 10
					$highestColumn = $objWorksheet->getHighestColumn();
					$headers=array();
					$items=array();
					$i=0;
					foreach($objWorksheet->getRowIterator() as $rowIndex => $row){
						if($i>3){
							break;
						}
						$array = $objWorksheet->rangeToArray('A'.$rowIndex.':'.$highestColumn.$rowIndex);
						foreach($array as $key=>$val){
							foreach($val as $key1=>$val1){
								if($i==0){
									$headers[$key1]=array('name'=>trim($val1));
								}else{
									if(!$items[$key1]){
										$items[$key1]=array('table'=>array());
									}					
									array_push($items[$key1]['table'],array('name'=>$val1));
								}											
							}									
						}
						$i++;
					}
					$in['headers']=$headers;
					$in['items']=$items;
					$in['base_drop']=array(array('id'=>'zip','name'=>gm('ZIP')),array('id'=>'code','name'=>gm('Code')));
					$in['link']=$inputFileName;
					msg::success(gm('File uploaded'),'success');
				}else{
					msg::error('error','error');
				}		
	 		}else{
	 			msg::error(gm('Invalid file type.'),'error');
	 		} 		
	 	}
	 	json_out($in);
	 }

	 function updateCodes(&$in){
	 	ini_set('memory_limit', '2048M');
		ini_set('max_execution_time', 0);
	 	require_once 'libraries/PHPExcel.php';		
		$inputFileName = $in['link'];
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($inputFileName);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		$highestRow = $objWorksheet->getHighestRow(); // e.g. 10
		$highestColumn = $objWorksheet->getHighestColumn();
		$row_array = array();
		$headers=array();
		$i=0;
		foreach($objWorksheet->getRowIterator() as $rowIndex => $row){
			if($i>$highestRow){
				break;
			}
			$array = $objWorksheet->rangeToArray('A'.$rowIndex.':'.$highestColumn.$rowIndex);
			foreach($array as $key=>$val){
				$item=array();
				foreach($val as $key1=>$val1){
					if($i==0){
						$headers[$key1]=array('name'=>trim($val1),'values'=>array());
					}else{
						array_push($headers[$key1]['values'], $val1);
					}											
				}											
			}
			$i++;
		}
		$columns=array();
		$column_array=array();
		foreach($headers as $key=>$value){
			$column='';
			foreach($in['headers'] as $key1=>$val1){
				if($value['name'] == $val1['name']){
					$column=$val1['id'];
					array_push($column_array,$val1['id']);
					break;
				}			
			}
			if($column!=''){
				if(!$columns[$column]){
					$columns[$column]=array();
				}
				$columns[$column]=$value['values'];
			}
		}
		if(!empty($column_array)){
			$temp_array=array();
			$sql_start="INSERT INTO cost_centre (";		
			foreach($column_array as $key=>$value){
				$j=0;
				$sql_start.="`".$value."`,";		
				foreach($columns[$value] as $key1=>$val){
					if(!$temp_array[$j]){
						$temp_array[$j]=array();
					}				
					array_push($temp_array[$j], $val);
					$j++;
				}			
			};
			$sql_start=rtrim($sql_start,",");
			$sql_start=$sql_start.") ";
			$sql_middle="VALUES ";
			foreach($temp_array as $key=>$value){
				$sql_middle_tmp="(";
				foreach($value as $key1=>$val1){
					$sql_middle_tmp.="'".$val1."'".",";
				}			
				$sql_middle_tmp=rtrim($sql_middle_tmp,",");
				$sql_middle_tmp=$sql_middle_tmp."), ";				
				$sql_middle.=$sql_middle_tmp;
			}
			$sql_middle=rtrim($sql_middle," ");
			$sql_middle=rtrim($sql_middle,",");
			$sql_fin=$sql_start.$sql_middle;
			$this->db->query("CREATE TABLE IF NOT EXISTS `cost_centre` (
 			`id` int(11) NOT NULL AUTO_INCREMENT,
  			`zip` varchar(255) NOT NULL,
  			`code` varchar(255) NOT NULL,
  			PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 ");
			$this->db->query("DELETE FROM cost_centre");
			$this->db->query($sql_fin);
			$this->db->query("INSERT INTO logging SET pag='cost_centre', message='".addslashes(gm('Cost Centre codes updated by').' '.get_user_name($_SESSION['u_id']))."', field_name='cost_centre', field_value='1', date='".time()."', type='0', reminder_date='".time()."' ");
		}
		@unlink($in['link']);
		msg::success(gm('Changes saved'),'success');
		json_out($in);
	 }

	function update_general_settings(&$in){

        $max_image_size = return_value($in['max_image_size']);

        if(is_numeric($max_image_size) && $max_image_size == 0 || empty($in['max_image_size'])){
            msg::error(gm("This field must contain only numbers."),'max_image_size');
            json_out($in);
            return false;
        }
        
        $imageSizeExists = $this->db->field("SELECT constant_name FROM settings WHERE constant_name = 'MAX_IMAGE_SIZE'");

        $value = $this->formatMBToBytes($max_image_size);
        if(empty($imageSizeExists)){
            $this->db->query("INSERT INTO settings SET constant_name='MAX_IMAGE_SIZE', value='".$value."'");
        } else {
            $this->db->query("UPDATE settings SET value='".$value."' WHERE constant_name='MAX_IMAGE_SIZE'");
        }

        msg::success(gm("Changes have been saved."),'success');
        return true;
    }

    function formatMBToBytes($mb, $precision = 2) { 
        //1 MB = 1048576 Bytes
        return ($mb * 1048576); 
    }

    function update_policy(&$in){
    	//$this->db_users->query("UPDATE users SET gdpr_status='".$in['type']."',gdpr_time='".time()."' WHERE user_id='".$_SESSION['u_id']."' ");
    	$this->db_users->query("UPDATE users SET gdpr_status= :gdpr_status,gdpr_time= :gdpr_time WHERE user_id= :user_id ",['gdpr_status'=>$in['type'],'gdpr_time'=>time(),'user_id'=>$_SESSION['u_id']]);
    	msg::success(gm('Changes saved'),'success');
    	return true;
    }

    function deleteApiApp(&$in){
    	//$this->db_users->query("DELETE FROM oauth_access_tokens WHERE client_id='".$in['client_id']."' ");
    	$this->db_users->query("DELETE FROM oauth_access_tokens WHERE client_id= :client_id ",['client_id'=>$in['client_id']]);
    	//$this->db_users->query("DELETE FROM oauth_authorization_codes WHERE client_id='".$in['client_id']."' ");
    	$this->db_users->query("DELETE FROM oauth_authorization_codes WHERE client_id= :client_id ",['client_id'=>$in['client_id']]);
    	//$this->db_users->query("DELETE FROM oauth_refresh_tokens WHERE client_id='".$in['client_id']."' ");
    	$this->db_users->query("DELETE FROM oauth_refresh_tokens WHERE client_id= :client_id ",['client_id'=>$in['client_id']]);
    	//$this->db_users->query("DELETE FROM oauth_clients WHERE client_id='".$in['client_id']."' ");
    	$this->db_users->query("DELETE FROM oauth_clients WHERE client_id= :client_id ",['client_id'=>$in['client_id']]);
    	msg::success(gm('Changes saved'),'success');
		return true;
    }

    function deleteApiToken(&$in){
    	//$this->db_users->query("DELETE FROM oauth_access_tokens WHERE access_token='".$in['access_token']."' ");
    	$this->db_users->query("DELETE FROM oauth_access_tokens WHERE access_token= :access_token ",['access_token'=>$in['access_token']]);
    	msg::success(gm('Changes saved'),'success');
		json_out($in);
    }

    function addApiApplication(&$in){
    	if(!$this->addApiApplication_validate($in)){
    		json_out();
    	} 	
    	$is_token=true;
		while($is_token){
			$client_id=substr(hash('sha256', time()), 0, 32);
			$client_exists=$this->db_users->field("SELECT client_id FROM oauth_clients WHERE client_id='".$client_id."' ");
			if(!$client_exists){
				$is_token=false;
			}
		}
		$client_secret = bin2hex(openssl_random_pseudo_bytes(32));
    	/*$this->db_users->query("INSERT INTO oauth_clients SET
    				client_id  = '".$client_id."',
    				client_secret='".$client_secret."',
    				redirect_uri='".addslashes($in['redirect_uri'])."',
    				application_name='".addslashes($in['application_name'])."'
    		");*/
    	$this->db_users->insert("INSERT INTO oauth_clients SET
    				client_id  = :client_id,
    				client_secret= :client_secret,
    				redirect_uri= :redirect_uri,
    				application_name= :application_name",
    			['client_id'  => $client_id,
    			 'client_secret'=>$client_secret,
    			 'redirect_uri'=>addslashes($in['redirect_uri']),
    			 'application_name'=>addslashes($in['application_name'])]
    		);
    	msg::success(gm('Changes saved'),'success');
    	return true;
    }

    function addApiApplication_validate(&$in){
    	$v=new validation($in);
    	$v->field("application_name","application_name","required");
    	$v->field("redirect_uri","redirect_uri","required:url");
    	return $v->run();
    }

    function updateApiApplication(&$in){
    	if(!$this->addApiApplication_validate($in)){
    		json_out();
    	}
    	/*$this->db_users->query("UPDATE oauth_clients SET
    				redirect_uri='".addslashes($in['redirect_uri'])."',
    				application_name='".addslashes($in['application_name'])."'
    				WHERE client_id='".$in['client_id']."'
    		");*/
    	$this->db_users->query("UPDATE oauth_clients SET
    				redirect_uri= :redirect_uri,
    				application_name= :application_name
    				WHERE client_id= :client_id",
    			['redirect_uri'=>addslashes($in['redirect_uri']),
    			 'application_name'=>addslashes($in['application_name']),
    			 'client_id'=>$in['client_id']]
    		);
    	msg::success(gm('Changes saved'),'success');
    	return true;
    }

	function update_setting_clearfacts(&$in){
        
        $exists = $this->db->field("SELECT constant_name FROM settings WHERE constant_name = 'CLEARFACTS_SYNC_STATUSES'");

        if(empty($exists)){
            $this->db->query("INSERT INTO settings SET constant_name='CLEARFACTS_SYNC_STATUSES', value='".$in['sync_statuses']."'");
        } else {
            $this->db->query("UPDATE settings SET value='".$in['sync_statuses']."' WHERE constant_name='CLEARFACTS_SYNC_STATUSES'");
        }

        msg::success(gm("Changes have been saved."),'success');
        return true;
    }

    function update_setting_exact_online(&$in){
        
        $exists = $this->db->field("SELECT constant_name FROM settings WHERE constant_name = 'EXACT_ONLINE_SYNC_STATUSES'");

        if(empty($exists)){
            $this->db->query("INSERT INTO settings SET constant_name='EXACT_ONLINE_SYNC_STATUSES', value='".$in['sync_statuses']."'");
        } else {
            $this->db->query("UPDATE settings SET value='".$in['sync_statuses']."' WHERE constant_name='EXACT_ONLINE_SYNC_STATUSES'");
        }

        msg::success(gm("Changes have been saved."),'success');
        return true;
    }

    function testSmtpConnection(&$in){
		if($in['MAIL_SETTINGS_PREFERRED_OPTION']==2 && !$this->smtpConnection_validate($in) && !$in['skip_validation']){
			json_out($in);
		}
		if($in['MAIL_SETTINGS_PREFERRED_OPTION']==2){
			$mail=new PHPMailer();
			$mail->IsSMTP();
	        $mail->SMTPSecure = $in['MAIL_SETTINGS_USE_SSL'] ? 'ssl':'tls';
	        $mail->Host = $in['MAIL_SETTINGS_SERVER_NAME'];
	        $mail->Port = $in['MAIL_SETTINGS_SERVER_PORT'];
	        //$mail->Username = $in['MAIL_SETTINGS_EMAIL'] && !$in['MAIL_SETTINGS_USERNAME'] ? $in['MAIL_SETTINGS_EMAIL'] : $in['MAIL_SETTINGS_USERNAME'];
	        $mail->Username = $in['MAIL_SETTINGS_EMAIL'];
	        $mail->Password = $in['MAIL_SETTINGS_PASSWORD'];
	        $mail->SMTPAuth = true;
	        $mail->AuthType=$in['MAIL_SETTINGS_AUTHENTICATION']==2 ? 'CRAM-MD5' : ($in['MAIL_SETTINGS_AUTHENTICATION']==3 ? 'NTLM' : 'LOGIN'); 
	        $mail->TestOnly=true;
	        try{
	        	$mail->smtpConnect();
	        }catch(Exception $e){
	        	msg::error($e->getMessage(),'error');
	        	if($in['skip_validation']){
	        		return false;
	        	}else{
	        		json_out($in);	        		
	        	}
	        }
		}	
		if(!$in['skip_validation']){
			msg::success(gm('Test succeeded'),'success');
			json_out($in);
		}else{
			return true;
		}	
	}

	function smtpConnection_validate(&$in){
		$v=new validation($in);
		$v->field("MAIL_SETTINGS_EMAIL","Email","required:email");
		$v->field("MAIL_SETTINGS_SERVER_NAME","Server Name","required");
		$v->field("MAIL_SETTINGS_PASSWORD","Password","required");
		$v->field("MAIL_SETTINGS_AUTHENTICATION","Authentication","required");
		$v->field("MAIL_SETTINGS_SERVER_PORT","Authentication","required");
		/*if($in['MAIL_SETTINGS_USERNAME']){
			$v->field("MAIL_SETTINGS_USERNAME","Username","email");
		}*/
		return $v->run();
	}

	function saveMailSettings(&$in){
		if($in['MAIL_SETTINGS_PREFERRED_OPTION']==2 && !$this->smtpConnection_validate($in)){
			json_out($in);
		}	
		if($in['MAIL_SETTINGS_PREFERRED_OPTION']==2){
			$in['skip_validation']=true;
			if(!$this->testSmtpConnection($in)){
				json_out($in);
			}
		}
		$stmp_data_array=array(
    		'MAIL_SETTINGS_PREFERRED_OPTION',
    		'ACCOUNT_SEND_EMAIL',
    		'MAIL_SETTINGS_EMAIL',
    		'MAIL_SETTINGS_SERVER_NAME',
    		'MAIL_SETTINGS_USERNAME',
    		'MAIL_SETTINGS_PASSWORD',
    		'MAIL_SETTINGS_USE_SSL',
    		'MAIL_SETTINGS_AUTHENTICATION',
    		'MAIL_SETTINGS_SERVER_PORT'
    	);
		foreach($stmp_data_array as $value){
			if($value=='MAIL_SETTINGS_PASSWORD'){
				$in[$value]=base64_encode($in[$value]);
			}
			$email_data_exist=$this->db->field("SELECT value FROM settings WHERE constant_name='".$value."' ");
			if(is_null($email_data_exist)){
				$this->db->query("INSERT INTO settings SET constant_name='".$value."',`value`='".$in[$value]."' ");
			}else{
				$this->db->query("UPDATE settings SET `value`='".$in[$value]."' WHERE constant_name='".$value."' ");
			}
		}
		msg::success(gm('Changes saved'),'success');
		json_out($in);
	}

	function sendgrid_connect(&$in){

		if(!$this->sendgrid_connect_validate($in)){
			json_out($in);
			return false;
		}

		$sendgrid_api_key = $in['api_key'];

		$data_to_send = array('sendgrid_api_key' => $sendgrid_api_key,
							'sendgrid_url'	=> "https://api.sendgrid.com/v3",
							'call'	=> "/api_keys",
							'method'	=> 'GET'
							);
    	$info = $this->sendGridSettingsCall($data_to_send);

    	global $config;

    	if($info['http_code']>300 || $info['http_code']==0){
    		if(is_string($put)){
    			msg::error($put,"error");
    		}else{
    			msg::error("An error occured","error");
    		}	
    		json_out($in);
    	}else{
    		$this->db->query("UPDATE apps SET active='1' WHERE name='SendGrid' AND type='main' AND main_app_id='0' ");

    		$sendgrid_id=$this->db->field("SELECT app_id FROM apps WHERE name='SendGrid' AND type='main' AND main_app_id='0' ");
			$api_key=$this->db->field("SELECT app_id FROM apps WHERE main_app_id='".$sendgrid_id."' AND type='api_key' ");
    		
    		if(!$api_key){
    			$this->db->query("INSERT INTO apps SET main_app_id='".$sendgrid_id."', api='".trim($sendgrid_api_key)."', type='api_key' ");
    		}else{
    			$this->db->query("UPDATE apps SET api='".trim($sendgrid_api_key)."' WHERE main_app_id='".$sendgrid_id."' AND type='api_key' ");
    		}

    		$data_to_send = $this->getSendGridPatchData($data_to_send,$config);
 			$info_patch = $this->sendGridSettingsCall($data_to_send);
 			if($info_patch['http_code']>300 || $info_patch['http_code']==0){
	    		if(is_string($put)){
	    			msg::error($put,"error");
	    		}else{
	    			msg::error("An error occured","error");
	    		}	
	    		json_out($in);
    		}

    		msg::success ( gm("Changes have been saved."),'success');
    		return true;
    	}
	}

	function getSendGridPatchData($data_to_send,$config){
		$data_to_send['method'] = 'PATCH';
		$data_to_send['call'] = '/user/webhooks/event/settings';
		$data_to_send['webservice_outgoing'] = array("enabled"=> true,
													  "url"=> $config['sendgrid_endpoint'],
													  "group_resubscribe"=> true,
													  "delivered"=> true,
													  "group_unsubscribe"=> true,
													  "spam_report"=> true,
													  "bounce"=> true,
													  "deferred"=> false,
													  "unsubscribe"=> true,
													  "processed"=> false,
													  "open"=> true,
													  "click"=> true,
													  "dropped"=> true
											);
		return $data_to_send;
	}

	function sendGridSettingsCall($data){

    	$sendgrid_url = $data['sendgrid_url'].$data['call'];
		$sendgrid_api_key = $data['sendgrid_api_key'];
		$method = $data['method'];

		$ch = curl_init();
    	$headers=array('Content-Type: application/json','Authorization: Bearer '.$sendgrid_api_key);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if($method == 'PATCH'){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data['webservice_outgoing']));
		}

      	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
      	curl_setopt($ch, CURLOPT_URL, $sendgrid_url);

    	$put = curl_exec($ch);
    	$info = curl_getinfo($ch);
    	curl_close($ch);

    	return $info;
	}

	function sendgrid_connect_validate(&$in){
		$v=new validation($in);
		$v->f('api_key', 'API Key', 'required');
		return $v->run();
	}

	function sendgrid_disconnect(){
		$this->db->query("UPDATE apps SET active='0' WHERE name='SendGrid' AND type='main' AND main_app_id='0'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function update_sendgrid_settings(&$in){
		
		$use_for_inv = $this->db->field("SELECT constant_name FROM settings WHERE constant_name = 'SENDGRID_SETTING_USE_FOR_INV'");
		if(empty($use_for_inv)){
			$this->db->query("INSERT INTO settings SET constant_name='SENDGRID_SETTING_USE_FOR_INV', value='".$in['use_for_inv']."'");
		} else {
			$this->db->query("UPDATE settings SET value='".$in['use_for_inv']."' WHERE constant_name='SENDGRID_SETTING_USE_FOR_INV'");
		}
		$uns_group_id = $this->db->field("SELECT constant_name FROM settings WHERE constant_name = 'SENDGRID_SETTING_UNS_GROUP_ID'");
		if(empty($uns_group_id)){
			$this->db->query("INSERT INTO settings SET constant_name='SENDGRID_SETTING_UNS_GROUP_ID', value='".$in['uns_group_id']."'");
		} else {
			$this->db->query("UPDATE settings SET value='".$in['uns_group_id']."' WHERE constant_name='SENDGRID_SETTING_UNS_GROUP_ID'");
		}

		foreach ($in as $key =>$block) {
			if(is_array($block)){
				foreach ($block as $b) {
					$c_name = "SENDGRID_SETTING_".$b['c_name'];

					$value_exists = $this->db->field("SELECT constant_name FROM settings WHERE constant_name = '".$c_name."'");
					if(empty($value_exists)){
						$this->db->query("INSERT INTO settings SET constant_name='".$c_name."', value='".$b['value']."'");
					} else {
						$this->db->query("UPDATE settings SET value='".$b['value']."' WHERE constant_name='".$c_name."'");
					}
				}
			}
		}
		
		msg::success(gm("Changes have been saved."),'success');
		return true;
	}

	function facq_disconnect(&$in){
		$facq_id=$this->db->field("SELECT app_id FROM apps WHERE name='Facq' AND type='main' AND main_app_id='0' ");
		$this->db->query("UPDATE apps SET active='0' WHERE app_id='".$facq_id."' ");
		msg::success ( gm("Account deactivated"),'success');
		$in['done']=0;
		$in['xget']='apps';
		return true;
	}

	function facq_connect_validate(&$in){

		$app = $this->db->field("SELECT app_id FROM apps WHERE name='Facq' AND app_type='ecommerce' AND main_app_id=0 AND type='main' ");
		if(!$app){
			msg::error(gm('Error'),'error');
			json_out($in);
		}

		$v=new validation($in);
		$v->field('UserID',gm('UserID'),'required');
		$v->field('Login',gm('Login'),'required');
		$v->field('Password',gm('Password'),'required');
		return $v->run();
	}

	function facq_connect(&$in){
		global $config;
		if(!$this->facq_connect_validate($in)){
			json_out($in);
			return false;
		}

		if($this->check_access_facq($in['UserID'],$in['Login'], $in['Password'])){

			$facq_id=$this->db->field("SELECT app_id FROM apps WHERE name='Facq' AND type='main' AND main_app_id='0' ");

			$user_id_entry=$this->db->field("SELECT app_id FROM apps WHERE type='UserID' AND main_app_id='".$facq_id."' ");
			if($user_id_entry){
				$this->db->query("UPDATE apps SET api='".$in['UserID']."' WHERE app_id='".$user_id_entry."' ");
			}else{
				$this->db->query("INSERT INTO apps SET api='".$in['UserID']."', type='UserID', main_app_id='".$facq_id."' ");
			}

			$login_entry=$this->db->field("SELECT app_id FROM apps WHERE type='Login' AND main_app_id='".$facq_id."' ");
			if($login_entry){
				$this->db->query("UPDATE apps SET api='".$in['Login']."' WHERE app_id='".$login_entry."' ");
			}else{
				$this->db->query("INSERT INTO apps SET api='".$in['Login']."', type='Login', main_app_id='".$facq_id."' ");
			}

			$pass_entry=$this->db->field("SELECT app_id FROM apps WHERE type='Password' AND main_app_id='".$facq_id."' ");
			if($pass_entry){
				$this->db->query("UPDATE apps SET api='".$in['Password']."' WHERE app_id='".$pass_entry."' ");
			}else{
				$this->db->query("INSERT INTO apps SET api='".$in['Password']."', type='Password', main_app_id='".$facq_id."' ");
			}
			$this->add_supplier_facq();
			if($in['active']){
				$this->db->query("UPDATE apps SET active='1' WHERE app_id='".$facq_id."' ");
				
				msg::success ( gm('Facq activated'),'success');
			}else{
				msg::success ( gm('Data has been succesfully saved'),'success');
			}

		}/*else{
			msg::error(gm('Please provide a valid User Id, Login and password to activate the integration'),"error");
		}*/
				
		
		$in['xget']='apps';
		return true;
	}


	function check_access_facq($UserID, $Login, $Password){
      global $config;

      $requestParams =array(
        'UserID'                => $UserID,
        'Login'         		=> $Login,
        'Password'            	=> $Password,
        'CultureID'           	=> 'FR',   
      );

      $opts = array(
	        'http' => array(
	            'user_agent' => 'PHPSoapClient'
	        ),
	        'ssl' => array(
			    'verify_peer' => false,
			    'verify_peer_name' => false,
			    'allow_self_signed' => true
			    )
	    );
	    $context = stream_context_create($opts);
	    
	   try
			{
		      $soap = new SoapClient($config['facq_wsdl'],array('trace' => true, 'cache_wsdl' => WSDL_CACHE_MEMORY, 'stream_context' => $context));
			  $res = $soap->GetBasket(array("request" => $requestParams));

	

			  $code = $res->GetBasketResult->Status->Code;
			  $desc = $res->GetBasketResult->Status->Description;
		    
		 
		      if($code=='0' || $code =='4'){
			      return true;
		      }elseif($code=='1'){
		      	msg::error(gm('Please provide a valid User Id, Login and password to activate the integration'),"error");
			    return false;
		      }else{
		      	msg::error($desc,"error");
			    return false;
		      }
           }
     	catch(SoapFault $fault)
			{
				//var_dump($fault); exit();
					msg::error ( gm('Error'), 'error');					
					return false;
		
			}
    }

    function add_supplier_facq(){
      global $config;

    $supplier_id=$this->db->field("SELECT customer_id FROM customers WHERE facq='1' ");

    if(!$supplier_id){

    	$in['cat_id'] = 0;
		if(SET_DEF_PRICE_CAT == 1){
			$in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
		}

        $vat = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_VAT' ");
        $vat_default=str_replace(',', '.', $vat);
        $selected_vat = $this->db->field("SELECT vat_id FROM vats WHERE value = '".$vat_default."' ");
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
       	$payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");

    	$vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

	 	if(empty($vat_regime_id)){
	 		$vat_regime_id = 0;
	 	}
	 
       $in['customer_id'] = $this->db->insert("INSERT INTO customers SET
												name 					= 'FACQ',
												commercial_name 		= 'FACQ',
												active					= '1',
												website					= 'https://www.facq.be',
												language				= '2',
												c_email					= 'info@facq.be',
												comp_phone				= '3227198511',
												comp_fax				= '',
												vat_id 					= '".$selected_vat."',
												zip_name				= '1050',
												our_reference			= '',
												invoice_email_type		= '1',
												city_name 				= 'Ixelles',
												country_name 			= 'Belgium',
												payment_term 			= '".$payment_term."',
												payment_term_type 		= '".$payment_term_type."',
												cat_id					= '".$in['cat_id']."',
												vat_regime_id			= '".$vat_regime_id."',
												btw_nr					= 'BE0416.587.977',
												check_vat_number		= 'BE0416.587.977',
												is_supplier				= '1',
												is_customer				= '0',
												user_id 				= '".$_SESSION['u_id']."',
												first_letter 			= 'F',
												facq 					='1',
												creation_date			= '".time()."' ");

		 $address_id = $this->db->insert("INSERT INTO customer_addresses SET
												customer_id			=	'".$in['customer_id']."',
												country_id			=	'26',
												state_id			=	'0',
												city				=	'Ixelles',
												zip					=	'1050',
												address				=	'Rue du Couloir 20',
												billing				=	'1',
												is_primary			=	'1',
												delivery			=	'1',
												site			=	'1'");
		

		$address ='Rue du Couloir 20, 1050 Ixelles, Belgium';

		$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&key=AIzaSyBRSP5cH-xB72XqXpUN2kANTRcmZYWAkxE&sensor=false');
		$output= json_decode($geocode);
		$in['lat'] = $output->results[0]->geometry->location->lat;
		$in['lng'] = $output->results[0]->geometry->location->lng;
		
		$this->db->query("INSERT INTO addresses_coord SET
							customer_id='".$in['customer_id']."',
							location_lat='".$in['lat']."',
							location_lng='".$in['lng']."'");
		
		insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']).' {l}by activating Facq integration{endl}','customer_id',$in['customer_id']);
    }
 
	  return true;

     
    }


    function exact_online_disconnect(&$in){
		$app_id=$this->db->field("SELECT app_id FROM apps WHERE name='Exact online' AND type='main' AND main_app_id='0' ");
		$this->db->query("UPDATE apps SET active='0' WHERE app_id='".$app_id."' ");
		msg::success ( gm("Account deactivated"),'success');
		$in['done']=0;
		$in['xget']='exact_online';
		return true;
	}

	function netsuite_disconnect(&$in){
		$netsuite_id=$this->db->field("SELECT app_id FROM apps WHERE name='Netsuite' AND type='main' AND main_app_id='0' ");
		$this->db->query("UPDATE apps SET active='0' WHERE app_id='".$netsuite_id."' ");
		msg::success ( gm("Account deactivated"),'success');
		$in['done']=0;
		$in['xget']='apps';
		return true;
	}

	function netsuite_connect_validate(&$in){

		$app = $this->db->field("SELECT app_id FROM apps WHERE name='Netsuite' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
		if(!$app){
			msg::error(gm('Error'),'error');
			json_out($in);
		}

		$v=new validation($in);
		return $v->run();
	}

	function netsuite_connect(&$in){
		if(!$this->netsuite_connect_validate($in)){
			json_out($in);
			return false;
		}

		$netsuite_id=$this->db->field("SELECT app_id FROM apps WHERE name='netsuite' AND type='main' AND main_app_id='0' ");

		if($netsuite_id && $in['active']){
			$this->db->query("UPDATE apps SET active='1' WHERE app_id='".$netsuite_id."' ");
			
			msg::success ( gm('Netsuite activated'),'success');
		}

		mark_invoices_as_exported('netsuite', false);	
		
		$in['xget']='apps';
		return true;
	}

	function jefacture_disconnect(&$in){
		$jefacture_id=$this->db->field("SELECT app_id FROM apps WHERE name='Jefacture' AND type='main' AND main_app_id='0' ");
		$this->db->query("UPDATE apps SET active='0', api ='' WHERE app_id='".$jefacture_id."' ");

		msg::success ( gm("Account deactivated"),'success');
		$in['done']=0;
		$in['xget']='apps';
		return true;
	}

	function jefacture_connect_validate(&$in){

		$app = $this->db->field("SELECT app_id FROM apps WHERE name='Jefacture' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
		if(!$app){
			msg::error(gm('Error'),'error');
			json_out($in);
		}

		$v=new validation($in);
		$v->field('api_key',gm('Api key'),'required');
		return $v->run();
	}

	function jefacture_connect(&$in){
		global $config;
		if(!$this->jefacture_connect_validate($in)){
			json_out($in);
			return false;
		}

		$jefacture_base_url = $config['jefacture_base_url'].'api/universal_connector/v1/uc/ar';
		$in['username'] =$config['jefacture_user'];
		
        $data=array(
			'file'				=> ''
			);

		$ch = curl_init();
    	$headers=array('Content-Type: multipart/form-data');
		
    	
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);   	
    	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($ch, CURLOPT_USERPWD, $in['username'].":".$in['api_key']);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_URL, $jefacture_base_url);

    	$put = curl_exec($ch);
    	$info = curl_getinfo($ch);
 
    	if($info['http_code']==401){
    		$err=json_decode($put);
    		msg::error(gm('The authentication failed. Check API key.'),"api_key");
    		$in['from_connect']=1;
    		json_out($in);
    	}elseif($info['http_code']==404){
    		$err=json_decode($put);
    		msg::error(gm('Error'),"error");
    		json_out($in);
    	}else{

    		$this->db->query("UPDATE apps SET active='1', api='".trim($in['api_key'])."' WHERE name='Jefacture' AND type='main' AND main_app_id='0' ");
    		
    		msg::success ( gm('Jefacture activated'),'success');
    		return true;
    	}
    		
		$in['xget']='apps';
		return true;
	}

	
}

?>