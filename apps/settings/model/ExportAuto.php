<?php

/**
* 
*/
class ExportAuto
{
	private $db;
	private $db_users;

	function __construct(){
		$this->db = new sqldb();
		global $database_config;
		$this->database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$this->db_users =  new sqldb($this->database_2);
	}

	function updateExportAutoInvoice($in){
		if(!$this->updateExportAutoInvoice_validate($in)){
			json_out($in);
		}
		$exportAutoInvoice=$this->db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_INVOICE_".strtoupper($in['module'])."' ");
		if(is_null($exportAutoInvoice)){
			$this->db->query("INSERT INTO settings SET constant_name='EXPORT_AUTO_INVOICE_".strtoupper($in['module'])."', value='".$in['invoice']."' ");
		}else{
			$this->db->query("UPDATE settings SET value='".$in['invoice']."' WHERE constant_name='EXPORT_AUTO_INVOICE_".strtoupper($in['module'])."'");
		}
		$exportAutoPInvoice=$this->db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_PINVOICE_".strtoupper($in['module'])."' ");
		if(is_null($exportAutoPInvoice)){
			$this->db->query("INSERT INTO settings SET constant_name='EXPORT_AUTO_PINVOICE_".strtoupper($in['module'])."', value='".$in['pinvoice']."' ");
		}else{
			$this->db->query("UPDATE settings SET value='".$in['pinvoice']."' WHERE constant_name='EXPORT_AUTO_PINVOICE_".strtoupper($in['module'])."'");
		}


		$yuki_projects_enabled = $this->db->field("SELECT value FROM settings WHERE constant_name='YUKI_PROJECTS' ");
        if(is_null($yuki_projects_enabled)){
          $this->db->query("INSERT INTO settings SET constant_name='YUKI_PROJECTS', value='".$in['yuki_projects_enabled']."' ");
        }else{
          $this->db->query("UPDATE settings SET value='".$in['yuki_projects_enabled']."' WHERE constant_name='YUKI_PROJECTS'");
        }
	 

		if(!$in['only_return']){
			msg::success(gm('Changes saved'),'success');
			json_out($in);
		}	
	}

	function updateExportAutoInvoice_validate($in){
		$v=new validation($in);
		$v->field('module','Module','required');
		$is_ok=$v->run();
		if($is_ok){
			$accepted_modules=array('codabox','yuki','clearfacts','efff','billtobox', 'exact_online');
			if(!in_array($in['module'], $accepted_modules)){
				msg::error('Wrong Module','error');
				$is_ok=false;
			}
		}
		return $is_ok;
	}

}



?>