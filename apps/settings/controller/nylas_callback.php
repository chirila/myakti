<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');
	global $config;

	global $database_config;

	$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
	);
	$db_users = new sqldb($database_users);

	$result=array();

	if(isset($in['code']) && !empty($in['code'])){
		$ch = curl_init();
		$headers=array('Content-Type: application/json');

		$data=array(
			'client_id'		 	=> $config['nylas_app_id'],
			'client_secret' 	=> $config['nylas_app_secret'],
			'grant_type' 		=> "authorization_code",
			'code'				=> $in['code']
			);
		$data=json_encode($data);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/oauth/token');
        	
        $put = curl_exec($ch);
	    $info = curl_getinfo($ch);

	    if($info['http_code']>300 || $info['http_code']==0){
    		msg::error(gm("Activation process not finished succesfully"),"error");
	    }else{
	    	$resp=json_decode($put);
	    	//$data_e=$db_users->field("SELECT id FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
	    	$data_e=$db_users->field("SELECT id FROM nylas_data WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	    	if($data_e){
	    		/*$db_users->query("UPDATE nylas_data SET email='".$resp->email_address."',
	    			 								token='".$resp->access_token."',
	    			 								account_id='".$resp->account_id."',
	    			 								active='1' 
	    			 								WHERE id='".$data_e."' ");*/
				$db_users->query("UPDATE nylas_data SET email= :email,
	    			 								token= :token,
	    			 								account_id= :account_id,
	    			 								active= :active 
	    			 								WHERE id= :id ",
	    			 							[	'email'=>$resp->email_address,
	    			 								'token'=>$resp->access_token,
	    			 								'account_id'=>$resp->account_id,
	    			 								'active'=>'1', 
	    			 								'id'=>$data_e]
	    			 						);	    			 								
	    	}else{
	    		/*$db_users->query("INSERT INTO nylas_data SET user_id='".$_SESSION['u_id']."',
	    			 								email='".$resp->email_address."',
	    			 								token='".$resp->access_token."',
	    			 								account_id='".$resp->account_id."',
	    			 								active='1' ");*/
				$db_users->insert("INSERT INTO nylas_data SET user_id= :user_id,
	    			 								email= :email,
	    			 								token= :token,
	    			 								account_id= :account_id,
	    			 								active= :active ",
	    			 							[	'user_id'=>$_SESSION['u_id'],
	    			 								'email'=>$resp->email_address,
	    			 								'token'=>$resp->access_token,
	    			 								'account_id'=>$resp->account_id,
	    			 								'active'=>'1']
	    			 						);	    			 								
	    	}
	    	msg::success(gm("Activation process finished succesfully"),"success");
	    }
				
	}else{
		msg::error(gm("Activation process not finished succesfully"),"error");
	}


return json_out($result);
?>