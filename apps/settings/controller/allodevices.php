<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	class AlloDevices extends Controller{

		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);		
		}

		public function get_List(){
			$in=$this->in;
			$result=array('list'=>array());

			$devices=$this->db->query("SELECT * FROM allo_devices WHERE user_id='".$in['user_id']."' ");
			while($devices->next()){
				$item=array(
						'id'			=> $devices->f('id'),
						'name'			=> stripslashes($devices->f('device_name')),
						'first_name'	=> stripslashes($devices->f('first_name')),
						'last_name'		=> stripslashes($devices->f('last_name')),
					);
				array_push($result['list'], $item);
			}

			$this->out = $result;
		}

		public function get_Devices(){
			$in=$this->in;
			$result=array('list'=>array(),'user_id'=>$in['user_id']);
			$devices=array();
			$users=array();
			
			$a_data=$this->db->query("SELECT app_id,api FROM apps WHERE name='Allocloud' AND type='main' AND main_app_id='0' ");
			$in['username']=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$a_data->f('app_id')."' AND type='username' ");
			$in['api_key']=$a_data->f('api');
			$in['url']=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$a_data->f('app_id')."' AND type='url' ");			
			$expire=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$a_data->f('app_id')."' AND type='expire' ");
			$token_reset=true;
			if(!$expire || $expire < time()){
				include(__DIR__.'/../../misc/model/misc.php');
				$misc_obj=new misc();
				$token_reset=$misc_obj->renewAlloToken($in);		
			}
			if(!$token_reset){
				json_out($result);
			}
			$token=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$a_data->f('app_id')."' AND type='token' ");
			$ch = curl_init();
    		$headers=array('Content-Type: application/json','X-Auth-Token:'.$token);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        	curl_setopt($ch, CURLOPT_URL, $in['url'].'/devices');

        	$put = curl_exec($ch);
	    	$info = curl_getinfo($ch);

	    	if($info['http_code']>300 || $info['http_code']==0){
	    		//
	    	}else{
	    		$data=json_decode($put);
	    		foreach($data->data as $key=>$value){
	    			$temp_devices=array(
	    					'device_id'	=> $value->id,
	    					'name'		=> $value->name,
	    					'owner_id'	=> $value->owner_id
	    				);
	    			array_push($devices, $temp_devices);
	    		}

	    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        	curl_setopt($ch, CURLOPT_URL, $in['url'].'/users');

	        	$put = curl_exec($ch);
		    	$info = curl_getinfo($ch);

		    	if($info['http_code']>300 || $info['http_code']==0){
		    		//
		    	}else{
		    		$user_data=json_decode($put);
		    		foreach($user_data->data as $key=>$value){
		    			$temp_users=array(
		    					'owner_id'		=> $value->id,
		    					'email'			=> $value->email,
		    					'first_name'	=> $value->first_name,
		    					'last_name'		=> $value->last_name,
		    				);
		    			array_push($users, $temp_users);
		    		}

		    	}
	    	}
	    	foreach($devices as $key=>$value){
	    		foreach($users as $key1=>$value1){
	    			if($value['owner_id'] == $value1['owner_id']){
	    				$devices[$key]['email']=$value1['email'];
	    				$devices[$key]['first_name']=$value1['first_name'];
	    				$devices[$key]['last_name']=$value1['last_name'];
	    				break;
	    			}
	    		}
	    	}
	    	$devices_new=array();
	    	$devices_e=$this->db->query("SELECT device_id FROM allo_devices WHERE user_id='".$in['user_id']."' ")->getAll();
	    	foreach($devices as $key=>$value){
	    		$is_in=false;
	    		foreach($devices_e as $key1=>$value1){
	    			if($value['device_id']==$value1['device_id']){
	    				$is_in=true;
	    			}
	    		}
	    		if(!$is_in){
	    			array_push($devices_new, $value);
	    		}
	    	}

	    	$result['list']=$devices_new;

        	$this->out = $result;
		}
	}

	$cash_data = new AlloDevices($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$cash_data->output($cash_data->$fname($in));
	}

	$cash_data->get_List();
	$cash_data->output();
?>