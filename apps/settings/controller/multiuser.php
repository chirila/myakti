<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db2 = new sqldb();
		$db = new sqldb($database_users);
		$data = array();
		$data['group_list'] = array('groups'=>array(),'language'=>array());
		//$main_id = $db->field("SELECT lang_id FROM users WHERE user_id='".$_SESSION['u_id']."'  ");
		$main_id = $db->field("SELECT lang_id FROM users WHERE user_id= :user_id  ",['user_id'=>$_SESSION['u_id']]);
		$data['group_list']['language'] = array(
			'LANGUAGE_DD'			=> build_language_dd_user($main_id),
			'lang_id'				=> $main_id,
		);

		$groups = $db2->query("SELECT * FROM groups WHERE group_id!=2");
		while ($groups->move_next()) {
				$group_data = array(
					"NAME"				=> $groups->f('name'),
					"EDIT_LINK"			=> 'index.php?do=settings-group&group_id='.$groups->f('group_id'),
					"group_id"			=> $groups->f('group_id'),
					'HIDE_DELETE'		=> $groups->f('is_default') == 1 ? false : true,
					'NAME'				=> $groups->f('name'),
					'show_me'			=> $show_me,
					'show_him'			=> $show_him,
					'ADMINISTRATOR'		=> $user,
					'GROUP_ID'			=> $groups->f('group_id'),
					'page_title'		=>	gm('Edit Group'),
					'CRM' 				=> in_array('1',$credentials) ? true : false,
					'PROJECTS' 			=> in_array('3',$credentials) ? true : false,
					'MAINTENANCE' 		=> in_array('13',$credentials) ? true : false,
					'TIMESHEET' 		=> True,
					'BILLING' 			=> in_array('4',$credentials) ? true : false,
					'QUOTES' 			=> in_array('5',$credentials) ? true : false,
					'CONTRACTS' 		=> in_array('11',$credentials) ? true : false,
					'ARTICLES' 			=> in_array('12',$credentials) ? true : false,
					'ORDERS' 			=> in_array('6',$credentials) ? true : false,
					'PO_ORDERS' 		=> in_array('14',$credentials) ? true : false,
					'MANAGE' 			=> in_array('7',$credentials) ? true : false,
					'WEBSHOP'			=> in_array('9',$credentials) ? true : false,
				  	'DISABLE'			=> $groups->f('is_default') == 1 ? 'disabled' : '',
					'HIDE'				=> $groups->f('is_default') == 1 ? 'hide' : '',
					'HIDE_A'			=> in_array('3',$credentials) ? '' : 'hide',
				);
			array_push($data['group_list']['groups'],$group_data);
		}


		$is_new_subscription = $db2->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION' ");

		$data['group'] =array();
		$data['modules'] =array();
		if(is_null($is_new_subscription)){
			
			$menu = array();
			

			$menu['free'] = array('1'=>gm('CRM'),'12'=>gm('Catalogue'),'16'=>gm('Stock'),'3'=>gm('Projects'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports') );
			$menu['services'] = array('1'=>gm('CRM'),'12'=>gm('Catalogue'),'5'=>gm('Quotes'),'16'=>gm('Stock'),'3'=>gm('Projects'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports') );
			$menu['goods'] = array('1'=>gm('CRM'),'5'=>gm('Quotes'),'12'=>gm('Catalogue'),'6'=>gm('Orders'),'14'=>gm('Purchase Orders'),'16'=>gm('Stock'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports') );
			$menu['both'] = array('1'=>gm('CRM'),'5'=>gm('Quotes'),'12'=>gm('Catalogue'),'6'=>gm('Orders'),'14'=>gm('Purchase Orders'),'16'=>gm('Stock'),'3'=>gm('Projects'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'4'=>gm('Invoices'),'11'=>gm('Contracts'),'15'=>gm('CashRegister'),'18'=>gm('Reports') );

			foreach ($menu[$_SESSION['acc_type']] as $key => $value) {
				//$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_".$key."' AND user_id='".$_SESSION['u_id']."' ");
				$val =$db->field("SELECT value FROM user_meta WHERE name=:name AND user_id= :user_id ",['name'=>'MODULE_'.$key,'user_id'=>$_SESSION['u_id']]);
				if($val===NULL){
					$val = $db2->field("SELECT value FROM settings WHERE constant_name='MODULE_".$key."' ");
				}
				if($key!='4' && $key!='10' && $user_type=='1000'){
					continue;
				}

				$line=array(
					'module'		=> $value,
					'key'			=> $key,
					'checked'		=> $val == 1 ? true : false,
					'disable'		=> in_array($key, perm::$allow_apps) ? false : true,
				);
				if($key=='12'){
					$line['disable'] = in_array(12, perm::$allow_apps) || in_array(3, perm::$allow_apps) ? false : true;
				}
				$data['modules'][$key] = $line;

			}

			

		}else{
		
			$menu = array('1'=>gm('CRM'),'12'=>gm('Catalogue'),'5'=>gm('Quotes'),'6'=>gm('Orders'),'14'=>gm('Purchase Orders'),'16'=>gm('Stock'),'3'=>gm('Projects'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'11'=>gm('Contracts'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports') );
			foreach (perm::$allow_apps as $key => $value) {
				if($menu[$value]){
					//$val =$db->field("SELECT value FROM user_meta WHERE name='MODULE_".$value."' AND user_id='".$_SESSION['u_id']."' ");
					$val =$db->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_'.$value,'user_id'=>$_SESSION['u_id']]);
					if($val===NULL){
						$val = $db2->field("SELECT value FROM settings WHERE constant_name='MODULE_".$value."' ");
					}
					if($value!='4' && $value!='10' && $user_type=='1000'){
						continue;
					}

					$line=array(
						'module'		=> $menu[$value],
						'key'			=> $value,
						'checked'		=> $val == 1 ? true : false,
						'disable'		=> array_key_exists($value, $menu) ? false : true,
					);
					if($value=='12'){
						$line['disable'] = in_array(12, perm::$allow_apps) || in_array(3, perm::$allow_apps) ? false : true;

					}
					$data['modules'][$value] = $line;
				}
			}
		}

		$data['group']['is_CRM'] = $data['modules'][1]['checked'];
		$data['group']['is_PROJECTS'] = $data['modules'][3]['checked'];
		$data['group']['is_MAINTENANCE'] = $data['modules'][13]['checked'];
		$data['group']['is_BILLING'] = $data['modules'][4]['checked'];
		$data['group']['is_QUOTES'] = $data['modules'][5]['checked'];
		$data['group']['is_CONTRACTS'] = $data['modules'][11]['checked'];
		$data['group']['is_ORDERS'] = $data['modules'][6]['checked'];
		$data['group']['is_PO_ORDERS'] = $data['modules'][14]['checked'];
		$data['group']['is_ARTICLES'] = $data['modules'][12]['checked'];
		$data['group']['is_MANAGE'] = $data['modules'][7]['checked'];
		$data['group']['is_WEBSHOP'] = $data['modules'][9]['checked'];
		$data['group']['is_STOCK'] = $data['modules'][16]['checked'];
		$data['group']['is_INSTALLATION'] = $data['modules'][17]['checked'];
		$data['group']['is_CASHREGISTER'] = $data['modules'][15]['checked'];
		$data['group']['is_REPORTS'] = $data['modules'][18]['checked'];
		$data['group']['page_title'] = gm('Add Group');
		$data['group']['show_him'] = true;


		json_out($data); 

?>