<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }

if($in['xget']){
		$fname = 'get_'.$in['xget'];
		if(function_exists($fname)){
			json_out($fname($in));
		}else{
			msg::error('Function does not exist','error');
			json_out($in);
		}
	}

	function get_setupIntent($in){
		global $config;
		ark::loadLibraries(array('stripe/init'));
		$result=array();
			
		\Stripe\Stripe::setApiKey($config['stripe_api_key']);

		$setup_intent = \Stripe\SetupIntent::create([
		  'usage' => 'off_session', 
		]);

		$result['client_secret']				= $setup_intent->client_secret;
		$result['intent_id']					= $setup_intent->id;
		$result['stripe_cust_id']				= $in['stripe_cust_id'];
		$result['change_card']					= $in['change_card'];
		$result['stripe_val']					= $in['stripe_val'];

		
		return $result;
	}

	function get_stripeCustomer($in){
		
		ark::loadLibraries(array('stripe/init'));
		global $database_config, $config;
		$result=array();
		
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$db_users = new sqldb($db_config);

		$u_plan = $db_users->query("SELECT * FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		
		\Stripe\Stripe::setApiKey($config['stripe_api_key']);

		if($in['payment_method']){
			if($in['stripe_cust_id']){
				if($in['change_card']){
					try{ //account existent, care are competat stripe_cust_id si vrea sa isi schimbe payment method
						$payment_method = \Stripe\PaymentMethod::retrieve($in['payment_method']);
						$payment_method->attach(['customer' => $in['stripe_cust_id']]);

						$customer = \Stripe\Customer::update(
							 $in['stripe_cust_id'],	
							[ 
							  'invoice_settings'=> ['default_payment_method' => $in['payment_method']]
							]);

						$update = $db_users->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account changed payment method', `date`='".time()."' ");

						msg::success(gm('The card details were successfully changed'),"success");

					}catch(\Stripe\Error\Card $e){
						msg::notice($e->getMessage(),"notice");
					}	
				}			
				
			}else{//account nou
				try{
					$customer = \Stripe\Customer::create([
					  'email'			=> $in['stripe_email'],	
					  'name'			=> $in['stripe_name'],
					  'description'		=> $u_plan->f('ACCOUNT_COMPANY').'-'.$u_plan->f('first_name').' '.$u_plan->f('last_name'),
					  'payment_method' 	=> $in['payment_method'],
					  'invoice_settings'=> ['default_payment_method' => $in['payment_method']]
					]);
					$result['customer']		= $customer->id;

					//preluate de pe subscription_new
					if(strpos($config['site_url'],'my.akti')){
					//if(strpos($config['site_url'],'akti')){
					
						$database_new = array(
								'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
								'username' => 'admin',
								'password' => 'hU2Qrk2JE9auQA',
								'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
								//'database' => '32ada082_5cf4_1518_3aa09bca7f29'
						);
							
						$db_user_new = new sqldb($database_new);

						$select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$_SESSION['u_id']."' AND field_id='1' ");
						if($select_contact){
							$select_customer = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$select_contact."' AND `primary`='1'");
						}
						if(!$select_customer){
							$select_customer = $db_user_new->field("SELECT customer_id FROM customer_field WHERE value='".$_SESSION['u_id']."' AND field_id='7' ");
						}		
						if($select_customer){
							$db_user_new->query("UPDATE customers SET stripe_cust_id='".$customer->id."' WHERE customer_id='".$select_customer."'");
							$db_user_new->query("INSERT INTO customer_field SET
														field_id='1',
														customer_id='".$select_customer."',
														value='".$customer->id."'");
						}			
						
					}
					include_once('apps/settings/model/payment.php');
					$payment=new payment(); 
					$payment->pay_new($in);
					$next_date = $in['period'] =='1' ? strtotime('+1 month') : strtotime('+1 year');
					$time = time();
					$update = $db_users->query("UPDATE users SET payed='1', alias='".$_SESSION['u_id']."', pricing_plan_id ='".$in['pricing_plan_id']."', base_plan_id ='".$in['pricing_plan_id']."', period ='".$in['period']."', stripe_cust_id= '". $customer->id."' WHERE database_name='".DATABASE_NAME."' AND active!='1000'");
					$db_users->query("UPDATE users SET payed= :payed, alias= :alias, pricing_plan_id =:pricing_plan_id, base_plan_id =:base_plan_id, period =:period WHERE database_name= :d AND active= :active",['payed'=>'1','alias'=>$_SESSION['u_id'],'pricing_plan_id'=>$in['pricing_plan_id'],'base_plan_id'=>$in['pricing_plan_id'],'period'=>$in['period'],'d'=>DATABASE_NAME,'active'=>'1000']);
					$user_update = $db_users->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' ");
					while ($user_update->next()) {
							$update = $db_users->query("UPDATE user_info SET end_date='".$next_date."', is_trial='1' WHERE user_id='".$user_update->f('user_id')."' ");
						}
					$result['payed'] = true;
					unset($_SESSION['is_trial']);
					$first_paid = $db_users->field("SELECT first_paid FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' ");
					if(!$first_paid){
						$paid_update = $db_users->query("UPDATE users SET first_paid='".$time."'
																	WHERE database_name='".DATABASE_NAME."'");
						
						$in['time']=$time;
						$in['next_date']=$next_date;
						create_reccuring_invoice($in);
						send_email_won_customer($_SESSION['u_id'], $in['period']);											
					}else{
						send_email_update_subscription($_SESSION['u_id'], $in['pricing_plan_id'], $in['period'], $in['users']);
					}

						
					msg::success(gm('The payment process was successful'),"success");
				}catch(\Stripe\Error\Card $e){
						msg::notice($e->getMessage(),"notice");
					}
			}




			
		}else{
			msg::error('',"error");
		}

		
		return $result;
	}

	function get_updateSubscription($in){
		
		global $database_config, $config;
		$result=array();
		
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$db_users = new sqldb($db_config);

		$u_plan = $db_users->query("SELECT * FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		
		include_once('apps/settings/model/payment.php');
		$payment=new payment(); 
		$payment->pay_new($in);
		$next_date = $in['period'] =='1' ? strtotime('+1 month') : strtotime('+1 year');
		$time = time();
		$update = $db_users->query("UPDATE users SET payed='1', alias='".$_SESSION['u_id']."', pricing_plan_id ='".$in['pricing_plan_id']."', base_plan_id ='".$in['pricing_plan_id']."', period ='".$in['period']."' WHERE database_name='".DATABASE_NAME."' AND active!='1000'");
		$db_users->query("UPDATE users SET payed= :payed, alias= :alias, pricing_plan_id =:pricing_plan_id, base_plan_id =:base_plan_id, period =:period WHERE database_name= :d AND active= :active",['payed'=>'1','alias'=>$_SESSION['u_id'],'pricing_plan_id'=>$in['pricing_plan_id'],'base_plan_id'=>$in['pricing_plan_id'],'period'=>$in['period'],'d'=>DATABASE_NAME,'active'=>'1000']);
		$user_update = $db_users->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' ");
		while ($user_update->next()) {
				$update = $db_users->query("UPDATE user_info SET end_date='".$next_date."', is_trial='1' WHERE user_id='".$user_update->f('user_id')."' ");
			}

		if(strpos($config['site_url'],'my.akti')){
			global $database_config;
			$database_new = array(
					'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
					'username' => 'admin',
					'password' => 'hU2Qrk2JE9auQA',
					'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
					//'database' => '32ada082_5cf4_1518_3aa09bca7f29'
			);
				
			$db_user_new = new sqldb($database_new);

			$select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$_SESSION['u_id']."' AND field_id='1' ");
			if($select_contact){
				$select_customer = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$select_contact."' AND `primary`='1'");
			}
			if(!$select_customer){
				$select_customer = $db_user_new->field("SELECT customer_id FROM customer_field WHERE value='".$_SESSION['u_id']."' AND field_id='7' ");
			}		
			if($select_customer){
				$stripe_cust_id = $db_users->query("SELECT stripe_cust_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']] );
				if($stripe_cust_id){
					$db_user_new->query("UPDATE customers SET stripe_cust_id='".$stripe_cust_id."' WHERE customer_id='".$select_customer."'");
					$select_stripe_cust_id = $db_user_new->field("SELECT value FROM customer_field WHERE customer_id='".$select_customer."' AND field_id='1' ");
					if(!$select_stripe_cust_id){
						$db_user_new->query("INSERT INTO customer_field SET
											field_id='1',
											customer_id='".$select_customer."',
											value='".$stripe_cust_id."'");
					}else{
						$db_user_new->query("UPDATE customer_field SET value ='".$stripe_cust_id."' WHERE customer_id='".$select_customer."' AND field_id='1'");
					}

				}		
				
			}				
		}

		$result['payed'] = true;
		unset($_SESSION['is_trial']);
		//$first_paid = $db_users->field("SELECT first_paid FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' ");
		/*if(!$first_paid){
			$paid_update = $db_users->query("UPDATE users SET first_paid='".$time."'
														WHERE database_name='".DATABASE_NAME."'");
			
			$in['time']=$time;
			$in['next_date']=$next_date;
			create_reccuring_invoice($in);
			send_email_won_customer($_SESSION['u_id'], $in['period']);											
		}else{*/
			send_email_update_subscription($_SESSION['u_id'], $in['pricing_plan_id'], $in['period'], $in['users']);
		/*}*/
		msg::success(gm('The update subscription process was successful'),"success");
		
		return $result;
	}

?>