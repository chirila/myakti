<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');
	global $config;

	global $database_config;

	$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
	);
	$db_users = new sqldb($database_users);
	$db = new sqldb();

	$result=array();

	if(isset($in['code']) && !empty($in['code'])){
		$ch = curl_init();

		$data=array(
			'client_id'		 	=> $config['clearfacts_app_id'],
			'client_secret' 	=> $config['clearfacts_app_secret'],
			'grant_type' 		=> 'authorization_code',
			'code'				=> $in['code'],
			'redirect_uri'		=> $config['clearfacts_redirect_url'],
			);

    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $config['clearfacts_base_url'].'/oauth2-server/token');
        	
        $put = curl_exec($ch);
	    $info = curl_getinfo($ch);

	    if($info['http_code']>300 || $info['http_code']==0){
    		msg::error(gm("Activation process not finished succesfully"),"error");
	    }else{
	    	$token=json_decode($put);
	    	$app = $db->query("SELECT * FROM apps WHERE name='ClearFacts' AND type='main' AND main_app_id='0' ");
			if($app->next()){
				$db->query("UPDATE apps SET api ='".$token->access_token."', active = '1' 
	    			 								WHERE app_id= '".$app->f('app_id')."'");	    
			}


	        mark_invoices_as_exported('clearfacts', true);

	    	msg::success(gm("Activation process finished succesfully"),"success");
	    	$result['redirect_link']=$config['site_url']."settings/?tab=apps&cfconn=1";
	    	$result['is_clearfacts']=true;
	    }
				
	}else{
		msg::error(gm("Activation process not finished succesfully"),"error");
	}


return json_out($result);
?>