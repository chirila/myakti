<?php

	/*if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}*/

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		if(function_exists($fname)){
			json_out($fname($in));
		}else{
			msg::error('Function does not exist','error');
			json_out($in);
		}
	}

	function get_company($in){
		$db = new sqldb();
		$data = array( 'account_address'=>array());
		$account_company = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_COMPANY'");
		$account_email = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_EMAIL'");
		$account_url = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_URL'");
		$account_description = $db->field("SELECT long_value FROM settings WHERE constant_name='ACCOUNT_DESCRIPTION'");
		$account_bank_name = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_BANK_NAME'");
		$account_bic_code = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_BIC_CODE'");
		$account_iban = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_IBAN'");
		$account_reg_number = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_REG_NUMBER'");
		$account_phone = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PHONE'");
		$account_fax = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_FAX'");
		$account_siret = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_SIRET'");
		$account_rpr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_RPR'");
		$account_den_sociale = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DEN_SOCIALE'");
		$account_reg_commerce = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_REG_COMMERCE'");
		$account_cap_social = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CAP_SOCIAL'");
		$account_code_naf = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CODE_NAF'");
		$account_juridical_form = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_JURIDICAL_FORM'");
		$invoice_address = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ADDRESS_INVOICE'");

		$logos =$db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_LOGO'");

	    	$account_country = $db->query("SELECT * from settings where  constant_name='ACCOUNT_DELIVERY_COUNTRY_ID'");
	    	
	    	$account_country->move_next();
	    	$account_delivery_country_id=$account_country->f('value');
	    	if (!$account_delivery_country_id)
	    	{
	    		$account_delivery_country_id = $in['account_delivery_country_id'];
	    	}

	    	$billing_country = $db->query("SELECT * from settings where  constant_name='ACCOUNT_BILLING_COUNTRY_ID'");
		    $billing_country->move_next();
		    $account_billing_country_id=$billing_country->f('value');
		    if (!$account_billing_country_id)
		    {
		    	$account_billing_country_id = $in['account_billing_country_id'];
		    }

		    $image ='images/no-logo.png';

	    	$account_delivery_zip = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DELIVERY_ZIP'");
	    	$account_delivery_address = $db->field("SELECT long_value FROM settings WHERE constant_name='ACCOUNT_DELIVERY_ADDRESS'");
	    	$account_delivery_city = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DELIVERY_CITY'");
	    	$account_billing_address = $db->field("SELECT long_value FROM settings WHERE constant_name='ACCOUNT_BILLING_ADDRESS'");
	    	$account_vat_number = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_VAT_NUMBER'");
	    	$account_billing_zip = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_BILLING_ZIP'");
	    	$account_billing_city = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_BILLING_CITY'");
	   	$data['account_address'] = array(
	   		'INVOICE_ADDRESS'				=> $invoice_address==1 ? true : false,
	   		'ACCOUNT_COMPANY' 				=> $account_company,
			'ACCOUNT_EMAIL' 				=> $account_email,
			'ACCOUNT_URL' 					=> $account_url,
			'ACCOUNT_DESCRIPTION' 			=> $account_description,
			'ACCOUNT_BANK_NAME' 			=> $account_bank_name,
			'ACCOUNT_BIC_CODE' 				=> $account_bic_code,
			'ACCOUNT_IBAN' 					=> $account_iban,
			'ACCOUNT_REG_NUMBER' 			=> $account_reg_number,
			'ACCOUNT_PHONE' 				=> $account_phone,
			'ACCOUNT_FAX' 					=> $account_fax,
			'ACCOUNT_SIRET' 				=> $account_siret,
			'ACCOUNT_RPR' 					=> $account_rpr,
			'ACCOUNT_DEN_SOCIALE' 			=> $account_den_sociale,
			'ACCOUNT_REG_COMMERCE' 			=> $account_reg_commerce,
			'ACCOUNT_CAP_SOCIAL' 			=> $account_cap_social,
			'ACCOUNT_CODE_NAF' 				=> $account_code_naf,
			'ACCOUNT_JURIDICAL_FORM' 		=> $account_juridical_form,
	   		'ACCOUNT_DELIVERY_COUNTRY_ID' 	=>$account_delivery_country_id,
	   		'ACCOUNT_DELIVERY_COUNTRY_DD' 	=>build_country_list($account_delivery_country_id),
	   		'ACCOUNT_DELIVERY_ZIP'			=>$account_delivery_zip,
	   		'ACCOUNT_DELIVERY_ADDRESS'		=>$account_delivery_address,
	   		'ACCOUNT_DELIVERY_CITY'			=>$account_delivery_city,
	   		'ACCOUNT_BILLING_ADDRESS'		=>$account_billing_address,
	   		'ACCOUNT_VAT_NUMBER'			=>$account_vat_number,
	   		'ACCOUNT_BILLING_ZIP'			=>$account_billing_zip,
	   		'ACCOUNT_BILLING_CITY'			=>$account_billing_city,
	   		'ACCOUNT_BILLING_COUNTRY_ID' 	=>$account_billing_country_id,
	   		'ACCOUNT_BILLING_COUNTRY_DD'	=>build_country_list($account_billing_country_id),
	   		'xget'							=>'company',
	   		'do'							=>'settings-settings-settings-CompanyInfo',	
	   		'logos'							=> $logos ? $logos.'?'.time() : $image,
	   	);
	   	global $database_config;
	   	$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);
		 // $accountant_id =$db_users->field("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' AND active=1000 ");
	   	// $data['is_accountant']	= $accountant_id ? true : false;
	   	$data['is_accountant'] =$db_users->field("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' AND active=1000 ") ? true : false;
	   	$data['is_zen']=$db->field("SELECT active FROM apps WHERE name='Zendesk' and type='main' AND main_app_id='0' ") ? true : false;
	   	$easyinvoice=$db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
	   	$data['easyinvoice']=$easyinvoice=='1'? true : false;
	   	$data['is_cozie']= DATABASE_NAME=='445e3208_dd94_fdd4_c53c135a2422'? true : false;
	   	$access_api_webhooks=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCESS_API_WEBHOOKS' ");
	   	$data['access_api_webhooks']=$access_api_webhooks=='1'? true : false;
	   	$sendgrid_activated = $db->field("SELECT `active` FROM apps WHERE name='SendGrid' AND type='main' AND main_app_id='0' ") ? true : false;
		$data['sendgrid_activated'] = $sendgrid_activated;
		$data['mail_settings']=get_mail_settings($in,true,false)['mail_settings'];
	   	
		return $data;
		return json_out($data, $showin,$exit);
	}
	function get_Regional($in){
		$db = new sqldb();
		$data = array( 'regional'=>array());
		$time_f = array(
	        // 'm/d/Y' => 'MM/DD/YYYY (e.g. 04/25/2011)',
			// 'd/m/Y' => 'DD/MM/YYYY (e.g. 25/04/2011)',
			// 'd/m/Y' => 'DD/MM/YYYY (e.g. 25/04/2011)',
	        // 'Y-m-d' => 'YYYY-MM-DD (e.g. 2011-04-25)',
	        // 'd.m.Y' => 'DD.MM.YYYY (e.g. 25.04.2011)',
	        // 'Y.m.d' => 'YYYY.MM.DD (e.g. 2011.04.25)',
			// 'Y/m/d' => 'YYYY/MM/DD (e.g. 2011/04/25)',
			'm/d/Y' => 'MM/DD/YYYY',
			'd/m/Y' => 'DD/MM/YYYY',
			'd/m/Y' => 'DD/MM/YYYY',
	        'Y-m-d' => 'YYYY-MM-DD',
	        'd.m.Y' => 'DD.MM.YYYY',
	        'Y.m.d' => 'YYYY.MM.DD',
	        'Y/m/d' => 'YYYY/MM/DD',
		);
		$cal_firs_week_day = array(
	        '1' => gm('Monday'),
	        '2' => gm('Tuesday'),
	        '3' => gm('Wednesday'),
	        '4' => gm('Thursday'),
	        '5' => gm('Friday'),
	        '6' => gm('Saturday'),
	        '7' => gm('Sunday'),
		);
		$currency_name = currency::get_currency(1,'array');
		$currency_type = $db->query("SELECT * from settings where constant_name='ACCOUNT_CURRENCY_TYPE'");
		$currency_type->move_next();
		$account_currency = $db->query("SELECT * from settings where constant_name='ACCOUNT_CURRENCY_FORMAT'");
		$account_currency->move_next();
		$account_number = $db->query("SELECT * from settings where constant_name='ACCOUNT_NUMBER_FORMAT'");
		$account_number->move_next();
		$time_zone = $db->query("SELECT * from settings where constant_name='ACCOUNT_TIME_ZONE'");
		$time_zone->move_next();
		$date_format = $db->query("SELECT * from settings where constant_name='ACCOUNT_DATE_FORMAT'");
		$date_format->move_next();
		$calendar = $db->query("SELECT * from settings where constant_name='CALENDAR_FIRST_WEEK_DAY'");
		$calendar->move_next();
		$week_day = $calendar->f('value');
		if($calendar->f('value')==''){
		    $week_day = 1;
		}
		if($calendar->f('value')=='0'){
		    $week_day = 7;
		}

		if($account_number->f('value') == ',.'){
			$special_nr_format='1';
		}else if($account_number->f('value') == '.,'){
			$special_nr_format='2';
		}else{
			$special_nr_format='3';
		}		

	   	$data['regional'] = array(
	   		'CURRENCY_TYPE_ID'				=> $currency_type->f('value'),
	   		'CURRENCY_TYPE_LIST'        	=> build_currency_list($currency_type->f('value')),
    		'DEFAULT_CURRENCY_NAME'     	=> build_currency_name_list($currency_type->f('value')),
    		'CURRENCY_SIGN_DEFAULT'     	=> get_commission_type_list($currency_type->f('value')),
    		'CURRENCY_NAME1'            	=> $currency_name[$currency_type->f('value')],
    		'CHECK_CUR1' 					=> $account_currency->f('value'),
		    'CHECK_FORMAT' 					=> $special_nr_format,
		    // 'CHECK_FORMAT' 				=> $account_number->f('value')=='.,'? true:false,
		    // 'CHECK_FORMAT' 				=> $account_number->f('value')==' ,'? true:false,
		    'TIME_ZONE_ID'					=> $time_zone->f('value'),
		    'TIME_ZONE_DD'					=> build_time_zone_dropdown($time_zone->f('value')),
		    'DATE_FORMAT_ID'				=> $date_format->f('value'),
		    'DATE_FORMAT_DD'				=> build_simple_dropdown_timezone($time_f,$date_format->f('value')),
		    'CAL_FIRST_WEEK_ID'				=> $week_day,
		    'CAL_FIRST_WEEK_DAY'			=> build_simple_dropdown_timezone($cal_firs_week_day,$week_day),
	   		'loopcurrency'					=> array(),
	   		'xget'							=>'Regional',
	   		'do'							=>'settings-settings-settings-Regional',	
	   	);

		/*		foreach ($currency_name as $key => $value) {
		    if($key == $currency_type->f('value')){
		        continue;
		    }
		    	   	$currency = $value;
					if(empty($currency)){
						$currency = "USD";
					}
					$separator = $account_number->f('value');
					$into = $in['into'];
		    $data['regional']['loopcurrency'][] = array(
		        'CURRENCY_SIGN'             => get_commission_type_list($key),
		        'CURRENCY_SIGN_DEFAULT'     => get_commission_type_list($currency_type->f('value')),
		        'CURRENCY_NAME'             => $value,
		        'CURRENCY_NR'				=> currency::getCurrency($currency,currency::get_currency($currency_type->f('value'),'code'), 1,$separator)
		    );
		}
		*/
		$data['regional']['currency_symbol_dd']=[
			['id'=>'0','name'=>gm('Place after the amount')],
			['id'=>'1','name'=>gm('Place before the amount')]
		];
		$data['regional']['check_format_dd']=[
			['id'=>'1','name'=>'1,234.56'],
			['id'=>'2','name'=>'1.234,56'],
			['id'=>'3','name'=>'1 234,56']
		];
		$data['language']=get_Language($in)['language'];
		return $data;
		return json_out($data, $showin,$exit);
	}
	function get_Financial($in){
		global $database_config,$config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);	
		$db = new sqldb();
		$data = array( 'financial'=>array());
		$currency_name = currency::get_currency(1,'array');
		$currency_type = $db->query("SELECT * from settings where constant_name='ACCOUNT_CURRENCY_TYPE'");
		$currency_type->move_next();
		$account_number = $db->query("SELECT * from settings where constant_name='ACCOUNT_NUMBER_FORMAT'");
		$account_number->move_next();
		$ARTICLE_PRICE_COMMA_DIGITS = $db->field("SELECT value FROM settings WHERE constant_name='ARTICLE_PRICE_COMMA_DIGITS'");
		$AAC = $db->field("SELECT value FROM settings WHERE constant_name='AAC'");
		$fiscal_year = $db->query("SELECT * from settings where constant_name='ACCOUNT_FISCAL_YEAR_START'");
		$fiscal_year->move_next();

		$data['financial'] = array(
			'CURRENCY_TYPE_ID'						=> $currency_type->f('value'),
	   		'CURRENCY_TYPE_LIST'        			=> build_currency_list($currency_type->f('value')),
	   		'CURRENCY_SIGN_DEFAULT'     			=> get_commission_type_list($currency_type->f('value')),
	   		'CURRENCY_NAME'            				=> $currency_name[$currency_type->f('value')],
	   		'currency_name_fixed'					=> currency::get_currency($currency_type->f('value'),'name'),
	   		'MONTH_ID'								=> $fiscal_year->f('value'),
		    'MONTH_DD'								=> build_month_list($fiscal_year->f('value')),
	   		'ARTICLE_PRICE_COMMA_DIGITS'			=> $ARTICLE_PRICE_COMMA_DIGITS,
	   		'payment_term'  						=> $db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' "),
			'choose_payment_term_type' 				=> $db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' "),
		/*	   		'payment_term'        					=> $db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' "),
    		'choose_payment_term_type'  			=> $db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' "),*/
	   		'xget'									=>'Financial',
	   		'do'									=>'settings-settings-settings-financial_settings',	
	   		'loopfinancial'							=> array(),
	   		'profit_margin_type'					=> $db->field("SELECT value FROM settings WHERE constant_name = 'PROFIT_MARGIN_TYPE' ")? 1: 0,
	   		'qrcode_purchaseinv'					=> $db->field("SELECT value FROM settings WHERE constant_name = 'QRCODE_PURCHASE_INVOICE' ")? true: false,
		);

		$now=time();
		$day=date('j',$now);
		$month = date('n',$now);
		$year = date('Y',$now);
		$day_start = mktime(0,0,0,$month,$day,$year);
		$day_end = mktime(23,59,59,$month,$day,$year);

		$rates_data=array();
		/*$rates_query=$db_users->query("SELECT * FROM conversion_rates WHERE `code`='".currency::get_currency($currency_type->f('value'),'code')."' AND `date` BETWEEN ".$day_start." AND ".$day_end." LIMIT 1")->getAll();
		if(!empty($rates_query)){
			$decoded_data=json_decode(unserialize($rates_query[0]['response']));
			if(is_object($decoded_data) && $decoded_data->result == 'success'){
				foreach($decoded_data->conversion_rates as $key=>$value){
					$rates_data[$key]=$value;
				}		
			}
		}else{
			$c_rest = new clientREST();	
			$return_data = $c_rest->execRequest("https://prime.exchangerate-api.com/v5/".$config['currency_api_key']."/latest/".currency::get_currency($currency_type->f('value'),'code'),'get',null);
			if($return_data){
				$db_users->query("INSERT INTO conversion_rates SET `code`='".currency::get_currency($currency_type->f('value'),'code')."',`response`='".addslashes(serialize($return_data))."',`date`='".time()."' ");
				$decoded_data=json_decode($return_data);
				if(is_object($decoded_data) && $decoded_data->result == 'success'){
					foreach($decoded_data->conversion_rates as $key=>$value){
						$rates_data[$key]=$value;
					}		
				}
			}
		}		*/
		$rates_query=$db->query("SELECT * FROM currency_rates 
			LEFT JOIN currency on currency_rates.currency_id_to_change = currency.id
			WHERE `currency_id_base`='".$currency_type->f('value')."' ");
		while($rates_query->move_next()){
			$code = $rates_query->f('code');
			$rates_data[$code]=$rates_query->f('rate');
		}
		foreach ($currency_name as $key => $value) {
		    if($key == $currency_type->f('value')){
		        continue;
		    }
		    	   	$currency = $value;
					if(empty($currency)){
						$currency = "USD";
					}
					$separator = $account_number->f('value');
					$into = $in['into'];
		    $data['financial']['loopfinancial'][] = array(
		        'FINANCIAL_SIGN'             	=> get_commission_type_list($key),
		        'FINANCIAL_SIGN_DEFAULT'     	=> get_commission_type_list($currency_type->f('value')),
		        'FINANCIAL_NAME'             	=> $value,
		        //'FINANCIAL_NR'					=> currency::getCurrency($currency,currency::get_currency($currency_type->f('value'),'code'), 1,$separator)
		        'FINANCIAL_NR'					=> array_key_exists($currency, $rates_data) ? display_number_var_dec($rates_data[$currency]) : display_number_var_dec('1'),
		        'currency_id' 					=> $key,
		    );
		}
		return $data;
		return json_out($data, $showin,$exit);
	}
	function get_Vat($in){
		$db = new sqldb();
		$data = array('vat'=>array());
		$def = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_VAT' ");

		$db->query("SELECT * FROM vats  ORDER BY value");
		$i=0;
		while($db->move_next()){
			$data['vat'][] = array(
				'VALUE'			=> display_number($db->f('value')),
				'ID'			=> $db->f('vat_id'),
				'DEFAULT'		=> display_number($def),
				'DELETE_LINK'	=> 'index.php?do=settings-vats_list-vat-delete&vat_id='.$db->f('vat_id').$arguments.$arguments_o,
				'xget'			=> 'Vat',
				'do'			=> 'settings-settings',
				'page_title'	=> gm('Edit Vat'),
			);
			$i++;
		}
		$data['vat_r']=get_Vat_regime($in,true,false)['vat_r'];
		return $data;
		return json_out($data, $showin,$exit); 
	}
	function get_Vat_regime($in){
		$db = new sqldb();
		$data = array('vat_r'=>array('vat'=>array()));
		$vat_r=array();
		$vats=$db->query("SELECT * FROM vats");
		while($vats->next()){
			array_push($vat_r,array('id'=>$vats->f('vat_id'),'name'=>$vats->f('value')));
		}
		$db->query("SELECT vat_new.*,vats.value FROM vat_new 
			LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
			ORDER BY IF(vat_new.name_id='' OR vat_new.name_id is null,1,0),vat_new.name_id ASC");
		$i=0;
		$regime_type=array(
			'1'		=> gm("Regular"),
			'2'		=> gm('Intra-EU'),
			'3'		=> gm("Export"),
		);
		$vat_regimes=array(
			array('id'=>'1','name'=>gm("Regular")),
			array('id'=>'2','name'=>gm('Intra-EU')),
			array('id'=>'3','name'=>gm("Export")),
			);
		while($db->move_next()){
			$data['vat_r']['vat'][] = array(
				'VALUE'			=> display_number($db->f('value')),
				'ID'			=> $db->f('id'),
				'NAME_ID'		=> stripslashes($db->f('name_id')),
				'DESCRIPTION'	=> stripslashes($db->f('description')),
				'NO_VAT'		=> $db->f('no_vat') ? true : false,
				'DEFAULT'		=> $db->f('default') ? $db->f('id') : 0,
				'vat_id'		=> $db->f('no_vat') ? 0 : $db->f('vat_id'),
				'xget'			=> 'Vat_regime',
				'do'			=> 'settings-settings',
				'page_title'	=> gm('Edit Vat Regime'),
				'vats'			=> $vat_r,
				'regs'			=> $vat_regimes,
				'regime_type'	=> $regime_type[$db->f('regime_type')],
				'regime_type_id'	=> $db->f('regime_type'),
			);
			$i++;
		}
		$data['vat_r']['vats']=$vat_r;
		$data['vat_r']['regs']=$vat_regimes;
		return $data;
	}
	function get_Language($in){
		$db = new sqldb();
		$data = array('language'=>array());
		$default_lang = $db->field("SELECT lang_id FROM pim_lang WHERE sort_order='1'");
		$publish_lang = $db->field("SELECT lang_id FROM pim_lang WHERE sort_order='1'");
		$data['language'] = array(
			'PUBLISH_LANG_ID'	=> $default_lang,
			'PUBLISH_LANG_DD'	=> build_publish_lang_dd($publish_lang,true),
			'public_lang'		=> array(
			       'public_lang' => array(),
			       'extra_lang' => array(),
			       'custom_lang' => array(),
			    ),
		);
		$k = 1;
		$i = 0;
		$db->query("select * from pim_lang GROUP BY lang_id ORDER BY lang_id");
		while ($db->move_next()) {
			//if($db->f('lang_id')!=$default_lang)
			//{
				$data['language']['public_lang']['public_lang'][] = array( 
					'NAME' 			=> gm($db->f('language')),
					'ID'			=> $db->f('lang_id'),
					'IS_CHECKED'	=> $db->f('active') == 1 ? true : false,
					'DEFAULT'		=> $db->f('lang_id')==$default_lang ? $db->f('lang_id') : '0'
				);
				$k++;
			//}
		}
		$db->query("SELECT * FROM pim_custom_lang WHERE flag!='' ORDER BY sort_order");
		while ($db->move_next()) {
			$data['language']['public_lang']['extra_lang'][] = array( 
				'NAME'		 			=> gm($db->f('language')),
				'ID'					=> $db->f('lang_id'),
				'IS_CHECKED'			=> $db->f('active') == 1 ? true : false,
			);
		}
		$db->query("SELECT * FROM pim_custom_lang WHERE flag='' ORDER BY sort_order");
		while ($db->move_next()) {
			$data['language']['public_lang']['custom_lang'][] = array( 
				'NAME_CUSTOM' 		=> gm($db->f('language')),
				'ID_CUSTOM'			=> $db->f('lang_id'),
				'IS_CUSTOM_CHECKED'	=> $db->f('active') == 1 ? true : false,
			);
			$i++;
		}
		return $data;
		return json_out($data, $showin,$exit); 
	}
	function get_Users($in){
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db2 = new sqldb();
		$db = new sqldb($database_users);
		$data = array();
		/*		$order_by_array = array('value','last_name','group_id','active','default_admin');
		$order_by = " ORDER BY name ";*/
		if(!isset($in['view'])){
			$in['view'] =1;
		}
		if(!isset($in['search'])){
			$in['search'] ='';
		}
		$data['users'] = array(
			'views' => array( array('name'=>gm('All Statuses'),'id'=>0,'active'=>'active'), array('name'=>gm('Active'),'id'=>1), array('name'=>gm('Inactive'),'id'=>2) ), 
            'activeView' => $in['view'],
            'search' =>array( 'search'=> $in['search'], 'do'=>'settings-settings', 'view'=>$in['view'], 'xget'=>'Users' ),
            'list' => array(),
            'max_rows'=>0,
            'nav'=>array()        
			);

		/*$l_r = ROW_PER_PAGE;*/
		$l_r = 100;
		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		$filter_q = ' AND users.active<>1000 ';
		$filter_s = ' ';
		$arguments = '';
		/*		if(!$in['archived']){
			$filter_q.= "  active = '1' ";
		}else{
			$filter_q.= "  active = '0' ";
			$arguments.="&archived=".$in['archived'];
			$arguments_o.="&archived=".$in['archived'];
		}*/
		if($in['search']){
			$filter_q.=" AND (users.first_name like '%".$in['search']."%' OR users.last_name like '%".$in['search']."%'  )";
			$arguments.="&search=".$in['search'];
		}
		if(!isset($in['view'])){
			$in['view'] = 0;
		}
		if($in['view'] == 0){
			$filter_s .= "AND user_meta.name='active' ";
			# do nothing
		}
		if($in['view'] == 1){
			$filter_s .= " AND user_meta.name='active' ";
			$filter_q .= " AND user_meta.value IS NULL ";
			# do nothing
		}
		if($in['view'] == 2){
			$filter_s .= " AND user_meta.name='active' ";
			$filter_q .= " AND user_meta.value = '1' ";
			# do nothing
		}
		if(!empty($in['order_by'])){
			if(in_array($in['order_by'], $order_by_array)){
				$order = " ASC ";
				if($in['desc'] == 'true'){
					$order = " DESC ";
				}
				$order_by =" ORDER BY ".$in['order_by']." ".$order;
				$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
			}
		}else{
				$order_by =" ORDER BY last_name ";
		}	

		$filter_c = " AND 1=1 ";
		$groupsQuery = $db2->query("SELECT * FROM groups")->getAll();
		$groupsArr = array();
		foreach ($groupsQuery as $key => $value) {
			$groupsArr[$value['group_id']] = $value;
		}

		$db->query("SELECT users.*, user_meta.value, user_meta.name FROM users
					LEFT JOIN user_meta ON users.user_id=user_meta.user_id ".$filter_s." 
					WHERE database_name='".DATABASE_NAME ."' ".$filter_q." GROUP BY users.user_id  ".$order_by." ");

		$max_rows=$db->records_count();
		//$db->move_to($offset*$l_r);
		$start = $offset*$l_r;
		//$start = 0;
		$all_rows=array();
		
		$k=0;


		while ($db->move_next()) {
			$all_rows[$k] = array(
				'name' 					=> htmlspecialchars_decode(stripslashes($db->f('last_name'))).' - '.htmlspecialchars_decode(stripslashes($db->f('first_name'))),
				'group' 				=> $groupsArr[$db->f('group_id')]['name'],
				'group_id' 				=> $db->f('group_id'),
				'user_id' 				=> $db->f('user_id'),
				'default_admin'			=> $db->f('default_admin'),
				'active'				=> $db->f('active'),
				'main_user_id'			=> $db->f('main_user_id'),
				'active2'				=> $db->f('value'),
				'credentials'			=> $db->f('credentials'),
				'user_list'				=> array(),
			);
			array_push($data['users']['nav'], (object)['user_id'=> $db->f('user_id') ]);
			$k++;
		}

			/*		if($in['order_by']){
				if ($in['desc']==1){
					$var = array_sort_by_column($all_rows,$in['order_by'],SORT_DESC);
				}
				else{
					$var = array_sort_by_column($all_rows,$in['order_by']);
				}
			}*/

		$end = ($start+$l_r>$max_rows)? $max_rows : $start+$l_r;

		for ($k=$start; $k<$end; $k++){

			$user_all = get_custom_right($all_rows[$k]['credentials'],$groupsArr[$all_rows[$k]['group_id']]);

			switch ($user_all[2]){
				case 'time':
					$type = 'Timesheet and expenses user';
					break;
				case 'power':
					$type = "Regular user";
					break;
				case 'admin':
					$type = "Administrator";
					break;
			}
			if($user_all[2]=='power'){
				$user_all[2]='';
			}elseif($user_all[2]=='time'){
				$user_all[2]='clock-o';
			}elseif($user_all[2]=='admin'){
				$user_all[2]='cogs';
			}

			$color = $all_rows[$k]['active2'];

			if($color==1){
				$colors='muted';
			}else{
				$colors='primary';
			}
			if($in['view']==1){
				$colors='primary';
			}
			$opts = '';
			if($all_rows[$k]['active2']==1){
				$opts = gm('Inactive');
			}else{
				$opts = gm('Active');
			}

			$data['users']['list'][] = array(
				'NAME'						=> $all_rows[$k]['name'],
				'ACTION' 			       	=> $all_rows[$k]['user_id'],
				'GROUP'						=> $all_rows[$k]['group'],
				'CUSTOM_RIGHTS' 			=> $user_all[1],
				'DEFAULT'					=> $all_rows[$k]['default_admin'] == 1 ? true : false,
				'DISABLE'					=> $_SESSION['access_level'] == 1 ? '' : 'disabled="disabled"',
				'HIDE_EDIT'					=> $all_rows[$k]['active'] == 1 ? true : false,
				'HIDE_DELETE'				=> $all_rows[$k]['active'] == 1 ? false : true,
				'HIDE_DEACTIVATE'			=> $all_rows[$k]['active'] == 1 ? (!$all_rows[$k]['main_user_id'] ? false : true) : false,
				'HIDE_ACTIVATE'				=> $all_rows[$k]['active'] == 1 ? false : true,
				'TYPE'						=> $user_all[2],
				'style'						=> $colors,
				'ALT'						=> $type,
				'ACTIVE'					=> $all_rows[$k]['active2'] == 1 ? 1 : 0,
				'ROL'						=> $opts,
				'ARGS'						=> $arguments.$arguments_o,
				'ACTIVE2'					=> $all_rows[$k]['active2'],
				'ACTIVE_CLS'				=> $all_rows[$k]['active2'] == 1 ? 'i_' : '',
				'LINK_CLASS1'				=> $all_rows[$k]['active2'] == 1 ? true : false,
				'LINK_CLASS'				=> $all_rows[$k]['active2'] == 1 ? 'check' : 'user-times',
				'TEXT_ACTIVE'				=> $all_rows[$k]['active2'] == 1 ? gm('Activate') : gm('Deactivate'),
				'ACT_ACTIVE'				=> $all_rows[$k]['active2'] == 1 ? 'activate2' : 'deactivate2',
				'is_main'					=> $all_rows[$k]['main_user_id'],
				'is_invitation'				=> true,
				'invited'					=> false,
			);

		}
		$y=0;
		$all_rows1=array();
		$email_users = $db->query("SELECT * FROM user_email WHERE user_id IN (SELECT user_id FROM users WHERE `database_name`='".DATABASE_NAME."') ");

		while ($email_users->move_next()) {
			$first=strrev($email_users->f('crypt'));
			$second=base64_decode($first);
			//$third = explode(' ', $second);
			$email_matches=preg_match_all('/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i', $second, $matches);
			$email_pos=strpos($second,$matches[0][count($matches[0])-1]);
			$third=explode(' ',substr($second,$email_pos));
			array_unshift($third, 'begin');

			$time_now=time();

			if($time_now>$email_users->f('time_email')){
				$opts1 = gm('Expired');
				$opts2 = 1;
				$active3=3;
			}else{
				$opts1 = gm('Invited');
				$opts2 = 2;
				$active3=4;
			}

			$all_rows1 = array(
				'NAME' 					=> $third[1],
				'ROL' 					=> $opts1,
				'GROUP'					=> get_group_name($third[3]),
				'RESSEND' 				=> $opts2==1 || $opts2==2 ? true : false,
				'CRYPT'					=> $email_users->f('crypt'),
				'HIDE_EDIT'				=> false,
				'ACTIVE'				=> $active3,
				'style'					=> 'primary',
				'HIDE_DELETE'			=> false,
				'HIDE_DEACTIVATE'		=> false,
				'HIDE_ACTIVATE'			=> false,
				'is_main'				=> false,
				'is_invitation'			=> false,
				'invited'				=> true
			);
			$y++;
			array_push($data['users']['list'], $all_rows1);
		}

		$data['users']['lr'] = $l_r;
		return $data;
		return json_out($data, $showin,$exit); 
	}
	function get_NewUser($in){
		$db = new sqldb();

		$data = array('user'=>array());

			if($in['user_id']=='tmp'){

				global $database_config;

				$database_users = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $database_config['user_db'],
				);
				$db_users = new sqldb($database_users);

				$next_function = 'settings-user-user-add';
				if(!isset($in['user_type'])){
					$in['user_type']=1;
				}
				if(!isset($in['country_id'])){
					$account_delivery_country_id = $db->field("SELECT value  FROM settings WHERE  constant_name='ACCOUNT_DELIVERY_COUNTRY_ID'");
					$in['country_id']=$account_delivery_country_id;
				}
				if(!isset($in['lang_id'])){
					$main_user_lang_id = $db_users->field("SELECT lang_id FROM users WHERE database_name = '".DATABASE_NAME."' AND main_user_id = '0' ");
					$in['lang_id']=$main_user_lang_id;
				}
				/*if($in['user_id']==tmp){
					$user_me=true;
					$user_him=false;

				}else{
					$user_him=true;
					$user_me=true;
				}*/
				$page_title=gm('New User');
				$data['user'] = array(
					'is_edit'				=> false,
				  'item_exists'				=> true,
				  'user_id' 					=> $in['user_id'],
				  'name'						=> $in['name'],
				  'first_name'					=> $in['first_name'],
				  'last_name'	  				=> $in['last_name'],
				  'email'						=> $in['email'],
				  // 'user_him'					=> $user_him,
				  // 'user_me'						=> $user_me,
				  'country_dd'					=> build_country_list($in['country_id']),
				  'country_id'					=> $in['country_id'],
				  'language_dd'					=> build_language_dd_user($in['lang_id']),
				  'lang_id'						=> $in['lang_id'],
				  'group_id'					=> $in['group_id'],
				  //'group'						=> build_group_list($in['group_id']),
				  'group'						=> get_groups($in),
				  'identity_dd'					=> build_identity_dd($in['identity_id']),
				  'identity_id'					=> $in['identity_id'],
				  'username'	  				=> $in['username'],
				  'password'					=> $in['password'],
				  'password1'					=> $in['password1'],
				  'hide_add'					=> 'hide',
				  'send_mail'					=> $in['send_mail'],
				  'h_rate'						=> '0,00',
				  'h_cost'						=> '0,00',
				  'd_rate'						=> '0,00',
				  'input_value'					=> $in['input_value'],
				  'd_cost'						=> '0,00',
				  'is_ajax'						=> $in['ajax']==1? 'hide':'',
				  'hide_email'					=> '',
				  'tab'							=> 'disabled',
				  'toggle'						=> '',
				  'page_title'					=> $page_title,
				  'hide_add_user'				=> 'hide',
				  'user_type'					=> $in['user_type'],
				  // 'hidegt'						=> $in['user_type'] == 2 ? '' : 'hide',
				  // 'hidega'						=> $in['user_type'] == 3 ? '' : 'hide',
				  // 'hideg'						=> $in['user_type'] == 1 ? '' : 'hide',
				  'do'							=> 'settings-settings-settings-add',
				  'xget'						=> 'NewUser',
				  'devices'						=> false,
				  'add_details'					=> true,
				);
				$data['user']['group_add'] = array(
							'NAME'			=> $in['name'],
							'ADMINISTRATOR'	=> $in['administrator'],
							'GROUP_ID'		=> $in['group_id'],
							'page_title' 	=> gm('Add Group'),
							'show_me'		=> false,
							'show_him'		=> true,
							'HIDE_A'		=> 'hide',
							'DISABLE'		=> '',
							'is_CRM' 			=> true,
							'is_PROJECTS' 		=> true,
							'is_TIMETRACKER' 	=> true,
							'is_MAINTENANCE'	=> true,
							'is_TIMESHEET' 		=> true,
							'is_BILLING' 		=> true,
							'is_QUOTES' 		=> true,
							'is_CONTRACTS' 		=> true,
							'is_ORDERS' 		=> true,
							'is_PO_ORDERS' 		=> true,
							'is_ARTICLES' 		=> true,
							'is_MANAGE' 		=> true,
							'is_WEBSHOP' 		=> true,
							'is_STOCK'			=> true,
							'is_INSTALLATION'	=> true,
							'is_CASHREGISTER'	=> true,
							'is_REPORTS'		=> true,
						);

			}else{

				$next_function = 'settings-user-user-update';
				global $database_config;

				$database_users = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $database_config['user_db'],
				);
				$db = new sqldb($database_users);
				$dbu  = new sqldb();
				//$db->query("SELECT * FROM users WHERE user_id='".$in['user_id']."' AND database_name='".DATABASE_NAME."' ");
				$db->query("SELECT users.*, user_info.default_identity_id FROM users LEFT JOIN user_info on users.user_id = user_info.user_id WHERE users.user_id= :user_id AND users.database_name= :d ",['user_id'=>$in['user_id'],'d'=>DATABASE_NAME]);
				if(!$db->f('user_id')){
					//json_out(array('redirect'=>'users'));
					msg::error('User does not exist','error');
					$data['user']['item_exists']=false;
    				return json_out($data, $showin,$exit); 
				}
				// if($in['user_id']==tmp){
				// 	$user_me=true;
				// 	$user_him=false;
				// }else{
				// 	$user_him=true;
				// 	$user_me=false;
				// }
					$page_title = gm('Edit User');
				
				$data['user'] = array(
					'is_edit'										=> true,
					'item_exists'									=> true,
				  	'user_id'										=> $in['user_id'],
				  	'first_name'	    							=> $in['first_name']? stripslashes($in['first_name']): htmlspecialchars_decode(stripslashes($db->f('first_name'))),
					'last_name'	    								=> $in['last_name']? stripslashes($in['last_name']) : htmlspecialchars_decode(stripslashes($db->f('last_name'))),
				  	'email'											=> $db->gf('email'),
				  	// 'user_him'										=> $user_him,
				  	'page_title'									=> $page_title.' '.htmlspecialchars_decode(stripslashes($db->f('first_name'))).' '.htmlspecialchars_decode(stripslashes($db->f('last_name'))),
				  	// 'user_me'										=> $user_me,
				  	'country_dd'	    							=> build_country_list($db->f('country_id')),
				  	'country_id'									=> $db->f('country_id'),
				  	'language_dd'	    							=> build_language_dd_user($db->f('lang_id')),
				  	'lang_id'										=> $db->f('lang_id'),
				  	'username'	    								=> $db->gf('username'),
				  	'group_id'										=> $db->f('group_id'),
				  	//'group'											=> build_group_list($db->f('group_id')),
				  	'group'											=> get_groups($in),
				  	'identity_dd'									=> build_identity_dd($db->f('default_identity_id'), true),
				  	'identity_id'									=> $db->f('default_identity_id'),
				  	'send_mail'										=> $db->f('send_mail'),
				  	'search'										=> $in['search']? '&search='.$in['search'] : '',
				  	'first'											=> $in['first']? '&first='.$in['first'] : '',
				  	'h_rate'										=> display_number($db->f('h_rate')),
				  	'h_cost'										=> display_number($db->f('h_cost')),
				  	'd_rate'										=> display_number($db->f('daily_rate')),
				  	'd_cost'										=> display_number($db->f('daily_cost')),
				  	'user_type'										=> $db->f('user_type') == 3 ? 1 : $db->f('user_type'),
				  	// 'hidegt'										=> $db->f('user_type') == 2 ? '' : 'hide',
				  	// 'hidega'										=> $db->f('user_type') == 3 ? '' : 'hide',
				  	// 'hideg'											=> $db->f('user_type') == 1 ? '' : 'hide',
				  	'is_ajax'										=> $in['ajax']==1? 'hide':'',
				  	'hide_mail'										=> 'hide',
				  	// 'hidegt'										=> $db->gf('user_type') == 2 ? '' : 'hide',
				  	// 'hidega'										=> $db->gf('user_type') == 3 ? '' : 'hide',
				  	// 'hideg'											=> $db->gf('user_type') == 1 ? '' : 'hide',
				  	'credentials'									=> array(),
				  	'admin_cred'									=> array(
												  						'ADMIN_1_V'=>false,
														               'ADMIN_12_V'=>false,
														               'ADMIN_6_V'=>false,
														               'ADMIN_14_V'=>false,
														               'ADMIN_5_V'=>false,
														               'ADMIN_13_V'=>false,
														               'ADMIN_3_V'=>false,
														               'ADMIN_11_V'=>false,
														               'ADMIN_4_V'=>false,
														               'ADMIN_17_V'=>false,
														               'ADMIN_19_V'=>false,
														               'ADMIN_16_V'=>false,
               																),
				  	'tab'											=> '',
				  	'toggle'										=> 'tab',
				  	'xget'											=> 'NewUser',
				  	'do'											=> 'settings-settings-settings-update_user',
				  	'hidelicense'									=> $db->f('active') == 1000 ? false :true,
				  	'devices'						=> defined('ALLOCLOUD_ACTIVE') && ALLOCLOUD_ACTIVE==1 ? true : false,
				  	'add_details'					=> true,
				);
				$data['user']['group_add'] = array(
							'NAME'			=> $in['name'],
							'ADMINISTRATOR'	=> $in['administrator'],
							'GROUP_ID'		=> $in['group_id'],
							'page_title' 	=> gm('Add Group'),
							'show_me'		=> false,
							'show_him'		=> true,
							'HIDE_A'		=> 'hide',
							'DISABLE'		=> '',
							'is_CRM' 			=> true,
							'is_PROJECTS' 		=> true,
							'is_TIMETRACKER' 	=> true,
							'is_MAINTENANCE'	=> true,
							'is_TIMESHEET' 		=> true,
							'is_BILLING' 		=> true,
							'is_QUOTES' 		=> true,
							'is_CONTRACTS' 		=> true,
							'is_ORDERS' 		=> true,
							'is_PO_ORDERS' 		=> true,
							'is_ARTICLES' 		=> true,
							'is_MANAGE' 		=> true,
							'is_WEBSHOP' 		=> true,
							'is_STOCK'			=> true,
							'is_INSTALLATION'	=> true,
							'is_CASHREGISTER'	=> true,
							'is_REPORTS'		=> true,
						);

				
				$did_it = explode('-', $in['do']);
				#we need an array with all the tabs controllers
				$tab_controller = array(
					1=>'settings-credentials',
					);
				#we assign the links on jquery tabs

				#here we load the controler that was submited directly from the main controler and not via ajax
				$controler_pag = '';
				if($did_it[2] && $in['tab'] != 0){

						$controler_pag = ark::run($tab_controller[$in['tab']]);
				}
					if(!$in['tab']){
						$in['tab'] = '0';
					}

					//$credential = $db->query("SELECT credentials, user_role, group_id, is_accountant, database_name,main_user_id FROM users WHERE user_id='".$in['user_id']."' ");
					$credential = $db->query("SELECT credentials, user_role, group_id, is_accountant, database_name,main_user_id FROM users WHERE user_id= :user_id ",['user_id'=>$in['user_id']]);

					$credential->next();
					$show_user_acc = $db->field("SELECT is_accountant FROM users WHERE user_id='".$credential->f('main_user_id')."'");

					$credentials = explode(';',$credential->f('credentials'));
					//$is_p_admin = $db->field("SELECT value FROM user_meta WHERE user_id='".$in['user_id']."' AND name='project_admin' ");
					$is_p_admin = $db->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'project_admin']);
					//$is_m_admin = $db->field("SELECT value FROM user_meta WHERE user_id='".$in['user_id']."' AND name='intervention_admin' ");
					$is_m_admin = $db->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'intervention_admin']);
					//$is_timesheet_admin=$db->field("SELECT value FROM user_meta WHERE user_id='".$in['user_id']."' AND name='timesheet_admin' ");
					$is_timesheet_admin=$db->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'timesheet_admin']);
					//$is_timetracker_admin=$db->field("SELECT value FROM user_meta WHERE user_id='".$in['user_id']."' AND name='timetracker_admin' ");
					$is_timetracker_admin=$db->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$in['user_id'],'name'=>'timetracker_admin']);

					if($credential->f('group_id') == 1){
						$is_p_admin = 1;
						$is_m_admin = 1;
						$is_timetracker_admin=1;
					}

					$k=0;
					$is_module_admin = $db->query("SELECT * FROM user_meta WHERE name REGEXP 'admin_[0-9]+' AND user_id='".$in['user_id']."' ");
					$is_module_admin = $is_module_admin->getAll();
					foreach ($is_module_admin as $key => $value) {
						if($value['value']){
							/*$data['user']['admin_cred'][$k]  = array(
								strtoupper($value['name']) 		=> '',
								strtoupper($value['name']).'_V'	=> true,
							);*/
							$data['user']['admin_cred'][strtoupper($value['name'])]='';
							$data['user']['admin_cred'][strtoupper($value['name']).'_V']=true;
							$k++;
						}
					}
					$g_cred = $dbu->field("SELECT credentials FROM groups WHERE group_id='".$credential->f('group_id')."'");
					$g_cred = explode(';',$g_cred);

					//Check for Client Subscription accessible modules
					$subscription_modules = get_subscription_active_modules(); 	
					//End Check for Client Subscription accessible modules

					$data['user']['credentials']    =    array(
						'CRM' 				=> in_array('1',$credentials) ? true : false,
						'Credentials'		=> $credentials,
						// 'ORDERS' 			=> in_array('2',$credentials) ? 'CHECKED' : '',
						'PROJECTS' 			=> in_array('3',$credentials) ? true : false,
						'TIMETRACKER' 		=> in_array('19',$credentials) ? true : false,
						'BILLING' 			=> in_array('4',$credentials) ? true : false,
						'QUOTES' 			=> in_array('5',$credentials) ? true : false,
						'CONTRACTS' 		=> in_array('11',$credentials) ? true : false,
						'MAINTENANCE' 		=> in_array('13',$credentials) ? true : false,
						'INSTALLATIONS' 		=> in_array('17',$credentials) ? true : false,
						'ARTICLES' 			=> in_array('12',$credentials) ? true : ($_SESSION['acc_type']=='services' && in_array('3',$credentials) ? true : false),
						'ORDERS' 			=> in_array('6',$credentials) ? true : false,
						'PO_ORDERS' 		=> in_array('14',$credentials) ? true : false,
						'STOCK' 			=> in_array('16',$credentials) ? true : false,
						'MANAGE' 			=> in_array('7',$credentials) ? true : false,
						'PIM' 				=> in_array('9',$credentials) ? true : false,
						'TIMESHEET' 		=> true,
						'PROJECTS_A'		=> $is_p_admin == 1 ? true : false,
						'TIMETRACKER_A'		=> $is_timetracker_admin == 1 ? true : false,
						'MAINTENANCE_A'		=> $is_m_admin == 1 ? true : false,
						'TIMESHEET_A'		=> $is_timesheet_admin == 1 ? true : false,

						'CRM_V' 			=> in_array('1',$credentials) ? '1' : '',
						// 'ORDERS_V' 			=> in_array('2',$credentials) ? '2' : '',
						'PROJECTS_V' 		=> in_array('3',$credentials) ? '3' : '',
						'TIMETRACKER_V' 		=> in_array('19',$credentials) ? '19' : '',
						'MAINTENANCE_V' 	=> in_array('13',$credentials) ? '13' : '',
						'PROJECTS_AV' 		=> $is_p_admin == 1 ? '1' : '0',
						'TIMETRACKER_AV' 		=> $is_timetracker_admin == 1 ? '1' : '0',
						'MAINTENANCES_AV' 	=> $is_m_admin == 1 ? '1' : '0',
						'BILLING_V' 		=> in_array('4',$credentials) ? '4' : '',
						'QUOTES_V' 			=> in_array('5',$credentials) ? '5' : '',
						'CONTRACTS_V' 		=> in_array('11',$credentials) ? '11' : '',
						'ARTICLES_V' 		=> in_array('12',$credentials) ? '12' : '',
						'ORDERS_V' 			=> in_array('6',$credentials) ? '6' : '',
						'PO_ORDERS_V' 		=> in_array('14',$credentials) ? '14' : '',
						'STOCK_V' 			=> in_array('16',$credentials) ? '16' : '',
						'MANAGE_V'	 		=> in_array('7',$credentials) ? '7' : '',
						'PIM_V'	 			=> in_array('9',$credentials) ? '9' : '',
						'TIMESHEET_V' 		=> '8',

						'CRM1' 				=> in_array('1',$g_cred) ? true: false,
						// 'ORDERS1' 			=> in_array('2',$g_cred) ? true: false,
						'PROJECTS1' 		=> in_array('3',$g_cred) ? true: false,
						'TIMETRACKER1' 		=> in_array('19',$g_cred) ? true: false,
						'MAINTENANCE1' 		=> in_array('13',$g_cred) ? true: false,
						'INSTALLATIONS1' 		=> in_array('17',$g_cred) ? true: false,
						'BILLING1' 			=> in_array('4',$g_cred) ? true: false,
						'QUOTES1' 			=> in_array('5',$g_cred) ? true: false,
						'CONTRACTS1' 		=> in_array('11',$g_cred) ? true: false,
						'ARTICLES1'			=> in_array('12',$g_cred) ? true: ($_SESSION['acc_type']=='services' && in_array('3',$g_cred) ? true: false),
						'ORDERS1'			=> in_array('6',$g_cred) ? true: false,
						'PO_ORDERS1'		=> in_array('14',$g_cred) ? true: false,
						'STOCK1'			=> in_array('16',$g_cred) ? true: false,
						'MANAGE1' 			=> in_array('7',$g_cred) ? true: false,
						'PIM1' 				=> in_array('9',$g_cred) ? true: false,
						'TIMESHEET1'		=> in_array('8',$g_cred) ? true: false,
						'PROJECTS_A1'		=> $credential->f('group_id') == 1 ? true: false,
						'TIMETRACKER_A1'		=> $credential->f('group_id') == 1 ? true: false,
						'MAINTENANCE_A1'		=> $credential->f('group_id') == 1 ? true: false,
						'TIMESHEET_A1'		=> $credential->f('group_id') == 1 ? true: false,
						'HIDE_A'			=> in_array('3',$credentials) ? '' : 'hide',
						'HIDE_M'			=> in_array('13',$credentials) ? '' : 'hide',
						'HIDE_CHECK'		=> in_array('13',$credentials) ? 'hide' : '',
						'HIDE_CHECK_A'		=> in_array('3',$credentials) ? 'hide' : '',
						'group_id'			=> $credential->f('group_id'),
						'GROUP'				=> get_group_name($credential->f('group_id')),
						'USER_ACCOUNTANT'	=> $credential->f('is_accountant') ? true : false,
						'show_user_acc'		=> $show_user_acc ==1 ? true : false,
						'REPORTS1'			=> in_array('18',$g_cred) ? true: false,
						'REPORTS' 			=> in_array('18',$credentials) ? true : false,
						'is_active_CRM' 			=> $subscription_modules[1] ? true : false, // CRM
						'is_active_PROJECTS' 		=> $subscription_modules[3] ? true : false, // Projects
						'is_active_TIMETRACKER' 	=> $subscription_modules[19] ? true: false, // Timetracker
						'is_active_MAINTENANCE'		=> $subscription_modules[13] ? true : false,// Interventions
						'is_active_BILLING' 		=> $subscription_modules[4] ? true : false, // Invoices
						'is_active_QUOTES' 			=> $subscription_modules[5] ? true : false, // Quotes
						'is_active_CONTRACTS' 		=> $subscription_modules[11] ? true : false,// Contracts
						'is_active_ORDERS' 			=> $subscription_modules[6] ? true : false, // Orders
						'is_active_PO_ORDERS' 		=> $subscription_modules[14] ? true : false,// Purchase Orders
						'is_active_ARTICLES' 		=> $subscription_modules[12] ? true : false,// Catalog
						'is_active_STOCK'			=> $subscription_modules[16] ? true : false,// Stock
						'is_active_INSTALLATION'	=> $subscription_modules[17] ? true : false,// Installations
						'is_active_CASHREGISTER'	=> $subscription_modules[15] ? true : false,// Cash Register
						'is_active_REPORTS'			=> $subscription_modules[18] ? true : false,// Reports
					);

				
			}
			$data['user']['group1_name'] = gm('Administrator');
			$data['user']['group2_name'] = gm('Timesheets &amp; Expenses only');


			#function
			$is_func = true;
			$dbs = new sqldb();
			$dbup = new sqldb();
			$i=0;
			$default_tasks = $dbs->query("SELECT * FROM default_data WHERE default_main_id='0' AND type='function' AND active='1' ");
			if($i==0){
				$is_func = false;
			}

			$show_for_one_user = true;
			if(defined('PAYMILL_CLIENT_ID') && (defined('HOW_MANY') && HOW_MANY < 2) ) {
			    $show_for_one_user = false;
			}
			return $data;
		return json_out($data, $showin,$exit); 
	}

	function get_subscription_active_modules(){

		global $database_config;

		$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
		$db = new sqldb($database_users);
		$dbu  = new sqldb();

		//$user_type = $db->field("SELECT active FROM users WHERE user_id='".$_SESSION['u_id']."' "); 
		$user_type = $db->field("SELECT active FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		$is_new_subscription = $dbu->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION' ");

		$modules_result =array();
		if(is_null($is_new_subscription)){
			
			$menu = array();
			$menu['free'] = array('1'=>gm('CRM'),'12'=>gm('Catalogue'),'16'=>gm('Stock'),'3'=>gm('Projects'),'19'=>gm('Timetracker'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports') );
			$menu['services'] = array('1'=>gm('CRM'),'12'=>gm('Catalogue'),'5'=>gm('Quotes'),'16'=>gm('Stock'),'3'=>gm('Projects'),'19'=>gm('Timetracker'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports'));
			$menu['goods'] = array('1'=>gm('CRM'),'5'=>gm('Quotes'),'12'=>gm('Catalogue'),'6'=>gm('Orders'),'14'=>gm('Purchase Orders'),'16'=>gm('Stock'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports'));
			$menu['both'] = array('1'=>gm('CRM'),'5'=>gm('Quotes'),'12'=>gm('Catalogue'),'6'=>gm('Orders'),'14'=>gm('Purchase Orders'),'16'=>gm('Stock'),'3'=>gm('Projects'),'19'=>gm('Timetracker'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'4'=>gm('Invoices'),'11'=>gm('Contracts'),'15'=>gm('CashRegister'),'18'=>gm('Reports'));

			foreach ($menu[$_SESSION['acc_type']] as $key => $value) {
				$modules_result[$key] = true;
			}
			
		}else{
			// ,'9'=>'Webshop' Removed
			$menu = array('1'=>gm('CRM'),'12'=>gm('Catalogue'),'5'=>gm('Quotes'),'6'=>gm('Orders'),'14'=>gm('Purchase Orders'),'16'=>gm('Stock'),'3'=>gm('Projects'),'19'=>gm('Timetracker'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'11'=>gm('Contracts'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports'));

			$modulesResult = $menu;

			foreach (perm::$allow_apps as $key => $value) {
				if($menu[$value]){
					$modules_result[$value] = true;
				}
			}
		}

		return $modules_result;
	}


	function get_group($in){
		$db = new sqldb();
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db2 = new sqldb($database_users);

		$data = array('groups'=>array());
		$data['groups']=array('group_list'=>array(),'group_add'=>array());
		global $cfg;
		$no_timesheets = false;
		if (ACCOUNT_TYPE=='goods')
		{
			$no_timesheets = true;
		}

		$is_new_subscription = $db->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION' ");

		$data['modules'] =array();
		if(is_null($is_new_subscription)){
			
			$menu = array();
			

			$menu['free'] = array('1'=>gm('CRM'),'12'=>gm('Catalogue'),'16'=>gm('Stock'),'3'=>gm('Projects'),'19'=>gm('Timetracker'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports') );
			$menu['services'] = array('1'=>gm('CRM'),'12'=>gm('Catalogue'),'5'=>gm('Quotes'),'16'=>gm('Stock'),'3'=>gm('Projects'),'19'=>gm('Timetracker'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports') );
			$menu['goods'] = array('1'=>gm('CRM'),'5'=>gm('Quotes'),'12'=>gm('Catalogue'),'6'=>gm('Orders'),'14'=>gm('Purchase Orders'),'16'=>gm('Stock'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports') );
			$menu['both'] = array('1'=>gm('CRM'),'5'=>gm('Quotes'),'12'=>gm('Catalogue'),'6'=>gm('Orders'),'14'=>gm('Purchase Orders'),'16'=>gm('Stock'),'3'=>gm('Projects'),'19'=>gm('Timetracker'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'4'=>gm('Invoices'),'11'=>gm('Contracts'),'15'=>gm('CashRegister'),'18'=>gm('Reports') );

			foreach ($menu[$_SESSION['acc_type']] as $key => $value) {
				//$val =$db2->field("SELECT value FROM user_meta WHERE name='MODULE_".$key."' AND user_id='".$_SESSION['u_id']."' ");
				$val =$db2->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_'.$key,'user_id'=>$_SESSION['u_id']]);
				if($val===NULL){
					$val = $db->field("SELECT value FROM settings WHERE constant_name='MODULE_".$key."' ");
				}
				if($key!='4' && $key!='10' && $user_type=='1000'){
					continue;
				}

				$line=array(
					'module'		=> $value,
					'key'			=> $key,
					'checked'		=> $val == 1 ? true : false,
					'disable'		=> in_array($key, perm::$allow_apps) ? false : true,
				);
				if($key=='12'){
					$line['disable'] = in_array(12, perm::$allow_apps) || in_array(3, perm::$allow_apps) ? false : true;
				}
				$data['modules'][$key] = $line;

			}

		}else{
		
			$menu = array('1'=>gm('CRM'),'12'=>gm('Catalogue'),'5'=>gm('Quotes'),'6'=>gm('Orders'),'14'=>gm('Purchase Orders'),'16'=>gm('Stock'),'3'=>gm('Projects'),'19'=>gm('Timetracker'),'13'=>gm('Intervention'),'17'=>gm('Installations'),'11'=>gm('Contracts'),'4'=>gm('Invoices'),'15'=>gm('CashRegister'),'18'=>gm('Reports') );
			foreach (perm::$allow_apps as $key => $value) {
				if($menu[$value]){
					//$val =$db2->field("SELECT value FROM user_meta WHERE name='MODULE_".$value."' AND user_id='".$_SESSION['u_id']."' ");
					$val =$db2->field("SELECT value FROM user_meta WHERE name= :name AND user_id= :user_id ",['name'=>'MODULE_'.$value,'user_id'=>$_SESSION['u_id']]);
					if($val===NULL){
						$val = $db->field("SELECT value FROM settings WHERE constant_name='MODULE_".$value."' ");
					}
					if($value!='4' && $value!='10' && $user_type=='1000'){
						continue;
					}

					$line=array(
						'module'		=> $menu[$value],
						'key'			=> $value,
						'checked'		=> $val == 1 ? true : false,
						'disable'		=> array_key_exists($value, $menu) ? false : true,
					);
					if($value=='12'){
						$line['disable'] = in_array(12, perm::$allow_apps) || in_array(3, perm::$allow_apps) ? false : true;

					}
					$data['modules'][$value] = $line;
				}
			}
		}

		$groups = $db->query("SELECT * FROM groups WHERE group_id!=1 AND group_id!=2");
		$i=0;
		while ($groups->next()) {
			if ($groups->f('group_id')!='2' || $no_timesheets==false)
			{
					global $database_config;
					$db_config = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => $database_config['user_db'],
					);

					$db = new sqldb($db_config);
					$dbu = new sqldb();

					$dbu->query("SELECT * FROM groups WHERE group_id='".$groups->f('group_id')."'  ");
					$dbu->move_next();
					$credentials = explode(';',$dbu->f('credentials'));

					$user = $db->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$dbu->f('admin_id')."'");
					if($groups->f('group_id')){
						$show_me=true;
						$show_him=false;
					}else{
						$show_me=false;
						$show_him=true;
					}

				$data['groups']['group_list'][] = array(
					"NAME"				=> $groups->f('name'),
					"EDIT_LINK"			=> 'index.php?do=settings-group&group_id='.$groups->f('group_id'),
					"group_id"			=> $groups->f('group_id'),
					'HIDE_DELETE'		=> $groups->f('is_default') == 1 ? false : true,
					'NAME'				=> $dbu->f('name'),
					'show_me'			=> $show_me,
					'show_him'			=> $show_him,
					'ADMINISTRATOR'		=> $user,
					'GROUP_ID'			=> $groups->f('group_id'),
					'page_title'		=>	gm('Edit Group'),
					'CRM' 				=> in_array('1',$credentials) ? true : false,
					'PROJECTS' 			=> in_array('3',$credentials) ? true : false,
					'TIMETRACKER' 		=> in_array('19',$credentials) ? true : false,
					'MAINTENANCE' 		=> in_array('13',$credentials) ? true : false,
					'TIMESHEET' 		=> True,
					'BILLING' 			=> in_array('4',$credentials) ? true : false,
					'QUOTES' 			=> in_array('5',$credentials) ? true : false,
					'CONTRACTS' 		=> in_array('11',$credentials) ? true : false,
					'ARTICLES' 			=> in_array('12',$credentials) ? true : false,
					'ORDERS' 			=> in_array('6',$credentials) ? true : false,
					'PO_ORDERS' 		=> in_array('14',$credentials) ? true : false,
					'MANAGE' 			=> in_array('7',$credentials) ? true : false,
					'WEBSHOP'			=> in_array('9',$credentials) ? true : false,
					'STOCK'				=> in_array('16',$credentials) ? true : false,
					'INSTALLATION'		=> in_array('17',$credentials) ? true : false,
					'CASHREGISTER'		=> in_array('15',$credentials) ? true : false,
					'REPORTS'			=> in_array('18',$credentials) ? true : false,
				  	'DISABLE'			=> $dbu->f('is_default') == 1 ? 'disabled' : '',
					'HIDE'				=> $dbu->f('is_default') == 1 ? 'hide' : '',
					'HIDE_A'			=> in_array('3',$credentials) ? '' : 'hide',
					'is_CRM' 			=> $data['modules'][1]['checked'],
					'is_PROJECTS' 		=> $data['modules'][3]['checked'],
					'is_TIMETRACKER' 		=> $data['modules'][19]['checked'],
					'is_MAINTENANCE'	=> $data['modules'][13]['checked'],
					'is_TIMESHEET' 		=> True,
					'is_BILLING' 		=> $data['modules'][4]['checked'],
					'is_QUOTES' 		=> $data['modules'][5]['checked'],
					'is_CONTRACTS' 		=> $data['modules'][11]['checked'],
					'is_ORDERS' 		=> $data['modules'][6]['checked'],
					'is_PO_ORDERS' 		=> $data['modules'][14]['checked'],
					'is_ARTICLES' 		=> $data['modules'][12]['checked'],
					'is_MANAGE' 		=> $data['modules'][7]['checked'],
					'is_WEBSHOP' 		=> $data['modules'][9]['checked'],
					'is_STOCK'			=> $data['modules'][16]['checked'],
					'is_INSTALLATION'	=> $data['modules'][17]['checked'],
					'is_CASHREGISTER'	=> $data['modules'][15]['checked'],
					'is_REPORTS'		=> $data['modules'][18]['checked'],
				);
				$i++;
			}
		}
		/*$data['credential']= $credentials;*/
		if(!$in['group_id']){
					if($in['group_id']){
						$show_him=false;
						$show_me=true;
					}else{
						$show_him=true;
						$show_me=false;
					}
			$data['groups']['group_add'] = array(
							'NAME'			=> $in['name'],
							'ADMINISTRATOR'	=> $in['administrator'],
							'GROUP_ID'		=> $in['group_id'],
							'page_title' 	=> gm('Add Group'),
							'show_me'		=> $show_me,
							'show_him'		=> $show_him,
							'HIDE_A'		=> 'hide',
							'DISABLE'		=> '',
							'is_CRM' 			=> $data['modules'][1]['checked'],
							'is_PROJECTS' 		=> $data['modules'][3]['checked'],
							'is_TIMETRACKER' 		=> $data['modules'][19]['checked'],
							'is_MAINTENANCE'	=> $data['modules'][13]['checked'],
							'is_TIMESHEET' 		=> True,
							'is_BILLING' 		=> $data['modules'][4]['checked'],
							'is_QUOTES' 		=> $data['modules'][5]['checked'],
							'is_CONTRACTS' 		=> $data['modules'][11]['checked'],
							'is_ORDERS' 		=> $data['modules'][6]['checked'],
							'is_PO_ORDERS' 		=> $data['modules'][14]['checked'],
							'is_ARTICLES' 		=> $data['modules'][12]['checked'],
							'is_MANAGE' 		=> $data['modules'][7]['checked'],
							'is_WEBSHOP' 		=> $data['modules'][9]['checked'],
							'is_STOCK'			=> $data['modules'][16]['checked'],
							'is_INSTALLATION'	=> $data['modules'][17]['checked'],
							'is_CASHREGISTER'	=> $data['modules'][15]['checked'],
							'is_REPORTS'		=> $data['modules'][18]['checked'],
			);
		}
			
		return $data;
		return json_out($data, $showin,$exit); 
	}
	function get_apps($in){
		$db = new sqldb();
		$data = array('integrated'=>array(),'integrate'=>array());
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$dbu = new sqldb($db_config);
		$dbu->field("SELECT credentials FROM users WHERE user_id= :user_id  ",['user_id'=>$_SESSION['u_id']]);
		$dbu->move_next();
		$credentials = explode(';',$dbu->f('credentials'));
		$projects = in_array('3',$credentials) ? true : false;
		$cash = in_array('15',$credentials) ? true : false;
		$i=0;
		$activ = 0;

		$acc_app_id=0;
		$accountant_apps=array('yuki'=>'1','codabox'=>'2','clearfacts'=>'3','billtobox'=>'4');
		if($_SESSION['main_u_id']=='0'){
  			$is_accountant=$dbu->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	    }else{
	        $is_accountant=$dbu->field("SELECT accountant_id FROM users WHERE `database_name`='".DATABASE_NAME."' AND main_user_id='0' ");
	    }
	    if($is_accountant){
	        $acc_app_id=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_APP_ID'");        
	    }

		if($in['field']==1){
			//var_dump($in['field']);
			$accountancy_apps = $db->query("SELECT * FROM apps WHERE main_app_id='0' AND type='main' AND template_file<>'' ");
		}elseif($in['field']!=null && $in['field2']==null && $in['field3']==null){
			$accountancy_apps = $db->query("SELECT * FROM apps WHERE main_app_id='0' AND type='main' AND template_file='".$in['field']."'  ");
		}elseif($in['field'] != null && $in['field2'] != null && $in['field3']==null){
			$accountancy_apps = $db->query("SELECT * FROM apps WHERE main_app_id='0' AND type='main' AND template_file='".$in['field']."' || template_file='".$in['field2']."' ");
		}elseif($in['field'] != null && $in['field2'] != null && $in['field3'] != null){
			$accountancy_apps = $db->query("SELECT * FROM apps WHERE main_app_id='0' AND type='main' AND template_file='".$in['field']."' || template_file='".$in['field2']."' || template_file='".$in['field3']."' ");
		}else{
			$accountancy_apps = $db->query("SELECT * FROM apps WHERE main_app_id='0' AND type='main' AND template_file<>'' ");
		}

		$ADV_CRM = $db->field("SELECT value FROM settings WHERE constant_name='ADV_CRM'");
		$is_easy_invoice_diy=aktiUser::get('is_easy_invoice_diy');
		$is_easy_invoice_billtobox=aktiUser::get('is_easy_invoice_billtobox');

		while ($accountancy_apps->next()) {
			if($accountancy_apps->f('app_type') == 'accountancy' && $acc_app_id && $acc_app_id!=$accountant_apps[$accountancy_apps->f('template_file')] && $accountancy_apps->f('template_file')!='codabox' && $accountancy_apps->f('template_file')!='netsuite'){
				if($accountancy_apps->f('template_file')!='btb'){
                    continue;
                }
			}
			//if($accountancy_apps->f('name') == 'Exact online' || $accountancy_apps->f('name') == 'Syneton'){
			if($accountancy_apps->f('name') == 'Syneton'){
				continue;
			}
			if($accountancy_apps->f('name') == 'Google Calendar'){
				continue;
			}
			if($accountancy_apps->f('name') == 'FTP Import'){
				continue;
			}
			if($accountancy_apps->f('name') == 'Mail chimp'  && $ADV_CRM==0){
				continue;
			}
			/*if($accountancy_apps->f('name') == 'Nylas' && $ADV_CRM==0){
				continue;
			}*/
			if($accountancy_apps->f('name') == 'Campaign monitor' && $ADV_CRM==0){
				continue;
			}
			/*if($accountancy_apps->f('name') == 'Zendesk' && $ADV_CRM==0){
				continue;
			}*/
			if($accountancy_apps->f('name') == 'Smartsheet' && $projects==false){
				continue;
			}
			/*if($accountancy_apps->f('name') == 'Tactill' && $cash==false){
				continue;
			}*/
			//here to hide unavailable apps
			if($accountancy_apps->f('name') == 'Nylas' || $accountancy_apps->f('name') == 'Tactill' || $accountancy_apps->f('name') == 'Icepay' || $accountancy_apps->f('name') == 'Edebex' || $accountancy_apps->f('name') == 'Lengow' || $accountancy_apps->f('name') == 'Allocloud' || $accountancy_apps->f('name') == 'Instapaid' || $accountancy_apps->f('name') == 'Mail chimp' || $accountancy_apps->f('name') == 'Campaign monitor' || $accountancy_apps->f('name') == 'Smartsheet' ){
				continue;
			}
			//end unavailable apps

			$view_beta = false;
			if($accountancy_apps->f('name') == 'Stripe' || $accountancy_apps->f('name') == 'CodaBox'){
				$view_beta = true;
			}
			if($is_easy_invoice_diy && $accountancy_apps->f('template_file')!='jefacture'){
				continue;
			}
			if(!$is_easy_invoice_diy && $accountancy_apps->f('template_file')=='jefacture'){
				continue;
			}
			if($is_easy_invoice_billtobox && $accountancy_apps->f('template_file')!='billtobox'){
				continue;
			}
			$data['integrated'][]	=    array(
				'CLASS2'		=> 'images/'.$accountancy_apps->f('template_file').'.png',
				'path'			=> $accountancy_apps->f('template_file'),
				'id'			=> $accountancy_apps->f('template_file'),
				'NOT'			=> $accountancy_apps->f('active') ? ($accountancy_apps->f('active') == 2 ? 'bad-activated' : '' ) : 'not-activated',
				'NOT2'			=> $accountancy_apps->f('active') ? ($accountancy_apps->f('active') == 2 ? 'not-activated2' : '' ) : 'not-activated2',
				'activ'			=> $accountancy_apps->f('active') ? 'app_active' : "",
				'app_type'		=> $accountancy_apps->f('app_type'),
				'view_beta'		=> $view_beta,
				'name'			=> $accountancy_apps->f('name'),
				'cfacts_class'	=> $accountancy_apps->f('name') == 'ClearFacts' ? 'cfacts_class':'',
				'exact_online_class'	=> $accountancy_apps->f('name') == 'Exact online' ? 'exact_online_class':'',
				'dropbox_class'	=> $accountancy_apps->f('name') == 'Dropbox' ? 'dropbox_class':'',

			);
			$i++;
			if($accountancy_apps->f('active')){
				$activ++;
			}
			switch($accountancy_apps->f('app_type')){
        	case 'accountancy':
        		if($accountancy_apps->f('active')){$accountancy_active ++;}
        		
        		break;
        	case 'payment':
        		if($accountancy_apps->f('active')){$payment_active ++;}
        		break;
        	default:
        		if($accountancy_apps->f('active')){$other_active ++;}
        		break;
        }  
		}
		$apps_integrated = $db->query("SELECT template_file FROM apps WHERE main_app_id='0' AND type='main' AND template_file<>'' ");
		$data['integrate'] = array(
			'camp'				=> 'camp',
			'chimp'				=> 'chimp',
			'dropbox_setup'		=> 'dropbox_setup',
			'codabox'			=> 'codabox',
			'calendar'			=> 'calendar',
			'ftp_import'		=> 'ftp_import',
			'btb'				=> 'btb',
			'yuki'				=> 'yuki',
			'pay_stripe'		=> 'pay_stripe',
			'pay_icepay'		=> 'pay_icepay',
			'lengow'			=> 'lengow',
			'edebex'			=> 'edebex',
			'instapaid'			=> 'instapaid',
			'pay_mollie'		=> 'pay_mollie',
			'smartsheet'		=> 'smartsheet',
			'tactill'			=> 'tactill',
			'nylas'				=> 'nylas',
			'zendesk'			=> 'zendesk',
			'clearfacts'		=> 'clearfacts',
			'exact_online'		=> 'exact_online',
			'xapp'				=> 1,
			'all'				=> $i,
			'actives'			=> $activ,
			'accountancy_active'=> $accountancy_active,
			'payment_active'	=> $payment_active,
			'other_active'		=> $other_active,
			'show_app_payments' => DATABASE_NAME == '04ed5b47_e424_5dd4_ad024bcf2e1d' ? true : false, // Only show for Robbie
		);
		$data['is_clearfacts']=$db->field("SELECT active FROM apps WHERE name='Clearfacts' and type='main' AND main_app_id='0' ") ? true : false;
		$data['is_exact_online']=$db->field("SELECT active FROM apps WHERE name='Exact online' and type='main' AND main_app_id='0' ") ? true : false;
		$data['is_dropbox']=$db->field("SELECT active FROM apps WHERE name='Dropbox' and type='main' AND main_app_id='0' ") ? true : false;

		return $data;
		return json_out($data, $showin,$exit); 
	}
	function get_camp($in){
		$db = new sqldb();
		$data = array('campain'=>array());

		/*		$view->assign(array(
			'CAMPAIGN_USER' => $in['campaign_user'],
			'CAMPAIGN_PASS'	=> $in['campaign_password'],
			'CAMPAIGN_SITE'	=> $in['campaign_site'],
			'CAPMAGN_APY'	=> ACCOUNT_CAMPAIGN_APY,
			'CLOSED'		=> ACCOUNT_CAMPAIGN_CLOSE == 1 ? "CHECKED disabled=\"disabled\" " : '', 
		));*/

		$active = $db->field("SELECT active FROM apps WHERE name='Campaign monitor' AND main_app_id='0' AND type='main' ");
		/*		0if(defined('ACCOUNT_CAMPAIGN_APY') && ACCOUNT_CAMPAIGN_APY != '' && ACCOUNT_CAMPAIGN_APY!=null && $active==1){
			require_once("../../campaignmonitor/campaign_all.php");

			$wrap1 = new CS_REST_General(ACCOUNT_CAMPAIGN_APY);
			$result = $wrap1->get_clients();
			$client = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CAMPAIGN_CLIENT'");
			foreach ($result->response as $key => $value) {
				$view->assign(array(
					'CLIENT_API'		=> $value->ClientID,
					'CLIENT_NAME'		=> $value->Name,
					'CLIENT_CHECKED'	=> ($value->ClientID == $client) ? 'CHECKED' : '',
				),'expense');
				$view->loop('expense');
			}
		}*/
		if($active==1){
			$data['campain']  =   array(
				'CLASS2'					=> 'images/camp.png',
				'campaign_apy'				=> ACCOUNT_CAMPAIGN_APY,
				'hide_active'				=> true,
				'clients'					=> array(),
			);
			require_once 'campaignmonitor/campaign_all.php';
			$wrap1 = new CS_REST_General(ACCOUNT_CAMPAIGN_APY);
			$result = $wrap1->get_clients();
			$client = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CAMPAIGN_CLIENT'");
			foreach ($result->response as $key => $value) {
				$data['campain']['clients'][]		=		array(
					'CLIENT_API'		=> $value->ClientID,
					'CLIENT_NAME'		=> $value->Name,
					'CLIENT_CHECKED'	=> ($value->ClientID == $client) ? 'CHECKED' : '',
				);
			}
		}else{
			$data['campain']  =   array(
				'CLASS2'					=> 'images/camp.png',
				'campaign_user'				=> '',
				'campaign_password'			=> '',
				'campaign_site'				=> '',
				'hide_deactive'				=> true,
			);
		}


		return $data['campain'];
		//return json_out($data['campain'], $showin,$exit); 
	}
	function get_chimp($in){
		$db = new sqldb();
		$data = array('chimp_mail'=>array());

		/*$tips = $page_tip->getTip('settings-settings');*/
		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Mail chimp' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			if($app->f('active') == 1){
				$app_active = true;
				$in['api_key'] = $app->f('api');
				$app_attr = $db->query("SELECT * FROM apps WHERE main_app_id='".$app->f('app_id')."' ");
				while ($app_attr->next()) {
					$in[$app_attr->f('type')] = $app_attr->f('api');
				}
			}
		}
		$app_key = $db->field("SELECT api FROM apps WHERE app_id='".$app->f('app_id')."' ");
		/*		$view->assign(array(
			'USER' 			=> $in['user'],
			'PASS' 			=> $in['password'],
			'API_KEY' 		=> $in['api_key'],
			'page_tip'		=> $tips,
			'is_active'		=> $app_active == true ? true : false,
		));*/

		if($app->f('active') == 1){
			$data['chimp_mail']  =   array(
				'CLASS2'					=> 'images/chimp.png',
				'api_key'					=> $app_key,
				'title'						=> gm('Deactivate'),
				'app'						=> $app->f('active') == 1 ? true : false,
			);
		}else{
			$data['chimp_mail']  =   array(
				'CLASS2'					=> 'images/chimp.png',
				'api_key'					=> '',
				'title'						=> gm('Connect application'),
				'app_me'						=> $app->f('active') == 1 ? false : true,
			);			
		}


		return $data['chimp_mail'];
		//return json_out($data['chimp_mail'], $showin,$exit); 
	}

	function get_dropbox_setup($in){
		global $config;
		$db = new sqldb();
		$data = array('dropbox'=>array());

		if(ALLOW_QUOTE_DROP_BOX == 1){
			$data['dropbox']		=   array(
				'CLASS2'			=> 'images/dropbox_setup.png',
			    'authCode'        	=> '',
			    'key'				=>  $config['dropbox_client_id'],
			    'redirect_url'		=>  $config['dropbox_redirect_url'],
			    'title'				=> 'Deactivate',
			    'hide_deactivate'	=> ALLOW_QUOTE_DROP_BOX == 1 ? true : false,
			);
		}else{
			$data['dropbox']		=   array(
				'CLASS2'			=> 'images/dropbox_setup.png',
			    'authCode'        	=> '',
			    'key' 				=>  $config['dropbox_client_id'],
			    'redirect_url'		=>  $config['dropbox_redirect_url'],
			    'title'				=> 'Connect application',
			    'if_dropbox'		=> ALLOW_QUOTE_DROP_BOX == 1 ? false : true,
			);			
		}


		return $data['dropbox'];
		//return json_out($data['dropbox'], $showin,$exit); 
	}
	function get_codabox($in){
		$db = new sqldb();
		$data = array('coda'=>array());

		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			if($app->f('active') == 1){
				$app_active = true;
				$in['api_key'] = $app->f('api');
				$app_attr = $db->query("SELECT * FROM apps WHERE main_app_id='".$app->f('app_id')."' ");
				while ($app_attr->next()) {
					$in[$app_attr->f('type')] = $app_attr->f('api');
				}
			}
			$step=$db->field("SELECT api FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='step' ");
			if(!$step){
				$db->query("INSERT INTO apps SET main_app_id='".$app->f('app_id')."', type='step', api='1' ");
				$step=1;
			}
			if($step==1){
				$do_next='settings--user-coda_tok';
				$api_key=$db->field("SELECT api FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' ");
				$vat_nr=$db->field("SELECT api FROM apps WHERE type='vat' AND main_app_id='".$app->f('app_id')."' ");
		/*				$view->assign(array(
					'api_key'	=> $api_key ? $api_key : ($in['api_key'] ? $in['api_key'] : ''),
					'vat_nr'	=> $vat_nr ? $vat_nr : ($in['vat_nr'] ? $in['vat_nr'] : ''),
				));*/
				$data['coda']			=    array(
					'CLASS2'			=> 'images/codabox.png',
					'api_key'			=> $api_key ? $api_key : ($in['api_key'] ? $in['api_key'] : ''),
					'vat_nr'			=> $vat_nr ? $vat_nr : ($in['vat_nr'] ? $in['vat_nr'] : ''),
					'hide_vat'				=> true,
				);
			}else if($step==2){
				$do_next='settings--user-coda_cred';
				$token=$db->field("SELECT api FROM apps WHERE type='token' AND main_app_id='".$app->f('app_id')."' ");
				$sales_check=$db->field("SELECT api FROM apps WHERE type='sales' AND main_app_id='".$app->f('app_id')."' ");
				$payments_check=$db->field("SELECT api FROM apps WHERE type='payments' AND main_app_id='".$app->f('app_id')."' ");
			/*				$view->assign(array(
					'token_id'	=> $token ? $token : ($in['token_id'] ? $in['token_id'] : ''),
				));*/
				$data['coda']			=    array(
					'CLASS2'					=> 'images/codabox.png',
					'token_id'					=> $token ? $token : ($in['token_id'] ? $in['token_id'] : ''),
					'hide_token'				=> true,
					'sales_check'		=> is_null($sales_check) || $sales_check==1 ? true : false,
					'payments_check'	=> is_null($payments_check) || $payments_check==1 ? true : false,
				);
			}else if($step==3){
				if($app->f('active')){
					$do_next='settings--user-coda_disconnect';
				}else{
					$do_next='settings--user-coda_connect';
				}
				$sales_check=$db->field("SELECT api FROM apps WHERE type='sales' AND main_app_id='".$app->f('app_id')."' ");
				$payments_check=$db->field("SELECT api FROM apps WHERE type='payments' AND main_app_id='".$app->f('app_id')."' ");
				$exportAutoInvoice=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_INVOICE_CODABOX'");
				$exportAutoPInvoice=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_PINVOICE_CODABOX'");

				$accountant_export_invoice_force=false;
				global $database_config;
				$database_users = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $database_config['user_db'],
				);
				$db_users = new sqldb($database_users);
				if($_SESSION['main_u_id']=='0'){
          			$is_accountant=$db_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
			    }else{
			        $is_accountant=$db_users->field("SELECT accountant_id FROM users WHERE `database_name`='".DATABASE_NAME."' AND main_user_id='0' ");
			    }
			    if($is_accountant){
			        $accountant_settings=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
			        $acc_export_id=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_EXPORT_ID' ");
			        if($accountant_settings=='1' && $acc_export_id){
			            $exportAutoInvoice=$acc_export_id;
			            $accountant_export_invoice_force=true;
			        }
			    }

				if($app->f('active')==1){
					$data['coda']			=    array(
						'title'				=> gm('Deactivate'),
						'CLASS2'			=> 'images/codabox.png',
						'api_key'			=> $db->field("SELECT api FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' "),
						'token'				=> $db->field("SELECT api FROM apps WHERE type='token' AND main_app_id='".$app->f('app_id')."' "),
						'vat_nr'			=> $db->field("SELECT api FROM apps WHERE type='vat' AND main_app_id='".$app->f('app_id')."' "),
						'active'			=> $app->f('active') ? true : false,
						'hide_button'		=> true,
						'sales_check'		=> is_null($sales_check) || $sales_check==1 ? true : false,
						'payments_check'	=> is_null($payments_check) || $payments_check==1 ? true : false,
						'exportAutoOpts'	=> get_export_auto_invoice_options(),
						'exportAutoInvoice'	=> $exportAutoInvoice ? $exportAutoInvoice : '0',
						'exportAutoPInvoice'=> $exportAutoPInvoice ? $exportAutoPInvoice : "0",
						'accountant_export_invoice_force'	=> $accountant_export_invoice_force
					);					
				}else{
					$data['coda']			=    array(
						'title'				=> gm('Connect application'),
						'CLASS2'			=> 'images/codabox.png',
						'api_key'			=> $db->field("SELECT api FROM apps WHERE name='CodaBox' AND type='main' AND main_app_id='0' "),
						'token'				=> $db->field("SELECT api FROM apps WHERE type='token' AND main_app_id='".$app->f('app_id')."' "),
						'vat_nr'			=> $db->field("SELECT api FROM apps WHERE type='vat' AND main_app_id='".$app->f('app_id')."' "),
						'is_active'			=> $app->f('active') ? false : true,
						'hide_button'		=> true,
						'sales_check'		=> is_null($sales_check) || $sales_check==1 ? true : false,
						'payments_check'	=> is_null($payments_check) || $payments_check==1 ? true : false,
						'exportAutoOpts'	=> get_export_auto_invoice_options(),
						'exportAutoInvoice'	=> $exportAutoInvoice ? $exportAutoInvoice : '0',
						'exportAutoPInvoice'=> $exportAutoPInvoice ? $exportAutoPInvoice : '0',
						'accountant_export_invoice_force'	=> $accountant_export_invoice_force 
					);
				}
			}
		}
		return $data['coda'];
		//return json_out($data['coda'], $showin,$exit); 
	}
	function get_calendar($in){
		$db = new sqldb();
		$data = array('calendars'=>array());

		$active = $db->field("SELECT active FROM apps WHERE name='Google Calendar' AND main_app_id='0' AND type='main' ");

		if($active==1){
			$data['calendars']    =   array(
				'CLASS2'			=> 'images/calendar.png',
				'title'				=> 'Deactivate',
				'active'			=> $active,
				'value'				=> '0',
				'hide_active'		=> true,
			);
		}else{
			$data['calendars']    =   array(
				'CLASS2'			=> 'images/calendar.png',
				'title'				=> 'Connect application',
				'active'			=> $active,
				'value'				=> '1',
				'hide_deactive'		=> true,
			);	
		}

		return $data['calendars'];
		//return json_out($data['calendars'], $showin,$exit); 
	}
	function get_ftp_import($in){
		$db = new sqldb();
		$data = array('import'=>array());

			$app_id=$db->field("SELECT app_id FROM apps WHERE name='FTP Import' AND app_type='ftp_import'");
			$is_active=$db->field("SELECT active FROM apps WHERE name='FTP Import' AND app_type='ftp_import'");
			if ($is_active){
				/*$view->assign('is_checked','checked=checked');*/
			}else{
				/*$view->assign('is_checked','');*/
			}
			$user=$db->field("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='user' ");
			$password=$db->field("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='password' ");
			$host=$db->field("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='host' ");

			if($is_active){
				$data['import'] = array(
					'CLASS2'			=> 'images/ftp_import.png',
					'title'				=> 'Deactivate',
					'activate'			=> 	'0',
					'ftp_user'          =>  $user,
					'ftp_password'		=>  $password,
					'ftp_host'			=>  $host,
					'app_id'			=>  'ftp_import',
					'hide_active'		=> true,
				);
			}else{
				$data['import'] = array(
					'CLASS2'			=> 'images/ftp_import.png',
					'title'				=> 'Connect application',
					'activate'			=> 	'1',
					'app_id'			=>  'ftp_import',
					'ftp_user'          =>  $user,
					'ftp_password'		=>  $password,
					'ftp_host'			=>  $host,
					'hide_deactive'		=> true,
				);
			}
			return $data['import'];
			//return json_out($data['import'], $showin,$exit); 
	}
	function get_btb($in){
		$db = new sqldb();
		$data = array('winbook'=>array());
			$app_id=$db->field("SELECT app_id FROM apps WHERE name='WinBooks' AND app_type='accountancy'");
			$is_active=$db->field("SELECT active FROM apps WHERE name='WinBooks' AND app_type='accountancy'");
			$push_user = $db->field("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='push_user' ");
			$ap_push_key = $db->field("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='ap_push_key' ");
			$ar_push_key = $db->field("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='ar_push_key' ");
			$pull_user = $db->field("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='pull_user' ");
			$ap_pull_key = $db->field("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='ap_pull_key' ");
			$ar_pull_key = $db->field("SELECT api FROM apps WHERE  main_app_id='".$app_id."' AND type='ar_pull_key' ");
			$dropbox_data=$db->query("SELECT app_id,active FROM apps WHERE name='Dropbox' AND type='main'");
			$eff_drop_data=$db->query("SELECT api,active FROM apps WHERE main_app_id='".$dropbox_data->f('app_id')."' AND type='eff_drop'");
			$exportAutoInvoice=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_INVOICE_EFFF'");
            $exportAutoPInvoice=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_PINVOICE_EFFF'");

            $accountant_export_invoice_force=false;
            $accountant_export_pinvoice_force=false;
            global $database_config;
            $database_users = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_config['user_db'],
            );
            $db_users = new sqldb($database_users);
            if($_SESSION['main_u_id']=='0'){
                $is_accountant=$db_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
            }else{
                $is_accountant=$db_users->field("SELECT accountant_id FROM users WHERE `database_name`='".DATABASE_NAME."' AND main_user_id='0' ");
            }
            if($is_accountant){
                $accountant_settings=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
                $acc_export_id=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_EXPORT_ID' ");
                if($accountant_settings=='1' && $acc_export_id){
                    $exportAutoInvoice=$acc_export_id;
                    $accountant_export_invoice_force=true;
                }
                $acc_export_id_pi=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_EXPORT_ID_PI' ");
                if($accountant_settings=='1' && $acc_export_id_pi){
                	$exportAutoPInvoice=$acc_export_id_pi;
                    $accountant_export_pinvoice_force=true;
                }
            }

			if($is_active){
				$data['winbook'] = array(
					'CLASS2'			=> 'images/btb.png',
					'title'				=> gm('Deactivate'),
					'activate'			=> 	'0',
					'host'				=>  $ap_push_key,
					'push_user'			=>  $push_user,
					'pull_user'			=>  $pull_user,
					'ap_push_key'		=>  $ap_push_key,
					'ar_push_key'		=>  $ar_push_key,
					'ap_pull_key'		=>  $ap_pull_key,
					'ar_pull_key'		=>  $ar_pull_key,
					'hide_active'		=> true,
					'drop_active'		=> $dropbox_data->f('active') ? true : false,
					//'eff_drop'			=> is_null($eff_drop_data->f('active')) ? '0' : $eff_drop_data->f('active'),
					'eff_drop'			=> '1',
					'eff_drop_link'		=> $eff_drop_data->f('api'),
					'exportAutoOpts'    => get_export_auto_invoice_options(),
                    'exportAutoInvoice' => $exportAutoInvoice ? $exportAutoInvoice : '0',
                    'exportAutoPInvoice'=> $exportAutoPInvoice ? $exportAutoPInvoice : '0',
                    'accountant_export_invoice_force'   => $accountant_export_invoice_force,
                    'accountant_export_pinvoice_force'	=> $accountant_export_pinvoice_force
				);
			}else{
				$data['winbook'] = array(
					'CLASS2'			=> 'images/btb.png',
					'title'				=> gm('Connect application'),
					'activate'			=> 	'1',
					'host'				=>  $ap_push_key,
					'push_user'			=>  $push_user,
					'pull_user'			=>  $pull_user,
					'ap_push_key'		=>  $ap_push_key,
					'ar_push_key'		=>  $ar_push_key,
					'ap_pull_key'		=>  $ap_pull_key,
					'ar_pull_key'		=>  $ar_pull_key,
					'hide_deactive'		=> true,
					'drop_active'		=> $dropbox_data->f('active') ? true : false,
					/*'eff_drop'			=> is_null($eff_drop_data->f('active')) ? '0' : '1',*/
					'eff_drop'			=> '1',
					'eff_drop_link'		=> $eff_drop_data->f('api'),
					'exportAutoOpts'    => get_export_auto_invoice_options(),
                    'exportAutoInvoice' => $exportAutoInvoice ? $exportAutoInvoice : '0',
                    'exportAutoPInvoice'=> $exportAutoPInvoice ? $exportAutoPInvoice : '0',
                    'accountant_export_invoice_force'   => $accountant_export_invoice_force,
                    'accountant_export_pinvoice_force'	=> $accountant_export_pinvoice_force
				);
			}
			$data['winbook']['available']		= ACCOUNT_DELIVERY_COUNTRY_ID == 26 ? true : false;
			return $data['winbook'];
			//return json_out($data['winbook'], $showin,$exit); 
	}
	function get_yuki($in){
		$db = new sqldb();
		$data = array('yuk'=>array());
			$app = $db->query("SELECT * FROM apps WHERE name='Yuki' AND app_type='accountancy'");
			$dropbox_data=$db->query("SELECT app_id,active FROM apps WHERE name='Dropbox' AND type='main'");
			$exportAutoInvoice=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_INVOICE_YUKI'");
            $exportAutoPInvoice=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_PINVOICE_YUKI'");
            $yuki_projects_enabled=$db->field("SELECT value FROM settings WHERE constant_name='YUKI_PROJECTS'");
			$is_active=$app->f("active");
			$apps = explode(';',$app->f("api"));
			$in['api_key'] = $apps[0];
			$in['admin_key'] = $apps[1];
			$in['yuki_drop'] = $apps[2];

			$accountant_export_invoice_force=false;
			$accountant_export_pinvoice_force=false;
            global $database_config;
            $database_users = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_config['user_db'],
            );
            $db_users = new sqldb($database_users);
            if($_SESSION['main_u_id']=='0'){
                $is_accountant=$db_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
            }else{
                $is_accountant=$db_users->field("SELECT accountant_id FROM users WHERE `database_name`='".DATABASE_NAME."' AND main_user_id='0' ");
            }
            if($is_accountant){
                $accountant_settings=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
                $acc_export_id=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_EXPORT_ID' ");
                if($accountant_settings=='1' && $acc_export_id){
                    $exportAutoInvoice=$acc_export_id;
                    $accountant_export_invoice_force=true;
                }
                $acc_export_id_pi=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_EXPORT_ID_PI' ");
                if($accountant_settings=='1' && $acc_export_id_pi){
                	$exportAutoPInvoice=$acc_export_id_pi;
                    $accountant_export_pinvoice_force=true;
                }
            }

			if(count($apps)>3){
				$in['yuki_drop_sale'] = $apps[3];
			}
			if ($is_active){
				$data['yuk'] = array(
					'CLASS2'			=> 'images/yuki.png',
					'title'				=> gm('Deactivate'),
					'is_activated'		=> 	'0',
					'admin_key'			=> $in['admin_key'],
					'api_key'			=> $in['api_key'],
					'yuki_drop'			=> $in['yuki_drop'],
					'yuki_drop_sale'	=> $in['yuki_drop_sale'],
					'hide_active'		=> true,
					'drop_active'		=> $dropbox_data->f('active') ? true : false,
					'eff_drop'			=> $app->f('active')=='2' ? '1' : '0',
					'exportAutoOpts'    => get_export_auto_invoice_options(),
                    'exportAutoInvoice' => $exportAutoInvoice ? $exportAutoInvoice : '0',
                    'exportAutoPInvoice'=> $exportAutoPInvoice ? $exportAutoPInvoice : '0',
                    'accountant_export_invoice_force'   => $accountant_export_invoice_force,
                    'accountant_export_pinvoice_force'	=> $accountant_export_pinvoice_force,
                    'yuki_projects_enabled' 			=> $yuki_projects_enabled==1 ? true : false,
				);
			}else{
				/*$folders = array();
				$d = new drop();
				if(strpos('my.akti', $config['site_url'])!==false && $_SESSION['u_id']<10000){
					$basic_folder="/";
				}else{
					$basic_folder='/APP/Akti';
				}*/
				/*$e = $d->getContent($basic_folder);
				foreach ($e->entries as $key => $value) {
					if($value->{'.tag'}=='folder'){
						array_push($folders,$value->path_display);
					}
				}*/
				$data['yuk'] = array(
					'CLASS2'			=> 'images/yuki.png',
					'title'				=> gm('Connect application'),
					'is_activated'		=> 	'1',
					'admin_key'			=> $in['admin_key'],
					'api_key'			=> $in['api_key'],
					'yuki_drop'			=> $in['yuki_drop'],
					'hide_deactive'		=> true,
					'drop_active'		=> $dropbox_data->f('active') ? true : false,
					'yuki_drop_sale'	=> '',
					'eff_drop'			=> '0',
					'exportAutoOpts'    => get_export_auto_invoice_options(),
                    'exportAutoInvoice' => $exportAutoInvoice ? $exportAutoInvoice : '0',
                    'exportAutoPInvoice'=> $exportAutoPInvoice ? $exportAutoPInvoice : '0',
                    'accountant_export_invoice_force'   => $accountant_export_invoice_force,
                    'accountant_export_pinvoice_force'	=> $accountant_export_pinvoice_force,
                    'yuki_projects_enabled' 			=> $yuki_projects_enabled==1 ? true : false,
					//'folderloop'		=> array(),
				);
				/*foreach ($folders as $value) {
					$data['yuk']['folderloop'][]  =   array(
						'folder'=>$value,
						'clicked' => false,
					);
				}*/
			}

			return $data['yuk'];
			//return json_out($data['yuk'], $showin,$exit); 
	}

	function get_dropboxPath($in)
	{
		$folders = array();
		$d = new drop();
		$result = array();
		$e = $d->getContent($in['path']);
		foreach ($e->entries as $key => $value) {
			if($value->{'.tag'}=='folder'){
				array_push($folders,$value->path_display);
			}
		}
		// console::log($folders);
		foreach ($folders as $value) {
			$result[] = array(
				'folder'=>$value,
				'clicked' => false,
			);
			
		}
		return $result;
		//return json_out($result, $showin,$exit); 
	}
	function get_pay_stripe($in){
		$db = new sqldb();
		$data = array('stripe'=>array());
		global $config;
		/*$tips = $page_tip->getTip('settings-pay_stripe');*/
		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			if($app->f('active') == 1){
				$app_active = true;
				$in['api_key'] = $app->f('api');
				$app_attr = $db->query("SELECT * FROM apps WHERE main_app_id='".$app->f('app_id')."' ");
				while ($app_attr->next()) {
					$in[$app_attr->f('type')] = $app_attr->f('api');
				}
			}	
			
		}
		if($app->f('active')){
				$data['stripe']		=    array(
					'CLASS2'			=> 'images/pay_stripe.png',
					'api_key'			=> $db->field("SELECT api FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' "),
					'publish_key'		=> $db->field("SELECT api FROM apps WHERE type='publish_key' AND main_app_id='".$app->f('app_id')."' "),
					'is_active'			=> $app->f('active') ? true : false,
					'hide_button'		=> $app->f('active') ? 'hide' : '',
					'readonly_disabled'	=> $app->f('active') ? 'readonly disabled' : '',
					'hide_active'		=> true,
					'title'				=> gm('Deactivate'),
				);
			}else{
				$data['stripe']		=    array(
					'CLASS2'			=> 'images/pay_stripe.png',
					'api_key'			=> $db->field("SELECT api FROM apps WHERE name='Stripe' AND type='main' AND main_app_id='0' "),
					'publish_key'		=> $db->field("SELECT api FROM apps WHERE type='publish_key' AND main_app_id='".$app->f('app_id')."' "),
					'is_active'			=> $app->f('active') ? true : false,
					'hide_button'		=> $app->f('active') ? 'hide' : '',
					'readonly_disabled'	=> $app->f('active') ? 'readonly disabled' : '',
					'hide_deactive'		=> true,
					'title'				=> gm('Connect application'),
				);
			}

			return $data['stripe'];
			//return json_out($data['stripe'], $showin,$exit); 
	}
	function get_pay_icepay($in){
		$db = new sqldb();
		$data = array('icepay'=>array());
		global $config;
		/*$tips = $page_tip->getTip('settings-pay_icepay');*/
		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Icepay' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			if($app->f('active') == 1){
				$app_active = true;
				$in['api_key'] = $app->f('api');
				$app_attr = $db->query("SELECT * FROM apps WHERE main_app_id='".$app->f('app_id')."' ");
				while ($app_attr->next()) {
					$in[$app_attr->f('type')] = $app_attr->f('api');
				}
			}	
		}

		if($app->f('active')){
				$data['icepay']		=    array(
					'CLASS2'			=> 'images/pay_icepay.png',
					'api_key'		=> $db->field("SELECT api FROM apps WHERE name='Icepay' AND type='main' AND main_app_id='0' "),
					'merchant_id'	=> $db->field("SELECT api FROM apps WHERE type='merchant_id' AND main_app_id='".$app->f('app_id')."' "),
					'is_active'		=> $app->f('active') ? true : false,
					'hide_button'	=> $app->f('active') ? 'hide' : '',
					'readonly_disabled'	=> $app->f('active') ? 'readonly disabled' : '',
					'hide_active'		=> true,
					'title'				=> 'Deactivate',
				);
			}else{
				$data['icepay']		=    array(
					'CLASS2'			=> 'images/pay_icepay.png',
					'api_key'		=> $db->field("SELECT api FROM apps WHERE name='Icepay' AND type='main' AND main_app_id='0' "),
					'merchant_id'	=> $db->field("SELECT api FROM apps WHERE type='merchant_id' AND main_app_id='".$app->f('app_id')."' "),
					'is_active'		=> $app->f('active') ? true : false,
					'hide_button'	=> $app->f('active') ? 'hide' : '',
					'readonly_disabled'	=> $app->f('active') ? 'readonly disabled' : '',
					'hide_deactive'		=> true,
					'title'				=> 'Connect application',
				);
			}

			return $data['icepay'];
			//return json_out($data['icepay'], $showin,$exit); 
	}
	function get_lengow($in){
		$db = new sqldb();
		$data = array('lengo'=>array());
		global $config;
		/*$tips = $page_tip->getTip('settings-lengow');*/
		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Lengow' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			if($app->f('active') == 1){
				$app_active = true;
				$in['api_key'] = $app->f('api');
				$app_attr = $db->query("SELECT * FROM apps WHERE main_app_id='".$app->f('app_id')."' ");
				while ($app_attr->next()) {
					$in[$app_attr->f('type')] = $app_attr->f('api');
				}
			}	
            $api = $db->query("SELECT * FROM apps WHERE name='Lengow' AND type='main' AND main_app_id='0' ");
	        $api=  explode( ';', $api->f('api'));
		}

		if($app->f('active')){
				$data['lengo']			=    array(
					'CLASS2'				=> 'images/lengow.png',
	                'access_token'          =>$api[0],
	                'secret'                =>$api[1],
					'api_key'				=> $db->field("SELECT api FROM apps WHERE name='Lengow' AND type='main' AND main_app_id='0' "),
					'merchant_id'           => $db->field("SELECT api FROM apps WHERE type='merchant_id' AND main_app_id='".$app->f('app_id')."' "),
					'is_active'				=> $app->f('active') ? true : false,
					'hide_button'           => $app->f('active') ? 'hide' : '',
					'readonly_disabled'		=> $app->f('active') ? 'readonly disabled' : '',
					'hide_active'			=> true,
					'title'					=> gm('Deactivate'),
				);
			}else{
				$data['lengo']			=    array(
					'CLASS2'				=> 'images/lengow.png',
	                'access_token'          =>$api[0],
	                'secret'                =>$api[1],
					'api_key'				=> $db->field("SELECT api FROM apps WHERE name='Lengow' AND type='main' AND main_app_id='0' "),
					'merchant_id'           => $db->field("SELECT api FROM apps WHERE type='merchant_id' AND main_app_id='".$app->f('app_id')."' "),
					'is_active'				=> $app->f('active') ? true : false,
					'hide_button'           => $app->f('active') ? 'hide' : '',
					'readonly_disabled'		=> $app->f('active') ? 'readonly disabled' : '',
					'hide_deactive'			=> true,
					'title'					=> gm('Connect application'),
				);
			}

			return $data['lengo'];
			//return json_out($data['lengo'], $showin,$exit); 
	}
	function get_access($in){
		$db = new sqldb();
		$data = array('user_access'=>array());
			$ONLY_IF_ACC_MANAG = $db->field("SELECT value FROM settings WHERE constant_name='ONLY_IF_ACC_MANAG' ");
			$ONLY_IF_ACC_MANAG2 = $db->field("SELECT value FROM settings WHERE constant_name='ONLY_IF_ACC_MANAG2' ");
			$ONLY_IF_ACC_MANAG3 = $db->field("SELECT value FROM settings WHERE constant_name='ONLY_IF_ACC_MANAG3' ");
			$NO_ACCESS_VIEW_ARTICLE = $db->field("SELECT value FROM settings WHERE constant_name='NO_ACCESS_VIEW_ARTICLE' ");
			$HIDE_PROFIT_MARGIN = $db->field("SELECT value FROM settings WHERE constant_name='HIDE_PROFIT_MARGIN' ");
			$only_if_int_user=$db->field("SELECT value FROM settings WHERE `constant_name`='ONLY_IF_INT_USER'");
			$NO_ACCESS_PRICE_ARTICLE_INT_REG_USER=$db->field("SELECT value FROM settings WHERE `constant_name`='NO_ACCESS_PRICE_ARTICLE_INT_REG_USER'");

		$data['user_access']   =    array(
			'only_if_acc_manag' 		=> $ONLY_IF_ACC_MANAG == 1 ? true : false,
			'only_if_acc_manag2' 		=> $ONLY_IF_ACC_MANAG2 == 1 ? true : false,
			'only_if_acc_manag3' 		=> $ONLY_IF_ACC_MANAG3 == 1 ? true : false,
			'no_access_view_article' 	=> $NO_ACCESS_VIEW_ARTICLE == 1 ? true : false,
			'hide_profit_margin' 		=> $HIDE_PROFIT_MARGIN == 1 ? true : false,
			'only_if_int_user'			=> $only_if_int_user ==1 ? true : false,
			'no_access_price_article_int_reg_user' 	=> $NO_ACCESS_PRICE_ARTICLE_INT_REG_USER == 1 ? true : false,
			'do'						=> 'settings-settings-settings-user_accees_if_acc_manag',
			'xget'						=> 'access',
		);
		return $data;
		return json_out($data, $showin,$exit); 
	}
	function get_identity($in){
		$db = new sqldb();
		$data = array('multiple'=>array());
		$multiple_identity = $db->query("SELECT * FROM multiple_identity");
		$i=0;
		while ($multiple_identity->next()) {
			
			$data['multiple'][]   =    array(
				"NAME"					=> $multiple_identity->f('identity_name'),
				"EDIT_LINK"				=> 'index.php?do=settings-multiple_identity&identity_id='.$multiple_identity->f('identity_id'),
				"MULTIPLE_ID"			=> $multiple_identity->f('identity_id'),
			);
			$i++;
		}
		return $data;
		return json_out($data, $showin,$exit); 
	}

	function get_NewIdentity($in){
		$db = new sqldb();
		$data = array('identitys'=>array());

		if($in['identity_id']=='tmp'){

			$page_title = gm('Add Identity');
			$next_function = 'settings-multiple_identity-user-add_identity';


		    $db->query("SELECT * from settings where  constant_name='ACCOUNT_DELIVERY_COUNTRY_ID'");
		    $db->move_next();
		    $account_delivery_country_id=$db->f('value');
		    if (!$account_delivery_country_id)
		    {
		    	$account_delivery_country_id = $in['country_id'];
		    }
		    /*$view->assign('ACCOUNT_DELIVERY_COUNTRY_DD',build_country_list($account_delivery_country_id));*/
		    $image ='images/no-logo.png';
		    if($in['file']){
		    	$logo = 'upload/'.DATABASE_NAME.'/identity/'.$in['file'];
		    }else{
		    	$logo =$in['image'];
		    }

		    
		   $data['identitys']		=     array(
		   		'identity_name' 					=> $in['identity_name'],
				'company_name' 						=> $in['company_name'],
				'company_email' 					=> $in['company_email'],
				'company_url' 						=> $in['company_url'],
				'company_description' 				=> $in['company_description'],
				'bank_name_identity' 				=> $in['bank_name_identity'],
				'bank_bic_code_identity' 			=> $in['bank_bic_code_identity'],
				'bank_iban_identity' 				=> $in['bank_iban_identity'],
				'company_reg_number' 				=> $in['company_reg_number'],
				'company_phone' 					=> $in['company_phone'],
				'company_fax' 						=> $in['company_fax'],
				'company_address'					=> ACCOUNT_DELIVERY_ADDRESS,
				'company_zip'						=> ACCOUNT_DELIVERY_ZIP,
				'city_name'							=> ACCOUNT_DELIVERY_CITY,
				'identity_id'						=> $in['identity_id'],
				'ACCOUNT_DELIVERY_COUNTRY_DD'		=> build_country_list($account_delivery_country_id),
				'country_id'						=> $account_delivery_country_id,
				'page_title'						=> $page_title,
				'xget'								=> 'NewIdentity',
				'image'								=> $logo ?  $logo : $image,
				'do'								=> 'settings-settings-settings-add_identity',
				'show_me'							=> true,
				'edit'								=> false,

			);

		}else{

			
			$next_function = 'settings-multiple_identity-user-update_identity';

			$multiple_id = $db->query("SELECT * FROM multiple_identity WHERE identity_id='".$in['identity_id']."' ");
			$multiple_id->move_next();
			$page_title = gm('Edit Identity').' '.$multiple_id->gf('identity_name');
			$image ='images/no-logo.png';
			$logo = $db->field("SELECT company_logo FROM multiple_identity WHERE identity_id='".$in['identity_id']."' ");
			$logo = str_replace('../', '', $logo);
			$data['identitys']		=     array(
				'identity_name' 				=> $multiple_id->gf('identity_name'),
				'company_name' 					=> $multiple_id->gf('company_name'),
				'company_email' 				=> $multiple_id->gf('company_email'),
				'company_url' 					=> $multiple_id->gf('company_url'),
				'company_description' 			=> $multiple_id->gf('company_description'),
				'bank_name_identity' 			=> $multiple_id->gf('bank_name_identity'),
				'bank_bic_code_identity' 		=> $multiple_id->gf('bank_bic_code_identity'),
				'bank_iban_identity' 			=> $multiple_id->gf('bank_iban_identity'),
				'company_reg_number' 			=> $multiple_id->gf('company_reg_number'),
				'company_phone' 				=> $multiple_id->gf('company_phone'),
				'company_fax' 					=> $multiple_id->gf('company_fax'),
				'company_siret'			 		=> $multiple_id->gf('company_siret'),
				'company_rpr' 					=> $multiple_id->gf('company_rpr'),
				'company_den_sociale' 			=> $multiple_id->gf('company_den_sociale'),
				'company_reg_commerce' 			=> $multiple_id->gf('company_reg_commerce'),
				'company_cap_social' 			=> $multiple_id->gf('company_cap_social'),
				'company_code_naf' 				=> $multiple_id->gf('company_code_naf'),
				'company_juridical_form' 		=> $multiple_id->gf('company_juridical_form'),
				'company_zip' 					=> $multiple_id->gf('company_zip'),
				'company_address' 				=> $multiple_id->gf('company_address'),
				'city_name' 					=> $multiple_id->gf('city_name'),
				'ACCOUNT_DELIVERY_COUNTRY_DD'	=> build_country_list($multiple_id->gf('country_id')),
				'country_id'					=> $multiple_id->gf('country_id'),
				'page_title'					=> $page_title,
				'identity_id'					=> $in['identity_id'],
				'image'							=> $logo ?  $logo : $image,
				'xget'							=> 'NewIdentity',
				'do'							=> 'settings-settings-settings-update_identity',
				'show_him'						=> true,
				'edit'							=> true,

			);


		/*		    $view->assign('ACCOUNT_DELIVERY_COUNTRY_DD',build_country_list($multiple_id->f('country_id')));


		    $view->assign(array(
				'folder'			=> DATABASE_NAME,
				'upload_path'		=> '../upload/'.DATABASE_NAME.'/identity/',
				'picture_name'		=> 'identity_'.$in['identity_id'].'.jpg',
				'ACCOUNT_LOGO'		=> $multiple_id->gf('company_logo') ? $multiple_id->gf('company_logo').'?'.time() : '../img/no-logo.png',
				'IDENTITY_ID'		=> $in['identity_id'],
				'edit'				=> true,
				'addmode'			=> false,

			));
			$view->assign(array(
			//fields for specific countries
		        'hide_rpr'                =>  $account_billing_country_id == '26' ? '' : 'hide',//belgium-26
		        'hide_siret'              =>  ($account_billing_country_id == '79' || $account_billing_country_id == '132') ? '' : 'hide',//france-79,luxembourg-132
		        'hide_cap_social'         =>  ($account_billing_country_id == '79' || $account_billing_country_id == '132') ? '' : 'hide',//france-79,luxembourg-132
		        'hide_juridical_form'     =>  ($account_billing_country_id == '79' || $account_billing_country_id == '132') ? '' : 'hide',//france-79,luxembourg-132
		        'hide_den_sociale'        =>  $account_billing_country_id == '79' ? '' : 'hide',//france-79
		        'hide_code_naf'           =>  $account_billing_country_id == '79' ? '' : 'hide',//france-79
		        'hide_reg_commerce'       =>  $account_billing_country_id == '132' ? '' : 'hide',//luxembourg-132
		        'hide_no_matricule'       =>  $account_billing_country_id == '132' ? '' : 'hide',//luxembourg-132
		        'show_for_one_user'       => $show_for_one_user,

			));*/
		}
		$show_for_one_user = true;
		if(defined('PAYMILL_CLIENT_ID') && (defined('HOW_MANY') && HOW_MANY == 0) ) {
		    $show_for_one_user = false;
		}

		return $data;
		//return json_out($data, $showin,$exit); 
	}
	function get_edebex($in){
		$db = new sqldb();
		$data = array('edebex'=>array());

		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Edebex' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			$data['edebex']['CLASS2']			= 'images/edebex.png';
			$data['edebex']['email']			= $db->field("SELECT api FROM apps WHERE main_app_id='".$app->f('app_id')."' and type='email' ");
			$data['edebex']['token']			= $app->f('api');					
			$data['edebex']['active']			= $app->f('active') ? true : false;
			$data['edebex']['available']			= ACCOUNT_DELIVERY_COUNTRY_ID == 26 ? true : false;
			if($app->f('active') == 1){
				$data['edebex']['title']		= gm('Deactivate');
			}else{
				$data['edebex']['title']		= gm('Connect application');
			}
		}
		return $data['edebex'];
		//return json_out($data['edebex'], $showin,$exit); 
	}

	function get_instapaid($in){
		$db = new sqldb();
		$data = array('instapaid'=>array());

		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Instapaid' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			$data['instapaid']['CLASS2']			= 'images/instapaid.png';				
			$data['instapaid']['active']			= $app->f('active') ? true : false;
			$data['instapaid']['available']		= ACCOUNT_DELIVERY_COUNTRY_ID == 26 ? true : false;
			if($app->f('active') == 1){
				$data['instapaid']['title']		= gm('Deactivate');
			}else{
				$data['instapaid']['title']		= gm('Connect application');
			}
		}
		return $data['instapaid'];
		//return json_out($data['instapaid'], $showin,$exit); 
	}

	function get_pay_mollie($in){
		$db = new sqldb();
		$data = array('mollie'=>array());

		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Mollie' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			$data['mollie']['CLASS2']			= 'images/pay_mollie.png';				
			$data['mollie']['active']			= $app->f('active') ? true : false;
			$data['mollie']['api_key']			= $app->f('api');
			if($app->f('active') == 1){
				$data['mollie']['title']		= gm('Deactivate');
			}else{
				$data['mollie']['title']		= gm('Connect application');
			}
		}
		return $data['mollie'];
		//return json_out($data['mollie'], $showin,$exit); 
	}

	function get_smartsheet($in){
		$db = new sqldb();
		$data = array('smart'=>array());

		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Smartsheet' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			$data['smart']['CLASS2']			= 'images/smartsheet.png';				
			$data['smart']['active']			= $app->f('active') ? true : false;
			$data['smart']['token_id']			= $app->f('api');
			if($app->f('active') == 1){
				$data['smart']['title']		= gm('Deactivate');
			}else{
				$data['smart']['title']		= gm('Connect application');
			}
		}
		return $data['smart'];
		//return json_out($data['smart'], $showin,$exit); 
	}

	function get_tactill($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('tact'=>array());

		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Tactill' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			$data['tact']['CLASS2']			= 'images/tactill.png';				
			$data['tact']['active']			= $app->f('active') ? true : false;
			$data['tact']['api_key']			= $app->f('api');
			if($app->f('active') == 1){
				$data['tact']['title']		= gm('Deactivate');
			}else{
				$data['tact']['title']		= gm('Connect application');
			}
		}
		return $data['tact']; 
	}

	function get_nylas($in,$showin=true,$exit=true){
		global $config;
		$db = new sqldb();
		$data = array('nylas'=>array());

		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Nylas' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			$data['nylas']['CLASS2']		= 'images/nylas.png';				
			$data['nylas']['active']		= $app->f('active') ? true : false;
			$data['nylas']['email']			= $app->f('api');
			$data['nylas']['app_id']					= $config['nylas_app_id'];
			$data['nylas']['app_secret']				= $config['nylas_app_secret'];
			$data['nylas']['redirect_uri']			= $config['nylas_authorize_url'];
			if($app->f('active') == 1){
				$data['nylas']['title']		= gm('Deactivate');
			}else{
				$data['nylas']['title']		= gm('Connect application');
			}
		}
		return $data['nylas']; 
	}

	function get_zendesk($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('zendesk'=>array());

		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Zendesk' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			$data['zendesk']['CLASS2']			= 'images/zendesk.png';				
			$data['zendesk']['active']			= $app->f('active') ? true : false;
			$data['zendesk']['url']				= $app->f('api');
			$data['zendesk']['email']			= $db->field("SELECT api FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='email' ");
			$data['zendesk']['password']		= $db->field("SELECT api FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='password' ");
			if($app->f('active') == 1){
				$data['zendesk']['title']		= gm('Deactivate');
			}else{
				$data['zendesk']['title']		= gm('Connect application');
			}
		}
		return $data['zendesk']; 
	}

	function get_clearfacts($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('clearfacts'=>array());

		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='ClearFacts' AND type='main' AND main_app_id='0' ");
		$sync_statuses = $db->query("SELECT constant_name, value FROM settings WHERE constant_name = 'CLEARFACTS_SYNC_STATUSES'");
		$exportAutoInvoice=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_INVOICE_CLEARFACTS'");
        $exportAutoPInvoice=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_PINVOICE_CLEARFACTS'");

        $accountant_export_invoice_force=false;
        $accountant_export_pinvoice_force=false;
        global $database_config;
        $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
        );
        $db_users = new sqldb($database_users);
        if($_SESSION['main_u_id']=='0'){
            $is_accountant=$db_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
        }else{
            $is_accountant=$db_users->field("SELECT accountant_id FROM users WHERE `database_name`='".DATABASE_NAME."' AND main_user_id='0' ");
        }
        if($is_accountant){
            $accountant_settings=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
            $acc_export_id=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_EXPORT_ID' ");
            if($accountant_settings=='1' && $acc_export_id){
                $exportAutoInvoice=$acc_export_id;
                $accountant_export_invoice_force=true;
            }
            $acc_export_id_pi=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_EXPORT_ID_PI' ");
            if($accountant_settings=='1' && $acc_export_id_pi){
            	$exportAutoPInvoice=$acc_export_id_pi;
                $accountant_export_pinvoice_force=true;
            }
        }

		if($sync_statuses->f('constant_name')){
			$sync = $sync_statuses->f('value')? true: false;
		}else{
			$sync = true;
		}
		if($app->next()){
			$data['clearfacts']['CLASS2']			= 'images/clearfacts.png';				
			$data['clearfacts']['active']			= $app->f('active') ? true : false;
			$data['clearfacts']['api_key']			= $app->f('api');
			$data['clearfacts']['username']			= $db->field("SELECT api FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='username' ");
			$data['clearfacts']['password']			= $db->field("SELECT api FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='password' ");
			if($app->f('active') == 1){
				$data['clearfacts']['title']		= gm('Deactivate');
			}else{
				$data['clearfacts']['title']		= gm('Connect application');
			}
			$data['clearfacts']['sync_statuses']	= $sync;
			$data['clearfacts']['exportAutoOpts'] = get_export_auto_invoice_options();
            $data['clearfacts']['exportAutoInvoice'] = $exportAutoInvoice ? $exportAutoInvoice : '0';
            $data['clearfacts']['exportAutoPInvoice']= $exportAutoPInvoice ? $exportAutoPInvoice : '0';
            $data['clearfacts']['accountant_export_invoice_force']   = $accountant_export_invoice_force;
            $data['clearfacts']['accountant_export_pinvoice_force']= $accountant_export_pinvoice_force;
		}
		return $data['clearfacts']; 
	}
	function get_clearfacts_url($in,$showin=true,$exit=true){
		global $config;
		$db = new sqldb();
		$vat_number = $db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNT_VAT_NUMBER' ");
		if($vat_number){
			$result = $config['clearfacts_base_url'].'/oauth2-server/authorize?scope=openid read_administrations upload_document&redirect_uri='.$config['clearfacts_redirect_url'].'&response_type=code&client_id='.$config['clearfacts_app_id'];
    		msg::success ( gm("Changes have been saved."),'success');
		}else{
			$result = '';
    		msg::error ( gm("To enable the integration with ClearFacts you first need to configure a valid VAT number in your Akti account. This can be done under your general setting. After completing this step you can continue setting up the integration with ClearFacts"),'error');
		}

    	return $result;
	}

	function get_allocloud($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('allo'=>array());

		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Allocloud' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			$data['allo']['CLASS2']			= 'images/allocloud.png';				
			$data['allo']['active']			= $app->f('active') ? true : false;
			$data['allo']['api_key']			= $app->f('api');
			$data['allo']['username']			= $db->field("SELECT api FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='username' ");
			$data['allo']['url']			= $db->field("SELECT api FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='url' ");
			if($app->f('active') == 1){
				$data['allo']['title']		= gm('Deactivate');
			}else{
				$data['allo']['title']		= gm('Connect application');
			}
		}
		return $data['allo']; 
	}


	$result = array(
		'company'				=> get_company($in,true,false),
		'NewUser'			=> get_NewUser($in,true,false),
		'NewIdentity'			=> get_NewIdentity($in,true,false),
	);
json_out($result);

/**
 * undocumented function
 *
 * @return void
 * @author 
 **/
function get_accountantAccess($in)
{	
	$data = array('accountantAccess'=>array());
	$data['accountantAccess']['language_dd']= build_language_dd_user($in['lang_id']);
	$next_function = 'settings-user-user-update';
	global $database_config;

	$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
	);
	$db = new sqldb($database_users);
	$dbu  = new sqldb();
	$db->query("SELECT * FROM users WHERE database_name='".DATABASE_NAME."' AND active=1000  ");
	$in['user_id'] = $db->f('user_id');
	
	$data['accountantAccess'] = array(
	  	'user_id'										=> $in['user_id'],
	  	'first_name'	    							=> $in['first_name']? $in['first_name']: utf8_encode($db->f('first_name')),
		'language_dd'	    							=> build_language_dd_user($db->f('lang_id')),
	  	'lang_id'										=> $db->f('lang_id'),
	  	
	);

	//$credential = $db->query("SELECT credentials, user_role, group_id, is_accountant, database_name,main_user_id FROM users WHERE user_id='".$in['user_id']."' ");
	$credential = $db->query("SELECT credentials, user_role, group_id, is_accountant, database_name,main_user_id FROM users WHERE user_id= :user_id ",['user_id'=>$in['user_id']]);

	$credential->next();
	
	$credentials = explode(';',$credential->f('credentials'));
	
	$g_cred = $dbu->field("SELECT credentials FROM groups WHERE group_id='".$credential->f('group_id')."'");
	$g_cred = explode(';',$g_cred);

	$data['accountantAccess']['credentials']    =    array(
		'CRM' 				=> in_array('1',$credentials) ? true : false,
		'Credentials'		=> $credentials,
		// 'ORDERS' 			=> in_array('2',$credentials) ? 'CHECKED' : '',
		'PROJECTS' 			=> in_array('3',$credentials) ? true : false,
		'BILLING' 			=> in_array('4',$credentials) ? true : false,
		'QUOTES' 			=> in_array('5',$credentials) ? true : false,
		'CONTRACTS' 		=> in_array('11',$credentials) ? true : false,
		'MAINTENANCE' 		=> in_array('13',$credentials) ? true : false,
		'ARTICLES' 			=> in_array('12',$credentials) ? true : ($_SESSION['acc_type']=='services' && in_array('3',$credentials) ? true : false),
		'ORDERS' 			=> in_array('6',$credentials) ? true : false,
		'PO_ORDERS' 		=> in_array('14',$credentials) ? true : false,
		'STOCK' 			=> in_array('16',$credentials) ? true : false,
		'MANAGE' 			=> in_array('7',$credentials) ? true : false,
		'PIM' 				=> in_array('9',$credentials) ? true : false,
		'TIMESHEET' 		=> in_array('8',$credentials) ? true : false,
		'PROJECTS_A'		=> $is_p_admin == 1 ? true : false,
		'MAINTENANCE_A'		=> $is_m_admin == 1 ? true : false,

		'CRM_V' 			=> in_array('1',$credentials) ? '1' : '',
		// 'ORDERS_V' 			=> in_array('2',$credentials) ? '2' : '',
		'PROJECTS_V' 		=> in_array('3',$credentials) ? '3' : '',
		'MAINTENANCE_V' 	=> in_array('13',$credentials) ? '13' : '',
		'PROJECTS_AV' 		=> $is_p_admin == 1 ? '1' : '0',
		'MAINTENANCES_AV' 	=> $is_m_admin == 1 ? '1' : '0',
		'BILLING_V' 		=> in_array('4',$credentials) ? '4' : '',
		'QUOTES_V' 			=> in_array('5',$credentials) ? '5' : '',
		'CONTRACTS_V' 		=> in_array('11',$credentials) ? '11' : '',
		'ARTICLES_V' 		=> in_array('12',$credentials) ? '12' : '',
		'ORDERS_V' 			=> in_array('6',$credentials) ? '6' : '',
		'PO_ORDERS_V' 		=> in_array('14',$credentials) ? '14' : '',
		'STOCK_V' 		=> in_array('14',$credentials) ? '14' : '',
		'MANAGE_V'	 		=> in_array('7',$credentials) ? '7' : '',
		'PIM_V'	 			=> in_array('9',$credentials) ? '9' : '',
		'TIMESHEET_V' 		=> '8',

		'CRM1' 				=> in_array('1',$g_cred) ? true: false,
		// 'ORDERS1' 			=> in_array('2',$g_cred) ? true: false,
		'PROJECTS1' 		=> in_array('3',$g_cred) ? true: false,
		'MAINTENANCE1' 		=> in_array('13',$g_cred) ? true: false,
		'BILLING1' 			=> in_array('4',$g_cred) ? true: false,
		'QUOTES1' 			=> in_array('5',$g_cred) ? true: false,
		'CONTRACTS1' 		=> in_array('11',$g_cred) ? true: false,
		'ARTICLES1'			=> in_array('12',$g_cred) ? true: ($_SESSION['acc_type']=='services' && in_array('3',$g_cred) ? true: false),
		'ORDERS1'			=> in_array('6',$g_cred) ? true: false,
		'PO_ORDERS1'		=> in_array('14',$g_cred) ? true: false,
		'STOCK1'			=> in_array('16',$g_cred) ? true: false,
		'MANAGE1' 			=> in_array('7',$g_cred) ? true: false,
		'PIM1' 				=> in_array('9',$g_cred) ? true: false,
		'TIMESHEET1'		=> in_array('8',$g_cred) ? true: false,
		'PROJECTS_A1'		=> $credential->f('group_id') == 1 ? true: false,
		'MAINTENANCE_A1'	=> $credential->f('group_id') == 1 ? true: false,
		'HIDE_A'			=> in_array('3',$credentials) ? '' : 'hide',
		'HIDE_M'			=> in_array('13',$credentials) ? '' : 'hide',
		'HIDE_CHECK'		=> in_array('13',$credentials) ? 'hide' : '',
		'HIDE_CHECK_A'		=> in_array('3',$credentials) ? 'hide' : '',
		'group_id'			=> $credential->f('group_id'),						
	);

	$data['accountantAccess']['admin_cred']=array(
			'ADMIN_1_V'=>false,
           	'ADMIN_12_V'=>false,
           	'ADMIN_6_V'=>false,
           	'ADMIN_14_V'=>false,
           	'ADMIN_5_V'=>false,
           	'ADMIN_13_V'=>false,
           	'ADMIN_3_V'=>false,
           	'ADMIN_11_V'=>false,
           	'ADMIN_4_V'=>false,
           	'ADMIN_17_V'=>false,
           	'ADMIN_19_V'=>false,
           	'ADMIN_16_V'=>false );
	$is_module_admin = $db->query("SELECT * FROM user_meta WHERE name REGEXP 'admin_[0-9]+' AND user_id='".$in['user_id']."' ");
	$is_module_admin = $is_module_admin->getAll();
	foreach ($is_module_admin as $key => $value) {
		if($value['value']){
			$data['accountantAccess']['admin_cred'][strtoupper($value['name'])]='';
			$data['accountantAccess']['admin_cred'][strtoupper($value['name']).'_V']=true;
		}
	}

	return $data;
}

	function get_zenFields(&$in){
		global $database_config;
	   	$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);
		$data=array('zenData'=>array('zenFields'=>array(),'zenDocs'=>array(),'zenTimeDocs'=>array()));
		$db  = new sqldb();
		$easyinvoice=$db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
		$ZEN_ACCOUNT_PRIMARY_ADD=$db->field("SELECT value FROM settings WHERE `constant_name`='ZEN_ACCOUNT_PRIMARY_ADD' ");
		$account_fields=$db->query("SELECT label_name, import_label_id, is_zen_field FROM import_labels WHERE type = 'customer' AND (tabel_name='customers' OR tabel_name='customer_field' OR tabel_name='customer_contact_language' OR tabel_name='customer_sector' OR tabel_name='customer_activity' OR tabel_name='customer_lead_source' OR tabel_name='sales_rep') ORDER BY label_name ASC")->getAll();
		array_push($account_fields,array('label_name'=>gm('Primary addr'),'import_label_id'=>'-1','is_zen_field'=>$ZEN_ACCOUNT_PRIMARY_ADD ? '1' : '0'));
		$temp_account_fields=array();
		foreach($account_fields as $key=>$value){
			$temp_account_fields[$value['import_label_id']]=$value['label_name'];
		}
		asort($temp_account_fields,SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
		foreach($temp_account_fields as $key=>$value){
			foreach($account_fields as $key1=>$value1){
				if($key==$value1['import_label_id']){
					$item=array(
				    	'id'		=> $value1['import_label_id'],
				    	'name'		=> gm(stripslashes($value1['label_name'])),
				    	'checked'	=> $value1['is_zen_field'] ? true : false,
				    		);
				    array_push($data['zenData']['zenFields'], $item);
					break;
				}
			}
		}
		
		$docsArray = array('1'=>gm('Deals'),'5'=>gm('Quotes'),'6'=>gm('Orders'),'3'=>gm('Projects'),'13'=>gm('Intervention'),'11'=>gm('Contracts'),'4'=>gm('Invoices'));
		foreach ($docsArray as $key=>$value){
			if(in_array($key,perm::$allow_apps)){
				if($easyinvoice=='1' && $key!='4'){
					continue;
				}
				$is_doc=$db->field("SELECT value FROM settings WHERE `constant_name`='ZEN_DOC_".$key."' ");
				$item=array(
					'id'		=> $key,
					'name'		=> $value,
					'checked'	=> (is_null($is_doc) || $is_doc=='1') ? true : false,
				);
				array_push($data['zenData']['zenDocs'], $item);
				$is_time_doc=$db->field("SELECT value FROM settings WHERE `constant_name`='ZEN_TIME_DOC_".$key."' ");
				if($key=='1'){
					continue;
				}
				$item=array(
					'id'		=> $key,
					'name'		=> $value,
					'checked'	=> (is_null($is_time_doc) || $is_time_doc=='1') ? true : false,
				);
				array_push($data['zenData']['zenTimeDocs'], $item);
			}		
		}
		$data['zenData']['zen_token']=$db_users->field("SELECT zen_token FROM users WHERE `database_name`='".DATABASE_NAME."' LIMIT 1");
		return $data;
	}

	function get_VatLang(&$in){
		$db  = new sqldb();
		$data=array();
		$vat_l=$db->query("SELECT * FROM vat_lang WHERE id='".$in['id']."' ");
		$data['en']=stripslashes($vat_l->f('en'));
		$data['fr']=stripslashes($vat_l->f('fr'));
		$data['nl']=stripslashes($vat_l->f('nl'));
		$data['de']=stripslashes($vat_l->f('de'));
		$data['id']=$in['id'];
		return $data;
	}

	function get_categorisation($in,$showin=true,$exit=true){
			$db = new sqldb();
			$i = 0;
			$j = 0;
			$data = array('bigloop'=>array());
			$array = array('segment','source','type');

			foreach ($array as $key) {
				$data['bigloop'][$j]=array(
					'name'			=> gm(ucfirst(str_replace('_', ' ', $key))),
					'table' 		=> $key,
					'count'			=> $i,
					'small'			=> array(),
				);
				$i=1;
				$db->query("SELECT * FROM tblquote_".$key." ORDER BY sort_order ");
				while ($db->next()) {
					$data['bigloop'][$j]['small'][]=array(
						'field_id'	=> $db->f('id'),
						'label'		=> gm(ucfirst($key)).' '.$i,
						'value'		=> $db->f('name'),
						'code'		=> !$db->f('code') ? '' : $db->f('code'),
					);
					$i++;
				}
				$j++;
			}
			return $data;
	}

	function get_generalSettings($in,$showin=true,$exit=true){
        $db = new sqldb();
        $data = array();    
        
        $max_image_size = $db->field("SELECT value FROM settings WHERE constant_name='MAX_IMAGE_SIZE'");

        $separator = ACCOUNT_NUMBER_FORMAT;
        $data['max_image_size'] = number_format(($max_image_size / 1048576),2,substr($separator, -1),'');
        json_out($data);
    }

    function get_legalInfo($in,$showin=true,$exit=true){
    	global $database_config;
        $database_2 = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users =  new sqldb($database_2);
        $data = array('legalInfo'=>array()); 
        //$user_gdpr=$db_users->query("SELECT gdpr_status,gdpr_time FROM users WHERE user_id='".$_SESSION['u_id']."' "); 
        $user_gdpr=$db_users->query("SELECT gdpr_status,gdpr_time FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
        $gdpr_info='';
        $data['legalInfo']['gdpr_action']=0;
        if($user_gdpr->next() && $user_gdpr->f('gdpr_status')){
        	$last_update=date('j',$user_gdpr->f('gdpr_time')).' '.date('F',$user_gdpr->f('gdpr_time')).' '.date('Y',$user_gdpr->f('gdpr_time'));
        	if($user_gdpr->f('gdpr_status')==1){
        		$gdpr_info.=gm('You selected to give consent on').' '.$last_update;
        	}else{
        		$gdpr_info.=gm('You selected not to give consent on').' '.$last_update;
        	}
        	$data['legalInfo']['gdpr_action']=$user_gdpr->f('gdpr_status');		
        }
        switch($_SESSION['l']){
        	case 'en':
        		$data['legalInfo']['service_terms_url']="https://support.akti.com/hc/nl/sections/360000205049-Voorwaarden-en-Privacy";
        		$data['legalInfo']['privay_policy_url']="https://support.akti.com/hc/nl/articles/360001239289-Privacy-Policy";
        		$data['legalInfo']['cookie_url']="https://support.akti.com/hc/nl/articles/360001240085-Cookies-Policy";
        		break;
        	case 'fr':
        		$data['legalInfo']['service_terms_url']="https://support.akti.com/hc/fr/articles/360000151329--Convention-d-utilisation-pour-Akti-";
        		$data['legalInfo']['privay_policy_url']="https://support.akti.com/hc/fr/articles/360001239289-Privacy-Policy";
        		$data['legalInfo']['cookie_url']="https://support.akti.com/hc/fr/articles/360001240085-Cookies-Policy";
        		break;
        	case 'nl':
        	case 'du':
        		$data['legalInfo']['service_terms_url']="https://support.akti.com/hc/nl/sections/360000205049-Voorwaarden-en-Privacy";
        		$data['legalInfo']['privay_policy_url']="https://support.akti.com/hc/nl/articles/360001239289-Privacy-Policy";
        		$data['legalInfo']['cookie_url']="https://support.akti.com/hc/nl/articles/360001240085-Cookies-Policy";
        		break;
        }  
        $data['legalInfo']['gdpr_info']=$gdpr_info;
        json_out($data);
    }

    function get_groups(&$in){
	    $db=new sqldb;
	    $db->query("select * from groups WHERE group_id <> 2");
	    $array = array();
	    $no_timesheets = false;
	    if (ACCOUNT_TYPE=='goods')
	    {
	        $no_timesheets = true;
	    }
	    if(!$selected){
	        if($no_timesheets)
	        {
	            $selected='1';
	        }else
	        {
	            $selected = '2';
	        }
	    }
	    $out_str="";
	    while($db->move_next()){
	        if($no_timesheets==false || $db->f('group_id')!=2)
	        {
	            //$array[$db->f('group_id')] = $db->f('name');
	            array_push( $array, array('group_id'=>$db->f('group_id'),'name'=>stripslashes($db->f('name'))));
	        }
	    }
	    array_push($array,array('group_id'=>'99999999999','name'=>''));
	    return $array;
    }

    function get_apiInfo($in,$showin=true,$exit=true){
    	global $database_config;
        $database_2 = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users =  new sqldb($database_2);
        $data = array('apiInfo'=>array('apps'=>array()));
        $user_array=array(); 
        $users=$db_users->query("SELECT user_id,first_name,last_name FROM users WHERE database_name='".DATABASE_NAME."' ");
        while($users->next()){
        	$user_array[$users->f('user_id')]=htmlspecialchars_decode(stripslashes($users->f('first_name').' '.$users->f('last_name')));
        }
        $applications=$db_users->query("SELECT * FROM oauth_clients ORDER BY application_name");
        while($applications->next()){
        	$access_tokens=array();
        	if($applications->f('is_internal')){
        		$tokens=$db_users->query("SELECT * FROM oauth_access_tokens WHERE client_id='".$applications->f('client_id')."' AND user_id IN (SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."') ORDER BY expires DESC")->getAll();
        		foreach($tokens as $key=>$value){
        			$tmp_token=array(
        				'access_token'	=> $value['access_token'],
        				'expires'		=> $value['expires'],
        				'user_name'		=> $user_array[$value['user_id']]
        			);
        			array_push($access_tokens,$tmp_token);
        		}
        	}else{
        		$tokens=$db_users->query("SELECT * FROM oauth_access_tokens WHERE client_id='".$applications->f('client_id')."' ORDER BY expires DESC")->getAll();
        		foreach($tokens as $key=>$value){
        			$tmp_token=array(
        				'access_token'	=> $value['access_token'],
        				'expires'		=> $value['expires']
        			);
        			array_push($access_tokens,$tmp_token);
        		}
        	}
        	$client_secret_hidden="";
        	for($i=0;$i<strlen($applications->f('client_secret'));$i++){
        		$client_secret_hidden.="*";
        	}
        	$app_data=array(
        		'application_name'		=> stripslashes($applications->f('application_name')),
        		'client_id'				=> $applications->f('client_id'),
        		'redirect_uri'			=> $applications->f('redirect_uri'),
        		'is_internal'			=> $applications->f('is_internal') ? true : false,
        		'access_tokens'			=> $access_tokens,
        		'expand_data'			=> false
        	);
        	array_push($data['apiInfo']['apps'],$app_data);
        }
         
        json_out($data);
    }

    function get_mail_settings($in,$showin=true,$exit=true){
    	$db=new sqldb;
    	$data=array('mail_settings'=>array());
    	$account_send_email=$db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_SEND_EMAIL'");
    	$data['mail_settings']['ACCOUNT_SEND_EMAIL']=$account_send_email=='1' ? true : false;
    	$stmp_data_array=array(
    		'PREFERRED_OPTION',
    		'EMAIL',
    		'SERVER_NAME',
    		'USERNAME',
    		'PASSWORD',
    		'USE_SSL',
    		'AUTHENTICATION',
    		'SERVER_PORT'
    	);
    	foreach($stmp_data_array as $value){
    		$constant_data=$db->field("SELECT value FROM settings WHERE constant_name='MAIL_SETTINGS_".$value."' ");
    		switch($value){
    			case 'PREFERRED_OPTION':
    				$data['mail_settings']['MAIL_SETTINGS_'.$value]=(is_null($constant_data) || $constant_data=='1') ? '1' : $constant_data;
    				break;
    			case 'PASSWORD':
    				$data['mail_settings']['MAIL_SETTINGS_'.$value]=base64_decode($constant_data);
    				break;
    			case 'USE_SSL':
    				$data['mail_settings']['MAIL_SETTINGS_'.$value]=$constant_data ? true :false;
    				break;
    			case 'AUTHENTICATION':
    				$data['mail_settings']['MAIL_SETTINGS_'.$value]=$constant_data ? $constant_data : '1';
    				break;
    			case 'SERVER_PORT':
    				$data['mail_settings']['MAIL_SETTINGS_'.$value]=is_null($constant_data) ? '587' : $constant_data;
    				break;
    			default:
    				$data['mail_settings']['MAIL_SETTINGS_'.$value]=$constant_data;
    		}
    	}
    	$data['mail_settings']['authentication_options']=array(
    		array('id'=>'1','name'=>gm('Password')),
    		array('id'=>'2','name'=>gm('MD5 Challenge-Response')),
    		array('id'=>'3','name'=>gm('NTLM'))
    	);
    	$data['mail_settings']['mail_settings_options']=array(
    		/*array('id'=>'1','name'=>gm('Akti mail server')),*/
    		array('id'=>'2','name'=>gm('Own outgoing email server (SMTP)')),
    		array('id'=>'3','name'=>gm('SendGrid (default)'))
    	);
        return json_out($data,$showin,$exit);
    }

    function get_billtobox($in){
		$db = new sqldb();
		$data = array('billtobox'=>array());
		global $config;
		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='BillToBox' AND type='main' AND main_app_id='0' ");
		$sendSalesInvoice=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_SEND_INVOICE_BILLTOBOX'");
		$exportAutoInvoice=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_INVOICE_BILLTOBOX'");

		$sendInvoiceOpts=array(
	        array('id'=>'0','name'=>gm('Via Akti')),
	        array('id'=>'1','name'=>gm('Via BillToBox')),
	    );

		//this is not used any more
        //$exportAutoPInvoice=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_PINVOICE_BILLTOBOX'");

       // $exportPurchaseInvoices=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_OPTION_PINVOICE_BILLTOBOX'");

        $accountant_export_invoice_force=false;
        global $database_config;
        $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
        );
        $db_users = new sqldb($database_users);
        if($_SESSION['main_u_id']=='0'){
            $is_accountant=$db_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
        }else{
            $is_accountant=$db_users->field("SELECT accountant_id FROM users WHERE `database_name`='".DATABASE_NAME."' AND main_user_id='0' ");
        }
        if($is_accountant){
            $accountant_settings=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
            $acc_export_id=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_EXPORT_ID' ");
            if($accountant_settings=='1' && $acc_export_id){
                $exportAutoInvoice=$acc_export_id;
                $accountant_export_invoice_force=true;
            }
        }

        $dropbox_active=$db->field("SELECT active FROM apps WHERE name='Dropbox' AND type='main' and main_app_id='0' ");
		// $export_billtobox_to_dropbox = $db->field("SELECT value FROM settings WHERE constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE' ");

		// if(!$dropbox_active && $export_billtobox_to_dropbox=='1'){
	 //        $db->query("UPDATE settings SET value='' WHERE constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE'");
	 //    }
		/*$export_billtobox_to_dropbox = $db->field("SELECT value FROM settings WHERE constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE' ");
		$export_billtobox_to_dropbox_key = $db->field("SELECT value FROM settings WHERE constant_name='EXPORT_BILLTOBOX_TO_DROPBOX_P_INVOICE_KEY' ");

		$export_pinv_to_billtobox = $db->field("SELECT value FROM settings WHERE constant_name='EXPORT_P_INVOICE_TO_BILLTOBOX' ");
		$export_pinv_to_billtobox_key = $db->field("SELECT value FROM settings WHERE constant_name='EXPORT_P_INVOICE_TO_BILLTOBOX_KEY' ");*/
				

		if($app->next()){
			if($app->f('active') == 1){
				$app_active = true;
				$in['api_key'] = $app->f('api');
				if($app->f('api')){
					$app_attr = $db->query("SELECT * FROM apps WHERE main_app_id='".$app->f('app_id')."' ");
					while ($app_attr->next()) {
						$in[$app_attr->f('type')] = $app_attr->f('active');
					}
				}
			}	
		}

		$exportPurchaseInvoices= 0;
		if($in['export_billtobox_to_dropbox'] =='1'){
			$exportPurchaseInvoices= 1; // automatic import from billtobox to dropbox
		}elseif($in['export_pinv_to_billtobox'] =='1'){
			$exportPurchaseInvoices= 2; // manual export option
		}
		

		if($app->f('active')){
				$data['billtobox']		=    array(
					'CLASS2'							=> 'images/billtobox.png',
					'api_key'							=> $db->field("SELECT api FROM apps WHERE name='BillToBox' AND type='main' AND main_app_id='0' "),
					'username'							=> $db->field("SELECT api FROM apps WHERE type='username' AND main_app_id='".$app->f('app_id')."' "),
					'is_active'							=> $app->f('active') ? true : false,
					'hide_button'						=> $app->f('active') ? 'hide' : '',
					'readonly_disabled'					=> $app->f('active') ? 'readonly disabled' : '',
					'hide_active'						=> true,
					'title'								=> gm('Deactivate'),
					'exportAutoOpts'    				=> get_export_auto_invoice_options(),
                    'exportAutoInvoice' 				=> $exportAutoInvoice ? $exportAutoInvoice : '0',
                    'exportAutoPInvoice'				=> $exportAutoPInvoice ? $exportAutoPInvoice : '0',
                    'accountant_export_invoice_force'   => $accountant_export_invoice_force,
                    'export_billtobox_to_dropbox' 			=> $in['export_billtobox_to_dropbox'],
					'access_key'							=> $db->field("SELECT api FROM apps WHERE type='export_billtobox_to_dropbox' AND main_app_id='".$app->f('app_id')."' "),

					'exportAutoPurchaseOpts'    			=> get_export_purchase_invoice_options(),
					'export_pinv_to_billtobox' 				=> $in['export_pinv_to_billtobox'],
					'access_key_pinv'						=> $db->field("SELECT api FROM apps WHERE type='export_pinv_to_billtobox' AND main_app_id='".$app->f('app_id')."' "),
					'exportPurchaseInvoices'				=> $exportPurchaseInvoices ? $exportPurchaseInvoices : '0',

					'automatic_xml_knob_class'				=> $dropbox_active!=1 ? 'not_allowed' : "",
					'dropbox_active'						=> $dropbox_active==1 ? true : false,
					'save_button_text'						=> gm('Save'),
					'active_sales_api_key'					=> $db->field("SELECT api FROM apps WHERE type='active_sales_api_key' AND main_app_id='".$app->f('app_id')."' "),
					'sendSalesInvoice'						=> $sendSalesInvoice ? $sendSalesInvoice : '0',
					'sendInvoiceOpts'                       => $sendInvoiceOpts 
				);
			}else{
				$data['billtobox']		=    array(
					'CLASS2'								=> 'images/billtobox.png',
					'api_key'								=> '',
					'username'								=>'',
					'is_active'								=> $app->f('active') ? true : false,
					'hide_button'							=> $app->f('active') ? 'hide' : '',
					'readonly_disabled'						=> $app->f('active') ? 'readonly disabled' : '',
					'hide_deactive'							=> true,
					'title'									=> gm('Connect application'),
					'exportAutoOpts'    					=> get_export_auto_invoice_options(),
                    'exportAutoInvoice' 					=> $exportAutoInvoice ? $exportAutoInvoice : '0',
                    'exportAutoPInvoice'					=> $exportAutoPInvoice ? $exportAutoPInvoice : '0',
                    'accountant_export_invoice_force'   	=> $accountant_export_invoice_force,
                    'export_billtobox_to_dropbox' 			=> 0,
					'access_key'							=> '',

					'exportAutoPurchaseOpts'    			=> get_export_purchase_invoice_options(),
					'export_pinv_to_billtobox' 				=> 0,
					'access_key_pinv'						=> '',
					'exportPurchaseInvoices'				=> 0,

					'dropbox_active'						=> false,
					'save_button_text'						=> gm('Save'),
					'active_sales_api_key'					=> '',
					'sendSalesInvoice'						=> $sendSalesInvoice ? $sendSalesInvoice : '0',
					'sendInvoiceOpts'                       => $sendInvoiceOpts 
				);
			}

			return $data['billtobox'];
			//return json_out($data['billtobox'], $showin,$exit); 
	}


	function get_sendgrid($in,$showin=true,$exit=true){
		/*$db = new sqldb();
		$data = array('sendgrid'=>array());

		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='SendGrid' AND type='main' AND main_app_id='0' ");
		if($app->next()){
			$data['sendgrid']['CLASS2']			= 'images/sendgrid.png';				
			$data['sendgrid']['active']			= $app->f('active') ? true : false;
			$data['sendgrid']['api_key']	= $db->field("SELECT api FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='api_key' ");
			if($app->f('active') == 1){
				$data['sendgrid']['title']		= gm('Deactivate application');
			}else{
				$data['sendgrid']['title']		= gm('Connect application');
			}
		}

		$has_admin_rights = getHasAdminRights(array('module'=>'invoice'));
		$data['sendgrid']['has_admin_rights'] = $has_admin_rights;
*/
		return $data['sendgrid']; 
	}

	function get_sendgridSettings($in,$showin=true,$exit=true){
        $db = new sqldb();
        $data = array('sendgridSettings'=>array('general_settings'=>array('sales'=>array(),'downpayment'=>array(),'credit_notes'=>array()),'history'=>array())); 
      
        $array_values = array(array('constant_name'=>'S_TEMPLATE_NL',
        							'for'=>'sales',
        							'label' => gm("Template (NL)")
        						),
        					array('constant_name'=>'S_TEMPLATE_FR',
        							'for'=>'sales',
        							'label' => gm("Template (FR)")
        						),
        					array('constant_name'=>'S_TEMPLATE_EN',
        							'for'=>'sales',
        							'label' => gm("Template (EN)")
        						),
        					array('constant_name'=>'D_TEMPLATE_NL',
        							'for'=>'downpayment',
        							'label' => gm("Template (NL)")
        						),
        					array('constant_name'=>'D_TEMPLATE_FR',
        							'for'=>'downpayment',
        							'label' => gm("Template (FR)")
        						),
        					array('constant_name'=>'D_TEMPLATE_EN',
        							'for'=>'downpayment',
        							'label' => gm("Template (EN)")
        						),
        					array('constant_name'=>'C_TEMPLATE_NL',
        							'for'=>'credit_notes',
        							'label' => gm("Template (NL)")
        						),
        					array('constant_name'=>'C_TEMPLATE_FR',
        							'for'=>'credit_notes',
        							'label' => gm("Template (FR)")
        						),
        					array('constant_name'=>'C_TEMPLATE_EN',
        							'for'=>'credit_notes',
        							'label' => gm("Template (EN)")
        						),
    					);

        // $block_res = array('sales'=>array(),'downpayment'=>array(),'credit_notes'=>array());

        foreach ($array_values as $value) {
        	$model_val = $db->field("SELECT value from settings where constant_name='".'SENDGRID_SETTING_'.$value['constant_name']."'");
        	$tmp = array('value'=>$model_val,'label'=>$value['label'],'c_name'=>$value['constant_name']);

        	if($value['for'] == 'sales'){
        		array_push($data['sendgridSettings']['general_settings']['sales'], $tmp);
        	} else if($value['for'] == 'downpayment'){
        		array_push($data['sendgridSettings']['general_settings']['downpayment'], $tmp);
        	} else if($value['for'] == 'credit_notes'){
        		array_push($data['sendgridSettings']['general_settings']['credit_notes'], $tmp);
        	}	
        }

        $use_for_inv = $db->field("SELECT value from settings where constant_name='SENDGRID_SETTING_USE_FOR_INV'");
        $uns_group_id = $db->field("SELECT value from settings where constant_name='SENDGRID_SETTING_UNS_GROUP_ID'");

        $data['sendgridSettings']['general_settings']['use_for_inv'] = $use_for_inv ? true : false;
		$data['sendgridSettings']['general_settings']['uns_group_id'] = $uns_group_id;
		// $data['sendgridSettings']['general_settings']['block_res'] = $block_res;
                    
        $data['sendgridSettings']['history']['total_history']=$db->field("SELECT COUNT(id) FROM sendgrid_history");
        $data['sendgridSettings']['history']['total_error']=$db->field("SELECT COUNT(id) FROM sendgrid_history WHERE webservice_response_code>'300' ");
        $data['sendgridSettings']['history']['total_success']=$db->field("SELECT COUNT(id) FROM sendgrid_history WHERE webservice_response_code BETWEEN '200' AND '299' ");

        $last_24=strtotime('-1 day');

        $data['sendgridSettings']['history']['total_last24']=$db->field("SELECT COUNT(id) FROM sendgrid_history WHERE UNIX_TIMESTAMP(sent_date)>='".$last_24."' ");
        $data['sendgridSettings']['history']['total_last24_error']=$db->field("SELECT COUNT(id) FROM sendgrid_history WHERE webservice_response_code>'300' AND UNIX_TIMESTAMP(sent_date)>='".$last_24."' ");
        $data['sendgridSettings']['history']['total_last24_success']=$db->field("SELECT COUNT(id) FROM sendgrid_history WHERE webservice_response_code BETWEEN '200' AND '299' AND UNIX_TIMESTAMP(sent_date)>='".$last_24."'");

        json_out($data);
    }

    function get_sendgridHistory($in,$showin=true,$exit=true){
    	$db = new sqldb();
    	$data=array('list'=>array());
    	$l_r =ROW_PER_PAGE;
		$data['lr']=ROW_PER_PAGE;
		if((!$in['offset']) || (!is_numeric($in['offset']))){
			$offset=0;
		}else{
			$offset=$in['offset']-1;
		}
		$limit=$offset*$l_r.','.$l_r;

		$last_24=strtotime('-1 day');
		$total_data=$db->field("SELECT COUNT(id) FROM sendgrid_history WHERE UNIX_TIMESTAMP(sent_date)>='".$last_24."' ");
		$max_rows=$total_data;
		$data['max_rows']=$max_rows;

		$log_data=$db->query("SELECT * FROM sendgrid_history WHERE UNIX_TIMESTAMP(sent_date)>='".$last_24."' ORDER BY sent_date DESC, id DESC LIMIT ".$limit." ");
		while($log_data->next()){
			$webservice_resource_name='';
			$webservice_resource_href='';
			switch($log_data->f('webservice_name')){
				case 'mail/send':
					if($log_data->f('webservice_id')){
						if($log_data->f('module') == 'invoice'){
							$webservice_resource_name=gm('Invoice').' '.stripslashes($db->field("SELECT serial_number FROM tblinvoice WHERE id='".$log_data->f('webservice_id')."' "));
							$webservice_resource_href='invoice/view/'.$log_data->f('webservice_id');
						}else{
							//FOR FURTHER DEVELOPMENT EXAMPLE
							// $webservice_resource_name=gm('Purchase Order').' '.stripslashes($db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id='".$log_data->f('webservice_id')."' "));
							// $webservice_resource_href='purchase_order/view/'.$log_data->f('webservice_id');
						}
					}								
					break;
				case 'mail/batch':
					if($log_data->f('webservice_id')){
						if($log_data->f('module') == 'invoice'){
							$webservice_resource_name=gm('Invoice').' '.stripslashes($db->field("SELECT serial_number FROM tblinvoice WHERE id='".$log_data->f('webservice_id')."' "));
							$webservice_resource_href='invoice/view/'.$log_data->f('webservice_id');
						}
					}								
					break;
				case 'user/scheduled_sends':
					if($log_data->f('webservice_id')){
						if($log_data->f('module') == 'invoice'){
							$webservice_resource_name=gm('Invoice').' '.stripslashes($db->field("SELECT serial_number FROM tblinvoice WHERE id='".$log_data->f('webservice_id')."' "));
							$webservice_resource_href='invoice/view/'.$log_data->f('webservice_id');
						}
					}								
					break;
				case 'asm/groups/{group_id}/suppressions':
					if($log_data->f('webservice_id')){
						if($log_data->f('module') == 'contact' || $log_data->f('module') == 'groupsuppressionscontact'){
							$webservice_resource_name=gm('Contact').' '.stripslashes($db->field("SELECT CONCAT(firstname,' ' ,lastname) FROM customer_contacts WHERE contact_id='".$log_data->f('webservice_id')."' "));
							$webservice_resource_href='contactView/'.$log_data->f('webservice_id');
						}
					}								
					break;
				case 'asm/groups/{group_id}/suppressions/{email}':
					if($log_data->f('webservice_id')){
						if($log_data->f('module') == 'contact' || $log_data->f('module') == 'groupsuppressionscontact'){
							$webservice_resource_name=gm('Contact').' '.stripslashes($db->field("SELECT CONCAT(firstname,' ',lastname) FROM customer_contacts WHERE contact_id='".$log_data->f('webservice_id')."' "));
							$webservice_resource_href='contactView/'.$log_data->f('webservice_id');
						}
					}								
					break;				
				default:
					break;
			}

			if(is_array(stripslashes(unserialize($log_data->f('webservice_incomming'))))){
				$web_incomming=json_encode(stripslashes(unserialize($log_data->f('webservice_incomming'))));
			}else{
				$web_incomming=stripslashes(unserialize($log_data->f('webservice_incomming')));
			}

			$item=array(
				'date'			=> $log_data->f('sent_date'),
				'webservice'	=> stripslashes($log_data->f('webservice_name')),
				'webservice_id'	=> $log_data->f('webservice_id'),
				'webservice_resource_name'	=> $webservice_resource_name,
				'webservice_resource_href'	=> $webservice_resource_href,
				'http_response'	=> $log_data->f('webservice_response_code'),
				'http_result'	=> $log_data->f('webservice_response_code') >= 200 && $log_data->f('webservice_response_code') < 300? gm('Success') : gm('Fail'),
				'webservice_incomming'	=> $web_incomming ? $web_incomming : gm('Success'),
				'webservice_outgoing'	=> is_array(unserialize($log_data->f('webservice_outgoing'))) ? json_encode(unserialize($log_data->f('webservice_outgoing'))) : unserialize(stripslashes($log_data->f('webservice_outgoing')))
			);
			array_push($data['list'], $item);
		}

    	json_out($data);
    }

    function get_facq($in){
		$db = new sqldb();
		$data = array('facq'=>array());
		global $config;
		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Facq' AND type='main' AND main_app_id='0' ");

        global $database_config;
        $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
        );
        $db_users = new sqldb($database_users);			

		if($app->next()){
			if($app->f('active') == 1){
				$app_active = true;
				
					$app_attr = $db->query("SELECT * FROM apps WHERE main_app_id='".$app->f('app_id')."' ");
					while ($app_attr->next()) {
						$in[$app_attr->f('type')] = $app_attr->f('api');
					}
			}	
		}

		if($app->f('active')){
				$data['facq']		=    array(
					'CLASS2'			=> 'images/facq.png',
					'UserID'			=> $in['UserID'],
					'Login'				=> $in['Login'],
					'Password' 			=> $in['Password'],
					'is_active'			=> $app->f('active') ? true : false,
					'hide_button'		=> $app->f('active') ? 'hide' : '',
					'readonly_disabled'	=> $app->f('active') ? 'readonly disabled' : '',
					'hide_active'		=> true,
					'title'				=> gm('Deactivate'),
					'automatic_xml_knob_class'				=> $dropbox_active!=1 ? 'not_allowed' : "",
					'save_button_text'						=> gm('Save'),
				);
			}else{
				$data['facq']		=    array(
					'CLASS2'			=> 'images/facq.png',
					'UserID'			=> $in['UserID'],
					'Login'				=> $in['Login'],
					'Password' 			=> $in['Password'],
					'is_active'			=> $app->f('active') ? true : false,
					'hide_button'		=> $app->f('active') ? 'hide' : '',
					'readonly_disabled'	=> $app->f('active') ? 'readonly disabled' : '',
					'hide_deactive'		=> true,
					'title'				=> gm('Connect application'),
					
					'save_button_text'						=> gm('Save'),
				);
			}

			return $data['facq'];

	}


	function get_exact_online($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('exact_online'=>array());

		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Exact online' AND type='main' AND main_app_id='0' ");
		$sync_statuses = $db->query("SELECT constant_name, value FROM settings WHERE constant_name = 'EXACT_ONLINE_SYNC_STATUSES'");
		$exportAutoInvoice=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_INVOICE_EXACT_ONLINE'");
        $exportAutoPInvoice=$db->field("SELECT value FROM settings WHERE constant_name='EXPORT_AUTO_PINVOICE_EXACT_ONLINE'");

        $accountant_export_invoice_force=false;
        $accountant_export_pinvoice_force=false;
        global $database_config;
        $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
        );
        $db_users = new sqldb($database_users);
        if($_SESSION['main_u_id']=='0'){
            $is_accountant=$db_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
        }else{
            $is_accountant=$db_users->field("SELECT accountant_id FROM users WHERE `database_name`='".DATABASE_NAME."' AND main_user_id='0' ");
        }
        if($is_accountant){
            $accountant_settings=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
            $acc_export_id=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_EXPORT_ID' ");
            if($accountant_settings=='1' && $acc_export_id){
                $exportAutoInvoice=$acc_export_id;
                $accountant_export_invoice_force=true;
            }
            $acc_export_id_pi=$db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_EXPORT_ID_PI' ");
            if($accountant_settings=='1' && $acc_export_id_pi){
            	$exportAutoPInvoice=$acc_export_id_pi;
                $accountant_export_pinvoice_force=true;
            }
        }

		if($sync_statuses->f('constant_name')){
			$sync = $sync_statuses->f('value')? true: false;
		}else{
			$sync = true;
		}
		if($app->next()){
			$data['exact_online']['CLASS2']			= 'images/exact_online.png';				
			$data['exact_online']['active']			= $app->f('active') ? true : false;
			$data['exact_online']['api_key']			= $app->f('api');
			$data['exact_online']['access_token']			= $db->field("SELECT api FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='access_token' ");
			$data['exact_online']['username']			= $db->field("SELECT api FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='username' ");
			$data['exact_online']['password']			= $db->field("SELECT api FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='password' ");
			$data['exact_online']['exact_baseurl']			= $db->field("SELECT api FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='exact_baseurl' ");
			if($app->f('active') == 1){
				$data['exact_online']['title']		= gm('Deactivate');
			}else{
				$data['exact_online']['title']		= gm('Connect application');
			}
			$data['exact_online']['sync_statuses']	= $sync;
			//$data['exact_online']['exportAutoOpts'] = get_export_auto_invoice_options();
			$data['exact_online']['exportAutoOpts'] = array(array('id'=>'0','name'=>gm('Manual'))); //temorary
            $data['exact_online']['exportAutoInvoice'] = $exportAutoInvoice ? $exportAutoInvoice : '0';
            $data['exact_online']['exportAutoPInvoice']= $exportAutoPInvoice ? $exportAutoPInvoice : '0';
            $data['exact_online']['accountant_export_invoice_force']   = $accountant_export_invoice_force;
            $data['exact_online']['accountant_export_pinvoice_force']= $accountant_export_pinvoice_force;
            $data['exact_online']['save_button_text']						= gm('Save');
		}
		return $data['exact_online']; 
	}
	function get_exact_online_url($in,$showin=true,$exit=true){
		global $config;
		$db = new sqldb();
		
		if($config['exact_baseurl']){
			$result = $config['exact_baseurl'].'/api/oauth2/auth?redirect_uri='.$config['exact_redirect_url'].'&response_type=code&client_id='.$config['exact_client_id'];
    		msg::success ( gm("Changes have been saved."),'success');
		}/*else{
			$result = '';
    		msg::error ( gm("To enable the integration with Exact Online you first need to select your country server."),'error');
		}*/

    	return $result;
	}

	    function get_netsuite($in){
		$db = new sqldb();
		$data = array('netsuite'=>array());
		global $config;
		$app_active = false;
		$app = $db->query("SELECT * FROM apps WHERE name='Netsuite' AND type='main' AND main_app_id='0' ");

        global $database_config;
        $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
        );
        $db_users = new sqldb($database_users);			

		if($app->next()){
			if($app->f('active') == 1){
				$app_active = true;
				
					$app_attr = $db->query("SELECT * FROM apps WHERE main_app_id='".$app->f('app_id')."' ");
					while ($app_attr->next()) {
						$in[$app_attr->f('type')] = $app_attr->f('api');
					}
			}	
		}

		if($app->f('active')){
				$data['netsuite']		=   array(
					'CLASS2'			=> 'images/netsuite.png',
					'is_active'			=> $app->f('active') ? true : false,
					'hide_button'		=> $app->f('active') ? 'hide' : '',
					'readonly_disabled'	=> $app->f('active') ? 'readonly disabled' : '',
					'hide_active'		=> true,
					'title'				=> gm('Deactivate'),
					'save_button_text'	=> gm('Save'),
				);
			}else{
				$data['netsuite']		=    array(
					'CLASS2'			=> 'images/netsuite.png',
					'is_active'			=> $app->f('active') ? true : false,
					'hide_button'		=> $app->f('active') ? 'hide' : '',
					'readonly_disabled'	=> $app->f('active') ? 'readonly disabled' : '',
					'hide_deactive'		=> true,
					'title'				=> gm('Connect application'),					
					'save_button_text'	=> gm('Save'),
				);
			}

			return $data['netsuite'];

	}

	function get_jefacture($in){
		$db = new sqldb();
		$data = array('jefacture'=>array());
		global $config;
		$app_active = false;

		$app = $db->query("SELECT * FROM apps WHERE name='Jefacture' AND type='main' AND main_app_id='0' ");

		if($app->f('active')){
				$data['jefacture']		=    array(
					'CLASS2'							=> 'images/jefacture.png',
					'api_key'							=> $db->field("SELECT api FROM apps WHERE name='Jefacture' AND type='main' AND main_app_id='0' "),
					'is_active'							=> $app->f('active') ? true : false,
					'hide_button'						=> $app->f('active') ? 'hide' : '',
					'readonly_disabled'					=> $app->f('active') ? 'readonly disabled' : '',
					'hide_active'						=> true,
					'title'								=> gm('Deactivate'),
					'save_button_text'						=> gm('Save'),
				);
			}else{
				$data['jefacture']		=    array(
					'CLASS2'								=> 'images/jefacture.png',
					'api_key'								=> '',
					'is_active'								=> $app->f('active') ? true : false,
					'hide_button'							=> $app->f('active') ? 'hide' : '',
					'readonly_disabled'						=> $app->f('active') ? 'readonly disabled' : '',
					'hide_deactive'							=> true,
					'title'									=> gm('Connect application'),			
					'save_button_text'						=> gm('Save'),
				);
			}
			return $data['jefacture'];
	}


?>
