<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }

	$result=array();

	global $config,$site_url,$database_config;
	if(strpos($config['site_url'],'akti.com')){
	   ark::loadLibraries(array('stripe/init'));
	}
	$db_config = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$db_users = new sqldb($db_config);

	$db = new sqldb();

	//$u_plan = $db_users->query("SELECT * FROM users WHERE user_id='".$_SESSION['u_id']."' ");
	$u_plan = $db_users->query("SELECT * FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	$user_paid=$u_plan->f('payed');
	$user_stripe=$u_plan->f('stripe_cust_id');
	$user_active=$u_plan->f('active');
	$user_stripe_subscr=$u_plan->f('stripe_subscr_id');
	$user_discount_id=$u_plan->f('stripe_discount_id');
	//$new_plan_reduced=$db->field("SELECT value FROM settings WHERE constant_name='REDUCED_START_PRICE' ");
	$new_plan_reduced=$db->field("SELECT value FROM settings WHERE constant_name='REDUCE_BASE_PRICE' ");
	$acc_type=$db->field("SELECT value FROM settings WHERE constant_name='SUBSCRIPTION_TYPE'");
	$easyinvoice=$db->field("SELECT value FROM settings WHERE constant_name='EASYINVOICE'");

	$plan_vat=0;
	if($in['stripe_vat']){
		$plan_vat=21;
	}
	
	$stripe_ids=array(
				'crm_adv'			=> 102,
				'quotes_adv'		=> 103,
				'products'			=> 104,
				'stocks'			=> 105,
				'sepa'				=> 106,
				'site'				=> 107,
				'contracts'			=> 108,
				'intervention'		=> 109,
				'projects'			=> 110,
				'report_adv'		=> 111,
				'orders' 			=> 112,
				'cashregister' 		=> 113,
				'timetracker'		=> 115);

	
	$selected_plan = $u_plan->f('pricing_plan_id');
	$result['selected_plan']=$selected_plan;
	if(!$in['pricing_plan_id']){
		$in['pricing_plan_id']=$selected_plan;	
	}
	
	$result['tooltip_text']=gm("Contact our support to change your subscription");

	$result['pricing_plans']= get_subscription_plans($selected_plan);
	$nr_users = $result['pricing_plans'][$selected_plan-1]['nr_users'];

	$stripe_plans=array();	
	if($in['aditional']){
		/*if($u_plan->f('accountant_id') && (is_null($new_plan_reduced) || $new_plan_reduced=='1')){
			array_push($stripe_plans, array("plan"=>$in['period']=='1' ? 300 : 100,"quantity"=>1));
		}else{
			array_push($stripe_plans, array("plan"=>$in['period']=='1' ? 301 : 101,"quantity"=>1));
		}*/
		if($acc_type=='1'){
			array_push($stripe_plans, array("plan"=>$in['period']=='1' ? 301 : 101,"quantity"=>1));
		}else{
			if($u_plan->f('accountant_id') && $new_plan_reduced=='1'){
				array_push($stripe_plans, array("plan"=>$in['period']=='1' ? 300 : 100,"quantity"=>1));
			}else{
				array_push($stripe_plans, array("plan"=>$in['period']=='1' ? 314 : 114,"quantity"=>1));
			}
		}
		foreach($in['aditional'] as $key=>$value){
			if($value[$value['model']]=='1'){
				/*if($u_plan->f('accountant_id') && (is_null($new_plan_reduced) || $new_plan_reduced=='1')){
					array_push($stripe_plans, array("plan"=>$in['period']=='1' ? $stripe_ids[$value['model']]+200 : $stripe_ids[$value['model']],"quantity"=>1));
				}else{
					if($value['model']=='crm_adv' || $value['model']=='quotes_adv'){
						continue;
					}else{
						array_push($stripe_plans, array("plan"=>$in['period']=='1' ? $stripe_ids[$value['model']]+200 : $stripe_ids[$value['model']],"quantity"=>1));
					}
				}*/
				if($acc_type=='1'){
					if($value['model']=='crm_adv' || $value['model']=='quotes_adv'){
						continue;
					}else{
						array_push($stripe_plans, array("plan"=>$in['period']=='1' ? $stripe_ids[$value['model']]+200 : $stripe_ids[$value['model']],"quantity"=>1));
					}
				}else{
					array_push($stripe_plans, array("plan"=>$in['period']=='1' ? $stripe_ids[$value['model']]+200 : $stripe_ids[$value['model']],"quantity"=>1));
				}
			}	
		}	
	}	
	if($in['users']>0){
		array_push($stripe_plans, array("plan"=>$in['period']=='1' ? 298 : 98,"quantity"=>(int)$in['users']-2));
	}
	/*if($in['users_t']>0){
		array_push($stripe_plans, array("plan"=>"99","quantity"=>(int)$in['users_t']));
	}*/


	if(((!$user_paid || $user_paid==2 || $user_paid==3 || $user_active==0 || !$user_stripe) && $in['stripeToken']) || ($user_stripe && $user_paid==1 && $u_plan->f('stripe_subscr_id')=='' && $in['stripeToken'])){

		\Stripe\Stripe::setApiKey($config['stripe_api_key']);

		if($u_plan->f('stripe_cust_id')!='' && $u_plan->f('stripe_subscr_id')==''){
			if($in['change_card']){
				try{
					$cu = \Stripe\Customer::retrieve($u_plan->f('stripe_cust_id'));
					$cu->source = $in['stripeToken']; 
					$cu->save();

					$update = $db_users->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account changed credit card', `date`='".time()."' ");
					$db_users->query("DELETE FROM settings WHERE constant_name='CHANGE_CARD' ");

					msg::success(gm("Changes saved"),"success");
				}catch(\Stripe\Error\Card $e){
					msg::notice($e->getMessage(),"notice");
				}	
			}else{
					try{
						/*$stripe_date=time();
						$cu = \Stripe\Customer::retrieve($u_plan->f('stripe_cust_id'));
						if($in['user_disc'] != '0'){
							$cu->coupon = $in['user_disc'];
						}
						$cu->source = $in['stripeToken'];
						$cu->email = $in['stripeEmail'];		
						$cu->save();
						$cu = \Stripe\Customer::retrieve($u_plan->f('stripe_cust_id'));
						$subscription=$cu->subscriptions->create(array(
							"items"		=> $stripe_plans,			
							"tax_percent" => $plan_vat
						));
						//$db_users->query("UPDATE users SET stripe_subscr_id='".$subscription['id']."' WHERE user_id='".$_SESSION['u_id']."'");
						$db_users->query("UPDATE users SET stripe_subscr_id= :stripe_subscr_id WHERE user_id= :user_id",['stripe_subscr_id'=>$subscription['id'],'user_id'=>$_SESSION['u_id']]);
						$subscription_update = $cu->subscriptions->retrieve($subscription['id']);
						$subscription_update->prorate = false;
						$subscription_update->save();
						$stripe_inv=\Stripe\Invoice::all(array("customer" => $u_plan->f('stripe_cust_id'),"date" => array("gte"=> $stripe_date)));*/

						include_once('apps/settings/model/payment.php');
						$payment=new payment(); 
						$payment->pay_new($in);
							
						/*$inv_incoming=\Stripe\Invoice::upcoming(array("customer" => $u_plan->f('stripe_cust_id')));*/
						/*$next_date = $inv_incoming['date'];*/
						$next_date = $in['period'] =='1' ? strtotime('+1 month') : strtotime('+1 year');
						$time = time();
						/*$update = $db_users->query("UPDATE users SET payed='1', active ='1', pay_id='".$stripe_inv['data'][0]['id']."', last_payment='".return_value($in['total_price'])."', next_date='".$next_date."', alias='".$_SESSION['u_id']."', pricing_plan_id ='".$in['pricing_plan_id']."' WHERE database_name='".DATABASE_NAME."' AND active!='1000'");*/
						$update = $db_users->query("UPDATE users SET payed='1', alias='".$_SESSION['u_id']."', pricing_plan_id ='".$in['pricing_plan_id']."', base_plan_id ='".$in['pricing_plan_id']."', period ='".$in['period']."' WHERE database_name='".DATABASE_NAME."' AND active!='1000'");
						/*$db_users->query("UPDATE users SET payed='1', pay_id='".$stripe_inv['data'][0]['id']."', last_payment='".return_value($in['total_price'])."', next_date='".$next_date."', alias='".$_SESSION['u_id']."' WHERE database_name='".DATABASE_NAME."' AND active='1000'");*/
						/*$db_users->query("UPDATE users SET payed= :payed, pay_id= :pay_id, last_payment= :last_payment, next_date= :next_date, alias= :alias, pricing_plan_id =:pricing_plan_id WHERE database_name= :d AND active= :active",['payed'=>'1','pay_id'=>$stripe_inv['data'][0]['id'],'last_payment'=>return_value($in['total_price']),'next_date'=>$next_date,'alias'=>$_SESSION['u_id'],'pricing_plan_id'=>$in['pricing_plan_id'],'d'=>DATABASE_NAME,'active'=>'1000']);*/
						$db_users->query("UPDATE users SET payed= :payed, alias= :alias, pricing_plan_id =:pricing_plan_id, base_plan_id =:base_plan_id, period =:period WHERE database_name= :d AND active= :active",['payed'=>'1','alias'=>$_SESSION['u_id'],'pricing_plan_id'=>$in['pricing_plan_id'],'base_plan_id'=>$in['pricing_plan_id'],'period'=>$in['period'],'d'=>DATABASE_NAME,'active'=>'1000']);
						//$payment->payment_invoice_stripe($_SESSION['u_id'],$stripe_inv['data'][0]['id']);
						
						$user_update = $db_users->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' ");
						while ($user_update->next()) {
							$update = $db_users->query("UPDATE user_info SET end_date='".$next_date."', is_trial='1' WHERE user_id='".$user_update->f('user_id')."' ");
						}
					/*	if(defined('CHANGE_CARD') && CHANGE_CARD == 1){
							$info = $db_users->query("SELECT payment_fail_next,payment_fail FROM users WHERE database_name='".DATABASE_NAME."' LIMIT 1");
							if($info->f('payment_fail_next') > ($next_date - (11*24*60*60)) ){
								# there is no cow level
								$update = $db_users->query("UPDATE users SET next_date='".$info->f('payment_fail_next')."' WHERE database_name='".DATABASE_NAME."'");
							}
							$update = $db_users->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account changed credit card', `date`='".$time."' ");
							$db_users->query("DELETE FROM settings WHERE constant_name='CHANGE_CARD' ");
						}else{
							$update = $db_users->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account activated by customer', `date`='".$time."' ");
						}
						$update = $db_users->query("UPDATE users SET payment_fail_next='0',payment_fail='0'
									WHERE database_name='".DATABASE_NAME."'");*/
						
						
						$result['payed'] = true;
						unset($_SESSION['is_trial']);
						$first_paid = $db_users->field("SELECT first_paid FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' ");
						if(!$first_paid){
							$paid_update = $db_users->query("UPDATE users SET first_paid='".$time."'
																		WHERE database_name='".DATABASE_NAME."'");
							
							$in['time']=$time;
							$in['next_date']=$next_date;
							create_reccuring_invoice($in);
							send_email_won_customer($_SESSION['u_id'], $in['period']);											
						}else{
							send_email_update_subscription($_SESSION['u_id'], $in['pricing_plan_id'], $in['period'], $in['users']);
						}
						
						msg::success(gm('The payment process was successful'),"success");
					}catch(\Stripe\Error\Card $e){
						msg::notice($e->getMessage(),"notice");
					}
		  	}
		}else{
			try{
				$stripe_date=time();
				if($in['user_disc'] != '0'){
					$new_cust=\Stripe\Customer::create(array(
					  "coupon" => $in['user_disc'],
					  "description" => $u_plan->f('ACCOUNT_COMPANY').'-'.$u_plan->f('first_name').' '.$u_plan->f('last_name'),
					  "source" => $in['stripeToken'],
					  "email"=> $in['stripeEmail'],		  
					));
				}else{
					$new_cust=\Stripe\Customer::create(array(
					  "description" => $u_plan->f('ACCOUNT_COMPANY').'-'.$u_plan->f('first_name').' '.$u_plan->f('last_name'),
					  "source" => $in['stripeToken'],
					  "email"=> $in['stripeEmail'],		  
					));
				}
				//$db_users->query("UPDATE users SET stripe_cust_id='".$new_cust['id']."' WHERE user_id='".$_SESSION['u_id']."'");
				$db_users->query("UPDATE users SET stripe_cust_id= :stripe_cust_id WHERE user_id= :user_id",['stripe_cust_id'=>$new_cust['id'],'user_id'=>$_SESSION['u_id']]);

				if(strpos($config['site_url'],'my.akti')){
				//if(strpos($config['site_url'],'akti')){
					global $database_config;
					$database_new = array(
							'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
							'username' => 'admin',
							'password' => 'hU2Qrk2JE9auQA',
							'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
							//'database' => '32ada082_5cf4_1518_3aa09bca7f29'
					);
						
					$db_user_new = new sqldb($database_new);

					$select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$_SESSION['u_id']."' AND field_id='1' ");
					if($select_contact){
						$select_customer = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$select_contact."' AND `primary`='1'");
					}		
					if($select_customer){
						$db_user_new->query("UPDATE customers SET stripe_cust_id='".$new_cust['id']."' WHERE customer_id='".$select_customer."'");
						$db_user_new->query("INSERT INTO customer_field SET
													field_id='1',
													customer_id='".$select_customer."',
													value='".$new_cust['id']."'");
					}			
					
				}
				/*$cu = \Stripe\Customer::retrieve($new_cust['id']);
				$subscription=$cu->subscriptions->create(array(
					"items"		=> $stripe_plans,			
					"tax_percent" => $plan_vat
				));*/
				//$db_users->query("UPDATE users SET stripe_subscr_id='".$subscription['id']."' WHERE user_id='".$_SESSION['u_id']."'");
				/*$db_users->query("UPDATE users SET stripe_subscr_id= :stripe_subscr_id WHERE user_id= :user_id",['stripe_subscr_id'=>$subscription['id'],'user_id'=>$_SESSION['u_id']]);
				$subscription_update = $cu->subscriptions->retrieve($subscription['id']);
				$subscription_update->prorate = false;
				$subscription_update->save();
				$stripe_inv=\Stripe\Invoice::all(array("customer" => $new_cust['id'],"date" => array("gte"=> $stripe_date)));*/

				include_once('apps/settings/model/payment.php');
				$payment=new payment(); 
				$payment->pay_new($in);

				/*$inv_incoming=\Stripe\Invoice::upcoming(array("customer" => $new_cust['id']));*/
				/*$next_date = $inv_incoming['date'];*/
				$next_date = $in['period'] =='1' ? strtotime('+1 month') : strtotime('+1 year');
				//$next_date = mktime(0,0,1,date('n')+1,date('j'),date('Y'));
				$time = time();
				/*$update = $db_users->query("UPDATE users SET payed='1', active ='1', pay_id='".$stripe_inv['data'][0]['id']."', last_payment='".return_value($in['total_price'])."', next_date='".$next_date."', alias='".$_SESSION['u_id']."', pricing_plan_id ='".$in['pricing_plan_id']."' WHERE database_name='".DATABASE_NAME."' AND active!='1000'");*/

				$update = $db_users->query("UPDATE users SET payed='1', next_date='".$next_date."', alias='".$_SESSION['u_id']."', pricing_plan_id ='".$in['pricing_plan_id']."', base_plan_id ='".$in['pricing_plan_id']."', period ='".$in['period']."' WHERE database_name='".DATABASE_NAME."' AND active!='1000'");

				/*$db_users->query("UPDATE users SET payed='1', pay_id='".$stripe_inv['data'][0]['id']."', last_payment='".return_value($in['total_price'])."', next_date='".$next_date."', alias='".$_SESSION['u_id']."' WHERE database_name='".DATABASE_NAME."' AND active='1000'");*/
				/*$db_users->query("UPDATE users SET payed= :payed, pay_id= :pay_id, last_payment= :last_payment, next_date= :next_date, alias= :alias, pricing_plan_id =:pricing_plan_id WHERE database_name= :d AND active= :active",['payed'=>'1','pay_id'=>$stripe_inv['data'][0]['id'],'last_payment'=>return_value($in['total_price']),'next_date'=>$next_date,'alias'=>$_SESSION['u_id'],'pricing_plan_id'=>$in['pricing_plan_id'],'d'=>DATABASE_NAME,'active'=>'1']);*/

				$db_users->query("UPDATE users SET payed= :payed, next_date= :next_date, alias= :alias, pricing_plan_id =:pricing_plan_id, base_plan_id =:base_plan_id, period =:period  WHERE database_name= :d AND active= :active",['payed'=>'1', 'next_date'=>$next_date,'alias'=>$_SESSION['u_id'],'pricing_plan_id'=>$in['pricing_plan_id'],'base_plan_id'=>$in['pricing_plan_id'],'period'=>$in['period'],'d'=>DATABASE_NAME,'active'=>'1000']);

				//$payment->payment_invoice_stripe($_SESSION['u_id'],$stripe_inv['data'][0]['id']);

				$user_update = $db_users->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' ");
				while ($user_update->next()) {
					$update = $db_users->query("UPDATE user_info SET end_date='".$next_date."', is_trial='1' WHERE user_id='".$user_update->f('user_id')."' ");
				}
				/*if(defined('CHANGE_CARD') && CHANGE_CARD == 1){
					$info = $db_users->query("SELECT payment_fail_next,payment_fail FROM users WHERE database_name='".DATABASE_NAME."' LIMIT 1");
					if($info->f('payment_fail_next') > ($next_date - (11*24*60*60)) ){
						# there is no cow level
						$update = $db_users->query("UPDATE users SET next_date='".$info->f('payment_fail_next')."' WHERE database_name='".DATABASE_NAME."'");
					}
					$update = $db_users->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account changed credit card', `date`='".$time."' ");
					$db_users->query("DELETE FROM settings WHERE constant_name='CHANGE_CARD' ");
				}else{
					$update = $db_users->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account activated by customer', `date`='".$time."' ");
				}
				$update = $db_users->query("UPDATE users SET payment_fail_next='0',payment_fail='0'
						WHERE database_name='".DATABASE_NAME."'");*/
				
				$result['payed'] = true;
				unset($_SESSION['is_trial']);
				$first_paid = $db_users->field("SELECT first_paid FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' ");
				if(!$first_paid){
					$paid_update = $db_users->query("UPDATE users SET first_paid='".$time."'
																WHERE database_name='".DATABASE_NAME."'");
					
					$in['time']=$time;
					$in['next_date']=$next_date;
					create_reccuring_invoice($in);
					send_email_won_customer($_SESSION['u_id'], $in['period']);
				}else{
					send_email_update_subscription($_SESSION['u_id'], $in['pricing_plan_id'], $in['period'], $in['users']);
				}

				msg::success(gm('The payment process was successful'),"success");
			}catch(\Stripe\Error\Card $e){
				msg::notice($e->getMessage(),"notice");
			}
		}
	}else if($user_paid==1 && $user_stripe!='' && $in['old_stripe']){
		\Stripe\Stripe::setApiKey($config['stripe_api_key']);
		if($in['change_card']){
			try{
				$cu = \Stripe\Customer::retrieve($u_plan->f('stripe_cust_id'));
				$cu->source = $in['stripeToken']; 
				$cu->save();

				$inv_incoming=\Stripe\Invoice::upcoming(array("customer" => $user_stripe));
				$info = $db_users->query("SELECT payment_fail_next,payment_fail FROM users WHERE database_name='".DATABASE_NAME."' LIMIT 1");
				if($info->f('payment_fail_next') > ($inv_incoming['date'] - (11*24*60*60)) ){
					# there is no cow level
					$update = $db_users->query("UPDATE users SET next_date='".$info->f('payment_fail_next')."' WHERE database_name='".DATABASE_NAME."'");
				}
				$update = $db_users->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account changed credit card', `date`='".time()."' ");
				$db_users->query("DELETE FROM settings WHERE constant_name='CHANGE_CARD' ");

				msg::success(gm("Changes saved"),"success");
			}catch(\Stripe\Error\Card $e){
				msg::notice($e->getMessage(),"notice");
			}	
		}else{
			try{
				$cu = \Stripe\Customer::retrieve($u_plan->f('stripe_cust_id'));
				if(($in['user_disc'] != '0') && ($u_plan->f('stripe_discount_id') != $in['user_disc'])){
					$cu->coupon = $in['user_disc']; 
				}
				$cu->save();
				$subscription_items=\Stripe\SubscriptionItem::all(array("subscription" => $u_plan->f('stripe_subscr_id'),'limit'=>20));
				foreach($subscription_items['data'] as $key=>$value){
					if($key == 0){
						$si = \Stripe\SubscriptionItem::retrieve($value['id']);
						$si->plan=$stripe_plans[0]['plan'];
						$si->prorate=false;
						$si->quantity=1;
						$si->save();
					}else{
						$is_plan=false;
						foreach($stripe_plans as $key1=>$value1){
							if($value1['plan'] == $value['plan']['id']){
								$is_plan=true;
								if($value['plan']['id']=='98' || $value['plan']['id']=='298'){
									$si = \Stripe\SubscriptionItem::retrieve($value['id']);
									$si->prorate=false;
									$si->quantity=$value1['quantity'];
									$si->save();
								}
								break;
							}
						}
						if(!$is_plan){
							$si = \Stripe\SubscriptionItem::retrieve($value['id']);
							$si->delete();
						}	
					}		
				}
				foreach($stripe_plans as $key=>$value){
					if($key == 0){
						//nothing
					}else{
						$is_new_plan=true;
						foreach($subscription_items['data'] as $key1=>$value1){
							if($value1['plan']['id']==$value['plan']){
								$is_new_plan=false;
							}
						}
						if($is_new_plan){
							\Stripe\SubscriptionItem::create(array(
							  "subscription" => $u_plan->f('stripe_subscr_id'),
							  "plan" => $value['plan'],
							  "quantity" => $value['quantity'],
							  "prorate"	=> false
							));
						}		
					}
				}
				$subscription = $cu->subscriptions->retrieve($u_plan->f('stripe_subscr_id'));
				$subscription->prorate = false;
				$subscription->tax_percent=$plan_vat;
				$subscription->save();

				include_once('apps/settings/model/payment.php');
				$payment=new payment(); 
				$payment->pay_new($in);

				msg::success(gm('Account successfully updated'),"success");
			}catch(\Stripe\Error\Card $e){
				msg::notice($e->getMessage(),"notice");
			}
		}
			
	}

	$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

	$user_dd=array();
	for($i=$nr_users;$i<26;$i++){
		array_push($user_dd,array('id'=>$i,'name'=>$i==25 ? '25+':$i, 'disabled'=>$i< $nr_users? true: false ));
	}

	//$user_info = $db_users->query("SELECT * FROM user_info WHERE user_id='".$_SESSION['u_id']."' ");
	$user_info = $db_users->query("SELECT * FROM user_info WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	/*$user_crm = $db_users->query("SELECT * FROM customers WHERE database_name='".DATABASE_NAME."' ");
	$user_crm_a = $db_users->query("SELECT * FROM customer_addresses WHERE customer_id='".$user_crm->f('customer_id')."' AND billing='1' ");*/

	$aditional=array(
			array(
				'txt'		=> gm('CRM'),
				'price'		=> 20,
				'model'		=> 'crm_adv',
				'crm_adv'	=> false,
				'credential'=> 0
				),
			array(
				'txt'		=> gm('Products'),
				'price'		=> 10,
				'model'		=> 'products',
				'products'	=> false,
				'credential'=> 0
				),
			array(
				'txt'		=> gm('Quotes'),
				'price'		=> 10,
				'model'		=> 'quotes_adv',
				'quotes_adv'=>false,
				'credential'=> 0
				),
			array(
				'txt'		=> gm('Orders'),
				'price'		=> 10,
				'model'		=> 'orders',
				'orders'	=> false,
				'credential'=> 6
				),
			array(
				'txt'		=> gm('Stocks'),
				'price'		=> 30,
				'model'		=> 'stocks',
				'stocks'	=> false,
				'credential'=> 16
				),
			array(
				'txt'		=> gm('Cashregister'),
				'price'		=> 50,
				'model'		=> 'cashregister',
				'cashregister'	=> false,
				'credential'=> 15
				),
			array(
				'txt'		=> gm('Projects'),
				'price'		=> 20,
				'model'		=> 'projects',
				'projects'	=> false,
				'credential'=> 3
				),
			array(
				'txt'		=> gm('Interventions'),
				'price'		=> 20,
				'model'		=> 'intervention',
				'intervention'=>false,
				'credential'=> 13
				),
			array(
				'txt'		=> gm('Contrats'),
				'price'		=> 20,
				'model'		=> 'contracts',
				'contracts'	=> false,
				'credential'=> 11
				),
			array(
				'txt'		=> gm('SEPA'),
				'price'		=> 10,
				'model'		=> 'sepa',
				'sepa'		=> false,
				'credential'=> 0
				),
			array(
				'txt'		=> gm('Reports'),
				'price'		=> 10,
				'model'		=> 'report_adv',
				'report_adv'=> false,
				'credential'=> 0
				),
			array(
                'txt'       => gm('API and webhooks'),
                'price'     => 0,
                'model'     => 'api_webhooks',
                'api_webhooks'=> false,
                'credential'=> 0,
                ),
            array(
                'txt'       => gm('Market place'),
                'price'     => 0,
                'model'     => 'marketplace',
                'marketplace'=> false,
                'credential'=> 0,
                ),
            array(
				'txt'		=> gm('Purchase Orders'),
				'price'		=> 0,
				'model'		=> 'p_orders',
				'p_orders'	=> false,
				'credential'=> 14
				),
	);

	if($acc_type!='1'){
		$aditional[6]=array(
				'txt'		=> gm('Timetracker'),
				'price'		=> 10,
				'model'		=> 'timetracker',
				'timetracker'	=> false,
				'credential'=> 19
				);
	}

	if($u_plan->f('credentials_serial')!=''){
		$stored_aditional=unserialize($u_plan->f('credentials_serial'));
		foreach($aditional as $key=>$value){
			foreach($stored_aditional as $key1=>$value1){
				if($value['model'] == $value1['model'] && $value1[$value1['model']]=='1'){
					$aditional[$key][$value['model']]=true;
					break;
				}
			}
		}
	}
	
	/*array(
				'txt'		=> gm('E-commerce Site'),
				'price'		=> 50,
				'model'		=> 'site',
				'site'		=> false,
				'credential'=> 9
				)*/

	$reseller_hide=false;
	/*if($u_plan->f('accountant_id') && (is_null($new_plan_reduced) || $new_plan_reduced=='1')){
		$start_price=20;
	}else{
		$start_price=30;
		$aditional[0]['crm_adv']=true;
		$aditional[2]['quotes_adv']=true;
		$reseller_hide=true;
	}*/
	if($acc_type=='1'){
		$start_price=30;
		$aditional[0]['crm_adv']=true;
		$aditional[2]['quotes_adv']=true;
		$reseller_hide=true;
		if($user_info->f('is_trial')==1){
			foreach($aditional as $key=>$value){
				$aditional[$key]['disabled']=true;
			}
		}	
	}else{
		$modules_allowed=array('crm_adv','quotes_adv','sepa','timetracker');
		if($u_plan->f('accountant_id') && $new_plan_reduced=='1'){
			$start_price=20;
		}else{
			$start_price=29;
		}
		if($user_info->f('is_trial')==1){
			foreach($aditional as $key=>$value){
				if(!in_array($value['model'], $modules_allowed)){
					$aditional[$key]['disabled']=true;
				}
			}
		}	
	}

	//$in['period'] = 1;
	$in['period'] = '2'; // by default, anual 
	$period = $db->query("SELECT value FROM settings WHERE constant_name='PERIOD' AND module='payment' ");
	if($period->next()){
		$in['period'] = $period->f('value');
	}

	//$total_price+=($u_plan->f('time_users')*5);

	$vat_value=0;
	if(in_array(get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')), $eu_countries)){
		if(get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')) == 'BE'){
				$vat_value=21;
		}else{
			if(!$u_plan->f('valid_vat')){
				$vat_value=21;
			}
		}
	}

 $base_plan_id = $u_plan->f('base_plan_id');
 $custom_pricing_plans =['1','2','3','4','5','7','8'];
 $plans =['3','4','5'];
 if(in_array($selected_plan, $custom_pricing_plans)){
 	$total_price=$in['period']=='1'? $result['pricing_plans'][$selected_plan-1]['value'] : $result['pricing_plans'][$selected_plan-1]['value_annual'];
 	$nr_users= $result['pricing_plans'][$selected_plan-1]['nr_users'];
 	if(in_array($selected_plan, $plans )){
 		$aditional[1]['products']=true;
 	}
 }else{
 	if ($base_plan_id=='6'){
	 	$total_price=$start_price;
	 	$nr_users=2;
		foreach($aditional as $key=>$value){
			if($value[$value['model']]){
				$total_price+=$value['price'];
			}
		}
	}else{
		$total_price=$in['period']=='1'? $result['pricing_plans'][$base_plan_id-1]['value'] : $result['pricing_plans'][$base_plan_id-1]['value_annual'];
		//console::log($total_price);
		$nr_users = $result['pricing_plans'][$base_plan_id-1]['nr_users'];
		foreach($result['pricing_plans'][$base_plan_id-1]['additionals'] as $key => $value){
				$aditional[$value][$aditional[$value]['model']]=true;
			}
		foreach($aditional as $key=>$value){
			if($value[$value['model']] && !in_array($key, $result['pricing_plans'][$base_plan_id-1]['additionals'])){
				//console::log($value['price']);
				$total_price+=$value['price'];							
			}
		}
	}
 }

	if($in['period']=='1'){
		$total_price+=(($u_plan->f('users')-$nr_users)*12);
	}else{
		//$total_price-=$total_price*10/100;
		$total_price+=(($u_plan->f('users')-$nr_users)*10);
	}	

	//$stripe_locale=$db_users->field("SELECT lang.code FROM lang INNER JOIN users ON users.lang_id=lang.lang_id WHERE user_id='".$_SESSION['u_id']."' ");
	$stripe_locale=$db_users->field("SELECT lang.code FROM lang INNER JOIN users ON users.lang_id=lang.lang_id WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);

	$stripe_discount=array();
	$discount_percent=0;
	$discount_amount=0;
	if($user_discount_id != '0' && $user_discount_id != ''){
		\Stripe\Stripe::setApiKey($config['stripe_api_key']);
		$user_discount=\Stripe\Coupon::retrieve($user_discount_id);

		$usr_dsc=array();
		$usr_dsc['id']=$user_discount['id'];
		if($user_discount['amount_off']==null){
			$usr_dsc['name']=(string)$user_discount['percent_off'];
			$discount_percent=1;
			if($user_stripe!=''){
				$total_price=$total_price-($total_price*$user_discount['percent_off']/100);
			}	
		}else{
			$total_price=$total_price-$user_discount['amount_off']/100;
			$usr_dsc['name']=(string)$user_discount['amount_off']/100;
			$discount_amount=1;
		}
		array_push($stripe_discount, $usr_dsc);
	}

	$days_left = ceil(($user_info->f('end_date') - time())/(24*60*60));
	
	//$plan_text = gm('('.$u_plan->f('users').' users and '.$u_plan->f('time_users').' timesheets users)');
	if($easyinvoice=='1'){
		$plan_text = '';
		$TOTAL_LAST=display_number(1);
		$total_price=1;
		$result['TYPE']						= ($u_plan->f('payed') == 2 ? gm('canceled') : ($u_plan->f('payed') == 1 || $u_plan->f('payed') == 3 || $user_info->f('is_trial')==1 ? gm('active') : gm('trial'))).' '.$plan_text;
	}else{
		$plan_text = gm('('.$u_plan->f('users').' users)');
		//$TOTAL_LAST=$u_plan->f('payed') == 0 ? display_number(0) : display_number($u_plan->f('last_payment'));
		$TOTAL_LAST=$u_plan->f('payed') == 2 ? display_number(0) :($u_plan->f('payed') == 0 ? display_number(0) : display_number($total_price));
		$result['TYPE']						= ($u_plan->f('payed') == 2 ? gm('canceled') : ($u_plan->f('payed') == 1 || $user_info->f('is_trial')==1 ? gm('active') : gm('trial'))).' '.$plan_text;
	}	
	

	$user_p = $db_users->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE credentials!='8' AND database_name='".DATABASE_NAME."' AND users.active='1' AND ( user_meta.value !=  '1' OR user_meta.value IS NULL ) ");
	$user_t = $db_users->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE credentials='8' AND database_name='".DATABASE_NAME."' AND users.active='1' AND ( user_meta.value !=  '1' OR user_meta.value IS NULL ) ");
	$next_charge = $in['period']=='1' ? ($total_price) : ($total_price*12 );

	$result['TOTAL_LAST']				= $TOTAL_LAST;
	$result['USER_DD']					= $user_dd;
	$result['users']					= $u_plan->f('users');
	$result['users_t']					= $u_plan->f('time_users');
	$result['actual_users']				= $user_p;
	$result['actual_users_t']			= $user_t;			
	$result['aditional']				= $aditional;
	$result['total_price']				= display_number($total_price);
	//$result['stripe_val']				= $total_price+$total_price*$vat_value/100;
	$result['stripe_val']				= $in['period']=='1' ? ($total_price+$total_price*$vat_value/100) : (($total_price+$total_price*$vat_value/100)*12 );
	//$result['stripe_vat']				= $total_price*$vat_value/100;
	$result['stripe_vat']				=  $in['period']=='1' ? $total_price*$vat_value/100 : (($total_price*$vat_value/100) *12);
	$result['start_price']				= $start_price;
	$result['COUNTRY_CODE']				= get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID'));
	$result['VALID_VAT']				= $u_plan->f('valid_vat');
	$result['vat_value']				= $vat_value == 0 ? display_number(0) : display_number($total_price*$vat_value/100);
	$result['PAYED']					= $u_plan->f('payed')? $u_plan->f('payed') : 0;
	$result['is_vat']					= $vat_value == 0 ? false : true;
	//$result['TYPE']						= ($u_plan->f('payed') == 2 ? 'canceled' : ($u_plan->f('payed') == 1 ? 'active' : ($u_plan->f('plan') == 1 ? 'active' : 'trial'))).' '.$plan_text;
	$result['HIDE_NEXT_CHARGE']			= $u_plan->f('plan') == 1 ? false :( $user_info->f('is_trial') == 0 ? false : true);
	$result['HIDE_NEXT_CHARGE2']		= $u_plan->f('plan') == 1 ? false :( $user_info->f('is_trial') == 0 ? false : ($u_plan->f('valid_vat') ? false : (in_array(get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')), $eu_countries) ? (get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')) == 'BE' ? false : true) : false) ));
	$result['NEXT_CHARGE_DATE']			= date(ACCOUNT_DATE_FORMAT,$user_info->f('end_date'));
	//$result['CHARGE_RATE']				= $u_plan->f('payed') == 2 ? display_number(0) :($u_plan->f('payed') == 0 ? display_number(0) : display_number($total_price));
	$result['CHARGE_RATE']				= $u_plan->f('payed') == 2 ? display_number(0) :($u_plan->f('payed') == 0 ? display_number(0) : display_number($next_charge));
	$result['HIDE_VAT_FROM_T']			= in_array(get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')), $eu_countries) ? (get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')) == 'BE' ? true : ($u_plan->f('valid_vat') ? false : true )) : false;
	$result['VAT_P']					= display_number($total_price*21/100);
	$result['DAYS_LEFT']				= $days_left;
	$result['HIDE_DAYS']				= $u_plan->f('plan') == 1 ? false : true;
	$result['HIDE_CANCEL']				= $u_plan->f('payed') == 1 ? true : false;
	$result['INVOICING_NAME']			= utf8_encode($u_plan->f('ACCOUNT_COMPANY'));
	$result['ACCOUNT_VAT_NUMBER']			= $u_plan->f('valid_vat') ? $u_plan->f('valid_vat') : (ACCOUNT_VAT_NUMBER ? ACCOUNT_VAT_NUMBER : gm('no VAT'));
	$result['is_eu_country']			= in_array(get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')), $eu_countries) ? true : false;
	$result['stripe_user']				= (!$user_paid || $user_paid==2 || $user_active==0) ? '1' : (($user_paid==1 && $user_stripe!='' && $user_stripe_subscr!='') ? '2' : '0');
	$result['reseller_hide']			= $reseller_hide;
	$result['user_disc_dd']				= $stripe_discount;
	$result['user_disc']				= ($user_discount_id=='0' || $user_discount_id=='') ? '0' : $user_discount_id; 
	$result['user_discount']			= ($user_discount_id=='0' || $user_discount_id=='') ? false : true;
	$result['stripe_discount_type']		= ($user_discount_id=='0' || $user_discount_id=='') ? '' : ($discount_percent==1 ? 'percent' : 'amount');
	$result['discount_type']			= ($user_discount_id=='0' || $user_discount_id=='') ? '' : ($discount_percent==1 ? '%' : '&euro;'); 
	$result['stripe_pub_key']			= $config['stripe_pub_key'];
	$result['stripe_locale']			= $stripe_locale;
	$result['BUY_TEXT']					= $u_plan->f('payed') == 1 && $user_stripe ? gm('Upgrade') : gm('Buy');
	$result['PAY_TEXT']					= gm('Update payment');
	$result['disabled_adv']				= $new_plan_reduced=='0' ? true : false;
	$result['period']					= $in['period'] == '1' ? false : true;
	$result['acc_type']				= $acc_type;
	$result['no_pay']					= $user_info->f('is_trial')==1 ? true : false;
	$result['easyinvoice']			= $easyinvoice=='1' ? true : false;
	$result['is_trial']				= !$user_info->f('is_trial') ? true : false;
	//$result['is_active_payed']				= ($user_info->f('is_trial') && $u_plan->f('payed') == 1 &&  $u_plan->f('active') == 1)? true : false;
	$result['is_active_payed']				= ($u_plan->f('active') == 1)? true : false;
	//$result['subscription_active']			= ( $u_plan->f('payed') == 1 &&  $u_plan->f('next_date') > time() )? true : false;
	$result['stripe_cust_id']				= $u_plan->f('stripe_cust_id');
	$result['can_change_card']				= $u_plan->f('stripe_cust_id') ? true : false;
	$result['button_text']					= gm("Pay").' '.place_currency(display_number($result['stripe_val']));


	return json_out($result);
?>