<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!empty($in['start_date_js'])){
	$in['start_date'] =strtotime($in['start_date_js']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['stop_date_js'])){
	$in['stop_date'] =strtotime($in['stop_date_js']);
	$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}


global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db = new sqldb($db_config);
$db2 = new sqldb();

$l_r = ROW_PER_PAGE;

$const['ACCOUNT_INVOICE_PDF_FORMAT'] = $db->field("SELECT value FROM settings WHERE constant_name ='ACCOUNT_INVOICE_PDF_FORMAT' ");

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
$arguments_o='';
$arguments_s='';

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}


$arguments='';
$filter = "WHERE buyer_id IN ( SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' ) AND tblinvoice.status='1' AND tblinvoice.type<>'2' ";
$filter_link = 'block';
$order_by = " ORDER BY t_date DESC, iii DESC ";

if(empty($in['archived'])){
	$filter.= " AND f_archived='0' ";
}else{
	$filter.= " AND f_archived='1' ";
	$arguments.="&archived=".$in['archived'];
}

if(!empty($in['filter'])){
	$arguments.="&filter=".$in['filter'];
}
if(!empty($in['c_invoice_id'])){
	$filter.=" and (tblinvoice.c_invoice_id = '".$in['c_invoice_id']."')";
	$arguments.="&c_invoice_id=".$in['c_invoice_id'];

}
if(!empty($in['search'])){
	$filter.=" AND (tblinvoice.serial_number LIKE '%".$in['search']."%' OR tblinvoice.buyer_name LIKE '%".$in['search']."%' OR tblinvoice.our_ref LIKE '%".$in['search']."%' )";
	$arguments_s.="&search=".$in['search'];
}
if(!empty($in['order_by'])){
	$order = " ASC ";
	if($in['desc']){
		$order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
	$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	// $view_list->assign(array(
	// 	'on_'.strtolower($in['order_by']) 	=> $in['desc'] ? 'on_asc' : 'on_desc',
	// ));
}

if(!empty($in['start_date']) && !empty($in['stop_date'])){
	list($d, $m, $y) = explode($spliter,  $in['start_date']);
	$start_date= mktime(0, 0, 0, $m, $d, $y);
	list($d, $m, $y) = explode($spliter,  $in['stop_date']);
	$stop_date= mktime(23, 59, 59, $m, $d, $y);
	$filter.=" and tblinvoice.invoice_date BETWEEN '".$start_date."' and '".$stop_date."' ";
	$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
	// $view_list->assign('start_date',$in['start_date']);
	// $view_list->assign('stop_date',$in['stop_date']);
	$filter_link = 'none';
}
else if(!empty($in['start_date'])){
	list($d, $m, $y) = explode($spliter,  $in['start_date']);
	$start_date= mktime(0, 0, 0, $m, $d, $y);
	$stop_date= mktime(23, 59, 59, $m, $d, $y);
//	$filter.=" and tblinvoice.invoice_date BETWEEN '".$start_date."' and '".$stop_date."' ";
	$filter.=" and cast(tblinvoice.invoice_date as signed) > ".$start_date." ";
	$arguments.="&start_date=".$in['start_date'];
	// $view_list->assign('start_date',$in['start_date']);
}
else if(!empty($in['stop_date'])){
	list($d, $m, $y) = explode($spliter,  $in['stop_date']);
	$start_date= mktime(0, 0, 0, $m, $d, $y);
	$stop_date= mktime(23, 59, 59, $m, $d, $y);
//	$filter.=" and tblinvoice.invoice_date BETWEEN '".$start_date."' and '".$stop_date."' ";
	$filter.=" and cast(tblinvoice.invoice_date as signed) < ".$stop_date." ";
	$arguments.="&stop_date=".$in['stop_date'];
	// $view_list->assign('stop_date',$in['stop_date']);
	$filter_link = 'none';
}
$arguments = $arguments.$arguments_s;
$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;
//$db->query("SELECT tblinvoice.*, CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) as iii

$tblinvoice = $db->query("SELECT tblinvoice.*, serial_number as iii, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date
			FROM tblinvoice ".$filter.$order_by );

$max_rows=$tblinvoice->records_count();
$result = array('list'=>array(),'max_rows'=>$max_rows);
$tblinvoice->move_to($offset*$l_r);
$j=0;
$color = '';
while($tblinvoice->move_next() && $j<$l_r){
	$status='';
	$status_title='';
	$type='';
	$type_title='';
	$color='';
	switch ($tblinvoice->f('type')){
		case '0':
			$type = 'regular_invoice';
			$type_title = gm('Regular invoice');
			break;
		case '1':
			$type = 'pro-forma_invoice';
			$type_title = gm('Proforma invoice');
			break;
		case '2':
			$type = 'credit_invoice';
			$type_title = gm('Credit Invoice');
			break;
	}


	switch ($tblinvoice->f('status')){
		case '0':
				if($tblinvoice->f('paid') == '2' ){
					// $status = 'Partially Paid';
					$status='partial';
					$status_title= gm('Partially Paid');
					$color = '';
				}
					if($tblinvoice->f('due_date') < time()){
					// $status = 'Late';
					$status = 'late';
					$status_title= gm('Late');
					$color = 'late';

			}
			break;
		case '1':
			// $status = 'Fully Paid';
			if($tblinvoice->f('type') != '2' ){
				$color = '';
				$status='paid';
				$status_title= gm('Paid');
			}
			break;
	}
	if($tblinvoice->f('sent') == '0'){
				// $status = 'Draft';
					if($tblinvoice->f('type')=='2'){
						$type='credit_draft';
						$type_title = gm('Credit Invoice');
					}else{
						$type = 'draft_invoice';
						$type_title = gm('Draft');
					}
					$status='';
					$status_title= '';
					$color='';
				}

	if($tblinvoice->f('created_by') == 'System'){
		$color .= ' recurring';
	}

	$tblinvoice2 = $db->query("SELECT id,c_invoice_id from tblinvoice where c_invoice_id='".$tblinvoice->f('id')."'");
	$pim_lang = $db2->field("SELECT lang_id FROM pim_lang WHERE active=1 AND sort_order='1' ");
	if(!$pim_lang){
		$pim_lang = 1;
	}
	$result['list'][]=array(
		'pdf_link'    		 		=> 'index.php?do=settings-invoice_print&id='.$tblinvoice->f('id').'&lid='.$pim_lang.'&type='.$const['ACCOUNT_INVOICE_PDF_FORMAT'],
		// 'type'						=> $type,
		// 'type_title'				=> $type_title,
		// 'status'					=> $status,
		// 'status_title'				=> $status_title,
		// 'invoice_property'			=> $color,
		'amount'					=> place_currency(display_number($tblinvoice->f('amount'))),
		'amount_vat'				=> place_currency(display_number($tblinvoice->f('amount_vat'))),
		'our_reference'				=> $tblinvoice->f('our_ref'),
		'id'                		=> $tblinvoice->f('id'),
		// 's_number'		  		   	=> $tblinvoice->f('iii') ? $tblinvoice->f('iii') : '',
		'created'           		=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('invoice_date')),
		// 'seller_name'       		=> $tblinvoice->f('seller_name'),
		// 'buyer_name'        		=> $tblinvoice->f('buyer_name'),
		// 'hide_paid'					=> $tblinvoice->f('status') ? 'hide' : '',
		// 'text'						=> $tblinvoice->f('created_by') == 'System' ? gm('Created from a recurring invoice') : '',
	);

	// $view_list->loop('invoice_row');
	$j++;

}

$result['lr'] = $l_r;
json_out($result);