<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');
	global $config;

	global $database_config;

	$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
	);
	$db_users = new sqldb($database_users);
	$db = new sqldb();

	$result=array();

	if(isset($in['code']) && !empty($in['code'])){
		$auth_code= $in['code'];
		$ch = curl_init();

		$data=array(
			'client_id'		 	=> $config['exact_client_id'],
			'client_secret' 	=> $config['exact_client_secret'],
			'grant_type' 		=> 'authorization_code',
			'code'				=> $in['code'],
			'redirect_uri'		=> $config['exact_redirect_url'],
			);

    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $config['exact_baseurl'].'/api/oauth2/token');
        	
        $put = curl_exec($ch);
	    $info = curl_getinfo($ch);

	    if($info['http_code']>300 || $info['http_code']==0){
    		msg::error(gm("Activation process not finished succesfully"),"error");
	    }else{
	    	$token=json_decode($put);
	    	$app = $db->query("SELECT * FROM apps WHERE name='Exact online' AND type='main' AND main_app_id='0' ");
			if($app->next()){
				$db->query("UPDATE apps SET active = '1' WHERE app_id= '".$app->f('app_id')."'");
				$access_token = $db->field("SELECT app_id FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='access_token' ");
				if(!$access_token){
					$db->query("INSERT INTO apps SET main_app_id='".$app->f('app_id')."', type='access_token', api='".$token->access_token."' ");
				}else{
					$db->query("UPDATE apps SET api ='".$token->access_token."'
	    			 								 WHERE main_app_id='".$app->f('app_id')."' AND type='access_token'");	
				}

				$refresh_token = $db->field("SELECT app_id FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='refresh_token' ");
				if(!$refresh_token){
					$db->query("INSERT INTO apps SET main_app_id='".$app->f('app_id')."', type='refresh_token', api='".$token->refresh_token."' ");
				}else{
					$db->query("UPDATE apps SET api ='".$token->refresh_token."'
	    			 								 WHERE main_app_id='".$app->f('app_id')."' AND type='refresh_token'");	
				}

				$authorizationcode = $db->field("SELECT app_id FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='authorizationcode' ");
				if(!$authorizationcode){
					$db->query("INSERT INTO apps SET main_app_id='".$app->f('app_id')."', type='authorizationcode', api='".$auth_code."' ");
				}else{
					$db->query("UPDATE apps SET api ='".$auth_code."'
	    			 								 WHERE main_app_id='".$app->f('app_id')."' AND type='authorizationcode'");	
				}

				$expires_in = $db->field("SELECT app_id FROM apps WHERE main_app_id='".$app->f('app_id')."' AND type='expires_in' ");
				$expires_time = time() + $token->expires_in;
				if(!$expires_in){
					$db->query("INSERT INTO apps SET main_app_id='".$app->f('app_id')."', type='expires_in', api='".$expires_time."' ");
				}else{
					$db->query("UPDATE apps SET api ='".$expires_time."'
	    			 								 WHERE main_app_id='".$app->f('app_id')."' AND type='expires_in'");	
				}
	    		 										 								    
			}

	    	msg::success(gm("Activation process finished succesfully"),"success");
	    	$result['redirect_link']=$config['site_url']."settings/?tab=apps&eoconn=1";
	    	$result['is_exact_online']=true;

	        mark_invoices_as_exported('exact_online', true);
	    }
				
	}else{
		msg::error(gm("Activation process not finished succesfully"),"error");
	}


return json_out($result);
?>