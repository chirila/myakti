<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$view = new at(ark::$viewpath.'multi_email.html');
global $database_config,$config;
// $view = new at();
$database_2 = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);

$user_db = new sqldb($database_2);
//$user = $user_db->field("SELECT username FROM users WHERE user_id='".$_SESSION['u_id']."'");
$user = $user_db->field("SELECT username FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
$site_url = $config['site_url'];
$crypt = base64_encode($user.' '.$in['email'].' '.$in['lang_id'].' '.$in['group_id']);
$crypt = strrev($crypt);

if($in['lang_id']==1){
	$mail_id = 'mail_content_nl';
	$subject_id = 'subject_nl';
}elseif($in['lang_id']==2){
	$mail_id = 'mail_content_en';
	$subject_id = 'subject_en';
}elseif($in['lang_id']==3){
	$mail_id = 'mail_content_fr';
	$subject_id = 'subject_fr';
}elseif($in['lang_id']==4){
	$mail_id = 'mail_content_ge';
	$subject_id = 'subject_ge';
}



$mail_content = $user_db->field("SELECT ".$mail_id ." FROM mail_content WHERE mail_content_id='1'");
$mail_subject = $user_db->field("SELECT ".$subject_id ." FROM mail_content WHERE mail_content_id='1'");


$in['time'] =mktime();
$timestamp = strtotime("+48 hours",$in['time']);
//$time =  $user_db->field("INSERT INTO user_email SET user_id='".$_SESSION['u_id']."', crypt='".$crypt."', time_email='".$timestamp."' ");
$time =  $user_db->insert("INSERT INTO user_email SET user_id= :user_id, crypt= :crypt, time_email= :time_email ",['user_id'=>$_SESSION['u_id'],'crypt'=>$crypt,'time_email'=>$timestamp]);


/*$duo= $crypt.';'.$user.';'.$in['email'].';'.$in['lang_id'].';'.$in['group_id'];*/
$duo=$crypt;

//$site = $site_url.'create_account/'.$duo;
$site = $site_url.'index.php?do=auth-create_account&duo='.$duo;
$view->assign(array(
    'link'		 	=> $site,
    'email'			=> $in['email'],
    'site_url'		=> $site_url,
    'content'		=> $mail_content
));

return $view->fetch();
