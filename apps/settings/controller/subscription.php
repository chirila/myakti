<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
	$result=array();
	ark::loadLibraries(array('stripe/init'));
	global $config,$site_url,$database_config;
	//$view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.$config['cache_version']);

	$db_config = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$dbu = new sqldb($db_config);

	$db = new sqldb();
	//$tips = $page_tip->getTip('settings-pricing_plan');

	$acc_type = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_TYPE'");
	$_SESSION['acc_type'] = $acc_type;

	$final_plan=0;
	$plan_webshop=1;
	if($in['new_plan_webshop']){
		$plan_webshop=1+($in['new_plan_webshop']/100);
	}
	//look in plan_price
	switch($in['plan']){
		case 3:
			$final_plan=(30*$in['period']+$in['how_many'])*$plan_webshop;
			break;
		case 4:
			$final_plan=(20*$in['period']+$in['how_many'])*$plan_webshop;
			break;
		default:
			$final_plan=5*$in['period']+$in['how_many'];
	}
	$plan_vat=0;
	if($in['stripe_vat']){
		$plan_vat=21;
	}

	//$user_paid=$dbu->field("SELECT payed FROM users WHERE user_id='".$_SESSION['u_id']."'");
	$user_paid=$dbu->field("SELECT payed FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
	//$user_stripe=$dbu->field("SELECT stripe_cust_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
	$user_stripe=$dbu->field("SELECT stripe_cust_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
	//$user_stripe_subscr=$dbu->field("SELECT stripe_subscr_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
	$user_stripe_subscr=$dbu->field("SELECT stripe_subscr_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
	//$user_active=$dbu->field("SELECT active FROM users WHERE user_id='".$_SESSION['u_id']."'");
	$user_active=$dbu->field("SELECT active FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);

	if(((!$user_paid || $user_paid==2 || $user_paid==3 || $user_active==0) && $in['stripeToken']) || (!$user_stripe && $user_paid==1 && $in['stripeToken']) || ($user_stripe && $user_paid==1 && $user_stripe_subscr=='' && $in['stripeToken'])){
		\Stripe\Stripe::setApiKey($config['stripe_api_key']);

		//$user_data=$dbu->query("SELECT first_name,last_name,ACCOUNT_COMPANY,stripe_cust_id,stripe_subscr_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$user_data=$dbu->query("SELECT first_name,last_name,ACCOUNT_COMPANY,stripe_cust_id,stripe_subscr_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
		if($user_data->f('stripe_cust_id')!='' && $user_data->f('stripe_subscr_id')==''){
			try{
				$stripe_date=time();
				$cu = \Stripe\Customer::retrieve($user_data->f('stripe_cust_id'));
				$cu->source = $in['stripeToken'];
				$cu->email = $in['stripeEmail'];
				if($in['user_disc'] != '0'){
					$cu->coupon = $in['user_disc'];
				}
				$cu->save();
				$cu = \Stripe\Customer::retrieve($user_data->f('stripe_cust_id'));
				$subscription=$cu->subscriptions->create(array(
					"items" => array(array("plan"=>$final_plan,"quantity"=>1)), 
					"tax_percent" => $plan_vat
				));
				//$dbu->query("UPDATE users SET stripe_subscr_id='".$subscription['id']."' WHERE user_id='".$_SESSION['u_id']."'");
				$dbu->query("UPDATE users SET stripe_subscr_id= :stripe_subscr_id WHERE user_id= :user_id",['stripe_subscr_id'=>$subscription['id'],'user_id'=>$_SESSION['u_id']]);
				$subscription_update = $cu->subscriptions->retrieve($subscription['id']);
				$subscription_update->prorate = false;
				$subscription_update->save();
				$stripe_inv=\Stripe\Invoice::all(array("customer" => $user_data->f('stripe_cust_id'),"date" => array("gte"=> $stripe_date)));

				include_once('apps/settings/model/payment.php');
				$payment=new payment(); 
				$payment->pay($in);
					
				$inv_incoming=\Stripe\Invoice::upcoming(array("customer" => $user_data->f('stripe_cust_id')));
				$next_date = $inv_incoming['date'];
				//$next_date = mktime(0,0,1,date('n')+1,date('j'),date('Y'));
				$time = time();
				$update = $dbu->query("UPDATE users SET payed='1', active ='1', pay_id='".$stripe_inv['data'][0]['id']."', next_date='".$next_date."', alias='".$_SESSION['u_id']."'
																WHERE database_name='".DATABASE_NAME."' AND active!='1000'");
				/*$dbu->query("UPDATE users SET payed='1', pay_id='".$stripe_inv['data'][0]['id']."', next_date='".$next_date."', alias='".$_SESSION['u_id']."'
																WHERE database_name='".DATABASE_NAME."' AND active='1000'");*/
				$dbu->query("UPDATE users SET payed= :payed, pay_id= :pay_id, next_date= :next_date, alias= :alias
																WHERE database_name= :d AND active= :active",['payed'=>'1','pay_id'=>$stripe_inv['data'][0]['id'],'next_date'=>$next_date,'alias'=>$_SESSION['u_id'],'d'=>DATABASE_NAME,'active'=>'1000']);																
				$payment->payment_invoice_stripe($_SESSION['u_id'],$stripe_inv['data'][0]['id']);
				
				$user_update = $dbu->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' ");
				while ($user_update->next()) {
					$update = $dbu->query("UPDATE user_info SET end_date='".$next_date."', is_trial='1' WHERE user_id='".$user_update->f('user_id')."' ");
				}
				if(defined('CHANGE_CARD') && CHANGE_CARD == 1){
					$info = $dbu->query("SELECT payment_fail_next,payment_fail FROM users WHERE database_name='".DATABASE_NAME."' LIMIT 1");
					if($info->f('payment_fail_next') > ($next_date - (11*24*60*60)) ){
						# there is no cow level
						$update = $dbu->query("UPDATE users SET next_date='".$info->f('payment_fail_next')."' WHERE database_name='".DATABASE_NAME."'");
					}
					$update = $dbu->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account changed credit card', `date`='".$time."' ");
					$dbu->query("DELETE FROM settings WHERE constant_name='CHANGE_CARD' ");
				}else{
					$update = $dbu->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account activated by customer', `date`='".$time."' ");
				}
				$update = $dbu->query("UPDATE users SET payment_fail_next='0',payment_fail='0'
																WHERE database_name='".DATABASE_NAME."'");
				
				$result['payed'] = true;
				unset($_SESSION['is_trial']);
				$first_paid = $dbu->field("SELECT first_paid FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' ");
				if(!$first_paid){
					$paid_update = $dbu->query("UPDATE users SET first_paid='".$time."'
																WHERE database_name='".DATABASE_NAME."'");
				}
				
				msg::success(gm('The payment process was successful'),"success");
			}catch(\Stripe\Error\Card $e){
				msg::notice($e->getMessage(),"notice");
			}
		}else{
			try{
				$stripe_date=time();
				if($in['user_disc'] != '0'){
					$new_cust=\Stripe\Customer::create(array(
					  "description" => $user_data->f('ACCOUNT_COMPANY').'-'.$user_data->f('first_name').' '.$user_data->f('last_name'),
					  "source" => $in['stripeToken'],
					  "email" => $in['stripeEmail'], 
					  "coupon" => $in['user_disc'],
					));
				}else{
					$new_cust=\Stripe\Customer::create(array(
					  "description" => $user_data->f('ACCOUNT_COMPANY').'-'.$user_data->f('first_name').' '.$user_data->f('last_name'),
					  "source" => $in['stripeToken'],
					  "email" => $in['stripeEmail'], 
					));
				}
				//$dbu->query("UPDATE users SET stripe_cust_id='".$new_cust['id']."' WHERE user_id='".$_SESSION['u_id']."'");
				$dbu->query("UPDATE users SET stripe_cust_id= :stripe_cust_id WHERE user_id= :user_id",['stripe_cust_id'=>$new_cust['id'],'user_id'=>$_SESSION['u_id']]);
				$cu = \Stripe\Customer::retrieve($new_cust['id']);
				$subscription=$cu->subscriptions->create(array(
					"items" => array(array("plan"=>$final_plan,"quantity"=>1)), 
					"tax_percent" => $plan_vat
				));
				//$dbu->query("UPDATE users SET stripe_subscr_id='".$subscription['id']."' WHERE user_id='".$_SESSION['u_id']."'");
				$dbu->query("UPDATE users SET stripe_subscr_id= :stripe_subscr_id WHERE user_id= :user_id",['stripe_subscr_id'=>$subscription['id'],'user_id'=>$_SESSION['u_id']]);
				$subscription_update = $cu->subscriptions->retrieve($subscription['id']);
				$subscription_update->prorate = false;
				$subscription_update->save();
				$stripe_inv=\Stripe\Invoice::all(array("customer" => $new_cust['id'],"date" => array("gte"=> $stripe_date)));

				include_once('apps/settings/model/payment.php');
				$payment=new payment(); 
				$payment->pay($in);
					
				$inv_incoming=\Stripe\Invoice::upcoming(array("customer" => $new_cust['id']));
				$next_date = $inv_incoming['date'];
				//$next_date = mktime(0,0,1,date('n')+1,date('j'),date('Y'));
				$time = time();
				$update = $dbu->query("UPDATE users SET payed='1', active ='1', pay_id='".$stripe_inv['data'][0]['id']."', next_date='".$next_date."', alias='".$_SESSION['u_id']."'
																WHERE database_name='".DATABASE_NAME."' AND active!='1000'");
				/*$dbu->query("UPDATE users SET payed='1', pay_id='".$stripe_inv['data'][0]['id']."', next_date='".$next_date."', alias='".$_SESSION['u_id']."'
																WHERE database_name='".DATABASE_NAME."' AND active='1000'");*/
				$dbu->query("UPDATE users SET payed= :payed, pay_id= :pay_id, next_date= :next_date, alias= :alias
																WHERE database_name= :d AND active= :active",['payed'=>'1','pay_id'=>$stripe_inv['data'][0]['id'],'next_date'=>$next_date,'alias'=>$_SESSION['u_id'],'d'=>DATABASE_NAME,'active'=>'1000']);																
				$payment->payment_invoice_stripe($_SESSION['u_id'],$stripe_inv['data'][0]['id']);

				$user_update = $dbu->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."' ");
				while ($user_update->next()) {
					$update = $dbu->query("UPDATE user_info SET end_date='".$next_date."', is_trial='1' WHERE user_id='".$user_update->f('user_id')."' ");
				}
				if(defined('CHANGE_CARD') && CHANGE_CARD == 1){
					$info = $dbu->query("SELECT payment_fail_next,payment_fail FROM users WHERE database_name='".DATABASE_NAME."' LIMIT 1");
					if($info->f('payment_fail_next') > ($next_date - (11*24*60*60)) ){
						# there is no cow level
						$update = $dbu->query("UPDATE users SET next_date='".$info->f('payment_fail_next')."' WHERE database_name='".DATABASE_NAME."'");
					}
					$update = $dbu->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account changed credit card', `date`='".$time."' ");
					$dbu->query("DELETE FROM settings WHERE constant_name='CHANGE_CARD' ");
				}else{
					$update = $dbu->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account activated by customer', `date`='".$time."' ");
				}
				$update = $dbu->query("UPDATE users SET payment_fail_next='0',payment_fail='0'
																WHERE database_name='".DATABASE_NAME."'");		
				$result['payed'] = true;
				unset($_SESSION['is_trial']);
				$first_paid = $dbu->field("SELECT first_paid FROM users WHERE database_name='".DATABASE_NAME."' AND main_user_id='0' ");
				if(!$first_paid){
					$paid_update = $dbu->query("UPDATE users SET first_paid='".$time."'
																WHERE database_name='".DATABASE_NAME."'");
				}

				msg::success(gm('The payment process was successful'),"success");
			}catch(\Stripe\Error\Card $e){
				msg::notice($e->getMessage(),"notice");
			}
		}
	}else if($user_paid==1 && $user_stripe!='' && $in['old_stripe']){
		\Stripe\Stripe::setApiKey($config['stripe_api_key']);
		//$user_data=$dbu->query("SELECT first_name,last_name,ACCOUNT_COMPANY,stripe_cust_id,stripe_subscr_id,stripe_discount_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$user_data=$dbu->query("SELECT first_name,last_name,ACCOUNT_COMPANY,stripe_cust_id,stripe_subscr_id,stripe_discount_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
		if($in['change_card']){
			try{
				$cu = \Stripe\Customer::retrieve($user_data->f('stripe_cust_id'));
				$cu->source = $in['stripeToken']; 
				$cu->save();

				$inv_incoming=\Stripe\Invoice::upcoming(array("customer" => $user_stripe));
				$info = $dbu->query("SELECT payment_fail_next,payment_fail FROM users WHERE database_name='".DATABASE_NAME."' LIMIT 1");
				if($info->f('payment_fail_next') > ($inv_incoming['date'] - (11*24*60*60)) ){
					# there is no cow level
					$update = $dbu->query("UPDATE users SET next_date='".$info->f('payment_fail_next')."' WHERE database_name='".DATABASE_NAME."'");
				}
				$update = $dbu->query("INSERT INTO `user_logs` SET `database_name`='".DATABASE_NAME."', `action`='Account changed credit card', `date`='".time()."' ");
				$dbu->query("DELETE FROM settings WHERE constant_name='CHANGE_CARD' ");

				msg::success(gm("Changes saved"),"success");
			}catch(\Stripe\Error\Card $e){
				msg::notice($e->getMessage(),"notice");
			}	
		}else{
			try{
				$cu = \Stripe\Customer::retrieve($user_data->f('stripe_cust_id'));
				if(($in['user_disc'] != '0') && ($user_data->f('stripe_discount_id') != $in['user_disc'])){
					$cu->coupon = $in['user_disc']; 
					$cu->save();
				}
				$subscription_items=\Stripe\SubscriptionItem::all(array("subscription" => $user_data->f('stripe_subscr_id')));
				if($subscription_items['data'][0]['plan']['id'] != $final_plan){
					$si = \Stripe\SubscriptionItem::retrieve($subscription_items['data'][0]['id']);
					$si->plan=$final_plan;
					$si->prorate=false;
					$si->quantity=1;
					$si->save();
				}	
				$subscription = $cu->subscriptions->retrieve($user_data->f('stripe_subscr_id'));
				$subscription->prorate = false;
				$subscription->tax_percent=$plan_vat;
				$subscription->save();

				include_once('apps/settings/model/payment.php');
				$payment=new payment(); 
				$payment->pay($in);

				msg::success(gm('Account successfully updated'),"success");
			}catch(\Stripe\Error\Card $e){
				msg::notice($e->getMessage(),"notice");
			}
		}
	}

	if($in['payedAllready']==1){
		$result['payed'] = true;
		$in['payedAllready']=0;
	}


	/*$user_crm = $dbu->query("SELECT * FROM customers WHERE database_name='".DATABASE_NAME."' ");
	$user_crm->next();
	$user_crm_a = $dbu->query("SELECT * FROM customer_addresses WHERE customer_id='".$user_crm->f('customer_id')."' AND billing='1' ");
	$user_crm_a->next();*/

	//$u_plan = $dbu->query("SELECT * FROM users WHERE user_id='".$_SESSION['u_id']."' ");
	$u_plan = $dbu->query("SELECT * FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	$u_plan->next();

	$plan = $dbu->query("SELECT * FROM plan WHERE plan_id='".$u_plan->f('plan')."' ");
	$plan->next();

	$order_id = $dbu->field("SELECT value FROM settings WHERE constant_name='ORDER_ID' ");

	//$user_info = $dbu->query("SELECT * FROM user_info WHERE user_id='".$_SESSION['u_id']."' ");
	$user_info = $dbu->query("SELECT * FROM user_info WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	$user_info->next();

	$user_p = $dbu->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE credentials!='8' AND database_name='".DATABASE_NAME."' AND users.active='1' AND ( user_meta.value !=  '1' OR user_meta.value IS NULL ) ");
	$in['users'] = $user_p;
	$user_t = $dbu->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE credentials='8' AND database_name='".DATABASE_NAME."' AND users.active='1' AND ( user_meta.value !=  '1' OR user_meta.value IS NULL ) ");
	$in['time_users'] = $user_t;


	$user_total = $user_p;
	switch (true) {
		case ($user_total <= 3):
			$in['how_many'] = 1;
			break;
		case ($user_total <= 7):
			$in['how_many'] = 2;
			break;
		case ($user_total <= 15):
			$in['how_many'] = 3;
			break;
		case ($user_total > 15):
			$in['how_many'] = 4;
			break;
		default:
			$in['how_many'] = 0;
			break;
	}

	$how_many = $db->query("SELECT value FROM settings WHERE constant_name='HOW_MANY' AND module='payment' ");
	if($how_many->next()){
		$in['how_many'] = $how_many->f('value');
	}
	$in['period'] = 1;
	$period = $db->query("SELECT value FROM settings WHERE constant_name='PERIOD' AND module='payment' ");
	if($period->next()){
		$in['period'] = $period->f('value');
	}


	$days_left = ceil(($user_info->f('end_date') - time())/(24*60*60));
	$in['plan'] = $u_plan->f('plan');
	switch ($u_plan->f('plan')) {
		case '1':
			$t_amount = display_number(0);
			$plan_text = '(type Free with 1 user)';
			break;
		case '2':
			$t_amount = display_number($u_plan->f('time_users')*$u_plan->f('user_time_amount'));
			$plan_text = '(type Services with '.$u_plan->f('users').' users and '.$u_plan->f('time_users').' timesheets users)';
			$total = ($u_plan->f('time_users')*$u_plan->f('user_time_amount')+$u_plan->f('users')*$u_plan->f('user_amount'));
			break;
		case '3':
			$t_amount = display_number($u_plan->f('time_users')*$u_plan->f('user_time_amount'));
			$plan_text = '(type Services and Products with '.$u_plan->f('users').' users and '.$u_plan->f('time_users').' timesheets users)';
			$total = ($u_plan->f('time_users')*$u_plan->f('user_time_amount')+$u_plan->f('users')*$u_plan->f('user_amount')+$u_plan->f('webshop_amount')+15);
			break;
		case '4':
			$t_amount = display_number($u_plan->f('time_users')*$u_plan->f('user_time_amount'));
			$plan_text = '(type Products with '.$u_plan->f('users').' users)';
			$total = ($u_plan->f('time_users')*$u_plan->f('user_time_amount')+$u_plan->f('users')*$u_plan->f('user_amount')+$u_plan->f('webshop_amount'));
			break;
		default:
			$t_amount = display_number(0);
			$plan_text = '';
			break;
	}
	$account_plan = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PLAN' ");
	$new_plan_webshop = $db->field("SELECT value FROM settings WHERE constant_name='NEW_PLAN_WEBSHOP' ");

	$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

	$total = $u_plan->f('total');
	$vat_value=0;
	if(in_array(get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')), $eu_countries)){
		if(get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')) == 'BE'){
				$total = $u_plan->f('total')/1.21;
				$vat_value=21;
		}else{
			if(!$u_plan->f('valid_vat')){
				$total = $u_plan->f('total')/1.21;
				$vat_value=21;
			}
		}
	}
	//$user_stripe_subscr=$dbu->field("SELECT stripe_subscr_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
	$user_stripe_subscr=$dbu->field("SELECT stripe_subscr_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
	if($user_stripe!='' && $user_stripe_subscr!=''){
		\Stripe\Stripe::setApiKey($config['stripe_api_key']);
		$inv_incoming=\Stripe\Invoice::upcoming(array("customer" => $user_stripe));
		$total=$inv_incoming['subtotal']/100;
	}
	//$stripe_locale=$dbu->field("SELECT lang.code FROM lang INNER JOIN users ON users.lang_id=lang.lang_id WHERE user_id='".$_SESSION['u_id']."' ");
	$stripe_locale=$dbu->field("SELECT lang.code FROM lang INNER JOIN users ON users.lang_id=lang.lang_id WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);

	$stripe_discount=array();
	$discount_percent=0;
	$discount_amount=0;
	//$user_discount_id=$dbu->field("SELECT stripe_discount_id FROM users WHERE user_id='".$_SESSION['u_id']."'");
	$user_discount_id=$dbu->field("SELECT stripe_discount_id FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
	if($user_discount_id != '0' && $user_discount_id != ''){
		\Stripe\Stripe::setApiKey($config['stripe_api_key']);
		$user_discount=\Stripe\Coupon::retrieve($user_discount_id);

		$usr_dsc=array();
		$usr_dsc['id']=$user_discount['id'];
		if($user_discount['amount_off']==null){
			$usr_dsc['name']=(string)$user_discount['percent_off'];
			$discount_percent=1;
			if($user_stripe!=''){
				$inv_incoming_disc=\Stripe\Invoice::upcoming(array("customer" => $user_stripe));
				if(count($inv_incoming_disc['lines']['data'])>1){
					$total=$total-$inv_incoming_disc['lines']['data'][2]['amount']/100*$user_discount['percent_off']/100;
				}else{
					$total=$total-($total*$user_discount['percent_off']/100);
				}
			}	
		}else{
			$total=$total-$user_discount['amount_off']/100;
			$usr_dsc['name']=(string)$user_discount['amount_off']/100;
			$discount_amount=1;
		}
		array_push($stripe_discount, $usr_dsc);
	}

		$result['CHARGE_RATE']				= $u_plan->f('payed') == 2 ? display_number(0) :($u_plan->f('payed') == 0 ? display_number(0) : display_number($total));
		$result['VAT_P']					= display_number($total*21/100);
		$result['HIDE_VAT_FROM_T']			= in_array(get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')), $eu_countries) ? (get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')) == 'BE' ? true : ($u_plan->f('valid_vat') ? false : true )) : false;
		$result['HIDE_CANCEL']				= $u_plan->f('payed') == 1 ? true : false;
		$result['TYPE']						= ($u_plan->f('payed') == 2 ? 'canceled' : ($u_plan->f('payed') == 1 ? 'active' : ($u_plan->f('plan') == 1 ? 'active' : 'trial'))).' '.$plan_text;
		$result['HIDE_NEXT_CHARGE']			= $u_plan->f('plan') == 1 ? false :( $user_info->f('is_trial') == 0 ? false : true);
		$result['HIDE_NEXT_CHARGE2']		= $u_plan->f('plan') == 1 ? false :( $user_info->f('is_trial') == 0 ? false : ($u_plan->f('valid_vat') ? false : (in_array(get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')), $eu_countries) ? (get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')) == 'BE' ? false : true) : false) ));
		$result['HIDE_DAYS']				= $u_plan->f('plan') == 1 ? false : true;
		$result['MAX_U']					= $user_total;
		$result['time_users']				= $user_t;
		$result['USER_ID']					= $_SESSION['u_id'];
		$result['COUNTRY_CODE']				= get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID'));
		$result['STYLE']     				= ACCOUNT_NUMBER_FORMAT;
		$result['VALID_VAT']				= $u_plan->f('valid_vat');
		$result['vat_value']				= $vat_value == 0 ? '' : display_number($u_plan->f('total')*21/100);
		$result['is_vat']					= $vat_value == 0 ? false : true;
		$result['ORDER_ID']					= $order_id ? $order_id+1 : '1';
		$result['PAYED']					= $u_plan->f('payed') && defined('PAYMILL_CLIENT_ID') ? $u_plan->f('payed') : 0;
		$result['TOTAL']					= $u_plan->f('payed') == 0 ? display_number(0) : display_number($u_plan->f('total'));
		$result['TOTAL_LAST']				= $u_plan->f('payed') == 0 ? display_number(0) : display_number($u_plan->f('last_payment'));
		$result['NEXT_CHARGE_DATE']			= date(ACCOUNT_DATE_FORMAT,$user_info->f('end_date'));
		$result['DAYS_LEFT']				= $days_left;
		$result['BUY_TEXT']					= $u_plan->f('payed') == 1 && $user_stripe ? gm('Upgrade') : gm('Buy');
		$result['HIDE_VAT_NR']				= 'hide';
		$result['ACCOUNT_VAT_NUMBER']		= $u_plan->f('valid_vat') ? $u_plan->f('valid_vat') : (ACCOUNT_VAT_NUMBER ? ACCOUNT_VAT_NUMBER : gm('no VAT'));
		$result['ACCOUNT_BILLING_ADDRESS']	= utf8_decode($u_plan->f('ACCOUNT_BILLING_ADDRESS'));
		$result['ACCOUNT_BILLING_ZIP']		= $u_plan->f('ACCOUNT_BILLING_ZIP');
		$result['ACCOUNT_BILLING_CITY']		= $u_plan->f('ACCOUNT_BILLING_CITY');
		$result['ACCOUNT_COUNTRY_DD']		= build_country_list($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID'));
		$result['CUST_ID']					= $u_plan->f('customer_id');
		$result['ACCOUNT_EMAIL']			= ACCOUNT_EMAIL;
		$result['INVOICING_NAME']			= utf8_encode($u_plan->f('ACCOUNT_COMPANY'));
		$result['is_eu_country']			= in_array(get_country_code($u_plan->f('ACCOUNT_BILLING_COUNTRY_ID')), $eu_countries) ? true : false;
		$result['page_tip']					= $tips;
		$result['you_sell_dd']				= defined('PAYMILL_CLIENT_ID') ? build_you_sell_dropdown($account_plan) : build_you_sell_dropdown($u_plan->f('plan'));
		$result['plan']						= defined('PAYMILL_CLIENT_ID') ? $account_plan : $u_plan->f('plan');
		$result['how_many_users_dd']		= build_how_many_dropdown($in['how_many']);
		$result['how_many']					= $in['how_many'];
		$result['period']					= $in['period'];
		$result['period_dd']				= build_period_dropdown($in['period']);
		$result['month_year']				= $in['period']==1 ? gm('per month') : gm('per year');
		$result['default']					= display_number($u_plan->f('total'));
		$result['disabledw']				= $account_plan < 3 ? true : false;
		$result['webshop']					= $new_plan_webshop;
		$result['stripe_user']				= (!$user_paid || $user_paid==2 || $user_active==0) ? '1' : (($user_paid==1 && $user_stripe!='' && $user_stripe_subscr!='') ? '2' : '0'); 
		$result['user_disc_dd']				= $stripe_discount;
		$result['user_disc']				= ($user_discount_id=='0' || $user_discount_id=='') ? '0' : $user_discount_id; 
		$result['user_discount']			= ($user_discount_id=='0' || $user_discount_id=='') ? false : true;
		$result['stripe_discount_type']		= ($user_discount_id=='0' || $user_discount_id=='') ? '' : ($discount_percent==1 ? 'percent' : 'amount');
		$result['discount_type']			= ($user_discount_id=='0' || $user_discount_id=='') ? '' : ($discount_percent==1 ? '%' : '&euro;');
		$result['public_key'] 				= "'".$config['paymill_public_key']."'";
		$result['start_at']					= 1439208173;
		$result['stripe_locale']			= $stripe_locale;
		$result['stripe_pub_key']			= $config['stripe_pub_key'];



	unset($_SESSION['articles_id']);

	return json_out($result);