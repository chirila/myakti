<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
session_start();
// ini_set('xdebug.var_display_max_data', 100024);
// if(!$in['db']){
// 	$db =  new sqldb();

// }else{
// 	$database_2 = array(
// 		'hostname' => $database_config['mysql']['hostname'],
// 		'username' => $database_config['mysql']['username'],
// 		'password' => $database_config['mysql']['password'],
// 		'database' => $in['db'],
// 		);
// 	$db =  new sqldb($database_2); // recurring billing
// }
global $database_config;
$database = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);

$dbu_users =  new sqldb($database);

$db=new sqldb($database);
$db2=new sqldb($database);
$db3 = new sqldb($database);
$db5 = new sqldb($database);

$db->query("SET names utf8");
$db->query("SET sql_mode= ''");

$db2->query("SET names utf8");
$db2->query("SET sql_mode= ''");

$db3->query("SET names utf8");
$db3->query("SET sql_mode= ''");

$db->query("SELECT constant_name, value, long_value FROM settings");
while($db->move_next()){
    if($db->f('constant_name')){
        $const[$db->f('constant_name')] = $db->f('value') ? $db->f('value') : utf8_decode($db->f('long_value'));
    }
}

$path = ark::$viewpath;

if($in['type']){
	$html='invoice_print_'.$in['type'].'.html';
}elseif ($in['custom_type']) {
	$path = INSTALLPATH.'upload/'.DATABASE_NAME.'/custom_pdf/';
	$html='custom_invoice_print-'.$in['custom_type'].'.html';
}elseif ($in['body']){
	$html='customizable_print_'.$in['layout'].'.html';
}elseif($in['body_id']){
	$html='customizable_body_print_'.$in['body_id'].'.html';
}elseif($in['header']){
	$html='customizable_print_header_'.$in['layout'].'.html';
}elseif($in['layout'] || $in['footer_id']){
	$html='customizable_print_footer_'.$in['layout'].'.html';
}else{
	$html='invoice_print_1.html';
}

// echo $path.$html;exit();
// $html='invoice_print_6.html';
$view_html = new at($path.$html);

//$labels_query = $db->query("SELECT * FROM label_language WHERE label_language_id='".$in['lid']."'")->getAll();
$labels_query = $db->query("SELECT * FROM label_language WHERE label_language_id= :label_language_id",['label_language_id'=>$in['lid']])->getAll();
$labels = array();
$j=1;
foreach ($labels_query['0'] as $key => $value) {
	$labels[$key] = $value;
	$j++;
}
//$labels_query_custom = $db->query("SELECT * FROM label_language WHERE lang_code='".$in['lid']."'")->getAll();
$labels_query_custom = $db->query("SELECT * FROM label_language WHERE lang_code= :lang_code",['lang_code'=>$in['lid']])->getAll();
if(is_array($labels_query_custom['0'])){
	foreach ($labels_query_custom['0'] as $key => $value) {
		$labels[$key] = $value;
		$j++;
	}
}

if($in['lid']<=4){
	//$labels_quotes = $db->query("SELECT * FROM label_language_quote WHERE label_language_id='".$in['lid']."'")->getAll();
	$labels_quotes = $db->query("SELECT * FROM label_language_quote WHERE label_language_id= :label_language_id",['label_language_id'=>$in['lid']])->getAll();
}else{
	//$labels_quotes = $db->query("SELECT * FROM label_language_quote WHERE lang_code='".$in['lid']."'")->getAll();
	$labels_quotes = $db->query("SELECT * FROM label_language_quote WHERE lang_code= :lang_code",['lang_code'=>$in['lid']])->getAll();
}
$labels_q = array();
foreach ($labels_quotes['0'] as $key => $value) {
	$labels_q[$key] = $value;
	$j++;
}


/*echo $labels['bic_code'];*/

//custom width default and we need it!!
$all_custom_widths = array(4,15,16,10,13,10,15);
$can_be_hidden_custom_widths = array('custom_line_1' => 13, 'custom_line_2' => 10, 'custom_line_3' => 0, 'custom_line_4' => 0, 'custom_line_5' => 0 );

if($in['id'] && !$in['reminder']){



	//$payments_info = $db->query("SELECT amount,credit_payment FROM tblinvoice_payments WHERE invoice_id='".$in['id']."' ORDER BY payment_id ASC ");
	$payments_info = $db->query("SELECT amount,credit_payment FROM tblinvoice_payments WHERE invoice_id= :invoice_id ORDER BY payment_id ASC ",['invoice_id'=>$in['id']]);
 	$total_paid=0;

 	$negative = 1;
 	if(defined('USE_NEGATIVE_CREDIT') && $const['USE_NEGATIVE_CREDIT']==1){
 		$negative = -1;
 	}

    while($payments_info->move_next()){
	    $total_paid+= $payments_info->f('credit_payment') == 1 ? ($negative*$payments_info->f('amount')) : $payments_info->f('amount');
	}

	if($in['lid']>=1000) {
		//$code = $db->field("SELECT code FROM pim_custom_lang WHERE lang_id='".$in['lid']."' ");
		$code = $db->field("SELECT code FROM pim_custom_lang WHERE lang_id= :lang_id ",['lang_id'=>$in['lid']]);
	} else {
		//$code = $db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['lid']."' ");
		$code = $db->field("SELECT code FROM pim_lang WHERE lang_id= :lang_id ",['lang_id'=>$in['lid']]);
	}
	/*$db->query("SELECT tblinvoice.* FROM tblinvoice
				WHERE tblinvoice.id='".$in['id']."'");*/
	$db->query("SELECT tblinvoice.* FROM tblinvoice
				WHERE tblinvoice.id= :id",['id'=>$in['id']]);				
	$db->move_next();
	$invoice_type = $db->f('type');
	$proforma_id = $db->f('proforma_id');

	if($proforma_id){
		$payments_info = $db2->query("SELECT amount FROM tblinvoice_payments WHERE invoice_id='".$proforma_id."' ORDER BY payment_id ASC ");
		while($payments_info->move_next()){
		    $total_paid+=$payments_info->f('amount');
		}
	}



	$discount = $db->f('discount');
	$apply_discount = $db->f('apply_discount');
	$serial_number = $db->f('serial_number');
	$due_days = $db->f('due_days');
	$vat = $db->f('vat');
	$factur_date = date($const['ACCOUNT_DATE_FORMAT'],  $db->f('invoice_date'));
	$facture_plus_8d = date($const['ACCOUNT_DATE_FORMAT'], strtotime('+8 days',$db->f('invoice_date')));
	$due_date = date($const['ACCOUNT_DATE_FORMAT'],  $db->f('due_date'));
	$currency_type = $db->f('currency_type');
	$currency_rate = $db->f('currency_rate');
	$img = 'images/no-logo.png';
	$attr = '';

	if($in['logo'])
	{
		/*switch($in['logo']){
		case 1: $print_logo=ACCOUNT_LOGO;break;
		case 3: $print_logo=ACCOUNT_LOGO_QUOTE;break;
		case 2: $print_logo=ACCOUNT_LOGO_ORDER;break;*/
		$print_logo = $in['logo'];
		// }
	}else {

		$print_logo=$const['ACCOUNT_LOGO'];
	}
	var_dump($print_logo,$const['ACCOUNT_LOGO']);
	if($db->f('company_logo')){
		$print_logo_idnt=$db->f('company_logo');
	}
	
	if($print_logo){
		$img = $print_logo;
		if($print_logo_idnt){
			$img=$print_logo_idnt;
		}
		$size = getimagesize($img);
		if($size === false){
			$img=$const['ACCOUNT_LOGO'] ? $const['ACCOUNT_LOGO'] :'images/no-logo.png';
			$size = getimagesize($img);
		}
		$ratio = 250 / 77;
		if($size[0]/$size[1] > $ratio ){
			$attr = 'width="250"';
		}else{
			$attr = 'height="77"';
		}
	}
	if($db->f('contact_id') && !$db->f('buyer_id'))
	{
		$contact_title = $db2->field("SELECT customer_contact_title.name FROM customer_contact_title
									INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
									WHERE customer_contacts.contact_id='".$db->f('contact_id')."'");
	}
	if($contact_title)
	{
		$contact_title .= " ";
	}
	$cr_inv = gm('Credit Invoice',true);
	$cr_nr = gm('Credit Note Nr:',true);

	if(!$code){$code = 'en';}
	if($code == 'du'){$code = 'nl';}
	$free_field = $db->f('buyer_address').'<br/>'.$db->f('buyer_zip').' '.$db->f('buyer_city').'<br/>'.get_country_name($db->f('buyer_country_id')).'<br/>';

	/*$notes = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'invoice'
								   AND item_name = 'notes'
								   AND active = '1'
								   AND item_id = '".$in['id']."' ");*/
	$notes = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type = :item_type
								   AND item_name = :item_name
								   AND active = :active
								   AND item_id = :item_id ",
								['item_type' => 'invoice',
								 'item_name' => 'notes',
								 'active' => '1',
								 'item_id' => $in['id']]
							);								   
	$modif_free_field = '<p>'.nl2br($db->f('free_field')).'</p>';
	if($db->f('type')==6){
		$modif_free_field = '<span>'.nl2br($db->f('free_field')).'</span>';
	}else{
		if(!$db->f('type') && ($in['type']==6)){
			$modif_free_field = '<span>'.nl2br($db->f('free_field')).'</span>';
		}
	}
	$width = 56;
	$pdf->setQuotePageLabel($labels['page']);

	// if($db->f('buyer_id')){
	// 	$buyer_data = $db3->query("SELECT comp_reg_number,bank_iban,bank_bic_code,bank_name,btw_nr FROM customers WHERE customer_id = '".$db->f('buyer_id')."' ");
	// 	while($buyer_data->next()){
			// $comp_reg_number 	= 	$buyer_data->f('comp_reg_number');
			$bank_name 		= 	$const['ACCOUNT_BANK_NAME'];
			$bank_iban 		= 	$const['ACCOUNT_IBAN'];
			$bank_bic_code 	= 	$const['ACCOUNT_BIC_CODE'];
			$buyer_btw 		= 	$const['ACCOUNT_VAT_NUMBER'];
		// }
	// }
	// if($db->f('sepa_number')){
	// 	$sepa_message = $db3->query("SELECT * FROM sepa_settings ");
	
	// 	$message_data=get_sys_message('sepamess',$db->f('email_language'));
	// 	$body=$message_data['text'];
	// 	$body=str_replace('[!MANDATE!]',$db->f('sepa_number'), $body);
	// 	$body=str_replace('[!IBAN!]',$sepa_message->f('sepa_iban'), $body);
	// 	$body=str_replace('[!DUE_DATE!]',date($const['ACCOUNT_DATE_FORMAT'],$db->f('due_date')), $body);
	// }
	// if($db->f('attention_of')){
	// 	$attention_text = $labels['attention_of'];
	// 	$attention_value = $db->f('attention_of');
	// }else{
		$attention_text ='';
		$attention_value = '';
	// }
	
	$view_html->assign(array(
		'account_logo'        =>  file_exists($img) ? $img : 'images/no-logo.png' ,
		'attr'				  => $attr,
		'billing_address_txt' => $labels['billing_address'],
		'invoice_note_txt'    => $db->f('type') == '2' ? $labels['credit_inv'] : ($db->f('type') == '1' ? $labels['pro_invoice'] : $labels['invoice_note']),
		'invoice_txt'         => $db->f('type') == '2' ? utf8_encode($cr_nr['0'][$code]) : $labels['invoice'].':',
		'free_text_br'	 	  => $db->f('general_conditions_new_page') == 1 ? '<br pagebreak="true"/>' : '',
		'date_txt'            => $labels['date'],
		'customer_txt'        => $labels['customer'],
		'item_txt'            => $labels['item'],
		'article_code_txt'    => $labels_q['article_code'],
		'unitmeasure_txt'     => $labels['unitmeasure'],
		'quantity_txt'        => $labels['quantity'],
		'unit_price_txt'      => $labels['unit_price'],
		'amount_txt'          => $labels['amount'],
		'subtotal_txt'        => $labels['subtotal'],
		'discount_txt'        => $labels['discount'],
		'vat_txt'             => $labels['vat'],
		'payments_txt'        => $labels['payments'],
		'amount_due_txt'      => $labels['amount_due'],
		'downpayment_txt'		=> $labels['downpayment'],
		'notes_txt'           => $notes ? $labels['notes'] : '',
		'notes2_txt'          => $db->f('notes2') ? $labels['invoice_note2'] : '',
		'duedate_txt'		  => $labels['duedate'],
		'bankd_txt'			  => $labels['bank_details'],
		'bank_txt'			  => $labels['bank_name'],
		'bic_txt'			  => $labels['bic_code'],
		'iban_txt'			  => $labels['iban'],
		'phone_txt'			  => $labels['phone'],
		'fax_txt'			  => $labels['fax'],
		'url_txt'			  => $labels['url'],
		'email_txt'			  => $labels['email'],
		'attention_of_txt'			  => $labels['attention_of'],
		'our_ref_txt'		  => $labels['our_ref'],
		'your_ref_txt'		  => $labels['your_ref'],
		'vat_number_txt' 	  => $labels['vat_number'],
		'payment_inst_txt'	  => $labels['pay_instructions'],
		'net_txt' 			  => $labels['net_amount'],
		'ogm_txt'			  => $labels['ogm'],
		'paid_txt'			  => $labels['paid'],
		'sepa_mess_txt'				  => $labels['sepa_mess'],

		'seller_name'         => $db->f('identity_id')? $db->f('company_name') : $db->f('seller_name'),
		'seller_d_country'    => get_country_name($db->f('seller_d_country_id')),
		'seller_d_state'      => get_state_name($db->f('seller_d_state_id')),
		'seller_d_city'       => $db->f('seller_d_city'),
		'seller_d_zip'        => $db->f('seller_d_zip'),
		'seller_d_address'    => $db->f('seller_d_address'),
		'seller_b_country'    => get_country_name($db->f('seller_b_country_id')),
		'seller_b_state'      => get_state_name($db->f('seller_b_state_id')),
		'seller_b_city'       => $db->f('seller_b_city'),
		'seller_b_zip'        => $db->f('seller_b_zip'),
		'seller_b_address'    => nl2br($db->f('seller_b_address')),
		'invoice_vat_no'      => $db->f('seller_bwt_nr'),
		'serial_number'       => $db->f('serial_number') ? $db->f('serial_number') : '',
		'due_days'			  => $due_days,

		'our_ref' 		      => $db->f('our_ref') ,
		'your_ref'     		  => $db->f('your_ref'),
		'hide_our'			  => $db->f('our_ref')? '':'hide',
		'hide_your'			  => $db->f('your_ref')? '':'hide',
		'nr'				  => $in['custom_type'],

		'seller_b_fax'        => $db->f('identity_id')? $db->f('company_fax') : $const['ACCOUNT_FAX'],
		'seller_b_email'      => $db->f('identity_id')? $db->f('company_email') : $const['ACCOUNT_EMAIL'],
		'seller_b_phone'      => $db->f('identity_id')? $db->f('company_phone') : $const['ACCOUNT_PHONE'],
		'seller_b_url'        => $db->f('identity_id')? $db->f('company_url') : $const['ACCOUNT_URL'],
		'ogm'				  => $db->f('ogm'),
		'use_ogm'			  => defined('USE_OGM') && USE_OGM == 1 ? true : false,

		//optional stuff
		'seller_b_reg_number' 		=> $db->f('identity_id')? $db->f('company_reg_number') : $const['ACCOUNT_REG_NUMBER'],
		'show_acc_reg_no'	    		=> $const['ACCOUNT_REG_NUMBER'] ? true : false,
		'comp_reg_number_txt' 		=> $labels['comp_reg_number'],
		'show_billing_addres_txt'	=> false,
		'show_comp_reg_no'				=> $comp_reg_number ? true : false,
		'buyer_b_reg_number'			=> $comp_reg_number,
		// 'comp_reg_number' 				=> $db->f('buyer_id') ? $buyer_data->f('comp_reg_number') : '',

		'buyer_bank' 							=> $bank_name,
		'buyer_iban' 							=> $bank_iban,
		'buyer_bic' 							=> $bank_bic_code,

		'stamp_signature'					=> $labels['stamp_signature'],
		'show_stamp_signature'		=> $const['SHOW_STAMP_SIGNATURE'] ? true : false,
		'show_bank_details'				=> $const['SHOW_BANK_DETAILS'] ? true : false,




		'hide_f'			  => defined('ACCOUNT_FAX') && !$const['ACCOUNT_FAX'] ? false : true,
		'hide_e'			  => defined('ACCOUNT_EMAIL') && !$const['ACCOUNT_EMAIL'] ? false : true,
		'hide_p'			  => defined('ACCOUNT_PHONE') && !$const['ACCOUNT_PHONE'] ? false : true,
		'hide_u'			  => defined('ACCOUNT_URL') && !$const['ACCOUNT_URL'] ? false : true,
		'is_proforma'		  => $db->f('type')==1? true:false,
		'not_proforma'		  => $db->f('type')==1? false: true,

		'invoice_date'        => $factur_date,
		'invoice_plus_8d'	  => $facture_plus_8d,
		'invoice_due_date'    => $due_date,
		'invoice_vat'         => $db->f('vat'),
	    'attention_of'      => $db->f('attention_of'),
		'show_attention_of'      => $db->f('attention_of')?true:false,

		'buyer_name'       	  => $contact_title.$db->f('buyer_name'),
		'buyer_email'      	  => $db->f('buyer_email'),
		'buyer_country'    	  => get_country_name($db->f('buyer_country_id')),
		'buyer_state'      	  => get_state_name($db->f('buyer_state_id')),
		'buyer_city'       	  => $db->f('buyer_city'),
		'buyer_zip'        	  => $db->f('buyer_zip'),
		'buyer_address'    	  => nl2br(utf8_encode($db->f('buyer_address'))),
		'buyer_btw'			  => $buyer_btw,
		'hide_buyer_btw'	  => $buyer_btw ? true : false,
		'free_field'		  => $db->f('free_field') ? $modif_free_field : $free_field,
		'invoice_buyer_vat'   => $const['ACCOUNT_VAT_NUMBER'],
		'bank'				  => $db->f('identity_id')? $db->f('bank_name_identity') : $const['ACCOUNT_BANK_NAME'],
  	'bic'				  => $db->f('identity_id')? $db->f('bank_bic_code_identity') : $const['ACCOUNT_BIC_CODE'],
  	'iban'				  => $db->f('identity_id')? $db->f('bank_iban_identity') : $const['ACCOUNT_IBAN'],


 	  'hide_b_d'			  => (!$const['ACCOUNT_BANK_NAME'])&&(!$const['ACCOUNT_IBAN']) ? false : true,

		'notes'            	  => $notes ? nl2br($notes) : '',
		'notes2'              => $db->f('notes2') ? nl2br(utf8_decode($db->f('notes2'))) : '',
		'view_notes'          => $notes ? '' : 'hide' ,
		'view_notes2'         => $db->f('notes2') ? '' : 'hide' ,
		'default_total'  	  => false,//$currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? false : true) : false,
		'def_currency'		  => build_currency_name_list($const['ACCOUNT_CURRENCY_TYPE']),
		'session'			  => DATABASE_NAME,
		'inv_type'			  => $db->f('type'),
		'red_if_credit'		  => $db->f('type')==2 ? 'color:#ff0000;' : '',
		'reminder'			  => false,
		'hide_disc_line'	  => ($apply_discount ==0 || $apply_discount == 2) ? false : true,
		'item_w'			  => ($apply_discount ==0 || $apply_discount == 2) ? $width : $width-15,
		'is_paid'			  => $total_paid ? '' : 'hide',
		'show_paid'			  => $const['SHOW_PAID_ON_INVOICE_PDF'] == 1 ? true : false,
		'sepa_mess'			  => $body,
		'show_sepa_mess'	  => $db->f('sepa_number') ? true : false,
	));

	if($apply_discount ==0 || $apply_discount == 2){
		//custom width
		$custom_line_2 = false;
	}else{
		//custom width
		$custom_line_2 = true;
	}

	$height = 261;
	if($in['type']==7)
		{$height = 245;}
	$hide_table_1 = false;
	$hide_table_2 = false;
	$sh_vat2=true;
	$sh_discount = true;

	if($db->f('discount') == 0){
		$hide_table_1 = true;
		$sh_discount = false;
		// $view_html->assign('sh_discount','hide');
		$discount_procent=0;
	} else {
		$height -= 5;
		$discount_procent = $db->f('discount');
		$view_html->assign('total_discount_procent',display_number($db->f('discount')));
	}

	if($apply_discount < 2){
		$hide_table_1 = true;
		$discount_procent = 0;
		$sh_discount = false;
		// $view_html->assign('sh_discount','hide');
	}
	$rm_vat = $db->f('remove_vat');
	$view_html->assign('sh_discount',$sh_discount);
	if($rm_vat != 1){
		$hide_table_2 = true;
		$sh_vat2=false;

		$vat_procent=0;
	} else {
		$vat_procent = $db->f('vat');
		$view_html->assign('total_vat_procent',$db->f('vat'));
	}
	$view_html->assign('sh_vat2',$sh_vat2);
	if($hide_table_1 == true && $hide_table_2 == true) {
		$view_html->assign('table_hide', 'hide_table');
	}
	//$inv=$db2->query("SELECT quote_id, apply_discount FROM tblinvoice WHERE id='".$in['id']."' ");
	$inv=$db2->query("SELECT quote_id, apply_discount FROM tblinvoice WHERE id= :id ",['id'=>$in['id']]);
	//PAID
	if($db->f('paid') == 1){
		$discount_value = 0;
		$sub_total = 0;
		$sub_total2 = 0;
		$vat_line = 0;
		$subtotal_vat = 0;
		$req_payment = $db->f('req_payment');
		//new
		$total_n = 0;
		$netto_amount=0;
		//
		//total
		//$db->query("SELECT SUM(amount) AS total, vat FROM tblinvoice_line WHERE invoice_id='".$in['id']."' GROUP BY vat ");
		//$db->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id='".$in['id']."' ");
		$db->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id= :invoice_id ",['invoice_id'=>$in['id']]);
		while ($db->move_next()) {
			$line_discount = $db->f('discount');
			if($apply_discount ==0 || $apply_discount == 2){
				$line_discount = 0;
			}
			// $discount_value += $db->f('total')*$discount_procent/100;
			// $sub_total += $db->f('total')-($db->f('total')*$discount_procent/100);
			// $sub_total2 += $db->f('total');
			// $vat_line += ($db->f('total')-($db->f('total')*$discount_procent/100))*($db->f('vat')/100);

			$amount_line = $db->f('amount') - $db->f('amount') * $line_discount / 100;
			$amount_line = round($amount_line,$const['ARTICLE_PRICE_COMMA_DIGITS']);
			$amount_line_disc = round($amount_line*$discount_procent/100,2);
			$discount_value += $amount_line_disc;
			$subtotal_vat += ( $amount_line - $amount_line_disc ) * $db->f('vat') / 100;
			$sub_total += $amount_line - $amount_line_disc;
			$sub_total2 += $amount_line;
			$netto_amount+=(round($amount_line,2)-$amount_line_disc);
		}
		$subtotal_vat=round($subtotal_vat,2);
		$sub_total=round($sub_total,2);
		if($discount_procent != 0){
			$view_html->assign(array(
				'discount_value' 	=> place_currency(display_number($discount_value)),
				'net_value'			=> place_currency(display_number($sub_total2-$discount_value))
			));
		}

		/*if($inv->f('quote_id') && $inv->f('downpayment_drawn')){
			$total = $sub_total+$subtotal_vat-$inv->f('downpayment_value');
		}else{*/
			$total = $sub_total+$subtotal_vat;
		// }


		if(($currency_type != $const['ACCOUNT_CURRENCY_TYPE']) && $currency_rate){
			$total_default = $total*return_value($currency_rate);
		}

		$req_payment_value=$total* $req_payment/100;
		if($req_payment != 100){
			$view_html->assign('sh_pay','hide');
		}
		if($discount_procent){
			$view_discount1 		= true;
		}else{
			$view_discount1			= false;
		}
		$apply_discount						= $inv->f('apply_discount');
		if($apply_discount>1){
			$apply_discount=true;
		}else{
			$apply_discount=false;
		}
// exit($total_paid);
		$view_html->assign('req_payment',100 - $req_payment);
		$view_html->assign(array(
			'total_paid'				=> place_currency(display_number($total_paid),get_commission_type_list($currency_type),'','helvetica'),
			'total_amount_due'				=> place_currency(display_number($total),get_commission_type_list($currency_type),'','helvetica'),
			'total_amount_due_default'		=> place_currency(display_number($total_default)),
			'total_payments'				=> $invoice_type == 2 ? place_currency(display_number($total),get_commission_type_list($currency_type)) : place_currency(0,get_commission_type_list($currency_type)),
			'sh_edit'						=> 'hide',
			'view_discount1'				=> $view_discount1,
			'apply_discount'				=> $apply_discount>1 ? true : false,
			'dis_value' 					=> place_currency(display_number($discount_value)),
			'net_amount'					=> place_currency(display_number($netto_amount),$currency_type),
			'total_novat'	   				=> place_currency(display_number($sub_total2),get_commission_type_list($currency_type)),
			'req_payment_value'         	=> place_currency(display_number($total-$req_payment_value),get_commission_type_list($currency_type),'','helvetica'),
			'vat'							=> $vat,
			// 'downpayment'		=> ($inv->f('quote_id')&&$inv->f('downpayment_drawn')) ? true : false,
			// 'total_downpayment'	=> ($inv->f('quote_id')&&$inv->f('downpayment_drawn')) ? place_currency(display_number($inv->f('downpayment_value')),get_commission_type_list($currency_type),'','helvetica') : '',
		));



	}//NOT PAID / NOT FULLY PAID
	else {
		$total_n = 0;
		$netto_amount=0;
		$discount_value = 0;
		$sub_total = 0;
		$sub_total2 = 0;
		$vat_line = 0;
		$subtotal_vat = 0;
		$req_payment = $db->f('req_payment');
		//total
		//$db->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id='".$in['id']."' ");
		$db->query("SELECT amount, vat,discount FROM tblinvoice_line WHERE invoice_id= :invoice_id ",['invoice_id'=>$in['id']]);
		while($db->move_next()){
			$line_discount = $db->f('discount');
			if($apply_discount ==0 || $apply_discount == 2){
				$line_discount = 0;
			}
			// $discount_value += $db->f('total')*$discount_procent/100;
			// $sub_total += $db->f('total')-($db->f('total')*$discount_procent/100);
			// $sub_total2 += $db->f('total');
			// $vat_line += ($db->f('total')-($db->f('total')*$discount_procent/100))*($db->f('vat')/100);

			$amount_line = $db->f('amount') - $db->f('amount') * $line_discount / 100;
			$amount_line = round($amount_line,$const['ARTICLE_PRICE_COMMA_DIGITS']);
			$amount_line_disc = round($amount_line*$discount_procent/100,2);
			$discount_value += $amount_line_disc;
			$netto_amount+=(round($amount_line,2)-$amount_line_disc);
			$subtotal_vat += ( $amount_line - $amount_line_disc ) * $db->f('vat') / 100;
			
			$sub_total += $amount_line - $amount_line_disc;
			$sub_total2 += $amount_line;
		}
		$subtotal_vat=round($subtotal_vat,2);
		$sub_total=round($sub_total,2);
		if($discount_procent != 0){
			$view_html->assign(array(
				'discount_value'	=> place_currency(display_number($discount_value),get_commission_type_list($currency_type)),
				'net_value'			=> place_currency(display_number($sub_total2-$discount_value),get_commission_type_list($currency_type))
			));
		}
		/*if($inv->f('quote_id') && $inv->f('downpayment_drawn')){
			$total = $sub_total+$subtotal_vat-$inv->f('downpayment_value');
		}else{*/
			$total = $sub_total+$subtotal_vat;
		// }

		if(($currency_type != $const['ACCOUNT_CURRENCY_TYPE']) && $currency_rate){
			$total_default = $total*return_value($currency_rate);
		}

		//already payed
		//$db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id='".$in['id']."' ");
		$db->query("SELECT SUM(amount) AS total_payed FROM tblinvoice_payments WHERE invoice_id= :invoice_id ",['invoice_id'=>$in['id']]);
		$db->move_next();
		$total_payed = $db->f('total_payed');

		if($proforma_id){
			$payments_info = $db2->query("SELECT amount FROM tblinvoice_payments WHERE invoice_id='".$proforma_id."' ORDER BY payment_id ASC ");
			while($payments_info->move_next()){
			    $total_payed+=$payments_info->f('amount');
			}
		}
		$req_payment_value=$total* $req_payment/100;
		if($req_payment == 100){
			$view_html->assign('is_req_payment',false);
			$amount_due = round($total - $total_payed,2);
		}else{
			$view_html->assign('is_req_payment',true);
			$amount_due = round($req_payment_value - $total_payed ,2);
		}


		$view_html->assign('req_payment',$req_payment);

		if($invoice_type == 2){
			$total_paid = $total;
		}
		if($discount_procent){
			$view_discount1 		= true;
		}else{
			$view_discount1			= false;
		}
		$apply_discount						= $inv->f('apply_discount');
		if($apply_discount>1){
			$apply_discount=true;
		}else{
			$apply_discount=false;
		}


		$view_html->assign(array(
			'total_payments'				=> $invoice_type == 2 ? place_currency(display_number($total),get_commission_type_list($currency_type)) : place_currency(display_number($amount_due),get_commission_type_list($currency_type)),
			'payment_date'  				=> $in['payment_date'],
			'total_paid'				=> place_currency(display_number($total_paid),get_commission_type_list($currency_type),'','helvetica'),
			'total_amount_due'				=> place_currency(display_number($total),get_commission_type_list($currency_type),'','helvetica'),
			'total_amount_due_default'		=> place_currency(display_number($total_default)),
			'total_novat'	   				=> place_currency(display_number($sub_total2),get_commission_type_list($currency_type)),
			'vat'							=> $vat,
			'apply_discount'				=> $apply_discount,
			'view_discount1'				=> $view_discount1,	
			'dis_value' 					=> place_currency(display_number($discount_value),get_commission_type_list($currency_type)),
			'net_amount'					=> place_currency(display_number($netto_amount)),
			'sh_pay'						=> $total_payed ? '' : 'hide',
			'req_payment_value'         	=> place_currency(display_number($req_payment_value-$total_payed),get_commission_type_list($currency_type),'','helvetica'),
			// 'downpayment'		=> ($inv->f('quote_id')&&$inv->f('downpayment_drawn')) ? true : false,
			// 'total_downpayment'	=> ($inv->f('quote_id')&&$inv->f('downpayment_drawn')) ? place_currency(display_number($inv->f('downpayment_value')),get_commission_type_list($currency_type),'','helvetica') : '',
		));
		if($db->f('req_payment') != 100){
			$view_html->assign('sh_pay','hide');
		}
	}

	//$vr = $db->field("SELECT COUNT(DISTINCT vat) FROM tblinvoice_line WHERE invoice_id='".$in['id']."' ");
	$vr = $db->field("SELECT COUNT(DISTINCT vat) FROM tblinvoice_line WHERE invoice_id= :invoice_id ",['invoice_id'=>$in['id']]);

	//GET invoice rows
	/*$db->query("SELECT tblinvoice_line.*,tblinvoice.id,tblinvoice.due_date_vat,tblinvoice.currency_type, tblinvoice_line.discount as disc_line
            FROM tblinvoice_line
            INNER JOIN tblinvoice ON tblinvoice.id=tblinvoice_line.invoice_id
            WHERE tblinvoice_line.invoice_id='".$in['id']."'
            order by tblinvoice_line.sort_order ASC ");*/
	$db->query("SELECT tblinvoice_line.*,tblinvoice.id,tblinvoice.due_date_vat,tblinvoice.currency_type, tblinvoice_line.discount as disc_line
            FROM tblinvoice_line
            INNER JOIN tblinvoice ON tblinvoice.id=tblinvoice_line.invoice_id
            WHERE tblinvoice_line.invoice_id= :invoice_id
            order by tblinvoice_line.sort_order ASC ",['invoice_id'=>$in['id']]);            

	/*$last_row=$db2->query("SELECT tblinvoice_line.content_title,tblinvoice_line.sort_order
            FROM tblinvoice_line

            WHERE tblinvoice_line.invoice_id='".$in['id']."'
            order by tblinvoice_line.sort_order DESC LIMIT 1");
	if($last_row->f('content_title')){
		$is_last_title=false;
	}*/


	$i = 0;
	$total_amount = 0;
	$vat_percent = array();
	$sub= array();
	$sub_t = array();
	$sub_disc = array();
	$vat_percent_val = 0;
	$invoice_due_date_vat = 0;
	$rows = $db->records_count();
	while ($db->move_next()){
		$invoice_due_date_vat = intval($db->f('due_date_vat'));
		$line_d = $db->f('discount');
		if($apply_discount ==0 || $apply_discount == 2){
			$line_d = 0;
		}
		// $amount = $db->f('amount') - $db->f('amount') * $line_d / 100;
		$amount = $db->f('quantity') *($db->f('price') - ( $db->f('price') * $line_d / 100 ) );
		
		if($db->f('content_title') && $i==0){

			$view_html->assign('first_title','style="display:none;"');
		}
		$is_last_title=false;
		if($db->f('content_title')){
			$is_last_title=true;
			if($i+1 == $rows){
				$is_last_title=false;
			}
		}
		//$amount = round($amount,2);
		$view_html->assign(array(
			'row_nr'								=> $i+1,
			'row_description'				=> nl2br(htmlentities($db->f('name'),null,'UTF-8')),
       'row_unitmeasure'			=> $db->f('unitmeasure'),
			'row_quantity'					=> display_number($db->f('quantity')),
			'row_unit_price'				=> display_number_var_dec($db->f('price')),
			'row_vat'								=> display_number($db->f('vat')),
			'row_amount'						=> place_currency(display_number($amount),get_commission_type_list($currency_type)),
			'row_discount'					=> display_number($db->f('disc_line')),
			'hide_disc_line'		  	=> ($apply_discount ==0 || $apply_discount == 2) ? false : true,
			'if_disc_line'					=> ($apply_discount ==0 || $apply_discount == 2) ? ($vr > 1 ? '39' : '49') : ($vr > 1 ? '29' : '39'),
			'if_disc_line2'					=> ($apply_discount ==0 || $apply_discount == 2) ? ($vr > 1 ? '59' : '69') : ($vr > 1 ? '49' : '59'),
			'content'               => $db->f('content'),
			'title'                 => $db->f('content_title'),
			'is_last_title'        	=> $is_last_title,
			'item_w'								=> ($apply_discount ==0 || $apply_discount == 2) ? $width-15 : $width-30,
			'colspan'               => DATABASE_NAME == '8f334428_fd04_3966_9c96445f4329' ? '' : ( ($apply_discount ==0 || $apply_discount == 2) ? ' colspan="6" class="last" ': 'colspan="6" class="last" '),
			'colspan'               => DATABASE_NAME == '8f334428_fd04_3966_9c96445f4329' && $in['custom_type'] ? '' : ( ($apply_discount ==0 || $apply_discount == 2) ? ' colspan="6" class="last" ': 'colspan="6" class="last" '),
			'i_w'										=> ($apply_discount ==0 || $apply_discount == 2) ? ($vr > 1 ? '35' : '45'): ($vr > 1 ? '25' : '35'),
			'hide_row_vat'					=> $vr > 1 ? true : false,
			'article_code' 					=> $db->f('item_code'),
			'show_for_articles' 		=> $db->f('item_code') ? true : false,
		),'invoice_row');

		$total_amount += $db->f('amount');
		$i++;
		$view_html->loop('invoice_row');
		//$view_html->parse('INVOICE_ROW_OUT','.invoice_row');
		$amount = round($amount,$const['ARTICLE_PRICE_COMMA_DIGITS']);
		$amount_d = $amount * $discount/100;
		if($apply_discount < 2){
			$amount_d = 0;
		}

		$vat_percent_val = ( $amount - ($amount * ($discount_procent / 100) )) * $db->f('vat')/100;
		$vat_percent[$db->f('vat')] += $vat_percent_val;
		$sub[$db->f('vat')] += $amount;
		$sub_t[$db->f('vat')] += $amount;
		$sub_disc[$db->f('vat')] += $amount_d;
	}

	$total_payments = 0;
	$total_amount_due = $total_amount - $total_payments;




	$i=0;
	$total_value_without_vat = 0;
	$count = 0;
	$ccount = 0;
	foreach ($vat_percent as $key => $val){
		if($sub[$key] - $sub_disc[$key]){
			$ccount++;
		}
	}

	// if($in['custom_type'] && DATABASE_NAME == '3165d928_1094_cded_af44a96d99e2'){
	// 	foreach ($vat_percent as $key => $val){
	// 		if($sub[$key] - $sub_disc[$key]){

	// 			$count++;
	// 			if($sh_discount){
	// 				$height -= 22;
	// 			}else{
	// 				$height -= 11;
	// 			}
	// 			$total_value_without_vat += $sub[$key];

	// 			$view_html->assign(array(
	// 				'vat_percent'   				=> display_number($key),
	// 				'vat_value'	   					=> place_currency(display_number($val),get_commission_type_list($currency_type)),
	// 				'vat_txt'             	=> $labels['vat'],
	// 				'subtotal_txt'        => $labels['subtotal'],
	// 				'sub_value'							=> place_currency(display_number($sub[$key]),get_commission_type_list($currency_type)),
	// 				'disc_value'						=> place_currency(display_number($sub_disc[$key]),get_commission_type_list($currency_type)),
	// 				'nett_value'						=> place_currency(display_number($sub_t[$key] - $sub_disc[$key]),get_commission_type_list($currency_type)),
	// 				'sh_vat'								=> $rm_vat == 1 ? false : true,
	// 				'article_amount' 				=> place_currency(display_number($val + $sub[$key]),get_commission_type_list($currency_type)),
	// 				'hide_vat_row_total'		=> $count <= 1 ? '' : 'hide',
	// 				'show_due_date_vat_pdf'	=> $count == $ccount && SHOW_DUE_DATE_VAT_PDF == 1 ? ( $invoice_due_date_vat > 86400 ? true : false ) : false ,
	// 				'invoice_due_date_vat'	=> date($const['ACCOUNT_DATE_FORMAT'],  $invoice_due_date_vat),
	// 				'due_date_vat_txt'			=> $labels['due_date_vat'],
	// 			),'vat_line');
	// 			$view_html->loop('vat_line');
	// 			$i++;
	// 		}
	// 	}
	// 	$view_html->assign(array(
	// 		'parse_for_custom_pomme' =>	'Cette facture est à payer endéant les 15 jours calendrier sur le compte<br/><span>avec comme communication: '.$serial_number.'.</span><br/><span>Voir condition générales sur la page suivante.</span>',
	// 		));
	// }else {
		foreach ($vat_percent as $key => $val){
			if($sub[$key] - $sub_disc[$key]){

				$count++;
				$height -= 22;
				$total_value_without_vat += $sub[$key];

				$view_html->assign(array(
					'vat_percent'   				=> display_number($key),
					'vat_value'	   					=> place_currency(display_number($val),get_commission_type_list($currency_type)),
					'vat_txt'             	=> $labels['vat'],
					'subtotal_txt'        => $labels['subtotal'],
					'sub_value'							=> place_currency(display_number($sub[$key]),get_commission_type_list($currency_type)),
					'disc_value'						=> place_currency(display_number($sub_disc[$key]),get_commission_type_list($currency_type)),
					'nett_value'						=> place_currency(display_number($sub_t[$key] - $sub_disc[$key]),get_commission_type_list($currency_type)),
					'sh_vat'								=> $rm_vat == 1 ? false : true,
					'article_amount' 				=> place_currency(display_number($val + $sub[$key]),get_commission_type_list($currency_type)),
					'hide_vat_row_total'		=> $count <= 1 ? '' : 'hide',
					'show_due_date_vat_pdf'	=> $count == $ccount && $const['SHOW_DUE_DATE_VAT_PDF'] == 1 ? ( $invoice_due_date_vat > 86400 ? true : false ) : false ,
					'invoice_due_date_vat'	=> date($const['ACCOUNT_DATE_FORMAT'],  $invoice_due_date_vat),
					'due_date_vat_txt'			=> $labels['due_date_vat'],
				),'vat_line');
				$view_html->loop('vat_line');
				$i++;
			}
		}
	// }

	$total_value_without_vat_2percent = $total_value_without_vat*0.02;
	$total_value_without_vat_2percent = place_currency(display_number($total_value_without_vat_2percent));
	$view_html->assign('some_value_8_days', $total_value_without_vat_2percent);
	if($i > 1){
		$view_html->assign(array(
			'hide_row_vat'		=> true,
			'item_w'			=> ($apply_discount ==0 || $apply_discount == 2) ? $width-15 : $width-30,
		));
		//custom width
		$custom_line_1 = true;

	}else{
		$view_html->assign(array(
			'hide_row_vat'		=> false,
		));
		//custom width
		$custom_line_1 = false;
	}


	//custom width
	$custom_width =  get_custom_width($all_custom_widths,$can_be_hidden_custom_widths,$custom_line_1,$custom_line_2);
	$view_html->assign(array(
		'custom_width'			=> $custom_width,
	));
}
$is_table = true;
$is_data = false;

#for layout nr 4. 
if($hide_all == 1){
	$is_table = false;
	$is_data = true;
}else if($hide_all == 2){
	$is_table = true;
	$is_data = false;
}


$view_html->assign(array(
	'is_table'=> $is_table,
	'is_data'=>$is_data,
));

return $view_html->fetch();

/*
ALTER TABLE `tblinvoice` ADD `apply_discount` INT(11) NOT NULL AFTER `not_paid`;
ALTER TABLE `tblinvoice` ADD `due_date_vat` VARCHAR(255) NOT NULL AFTER `apply_discount`;
INSERT INTO `settings` (`constant_name`, `value`, `long_value`, `module`, `type`) VALUES ('ACCOUNT_INVOICE_HEADER_PDF_FORMAT', '1', NULL, '', '1');
INSERT INTO `settings` (`constant_name`, `value`, `long_value`, `module`, `type`) VALUES ('ACCOUNT_INVOICE_BODY_PDF_FORMAT', '1', NULL, '', '1'), ('ACCOUNT_INVOICE_FOOTER_PDF_FORMAT', '1', NULL, '', '1');
INSERT INTO `settings` (`constant_name`, `value`, `long_value`, `module`, `type`) VALUES ('ARTICLE_PRICE_COMMA_DIGITS', '2', NULL, '', '1');
*/
?>
