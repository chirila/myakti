<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
class Api_Edit extends Controller{

	function __construct($in,$db = null)
	{
		parent::__construct($in,$db = null);		
	}

	public function getData(){
		if($this->in['client_id'] == 'tmp'){ //add
			$this->getAddApp();
		}else{ # edit
			$this->getEditApp();
		}
	}

	public function getAddApp(){
		$in = $this->in;
		$output=array(
			"application_name"	=> stripslashes($in['application_name']),
			"redirect_uri"		=> stripslashes($in['redirect_uri']),
			"page_title"		=> gm("Add App"),
		);

		$this->out = $output;
	}

	public function getEditApp(){
		$in = $this->in;
		$apps=$this->db_users->query("SELECT * FROM oauth_clients WHERE client_id='".$in['client_id']."' ");
		$output=array(
			"application_name"	=> stripslashes($apps->f('application_name')),
			"redirect_uri"		=> stripslashes($apps->f('redirect_uri')),
			"client_id"			=> $apps->f("client_id"),
			"client_secret"			=> $apps->f("client_secret"),
			"page_title"		=> gm("Edit App"),
			"is_internal"		=> $apps->f('is_internal') ? true : false
			);

		$this->out = $output;
	}

}

	$apiApp = new Api_Edit($in,$db);

	if($in['xget']){
	    $fname = 'get_'.$in['xget'];
	    $apiApp->output($apiApp->$fname($in));
	}

	$apiApp->getData();
	$apiApp->output();

?>