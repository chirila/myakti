<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }

global $site_url;

global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);

$dbu = new sqldb($db_config);
$db= new sqldb();
$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

/*$user_crm = $dbu->query("SELECT * FROM customers WHERE database_name='".DATABASE_NAME."' ");
$user_crm->next();
$user_crm_a = $dbu->query("SELECT * FROM customer_addresses WHERE customer_id='".$user_crm->f('customer_id')."' AND billing='1' ");
$user_crm_a->next();*/
//$user_data=$dbu->query("SELECT * FROM users WHERE user_id='".$_SESSION['u_id']."' AND default_admin='1' ");
$user_data=$dbu->query("SELECT * FROM users WHERE user_id= :user_id AND default_admin= :default_admin ",['user_id'=>$_SESSION['u_id'],'default_admin'=>'1']);

$result=array(
	'account_vat_number'		=> $user_data->f('valid_vat'),
	'account_billing_address'	=> utf8_encode($user_data->f('ACCOUNT_BILLING_ADDRESS')),
	'account_billing_zip'		=> $user_data->f('ACCOUNT_BILLING_ZIP'),
	'account_billing_city'		=> $user_data->f('ACCOUNT_BILLING_CITY'),
	'ACCOUNT_COUNTRY_DD'		=> build_country_list($user_data->f('ACCOUNT_BILLING_COUNTRY_ID')),
	'account_billing_country_id'	=> $user_data->f('ACCOUNT_BILLING_COUNTRY_ID'),
	//'customer_id'				=> $user_crm->f('customer_id'),
	'name'						=> utf8_encode($user_data->f('ACCOUNT_COMPANY')),
	// 'HIDE_X'					=> in_array(get_country_code($user_crm_a->f('country_id')), $eu_countries) ? '' : 'hide',
	'is_address'				=> $in['type'] == 1 ? true : false,
	'do_next'					=> $in['type'] == 1 ? 'settings--settings-save_payment_details' : 'settings--settings-save_valid_vat'
);



return json_out($result);