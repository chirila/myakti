<?php
	if(!defined('BASEPATH')) exit('No direct script access allowed');
	if($_SESSION['u_id']){
		header("Location: /");
		exit();
	}else{
		$view = new at(ark::$viewpath.'keycloack.html');

		$view->assign(array(
			'do_next'	    => 'auth-isLoged-auth-authorize',
			'auth_code' 	=> $in['code'],
		));

		return $view->fetch();

	}
?>