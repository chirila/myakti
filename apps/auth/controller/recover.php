<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');
$view = new at(ark::$viewpath.'recover.html');

global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db = new sqldb($db_config);

$view->assign('do_next','auth-isLoged-auth-reset');

$messages= msg::get_all_messages();

/*$view->assign(array(
	'msg_error'		=>$messages['error'],
	'BUTTON'	    => 'Sign in',
	'HIDE_INPUT2' 	=> 'hide',
	'HIDE_INPUT3' 	=> '',
	'HIDE_INPUT4' 	=> 'hide',
	'HIDE_L'				=> 'hide',
	'RECOVER'				=> '',
	'CRYPT'					=> '',
	'PASSWORD'			=> isset($_COOKIE["pwd"]) ? $_COOKIE["pwd"] : '',
	'USERNAME'			=> isset($_COOKIE["username"]) ? $_COOKIE["username"] : '',
	'CHECKED'				=> isset($_COOKIE["username"]) ? 'checked':'',
	'rem_user_data'	=> isset($_COOKIE["username"]) ? 'rem_data':'',
	'remember_data'	=> isset($_COOKIE["username"]) ? '1':'0',
	'FORGOT_SCREEN'		=> 'hide',
	'LOGIN_SCREEN'		=> '',
));*/



/*if(isset($in['recover']) && $in['recover'] == 1){*/
	if($in['crypt']){
		
		$pp = base64_decode(strrev($in['crypt']));
		$user = explode(' ',$pp);
		$crypt = $db->field("SELECT reset_request FROM user_info INNER JOIN users ON user_info.user_id = users.user_id WHERE users.username='".$user[0]."' ");
		//var_dump($pp,$user,$crypt , $in['crypt']);exit();
		if($crypt == $in['crypt']){

			$view->assign('do_next','auth-isLoged-auth-reset');

			$view->assign(array(
				'HIDE_INPUT2' 	    => '',
				'HIDE_INPUT' 		=> '',
				'SHOW_STR'			=> 'show_str',
				'HIDE_INPUT3' 	    => 'hide',
				'BUTTON'	  		=> 'Recover your password',
				'USERNAME'			=> $user[0],
				'BUTTON'	  		=> 'Change password',
				'RECOVER'			=> $in['recover'],
				'CRYPT'				=> $in['crypt'],
				'HIDE_L'			=> '',
				'PASSWORD'			=> '',
				'FORGOT_SCREEN'		=> 'hide',
				'LOGIN_SCREEN'		=> '',
			));
		}
	}
/*}*/

$view->assign(array(
	#mesaj
	'FORGOT'	=> isset($in['forget']) ? $in['forget'] : '',
	'PAG'		=> isset($in['pag']) ? $in['pag'] : '',
));

switch (true) {
	case  !empty($_SESSION['l']):
		$info_lang_code = $_SESSION['l'];
		break;
	case  !empty($_COOKIE["l"]):
		$info_lang_code = $_COOKIE["l"];
		break;
	default:
		$info_lang_code = LANGUAGE_TR;
		break;
}

$allowd = array('nl','en','fr');
if(!in_array($info_lang_code, $allowd)){
	$info_lang_code = 'en';
}

switch ($info_lang_code) {
	case 'en':

		$banner_link = 'https://akti.com/lp/service-premium/';
		$banner_img = 'en';
		break;
	case 'fr':
		$banner_link = 'https://akti.com/fr_BE/lp/service-premium/';
		$banner_img = 'fr';
		break;

	case 'nl':
		$banner_link = 'https://akti.com/nl_BE/lp/service-premium/';
		$banner_img = 'nl';
		break;
	default:
		$banner_link = 'https://akti.com/lp/service-premium/';
		$banner_img = 'en';
		break;
}

/*
$response=array(
	'username'				=> $in['username'],
	'password'				=> '',
	'password2'				=> '',
	'banner'				=> $banner_link,
	'lang'					=> $banner_img,
	'crypt'					=> $in['crypt']
	);*/
/*$view->assign(array(
	'no_float'					=> $is_info == false ? '': '',
	'is_banner_no_info'			=> $is_info == false ? true: false,
	'is_banner_and_info'		=> $is_info == false ? false: true,
	'banner_img'			=> $banner_img,
	'banner_link'			=> $banner_link,
	

	'down_app_store'			=> 'down_app_store_en',
	'down_google'				=> 'down_google_en',
	'down_app_store_erp'		=> 'down_app_store_erp_en',
	'is_maintenance'			=> $is_maintenance,

));*/
return $view->fetch();

/*switch (true) {
	case  !empty($_SESSION['l']):
		$info_lang_code = $_SESSION['l'];
		break;
	case  !empty($_COOKIE["l"]):
		$info_lang_code = $_COOKIE["l"];
		break;
	default:
		$info_lang_code = LANGUAGE_TR;
		break;
}

$allowd = array('nl','en','fr');
if(!in_array($info_lang_code, $allowd)){
	$info_lang_code = 'en';
}

switch ($info_lang_code) {
	case 'en':

		$banner_link = 'https://akti.com/lp/service-premium/';
		$banner_img = 'en';
		break;
	case 'fr':
		$banner_link = 'https://akti.com/fr_BE/lp/service-premium/';
		$banner_img = 'fr';
		break;

	case 'nl':
		$banner_link = 'https://akti.com/nl_BE/lp/service-premium/';
		$banner_img = 'nl';
		break;
	default:
		$banner_link = 'https://akti.com/lp/service-premium/';
		$banner_img = 'en';
		break;
}

$response=array(
	'username'				=> $in['username'],
	'password'				=> '',
	'password2'				=> '',
	'banner'				=> $banner_link,
	'lang'					=> $banner_img,
	'crypt'					=> $in['crypt']
	);

json_out($response);
*/