<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');
$view = new at(ark::$viewpath.'new_account.html');

global $database_config,$config;

$messages= msg::get_all_messages();

	if(!$in['duo']){
		msg::error('Error','error');
		//json_out($in);
		return false;
	}

	global $database_config;
	$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$user_db = new sqldb($database_2);

	//$crypt_data=$user_db->field("SELECT id FROM new_user WHERE crypt='".$in['duo']."' AND done='0' ");
	$crypt_data=$user_db->field("SELECT id FROM new_user WHERE crypt= :crypt AND done= :done ",['crypt'=>$in['duo'],'done'=>'0']);
	if(!$crypt_data){
		msg::error('Error','error');
		//json_out($in);
		return false;
	}

	$first=strrev($in['duo']);
	$second=base64_decode($first);
	$third = explode(' ', $second);
	switch($third[1]){
		case '1':
			$info_lang_code='nl';
			$_SESSION['l']='nl';
			break;
		case '3':
			$info_lang_code='fr';
			$_SESSION['l']='fr';
			break;
		case '4':
			$info_lang_code='de';
			$_SESSION['l']='de';
			break;
		default:
			$info_lang_code='en';
			$_SESSION['l']='en';
			break;
	}

	switch ($info_lang_code) {
		case 'en':
			$banner_link = 'https://akti.com/lp/service-premium/';
			$banner_img = 'en';
			break;
		case 'fr':
			$banner_link = 'https://akti.com/fr_BE/lp/service-premium/';
			$banner_img = 'fr';
			break;
		case 'nl':
			$banner_link = 'https://akti.com/nl_BE/lp/service-premium/';
			$banner_img = 'nl';
			break;
		default:
			$banner_link = 'https://akti.com/lp/service-premium/';
			$banner_img = 'en';
			break;
	}

	$view->assign('do_next','auth-new_account-auth-newAccount');

	$view->assign(array(
	'username'				=> $third[0],
	'lang_id'				=> $third[1],
	'password'				=> '',
	'password2'				=> '',
	'banner'				=> $banner_link,
	'lang'					=> $banner_img,
	'crypt'					=> $in['duo'],
	'accountant_id'			=> count($third)>2 ? $third[2] : '0',
	'easy_invoice'			=> count($third)>3 ? true : false,
	'ok'					=> true,
	'msg_error'				=>!($messages['msg']['firstname'] && $messages['msg']['lastname'] && $messages['msg']['username'] && 
								$messages['msg']['password']  && $messages['msg']['company'] && $messages['msg']['phone'])? $messages['error'] :'',
	'msg_success'			=>$messages['success'],
	'error_firstname'       => $messages['msg']['firstname'],
	'error_lastname'        => $messages['msg']['lastname'],
	'error_username'        => $messages['msg']['username'],
	'error_password'        => $messages['msg']['password'],
	'error_password2'       => $messages['msg']['password2'],
	'error_company'         => $messages['msg']['company'],
	'error_phone'         	=> $messages['msg']['phone'],
));
/* if($messages['error']){
 		var_dump($messages);exit();
 }*/


return $view->fetch();

	/*$response=array(
		'username'				=> $third[0],
		'lang_id'				=> $third[1],
		'password'				=> '',
		'password2'				=> '',
		'banner'				=> $banner_link,
		'lang'					=> $banner_img,
		'crypt'					=> $in['duo'],
		'accountant_id'			=> count($third)>2 ? $third[2] : '0',
		'easy_invoice'			=> count($third)>3 ? true : false,
		'ok'					=> true,
		);*/

	//json_out($response);
?>
