<?php
global $config, $database_config;
$response = array('version'=>$config['version'], 'show_banner'=>false, 'banner_message'=>'');

$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
	);

$db_u = new sqldb($database_users);

$response['show_banner'] =$db_u->field("SELECT active FROM banner_message WHERE msg_id= :msg_id ",['msg_id'=>'1']);
if($response['show_banner']){
	$lang = $_SESSION['l']? $_SESSION['l']:en ;
	$banner_message = $db_u->field("SELECT ".$lang." FROM banner_message WHERE msg_id= '1' ");
	$response['banner_message'] =htmlspecialchars_decode(utf8_encode(stripslashes($banner_message)));

}

json_out($response);