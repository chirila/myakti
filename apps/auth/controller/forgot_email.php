<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$view = new at(ark::$viewpath.'forgot_email.html');
global $database_config,$config;
// $view = new at();
$database_2 = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$user_db = new sqldb($database_2);

//$user1 = $user_db->field("SELECT username FROM users WHERE username='".$in['username']."' OR email='".$in['email']."' ");
$user1 = $user_db->field("SELECT username FROM users WHERE username= :username OR email= :email ",['username'=>$in['username'],'email'=>$in['email']]);
//$in['lang_id'] = $user_db->field("SELECT lang_id FROM users WHERE username='".$in['username']."' OR email='".$in['email']."' ");
$in['lang_id'] = $user_db->field("SELECT lang_id FROM users WHERE username= :username OR email= :email ",['username'=>$in['username'],'email'=>$in['email']]);
$site_url = $config['site_url'];
$crypt = base64_encode($user1.' '.$in['email'].' '.$in['lang_id'].' '.$in['group_id']);
$crypt = strrev($crypt);

if($_SESSION['l']=='nl' || $in['lang_id']==1){
	$mail_id = 'reset_nl';
	$subject_id = 'subject_nl';
}elseif($_SESSION['l']=='en' || $in['lang_id']==2){
	$mail_id = 'reset_en';
	$subject_id = 'subject_en';
}elseif($_SESSION['l']=='fr' || $in['lang_id']==3){
	$mail_id = 'reset_fr';
	$subject_id = 'subject_fr';
}elseif($_SESSION['l']=='ge' || $in['lang_id']==4){
	$mail_id = 'reset_ge';
	$subject_id = 'subject_ge';
}



$mail_content = $user_db->field("SELECT ".$mail_id ." FROM reset_password WHERE reset_id='1'");
$mail_subject = $user_db->field("SELECT ".$subject_id ." FROM reset_password WHERE reset_id='1'");



$site_url = $config['site_url'];
$crypt = base64_encode($user1.' '.time());
$crypt = strrev($crypt);

//$site = $site_url.'recover/'.$crypt.'?'.$_SESSION['l'].'?'.$user1;
$site = $site_url.'index.php?do=auth-recover&crypt='.$crypt;
$view->assign(array(
    'first_name'    => $user1,
    'link'		 	=> $site,
    'site_url'		=> $site_url,
    'content'		=> $mail_content
));

return $view->fetch();
