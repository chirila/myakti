<?php
global $database_config;
$response = array('isLoggedIn'=>false);
if($in['do']=='auth-isLoged-auth-logout'){
	json_out($response);
}elseif($_SESSION['u_id']){
	$response['isLoggedIn'] = true;
	$response['name'] = get_user_name($_SESSION['u_id']);
	$response['group'] = $_SESSION['group'];
	$response['adminSetting'] = array();
	if(isset($_SESSION['admin_sett'])){
		foreach ($_SESSION['admin_sett'] as $key => $value) {
			array_push($response['adminSetting'],$value);
		}
	}
	$db_config = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	


	$db_users = new sqldb($db_config);
	//$db=new sqldb();
	//$data_name = $db_users->field("SELECT database_name FROM users WHERE user_id='".$_SESSION['u_id']."' ");
	$data_name = $db_users->field("SELECT database_name FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);

	$db_user = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $data_name,
	);
	$db=new sqldb($db_user);

	$is_new_subscription = $db->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION' ");
	$WIZZARD = $db->field("SELECT value FROM settings WHERE constant_name='WIZZARD_COMPLETE' ");

	$response['must_pay']=false;
	$showGdpr=false;
	//$user_info = $db_users->query("SELECT user_info.*, users.active,users.two_factor,users.gdpr_status FROM user_info JOIN users ON user_info.user_id = users.user_id WHERE user_info.user_id='".$_SESSION['u_id']."' ");
	$user_info = $db_users->query("SELECT user_info.*, users.active,users.two_factor,users.gdpr_status FROM user_info JOIN users ON user_info.user_id = users.user_id WHERE user_info.user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	if(time()>$user_info->f('end_date') && ($user_info->f('is_trial') =='0' || $user_info->f('active') =='2') ){

		$response['must_pay']=true;
		if(is_null($is_new_subscription)){
			//$response['pag']='subscription';
			$response['pag']='subscription_new';
		}else{
			$response['pag']='subscription_new';
		}
		if(!$user_info->f('gdpr_status') || $user_info->f('gdpr_status')==2){
			$showGdpr=true;
		}
	}
	//$group_id=$db_users->field("SELECT group_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");
	$group_id=$db_users->field("SELECT group_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	//$credential=$db_users->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
	$credential=$db_users->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	$credentials=explode(';', $credential);
	if($group_id==2 || (in_array('1', $credentials) === false)){
		$response['default_pag']='calendar';
		if(!$user_info->f('gdpr_status') || $user_info->f('gdpr_status')==2){
			$showGdpr=true;
		}
	}else{
		$is_admin_company=aktiUser::get('is_admin_company');
        $select_company=false;
        if($is_admin_company=='company' && $_SESSION['group']=='user'){
            //
        }else if($_SESSION['group']=='user'){
            $select_company=true;
        }
		$response['default_pag']=$select_company ? 'customers' : 'dash';
		if(!$user_info->f('gdpr_status') || $user_info->f('gdpr_status')==2){
			$showGdpr=true;
		}
	}
	if($WIZZARD=='0'){
		$response['default_pag']='board';
		$showGdpr=false;
	}
	//$easyinvoice=$db->field("SELECT value FROM settings WHERE `constant_name`='EASYINVOICE' ");
	$easyinvoice=$db->field("SELECT value FROM settings WHERE `constant_name`= :constant_name ",['constant_name'=>'EASYINVOICE']);
	if($easyinvoice=='1'){
		$response['default_pag']='invoices';
		if(!$user_info->f('gdpr_status') || $user_info->f('gdpr_status')==2){
			$showGdpr=true;
		}
	}
	
	$old_default_pag=$response['default_pag'];
	$response['two_done']=true;
	if($user_info->f('two_factor')){
		$response['two_done']=false;
		if($_SESSION['logedAsAdmin']){
			$response['two_done']=true;
			$_SESSION['two_done']=true;
		}else if($_COOKIE["trust".$_SESSION['u_id']] && $_COOKIE["trust".$_SESSION['u_id']]!='canci'){
			//$user_trust=$db_users->field("SELECT id FROM user_trust WHERE user_id='".$_SESSION['u_id']."' AND trust_code='".$_COOKIE["trust".$_SESSION['u_id']]."' ");
			$user_trust=$db_users->field("SELECT id FROM user_trust WHERE user_id= :user_id AND trust_code= :trust_code ",['user_id'=>$_SESSION['u_id'],'trust_code'=>$_COOKIE["trust".$_SESSION['u_id']]]);
			if($user_trust){
				$response['two_done']=true;
		    	$_SESSION['two_done']=true;
		    }
		}else if($_SESSION['two_done']===true){
			$response['two_done']=true;
		}else if($_SESSION['two_done']===false){
			$response['default_pag']='login_two';
			$showGdpr=false;
		}
	}
	if($_SESSION['remember_url']){
		$response['default_pag']=$_SESSION['remember_url'];
		$response['remember_url']=true;
	}
	$response['showGdpr']=$showGdpr;
	
//}
//var_dump($response);exit();
	if($_SESSION['initial_login']){
		unset($_SESSION['initial_login']);
		return true;
	}else{
		json_out($response);
	}


}else{
	if(!defined('BASEPATH')) exit('No direct script access allowed');
$view = new at(ark::$viewpath.'login.html');

//global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db = new sqldb($db_config);

$view->assign('do_next','auth-isLoged-auth-login');

$messages= msg::get_all_messages();

$view->assign(array(
	'msg_error'		=>$messages['error'],
	'msg_success'	=>$messages['success'],
	'BUTTON'	    => 'Sign in',
	'HIDE_INPUT2' 	=> 'hide',
	'HIDE_INPUT3' 	=> '',
	'HIDE_INPUT4' 	=> 'hide',
	'HIDE_L'				=> 'hide',
	'RECOVER'				=> '',
	'CRYPT'					=> '',
	'PASSWORD'			=> isset($_COOKIE["pwd"]) ? $_COOKIE["pwd"] : '',
	'USERNAME'			=> isset($_COOKIE["username"]) ? $_COOKIE["username"] : '',
	'CHECKED'				=> isset($_COOKIE["username"]) ? 'checked':'',
	'rem_user_data'	=> isset($_COOKIE["username"]) ? 'rem_data':'',
	//'remember_data'	=> isset($_COOKIE["username"]) ? '1':'0',
	'remember'	=> isset($_COOKIE["username"]) ? '1':'0',
	'FORGOT_SCREEN'		=> 'hide',
	'LOGIN_SCREEN'		=> '',
));


if(isset($in['forget']) && $in['forget'] == 1){

	$view->assign('do_next','auth-isLoged-auth-reset_pass');

	$view->assign(array(
		'HIDE_INPUT' 	=> 'hide',
		'HIDE_INPUT2' => 'hide',
		'HIDE_INPUT3' => 'hide',
		'HIDE_INPUT4' => '',
		'BUTTON'	  	=> 'Recover your password',
		'HIDE_L'			=> '',
		'PASSWORD'		=> '',
		'USERNAME'		=> '',
		'FORGOT_SCREEN'		=> 'hide',
		'LOGIN_SCREEN'		=> '',
	));
}
if(isset($in['recover']) && $in['recover'] == 1){
	if($in['crypt']){
		$pp = base64_decode(strrev($in['crypt']));
		$user = explode(' ',$pp);
		$crypt = $db->field("SELECT reset_request FROM user_info INNER JOIN users ON user_info.user_id = users.user_id WHERE users.username='".$user[0]."' ");
		if($crypt == $in['crypt']){

			$view->assign('do_next','auth-isLoged-auth-reset');

			$view->assign(array(
				'HIDE_INPUT2' 	    => '',
				'HIDE_INPUT' 		=> '',
				'SHOW_STR'			=> 'show_str',
				'HIDE_INPUT3' 	    => 'hide',
				'BUTTON'	  		=> 'Recover your password',
				'USERNAME'			=> $user[0],
				'BUTTON'	  		=> 'Change password',
				'RECOVER'			=> $in['recover'],
				'CRYPT'				=> $in['crypt'],
				'HIDE_L'			=> '',
				'PASSWORD'			=> '',
				'FORGOT_SCREEN'		=> 'hide',
				'LOGIN_SCREEN'		=> '',
			));
		}
	}
}

if(isset($in['reset_pass']) && $in['reset_pass'] == 1){
			$view->assign(array(
				'FORGOT_SCREEN'		=> '',
				'LOGIN_SCREEN'		=> 'hide',
				'email'			=> $in['email'],
			));
	}



$view->assign(array(
	#mesaj
	'FORGOT'	=> isset($in['forget']) ? $in['forget'] : '',
	'PAG'		=> isset($in['pag']) ? $in['pag'] : '',
));

//update information for login page

$allowd = array('nl','en','fr');

if(!empty($in['lang'])){
	if(in_array($in['lang'], $allowd)){
		$_SESSION['l'] = $in['lang'];
	}	
}else{
	$_SESSION['l'] = $in['source'] && $in['source'] == 'mcf' ? 'fr' : substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
}

switch (true) {
	case  !empty($_SESSION['l']):
		$info_lang_code = $_SESSION['l'];
		break;
	case  !empty($_COOKIE["l"]):
		$info_lang_code = $_COOKIE["l"];
		break;
	default:
		$info_lang_code = LANGUAGE_TR;
		break;
}


if(!in_array($info_lang_code, $allowd)){
	$info_lang_code = 'en';
}

switch ($info_lang_code) {
	case 'en':

		$banner_link = 'https://akti.com/lp/service-premium/';
		$banner_img = 'en';
		break;
	case 'fr':
		$banner_link = 'https://akti.com/fr_BE/lp/service-premium/';
		$banner_img = 'fr';
		break;

	case 'nl':
		$banner_link = 'https://akti.com/nl_BE/lp/service-premium/';
		$banner_img = 'nl';
		break;
	default:
		$banner_link = 'https://akti.com/lp/service-premium/';
		$banner_img = 'en';
		break;
}

$title['nl'] = 'Gepland Onderhoud';

$text['nl'] = "OPGELET! Vandaag 30/07, vanaf 20u CEST, zal de toegang tot Akti even onderbroken worden om een belangrijke update te installeren. Onze excuses voor het ongemak. Wanneer we weer online zijn, kan u profiteren van de vereenvoudigde navigatie, de vernieuwde layout voor de CRM en... Aankoopfacturen!";

$title['en'] ='Planned Maintenance';

$text['en'] = "ATTENTION! Today, 30/07 from 8:00 PM CEST onwards, access to Akti will be interrupted for a short while, in order to install an important update. We apologise for any inconvenience. When we're back online, you will have access to our simplified navigation, our new CRM layout and... Purchase Invoices! ";

$title['fr'] = 'Entretien planifié';

$text['fr'] = "ATTENTION: Le 30/07 à partir de 20h CEST, l'équipe Akti effectuera une mise à jour importante. L'accès à l'application sera temporairement interrompu. Veuillez nous excuser pour ce désagrément. Dès la remise en ligne, vous pourrez profiter des nouvelles fonctionnalités, comme une navigation simplifiée, une nouvelle presentation du CRM, et ... des factures d'achat !";



$is_info = false;
$show_update_info = $db->field("SELECT value FROM user_meta WHERE name = 'show_on_gen_page' ");

if($show_update_info==1){
	$j = 0;

$now = time();

	$update_info = $db->query("SELECT ".$info_lang_code.", show_more_link_".$info_lang_code.", up_date, update_id FROM update_information WHERE show_on_gen_page = '1' AND up_date<'".$now."' ORDER BY up_date DESC,update_id DESC LIMIT 3");
	while($update_info->next()){
		$up_date = date("j F, Y",$update_info->f('up_date'));
		$up_info = utf8_encode($update_info->f($info_lang_code));
		$up_id = $update_info->f('update_id');

		$view->assign(array(
			'up_info'		=> $up_info,
			'show_more_link'=> '<a class="show_more" href="index.php?do=auth-news&up_id=div_'.$up_id.'">'.gm('Learn more').'</a>',
			'up_date'		=> $up_date,
			'update_id'		=> $up_id,
			'first_day'		=> (time() >($update_info->f('up_date')+(60*60*24))) ? '' : 'first_day',
		),'update_info');
		$view->loop('update_info');
		$j++;
	}
	if($j>0){
		$is_info=true;
	}
}

$view->assign(array(
	'is_info'				=> $is_info,
	'update_info_login'		=> $is_info == true ? 'update_info_login' : '',
	'no_float'				=> $is_info == false ? 'style="float:none;"': '',
	'title' => $title[$info_lang_code],
	'text' => $text[$info_lang_code],
));

//for the little banner on login page

switch ($info_lang_code) {
	case 'en':
		// $banner_link = 'https://itunes.apple.com/gb/app/salesassist-light-erp/id904187813?mt=8';
		$banner_link_app_store = 'https://itunes.apple.com/gb/app/salesassist/id642313350?mt=8';
		$banner_link_google = 'https://play.google.com/store/apps/details?id=eu.salesassist.timetracker';
		$banner_link_erp = 'https://itunes.apple.com/gb/app/salesassist-light-erp/id904187813?mt=8';
		break;
	case 'fr':
		// $banner_link = 'https://itunes.apple.com/fr/app/salesassist-light-erp/id904187813?mt=8';
		$banner_link_app_store = 'https://itunes.apple.com/fr/app/salesassist/id642313350?mt=8';
		$banner_link_google = 'https://play.google.com/store/apps/details?id=eu.salesassist.timetracker';
		$banner_link_erp = 'https://itunes.apple.com/fr/app/salesassist-light-erp/id904187813?mt=8';
		break;
	case 'nl':
		// $banner_link = 'https://itunes.apple.com/nl/app/salesassist-light-erp/id904187813?mt=8';
		$banner_link_app_store = 'https://itunes.apple.com/nl/app/salesassist/id642313350?mt=8';
		$banner_link_google = 'https://play.google.com/store/apps/details?id=eu.salesassist.timetracker';
		$banner_link_erp = 'https://itunes.apple.com/nl/app/salesassist-light-erp/id904187813?mt=8';
		break;
	case 'de':
		// $banner_link = 'https://itunes.apple.com/de/app/salesassist-light-erp/id904187813?mt=8';
		$banner_link_app_store = 'https://itunes.apple.com/de/app/salesassist/id642313350?mt=8';
		$banner_link_google = 'https://play.google.com/store/apps/details?id=eu.salesassist.timetracker';
		$banner_link_erp = 'https://itunes.apple.com/de/app/salesassist-light-erp/id904187813?mt=8';
		break;
	default:
		// $banner_link = 'https://itunes.apple.com/gb/app/salesassist-light-erp/id904187813?mt=8';
		$banner_link_app_store = 'https://itunes.apple.com/gb/app/salesassist/id642313350?mt=8';
		$banner_link_google = 'https://play.google.com/store/apps/details?id=eu.salesassist.timetracker';
		$banner_link_erp = 'https://itunes.apple.com/gb/app/salesassist-light-erp/id904187813?mt=8';
		break;
}

$is_maintenance = false;
// if($_SERVER['REMOTE_ADDR'] != '5.2.200.43'){
// 	$is_maintenance = true;
// }
$view->assign(array(
	'no_float'					=> $is_info == false ? '': '',
	'is_banner_no_info'			=> $is_info == false ? true: false,
	'is_banner_and_info'		=> $is_info == false ? false: true,
	'banner_img'			=> $banner_img,
	'banner_link'			=> $banner_link,
	

	'down_app_store'			=> 'down_app_store_en',
	'down_google'				=> 'down_google_en',
	'down_app_store_erp'		=> 'down_app_store_erp_en',
	'is_maintenance'			=> $is_maintenance,
	'source'					=> $in['source'] ? $in['source']:'',
	'source_param'				=> $in['source'] ? '&source='.$in['source']:'',

));
return $view->fetch();
}