<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
$view = new at(ark::$viewpath.'create_account.html');

global $database_config,$config;

$messages= msg::get_all_messages();

$database_2 = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$user_db = new sqldb($database_2);
switch (true) {
	case  !empty($_SESSION['l']):
		$info_lang_code = $_SESSION['l'];
		break;
	case  !empty($_COOKIE["l"]):
		$info_lang_code = $_COOKIE["l"];
		break;
	default:
		$info_lang_code = LANGUAGE_TR;
		break;
}

$allowd = array('nl','en','fr');
if(!in_array($info_lang_code, $allowd)){
	$info_lang_code = 'en';
}

switch ($info_lang_code) {
	case 'en':

		$banner_link = 'https://akti.com/lp/service-premium/';
		$banner_img = 'en';
		$trial_link = 'https://akti.com/inscription/';
		break;
	case 'fr':
		$banner_link = 'https://akti.com/fr_BE/lp/service-premium/';
		$banner_img = 'fr';
		$trial_link = 'https://akti.com/fr/inscription/';
		break;

	case 'nl':
		$banner_link = 'https://akti.com/nl_BE/lp/service-premium/';
		$banner_img = 'nl';
		$trial_link = 'https://akti.com/nl/inscription/';
		break;
	default:
		$banner_link = 'https://akti.com/lp/service-premium/';
		$banner_img = 'en';
		$trial_link = 'https://akti.com/inscription/';
		break;
}
// print_r($in);
//$in['duo'] = explode(';', $in['duo']);
$first=strrev($in['duo']);
$second=base64_decode($first);
//$third = explode(' ', $second);
$email_matches=preg_match_all('/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i', $second, $matches);
$email_pos=strpos($second,$matches[0][count($matches[0])-1]);
$third=explode(' ',substr($second,$email_pos));
array_unshift($third, 'begin');

$now =mktime();
//$time = $user_db->field("SELECT time_email FROM user_email WHERE crypt='".$in['duo']."'");
$time = $user_db->field("SELECT time_email FROM user_email WHERE crypt= :crypt",['crypt'=>$in['duo']]);

if($time>$now){
	$expired=true;
}else{
	$expired=false;
}
//$original_user=$user_db->field("SELECT user_id FROM user_email WHERE crypt='".$in['duo']."'");
$original_user=$user_db->field("SELECT user_id FROM user_email WHERE crypt= :crypt",['crypt'=>$in['duo']]);



//$database = $user_db->field("SELECT database_name FROM users WHERE username LIKE '%".addslashes($third[0])."%'");
$database = $user_db->field("SELECT database_name FROM users WHERE user_id='".$original_user."' ");
$in['lang_id']='';

$view->assign('do_next','auth-create_account-auth-create');

$view->assign(array(
	'LANGUAGE_DD'			=> build_language_dd_user($third[2]),
	'username'				=> $third[1],
	'lang_id'				=> $third[2],
	'group_id'				=> $third[3] ? $third[3] : '',
	'database'				=> $database,
	'lang_id'				=> $third[2],
	'password'				=> '',
	'password2'				=> '',
	'banner'				=> $banner_link,
	'lang'					=> $banner_img,
	'crypt'					=> $in['duo'],
	'expired'				=> $expired,
	'is_expired'			=> $expired? 'hide':'',
	'is_not_expired'		=> $expired? '':'hide',
	'trial_link'			=> $trial_link,
	'msg_error'				=>!($messages['msg']['firstname'] && $messages['msg']['lastname'] && $messages['msg']['username'] && 
								$messages['msg']['password'] && $messages['msg']['password2'])? $messages['error'] :'',
	'msg_success'			=>$messages['success'],
	'error_firstname'       => $messages['msg']['firstname'],
	'error_lastname'        => $messages['msg']['lastname'],
	'error_username'        => $messages['msg']['username'],
	'error_password'        => $messages['msg']['password'],
	'error_password2'        => $messages['msg']['password2'],
));

return $view->fetch();

/*$response=array(
	'LANGUAGE_DD'			=> build_language_dd_user($third[2]),
	'username'				=> $third[1],
	'lang_id'				=> $third[2],
	'group_id'				=> $third[3] ? $third[3] : '',
	'database'				=> $database,
	'lang_id'				=> $third[2],
	'password'				=> '',
	'password2'				=> '',
	'banner'				=> $banner_link,
	'lang'					=> $banner_img,
	'crypt'					=> $in['duo'],
	'expired'				=> $expired
	);

json_out($response);
*/