<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');
	$view = new at(ark::$viewpath.'first.html');

	global $database_config,$config;

	if(!$in['crypt']){
		msg::error('Invalid Code','error');
		$messages= msg::get_all_messages();
		$view->assign(array('msg_error'=>$messages['error'],'disabled'=>'disabled'));		
		return $view->fetch();
	}

	global $database_config;
	$database_2 = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$user_db = new sqldb($database_2);

	$crypt_data=$user_db->query("SELECT * FROM activation_user WHERE crypt= :crypt AND done= :done ",['crypt'=>$in['crypt'],'done'=>'0'])->getAll();
	if(empty($crypt_data)){
		msg::error('Invalid Code','error');
		$messages= msg::get_all_messages();
		$view->assign(array('msg_error'=>$messages['error'],'disabled'=>'disabled="disabled"'));		
		return $view->fetch();
	}

	$crypt_info=$crypt_data[0];
	$user_data=unserialize($crypt_info['info']);

	$messages= msg::get_all_messages();

	//var_dump($messages);

	switch ($user_data['lang_id']) {
			case '1':		
				$link1 = 'https://support.akti.com/hc/nl/articles/360000151329-Gebruikersovereenkomst-voor-Akti';
				$link2 = 'https://support.akti.com/hc/nl/articles/360001239289-Privacy-Policy';
			break;
			case '2':
				$link1 = 'https://support.akti.com/hc/nl/articles/360000151329-Gebruikersovereenkomst-voor-Akti';
				$link2 = 'https://support.akti.com/hc/nl/articles/360001239289-Privacy-Policy';
			break;
			case '3':
				$link1 = 'https://support.akti.com/hc/fr/articles/360000151329--Convention-d-utilisation-pour-Akti-';
				$link2 = 'https://support.akti.com/hc/fr/articles/360001239289-Privacy-Policy';
			break;
			default:
				$link1 = 'https://support.akti.com/hc/nl/articles/360000151329-Gebruikersovereenkomst-voor-Akti';
				$link2 = 'https://support.akti.com/hc/nl/articles/360001239289-Privacy-Policy';
			break;
		}

	$view->assign(array(
		'username'				=> $user_data['email'],
		'lang_id'				=> $user_data['lang_id'],
		'company'				=> $user_data['company'],
		'firstname'				=> $user_data['firstname'],
		'lastname'				=> $user_data['lastname'],
		'phone'					=> $user_data['phone'],
		'password'				=> '',
		'password2'				=> '',
		'crypt'					=> $in['crypt'],
		'ok'					=> true,
		// 'msg_error'				=>!($messages['msg']['firstname'] && $messages['msg']['lastname'] && $messages['msg']['username'] && 
		// 							$messages['msg']['password']  && $messages['msg']['company'] && $messages['msg']['phone'])? $messages['error'] :'',
		'msg_error' 			=> (!isset($messages['msg']) && $messages['error'] )? $messages['error'] :'',
		'msg_success'			=>$messages['success'],
		'error_firstname'       => $messages['msg']['firstname'],
		'error_lastname'        => $messages['msg']['lastname'],
		'error_username'        => $messages['msg']['username'],
		'error_password'        => $messages['msg']['password'],
		'error_password2'       => $messages['msg']['password2'],
		'error_company'         => $messages['msg']['company'],
		'error_phone'         	=> $messages['msg']['phone'],
		'do_next'				=> 'auth-first-auth-firstActivation',
		'agree_checkbox'		=> '1',
		'disabled'				=> 'disabled',
		'link1'					=> $link1,
		'link2'					=> $link2,
));


return $view->fetch();

?>
