<?php
/************************************************************************
* @Author: MedeeaWeb Works                                                   *
************************************************************************/
class auth
{
/****************************************************************
* function login(&$in)                                          *
****************************************************************/

public function __construct(){
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$this->db = new sqldb($database_users);
}

function login(&$in)
{
	global $user_level;
	global $config;
	$href = $config['site_url'];

	//de aici
/*	if($in['client']){
		$new_client = strrev($in['client']);
		$new_client = base64_decode($new_client);
		$check_new_user = $this->db->field("SELECT username FROM users WHERE check_new_user = '1' AND username = '".$new_client."' ");
		if($check_new_user==$new_client){
			$this->db->query("UPDATE users SET check_new_user = '0' WHERE username = '".$new_client."' AND check_new_user = '1' ");
			$db = $this->db->query("SELECT password, user_id, main_user_id, user_role, database_name, lang.code, group_id
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE username = '".$check_new_user."'");
			if($db->move_next()){
			 	session_unset();
				session_start();
		    $_SESSION['first_time_user'] = '1';
		    $_SESSION['triales'] = 1;
		   	$this->set_session($db,$in);
		   	$q = $this->db->query("UPDATE user_info SET chargify_user_id='".time()."' WHERE user_id = '".$_SESSION['u_id']."' ");
		   	$t = (strtotime("midnight")+date('Z'))*1000;
		   	doManageLog('Last activity inserted.',$db->f('database_name'),array( array("property"=>'last_login',"value"=>$t) ));
			 	header("Location: ".$href."?do=home-account_details");
				return true;
			}
		}else{
			return false;
		}
	}*/
	//pana aici

	if(!$this->try_this($in)){
		return false; # we authenticate the easy way
	}

	//$this->try_this_G($in);
	$v = new  validation($in);
	$v->field('username',gm('Username'),'required',gm('Please fill the username').'<br>');
	$v->field('password',gm('Password'),'required',gm('Please fill the password'));

	if(!$v->run()){
		return false;
	}

    // $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);

    /*$db = $this->db->query("SELECT password, user_id, main_user_id, user_role, database_name, lang.code, group_id,login_tmps,login_try,two_factor,users.active
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE username = '".$in['username']."'");*/
	$db = $this->db->query("SELECT password, user_id, main_user_id, user_role, database_name, lang.code, group_id,login_tmps,login_try,two_factor,users.active
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE username = :username",['username'=>$in['username']]);    			

    if($db->move_next()){

    	$check_if_db_exists = $this->db->query("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '".$db->f('database_name')."'")->getAll();
    	if(empty($check_if_db_exists)){
    		msg::$error .= gm("Wrong email address or password. Try again.");
    		return false;
    		//json_out($in);
    	}else if($db->f('database_name')=='445e3208_dd94_fdd4_c53c135a2422'){
    		msg::$error .= "Please go to cozie.akti.com";
    		return false;
    		//json_out($in);
    	}

    	$t = $db->f('login_tmps')+30*60;
    	if($t < time() ){
    		/*$this->db->query("UPDATE users SET
        	 	 login_tmps='0',
        	 	 login_try='0'                
    			WHERE username = '".$in['username']."'");*/
    		$this->db->query("UPDATE users SET
        	 	 login_tmps= :login_tmps,
        	 	 login_try= :login_try                
    			WHERE username = :username",
    			['login_tmps'=>'0',
        	 	 'login_try'=>'0',                
    			 'username' => $in['username']]
    		);
    	}
    	elseif($db->f('login_try')>=10){
    		msg::$error .= gm("Please try again in 30 minutes");
    		return false;
    	}

    	if(md5($in['password']) == $db->f('password'))
        {
            session_destroy();
			session_start();
        	session_regenerate_id();

        	if(!$this->check_user($db->f('user_id'),$in)){
        		return false;
        	}
        	$this->set_session($db,$in);
        	/*var_dump($in);exit();*/
	        if($in['remember']){
	        	setcookie("username",$in['username'],time()+60*60*24*365);
	        	setcookie("pwd",$in['password'],time()-60*60*24*365);
	        }else{
	        	setcookie("username",$in['username'],time()-60*60*24*365);
	        }

	        $this->db->query("UPDATE users SET
        	 	 login_tmps= :login_tmps,
        	 	 login_try= :login_try                
    			WHERE username = :username",
    			['login_tmps'=>'0',
        	 	 'login_try'=>'0',                
    			 'username' => $in['username']]
    		);

            //$q = $this->db->query("UPDATE user_info SET chargify_user_id='".time()."' WHERE user_id = '".$_SESSION['u_id']."' ");
            $q = $this->db->query("UPDATE user_info SET chargify_user_id= :chargify_user_id WHERE user_id = :user_id ",['chargify_user_id'=>time(),'user_id'=>$_SESSION['u_id']]);
            $t = (strtotime("midnight")+date('Z'))*1000;
            doManageLog('Last activity inserted.',$db->f('database_name'),array( array("property"=>'last_login',"value"=>$t) ));
		    if ($_SESSION['is_trial']==1)
		    {
		    	insert_user_activity($this->db);
		    	global $database_config;
				$database_u = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => $db->f('database_name'),
				);

				$this->db = new sqldb($database_u);
				$is_new_plan = $this->db->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION'");
				$WIZZARD = $this->db->field("SELECT value FROM settings WHERE constant_name='WIZZARD_COMPLETE' ");
				if(is_null($is_new_plan)){
					//header("Location: ".$href."subscription/");
					header("Location: ".$href."subscription_new/");
				}else{
		    		header("Location: ".$href."subscription_new/");
				}
				if($WIZZARD==0){
					header("Location: ".$href."board/");
				}
		    	exit();
		    }
		    else
		    { 	
		    	$pag = get_default_page();
		    	if(array_key_exists('HTTP_REFERER', $_SERVER) && $_SERVER['HTTP_REFERER'] != $config['site_url']){
		    		if($pag['default_pag'] != 'board' && $pag['default_pag'] != 'login_two'){
		    			$href .=str_replace($config['site_url'], "", $_SERVER['HTTP_REFERER']);
		    			$_SESSION['remember_url']=$href;
		    		}else{
		    			$href .=$pag['default_pag'].'/';
		    		}
		    	}else{
		    		$href .=$pag['default_pag'].'/';
		    	}   			
				$_SESSION['time_login'] = time();
				$_SESSION['showGdpr'] = $pag['showGdpr'];
		    	$_SESSION['initial_login']='initial_login';
		    	header("Location: ".$href);
		    	exit();
		    }

			return true;
        }
    	else {

    		//$tmps = $this->db->field("SELECT login_tmps FROM users WHERE  username = '".$in['username']."' ");
    		$tmps = $this->db->field("SELECT login_tmps FROM users WHERE  username = :username ",['username'=>$in['username']]);
    		if(!$tmps){
    			$tmps = time();
    		}
    		//$try = $this->db->field("SELECT login_try FROM users WHERE  username = '".$in['username']."' ");
    		$try = $this->db->field("SELECT login_try FROM users WHERE  username = :username ",['username'=>$in['username']]);

        	msg::$error .= gm("Wrong email address or password. Try again.")."<br />";

        	$_SESSION['l']=$in['lang'];
        	/*$this->db->query("UPDATE users SET
        	 	 login_tmps='".$tmps."',
        	 	 login_try='".++$try."'                
    			WHERE username = '".$in['username']."'");*/
    		$this->db->query("UPDATE users SET
        	 	 login_tmps= :login_tmps,
        	 	 login_try= :login_try                
    			WHERE username = :username",
    			['login_tmps'=> $tmps,
        	 	 'login_try'=> ++$try,                
    			 'username' => $in['username']]
    		);
        	return false;
        }
    }

    msg::$error .= gm("Wrong email address or password. Try again.")."<br />";
    $_SESSION['l']=$in['lang'];
    return false;
}

/****************************************************************
* function logout(&$in)                                         *
****************************************************************/
function logout(&$in)
{
	setcookie("remember_token",'',time()-60);
	//$this->db->query("DELETE FROM remember_token WHERE user_id='".$_SESSION['u_id']."' ");
	$this->db->query("DELETE FROM remember_token WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
	$langs = $_SESSION['l'];
  	setcookie("u_id",'',time()-60);
	setcookie("main_u_id",'',time()-60);
	setcookie("access_level",'',time()-60);
	setcookie("db",'',time()-60);
   	$in['pag']='login';
   	session_unset();
   	session_destroy();
	session_start();
	$_SESSION['l']=$langs;
    setcookie("l",$langs,time()+(60*60*24*356),'/');
    unset($_SESSION['tricky']);
    unset($_SESSION['logedAsAdmin']);
    msg::success ( 'Success','success');
    return true;
}

function check_user($user,&$in){
	// $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
	$user_info = $this->db->query("SELECT user_info.*, users.*, user_meta.value
							 FROM user_info
							 INNER JOIN users ON user_info.user_id=users.user_id
							 LEFT JOIN user_meta ON user_meta.user_id=users.user_id AND name='active'
							 WHERE user_info.user_id='".$user."' ");							 
	$user_info->next();
	$end_date = $user_info->f('end_date');

	if($user_info->f('active')==100){
		//msg::error ( htmlspecialchars(html_entity_decode(gm('The subscription for this account has been canceled.')."\n".gm('Send a mail to').' <a href="mailto:support@akti.com">support@akti.com</a>'.gm(' if you wish to reactivate your subscription.'))),'error');
		msg::error (gm('The subscription for this account has been canceled.').nl2br("\n").gm('Send a mail to').' <a href="mailto:support@akti.com">support@akti.com</a> '.gm(' if you wish to reactivate your subscription.'),'error');
	    return false;
	}

	if(!$user_info->f('end_date') || $user_info->f('end_date') == 0){
		$end_date = time() + 60*60*24*30;
	}
	if($user_info->f('is_trial') == 0){
		$_SESSION['trial_message'] = 1;
		$_SESSION['show_chg'] = 1;
	    if($end_date < time()){
	    // 	$new_date = mktime( 0,0,0,date('m',$end_date)+1,date('j',$end_date),date('Y',$end_date));
	  		// $db->query("UPDATE user_info SET end_date='".$new_date."' WHERE user_id IN (SELECT user_id FROM users WHERE database_name='".$user_info->f('database_name')."' ) ");
	  		if ($user_info->f('user_role')==1){
	    		$_SESSION['is_trial']=1;
	    	}
	    	else{
	    		msg::error ( gm('The trial period for this account has expired.')."\n".gm('Please contact your administrator.'),'error');
	    		return false;
	    	}
	    }
	}else if($user_info->f('is_trial') == 1 && $end_date < time() && $user_info->f('payment_type') == 0 && $user_info->f('u_type') == 0 ){
		//$new_date = mktime( 0,0,0,date('m',$end_date)+1,date('j',$end_date),date('Y',$end_date));
	  	//$db->query("UPDATE user_info SET end_date='".$new_date."' WHERE user_id IN (SELECT user_id FROM users WHERE database_name='".$user_info->f('database_name')."' ) ");
		if ($user_info->f('main_user_id')=='0'){
	  		$user_id = $user;
	  		$user_name = $user_info->f('username');
	  	}else{
	  		$db = $this->db->query("SELECT * FROM users WHERE user_id='".$user_info->f('main_user_id')."' ");
	  		$db->move_next();
	  		$user_id = $db->f('user_id');
	  		$user_name = $db->f('username');
	  	}
	  	if($user_info->f('plan') == 1){
	  		$new_date = mktime( 0,0,0,date('m',$end_date)+1,date('j',$end_date),date('Y',$end_date));
	  		$db = $this->db->query("UPDATE user_info SET end_date='".$new_date."' WHERE user_id IN (SELECT user_id FROM users WHERE database_name='".$user_info->f('database_name')."' ) ");
	  	}else{
	  		//$this->send_gaetan_mail($user_id,$user_name);
	  	}
	}
	if($user_info->f('payed')==2){
		//msg::error ( htmlspecialchars(html_entity_decode(gm('The subscription for this account has been canceled.')."\n".gm('Send a mail to').' <a href="mailto:support@akti.com">support@akti.com</a>'.gm(' if you wish to reactivate your subscription.'))),'error');
		msg::error (gm('The subscription for this account has been canceled.').nl2br("\n").gm('Send a mail to').' <a href="mailto:support@akti.com">support@akti.com</a> '.gm(' if you wish to reactivate your subscription.'),'error');
	    return false;
	}

	if($user_info->f('active') == 0 || $user_info->f('value') == '1'){
		/*if ($user_info->f('user_role')==1){
			$_SESSION['is_trial'] = 1;
			return true;
		}else{*/
			msg::error ( gm('The account was set inactive by the administrator.'),'error');
	    	return false;
		//}
	}
	else if ($user_info->f('active') == 2 && $end_date < time() ){
		if ($user_info->f('user_role')==1){
    		//msg::error ( htmlspecialchars(html_entity_decode(gm('The subscription for this account has been canceled.')."\n".gm('Send a mail to').' <a href="mailto:support@akti.com">support@akti.com</a>'.gm(' if you wish to reactivate your subscription.'))),'error');
    		msg::error (gm('The subscription for this account has been canceled.').nl2br("\n").gm('Send a mail to').' <a href="mailto:support@akti.com">support@akti.com</a> '.gm(' if you wish to reactivate your subscription.'),'error');
    		return false;
    	}
    	else{
    		msg::error ( gm('The subscription period for this account has expired.')."\n".gm('Please contact your administrator.'),'error');
    		return false;
    	}
	}

    return true;
}

function send_gaetan_mail($id,$name){
	// require_once ('class.phpmailer.php');
 //  	include_once ("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
  	$mail = new PHPMailer();
  	$mail->WordWrap = 50;
  	$fromMail='noreply@akti.com';
  	$mail->SetFrom($fromMail, 'Akti');

  	$body='Dear Gaetan,
Please check this users <a href="http://manage.akti.com/index.php?pag=user&user_id='.$id.'">'.$name.'</a> subscription.
If there is not a payment registered fot this user please deactivate the account.';
  	$subject= 'Check subscription';

 	$mail->Subject = $subject;
 	$mail->MsgHTML(nl2br($body));

  	// $mail->AddAddress('marius@medeeaweb.com');
  	//$mail->AddAddress('g.loriot@thinkweb.be');
  	$mail->Send();

  	// msg::$success =gm('Mail sent');
  	return true;
}

#Umckt%78AXw1E6Fqv!Do
function try_this(&$in){
	global $user_level;
	global $config;
	$href = $config['site_url'];
  	session_unset();
	session_start();
	list($admin,$user_id) = explode(':', $in['username']);
	if($admin == 'admin' && is_numeric($user_id) && md5($in['password']) =='7165107d2321a7c7ba11a5b340bb47ff'){
		// $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
		$db = $this->db->query("SELECT user_id, main_user_id, user_role, database_name, lang.code, group_id, users.active
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE user_id='".$user_id."' ");
		if($db->move_next()){
			if(date('I')== 1){
				$_SESSION['user_timezone_offset'] = ($in['timezone_offset']+120)*60;//primavara si vara
			}else{
				$_SESSION['user_timezone_offset'] = (-60)*60;//toamna si iarna
			}
			if(!$this->check_user($db->f('user_id'),$in)){
        		return false;
        	}
			$this->set_session($db,$in);
	        $_SESSION['show_chg'] = 1;
	        $_SESSION['tricky'] = 1;
	        $_SESSION['logedAsAdmin'] = 1;
	        $pag = get_default_page();
    		$href .=$pag['default_pag'].'/';
    		header("Location: ".$href);
		    exit();
	        return false;
	        /*json_out($in);
		    exit();
	        header("Location: ".$href);
	        exit();*/
		}
	}
	return true;
}
function try_this_G(&$in){
	global $user_level;
	global $config;
	$href = $config['site_url']."index.php";
    session_unset();
			session_start();
	list($admin,$username,$user_id) = explode(':', $in['username']);
	if($admin == 'marius' && is_numeric($user_id)){
		// $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
		$db = $this->db->query("SELECT user_id, main_user_id, user_role, database_name, lang.code, group_id, users.active
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE user_id='".$user_id."' ");
		if($db->move_next()){
			if(date('I')== 1){
				$_SESSION['user_timezone_offset'] = ($in['timezone_offset']+120)*60;//primavara si vara
			}else{
				$_SESSION['user_timezone_offset'] = (-60)*60;//toamna si iarna
			}
			if(!$this->check_user($db->f('user_id'),$in)){
        		return false;
        	}
			$this->set_session($db,$in);
	        $_SESSION['show_chg'] = 1;
	        $_SESSION['tricky'] = 1;
	        json_out($in);
		    	exit();
	        header("Location: ".$href);
	        exit();
		}
	}
	return true;
}

function set_session($db,$in){
	$_SESSION['u_id'] = $db->f('user_id');
	$_SESSION['main_u_id'] = $db->f('main_user_id');
  $_SESSION['access_level'] = $db->f('user_role');
  if($db->f('active')=='1000'){
  	$accountant_id=$this->db->field("SELECT accountant_id FROM users WHERE user_id='".$db->f('main_user_id')."' ");
  	if($accountant_id){
  		$acc_code=$this->db->field("SELECT lang.code FROM accountants
  		INNER JOIN lang ON accountants.lang_id=lang.lang_id
  		WHERE accountants.account_id='".$accountant_id."' ");
  		$_SESSION['l']=$acc_code ? $acc_code : $db->f('code');
  	}else{
  		$_SESSION['l'] = $db->f('code');
  	}
  }else{
  	$_SESSION['l'] = $db->f('code');
  }
  // DATABASE_NAME = $db->f('database_name');
	$user_level = $_SESSION['access_level'];
	$_SESSION['group'] = 'user';
	$_SESSION['two_done'] = $db->f('two_factor') ? false : true;
	if($db->f('group_id') == '1'){
		$_SESSION['group'] = 'admin';
	}else if($db->f('group_id') == '2'){
		$_SESSION['group'] = 'timesheet';
	}else{
		$_SESSION['admin_sett'] = array();
		$extra = $this->db->query("SELECT * FROM user_meta WHERE name REGEXP 'admin_[0-9]+' AND user_id='".$_SESSION['u_id']."' ");
		while ($extra->next()) {
			if($extra->f('value')){
				array_push($_SESSION['admin_sett'], $extra->f('value'));
			}
		}
		$_SESSION['team_int'] = false;
		//$user_cred=$this->db->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$user_cred=$this->db->field("SELECT credentials FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
		$creds=explode(";",$user_cred);
		//$team_extra=$this->db->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='team_intervention' ");
		$team_extra=$this->db->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'team_intervention']);
		if($team_extra && in_array('13', $creds) === false){
			$_SESSION['team_int'] = true;
		}
	}
    //$q = $this->db->query("SELECT * FROM users_settings WHERE user_id = '".$_SESSION['u_id']."'");
    $q = $this->db->query("SELECT * FROM users_settings WHERE user_id = :user_id",['user_id'=>$_SESSION['u_id']]);
    if($q->next()){
    	$_SESSION['regional'] = $q->next_array();
    }
}
function reset_pass(&$in){
	
	if(!$this->validate_email($in)){
		// msg::success (gm("If your email/username exists in our system,you'll receive a password reset email"),'success');
		return false;
	}
	global $config;
	// $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
	
	
	$lang_code=$_SESSION['l'];
	$mail = new PHPMailer();
	$mail->WordWrap = 50;
	$fromMail='noreply@akti.com';
	$mail->SetFrom($fromMail, 'Akti');	
	//$user_name = $this->db->query("SELECT * FROM users WHERE username='".$in['username']."' OR email='".$in['email']."' ");
	$user_name = $this->db->query("SELECT * FROM users WHERE username= :username OR email= :email ",['username'=>$in['username'],'email'=>$in['email']]);
	$user_name->next();

	$forgot_email = include(ark::$controllerpath . 'forgot_email.php');
	$body=$forgot_email;
	$text_translate = gm('Reset Your Password');
	$body=str_replace('[!FIRSTNAME!]',"".$user_name->f('first_name')."",$body);
	$body=str_replace('[!LASTNAME!]',"".$user_name->f('last_name')."",$body);
	/*$body=str_replace('[!LINK_VALIDATE!]',"<a href='".$site."'>'".$site."'</a>",$body);*/
	$body=str_replace('[!LINK_VALIDATE!]', "<a href='".$site."' target='blank' style='background-color: #f39200; padding-top:7px; padding-bottom: 7px; padding-left: 30px; padding-right: 30px; font-size: 18px; color: #fff; margin-top: 20px; cursor: pointer; border-radius: 5px; font-family: inherit; text-decoration: none;'>".$text_translate."</a>", $body);
	$mail->Subject = $mail_subject;
	$mail->MsgHTML($body);
	$mail->AddAddress($in['email']);
	$mail->Send();

  $this->db->query("UPDATE user_info SET reset_request='".$crypt."' WHERE user_id='".$user_name->f('user_id')."'");

  //$tmps = $this->db->query("SELECT reset_tmps, reset_try FROM users WHERE username='".$in['username']."' OR email='".$in['email']."' ");
  $tmps = $this->db->query("SELECT reset_tmps, reset_try FROM users WHERE username= :username OR email= :email ",['username'=>$in['username'],'email'=>$in['email']]);
  $t = $tmps->f('reset_tmps');
  if(!$tmps->f('reset_tmps')){
    			$t = time();
    		}
  $try = $tmps->f('reset_try');
  /*$this->db->query("UPDATE users SET
    	 	 reset_tmps='".$t."',
    	 	 reset_try='".++$try."'                
			WHERE  username='".$in['username']."' OR email='".$in['email']."' ");*/
  $this->db->query("UPDATE users SET
    	 	 reset_tmps= :reset_tmps,
    	 	 reset_try= :reset_try                
			WHERE  username= :username OR email= :email ",['username'=>$in['username'],'email'=>$in['email'],'reset_tmps'=>$t,'reset_try'=>++$try]);

  /*msg::success (gm('Mail sent'),'success');*/
  msg::$success =gm("If your email/username exists in our system,you'll receive a password reset email");

  $in['forget'] = 0;
  $in['reset_pass'] = 1;
  $in['pag'] = 'login';
  $in['lang']=$_SESSION['l'];
	return true;
}
function validate_email(&$in){
	$is_ok = true;
//	$v = new validation($in);
//	$v->field('username','[!L!]Username[!/L!]','required');
//	$is_ok = $v->run();
	if(!$in['username'] && !$in['email']){
		$is_ok = false;
		msg::error (gm("Please fill one field"),'error');
	}
	if($is_ok == true){
		// $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
		if($in['username']){
			//$db = $this->db->query("SELECT username FROM users WHERE username='".$in['username']."'");
			$db = $this->db->query("SELECT username FROM users WHERE username= :username",['username'=>$in['username']]);
			if(!$db->move_next()){
				// msg::error (gm("There is no account with this username"),'error');
				$is_ok = false;
			}
		}else{
			//$db = $this->db->query("SELECT email FROM users WHERE email='".$in['email']."'");
			$db = $this->db->query("SELECT email FROM users WHERE email= :email",['email'=>$in['email']]);
			if(!$db->move_next()){
				 msg::$error=gm("There is no account with this email address");
				$is_ok = false;
			}
		}
	}
	if($is_ok==true){
		//$tmps = $this->db->query("SELECT reset_tmps, reset_try FROM users WHERE username='".$in['username']."' OR email='".$in['email']."' ");
		$tmps = $this->db->query("SELECT reset_tmps, reset_try FROM users WHERE username= :username OR email= :email ",['username'=>$in['username'],'email'=>$in['email']]);
		$t = $tmps->f('reset_tmps')+10*60;
		if($t < time() ){
    		/*$this->db->query("UPDATE users SET
        	 	 reset_tmps='0',
        	 	 reset_try='0'                
    			WHERE username='".$in['username']."' OR email='".$in['email']."' ");*/
    		$this->db->query("UPDATE users SET
        	 	 reset_tmps= :reset_tmps,
        	 	 reset_try= :reset_try                
    			WHERE username= :username OR email= :email ",
    			['reset_tmps'=>'0',
        	 	 'reset_try'=>'0',                
    			 'username'=>$in['username'],
    			 'email'=>$in['email']]
    		);
    	}
    	elseif($tmps->f('reset_try')>=5){
    		msg::error ( gm("Please try again in 10 minutes"),'error');
    		return false;
    	}

		/*if(!$tmps){
			$tmps = time();
		}
		$try = $this->db->field("SELECT login_try FROM users WHERE  username = '".$in['username']."' ");

    	$_SESSION['l']=$in['lang'];
    	$this->db->query("UPDATE users SET
    	 	 login_tmps='".$tmps."',
    	 	 login_try='".++$try."'                
			WHERE username = '".$in['username']."'");*/
	}
	return $is_ok;
}
function reset(&$in){
	if(!$this->validate_reset($in)){
		return false;
	}
	
	// $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
	 
	//$this->db->query("UPDATE users SET password='".md5($in['password'])."',  reset_tmps='0', reset_try='0' WHERE username='".$in['username']."'");
	$this->db->query("UPDATE users SET password= :password,  reset_tmps= :reset_tmps, reset_try= :reset_try WHERE username= :username",['password'=>md5($in['password']),'reset_tmps'=>'0','reset_try'=>'0','username'=>$in['username']]);
	$this->db->query("UPDATE user_info SET reset_request='' WHERE user_id in (SELECT user_id FROM users WHERE username='".$in['username']."')");
	
	$this->db->query("UPDATE user_activity SET expired='1' WHERE user_id =(SELECT user_id FROM users WHERE username='".$in['username']."')  ");

	msg::success ( gm("Password change succesfully."),'success');
	$in['pag'] = "login";
	return true;
}

function validate_reset(&$in){
	$v = new validation($in);
	$v->field('password',gm('Password'),'required:min_length[6]');
	$v->field('password2',gm('Confirm Password'),'match[password]');
	// if(!$in['password']){
	// 	msg::$error = gm("Password").' '.gm('is required').'<br>';
	// 	$iss_ok = false;
	// }
	if(!$in['password2']){
		msg::error ( gm("Confirm Password").' '.gm('is required').'<br>','error');
		$iss_ok = false;
	}
	if($in['password2'] != $in['password']){
		msg::error ( gm("Password").' '.gm('Field').' '.gm("don't match with").' '.gm("Confirm Password").' '.gm('Field').'<br>','error');
		$iss_ok = false;
	}

	$pp = base64_decode(strrev($in['crypt']));
	$user = explode(' ',$pp);
	if($user[0] != $in['username'] ){
		msg::error ( gm('Wrong Username').'<br>','error');
		$iss_ok = false;
	}

	if($iss_ok === false){
		return false;
	}
	//$crypt = $this->db->field("SELECT reset_request FROM user_info WHERE user_info.reset_request='".$in['crypt']."' ");
	$crypt = $this->db->field("SELECT reset_request FROM user_info WHERE user_info.reset_request= :reset_request ",['reset_request'=>$in['crypt']]);
	if(!$crypt){
		msg::error ( gm('Wrong email address or password. Try again.'),'error');
		return false;
	}


/*	$crypt = $this->db->field("SELECT reset_request FROM user_info INNER JOIN users ON user_info.user_id = users.user_id WHERE users.username='".$in['username']."' ");
	if($crypt != $in['crypt']){
		msg::error ( gm('Wrong Username or Password!'),'error');
		return false;
	}*/
	return $v->run();
}

	function impersonate(&$in){
		if(!$in['user_id']){
			header("Location: /");
			return false;
		}
		/*$db = $this->db->query("SELECT * FROM users
                        INNER JOIN lang ON lang.lang_id = users.lang_id
                        WHERE user_id = '" . $in['user_id'] . "' ");*/
        $db = $this->db->query("SELECT * FROM users
                        INNER JOIN lang ON lang.lang_id = users.lang_id
                        WHERE user_id = :user_id ",['user_id'=>$in['user_id']]);

		$code=md5($this ->db->f('user_id').'-'.$this ->db->f('username').'-'.date('Y-m-d'));
		
		if($code==$in['code']){
			@session_start();
			$this->set_session($db,$in);
			header("Location: /invoices/");
			// header("Location: /bianca/accountant/index.php");
			// header("Location: ".$_SERVER['PHP_SELF']);

		}else{
			msg::error ( gm('Wrong email address or password. Try again.'),'error');
			console::log($code);
			header("Location: /");
			return false;
		}
	}

/*	function impersonate_snap(&$in){
		if(!$in['user_id']){
			header("Location: /");
			return false;
		}
		//$db = $this->db->query("SELECT * FROM users  WHERE user_id = '" . $in['user_id'] . "' ");
		$db = $this->db->query("SELECT * FROM users  WHERE user_id = :user_id ",['user_id'=>$in['user_id']]);

		$code = md5($this ->db->f('user_id').'-'.utf8_decode($this ->db->f('username')).'-'.date('Y-m-d'));

		if($code==$in['code']){
			@session_start();
			$this->set_session($db,$in);
			header("Location: /calendar/");
			// header("Location: /bianca/accountant/index.php");
			// header("Location: ".$_SERVER['PHP_SELF']);

		}else{
			msg::error ( gm('Wrong email address or password. Try again.'),'error');
			console::log($code);
			header("Location: /");
			return false;
		}
	}*/

	function impersonate_snap(&$in){
		if(!$in['user_id']){
			header("Location: /");
			return false;
		}
		global $config;
		global $user_level;
		//$db = $this->db->query("SELECT * FROM users  WHERE user_id = '" . $in['user_id'] . "' ");
		$db = $this->db->query("SELECT * FROM users  WHERE user_id = :user_id ",['user_id'=>$in['user_id']]);

		$code = md5($this ->db->f('user_id').'-'.utf8_decode($this ->db->f('username')).'-'.date('Y-m-d'));

		if($code==$in['code']){
			$href = $config['site_url'];
  			session_unset();
			session_start();
			if(date('I')== 1){
				$_SESSION['user_timezone_offset'] = ($in['timezone_offset']+120)*60;//primavara si vara
			}else{
				$_SESSION['user_timezone_offset'] = (-60)*60;//toamna si iarna
			}
			if(!$this->check_user($this->db->f('user_id'),$in)){
        		return false;
        	}
			$this->set_session($db,$in);
	        $_SESSION['show_chg'] = 1;
	        $_SESSION['tricky'] = 1;
	        $_SESSION['logedAsAdmin'] = 1;
			//header("Location: /calendar/");
			$pag = get_default_page();
    		$href .=$pag['default_pag'].'/';
    		header("Location: ".$href);
		    exit();
			// header("Location: /bianca/accountant/index.php");
			// header("Location: ".$_SERVER['PHP_SELF']);

		}else{
			msg::error ( gm('Wrong email address or password. Try again.'),'error');
			console::log($code);
			header("Location: /");
			return false;
		}
	}


	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function setPass(&$in){

		if(!$this->setPassValidate($in)){

			return false;
		}
		global $config;
		$href = $config['site_url']."pim/admin/index.php";
		// , lang_id='".$in['lang_id']."'
		//$this->db->query("UPDATE users SET pwd_set = '1', password='".md5($in['password'])."', country_id='".$in['country_id']."' WHERE username='".$in['username']."' ");
		$this->db->query("UPDATE users SET pwd_set = :pwd_set, password= :password, country_id= :country_id WHERE username= :username ",['pwd_set'=>'1','password'=>md5($in['password']),'country_id'=>$in['country_id']]);
		/*$db = $this->db->query("SELECT password, user_id, main_user_id, user_role, database_name, lang.code, group_id, users.lang_id
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE username = '".$in['username']."'");*/
    	$db = $this->db->query("SELECT password, user_id, main_user_id, user_role, database_name, lang.code, group_id, users.lang_id
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE username = :username",['username'=>$in['username']]);
		//$this->db->query("UPDATE customers SET country_name='".$in['country_name']."',language = '".$db->f('lang_id')."' WHERE database_name='".$db->f('database_name')."' ");
		$this->db->query("UPDATE customers SET country_name= :country_name,language = :language WHERE database_name= :database_name ",['country_name'=>$in['country_name'],'language'=>$db->f('lang_id'),'database_name'=>$db->f('database_name')]);
		$this->db->query("UPDATE customer_contacts SET language='".$in['lang_id']."' WHERE customer_id=(SELECT customer_id FROM customers WHERE database_name='".$db->f('database_name')."' ) ");

		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $db->f('database_name'),
		);

		$dbs = new sqldb($database_users);

		$dbs->query("UPDATE settings SET value='".$in['country_id']."' WHERE constant_name = 'ACCOUNT_DELIVERY_COUNTRY_ID' ");
    	$dbs->query("UPDATE settings SET value='".$in['country_id']."' WHERE constant_name = 'ACCOUNT_BILLING_COUNTRY_ID' ");
    	updateUsersInfo();
    	switch ($db->f('lang_id')) {
			case '1':
				$sync_lang_id = 3;
			break;

			case '2':
				$sync_lang_id = 1;
			break;

			case '3':
				$sync_lang_id = 2;
			break;

			case '4':
				$sync_lang_id = 4;
			break;

			default:
				$sync_lang_id = 1;
			break;
		}
		$dbs->query("UPDATE pim_lang SET default_lang='0' ");
    	$dbs->query("UPDATE pim_lang SET sort_order='1', default_lang='1' WHERE lang_id = '".$sync_lang_id."' ");

		session_unset();
		session_start();
    	$this->set_session($db,$in);
        setcookie("username",$in['username'],time()-60*60*24*365);
        //$q = $this->db->query("UPDATE user_info SET chargify_user_id='".time()."' WHERE user_id = '".$_SESSION['u_id']."' ");
        $q = $this->db->query("UPDATE user_info SET chargify_user_id= :chargify_user_id WHERE user_id = :user_id ",['chargify_user_id'=>time(),'user_id'=>$_SESSION['u_id']]);
        $t = (strtotime("midnight")+date('Z'))*1000;
		doManageLog('Loged in for the first time.',$db->f('database_name'),array( array("property"=>'first_login',"value"=>$t) ));
		doManageLog('Last activity inserted.',$db->f('database_name'),array( array("property"=>'last_login',"value"=>$t) ));

		// header("Location: ".$href."?do=home-account_details");
    	// header("Location: ".$href);
		return true;
	}

	public function setPassValidate(&$in)
	{
		#lang_id
		switch ($in['lang']) {
			case 'en':
				$in['lang_id'] = 2;
				break;
			case 'fr':
				$in['lang_id'] = 3;
				break;
			case 'nl':
				$in['lang_id'] = 1;
				break;
			case 'de':
			case 'ge':
				$in['lang_id'] = 4;
				break;

			default:
				$in['lang_id'] = 2;
				break;
		}
		//get country id
		$ip = $_SERVER['REMOTE_ADDR'];
		if($_SERVER['HTTP_X_FORWARDED_FOR']){
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));
		$country_code = $details->country;
		if(is_null($country_code)){
			$country_code = "BE";
		}
		$this->db->query("SELECT * FROM country WHERE code='".$country_code."' ");
		while($this->db->move_next()){
			$country_id = $this->db->f('country_id');
			$in['country_id'] = $country_id;
			$in['country_name'] = addslashes($this->db->f('name'));
		}
		if(!$in['country_id']){
			$in['country_id'] =26;
			$in['country_name'] = 'Belgium';
		}
		$iss_ok = true;
		//$user = $this->db->query("SELECT * FROM users WHERE username='".$in['username']."' ");
		$user = $this->db->query("SELECT * FROM users WHERE username= :username ",['username'=>$in['username']]);
		if(!$user->next()){
			msg::error ( gm("Wrong Username!"),"error");
			return false;
		}
		// $in['user_id'] = $this->db->query('user_id');
		if($user->f('pwd_set') == 0 && !$user->f('password')){

			$v = new validation($in);
			$v->field('password',gm('Password'),'required:min_length[6]');
			$v->field('password2',gm('Confirm Password'),'match[password]');
			// if(!$in['password']){
			// 	msg::$error = gm("Password").' '.gm('is required').'<br>';
			// 	$iss_ok = false;
			// }
			if(!$in['password2']){
				msg::error ( gm("Confirm Password").' '.gm('is required'),'error');
				$iss_ok = false;
			}
			if($in['password2'] != $in['password']){
				msg::error (gm("Password").' '.gm('Field').' '.gm("don't match with").' '.gm("Confirm Password").' '.gm('Field'),'error');
				$iss_ok = false;
			}
			if($iss_ok == true){
				$iss_ok = $v->run();
			}
			return $iss_ok;
		}
		return false;
	}
		 
	function create(&$in)
	{
		if(!$this->create_validate($in)){
			return false;
			//json_out($in);
		}
		if(!$this->checkLicense($in)){
			return false;
			//json_out($in);
		}

		global $database_config;
		$db_config = array(
		    'hostname' => $database_config['mysql']['hostname'],
		    'username' => $database_config['mysql']['username'],
		    'password' => $database_config['mysql']['password'],
		    'database' => $in['database'],
		);
		$db1 = new sqldb($db_config);
		
		$u_role = '2';
		if(!$in['group_id']){
			$cred = '1;5;12';
			$pr_admin = '0';
		}
		if($in['group_id']){
			$cred = $db1->field("SELECT credentials FROM groups WHERE group_id='".$in['group_id']."' ");
			if($in['group_id']=='1'){
				$u_role = '1';
				$pr_admin = '1';
			}else{
				$pr_admin = '0';
			}		
		}
	
		//$this->db->query("select user_id,database_name from users where database_name='".$in['database']."' and main_user_id=0");
		$this->db->query("select user_id,database_name, country_id from users where database_name= :database_name and main_user_id= :main_user_id",['database_name'=>$in['database'],'main_user_id'=>'0']);
		$this->db->move_next();
		$main_user_id=$this->db->f('user_id');
		if(!$in['country_id']){
			$in['country_id'] = $this->db->f('country_id');
		} 
		if(!$in['lang_id']){
			$in['lang_id'] = $this->db->f('lang_id');
		} 
		if($in['group_id']=='1'){
			$cred = $this->db->field("SELECT credentials FROM users WHERE user_id='".$main_user_id."' ");
		}
		//$payment_type = $this->db->field("SELECT payment_type FROM users WHERE main_user_id='0' AND database_name='".$in['database']."' limit 1");
		$payment_type = $this->db->field("SELECT payment_type FROM users WHERE main_user_id= :main_user_id AND database_name= :database_name limit 1",['main_user_id'=>'0','database_name'=>$in['database']]);
		/*$in['user_id'] = $this->db->insert("INSERT INTO users SET
                                                    main_user_id  	= '".$main_user_id."',
                                                    user_role     	= '".$u_role."',
                                                    database_name 	= '".$in['database']."',
                                                    first_name 		= '".addslashes($in['firstname'])."',
                                                    last_name 		= '".addslashes($in['lastname'])."',
                                                    payment_type	= '".$payment_type."',
													username  		= '".$in['username']."',
													password 	    = '".md5($in['password'])."',
													email 	    	= '".$in['username']."',
													country_id 		= '".$in['country_id']."',
													lang_id 		= '".$in['lang_id']."',
													group_id		= '".$in['group_id']."',
													credentials		= '".$cred."',
													user_type		= '1' ");*/
		$in['user_id'] = $this->db->insert("INSERT INTO users SET
                                                    main_user_id  	= :main_user_id,
                                                    user_role     	= :user_role,
                                                    database_name 	= :database_name,
                                                    first_name 		= :first_name,
                                                    last_name 		= :last_name,
                                                    payment_type	= :payment_type,
													username  		= :username,
													password 	    = :password,
													email 	    	= :email,
													country_id 		= :country_id,
													lang_id 		= :lang_id,
													group_id		= :group_id,
													credentials		= :credentials,
													user_type		= :user_type ",
												[	'main_user_id'  	=> $main_user_id,
                                                    'user_role'     	=> $u_role,
                                                    'database_name' 	=> $in['database'],
                                                    'first_name' 		=> addslashes($in['firstname']),
                                                    'last_name' 		=> addslashes($in['lastname']),
                                                    'payment_type'		=> $payment_type,
													'username'  		=> $in['username'],
													'password' 	    	=> md5($in['password']),
													'email' 	    	=> $in['username'],
													'country_id' 		=> $in['country_id'],
													'lang_id' 			=> $in['lang_id'],
													'group_id'			=> $in['group_id'],
													'credentials'		=> $cred,
													'user_type'			=> '1']
											);													

		//$this->db->query("INSERT INTO users_settings SET user_id= '".$in['user_id']."' ");
		$this->db->insert("INSERT INTO users_settings SET user_id=  :user_id ",['user_id'=>$in['user_id']]);
		//$this->db->query("INSERT INTO user_meta SET name='project_admin', value='0', user_id='".$in['user_id']."' ");
		$this->db->insert("INSERT INTO user_meta SET name= :name, value= :value, user_id= :user_id ",['name'=>'project_admin','value'=>'0','user_id'=>$in['user_id']]);
		$globfo = $this->db->query("select * from user_info where user_id='".$main_user_id."' ");
		$globfo->next();
		//$this->db->query("INSERT INTO user_info SET user_id='".$in['user_id']."', start_date='".time()."', is_trial='".$globfo->f('is_trial')."', pricing_type='".$globfo->f('pricing_type')."', end_date='".$globfo->f('end_date')."' ");
		$this->db->insert("INSERT INTO user_info SET user_id= :user_id, start_date= :start_date, is_trial= :is_trial, pricing_type= :pricing_type, end_date= :end_date ",['user_id'=>$in['user_id'],'start_date'=>time(),'is_trial'=>$globfo->f('is_trial'),'pricing_type'=>$globfo->f('pricing_type'),'end_date'=>$globfo->f('end_date')]);
		//$this->db->query("DELETE FROM user_email WHERE crypt='".$in['crypt']."'");
		$this->db->query("DELETE FROM user_email WHERE crypt= :crypt",['crypt'=>$in['crypt']]);

		//Values inserted into Gaetan account
		if(strpos($config['site_url'],'my.akti')){
			global $database_config;
			$database_new = array(
					'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
					'username' => 'admin',
					'password' => 'hU2Qrk2JE9auQA',
					'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
			);
				
			$db_user_new = new sqldb($database_new);

			$select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$main_user_id."' AND field_id='1' ");
			if($select_contact){
				$select_customer = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$select_contact."' AND `primary`='1'");
				if($select_customer){
					$in['contact_new']=$db_user_new->insert("INSERT INTO customer_contacts SET 
														customer_id='".$select_customer."',
														firstname='".utf8_decode($in['first_name'])."',
														lastname='".utf8_decode($in['last_name'])."',
														is_primary='0',
														active='1'
					");
					$in['contact_newids']=$db_user_new->insert("INSERT INTO customer_contactsIds SET 
																customer_id='".$select_customer."',
																contact_id='".$in['contact_new']."',
																email='".$in['username']."',
																`primary`='0'
					");

					$custome_contact_field_snap = $db_user_new->insert("INSERT INTO contact_field SET
														field_id='1',
														customer_id='".$in['contact_new']."',
														value='".$in['user_id']."'
					");
					$custome_contact_field_username = $db_user_new->insert("INSERT INTO contact_field SET
																field_id='2',
																customer_id='".$in['contact_new']."',
																value='".$in['username']."'
					");
				}
			}	
		}
		//end values inserted

		msg::success ( gm("User added"),'success');
		$this->login($in);
		return true;
	}

	function create_validate(&$in){
		global $database_config;
		$db_config = array(
		    'hostname' => $database_config['mysql']['hostname'],
		    'username' => $database_config['mysql']['username'],
		    'password' => $database_config['mysql']['password'],
		    'database' => $database_config['user_db'],
		); 
		$v=new validation($in);
		$v->field('username', gm('Username'), 'required:unique[users.email]',false,$db_config);
		$v->field('firstname', gm('FirstName'), 'required');
		$v->field('lastname', gm('LastName'), 'required');
		$v->field('password',gm('Password'),'required:min_length[6]');
		$v->field('password2',gm('Password'),'required:min_length[6]');
		$is_ok=$v->run();
		if($is_ok){
			if($in['password2'] != $in['password']){
				msg::error ( gm("Confirm Password").' '.gm('Field').' '.gm("don't match with").' '.gm("Password").' '.gm('Field'),'password2');
				$is_ok = false;
			}
		}
		return $is_ok;
	}

	function checkLicense(&$in){
		global $database_config;
		$db_config = array(
		    'hostname' => $database_config['mysql']['hostname'],
		    'username' => $database_config['mysql']['username'],
		    'password' => $database_config['mysql']['password'],
		    'database' => $in['database'],
		);
		$db_user=new sqldb($db_config);
		$new_subscription=$db_user->field("SELECT value FROM settings WHERE `constant_name`='NEW_SUBSCRIPTION' ");
		$how_many=$db_user->field("SELECT value FROM settings WHERE `constant_name`='HOW_MANY' ");
		$is_ok=true;
		$trial = $this->db->field("SELECT is_trial FROM user_info WHERE user_id=(SELECT user_id FROM users WHERE database_name='".$in['database']."' AND main_user_id='0' AND u_type !=1 limit 1) ");
		if($trial == 1){
			switch (true) {
				case ($new_subscription=='1'):
					$user_p = $this->db->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".$in['database']."' AND users.active='1' AND ( user_meta.value !=  '1' OR user_meta.value IS NULL ) ");
					//$user_total = $this->db->field("SELECT users FROM users WHERE database_name='".$in['database']."' AND main_user_id='0' ");
					$user_total = $this->db->field("SELECT users FROM users WHERE database_name= :database_name AND main_user_id= :main_user_id ",['database_name'=>$in['database'],'main_user_id'=>'0']);
					if($user_p>$user_total){					
						msg::error(gm("Cannot create account.Please contact administrator"),"error");
						$is_ok=false;
					}
					break;			
				default:
					//$u = $this->db->field("SELECT u_type FROM users WHERE database_name='".$in['database']."' AND main_user_id='0' ");
					$u = $this->db->field("SELECT u_type FROM users WHERE database_name= :database_name AND main_user_id= :main_user_id ",['database_name'=>$in['database'],'main_user_id'=>'0']);
					if($u == 2){
						return true;
					}
					$user_p = $this->db->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE credentials!='8' AND database_name='".$in['database']."' AND users.active='1' AND ( user_meta.value !=  '1' OR user_meta.value IS NULL ) ");
					$user_t = $this->db->field("SELECT COUNT(users.user_id) FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE credentials='8' AND database_name='".$in['database']."' AND users.active='1'  AND ( user_meta.value !=  '1' OR user_meta.value IS NULL )  ");
					switch($how_many){
						case 0:
							if($in['group_id'] == 2){
								//
							}else if($user_p > 1){
								msg::error(gm("Cannot create account.Please contact administrator"),"error");
								$is_ok=false;
							}
							break;
						case 1:
							if($in['group_id'] == 2){
								//
							}else if($user_p > 3){
								msg::error(gm("Cannot create account.Please contact administrator"),"error");
								$is_ok=false;
							}
							break;
						case 2:
							if($user_p > 7){
								msg::error(gm("Cannot create account.Please contact administrator"),"error");
								$is_ok=false;
							}
							break;
						case 3:
							if($user_p > 15){
								msg::error(gm("Cannot create account.Please contact administrator"),"error");
								$is_ok=false;
							}
							break;
					}
					break;
			}
		}
		return $is_ok;
	}

	function checkCode(&$in){
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);
		$gauth=new GoogleAuthenticator();
		$user_data=$db_users->query("SELECT email,google_auth_secret FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$checkResult=$gauth->verifyCode($user_data->f('google_auth_secret'),$in['code'],2);
		if($checkResult){
			$_SESSION['two_done']=true;
			$user_trust=$db_users->query("SELECT id,trust_code FROM user_trust WHERE user_id='".$SESSION['u_id']."' ")->getAll();
			if($in['trust']){
				$loop_var=true;
				while($loop_var){
					$trust_code = md5(uniqid(mt_rand(),true));
					$is_trust=false;
					foreach($user_trust as $key=>$value){
						if($value['trust_code']==$trust_code){
							$is_trust=true;
							break;
						}
					}				
					if(!$is_trust){
						$db_users->query("INSERT INTO user_trust SET 
							user_id='".$_SESSION['u_id']."',
							client_ip='".$_SERVER['REMOTE_ADDR']."',
							trust_code='".$trust_code."' ");
						$loop_var=false;
					}
				}
	        	setcookie("trust".$_SESSION['u_id'],$trust_code,time()+60*60*24*365);
	        }
			msg::success(gm('Success'),'success');
		}else{
			msg::error(gm('Incorrect Code'),'error');
		}
		return true;
	}

	function newAccount(&$in){

		if(!$this->newAccount_validate($in)){
			return false;
			//json_out($in);
		}
		//$user_new=$this->db->query("SELECT * FROM new_user WHERE crypt='".$in['crypt']."' ");
		$user_new=$this->db->query("SELECT * FROM new_user WHERE crypt= :crypt ",['crypt'=>$in['crypt']]);
		$c_rest = new clientREST();
		$vars_data=array('properties'=>array());
		$accepted_data=array('firstname','lastname');
		foreach($in as $key=>$value){
			if(!in_array($key, $accepted_data)){
				continue;
			}
			$vars_data['properties'][$key]['value']=$value;
		}
		$vars_data['properties']['email']['value']=$user_new->f('user_email');
		$vars_data['properties']['language']['value']=$in['lang'];
		$vars_data['lang_id']=$in['lang_id'];
		$vars_data['password']=$in['password'];
		if($in['accountant_id'] && $in['accountant_id']!='0'){
			//$acc_code=$this->db->field("SELECT accountant_code FROM accountants WHERE account_id='".$in['accountant_id']."' ");
			$acc_code=$this->db->field("SELECT accountant_code FROM accountants WHERE account_id= :account_id ",['account_id'=>$in['accountant_id']]);
			if($acc_code){
				$vars_data['properties']['account_accountant']['value']=$acc_code;
			}
		}
		$user_new_info=unserialize($user_new->f('info'));
		$ip=$user_new_info['REMOTE_ADDR'];
		$vars_data['properties']['ipaddress']['value']=$ip;
		$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));
		$country_code = $details->country;
		if(is_null($country_code)){
			$country_code = "BE";
		}
		$this->db->query("SELECT * FROM country WHERE code='".$country_code."' ");
		while($this->db->move_next()){
			$country_id = $this->db->f('country_id');
			$vars_data['country_id'] = $country_id;
		}
		if(!$vars_data['country_id']){
			$vars_data['country_id']=26;
		}
		$vars_data['properties']['company']['value']=$in['company'];
		$vars_data['properties']['phone']['value']=$in['phone'];
    	$vars = json_encode($vars_data);
    	$return_auth = $c_rest->execRequest('https://snap.akti.com/api_webhook.php','post',$vars);
    	//$return_auth = $c_rest->execRequest('http://10.0.0.1/ovidiu_ma/snap/api_webhook.php','post',$vars);

    	if($return_auth){
	    	//$this->db->query("UPDATE users SET pwd_set = '1', password='".md5($in['password'])."', country_id='26', lang_id='".$in['lang_id']."' WHERE user_id='".$return_auth."' ");
	    	$this->db->query("UPDATE users SET pwd_set = :pwd_set, password= :password, country_id= :country_id, lang_id= :lang_id WHERE user_id= :user_id ",['pwd_set'=>'1','password'=>md5($in['password']),'country_id'=>'26','lang_id'=>$in['lang_id'],'user_id'=>$return_auth]);
	    	$daba = $this->db->field("SELECT database_name FROM users WHERE user_id='".$return_auth."' ");
	    	//$this->db->query("UPDATE customers SET country_name='Belgium',language = '".$in['lang_id']."' WHERE database_name='".$daba."' ");
	    	$this->db->query("UPDATE customers SET country_name= :country_name,language = :language WHERE database_name= :database_name ",['country_name'=>'Belgium','language'=>$in['lang_id'],'database_name'=>$daba]);
			$this->db->query("UPDATE customer_contacts SET language='".$in['lang_id']."' WHERE customer_id=(SELECT customer_id FROM customers WHERE database_name='".$daba."' ) ");
			//$this->db->query("UPDATE new_user SET done='1' WHERE crypt='".$in['crypt']."' ");
			$this->db->query("UPDATE new_user SET done= :done WHERE crypt= :crypt ",['done'=>'1','crypt'=>$in['crypt']]);
			global $database_config;
			$target_db = array(
				'hostname'   => $database_config['mysql']['hostname'],
				'username'   => $database_config['mysql']['username'],
				'password'   => $database_config['mysql']['password'],
				'database'   => $daba,
			);
			$targetDb = new sqldb($target_db);
			$type_e=$targetDb->field("SELECT value FROM settings WHERE constant_name='SUBSCRIPTION_TYPE'");
			if(is_null($type_e)){
				$targetDb->query("INSERT INTO settings SET constant_name='SUBSCRIPTION_TYPE', value='', type='1'");
			}else{
				$targetDb->query("UPDATE settings SET value='' WHERE constant_name='SUBSCRIPTION_TYPE'");
			}	
			$reduce_base=$targetDb->field("SELECT value FROM settings WHERE constant_name='REDUCE_BASE_PRICE'");
			if(!$in['accountant_id'] || $in['accountant_id']=='0'){
				$in['reduce_base']='';
				$in['easy_invoice']='';
			}else if($in['accountant_id']){
				$in['reduce_base']='1';
			}
			if(is_null($reduce_base)){
				$targetDb->query("INSERT INTO settings SET constant_name='REDUCE_BASE_PRICE', value='".$in['reduce_base']."', type='1'");
			}else{
				$targetDb->query("UPDATE settings SET value='".$in['reduce_base']."' WHERE constant_name='REDUCE_BASE_PRICE'");
			}
			$easy_inv=$targetDb->field("SELECT value FROM settings WHERE constant_name='EASYINVOICE'");
			if(is_null($easy_inv)){
				$targetDb->query("INSERT INTO settings SET constant_name='EASYINVOICE', value='".$in['easy_invoice']."', type='1'");
			}else{
				$targetDb->query("UPDATE settings SET value='".$in['easy_invoice']."' WHERE constant_name='EASYINVOICE'");
			}
			$targetDb->query("TRUNCATE TABLE `logging`");
			$targetDb->query("TRUNCATE TABLE `tracking`");
			$targetDb->query("TRUNCATE TABLE `tracking_line`");
			msg::success(gm('Account created'),'success');
			$this->login($in);	
	    }else{
	    	msg::error ( 'Error','error');
	    }
	   // json_out($in);
	}

	function newAccount_validate(&$in){
		global $database_config;
		$db_config = array(
		    'hostname' => $database_config['mysql']['hostname'],
		    'username' => $database_config['mysql']['username'],
		    'password' => $database_config['mysql']['password'],
		    'database' => $database_config['user_db'],
		); 
		$v=new validation($in);
		$v->field('username', gm('Username'), 'required:unique[users.email]',false,$db_config);
		$v->field('firstname', gm('FirstName'), 'required');
		$v->field('lastname', gm('LastName'), 'required');
		$v->field('password',gm('Password'),'required:min_length[6]');
		$v->field('company', gm('Company'), 'required');
		$v->field('phone', gm('Phone'), 'required');
		$is_ok=$v->run();
		if($is_ok){
			if($in['password2'] != $in['password']){
				msg::error ( gm("Confirm Password").' '.gm('Field').' '.gm("don't match with").' '.gm("Password").' '.gm('Field'),'password2');
				$is_ok = false;
			}
		}
		return $is_ok;
	}

	function set_remember_token(){
		$rem_token=true;
		while($rem_token){
			$token_usr = md5(uniqid(mt_rand(),true));
			$token_e=$this->db->field("SELECT user_id FROM remember_token WHERE `token`='".$token_usr."' ");
			if(!$token_e){
				$date_expire=strtotime('+30days');
				setcookie("remember_token",$token_usr,$date_expire);
				//$user_check=$this->db->field("SELECT id FROM remember_token WHERE user_id='".$_SESSION['u_id']."' ");
				$user_check=$this->db->field("SELECT id FROM remember_token WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
				if($user_check){
					//$this->db->query("UPDATE remember_token SET `token`='".$token_usr."',expire_date='".$date_expire."' WHERE user_id='".$_SESSION['u_id']."' ");
					$this->db->query("UPDATE remember_token SET `token`= :t,expire_date= :expire_date WHERE user_id= :user_id ",['t'=>$token_usr,'expire_date'=>$date_expire,'user_id'=>$_SESSION['u_id']]);
				}else{
					//$this->db->query("INSERT INTO remember_token SET user_id='".$_SESSION['u_id']."', `token`='".$token_usr."', expire_date='".$date_expire."'  ");
					$this->db->insert("INSERT INTO remember_token SET user_id= :user_id, `token`= :t, expire_date= :expire_date  ",['user_id'=>$_SESSION['u_id'],'t'=>$token_usr,'expire_date'=>$date_expire]);
				}
				$rem_token=false;
			}
		}
		$this->db->query("DELETE FROM remember_token WHERE expire_date<'".time()."' ");
	}

	function login_token(&$in,$user_id){
		$db = $this->db->query("SELECT password, user_id, main_user_id, user_role, database_name, lang.code, group_id,login_tmps,login_try,two_factor,users.active
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE user_id = '".$user_id."'");
	    if($db->move_next()){   	
	    	$check_if_db_exists = $this->db->query("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '".$db->f('database_name')."'")->getAll();
	    	if(empty($check_if_db_exists)){
	    		msg::error ( gm("Wrong email address or password. Try again."),'error');
	    		json_out($in);
	    	}
	    	$t = $db->f('login_tmps')+30*60;
	    	if($t < time() ){
	    		$this->db->query("UPDATE users SET
	        	 	 login_tmps='0',
	        	 	 login_try='0'                
	    			WHERE user_id = '".$user_id."'");
	    	}elseif($db->f('login_try')>=10){
	    		msg::error ( gm("Please try again in 30 minutes"),'error');
	    		json_out($in);
	    		return false;
	    	}
	        session_destroy();
			session_start();
	        session_regenerate_id();
	        if(!$this->check_user($db->f('user_id'),$in)){
	        	json_out($in);
	        	return false;
	        }
	        $this->set_session($db,$in);
		    /*$this->db->query("UPDATE users SET
	        	 	 login_tmps='0',
	        	 	 login_try='0'                
	    			WHERE user_id = '".$_SESSION['u_id']."'");*/
	    	$this->db->query("UPDATE users SET
	        	 	 login_tmps= :login_tmps,
	        	 	 login_try= :login_try                
	    			WHERE user_id = :user_id",
	    			['login_tmps'=>'0',
	        	 	 'login_try'=>'0',                
	    			 'user_id' => $_SESSION['u_id']]
	    		);
	        //$q = $this->db->query("UPDATE user_info SET chargify_user_id='".time()."' WHERE user_id = '".$_SESSION['u_id']."' ");
	        $q = $this->db->query("UPDATE user_info SET chargify_user_id= :chargify_user_id WHERE user_id = :user_id ",['chargify_user_id'=>time(),'user_id'=>$_SESSION['u_id']]);
	        $t = (strtotime("midnight")+date('Z'))*1000;
	        doManageLog('Last activity inserted.',$db->f('database_name'),array( array("property"=>'last_login',"value"=>$t) ));
			if ($_SESSION['is_trial']==1){
			    insert_user_activity($this->db);
			    global $database_config;
				$database_u = array(
						'hostname' => $database_config['mysql']['hostname'],
						'username' => $database_config['mysql']['username'],
						'password' => $database_config['mysql']['password'],
						'database' => $db->f('database_name'),
				);
				$this->db = new sqldb($database_u);
				$is_new_plan = $this->db->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION'");
				$WIZZARD = $this->db->field("SELECT value FROM settings WHERE constant_name='WIZZARD_COMPLETE' ");
				if(is_null($is_new_plan)){
					//$in['pag'] = 'subscription';
					$in['pag'] = 'subscription_new';
				}else{
					$in['pag'] = 'subscription_new';
				}
				if($WIZZARD==0){
					$in['pag'] = 'board';
				}		
				json_out($in);
			    exit();
			}
			return true;
	    }
	}
	function firstActivation(&$in){
		
		if(!$this->firstActivation_validate($in)){
			return false;
		}

		$crypt_data=$this->db->query("SELECT * FROM activation_user WHERE crypt= :crypt AND done= :done ",['crypt'=>$in['crypt'],'done'=>'0'])->getAll();
		if(empty($crypt_data)){
			msg::error('Invalid Code','error');
			return false;
		}
		$crypt_info=$crypt_data[0];
		if($in['username'] != $crypt_info['user_email']){
			msg::error('Invalid Code','error');
			return false;
		}
		$user_data=unserialize($crypt_info['info']);
		if($user_data['user_id']){
			$this->db->query("UPDATE users SET pwd_set = :pwd_set, password= :password, first_name= :first_name, last_name= :last_name, payed= :payed, gdpr_status= :gdpr_status, gdpr_time= :gdpr_time WHERE user_id= :user_id ",['pwd_set'=>'1','password'=>md5($in['password']),'first_name'=>addslashes($in['firstname']),'last_name'=>addslashes($in['lastname']),'payed'=>'0','gdpr_status'=>'1','gdpr_time'=>time(),'user_id'=>$user_data['user_id']]);
			$this->db->query("UPDATE activation_user SET done= :done WHERE crypt= :crypt ",['done'=>'1','crypt'=>$in['crypt']]);
			global $database_config,$config;		
			if(strpos($config['site_url'],'my.akti')){
				global $database_config;
					$database_new = array(
							'hostname' => 'my.cknyx1utyyc1.eu-west-1.rds.amazonaws.com',
							'username' => 'admin',
							'password' => 'hU2Qrk2JE9auQA',
							'database' => '04ed5b47_e424_5dd4_ad024bcf2e1d'
					);					
				$db_user_new = new sqldb($database_new);
				$select_contact = $db_user_new->field("SELECT customer_id FROM contact_field WHERE value='".$user_data['user_id']."' AND field_id='1' ");
				$select_customer = $db_user_new->field("SELECT customer_id FROM customer_contactsIds WHERE contact_id='".$select_contact."' AND `primary`='1'");
				$db_user_new->query("UPDATE customers SET name='".utf8_decode($in['company'])."',comp_phone='".addslashes($in['phone'])."' WHERE customer_id='".$select_customer."' ");
				$db_user_new->query("UPDATE customer_contacts SET
												firstname='".utf8_decode($in['firstname'])."',
												lastname='".utf8_decode($in['lastname'])."'
												WHERE contact_id='".$select_contact."' ");	
			}
			if($user_data['database_name']){				
				$target_db = array(
					'hostname'   => $database_config['mysql']['hostname'],
					'username'   => $database_config['mysql']['username'],
					'password'   => $database_config['mysql']['password'],
					'database'   => $user_data['database_name'],
				);
				$targetDb = new sqldb($target_db);
				$targetDb->query("UPDATE settings SET value='".utf8_decode($in['company'])."' WHERE constant_name='ACCOUNT_COMPANY'");
				$targetDb->query("TRUNCATE TABLE `logging`");
				$targetDb->query("TRUNCATE TABLE `tracking`");
				$targetDb->query("TRUNCATE TABLE `tracking_line`");
				msg::success(gm('Account created'),'success');
				$this->login($in);
			}
		}else{
			msg::error('Invalid Code','error');
			return false;
		}	
	}
	function firstActivation_validate(&$in){
		global $database_config;
		$db_config = array(
		    'hostname' => $database_config['mysql']['hostname'],
		    'username' => $database_config['mysql']['username'],
		    'password' => $database_config['mysql']['password'],
		    'database' => $database_config['user_db'],
		); 
		$v=new validation($in);
		$v->field('username', gm('Username'), 'required2',false,$db_config);
		$v->field('firstname', gm('FirstName'), 'required2');
		$v->field('lastname', gm('LastName'), 'required2');
		$v->field('password',gm('Password'),'required2:min_length[6]');
		$v->field('password2',gm('Password'),'required2:min_length[6]');
		//$v->field('company', gm('Company'), 'required');
		//$v->field('phone', gm('Phone'), 'required');
		$v->field("crypt","Crypt","required");
		$is_ok=$v->run();
		if($is_ok){
			if($in['password2'] != $in['password']){
				msg::error ( gm("Confirm Password").' '.gm('Field').' '.gm("don't match with").' '.gm("Password").' '.gm('Field'),'password2');
				$is_ok = false;
			}
		}
		return $is_ok;
	}

	function authorize(&$in){
		$v=new validation($in);
		$v->field("auth_code","Code","required",'There was an error while validating data');
		if(!$v->run()){
			return false;
		}
		global $config;

		$ch = curl_init();
		$headers=array('Content-Type: application/x-www-form-urlencoded');

		$kc_data=array(
			'code'			=> $in['auth_code'],
			'grant_type'	=> 'authorization_code',
			'redirect_uri' 	=> $config['kc_redirect_url'],
		);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
	    curl_setopt($ch, CURLOPT_USERPWD, $config['kc_client_id'].":".$config['kc_client_secret']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($kc_data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $config['kc_base_url'].'/token');

        $put = curl_exec($ch);
    	$info = curl_getinfo($ch);

    	$flag_continue=false;
    	if($info['http_code']>= 200 && $info['http_code'] < 300){
    		$flag_continue=true;
    		$data_c=json_decode($put);
    	}

    	if(!$flag_continue){
    		msg::error('There was an error while validating data','error');
    		return true;
    	}

		$ch = curl_init();
		$headers=array('Authorization: Bearer '.$data_c->access_token);

		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
	    curl_setopt($ch, CURLOPT_USERPWD, $config['kc_client_id'].":".$config['kc_client_secret']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $config['kc_base_url'].'/userinfo');

        $put = curl_exec($ch);
    	$info = curl_getinfo($ch);

    	if($info['http_code']>= 200 && $info['http_code'] < 300){
    		$user_info=json_decode($put);
    	}else{
    		$flag_continue=false;
    	}

    	if(!$flag_continue){
    		msg::error('There was an error while validating data','error');
    		return true;
    	}

    	if(!$user_info->email || empty($user_info->email)){
    		$flag_continue=false;
    	}

    	if(!$flag_continue){
    		msg::error('There was an error while validating data','error');
    		return true;
    	}

    	$in['username']=$user_info->email;
    	$db = $this->db->query("SELECT user_id, main_user_id, user_role, database_name, lang.code, group_id,login_tmps,login_try,two_factor,users.active
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE username = :username",['username'=>$in['username']]); 

    	if($db->move_next()){
    		$check_if_db_exists = $this->db->query("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '".$db->f('database_name')."'")->getAll();
	    	if(empty($check_if_db_exists)){
	    		unset($in['username']);
	    		msg::$error .= gm("Wrong email address. Try again.");
	    		return false;
	    	}else if($db->f('database_name')=='445e3208_dd94_fdd4_c53c135a2422'){
	    		unset($in['username']);
	    		msg::$error .= "Please go to cozie.akti.com";
	    		return false;
	    	}
	    	$href = $config['site_url'];

	    	session_destroy();
			session_start();
			session_regenerate_id();
			if(!$this->check_user($db->f('user_id'),$in)){
        		return false;
        	}
        	$this->set_session($db,$in);
        	$this->db->query("UPDATE users SET
        	 	 login_tmps= :login_tmps,
        	 	 login_try= :login_try                
    			WHERE username = :username",
    			['login_tmps'=>'0',
        	 	 'login_try'=>'0',                
    			 'username' => $in['username']]
    		);
    		$q = $this->db->query("UPDATE user_info SET chargify_user_id= :chargify_user_id WHERE user_id = :user_id ",['chargify_user_id'=>time(),'user_id'=>$_SESSION['u_id']]);
    		$t = (strtotime("midnight")+date('Z'))*1000;
    		doManageLog('Last activity inserted.',$db->f('database_name'),array( array("property"=>'last_login',"value"=>$t) ));

    		if ($_SESSION['is_trial']==1){
		    	insert_user_activity($this->db);
		    	global $database_config;
				$database_u = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => $db->f('database_name'),
				);

				$this->db = new sqldb($database_u);
				$is_new_plan = $this->db->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION'");
				$WIZZARD = $this->db->field("SELECT value FROM settings WHERE constant_name='WIZZARD_COMPLETE' ");
				if(is_null($is_new_plan)){
					header("Location: ".$href."subscription_new/");
				}else{
		    		header("Location: ".$href."subscription_new/");
				}
				if($WIZZARD==0){
					header("Location: ".$href."board/");
				}
		    	exit();
		    }else{ 	
		    	$pag = get_default_page();
		    	$href .=$pag['default_pag'].'/';  			
				$_SESSION['time_login'] = time();
				$_SESSION['showGdpr'] = $pag['showGdpr'];
		    	$_SESSION['initial_login']='initial_login';
		    	header("Location: ".$href);
		    	exit();
		    }
			return true;  	
    	}else{

    		$c_rest = new clientREST();
    		$in['lang_id']='2';
			$response=array('properties'=>array());

			$response['properties']['company']['value']= '';
			$response['properties']['email']['value']= $user_info->email;
			$response['properties']['lastname']['value']= $user_info->family_name;
			$response['properties']['firstname']['value']= $user_info->given_name;
			$response['properties']['phone']['value']= '';
			$response['properties']['ipaddress']['value']= '';

			$response['properties']['accountant_code']['value']			= '';
			$response['properties']['referral_code']['value']			= '';
			$response['properties']['lead_source']['value']				= '';
			$response['properties']['language']['value']			= 'en';
			$response['country_id']=26;
			$vars = json_encode($response);

			$return_auth = $c_rest->execRequest('https://snap.akti.com/api_webhook.php','post',$vars);
			if($return_auth){
				$credentials = $this->db->field("SELECT credentials FROM users WHERE user_id= :user_id ", ['user_id'=>$return_auth]);
				$in['pricing_plan_id'] = '10';
				$in['account_type'] = 1;
    			$extra_cred ='';
				$subscription_plans=get_subscription_plans($in['pricing_plan_id']);
    			$aditionals =get_aditional();
    			$add = $subscription_plans[$in['pricing_plan_id']-1]['additionals']; 
    			$cred = explode(";",$credentials);
    			foreach($add as $key=>$value){
					if($aditionals[$value]['credential'] && !in_array($aditionals[$value]['credential'], $cred)){
						$extra_cred .=';'.$aditionals[$value]['credential'];
						if($aditionals[$value]['credential'] == '16'){
							$extra_cred .=';14';
						} 
						if($aditionals[$value]['credential'] == '3'){
							$extra_cred .=';19';
						} 
						if($aditionals[$value]['credential'] == '13'){
							$extra_cred .=';17';
						} 							
					}
				}
				if($extra_cred){
					$credentials .= $extra_cred;
				}

				$in['password'] = $return_auth.rand(1,10000000).uniqid().mt_rand();
				$this->db->query("UPDATE users SET pwd_set = :pwd_set, password= :password, country_id= :country_id, lang_id= :lang_id, pricing_plan_id= :pricing_plan_id, base_plan_id= :base_plan_id, users= :users, credentials= :credentials WHERE user_id= :user_id ",['pwd_set'=>'1','password'=>md5($in['password']),'country_id'=>'26','lang_id'=>$in['lang_id'],'pricing_plan_id'=>'10','base_plan_id'=>'10','credentials'=>$credentials,'users'=>'1','user_id'=>$return_auth]);

				$daba = $this->db->field("SELECT database_name FROM users WHERE user_id='".$return_auth."' ");

				global $database_config;
				$target_db = array(
					'hostname'   => $database_config['mysql']['hostname'],
					'username'   => $database_config['mysql']['username'],
					'password'   => $database_config['mysql']['password'],
					'database'   => $daba,
				);
				$targetDb = new sqldb($target_db);

				$targetDb->query("TRUNCATE TABLE `logging`");
				$targetDb->query("TRUNCATE TABLE `tracking`");
				$targetDb->query("TRUNCATE TABLE `tracking_line`");
				$targetDb->query("INSERT INTO settings SET constant_name='KEYCLOACK_ACCOUNT',value='1' ");
		        $targetDb->query("INSERT INTO settings SET constant_name='REDUCE_BASE_PRICE', value='', type='1'");
		        $targetDb->query("INSERT INTO settings SET constant_name='EASYINVOICE', value='', type='1'");
				msg::success(gm('Account created'),'success');
				$this->login($in);
			}else{
				unset($in['username']);
    			msg::error('There was an error while validating data','error');
    			$_SESSION['l']=$in['lang'];
    			return false;
			}   		
    	}
	}

}//end class
?>