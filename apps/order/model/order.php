<?php

class order {

 	var $pag = 'order';

	function __construct() {
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db = new sqldb();
    $this->dbu = new sqldb();
		$this->db2 = new sqldb();
		$this->db_users = new sqldb($db_config);
		$this->db_users2 = new sqldb($db_config);
		$this->field_n = 'order_id';
	}

	 function saveAddToPdf(&$in)
  {
  	if($in['all']){
  		if($in['value'] == 1){
  			foreach ($in['item'] as $key => $value) {
			    $_SESSION['add_to_pdf_o'][$value]= $in['value'];
	  		}
  		}else{
  			foreach ($in['item'] as $key => $value) {
			    // $_SESSION['add_to_pdf_o'][$value]= $in['value'];
			    unset($_SESSION['add_to_pdf_o'][$value]);
	  		}
		  }
  	}else{
	    if($in['value'] == 1){
	    	$_SESSION['add_to_pdf_o'][$in['item']]= $in['value'];
	    }else{
	    	unset($_SESSION['add_to_pdf_o'][$in['item']]);
	    }
	  }
    $all_pages_selected=false;
    $minimum_selected=false;
    if($_SESSION['add_to_pdf_o'] && count($_SESSION['add_to_pdf_o'])){
      if($_SESSION['tmp_add_to_pdf_o'] && count($_SESSION['tmp_add_to_pdf_o']) == count($_SESSION['add_to_pdf_o'])){
        $all_pages_selected=true;
      }else{
        $minimum_selected=true;
      }
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
  }

  function saveAddToPdfAll(&$in){
    $all_pages_selected=false;
    $minimum_selected=false;
    if($in['value']){
      unset($_SESSION['add_to_pdf_o']);
      if($_SESSION['tmp_add_to_pdf_o'] && count($_SESSION['tmp_add_to_pdf_o'])){
        $_SESSION['add_to_pdf_o']=$_SESSION['tmp_add_to_pdf_o'];
        $all_pages_selected=true;
      }    
    }else{
      unset($_SESSION['add_to_pdf_o']);
    }
    $in['all_pages_selected']=$all_pages_selected;
    $in['minimum_selected']=$minimum_selected;
    json_out($in);
  }

    function saveAddToPdfToBeDelivered(&$in)
    {
        if($in['all']){
            if($in['value'] == 1){
                foreach ($in['item'] as $key => $value) {
                    $_SESSION['add_to_pdf_otd'][$value]= $in['value'];
                }
            }else{
                foreach ($in['item'] as $key => $value) {
                    // $_SESSION['add_to_pdf_o'][$value]= $in['value'];
                    unset($_SESSION['add_to_pdf_otd'][$value]);
                }
            }
        }else{
            if($in['value'] == 1){
                $_SESSION['add_to_pdf_otd'][$in['item']]= $in['value'];
            }else{
                unset($_SESSION['add_to_pdf_otd'][$in['item']]);
            }
        }
        $all_pages_selected=false;
        $minimum_selected=false;
        if($_SESSION['add_to_pdf_otd'] && count($_SESSION['add_to_pdf_otd'])){
            if($_SESSION['tmp_add_to_pdf_otd'] && count($_SESSION['tmp_add_to_pdf_otd']) == count($_SESSION['add_to_pdf_otd'])){
                $all_pages_selected=true;
            }else{
                $minimum_selected=true;
            }
        }
        $in['all_pages_selected']=$all_pages_selected;
        $in['minimum_selected']=$minimum_selected;
        json_out($in);
    }

    function saveAddToPdfAllToBeDelivered(&$in){
        $all_pages_selected=false;
        $minimum_selected=false;
        if($in['value']){
            unset($_SESSION['add_to_pdf_otd']);
            if($_SESSION['tmp_add_to_pdf_otd'] && count($_SESSION['tmp_add_to_pdf_otd'])){
                $_SESSION['add_to_pdf_otd']=$_SESSION['tmp_add_to_pdf_otd'];
                $all_pages_selected=true;
            }
        }else{
            unset($_SESSION['add_to_pdf_otd']);
        }
        $in['all_pages_selected']=$all_pages_selected;
        $in['minimum_selected']=$minimum_selected;
        json_out($in);
    }



  function sendNew(&$in){

      if(!$this->sendNewEmailValidate($in)){
        msg::error ( gm('Invalid email address').' - '.$in['new_email'],'error');
        json_out($in);
        return false;
      }
      global $config;
      $this->db->query("SELECT pim_orders.pdf_logo, pim_orders.identity_id, pim_orders.pdf_layout,pim_orders.acc_manager_id,pim_orders.email_language
                        FROM pim_orders
                        WHERE pim_orders.order_id ='".$in['order_id']."'");
      $this->db->move_next();
      if($this->db->f('pdf_layout')){
        $in['type']=$this->db->f('pdf_layout');
        $in['logo']=$this->db->f('pdf_logo');
      }else {
        $in['type'] = ACCOUNT_ORDER_PDF_FORMAT;
      }
      $in['lid']=$this->db->f('email_language');

      $identity_logo ='';
      $logo = 'images/no-logo.png';
      if($this->db->f('identity_id')){
        $identity_logo = $this->dbu->field("SELECT company_logo FROM multiple_identity WHERE identity_id='".$this->db->f('identity_id')."'");
      }
      if($identity_logo){
        $logo = $identity_logo;
      }else{
        $print_logo = ACCOUNT_LOGO_ORDER;
        if ($print_logo) {
              $logo = $print_logo;
          }
      }

      $acc_manager_id = $this->db->f('acc_manager_id');
      $author = $this->db->f('author_id');
      
      //regenerate pdf
      $params = array();
      if($this->db->f('pdf_layout')){
        $params['logo'] = $this->db->f('pdf_logo');
        $params['type']=$this->db->f('pdf_layout');
      }else{
        $params['type']=ACCOUNT_ORDER_PDF_FORMAT;
      }
      #if we are using a customer pdf template
      if(defined('USE_CUSTOME_ORDER_PDF') && USE_CUSTOME_ORDER_PDF == 1 && $this->db->f('pdf_layout') == 0){
        $params['custom_type']=ACCOUNT_ORDER_PDF_FORMAT;
        unset($params['type']);
      }
      $params['order_id'] = $in['order_id'];
      $params['lid'] = $this->db->f('email_language');
      $params['save_as'] = 'F';

      $this->generate_pdf($params);
      //end regenerate pdf

      $this->generate_pdf($in);
      
      $mail = new PHPMailer();
      $mail->WordWrap = 50;
      $def_email = $this->default_email();

      //add images to mail
        $xxx=$this->getImagesFromMsg($in['e_message'],'upload');
        foreach($xxx as $location => $value){
          $tmp_start=strpos($location,'/');
          $tmp_end=strpos($location,'.');
          $tmp_cid_name=substr($location,$tmp_start+1,$tmp_end-1-$tmp_start);
          $tmp_file_name=$tmp_cid_name.substr($location, $tmp_end, strlen($location)-$tmp_end);
          $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'].$config['install_path'].$location, 'sign_'.$tmp_cid_name, $tmp_file_name); 
          //$in['e_message']=str_replace($value,'<img src="cid:'.'sign_'.$tmp_cid_name.'">',$in['e_message']);
          $in['e_message']=str_replace($value,generate_img_with_cid($value,$tmp_cid_name),$in['e_message']);
        }

      //$body= stripslashes(htmlentities($in['e_message'],ENT_COMPAT | ENT_HTML401,'UTF-8'));
      $body=stripslashes(utf8_decode($in['e_message']));
      //$mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
      /*$mail->SetFrom($def_email['from']['email'], utf8_decode($def_email['from']['name']));
    
      if($def_email['reply']['email']){
      	$mail->AddReplyTo($def_email['reply']['email'], utf8_decode($def_email['reply']['name']));
  	  }*/

      /* 1 - use user logged email
         2 - use default email
         3 - use author email
      */
      $mail_sender_type = $this->db->field("SELECT value FROM default_data WHERE type='order_email_type' ");
      
      if($mail_sender_type == 1 || $mail_sender_type == 3) {
        if($mail_sender_type == 1) {
          //$this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
          $this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
          $mail_name = htmlspecialchars(stripslashes($this->db_users->f('first_name'))).' '.htmlspecialchars(stripslashes($this->db_users->f('last_name')));
          if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && $this->db_users->f('email')!=''){

            $mail->SetFrom($this->db_users->f('email'), utf8_decode($mail_name ),0);
            $def_email['from']['email'] = $this->db_users->f('email');//pt sendgrid
            $def_email['from']['name'] = $mail_name;//pt sendgrid

            $mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name ) );
            $def_email['reply']['email'] = $this->db_users->f('email');//pt sendgrid
            $def_email['reply']['name'] =  $mail_name;//pt sendgrid

            $mail->set('Sender',$this->db_users->f('email'));
          }else{
            $mail->SetFrom($def_email['from']['email'], utf8_decode( $mail_name),0);
            $def_email['from']['name'] = $mail_name;//pt sendgrid

            $mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name ) );
            $def_email['reply']['email'] = $this->db_users->f('email');//pt sendgrid
            $def_email['reply']['name'] =  $mail_name;//pt sendgrid

            $mail->set('Sender',$def_email['reply']['email']);
          }   
        } else {
          $this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = '".$author."' ");
          $mail_name = htmlspecialchars(stripslashes($this->db_users->f('first_name'))).' '.htmlspecialchars(stripslashes($this->db_users->f('last_name')));
          if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && $this->db_users->f('email')!=''){

            $mail->SetFrom($this->db_users->f('email'), utf8_decode( $mail_name),0);
            $def_email['from']['email'] = $this->db_users->f('email');//pt sendgrid
            $def_email['from']['name'] = $mail_name;//pt sendgrid

            $mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name));
            $def_email['reply']['email'] = $this->db_users->f('email');//pt sendgrid
            $def_email['reply']['name'] =  $mail_name;//pt sendgrid

            $mail->set('Sender',$this->db_users->f('email'));
          }else{
            $mail->SetFrom($def_email['from']['email'], utf8_decode( $mail_name),0);
            $def_email['from']['name'] = $mail_name;//pt sendgrid

            $mail->AddReplyTo($this->db_users->f('email'), utf8_decode( $mail_name));
            $def_email['reply']['email'] = $this->db_users->f('email');//pt sendgrid
            $def_email['reply']['name'] =  $mail_name;//pt sendgrid

            $mail->set('Sender',$def_email['reply']['email']);
          }   
        }
      }else{
        $mail->SetFrom($def_email['from']['email'], utf8_decode($def_email['from']['name']),0);
        $mail->AddReplyTo($def_email['reply']['email'], utf8_decode($def_email['reply']['name']));
        $mail->set('Sender',$def_email['reply']['email']);
      }

      $subject=stripslashes(utf8_decode($in['e_subject']));
      $mail->Subject = $subject;
      if($in['include_pdf']){
        $tmp_file_name = 'order_.pdf';
        if($in['attach_file_name']){
          $tmp_file_name = $in['attach_file_name'];
        }
         $mail->AddAttachment($tmp_file_name, $in['serial_number'].'.pdf'); // attach files/invoice-user-1234.pdf, and rename it to invoice.pdf
      }
      if($in['files']){
        foreach ($in['files'] as $key => $value) {
        	if($value['checked'] == 1){
	          $mime = mime_content_type($value['path'].$value['file']);
	          $mail->AddAttachment($value['path'].$value['file'], $value['file']); // attach files/quote-user-1234.pdf, and rename it to quote.pdf
	        }
        }
      }

      if($in['dropbox_files']){
              $path_dropbox = 'upload/'.DATABASE_NAME.'/dropbox_files/';
              if(!file_exists($path_dropbox)){
                mkdir($path_dropbox,0775,true);
              }
              $dis = new drop($in['for'],$in['customer_id'],$in['item_id'],true,'',$in['isConcact'],$in['serial_number']);

                foreach ($in['dropbox_files'] as $key => $value) {
                  if($value['checked'] == 1){
                    $value['file'] = str_replace('/', '_', $value['file']);

                    $file_path = $dis->getFile($value['path'], $path_dropbox.$value['file']);
                    
                    $mime = mime_content_type($path_dropbox.$value['file']);
                    $mail->AddAttachment($path_dropbox.$value['file'], $value['file']);
                  }
                }
            }
      if($in['file_d_id']){
        $body.="\n\n";
        foreach ($in['file_d_id'] as $key => $value) {
          $body.="<p><a href=\"".$in['file_d_path'][$key]."\">".$in['file_d_name'][$key]."</a></p>\n";
        }
      }
        if($in['copy']){
            //$u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."' ");
            $u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
            if($u->f('email')){
              $emails .=$u->f('email').', ';
              $mail->AddCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
            }
            // $mail->AddBCC(ACCOUNT_EMAIL,ACCOUNT_COMPANY);
        }
        if($in['copy_acc']){
            $u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$acc_manager_id."' ");
            if($u->f('email')){
              $emails .=$u->f('email').', ';
              $mail->AddBCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
            }
            // $mail->AddBCC(ACCOUNT_EMAIL,ACCOUNT_COMPANY);
        }
      if($in['use_html']){
      	    $order = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id='".$in['order_id']."'");
          	$contact =  $this->dbu->query("SELECT firstname,lastname,title FROM customer_contacts WHERE contact_id='".$order->f('contact_id')."'");
          	$title_cont =  $this->dbu->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'");
            $customer =  $this->dbu->field("SELECT name FROM customers WHERE customer_id='".$order->f('buyer_id')."'");

        if(defined('ACCOUNT_DATE_FORMAT') && ACCOUNT_DATE_FORMAT!=''){
            $date_format = ACCOUNT_DATE_FORMAT;
          }else{
            $date_format ='m/d/Y';
          }

      	$head = '<style>p { margin: 0px; padding: 0px; }</style>';
        $mail->IsHTML(true);
        $body_style='<style>body{background-color: #f2f2f2;}</style><div style="margin: 3px auto; max-width: 600px; border-collapse: collapse; border-spacing: 0; font: inherit; vertical-align: baseline; background-color: #fff; padding: 0px 30px;">'.$in['e_message'].'</div>';
        $body = stripslashes($body_style);
    		$body=str_replace('[!CONTACT_FIRST_NAME!]',"".utf8_decode($contact->f('firstname'))."",$body);
    		$body=str_replace('[!CONTACT_LAST_NAME!]',"".utf8_decode($contact->f('lastname'))."",$body);
    		$body=str_replace('[!SALUTATION!]',"".$title_cont."",$body);
    		$body=str_replace('[!SERIAL_NUMBER!]',"".$order->f('serial_number')."",$body);
    		$body=str_replace('[!DATE!]',"".date($date_format, $order->f('date'))."",$body);
    		$body=str_replace('[!SUBJECT!]',"".$contact->f('del_note')."",$body);
    		$body=str_replace('[!DUE_DATE!]',"".date($date_format, $order->f('del_date'))."",$body);
    		$body=str_replace('[!DELIVERY_DATE!]',"".date($date_format, $order->f('del_date'))."",$body);
    		$body=str_replace('[!YOUR_REFERENCE!]',"".$order->f('your_ref')."",$body);

        if($in['copy']){
    	    	$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto; padding-bottom:15px;\" src=\"".$config['site_url'].$logo."\" alt=\"\">",$body);
    		}elseif($in['copy_acc']){
    			$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto; padding-bottom:15px;\" src=\"".$config['site_url']."images/akti-logo.png\" alt=\"\">",$body);
    		}else{
    		     $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto; padding-bottom:15px;\" src=\"".$config['site_url'].$logo."\" alt=\"\">",$body);
    		}
        if($in['file_d_id']){
          $body .="<br><br>";
          foreach ($in['file_d_id'] as $key => $value) {
            $body.="<p><a href=".$in['file_d_path'][$key].">".$in['file_d_name'][$key]."</a></p>";
          }
        }
        $mail->Body = $head.$body;
      }else{
        $mail->MsgHTML(nl2br(($body)));
      }

        /*$this->db->query("SELECT customer_contacts.*,pim_orders.order_id,pim_orders.customer_id,pim_orders.serial_number,pim_orders.pdf_logo, pim_orders.pdf_layout
                          FROM pim_orders
                          INNER JOIN customer_contacts ON customer_contacts.customer_id=pim_orders.customer_id
                          WHERE pim_orders.order_id ='".$in['order_id']."'");
        $this->db->move_next();*/
      /*if($in['new_email_id']==$this->db->f('email')){
        $mail->AddAddress(trim($this->db->f('email')),$this->db->f('firstname').' '.$this->db->f('lastname'));
      }else{
        $mail->AddAddress(trim($in['new_email']));
      }*/
      $mail->AddAddress(trim($in['new_email']));
      $sent_date= time();
      $this->db->query("UPDATE pim_orders SET sent='1',sent_date='".$sent_date."' WHERE order_id='".$in['order_id']."'");

      $msg_log = '{l}Order was sent by{endl} '.get_user_name($_SESSION['u_id']).' {l}to:{endl}: '.$in['new_email'];

      if($in['sendgrid_selected']){

          $body=$head.$body;

          include_once(__DIR__."/../../misc/model/sendgrid.php");
          $sendgrid = new sendgrid($in, $this->pag, $this->field_n,$in['order_id'], $body,$def_email, DATABASE_NAME);

          $sendgrid_data = $sendgrid->get_sendgrid($in);

          if($sendgrid_data['error']){
            msg::error($sendgrid_data['error'],'error');
          }elseif($sendgrid_data['success']){
            msg::success(gm("Email sent by Sendgrid"),'success');
          }

          //$msg_log = $msg_log .= " {l}via SendGrid{endl}.";
          
        }else{

           $mail->Send();
           if($mail->IsError()){
              msg::error ( $mail->ErrorInfo,'error');
              json_out($in);
              return false;
            }
            /*if($mail->ErrorInfo){
              msg::notice( $mail->ErrorInfo,'notice');
            }*/

            /*if($mail->ErrorInfo){
              msg::error( $mail->ErrorInfo,'error');
              json_out($in);
              return false;
            }*/
             msg::success( gm('Order has been successfully sent'),'success');
                  
            if(isset($in['logging_id']) && is_numeric($in['logging_id'])){
              update_log_message($in['logging_id'],' '.$in['new_email']);
            }else{
              $in['logging_id'] = insert_message_log($this->pag,$msg_log,$this->field_n,$in['order_id']);
            }
        }


     
      foreach($xxx as $location => $value){
          unlink($location);
        }
      if($in['attach_file_name']){
        unlink($in['attach_file_name']);
      }

      if($in['dropbox_files']){
        $path_dropbox = 'upload/'.DATABASE_NAME.'/dropbox_files/';
        
            foreach ($in['dropbox_files'] as $key => $value) {
              if($value['checked'] == 1){
                $value['file'] = str_replace('/', '_', $value['file']);
                unlink($path_dropbox.$value['file']);
              }
            }
        }
      if($xml_tmp_file_name){
        unlink($xml_tmp_file_name);
      }

      json_out($in);
      return true;
    }

    function sendNewEmailValidate(&$in)
    {
      $v = new validation($in);
      $in['new_email'] = trim($in['new_email']);
      $v->field('new_email', 'Email', 'email');
      return $v->run();
    }

    /**
	 * Send order by email
	 *
	 * @return bool
	 * @author Mp
	 **/
	function generate_pdf(&$in)
	{
		include(__DIR__.'/../controller/order_print.php');
	}

	/**
	 * return the default email address
	 *
	 * @return array
	 * @author
	 **/
	function default_email($delivery_id=0)
	{
		$array = array();
		$array['from']['name'] = ACCOUNT_COMPANY;
		$array['from']['email'] = 'noreply@akti.com';
		$array['reply']['name'] = ACCOUNT_COMPANY;
		$array['reply']['email'] = ACCOUNT_EMAIL;
		/*if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
			$array['from']['email'] = ACCOUNT_EMAIL;
		}*/
    if(defined('MAIL_SETTINGS_PREFERRED_OPTION')){
      if(MAIL_SETTINGS_PREFERRED_OPTION==2){
        if(defined('MAIL_SETTINGS_EMAIL') && MAIL_SETTINGS_EMAIL!=''){
          $array['from']['email'] = MAIL_SETTINGS_EMAIL;     
        }
      }else{
        if(defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1' && defined('ACCOUNT_EMAIL') && ACCOUNT_EMAIL!=''){
          $array['from']['email'] = ACCOUNT_EMAIL;
        }
      }
    }
		$this->db->query("SELECT * FROM default_data WHERE type='order_email' ");
		//if($this->db->move_next()){
		if($this->db->move_next() && $this->db->f('value') && $this->db->f('default_name')){
			$array['reply']['name'] = $this->db->f('default_name');
			$array['reply']['email'] = $this->db->f('value');
			$array['from']['name'] = $this->db->f('default_name');
			if(((defined('ACCOUNT_SEND_EMAIL') && ACCOUNT_SEND_EMAIL=='1') || (defined('MAIL_SETTINGS_PREFERRED_OPTION') && MAIL_SETTINGS_PREFERRED_OPTION==2)) && $this->db->f('value')!=''){
				$array['from']['email'] = $this->db->f('value');
			}
		}

    //bcc email
    if($delivery_id){
          $this->db->query("SELECT * FROM default_data WHERE type='bcc_delivery_email' ");
          if($this->db->move_next() && $this->db->f('value')){
            $array['bcc']['email'] = $this->db->f('value'); // pot fi valori multiple, delimitate de ;
          }
    }else{
          $this->db->query("SELECT * FROM default_data WHERE type='bcc_order_email' ");
          if($this->db->move_next() && $this->db->f('value')){
            $array['bcc']['email'] = $this->db->f('value'); // pot fi valori multiple, delimitate de ;
          }
    }

		return $array;
	}

	function getImagesFromMsg($msg, $tmpFolderPath)
    {
        $arrSrc = array();
        if (!empty($msg))
        {
            preg_match_all('/<img[^>]+>/i', stripcslashes($msg), $imgTags);
            preg_match_all('/<img[^>]+>/i', $msg, $imgTags_1);

            for ($i=0; $i < count($imgTags[0]); $i++)
            {
                preg_match('/src="([^"]+)/i', $imgTags[0][$i], $withSrc);
                //Remove src
                $withoutSrc = str_ireplace('src="', '', $withSrc[0]);

                //data:image/png;base64,
                if (strpos($withoutSrc, ";base64,"))
                {
                    //data:image/png;base64,.....
                    list($type, $data) = explode(";base64,", $withoutSrc);
                    //data:image/png
                    list($part, $ext) = explode("/", $type);
                    //Paste in temp file
                    $withoutSrc = $tmpFolderPath."/".uniqid("temp_").".".$ext;
                    @file_put_contents($withoutSrc, base64_decode($data));
                    $arrSrc[$withoutSrc] = $imgTags_1[0][$i];
                }      
                //$arrSrc[$withoutSrc] = $imgTags_1[0][$i];
            }
        }
        return $arrSrc;
    }

	/**
	 * Mark as sent
	 *
	 * @return bool
	 * @author Mp
	 **/
	function mark_sent(&$in){
		if(!$this->mark_sent_validate($in))
		{
			return false;
		}
    $in['s_date']=strtotime('now');

		$this->db->query("UPDATE pim_orders SET sent='1',sent_date='".$in['s_date']."' WHERE order_id='".$in['order_id']."'");

		msg::success( gm('Order has been successfully marked as ready to deliver'),'success');
		insert_message_log($this->pag, '{l}Order has been successfully marked as ready to deliver by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;

    $inv = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
      $params = array();
      if($inv->f('pdf_layout')){
        $params['logo'] = $inv->f('pdf_logo');
        $params['type']=$inv->f('pdf_layout');
      }else{
        $params['type']=ACCOUNT_ORDER_PDF_FORMAT;
      }
      #if we are using a customer pdf template
      if(defined('USE_CUSTOME_ORDER_PDF') && USE_CUSTOME_ORDER_PDF == 1 && $inv->f('pdf_layout') == 0){
        $params['custom_type']=ACCOUNT_ORDER_PDF_FORMAT;
        unset($params['type']);
      }
      $params['order_id'] = $in['order_id'];
      $params['lid'] = $inv->f('email_language');
      $params['save_as'] = 'F';
      $this->generate_pdf($params);

		return true;
	}

	/****************************************************************
	* function mark_sent_validate(&$in)                                *
	****************************************************************/
	function mark_sent_validate(&$in)
	{
		$v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', gm('Invalid ID'));
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author me
	 **/
	function mark_as_draft(&$in)
	{
		if(!$this->delete_order_validate($in)){
			return false;
		}
    insert_message_log($this->pag, '{l}Order has been returned to draft{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$this->db->query("UPDATE pim_orders SET sent='0',sent_date='' WHERE order_id='".$in['order_id']."' ");

    $inv = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
      $params = array();
      if($inv->f('pdf_layout')){
        $params['logo'] = $inv->f('pdf_logo');
        $params['type']=$inv->f('pdf_layout');
      }else{
        $params['type']=ACCOUNT_ORDER_PDF_FORMAT;
      }
      #if we are using a customer pdf template
      if(defined('USE_CUSTOME_ORDER_PDF') && USE_CUSTOME_ORDER_PDF == 1 && $inv->f('pdf_layout') == 0){
        $params['custom_type']=ACCOUNT_ORDER_PDF_FORMAT;
        unset($params['type']);
      }
      $params['order_id'] = $in['order_id'];
      $params['lid'] = $inv->f('email_language');
      $params['save_as'] = 'F';
      $this->generate_pdf($params);

	}

	/****************************************************************
	* function delete_po_order_validate(&$in)                                          *
	****************************************************************/
	function delete_order_validate(&$in)
	{
		$v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
		return $v->run();
	}

	/**
	 * make delivery
	 *
	 * @return bool
	 * @author PM
	 **/
	function make_delivery(&$in)
	{
		if(!$this->make_delivery_validate($in)){
			return false;
		}


    if(!$in['confirm_over_total_delivery']){
      if(!$this->check_delivery_quantities($in)){
        json_out($in);
      }
    }

// var_dump($in['lines'][0]['disp_addres_info']);exit();

		$const =array();
    $articles_ids=array();
		$const['CAN_SYNC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='CAN_SYNC' ");
		$const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
		$const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
		$const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
		$const['ORDER_DELIVERY_STEPS'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ORDER_DELIVERY_STEPS' ");
		$commands = array();
		if($const['CAN_SYNC']){
		    Sync::start(ark::$model.'-'.ark::$method);
	  }
		$now = time();
		$delivery_done = 0;
		if($const['ORDER_DELIVERY_STEPS']==2 && $in['delivery_approved']){
			$delivery_done = 1;
		}
		if(isset($in['date_del_line']) && !empty($in['date_del_line']) ){
			$in['date_del_line_h'] = strtotime($in['date_del_line']);
		}
		$del = $this->db->insert("INSERT INTO pim_order_deliveries
			                      SET order_id='".$in['order_id']."',
			                         `date` = '".$in['date_del_line_h']."',
                                     `hour` = '".$in['hour']."',
                                     `minute` = '".$in['minute']."',
                                     `pm` = '".$in['pm']."',
                                      contact_name 	= '".$in['contact_name']."',
		                              contact_id	= '".$in['contact_id']."',
		                              delivery_address	= '".$in['delivery_address']."',
                                  delivery_address_id  = '".($in['is_custom_address'] ? '0': $in['delivery_address_id'])."',
		                              delivery_done 	= '".$delivery_done."',
                                  is_custom_address='".$in['is_custom_address']."',
                                  buyer_id='".$in['buyer_id']."',
                                  buyer_name='".addslashes($in['buyer_name'])."'

			                         ");
		if($in['approve_from_list']){
			$in['new_delivery_id'] = $del;
		}
		if($in['mark_as_delivered']==1){
			$this->db->query("UPDATE pim_orders SET rdy_invoice='1' WHERE order_id='".$in['order_id']."' ");
		}else{
			$this->db->query("UPDATE pim_orders SET rdy_invoice='2' WHERE order_id='".$in['order_id']."' ");
		}
		$in['quantity_dispatch'] = array();
		foreach ($in['lines'] as $key => $value) {
			if(return_value($value['quantity']) != '0.00'){
				$article_id =  $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id='".$value['order_articles_id']."'");
                array_push($articles_ids,$article_id);

        $hide_stock =  $this->db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$article_id."'");

        $in['quantity_dispatch'][$value['order_articles_id']]=array();
				foreach ($value['disp_addres_info'] as $k => $v) {
					$in['quantity_dispatch'][$value['order_articles_id']][$v['cust_addr_id']]=$v['quantity_delivered'];
				}

				if($const['ALLOW_STOCK'] && $hide_stock==0 ){

					$use_serial_no = $this->db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$article_id."' ");
					$use_batch_no = $this->db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$article_id."' ");

					if($const['ORDER_DELIVERY_STEPS'] == 2){
						$serial_number = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");
            if($article_id){
              $article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
              $article_data->next();
              $stock = $article_data->f("stock");

              $article_name = $article_data->f("internal_name");
              $item_code = $article_data->f("item_code");

              $stock_packing = $article_data->f("packing");
              if(!$const['ALLOW_ARTICLE_PACKING']){
                $stock_packing = 1;
              }
              $stock_sale_unit = $article_data->f("sale_unit");
              if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
                $stock_sale_unit = 1;
              }
            }else{
               $article_name = $value['article'];
               $item_code = '';
               $stock = 0;
               $stock_packing = 1;
               $stock_sale_unit = 1;
            }
						

						if($in['delivery_approved']){
							$new_stock = $stock - (return_value($value['quantity'])*$stock_packing/$stock_sale_unit);

							$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");

							/*start for  stock movements*/
							$backorder_articles = (return_value($value['quantity2']) - return_value($value['quantity'])) * $stock_packing/$stock_sale_unit;
							$this->db->insert("INSERT INTO stock_movements SET
								date 						=	'".time()."',
								created_by 					=	'".$_SESSION['u_id']."',
								order_id 					= 	'".$in['order_id']."',
								serial_number 				= 	'".$serial_number."',
								article_id 					=	'".$article_id."',
								article_name       			= 	'".addslashes($article_name)."',
								item_code  					= 	'".addslashes($item_code)."',
								movement_type				=	'3',
								quantity 					= 	'".return_value($value['quantity'])*$stock_packing/$stock_sale_unit."',
								stock 						=	'".$stock."',
								new_stock 					= 	'".$new_stock."',
								backorder_articles 			= 	'".$backorder_articles."',
								location_info               =   '".serialize($in['quantity_dispatch'][$value['order_articles_id']])."',
								delivery_id                 =    '".$del."',
                delivery_date       ='".$in['date_del_line_h']."'

					        	");
					        	@array_push($commands,"UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");

              /*end for  stock movements*/
						}
					}else{
						$article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
						$article_data->next();
						$stock = $article_data->f("stock");

						$stock_packing = $article_data->f("packing");
						if(!$const['ALLOW_ARTICLE_PACKING']){
							$stock_packing = 1;
						}
						$stock_sale_unit = $article_data->f("sale_unit");
						if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
							$stock_sale_unit = 1;
						}

						$new_stock = $stock - (return_value($value['quantity'])*$stock_packing/$stock_sale_unit);

						$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");

						/*start for  stock movements*/
						$backorder_articles = (return_value($value['quantity2']) - return_value($value['quantity'])) * $stock_packing/$stock_sale_unit;
						$serial_number = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");

						$this->db->insert("INSERT INTO stock_movements SET
							date 						=	'".time()."',
							created_by 					=	'".$_SESSION['u_id']."',
							order_id 					= 	'".$in['order_id']."',
							serial_number 				= 	'".$serial_number."',
							article_id 					=	'".$article_id."',
							article_name       			= 	'".addslashes($article_data->f("internal_name"))."',
							item_code  					= 	'".addslashes($article_data->f("item_code"))."',
							movement_type				=	'3',
							quantity 					= 	'".return_value($value['quantity'])*$stock_packing/$stock_sale_unit."',
							stock 						=	'".$stock."',
							new_stock 					= 	'".$new_stock."',
							backorder_articles 			= 	'".$backorder_articles."',
							location_info               =   '".serialize($in['quantity_dispatch'][$value['order_articles_id']])."',
							delivery_id                 =    '".$del."',
              delivery_date       ='".$in['date_del_line_h']."'
				        	");
				        	@array_push($commands,"UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");
						/*end for  stock movements*/

					}

					# article serial numbers
					if($use_serial_no == 1){
						$status_details_2 = $serial_number;
						// $selected_s_n = explode(',', $in['selected_s_n'][$key]);

						if($const['ORDER_DELIVERY_STEPS'] == 2){
                if($in['delivery_approved']){
                      foreach ($value['selected_s_n'] as $key2 => $v) {
                        $this->db->query("UPDATE serial_numbers
                              SET       status_id   = '2',
                                  status_details_2  = '".$status_details_2."',
                                  date_out    = '".$now."',
                                  delivery_id   = '".$del."'
                              WHERE id = '".$v['serial_nr_id']."' ");
                      }
                }
            }else{
                foreach ($value['selected_s_n'] as $key2 => $v) {
                        $this->db->query("UPDATE serial_numbers
                              SET       status_id   = '2',
                                  status_details_2  = '".$status_details_2."',
                                  date_out    = '".$now."',
                                  delivery_id   = '".$del."'
                              WHERE id = '".$v['serial_nr_id']."' ");
                      }
            }

					}

					# batch numbers
					if( $use_batch_no == 1 ){
						$status_details_2 = $serial_number;

            $location_batch=explode("-",$in['select_main_location']);
                if($const['ORDER_DELIVERY_STEPS'] == 2){
                  foreach ($value['selected_b_n'] as $key3 => $v) {
                      $location_id = $v['address_id'];
                    if(!$in['delivery_approved']){
                      $v['b_n_q_selected'] = return_value($v['b_n_q_selected']);
                    }
                    $current_in_stock = $this->db->field("SELECT in_stock FROM batches WHERE id = '".$v['b_n_id']."' AND article_id = '".$article_id."' ");
                    $new_in_stock = $current_in_stock-($v['b_n_q_selected']*$stock_packing/$stock_sale_unit);
                    $status_id = 1;
                    if($new_in_stock==0){
                      $status_id = 2;
                    }
                    
                    if($in['delivery_approved']){
                      $this->db->query("  UPDATE batches
                            SET   status_id     = '".$status_id."',
                              status_details_2  = '".$status_details_2."',
                              in_stock    = '".$new_in_stock."',
                              date_out    = '".$now."',
                              delivery_id   = '".$del."'
                            WHERE id = '".$v['b_n_id']."' AND article_id = '".$article_id."' ");

                      $location_current_stock_batch = $this->db->query("SELECT quantity FROM  batches_dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$location_id."' and customer_id='0' and batch_id='".$v['b_n_id']."'");

                        if($location_current_stock_batch->move_next()){
                        //update dispach batch from stock
                          $this->db->query("UPDATE batches_dispatch_stock SET quantity='".($location_current_stock_batch->f('quantity')-($v['b_n_q_selected']*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$article_id."' AND  address_id='".$location_id."' and customer_id='0' and batch_id='".$v['b_n_id']."'");
                        }
                    }
                      /*$this->db->query("INSERT INTO batches_from_orders SET
                        order_articles_id =     '".$value['order_articles_id']."',
                        article_id    =     '".$article_id."',
                        batch_id    =     '".$v['b_n_id']."',
                        delivery_id   =     '".$del."',
                        order_id    =     '".$in['order_id']."',
                        quantity    =     '".$v['b_n_q_selected']."'  ");*/
                      if($v['b_n_q_selected']!=0){
                        $this->db->query("INSERT INTO batches_from_orders SET
                        order_articles_id =     '".$value['order_articles_id']."',
                        article_id    =     '".$article_id."',
                        batch_id    =     '".$v['b_n_id']."',
                        delivery_id   =     '".$del."',
                        order_id    =     '".$in['order_id']."',
                        return_id       =       '0',
                        address_id       =    '".$location_id."',
                        quantity    =     '".$v['b_n_q_selected']*$stock_packing/$stock_sale_unit."'  ");
                      }
                    
                    
                }

            }else{
                  foreach ($value['selected_b_n'] as $key3 => $v) {
                     if($v['batch_no_checked']){
                          $location_id = $v['address_id'];

                           $v['b_n_q_selected'] = return_value($v['b_n_q_selected']);
                          $current_in_stock = $this->db->field("SELECT in_stock FROM batches WHERE id = '".$v['b_n_id']."' AND article_id = '".$article_id."' ");
                          $new_in_stock = $current_in_stock-($v['b_n_q_selected']*$stock_packing/$stock_sale_unit);
                          $status_id = 1;
                          if($new_in_stock==0){
                            $status_id = 2;
                          }
                        $this->db->query("  UPDATE batches
                                SET   status_id     = '".$status_id."',
                                  status_details_2  = '".$status_details_2."',
                                  in_stock    = '".$new_in_stock."',
                                  date_out    = '".$now."',
                                  delivery_id   = '".$del."'
                                WHERE id = '".$v['b_n_id']."' AND article_id = '".$article_id."' ");

                        $location_current_stock_batch = $this->db->query("SELECT quantity FROM  batches_dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$location_id."' and customer_id='0' and batch_id='".$v['b_n_id']."'");

                        if($location_current_stock_batch->move_next()){
                              //update dispach batch from stock
                                $this->db->query("UPDATE batches_dispatch_stock SET quantity='".($location_current_stock_batch->f('quantity')-($v['b_n_q_selected']*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$article_id."' AND  address_id='".$location_id."' and customer_id='0' and batch_id='".$v['b_n_id']."'");
                          }

                          if($v['b_n_q_selected']!=0){
                            $this->db->query("INSERT INTO batches_from_orders SET
                            order_articles_id =     '".$value['order_articles_id']."',
                            article_id    =     '".$article_id."',
                            batch_id    =     '".$v['b_n_id']."',
                            delivery_id   =     '".$del."',
                            order_id    =     '".$in['order_id']."',
                            return_id       =       '0',
                            address_id       =    '".$location_id."',
                            quantity    =     '".$v['b_n_q_selected']*$stock_packing/$stock_sale_unit."'  ");
                          }
                     }

                          
                  }

            }

					}
				}

				# check to see if the value is bigger than what we have
				$q = return_value($value['quantity']);
				$line = $this->db->field("SELECT quantity FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND order_articles_id='".$value['order_articles_id']."' AND content='0' ");
				$delivered = $this->db->field("SELECT sum(quantity) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$value['order_articles_id']."' ");
				if( $q+$delivered > $line){
					//$q = $line - $delivered;
				}
				# update deliveries
				$this->db->query("INSERT INTO pim_orders_delivery SET order_id='".$in['order_id']."' , order_articles_id='".$value['order_articles_id']."', quantity='".$q."', delivery_id='".$del."', delivery_note='".$in['delivery_note']."' ");

        //in cazul in care delivery steps = 2, delivered_all ia in considerare doar livrarile confirmate
        $filter_o = ' ';
        $filter_o2 = " ";
        if(ORDER_DELIVERY_STEPS == '2'){
                $filter_o = ' INNER JOIN pim_order_deliveries ON pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id ';
                $filter_o2 = " AND pim_order_deliveries.delivery_done ='1'  ";
            }

        $delivered_all = $this->db->field("SELECT sum(quantity) FROM pim_orders_delivery ".$filter_o." WHERE pim_orders_delivery.order_id='".$in['order_id']."' AND order_articles_id='".$value['order_articles_id']."' ".$filter_o2."");
				if($in['delivery_approved']){
					$this->db->query("UPDATE pim_order_articles SET delivered='0' WHERE order_articles_id='".$value['order_articles_id']."' ");
				}
				# delivery complete
				if($delivered_all > $line || $delivered_all == $line ){

						$this->db->query("UPDATE pim_order_articles SET delivered='1' WHERE order_articles_id='".$value['order_articles_id']."' ");

				}
			}
		}
        if($in['quantity_dispatch'] && $const['ALLOW_STOCK']  ){
      foreach ($in['quantity_dispatch'] as $order_articles_id => $stock_disp_delivery_info) {
        foreach ($stock_disp_delivery_info as $location_info => $dispatch_quantity) {
          if(return_value($dispatch_quantity) != '0.00') {
            $article_id =  $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id='".$order_articles_id."'");

            $art_data = $this->db->query("SELECT packing, sale_unit FROM pim_articles WHERE article_id = '".$article_id."' ");
            $art_data->move_next();

            $stock_packing = $art_data->f('packing');
            if(!$const['ALLOW_ARTICLE_PACKING']){
							$stock_packing = 1;
						}
						$stock_sale_unit = $art_data->f('sale_unit');
            if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
							$stock_sale_unit = 1;
						}
         	}

					$dispatch_quantity=return_value($dispatch_quantity);
          //new way of working we have to decrese stock at that address
          //select from adress stock info
	        if($const['ORDER_DELIVERY_STEPS'] == 2){
          	if($in['delivery_approved']){
            	$location=explode("-", $location_info);
              $from_address_current_stock = $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$location[1]."' and customer_id='".$location[0]."'");

              $this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock -($dispatch_quantity*($stock_packing/$stock_sale_unit ))) ."' WHERE article_id='".$article_id."'   AND  address_id='".$location[1]."' and customer_id='".$location[0]."'");
            }
          }else{
            $location=explode("-", $location_info);
            $from_address_current_stock = $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$location[1]."' and customer_id='".$location[0]."'");
            $this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock -($dispatch_quantity*($stock_packing/$stock_sale_unit )))."' WHERE article_id='".$article_id."'   AND  address_id='".$location[1]."' and customer_id='".$location[0]."'");
          }
				}
     	}
		}

		if(WAC){
		  foreach ($articles_ids as $key => $value) {
		    generate_purchase_price($value);
		  }
		}
    if($const['CAN_SYNC']){
      $packet = array(
			  'command' => base64_encode(serialize($commands)),
			  'command_id' => 1
		  );
      Sync::send(WEB_URL, $packet);
      Sync::end($in['order_id']);
    }

		$articles = $this->db->field("SELECT count(order_articles_id) FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND content='0' AND tax_id='0' AND has_variants='0'");
		//$articles = $this->db->field("SELECT count(order_articles_id) FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND content='0' ");
		$articles_delivered = $this->db->field("SELECT SUM(delivered) FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND content='0' AND tax_id='0'");
		//$articles_delivered = $this->db->field("SELECT SUM(delivered) FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND content='0' ");

		//if($articles == $articles_delivered){
		if($articles == $articles_delivered && ($const['ORDER_DELIVERY_STEPS']!=2 ||  ($const['ORDER_DELIVERY_STEPS']==2 && $in['delivery_approved']) )){
			$this->db->query("UPDATE pim_orders SET rdy_invoice='1' WHERE order_id='".$in['order_id']."' ");
		}

		insert_message_log($this->pag,'{l}Delivery made by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);

    $inv = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
      $params = array();
      if($inv->f('pdf_layout')){
        $params['logo'] = $inv->f('pdf_logo');
        $params['type']=$inv->f('pdf_layout');
      }else{
        $params['type']=ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;
      }
      #if we are using a customer pdf template
      if(defined('USE_CUSTOME_DELIVERY_PDF') && USE_CUSTOME_DELIVERY_PDF == 1){
        $params['custom_type']=ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;
        unset($params['type']);
      }
      $params['order_id'] = $in['order_id'];
      $params['lid'] = $inv->f('email_language');
      $params['delivery_id']=$del;
      $params['save_as'] = 'F';
      $this->generate_pdf($params);

		return true;
	}

  function check_delivery_quantities(&$in){
    $is_ok=true;
    foreach ($in['lines'] as $key => $value) {
      $delivered = $this->db->field("SELECT COALESCE(SUM(quantity),0) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$value['order_articles_id']."' ");
      $article_quantity = $this->db->field("SELECT quantity FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND order_articles_id='".$value['order_articles_id']."' ");
      if($delivered+return_value($value['quantity'])>$article_quantity){
        $is_ok=false;
        msg::notice('Quantities exceed total','notice');
        break;
      }
    }
    return $is_ok;
  }

	/**
	 * validate delivery
	 *
	 * @return bool
	 * @author me
	 **/
	function make_delivery_validate(&$in)
    {
        $v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");

		$sum_of_delivery_items = 0;

		foreach ($in['lines'] as $key => $value) {
			// if(return_value($value)<0) {
				//return false;
			// }
			$sum_of_delivery_items += (return_value($value['quantity']));
			if(return_value($value['quantity']<0)){
				return $v->run();
			}
		}
		if($sum_of_delivery_items==0){
			return false;	
		}else{
			// validation for articles that use serial numbers
			$err = false;
			if ($in['lines'][0]['rdy_invoice'] == 2) {
			    return true;
            } else {
                if ($in['order_articles_id']) {
                    foreach ($in['order_articles_id'] as $key => $order_articles_id) {
                        $val = 0;
                        $count_selected_s_n = 0;
                        $name = '';
                        foreach ($in['lines'] as $k => $value) {
                            if ($value['order_articles_id'] == $order_articles_id) {
                                $val = intval(return_value($value['quantity']));
                                $count_selected_s_n = intval($value['count_selected_s_n']);
                                $name = $value['article'];
                                break;
                            }
                        }
                        if ($val > 0) {
                            $article_id = $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '" . $order_articles_id . "' ");
                            $use_serial_no = $this->db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '" . $article_id . "' ");
                            if ($use_serial_no == 1 && ALLOW_STOCK) {
                                if ($val != $count_selected_s_n) {
                                    $error_match .= $name . "\n";
                                    $err = true;
                                }
                            }
                        }
                    }
                    if ($err != '') {
                        msg::error(gm("Serial Numbers") . ' ' . gm("quantities don't match for") . ": \n" . rtrim($error_match, "\n"), 'error');
                        return false;
                    }
                }
            }

			//validation for articles that use batches
			$err = false;
			if($in['order_articles_id']){
				foreach($in['order_articles_id'] as $key => $order_articles_id){
					$val = 0;
					$count_selected_s_n = 0;
					$name = '';
					foreach ($in['lines'] as $k => $value) {
						if($value['order_articles_id'] == $order_articles_id){
							/*$val = intval(return_value($value['quantity']));
							$count_selected_s_n = intval($value['count_selected_s_n']);*/
                            $val = return_value($value['quantity']);
                             $count_selected_s_n = $value['count_selected_s_n'];
							$name = $value['article'];
							break;
						}
					}
					if( $val > 0 ){
						$article_id = $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '".$order_articles_id."' ");
						$use_batch_no = $this->db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$article_id."' ");
           if($use_batch_no == 1 && ALLOW_STOCK){
             if(!$value['count_selected_b_n']){
               $error_match2 = $name."\n";
               $err2 = true;
             }else{
              $count_batch_q = $value['count_selected_b_n'];
                if($count_batch_q !=return_value($value['quantity'])) {
                 $error_match2 = $name."\n";
                 $err2 = true;
              }
             }
           }
					}
				}
				if($err2 != ''){
				 	msg::error ( gm("Batch Numbers").' '.gm("quantities don't match for").": \n".rtrim($error_match2,"\n"),'error');
				 	return false;
				}
			}

			return $v->run();
		}
	}

	/**
	 * validate delivery
	 *
	 * @return bool
	 * @author PM
	 **/
	function edit_delivery(&$in)
    {
		// printvar($in);exit();
		if(!$this->edit_delivery_validate($in)){
			return false;
		}
		Sync::start(ark::$model.'-'.ark::$method);
		$now = time();
		if(isset($in['date_del_line']) && !empty($in['date_del_line']) ){
			$in['date_del_line_h'] = strtotime($in['date_del_line']);
		}
		$this->db->query("UPDATE  pim_order_deliveries
		                  SET 	`date` = '".$in['date_del_line_h']."',
                                    `hour` = '".$in['hour']."',
                                    `minute` = '".$in['minute']."',
                                    `pm` = '".$in['pm']."',
                                    contact_name 	= '".$in['contact_name']."',
		                        contact_id	= '".$in['contact_id']."',
                            delivery_address_id='".($in['is_custom_address'] ? '0' : $in['delivery_address_id'])."',
		                        delivery_address	= '".$in['delivery_address']."',
                            is_custom_address= '".$in['is_custom_address']."',
                                  buyer_id='".$in['buyer_id']."',
                                  buyer_name='".addslashes($in['buyer_name'])."'
                              WHERE order_id='".$in['order_id']."' and delivery_id='".$in['delivery_id']."' ");
		$this->db->query("UPDATE 	pim_orders_delivery
		                 	SET   	`delivery_note` = '".$in['delivery_note']."'
                              WHERE     	order_id='".$in['order_id']."' and delivery_id='".$in['delivery_id']."' ");
        	Sync::end($in['order_id']);
		insert_message_log($this->pag,'{l}Delivery edited by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		if(ORDER_DELIVERY_STEPS == 2 && $in['delivery_approved']){
			$this->approve_delivery($in);
		}else{
      $inv = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
      $params = array();
      if($inv->f('pdf_layout')){
        $params['logo'] = $inv->f('pdf_logo');
        $params['type']=$inv->f('pdf_layout');
      }else{
        $params['type']=ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;
      }
      #if we are using a customer pdf template
      if(defined('USE_CUSTOME_DELIVERY_PDF') && USE_CUSTOME_DELIVERY_PDF == 1){
        $params['custom_type']=ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;
        unset($params['type']);
      }
      $params['order_id'] = $in['order_id'];
      $params['lid'] = $inv->f('email_language');
      $params['delivery_id']=$in['delivery_id'];
      $params['save_as'] = 'F';
      $this->generate_pdf($params);
    }
		return true;
	}

	/**
	 * validate delivery
	 *
	 * @return bool
	 * @author PM
	 **/
	function edit_delivery_validate(&$in)
	{

		$v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
		$v->field('delivery_id', 'ID', 'required:exist[pim_order_deliveries.delivery_id]', "Invalid Id");
		// validation for articles that uses serial numbers
		$err = false;
		foreach($in['order_articles_id'] as $key => $order_articles_id) {
            $val = 0;
            $count_selected_s_n = 0;
            $name = '';
            foreach ($in['lines'] as $k => $value) {
                if ($value['order_articles_id'] == $order_articles_id) {
                    $val = intval($value['quantity']);
                    $count_selected_s_n = intval($value['count_selected_s_n']);
                    $name = $value['article'];
                    break;
                }
                if ($val > 0) {
                    $article_id = $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '" . $order_articles_id . "' ");
                    $use_serial_no = $this->db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '" . $article_id . "' ");
                    if ($use_serial_no == 1 && ALLOW_STOCK) {
                        if ($val != $count_selected_s_n) {
                            $error_match .= $name . '<br />';
                            $err = true;
                        }
                    }
                }
            }
        }
		if($err != ''){
		 	msg::$error = gm("Quantities don't match for").': <br />'.rtrim($error_match,'<br />');
		 	return false;
		}


	return $v->run();

	}

	/**
	 * approve delivery
	 *
	 * @return bool
	 * @author PM
	 **/

	function approve_delivery(&$in){
		if(!$this->make_delivery_validate($in) || !$in['delivery_id']){
			return false;
		}
		if(!$this->delete_delivery($in)){
			return false;
		}
		if(!$this->make_delivery($in)){
			return false;
		}

		if($in['approve_from_list']==1){
			$in['is_art_in_back'] = $this->db->field("	SELECT COUNT(order_id) FROM pim_orders
										WHERE order_id= '".$in['order_id']."'
										AND rdy_invoice!='1'
										AND sent=1
										AND pim_orders.active=1
										GROUP BY pim_orders.order_id");
		}
		msg::success ( gm('Delivery was approved'),'success');

		return true;
	}

	/**
	 * approve delivery
	 *
	 * @return bool
	 * @author PM
	 **/
	function delete_delivery(&$in){

		if(!$this->delete_delivery_validate($in)){
			return false;
		}
		$commands = array();
    if(CAN_SYNC){
		  Sync::start(ark::$model.'-'.ark::$method);
    }
    $articles_ids=array();
    $delivery_date = $this->db->field("SELECT pim_order_deliveries.date FROM pim_order_deliveries WHERE order_id ='".$in['order_id']."' AND delivery_id='".$in['delivery_id']."' ");
	$articles = $this->db->query("SELECT * FROM pim_orders_delivery WHERE order_id ='".$in['order_id']."' AND delivery_id='".$in['delivery_id']."' ");
		while($articles->next()){
			$article_id = $this->db2->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '".$articles->f('order_articles_id')."' ");
			$order_article_id = $articles->f('order_articles_id');

      array_push($articles_ids,$article_id);

      $this->db->query("DELETE FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$order_article_id."' AND delivery_id='".$in['delivery_id']."' ");
			if($in['mark_as_delivered']!=1){
				$this->db->query("UPDATE pim_order_articles SET delivered = '0' WHERE order_id='".$in['order_id']."' AND order_articles_id='".$order_article_id."' ");
			}

			//delete order details from article serial numbers
			$this->db->field("UPDATE 	serial_numbers
						SET 		date_out 		= '0',
								delivery_id 	= '0',
								status_details_2 	= '',
								status_id 		= 1
						WHERE delivery_id = '".$in['delivery_id']."' ");


			//delete order details from article batches
			$batches_data = $this->db2->query("SELECT * FROM batches_from_orders
				WHERE 		order_id 		= 	'".$in['order_id']."'
				AND 			delivery_id 	= 	'".$in['delivery_id']."'
				AND 			order_articles_id = 	'".$order_article_id."' ");
			while($batches_data->next()){
				$batch_in_stock = $this->db2->field("SELECT in_stock FROM batches WHERE id = '".$batches_data->f('batch_id')."' ");
				$new_batch_stock = $batch_in_stock + $batches_data->f('quantity');
        if(ORDER_DELIVERY_STEPS==2){
          if(!$in['delivery_approved']){
				    $this->db2->query("UPDATE batches SET status_id = 1, in_stock = '".$new_batch_stock."', status_details_2 = '' WHERE id = '".$batches_data->f('batch_id')."' ");

            $location_current_stock_batch = $this->db->query("SELECT quantity FROM  batches_dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$batches_data->f('address_id')."' and customer_id='0' and batch_id='".$batches_data->f('batch_id')."'");

            if($location_current_stock_batch->move_next()){
                  //update dispach batch from stock
                    $this->db->query("UPDATE batches_dispatch_stock SET quantity='".($location_current_stock_batch->f('quantity')+ $batches_data->f('quantity'))."' WHERE article_id='".$article_id."' AND  address_id='".$batches_data->f('address_id')."' and customer_id='0' and batch_id='".$batches_data->f('batch_id')."'");
              }
          }
        }else{
          $this->db2->query("UPDATE batches SET status_id = 1, in_stock = '".$new_batch_stock."', status_details_2 = '' WHERE id = '".$batches_data->f('batch_id')."' ");
            $location_current_stock_batch = $this->db->query("SELECT quantity FROM  batches_dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$batches_data->f('address_id')."' and customer_id='0' and batch_id='".$batches_data->f('batch_id')."'");

            if($location_current_stock_batch->move_next()){
                  //update dispach batch from stock
                    $this->db->query("UPDATE batches_dispatch_stock SET quantity='".($location_current_stock_batch->f('quantity')+ $batches_data->f('quantity'))."' WHERE article_id='".$article_id."' AND  address_id='".$batches_data->f('address_id')."' and customer_id='0' and batch_id='".$batches_data->f('batch_id')."'");
              }
        }
			}
			$this->db2->query("DELETE FROM batches_from_orders
				WHERE 		order_id 		= 	'".$in['order_id']."'
				AND 			delivery_id 	= 	'".$in['delivery_id']."'
				AND 			order_articles_id = 	'".$order_article_id."' ");

		  $article_id = $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id ='".$order_article_id."'");
			$hide_stock =  $this->db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$article_id."'");

      if(ALLOW_STOCK && $hide_stock==0){

				if(ORDER_DELIVERY_STEPS==2){

					if(!$in['delivery_approved']){

						$article_quantity = $articles->f('quantity');

						//$article_id = $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id ='".$order_article_id."'");
						$article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
						$article_data->next();
						$stock = $article_data->f("stock");

						$stock_packing = $article_data->f("packing");
						if(!ALLOW_ARTICLE_PACKING){
							$stock_packing = 1;
						}
						$stock_sale_unit = $article_data->f("sale_unit");
						if(!ALLOW_ARTICLE_SALE_UNIT){
							$stock_sale_unit = 1;
						}

						$new_stock = $stock + ($article_quantity*$stock_packing/$stock_sale_unit);

						$this->db->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$article_id."'");

						// $this->db->query("UPDATE pim_order_articles SET delivered = '0' WHERE order_id='".$in['order_id']."' AND order_articles_id='".$order_article_id."' ");

						/*start for  stock movements*/
						$order_article_quantity = $this->db->field("SELECT quantity FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND article_id='".$article_id."' ");
						$delivered = $this->db->field("SELECT SUM(quantity) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$order_article_id."' ");
						$backorder_articles = ($order_article_quantity - $delivered)*$stock_packing/$stock_sale_unit;
						$now = time();
						$serial_number = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");

        		$this->db->insert("INSERT INTO stock_movements SET
						date 						=	'".time()."',
						created_by 					=	'".$_SESSION['u_id']."',
						serial_number 				= 	'".$serial_number."',
						article_id 					=	'".$article_id."',
						article_name       			= 	'".addslashes($article_data->f("internal_name"))."',
						item_code  					= 	'".addslashes($article_data->f("item_code"))."',
						movement_type				=	'4',
            delivery_id       = '".$in['delivery_id']."',
            order_id          = '".$in['order_id']."',
						quantity 					= 	'".$article_quantity*$stock_packing/$stock_sale_unit."',
						stock 						=	'".$stock."',
						new_stock 					= 	'".$new_stock."',
						backorder_articles			= 	'".$backorder_articles."'
        		");
		        /*end for  stock movements*/
		        array_push($commands,"UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");
	      	}
				}else{
					$article_quantity = $articles->f('quantity');

					//$article_id = $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id ='".$order_article_id."'");
					$article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
					$article_data->next();
					$stock = $article_data->f("stock");

					$stock_packing = $article_data->f("packing");
					if(!ALLOW_ARTICLE_PACKING){
						$stock_packing = 1;
					}
					$stock_sale_unit = $article_data->f("sale_unit");
					if(!ALLOW_ARTICLE_SALE_UNIT){
						$stock_sale_unit = 1;
					}

					$new_stock = $stock + ($article_quantity*$stock_packing/$stock_sale_unit);

					$this->db->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$article_id."'");

					// $this->db->query("UPDATE pim_order_articles SET delivered = '0' WHERE order_id='".$in['order_id']."' AND order_articles_id='".$order_article_id."' ");

					/*start for  stock movements*/
					$order_article_quantity = $this->db->field("SELECT quantity FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND article_id='".$article_id."' ");
					$delivered = $this->db->field("SELECT SUM(quantity) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$order_article_id."' ");
					$backorder_articles = ($order_article_quantity - $delivered)*$stock_packing/$stock_sale_unit;
					$now = time();
					$serial_number = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");

      		$this->db->insert("INSERT INTO stock_movements SET
					date 						=	'".time()."',
					created_by 					=	'".$_SESSION['u_id']."',
					serial_number 				= 	'".$serial_number."',
					article_id 					=	'".$article_id."',
					article_name       			= 	'".addslashes($article_data->f("internal_name"))."',
					item_code  					= 	'".addslashes($article_data->f("item_code"))."',
					movement_type				=	'4',
          delivery_id       = '".$in['delivery_id']."',
          order_id          = '".$in['order_id']."',
					quantity 					= 	'".$article_quantity*$stock_packing/$stock_sale_unit."',
					stock 						=	'".$stock."',
					new_stock 					= 	'".$new_stock."',
					backorder_articles			= 	'".$backorder_articles."'
      		");
	        /*end for  stock movements*/
	        array_push($commands,"UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$article_id."'");
				}

				//new way of working we have to decrese stock at that address
                         //select from adress stock info
	      $this->db->query("SELECT location_info FROM stock_movements WHERE delivery_id='".$in['delivery_id']."'  and article_id='".$article_id."'");
				if($this->db->f('location_info')){
					$location=unserialize($this->db->f('location_info'));
					foreach ($location as $key => $value) {
				    if(return_value($value)!=0.00){
				      $info=explode("-", $key);
				      $from_address_current_stock = $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$info[1]."' and customer_id='".$info[0]."'");
				      $this->db->query("UPDATE dispatch_stock SET stock='".($from_address_current_stock+return_value($value*$stock_packing/$stock_sale_unit ))."' WHERE article_id='".$article_id."'   AND  address_id='".$info[1]."' and customer_id='".$info[0]."'");
				    }
					}
				}
			}
		}
		$this->db->query("DELETE FROM pim_order_deliveries WHERE order_id='".$in['order_id']."' AND delivery_id='".$in['delivery_id']."' ");

		if(ORDER_DELIVERY_STEPS==2){
			if(!$in['delivery_approved']){
				$this->db->query("DELETE FROM dispatch_stock_movements WHERE  delivery_id='".$in['delivery_id']."'");
			}
		}else{
			$this->db->query("DELETE FROM dispatch_stock_movements WHERE  delivery_id='".$in['delivery_id']."'");

		}

		if(ark::$method=='delete_delivery' && ark::$model=='order'){
			$update = $this->db->query("UPDATE `pim_order_articles` SET  `delivered` = '0'
				WHERE order_id='".$in['order_id']."'
				AND `order_articles_id` NOT IN (SELECT `order_articles_id` FROM `pim_orders_delivery` WHERE `order_id` ='".$in['order_id']."' GROUP BY `order_articles_id` ) ");
		}


		if($in['del_nr']==1){
			$this->db->query("UPDATE pim_orders SET rdy_invoice='0' WHERE order_id='".$in['order_id']."' ");
		}else{
			$this->db->query("UPDATE pim_orders SET rdy_invoice='2' WHERE order_id='".$in['order_id']."' ");
		}

    if(CAN_SYNC){
		 	$packet = array(
			  'command' => base64_encode(serialize($commands)),
			  'command_id' => 1
		  );
			Sync::end();
		}

    if(WAC){
     	foreach ($articles_ids as $key => $value) {
        generate_purchase_price($value);
      }
 		}

     $certificates = $this->db->query("SELECT certificate_id FROM pim_certificates WHERE delivery_id='".$in['delivery_id']."'");

     while($certificates->next()){
         $this->db->query("DELETE FROM pim_order_certificate WHERE delivery_id='".$in['delivery_id']."' AND certificate_id='".$certificates->f('certificate_id')."' "); 
         $this->db->query("DELETE FROM pim_certificates WHERE certificate_id='".$certificates->f('certificate_id')."' "); 
    }

    insert_message_log($this->pag,'{l}Delivery was deleted by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		msg::success ( gm('Delivery was succesfully deleted'),'success');

    $inv = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
      $params = array();
      if($inv->f('pdf_layout')){
        $params['logo'] = $inv->f('pdf_logo');
        $params['type']=$inv->f('pdf_layout');
      }else{
        $params['type']=ACCOUNT_ORDER_PDF_FORMAT;
      }
      #if we are using a customer pdf template
      if(defined('USE_CUSTOME_ORDER_PDF') && USE_CUSTOME_ORDER_PDF == 1 && $inv->f('pdf_layout') == 0){
        $params['custom_type']=ACCOUNT_ORDER_PDF_FORMAT;
        unset($params['type']);
      }
      $params['order_id'] = $in['order_id'];
      $params['lid'] = $inv->f('email_language');
      $params['save_as'] = 'F';
      $this->generate_pdf($params);
		return true;
	}
	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author PM
	**/
	function delete_delivery_validate(&$in){

		$v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
		$v->field('delivery_id', 'ID', 'required:exist[pim_order_deliveries.delivery_id]', "Invalid Id");

		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author PM
	 **/
	function mark_as_delivered(&$in)
	{
		if(!$this->delete_order_validate($in)){
			return false;
		}

		$this->db->query("UPDATE pim_orders SET rdy_invoice='1' WHERE order_id='".$in['order_id']."' ");
    insert_message_log($this->pag,'{l}Order was marked as delivered by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);

    $inv = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
      $params = array();
      if($inv->f('pdf_layout')){
        $params['logo'] = $inv->f('pdf_logo');
        $params['type']=$inv->f('pdf_layout');
      }else{
        $params['type']=ACCOUNT_ORDER_PDF_FORMAT;
      }
      #if we are using a customer pdf template
      if(defined('USE_CUSTOME_ORDER_PDF') && USE_CUSTOME_ORDER_PDF == 1 && $inv->f('pdf_layout') == 0){
        $params['custom_type']=ACCOUNT_ORDER_PDF_FORMAT;
        unset($params['type']);
      }
      $params['order_id'] = $in['order_id'];
      $params['lid'] = $inv->f('email_language');
      $params['save_as'] = 'F';
      $this->generate_pdf($params);

		msg::success ( gm('Changes saved'),'success');
	}

	/**
	 * Send a reminder to the company about the delivery order
	 *
	 * @return true
	 * @param array $in
	 * @author Marius Pop
	 **/
	function notice(&$in)
	{	
		if(!$in['p_order_id'] && !$in['order_id']){
	    	msg::error ( gm('Invalid ID'),'error');
			return false;
		}
		//$in['new_email'] = $in['recipients'][0];
		if(!$this->sendNewEmailValidate($in)){
			msg::error ( gm('Invalid email address').' - '.$in['new_email'],'error');
        	return false;	
      	}
      	global $config;

		$in['type'] = ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;

		if($in['order_id']){
			include_once(__DIR__.'/../controller/order_print.php');
		}else{
			include_once('../apps/order/admin/controller/p_order_print.php');
		}

		// require_once ('../../classes/class.phpmailer.php');
        // include_once ("../../classes/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

    $mail = new PHPMailer();
	$mail->WordWrap = 50;
  $mail->AddAddress(trim($in['new_email']));
    if($in['order_id']){ //print_r($def_email);
		$def_email = $this->default_email($in['delivery_id']);
    }else{
    	$def_email = $this->default_email_po();
    }

      //add images to mail
    $xxx=$this->getImagesFromMsg($in['e_message'],'upload');
    foreach($xxx as $location => $value){
      $tmp_start=strpos($location,'/');
      $tmp_end=strpos($location,'.');
      $tmp_cid_name=substr($location,$tmp_start+1,$tmp_end-1-$tmp_start);
      $tmp_file_name=$tmp_cid_name.substr($location, $tmp_end, strlen($location)-$tmp_end);
      $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'].$config['install_path'].$location, 'sign_'.$tmp_cid_name, $tmp_file_name); 
      //$in['e_message']=str_replace($value,'<img src="cid:'.'sign_'.$tmp_cid_name.'">',$in['e_message']);
      $in['e_message']=str_replace($value,generate_img_with_cid($value,$tmp_cid_name),$in['e_message']);
    } 

    //$body = stripslashes($in['e_message']);
    $body=stripslashes(utf8_decode($in['e_message']));
    $body = str_replace("\r\n", "\n", $body);
    if($in['file_d_id']){
        $body.="\n\n";
        foreach ($in['file_d_id'] as $key => $value) {
          $body.="<p><a href=\"".$in['file_d_path'][$key]."\">".$in['file_d_name'][$key]."</a></p>\n";
        }
      }

    // $body= strip_tags($body,'<table><tbody><tr><td><div><th><thead><strong>');
    //$mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
    $mail->SetFrom($def_email['from']['email'], utf8_decode($def_email['from']['name']));
    //$mail->AddReplyTo($def_email['reply']['email'], $def_email['reply']['name']);
    if($def_email['reply']['email']){
      	$mail->AddReplyTo($def_email['reply']['email'], utf8_decode($def_email['reply']['name']));
  	}

    //$subject=$in['subject'];
  	$subject=stripslashes(utf8_decode($in['e_subject']));
    $mail->Subject = $subject;
    if($in['files']){
        foreach ($in['files'] as $key => $value) {
        	if($value['checked'] == 1){
	          $mime = mime_content_type($value['path'].$value['file']);
	          $mail->AddAttachment($value['path'].$value['file'], $value['file']); // attach files/quote-user-1234.pdf, and rename it to quote.pdf
	        }
        }
    }

    if($in['use_html']){
    	$order = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id='".$in['order_id']."'");
        $contact =  $this->dbu->query("SELECT firstname,lastname,title FROM customer_contacts WHERE contact_id='".$order->f('contact_id')."'");
        $title_cont =  $this->dbu->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'");
        $customer =  $this->dbu->field("SELECT name FROM customers WHERE customer_id='".$order->f('customer_id')."'");

        if(defined('ACCOUNT_DATE_FORMAT') && ACCOUNT_DATE_FORMAT!=''){
            $date_format = ACCOUNT_DATE_FORMAT;
          }else{
            $date_format ='m/d/Y';
          }

      	$head = '<style>p { margin: 0px; padding: 0px; }</style>';
        $mail->IsHTML(true);
        $body_style='<style>body{background-color: #f2f2f2;}</style><div style="margin: 3px auto; max-width: 600px; border-collapse: collapse; border-spacing: 0; font: inherit; vertical-align: baseline; background-color: #fff; padding: 0px 30px;">'.$in['e_message'].'</div>';
        $body = stripslashes($body_style);
		$body=str_replace('[!CONTACT_FIRST_NAME!]',"".utf8_decode($contact->f('firstname'))."",$body);
		$body=str_replace('[!CONTACT_LAST_NAME!]',"".utf8_decode($contact->f('lastname'))."",$body);
		$body=str_replace('[!SALUTATION!]',"".$title_cont."",$body);
		$body=str_replace('[!SERIAL_NUMBER!]',"".$order->f('serial_number')."",$body);
		$body=str_replace('[!DATE!]',"".date($date_format, $order->f('date'))."",$body);
		$body=str_replace('[!SUBJECT!]',"".$contact->f('del_note')."",$body);
		$body=str_replace('[!DUE_DATE!]',"".date($date_format, $order->f('del_date'))."",$body);
		$body=str_replace('[!YOUR_REFERENCE!]',"".$order->f('your_ref')."",$body);
        if($in['copy']){
	    	$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto; padding-bottom:15px;\" src=\"".$config['site_url'].ACCOUNT_LOGO_ORDER."\" alt=\"\">",$body);
		}elseif($in['c_email_ch']){
			$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto; padding-bottom:15px;\" src=\"".$config['site_url']."images/akti-logo.png\" alt=\"\">",$body);
		}else{
		     $body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto; padding-bottom:15px;\" src=\"".$config['site_url'].ACCOUNT_LOGO_ORDER."\" alt=\"\">",$body);
		}
        if($in['file_d_id']){
          $body .="<br><br>";
          foreach ($in['file_d_id'] as $key => $value) {
            $body.="<p><a href=".$in['file_d_path'][$key].">".$in['file_d_name'][$key]."</a></p>";
          }
        }
        $mail->Body = $head.$body;
    } else {
    	$mail->MsgHTML(nl2br(($body)));
	}
    /*if($in['use_html']){
    	$mail->IsHTML(true);
    	$head = '<style>p {	margin: 0px; padding: 0px; }</style>';
    	$body = stripslashes($in['e_message']);
      $mail->Body    = $head.$body;
    }*/

    /*if($in['copy']){
        $mail->AddBCC(ACCOUNT_EMAIL,ACCOUNT_COMPANY);
    }*/
    if($in['copy']){
        //$u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."' ");
        $u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);
        if($u->f('email')){
            $emails .=$u->f('email').', ';
            $mail->AddCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
        }
    }
    $this->db->query("SELECT pim_orders.pdf_logo, pim_orders.pdf_layout,pim_orders.acc_manager_id
                        FROM pim_orders
                        WHERE pim_orders.order_id ='".$in['order_id']."'");
    $this->db->move_next();
    if($this->db->f('pdf_layout')){
        $in['type']=$this->db->f('pdf_layout');
        $in['logo']=$this->db->f('pdf_logo');
    }else {
        $in['type'] = ACCOUNT_ORDER_PDF_FORMAT;
    }
    $acc_manager_id = $this->db->f('acc_manager_id');
    if($in['c_email_ch']){
        $u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$acc_manager_id."' ");
        if($u->f('email')){
            $emails .=$u->f('email').', ';
            $mail->AddBCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
        }
    }

    /*if($in['c_email']){
    	$mail->AddAddress($in['c_email'],$in['c_email']);
    }*/

    /*if($in['email_address']){
      foreach ($in['email_address'] as $contact_id => $email) {
        $mail->AddAddress(trim($email),$email);
      }
    }*/

	/*if($in['send_to']){
		$mail->AddAddress(trim($in['send_to']));
	}*/

	/*if($in['recipients']){
		foreach ($in['recipients'] as $key => $value) {
			$mail->AddAddress(trim($in['recipients'][$key]));
		}
	}*/

	if($in['contact_id']){
		$contact = $this->db->query("SELECT email, lastname, firstname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
		if($contact->next()){
			$mail->AddAddress($contact->f('email'),$contact->f('lastname').' '.$contact->f('firstname'));
		}
	}
	/*if($in['contacts_id']){
		$contact = $this->db->query("SELECT email, lastname, firstname FROM customer_contacts WHERE contact_id='".$in['contacts_id']."' ");
		if($contact->next()){
			$mail->AddAddress($contact->f('email'),$contact->f('lastname').' '.$contact->f('firstname'));
		}
	}*/

  	if($in['include_pdf']){
  		if($in['order_id']){
  			$mail->AddAttachment("order_.pdf", $in['serial_number'].'.pdf');
  		}else{
  			$mail->AddAttachment("p_order.pdf", $in['serial_number'].'.pdf');
  		}
    }

    $msg_log = '{l}Order was sent by{endl} '.get_user_name($_SESSION['u_id']).' {l}to:{endl}: '.$in['new_email'];

    $body=$head.$body;

    include_once(__DIR__."/../../misc/model/sendgrid.php");
    $sendgrid = new sendgrid($in, $this->pag, $this->field_n,$in['order_id'], $body,$def_email, DATABASE_NAME);

    $sendgrid_data = $sendgrid->get_sendgrid($in);

    if($sendgrid_data['error']){
      msg::error($sendgrid_data['error'],'error');
    }elseif($sendgrid_data['success']){
      msg::success(gm("Email sent by Sendgrid"),'success');
    }
          

   	foreach($xxx as $location => $value){
          unlink($location);
        }

 /*   msg::success( gm('Order has been successfully sent'),'success');*/
    json_out($in);
		return true;
	}


	/**
   * make_return
   *
   * @return bool
   * @author PM
   **/
  function make_return(&$in)
  { 
    $in['failure'] = false;
    if(!$this->make_return_validate($in)){
    	$in['failure'] = true;
      return false;
    }

    $const =array();
    $const['CAN_SYNC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='CAN_SYNC' ");
    $const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
    $const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
    $const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
    $const['STOCK_MULTIPLE_LOCATIONS'] = $this->db->field("SELECT value FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS' ");
    $const['DEFAULT_STOCK_ADDRESS']=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default='1'");

    $commands = array();
    if($const['CAN_SYNC']){
      Sync::start(ark::$model.'-'.ark::$method);
    }
    if(isset($in['date']) && !empty($in['date']) ){
			$in['date'] = strtotime($in['date']);
		}
  	$return_id=$this->db->insert("INSERT INTO  pim_orders_return
                            SET order_id='".$in['order_id']."',
                               `date` = '".$in['date']."',
                                note = '".$in['note']."' ");

   	if($in['lines']){
    	foreach ($in['lines'] as $key => $val) {
    		$article_id = $val['article_id'];
    		$q = $val['ret_quantity'];
    		$article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
        	$article_data->next();
       		
        	$stock_packing = $article_data->f("packing");
       		 if(!$const['ALLOW_ARTICLE_PACKING']){
         		 $stock_packing = 1;
        	}
       		 $stock_sale_unit = $article_data->f("sale_unit");
        	 if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
          		$stock_sale_unit = 1;
        	}

        	$this->db->query("INSERT INTO  pim_articles_return
                            SET return_id='".$return_id."',
                                article_id = '".$article_id."',
                                quantity = '".return_value($q)."',
                                order_id='".$in['order_id']."' ");
        //update batches
        foreach ($val['selected_b_n'] as $key3 => $v) {

          if($v['batch_no_checked']){
        	 $location_id = $v['address_id'];
           $v['b_n_q_selected'] = return_value($v['b_n_q_selected']);

      			$current_in_stock = $this->db->field("SELECT in_stock FROM batches WHERE id = '".$v['batch_id']."' AND article_id = '".$article_id."' ");
      			$new_in_stock = $current_in_stock+$v['b_n_q_selected']*$stock_packing/$stock_sale_unit;

            $status_id = 1;
            if($new_in_stock==0){
              $status_id = 2;
            }

            $this->db->query("UPDATE batches SET in_stock= '".$new_in_stock."' WHERE id = '".$v['batch_id']."' AND article_id = '".$article_id."' ");

            $location_current_stock_batch = $this->db->query("SELECT quantity FROM  batches_dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$location_id."' and customer_id='0' and batch_id='".$v['batch_id']."'");

              if($location_current_stock_batch->move_next()){
                    //update dispach batch from stock
                      $this->db->query("UPDATE batches_dispatch_stock SET quantity='".($location_current_stock_batch->f('quantity')+($v['b_n_q_selected']*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$article_id."' AND  address_id='".$location_id."' and customer_id='0' and batch_id='".$v['batch_id']."'");
                }



            $current_in_batches = $this->db->field("SELECT quantity FROM batches_from_orders WHERE batch_id = '".$v['batch_id']."' AND article_id = '".$article_id."' AND order_id='".$in['order_id']."' ");
              	if($current_in_batches){
              		$this->db->query("INSERT INTO batches_from_orders SET
      								order_articles_id = 	'".$v['order_articles_id']."',
      								article_id 		= 		'".$article_id."',
      								batch_id 		= 		'".$v['batch_id']."',
      								return_id 	 	= 		'".$return_id."',
      								order_id 		= 		'".$in['order_id']."',
      								quantity 		= 		'".$v['b_n_q_selected']*$stock_packing/$stock_sale_unit."',
                      address_id    =     '".$v['address_id']."',
      								delivery_id     =       '0'  
      								");
              	}
        }
    	}
    	//update serial_numbers
    	foreach ($val['selected_s_n'] as $k2 => $v2) {
		    
		    $this->db->query("INSERT INTO serial_numbers_from_orders SET
								order_articles_id = 	'".$v2['order_articles_id']."',
								article_id 		= 		'".$article_id."',
								serial_number_id 		= '".$v2['serial_nr_id']."',
								return_id 	 	= 		'".$return_id."',
								order_id 		= 		'".$in['order_id']."',
								quantity 		= 		'1',
								delivery_id     =       '0'  
								");
		    $this->db->query("UPDATE serial_numbers SET status_id = 1 WHERE id = '".$v2['serial_nr_id']."' AND article_id = '".$article_id."' ");
    	}
        //update stock
        $article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
        $article_data->next();
        $stock = $article_data->f("stock");

        $stock_packing = $article_data->f("packing");
        if(!$const['ALLOW_ARTICLE_PACKING']){
          $stock_packing = 1;
        }
        $stock_sale_unit = $article_data->f("sale_unit");
        if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
          $stock_sale_unit = 1;
        }

        $new_stock = $stock + (return_value($q)*$stock_packing/$stock_sale_unit);
        $this->db->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$article_id."'");

        $now=time();
        $serial_number = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");
        $this->db->insert("INSERT INTO stock_movements SET
          `date`              = '".time()."',
          created_by 					=	'".$_SESSION['u_id']."',
          order_id          =   '".$in['order_id']."',
          serial_number         =   '".$serial_number."',
          article_id          = '".$article_id."',
          article_name            =   '".addslashes($article_data->f("internal_name"))."',
          item_code           =   '".addslashes($article_data->f("item_code"))."',
          movement_type       = '13',
          quantity          =   '".return_value($q)*$stock_packing/$stock_sale_unit."',
          stock               = '".$stock."',
          new_stock           =   '".$new_stock."',
          delivery_date       ='".$in['date']."' ");
        if(WAC){
          generate_purchase_price($article_id);
        }
        //now we have to modify the stock on main location

        if($const['DEFAULT_STOCK_ADDRESS']){
          $current_stock_main= $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
				  $this->db->query("UPDATE dispatch_stock SET stock='".($current_stock_main +(return_value($q)*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$article_id."'   AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
        }
      }
    }
    if($const['CAN_SYNC']){
      $packet = array(
	      'command' => base64_encode(serialize($commands)),
        'command_id' => 1
      );
      Sync::send(WEB_URL, $packet);
      Sync::end($in['order_id']);
    }
    msg::success ( gm('Changes saved'),'success');
    return true;
  }

  /**
   * make_return_validate
   *
   * @return bool
   * @author PM
   **/
  function make_return_validate(&$in){

    $v = new validation($in);
    if($in['lines']){
			foreach ($in['lines'] as $key => $val) {
				if(return_value($val['ret_quantity'])> return_value($val['quantity_rem'])) {
				 	msg::error(gm('Invalid Quantities'),'error');
				 	return false;
				}
			}
    }
    $v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
    return $v->run();

  }

  /**
   * edit_return
   *
   * @return bool
   * @author me
   **/
  function edit_return(&$in)
  { 
    if(!$this->update_return_validate($in)){
      return false;
    }
    $const =array();
    $const['CAN_SYNC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='CAN_SYNC' ");
    $const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
    $const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
    $const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");
    $const['STOCK_MULTIPLE_LOCATIONS'] = $this->db->field("SELECT value FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS' ");
    $const['DEFAULT_STOCK_ADDRESS']=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default='1'");

    $commands = array();
    if($const['CAN_SYNC']){
      Sync::start(ark::$model.'-'.ark::$method);
    }

    if(isset($in['date']) && !empty($in['date']) ){
			$in['date'] = strtotime($in['date']);
		}

    $this->db->query("UPDATE pim_orders_return SET
                               `date` = '".$in['date']."',
                                note = '".$in['note']."'
                      WHERE  return_id='".$in['return_id']."' ");

    $this->db->query("DELETE FROM pim_articles_return WHERE  return_id='".$in['return_id']."'");

    if($in['lines']){
      foreach ($in['lines'] as $key => $val) {
      	$article_id = $val['article_id'];
      	$q = $val['ret_quantity'];
        $this->db->query("INSERT INTO  pim_articles_return
                            SET return_id='".$in['return_id']."',
                                article_id = '".$article_id."',
                                quantity = '".return_value($q)."',
                                order_id='".$in['order_id']."' ");

        // $q=return_value($q)-return_value($in['old_ret_quantity'][$article_id]);
        $q=return_value($q)-$val['old_ret_quantity'];

        //update batches
        $this->db->query("DELETE FROM batches_from_orders WHERE article_id =	'".$article_id."'  AND return_id='".$in['return_id']."'");

        foreach ($val['selected_b_n'] as $key3 => $v) {
			$current_in_stock = $this->db->field("SELECT in_stock FROM batches WHERE id = '".$v['batch_id']."' AND article_id = '".$article_id."' ");
			$new_in_stock = $current_in_stock+$v['b_n_q_selected'];
        	$this->db->query("UPDATE batches SET in_stock= '".$new_in_stock."' WHERE id = '".$v['batch_id']."' AND article_id = '".$article_id."' ");
        	$current_in_batches = $this->db->field("SELECT quantity FROM batches_from_orders WHERE batch_id = '".$v['batch_id']."' AND article_id = '".$article_id."' AND order_id='".$in['order_id']."' ");
        	if($current_in_batches){
        		$this->db->query("INSERT INTO batches_from_orders SET
								order_articles_id = 	'".$v['order_articles_id']."',
								article_id 		= 		'".$article_id."',
								batch_id 		= 		'".$v['batch_id']."',
								return_id 	 	= 		'".$in['return_id']."',
								order_id 		= 		'".$in['order_id']."',
								quantity 		= 		'".$v['b_n_q_selected']."',
                address_id    =     '".$v['address_id']."',
								delivery_id     =       '0'  
								");
        	}
    	}

    	$serial_numbers = $this->db->query("SELECT *  FROM serial_numbers_from_orders WHERE article_id =	'".$article_id."'  AND return_id='".$in['return_id']."'");
    	while($serial_numbers->move_next()){
    		$this->db->query("UPDATE serial_numbers SET status_id = 2 WHERE id = '".$serial_numbers->f('serial_number_id')."' AND article_id = '".$article_id."'  ");
    	}

        $this->db->query("DELETE FROM serial_numbers_from_orders WHERE article_id =	'".$article_id."'  AND return_id='".$in['return_id']."'");

        foreach ($val['selected_s_n'] as $k2 => $v2) {
		    
		    $this->db->query("INSERT INTO serial_numbers_from_orders SET
								order_articles_id = 	'".$v2['order_articles_id']."',
								article_id 		= 		'".$article_id."',
								serial_number_id 		= '".$v2['serial_nr_id']."',
								return_id 	 	= 		'".$in['return_id']."',
								order_id 		= 		'".$in['order_id']."',
								quantity 		= 		'1',
								delivery_id     =       '0'  
								");
		    $this->db->query("UPDATE serial_numbers SET status_id = 1 WHERE id = '".$v2['serial_nr_id']."' AND article_id = '".$article_id."' ");
    	}
        
        //update stock
        $article_data = $this->db->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$article_id."' ");
        $article_data->next();
        $stock = $article_data->f("stock");

        $stock_packing = $article_data->f("packing");
        if(!$const['ALLOW_ARTICLE_PACKING']){
          $stock_packing = 1;
        }
        $stock_sale_unit = $article_data->f("sale_unit");
        if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
          $stock_sale_unit = 1;
        }

        $new_stock = $stock + ($q*$stock_packing/$stock_sale_unit);
        $this->db->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$article_id."'");

        $now=time();
        $serial_number = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");
        if($new_stock!=$stock){
	        $this->db->insert("INSERT INTO stock_movements SET
	          date              = '".time()."',
	          created_by 		=	'".$_SESSION['u_id']."',
	          order_id          =   '".$in['order_id']."',
	          serial_number     =   '".$serial_number."',
	          article_id        = '".$article_id."',
	          article_name        =   '".addslashes($article_data->f("internal_name"))."',
	          item_code           =   '".addslashes($article_data->f("item_code"))."',
	          movement_type       = '14',
	          quantity          =   '".return_value($q)*$stock_packing/$stock_sale_unit."',
	          stock               = '".$stock."',
	          new_stock           =   '".$new_stock."',
            delivery_date       ='".$in['date']."' ");
	        //now we have to modify the stock on main location
		      if(WAC){
		        generate_purchase_price($article_id);
		      }

		      if($const['DEFAULT_STOCK_ADDRESS']){
		        $current_stock_main= $this->db->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$article_id."' AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
		        $this->db->query("UPDATE dispatch_stock SET stock='".($current_stock_main +(return_value($q)*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$article_id."'   AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
		      }
		    }
			}
    }

    if($const['CAN_SYNC']){
      $packet = array(
        'command' => base64_encode(serialize($commands)),
        'command_id' => 1
      );
      Sync::send(WEB_URL, $packet);
      Sync::end($in['order_id']);
    }
    msg::success ( gm('Changes saved'),'success');
    return true;
  }

  /**
   * update_return_validate
   *
   * @return bool
   * @author PM
   **/
  function update_return_validate(&$in){

    $v = new validation($in);
    $v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
    $v->field('return_id', 'ID', 'required:exist[pim_orders_return.return_id]', "Invalid Id");
    if($in['lines']){
			foreach ($in['lines'] as $key => $val) {
			  if(return_value($val['ret_quantity'])> return_value($val['quantity_rem'])){
			  	msg::error(gm('Invalid Quantities'),'error');
			    return false;
				}
      }
    }
    return $v->run();

  }

  /**
   * update_return_validate
   *
   * @return bool
   * @author PM
   **/
  function delete_return(&$in){

    if(!$this->delete_return_validate($in)){
      return false;
    }

    $const =array();
    $const['CAN_SYNC'] = $this->db->field("SELECT value FROM settings WHERE constant_name='CAN_SYNC' ");
    $const['ALLOW_STOCK'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_STOCK' ");
    $const['ALLOW_ARTICLE_PACKING'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING' ");
    $const['ALLOW_ARTICLE_SALE_UNIT'] = $this->db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT' ");

    $const['STOCK_MULTIPLE_LOCATIONS'] = $this->db->field("SELECT value FROM settings WHERE constant_name='STOCK_MULTIPLE_LOCATIONS' ");
    $const['DEFAULT_STOCK_ADDRESS']=$this->db->field("SELECT address_id FROM dispatch_stock_address WHERE is_default='1'");

    $commands = array();
    if($const['CAN_SYNC']){
      Sync::start(ark::$model.'-'.ark::$method);
    }
    $return_date = $this->db->field("SELECT pim_orders_return.date FROM pim_orders_return WHERE order_id='".$in['order_id']."' AND return_id='".$in['return_id']."' ");
   	$this->db->query("SELECT * FROM pim_articles_return WHERE  return_id='".$in['return_id']."' ");
   	while ($this->db->move_next()) {
      //update stock
      $article_data = $this->db2->query("SELECT item_code, internal_name, stock, packing, sale_unit FROM pim_articles WHERE article_id='".$this->db->f('article_id')."' ");
      $article_data->next();
      $stock = $article_data->f("stock");

      $stock_packing = $article_data->f("packing");
      if(!$const['ALLOW_ARTICLE_PACKING']){
        $stock_packing = 1;
      }
      $stock_sale_unit = $article_data->f("sale_unit");
      if(!$const['ALLOW_ARTICLE_SALE_UNIT']){
        $stock_sale_unit = 1;
      }

      $new_stock = $stock - ($this->db->f('quantity')*$stock_packing/$stock_sale_unit);
      $this->db2->query("UPDATE pim_articles SET stock='".$new_stock."' WHERE article_id='".$this->db->f('article_id')."'");

      //delete order details from article serial numbers
	$serial_numbers = $this->db2->query("SELECT * FROM serial_numbers_from_orders
				WHERE 		order_id 		= 	'".$in['order_id']."'
				AND 			return_id 	= 	'".$in['return_id']."'
				AND 			article_id = 	'".$this->db->f('article_id')."'");
	while($serial_numbers->next()){
			$this->db2->query("UPDATE 	serial_numbers
						SET 	status_id 		= 2
								 WHERE id = '".$serial_numbers->f('serial_number_id')."' ");
			}		
	$this->db2->query("DELETE FROM serial_numbers_from_orders WHERE article_id =	'".$this->db->f('article_id')."'  AND return_id='".$in['return_id']."'");
	


	//delete order details from article batches
	$batches_data = $this->db2->query("SELECT * FROM batches_from_orders
				WHERE 		order_id 		= 	'".$in['order_id']."'
				AND 			return_id 	= 	'".$in['return_id']."'
				AND 			article_id = 	'".$this->db->f('article_id')."' ");
	while($batches_data->next()){
				$batch_in_stock = $this->db2->field("SELECT in_stock FROM batches WHERE id = '".$batches_data->f('batch_id')."' ");
				$new_batch_stock = $batch_in_stock - $batches_data->f('quantity');
				$this->db2->query("UPDATE batches SET status_id = 1, in_stock = '".$new_batch_stock."', status_details_2 = '' WHERE id = '".$batches_data->f('batch_id')."' ");

        $location_current_stock_batch = $this->db2->query("SELECT quantity FROM  batches_dispatch_stock  WHERE article_id='".$batches_data->f('article_id')."' AND  address_id='".$batches_data->f('address_id')."' and customer_id='0' and batch_id='".$batches_data->f('batch_id')."'");

        if($location_current_stock_batch->move_next()){
              //update dispach batch from stock
                $this->db2->query("UPDATE batches_dispatch_stock SET quantity='".($location_current_stock_batch->f('quantity')- $batches_data->f('quantity'))."' WHERE article_id='".$batches_data->f('article_id')."' AND  address_id='".$batches_data->f('address_id')."' and customer_id='0' and batch_id='".$batches_data->f('batch_id')."'");
          }
			}
	$this->db2->query("DELETE FROM batches_from_orders
				WHERE 		order_id 		= 	'".$in['order_id']."'
				AND 			return_id 	= 	'".$in['return_id']."' 
				AND 			article_id = 	'".$this->db->f('article_id')."' ");


      $now = time();
      $serial_number = $this->db2->field("SELECT serial_number FROM pim_orders WHERE order_id = '".$in['order_id']."'");

      $this->db2->insert("INSERT INTO stock_movements SET
      date            = '".time()."',
      created_by 					=	'".$_SESSION['u_id']."',
      serial_number         =   '".$serial_number."',
      order_id          =   '".$in['order_id']."',
      article_id          = '".$this->db->f('article_id')."',
      article_name            =   '".addslashes($article_data->f("internal_name"))."',
      item_code           =   '".addslashes($article_data->f("item_code"))."',
      movement_type       = '15',
      quantity          =   '".$this->db->f('quantity')*$stock_packing/$stock_sale_unit."',
      stock             = '".$stock."',
      new_stock           =   '".$new_stock."',
      backorder_articles      =   '".$backorder_articles."'
      ");
      /*end for  stock movements*/
      array_push($commands,"UPDATE pim_articles SET stock=".$new_stock." WHERE article_id='".$this->db->f('article_id')."'");
      if(WAC){
        generate_purchase_price($article_id);
      }
      //now we have to modify the stock on main location

      if($const['DEFAULT_STOCK_ADDRESS']){
        $current_stock_main= $this->db2->field("SELECT  stock FROM  dispatch_stock  WHERE article_id='".$this->db->f('article_id')."' AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
        $this->db2->query("UPDATE dispatch_stock SET stock='".($current_stock_main -($this->db->f('quantity')*$stock_packing/$stock_sale_unit))."' WHERE article_id='".$this->db->f('article_id')."'   AND  address_id='".$const['DEFAULT_STOCK_ADDRESS']."' and customer_id='0'");
      }
   	}

    $this->db->query("DELETE FROM pim_orders_return WHERE order_id='".$in['order_id']."' AND return_id='".$in['return_id']."' ");
    $this->db->query("DELETE FROM pim_articles_return WHERE  return_id='".$in['return_id']."' ");

    /*#update batches
    $batch_q = $this->db->query("SELECT batch_id, quantity AS quantity_b FROM batches_from_orders WHERE order_id='".$in['order_id']."' AND return_id='".$in['return_id']."' ");
    while($batch_q->move_next()){
    	$batches_q = $this->db->query("SELECT quantity FROM batches WHERE id='".$batch_q->f('batch_id')."' ");
    	$new_in_stock = $batches_q->f('quantity')-$batch_q->f('quantity_b');
        $this->db->query("UPDATE batches SET in_stock= '".$new_in_stock."' WHERE id = '".$batch_q->f('batch_id')."' ");
    }
    $this->db->query("DELETE FROM batches_from_orders WHERE order_id='".$in['order_id']."' AND return_id='".$in['return_id']."' ");*/

    if(CAN_SYNC){
     	$packet = array(
        'command' => base64_encode(serialize($commands)),
        'command_id' => 1
      );
    	Sync::end();
  	}

    msg::success ( gm('Return was succesfully deleted'),'success');
    return true;
  }

  /**
   * update_return_validate
   *
   * @return bool
   * @author PM
   **/
  function delete_return_validate(&$in){

    $v = new validation($in);
    $v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
    $v->field('return_id', 'ID', 'required:exist[pim_orders_return.return_id]', "Invalid Id");

    return $v->run();
  }

  /**
	 * Send order by email
	 *
	 * @return bool
	 * @author PM
	 **/
	function pdf_settings(&$in){

   	if(!$in['p_order_id']){
		  if(!($in['order_id']&&$in['pdf_layout']&&$in['logo']) )
			{
				return false;
			}
    }else{
      if(!$in['p_order_id'] )
      {
        return false;
      }
    }
		if($in['logo'] == '../img/no-logo.png'){
			$in['logo'] = 'img/no-logo.png';
		}
    if($in['p_order_id']){
     
      $this->db->query("UPDATE pim_p_orders SET email_language='".$in['email_language']."',p_order_type='".$in['pdf_layout']."',pdf_logo='".$in['logo']."', identity_id='".$in['identity_id']."' WHERE p_order_id = '".$in['p_order_id']."' ");
	  }else{
	    $this->db->query("UPDATE pim_orders SET pdf_layout='".$in['pdf_layout']."',pdf_logo='".$in['logo']."',email_language='".$in['email_language']."', identity_id='".$in['identity_id']."' WHERE order_id = '".$in['order_id']."' ");

      # for pdf
      $inv = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
      $params = array();
      if($inv->f('pdf_layout')){
        $params['logo'] = $inv->f('pdf_logo');
        $params['type']=$inv->f('pdf_layout');
      }else{
        $params['type']=ACCOUNT_ORDER_PDF_FORMAT;
      }
      #if we are using a customer pdf template
      if(defined('USE_CUSTOME_ORDER_PDF') && USE_CUSTOME_ORDER_PDF == 1 && $inv->f('pdf_layout') == 0){
        $params['custom_type']=ACCOUNT_ORDER_PDF_FORMAT;
        unset($params['type']);
      }
      $params['order_id'] = $in['order_id'];
      $params['lid'] = $inv->f('email_language');
      $params['save_as'] = 'F';
      $this->generate_pdf($params);
    }
		  // $this->db->query("UPDATE pim_orders SET WHERE order_id = '".$in['order_id']."' ");

	  msg::success ( gm("Pdf settings have been updated"),'success');
	  return true;
	}

	/**
	 * prepare the orders
	 *
	 * @return nothing
	 * @author PM
	 */

	function order_prepare(&$in){

		if(!$this->prepare_validate($in))
		{
			json_out($in);
			return false;
		}
		// if(!$in['purchase']){
		// 	ark::$controller = 'norder';
		// 	// ark::run('order-norder');
		// }else{
		// 	if(!$this->tryAddC($in)){
  //     return false;
  //   }

  //     ark::$controller = 'npo_order';
		// 	// ark::run('order-npo_order');
		// }
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
		return true;
	}

	/**
	 * order_prepare validate
	 * if none of the fields are filed it
	 * will return false else will return true
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */

	function prepare_validate(&$in)
	{

		$v = new validation($in);
		$v->field('contact_id','Contact','required');
		$contact = $v->run();
		$z = new validation($in);
		$z->field('buyer_id','Company','required');
		$customer = $z->run();
		if(!$contact && !$customer){
			if(ark::$controller=='orders'){
				msg::error ( gm('Please select a company or a contact'),'error');
			}elseif(!$in['add_customer']){
				msg::error ( gm('Please select a Company'),'error');
			}
			return false;
		}
		return true;
	}

	/**
	 * order add
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function add_order(&$in)
	{
		if(!$this->add_order_validate($in)){
			return false;
		}

		if ($in['rdy_invoice'])
		{
			$rdy_invoice = 1;
		}
		else
		{
			$rdy_invoice = 0;
		}

		if(!$in['notes']){
			$in['notes'] = $this->db->field("SELECT value FROM default_data WHERE type='order_note'");
    }
    if(!$in['pdf_layout'] && $in['identity_name']){
      $in['pdf_layout'] = $this->db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$in['identity_name']."' and module='order'");
    }
    if(isset($in['date_h']) && !empty($in['date_h']) ){
			$in['date_h'] = strtotime($in['date_h']);
		}
		if(isset($in['del_date']) && !empty($in['del_date']) ){
			$in['del_date'] = strtotime($in['del_date']);
		}

		 if($in['contact_id']) {
      $in['contact_name']=addslashes($this->dbu->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE customer_contacts.contact_id='".$in['contact_id']."'"));
   }

   if($in['buyer_id']){
      $buyer_info = $this->dbu->query("SELECT customer_addresses.address_id,customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id
                  FROM customers
                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.is_primary=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
        $same_address=0;
       //if($in['sameAddress']==1){
            $address_info = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
            $free_field=addslashes($address_info);
            $in['customer_address']=addslashes($buyer_info->f('address'));
            $in['customer_zip']=$buyer_info->f('zip');
            $in['customer_city']=addslashes($buyer_info->f('city'));
            $in['customer_country_id']=$buyer_info->f('country_id');
            /*$in['delivery_address_id']=0;
            $in['delivery_address']='';
            if($in['main_address_id']!=$buyer_info->f('address_id')){
            	$new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
	            $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
	            $free_field=addslashes($new_address_txt);
            }*/
       //}else{
            if($in['delivery_address_id'] && $in['delivery_address_id'] != $buyer_info->f('address_id')){
              $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
              $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
              $same_address=$in['delivery_address_id'];
              $free_field=addslashes($new_address_txt);
              $in['delivery_address']=$free_field;
            }                
        //}
   }else{
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
   
      $same_address=0;  
      if($in['sameAddress']==1){
        $address_info = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
        $free_field=addslashes($address_info);
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $same_address=$in['delivery_address_id'];
        $free_field=addslashes($new_address_txt);      
      }
   } 

  /* if(!$in['c_quote_id'] && !$in['deal_id']){
      $in['identity_id']= $this->db_users->field("SELECT default_identity_id FROM user_info WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
    }else{
      $in['identity_id']=0;
    }*/

   //Set email language as account / contact language
    if(!$in['email_language']){
          $emailMessageData = array('buyer_id'    => $in['buyer_id'],
                  'contact_id'    => $in['contact_id'],
                  'param'     => 'add');
          $in['email_language'] = get_email_language($emailMessageData);
    }

    //End Set email language as account / contact language

    $customer = $this->dbu->query("SELECT customers.c_email, customers.comp_phone, customers.btw_nr,customers.name AS company_name,customer_legal_type.name as l_name FROM customers
      LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
     WHERE customers.customer_id='".$in['customer_id']."' ");
    $in['customer_name']=addslashes(stripslashes($customer->f('company_name').' '.$customer->f('l_name')));
    $in['customer_phone']      = $customer->f('comp_phone');
    $in['customer_email']      = $customer->f('c_email');
    $in['customer_vat_number']   = $customer->f('btw_nr');

    if(!$in['currency_rate']){
      $in['currency_rate'] = 1;
    }

   	$in['serial_number'] = generate_order_serial_number(DATABASE_NAME);
		$in['order_id'] = $this->db->insert("INSERT INTO pim_orders SET
										customer_id 						= '".$in['customer_id']."',
										customer_name 						= '".$in['customer_name']."',
										contact_name 						= '".$in['contact_name']."',
										contact_id							= '".$in['contact_id']."',
										company_number 						= '".$in['company_number']."',
										customer_vat_id 					= '".$in['customer_vat_id']."',
										customer_email 						= '".$in['customer_email']."',
										customer_bank_name 					= '".$in['customer_bank_name']."',
										customer_bank_iban 					= '".$in['customer_bank_iban']."',
										customer_bic_code 					= '".$in['customer_bic_code']."',
										customer_vat_number 				= '".$in['customer_vat_number']."',
										customer_country_id 				= '".$in['customer_country_id']."',
										customer_state						= '".$in['customer_state']."',
										customer_city 						= '".$in['customer_city']."',
										rdy_invoice 						= '".$rdy_invoice."',
										customer_zip 						= '".$in['customer_zip']."',
										customer_phone 						= '".$in['customer_phone']."',
										customer_cell 						= '".$in['customer_cell']."',
										customer_address 					= '".$in['customer_address']."',
										created_by							= '".$_SESSION['u_id']."',
										seller_name       					= '".addslashes(utf8_encode(ACCOUNT_COMPANY))."',
                    seller_d_address     				= '".addslashes(utf8_encode(ACCOUNT_DELIVERY_ADDRESS))."',
                    seller_d_zip       					= '".addslashes(ACCOUNT_DELIVERY_ZIP)."',
                    seller_d_city         				= '".addslashes(utf8_encode(ACCOUNT_DELIVERY_CITY))."',
                    seller_d_country_id   				= '".ACCOUNT_DELIVERY_COUNTRY_ID."',
                    seller_d_state_id     				= '".ACCOUNT_DELIVERY_STATE_ID."',
                    seller_bwt_nr         				= '".addslashes(ACCOUNT_VAT_NUMBER)."',
                    delivery_id 			    		= '".$in['delivery_id']."',
                    delivery_address 					= '".$in['delivery_address']."',
                    `date` 								= '".$in['date_h']."',
					    			payment_term 						= '".$in['payment_term']."',
					    			payment_type 						= '".$in['payment_type']."',
										serial_number 						= '".$in['serial_number']."',
										field								= '".$in['field']."',
										active								= '1',
										buyer_ref							= '".$in['buyer_ref']."',
										customer_ref						= '".$in['customer_ref']."',
										discount							= '".return_value($in['discount'])."',
										use_package           				= '".ALLOW_ARTICLE_PACKING."',
                    use_sale_unit         				= '".ALLOW_ARTICLE_SALE_UNIT."',
										currency_rate						= '".$in['currency_rate']."',
										our_ref								= '".$in['our_ref']."',
										your_ref							= '".$in['your_ref']."',
										del_date							= '".$in['del_date']."',
                    del_note                    		= '".$in['del_note']."',
										remove_vat							= '".$in['remove_vat']."',
                    quote_id 							= '".$in['quote_id']."',
                    email_language						= '".$in['email_language']."',
										currency_type						= '".$in['currency_type']."' ,
										show_vat              				= '".$in['show_vat']."',
										address_info        				= '".$free_field."',
										default_delivery_address    		= '".$in['delivery_address']."',
										apply_discount						= '".$in['apply_discount']."',
										author_id							= '".$in['author_id']."',
                    acc_manager_id           			= '".$in['acc_manager_id']."',
                    acc_manager_name           			= '".$in['acc_manager_name']."',
										discount_line_gen					= 	'".return_value($in['discount_line_gen'])."',
                    identity_id             			= '".$in['identity_id']."',
                    delivery_address_id					= '".$in['delivery_address_id']."',
                    pdf_layout              			= '".$in['pdf_layout']."',
                    same_address='".$same_address."',
                    main_address_id           = '".$in['main_address_id']."',
                    vat_regime_id 		='".$in['vat_regime_id']."',
                    source_id 			='".$in['source_id']."',
                    type_id 			='".$in['type_id']."',
                    segment_id 		='".$in['segment_id']."',
                    yuki_project_id     ='".$in['yuki_project_id']."',
                    cat_id          = '".$in['cat_id']."',
                    subject ='".addslashes($in['subject'])."' ");

		// add multilanguage order notes in note_fields
		$lang_note_id = $in['email_language'];
		
/*    $langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
    while($langs->next()){
    	foreach ($in['translate_loop'] as $key => $value) {
    		if($value['lang_id'] == $langs->f('lang_id')){
    			$in['nota'] = $value['notes'];
    			break;
    		}
    	}

    	if($lang_note_id == $langs->f('lang_id')){
    		$active_lang_note = 1;
    	}else{
    		$active_lang_note = 0;
    	}*/

      //commented because it did not take into consideration legal mentions from vat regimes
    	//Add default notes values based on language
      	/*$emailLanguageDefaultNotesData = array('email_language' => $in['email_language'],
          										'default_name_note'	=> 'order_note',
          										'default_type_note'	=> 'order_note',
          										'default_name_free_text_content' => 'quote_term',
          										'default_type_free_text_content' => 'quote_terms',
          									);

        $defaultNotesData = get_email_message_default_notes($emailLanguageDefaultNotesData);
        //End Add default notes values based on language

    	$this->db->insert("INSERT INTO note_fields SET
    										item_id 					=	'".$in['order_id']."',
    										lang_id 					= 	'".$in['email_language']."',
    										active 						= 	'1',
    										item_type 				= 	'order',
    										item_name 				= 	'notes',
    										item_value				= 	'".addslashes($defaultNotesData['notes'])."'
    	");*/

      $this->db->insert("INSERT INTO note_fields SET
                        item_id           = '".$in['order_id']."',
                        lang_id           =   '".$in['email_language']."',
                        active            =   '1',
                        item_type         =   'order',
                        item_name         =   'notes',
                        item_value        =   '".$in['notes']."'
      ");

    	
   /* }*/
/*    $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active = '1' ORDER BY sort_order ");
    while($custom_langs->next()) {
    	foreach ($in['translate_loop_custom'] as $key => $value) {
    		if($value['lang_id'] == $custom_langs->f('lang_id')){
    			$in['nota'] = $value['notes'];
    			break;
    		}
    	}
    	if($lang_note_id == $custom_langs->f('lang_id')) {
    		$active_lang_note = 1;
    	} else {
    		$active_lang_note = 0;
    	}
    	$this->db->insert("INSERT INTO note_fields SET
    									item_id 		= '".$in['order_id']."',
    									lang_id 		= '".$in['email_language']."',
    									active 			= '".$active_lang_note."',
    									item_type 		= 'order',
    									item_name 		= 'notes',
    									item_value 		= '".$in['notes']."'
    	");
    }*/

		//add articles
		$global_disc = return_value($in['discount']);
		if($in['apply_discount'] < 2){
			$global_disc = 0;
		}
    $order_total=0;
		foreach ($in['tr_id'] as $nr => $row)
		{   $line_total=0;

	    if(!$row['sale_unit'] || !ALLOW_ARTICLE_PACKING){
				$row['sale_unit']=1;
			}
			if(!$row['packing'] ||  !ALLOW_ARTICLE_PACKING){
				$row['packing']=1;
			}

			$discount_line = return_value($row['disc']);
			if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
				$discount_line = 0;
			}

			$q = return_value2($row['packing']) && $row['sale_unit'] ? return_value($row['quantity']) * return_value2($row['packing']) / $row['sale_unit'] : 0;

			$price_line = return_value($row['price']) - (return_value($row['price']) * $discount_line /100);

			$line_total=$price_line * $q;

      $order_total+= $line_total - ($line_total*$global_disc/100);
      $is_from_import = $this->db->field("SELECT from_adveo FROM pim_articles WHERE article_id='".$row['article_id']."'");
      $use_combined = $this->db->field("SELECT use_combined FROM pim_articles WHERE article_id='".$row['article_id']."'");

			if($row['is_tax'] != 1){

        if($row['is_combined'] || (!$use_combined && !$row['component_for']) ){
            $last_combined = '';
          }

        if( $row['has_variants'] ){
          $last_variant_parent = '';
        } 

				$in['order_articles_id'] = $this->db->insert("INSERT INTO pim_order_articles SET
																		 from_import = '".$is_from_import."',
																		 order_id = '".$in['order_id']."',
																		 article_id = '".$row['article_id']."',
																		 discount = '".return_value($row['disc'])."',
																		 quantity = '".return_value($row['quantity'])."',
																		 article = '".htmlspecialchars($row['article'],ENT_QUOTES,'UTF-8')."',
																		 price = '".return_value($row['price'])."',
                                     purchase_price = '".$row['purchase_price']."',
                                     purchase_price_other_currency='".$row['purchase_price_other_currency']."',
                                     purchase_price_other_currency_id='".$row['purchase_price_other_currency_id']."',
																		 price_with_vat = '".return_value($row['price_vat'])."',
																		 vat_value = '".$row['vat_value']."',
																		 vat_percent = '".$row['percent']."',
																		 sort_order='".$nr."',
																		 sale_unit = '".$row['sale_unit']."',
																		 packing = '".return_value2($row['packing'])."',
																		 content = '".$row['content']."',
																		 article_code = '".addslashes($row['article_code'])."',
                                     has_variants = '".$row['has_variants']."',
                                     has_variants_done ='".$row['has_variants_done']."',
                                     is_variant_for ='".$row['is_variant_for']."',
                                     is_variant_for_line ='".$last_variant_parent."',
                                     variant_type='".$row['variant_type']."',
                                     is_combined ='".$row['is_combined']."',
                                     component_for ='".$last_combined."',
                                     visible='".$row['visible']."'
															");
        if($row['is_combined']){
            $last_combined = $in['order_articles_id'];
          }
        if($row['has_variants'] && $row['has_variants_done']){
            $last_variant_parent = $in['order_articles_id'];
          }
        if($row['is_variant_for']){
            $last_variant_parent = '';
          }
			}else{ //is_tax
				$in['order_articles_id'] = $this->db->insert("INSERT INTO pim_order_articles SET
																		 from_import = '".$is_from_import."',
																		 order_id = '".$in['order_id']."',
																		 tax_id = '".$row['article_id']."',
																		 tax_for_article_id = '".$row['tax_for_article_id']."',
																		 quantity = '".return_value($row['quantity'])."',
																		 article = '".htmlspecialchars($row['article'],ENT_QUOTES,'UTF-8')."',
																		 price = '".return_value($row['price'])."',
																		 price_with_vat = '".return_value($row['price_vat'])."',
																		 vat_value = '".$row['vat_value']."',
																		 vat_percent = '".$row['percent']."',
																		 sort_order='".$nr."',
																		 discount = '".return_value($row['disc'])."',
																		 sale_unit = '".$row['sale_unit']."',
																		 packing = '".return_value2($row['packing'])."',
																		 content = '".$row['content']."',
																		 article_code = '".addslashes($row['article_code'])."',
                                     visible = '1'
															");
			}
		}

   	if($in['currency_type'] != ACCOUNT_CURRENCY_TYPE){
			$order_total = $order_total*return_value($in['currency_rate']);
		}
		if($in['c_quote_id']){
			$this->db->query("UPDATE tblquote SET pending = '0' WHERE id = '".$in['c_quote_id']."' ");
			$tracking_data=array(
		            'target_id'         => $in['order_id'],
		            'target_type'       => '4',
		            'target_buyer_id'   => $in['customer_id'],
		            'lines'             => array(
		                                          array('origin_id'=>$in['c_quote_id'],'origin_type'=>'2')
		                                    )
		          );
		      addTracking($tracking_data);
		}
		if($in['duplicate_order_id'] ){
			$tracking_data=array(
		            'target_id'         => $in['order_id'],
		            'target_type'       => '4',
		            'target_buyer_id'   => $in['customer_id'],
		            'lines'             => array(
		                                          array('origin_id'=>$in['duplicate_order_id'],'origin_type'=>'4')
		                                    )
		          );
		      addTracking($tracking_data);
		}
		if($in['deal_id']){
			$trace_id=$this->db->field("SELECT trace_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
			if($trace_id){
				$linked_doc=$this->db->field("SELECT tracking_line.line_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
						WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$trace_id."' ");
				if($linked_doc){
					$this->db->query("UPDATE tracking_line SET origin_id='".$in['deal_id']."' WHERE line_id='".$linked_doc."' ");	
				}else{
					$this->db->query("INSERT INTO tracking_line SET
							origin_id='".$in['deal_id']."',
							origin_type='11',
							trace_id='".$trace_id."' ");
				}
			}else{
				$tracking_data=array(
			            'target_id'         => $in['order_id'],
			            'target_type'       => '4',
			            'target_buyer_id'   => $in['buyer_id'],
			            'lines'             => array(
			                                          array('origin_id'=>$in['deal_id'],'origin_type'=>'11')
			                                    )
			          );
			      addTracking($tracking_data);
			}
		}
		if(!$in['c_quote_id'] && !$in['deal_id']  && !$in['duplicate_order_id']){
			$tracking_data=array(
		            'target_id'         => $in['order_id'],
		            'target_type'       => '4',
		            'target_buyer_id'   => $in['customer_id'],
		            'lines'             => array()
		          );
		      addTracking($tracking_data);
		}
		$this->db->query("UPDATE pim_orders SET amount='".$order_total."' WHERE order_id='".$in['order_id']."'");
    msg::success ( gm("Order successfully added"),'success');
		insert_message_log($this->pag,'{l}Order successfully added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;
		$in['add_problem'] = $this->pag;

    if($in['duplicate_order_id']){
      $original_duplicate_serial=$this->db->field("SELECT serial_number FROM pim_orders WHERE order_id='".$in['duplicate_order_id']."' ");

      //on the original order
      insert_message_log('order','{l}This order was duplicated by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl} <a href="[SITE_URL]order/view/'.$in['order_id'].'">{l}Order{endl} '.$in['serial_number'].'</a>',$this->field_n,$in['duplicate_order_id']);

      //on the resulting order
      insert_message_log('order','{l}This order was duplicated by{endl} '.get_user_name($_SESSION['u_id']).' {l}from{endl} <a href="[SITE_URL]order/view/'.$in['duplicate_order_id'].'">{l}Order{endl} '.$original_duplicate_serial.'</a>',$this->field_n,$in['order_id']);

      //insert_message_log($this->pag,'{l}Order successfully added by duplicating{endl} '.$original_duplicate_serial,$this->field_n,$in['order_id']);
    }

		// ark::$controller = 'order';
		/*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
															AND name		= 'order-orders_show_info'	");*/
		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= :user_id
															AND name		= :name	",['user_id'=>$_SESSION['u_id'],'name'=>'order-orders_show_info']);															
		if(!$show_info->move_next()) {
			/*$this->db_users->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
													name 	= 'order-orders_show_info',
													value 	= '1' ");*/
			$this->db_users->insert("INSERT INTO user_meta SET 	user_id = :user_id,
													name 	= :name,
													value 	= :value ",
												['user_id' => $_SESSION['u_id'],
												 'name' 	=> 'order-orders_show_info',
												 'value' 	=> '1']
											);													
		} else {
			/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id = '".$_SESSION['u_id']."'
														  AND name 	 	= 'order-orders_show_info' ");*/
			$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id = :user_id
														  AND name 	 	= :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'order-orders_show_info']);														  
		}
		unset($in['c_quote_id']);
		unset($in['quote_id']);
    $count = $this->db->field("SELECT COUNT(order_id) FROM pim_orders ");
    if($count == 1){
      doManageLog('Created the first order.','',array( array("property"=>'first_module_usage',"value"=>'Order') ));
    }

    $inv = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
      $params = array();
      if($inv->f('pdf_layout')){
        $params['logo'] = $inv->f('pdf_logo');
        $params['type']=$inv->f('pdf_layout');
      }else{
        $params['type']=ACCOUNT_ORDER_PDF_FORMAT;
      }
      #if we are using a customer pdf template
      if(defined('USE_CUSTOME_ORDER_PDF') && USE_CUSTOME_ORDER_PDF == 1 && $inv->f('pdf_layout') == 0){
        $params['custom_type']=ACCOUNT_ORDER_PDF_FORMAT;
        unset($params['type']);
      }
      $params['order_id'] = $in['order_id'];
      $params['lid'] = $inv->f('email_language');
      $params['save_as'] = 'F';
      $this->generate_pdf($params);

		return true;
	}
	/**
	 * order add validate
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function add_order_validate(&$in)
	{
		$v = new validation($in);
		/*$v->field('serial_number', 'Name', 'required',gm('Order Nr').'* '.gm('is required').'<br />');
		if(!$v->run()){
			return false;
		}*/
		if (ark::$method == 'add_order') {
			/*$v->field('serial_number', 'Serial Number', 'unique[pim_orders.serial_number]',gm('Order Nr').' '.gm('is already in use'));
			if(!$v->run()){
				$v = null;
				$in['serial_number'] = generate_order_serial_number(DATABASE_NAME);
				$v = new validation($in);
				$v->field('serial_number', 'Name', 'required',gm('Order Nr').'* '.gm('is required').'<br />');
			}*/
		}else{
			$v->field('serial_number', "Serial Number", "required:unique[pim_orders.serial_number.( order_id!='".$in['order_id']."')]");
		}

		$v->field('customer_name', "Company Name", "required",gm('Customer Name').' '.gm('is required').'.<br />');

		return $v->run();
	}

	/**
	 * order update
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function update_order(&$in)
	{
		if(!$this->update_order_validate($in)){
			return false;
		}
		if ($in['rdy_invoice'])
		{
			$rdy_invoice = 1;
		}
		else
		{
			$rdy_invoice = 0;
		}

		if(!$in['delivery_cost_amount']){
			$in['delivery_cost_amount'] = return_value(0);
		}
		if(!$in['show_shipping_price']){
			$in['show_shipping_price'] = 0;
		}
		if(!$in['acc_manager_name']){
		  $in['acc_manager_id']=0;
		}
		if(isset($in['date_h']) && !empty($in['date_h']) ){
			$in['date_h'] = strtotime($in['date_h']);
		}
		if(isset($in['del_date']) && !empty($in['del_date']) ){
			$in['del_date'] = strtotime($in['del_date']);
		}

		   if($in['buyer_id']){
        $customer_data=$this->dbu->query("SELECT customers.c_email,customers.comp_phone,customers.btw_nr,customers.name AS company_name,customer_legal_type.name as l_name FROM customers
          LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
         WHERE customers.customer_id='".$in['buyer_id']."' ");
          $in['customer_name']=addslashes(stripslashes($customer_data->f('company_name').' '.$customer_data->f('l_name')));
          $in['customer_email']=$customer_data->f('c_email');
          $in['customer_phone']=$customer_data->f('comp_phone');
          $in['customer_vat_number']=$customer_data->f('btw_nr');

      $buyer_info = $this->dbu->query("SELECT customer_addresses.*
                  FROM customers
                  LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
                  AND customer_addresses.is_primary=1
                  WHERE customers.customer_id='".$in['buyer_id']."' ");
        $same_address=0;
       //if($in['sameAddress']==1){
            $address_info = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
            /*if($buyer_info->f('address_id') != $in['main_address_id']){
                $buyer_addr = $this->dbu->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
                $address_info = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
            }*/
            $free_field=addslashes($address_info);
            $in['customer_address']=addslashes($buyer_info->f('address'));
            $in['customer_zip']=$buyer_info->f('zip');
            $in['customer_city']=addslashes($buyer_info->f('city'));
            $in['customer_country_id']=$buyer_info->f('country_id');

            /*$in['delivery_address']='';
            $in['delivery_address_id']=0;*/
            
       //}else{
            if($in['delivery_address_id'] && $in['delivery_address_id'] != $buyer_info->f('address_id')){
                $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
              $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
              $same_address=$in['delivery_address_id'];
              $free_field=addslashes($new_address_txt);
              $in['delivery_address']=$free_field;
            }       
        //}
   }else{
      $contact_address = $this->dbu->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
   
      $same_address=0;  
      if($in['sameAddress']==1){
        $address_info = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
        $free_field=addslashes($address_info);
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ");
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $same_address=$in['delivery_address_id'];
        $free_field=addslashes($new_address_txt);      
      }
   }

   		//Set email language as account / contact language
		$emailMessageData = array('buyer_id' 		=> $in['buyer_id'],
	    						  'contact_id' 		=> $in['contact_id'],
	    						  'item_id'			=> $in['order_id'],
	    						  'email_language' 	=> $in['email_language'],
	    						  'table'			=> 'pim_orders',
	    						  'table_label'		=> 'order_id',
	    						  'table_buyer_label' => 'customer_id',
	    						  'table_contact_label' => 'contact_id',
	    						  'param'		 	=> 'edit');
		$in['email_language'] = get_email_language($emailMessageData);
	    //End Set email language as account / contact language

    	$default_email_language = $this->dbu->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");
	 	if($in['email_language']==0){
	 		$in['email_language'] = $default_email_language;
	 	}

    	$this->db->query("UPDATE pim_orders SET customer_name 					= '".$in['customer_name']."',
                                            contact_name 					= '".$in['contact_name']."',
                                            contact_id						= '".$in['contact_id']."',
                                            company_number 					= '".$in['company_number']."',
                                            customer_vat_id 				= '".$in['customer_vat_id']."',
                                            customer_email 					= '".$in['customer_email']."',
                                            customer_bank_name 				= '".$in['customer_bank_name']."',
                                            customer_bank_iban 				= '".$in['customer_bank_iban']."',
                                            customer_bic_code 				= '".$in['customer_bic_code']."',
                                            customer_vat_number 			= '".$in['customer_vat_number']."',
                                            customer_country_id 			= '".$in['customer_country_id']."',
                                            customer_state 					= '".$in['customer_state']."',
                                            customer_city 					= '".$in['customer_city']."',
                                            rdy_invoice 					= '".$rdy_invoice."',
                                            customer_zip 					= '".$in['customer_zip']."',
                                            customer_phone 					= '".$in['customer_phone']."',
                                            buyer_ref 						= '".$in['buyer_ref']."',
                                            customer_address 				= '".$in['customer_address']."',
                                            seller_name       				= '".addslashes(utf8_encode(ACCOUNT_COMPANY))."',
                                            seller_d_address    			= '".addslashes(utf8_encode(ACCOUNT_DELIVERY_ADDRESS))."',
                                      		  seller_d_zip       				= '".ACCOUNT_DELIVERY_ZIP."',
                                     		    seller_d_city       			= '".addslashes(utf8_encode(ACCOUNT_DELIVERY_CITY))."',
                                      		  seller_d_country_id 			= '".ACCOUNT_DELIVERY_COUNTRY_ID."',
                                      		  seller_d_state_id   			= '".ACCOUNT_DELIVERY_STATE_ID."',
                                      		  seller_bwt_nr       			= '".addslashes(ACCOUNT_VAT_NUMBER)."',
											                      `date` 							= '".$in['date_h']."',
											                      payment_term 					= '".$in['payment_term']."',
											                      payment_type 					= '".$in['payment_type']."',
                                            serial_number 					= '".$in['serial_number']."',
                                            discount						= '".return_value($in['discount'])."',
                                            delivery_id						= '".$in['delivery_id']."',
                                            our_ref							= '".$in['our_ref']."',
                                            your_ref						= '".$in['your_ref']."',
                                            del_date						= '".$in['del_date']."',
                                            del_note      					= '".$in['del_note']."',
                                            delivery_address 				= '".$in['delivery_address']."',
                                            show_vat            			= '".$in['show_vat']."',
                                            address_info        			= '".trim($free_field)."',
                                            main_address_id           = '".$in['main_address_id']."',
                                            same_address              ='".$same_address."',
                                            apply_discount					= '".$in['apply_discount']."',
                                            author_id						= '".$in['author_id']."',
                                            discount_line_gen				= '".return_value($in['discount_line_gen'])."',
                                            shipping_price 		        	= '".$in['delivery_cost_amount']."',
                                            show_shipping_price 		  	= '".$in['show_shipping_price']."',
                                            acc_manager_id            		= '".$in['acc_manager_id']."',
                                            acc_manager_name          		= '".$in['acc_manager_name']."',
                                            shipping_price 					= '".$in['delivery_cost_amount']."',
                                            show_shipping_price 			= '".$in['show_shipping_price']."',
                                            email_language					= '".$in['email_language']."',
                                            delivery_address_id				 = '".$in['delivery_address_id']."',
                                            identity_id             		='".$in['identity_id']."',
                                            vat_regime_id 		  ='".$in['vat_regime_id']."',
                                            source_id 			    ='".$in['source_id']."',
                                            type_id 			      ='".$in['type_id']."',
                                            segment_id 		      ='".$in['segment_id']."',
                                            yuki_project_id     ='".$in['yuki_project_id']."',
                                            cat_id              = '".$in['cat_id']."',
                                            subject ='".addslashes($in['subject'])."',
                                            currency_rate           = '".$in['currency_rate']."',
                                            currency_type           = '".$in['currency_type']."' 
                                            WHERE order_id 			='".$in['order_id']."' ");

		// update multilanguage order note in note_fields
		$lang_note_id = $in['email_language'];
		$exist_note = $this->db->field("SELECT note_fields_id FROM note_fields WHERE active='1' AND item_type='order' AND item_name='notes' AND lang_id='".$in['email_language']."' AND item_id='".$in['order_id']."'");
		if($exist_note){
			//Update other entries as inactive
    		$this->db->query("UPDATE note_fields SET
    										active 					= 	'0'
    								WHERE 	item_type 				= 	'order'
    								AND		item_name 				= 	'notes'
    								AND		item_id 				= 	'".$in['order_id']."'"
    								);

    		$this->db->query("UPDATE note_fields SET
    										active 					= 	'1',
    										item_value				= 	'".$in['notes']."'
    								WHERE 	item_type 				= 	'order'
    								AND		item_name 				= 	'notes'
    								AND		lang_id 				= 	'".$in['email_language']."'
    								AND		item_id 				= 	'".$in['order_id']."'

    		");
    	}else{
    		//Update other entries as inactive
    		$this->db->query("UPDATE note_fields SET
    										active 					= 	'0'
    								WHERE 	item_type 				= 	'order'
    								AND		item_name 				= 	'notes'
    								AND		item_id 				= 	'".$in['order_id']."'"
    								);

    		$this->db->insert("INSERT INTO note_fields SET
    										item_id 				=	'".$in['order_id']."',
    										lang_id 				= 	'".$in['email_language']."',
    										active 					= 	'1',
    										item_type 				= 	'order',
    										item_name 				= 	'notes',
    										item_value				= 	'".$in['notes']."'
    		");
    	}
/*    $langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
    while($langs->next()){
    	foreach ($in['translate_loop'] as $key => $value) {
    		if($value['lang_id'] == $langs->f('lang_id')){
    			$in['nota'] = $value['notes'];
    			break;
    		}
    	}
    	if($lang_note_id == $langs->f('lang_id')){
    		$active_lang_note = 1;
    	}else{
    		$active_lang_note = 0;
    	}
    	if(!$in['order_from_front']){
    		$this->db->query("UPDATE note_fields SET
    										active 					= 	'".$active_lang_note."',
    										item_value				= 	'".$in['notes']."'
    								WHERE 	item_type 				= 	'order'
    								AND		item_name 				= 	'notes'
    								AND		lang_id 				= 	'".$langs->f('lang_id')."'
    								AND		item_id 				= 	'".$in['order_id']."'

    		");
    	}else{
    		$this->db->insert("INSERT INTO note_fields SET
    										item_id 				=	'".$in['order_id']."',
    										lang_id 				= 	'".$langs->f('lang_id')."',
    										active 					= 	'".$active_lang_note."',
    										item_type 				= 	'order',
    										item_name 				= 	'notes',
    										item_value				= 	'".$in['notes']."'
    		");
    	}
    }*/
/*    $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
    while($custom_langs->next()) {
    	foreach ($in['translate_loop_custom'] as $key => $value) {
    		if($value['lang_id'] == $custom_langs->f('lang_id')){
    			$in['nota'] = $value['notes'];
    			break;
    		}
    	}
    	if($lang_note_id == $custom_langs->f('lang_id')) {
    		$active_lang_note_custom = 1;
    	} else {
    		$active_lang_note_custom = 0;
    	}
    	$this->db->query("UPDATE note_fields SET 	active 		=  	'".$active_lang_note_custom."',
    												item_value 	=  	'".$in['notes_'.$custom_langs->f('lang_id')]."'
    										 WHERE  item_type 	=  	'order'
    										 AND 	item_name 	=  	'notes'
    										 AND 	lang_id 	= 	'".$custom_langs->f('lang_id')."'
    										 AND 	item_id 	= 	'".$in['order_id']."'
    	");
    }*/

    $this->db->query("DELETE FROM pim_order_articles WHERE order_id='".$in['order_id']."'");

		$use_package=$this->db->field("SELECT use_package FROM pim_orders WHERE order_id='".$in['order_id']."'");
		$use_sale_unit=$this->db->field("SELECT use_sale_unit FROM pim_orders WHERE order_id='".$in['order_id']."'");

		$global_disc = return_value($in['discount']);
		if($in['apply_discount'] < 2){
			$global_disc = 0;
		}
		$order_total=0;
    $last_combined ='';
    $last_variant_parent ='';
		foreach ($in['tr_id'] as $nr => $row)
		{
	    if(!$use_package){
				$row['packing']=1;
			}
			if(!$use_sale_unit){
				$row['sale_unit']=1;
			}

			$discount_line = return_value($row['disc']);
			if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
				$discount_line = 0;
			}

			$q = return_value2($row['packing']) && $row['sale_unit'] ? @(return_value($row['quantity']) * return_value2($row['packing']) / $row['sale_unit']) : 0;

			$price_line = return_value($row['price']) - (return_value($row['price']) * $discount_line /100);

			$line_total=$price_line * $q;

			$order_total+= $line_total - ($line_total*$global_disc/100);

			$is_from_import = $this->db->field("SELECT from_adveo FROM pim_articles WHERE article_id='".$row['article_id']."'");
      $use_combined = $this->db->field("SELECT use_combined FROM pim_articles WHERE article_id='".$row['article_id']."'");

			if($row['is_tax']!=1){
        if($row['is_combined'] || (!$use_combined && !$row['component_for']) ){
            $last_combined = '';
          }
        if( $row['has_variants'] ){
            $last_variant_parent = '';
          } 
      	$in['order_articles_id'] = $this->db->insert("INSERT INTO pim_order_articles SET
																		 from_import = '".$is_from_import."',
																		 order_id = '".$in['order_id']."',
																		 article_id = '".$row['article_id']."',
																		 quantity = '".return_value($row['quantity'])."',
																		 discount = '".return_value($row['disc'])."',
																		 article = '".htmlspecialchars($row['article'],ENT_QUOTES,'UTF-8')."',
																     purchase_price = '".$row['purchase_price']."',
                                     purchase_price_other_currency='".$row['purchase_price_other_currency']."',
                                     purchase_price_other_currency_id='".$row['purchase_price_other_currency_id']."',
                                     price = '".return_value($row['price'])."',
																		 price_with_vat = '".return_value($row['price_vat'])."',
																		 vat_value   = '".$row['vat_value']."',
																		 vat_percent = '".$row['percent']."',
																		 sort_order  ='".$nr."',
																		 sale_unit = '".$row['sale_unit']."',
																		 packing = '".return_value2($row['packing'])."',
																		 content = '".$row['content']."',
																		 article_code = '".addslashes($row['article_code'])."',
                                     has_variants = '".$row['has_variants']."',
                                    has_variants_done ='".$row['has_variants_done']."',
                                    is_variant_for ='".$row['is_variant_for']."',
                                    is_variant_for_line ='".$last_variant_parent."',
                                    variant_type='".$row['variant_type']."',
                                    is_combined ='".$row['is_combined']."',                                    
                                    component_for ='".$last_combined."',
                                    visible ='".$row['visible']."'
															");
          if($row['is_combined']){
            $last_combined = $in['order_articles_id'];
          }
          if($row['has_variants'] && $row['has_variants_done']){
            $last_variant_parent = $in['order_articles_id'];
          }
        if($row['is_variant_for']){
            $last_variant_parent = '';
          }
			}else{
				$in['order_articles_id'] = $this->db->insert("INSERT INTO pim_order_articles SET
																		 from_import = '".$is_from_import."',
																		 order_id = '".$in['order_id']."',
																		 tax_id = '".$row['article_id']."',
																		 tax_for_article_id = '".$row['tax_for_article_id']."',
																		 quantity = '".return_value($row['quantity'])."',
																		 discount = '".return_value($row['disc'])."',
																		 article  = '".htmlspecialchars($row['article'],ENT_QUOTES,'UTF-8')."',
																		 price    = '".return_value($row['price'])."',
																		 price_with_vat = '".return_value($row['price_vat'])."',
																		 vat_value = '".$row['vat_value']."',
																		 vat_percent = '".$row['percent']."',
																		 sort_order  ='".$nr."',
																		 sale_unit = '".$row['sale_unit']."',
																		 packing = '".return_value2($row['packing'])."',
																		 content = '".$row['content']."',
																		 article_code = '".addslashes($row['article_code'])."',
                                     visible ='1'
															");
			}

		}
		if($in['currency_type'] != ACCOUNT_CURRENCY_TYPE){
			$order_total = $order_total*return_value($in['currency_rate']);
		}
		$this->db->query("UPDATE pim_orders SET amount='".$order_total."' WHERE order_id='".$in['order_id']."'");

    $trace_id=$this->db->field("SELECT trace_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
		if($in['deal_id']){
			//$trace_id=$this->db->field("SELECT trace_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
			if($trace_id){
				$linked_doc=$this->db->field("SELECT tracking_line.line_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
						WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$trace_id."' ");
				if($linked_doc){
					$this->db->query("UPDATE tracking_line SET origin_id='".$in['deal_id']."' WHERE line_id='".$linked_doc."' ");	
				}else{
					$this->db->query("INSERT INTO tracking_line SET
							origin_id='".$in['deal_id']."',
							origin_type='11',
							trace_id='".$trace_id."' ");
				}
			}else{
				$tracking_data=array(
			            'target_id'         => $in['order_id'],
			            'target_type'       => '4',
			            'target_buyer_id'   => $in['buyer_id'],
			            'lines'             => array(
			                                          array('origin_id'=>$in['deal_id'],'origin_type'=>'11')
			                                    )
			          );
			      addTracking($tracking_data);
			}
		}else{
			//$trace_id=$this->db->field("SELECT trace_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
			if($trace_id){
				$linked_doc=$this->db->field("SELECT tracking_line.line_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
						WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$trace_id."' ");
				if($linked_doc){
					$this->db->query("DELETE FROM tracking_line WHERE line_id='".$linked_doc."' ");	
				}
			}
		}
    if($trace_id){
      $this->db->query("UPDATE tracking SET origin_buyer_id='".$in['buyer_id']."',target_buyer_id='".$in['buyer_id']."' WHERE trace_id='".$trace_id."' ");
    }

		msg::success ( gm("Order successfully updated"),'success');
		// ark::$controller = 'order';
		// ark::run('order-order');
		insert_message_log($this->pag,'{l}Order successfully updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;
		$in['add_problem'] = $this->pag;
    $inv = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
      $params = array();
      if($inv->f('pdf_layout')){
        $params['logo'] = $inv->f('pdf_logo');
        $params['type']=$inv->f('pdf_layout');
      }else{
        $params['type']=ACCOUNT_ORDER_PDF_FORMAT;
      }
      #if we are using a customer pdf template
      if(defined('USE_CUSTOME_ORDER_PDF') && USE_CUSTOME_ORDER_PDF == 1 && $inv->f('pdf_layout') == 0){
        $params['custom_type']=ACCOUNT_ORDER_PDF_FORMAT;
        unset($params['type']);
      }
      $params['order_id'] = $in['order_id'];
      $params['lid'] = $inv->f('email_language');
      $params['save_as'] = 'F';
      $this->generate_pdf($params);
		// json_out($in);
		return true;
	}

	/**
	 * order update validate
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function update_order_validate(&$in)
	{

		$v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', gm('Invalid ID'));
		if(!$v->run()){
			return false;
		} else {
			return $this->add_order_validate($in);
		}
	}

	/**
	 * order update validate
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function get_article_quantity_price(&$in)
	{
	    if($in['customer_id']){
	      $customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$in['article_id']."' AND customer_id='".$in['customer_id']."'");
	      if($customer_custom_article_price->move_next()){
	        $price = $customer_custom_article_price->f('price');
	        if($in['asString']){
	        	return $price;
	        }
	        $in['new_price']=$price;
	        json_out($in);
	        return true;
	      }
	    }
		$price_type=$this->db->field("SELECT price_type FROM pim_articles WHERE article_id='".$in['article_id']."'");
    	$is_block_discount=$this->db->query("SELECT pim_articles.block_discount FROM pim_articles
                               WHERE pim_articles.article_id='".$in['article_id']."' AND pim_articles.block_discount='1'");

	    if($is_block_discount->next()){
		  	$price=$in['price'];
		}else{
			if($price_type==1){
		   		$price=$in['price'];
		    }else{
		    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices
		    		                     WHERE pim_article_prices.from_q<='".$in['quantity']."'
		    		                     AND  pim_article_prices.article_id='".$in['article_id']."'
		                                 ORDER BY pim_article_prices.from_q DESC
		                                 LIMIT 1
		    		                     ");

			    if(!$price){
			     	$price=$in['price'];
			    }
		    }
		}
		if($in['asString']){
        	return $price;
        }
	  	$in['new_price']=$price;
    	json_out($in);
		return true;
	}

	/**
	 * order update validate
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function update_purchase_price(&$in) {


    $this->db->query("UPDATE pim_order_articles SET purchase_price='".return_value($in['purchase_price'])."',purchase_price_other_currency='".return_value($in['purchase_price_other_currency'])."',purchase_price_other_currency_id='".$in['purchase_price_other_currency_id']."' WHERE order_articles_id='".$in['order_articles_id']."' ");

    msg::success("Price settings has been successfully updated",'success');
    return true;
  }
  /**
	 * Archive a order
	 *
	 * @param array $in
	 * @return bool
	 * @author PM
	 */
	function archive_order(&$in){
		if(!$this->delete_order_validate($in)){
			return false;
		}
		Sync::start(ark::$model.'-'.ark::$method);

		$this->db->query("UPDATE pim_orders SET active='0', serial_number='' WHERE order_id='".$in['order_id']."' ");

		msg::success ( gm("Order has been archived"),'success');
		insert_message_log($this->pag,'{l}Order has been archived by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;
		$tracking_trace=$this->db->field("SELECT trace_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
	    	if($tracking_trace){
	      	$this->db->query("UPDATE tracking SET archived='1' WHERE trace_id='".$tracking_trace."' ");
	    	}
		Sync::endSend(WEB_URL, HOME_API_KEY);
		return true;
	}
		function delete_order(&$in)
	{
		if(!$this->delete_order_validate($in)){
			return false;
		}
		$tracking_trace=$this->db->field("SELECT trace_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
		if(ALLOW_STOCK){
		Sync::start(ark::$model.'-'.ark::$method);
		$delivery = $this->db->query("SELECT order_articles_id, quantity
			                      FROM pim_orders_delivery
			                      WHERE order_id='".$in['order_id']."'");


		while ($delivery->next())
		{
			// $article_id =  $this->db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id='".$delivery->f('order_articles_id')."'");
			$article_data = $this->db->query("SELECT article_id, article FROM pim_order_articles WHERE order_articles_id='".$delivery->f('order_articles_id')."'");
			$article_data->next();
			$article_id = $article_data->f("article_id");
			// $stock = $this->db->field("SELECT stock FROM pim_articles WHERE article_id=".$article_id);
			$stock_and_item_code = $this->db->query("SELECT stock, item_code, packing, sale_unit FROM pim_articles WHERE article_id=".$article_id);
			$item_code = $stock_and_item_code->f("item_code");
			$stock = $stock_and_item_code->f("stock");
			$new_stock = $stock + ($delivery->f('quantity')*$stock_and_item_code->f("packing")/$stock_and_item_code->f("sale_unit"));
			$this->db->query("UPDATE pim_articles SET stock=".$new_stock." WHERE article_id=".$article_id);

		}
		Sync::end($in['order_id']);
       }


		$this->db->query("DELETE FROM pim_order_deliveries WHERE order_id='".$in['order_id']."' ");
		$this->db->query("DELETE FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' ");
		$this->db->query("DELETE FROM pim_orders WHERE order_id='".$in['order_id']."' ");
		$this->db->query("DELETE FROM pim_order_articles  WHERE order_id='".$in['order_id']."' ");
		$this->db->query("DELETE FROM pim_order_articles  WHERE order_id='".$in['order_id']."' ");
		$this->db->query("DELETE FROM note_fields WHERE item_type = 'order' AND item_name = 'notes' AND item_id = '".$in['order_id']."'");
        $this->db->query("DELETE FROM dispatch_stock_movements  WHERE order_id='".$in['order_id']."' ");


		msg::success (gm("Order has been deleted"),'success');
		insert_message_log($this->pag,'{l}Order deleted by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;
		if($tracking_trace){
	      	$this->db->query("DELETE FROM tracking WHERE trace_id='".$tracking_trace."' ");
	      	$this->db->query("DELETE FROM tracking_line WHERE trace_id='".$tracking_trace."' ");
	    	}
		return true;
	}

	/**
	 * Archive a order
	 *
	 * @param array $in
	 * @return bool
	 * @author PM
	 */
	function activate_order(&$in){
		if(!$this->delete_order_validate($in)){
			return false;
		}
		$serial_number = generate_order_serial_number(DATABASE_NAME);

		$this->db->query("UPDATE pim_orders SET active='1' , serial_number='".$serial_number."' WHERE order_id='".$in['order_id']."' ");

		msg::success ( gm("Order has been activated"),'success');
		insert_message_log($this->pag,'{l}Order has been activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
		$in['pagl'] = $this->pag;
		$tracking_trace=$this->db->field("SELECT trace_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
        	if($tracking_trace){
          		$this->db->query("UPDATE tracking SET archived='0' WHERE trace_id='".$tracking_trace."' ");
        	}
    $inv = $this->dbu->query("SELECT * FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
      $params = array();
      if($inv->f('pdf_layout')){
        $params['logo'] = $inv->f('pdf_logo');
        $params['type']=$inv->f('pdf_layout');
      }else{
        $params['type']=ACCOUNT_ORDER_PDF_FORMAT;
      }
      #if we are using a customer pdf template
      if(defined('USE_CUSTOME_ORDER_PDF') && USE_CUSTOME_ORDER_PDF == 1 && $inv->f('pdf_layout') == 0){
        $params['custom_type']=ACCOUNT_ORDER_PDF_FORMAT;
        unset($params['type']);
      }
      $params['order_id'] = $in['order_id'];
      $params['lid'] = $inv->f('email_language');
      $params['save_as'] = 'F';
      $this->generate_pdf($params);
		return true;
	}

	function check_vies_vat_number(&$in){
		$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

		if(!$in['value'] || $in['value']==''){			
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		$value=trim($in['value']," ");
		$value=str_replace(" ","",$value);
		$value=str_replace(".","",$value);
		$value=strtoupper($value);

		$vat_numeric=is_numeric(substr($value,0,2));

		/*if($vat_numeric || substr($value,0,2)=="BE"){
			$trends_access_token=$this->db->field("SELECT api FROM apps WHERE type='trends_access_token' ");
			$trends_expiration=$this->db->field("SELECT api FROM apps WHERE type='trends_expiration' ");
			$trends_refresh_token=$this->db->field("SELECT api FROM apps WHERE type='trends_refresh_token' ");

			if(!$trends_access_token || $trends_expiration<time()){
				$ch = curl_init();
				$headers=array(
					"Content-Type: x-www-form-urlencoded",
					"Authorization: Basic ". base64_encode("801df338bf4b46a6af4f2c850419c098:B4BDDEF6F33C4CA2B0F484")
					);
				/*$trends_data=!$trends_access_token ? "grant_type=password&username=akti_api&password=akti_api" : "grant_type=refresh_token&&refresh_token=".$trends_refresh_token;* /
				$trends_data="grant_type=password&username=akti_api&password=akti_api";

			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    curl_setopt($ch, CURLOPT_POST, true);
		       	curl_setopt($ch, CURLOPT_POSTFIELDS, $trends_data);
			    curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/oauth2/token');

			    $put = json_decode(curl_exec($ch));
			 	$info = curl_getinfo($ch);

			 	/*console::log($put);
			 	console::log('aaaaa');
			 	console::log($info);* /

			 	if($info['http_code']==400){
			 		if(ark::$method == 'check_vies_vat_number'){
				 		msg::error ( $put->error,'error');
				 		json_out($in);
				 	}
					return false;
			 	}else{
			 		if(!$trends_access_token){
			 			$this->db->query("INSERT INTO apps SET api='".$put->access_token."', type='trends_access_token' ");
					 	$this->db->query("INSERT INTO apps SET api='".(time()+1800)."', type='trends_expiration' ");
					 	$this->db->query("INSERT INTO apps SET api='".$put->refresh_token."', type='trends_refresh_token' ");
			 		}else{
			 			$this->db->query("UPDATE apps SET api='".$put->access_token."' WHERE type='trends_access_token' ");
			 			$this->db->query("UPDATE apps SET api='".(time()+1800)."' WHERE type='trends_expiration' ");
			 			$this->db->query("UPDATE apps SET api='".$put->refresh_token."' WHERE type='trends_refresh_token' ");
			 		}

			 		$ch = curl_init();
					$headers=array(
						"Authorization: Bearer ".$put->access_token
						);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				    if($vat_numeric){
				      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
				    }else{
				      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
				    }
				    $put = json_decode(curl_exec($ch));
				 	$info = curl_getinfo($ch);

				 	/*console::log($put);
				 	console::log('bbbbb');
				 	console::log($info);* /

				    if($info['http_code']==400 || $info['http_code']==429){
				    	if(ark::$method == 'check_vies_vat_number'){
					 		msg::error ( $put->error,'error');
					 		json_out($in);
					 	}
						return false;
				 	}else if($info['http_code']==404){
				 		if($vat_numeric){
				 			if(ark::$method == 'check_vies_vat_number'){
					 			msg::error ( gm("Not a valid vat number"),'error');
					 			json_out($in);
					 		}
							return false;
				 		}
				 	}else{
				 		if($in['customer_id']){
				 			$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				 			if($country_id != 26){
				 				$this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
				 				$in['remove_v']=1;
				 			}else if($country_id == 26){
					 			$this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
					 			$in['add_v']=1;
					 		}
				 		}
				 		$in['comp_name']=$put->officialName;
				 		$in['comp_address']=$put->street.' '.$put->houseNumber;
						$in['comp_zip']=$put->postalCode;
						$in['comp_city']=$put->city;
				 		$in['comp_country']='26';
				 		$in['trends_ok']=true;
				 		$in['trends_lang']=$_SESSION['l'];
				 		$in['full_details']=$put;
				 		if(ark::$method == 'check_vies_vat_number'){
					 		msg::success(gm('Success'),'success');
					 		json_out($in);
					 	}
				 		return false;
				 	}
			 	}
			}else{
				$ch = curl_init();
				$headers=array(
					"Authorization: Bearer ".$trends_access_token
					);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    if($vat_numeric){
			      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
			    }else{
			      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
			    }
			    $put = json_decode(curl_exec($ch));
			 	$info = curl_getinfo($ch);

			 	/*console::log($put);
				console::log('ccccc');
				console::log($info);* /

			    if($info['http_code']==400 || $info['http_code']==429){
			    	if(ark::$method == 'check_vies_vat_number'){
				 		msg::error ( $put->error,'error');
				 		json_out($in);
				 	}
					return false;
			 	}else if($info['http_code']==404){
			 		if($vat_numeric){
			 			if(ark::$method == 'check_vies_vat_number'){
				 			msg::error (gm("Not a valid vat number"),'error');
				 			json_out($in);
				 		}
						return false;
			 		}
			 	}else{
			 		if($in['customer_id']){
			 			$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				 		if($country_id != 26){
				 			$this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
				 			$in['remove_v']=1;
				 		}else if($country_id == 26){
				 			$this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
				 			$in['add_v']=1;
				 		}
				 	}
			 		$in['comp_name']=$put->officialName;
			 		$in['comp_address']=$put->street.' '.$put->houseNumber;
					$in['comp_zip']=$put->postalCode;
					$in['comp_city']=$put->city;
				 	$in['comp_country']='26';
			 		$in['trends_ok']=true;
			 		$in['trends_lang']=$_SESSION['l'];
			 		$in['full_details']=$put;
			 		if(ark::$method == 'check_vies_vat_number'){
				 		msg::success(gm('Success'),'success');
				 		json_out($in);
				 	}
			 		return false;
			 	}
			}
		}*/


		if(!in_array(substr($value,0,2), $eu_countries)){
			$value='BE'.$value;
		}
		if(in_array(substr($value,0,2), $eu_countries)){
			$search   = array(" ", ".");
			$vat = str_replace($search, "", $value);
			$_GET['a'] = substr($vat,0,2);
			$_GET['b'] = substr($vat,2);
			$_GET['c'] = '1';
			$dd = include('valid_vat.php');
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		if(isset($response) && $response == 'invalid'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'error'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'valid'){
			$full_address=explode("\n",$result->address);
			switch($result->countryCode){
				case "RO":
					$in['comp_address']=$full_address[1];
					$in['comp_city']=$full_address[0];
					$in['comp_zip']=" ";
					break;
				case "NL":
					$zip=explode(" ",$full_address[2],2);
					$in['comp_address']=$full_address[1];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
				default:
					$zip=explode(" ",$full_address[1],2);
					$in['comp_address']=$full_address[0];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
			}

			$in['comp_name']=$result->name;

			$in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
			$in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
			$in['full_details']=$result;
			$in['vies_ok']=1;
			if($in['customer_id']){
				$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				if($in['comp_country'] != $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE regime_type='2' ");
				 	$this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['remove_v']=1;
				}else if($in['comp_country'] == $country_id){
					$vat_reg=$this->db->field("SELECT id FROM vat_new WHERE `default` ='1' ");
					$this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='".$vat_reg."' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['add_v']=1;
				}
			}
			if(ark::$method == 'check_vies_vat_number'){
				msg::success ( gm('VAT Number is valid'),'success');
				json_out($in);
			}
			return true;
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		if(ark::$method == 'check_vies_vat_number'){
			json_out($in);
		}
	}

	function tryAddC(&$in){
		console::benchmark('tryAddCValid');
		if(!$this->tryAddCValid($in)){ 
			console::end('tryAddCValid');
			json_out($in);
			return false; 
		}

		if (!$in['order_id']) {
            $in['order_id'] = $in['item_id'];
        }

		//$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
		//Set account default vat on customer creation
	    $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");

	    if(empty($vat_regime_id)){
	      $vat_regime_id = 0;
	    }

    $c_types = '';
    $c_type_name = '';
    if($in['c_type']){
      /*foreach ($in['c_type'] as $key) {
        if($key){
          $type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
          $c_type_name .= $type.',';
        }
      }
      $c_types = implode(',', $in['c_type']);
      $c_type_name = rtrim($c_type_name,',');*/
      if(is_array($in['c_type'])){
        foreach ($in['c_type'] as $key) {
          if($key){
            $type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
            $c_type_name .= $type.',';
          }
        }
        $c_types = implode(',', $in['c_type']);
        $c_type_name = rtrim($c_type_name,',');
      }else{
        $c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");        
        $c_types = $in['c_type'];
      }
    }


    if($in['user_id']==''){
      $in['user_id']=$_SESSION['u_id'];
    }


    if($in['user_id']){
      /*foreach ($in['user_id'] as $key => $value) {
        $manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
          $acc_manager .= $manager_id.',';
      }     
      $in['acc_manager'] = rtrim($acc_manager,',');
      $acc_manager_ids = implode(',', $in['user_id']);*/
      if(is_array($in['user_id'])){
        foreach ($in['user_id'] as $key => $value) {
          $manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
            $acc_manager .= $manager_id.',';
        }     
        $in['acc_manager'] = rtrim($acc_manager,',');
        $acc_manager_ids = implode(',', $in['user_id']);
      }else{
        //$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
        $in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id = :user_id AND database_name = :d ",['user_id'=>$in['user_id'],'d'=>DATABASE_NAME]);
        $acc_manager_ids = $in['user_id'];
      }
    }else{
      $acc_manager_ids = '';
      $in['acc_manager'] = '';
    }
    $vat = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_VAT' ");
          $vat_default=str_replace(',', '.', $vat);
    $selected_vat = $this->db->field("SELECT vat_id FROM vats WHERE value = '".$vat_default."' ");

		if($in['add_customer']){
		  	$payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
       		$payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
       		$account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
          if(SET_DEF_PRICE_CAT == 1){
            $in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
          }
		  	$in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
		                                        name='".addslashes($in['name'])."',
		                                        our_reference = '".$account_reference_number."',
		                                        btw_nr='".$in['btw_nr']."',
		                                        vat_regime_id 		='".$vat_regime_id."',
		                                        city_name = '".$in['city']."',
		                                        user_id = '".$acc_manager_ids."',
                                            acc_manager_name = '".addslashes($in['acc_manager'])."',
		                                        country_name ='".get_country_name($in['country_id'])."',
                                            c_email ='".$in['email']."',
                                            comp_phone ='".$in['phone']."',
		                                        active='1',
		                                        creation_date = '".time()."',
		                                        payment_term 			= '".$payment_term."',
												                    payment_term_type 		= '".$payment_term_type."',
		                                        zip_name  = '".$in['zip']."',
                                            commercial_name     = '".$in['commercial_name']."',
                                            legal_type        = '".$in['legal_type']."',
                                            c_type          = '".$c_types."',
                                            website         = '".$in['website']."',
                                            language        = '".$in['language']."',
                                            sector          = '".$in['sector']."',
                                            comp_fax        = '".$in['comp_fax']."',
                                            activity        = '".$in['activity']."',
                                            comp_size       = '".$in['comp_size']."',
                                            lead_source       = '".$in['lead_source']."',
                                            various1        = '".$in['various1']."',
                                            various2        = '".$in['various2']."',
                                            c_type_name       = '".$c_type_name."',
                                            vat_id          = '".$selected_vat."',
                                            invoice_email_type    = '1',
                                            sales_rep       = '".$in['sales_rep_id']."',
                                            internal_language   = '".$in['internal_language']."',
                                            is_supplier       = '".$in['is_supplier']."',
                                            is_customer       = '".$in['is_customer']."',
                                            stripe_cust_id      = '".$in['stripe_cust_id']."',
                                            cat_id          = '".$in['cat_id']."'");
		  
		  	$this->db->query("INSERT INTO customer_addresses SET
		                    address='".$in['address']."',
		                    zip='".$in['zip']."',
		                    city='".$in['city']."',
		                    country_id='".$in['country_id']."',
		                    customer_id='".$in['buyer_id']."',
		                    is_primary='1',
		                    delivery='1',
		                    billing='1',
		                    site='1' ");
		  	$in['customer_name'] = $in['name'];

        if($in['extra']){
          foreach ($in['extra'] as $key => $value) {
            $this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
            if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
              $this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
            }
          }
        }

		  // include_once('../apps/company/admin/model/customer.php');
		  	$in['value']=$in['btw_nr'];
		  // $comp=new customer();       
		  // $this->check_vies_vat_number($in);
		  	if($this->check_vies_vat_number($in) != false){
		    	$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
		  	}else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
		    	$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
		  	}

		  	/*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
		                            AND name    = 'company-customers_show_info' ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
		                            AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);		                            
		  	if(!$show_info->move_next()) {
		    	/*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
		                        name    = 'company-customers_show_info',
		                        value   = '1' ");*/
				$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
		                        name    = :name,
		                        value   = :value ",
		                    ['user_id' => $_SESSION['u_id'],
		                     'name'    => 'company-customers_show_info',
		                     'value'   => '1']
		                );		                        
		  	} else {
		    	/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
		                            AND name    = 'company-customers_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
		                            AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);		                            
		 	}
		  	$count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
		  	if($count == 1){
		    	doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
		  	}
		  	if($in['quote_id'] && is_numeric($in['quote_id'])){
		  		$address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
		  		$this->db->query("UPDATE tblquote SET 
		  			customer_id='".$in['buyer_id']."', 
		  			customer_name='".addslashes($in['name'])."',
		  			contact_id='',
		  			contact_name='',
		  			field='customer_id',
		  			delivery_address='', 
		  			delivery_address_id='', 
		  			address_info = '".$address."'
		  			WHERE id='".$in['quote_id']."' ");		  		
		  	}
		  	insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
		  	msg::success (gm('Success'),'success');
		  	return true;
		}
		if($in['add_individual']){
	        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
	        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
	        $account_reference_number = $in['our_reference'] ? $in['our_reference'] : (defined('USE_COMPANY_NUMBER') && USE_COMPANY_NUMBER == 1 ? generate_customer_number(DATABASE_NAME) : '');
          if(SET_DEF_PRICE_CAT == 1){
            $in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
          }
	        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
	                                            name='".addslashes($in['lastname'])."',
	                                            our_reference = '".$account_reference_number."',
	                                            firstname = '".addslashes($in['firstname'])."',
	                                            user_id = '".$acc_manager_ids."',
                                              acc_manager_name = '".addslashes($in['acc_manager'])."',
	                                            btw_nr='".$in['btw_nr']."',
	                                            vat_regime_id 		='".$vat_regime_id."',
	                                            city_name = '".$in['city']."',
	                                            c_email = '".$in['email']."',
                                              comp_phone ='".$in['phone']."',
	                                            type = 1,
	                                            country_name ='".get_country_name($in['country_id'])."',
	                                            active='1',
	                                            creation_date = '".time()."',
	                                            payment_term      = '".$payment_term."',
	                                            payment_term_type     = '".$payment_term_type."',
	                                            zip_name  = '".$in['zip']."',
                                              commercial_name     = '".$in['commercial_name']."',
                                              legal_type        = '".$in['legal_type']."',
                                              c_type          = '".$c_types."',
                                              website         = '".$in['website']."',
                                              language        = '".$in['language']."',
                                              sector          = '".$in['sector']."',
                                              comp_fax        = '".$in['comp_fax']."',
                                              activity        = '".$in['activity']."',
                                              comp_size       = '".$in['comp_size']."',
                                              lead_source       = '".$in['lead_source']."',
                                              various1        = '".$in['various1']."',
                                              various2        = '".$in['various2']."',
                                              c_type_name       = '".$c_type_name."',
                                              vat_id          = '".$selected_vat."',
                                              invoice_email_type    = '1',
                                              sales_rep       = '".$in['sales_rep_id']."',
                                              internal_language   = '".$in['internal_language']."',
                                              is_supplier       = '".$in['is_supplier']."',
                                              is_customer       = '".$in['is_customer']."',
                                              stripe_cust_id      = '".$in['stripe_cust_id']."',
                                              cat_id          = '".$in['cat_id']."'");
	      
	        $in['main_address_id'] = $this->db->insert("INSERT INTO customer_addresses SET
	                        address='".$in['address']."',
	                        zip='".$in['zip']."',
	                        city='".$in['city']."',
	                        country_id='".$in['country_id']."',
	                        customer_id='".$in['buyer_id']."',
	                        is_primary='1',
	                        delivery='1',
	                        billing='1',
	                        site='1' ");
	        $in['customer_name'] = $in['name'];

          if($in['extra']){
            foreach ($in['extra'] as $key => $value) {
              $this->db->query("INSERT INTO customer_field SET customer_id='".$in['buyer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
              if(DATABASE_NAME =='04ed5b47_e424_5dd4_ad024bcf2e1d' && $key=='1' && $in['buyer_id']){
                $this->db->query("UPDATE customers SET stripe_cust_id='".$value."' WHERE customer_id='".$in['buyer_id']."'");
              }
            }
          }

	      // include_once('../apps/company/admin/model/customer.php');
	        $in['value']=$in['btw_nr'];
	      // $comp=new customer();       
	      // $this->check_vies_vat_number($in);
	        if($this->check_vies_vat_number($in) != false){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
	        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
	        }

	        /*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
	                                AND name    = :name ",['user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);	                                
	        if(!$show_info->move_next()) {
	          /*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
	                            name    = 'company-customers_show_info',
	                            value   = '1' ");*/
	            $this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
	                            name    = :name,
	                            value   = :value ",
	                        ['user_id' => $_SESSION['u_id'],
	                         'name'    => 'company-customers_show_info',
	                         'value'   => '1']
	                    );
	        } else {
	          /*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
	                                AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-customers_show_info']);	                                
	      }
	        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
	        if($count == 1){
	          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
	        }
	        if($in['quote_id'] && is_numeric($in['quote_id'])){
		  		$address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
		  		$this->db->query("UPDATE tblquote SET 
		  			customer_id='".$in['buyer_id']."', 
		  			customer_name='".addslashes($in['name'])."',
		  			contact_id='',
		  			contact_name='',
		  			field='customer_id',
		  			delivery_address='', 
		  			delivery_address_id='', 
		  			address_info = '".$address."'
		  			WHERE id='".$in['quote_id']."' ");		  		
		  	}
	        insert_message_log('customer','{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['buyer_id']);
	        msg::success (gm('Success'),'success');
	        return true;
	    }
		if($in['add_contact']){
			$customer_name='';
			if($in['buyer_id']){
				$customer_name = addslashes($this->db->field("SELECT name FROM customers WHERE customer_id ='".$in['buyer_id']."' "));
			}
      if($in['birthdate']){
        $in['birthdate'] = strtotime($in['birthdate']);
      }
		  	$in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
            customer_id = '".$in['buyer_id']."',
            firstname = '".$in['firstname']."',
            lastname  = '".$in['lastname']."',
            email   = '".$in['email']."',
            birthdate = '".$in['birthdate']."',
            cell    = '".$in['cell']."',
            sex     = '".$in['sex']."',
            title   = '".$in['title_contact_id']."',
            language  = '".$in['language']."',
            company_name= '".$customer_name."',
            `create`  = '".time()."'");
		  	$this->db->query("DELETE FROM contact_field WHERE customer_id='".$in['contact_id']."' ");
        if($in['extra']){
          foreach ($in['extra'] as $key => $value) {
            $this->db->query("INSERT INTO contact_field SET customer_id='".$in['contact_id']."', value='".addslashes($value)."', field_id='".$key."' ");
          }
        }

          if($in['buyer_id']){
          $contact_accounts_sql="";
          $accepted_keys=["position","department","title","e_title","email","phone","fax","s_email","customer_address_id"];
          foreach($in['accountRelatedObj'] as $key=>$value){
            if(in_array($value['model'],$accepted_keys)){
              $contact_accounts_sql.="`".$value['model']."`='".addslashes($value['model_value'])."',";
            }
          }
          if($in['email']){
            $contact_accounts_sql.="`email`='".$in['email']."',";
          }
          if(!empty($contact_accounts_sql)){
            $contact_accounts_sql=rtrim($contact_accounts_sql,",");
            $this->db->query("INSERT INTO customer_contactsIds SET 
                          customer_id='".$in['buyer_id']."', 
                          contact_id='".$in['contact_id']."',
                          ".$contact_accounts_sql." ");
          } 
        }
		  	$in['contact_name']=$in['firstname'].' '.$in['lastname'].' >';
		  	if(defined('ZENDESK_ACTIVE') && ZENDESK_ACTIVE==1 && defined('ZEN_SYNC_NEW_C') && ZEN_SYNC_NEW_C==1){
	                $vars=array();
	                if($in['buyer_id']){
	                    $vars['customer_id']=$in['buyer_id'];
	                    $vars['customer_name']=$customer_name;
	                }          
	                $vars['contact_id']=$in['contact_id'];
	                $vars['firstname']=$in['firstname'];
	                $vars['lastname']=$in['lastname'];
	                $vars['table']='customer_contacts';
	                $vars['email']=$in['email'];
	                $vars['op']='add';
	                synctoZendesk($vars);
	            }
		  	if($in['country_id']){
		    	$this->db->query("INSERT INTO customer_contact_address SET
		                      address='".$in['address']."',
		                      zip='".$in['zip']."',
		                      city='".$in['city']."',
		                      country_id='".$in['country_id']."',
		                      contact_id='".$in['contact_id']."',
		                      is_primary='1',
		                      delivery='1' ");
		  	}
		  	/*$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
		                                AND name    = 'company-contacts_show_info'  ");*/
			$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = :user_id
		                                AND name    = :name  ",['user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);		                                
		  	if(!$show_info->move_next()) {
		    	/*$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
		                              name    = 'company-contacts_show_info',
		                              value   = '1' ");*/
				$this->db_users->insert("INSERT INTO user_meta SET  user_id = :user_id,
		                              name    = :name,
		                              value   = :value ",
		                            ['user_id' => $_SESSION['u_id'],
		                             'name'    => 'company-contacts_show_info',
		                             'value'   => '1']
		                        );		                              
		  	} else {
		    	/*$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
		                                  AND name    = 'company-contacts_show_info' ");*/
				$this->db_users->query("UPDATE user_meta set value = :value WHERE user_id  = :user_id
		                                  AND name    = :name ",['value'=>'1','user_id'=>$_SESSION['u_id'],'name'=>'company-contacts_show_info']);		                                  
		  	}
		  	$count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
		  	if($count == 1){
		    	doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
		  	}
		  	if($in['quote_id'] && is_numeric($in['quote_id'])){
		  		$this->db->query("UPDATE tblquote SET contact_id='".$in['contact_id']."', contact_name='".($in['firstname'].' '.$in['lastname'])."' WHERE id='".$in['quote_id']."' ");	
		  	}
		  	insert_message_log('xcustomer_contact','{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
		  	msg::success (gm('Success'),'success');
		}
		return true;
	}

	/**
	* undocumented function
	*
	* @return void
	* @author PM
	**/
	function tryAddCValid(&$in)
	{
		if($in['add_customer']){
		  	$v = new validation($in);
			$v->field('name', 'name', 'required:unique[customers.name]');
			$v->field('country_id', 'country_id', 'required');
			return $v->run();  
		}
		if($in['add_contact']){
			$v = new validation($in);
			$v->field('firstname', 'firstname', 'required');
			$v->field('lastname', 'lastname', 'required');
/*			$v->field('email', 'Email', 'required:email:unique[customer_contacts.email]');*/
/*			$v->field('country_id', 'country_id', 'required');*/
			return $v->run();  
		}
		return true;
	}

	

	/**
	 * undocumented function
	 *
	 * @return boolean
	 * @author PM
	 **/
	function updateCustomerData(&$in)


	{
		if($in['field'] == 'contact_id'){
			$in['buyer_id'] ='';
		}
    if($in['order_id'] && is_numeric($in['order_id'])){
        $buyer_exist = $this->db->field("SELECT customer_id FROM pim_orders WHERE order_id='".$in['order_id']."'");
        if($buyer_exist!=$in['buyer_id']){
          $in['save_final']=1;
        }elseif($buyer_exist==$in['buyer_id']){
          $in['save_final']=0;
        }
    }elseif($in['duplicate_order_id'] && is_numeric($in['duplicate_order_id'])){
      $buyer_exist = $this->db->field("SELECT customer_id FROM pim_orders WHERE order_id='".$in['duplicate_order_id']."'");
        if($buyer_exist!=$in['buyer_id']){
          $in['save_final']=1;
        }elseif($buyer_exist==$in['buyer_id']){
          $in['save_final']=0;
        }
    }
		$sql = "UPDATE pim_orders SET ";
		if($in['buyer_id']){
      if($in['main_address_id']){
        $is_customer_address=$this->db->field("SELECT address_id FROM customer_addresses WHERE address_id='".$in['main_address_id']."' AND customer_id='".$in['buyer_id']."' ");
        if(!$is_customer_address){
          unset($in['main_address_id']);
        }
      }

			if($in['main_address_id']){
				$filter = " AND customer_addresses.address_id='".$in['main_address_id']."' ";
			}else{
				$filter = " AND customer_addresses.is_primary=1 ";
			}
			$buyer_info = $this->db->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.name AS company_name, customers.no_vat, customers.btw_nr, 
									customers.internal_language, customers.line_discount, customers.apply_fix_disc, customers.currency_id, customers.apply_line_disc, customers.identity_id, customer_addresses.address_id,
									customer_addresses.address, customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount, customers.cat_id, customers.vat_regime_id,customer_legal_type.name as l_name
									FROM customers
									LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id              
									$filter
                  LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
									WHERE customers.customer_id='".$in['buyer_id']."' ");
			$buyer_info->next();
			//if(!$in['delivery_address']){
				$delivery_addr = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND delivery=1 LIMIT 1 ");
				if($delivery_addr){
					$in['delivery_address'] = $delivery_addr->f('address')."\n".$delivery_addr->f('zip').'  '.$delivery_addr->f('city')."\n".get_country_name($delivery_addr->f('country_id'));
					$in['delivery_address_id']=$delivery_addr->f('address_id');
				}else{
          $in['delivery_address_id']=$buyer_info->f('address_id');
        }
			//}
      $in['vat_regime_id']=$buyer_info->f('vat_regime_id');
			if($in['sameAddress']==1){
		        $in['delivery_address']='';
		        $in['delivery_address_id']='';
		      }

			$apply_discount =ORDER_APPLY_DISCOUNT;
      if(!$in['main_address_id']){
        $in['main_address_id'] = $buyer_info->f('address_id');
      }
			/*if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
				$apply_discount = 3;
			}else{
				if($buyer_info->f('apply_fix_disc')){
					$apply_discount = 2;
				}
				if($buyer_info->f('apply_line_disc')){
					$apply_discount = 1;
				}
			}*/
			/*if(!$buyer_info->f('apply_line_disc')){
					if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
						$apply_discount = 3;
					}else{
						if($buyer_info->f('apply_fix_disc')){
							$apply_discount = 2;
						}
						if($buyer_info->f('apply_line_disc')){
							$apply_discount = 1;
						}
					}
			} else if($buyer_info->f('apply_line_disc')){
				$apply_discount = 1;
			}

			if($buyer_info->f('apply_fix_disc')){
				$apply_fix_disc = 1;
			}*/
			if(!$buyer_info->f('apply_line_disc')){
					if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
						$apply_discount = 3;
					}else{
						if($buyer_info->f('apply_fix_disc')){
							$apply_discount = 2;
						}
					}
			} else {
				$apply_discount = 1;
				if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
					$apply_discount = 3;
				}else{
					if($buyer_info->f('apply_fix_disc')){
						$apply_discount = 2;
					}
				}
			}

      if($in['buyer_changed_save']){
        unset($in['buyer_changed_save']);
      }

      if($in['buyer_changed']){
        unset($in['buyer_changed']);
        $in['buyer_changed_save']=true;
      }

      if($in['save_final']==1){
        $in['buyer_changed']=true;
      }
			
			$in['apply_discount'] = $apply_discount;
			$in['apply_fix_disc'] = $apply_fix_disc;
      $in['discount']=$buyer_info->f('fixed_discount');
      $in['discount_line_gen']=$buyer_info->f('line_discount');
		    
			if(ACCOUNT_ORDER_REF && $buyer_info->f('our_reference')){
				$ref = $buyer_info->f('our_reference').ACCOUNT_ORDER_DEL;
			}
			
			$in['currency_rate']=0;
			$in['currency_id']	= $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
			if($in['currency_id'] != ACCOUNT_CURRENCY_TYPE){
				$in['currency_rate'] = $this->currencyRate(currency::get_currency($in['currency_id'],'code'));
			}
			$in['contact_name'] ='';
			if($in['order_id'] == 'tmp'){
        //$in['delivery_address_id'] = $buyer_info->f('address_id');

      }
      		//Set email language as account / contact language
			$emailMessageData = array('buyer_id' 		=> $in['buyer_id'],
		    						  'contact_id' 		=> $in['contact_id'],
		    						  'item_id'			=> $in['order_id'],
		    						  'email_language' 	=> $in['email_language'],
		    						  'table'			=> 'pim_orders',
		    						  'table_label'		=> 'order_id',
		    						  'table_buyer_label' => 'customer_id',
		    						  'table_contact_label' => 'contact_id',
		    						  'param'		 	=> 'update_customer_data');
			$in['email_language'] = get_email_language($emailMessageData);
		    //End Set email language as account / contact language

      $in['identity_id'] = get_user_customer_identity($_SESSION['u_id'],$in['buyer_id']);

			$sql .= " customer_id = '".$in['buyer_id']."', ";
			$sql .= " customer_name = '".addslashes(stripslashes($buyer_info->f('company_name').' '.$buyer_info->f('l_name')))."', ";
			$sql .= " customer_email = '".$buyer_info->f('c_email')."', ";
			$sql .= " customer_vat_number = '".$buyer_info->f('btw_nr')."', ";
			$sql .= " customer_country_id = '".$buyer_info->f('country_id')."', ";
			$sql .= " customer_city = '".addslashes($buyer_info->f('city'))."', ";
			$sql .= " customer_zip = '".$buyer_info->f('zip')."', ";
			$sql .= " customer_phone = '".$buyer_info->f('comp_phone')."', ";
			$sql .= " customer_address = '".addslashes($buyer_info->f('address'))."', ";
			$sql .= " main_address_id = '".$in['main_address_id']."', ";
			$sql .= " email_language = '".$in['email_language']."', ";
			// $sql .= " email_language = '".($buyer_info->f('internal_language') ? $buyer_info->f('internal_language') : DEFAULT_LANG_ID )."', ";
			$sql .= " delivery_address = '".addslashes($in['delivery_address'])."', ";
			$sql .= " delivery_address_id = '".$in['delivery_address_id']."', ";
			//$sql .= " identity_id = '".$buyer_info->f('identity_id')."', ";
      $sql .= " identity_id = '".$in['identity_id']."', ";
			$sql .= " customer_ref = '".addslashes($buyer_info->f('our_reference'))."', ";
			$sql .= " buyer_ref = '".addslashes($ref)."', ";
			$sql .= " currency_type = '".$in['currency_id']."', ";
			$sql .= " currency_rate = '".$in['currency_rate']."', ";
      $sql .= " cat_id = '".$buyer_info->f('cat_id')."', ";
			
			if($in['contact_id']){
				$contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
				$sql .= " contact_name = '".( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
				$sql .= " contact_id = '".$in['contact_id']."', ";
				$in['contact_name'] = $contact_info->f('firstname').' '.$contact_info->f('lastname')."\n";
			}else{
				$sql .= " contact_name = '', ";
				$sql .= " contact_id = '', ";
			}
			

			$in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));

			if($buyer_info->f('address_id') != $in['main_address_id']){
        $buyer_addr = $this->dbu->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
        $in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
      }
               if($in['sameAddress']==1){
        $sql .= " same_address = '0', ";
        $sql .= " address_info = '".addslashes($in['address_info'])."', ";
      }else{
        $new_address=$this->dbu->query("SELECT address,zip,city,country_id FROM customer_addresses WHERE address_id='".$in['delivery_address_id']."' ");
        $new_address->next();
        $new_address_txt = $new_address->f('address')."\n".$new_address->f('zip').'  '.$new_address->f('city')."\n".get_country_name($new_address->f('country_id'));
        $sql .= " same_address = '".$in['delivery_address_id']."', ";
        $sql .= " address_info = '".addslashes($new_address_txt)."', ";      
      }

		
			


			$sql .= " field = 'customer_id' ";


			if($in['changePrices']==1){
				$sql .= ",  apply_discount = '".$in['apply_discount']."' ";
				$sql .= ",  remove_vat = '".$buyer_info->f('no_vat')."' ";
				$sql .= ",  discount_line_gen = '".$buyer_info->f('line_discount')."' ";
				$sql .= ",  discount = '".$buyer_info->f('fixed_discount')."' ";
				foreach ($in['article'] as $key => $value) {
					$params = array(
						'article_id' => $value['article_id'], 
						'price' => $value['price'], 
						'quantity' => $value['quantity'], 
						'customer_id' => $in['buyer_id'],
						'cat_id' => $buyer_info->f('cat_id'),
						'asString' => true
					);
					$in['article'][$key]['price'] = $value['article_id']? display_number($this->getArticlePrice($params)) : $this->getArticlePrice($params) ;
				}
			}
			
		}else{
			if(!$in['contact_id']){
				msg::error ( gm('Please select a company or a contact'),'error');
				return false;
			}
			$contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			$contact_address = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
			$sql .= " customer_id = '".$in['contact_id']."', ";
			$sql .= " customer_name = '".addslashes( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
			$sql .= " contact_name = '', ";
			$sql .= " contact_id = '".$in['contact_id']."', ";
			$sql .= " customer_email = '".$contact_info->f('email')."', ";
			$sql .= " customer_vat_number = '', ";
			$sql .= " customer_country_id = '".$contact_address->f('country_id')."', ";
			$sql .= " customer_city = '".addslashes($contact_address->f('city'))."', ";
			$sql .= " customer_zip = '".$contact_address->f('zip')."', ";
			$sql .= " customer_phone = '".$contact_info->f('phone')."', ";
			$sql .= " customer_address = '".addslashes($contact_address->f('address'))."', ";
			$sql .= " email_language = '".DEFAULT_LANG_ID."', ";
			//$sql .= " delivery_address = '', ";
			//$sql .= " delivery_address_id = '', ";
			$sql .= " identity_id = '".$in['identity_id']."', ";
			$sql .= " customer_ref = '', ";
			$sql .= " buyer_ref = '', ";
			$sql .= " currency_type = '".ACCOUNT_CURRENCY_TYPE."', ";
			$sql .= " currency_rate = '0', ";
			$in['address_info'] = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
			$sql .= " address_info = '".addslashes($in['address_info'])."', ";
			$sql .= " field = 'contact_id' ";
			if($in['changePrices']==1){
				$sql .= ",  apply_discount = '0' ";
				$sql .= ",  remove_vat = '0' ";
				$sql .= ",  discount_line_gen = '0' ";
				$sql .= ",  discount = '0' ";
				foreach ($in['article'] as $key => $value) {
					$params = array(
						'article_id' => $value['article_id'], 
						'price' => return_value($value['price']), 
						'quantity' => return_value($value['quantity']),
						'cat_id' => '0',
						'asString' => true
					);
					$in['article'][$key]['price'] = $value['article_id']? display_number($this->getArticlePrice($params)) : $this->getArticlePrice($params);
				}
			}
		}
		$sql .=" WHERE order_id ='".$in['item_id']."' ";
		if(!$in['isAdd'] && !$in['duplicate_order_id']){
		
			$this->db->query($sql);	
      if($in['item_id'] && is_numeric($in['item_id'])){
        $trace_id=$this->db->field("SELECT trace_id FROM pim_orders WHERE order_id='".$in['item_id']."' ");
        if($trace_id && $in['buyer_id']){
          $this->db->query("UPDATE tracking SET origin_buyer_id='".$in['buyer_id']."',target_buyer_id='".$in['buyer_id']."' WHERE trace_id='".$trace_id."' ");
        }
        if(!$in['changePrices']){
            ark::run('order-norder-order-update_order');
        }
      }

		}
		$in['order_id'] = $in['item_id'];
		msg::$success = gm('Sync successfull.');
		return true;
	}

  function updatePriceCategory(&$in)
  {
    if($in['order_id']){
      $this->db->query("UPDATE pim_orders SET cat_id ='".$in['cat_id']."' WHERE order_id ='".$in['order_id']."'");

      if($in['buyer_id']){
        if($in['changeCatPrices']==1){
         
            foreach ($in['tr_id'] as $key => $value) {
                $params = array(
                  'article_id' => $value['article_id'], 
                  'price' => return_value($value['price']), 
                  'quantity' => return_value($value['quantity']),
                  'customer_id' => $in['buyer_id'],
                  'cat_id' => $in['cat_id'],
                  'asString' => true
                );
                $price = $this->getArticlePrice($params);
                
                if($value['article_id']){
                  $in['tr_id'][$key]['price'] = display_number($price);
                  $in['tr_id'][$key]['price_vat'] = display_number($price+$price*$in['tr_id'][$key]['percent']/100);
                  $in['tr_id'][$key]['vat_value'] = $price*$in['tr_id'][$key]['percent']/100;
                  $in['tr_id'][$key]['vat_value_x'] = display_number($price*$in['tr_id'][$key]['percent']/100);
                }                
                
              }
          


        }
        msg::success (gm("Sync successfull."),'success'); 
        ark::run('order-norder-order-update_order');
      }
    }

    return true;
   }

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function getArticlePrice($in){
    $cat_id = $in['cat_id'];
		$article = $this->db->query("SELECT article_category_id, block_discount,price_type FROM pim_articles WHERE article_id='".$in['article_id']."' ");
		if($article->f('price_type')==1){

		    $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");
	        $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$in['article_id']."' and category_id='".$cat_id."' ");

	       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
	            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$in['article_id']."'");
	        }else{
	       	   	$price_value=$price_value_custom_fam;

	         	 //we have to apply to the base price the category spec
	    	 	$cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
	    	    $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
	    	    $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

	    	    if($cat_price_type==2){
	                $article_base_price=get_article_calc_price($in['article_id'],3);
	            }else{
	                $article_base_price=get_article_calc_price($in['article_id'],1);
	            }

	       		switch ($cat_type) {
					case 1:                  //discount
						if($price_value_type==1){  // %
							$price = $article_base_price - $price_value * $article_base_price / 100;
						}else{ //fix
							$price = $article_base_price - $price_value;
						}
						break;
					case 2:                 //profit margin
						if($price_value_type==1){  // %
							$price = $article_base_price + $price_value * $article_base_price / 100;
						}else{ //fix
							$price =$article_base_price + $price_value;
						}
						break;
				}
	        }

		    if(!$price || $article->f('block_discount')==1 ){
	        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$in['article_id']."' AND base_price=1");
            if(!$price){
              $price=$in['price'];
            }
	        }
	    }else{
	    	$price = $in['price'];
	    	// return $this->get_article_quantity_price($in);
	    }

	    $start= mktime(0, 0, 0);
	    $end= mktime(23, 59, 59);
	    $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$in['article_id']."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
	    if($promo_price->move_next()){
	    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

	        }else{
	            $price=$promo_price->f('price');
	        }
	    }
	 	if($in['customer_id']){
	  		$customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$in['article_id']."' AND customer_id='".$in['customer_id']."'");
	    	if($customer_custom_article_price->move_next()){
	            $price = $customer_custom_article_price->f('price');
	       	}
	   	}
	   	return $price;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function tryAddAddress(&$in){
		if(!$this->CanEdit($in)){
			json_out($in);
			return false;
		}
		if(!$this->tryAddAddressValidate($in)){
			json_out($in);
			return false;
		}

		$country_n = get_country_name($in['country_id']);
		if($in['field']=='customer_id'){
	 		Sync::start(ark::$model.'-'.ark::$method);

			$address_id = $this->db->insert("INSERT INTO customer_addresses SET
																				customer_id			=	'".$in['customer_id']."',
																				country_id			=	'".$in['country_id']."',
																				state_id			=	'".$in['state_id']."',
																				city				=	'".$in['city']."',
																				zip					=	'".$in['zip']."',
																				address				=	'".$in['address']."',
																				billing				=	'".$in['billing']."',
																				is_primary			=	'".$in['primary']."',
																				delivery			=	'".$in['delivery']."'");
			if($in['billing']){
				$this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
			}

			Sync::end($address_id);

			/*if($in['delivery']){
				$this->db->query("UPDATE customer_addresses SET delivery=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
			}*/
			if($in['primary']){
				if($in['address'])
				{
					if(!$in['zip'] && $in['city'])
					{
						$address = $in['address'].', '.$in['city'].', '.$country_n;
					}elseif(!$in['city'] && $in['zip'])
					{
						$address = $in['address'].', '.$in['zip'].', '.$country_n;
					}elseif(!$in['zip'] && !$in['city'])
					{
						$address = $in['address'].', '.$country_n;
					}else
					{
						$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
					}
					$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
					$output= json_decode($geocode);
					$lat = $output->results[0]->geometry->location->lat;
					$long = $output->results[0]->geometry->location->lng;
				}
				$this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
				$this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
				$this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
			}
		}else{			
			$address_id = $this->db->insert("INSERT INTO customer_contact_address SET
																				contact_id			=	'".$in['contact_id']."',
																				country_id			=	'".$in['country_id']."',																				city				=	'".$in['city']."',
																				zip					=	'".$in['zip']."',
																				address				=	'".$in['address']."',
																				is_primary			=	'".$in['primary']."',
																				delivery			=	'".$in['delivery']."'");
		}
		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer address added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['address_id'] = $address_id;
		$in['country'] = $country_n;
		$in['order_id'] = $in['item_id'];
		if($in['order_id'] && is_numeric($in['order_id'])){
			$delivery_address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
			$this->db->query("UPDATE pim_orders SET 
	  			delivery_address='".$delivery_address."', 
	  			address_info='".$delivery_address."',
	  			same_address ='".$address_id."',
	  			delivery_address_id='".$address_id."' WHERE order_id='".$in['order_id']."'  ");	
	  	}elseif ($in['order_id'] == 'tmp') {
	  		$in['delivery_address'] = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
	  		$in['delivery_address_id'] = $address_id;
	  	}
	  	$in['buyer_id'] = $in['customer_id'];
		// $in['pagl'] = $this->pag;
		
		// json_out($in);
		return true;

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function tryAddAddressValidate(&$in)
	{
		$v = new validation($in);

		if($in['customer_id']){
			$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
			if(!$in['primary'] && !$in['delivery'] && !$in['billing'] && !$in['site']){
				$v->field('primary', 'primary', 'required',gm("Main Address is mandatory"));
				$v->field('delivery', 'delivery', 'required',gm("Delivery Address is mandatory"));
				$v->field('billing', 'billing', 'required',gm("Billing Address is mandatory"));
				$v->field('site', 'site', 'required',gm("Site Address is mandatory"));

				$message = 'You must select an address type.';
				$is_ok = $v->run();
				if(!$is_ok){
					msg::error(gm($message),'error');
					return false;		
				}
			} else {
				$v->field('address', 'Address', 'required:text',gm("Street is mandatory"));
				$v->field('zip', 'Zip', 'required:text',gm("Zip Code is mandatory"));
				$v->field('city', 'City', 'required:text',gm("City is mandatory"));
				$v->field('country_id', 'Country', 'required:exist[country.country_id]');
			}
		}else if($in['contact_id']){
			$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
		}else{
			msg::error(gm('Invalid ID'),'error');
			return false;
		}
		$v->field('country_id', 'Country', 'required:exist[country.country_id]',gm("Country is mandatory"));

		return $v->run();
	}

	function CanEdit(&$in){
		if(ONLY_IF_ACC_MANAG ==0 && ONLY_IF_ACC_MANAG2==0){
			return true;
		}
		$c_id = $in['customer_id'];
		if(!$in['customer_id'] && $in['contact_id']) {
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
		}
		if($c_id){
			if($_SESSION['group'] == 'user' && !in_array('company', $_SESSION['admin_sett']) ){
				$u = $this->db->field("SELECT user_id FROM customers WHERE customer_id='".$c_id."' ");
				if($u){
					$u = explode(',', $u);
					if(in_array($_SESSION['u_id'], $u)){
						return true;
					}
					else{
						msg::$warning = gm("You don't have enought privileges");
						return false;
					}
				}else{
					msg::$warning = gm("You don't have enought privileges");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addDispatchNote(&$in){
		if(!$this->addDispatchNoteValid($in)){
			json_out($in);
			return false;
		}
		$in['del_note'] = trim($in['del_note']);
		$this->db->query("UPDATE pim_orders SET del_note='".$in['del_note']."' WHERE order_id='".$in['order_id']."' ");
		msg::success ( gm('Changes saved'),'success');
		$in['del_note_txt'] = nl2br($in['del_note']);
		json_out($in);

	}
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addDispatchNoteValid(&$in)
	{
		$v = new validation($in);
		$v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', gm('Invalid ID'));
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->updateCustomer_validate($in)){
			return false;
		}
        $query_sync=array();

		// $c_type = explode(',', $in['c_type']);
		
		foreach ($in['c_type'] as $key) {
			if($key){
				$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
				$c_type_name .= $type.',';
			}
		}
		$c_types = implode(',', $in['c_type']);
		$c_type_name = rtrim($c_type_name,',');


		if($in['user_id']){
			foreach ($in['user_id'] as $key => $value) {
				$manager_id = $this->db_users->field("SELECT first_name,last_name FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
					$acc_manager .= $manager_id.',';
			}			
			$in['acc_manager'] = rtrim($acc_manager,',');
			$acc_manager_ids = implode(',', $in['user_id']);
		}else{
			$acc_manager_ids = '';
			$in['acc_manager'] = '';
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET name 				= '".$in['name']."',
											serial_number_customer  =   '".$in['serial_number_customer']."',
											commercial_name 		= '".$in['commercial_name']."',
											active					= '".$in['active']."',
											legal_type				= '".$in['legal_type']."',
											user_id					= '".$acc_manager_ids."',
											c_type					= '".$c_types."',
											website					= '".$in['website']."',
											language				= '".$in['language']."',
											c_email					= '".$in['c_email']."',
											sector					= '".$in['sector']."',
											comp_phone				= '".$in['comp_phone']."',
											comp_fax				= '".$in['comp_fax']."',
											activity				= '".$in['activity']."',
											comp_size				= '".$in['comp_size']."',
											lead_source				= '".$in['lead_source']."',
											c_type_name				= '".$c_type_name."',
											various1				= '".$in['various1']."',
											various2				= '".$in['various2']."',
											acc_manager_name		= '".$in['acc_manager']."',
											sales_rep				= '".$in['sales_rep']."',
											is_supplier				= '".$in['is_supplier']."',
											our_reference			= '".$in['our_reference']."'
					  	WHERE customer_id='".$in['customer_id']."' ");

		if($in['customer_id_linked']){
			$link_id = $this->db->insert("INSERT INTO customer_link SET customer_id='".$in['customer_id']."',
														customer_id_linked='".$in['customer_id_linked']."',
														link_type='".$in['link_type']."'");
			if($in['link_type'] == 1){
				$type = 2;
			}
			else{
				$type = 1;
			}
			$this->db->query("INSERT INTO customer_link SET customer_id='".$in['customer_id_linked']."',
														customer_id_linked='".$in['customer_id']."',
														link_type='".$type."',
														link_id='".$link_id."'");
		}
		$this->db->query("UPDATE customer_contacts SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$this->db->query("UPDATE projects SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$legal_type = $this->db->field("SELECT name FROM customer_legal_type WHERE id='".$in['legal_type']."' ");
		$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['name'].' '.$legal_type."'	WHERE buyer_id='".$in['customer_id']."' ");

		
		Sync::end($in['customer_id']);

		set_first_letter('customers',$in['name'],'customer_id',$in['customer_id']);

		//save extra fields values
		$this->update_custom_field($in);

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		// $in['pagl'] = $this->pag;
		self::sendCustomerToZintra($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer_validate(&$in)
	{

		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		if($in['customer_link']){
			$v->field('link_type', 'Type of link', 'required');
		}
		return $this -> addCustomer_validate($in);

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addCustomer_validate(&$in)
	{
		$in['c_email'] = trim($in['c_email']);
		$v = new validation($in);
		$v->field('name', 'Name', 'required');

		if (ark::$method == 'updateCustomer') {
			$our_ref_val=$this->db->field("SELECT value FROM settings WHERE constant_name='USE_COMPANY_NUMBER' ");
		  	if($in['our_reference'] && $our_ref_val){
		 		$v->field('our_reference','Company Nr.',"unique[customers.our_reference.(customer_id!='".$in['customer_id']."')]");
		  	}
	  	}


		if($in['c_email']){
			$v->field('c_email','Email',"email:unique[customers.c_email.(customer_id!='".$in['customer_id']."')]");			
		}
		
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_custom_field(&$in)
	{
		$this->db->query("DELETE FROM customer_field WHERE customer_id='".$in['customer_id']."' ");
		if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO customer_field SET customer_id='".$in['customer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
			}
		}
		// msg::$success = gm("Customer updated");
		if(ark::$model == 'update_custom_field'){
			self::sendCustomerToZintra($in);
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function sendCustomerToZintra(&$in)
	{
		if(!defined("ZINTRA_ACTIVE") || ZINTRA_ACTIVE != 1){
			return true;
		}
		$vars_post = array();
		$data = $this->db->query("SELECT * FROM customers WHERE customer_id='".$in['customer_id']."' ");
		if($data){
			$bar_code_start = null;
			$bar_code_end = null;
			$custom_fields = $this->db->query("SELECT * FROM customer_field WHERE customer_id='".$in['customer_id']."' ");
			while($custom_fields->next()){
				if($custom_fields->f('field_id') == 1){
					$bar_code_start = $custom_fields->f('value');
				}
				if($custom_fields->f('field_id') == 2){
					$bar_code_end = $custom_fields->f('value');
				}
			}
			//asdkauas
			$primary = array();
			$address = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
			if($address) {
				$primary = array(
					"Unformatted"=> null,
			    "Label"=> null,
			    "Building"=> null,
			    "Street"=> $address->f('address'),
			    "PostalCode"=> $address->f('zip'),
			    "City"=> $address->f('city'),
			    "State"=> $address->f('state_id'),
			    "Country"=> get_country_name($address->f('country_id')),
			    "CountryCode"=> get_country_code($address->f('country_id'))
			  );
			}
			$test = array();
			$vars_post = array(
				'Id'											=> (string)$in['customer_id'],
				"Name"										=> $data->f('name'),
			  "VatNumber"								=> $data->f('btw_nr') ? $data->f('btw_nr') : null,
			  "TimeZone"								=> null,
			  "Note"										=> null,
			  "PictureURI"							=> null,
			  "Importance"							=> null,
			  "Blog"										=> null,
			  "WebSite"									=> $data->f('website') ? $data->f('website') : null,
			  "FixedPhoneNumber"				=> $data->f('comp_phone') ? $data->f('comp_phone') : null,
			  "MobilePhoneNumber"				=> $data->f('other_phone') ? $data->f('other_phone') : null,
			  "FaxNumber"								=> $data->f('comp_fax') ? $data->f('comp_fax') : null,
			  "EMailAddress"						=> $data->f('c_email') ? $data->f('c_email') : null,
			  "DeliveryPostalAddress"		=> null,
			  "InvoicePostalAddress"		=> null,
			  "PostalAddress"						=> $primary,
			  "Fields"									=> array(
					array(
					  "Name"								=> "Reference",
					  "Value"								=> $data->f('our_reference')
					),
					array(
					  "Name"								=> "BarcodeFrom",
					  "Value"								=> $bar_code_start
					),
					array(
					  "Name"								=> "BarcodeTo",
					  "Value"								=> $bar_code_end
					)
			  ),
			  "Links"										=> $test,
			  "Categories"							=> null
			);

			$put = $this->c_rest->execRequest('https://app.zintra.eu/datas/Organization/'.$in['customer_id'],'put',$vars_post);
			console::log($put);
			$this->db->query("UPDATE customers SET zintraadded='1' WHERE customer_id='".$in['customer_id']."' ");
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->update_financial_valid($in)){
			return false;
		}
		$apply_fix_disc = 0;
		if($in['apply_fix_disc']){$apply_fix_disc = 1;}
		$apply_line_disc = 0;
		if($in['apply_line_disc']){$apply_line_disc = 1;}
		$check_vat_number = $this->db->field("SELECT check_vat_number FROM customers WHERE customer_id = '".$in['customer_id']."' ");
		if($in['btw_nr']!=$check_vat_number){
			$check_vat_number = '';
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET
											btw_nr								=	'".$in['btw_nr']."',
											bank_name							=	'".$in['bank_name']."',
											payment_term					=	'".$in['payment_term']."',
											payment_term_type 		= '".$in['payment_term_start']."',
											bank_bic_code					=	'".$in['bank_bic_code']."',
											fixed_discount				=	'".return_value($in['fixed_discount'])."',
											bank_iban							=	'".$in['bank_iban']."',
                      						vat_id								=	'".$in['vat_id']."',
											cat_id								=	'".$in['cat_id']."',
											no_vat								=	'".$in['no_vat']."',
											currency_id						= '".$in['currency_id']."',
											invoice_email					=	'".$in['invoice_email']."',
											attention_of_invoice		   =	'".$in['attention_of_invoice']."',
											invoice_note2					=	'".$in['invoice_note2']."',
											deliv_disp_note				=	'".$in['deliv_disp_note']."',
											external_id						=	'".$in['external_id']."',
											internal_language			=	'".$in['internal_language']."',
											apply_fix_disc				= '".$apply_fix_disc."',
											apply_line_disc				= '".$apply_line_disc."',
											line_discount 				= '".return_value($in['line_discount'])."',
											comp_reg_number 				= '".$in['comp_reg_number']."',
											vat_regime_id				= '".$in['vat_regime_id']."',
											siret						= '".$in['siret']."',
											check_vat_number 			= '".$check_vat_number."',
											identity_id                 = '".$in['identity_id']."'
											WHERE customer_id			= '".$in['customer_id']."' ");
		//quote_reference			=	'".$in['quote_reference']."',

		Sync::end($in['customer_id']);
		// msg::$success = gm("Changes have been saved.");
		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		self::sendCustomerToZintra($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial_valid(&$in)
	{
		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
		if($in['invoice_email']){
			$v->field('invoice_email', 'Type of link', 'email');
		}

		return $v->run();
	}

	/**
	 * Update contact data
	 * @param  array $in
	 * @return bool
	 * @author PM
	 */
	function contact_update(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->contact_update_validate($in)){
			return false;
		}
		if(isset($in['birthdate']) && !empty($in['birthdate']) ){
			$in['birthdate'] = strtotime($in['birthdate']);
		}
		$position = implode(',', $in['position']);
		$pos = '';
		foreach ($in['position_n'] as $key => $value) {
			$pos .= $value['name'].',';
		}
		$pos = addslashes(rtrim($pos,','));
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customer_contacts SET
                                            customer_id	=	'".$in['customer_id']."',
                                            firstname	=	'".$in['firstname']."',
                                            lastname	=	'".$in['lastname']."',
                                            position	=	'".$position."',
                                            department	=	'".$in['department']."',
                                            email		=	'".$in['email']."',
                                            birthdate	=	'".$in['birthdate']."',
					    					phone		=	'".$in['phone']."',
										    cell		=	'".$in['cell']."',
										    note		=	'".$in['note']."',
										    sex			=	'".$in['sex']."',
										    language	=	'".$in['language']."',
										    title		=	'".$in['title']."',
										    e_title		=	'".$in['e_title']."',
											fax			=	'".$in['fax']."',
											company_name=	'".$in['customer']."',
											title_name	=	'".$in['title_name']."',
											position_n  = 	'".$pos."',
										    last_update =	'".time()."'
				WHERE
			 	contact_id	=	'".$in['contact_id']."'");
		$this->db->query("UPDATE customer_contacts SET s_email='0' WHERE contact_id='".$in['contact_id']."' ");
		if($in['s_email']){
			$this->db->query("UPDATE customer_contacts SET s_email='1', unsubscribe='' WHERE contact_id='".$in['contact_id']."' ");
		}

		if ($in['password'])
		{
			$this->db->query("UPDATE customer_contacts SET password='".md5($in['password'])."' WHERE contact_id='".$in['contact_id']."'");
		}

		Sync::end($in['contact_id']);

		$this->db->query("SELECT * FROM recurring_invoice WHERE contact_id='".$in['contact_id']."' ");
		while ($this->db->move_next()) {
			$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['firstname'].' '.$in['lastname']."',
																							buyer_email='".$in['email']."',
																							buyer_phone='".$in['phone']."'
												WHERE contact_id='".$in['contact_id']."' ");
		}
		if(!$in['customer_id']){
			$this->removeFromAddress($in);
		}

		$project_company_name = $in['firstname'].' '.$in['lastname'];
		$this->db->query("UPDATE projects SET company_name = '".$project_company_name."' WHERE customer_id = '".$in['contact_id']."' AND is_contact = '1' ");

		set_first_letter('customer_contacts',$in['lastname'],'contact_id',$in['contact_id']);

		//save extra fields values
		$this->update_custom_contact_field($in);

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pagc,'{l}Contact updated by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
		// $in['pagl'] = $this->pagc;
		return true;
	}

	/**
	 * Validate update contact data
	 * @param  array $in
	 * @return [type]     [description]
	 */
	function contact_update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', gm('Invalid ID'));
		$is_ok = $v->run();

		if(!$this->contact_add_validate($in)){
			$is_ok = false;
		}
		if($in['customer_id']){
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			if($c_id){
				if($c_id != $in['customer_id']){
					$this->removeFromAddress($in);
				}
			}
		}

		return $is_ok;
	}

	/**
	 * undocumented function
	 *
	 * @return true
	 * @param array $in
	 * @author PM
	 **/
	function contact_add_validate(&$in)
	{
		$in['email'] = trim($in['email']);
		$v = new validation($in);
		$v->field('firstname', 'First name', 'required');
		$v->field('lastname', 'Last name', 'required');
		if ($in['do']=='company-xcustomer_contact-customer-contact_add')
		{
			$v->field('email', 'Email', 'email:unique[customer_contacts.email]');
		}
		else
		{
			$v->field('email','Email',"email:unique[customer_contacts.email.(contact_id!='".$in['contact_id']."')]");
		}
		return $v->run();

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function removeFromAddress(&$in)
	{
		$this->db->query("DELETE FROM customer_contact_addresses WHERE contact_id='".$in['contact_id']."' ");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_custom_contact_field(&$in)
	{
		$this->db->query("DELETE FROM contact_field WHERE customer_id='".$in['contact_id']."' ");
		if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO contact_field SET customer_id='".$in['contact_id']."', value='".addslashes($value)."', field_id='".$key."' ");
			}
		}
		// msg::$success = gm("Customer updated");
		if(ark::$model == 'update_custom_field'){
			self::sendCustomerToZintra($in);
		}
		return true;
	}

	function currencyRate($currency=''){
		if(empty($currency)){
			$currency = "USD";
		}
		$separator = ACCOUNT_NUMBER_FORMAT;
		$into = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
		return currency::getCurrency($currency, $into, 1,$separator);
	}
	function uploadify(&$in)
	{
	    $response=array();
	    if (!empty($_FILES)) {
	      $tempFile = $_FILES['Filedata']['tmp_name'];
	      // $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
	      $targetPath = 'upload/'.DATABASE_NAME;
	      $in['name'] = 'o_logo_img_'.DATABASE_NAME.'_'.time().'.jpg';
	      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
	      // Validate the file type
	      $fileTypes = array('jpg','jpeg','gif'); // File extensions
	      $fileParts = pathinfo($_FILES['Filedata']['name']);
	      @mkdir(str_replace('//','/',$targetPath), 0775, true);
	      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
	        move_uploaded_file($tempFile,$targetFile);
	        global $database_config;

	        $database_2 = array(
	          'hostname' => $database_config['mysql']['hostname'],
	          'username' => $database_config['mysql']['username'],
	          'password' => $database_config['mysql']['password'],
	          'database' => DATABASE_NAME,
	        );
	        $db_upload = new sqldb($database_2);
	        $logo = $db_upload->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_LOGO_ORDER' ");
	        if($logo == ''){
	          if(defined('ACCOUNT_LOGO')){
	            $db_upload->query("UPDATE settings SET
	                               value = 'upload/".DATABASE_NAME."/".$in['name']."'
	                               WHERE constant_name='ACCOUNT_LOGO_ORDER' ");
	          }else{
	            $db_upload->query("INSERT INTO settings SET
	                               value = 'upload/".DATABASE_NAME."/".$in['name']."',
	                               constant_name='ACCOUNT_LOGO_ORDER' ");
	          }
	        }
	        $response['success'] = 'success';
	        echo json_encode($response);
	        // ob_clean();
	      } else {
	        $response['error'] = gm('Invalid file type.');
	        echo json_encode($response);
	        // echo gm('Invalid file type.');
	      }
	    }
	}
	  function default_pdf_format_header(&$in)
	  {

	/*    if(!$in['type']){
	      msg::$error = gm('Invalid ID');
	      return false;
	    }*/

	    if($in['identity_id']>0){
	        $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='order' ");
	        $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='order' ");

	      }else{

	      $this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_ORDER_PDF' ");
	      $this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_ORDER_HEADER_PDF_FORMAT'");
	      $this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_ORDER_PDF_MODULE'");
	    }
	      global $config;
	      $this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");
	      msg::success ( gm('Changes saved'),'success');
	      // json_out($in);

	    return true;
	  }
	  function default_pdf_format_body(&$in)
	  {

	/*    if(!$in['type']){
	      msg::$error = gm('Invalid ID');
	      return false;
	    }*/

	    if($in['identity_id']>0){
	        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='order' ");
	        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='order' ");

	      }else{

	      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_ORDER_PDF' ");
	      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_ORDER_BODY_PDF_FORMAT'");
	      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_ORDER_PDF_MODULE'");
	      $this->dbu->query("UPDATE settings SET value='".$in['type']."' WHERE constant_name = 'ACCOUNT_ORDER_PDF_FORMAT'");
	    }
	      global $config;
	      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

	      msg::success ( gm('Changes saved'),'success');
	      // json_out($in);

	    return true;
	  }
	  function default_pdf_format_footer(&$in)
	  {

	    if($in['identity_id']>0){
	        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='order' ");
	        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='order' ");
	      }else{
	      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_ORDER_PDF' ");
	      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_ORDER_FOOTER_PDF_FORMAT'");
	      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_ORDER_PDF_MODULE'");
	    }
	      global $config;
	      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

	      msg::success ( gm('Changes saved'),'success');
	      // json_out($in);

	    return true;
	  }
	  function reset_data(&$in){
	    if($in['header'] && $in['header']!='undefined'){
	      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='order' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."'");
	    }elseif($in['footer']  && $in['footer']!='undefined'){
	      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='order' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."'");
	    }
	    $result=array("var_data" => $variable_data);
	    json_out($result);
	    return true;
	  }

	  function pdfSaveData(&$in){

	    if($in['header']=='header'){
	      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='order' AND type='".$in['header']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
	      $exist=$this->dbu->field("SELECT id FROM tblquote_pdf_data WHERE master='order' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");
	      if($exist){
	        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
	      }else{
	        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='order',
	                          type='".$in['header']."',
	                          content='".$in['variable_data']."',
	                          initial='1', 
	                          layout='".$in['layout']."',
	                          `default`='1',
                            identity_id='".$in['identity_id']."' ");
	      }
	    }else{
	      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='order' AND type='".$in['footer']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
	      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='order' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");

	      if($exist){
	        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
	      }else{
	        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='order',
	                          type='".$in['footer']."',
	                          content='".$in['variable_data']."',
	                          initial='1', 
	                          layout='".$in['layout']."',
	                          `default`='1',
                            identity_id='".$in['identity_id']."' ");
	      }
	    }
	    msg::success ( gm('Data saved'),'success');
	    return true;
	  }
	   function default_pdf_format_header_delivery(&$in)
	  {

	/*    if(!$in['type']){
	      msg::$error = gm('Invalid ID');
	      return false;
	    }*/

	    if($in['identity_id']>0){
	        $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='order' ");
	        $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='order' ");

	      }else{

	      $this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_ORDER_DELIVERY_PDF' ");
	      $this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_ORDER_DELIVERY_HEADER_PDF_FORMAT'");
	      $this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_ORDER_DELIVERY_PDF_MODULE'");
	    }
	      global $config;
	      $this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");
	      msg::success ( gm('Changes saved'),'success');
	      // json_out($in);

	    return true;
	  }
	  function default_pdf_format_body_delivery(&$in)
	  {

	/*    if(!$in['type']){
	      msg::$error = gm('Invalid ID');
	      return false;
	    }*/

	    if($in['identity_id']>0){
	        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='order' ");
	        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='order' ");

	      }else{

	      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_DELIVERY_PDF' ");
	      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_ORDER_DELIVERY_BODY_PDF_FORMAT'");
	      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_ORDER_DELIVERY_PDF_MODULE'");
	      $this->dbu->query("UPDATE settings SET value='".$in['type']."' WHERE constant_name = 'ACCOUNT_ORDER_DELIVERY_PDF_FORMAT'");
	    }
	      global $config;
	      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

	      msg::success ( gm('Changes saved'),'success');
	      // json_out($in);

	    return true;
	  }
	  function default_pdf_format_footer_delivery(&$in)
	  {

	    if($in['identity_id']>0){
	        $this->dbu->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='order' ");
	        $this->dbu->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='order' ");
	      }else{
	      $this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_ORDER_DELIVERY_PDF' ");
	      $this->dbu->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_ORDER_DELIVERY_FOOTER_PDF_FORMAT'");
	      $this->dbu->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_ORDER_DELIVERY_PDF_MODULE'");
	    }
	      global $config;
	      $this->dbu->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

	      msg::success ( gm('Changes saved'),'success');
	      // json_out($in);

	    return true;
	  }
	  function reset_data_delivery(&$in){
	    if($in['header'] && $in['header']!='undefined'){
	      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='order_delivery' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."'");
	    }elseif($in['footer'] && $in['footer']!='undefined'){
	      $variable_data=$this->dbu->field("SELECT content FROM tblquote_pdf_data WHERE master='order_delivery' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."'");
	    }
	    $result=array("var_data" => $variable_data);
	    json_out($result);
	    return true;
	  }

	  function pdfSaveData_delivery(&$in){

	    if($in['header']=='header'){
	      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='order_delivery' AND type='".$in['header']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
	      $exist=$this->dbu->field("SELECT id FROM tblquote_pdf_data WHERE master='order_delivery' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");
	      if($exist){
	        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
	      }else{
	        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='order_delivery',
	                          type='".$in['header']."',
	                          content='".$in['variable_data']."',
	                          initial='1', 
	                          layout='".$in['layout']."',
	                          `default`='1',
                            identity_id='".$in['identity_id']."'");
	      }
	    }else{
	      $this->dbu->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='order_delivery' AND type='".$in['footer']."' AND initial='1' AND identity_id='".$in['identity_id']."'");
	      $exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='order_delivery' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' AND identity_id='".$in['identity_id']."'");

	      if($exist){
	        $this->dbu->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
	      }else{
	        $this->dbu->query("INSERT INTO tblquote_pdf_data SET master='order_delivery',
	                          type='".$in['footer']."',
	                          content='".$in['variable_data']."',
	                          initial='1', 
	                          layout='".$in['layout']."',
	                          `default`='1',
                            identity_id='".$in['identity_id']."' ");
	      }
	    }
	    msg::success ( gm('Data saved'),'success');
	    return true;
	  }
	  function Convention(&$in){

			if($in['ACCOUNT_ORDER_START']){
				$this->db->query("SELECT constant_name FROM settings WHERE constant_name='ACCOUNT_ORDER_START' ");
				if($this->db->move_next()){
					$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_ORDER_START']."' WHERE constant_name='ACCOUNT_ORDER_START' ");
				}else{
					$this->db->query("INSERT INTO settings SET value='".$in['ACCOUNT_ORDER_START']."', constant_name='ACCOUNT_ORDER_START' ");
				}
				msg::success ( gm('Settings updated'),'success');
			}

			if(!defined('ACCOUNT_ORDER_DIGIT_NR')){
				$this->db->query("INSERT INTO settings SET value='".$in['ACCOUNT_ORDER_DIGIT_NR']."', constant_name='ACCOUNT_ORDER_DIGIT_NR', module='billing' ");
					msg::success ( gm('Setting added'),'success');
				}else{
					$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_ORDER_DIGIT_NR']."' WHERE constant_name='ACCOUNT_ORDER_DIGIT_NR'");
					msg::success ( gm('Settings updated'),'success');
				}

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ACCOUNT_ORDER_REF' ");
			if($in['CHECKED']){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ACCOUNT_ORDER_REF' ");
			}
      $ACCOUNT_ORDER_WEIGHT=$this->db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_WEIGHT' ");
      if(is_null($ACCOUNT_ORDER_WEIGHT)){
        $this->db->query("INSERT INTO settings SET constant_name='ACCOUNT_ORDER_WEIGHT',value='".$in['WEIGHT_CHECKED']."' ");
      }else{
        $this->db->query("UPDATE settings SET value='".$in['WEIGHT_CHECKED']."' WHERE constant_name='ACCOUNT_ORDER_WEIGHT' ");
      }		      
		      
			$this->db->query("UPDATE settings SET value='".$in['ACCOUNT_ORDER_DEL']."' WHERE constant_name='ACCOUNT_ORDER_DEL' ");
			msg::success ( gm('Changes saved'),'success');

		return true;
	}
	function save_language(&$in){
			switch ($in['languages']){
				case '1':
					$lang = '';
					break;
				case '2':
					$lang = '_2';
					break;
				case '3':
					$lang = '_3';
					break;
				case '4':
					$lang = '_4';
					break;
				default:
					$lang = '';
					break;
			}
			if($in['languages']>=1000) {
				$lang = '_'.$in['languages'];
			}
			$exist = $this->db->field("SELECT default_id FROM default_data WHERE default_name='order_note' AND type='order_note".$lang."' AND default_main_id='0'");
			if($exist){
				$this->db->query("UPDATE default_data SET value='".$in['notes']."' WHERE default_name='order_note' AND type='order_note".$lang."' AND default_main_id='0' ");
			}else{
				$this->db->insert("INSERT INTO default_data SET default_name='order_note', value='".$in['notes']."', type='order_note".$lang."', default_main_id='0'");
			}

		msg::success ( gm('Changes saved'),'success');
		return true;
	}
	function default_language(&$in){
	   msg::success ( gm("Changes have been saved."),'success');
	    return true;

	}
	function label_update(&$in){

      $fields = '';
      $table = 'label_language_order';

      $exist = $this->dbu->query("SELECT label_language_id FROM $table WHERE label_language_id='".$in['label_language_id']."' ");
      if($exist->next()){
        $this->dbu->query("UPDATE $table SET
        		company_name						= '".$in['company_name']."',
        		comp_reg_number						= '".$in['comp_reg_number']."',
				stock_dispatching_note		 		= '".$in['STOCK_DISPATCHING_NOTE']."',     
				stock_dispatching	         		= '".$in['STOCK_DISPATCHING']."',
				invoice_note		 				= '".$in['INVOICE_NOTE']."',     
				invoice	         					='".$in['INVOICE']."',
				quote_note		 					='".$in['QUOTE_NOTE']."',     
				quote	             				='".$in['QUOTE']."',
				order_note		 					='".addslashes($in['ORDER_NOTE'])."',     
				`order`             				='".$in['ORDER']."',
				p_order_note		 				='".addslashes($in['P_ORDER_NOTE'])."',     
				p_order	         					='".$in['P_ORDER']."',
				article	         					='".$in['ARTICLE']."',
				reference	         				='".$in['REFERENCE']."',
				billing_address	 					='".$in['BILLING_ADDRESS']."',
				date	             				='".$in['DATE']."',
				customer	         				='".$in['CUSTOMER']."',
				item	             				='".$in['ITEM']."',
				quantity	         				='".$in['QUANTITY']."',
				unitmeasure	     					='".$in['UNITMEASURE']."',
				unit_price	     					='".$in['UNIT_PRICE']."',
				amount	         					='".$in['AMOUNT']."',
				subtotal	         				='".$in['SUBTOTAL']."',
				discount	         				='".$in['DISCOUNT']."',
				vat	             					='".$in['VAT']."',
				payments         					='".$in['PAYMENTS']."',
				amount_due	     					='".$in['AMOUNT_DUE']."',
				grand_total	     					='".$in['GRAND_TOTAL']."',
				notes	             				='".$in['NOTES']."',
				bank_details				 		='".$in['BANKD']."',
				duedate			 					='".$in['DUEDATE']."',
				bank_name			 				='".$in['BANK_NAME']."',
				iban				 				='".$in['IBAN']."',
				bic_code			 				='".$in['BIC_CODE']."',
				phone				 				='".$in['PHONE']."',
				fax				 					='".$in['FAX']."',
				url				 					='".$in['URL']."',
				email				 				='".$in['EMAIL']."',
				our_ref								='".$in['OUR_REF']."',
				your_ref			 				='".$in['YOUR_REF']."',
				vat_number		 					  ='".$in['VAT_NUMBER']."',
				CUSTOMER_REF		 				  ='".$in['CUSTOMER_REF']."',
				gov_taxes			 				    ='".$in['GOV_TAXES']."',
				gov_taxes_code	 					='".$in['GOV_TAXES_CODE']."',
				gov_taxes_type	 					='".$in['GOV_TAXES_TYPE']."',
				sale_unit	         				='".$in['SALE_UNIT']."',
				package	         					='".$in['PACKAGE']."',
				shipping_price	 					='".$in['SHIPPING_PRICE']."',
				article_code	     				='".$in['ARTICLE_CODE']."',
				delivery_date	     				='".$in['DELIVERY_DATE']."',
				entry_date	     					='".$in['ENTRY_DATE']."',
				delivery_note	 	 				  ='".$in['DELIVERY_NOTE']."',
				entry_note	 	 					  ='".$in['ENTRY_NOTE']."',
				pick_up_from_store 				='".$in['PICK_UP_FROM_STORE']."',
        pickup_address            ='".$in['pickup_address']."',
        downpayment_paid          ='".$in['downpayment_paid']."',
        downpayment_amount_vat    ='".$in['downpayment_amount_vat']."',
        downpayment_balance       ='".$in['downpayment_balance']."',
        signature                 ='".$in['signature']."',
        amount_order_vat              ='".$in['amount_order_vat']."',
        serial_numbers            ='".$in['serial_numbers']."',
        batches                   ='".$in['batches']."',
        batch_expiration_date     ='".$in['batch_expiration_date']."',
        origin_code               ='".$in['origin_code']."',
				page				 				      ='".$in['PAGE']."'
                WHERE label_language_id='".$in['LABEL_LANGUAGE_ID']."'");
      }else{
        $this->dbu->query("UPDATE $table SET
        		company_name						= '".$in['company_name']."',
        		comp_reg_number						='".$in['comp_reg_number']."',
				stock_dispatching_note		 		='".$in['STOCK_DISPATCHING_NOTE']."',     
				stock_dispatching	         		='".$in['STOCK_DISPATCHING']."',
				invoice_note		 				='".$in['INVOICE_NOTE']."',     
				invoice	         					='".$in['INVOICE']."',
				quote_note		 					='".$in['QUOTE_NOTE']."',     
				quote	             				='".$in['QUOTE']."',
				order_note		 					='".addslashes($in['ORDER_NOTE'])."',     
				`order`	             				='".$in['ORDER']."',
				p_order_note		 				='".addslashes($in['P_ORDER_NOTE'])."',     
				p_order	         					='".$in['P_ORDER']."',
				article	         					='".$in['ARTICLE']."',
				reference	         				='".$in['REFERENCE']."',
				billing_address	 					='".$in['BILLING_ADDRESS']."',
				date	             				='".$in['DATE']."',
				customer	         				='".$in['CUSTOMER']."',
				item	             				='".$in['ITEM']."',
				quantity	         				='".$in['QUANTITY']."',
				unitmeasure	     					='".$in['UNITMEASURE']."',
				unit_price	     					='".$in['UNIT_PRICE']."',
				amount         						='".$in['AMOUNT']."',
				subtotal	         				='".$in['SUBTOTAL']."',
				discount	         				='".$in['DISCOUNT']."',
				vat	             					='".$in['VAT']."',
				payments	         				='".$in['PAYMENTS']."',
				amount_due	     					='".$in['AMOUNT_DUE']."',
				grand_total	     					='".$in['GRAND_TOTAL']."',
				notes	             				='".$in['NOTES']."',
				bank_details				 		='".$in['BANKD']."',
				duedate			 					='".$in['DUEDATE']."',
				bank_name			 				='".$in['BANK_NAME']."',
				iban				 				='".$in['IBAN']."',
				bic_code			 				='".$in['BIC_CODE']."',
				phone				 				='".$in['PHONE']."',
				fax				 					='".$in['FAX']."',
				url				 					='".$in['URL']."',
				email				 				='".$in['EMAIL']."',
				our_ref								='".$in['OUR_REF']."',
				your_ref			 				='".$in['YOUR_REF']."',
				vat_number		 					='".$in['VAT_NUMBER']."',
				CUSTOMER_REF		 				='".$in['CUSTOMER_REF']."',
				gov_taxes			 				='".$in['GOV_TAXES']."',
				gov_taxes_code	 					='".$in['GOV_TAXES_CODE']."',
				gov_taxes_type	 					='".$in['GOV_TAXES_TYPE']."',
				sale_unit	         				='".$in['SALE_UNIT']."',
				package	         					='".$in['PACKAGE']."',
				shipping_price	 					='".$in['SHIPPING_PRICE']."',
				article_code	     				='".$in['ARTICLE_CODE']."',
				delivery_date	     				='".$in['DELIVERY_DATE']."',
				entry_date	     					='".$in['ENTRY_DATE']."',
				delivery_note	 	 				  ='".$in['DELIVERY_NOTE']."',
				entry_note	 	 					  ='".$in['ENTRY_NOTE']."',
				pick_up_from_store 				='".$in['PICK_UP_FROM_STORE']."',
        pickup_address            ='".$in['pickup_address']."',
        downpayment_paid          ='".$in['downpayment_paid']."',
        downpayment_amount_vat    ='".$in['downpayment_amount_vat']."',
        downpayment_balance       ='".$in['downpayment_balance']."',
        signature                 ='".$in['signature']."',
        amount_order_vat              ='".$in['amount_order_vat']."',
        serial_numbers            ='".$in['serial_numbers']."',
        batches                   ='".$in['batches']."',
        batch_expiration_date     ='".$in['batch_expiration_date']."',
        origin_code               ='".$in['origin_code']."',
				page				 				='".$in['PAGE']."'
                WHERE label_language_id='".$in['LABEL_LANGUAGE_ID']."'");
    }
      /*msg::success ( gm('Labels has been successfully updated'),'success');*/
      msg::success ( gm("Changes have been saved."),'success');
      // $in['failure']=false;
      
      return true;
    }
      function default_message(&$in)
	  {
	    $set = "text  = '".$in['text']."', ";
	    /*if($in['use_html']){*/
	      $set1 = "html_content  = '".$in['html_content']."' ";
	    /*}*/
	    $this->dbu->query("UPDATE sys_message SET
	          subject = '".$in['subject']."',
	          ".$set."
	          ".$set1."
	          WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
	    /*$this->dbu->query("UPDATE sys_message SET use_html='".(int)$in['use_html']."' WHERE name='".$in['name']."' ");*/
	    msg::success( gm('Message has been successfully updated'),'success');
	    // json_out($in);
	    return true;
	  }
	function default_message_order(&$in)
	{
		if($in['use_html']){
			$this->db->query("UPDATE sys_message SET
					subject = '".$in['subject']."',
					html_content	= '".$in['html_content']."'
					WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
		}else{
			$this->db->query("UPDATE sys_message SET
					subject = '".$in['subject']."',
					text	= '".$in['html_content']."'
					WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
		}
		 msg::success( gm('Message has been successfully updated'),'success');
		return true;
	}
	function default_message_po(&$in)
	  {
	    $set = "text  = '".$in['text_po']."', ";
	    /*if($in['use_html']){*/
	      $set1 = "html_content  = '".$in['html_content_po']."' ";
	    /*}*/
	    $this->dbu->query("UPDATE sys_message SET
	          subject = '".$in['subject_po']."',
	          ".$set."
	          ".$set1."
	          WHERE name='".$in['name_po']."' AND lang_code='".$in['lang_code']."' ");
	    /*$this->dbu->query("UPDATE sys_message SET use_html='".(int)$in['use_html']."' WHERE name='".$in['name']."' ");*/
	    msg::success( gm('Message has been successfully updated'),'success');
	    // json_out($in);
	    return true;
	  }
	function default_message_order_po(&$in)
	{
		if($in['use_html']){
			$this->db->query("UPDATE sys_message SET
					subject = '".$in['subject_po']."',
					html_content	= '".$in['html_content_po']."'
					WHERE name='".$in['name_po']."' AND lang_code='".$in['lang_code']."' ");
		}else{
			$this->db->query("UPDATE sys_message SET
					subject = '".$in['subject_po']."',
					text	= '".$in['html_content_po']."'
					WHERE name='".$in['name_po']."' AND lang_code='".$in['lang_code']."' ");
		}
		 msg::success( gm('Message has been successfully updated'),'success');
		return true;
	}
	function update_default_email(&$in)
	{
		if(!$this->validate_default_email($in)){
			msg::error ( gm('Write email'),'error');
			return false;
		}

    $mail_type = $in['mail_type_1'];

    $this->db->query("SELECT * FROM default_data WHERE type='order_email_type' ");
    if($this->db->next()){
      $this->db->query("UPDATE default_data SET value='".$mail_type."' WHERE type='order_email_type' ");
    }else{
      $this->db->query("INSERT INTO default_data SET value='".$mail_type."', type='order_email_type' ");
    }

		$this->db->query("UPDATE default_data SET default_name='".$in['default_name']."', value='".$in['email_value']."' WHERE type='order_email' ");
		$this->db->query("UPDATE default_data SET default_name='".$in['po_default_name']."', value='".$in['po_email_value']."' WHERE type='p_order_email' ");

    $this->db->query("SELECT * FROM default_data WHERE type='bcc_order_email' ");
    if($this->db->next()){
      $this->db->query("UPDATE default_data SET value='".$in['bcc_email']."' WHERE type='bcc_order_email' ");
    }else{
      $this->db->query("INSERT INTO default_data SET value='".$in['bcc_email']."', type='bcc_order_email' ");
    }
    $this->db->query("SELECT * FROM default_data WHERE type='bcc_delivery_email' ");
    if($this->db->next()){
      $this->db->query("UPDATE default_data SET value='".$in['bcc_delivery_email']."' WHERE type='bcc_delivery_email' ");
    }else{
      $this->db->query("INSERT INTO default_data SET value='".$in['bcc_delivery_email']."', type='bcc_delivery_email' ");
    }

		msg::success ( gm('Default email updated.'),'success');
		return true;
	}
	function validate_default_email(&$in)
	{
		$v = new validation($in);
		$v->field('value', 'Email', 'email');
		$v->field('po_value', 'Email', 'email');
    $v->field('mail_type_1', 'Email', 'required');
    if($in['mail_type_1'] == 2){
      $v->field('default_name', 'Email', 'required');
      $v->field('email_value', 'Email', 'email:required');
    }

    if($in['bcc_email']){   
      $v->field('bcc_email', 'Type of link', 'emails');
    }
    if($in['bcc_email_delivery']){   
      $v->field('bcc_email_delivery', 'Type of link', 'emails');
    }

		return $v->run();
	}
	function label(&$in){
		$v = new validation($in);
		$v->field('text', 'Field', 'required', gm("Invalid Field Label"));

    	$this->dbu->query("UPDATE settings SET value='0' WHERE constant_name='NOT_COPY_ARTICLE_ORD' ");

    	if($in['not_copy_article_info']){
       		$this->dbu->query("UPDATE settings SET value='1' WHERE constant_name='NOT_COPY_ARTICLE_ORD' ");
    	}

		if(!$v->run()){
			return false;
		}
		$this->db->query("UPDATE `settings` SET `long_value` = '".$in['text']."' WHERE `constant_name` = 'ORDER_FIELD_LABEL'");
		msg::success ( gm('Field label has been updated'),'success');
		return true;
	}
	  function atach(&$in)
	  {
	    $response=array();
	    if (!empty($_FILES)) {
	    	$fileImages = array('jpg','jpeg','gif'); // File extensions
			$size = $_FILES['Filedata']['size'];
			if(!defined('MAX_IMAGE_SIZE') && $size>512000 && in_array(strtolower($fileParts['extension']),$fileImages)){
                  $response['error'] = gm('Size exceeds 500kb');
                  $response['filename'] = $_FILES['Filedata']['name'];
                echo json_encode($response);
                exit();
              }
	      $tempFile = $_FILES['Filedata']['tmp_name'];
	      // $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
	/*      $targetPath = $in['path'];
	*/      $in['name'] = $_FILES['Filedata']['name'];
	      $targetPath = INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/orders';

	      $targetFile = rtrim($targetPath,'/') . '/' . $in['name'];

	      // Validate the file type
	      $fileTypes = array('jpg','jpeg','gif','pdf','docx'); // File extensions
	      $fileParts = pathinfo($_FILES['Filedata']['name']);
	      @mkdir(str_replace('//','/',$targetPath), 0775, true);
	      if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
	      	 if(defined('MAX_IMAGE_SIZE') && $size>MAX_IMAGE_SIZE && in_array(strtolower($fileParts['extension']),$fileImages)){
                $image = new SimpleImage();
                $image->load($_FILES['Filedata']['tmp_name']);
                $image->scale(MAX_IMAGE_SIZE,$size);
                $image->save($targetFile);
            }else{
                move_uploaded_file($tempFile,$targetFile);             
            }
	        global $database_config;

	        $database_2 = array(
	          'hostname' => $database_config['mysql']['hostname'],
	          'username' => $database_config['mysql']['username'],
	          'password' => $database_config['mysql']['password'],
	          'database' => DATABASE_NAME,
	        );
	        $db_upload = new sqldb($database_2);
	        $file_id = $db_upload->insert("INSERT INTO attached_files SET `path` = 'upload/".DATABASE_NAME."/orders/', name = '".$in['name']."', type='3' ");
	        $response['success'] = 'success';
	        $response['filename'] = $_FILES['Filedata']['name'];
	        echo json_encode($response);
	        // ob_clean();
	      } else {
	        $response['error'] = gm('Invalid file type.');
	        $response['filename'] = $_FILES['Filedata']['name'];
	        echo json_encode($response);
	        // echo gm('Invalid file type.');
	      }
	    }
	  }
	  function deleteatach(&$in){
	  	$path=$this->dbu->field("SELECT `path` FROM attached_files WHERE file_id='".$in['id']."' ");
           unlink($path.stripslashes($in['name']));
	    $this->dbu->query("DELETE FROM attached_files WHERE file_id='".$in['id']."' AND name='".$in['name']."' ");
	    msg::success ( gm("Changes have been saved."),'success');
	    return true;
	  }
	  function defaultcheck(&$in){
	    if($in['default_id']==1){
	      $this->dbu->query("UPDATE `attached_files` SET `default`='".$in['default_id']."' WHERE `file_id`='".$in['id']."' ");
	      msg::success ( gm("Changes have been saved."),'success');
	    }else{
	      $this->dbu->query("UPDATE `attached_files` SET `default`='".$in['default_id']."' WHERE `file_id`='".$in['id']."' ");
	      msg::success ( gm("Changes have been saved."),'success');
	    }
	        return true;
	  }
	 function setSetting(&$in)
	{
/*		if(!$this->setSetting_validate($in)){
			return false;
		}
		switch ($in['setting']) {
			case 1:
				$const = 'ACTIVATE_NOTIFICATION_MOBILE_ORDER';
				break;
			case 2:
				$const = 'USER_NOTIFICATION_MOBILE_ORDER';
				break;
			case 3:
				$const = 'SHOW_MY_ORDERS_ONLY';
				break;
			case 4:
				$const = 'ALLOW_ORDER_DROP_BOX';
				break;
			default:
				$const = null;
				break;
		}
		if(!$const){
			return;
		}*/
/*		$this->db->query("SELECT * FROM settings WHERE constant_name='ALLOW_ORDER_DROP_BOX' ");
		if($this->db->next()){*/
			$this->db->query("UPDATE settings SET value='".$in['allow']."' WHERE constant_name='ALLOW_ORDER_DROP_BOX' ");
		/*}else{
			$this->db->query("INSERT INTO settings SET value='".$in['allow']."', constant_name='ALLOW_ORDER_DROP_BOX' ");
		}
*/		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function changedelivery(&$in){
		if(!$in['delivery_type'] || $in['delivery_type'] > 2){
			msg::error ( gm("Invalid ID"),'error');
			return false;
		}else{
			$order_del_step = $this->db->field("SELECT value FROM settings WHERE constant_name = 'ORDER_DELIVERY_STEPS' ");
			$this->db->query("UPDATE settings SET value = '".$in['delivery_type']."' WHERE constant_name = 'ORDER_DELIVERY_STEPS' ");
			if($in['delivery_type']==2&&$order_del_step==1){
				$this->db->query("UPDATE pim_order_deliveries SET delivery_done = '1' ");
			}
			msg::success ( gm("Changes have been saved."),'success');
			return true;
		}
/*
		$this->db->query("UPDATE settings SET value='".$in['delivery_type']."' WHERE constant_name='ORDER_DELIVERY_STEPS'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;*/
	}
	function setMobile(&$in){

		$this->db->query("UPDATE settings SET value='".$in['active']."' WHERE constant_name='ACTIVATE_NOTIFICATION_MOBILE_ORDER'");
		$this->db->query("UPDATE settings SET value='".$in['user_id']."' WHERE constant_name='USER_NOTIFICATION_MOBILE_ORDER'");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function order_note_list(&$in){
    $is_set=$this->db->field("SELECT constant_name FROM settings WHERE constant_name='SHOW_MY_ORDERS_ONLY' ");
    if(is_null($is_set)){
      $this->db->insert("INSERT INTO settings SET constant_name='SHOW_MY_ORDERS_ONLY',value='".$in['list']."',type='1' ");
    }else{
      $this->db->insert("UPDATE settings SET value='".$in['list']."' WHERE constant_name='SHOW_MY_ORDERS_ONLY'  ");
    } 

		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	  function set_default_logo(&$in)
	  {

	      $this->dbu->query("UPDATE settings SET value = '".$in['name']."' WHERE constant_name='ACCOUNT_LOGO_ORDER' ");
	      msg::success (gm("Default logo set."),'success');
	      json_out($in);
	      return true;
	  }

	  function postRegister(&$in){
        global $config;

        $env_usr = $config['postgreen_user'];
        $env_pwd = $config['postgreen_pswd'];

        $post_green_data = array('user_id' => $_SESSION['u_id'],
	      							'database_name' => DATABASE_NAME,
	      							'item_id'		=> '',
	      							'module'		=> 'order',
	      							'date'			=> time(),
	      							'action'		=> 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'],
	      							'content'		=> '',
									'login_user'	=> addslashes($in['user']),
	      							'error_message'	=> ''
	      			);

        try{

        $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
        $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
        $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
        $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
        $soap->__setSoapHeaders(array($objHeader_Session_Outside));
        $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$in['user'],'password'=>$in['pass']));

        if($res->action->data[0]->_ == '000'){
          $post_id=$this->dbu->insert("INSERT INTO apps SET
                                                  main_app_id='0',
                                                  name='PostGreen',
                                                  api='".$in['user']."',
                                                  type='main',
                                                  active='1' ");
          $this->dbu->query("INSERT INTO apps SET main_app_id='".$post_id."', api='".$in['pass']."' ");
          /*msg::success(gm("PostGreen service activated"),"success");*/
          msg::success ( gm("Changes have been saved."),'success');
        }else{
          msg::error(gm('Incorrect credentials provided'),"error");

        }

    } catch (Exception $e) {
        msg::error(gm('PostGeen operation could not be completed, please try again later.'),"error");

        $post_green_data['error_message'] = addslashes($e->getMessage());
        error_post($post_green_data);
        return true;
    }
        return true;
    }

    function postIt(&$in){
	      global $config;

	      $id=$in['id'];

	      $order=$this->dbu->query("SELECT * FROM pim_orders WHERE order_id='".$id."' ");

	      $buyer_country=$this->dbu->field("SELECT name FROM country WHERE country_id='".$order->f('customer_country_id')."' ");
	      $seller_country=$this->dbu->field("SELECT name FROM country WHERE country_id='".ACCOUNT_BILLING_COUNTRY_ID."' ");

	      $in['order_id']=$id;
	      $in['lid'] = $order->f('email_language');
		  if(!$in['lid']){
				$in['lid'] = 1;
		  }
		  if($order->f('pdf_layout')){
		  	$in['type']=$order->f('pdf_layout');
		  	$in['logo']=$order->f('pdf_logo');
		  }else{
		  	$in['type']=ACCOUNT_ORDER_PDF_FORMAT;
		  }
	      $this->generate_pdf($in);
	      $in['attach_file_name'] = 'order_.pdf';
	      $doc_name='order_'.$order->f('serial_number').'.pdf';

	      $handle = fopen($in['attach_file_name'], "r");   
	      $contents = fread($handle, filesize($in['attach_file_name'])); 
	      fclose($handle);                               
	      $decodeContent = base64_encode($contents);

	      $filename ="addresses.csv";
	      ark::loadLibraries(array('PHPExcel'));
	      $objPHPExcel = new PHPExcel();
	      $objPHPExcel->getProperties()->setCreator("Akti")
	               ->setLastModifiedBy("Akti")
	               ->setTitle("Addresses")
	               ->setSubject("Addresses")
	               ->setDescription("Addresses")
	               ->setKeywords("Addresses")
	               ->setCategory("Addresses");

	      $objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A1', "TITLE")
	            ->setCellValue('B1', "FIRST_NAME")
	            ->setCellValue('C1', "LAST_NAME")
	            ->setCellValue('D1', "EMAIL")
	            ->setCellValue('E1', "PHONE")
	            ->setCellValue('F1', "MOBILE")
	            ->setCellValue('G1', "FAX")
	            ->setCellValue('H1', "COMPANY")
	            ->setCellValue('I1', "ADDRESS_LINE_1")
	            ->setCellValue('J1', "ADDRESS_LINE_2")
	            ->setCellValue('K1', "ADDRESS_LINE_3")
	            ->setCellValue('L1', "ADDRESS_POSTCODE")
	            ->setCellValue('M1', "ADDRESS_CITY")
	            ->setCellValue('N1', "ADDRESS_STATE")
	            ->setCellValue('O1', "ADDRESS_COUNTRY");

	      $objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A2', "")
	            ->setCellValue('B2', "")
	            ->setCellValue('C2', "")
	            ->setCellValue('D2', $order->f('customer_email'))
	            ->setCellValue('E2', $order->f('customer_phone'))
	            ->setCellValue('F2', "")
	            ->setCellValue('G2', $order->f('customer_fax'))
	            ->setCellValue('H2', $order->f('customer_name'))
	            ->setCellValue('I2', $order->f('customer_address'))
	            ->setCellValue('J2', "")
	            ->setCellValue('K2', "")
	            ->setCellValue('L2', $order->f('customer_zip'))
	            ->setCellValue('M2', $order->f('customer_city'))
	            ->setCellValue('N2', "")
	            ->setCellValue('O2', $buyer_country);

	      $objPHPExcel->getActiveSheet()->setTitle('Addresses');
	      $objPHPExcel->setActiveSheetIndex(0);
	      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
	      $objWriter->setDelimiter(';');
	      $objWriter->save($filename);

	      $handle_csv = fopen($filename, "r");   
	      $contents_csv = fread($handle_csv, filesize($filename)); 
	      fclose($handle_csv);                               
	      $csvContent   = base64_encode($contents_csv);
	     
	      $env_usr = $config['postgreen_user'];
	      $env_pwd = $config['postgreen_pswd'];
	      $postg_data=$this->dbu->query("SELECT api,app_id FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
	      $panel_usr=$postg_data->f("api");
	      $panel_pswd=$this->dbu->field("SELECT api FROM apps WHERE main_app_id='".$postg_data->f("app_id")."' ");

	      $post_green_data = array('user_id' => $_SESSION['u_id'],
	      							'database_name' => DATABASE_NAME,
	      							'item_id'		=> $in['id'],
	      							'module'		=> 'order',
	      							'date'			=> time(),
	      							'action'		=> 'ORDER_CREATE'.' - '.$config['postgreen_wsdl'],
	      							'login_user'	=> addslashes($panel_usr),
									'content'		=> '',
	      							'error_message'	=> ''
	      			);

	      try{

	      $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
	      $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
	      $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
	      $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
	      $soap->__setSoapHeaders(array($objHeader_Session_Outside));
	      $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$panel_usr,'password'=>$panel_pswd));

	      $soapVr='
	        <order_create_IN>
	          <action type="string">ORDER_CREATE</action> 
	          <attachment_list type="stringlist"></attachment_list>'; 
	      if($in['stationary_id']){
	        $soapVr.='<background type="string">'.$in['stationary_id'].'</background>';
	      }else{
	        $soapVr.='<background type="string"></background>';
	      }
	      $soapVr.='<content_parameters type="indstringlist"></content_parameters> 
	          <csv type="file" extension="csv"> 
	            <nir:data>'.$csvContent.'</nir:data> 
	          </csv> 
	          <document type="file" extension="pdf"> 
	            <nir:data>'.$decodeContent.'</nir:data> 
	          </document> 
	          <document_name type="string">'.$doc_name.'</document_name> 
	          <get_proof type="string">NO</get_proof> 
	          <identifier type="indstringlist"> 
	            <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data> 
	          </identifier> 
	          <insert_list type="indstringlist"></insert_list> 
	          <mailing_type type="string">NONE</mailing_type> 
	          <order_parameters type="indstringlist"></order_parameters> 
	          <page_list type="stringlist"></page_list> 
	          <return_address type="indstringlist">
	            <nir:data key="ADDRESSLINE1">'.ACCOUNT_BILLING_ADDRESS.'</nir:data>
	            <nir:data key="ADDRESSLINE2"></nir:data>
	            <nir:data key="ADDRESSLINE3"></nir:data>
	            <nir:data key="ADDRESSLINE4">'.ACCOUNT_BILLING_ZIP.'</nir:data>
	            <nir:data key="ADDRESSLINE5">'.ACCOUNT_BILLING_CITY.'</nir:data>
	            <nir:data key="ADDRESSLINE6"></nir:data>
	            <nir:data key="ADDRESSLINE7">'.$seller_country.'</nir:data>
	          </return_address> 
	          <sender_address type="indstringlist">
	            <nir:data key="ADDRESSLINE1">'.ACCOUNT_BILLING_ADDRESS.'</nir:data>
	            <nir:data key="ADDRESSLINE2"></nir:data>
	            <nir:data key="ADDRESSLINE3"></nir:data>
	            <nir:data key="ADDRESSLINE4">'.ACCOUNT_BILLING_ZIP.'</nir:data>
	            <nir:data key="ADDRESSLINE5">'.ACCOUNT_BILLING_CITY.'</nir:data>
	            <nir:data key="ADDRESSLINE6"></nir:data>
	            <nir:data key="ADDRESSLINE7">'.$seller_country.'</nir:data>
	          </sender_address> 
	          <service_profile type="indstringlist"> 
	            <nir:data key="service_id">CUSTOM_ENVELOPE</nir:data> 
	            <nir:data key="print_mode">1SIDE</nir:data>
	            <nir:data key="envelop_type">CUSTOM</nir:data>
	            <nir:data key="paper_weight">80g</nir:data>
	            <nir:data key="print_color">COLOR</nir:data>
	            <nir:data key="mail_type">EXPRESS</nir:data>
	            <nir:data key="address_page">EXTRA_PAGE</nir:data>
	            <nir:data key="validation">NO</nir:data>
	          </service_profile> 
	        </order_create_IN>';

	       $post_green_data['content'] = addslashes($soapVr); 
	        
	      $params = new \SoapVar($soapVr,XSD_ANYXML);
	      
	      $result = $soap->order_create($params);
	      if($result->action->data[0]->_ == '000'){
	        $this->dbu->query("UPDATE pim_orders SET postgreen_id='".$result->order_id->_."' WHERE order_id='".$id."' ");
	        $in['sent_date']= time();
	        $this->mark_sent($in);
	        $msg_txt =gm('Order successfully exported');
	      }else{
	        msg::error($result->action->data[1]->_,"error");
	        return true;
	      }

	  } catch (Exception $e) {
        unlink($filename);
        msg::error(gm('PostGeen operation could not be completed, please try again later.'),"error");

        $post_green_data['error_message'] = addslashes($e->getMessage());
        if(empty($soapVr)){
			$post_green_data['action'] = 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'];
	  	}
	  	error_post($post_green_data);

        json_out($in);
    }

	      unlink($filename);

	      msg::success($msg_txt,"success");
	      if($in['related']=='view'){        
	        // return true;
	      }else{
	        unset($in['order_id']); 
	      }
	      return true;
	}

	function postOrderData(&$in){
		if($in['old_obj']['no_status']){
	        json_out($in);
	    }else{
	    	try{
		    	global $config;
		      $env_usr = $config['postgreen_user'];
		      $env_pwd = $config['postgreen_pswd'];
		      $postg_data=$this->dbu->query("SELECT api,app_id FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
		      $panel_usr=$postg_data->f("api");

		      $post_green_data = array('user_id' => $_SESSION['u_id'],
	      							'database_name' => DATABASE_NAME,
	      							'item_id'		=> $in['order_id'],
	      							'module'		=> 'order',
	      							'date'			=> time(),
	      							'action'		=> 'ORDER_DETAILS'.' - '.$config['postgreen_wsdl'],
	      							'login_user'	=> addslashes($panel_usr),
	      							'content'		=> '',
	      							'error_message'	=> ''
	      			);

		      $panel_pswd=$this->dbu->field("SELECT api FROM apps WHERE main_app_id='".$postg_data->f("app_id")."' ");
		      $soap = new SoapClient($config['postgreen_wsdl'],array('trace' => true, 'use' => SOAP_LITERAL, 'cache_wsdl' => WSDL_CACHE_MEMORY));
		      $strHeaderComponent_Session = "<nvheader><nvcommand><nv_user>$env_usr</nv_user><nv_password>$env_pwd</nv_password></nvcommand></nvheader>";
		      $objVar_Session_Inside = new SoapVar($strHeaderComponent_Session, XSD_ANYXML, null, null, null);
		      $objHeader_Session_Outside = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'nvheader', $objVar_Session_Inside);
		      $soap->__setSoapHeaders(array($objHeader_Session_Outside));
		      $res = $soap->authenticate(array('action' => 'AUTHENTICATE','login'=>$panel_usr,'password'=>$panel_pswd));
		      
		      $order_id=$this->dbu->field("SELECT postgreen_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");

		      $post_green_data['content'] = addslashes('<order_details_IN>
		          <action type="string">ORDER_DETAILS</action>
		          <get_billing>NO</get_billing>
		          <get_document>NO</get_document>
		          <get_proof>NO</get_proof>
		          <identifier type="indstringlist">
		            <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
		          </identifier>
		          <order_id type="string">'.$order_id.'</order_id>
		        </order_details_IN>');

		      $params = new \SoapVar('
		        <order_details_IN>
		          <action type="string">ORDER_DETAILS</action>
		          <get_billing>NO</get_billing>
		          <get_document>NO</get_document>
		          <get_proof>NO</get_proof>
		          <identifier type="indstringlist">
		            <nir:data key="SESSION_ID">'.$res->session_id->_.'</nir:data>
		          </identifier>
		          <order_id type="string">'.$order_id.'</order_id>
		        </order_details_IN>',
		         XSD_ANYXML);

		      $result=$soap->order_details($params);
		}catch(Exception $e){
			$post_green_data['error_message'] = addslashes($e->getMessage());

			if(empty($post_green_data['content'])){
				$post_green_data['action'] = 'AUTHENTICATE'.' - '.$config['postgreen_wsdl'];
		  	}	

			error_post($post_green_data);
			
	            $post_status=array('status'=>gm('Postgreen service is temporarily unavailable. Please retry later.'));
	          if($in['related']=='minilist'){
	            json_out($post_status);
	          }else if($in['related']=='status'){
	            $in['status']=$post_status['status'];
	            json_out($in);
	          }else{
	            return json_encode($post_status);
	          }
	      }
	      $PGreen_status=array(
	        '100'   => gm('PostGreen Received'),
	        '140'   => gm('PostGreen Waiting for validation'),
	        '-140'  => gm('PostGreen Rejected'),
	        '160'   => gm('PostGreen Saved'),
	        '200'   => gm('PostGreen Confirmed'),
	        '210'   => gm('PostGreen Processing'),
	        '300'   => gm('PostGreen Processed'),
	        '400'   => gm('PostGreen Validated'),
	        '500'   => gm('PostGreen Printed'),
	        '600'   => gm('PostGreen Posted'),
	        '700'   => gm('PostGreen Sent'),
	        '800'   => gm('PostGreen Arrived'),
	        '810'   => gm('PostGreen Opened'),
	        '900'   => gm('PostGreen Canceled')
	      );
	      $post_status=array('status'=>gm('Post status').": ".$PGreen_status[$result->order_info->row->col[3]->data]);
	      if($in['related']=='minilist'){
	        json_out($post_status);
	      }else if($in['related']=='status'){
	        $in['status']=$post_status['status'];
	        json_out($in);
	      }else{
	        return json_encode($post_status);
	      } 
	    }  
	}
	function saveterm(&$in){
      $order_apply_discount=$this->db->field("SELECT constant_name FROM settings WHERE constant_name='ORDER_APPLY_DISCOUNT' ");
      if(!$order_apply_discount){
          $this->db->query("INSERT INTO settings SET value='".$in['apply_discount']."', constant_name='ORDER_APPLY_DISCOUNT', module='orders' ");
      }else{
        $this->db->query("UPDATE settings SET value='".$in['apply_discount']."' WHERE constant_name='ORDER_APPLY_DISCOUNT' ");
      }

	    $order_delivery_term=$this->db->field("SELECT constant_name FROM settings WHERE constant_name='ORDER_DELIVERY_TERM' ");
      if(!$order_delivery_term){
          $this->db->query("INSERT INTO settings SET value='".$in['delivery']['delivery_term']."', constant_name='ORDER_DELIVERY_TERM', module='orders' ");
      }else{
        $this->db->query("UPDATE settings SET value='".$in['delivery']['delivery_term']."' WHERE constant_name='ORDER_DELIVERY_TERM' ");
      }

      $edit_confirmed_orders=$this->db->field("SELECT constant_name FROM settings WHERE constant_name='EDIT_CONFIRMED_ORDERS' ");
      if(is_null($edit_confirmed_orders)){
          $this->db->query("INSERT INTO settings SET value='".$in['edit_confirmed_orders']."', constant_name='EDIT_CONFIRMED_ORDERS', module='orders' ");
      }else{
        $this->db->query("UPDATE settings SET value='".$in['edit_confirmed_orders']."' WHERE constant_name='EDIT_CONFIRMED_ORDERS' ");
      }

	    msg::success ( gm("Changes have been saved."),'success');
	    return true;
	}



	function downpaymentInvoice_validate(&$in){
		$is_ok=true;
		if($in['downpayment'] && !$in['downpayment_type']){
			msg::error( gm("Please select downpayment value"),'error');
			$is_ok=false;
		}
		if($in['downpayment'] && $in['downpayment_type']){
			if( !return_value($in['downpayment_fixed'] ) ){
				msg::error( gm("Please select downpayment value"),'error');
				$is_ok=false;
			}else if(!return_value($in['downpayment_fixed']) && !return_value($in['downpayment_percent'])){
				msg::error( gm("Please select downpayment value"),'error');
				$is_ok=false;
			}
		}
		return $is_ok;
	}

	function downpaymentInvoice(&$in){
		
		if(!$this->downpaymentInvoice_validate($in)){
			json_out($in);
			return false;
		}
		$order_data=$this->db->query("SELECT * FROM pim_orders WHERE order_id='".$in['order_id']."' ");
		//if($in['downpayment_type'] == '1'){
			$price=return_value($in['downpayment_fixed']);
		/*}else{
			$service_price=$order_data->f('price');
			$price=$service_price*return_value($in['downpayment_percent'])/100;
		}*/
		
		$buyer_details = $this->db->query("SELECT customers.*,customer_legal_type.name as l_name FROM customers
                        LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
                        WHERE customer_id='".$order_data->f('customer_id')."' ");
      	$buyer_address = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$order_data->f('customer_id')."' AND billing ='1' ");
      	$address_info = $buyer_address->f('address')."\n".$buyer_address->f('zip').'  '.$buyer_address->f('city')."\n".get_country_name($buyer_address->f('country_id'));
        //$free_field=$order_data->f('free_field');
        $free_field=$order_data->f('address_info');

	    $email_language = $order_data->f('email_language');
	    $langs = $order_data->f('email_language');
	    if(!$email_language){
	    	$email_language = $buyer_details->f('internal_language');
      	$langs = $buyer_details->f('internal_language');      
	    }
	    if(!$email_language){
	    	$user_lang = $_SESSION['l'];
		if ($user_lang=='nl')
		{
			$user_lang='du';
		}
		switch ($user_lang) {
			case 'en':
				$user_lang_id='1';
				break;
			case 'du':
				$user_lang_id='3';
				break;
			case 'fr':
				$user_lang_id='2';
				break;
		}
	      $email_language = $user_lang_id;
	      $langs = $user_lang_id;
	    }

	    $quote_ts=time();
    	$due_days = $buyer_details->f('payment_term');
	    $invoice_due_date = $buyer_details->f('payment_term') ? $quote_ts + ($buyer_details->f('payment_term') * (60*60*24)) : time();
	    $payment_term_type = $buyer_details->f('payment_term_type') ? $buyer_details->f('payment_term_type') : PAYMENT_TERM_TYPE;
	    $payment_type_choose = $buyer_details->f('payment_term_type') ? $buyer_details->f('payment_term_type') : PAYMENT_TERM_TYPE;
	    if($payment_term_type == 2){
	      /*$curMonth = date('n',$quote_ts);
	      $curYear  = date('Y',$quote_ts);
	        $firstDayNextMonth = mktime(0, 0, 0,$curMonth+1, 1,$curYear);
	      $invoice_due_date = $buyer_details->f('payment_term') ? $firstDayNextMonth + ($buyer_details->f('payment_term') * (60*60*24)-1) : time();*/
        if($buyer_details->f('payment_term')){
          $tmstmp_month=$quote_ts + ( $buyer_details->f('payment_term') * ( 60*60*24 )-1);
          $lastday = date('t',$tmstmp_month);
          $invoice_due_date = mktime(23, 59, 59,date('m',$tmstmp_month), $lastday,date('Y',$tmstmp_month));
        }else{
          $invoice_due_date=time();
        }
	    }

	    $currency_type = $order_data->f('currency_type') ? $order_data->f('currency_type') : ($buyer_details->f('currency_type') ? $buyer_details->f('currency_type') : ACCOUNT_CURRENCY_TYPE);

	    $invoice_note_lang_id = $this->db->field("SELECT lang_id FROM pim_lang WHERE sort_order='1' AND active='1'");
	    // multilanguage notes
	    if($invoice_note_lang_id==1){
	      $invoice_note = 'invoice_note';
	    }else{
	      $invoice_note = 'invoice_note_'.$invoice_note_lang_id;
	    }

	    $notes_1 = addslashes($this->db->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note' "));
	    $notes_2 = addslashes($this->db->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note_2' "));
	    $notes_3 = addslashes($this->db->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note_3' "));
	    $notes_4 = addslashes($this->db->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note_4' "));

	    $custom_translate_loop=array();
	    $custom_langs_inv = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ");
	    while($custom_langs_inv->next()) {
	      $cust_translate_loop=array(
	        'lang_id'           =>    $custom_langs_inv->f('lang_id'),
	        'NOTES'             =>    addslashes($this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type='invoice_note_".$custom_langs_inv->f('lang_id')."' ")),
	      );
	      array_push($custom_translate_loop, $cust_translate_loop);
	    }

	    $NOTES=$notes_1;
	    $notes2 =  addslashes($buyer_details->f('invoice_note2'));
	    $contact_id = $order_data->f('contact_id');
	    $buyer_id = $order_data->f('customer_id');
	    $buyer_name = $order_data->f('customer_id') ? addslashes($buyer_details->f('name')).' '.addslashes($buyer_details->f('l_name')) : addslashes($buyer_details->f('firstname')).' '.addslashes($buyer_details->f('lastname'));
	    $customer_address = addslashes($buyer_address->f('address'));
	    $buyer_email = $buyer_id ? addslashes($buyer_details->f('c_email')) : addslashes($buyer_details->f('email'));
	    $buyer_phone = $buyer_id ? $buyer_details->f('comp_phone') : $buyer_details->f('phone');
	    $buyer_fax = $buyer_id ? $buyer_details->f('comp_fax') : '';
	    $buyer_zip = $buyer_address->f('zip');
	    $buyer_city = addslashes($buyer_address->f('city'));
	    $buyer_country_id = $buyer_address->f('country_id');
	    $buyer_state_id = $buyer_address->f('state_id');
	 
        $vat_regime_id = $this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$buyer_id."'");
        if(!$vat_regime_id){
            $vat_regime_id = $this->db->field("SELECT id FROM vat_new WHERE `default`='1'");
                if(empty($vat_regime_id)){
                  $vat_regime_id = 1;
                }               
            }
        $remove_vat = $this->db->field("SELECT no_vat FROM vat_new WHERE id='".$vat_regime_id."' ");    


	    $currency_rate = $currency_type != ACCOUNT_CURRENCY_TYPE ? $this->get_rate($currency_type) : 1;

	    $acc_manager=$this->db->field("SELECT acc_manager_name FROM customers WHERE customer_id = '".$buyer_id."'");
	    $acc_manager = explode(',',$acc_manager);
	    $acc_manager_id=$this->db->field("SELECT user_id FROM customers WHERE customer_id = '".$buyer_id."'");
	    $acc_manager_id = explode(',',$acc_manager_id);

	    $ac_manager_id=$acc_manager_id[0];
	    $ac_manager_name=$acc_manager[0];
	    $pdf_layout = $this->db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$buyer_details->f('identity_id')."' and module='inv'");
	    if($contact_id) {
	      $contact_name=addslashes($this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE customer_contacts.contact_id='".$contact_id."'"));
	    }

	    $invoice_total = $price*$currency_rate;
	    $invoice_total_vat=$invoice_total+($invoice_total*get_customer_vat($buyer_id)/100);

	    if($_SESSION['main_u_id']=='0'){
			//$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE user_id='".$_SESSION['u_id']."' ");				
			$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);	
		}else{
			$is_accountant=$this->db_users->field("SELECT accountant_id FROM users WHERE `database_name`='".DATABASE_NAME."' AND main_user_id='0' ");
		}
		if($is_accountant){
			$accountant_settings=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_OPTS'");
			if($accountant_settings=='1'){
				$acc_force=$this->db->field("SELECT value FROM settings WHERE `constant_name`='ACCOUNTANT_FORCE_DRAFT' ");
				if($acc_force){
					if(!defined('DRAFT_INVOICE_NO_NUMBER')){
					    $this->db->query("INSERT INTO settings SET value='1', constant_name='DRAFT_INVOICE_NO_NUMBER', module='billing' ");
					}else{
					    $this->db->query("UPDATE settings SET value='1' WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
					}
				}
			}else{
				$force_draft=$this->db_users->field("SELECT force_draft FROM accountants WHERE account_id='".$is_accountant."' ");
				if($force_draft){
					if(!defined('DRAFT_INVOICE_NO_NUMBER')){
					      $this->db->query("INSERT INTO settings SET value='1', constant_name='DRAFT_INVOICE_NO_NUMBER', module='billing' ");
					}else{
					      $this->db->query("UPDATE settings SET value='1' WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
					}
				}
			}	
	     }
	     $DRAFT_INVOICE_NO_NUMBER=$this->db->field("SELECT value FROM settings WHERE constant_name='DRAFT_INVOICE_NO_NUMBER'");
	     if($DRAFT_INVOICE_NO_NUMBER){
	     		$s_number='';
	     }else{
	     		$s_number=addslashes(generate_invoice_number(DATABASE_NAME,false));
	     }
	     
	    $invoice_id=$this->db->insert("INSERT INTO tblinvoice SET
                            serial_number           =   '".$s_number."',
                            invoice_date            =   '".$quote_ts."',
                            due_date          		= '".$invoice_due_date."',
                            due_days          		=   '".$due_days."',
                            payment_term_type     	=   '".$payment_type_choose."',
                            req_payment             =   '100',
                            currency_type           =   '".$currency_type."',
                            notes2                  =   '".utf8_encode($notes2)."',
                            type                    =   '0',
                            contact_id          	=   '".$contact_id."',
                            contact_name         	=   '".$contact_name."',
                            status                  =   '0',
                            f_archived              =   '0',
                            paid                  	=   '0',
                            buyer_id                =   '".$buyer_id."',
                            buyer_name            	=   '".$buyer_name."',
                            buyer_address           =   '".$customer_address."',
                            buyer_email             =   '".$buyer_email."',
                            buyer_phone             =   '".$buyer_phone."',
                            buyer_fax             	=   '".$buyer_fax."',
                            buyer_zip             	=   '".$buyer_zip."',
                            buyer_city            	=   '".$buyer_city."',
                            buyer_country_id        =   '".$buyer_country_id."',
                            buyer_state_id          =   '".$buyer_state_id."',
                            seller_id             	=   '".$in['seller_id']."',
                            seller_name             =   '".addslashes(ACCOUNT_COMPANY)."',
                            seller_d_address        =   '".addslashes(ACCOUNT_DELIVERY_ADDRESS)."',
                            seller_d_zip            =   '".ACCOUNT_DELIVERY_ZIP."',
                            seller_d_city           =   '".addslashes(ACCOUNT_DELIVERY_CITY)."',
                            seller_d_country_id     =   '".ACCOUNT_DELIVERY_COUNTRY_ID."',
                            seller_d_state_id       =   '".ACCOUNT_DELIVERY_STATE_ID."',
                            seller_b_address        =   '".addslashes(ACCOUNT_BILLING_ADDRESS)."',
                            seller_b_zip            =   '".ACCOUNT_BILLING_ZIP."',
                            seller_b_city           =   '".addslashes(ACCOUNT_BILLING_CITY)."',
                            seller_b_country_id     =   '".ACCOUNT_BILLING_COUNTRY_ID."',
                            seller_b_state_id       =   '".ACCOUNT_BILLING_STATE_ID."',
                            seller_bwt_nr           =   '".$buyer_details->f('btw_nr')."',
                            created                 =   '".date(ACCOUNT_DATE_FORMAT,$quote_ts)."',
                            created_by              =   '".$_SESSION['u_id']."',
                            last_upd                =   '".date(ACCOUNT_DATE_FORMAT,$quote_ts)."',
                            last_upd_by             =   '".$_SESSION['u_id']."',
                            vat             		=   '".get_customer_vat($buyer_id)."',
                            our_ref           		=   '".$order_data->f('serial_number')."',
							your_ref				=	'".$order_data->f('your_ref')."',
                            remove_vat          	=   '".$remove_vat."',
                            discount 				= '0',
                            currency_rate       	= '".$currency_rate."',
                            quote_id          		= '0',
                            contract_id         	= '0',
                            email_language        	= '".$email_language."',
                            service_id          	= '0',
                            free_field          	= '".addslashes($free_field)."',
                            same_invoice        	= '0',
                            apply_discount        	= '0',
                            order_id          	= '".$in['order_id']."',
                            attach_timesheet        =   '0',
                            attach_intervention     =     '0',
                            vat_regime_id           ='".$order_data->f('vat_regime_id')."',
                            acc_manager_id          ='".$ac_manager_id."',
                            acc_manager_name        ='".addslashes($ac_manager_name)."',
                            identity_id 			='".$order_data->f('identity_id')."',
                            source_id 			='".$order_data->f('source_id')."',
                            type_id 			='".$order_data->f('type_id')."',
                            pdf_layout				='".$pdf_layout."',
                            same_address			='',
                            amount                  ='".$invoice_total."', 
                            amount_vat              ='".$invoice_total_vat."',
                            margin                  ='".$invoice_total."', 
                            margin_percent          ='0' ");

    if($DRAFT_INVOICE_NO_NUMBER!='1'){
      $ogm = generate_ogm(DATABASE_NAME,$invoice_id);
      $this->db->query("UPDATE tblinvoice SET ogm = '".$ogm."' WHERE id='".$invoice_id."' ");
    }	

	    $lang_note_id = $email_language;
        if($lang_note_id == $email_language){
            $active_lang_note = 1;
        }else{
            $active_lang_note = 0;
        }

        $this->db->insert("INSERT INTO note_fields SET
                            item_id         = '".$invoice_id."',
                            lang_id         =   '".$email_language."',
                            active          =   '".$active_lang_note."',
                            item_type         =   'invoice',
                            item_name         =   'notes',
                            item_value        =   '".$NOTES."' ");
        $custom_lang_note_id = $email_language;

        $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
        while($custom_langs->next()) {
          foreach ($custom_translate_loop as $key => $value) {
            if($value['lang_id'] == $custom_langs->f('lang_id')){
              $nota = $value['NOTES'];
              break;
            }
          }
          if($custom_lang_note_id == $custom_langs->f('lang_id')) {
            $custom_active_lang_note = 1;
          } else {
            $custom_active_lang_note = 0;
          }
          $this->db->insert("INSERT INTO note_fields SET   item_id     = '".$invoice_id."',
                                      lang_id     = '".$custom_langs->f('lang_id')."',
                                      active      = '".$custom_active_lang_note."',
                                      item_type     = 'invoice',
                                      item_name     = 'notes',
                                      item_value    = '".$nota."' ");
        }

        //$vat_percent =return_value(get_customer_vat($buyer_id))
      	$vat_percent=0;
        if($order_data->f('vat_regime_id')){
			$vat_regime=$this->db->field("SELECT vats.value FROM vat_new 
					LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
					WHERE vat_new.id='".$order_data->f('vat_regime_id')."'");
			if(!$vat_regime){
				$vat_regime=0;
			}
			
			$vat_percent=$vat_regime;
			
		}

		$this->db->query("INSERT INTO tblinvoice_line SET
                                    name                =   '".gm('Downpayment')."', 
                                    invoice_id          =   '".$invoice_id."',
                                    quantity            =   '1',
                                    price               =   '".$invoice_total."',
                                    amount              =   '".$invoice_total."',
                                    f_archived          =   '0',
                                    created             =   '".date(ACCOUNT_DATE_FORMAT,$quote_ts)."',
                                    created_by          =   '".$_SESSION['u_id']."',
                                    last_upd            =   '".$created_date."',
                                    last_upd_by         =   '".$_SESSION['u_id']."',
                                    vat                 = '".$vat_percent."',
                                    discount            = '0',
                                    visible             ='1' ");
                
  		insert_message_log($this->pag,'{l}Invoice was created by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['invoice_id']);

  		$down_exist=$this->db->field("SELECT id FROM tbldownpayments WHERE order_id='".$in['order_id']."' ");
  		if($down_exist){
  			$this->db->query("UPDATE tbldownpayments SET
  									invoice_id  = '".$invoice_id."',
  									date        = '".$quote_ts."',
  									user_id 	= '".$_SESSION['u_id']."',
  									value_fixed = '".return_value($in['downpayment_fixed'])."',
  									value_percent='".return_value($in['downpayment_percent'])."'
  									WHERE order_id='".$in['order_id']."' ");
  		}else{
  			$this->db->query("INSERT INTO tbldownpayments SET
  									invoice_id  = '".$invoice_id."',
  									date        = '".$quote_ts."',
  									user_id 	= '".$_SESSION['u_id']."',
  									value_fixed = '".return_value($in['downpayment_fixed'])."',
  									value_percent='".return_value($in['downpayment_percent'])."',
  									order_id   = '".$in['order_id']."' ");
  		}	

  		$tracking_data=array(
	            'target_id'         => $invoice_id,
	            'target_type'       => '1',
	            'target_buyer_id'   => $buyer_id,
	            'lines'             => array(
	                                          array('origin_id'=>$in['order_id'],'origin_type'=>'4')
	                                    )
	          );
	      addTracking($tracking_data);	

  		# for pdf
	    $inv = $this->db->query("SELECT * FROM tblinvoice WHERE id = '".$invoice_id."' ");
	    $params = array();
	    $params['use_custom'] = 0;
	    if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
	      $params['logo'] = $inv->f('pdf_logo');
	      $params['type']=$inv->f('pdf_layout');
	      $params['logo']=$inv->f('pdf_logo');
	      $params['template_type'] = $inv->f('pdf_layout');
	    }elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
	      $params['custom_type']=$inv->f('pdf_layout');
	      unset($params['type']);
	      $params['logo']=$inv->f('pdf_logo');
	      $params['template_type'] = $inv->f('pdf_layout');
	      $params['use_custom'] = 1;
	    }else{
	      $params['type']=ACCOUNT_INVOICE_PDF_FORMAT;
	      $params['template_type'] = ACCOUNT_INVOICE_PDF_FORMAT;
	    }
	    #if we are using a customer pdf template
	    if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
	      $params['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
	      unset($params['type']);
	    }
	    $params['id'] = $invoice_id;
	    $params['lid'] = $email_language;
	    $params['save_as'] = 'F';
	    //$this->generate_pdf($params);
	    # for pdf
	      

	    msg::success(gm('Downpayment invoice created'),"success");
	    return true;	
	}
	function updateSegment(&$in){
		$this->db->query("UPDATE pim_orders SET segment_id='".$in['segment_id']."' WHERE order_id='".$in['order_id']."' ");
		if($in['segment_id']){
			$in['segment'] = $this->db->field("SELECT name FROM tblquote_segment WHERE id='".$in['segment_id']."' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		json_out($in);
	}

	function updateSource(&$in){
		$this->db->query("UPDATE pim_orders SET source_id='".$in['source_id']."' WHERE order_id='".$in['order_id']."' ");
		if($in['source_id']){
			$in['source'] = $this->db->field("SELECT name FROM tblquote_source WHERE id='".$in['source_id']."' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		json_out($in);
	}

	function updateType(&$in){
		$this->db->query("UPDATE pim_orders SET type_id='".$in['type_id']."' WHERE order_id='".$in['order_id']."' ");
		if($in['type_id']){
			$in['xtype']=$this->db->field("SELECT name FROM tblquote_type WHERE id='".$in['type_id']."' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		json_out($in);
	}

	  function set_custom_pdf(&$in)
  {
    $this->dbu->query("DELETE FROM settings WHERE constant_name='USE_CUSTOME_ORDER_PDF' ");
    $this->dbu->query("INSERT INTO settings SET value='1', constant_name='USE_CUSTOME_ORDER_PDF' ");
    $this->dbu->query("UPDATE settings SET value='".$in['custom_type']."' where constant_name='ACCOUNT_ORDER_PDF_FORMAT'");
    msg::success ( gm('Changes saved'),'success');

    return true;
  }

  	  function set_delivery_custom_pdf(&$in)
  {
    $this->dbu->query("DELETE FROM settings WHERE constant_name='USE_CUSTOME_DELIVERY_PDF' ");
    $this->dbu->query("INSERT INTO settings SET value='1', constant_name='USE_CUSTOME_DELIVERY_PDF' ");
    $this->dbu->query("UPDATE settings SET value='".$in['custom_type']."' where constant_name='ACCOUNT_ORDER_DELIVERY_PDF_FORMAT'");
    msg::success ( gm('Changes saved'),'success');

    return true;
  }

    function get_rate($from)
  {

    $currency = currency::get_currency($from,'code');
    if(empty($currency)){
      $currency = "USD";
    }
    $separator = ACCOUNT_NUMBER_FORMAT;
    $into = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');

    return currency::getCurrency($currency, $into, 1,$separator);
  }

  function set_article_visibility(&$in)
  {
    if(!$this->set_article_visibility_validate($in)){
      return false;
    }
      $this->db->query("UPDATE pim_order_articles SET visible='".$in['visible']."' where order_articles_id='".$in['order_articles_id']."' ");
    
    msg::success(gm('Changes have been saved.'),'success');


    return true;
  }

  function set_article_visibility_validate(&$in)
  {
    $v = new validation($in);
    $v -> f('order_articles_id', 'Order Article Id', 'required:exist[pim_order_articles.order_articles_id]');
    $v -> f('order_id', 'Order Id', 'required:exist[pim_orders.order_id]');

    return $v -> run();
      }

    function settings(&$in){

    $show_backorders=$this->db->field("SELECT constant_name FROM settings WHERE constant_name='SHOW_BACKORDERS_DELIVERY' ");
      if(!$show_backorders){
          $this->db->query("INSERT INTO settings SET value='".(int)$in['show_backorders']."', constant_name='SHOW_BACKORDERS_DELIVERY', module='orders' ");
      }else{
        $this->db->query("UPDATE settings SET value='".(int)$in['show_backorders']."' WHERE constant_name='SHOW_BACKORDERS_DELIVERY' ");
      }

    msg::success ( gm('Changes saved'),'success');
    //json_out($in);
    return true;
  }

  function updateDeliveryDate(&$in){
    if(!$in['del_date']){
      json_out($in);
    }
    $this->db->query("UPDATE pim_orders SET del_date='".$in['del_date']."' WHERE order_id='".$in['order_id']."' ");
    $in['factur_del_date']=date(ACCOUNT_DATE_FORMAT,  $in['del_date']);
    msg::success ( gm("Changes have been saved."),'success');
    json_out($in);
  }

  function updateOurReference(&$in){
    $this->db->query("UPDATE pim_orders SET our_ref='".addslashes($in['our_ref'])."' WHERE order_id='".$in['order_id']."' ");
    msg::success ( gm("Changes have been saved."),'success');
    json_out($in);
  }

  function updateYourReference(&$in){
    $this->db->query("UPDATE pim_orders SET your_ref='".addslashes($in['your_ref'])."' WHERE order_id='".$in['order_id']."' ");
    msg::success ( gm("Changes have been saved."),'success');
    json_out($in);
  }

  function update_order_line($in){
    $this->db->query("UPDATE pim_order_articles SET 
      purchase_price='".$in['purchase_price']."',
      purchase_price_other_currency='".$in['purchase_price_other_currency']."',
      purchase_price_other_currency_id='".$in['purchase_price_other_currency_id']."',
      discount = '".return_value($in['disc'])."',
      price = '".return_value($in['price'])."',
      price_with_vat = '".return_value($in['price_vat'])."',
      vat_value   = '".return_value($in['vat_value'])."',
      vat_percent = '".return_value($in['percent'])."'
      WHERE order_articles_id='".$in['order_articles_id']."' ");

    $use_package=$this->db->field("SELECT use_package FROM pim_orders WHERE order_id='".$in['order_id']."'");
    $use_sale_unit=$this->db->field("SELECT use_sale_unit FROM pim_orders WHERE order_id='".$in['order_id']."'");

    $lines=array();
    $order_lines=$this->db->query("SELECT * FROM pim_order_articles 
    WHERE order_id='".$in['order_id']."' ORDER BY sort_order ASC");

    while ($order_lines->next())
    {
      $nr_dec_q = strlen(rtrim(substr(strrchr($order_lines->f('quantity'), "."), 1),'0'));

      $line = array(
        'quantity'            => display_number($order_lines->f('quantity'), $nr_dec_q),
        'price'               => display_number_var_dec($order_lines->f('price')),
        'sale_unit'           => $order_lines->f('sale_unit'),
        'packing'           => remove_zero_decimals($order_lines->f('packing')),
        'disc'            => display_number($order_lines->f('discount')),
      );
      array_push($lines, $line);
    }


    $global_disc = $in['discount'];
    if($in['apply_discount'] < 2){
      $global_disc = 0;
    }
    $order_total=0;
    foreach ($lines as $nr => $row)
    {
        if(!$use_package){
        $row['packing']=1;
      }
      if(!$use_sale_unit){
        $row['sale_unit']=1;
      }
      $discount_line = return_value($row['disc']);
      if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
        $discount_line = 0;
      }
      $q = return_value2($row['packing']) && $row['sale_unit'] ? @(return_value($row['quantity']) * return_value2($row['packing']) / $row['sale_unit']) : 0;
      $price_line = return_value($row['price']) - (return_value($row['price']) * $discount_line /100);
      $line_total=$price_line * $q;
      $order_total+= $line_total - ($line_total*$global_disc/100);
    }

    $this->db->query("UPDATE pim_orders SET amount='".$order_total."' WHERE order_id='".$in['order_id']."'");

    msg::success ( gm("Changes have been saved."),'success');
    return true;
  }

  function generate_certificate(&$in)
  {
    include_once(__DIR__.'/../controller/certificate_print.php');
    //return $str;
  }

   /**
   * make certificate
   *
   * @return bool
   * @author me
   **/
  function make_certificate(&$in)
  {

    if(!$this->make_certificate_validate($in)){
      return false;
    }
 //var_dump($in);exit();

  if(!$in['lid']){
    $in['lid'] =1;
  }
 
   // $now = time(); 
   if (!empty($in['order_articles_id'])) {
      $cert = $this->db->insert("INSERT INTO pim_order_certificate
                              SET 
                                  `order_id`='".$in['order_id']."',
                                  `delivery_id`='".$in['delivery_id']."',
                                  `date` = '".$in['date_cert_line_h']."',
                                  `lang` = '".$in['lid']."',
                                  `certificate_note`  = '".$in['certificate_note']."',
                                  `use_sign_img` = '1'
                                  ");
      
        $in['new_certificate_id'] = $cert;
        $in['certificate_id'] = $cert;
    }else{ 
      msg::error( gm('The certificate has no items'), 'error');
      return false;
    }

    foreach ($in['order_articles_id'] as $key => $value) {

        if ($in['order_id'] && is_numeric($in['delivery_id']) ) {
          $articles = $this->db->query("SELECT quantity, delivery_id FROM pim_orders_delivery WHERE order_articles_id ='".$value."' ");
          $this->db->query("INSERT INTO pim_certificates SET certificate_id='".$cert."', article_id='".$value."',delivery_id='".$in['delivery_id']."', quantity='".$articles->f('quantity')."'"); 
          $this->db->query("UPDATE pim_orders_delivery
                            SET `certified`  = '1'
                              WHERE order_articles_id='".$value."' AND delivery_id = '".$in['delivery_id']."'");

        }
      
      }   
    if($in['certificate_id']){
       //$in['base64'] = 1;
       //$in['str'] = $this->generate_certificate($in);
       $this->generate_certificate($in);
       $date_reversed = date('Ymd');
       $serial_number = $this->db->field("SELECT serial_number FROM pim_orders WHERE order_id ='".$in['order_id']."' ");
       $customer_id = $this->db->field("SELECT customer_id FROM pim_orders WHERE order_id ='".$in['order_id']."' ");
       $file_name = 'Certificate_'.$in['delivery_id'].'_'.$date_reversed;
       $from = __DIR__."/../../../upload/".DATABASE_NAME."/".$file_name.".pdf";

        $d = new drop('orders',$customer_id,$in['order_id'],true,DATABASE_NAME, 0, $serial_number);
        $e = $d->upload($file_name.'.pdf',$from,filesize($from));
     
        unlink($from);
          if(!is_null($e)){  
                $l = $d->getLink(urldecode($e->path_display));
                $this->db->query("INSERT INTO dropbox_files SET 
                  dropbox_folder='orders',
                  parent_id='".$in['order_id']."',
                  name='".addslashes($file_name)."',
                  type='1',
                  serialized_content='".addslashes(serialize($e))."',
                  link='".addslashes($l)."',
                  certificate_id ='".$in['certificate_id']."'");              
                insert_message_log($this->pag,'{l}Certificate generated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['order_id']);
                msg::success ( gm("Success"),'success');
                //json_out($in);
                return true;
                    
          }else{
                msg::error( gm('Error'), 'error');
                return false;
          }
        
     }else{
        msg::error( gm('Error'), 'error');
        return false;
     }
   

  }

  /**
   * validate certificate
   *
   * @return bool
   * @author me
   **/
  function make_certificate_validate(&$in)
  {
    $v = new validation($in);
    $v->field('order_id', 'ID', 'required:exist[pim_orders_delivery.order_id]', "Invalid Id");

    return $v->run();
    
  }

    function edit_certificate(&$in)
  {

    if(!$this->edit_certificate_validate($in)){
      return false;
    } 


    $this->db->query("UPDATE pim_order_certificate
                      SET `certificate_note`  = '".$in['certificate_note']."',
                          `lang`              = '".$in['languages']."',
                          `use_sign_img` = '".$in['add_signature']."'
                           WHERE certificate_id='".$in['certificate_id']."' ");
   
    return true;
  }

  /**
   * validate certificate
   *
   * @return bool
   * @author me
   **/
  function edit_certificate_validate(&$in)
  {

    $v = new validation($in);
    $v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
    $v->field('certificate_id', 'ID', 'required:exist[pim_order_certificate.certificate_id]', "Invalid Id");
 
    
  return $v->run();

  }


  function delete_certificate(&$in){

    if(!$this->delete_certificate_validate($in)){
      return false;
    }

    $this->db->query("DELETE FROM pim_order_certificate WHERE order_id='".$in['order_id']."' AND certificate_id='".$in['certificate_id']."' "); 
        
    $article_id = $this->db->query("SELECT article_id, delivery_id FROM pim_certificates WHERE certificate_id='".$in['certificate_id']."' ");
     while($article_id->next()){
       $this->db->query("UPDATE pim_orders_delivery
                          SET `certified`  = '0'
                         WHERE order_articles_id='".$article_id->f('article_id')."' AND delivery_id='".$article_id->f('delivery_id')."'");

     }

    $this->db->query("DELETE FROM pim_certificates WHERE certificate_id='".$in['certificate_id']."' "); 
    
   
    msg::$success = gm('The certificate was succesfully deleted');
    return true;
  }

  function delete_certificate_validate(&$in){

    $v = new validation($in);
    $v->field('order_id', 'ID', 'required:exist[pim_orders.order_id]', "Invalid Id");
    $v->field('certificate_id', 'ID', 'required:exist[pim_order_certificate.certificate_id]', "Invalid Id");

    return $v->run();
  }

}



?>