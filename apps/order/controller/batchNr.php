<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$batch = array('b_n_row'=>array());

$is_data = false;

if(($in['order_id'] && $in['order_articles_id']) && ($in['count_quantity_nr'] > 0) ){
	$article_id = $db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '".$in['order_articles_id']."' ");
	$packing = $db->field("SELECT packing FROM pim_order_articles WHERE order_articles_id = '".$in['order_articles_id']."' ");
	if(!$packing){
		$packing = 1;
	}
	$sale_unit = $db->field("SELECT sale_unit FROM pim_order_articles WHERE order_articles_id = '".$in['order_articles_id']."' ");
	if(!$sale_unit){
		$sale_unit = 1;
	}

	$batch_locations=array();

	if($in['delivery_approved']==1){
		$batch_number_data = $db->query("SELECT batches.*,batches_dispatch_stock.quantity AS location_qty, batches_dispatch_stock.address_id FROM batches
			LEFT JOIN `batches_dispatch_stock` ON batches.id = batches_dispatch_stock.batch_id
		 WHERE batches.article_id = '".$article_id."' AND batches.in_stock > '0' ORDER BY batches.in_stock ASC ");
	}else{
		$batch_number_data = $db->query("SELECT batches.*,batches_dispatch_stock.quantity AS location_qty, batches_dispatch_stock.address_id  FROM batches 
			LEFT JOIN `batches_dispatch_stock` ON batches.id = batches_dispatch_stock.batch_id
			WHERE batches.article_id = '".$article_id."'  AND batches.status_id != '3' AND batches.in_stock > '0' ORDER BY batches.in_stock ASC ");

	}

	$filter = "batches_from_orders.delivery_id!='0'";
	if($in['return']){
		if($in['return_id']){
			$filter = "batches_from_orders.return_id='".$in['return_id']."' ";
		}
		$batch_number_data = $db->query("SELECT  batches_from_orders.*, batches.batch_number,batches.in_stock, batches.date_exp, batches.id AS batch_id FROM batches_from_orders
		LEFT JOIN batches
		ON batches_from_orders.batch_id = batches.id
		WHERE 	batches_from_orders.article_id =	'".$article_id."'
		AND 		batches_from_orders.order_id 		= 	'".$in['order_id']."'
		AND 		order_articles_id 	= 	'".$in['order_articles_id']."' 
		AND 		$filter ");
	}

	/*print_r("SELECT  batches_from_orders.*, batches.batch_number,batches.in_stock, batches.date_exp, batches.id AS batch_id FROM batches_from_orders
		LEFT JOIN batches
		ON batches_from_orders.batch_id = batches.id
		WHERE 	batches_from_orders.article_id =	'".$article_id."'
		AND 		batches_from_orders.order_id 		= 	'".$in['order_id']."'
		AND 		order_articles_id 	= 	'".$in['order_articles_id']."' 
		AND 		batches_from_orders.delivery_id!='0' ");exit;*/
	
	$i = 0;

	/*$b_n_q_selected = intval('0.00');
	$b_n_q_selected_p = intval('0.00');
	$rest = $in['count_quantity_nr'];
	$rest_p = intval($in['ret_quantity']);*/

	$b_n_q_selected = 0;
	$b_n_q_selected_p = 0;
	$rest = $in['count_quantity_nr'];
	$rest_p = 0;

	$selected_quantity = 0;
	$selected_quantity_p = 0;
	$selected_batches = 0;
	$selected_batches_p = 0;

	while($batch_number_data->next()){

		/*if( $rest > $batch_number_data->f('in_stock')/$packing*$sale_unit ){
			//$b_n_q_selected = intval($batch_number_data->f('in_stock')/$packing*$sale_unit);
			$b_n_q_selected = $batch_number_data->f('in_stock')/$packing*$sale_unit;
			$rest = $rest - $batch_number_data->f('in_stock')/$packing*$sale_unit;
		}elseif($rest<=$batch_number_data->f('in_stock')/$packing*$sale_unit && $rest != 0 ){
			//$b_n_q_selected = intval($rest);
			$b_n_q_selected = $rest;
			//$rest = intval('0.00');
			$rest = 0;
		}else{
			//$b_n_q_selected = intval($rest);
			//$rest = intval('0.00');
			$b_n_q_selected = $rest;
			$rest = 0;
		}*/

		if( $rest > $batch_number_data->f('location_qty')/$packing*$sale_unit ){
			$b_n_q_selected = $batch_number_data->f('location_qty')/$packing*$sale_unit;
			$rest = $rest - $batch_number_data->f('location_qty')/$packing*$sale_unit;
		}elseif($rest<=$batch_number_data->f('location_qty')/$packing*$sale_unit && $rest != 0 ){
			$b_n_q_selected = $rest;
			$rest = 0;
		}else{
			$b_n_q_selected = $rest;
			$rest = 0;
		}

		$selected_quantity += $b_n_q_selected;
		if($b_n_q_selected>0){
			$selected_batches++;
		}

		$add_location_name = '';
		$location_name=get_location_name($batch_number_data->f('address_id'));
		if($location_name){
			$add_location_name = " (".$location_name.")";
		}

		if($in['delivery_approved']){
			if($in['selected_items']){
				$quantity_sel = 0;
				foreach ($in['selected_items'] as $key => $value) {
					if($value['order_articles_id'] == $in['order_articles_id'] && $value['batch_id'] == $batch_number_data->f('id') && $value['address_id'] == $batch_number_data->f('address_id')){
						$quantity_sel = $value['batch_q'];
					}
				}
				$linie=array(
					'article_id'				=> $article_id,
					'order_articles_id'			=> $in['order_articles_id'],
					'batch_no_checked'			=> $quantity_sel > 0 ? true : false,
					'batch_checked'				=> $quantity_sel > 0 ? 'active' : '',
					'batch_number'				=> $batch_number_data->f('batch_number').$add_location_name,
					//'in_stock'					=> intval($batch_number_data->f('in_stock')+$quantity_sel),
					//'in_stock'					=> display_number($batch_number_data->f('in_stock')+$quantity_sel),
					'in_stock'					=> display_number($batch_number_data->f('location_qty')),
					'b_n_q_selected'			=> $quantity_sel > 0 ? display_number($quantity_sel) : display_number(0) ,
					'b_n_id'					=> $batch_number_data->f('id'),
					//'red_row'					=> (intval($batch_number_data->f('in_stock'))+$quantity_sel) < $quantity_sel ? 'red' : '',
					'red_row'					=> ($batch_number_data->f('in_stock')+$quantity_sel) < $quantity_sel ? 'red' : '',
					'date_exp'					=> $batch_number_data->f('date_exp')<10000? '':date(ACCOUNT_DATE_FORMAT,$batch_number_data->f('date_exp')),
					'is_error'					=> false,
					'address_id'				=> $batch_number_data->f('address_id'),
				);
				if(array_key_exists($batch_number_data->f('address_id'), $batch_locations) === false){
					$batch_locations[$batch_number_data->f('address_id')]=get_location_name($batch_number_data->f('address_id'));
				}
			}else{
				$linie=array(
					'article_id'				=> $article_id,
					'order_articles_id'			=> $in['order_articles_id'],
					'batch_no_checked'			=> $b_n_q_selected > 0 ? true : false,
					'batch_checked'				=> $b_n_q_selected > 0 ? 'active' : '',
					'batch_number'				=> $batch_number_data->f('batch_number').$add_location_name,
					//'in_stock'					=> intval($batch_number_data->f('in_stock')),
					//'in_stock'					=> display_number($batch_number_data->f('in_stock')),
					'in_stock'					=> display_number($batch_number_data->f('location_qty')),
					'b_n_q_selected'			=> display_number($b_n_q_selected),
					'b_n_id'					=> $batch_number_data->f('id'),
					'date_exp'					=> $batch_number_data->f('date_exp')<10000? '':date(ACCOUNT_DATE_FORMAT,$batch_number_data->f('date_exp')),
					'is_error'					=> false,
					'address_id'				=> $batch_number_data->f('address_id'),
				);
				if(array_key_exists($batch_number_data->f('address_id'), $batch_locations) === false){
					$batch_locations[$batch_number_data->f('address_id')]=get_location_name($batch_number_data->f('address_id'));
				}
			}
		}else{
			if($in['selected_items']){
				$quantity_sel = 0;
				foreach ($in['selected_items'] as $key => $value) {
					if($value['order_articles_id'] == $in['order_articles_id'] && $value['batch_id'] == $batch_number_data->f('id')){
						$quantity_sel = $value['batch_q'];
					}
				}
				$linie=array(
					'article_id'				=> $article_id,
					'order_articles_id'			=> $in['order_articles_id'],
					'batch_no_checked'			=> $quantity_sel > 0 ? true : false,
					'batch_checked'				=> $quantity_sel > 0 ? 'active' : '',
					'batch_number'				=> $batch_number_data->f('batch_number').$add_location_name,
					//'in_stock'					=> intval($batch_number_data->f('in_stock')),
					//'in_stock'					=> display_number($batch_number_data->f('in_stock')),
					'in_stock'					=> display_number($batch_number_data->f('location_qty')),
					'b_n_q_selected'			=> $quantity_sel > 0 ? display_number($quantity_sel) : display_number(0) ,
					'b_n_id'					=> $batch_number_data->f('id'),
					//'red_row'					=> intval($batch_number_data->f('in_stock')) < $quantity_sel ? 'red' : '',
					'red_row'					=> $batch_number_data->f('in_stock') < $quantity_sel ? 'red' : '',
					'date_exp'					=> $batch_number_data->f('date_exp')<10000? '':date(ACCOUNT_DATE_FORMAT,$batch_number_data->f('date_exp')),
					'is_error'					=> false,
					'address_id'				=> $batch_number_data->f('address_id'),
				);
				if(array_key_exists($batch_number_data->f('address_id'), $batch_locations) === false){
					$batch_locations[$batch_number_data->f('address_id')]=get_location_name($batch_number_data->f('address_id'));
				}
			}else{
				$linie=array(
					'article_id'				=> $article_id,
					'order_articles_id'			=> $in['order_articles_id'],
					'batch_no_checked'			=> $b_n_q_selected > 0 ? true : false,
					'batch_checked'				=> $b_n_q_selected > 0 ? 'active' : '',
					'batch_number'				=> $batch_number_data->f('batch_number').$add_location_name,
					//'in_stock'					=> intval($batch_number_data->f('in_stock')),
					//'in_stock'					=> display_number($batch_number_data->f('in_stock')),
					'in_stock'					=> display_number($batch_number_data->f('location_qty')),
					'b_n_q_selected'			=> display_number($b_n_q_selected),
					'b_n_id'					=> $batch_number_data->f('id'),
					'date_exp'					=> $batch_number_data->f('date_exp')<10000? '':date(ACCOUNT_DATE_FORMAT,$batch_number_data->f('date_exp')),
					'is_error'					=> false,
					'address_id'				=> $batch_number_data->f('address_id'),
				);
				if(array_key_exists($batch_number_data->f('address_id'), $batch_locations) === false){
					$batch_locations[$batch_number_data->f('address_id')]=get_location_name($batch_number_data->f('address_id'));
				}
			}
			

			//$q1 = intval($batch_number_data->f('quantity')/$packing*$sale_unit);
			$q1 = $batch_number_data->f('quantity')/$packing*$sale_unit;

			if( $rest_p > $q1 ){
				//$b_n_q_selected_p = intval($q1);
				$b_n_q_selected_p = $q1;
				$rest_p = $rest_p - $q1;
			}elseif($rest_p<=$q1 && $rest_p != 0 ){
				/*$b_n_q_selected_p = intval($rest_p);
				$rest_p = intval('0.00');*/
				$b_n_q_selected_p = $rest_p;
				$rest_p = 0;
			}else{
				/*$b_n_q_selected_p = intval($rest_p);
				$rest_p = intval('0.00');*/
				$b_n_q_selected_p = $rest_p;
				$rest_p = 0;
			}
			$selected_quantity_p += $b_n_q_selected_p;
			if($b_n_q_selected_p>0){
				$selected_batches_p++;
			}


			if($in['return_id']){
				if(!$in['has_changed']){
					//$b_n_q_selected_p = intval($batch_number_data->f('quantity'));
					$b_n_q_selected_p = $batch_number_data->f('quantity');
				}
			}

			if($in['return']){
				$linie=array(
					'article_id'				=> $article_id,
					'order_articles_id'			=> $in['order_articles_id'],
					'batch_no_checked'			=> $b_n_q_selected_p > 0 ? true : false,
					'batch_checked'				=>'active',
					'batch_number'				=> $batch_number_data->f('batch_number').$add_location_name,
					//'in_stock'					=> intval($batch_number_data->f('quantity')),
					'in_stock'					=> display_number($batch_number_data->f('quantity')),
					'b_n_q_selected'			=> display_number($b_n_q_selected_p),
					'b_n_id'					=> $batch_number_data->f('id'),
					'date_exp'					=> $batch_number_data->f('date_exp')<10000? '':date(ACCOUNT_DATE_FORMAT,$batch_number_data->f('date_exp')),
					'batch_id'					=> $batch_number_data->f('batch_id'),
					'is_error'					=> false,
					'address_id'				=> $batch_number_data->f('address_id'),
				);
				if(array_key_exists($batch_number_data->f('address_id'), $batch_locations) === false){
					$batch_locations[$batch_number_data->f('address_id')]=get_location_name($batch_number_data->f('address_id'));
				}
			}
		}
		array_push($batch['b_n_row'],$linie);
		// $view_b_n->loop('b_n_row');
		$i++;
	}

// $view_b_n->assign(array(
	$batch['packing']					= $packing;
	$batch['sale_unit']					= $sale_unit;
	$batch['is_delivery_id']			= true;
	$batch['page_title']			= gm("Select batches");
	$batch['batch_locations']		= array();
	foreach($batch_locations as $key=>$val){
		$batch['batch_locations'][]=array('address_id'=>$key,'name'=>$val);
	}
	// ));

}else{//only view files
	$i = 0;
	$article_id = $db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '".$in['order_articles_id']."' ");
	$packing = $db->field("SELECT packing FROM pim_order_articles WHERE order_articles_id = '".$in['order_articles_id']."' ");
	$sale_unit = $db->field("SELECT sale_unit FROM pim_order_articles WHERE order_articles_id = '".$in['order_articles_id']."' ");

	$batch_number_data = $db->query("SELECT  batches_from_orders.*, batches.batch_number,batches.in_stock FROM batches_from_orders
		LEFT JOIN batches
		ON batches_from_orders.batch_id = batches.id
		WHERE 	batches_from_orders.article_id =	'".$article_id."'
		AND 		batches_from_orders.delivery_id 		= 	'".$in['delivery_id']."'
		AND 		order_articles_id 	= 	'".$in['order_articles_id']."' ");
	if($in['return']){
		$batch_number_data = $db->query("SELECT  batches_from_orders.*, batches.batch_number,batches.in_stock, batches.date_exp, batches.id AS batch_id FROM batches_from_orders
		LEFT JOIN batches
		ON batches_from_orders.batch_id = batches.id
		WHERE 	batches_from_orders.article_id =	'".$article_id."'
		AND 		batches_from_orders.order_id 		= 	'".$in['order_id']."'
		AND 		order_articles_id 	= 	'".$in['order_articles_id']."' ");
	}
	while($batch_number_data->next()){
		$quantity_sel = 0;
		if($in['selected_items']){
			foreach ($in['selected_items'] as $key => $value) {
				if($value['order_articles_id'] == $in['order_articles_id'] && $value['batch_id'] == $batch_number_data->f('id')){
					$quantity_sel = $value['batch_q'];
				}
			}
		}
		$linie=array(
			'article_id'				=> $article_id,
			'order_articles_id'			=> $in['order_articles_id'],
			'batch_no_checked'			=> true,
			'batch_checked'				=> 'active',
			'batch_number'				=> $batch_number_data->f('batch_number'),
			/*'in_stock'					=> intval($batch_number_data->f('in_stock'))+intval($batch_number_data->f('quantity')),
			'b_n_q_selected'				=> intval($batch_number_data->f('quantity')),*/
			'in_stock'					=> display_number($batch_number_data->f('in_stock')+$batch_number_data->f('quantity')),
			'b_n_q_selected'			=> display_number($batch_number_data->f('quantity')),
			'b_n_id'					=> $batch_number_data->f('id'),
			//'red_row'					=> intval($batch_number_data->f('in_stock')) < $quantity_sel ? 'red' : '',
			'red_row'					=> $batch_number_data->f('in_stock') < $quantity_sel ? 'red' : '',
			'disabled_on_view'			=> true,
			'date_exp'					=> $batch_number_data->f('date_exp')<10000? '':date(ACCOUNT_DATE_FORMAT,$batch_number_data->f('date_exp')),
		);

		if($in['return']){
				$linie=array(
					'article_id'				=> $article_id,
					'order_articles_id'			=> $in['order_articles_id'],
					'batch_no_checked'			=>  true ,
					'batch_checked'				=>'active',
					'batch_number'				=> $batch_number_data->f('batch_number'),
					/*'in_stock'					=> intval($batch_number_data->f('in_stock')),
					'b_n_q_selected'				=> $batch_number_data->f('quantity'),*/
					'in_stock'					=> display_number($batch_number_data->f('in_stock')),
					'b_n_q_selected'			=> display_number($batch_number_data->f('quantity')),
					'b_n_id'					=> $batch_number_data->f('id'),
					'date_exp'					=> $batch_number_data->f('date_exp')<10000? '':date(ACCOUNT_DATE_FORMAT,$batch_number_data->f('date_exp')),
					'batch_id'					=> $batch_number_data->f('batch_id')
				);
			}

		array_push($batch['b_n_row'],$linie);

		$i++;
	}
	// $view_b_n->assign(array(
		$batch['is_delivery_id']			= false;
		$batch['page_title']			= gm("Selected batches");
	// ));

}
if($i>0){
	$is_data = true;
}
// $view_b_n->assign(array(
$batch['is_data']							= $is_data;
$batch['order_articles_id']		= $in['order_articles_id'];
$batch['total_b_n']						= display_number($selected_quantity);
$batch['selected_b_n']				= display_number($in['count_quantity_nr']);
$batch['green']								= $selected_quantity == $in['count_quantity_nr'] ? 'green' : '';
$batch['selected_batches']		= $selected_batches;
// ));
if($in['return']){
	$batch['total_b_n']						= display_number($selected_quantity_p);
	$batch['selected_b_n']				= display_number($in['ret_quantity']);
	$batch['selected_batches']		= $selected_batches_p;
}
if($in['selected_items']){
	$selected_quantity = 0;
	$selected_batches = 0;
	foreach ($in['selected_items'] as $key => $value) {
		$selected_quantity+=$value['batch_q'];
		$selected_batches++;
	}
	// $view_b_n->assign(array(
	$batch['total_b_n']						= display_number($selected_quantity);
	$batch['selected_b_n']				= display_number($in['count_quantity_nr']);
	$batch['green']								= $selected_quantity == $in['count_quantity_nr'] ? 'green' : '';
	$batch['selected_batches']		= $selected_batches;
	// ));
}


json_out($batch);
?>