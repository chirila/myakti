<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
$db6= new sqldb();
$o=array('recipients'=>array(),'lines'=>array(),'order_articles_id' =>array());

if(ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS){
	$o['view_location']=true;
}else{
	$o['view_location']=false;
	$o['quantity_last']='last';
}

//delivery data without articles
$in['edit_note']=1;
$db->query("SELECT pim_order_deliveries.*,pim_orders_delivery.delivery_note
		FROM  pim_order_deliveries
		INNER JOIN pim_orders_delivery ON pim_orders_delivery.delivery_id = pim_order_deliveries.delivery_id
		WHERE pim_order_deliveries.order_id='".$in['order_id']."' AND pim_order_deliveries.delivery_id='".$in['delivery_id']."'");
$db->move_next();
$in['date_del_line_h']  = $db->gf('date');
$o['date_del_line_h']		= $db->gf('date')*1000;
$o['delivery_date_js'] 		= $db->gf('date')*1000;
$in['date_del_line'] 		= date(ACCOUNT_DATE_FORMAT,$db->gf('date'));
$o['date_del_line'] 		= date(ACCOUNT_DATE_FORMAT,$db->gf('date'));
$in['contact_name'] 		= $db->gf('contact_name');
$o['contact_name'] 			= $db->gf('contact_name');
$in['contact_id'] 			= $db->gf('contact_id');
$o['contact_id'] 				= $db->gf('contact_id');
$o['hour']          		= $db->f('hour')?$db->f('hour'):'';
$o['minute']        		= $db->f('minute')?$db->f('minute'):'';
$o['pm']            		= $db->f('pm')?$db->f('pm'):'';
$o['is_custom_address']		= $db->f('is_custom_address') ? true : false;

$order =  $db6->query("SELECT pim_orders.* FROM pim_orders WHERE pim_orders.order_id='".$in['order_id']."'");
$order->move_next();
$allow_packing = $order->f('use_package');
$allow_sale_unit = $order->f('use_sale_unit');

$in['buyer_id']=$db->f('buyer_id') ? $db->f('buyer_id') : $order->f('customer_id');
$o['buyer_id']=$db->f('buyer_id') ? $db->f('buyer_id') : $order->f('customer_id');
$o['buyer_name']=$db->f('buyer_id') ? stripslashes($db->f('buyer_name')) : stripslashes($order->f('customer_name'));
$o['cc']	= get_cc($in,true,false);

$o['allow_article_packing']       = ALLOW_ARTICLE_PACKING ? $allow_packing : false;
$o['allow_article_sale_unit']     = ALLOW_ARTICLE_PACKING ? $allow_sale_unit : false;

$o['delivery_note']= $db->gf('delivery_note');

if($db->f('delivery_address')){
	$o['delivery_address']					=$db->f('delivery_address');
}else{
	$order =  $db6->query("SELECT pim_orders.* FROM pim_orders WHERE pim_orders.order_id='".$in['order_id']."'");
	if($order->f('delivery_address')){
		$o['delivery_address']=$order->gf('delivery_address');
	}else{
		$o['delivery_address']=$order->f('address_info');
	}
}

$o['delivery_address_id']				=$db->f('delivery_address_id') && !$db->f('is_custom_address') ? $db->f('delivery_address_id'):(!$db->f('is_custom_address') ? $order->f('delivery_address_id') : '');
//$o['delivery_addresses']				=get_delivery_addresses($order->f('customer_id'),$o['delivery_address_id']);
$o['delivery_addresses']	= get_addresses($in,true,false);
$o['recipients']= get_contacts($in,true,false);

//articles data
$i = 0;
$j = 0;
$m = 0;
$article_ids="";
$customers_dep = $db->query("SELECT  dispatch_stock_address.* FROM  dispatch_stock_address order by is_default DESC")->getAll();
$un_query = $db->query("SELECT  dispatch_stock.*,pim_stock_disp.to_customer_id,pim_stock_disp.to_naming,pim_stock_disp.to_address_id,pim_stock_disp.to_address,pim_stock_disp.to_zip,pim_stock_disp.to_city,pim_stock_disp.to_country
           FROM  dispatch_stock
           INNER JOIN pim_stock_disp ON pim_stock_disp.to_customer_id=dispatch_stock.customer_id and pim_stock_disp.to_address_id=dispatch_stock.address_id
           WHERE dispatch_stock.customer_id!=0
           GROUp BY dispatch_stock.customer_id,dispatch_stock.address_id")->getAll();
$line = $db->query("	SELECT pim_order_articles.*, pim_orders_delivery.*, pim_orders.rdy_invoice FROM pim_order_articles
				INNER JOIN pim_orders_delivery ON pim_orders_delivery.order_articles_id=pim_order_articles.order_articles_id
	            LEFT JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
				WHERE pim_orders_delivery.order_id='".$in['order_id']."' AND pim_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC");
while($line->next()) {

	$is_service = $db->field("SELECT is_service FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
	$is_combined = $db->field("SELECT use_combined FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
	$left = $line->f('quantity');
	$array_disp = array();
	$nr = 0;
	foreach ($customers_dep as $key => $value) {
	  $qty=$db->field("SELECT dispatch_stock.stock FROM dispatch_stock WHERE article_id = '".$line->f('article_id')."' and address_id='".$value['address_id']."' and main_address=1");
	  if(!$key){
		  	$quantity_delivered=$left;
		  }else{
		  	$quantity_delivered=0;
		  }
		// var_dump($left, $qty, $nr);
	  array_push($array_disp, array(
       'quantity'                  => display_number($qty),
       'qty'                  	   => $qty,
       //'quantity_delivered'        => display_number($view_quantity),
       'quantity_delivered'        => display_number($quantity_delivered),
       'order_articles_id'         => $line->f('order_articles_id'),
       'customer_name'             =>  $value['naming'],
       'customer_id'               =>  0,
       'address_id'                =>  $value['address_id'],
       'order_buyer_country'       => get_country_name($value['country_id']),
       'order_buyer_state'         => $value['state'],
       'order_buyer_city'          => $value['city'],
       'order_buyer_zip'           => $value['zip'],
       'order_buyer_address'       => $value['address'],
       'cust_addr_id'			   => '0-'.$value['address_id'],
       'is_article' 			   => !$is_service,
       'is_not_combined' 		   => $is_combined? false:true,
       'is_error'				   => (!$is_combined && !$is_service && $nr == 0 && ($left > $qty) )? true : false,
    ));
    if($nr==0){
    	$new_qty=$qty;
    }
	  $nr++;
	}
	foreach ($un_query as $key => $value) {

		  $qty=$db->field("SELECT dispatch_stock.stock FROM dispatch_stock WHERE article_id = '".$line->f('article_id')."' and address_id='".$value['to_address_id']."' and customer_id='".$value['customer_id']."'");
		  array_push($array_disp, array(
				'quantity'					=>  display_number($qty),
				'qty'                  		=> $qty,
				'quantity_delivered'        => display_number($in['quantity_delivered']),
				'order_articles_id'			=> $line->f('order_articles_id'),
				'customer_name'				=> $value['to_naming'],
				'customer_id'   			=> $value['to_customer_id'],
				'address_id'    			=> $value['to_address_id'],
				'type'          			=> 2,
				'order_buyer_country'     	=> $value['to_country'],
				'order_buyer_city'         	=> $value['to_city'],
				'order_buyer_zip'          	=> $value['to_zip'],
				'order_buyer_address'      	=> $value['to_address'],
				'cust_addr_id'				=> $value['customer_id'].'-'.$value['address_id'],
				'is_error'					=> false,
		  ));

		}
	$linie = array();
	$delivered = 0;
	$left = 0;
	$delivered = $db->field("SELECT SUM(quantity) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$line->f('order_articles_id')."' ");
	$left = $line->f('quantity');
	$hide_stock = $db->field("SELECT hide_stock FROM pim_articles WHERE article_id='".$line->f('article_id')."' ");

	// article serial numbers
	$use_serial_no = $db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
	if($use_serial_no==1){
		$j++;
		$article_s_n_data = $db6->query("SELECT id FROM serial_numbers WHERE article_id = '".$line->f('article_id')."' AND delivery_id = '".$in['delivery_id']."' ");
		$k = 0;
		$selected_s_n =array();
		while($article_s_n_data->next()){
		    /*if($k==0){
		      $selected_s_n .= $article_s_n_data->f('id');
		    }else{
		      $selected_s_n .= ','.$article_s_n_data->f('id');
		    }*/
		    array_push($selected_s_n, array('serial_nr_id'=>$article_s_n_data->f('id')));
		  $k++;
		}
		if($k>0){
		  $use_serial_no = true;
		  $linie['use_serial_no_art']     = true;
		}
		$count_selected_s_n = $k;
		 $linie['selected_s_n']          = $selected_s_n;
		 $linie['count_selected_s_n']    = $count_selected_s_n;
	}

	$packing = $db->field("SELECT packing FROM pim_articles WHERE article_id='".$line->f('article_id')."' ");
	$sale_unit = $db->field("SELECT sale_unit FROM pim_articles WHERE article_id='".$line->f('article_id')."' ");

	$first_location_qty =$array_disp[0]['quantity'];

	$linie['article']										= $line->f('article');
	$linie['quantity']										= display_number($left);
	$linie['quantity2']										= display_number($left);
	$linie['rdy_invoice']										= $line->f('rdy_invoice');
	$linie['order_articles_id'] 							= $line->f('order_articles_id');
	$linie['article_id']									= $line->f('article_id');
	$linie['show_stock']           							= ($hide_stock == 0 && $line->f('article_id'))? true : false;
	$linie['quantity_value']								= $left;
	$linie['use_serial_no_art']								= $use_serial_no == 1 ? true : false;
	$linie['serialOk']										= true;
	$linie['allow_article_packing']       					= ALLOW_ARTICLE_PACKING ? $allow_packing : false;
	$linie['allow_article_sale_unit']     					= ALLOW_ARTICLE_PACKING ? $allow_sale_unit : false;
	$linie['packing']										= $packing ? remove_zero_decimals($packing) : 1;
	$linie['sale_unit']										= $sale_unit ? $sale_unit : 1;
	$linie['disp_addres_info'] 								= $array_disp;
	//$linie['is_error']										= (!$hide_stock && $line->f('article_id') && ALLOW_STOCK) ? ($left > $first_location_qty ? true : false) : false;
	$linie['is_error']										= (!$is_combined && !$is_service && !$hide_stock && $line->f('article_id') && ALLOW_STOCK) ? ($left > return_value($first_location_qty) ? true : false) : false;
	$linie['is_article']									= !$is_service;
	$linie['is_not_combined']								= $is_combined? false:true;

	// article batch numbers
	$use_batch_no = $db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
	if($use_batch_no==1){
		$batches_orders_data = $db6->query("SELECT * FROM batches_from_orders WHERE order_articles_id = '".$line->f('order_articles_id')."' AND delivery_id = '".$in['delivery_id']."' ");
		$batches_orders_data = $batches_orders_data->getAll();
		$linie['edit_batches']=array();
		$count_selected_b_n =0;
		$selected_b_n = array();
		foreach ($batches_orders_data as $key => $value) {
			array_push($linie['edit_batches'],array(
				'batch_id'			=> 		$value['batch_id'],
				'batch_q'			=> 		$value['quantity']*$sale_unit/$packing,
				'order_articles_id'	=> 		$value['order_articles_id'],
				'address_id' 		=> 		$value['address_id']
			));
			array_push($selected_b_n, array('b_n_id'=>$value['batch_id'],'b_n_q_selected'=>$value['quantity']*$sale_unit/$packing,'address_id'=>$value['address_id']));
			$count_selected_b_n +=$value['quantity']*$sale_unit/$packing;
			$m++;
		}
		if($m>0){
		  $use_batch_no = true;
		  $linie['use_batch_no_art']     = true;
		  $linie['selected_b_n']				 = $selected_b_n;
		  $linie['count_selected_b_n']	 = $count_selected_b_n;
		  //$linie['batchOk']							 = true;
		  $linie['batchOk']							 = false;
		  $linie['green_b'] 		         = 'green_b';
		}
	}

	if(ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS){
		$linie['view_location']=true;
		$j = 0;
		$linie['view_location_line']=array();
		$dispatch_stock_movements=$db->query("	SELECT dispatch_stock_movements.*  FROM dispatch_stock_movements
									WHERE dispatch_stock_movements.order_articles_id='".$line->f('order_articles_id')."'
									AND dispatch_stock_movements.delivery_id='".$in['delivery_id']."'");
		while($dispatch_stock_movements->next()) {
			$linie2 = array();
			if( $dispatch_stock_movements->f('dispatch_customer_id')==0 && $dispatch_stock_movements->f('stock_disp_delivery_id')==0 ){ /*base loc*/
				$customers_dep = $db->query("SELECT account_address.* FROM account_address WHERE account_address_id=1");
				$linie2['customer_name']						= ACCOUNT_COMPANY;
				$linie2['depo']                   	= '/ '.$customers_dep->f('naming');
				$linie2['order_buyer_country']      = get_country_name($customers_dep->f('country_id'));
				$linie2['order_buyer_state']        = $customers_dep->f('state');
				$linie2['order_buyer_city']         = $customers_dep->f('city');
				$linie2['order_buyer_zip']          = $customers_dep->f('zip');
				$linie2['order_buyer_address']      = $customers_dep->f('address');
			}elseif($dispatch_stock_movements->f('stock_disp_delivery_id')){
				$stock_disp_id=0;
				$stock_disp_id=$db->field("SELECT stock_disp_id FROM pim_stock_disp_delivery WHERE stock_disp_delivery_id='".$dispatch_stock_movements->f('stock_disp_delivery_id')."'");
				$pim_stock_disp=$db->query("SELECT * FROM pim_stock_disp WHERE stock_disp_id='".$stock_disp_id."'");
				/*
				if($pim_stock_disp->move_next()){
					$view_list->assign(array(
						'customer_name'=>  $pim_stock_disp->f('customer_name'),
						'depo'=>    '',
						'order_buyer_state'        => $pim_stock_disp->f('customer_state'),
						'order_buyer_city'         => $pim_stock_disp->f('customer_city'),
						'order_buyer_zip'          => $pim_stock_disp->f('customer_zip'),
						'order_buyer_address'      => $pim_stock_disp->f('customer_address'),
						'order_buyer_country'      => get_country_name($pim_stock_disp->f('customer_country_id')),
					),'view_location_line');
					if($pim_stock_disp->f('delivery_address')){
						$view_list->assign(array(
							'delivery_address'=>nl2br($pim_stock_disp->f('delivery_address')),
							'is_delivery'=>true
						),'view_location_line');
					}else{
						$view_list->assign(array(
							'delivery_address'=>'',
							'is_delivery'=>false
						),'view_location_line');
					}
				}
				*/
			}
			$linie2['quantity_loc']				= display_number($dispatch_stock_movements->f('quantity'));
			array_push($linie['view_location_line'], $linie2);
			var_dump($linie2);
		}
	}else{
		$linie['view_location']=false;
	}
	$article_ids.= $line->f('article_id').',';
	array_push($o['lines'],$linie);
	array_push($o['order_articles_id'] , $linie['order_articles_id']);
	$o['delivery_note']=$line->f('delivery_note');
	$i++;
}

if($i>0){
	$data = true;
	$article_ids=rtrim($article_ids,',');
	$o['location_dd']=build_dispach_location_dd($in['select_main_location'],$article_ids);
	$o['select_main_location'] = $in['select_main_location'] ? $in['select_main_location'] : $o['location_dd'][0]['id'];
}

$o['style']     			= ACCOUNT_NUMBER_FORMAT;
$o['is_data']				= $data;
$o['edit_note'] 			= $in['edit_note'] ? true : false;
$o['use_serial_no'] 		= $j > 0 && ALLOW_STOCK ? true : false;
$o['use_batch_no'] 			= $m > 0 && ALLOW_STOCK ? true : false;

//order data
$del_nr = $db->field("SELECT count(delivery_id) FROM pim_order_deliveries WHERE order_id='".$in['order_id']."' ");

$in['del_nr'] = $del_nr;

$order =  $db->query("SELECT pim_orders.* ,customer_contacts.firstname,customer_contacts.lastname, customer_contacts.customer_id as cust_id, customer_contacts.contact_id
      FROM pim_orders
      LEFT JOIN customer_contacts ON customer_contacts.contact_id=pim_orders.customer_id
      WHERE pim_orders.order_id='".$in['order_id']."'
     ");
if($order->f('field') == 'customer_id'){

	$o['is_buyer']=true;
	//$in['buyer_id']      	= $order->f('customer_id');
	//$o['buyer_id']      	= $order->f('customer_id');

}else{
	$o['is_buyer']=false;
}

if(!$in['contact_name']){
    	$in['contact_name']= $order->f('contact_name');
    	$o['contact_name']= $order->f('contact_name');
}

/*$tblinvoice_customer_contacts = $db->query("SELECT customer_contacts.*,pim_orders.order_id,pim_orders.customer_id,pim_orders.serial_number
	             FROM pim_orders
	             INNER JOIN customer_contactsIds ON pim_orders.customer_id = customer_contactsIds.customer_id
	             INNER JOIN customer_contacts ON customer_contactsIds.contact_id = customer_contacts.contact_id
	             WHERE pim_orders.order_id='".$in['order_id']."' AND customer_contacts.active ='1' ORDER BY customer_contacts.lastname");

while ($tblinvoice_customer_contacts->move_next()){
	$recipient = array(
		'recipient_name'  			=> $tblinvoice_customer_contacts->f('firstname').' '.$tblinvoice_customer_contacts->f('lastname'),
		'recipient_email' 			=> $tblinvoice_customer_contacts->f('email'),
		'recipient_id'    			=> $tblinvoice_customer_contacts->f('contact_id'),
	);
	array_push($o['recipients'], $recipient);
}*/

$o['do_me']						= $in['delivery_id'] ? 'order--order-edit_delivery' : 'order--order-make_delivery';
$o['style']     				= ACCOUNT_NUMBER_FORMAT;
$o['is_data']					= $data;
$o['pick_date_format']      	= pick_date_format(ACCOUNT_DATE_FORMAT);
$o['mark_as_delivered']			= $order->f('rdy_invoice') == 1 ? 1 : 0;
$o['is_add']					= true;
$o['order_id'] 					= $in['order_id'];
$o['delivery_id'] 				= $in['delivery_id'];
$o['delivery_approved']			= "1";

json_out($o);

function get_cc($in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_user = new sqldb($database_users);
	$filter =" is_admin='0' AND customers.active=1 ";
	$filter .=" AND ((customers.is_supplier='' AND customers.is_customer='') OR ( customers.is_customer='1')) " ;
	if($q){
		$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
	}
	if($in['buyer_id'] && is_numeric($in['buyer_id'])){
		$filter .=" AND customers.customer_id='".$in['buyer_id']."'";
	}

	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$db= new sqldb();

	$cust = $db->query("SELECT customers.customer_id as cust_id, name
			FROM customers
			WHERE $filter
			GROUP BY customers.customer_id
			ORDER BY name
			LIMIT 9")->getAll();

	$result = array();
	foreach ($cust as $key => $value) {
		$result[]=array(
				"id"		=> $value['cust_id'],
				"name" 		=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($value['name']))
		);
	}

	return json_out($result, $showin,$exit);

}
function get_contacts($in,$showin=true,$exit=true){

	if(!$in['buyer_id'] || ($in['buyer_id'] && !is_numeric($in['buyer_id']))){
		return array();
	}
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db2 = new sqldb();
	$db_user = new sqldb($database_users);
	$filter .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";

	if($in['buyer_id']){
		$filter .= " AND customers.customer_id='".$in['buyer_id']."'";
	}
	if($in['contact_id']){
		$filter .= " AND customer_contacts.contact_id='".$in['contact_id']."'";
	}

	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){

		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$items = array();
	$db= new sqldb();

	$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contacts.firstname,customer_contacts.lastname
		FROM customer_contacts
		INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
	 	LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
		WHERE customer_contacts.active=1 $filter ORDER BY lastname limit 9")->getAll();
	$result = array();
	foreach ($contacts as $key => $value) {
		$result[]=array(
			"recipient_id"					=> $value['contact_id'],
			"recipient_name" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($value['firstname'].' '.$value['lastname']))

		);
	}

	return json_out($result, $showin,$exit);
}
function get_addresses($in,$showin=true,$exit=true){
	if(!$in['buyer_id'] || ($in['buyer_id'] && !is_numeric($in['buyer_id']))){
		return array();
	}
	$db= new sqldb();
	$q = strtolower($in["term"]);
	$filter=" AND customer_addresses.delivery='1' ";
	if($q){
		$filter .=" AND (customer_addresses.address LIKE '%".addslashes($q)."%' OR customer_addresses.zip LIKE '%".addslashes($q)."%' OR customer_addresses.city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
	}

	if($in['buyer_id'] && is_numeric($in['buyer_id'])){
		$filter.=" AND customer_addresses.customer_id='".$in['buyer_id']."' ";
	}
	if($in['delivery_address_id'] && is_numeric($in['delivery_address_id'])){
		$filter.=" AND customer_addresses.address_id='".$in['delivery_address_id']."' ";
	}

	$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
								 FROM customer_addresses
								 LEFT JOIN country ON country.country_id=customer_addresses.country_id
								 LEFT JOIN state ON state.state_id=customer_addresses.state_id
								 WHERE 1=1 ".$filter."
								 ORDER BY customer_addresses.address_id limit 9");

	$addresses=array();
	if($address){
		while($address->next()){
		  	$a = array(
			  	'delivery_address_id'	=> $address->f('address_id'),
			  	'address_more'			=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country'))
		  	);
			array_push($addresses, $a);
		}
	}

	return json_out($addresses, $showin,$exit);

}

?>