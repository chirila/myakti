<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
global $config;
// $view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.$config['cache_version']);
global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($database_users);

if($in['order_id']){
	$order = $db->query("SELECT pim_orders.*,pim_order_articles.vat_percent,pim_orders.pdf_layout,pim_orders.pdf_logo FROM pim_orders
			LEFT JOIN pim_order_articles ON pim_orders.order_id=pim_order_articles.order_id
			WHERE pim_orders.order_id='".$in['order_id']."'");
	$email_q = $db->query("SELECT customer_contacts.firstname,customer_contacts.lastname,customer_contacts.title as title_id
            FROM pim_orders
            LEFT JOIN customer_contacts ON customer_contacts.customer_id=pim_orders.customer_id
            WHERE pim_orders.order_id='".$in['order_id']."' ")->getAll();

	$serial_number = $db->field("SELECT serial_number FROM pim_orders WHERE order_id ='".$in['order_id']."' ");
	$email_language = $db->field("SELECT email_language FROM pim_orders WHERE order_id ='".$in['order_id']."' ");
	if(!$email_language){
		$email_language = 1;
	}
	$customer_id = $db->field("SELECT customer_id from pim_orders WHERE order_id ='".$in['order_id']."' ");
	/*$order_data = $db->query("SELECT pim_order_articles.*, pim_orders_delivery.* FROM pim_order_articles
					INNER JOIN pim_orders_delivery ON pim_orders_delivery.order_articles_id=pim_order_articles.order_articles_id
					WHERE pim_orders_delivery.order_id='".$in['order_id']."' AND pim_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC");*/
	$tblinvoice_customer_contacts = $db->query("SELECT customer_contacts.*,pim_orders.order_id,pim_orders.customer_id,pim_orders.serial_number
	             FROM pim_orders
	             INNER JOIN customer_contacts ON customer_contacts.customer_id=pim_orders.customer_id
	             WHERE pim_orders.order_id='".$in['order_id']."' AND customer_contacts.active ='1'");

	$recipients=array();
	while ($tblinvoice_customer_contacts->move_next()){
		$in['recipients'][$tblinvoice_customer_contacts->f('contact_id')]=1;

		$recipient = array(
			'recipient_name'  			=> $tblinvoice_customer_contacts->f('firstname').' '.$tblinvoice_customer_contacts->f('lastname'),
			'recipient_email' 			=> $tblinvoice_customer_contacts->f('email'),
			'recipient_id'    			=> $tblinvoice_customer_contacts->f('contact_id'),
		);

		array_push($recipients, $recipient);

	}
	$contact = $db->query("SELECT firstname,lastname,title FROM customer_contacts WHERE contact_id='".$order->f('contact_id')."'");
	$title_cont =  $db->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'");
}elseif($in['p_order_id']){
	$serial_number = $db->field("SELECT serial_number FROM pim_p_orders WHERE p_order_id ='".$in['p_order_id']."' ");
	$customer_id = $db->field("SELECT customer_id from pim_p_orders WHERE p_order_id ='".$in['p_order_id']."' ");
	/*$order_data = $db->query("SELECT pim_p_order_articles.*, pim_p_orders_delivery.* FROM pim_p_order_articles
					INNER JOIN pim_p_orders_delivery ON pim_p_orders_delivery.order_articles_id=pim_p_order_articles.order_articles_id
					WHERE pim_p_orders_delivery.p_order_id='".$in['p_order_id']."' AND pim_p_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC");*/
}

$customer_data = $db->query("SELECT c_email, customer_id, name FROM customers WHERE customer_id = '".$customer_id."'");
$customer_data->next();

$tblinvoice123 = $db->query("SELECT pim_orders.order_id,pim_orders.serial_number,pim_orders.discount, pim_orders.date, pim_orders.customer_name, pim_orders.currency_type, pim_orders.amount as total,
                           pim_orders.email_language,pim_orders.identity_id
                           FROM pim_orders
                           WHERE pim_orders.order_id='".$in['order_id']."'");
$tblinvoice123->move_next();
$in['lid'] = $tblinvoice123->f('email_language');
if(!$in['lid']){
	$in['lid'] = 1;
}
$factur_date = date(ACCOUNT_DATE_FORMAT,  $order->f('date'));
$i=0;
$discount_total = 0;
$vat_value = 0;
$show_discount_total = false;
$fully_delivered = true;
$vat_array = array();
$global_disc = $order->f('discount');
$is_profi=false;
$find_if_delivered = $db->query("SELECT pim_order_articles.* FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND content='0' ORDER BY sort_order ASC");
while ($find_if_delivered->next()) {
     if(return_value($find_if_delivered->f('purchase_price'))) {
             $is_profi=true;
      }
	if(!$find_if_delivered->f('delivered')){
		$fully_delivered = false;
	}
}

$from_import=false;

$suppliers=array();
$adveo_id = $db->field("SELECT customer_id FROM customers WHERE name like 'adveo' and active='5'");
	$articles = $db->query("SELECT pim_order_articles.* FROM pim_order_articles WHERE order_id='".$in['order_id']."' ORDER BY sort_order ASC");
	$margin=0;
	$line_margin=0;
	$o['lines']=array();
	while ($articles->move_next())
	{
		$supplier_id=$db->field("SELECT supplier_id FROM pim_articles WHERE article_id='".$articles->f('article_id')."'");
        if($supplier_id && !in_array($supplier_id,$suppliers)) {
		   array_push($suppliers,$supplier_id);
        }

		if(!$show_discount_total && $articles->f('discount') !=0){
			$show_discount_total = true;
		}
		if($articles->f('from_import')){
			$from_import=true;
		}
		
		$discount_line = $articles->f('discount');
		if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
			$discount_line = 0;
		}
		if($in['apply_discount'] < 2){
			$global_disc = 0;
		}
		$q = @($articles->f('quantity') * $articles->f('packing') / $articles->f('sale_unit'));

		$price_line = $articles->f('price') - ($articles->f('price') * $discount_line /100);
		$discount_total += $price_line * $global_disc / 100 * $q;

		$vat_total += ($price_line - $price_line * $global_disc /100) *$articles->f('vat_percent') / 100 * $q;
		$line_total=$price_line * $q;

		$vat_value_line =($price_line - $price_line * $global_disc /100) *$articles->f('vat_percent') / 100 * $q;
		$vat_percent[$articles->f('vat_percent')] += $vat_value_line;

		$sub_t[$articles->f('vat_percent')] += $line_total;
		$sub_discount[$articles->f('vat_percent')] += ($line_total * $global_disc /100);

		$total += $line_total;
       if(!$articles->f('tax_id')){
       	   $total_no_tax += $line_total;
           
            if($global_disc){
        	  $line_total=$line_total-$line_total*$global_disc /100;
        	  $line_margin=$line_total-($articles->f('purchase_price')*$q);
        	  $margin+=$line_margin;
           }else{
           	 $line_margin=$line_total-($articles->f('purchase_price')*$q);
           	 $margin+=$line_margin;
           }
        }

		$vat_array[$articles->f('vat_percent')] += ($price_line - $price_line * $global_disc /100) *$articles->f('vat_percent') / 100 * $q;
		
	}

$account_company = ACCOUNT_COMPANY;
$account_company = htmlentities($account_company,ENT_COMPAT | ENT_HTML401,'UTF-8');
$currency = get_commission_type_list($order->f('currency_type'));
if( $in['delivery_id']){
	$message_data=get_sys_message('orddelmess',$order->f('email_language'));
}else{
	$message_data=get_sys_message('ordmess',$order->f('email_language'));
}

$identity_logo ='';
$logo = 'images/no-logo.png';
if($tblinvoice123->f('identity_id')){
	$identity_logo = $db->field("SELECT company_logo FROM multiple_identity WHERE identity_id='".$tblinvoice123->f('identity_id')."'");
}
if($identity_logo){
	$logo = $identity_logo;
}else{
	$print_logo = ACCOUNT_LOGO_ORDER;
	if ($print_logo) {
        $logo = $print_logo;
    }
}
if(defined('ACCOUNT_DATE_FORMAT') && ACCOUNT_DATE_FORMAT!=''){
	$date_format = ACCOUNT_DATE_FORMAT;
}else{
	$date_format ='m/d/Y';
}

$subject=$message_data['subject'];
$subject=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $subject );
$subject=str_replace('[!SERIAL_NUMBER!]',$tblinvoice123->f('serial_number'), $subject);
$subject=str_replace('[!ORDER_DATE!]',$factur_date, $subject);
$subject=str_replace('[!CUSTOMER!]',$tblinvoice123->f('customer_name'), $subject);
$subject=str_replace('[!DISCOUNT!]',$tblinvoice123->f('discount').'%', $subject);
$subject=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($discount_total),$currency), $subject);
$subject=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),$currency), $subject);
$subject=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($tblinvoice123->f('total')),$currency), $subject);

$body=$message_data['text'];
$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].$logo."\" alt=\"\">",$body);
$body=str_replace('[!ACCOUNT_COMPANY!]',$account_company, $body );
$body=str_replace('[!SERIAL_NUMBER!]',$tblinvoice123->f('serial_number'), $body);
$body=str_replace('[!ORDER_DATE!]',$factur_date, $body);
$body=str_replace('[!CUSTOMER!]',$tblinvoice123->f('customer_name'), $body);
$body=str_replace('[!DISCOUNT!]',$tblinvoice123->f('discount').'%', $body);
$body=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($discount_total),$currency), $body);
$body=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),$currency), $body);
$body=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($tblinvoice123->f('total')),$currency), $body);


$body=str_replace('[!CONTACT_FIRST_NAME!]',"".utf8_decode($contact->f('firstname'))."",$body);
$body=str_replace('[!CONTACT_LAST_NAME!]',"".utf8_decode($contact->f('lastname'))."",$body);
$body=str_replace('[!SALUTATION!]',"".$title_cont."",$body);
$body=str_replace('[!DATE!]',"".date($date_format, $order->f('date'))."",$body);
$body=str_replace('[!SUBJECT!]',"".$contact->f('del_note')."",$body);
$body=str_replace('[!DUE_DATE!]',"".date($date_format, $order->f('del_date'))."",$body);
$body=str_replace('[!YOUR_REFERENCE!]',"".$order->f('your_ref')."",$body);


if($message_data['use_html']){
   //$body=str_replace('[!SIGNATURE!]',get_signature(), $body);
	$body=str_replace('[!SIGNATURE!]',get_signature_new(), $body);
}else{
	 $body=str_replace('[!SIGNATURE!]','', $body);
}

$c_email = $db->field("SELECT invoice_email FROM customers WHERE customer_id='".$order->f('customer_id')."' ");
$items = array();
$items2 = array();
$contact_email='';
$customer_exist = false;
$invoice_email_type = $db->field("SELECT invoice_email_type FROM customers WHERE customer_id='".$order->f('customer_id')."' ");
if($order->f('customer_id')){
	$filter .= " AND customers.customer_id='".$order->f('customer_id')."'";
}
if($order->f('contact_id')){
	$filter .= " AND customer_contacts.contact_id='".$order->f('contact_id')."'";
}

if($order->f('customer_id') && $order->f('contact_id')){
	$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 AND customers.customer_id='".$order->f('customer_id')."' ORDER BY lastname ")->getAll();
}else{
	$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.customer_id = customer_contactsIds.customer_id AND customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1  AND customers.customer_id='".$order->f('customer_id')."'  ORDER BY lastname ")->getAll();	
}
if($order->f('customer_id') && !$order->f('contact_id')){
	$customer = $db->query("SELECT customers.name as name,customers.c_email as email
					FROM customers
					WHERE 1=1 $filter ");
	$customer_exist = true;


}else{
	$customer = $db->query("SELECT customers.name as name,customers.c_email as email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 $filter ");
}
$items2 = array(
	'id' => $customer->f('email'), 
	'label' => $customer->f('name'),
	'value' => $customer->f('name').' - '.$customer->f('email'),
);
if($invoice_email_type==1 && $order->f('customer_id')){
	$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1  AND customers.customer_id='".$order->f('customer_id')."'  ORDER BY lastname ")->getAll();
}
$contacts_exist = false;
foreach ($contacts as $key => $value) {

	if($value['name']){
		$name = $value['firstname'].' '.$value['lastname'].' > '.$value['name'];
	}else{
		$name = $value['firstname'].' '.$value['lastname'];
	}
	$items[] = array(
		'id' => $value['email'], 
		'label' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
		'value' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)).' - '.$value['email'],
	);
	$contacts_exist = true;
	if($value['contact_id']==$order->f('contact_id')){
		$contact_email=$value['email'];
		}
	}

	$added = false;
	foreach ($items as $key => $value) {
		if($value['id'] == $items2['id']){
			$added = true;
		}
	}
	if(!$added){
		array_push($items, $items2);
	}
	if($invoice_email_type==1 && $order->f('customer_id')){
		array_push($items, $items2);
	}else{	
		if($c_email){
			array_push($items, array(
				'id' => $c_email, 
				'label' =>$c_email,
				'value' => $c_email,
			));
		}
	}

$recipients_ids = array();
/*if($invoice_email_type==1){
	if(!$order->f('contact_id')){
		array_push($recipients_ids, $items2['id']);
	}else{
		array_push($recipients_ids, $contact_email);
	}
}else{
	if($c_email){
		$recipients_ids = array();
		array_push($recipients_ids,$c_email);
	}
}*/

if($order->f('contact_id')){
	if($order->f('customer_id')){
		$contact_email = $db->field("SELECT email FROM customer_contactsIds WHERE contact_id='".$order->f('contact_id')."' AND customer_id = '".$order->f('customer_id')."'");
	}else{
		$contact_email = $db->field("SELECT email FROM customer_contactsIds WHERE contact_id='".$order->f('contact_id')."'");
	}
	if($contact_email){
		$cc_email = $contact_email;
	}else{
		$customer_email = $db->field("SELECT c_email FROM customers WHERE customer_id='".$order->f('customer_id')."'");
		if($customer_email){
			$cc_email = $customer_email;
		}
	}
}else{
	$customer_email = $db->field("SELECT c_email FROM customers WHERE customer_id='".$order->f('customer_id')."'");
	if($customer_email){
		$cc_email = $customer_email;
	}
}

if($cc_email){
	$recipients_ids = $cc_email;
}

//attached files
$is_file = false;
$f = $db->query("SELECT * FROM attached_files WHERE type='3'");
$files=array();
while ($f->next()) {
	$path = $f->f('path');
	$p = substr($f->f('path'), -1);
	if($p != '/'){
		$path = $f->f('path').'/';
	}
	$file = array(
		'file'		=> $f->f('name'),
		'checked'	=> $f->f('default') == 1 ? true : false,
		'file_id'	=> $f->f('file_id'),
		'path'		=> $path,
		);
	array_push($files, $file);
	$is_file = true;
}
$include_pdf = true;
//$account_manager_email = $db_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."'");
$account_manager_email = $db_users->field("SELECT email FROM users WHERE user_id= :user_id",['user_id'=>$_SESSION['u_id']]);
$account_user_email = $db_users->field("SELECT email FROM users WHERE user_id='".$order->f('acc_manager_id')."'");
$email =array(
	'customer_id'				=> $customer_data->f('customer_id'),
	'c_email'					=> $customer_data->f('c_email'),
	'is_c_email'				=> $customer_data->f('c_email') ? true : false,
	'copy_acc'					=> $customer_data->f('c_email') ? true : false,
	'customer_name'				=> $customer_data->f('name'),
	'language_dd'	    		=> build_pdf_language_dd($in['lid'],1),
	'serial_number'	    		=> $serial_number,
	'order_id'					=> $in['order_id'],
	'do_next'					=> $in['order_id'] ? 'order--order-notice' : 'order-p_order-order-notice' ,
	'pdf_version_text'			=> $in['order_id'] ? gm('Include a pdf version of the delivery') : gm('Include a pdf version of the entry') ,
	//'recipients'				=> $recipients,
	'recipients_auto'			=> $items,
	'recipients'				=> $recipients_ids,
	'is_file'					=> $is_file,
	'files'						=> $files,
	'include_pdf'				=> $include_pdf,
	'lid'						=> $email_language,
	'email_language'			=> $email_language,
	'acc_manager_id'			=> $order->f('acc_manager_id'),
	'e_subject'					=> $subject,
	'e_message'					=> $in['e_message'] ? $in['e_message'] : ($body),
	'use_html'					=> $message_data['use_html'],
	'delivery_id'				=> $include_pdf ? $in['delivery_id'] : '',
	'user_acc_email'			=> $account_user_email,
	'user_loged_email'			=> $account_manager_email,
);

json_out($email);
?>