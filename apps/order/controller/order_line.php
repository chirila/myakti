<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}

$o=array('cc'=>array(),'recipients'=>array(),'lines'=>array(),'order_articles_id' =>array());

if(ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS){
	$o['view_location']=true;
}else{
	$o['view_location']=false;
}

$now 		= time();
$today  	= date('d', $now);
$month      = date('n', $now);
$year       = date('Y', $now);
$day_end   = mktime(23,59,59,$month,$today,$year);

if($in['order_id'] && !$in['delivery_id']){
	$o['order_id'] =$in['order_id'];
	$o['is_add'] = true;
	$o['page_title'] = gm('Add Delivery');
	$order =  $db->query("SELECT pim_orders.* FROM pim_orders WHERE pim_orders.order_id='".$in['order_id']."'");
	$order->move_next();
	$o['delivery_note'] = $in['delivery_note'];
	if($order->f('field') == 'customer_id'){
		$customer_id = $order->f('customer_id');
		$c_details = $db->query("SELECT * FROM customers WHERE customer_id ='".$customer_id."' ");
		$o['delivery_note'] = $c_details->f('deliv_disp_note');
		$in['buyer_id']=$order->f('customer_id');
		$o['buyer_id']=$order->f('customer_id');
		$o['buyer_name']=$order->f('customer_name');
		$o['cc']	= get_cc($in,true,false);
	}

	if($order->f('contact_id')){
		$o['contact_id']=$order->f('contact_id');
	}

	$allow_packing = $order->f('use_package');
	$allow_sale_unit = $order->f('use_sale_unit');

	$o['allow_article_packing']       = ALLOW_ARTICLE_PACKING ? $allow_packing : false;
	$o['allow_article_sale_unit']     = ALLOW_ARTICLE_PACKING ? $allow_sale_unit : false;


	if($order->f('delivery_address')){
		$o['delivery_address']=$order->gf('delivery_address');
		$o['delivery_address_id']=$order->gf('delivery_address_id');
	}else{
		$o['delivery_address']=$order->gf('address_info');
		$o['delivery_address_id']=$order->gf('main_address_id');
	}
	$customers_dep = $db->query("SELECT  dispatch_stock_address.* FROM  dispatch_stock_address order by is_default DESC")->getAll();
	$un_query = $db->query("SELECT  dispatch_stock.*,pim_stock_disp.to_customer_id,pim_stock_disp.to_naming,pim_stock_disp.to_address_id,pim_stock_disp.to_address,pim_stock_disp.to_zip,pim_stock_disp.to_city,pim_stock_disp.to_country
           FROM  dispatch_stock
           INNER JOIN pim_stock_disp ON pim_stock_disp.to_customer_id=dispatch_stock.customer_id and pim_stock_disp.to_address_id=dispatch_stock.address_id
           WHERE dispatch_stock.customer_id!=0
           GROUp BY dispatch_stock.customer_id,dispatch_stock.address_id")->getAll();

	$i = 0;
	$j = 0;
	$k = 0;
	$o['delivery_addresses']=get_delivery_addresses($order->f('customer_id'),$o['delivery_address_id']);
	$o['location_dd']=build_dispach_location_dd($in['select_main_location'],$article_ids);
	$o['select_main_location'] = $in['select_main_location'] ? $in['select_main_location'] : $o['location_dd'][0]['id'];
	$line = $db->query("SELECT * FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND delivered='0' AND content='0' AND tax_id='0' AND has_variants='0' ORDER BY sort_order ASC ");
	$article_ids="";
	while ($line->next()) {
		$delivered = 0;
		$left = 0;
		$delivered = $db->field("SELECT SUM(quantity) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$line->f('order_articles_id')."' ");

		$article_data = $db->query("SELECT * FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");

		$use_serial_no = $article_data->f('use_serial_no'); 
		if($use_serial_no==1){
			$j++;
		}

		$use_batch_no = $article_data->f('use_batch_no'); 
		if($use_batch_no==1){
			$k++;
		}
		$is_service = 0;
		$is_combined = 0;
		$is_service = $article_data->f('is_service'); 
		$is_combined = $article_data->f('use_combined'); 
		$hide_stock = $article_data->f('hide_stock'); 
		$stock = $article_data->f('stock'); 
		$left = $line->f('quantity') - $delivered;

		//$packing = $db->field("SELECT packing FROM pim_articles WHERE article_id='".$line->f('article_id')."' ");
		//$packing = $db->field("SELECT packing FROM pim_order_articles WHERE order_articles_id='".$line->f('order_articles_id')."' ");
		$packing = $line->f('packing');
		if(!$packing){
			$packing=1;
		}
		//$sale_unit = $db->field("SELECT sale_unit FROM pim_articles WHERE article_id='".$line->f('article_id')."' ");
		//$sale_unit = $db->field("SELECT sale_unit FROM pim_order_articles WHERE order_articles_id='".$line->f('order_articles_id')."' ");
		$sale_unit = $line->f('sale_unit');
		if(!$sale_unit){
			$sale_unit=1;
		}

		$array_disp = array();
		$nr = 0;
		$new_qty=0;
		foreach ($customers_dep as $key => $value) {
			if(defined('STOCK_MULTIPLE_LOCATIONS') && STOCK_MULTIPLE_LOCATIONS == 1 ){
				 $qty=$db->field("SELECT dispatch_stock.stock FROM dispatch_stock WHERE article_id = '".$line->f('article_id')."' and address_id='".$value['address_id']."' and main_address=1");
			}else{
				$qty=$stock;
			}
		 
		  if(!$key){
		  	$quantity_delivered=$left;
		  }else{
		  	$quantity_delivered=0;
		  }

		 // console::log($left*$packing/$sale_unit,$qty);
		  array_push($array_disp, array(
         'quantity'                  => display_number($qty),
         'qty'                  		 => $qty,
         'order_qty'                 => $left,
         'order_quantity'            => display_number($left),
         //'quantity_delivered'        => display_number($view_quantity),
         'quantity_delivered'        => display_number($quantity_delivered),
         'order_articles_id'         => $line->f('order_articles_id'),
         'customer_name'           	 =>  $value['naming'],
         'customer_id'               =>  0,
         'address_id'                =>  $value['address_id'],
         'order_buyer_country'       => get_country_name($value['country_id']),
         'order_buyer_state'         => $value['state'],
         'order_buyer_city'          => $value['city'],
         'order_buyer_zip'           => $value['zip'],
         'order_buyer_address'       => $value['address'],
         'cust_addr_id'				 => '0-'.$value['address_id'],
         'is_article' 				 => !$is_service,
         'is_not_combined' 			=> $is_combined? false:true,
         'is_error'					 =>(!$is_combined && !$is_service && $nr == 0 && ($left*$packing/$sale_unit > $qty) )? true : false,
      ));
      if($nr==0 && ALLOW_STOCK==0){
      	$new_qty=$left;
      	/*$new_qty=$qty;*/
      }
		  $nr++;
		}
		foreach ($un_query as $key => $value) {

		  $qty=$db->field("SELECT dispatch_stock.stock FROM dispatch_stock WHERE article_id = '".$line->f('article_id')."' and address_id='".$value['to_address_id']."' and customer_id='".$value['customer_id']."'");
		  array_push($array_disp, array(
				'quantity'								 	=>  display_number($qty),
				'qty'                  		 	=> $qty,
				'quantity_delivered'        => display_number($in['quantity_delivered']),
				'order_articles_id'					=> $line->f('order_articles_id'),
				'customer_name'						 	=> $value['to_naming'],
				'customer_id'   						=> $value['to_customer_id'],
				'address_id'    						=> $value['to_address_id'],
				'type'          						=> 2,
				'order_buyer_country'     	=> $value['to_country'],
				'order_buyer_city'         	=> $value['to_city'],
				'order_buyer_zip'          	=> $value['to_zip'],
				'order_buyer_address'      	=> $value['to_address'],
				'cust_addr_id'				=> $value['customer_id'].'-'.$value['address_id'],
				'is_error'					=> false,
		  ));

		}

		$first_location_qty =$array_disp[0]['quantity'];
		//console::log($left*$packing/$sale_unit,$first_location_qty);

		$batchOk = false;
		$count_selected_b_n =0;
		$selected_b_n = array();
		$location_batch=explode("-",$in['select_main_location']);
		//check if the article has only one batch available in stock, but not out of date
		//$check_article = $db->field("SELECT COUNT( * ) FROM  `batches` WHERE article_id= '".$line->f('article_id')."'  AND in_stock !=0 ");
		$check_article = $db->field("SELECT COUNT( * ) FROM  `batches` 
			LEFT JOIN batches_dispatch_stock ON batches.id = batches_dispatch_stock.batch_id
			WHERE batches.article_id= '".$line->f('article_id')."'  AND batches_dispatch_stock.quantity !=0 ");

		if($check_article=='1'){
			//$single_batch=$db->query("SELECT id, date_exp, in_stock, batch_number FROM  `batches` WHERE article_id= '".$line->f('article_id')."'  AND in_stock !=0 ");
			$single_batch=$db->query("SELECT id, date_exp, in_stock, batch_number, batches_dispatch_stock.address_id FROM  `batches`
			LEFT JOIN batches_dispatch_stock ON batches.id = batches_dispatch_stock.batch_id
			 WHERE batches.article_id= '".$line->f('article_id')."'  AND batches_dispatch_stock.quantity !=0 ");

			if($single_batch->move_next()){
				//console::log($single_batch->f('date_exp')>$day_end ,return_value($single_batch->f('in_stock')) , $left,$packing,$sale_unit );
				//if($single_batch->f('date_exp')>$day_end && return_value($single_batch->f('in_stock')) > $left*$packing/$sale_unit  ){
				if($single_batch->f('date_exp')>$day_end && $single_batch->f('in_stock') > $left*$packing/$sale_unit ){
					
					array_push($selected_b_n, array(
							'article_id'			=> $line->f('article_id'),
							'b_n_id'				=> $single_batch->f('id'),
							'b_n_q_selected'		=> $left,
							'batch_checked'			=> "active",
							'batch_no_checked'		=> true,
							'batch_number'			=> $single_batch->f('batch_number'),
							'date_exp'				=> date(ACCOUNT_DATE_FORMAT,$single_batch->f('date_exp')),
							'in_stock'				=> $single_batch->f('in_stock'),
							'is_error'				=> false,
							'order_articles_id'		=> $line->f('order_articles_id'),
							'address_id' 			=> $single_batch->f('address_id')

						));
					$batchOk = true;
					$count_selected_b_n = $left;
				}
			}
			
		}

		array_push($o['lines'], array(
			'article'								=> stripslashes(htmlspecialchars_decode($line->f('article'),ENT_QUOTES)),
			'code'									=> $line->f('article_code'),
			'article_id'							=> $line->f('article_id'),			
      		//'quantity_value'						=> $left > $new_qty ? $new_qty : $left,
			//'quantity'							=> $hide_stock ? display_number($left) : ($left > $new_qty ? display_number($new_qty) : display_number($left)),
			'quantity_value'						=> $left,
      		'quantity'								=> display_number($left),
			'hide_stock'							=> ($hide_stock ==1 || !$line->f('article_id'))? 1:0 ,
			'quantity_value2'						=> $left,
			'quantity2'								=> display_number($left),
			'order_articles_id' 					=> $line->f('order_articles_id'),
			'show_stock'           					=> ($hide_stock == 0 && $line->f('article_id'))? true : false,
			'use_serial_no_art'						=> $use_serial_no == 1 ? true : false,
			'count_selected_s_n'					=> 0,
			'selected_s_n'							=> '',
			'use_batch_no_art'						=> $use_batch_no == 1 ? true : false,
			'count_selected_b_n'					=> $count_selected_b_n,
			'allow_article_packing'       			=> ALLOW_ARTICLE_PACKING ? $allow_packing : false,
			'allow_article_sale_unit'     			=> ALLOW_ARTICLE_PACKING ? $allow_sale_unit: false,
			'packing'								=> $line->f('article_id')? remove_zero_decimals($packing):'1',
			'sale_unit'								=> $line->f('article_id')? $sale_unit:'1',
			'view_location'							=> ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS ? true : false,
			'disp_addres_info' 						=> $array_disp,
			//'is_error'							=> $hide_stock ? false : ($left > $new_qty ? true : false),
			'is_error'								=> (!$is_combined && !$hide_stock && !$is_service  && $line->f('article_id') && ALLOW_STOCK) ? ($left*$packing/$sale_unit > return_value($first_location_qty) ? true : false) : false,
			'selected_b_n'							=> $selected_b_n,
			'batchOk'								=> $batchOk,
			'is_article' 							=> !$is_service,
			'is_not_combined' 						=> $is_combined? false:true,
		));

		array_push($o['order_articles_id'] , $line->f('order_articles_id'));
		$i++;
	}

	$o['use_serial_no'] 	= $j > 0 && ALLOW_STOCK ? true : false;
	$o['use_batch_no'] 		= $k > 0 && ALLOW_STOCK ? true : false;
	$o['is_custom_address'] = false;


}elseif($in['delivery_id']){
	$o['order_id'] = $in['order_id'];
	$o['delivery_id'] = $in['delivery_id'];
	$o['page_title'] = gm('Edit Delivery');
	$order =  $db->query("SELECT pim_orders.* FROM pim_orders WHERE pim_orders.order_id='".$in['order_id']."'");
	$order->next();

	$allow_packing = $order->f('use_package');
	$allow_sale_unit = $order->f('use_sale_unit');

	$in['edit_note']=1;
  $del = $db->query("SELECT pim_order_deliveries.*,pim_orders_delivery.delivery_note
  	FROM  pim_order_deliveries
  	INNER JOIN pim_orders_delivery ON pim_orders_delivery.delivery_id = pim_order_deliveries.delivery_id
  	WHERE pim_order_deliveries.order_id='".$in['order_id']."' AND pim_order_deliveries.delivery_id='".$in['delivery_id']."'");

  $del->next();

  	$in['buyer_id']=$del->f('buyer_id') ? $del->f('buyer_id') : $order->f('customer_id');
	$o['buyer_id']=$del->f('buyer_id') ? $del->f('buyer_id') : $order->f('customer_id');
	$o['buyer_name']=$del->f('buyer_id') ? stripslashes($del->f('buyer_name')) : stripslashes($order->f('customer_name'));
	$o['cc']	= get_cc($in,true,false);
  /*$o['date_del_line_h']  	= $del->gf('date');
  $o['date_del_line']   	= date(ACCOUNT_DATE_FORMAT,$del->gf('date'));
  $o['contact_name'] 			= $del->gf('contact_name');
  $o['contact_id'] 				= $del->gf('contact_id');*/

  /*$view_list->assign(array(
		'hour_dd'      => hourmin('hour','minute','pm', $db->gf('hour'),$db->gf('minute'),$db->gf('pm')),*/
	$o['delivery_note']	= $del->gf('delivery_note');
	$o['is_add']       	=  false;
		// 'is_edit'      =>  1,
	$o['order_info']   	= array();

###### view order line ######
	/*$delis = $db->query("SELECT pim_order_deliveries.*,pim_orders_delivery.delivery_note
      FROM  pim_order_deliveries
      INNER JOIN pim_orders_delivery ON pim_orders_delivery.delivery_id = pim_order_deliveries.delivery_id
      WHERE pim_order_deliveries.order_id='".$in['order_id']."' AND pim_order_deliveries.delivery_id='".$in['delivery_id']."'");
*/

	$o['delivery_date'] 					= date(ACCOUNT_DATE_FORMAT,$del->gf('date'));
	$o['delivery_date_js'] 					= $del->gf('date')*1000;
	$o['hour']          					= $del->f('hour')?$del->f('hour'):'';
	$o['minute']        					= $del->f('minute')?$del->f('minute'):'';
	$o['pm']            					= $del->f('pm')?$del->f('pm'):'';
	$o['contact_name']  					= $del->f('contact_name')?$del->f('contact_name'):'-';
	$o['contact_id']  						= $del->f('contact_id')?$del->f('contact_id'):'';
	$o['delivery_address_txt']  			= nl2br($del->f('delivery_address'));
	$o['delivery_address_id']				=$order->gf('delivery_address_id') && !$del->f('is_custom_address') ? $order->gf('delivery_address_id'):(!$del->f('is_custom_address') ? $order->gf('main_address_id') : '');
	$o['delivery_addresses']				=get_delivery_addresses($order->f('customer_id'),$o['delivery_address_id']);
	$o['is_custom_address']		= $del->f('is_custom_address') ? true : false;
	
	$o['lines'] 		= array();
	$i = 0;
	$line = $db->query("SELECT pim_order_articles.*, pim_orders_delivery.* FROM pim_order_articles
					INNER JOIN pim_orders_delivery ON pim_orders_delivery.order_articles_id=pim_order_articles.order_articles_id
					WHERE pim_orders_delivery.order_id='".$in['order_id']."' AND pim_orders_delivery.delivery_id='".$in['delivery_id']."' ORDER BY sort_order ASC");
	while ($line->next()) {
		$delivered = 0;
		$left = 0;
		$delivered = $db->field("SELECT SUM(quantity) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND order_articles_id='".$line->f('order_articles_id')."' ");
		$left = $line->f('quantity');

    $packing = $line->f('packing');
    $sale_unit = $line->f('sale_unit');

		$linie = array(
			'code'					=> $line->f('article_code'),
			'article'							=> $line->f('article'),
			'quantity'						=> display_number($left),
			'order_articles_id' 	=> $line->f('order_articles_id'),
      'packing'       			=> remove_zero_decimals($packing),
      'sale_unit'       		=> $sale_unit,
		);

    //article serial numbers
    $use_serial_nr = $db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
    if( $use_serial_nr == 1 ){
      $article_s_n_data = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$line->f('article_id')."' AND delivery_id = '".$in['delivery_id']."' ");
      $k = 0;
      $selected_s_n ='';
      while($article_s_n_data->next()){
          if($k==0){
            $selected_s_n .= $article_s_n_data->f('id');
          }else{
            $selected_s_n .= ','.$article_s_n_data->f('id');
          }
        $k++;
      }
      if($k>0){
        $use_serial_no = true;
        $linie['use_serial_no_art']     = true;
      }
      $count_selected_s_n = $k;
      $linie['selected_s_n']          = $selected_s_n;
      $linie['count_selected_s_n']    = $count_selected_s_n;
    }

    // article batch numbers
    $use_batch_nr = $db->field("SELECT use_batch_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
    if( $use_batch_nr == 1 ){
      $article_b_n_data = $db->query("SELECT * FROM batches WHERE article_id = '".$line->f('article_id')."' AND delivery_id = '".$in['delivery_id']."' ");
      $m = 0;
      while($article_b_n_data->next()){
        $m++;
      }
      if($m > 0){
        $use_batch_no = true;
        $linie['use_batch_no_art']     = true;
      }
    }

	  if(ALLOW_STOCK && STOCK_MULTIPLE_LOCATIONS){
      $linie['view_location']=true;
      $j = 0;

      $locations=$db->field("SELECT stock_movements.location_info  FROM stock_movements
        WHERE stock_movements.article_id='".$line->f('article_id')."' AND stock_movements.delivery_id='".$in['delivery_id']."'");
      $locations=unserialize($locations);
      $linie['locations']=array();
      if(!empty($locations)){
	      foreach ($locations as $customer => $value) {
					if(return_value($value)!='0.00'){
	          $customeres=explode('-', $customer);
	          if($customeres[0]==0){
	            $customer_info=$db->query("SELECT dispatch_stock_address.* FROM dispatch_stock_address WHERE address_id='".$customeres['1']."'");
	          }else{
	            $customer_info=$db->query("SELECT  dispatch_stock.*,pim_stock_disp.to_naming,pim_stock_disp.to_address,pim_stock_disp.to_zip,pim_stock_disp.to_city,pim_stock_disp.to_country
	         		FROM  dispatch_stock
	         		INNER JOIN pim_stock_disp ON pim_stock_disp.to_customer_id=dispatch_stock.customer_id
	         		WHERE dispatch_stock.customer_id='".$customeres['0']."' and dispatch_stock.address_id='".$customeres['1']."' ");
	          }
						array_push($linie['locations'],array(
							'quantity_loc'=> remove_zero_decimals($value),
							'depo'   => $customer_info->f('naming')?$customer_info->f('naming'):$customer_info->f('to_naming'),
							'address'   =>$customer_info->f('address')?$customer_info->f('address'):$customer_info->f('to_address'),
							'city'     =>$customer_info->f('city')?$customer_info->f('city'):$customer_info->f('to_city'),
							'zip'   =>$customer_info->f('zip')?$customer_info->f('zip'):$customer_info->f('to_zip'),
							'country'   =>get_country_name($customer_info->f('country_id'))?get_country_name($customer_info->f('country_id')):get_country_name($customer_info->f('to)country_id')),
						));
	        }
	      }
	    }
      $j++;
	  }else{
	  	$linie['view_location']=false;
	  }

		array_push($o['lines'],$linie);
		array_push($o['order_articles_id'] , $linie['order_articles_id']);
		$o['order_info']['delivery_note']=$line->f('delivery_note');
		$i++;

	}
	if($i>0){
		$data = true;
	}

  $o['allow_article_packing']       = $allow_packing;
  $o['allow_article_sale_unit']     = $allow_sale_unit;
  $o['use_serial_no']         			= $use_serial_no;
  $o['use_batch_no']         				= $use_batch_no;

###### view order line ######

  if($del->f('delivery_address')){
   	$o['delivery_address']=$del->f('delivery_address');
  }
  else{
  	$order =  $db->query("SELECT pim_orders.* FROM pim_orders WHERE pim_orders.order_id='".$in['order_id']."'");

		if($order->f('delivery_address')){
			$o['delivery_address']=$order->gf('delivery_address');
		}else{
			$o['delivery_address']=$order->f('address_info');
		}
  }

}
$tblinvoice_customer_contacts = $db->query("SELECT customer_contacts.*,pim_orders.order_id,pim_orders.customer_id,pim_orders.serial_number
	             FROM pim_orders
	             INNER JOIN customer_contactsIds ON pim_orders.customer_id = customer_contactsIds.customer_id
	             INNER JOIN customer_contacts ON customer_contactsIds.contact_id = customer_contacts.contact_id
	             WHERE pim_orders.order_id='".$in['order_id']."' AND customer_contacts.active ='1' ORDER BY customer_contacts.lastname");

while ($tblinvoice_customer_contacts->move_next()){
	$recipient = array(
		'recipient_name'  			=> $tblinvoice_customer_contacts->f('firstname').' '.$tblinvoice_customer_contacts->f('lastname'),
		'recipient_email' 			=> $tblinvoice_customer_contacts->f('email'),
		'recipient_id'    			=> $tblinvoice_customer_contacts->f('contact_id'),
	);
	array_push($o['recipients'], $recipient);
}

json_out($o);

function get_cc($in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_user = new sqldb($database_users);
	$filter =" is_admin='0' AND customers.active=1 ";
	$filter .=" AND ((customers.is_supplier='' AND customers.is_customer='') OR ( customers.is_customer='1')) " ;
	if($q){
		$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
	}
	if($in['buyer_id'] && is_numeric($in['buyer_id'])){
		$filter .=" AND customers.customer_id='".$in['buyer_id']."'";
	}

	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$db= new sqldb();

	$cust = $db->query("SELECT customers.customer_id as cust_id, name
			FROM customers
			WHERE $filter
			GROUP BY customers.customer_id			
			ORDER BY name
			LIMIT 9")->getAll();

	$result = array();
	foreach ($cust as $key => $value) {
		$result[]=array(
				"id"		=> $value['cust_id'],
				"name" 		=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($value['name']))
		);
	}
	
	return json_out($result, $showin,$exit);

}
function get_contacts($in,$showin=true,$exit=true){

	if(!$in['buyer_id'] || ($in['buyer_id'] && !is_numeric($in['buyer_id']))){
		return array();
	}
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db2 = new sqldb();
	$db_user = new sqldb($database_users);
	$filter .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";

	if($in['buyer_id']){
		$filter .= " AND customers.customer_id='".$in['buyer_id']."'";
	}
	if($in['contact_id']){
		$filter .= " AND customer_contacts.contact_id='".$in['contact_id']."'";
	}

	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){

		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$items = array();
	$db= new sqldb();

	$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contacts.firstname,customer_contacts.lastname
		FROM customer_contacts
		INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
	 	LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
		WHERE customer_contacts.active=1 $filter ORDER BY lastname limit 9")->getAll();
	$result = array();
	foreach ($contacts as $key => $value) {
		$result[]=array(
			"recipient_id"					=> $value['contact_id'],
			"recipient_name" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($value['firstname'].' '.$value['lastname']))

		);
	}
	
	return json_out($result, $showin,$exit);
}
function get_addresses($in,$showin=true,$exit=true){
	if(!$in['buyer_id'] || ($in['buyer_id'] && !is_numeric($in['buyer_id']))){
		return array();
	}
	$db= new sqldb();
	$q = strtolower($in["term"]);
	$filter=" AND customer_addresses.delivery='1' ";
	if($q){
		$filter .=" AND (customer_addresses.address LIKE '%".addslashes($q)."%' OR customer_addresses.zip LIKE '%".addslashes($q)."%' OR customer_addresses.city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
	}

	if($in['buyer_id'] && is_numeric($in['buyer_id'])){
		$filter.=" AND customer_addresses.customer_id='".$in['buyer_id']."' ";
	}
	if($in['delivery_address_id'] && is_numeric($in['delivery_address_id'])){
		$filter.=" AND customer_addresses.address_id='".$in['delivery_address_id']."' ";
	}
	
	$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
								 FROM customer_addresses
								 LEFT JOIN country ON country.country_id=customer_addresses.country_id
								 LEFT JOIN state ON state.state_id=customer_addresses.state_id
								 WHERE 1=1 ".$filter."
								 ORDER BY customer_addresses.address_id limit 9");

	$addresses=array();
	if($address){
		while($address->next()){
		  	$a = array(
			  	'delivery_address_id'	=> $address->f('address_id'),
			  	'address_more'			=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country'))
		  	);
			array_push($addresses, $a);
		}
	}
	
	return json_out($addresses, $showin,$exit);

}