<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$db2 = new sqldb();
$db3 = new sqldb();
$db4 = new sqldb();

global $database_config;
$database = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);
$dbu_users =  new sqldb($database);

if(!$in['order_id'])
{
	page_redirect('index.php?do=order-orders');
}

$order = $db->field("SELECT order_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");

if(!$order){
	msg::error('Order does not exist','error');
	$in['item_exists']= false;
    json_out($in);
}

$quote_nr_row = array();
$quote_nr = $db->query("SELECT tracking_line.*, tracking.*, tblquote.serial_number, tblquote.sent, tblquote_version.version_status_customer, tblquote.f_archived FROM tracking
						INNER JOIN tracking_line ON tracking.trace_id = tracking_line.trace_id
						INNER JOIN tblquote ON tracking_line.origin_id = tblquote.id
						INNER JOIN (SELECT tblquote_version.quote_id,MAX(tblquote_version.version_id) AS v_id FROM tblquote_version GROUP BY tblquote_version.quote_id) AS tblq_version ON tblquote.id=tblq_version.quote_id
            			INNER JOIN tblquote_version ON tblq_version.quote_id=tblquote_version.quote_id AND tblq_version.v_id=tblquote_version.version_id
						WHERE tracking.target_id = '".$in['order_id']."' AND tracking.target_type ='4' AND tracking_line.origin_type ='2' ");

$quote_opts = array(gm('Draft'),gm('Rejected'),gm('Accepted'),gm('Accepted'),gm('Accepted'),gm('Sent'));
while($quote_nr->move_next()){
	$status=$quote_opts[$quote_nr->f('version_status_customer')];
	if($quote_nr->f('sent') == 1 && $quote_nr->f('version_status_customer') == 0){
		$status=$quote_opts[5];
	}
    array_push($quote_nr_row, array( 'serial' => ($quote_nr->f('f_archived') ? '' : $quote_nr->f('serial_number')), 'id'=> $quote_nr->f('origin_id'), 'status'=> ($quote_nr->f('f_archived') ? gm('Archived') : $status)));   
}

$is_inv_nr=false;
$invoice_nr_row = array();
/*$inv_nr = $db->query("SELECT serial_number,id, type, sent, status, not_paid FROM tblinvoice WHERE order_id LIKE '%;".$in['order_id'].";%' AND f_archived !='1' order by id desc");
while($inv_nr->move_next()){
		$type_title='';
	    switch ($inv_nr->f('type')){
		case '0':
		    $type_title = gm('Regular invoice');
		    break;
		case '1':
		    $type_title = gm('Proforma invoice');
		    break;
		case '2':
		    $type_title = gm('Credit Invoice');
		    break;
	    }	    

	    if($inv_nr->f('sent') == '0' && $inv_nr->f('type')!='2'){
			$type_title = gm('Draft');
	    }else if($inv_nr->f('status')=='1'){
			$type_title = gm('Paid');
	    }else if($inv_nr->f('sent')!='0' && $inv_nr->f('not_paid')=='0'){
			$type_title = gm('Final');
	    } else if($inv_nr->f('sent')!='0' && $inv_nr->f('not_paid')=='1'){
			$type_title = gm('No Payment');
	    }
    	array_push($invoice_nr_row, array( 'serial' => $inv_nr->f('serial_number'), 'id'=> $inv_nr->f('id'), 'status'=> $type_title  ) );
}*/
$linked_downpayment_invoice=0;
//if(empty($invoice_nr_row)){
	$inv_nr = $db->query("SELECT tblinvoice.id,tblinvoice.serial_number, tblinvoice.type, tblinvoice.sent, tblinvoice.status, tblinvoice.not_paid,tblinvoice.due_date, tblinvoice.paid,tblinvoice.f_archived,tbldownpayments.id as downpayent_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id = tracking.trace_id
						INNER JOIN tblinvoice ON tracking.target_id = tblinvoice.id
						LEFT JOIN tbldownpayments ON tblinvoice.id = tbldownpayments.invoice_id AND tbldownpayments.order_id='".$in['order_id']."'
						WHERE tracking_line.origin_id = '".$in['order_id']."' AND tracking_line.origin_type ='4' AND (tracking.target_type ='1' OR tracking.target_type ='7') ")->getAll();

	$invoices_from_proforma=$db->query("SELECT tblinvoice.id,tblinvoice.serial_number, tblinvoice.type, tblinvoice.sent, tblinvoice.status, tblinvoice.not_paid,tblinvoice.due_date, tblinvoice.paid,tblinvoice.f_archived,tbldownpayments.id as downpayent_id 
		FROM tracking_line
		INNER JOIN tracking AS tracking_proforma ON tracking_line.trace_id=tracking_proforma.trace_id AND tracking_proforma.target_type='7'
		INNER JOIN tracking_line AS tracking_line_invoice ON tracking_proforma.target_id=tracking_line_invoice.origin_id AND tracking_proforma.target_type=tracking_line_invoice.origin_type
		INNER JOIN tracking AS tracking_invoice ON tracking_line_invoice.trace_id=tracking_invoice.trace_id AND tracking_invoice.target_type='1'
		INNER JOIN tblinvoice ON tracking_invoice.trace_id=tblinvoice.trace_id
		LEFT JOIN tbldownpayments ON tblinvoice.id = tbldownpayments.invoice_id AND tbldownpayments.order_id='".$in['order_id']."'
		WHERE tracking_line.origin_id='".$in['order_id']."' AND tracking_line.origin_type='4'")->getAll();

	if(!empty($invoices_from_proforma)){
		$inv_nr=array_merge($inv_nr,$invoices_from_proforma);
	}

	$credit_invoices_from_invoices=$db->query("SELECT tblinvoice.id,tblinvoice.serial_number, tblinvoice.type, tblinvoice.sent, tblinvoice.status, tblinvoice.not_paid,tblinvoice.due_date, tblinvoice.paid,tblinvoice.f_archived,tbldownpayments.id as downpayent_id 
		FROM tracking_line
		INNER JOIN tracking AS tracking_invoice ON tracking_line.trace_id=tracking_invoice.trace_id AND (tracking_invoice.target_type='1' OR tracking_invoice.target_type='7')
		INNER JOIN tracking_line AS tracking_line_invoice ON tracking_invoice.target_id=tracking_line_invoice.origin_id AND tracking_invoice.target_type=tracking_line_invoice.origin_type
		INNER JOIN tracking AS tracking_credit_invoice ON tracking_line_invoice.trace_id=tracking_credit_invoice.trace_id AND tracking_credit_invoice.target_type='8'
		INNER JOIN tblinvoice ON tracking_credit_invoice.trace_id=tblinvoice.trace_id
		LEFT JOIN tbldownpayments ON tblinvoice.id = tbldownpayments.invoice_id AND tbldownpayments.order_id='".$in['order_id']."'
		WHERE tracking_line.origin_id='".$in['order_id']."' AND tracking_line.origin_type='4'")->getAll();

	if(!empty($credit_invoices_from_invoices)){
		$inv_nr=array_merge($inv_nr,$credit_invoices_from_invoices);
	}

	foreach($inv_nr as $key=>$value){
		$type_title='';
	    switch ($value['type']){
		case '0':
		    $type_title = gm('Regular invoice');
		    break;
		case '1':
		    $type_title = gm('Proforma invoice');
		    break;
		case '2':
		    $type_title = gm('Credit Invoice');
		    break;
	    }	    

	    if($value['sent'] == '0' && $value['type']!='2'){
			$type_title = gm('Draft');
	    }else if($value['status']=='1'){
			$type_title = $value['paid']=='2' ? gm('Partially Paid') : gm('Paid');
	    }else if($value['sent']!='0' && $value['not_paid']=='0'){
			$type_title = $value['status']=='0' && $value['due_date']< time() ? gm('Late') : gm('Final');
	    } else if($value['sent']!='0' && $value['not_paid']=='1'){
			$type_title = gm('No Payment');
	    }

	    $downp_txt = "";
	    if($value['downpayent_id']){
	    	$downp_txt = gm('Downpayment');
	    	$linked_downpayment_invoice++;
	    }


    	array_push($invoice_nr_row, array( 'serial' => ($value['f_archived']=='1' ? '': $value['serial_number']), 'id'=> $value['id'], 'status'=> ($value['f_archived']=='1' ? gm('Archived') : $type_title),'downp_txt'=>$downp_txt));
    
	}
//}
$allow_purchase_price_change=empty($invoice_nr_row) || (count($invoice_nr_row)>0 && count($invoice_nr_row) == $linked_downpayment_invoice)  ? true : false;

$p_order_nr_row = array();

$pord_nr = $db->query("SELECT tracking.*, tracking_line.*, pim_p_orders.serial_number, pim_p_orders.sent, pim_p_orders.invoiced, pim_p_orders.rdy_invoice,pim_p_orders.active,pim_p_order_deliveries.delivery_id AS del_id FROM tracking_line
						INNER JOIN tracking ON tracking_line.trace_id = tracking.trace_id
						INNER JOIN pim_p_orders ON tracking.target_id = pim_p_orders.p_order_id
						LEFT JOIN pim_p_order_deliveries ON pim_p_orders.p_order_id = pim_p_order_deliveries.p_order_id
						WHERE tracking_line.origin_id = '".$in['order_id']."' AND tracking_line.origin_type ='4' AND tracking.target_type ='6' GROUP BY pim_p_orders.p_order_id");

while($pord_nr->move_next()){
	$status ='';
		if($pord_nr->f('sent')==0 ){
			$status = gm('Draft');
		}
		if($pord_nr->f('invoiced')){
			if($pord_nr->f('rdy_invoice') == 1){
				$status = gm('Received');
			}else{
				$status = gm('Final');
			}
		}else{
			if($pord_nr->f('rdy_invoice') == 2){
				if($pord_nr->f('sent')=='1' && $pord_nr->f('del_id') !=0){
					$status = gm('Partially Delivered');
				}else{
					$status = gm('Final');
				}
			}else{
				if($pord_nr->f('rdy_invoice') == 1){
					$status = gm('Received');
				}else{
					if($pord_nr->f('sent') == 1){
						$status = gm('Final');
					}else{
						$status = gm('Draft');
					}
				}
			}
		}

		$is_p_delivery_planned=false;
		if(P_ORDER_DELIVERY_STEPS==2){
			if($pord_nr->f('rdy_invoice') == 2){
				$dels_p=$db2->query("SELECT * FROM pim_p_order_deliveries WHERE p_order_id = '".$pord_nr->f('target_id')."' ");
				while($dels_p->next()){
					if(!$dels_p->f('delivery_done')){
						$is_p_delivery_planned=true;
					}
				}
			}
		}
		if($is_p_delivery_planned){
			$status=gm('Delivery planned');
		}

    	array_push($p_order_nr_row, array( 'serial' => (!$pord_nr->f('active') ? '' : $pord_nr->f('serial_number')), 'id'=> $pord_nr->f('target_id'), 'status'=> (!$pord_nr->f('active') ? gm('Archived') : $status)));
    
	}

$deal_nr_row = array();

$deal_nr = $db->query("SELECT tracking . * , tracking_line . * , tblopportunity.serial_number,tblopportunity_stage.name AS stage_name
						FROM tracking_line
						LEFT JOIN tracking ON tracking_line.trace_id = tracking.trace_id
						LEFT JOIN tblopportunity ON tracking_line.origin_id = tblopportunity.opportunity_id
						LEFT JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
						WHERE tracking.target_id =  '".$in['order_id']."' AND tracking.target_type='4' AND tracking_line.origin_type =  '11'");
while($deal_nr->move_next()){
    	array_push($deal_nr_row, array( 'serial' => $deal_nr->f('serial_number'), 'id'=> $deal_nr->f('origin_id'), 'stage_name'=>$deal_nr->f('stage_name') ) );
    
	}


$page_title= gm("Order Information");

$order = $db->query("SELECT pim_orders.*,pim_order_articles.vat_percent,pim_orders.pdf_layout,pim_orders.pdf_logo FROM pim_orders
			LEFT JOIN pim_order_articles ON pim_orders.order_id=pim_order_articles.order_id
			WHERE pim_orders.order_id='".$in['order_id']."'");
if(!$order->move_next()){
	page_redirect('index.php?do=order-orders');
}
/*if($order->f('pdf_layout')){
	$link_end='&type='.$order->f('pdf_layout').'&logo='.$order->f('pdf_logo');
}else{*/
	$link_end = '&type='.ACCOUNT_ORDER_PDF_FORMAT;
/*}*/

if(defined('USE_CUSTOME_ORDER_PDF') && USE_CUSTOME_ORDER_PDF == 1 && $order->f('pdf_layout') == 0){
	$link_end = '&custom_type='.ACCOUNT_ORDER_PDF_FORMAT;
}

$is_admin = getItemPerm(array('module'=>'order','item'=>$order->f('author_id')));
$has_admin_rights = getHasAdminRights(array('module'=>'order'));

$is_contacts = false;
$link_b_text = 'contact_id';
$link_c_text = 'contact_name';
$remove_vat = $order->f('remove_vat');
if($order->f('field') == 'customer_id'){
	$customer_id = $order->f('customer_id');
	$link_b_text = 'buyer_id';
	$link_c_text = 'customer_name';
}else{
	$is_contacts = true;
	$contact_id = $order->f('contact_id');

}
/*
if($order->f('delivery_address')){
	$view->assign(array(
		'delivery_address'=>nl2br($order->f('delivery_address')),
		'is_delivery'=>true
	));
}else{
	$view->assign(array(
		'delivery_address'=>'',
		'is_delivery'=>false
	));
}*/
if($order->f('comments')){
	$view->assign(array(
		'comments'=>nl2br($order->f('comments')),
		'is_comments'=>true
	));
}else{
	$view->assign(array(
		'comments'=>'',
		'is_comments'=>false
	));
}

if($order->f('contact_id'))
	{
		$contact_title = $db->field("SELECT customer_contact_title.name FROM customer_contact_title
									INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
									WHERE customer_contacts.contact_id='".$order->f('contact_id')."'");
	}
	if($contact_title)
	{
		$contact_title .= " ";
	}
$in['apply_discount'] = $order->f('apply_discount');
$rate = return_value($order->f('currency_rate'));
$vat = $order->f('vat_percent');
$factur_date = date(ACCOUNT_DATE_FORMAT,  $order->f('date'));
$factur_del_date = $order->f('del_date') ? date(ACCOUNT_DATE_FORMAT,  $order->f('del_date')) : '';
$currency = get_commission_type_list($order->f('currency_type'));
$currency_type = $order->f('currency_type');
$serial_number = $order->f('serial_number');
$is_ref = false;
if($order->f('buyer_ref')){
	$is_ref = true;
}

$ref = '';
if(ACCOUNT_ORDER_REF && $order->f('buyer_ref')){
	$ref = $order->f('buyer_ref');
}else{
	$is_ref = false;
}
$delivery_status = $order->f('rdy_invoice');
if($order->f('shipping_price')){
	$shipping_price = $order->f('shipping_price');
}else{
	$shipping_price = 0;
}
$notes = $db->field("SELECT item_value FROM note_fields
								   WHERE item_type = 'order'
								   AND item_name = 'notes'
								   And active = '1'
								   AND item_id = '".$in['order_id']."' ");
$total_order_incomplete = false;
$invoiced_deliveries = $db->query("SELECT * FROM pim_orders_delivery WHERE order_id='".$order->f('order_id')."' AND invoiced='1' ");
if(!$invoiced_deliveries->next()){
	$total_order_incomplete = true;
}

$is_del_not_invoiced = false;
$invoice_de_not = $db->query("SELECT invoiced FROM pim_orders_delivery WHERE order_id='".$order->f('order_id')."' AND invoiced='0' ");
if(ORDER_DELIVERY_STEPS == 2){
	$invoice_de_not = $db->query("SELECT invoiced FROM pim_orders_delivery
						LEFT JOIN pim_order_deliveries
						ON pim_orders_delivery.delivery_id = pim_order_deliveries.delivery_id
						WHERE pim_orders_delivery.order_id='".$order->f('order_id')."'
						AND invoiced='0'
						AND delivery_done = '1' ");
}

if($invoice_de_not->next()){
	$is_del_not_invoiced = true;
}
//echo $is_del_not_invoiced;
$email_language = $order->f('email_language');
if(!$email_language){
	$email_language = 1;
}
$author = '';
if($order->f('author_id')){
	$author = get_user_name($order->f('author_id'));
}
$post_active=$db->field("SELECT active FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");
$SHOW_TOTAL_MARGIN_ORDER=$db->field("SELECT value FROM settings WHERE constant_name='SHOW_TOTAL_MARGIN_ORDER' ");
$ACCOUNT_ORDER_WEIGHT=$db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_WEIGHT' ");

 $certif_db = false;
if(DATABASE_NAME=='e5e54f15_47f4_b527_9a9973b007d2' || DATABASE_NAME=='c76f5025_7c04_19f0_fa5008a84d8a' ||
   DATABASE_NAME=='48885ce5_df34_4196_f295c531431e' || DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='SalesAssist_11'){
	$certif_db = true;
}

$o = array(
	'item_exists'						=> true,
	'is_admin'							=> $is_admin,
	'link_c_text'						=> $link_c_text,
	'link_b_text'						=> $link_b_text,
	'check_acc_manager'				        => $order->f('acc_manager_id')!=0 ? true : false,
	'is_sent'				        => $order->f('sent')==1 ? true : false,
	'selectedDraft'					=> $order->f('sent')==0 ? true:false,
	'selectedSent'					=> $order->f('invoiced') ? ( $order->f('rdy_invoice') == 1 ? false : ($order->f('sent')==1 ? true : false)) : ( $order->f('rdy_invoice') == 2 ? true : ($order->f('rdy_invoice') == 1 ? false : ($order->f('sent') == 1 ? true : false)  ) ),
	'selectedWon'						=> $order->f('invoiced') ? ( $order->f('rdy_invoice') == 1 ? true : false) : ( $order->f('rdy_invoice') == 2 ? false : ($order->f('rdy_invoice') == 1 ? true : ($order->f('sent') == 1 ? false : false ) ) ),
	'hide_sent'     				=> $order->f('sent')==0 ? '':'hide',
	'delivery_address'			=> $order->f('delivery_address') ? nl2br($order->f('delivery_address')) : '',
	// 'hide_unsent'   				=> $order->f('sent')==0?'hide': ($_SESSION['access_level'] ==1 ? '' : 'hide'),
	'hide_unsent'   				=> $order->f('sent')==0?'hide': ($_SESSION['access_level'] ==1 || in_array('order', $_SESSION['admin_sett']) ? '' : 'hide'),
	'factur_nr' 						=> $order->f('serial_number') ? $ref.$order->f('serial_number'): '',
	'ref' 									=> $ref,
	'is_ref'								=> $is_ref,
	'factur_date' 					=> $factur_date,
	'factur_del_date' 			=> $factur_del_date,
	'del_date'					=> $order->f('del_date') ? $order->f('del_date')*1000 : '',
	//'inv_nr'	 					=> $inv_nr,
	'is_inv'								=> $is_inv_nr && $order->f('invoiced') ? true : false,
	'due_date'							=> date(ACCOUNT_DATE_FORMAT,$order->f('due_date')),
	'delete_link'   				=> 'index.php?do=order-orders-order-delete_order&order_id='.$order->f('order_id'),
	'draft_link'  	 				=> 'index.php?do=order-order-order-mark_as_draft&order_id='.$order->f('order_id'),
	'ready_link'  	 				=> 'index.php?do=order-order-order-mark_sent&order_id='.$order->f('order_id'),
	'duplicate_link'    		    => 'index.php?do=order-norder&duplicate_order_id='.$order->f('order_id'),
	'order_buyer_name'              => $order->f('field') == 'customer_id' ? ($order->f('customer_name')) : $contact_title.($order->f('customer_name'))." > " ,
	'order_buyer_name_encode'       => $order->f('field') == 'customer_id' ? (urlencode($order->f('customer_name'))) : urlencode($order->f('customer_name'))." > " ,
	'your_ref_encode'			  => $order->f('customer_reference') ? $order->f('customer_reference') : urlencode($order->f('your_ref')),
	'order_contact_name'            => $contact_title.$order->f('contact_name'),
	'field_customer_id'             => $order->f('field')=='customer_id'?true:false,
	'order_buyer_country'   		=> get_country_name($order->f('customer_country_id')),
	'order_buyer_state'     		=> $order->f('customer_state'),
	'order_buyer_city'      		=> $order->f('customer_city'),
	'order_buyer_zip'       		=> $order->f('customer_zip'),
	'order_buyer_address'   		=> $order->f('customer_address'),
	'buyer_email'                   => $order->f('customer_email'),
	'buyer_fax'                     => $order->f('customer_fax'),
	'buyer_phone'                   => $order->f('customer_phone'),
	'order_seller_name'             => utf8_decode($order->f('seller_name')),
	'order_seller_d_address'        => utf8_decode($order->f('seller_d_address')),
	'seller_d_city'                 => utf8_decode($order->f('seller_d_city')),
	'seller_d_zip'                  => $order->f('seller_d_zip'),
	'seller_d_country'              => get_country_name( $order->f('seller_d_country_id')),
	'order_vat_no'                  => $order->f('seller_bwt_nr'),
	'customer_vat_number'			=> $order->f('customer_vat_number'),

	'serial_number'			 		=> $order->f('serial_number'),
	'is_editable'					=> ($order->f('rdy_invoice') > 0 || $order->f('sent')) ? false : true,

	'are_delivery'					=> $order->f('rdy_invoice') == 2 ? true : false,
	'rdy_invoice_not_1'					=> $order->f('rdy_invoice') != 1 ? true : false,
	// 'undo'							=> $order->f('rdy_invoice') > 0 ? true : false,
	'undo'							=> false,
	'status'						=> $order->f('invoiced') ? ( $order->f('rdy_invoice') == 1 ? gm('Invoiced') : gm('Partially delivered')) : ( $order->f('rdy_invoice') == 2 ? gm('Partially delivered') : ($order->f('rdy_invoice') == 1 ? gm("Delivered")  :  ($order->f('sent') == 1 ? gm('Ready To Deliver') : gm('Draft'))  ) ),
	'is_invoice'					=> $order->f('rdy_invoice') > 0 ? ($order->f('invoiced') ? false : true) : false,
	'not_invoiced'					=> $order->f('rdy_invoice') == 0 ?  true : false,
	'create_proforma'		        => $order->f('sent') == 1 ? true : false,
	'is_revert'						=> $order->f('rdy_invoice') > 0  ? false : true,
	'del_note_txt'					=> nl2br($order->f('del_note')),
	'del_note'						=> $order->f('del_note'),

	//'is_revert'						=> false,
	'img'							=> $order->f('rdy_invoice') ? ($order->f('rdy_invoice')==1 ? 'green' : 'orange') : ($order->f('sent') == 1 ? 'orange' : 'gray'),
	'discount_percent'      		=> '( '.$order->f('discount').'% )',
	'discount'      				=> $order->f('discount'),
	'notes'   		                => nl2br($notes),
	'buyer_id'						=> $order->f('customer_id'),
	'order_id'						=> $in['order_id'],
	'allow_article_packing'         => $order->f('use_package') ? true : false,
	'allow_article_sale_unit'       => $order->f('use_sale_unit') ? true : false,
	'your_ref'       				=> $order->f('your_ref'),
	'customer_reference'       		=> $order->f('customer_reference'),
	'our_ref' 				      	=> $order->f('our_ref'),
	'shipping_price'				=> place_currency( display_number($shipping_price)),
	'shipping_price_number'			=> $shipping_price,
	'shipping_type'					=> $order->f('shipping_type'),
	'is_free_delivery'				=> $shipping_price > 0 ? true : false,
	'address_info'					=> nl2br($order->f('address_info')),
	// 'total_orderer'					=> $order->f('rdy_invoice') == 1 ? '&total_order=1' : '',
	'total_orderer'					=> ($order->f('rdy_invoice') == 1 && $total_order_incomplete == true) ? '&total_order=1' : '',
	'order_author'					=> $order->f('author_id') ? $author : '-',
	'acc_manager_name'					=> $order->f('acc_manager_name'),
	'languages'					    => $order->f('email_language'),
	'disable_option'				=> ($order->f('rdy_invoice') && $is_del_not_invoiced==true) > 0 ? ($order->f('invoiced') ? true : false) : true,

  'disable_option2'			  => $order->f('sent') == 1  ? false : true,
  'whole_quantity'			  => ($order->f('rdy_invoice') == 0 && ($order->f('sent') == 1 || $order->f('sent') == 0)) ? '&total_order=1' : '',
	'is_pickup'                     => $order->f('delivery_type')==2 ? true : false,
	'is_pickup'                     => $order->f('delivery_type')==2 ? true : false,
	'disable_option3'			  => false,
	'is_front'							=> $order->f('order_email')?true:false,
	//'contact_email'					=> $order->f('customer_email'),
	'hide_notes'						=> $notes == '' ? false : true,
	'post_order'					=> $order->f('postgreen_id')!='' ? true : false,
	'post_active'					=> !$post_active || $post_active==0 ? false : true,
	'ACCOUNT_CURRENCY' 				=> get_currency(ACCOUNT_CURRENCY_TYPE),
	'segment'						=> $order->f('segment_id') ? $db->field("SELECT name FROM tblquote_segment WHERE id='".$order->f('segment_id')."' ") : '',
	'segment_id'						=> $order->f('segment_id'),
	'source'						=> $order->f('source_id') ? $db->field("SELECT name FROM tblquote_source WHERE id='".$order->f('source_id')."' ") : '',
	'source_id'						=> $order->f('source_id'),
	'xtype'							=> $order->f('type_id') ? $db->field("SELECT name FROM tblquote_type WHERE id='".$order->f('type_id')."' ") : '',
	'type_id'						=> $order->f('type_id'),
	'source_dd'						=> get_categorisation_source(),
	'type_dd'						=> get_categorisation_type(),
	'segment_dd'					=> get_categorisation_segment(),
	'hide_margin'					=> defined('HIDE_PROFIT_MARGIN') && HIDE_PROFIT_MARGIN == 1 && !$has_admin_rights ? true : false, 
	'show_margin'					=> is_null($SHOW_TOTAL_MARGIN_ORDER) || $SHOW_TOTAL_MARGIN_ORDER==1 ? true : false,
	'is_active'				=> $order->f('active')=='1'? true:false,
	'is_archived'			=> $order->f('active')=='0'? true:false,	
	'subject_field'			=> stripslashes($order->f('subject')),
	'edit_confirmed_order'	=> defined('EDIT_CONFIRMED_ORDERS') && EDIT_CONFIRMED_ORDERS == '1' ? true : false,
	'generalvats'			=> build_vat_dd(),
	'apply_discount'		=> $in['apply_discount'],
	'currency_rate'    		=> $order->f('currency_rate'),
	'certif_db' 			=> $certif_db, 
);

if($order->f('vat_regime_id') && $order->f('vat_regime_id')>=1000){
	$extra_eu=$db2->field("SELECT regime_type FROM vat_new WHERE id='".$order->f('vat_regime_id')."' ");
	if($extra_eu==2){
		$o['block_vat']=true;
	}
	if($extra_eu=='1'){
		$o['vat_regular']=true;
	}
	$contracting_partner=$db2->field("SELECT vat_regime_id FROM vat_new WHERE id='".$order->f('vat_regime_id')."' ");
	if($contracting_partner==4){
		$o['block_vat']=true;
	}
}
$o['remove_vat']=$db2->field("SELECT no_vat FROM vat_new WHERE id='".$order->f('vat_regime_id')."' ");

$o['yuki_project_id']					= $order->f('yuki_project_id');
$o['yuki_project_name']					= get_yuki_project_name($order->f('yuki_project_id'));

$o['vat_regim_dd']		= build_vat_regime_dd();
$o['vat_regime_id']		= $order->f('vat_regime_id') ? $order->f('vat_regime_id') : '0';

$nr_status=$order->f('sent') == 1 && $order->f('rdy_invoice') ? ($order->f('rdy_invoice')==1 ? 'green' : 'orange') : ($order->f('sent') == 1 ? 'orange' : 'gray');

if($order->f('sent')==0 ){
    $type_title = gm('Draft');
}else if($order->f('invoiced')){
    if($order->f('rdy_invoice') == 1){
        $type_title = gm('Delivered');
    }else{
        $type_title = gm('Confirmed');
    }
}else{
    if($order->f('rdy_invoice') == 2){
        $type_title = gm('Confirmed');
    }else{
        if($order->f('rdy_invoice') == 1){
            $type_title = gm('Delivered');
        }else{
            if($order->f('sent') == 1){
                $type_title = gm('Confirmed');
            }else{
                $type_title = gm('Draft');
            }
        }
    }
}
if(!$order->f('active')){
	$type_title = gm('Archived');
	$nr_status='gray';
}
$o['type_title']=$type_title;
$o['nr_status']=$nr_status;

if($order->f('contact_id')){
	$contact = $db->query(" SELECT CONCAT_WS(' ',firstname, lastname) as contact_name, cell, email FROM customer_contacts
							WHERE contact_id='".$order->f('contact_id')."' ");
	$o['contact_name'] 		= $contact->f('contact_name');

	if($order->f('customer_id')){
		$contact = $db->query(" SELECT phone as cell, email FROM customer_contactsIds
								WHERE contact_id='".$order->f('contact_id')."' AND customer_id= '".$order->f('customer_id')."'");
	}			
			$o['contact_phone'] 		= $contact->f('cell');
			$o['contact_email'] 		= $contact->f('email');
}

$o['pdf_del_type'] = ACCOUNT_ORDER_DELIVERY_BODY_PDF_FORMAT;
$o['quotes']=$quote_nr_row;
$o['invoices'] = $invoice_nr_row;
$o['p_orders'] = $p_order_nr_row;
$o['deals'] = $deal_nr_row;
$o['allow_purchase_price_change']=$allow_purchase_price_change;

$default_lang = $order->f('email_language');
if(!$default_lang){
	$default_lang = 1;
}
$o['default_lang'] = $default_lang;
$show_confirm_delivery=false;
$is_delivery_planned=false;
$show_add_new_delivery=true;
if(ORDER_DELIVERY_STEPS==2){
	$count_del = $db->field("SELECT COUNT(delivery_id) FROM pim_order_deliveries WHERE order_id = '".$in['order_id']."' AND delivery_done = '0' ");
	$count_done_del = $db->field("SELECT COUNT(delivery_id) FROM pim_order_deliveries WHERE order_id = '".$in['order_id']."' AND delivery_done = '1' ");
	// $o['delivery_steps_2'] = array(
		$o['status']= $order->f('invoiced') ? ( $order->f('rdy_invoice') == 1 ? gm('Invoiced') : gm('Partially delivered')) : ( ($order->f('rdy_invoice') == 2 && $count_done_del>0) ? gm('Partially delivered') : ( ($order->f('rdy_invoice') == 1 && $count_del==0) ? gm("Delivered")  :  ( ($order->f('sent') == 1 && $count_done_del==0) ? gm('Ready To Deliver') : ( $count_del>0 ? gm('Partially delivered') : gm('Draft') ) ) ) );
		$o['img']= $order->f('rdy_invoice') ? ( ($order->f('rdy_invoice')==1 && $count_del==0) ? 'green' : 'orange') : ($order->f('sent') == 1 ? 'orange' : 'gray');
		$o['disable_option']= ($order->f('rdy_invoice') && $is_del_not_invoiced==true) > 0 ? ($order->f('invoiced') ? ($is_del_not_invoiced==true ? false : true) : ($count_done_del > 0 ? false:true ) ) : true;
		$o['total_orderer']= ($order->f('rdy_invoice') == 1 && $total_order_incomplete == true) ? ($count_done_del > 0 ? '' :'&total_order=1') : '';
		$o['selectedSent']= $order->f('invoiced') ? ( $order->f('rdy_invoice') == 1 ? '' : 'selected') : ( ($order->f('sent') == 1 && $order->f('rdy_invoice') == 2 && $count_done_del>0) ? 'selected' : ( ($order->f('rdy_invoice') == 1 && $count_del==0) ? ''  :  ( ($order->f('sent') == 1 && $count_done_del==0) ? 'selected' : ( $count_del>0 ? 'selected' : '' ) ) ) );
		$o['selectedWon']= $order->f('invoiced') ? ( $order->f('rdy_invoice') == 1 ? 'selected' : '') : ( ($order->f('rdy_invoice') == 2 && $count_done_del>0) ? '' : ( ($order->f('rdy_invoice') == 1 && $count_del==0) ? 'selected'  :  ( ($order->f('sent') == 1 && $count_done_del==0) ? '' : ( $count_del>0 ? '' : '' ) ) ) );
		$o['hide_this_or']= $count_done_del == 0 && $order->f('rdy_invoice') == 1 ? 'hide' : '';
		$o['are_delivery']= $order->f('rdy_invoice') == 2 ? true : ( $count_del > 0 ? true : false);
	// );
    $is_partial_deliver = false;
	if($order->f('rdy_invoice')==2){
		$dels=$db->query("SELECT * FROM pim_order_deliveries WHERE order_id='".$in['order_id']."'");
		while($dels->next()){
			//daca cel putin una din livrarile partiale sunt confirmate, atunci is_partial_deliver = true
			if($dels->f('delivery_done')=='1'){
				$is_partial_deliver = true;
				$is_delivery_planned=false;
				break;
			}else{
				$is_delivery_planned=true;
			}
		}
	}
	if($count_del){
		$show_confirm_delivery=true;
	}	

	$total_q_to_deliver=$db->field("SELECT COALESCE(SUM(quantity),0) FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND delivered='0' AND content='0' AND tax_id='0' AND has_variants='0'");
	$total_q_delivered=$db->field("SELECT COALESCE(SUM(quantity),0) FROM pim_orders_delivery WHERE order_id='".$in['order_id']."' AND delivery_id IN (SELECT delivery_id FROM pim_order_deliveries WHERE order_id='".$in['order_id']."') ");

	if($total_q_delivered>0 && $total_q_delivered == $total_q_to_deliver){
		$show_add_new_delivery=false;
	}
	
}else{
	$is_partial_deliver= $order->f('rdy_invoice')==2?true:false;
}
if($is_partial_deliver && !$is_delivery_planned && $order->f('sent') == 1){
	$o['type_title']=gm('Partially Delivered');
}else if($is_delivery_planned && $order->f('sent') == 1){
	$o['type_title']=gm('Delivery planned');
}

if($order->f('sent')==0 ){
		$o['disable_option']				= true;
    $o['disable_option2']			  = true;
    $o['disable_option3']			  = true;
}
$o['show_confirm_delivery']=$show_confirm_delivery;
$o['show_add_new_delivery']=$show_add_new_delivery;
// $view->assign('contact_email',$order->f('customer_email'));


$discount_percent= $order->f('discount');


$i=0;
$discount_total = 0;
$vat_value = 0;
$show_discount_total = false;
$fully_delivered = true;
// $disc_array = array();
$vat_array = array();
$global_disc = $order->f('discount');
$is_profi=false;
$find_if_delivered = $db->query("SELECT pim_order_articles.* FROM pim_order_articles WHERE order_id='".$in['order_id']."' AND content='0' AND has_variants='0' ORDER BY sort_order ASC");
while ($find_if_delivered->next()) {
      if(return_value($find_if_delivered->f('purchase_price')) || return_value($find_if_delivered->f('purchase_price_other_currency'))) {
             $is_profi=true;
      }
	if(!$find_if_delivered->f('delivered')){
		$fully_delivered = false;
	}
}

$from_import=false;

$suppliers=array();
$adveo_id = $db->field("SELECT customer_id FROM customers WHERE name like 'adveo' and active='5'");
	$db->query("SELECT pim_order_articles.*,pim_articles.weight FROM pim_order_articles 
		LEFT JOIN pim_articles ON pim_articles.article_id = pim_order_articles.article_id
		WHERE order_id='".$in['order_id']."' ORDER BY sort_order ASC");
	$margin=0;
	$line_margin=0;
	$purchase_amount=0;
	$o['lines']=array();
	$weight_value=0;
	while ($db->move_next())
	{
		$delivered_article_q = $db2->field("SELECT COALESCE(SUM(quantity),0) FROM pim_orders_delivery WHERE order_id = '".$in['order_id']."' AND order_articles_id = '".$db->f('order_articles_id')."' ");

		$weight_value += $db->f('quantity') * $db->f('weight');
		$supplier_id=$db2->field("SELECT supplier_id FROM pim_articles WHERE article_id='".$db->f('article_id')."'");
        if($supplier_id && !in_array($supplier_id,$suppliers)) {
		   array_push($suppliers,$supplier_id);
        }

		if(!$show_discount_total && $db->f('discount') !=0){
			$show_discount_total = true;
		}
		if($db->f('from_import')){
			$from_import=true;
		}

		// if(!in_array($db->f('discount'), $disc_array)){
		// 	array_push($disc_array, $db->f('discount'));
		// }
		$discount_line = $db->f('discount');
		if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
			$discount_line = 0;
		}
		if($in['apply_discount'] < 2){
			$global_disc = 0;
		}
		//$q = @($db->f('quantity') * $db->f('packing') / $db->f('sale_unit'));
		$q = $db->f('packing') && $db->f('sale_unit') ? @($db->f('quantity') * $db->f('packing') / $db->f('sale_unit')) : 0;

		$price_line = $db->f('price') - ($db->f('price') * $discount_line /100);
		$discount_total += $price_line * $global_disc / 100 * $q;

		$vat_total += ($price_line - $price_line * $global_disc /100) *$db->f('vat_percent') / 100 * $q;
		$line_total=$price_line * $q;

		$vat_value_line =($price_line - $price_line * $global_disc /100) *$db->f('vat_percent') / 100 * $q;
		$vat_percent[$db->f('vat_percent')] += $vat_value_line;

		$sub_t[$db->f('vat_percent')] += $line_total;
		$sub_discount[$db->f('vat_percent')] += ($line_total * $global_disc /100);

		$total += $line_total;
       if(!$db->f('tax_id')){
       	   $total_no_tax += $line_total;
       	   $purchase_amount += $db->f('purchase_price')*$q;
           
            if($global_disc){
        	  $line_total=$line_total-$line_total*$global_disc /100;
        	  $line_margin=$line_total-($db->f('purchase_price')*$q);
        	  $margin+=$line_margin;
           }else{
           	 $line_margin=$line_total-($db->f('purchase_price')*$q);
           	 $margin+=$line_margin;
           }
        }

		//$price_line = $db->f('price') - ($db->f('price')*$db->f('discount')/100);
		//$vat_total += ($price_line*$db->f('vat_percent')/100)*$db->f('quantity')* ($db->f('packing')/$db->f('sale_unit'));

		$vat_array[$db->f('vat_percent')] += ($price_line - $price_line * $global_disc /100) *$db->f('vat_percent') / 100 * $q;
		//$discount_total += ($db->f('price')*$db->f('discount')/100)*$db->f('quantity')*($db->f('packing')/$db->f('sale_unit'));

		//$total += $db->f('price')*$db->f('quantity') * ($db->f('packing')/$db->f('sale_unit'));
		// $total_with_vat += $db->f('price_with_vat')*$db->f('quantity') * ($db->f('packing')/$db->f('sale_unit'));
		$nr_dec_q = strlen(rtrim(substr(strrchr($db->f('quantity'), "."), 1),'0'));

		$line = array(
			'article' 					=> $db->f('content') ? htmlspecialchars_decode($db->f('article')) : nl2br($db->f('article')),
			'quantity'      			=> display_number($db->f('quantity'), $nr_dec_q),
//			'price_vat'      			=> display_number($db->f('price_with_vat')),   #changed to amount
			'price_vat'      			=> display_number($db->f('quantity')*$price_line * ($db->f('packing')/$db->f('sale_unit'))),   #changed to amount
			'price'         			=> display_number_var_dec($db->f('price')),
			'percent'				    => display_number($db->f('vat_percent')),
			'vat_value'				    => display_number($db->f('vat_value')),
			'sale_unit'				    => $db->f('sale_unit'),
			'packing'				    => remove_zero_decimals($db->f('packing')),
			'disc'						=> display_number($db->f('discount')),
			'colspan'					=> $db->f('content') ? ' colspan=9 ' : '',
			'content'					=> $db->f('content') ? false : true,
			'content_class'				=> $db->f('content') ? ' last_nocenter ' : '',
			'article_code'				=> $db->f('article_code'),
			'is_article_code'			=> $db->f('article_code') ? true : false,
			'has_variants' 				=> $db->f('has_variants'),
		 	'has_variants_done' 		=> $db->f('has_variants_done'),
		 	'is_variant_for' 			=> $db->f('is_variant_for'),
		 	'variant_type' 				=> $db->f('variant_type'), 
		 	'is_combined' 				=> $db->f('is_combined'),
		 	'is_component' 				=> $db->f('component_for')? true:false, 
		 	'visible'					=> $db->f('visible')? true:false,
		 	'order_articles_id'			=> $db->f('order_articles_id'),
		 	'purchase_price'			=> $db->f('purchase_price'),
		 	'purchase_price_other_currency'	=> $db->f('purchase_price_other_currency'),
		 	'purchase_price_other_currency_id'	=> $db->f('purchase_price_other_currency_id'),
		 	'to_deliver'				=> display_number($db->f('quantity')-$delivered_article_q)
		);
		array_push($o['lines'], $line);
		// $view->loop('order_row');
		$i++;
	}$prev='';

	$o['show_articles_pdf']         =  defined('SHOW_ARTICLES_PDF') && SHOW_ARTICLES_PDF=='1' ? true : false;

    $diffSuppliers = (count($suppliers) === count(array_unique($suppliers)));

    if($diffSuppliers){
        // $view->assign("single_supplier",false);
    }else{
         // $view->assign("single_supplier",true);
    }
    if(count($suppliers)==1){
    	// $view->assign("single_supplier",true);
    }


	// $discount_total=$total*(($discount_percent)/100);
 /*   $o['vat_lines']=array();
	foreach ($vat_array as $key => $value) {
		if($value){
			$vat = array(
				'vat_total'				=> place_currency(display_number($value)),
 	 			'percent'				=> display_number($key),
			);
			array_push($o['vat_lines'], $vat);
			// $view->loop('vat_loop');
		}
	}*/

	$o['is_order_weight']=$ACCOUNT_ORDER_WEIGHT==1 ? true : false;
	$o['weight_value']=$weight_value;

	$o['vat_line']=array();
	$t=0;
	$o['hide_total'] = true;
	foreach ($vat_percent as $key => $val){ 
		 if($sub_t[$key] - $sub_disc[$key]){

			$vat_line=array(
				'vat_percent'   					=> display_number($key),
				'vat_value'	   						=> place_currency(display_number(round($val,ARTICLE_PRICE_COMMA_DIGITS)),$currency),
				'total_novat'	   	 				=> place_currency(display_number(round($sub_t[$key],ARTICLE_PRICE_COMMA_DIGITS)),$currency),
				'subtotal'							=> place_currency(display_number(round($sub_t[$key],ARTICLE_PRICE_COMMA_DIGITS)),$currency),
				'discount_value'	 		 	 	=> place_currency(display_number($sub_discount[$key]),$currency),
				'sh_discount'						=> $in['apply_discount'] == 0 || $in['apply_discount'] == 2 ? false : true,
				'sh_vat'							=> $remove_vat == 1 ? false : true,
				'net_amount'						=> place_currency(display_number($sub_t[$key] - $sub_discount[$key]),$currency),
				'is_ord'				=> $is_ord,
				'view_discount'			=> ($discount || $is_ord) ? true : false,

			);
			array_push($o['vat_line'], $vat_line);
		}
		$t++;
	}

	// $total=$total-$discount_total;
	 // $total = $total + $shipping_price;
	$total = round($total,2);
	$vat_total = round($vat_total,2);
	$total_with_vat=$vat_total+$total-$discount_total+$shipping_price;
	$total_default = $total_with_vat*$rate;
	$total_default_rm_vat = ($total-$discount_total+$shipping_price)*$rate;

	$o['totalul'] = array(
	  	'discount_total'    	=> place_currency(display_number($discount_total),$currency),
	  	'vat_total'				=> display_number($vat_total),
 	 	'total'					=> place_currency(display_number($total),$currency),
	  	'total_vat'			    => $remove_vat ? place_currency(display_number($total-$discount_total+$shipping_price),$currency) : place_currency(display_number($total_with_vat),$currency),
	  	'total_order_vat'		=> $remove_vat ? $total-$discount_total+$shipping_price : $total_with_vat,
	  	'total_order_no_vat'	=> $rate? (($total-$discount_total+$shipping_price)*$rate ): $total-$discount_total+$shipping_price,
	  	'gov_taxes_value'   	=> display_number($total_gov_taxes),
	  	'hide_currency2'		=> ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide',
		'hide_currency1'		=> ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide',
		'currency_type'			=> $currency,#ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
		'currency_d_type'		=> get_commission_type_list(ACCOUNT_CURRENCY_TYPE),#ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
		'total_default'			=> $remove_vat ? place_currency(display_number($total_default_rm_vat)) : place_currency(display_number($total_default)),
		'adveo_id'				=> $adveo_id,
	);
$l=1;

if($l==1){
	// $o['pdf_links'] = array(
	  $o['generate_pdf_id']  	= '';
	  $o['generate_xls_id']  	= '';
	  $o['hide_language']	   	= 'hide';
	  $o['pdf_link']          = 'index.php?do=order-order_print&order_id='.$in['order_id'].'&lid='.$order->f('email_language').$link_end;
	  $o['xls_link']          = 'index.php?do=order-order_xls&order_id='.$in['order_id'].'&lid='.$order->f('email_language').$link_end;
	// );
}
$l=0;
//print languages
 $db->query("SELECT * FROM pim_lang WHERE active=1");
 /*while($db->move_next()){
    $view->assign(array(
		'language'	        => $db->f('language'),
	),'languages_web');

	$view->loop('languages_web');
	$l++;
}
if($l==1){
	$view->assign(array(
	 'web_order_id'   	=> '',
	 'hide_language'	  => 'hide'
	));
}else{
	$view->assign(array(
		'web_order_id'   => 'web_invoice',
		'hide_language'	  => ''
	));
}*/


$link_end_delivery = '&type='.ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;
/*}*/

if(defined('USE_CUSTOME_DELIVERY_PDF') && USE_CUSTOME_DELIVERY_PDF == 1 ){
	$link_end_delivery = '&custom_type='.ACCOUNT_ORDER_DELIVERY_PDF_FORMAT;
}

$j = 0;
$is_delivered = false;
$deliveries = $db->query("SELECT * FROM pim_order_deliveries WHERE order_id='".$in['order_id']."' ORDER BY delivery_id DESC");
$del_nr = $deliveries->records_count();
$o['deliveries']=array();
while ($deliveries->next()) {
	$j++;
	$invoice_del = $db->query("SELECT invoiced FROM pim_orders_delivery WHERE delivery_id='".$deliveries->f('delivery_id')."' AND invoiced='0' ");
	$nr = $invoice_del->records_count();
	$delivery = array(
		'delivey'					=> date(ACCOUNT_DATE_FORMAT,$deliveries->f('date')),
		'delivery_link'				=> 'index.php?do=order-order_print&order_id='.$in['order_id'].'&lid='.$email_language.'&delivery_id='.$deliveries->f('delivery_id').$link_end_delivery,
		'del_delivery_link'			=> $deliveries->f('delivery_done')==0?'index.php?do=order-order-order-delete_delivery&order_id='.$in['order_id'].'&del_nr='.$del_nr.'&delivery_id='.$deliveries->f('delivery_id').'&delivery_approved=1':'index.php?do=order-order-order-delete_delivery&order_id='.$in['order_id'].'&del_nr='.$del_nr.'&delivery_id='.$deliveries->f('delivery_id'),
		'id'						=> $deliveries->f('delivery_id'),
		'active'					=> $j==1 && $delivery_status==1 ? 'active' : 'delivered',
		'del_title'					=> $nr ? gm('Delivered') : gm('Invoiced'),
		'delete_delivery_title'			=> gm('Delete delivery'),
		'send_mail_title'				=> gm('Send mail'),
		'delivery_id'				=> $deliveries->f('delivery_id'),
		'del_by_two_steps'			=> ORDER_DELIVERY_STEPS == 2 ? true : false,
		'not_approved'				=> $deliveries->f('delivery_done')==0 ? true : false,
	);
	if(ORDER_DELIVERY_STEPS==2){
		$delivery['active']	= ($j == 1 && $delivery_status == 1 &&  $count_del == 0) ? 'active' : ( $deliveries->f('delivery_done') == 1 ? 'delivered' : 'not_delivered');
	}
	array_push($o['deliveries'], $delivery);
	// $view->loop('delivery');
}
if($j > 0){
	$is_delivered = true;
}


$j = 0;
$is_returned = false;
$returns = $db->query("SELECT * FROM pim_orders_return WHERE order_id='".$in['order_id']."' ORDER BY return_id DESC");
$ret_nr = $returns->records_count();
$o['returns']=array();
while ($returns->next()) {
	$j++;

	$return = array(
		'date_ret'					=> date(ACCOUNT_DATE_FORMAT,$returns->f('date')),
		'del_return_link'			=> 'index.php?do=order-order-order-delete_return&order_id='.$in['order_id'].'&return_id='.$returns->f('return_id'),
		'id'						=> $returns->f('return_id'),
		'delete_return_title'			=> gm('Delete return'),
		'edit_return_title'			=> gm('Edit return'),
		'return_id'				=> $returns->f('return_id')
	);
	array_push($o['returns'],$return);
	// $view->loop('returns');
}
if($j > 0){
	$is_returns = true;
}

if(!$in['s_date']){
	$in['s_date'] = time();
}
$o['sh_vat'] = true;
if($vat_total == 0){
	$o['sh_vat'] = false;
	// $view->assign('sh_vat','hide');
}

$is_extra = false;
if($customer_id){
	$c_details = $db->query("SELECT * FROM customers WHERE customer_id ='".$customer_id."' ");

	$in['deliv_disp_note'] = $c_details->f('deliv_disp_note');

	if($c_details->f('bank_name') || $c_details->f('bank_bic_code') || $c_details->f('bank_iban') ){
		$extra_info .= '<p><strong>'.$c_details->f('bank_name').'</strong></p>
	<p>'.$c_details->f('bank_bic_code').'</p>
	<p>'.$c_details->f('bank_iban').'</p>
	';
	}
	if($extra_info){
		$is_extra = true;
		// $extra_info = '<div style="width: inherit;" >'.$extra_info.'</div>';
	}

	/*$invoice_email = $db->field("SELECT invoice_email FROM customers WHERE customer_id='".$customer_id."' ");
	if($invoice_email){
		$view->assign(array(
			'inv_email'	=> true,
			'invoice_email'	=> $invoice_email,
		));
	}*/
	$o['customer_notes'] = stripslashes($c_details->f('customer_notes'));
}

global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($database_users);

if(!$in['include_pdf'])
{
	$in['include_pdf'] = 1;
}

$tblinvoice123 = $db->query("SELECT pim_orders.order_id,pim_orders.serial_number,pim_orders.discount, pim_orders.date, pim_orders.customer_name, pim_orders.currency_type, pim_orders.amount as total,
                           pim_orders.email_language
                           FROM pim_orders
                           WHERE pim_orders.order_id='".$in['order_id']."'");
$tblinvoice123->move_next();
$in['lid'] = $tblinvoice123->f('email_language');
if(!$in['lid']){
	$in['lid'] = 1;
}
$message_data=get_sys_message('ordmess',$tblinvoice123->f('email_language'));
$subject=$message_data['subject'];
$identity_id = $db->field("SELECT pim_orders.identity_id FROM pim_orders WHERE pim_orders.order_id='".$in['order_id']."'");
if (!$identity_id) {
    $company = ACCOUNT_COMPANY;
} else {
    $identity = $db->field("SELECT multiple_identity.company_name FROM multiple_identity WHERE multiple_identity.identity_id='". $identity_id ."'");
    if ($identity) {
        $company = $identity;
    } else {
        $company = '';
    }
}
$subject=str_replace('[!ACCOUNT_COMPANY!]',$company, $subject );
$subject=str_replace('[!SERIAL_NUMBER!]',$tblinvoice123->f('serial_number'), $subject);
$subject=str_replace('[!ORDER_DATE!]',$factur_date, $subject);
$subject=str_replace('[!CUSTOMER!]',$tblinvoice123->f('customer_name'), $subject);
// $subject=str_replace('[!SUBTOTAL!]',place_currency(display_number($tblinvoice123->f('total')),$currency), $subject);
$subject=str_replace('[!DISCOUNT!]',$tblinvoice123->f('discount').'%', $subject);
$subject=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($discount_total),$currency), $subject);
//$subject=str_replace('[!VAT!]',$db->f('vat').'%', $subject);
$subject=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),$currency), $subject);
// $subject=str_replace('[!PAYMENTS!]',place_currency(display_number($total_payed),$currency), $subject);
$subject=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($tblinvoice123->f('total')),$currency), $subject);

$body=$message_data['text'];
$account_company = $company;
$account_company = htmlentities($account_company,ENT_COMPAT | ENT_HTML401,'UTF-8');
$body=str_replace('[!ACCOUNT_COMPANY!]',$account_company, $body );
$body=str_replace('[!SERIAL_NUMBER!]',$tblinvoice123->f('serial_number'), $body);
$body=str_replace('[!ORDER_DATE!]',$factur_date, $body);
$body=str_replace('[!CUSTOMER!]',$tblinvoice123->f('customer_name'), $body);
// $body=str_replace('[!SUBTOTAL!]',place_currency(display_number($tblinvoice123->f('total')),$currency), $body);
$body=str_replace('[!DISCOUNT!]',$tblinvoice123->f('discount').'%', $body);
$body=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($discount_total),$currency), $body);
//$body=str_replace('[!VAT!]',$db->f('vat').'%', $body);
$body=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),$currency), $body);
// $body=str_replace('[!PAYMENTS!]',place_currency(display_number($total_payed),$currency), $body);
$body=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($tblinvoice123->f('total')),$currency), $body);

if($message_data['use_html']){
   //$body=str_replace('[!SIGNATURE!]',get_signature(), $body);
	$body=str_replace('[!SIGNATURE!]',get_signature_new(), $body);
}else{
	 $body=str_replace('[!SIGNATURE!]','', $body);
}
$account_manager_email = $db_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."'");
$account_user_email = $db_users->field("SELECT email FROM users WHERE user_id='".$order->f('acc_manager_id')."'");	


	$o['include_pdf']  				 = $in['include_pdf'] ? true : false;
	$o['copy_checked']         = $in['copy'] ? 'checked="checked"' : '';
	$o['subject']              = $in['subject'] ? $in['subject'] : $subject;
	$o['e_message']            = $in['e_message'] ? $in['e_message'] : $body;
	$o['language_dd']	       	 = build_pdf_language_dd($in['lid'],1);
	$o['use_html']			   		 = $message_data['use_html'];
	$o['user_loged_email']		= $account_manager_email;
	$o['user_acc_email']		= $account_user_email;

$tblinvoice_customer_contacts = $db->query("SELECT customer_contacts.*,pim_orders.order_id,pim_orders.customer_id,pim_orders.serial_number
             FROM pim_orders
             INNER JOIN customer_contacts ON customer_contacts.customer_id=pim_orders.customer_id
             WHERE pim_orders.order_id='".$in['order_id']."' AND customer_contacts.active ='1'");
$j = 0;


if($customer_id){
	// $autocomplete_path = '../apps/order/admin/controller/contacts_autocomplete.php?customer_id='.$customer_id;
	$customer_name = $db->field("SELECT name FROM customers WHERE customer_id = '".$customer_id."' ");
}
$jj=1;
$o['recipients']=array();
/*while ($tblinvoice_customer_contacts->move_next()){
	$in['recipients'][$tblinvoice_customer_contacts->f('contact_id')]=1;

	$recipient = array(
		'recipient_name'  			=> $tblinvoice_customer_contacts->f('firstname').' '.$tblinvoice_customer_contacts->f('lastname'),
		'recipient_email' 			=> $tblinvoice_customer_contacts->f('email'),
		'recipient_id'    			=> $tblinvoice_customer_contacts->f('contact_id'),
		'recipients_checked'    	=> $in['recipients'][$tblinvoice_customer_contacts->f('contact_id')] ? 'checked="checked"' : '',

		'contact_email_data' 			=> $tblinvoice_customer_contacts->f('email'),
		'contact_id_data'				=> $tblinvoice_customer_contacts->f('contact_id'),
		'contact_name_data'			=> $customer_id ? $tblinvoice_customer_contacts->f('firstname').' '.$tblinvoice_customer_contacts->f('lastname').' > '.$customer_name : $tblinvoice_customer_contacts->f('firstname').' '.$tblinvoice_customer_contacts->f('lastname'),
		// 'auto_complete_small'			=> $count_recipients == $jj ? '<input type="text" class="auto_complete_small" name="email_addr" value="" data-autocomplete_file="'.$autocomplete_path.'" />' : '',
	);

	$j++;
	$jj++;
	array_push($o['recipients'], $recipient);
	// $view->loop('recipient_row');
}*/
$o['recipients_ids'] = array();
$c_email = $db->field("SELECT invoice_email FROM customers WHERE customer_id='".$order->f('customer_id')."' ");
$invoice_email_type = $db->field("SELECT invoice_email_type FROM customers WHERE customer_id='".$order->f('customer_id')."' ");

		if($order->f('customer_id')){
			$filter .= " AND customers.customer_id='".$order->f('customer_id')."'";
		}
		if($order->f('contact_id')){
			$filter .= " AND customer_contacts.contact_id='".$order->f('contact_id')."'";
		}

		$items = array();
		$items2 = array();
		$contact_email='';
		$customer_exist = false;
		if($order->f('customer_id') && $order->f('contact_id')){
			$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$order->f('customer_id')."' ORDER BY lastname ")->getAll();
		}else{
			/*$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.customer_id = customer_contactsIds.customer_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$order->f('customer_id')."'  ORDER BY lastname ")->getAll();*/
			$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email 
							FROM customers 
							INNER JOIN customer_contactsIds ON customers.customer_id = customer_contactsIds.customer_id 
							INNER JOIN customer_contacts ON customer_contactsIds.contact_id = customer_contacts.contact_id 
							WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$order->f('customer_id')."' ORDER BY customer_contacts.lastname")->getAll();
		}
		if($order->f('customer_id') && !$order->f('contact_id')){
			$customer = $db->query("SELECT customers.name as name,customers.c_email as email
					FROM customers
					WHERE 1=1 $filter ");
			$customer_exist = true;


		}else{
			$customer = $db->query("SELECT customers.name as name,customers.c_email as email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 $filter ");
		}
		$items2 = array(
			    'id' => $customer->f('email'), 
			    'label' => $customer->f('name'),
			    'value' => $customer->f('name').' - '.$customer->f('email'),
		);
		if($invoice_email_type==1 && $order->f('customer_id')){
			$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname, customers.name,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 AND customer_contacts.active='1' AND customers.customer_id='".$order->f('customer_id')."'  ORDER BY lastname ")->getAll();
		}
		$contacts_exist = false;
		foreach ($contacts as $key => $value) {

			if($value['name']){
				$name = $value['firstname'].' '.$value['lastname'].' > '.$value['name'];
			}else{
				$name = $value['firstname'].' '.$value['lastname'];
			}
			$items[] = array(
			    'id' => $value['email'], 
			    'label' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
			    'value' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)).' - '.$value['email'],
			);
			$contacts_exist = true;
			if($value['contact_id']==$order->f('contact_id')){
				$contact_email=$value['email'];
			}
		}

		/*if($invoice_email_type==1){
			if(!$order->f('contact_id')){
				//array_push($o['recipients_ids'], $items2['id']);
			}else{
				array_push($o['recipients_ids'], $contact_email);
			}
		}else{
			if($c_email){
				$o['recipients_ids'] = array();
				array_push($o['recipients_ids'],$c_email);
			}
		}*/
		
		if($order->f('contact_id')){
			if($order->f('customer_id')){
				$contact_email = $db->field("SELECT email FROM customer_contactsIds WHERE contact_id='".$order->f('contact_id')."' AND customer_id = '".$order->f('customer_id')."'");
			}else{
				$contact_email = $db->field("SELECT email FROM customer_contactsIds WHERE contact_id='".$order->f('contact_id')."'");
			}
			if($contact_email){
				$cc_email = $contact_email;
			}else{
				$customer_email = $db->field("SELECT c_email FROM customers WHERE customer_id='".$order->f('customer_id')."'");
				if($customer_email){
					$cc_email = $customer_email;
				}
			}
		}else{
			$customer_email = $db->field("SELECT c_email FROM customers WHERE customer_id='".$order->f('customer_id')."'");
			if($customer_email){
				$cc_email = $customer_email;
			}
		}

		if($cc_email){
			$o['recipients_ids'] = $cc_email;
		}

		$added = false;
		foreach ($items as $key => $value) {
			if($value['id'] == $items2['id']){
				$added = true;
			}
		}
		if(!$added){
			array_push($items, $items2);
		}
		if($invoice_email_type==1 && $order->f('customer_id')){
			array_push($items, $items2);
		}/*else{	
			if($c_email){
				array_push($items, array(
					'id' => $c_email, 
					'label' =>$c_email,
					'value' => $c_email,
				));
			}
		}*/
	//$o['contact_email_data'] 			= $invoice_email;
/*	$o['recipients'] = array_map(function($field){
										return $field['id'];
									},$items);*/

	$o['recipients'] =$items;
	$o['save_disabled'] 			= sizeof($o['recipients_ids'])? false: true;
/*	array_push($o['recipients'], array(
		'recipient_email' 			=> $items
	));*/
	$o['contact_id_data']				= $customer_id;
	$o['contact_name_data']			= $invoice_email;
	$o['auto_complete_small']			= '<input type="text" class="auto_complete_small" name="email_addr" value="" data-autocomplete_file="'.$autocomplete_path.'" />';
	$o['autocomplete_file']			= $autocomplete_path;




/*$view->assign(array(
	'autocomplete_file'			=> $autocomplete_path,
));*/
if($j == 0){
	$o["hide_send_to"]="";
}
else{
	$o["hide_send_to"]="hide";
}

//sent box
$o["view_sent"]="style='display:none;'";


$id= $customer_id;
$is_contact = 0;
if($is_contacts){
	$is_contact = 1;
	$id = $contact_id;
}
$is_file = false;
//attached files
$f = $db->query("SELECT * FROM attached_files WHERE type='3'");
$o['files']=array();
while ($f->next()) {
	$path = $f->f('path');
	$p = substr($f->f('path'), -1);
	if($p != '/'){
		$path = $f->f('path').'/';
	}
	$file = array(
		'file'		=> $f->f('name'),
		'checked'	=> $f->f('default') == 1 ? true : false,
		'file_id'	=> $f->f('file_id'),
		'path'		=> $path,
		);
	// $view->loop('files_row');
	array_push($o['files'], $file);
	$is_file = true;
}

$drop_info = array('drop_folder' => 'orders', 'customer_id' => $id, 'item_id' => $in['order_id'], 'isConcact' => $is_contact, 'serial_number' => $serial_number);

//$user_email = $dbu_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."' ");
$user_email = $dbu_users->field("SELECT email FROM users WHERE user_id= :user_id ",['user_id'=>$_SESSION['u_id']]);


$show_for_one_user = true;
if(defined('PAYMILL_CLIENT_ID') && (defined('HOW_MANY') && HOW_MANY == 0) ) {
    $show_for_one_user = false;
}

//$margin=$discount_percent;

//$margin_percent=($margin/($total_no_tax))*100;
//$margin_percent=($margin/($total_no_tax-$discount_total))*100;
$margin_percent= 0;
if(defined('PROFIT_MARGIN_TYPE') && PROFIT_MARGIN_TYPE == 1){
	if($purchase_amount){
		$margin_percent=($margin/$purchase_amount)*100;
	}elseif($margin){
		$margin_percent=100;
	}
}else{
	if($total_no_tax-$discount_total){
		$margin_percent=($margin/($total_no_tax-$discount_total))*100;
	}elseif($margin){
		$margin_percent=100;
	}
}

$downpayment_data=$db->query("SELECT tbldownpayments.invoice_id,tblinvoice.serial_number FROM tbldownpayments 
			INNER JOIN tblinvoice ON tbldownpayments.invoice_id=tblinvoice.id
			WHERE tbldownpayments.order_id='".$in['order_id']."' AND tblinvoice.f_archived='0' ");

	$o['downpayment_made']				=$downpayment_data->f('invoice_id') ? true : false;
	$o['downpayment_invoice']			=$downpayment_data->f('invoice_id');
	$o['downpayment_sn']				=$downpayment_data->f('serial_number');
	$o['ADV_PRODUCT']					= ADV_PRODUCT==1 ? true : false;
  	$o['create_purchase_order_link']    = 'index.php?do=order-npo_order-order-add_po_order_direct&customer_id='.$supplier_id.'&order_id='.$in['order_id'];
	$o['is_file']				        = $is_file;
 	$o['s_date']				  	    = $in['s_date'];
  	$o['extra_info']				    = $extra_info;
	$o['is_extra']				   		= $is_extra;
	$o['sent_date']						= date(ACCOUNT_DATE_FORMAT,$in['s_date']);
	$o['hide_activity']			    	= $_SESSION['access_level'] == 1 ? '' : 'hide';
	$o['page_title']	    	    	= $page_title;
	$o['pick_date_format']          	= pick_date_format();
	$o['search']						= $in['search']? '&search='.$in['search'] : '';
	$o['view']							= $in['view']? '&view='.$in['view'] : '';
	$o['total_hide']					= $currency_type ? ($currency_type != ACCOUNT_CURRENCY_TYPE ? true:false) : false;
	$o['currency_type2']				= $currency_type;
	$o['is_delivered']					= $is_delivered;
	// 'discount_percent'			=> count($disc_array) == 1 ? '( '.$disc_array[0].'% )' : "",
	$o['sh_discount']					= $in['apply_discount'] == 0 || $in['apply_discount'] == 2 ? false : true;
	$o['sh_discount1']					= $in['apply_discount'] < 2 ? false : true;
	$o['is_deliveredd']					= $db->f('invoiced') ? false : (!$fully_delivered ? true : false);
	$o['is_vat']						= $remove_vat == 1 ? false : true;
	$o['drop_info']						= $drop_info;
	// 'is_customer'				=> $customer_id ? true : false,
	$o['is_customer']					= true;
	$o['is_drop']						= defined('DROPBOX') && (DROPBOX != '') ? true : false;
	$o['from_import']					= $from_import? true:false;
	$o['style']     					= ACCOUNT_NUMBER_FORMAT;
	$o['del_two_steps']					= ORDER_DELIVERY_STEPS == 2 ? 1 : 0;
	$o['not_fully_delivered']			= !$fully_delivered ? true : false;
	$o['attach_drop_files']				= ALLOW_ORDER_DROP_BOX;
	$o['is_profi']                      = $is_profi;
	$o['margin_value']                  = place_currency( display_number($margin),$currency);
	$o['margin_percent']                = display_number($margin_percent);
	$o['purchase_amount']               = display_number($total_no_tax-$margin);
	$o['user_email'] 					= $user_email;
	$o['show_for_one_user'] 			= $show_for_one_user;

	$o['invoice_link']			= 'index.php?do=invoice-ninvoice&base_type=3&'.$o['link_b_text'].'='.$o['buyer_id'].'&'.$o['link_c_text'].'='.$o['order_buyer_name_encode'].'&orders_id='.$o['order_id'].'&languages='.$o['languages'].'&currency_type='.$o['currency_type2'].$o['total_orderer'].'&from_orders=1&type=0&your_ref='.$o['your_ref_encode'];
	$o['proforma_link']			= 'index.php?do=invoice-ninvoice&base_type=3&'.$o['link_b_text'].'='.$o['buyer_id'].'&'.$o['link_c_text'].'='.$o['order_buyer_name_encode'].'&orders_id='.$o['order_id'].'&currency_type='.$o['currency_type2'].'&languages='.$o['languages'].'&type=1'.$o['whole_quantity'].'&your_ref='.$o['your_ref_encode'];

	
	$o['po_order_link']			= 'index.php?do=order-po_orders&add=true&order_id='.$o['order_id'].'&languages='.$o['languages'].'&currency_type='.$o['currency_type2'];

	$post_library=array();
/*	if($order->f('postgreen_id')!=''){  	
    	include_once('apps/order/model/order.php');
		$postg=new order(); 
		$post_status=json_decode($postg->postOrderData($in));
    	$o['postg_status']		= $post_status->status;
    }
    if($post_active){
    	$in['ret_res']=true;
    	$post_library=include_once('apps/misc/controller/post_library.php');
    }*/
    $o['post_library']=$post_library;

    $mail_setting_option = $db->field("SELECT value from settings where constant_name='MAIL_SETTINGS_PREFERRED_OPTION'");
	$o['sendgrid_selected'] = false;
	if($mail_setting_option==3){
		$o['sendgrid_selected'] =true;
	}

$db->query("UPDATE pim_orders SET margin='".$margin."',margin_percent= '".$margin_percent."' WHERE  pim_orders.order_id='".$in['order_id']."'");

json_out($o);
?>