<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

	ini_set('memory_limit', '1000M');
	$in['orders_id'] ='';
	if($_SESSION['add_to_pdf_o']){
	  foreach ($_SESSION['add_to_pdf_o'] as $key => $value) {
	    $in['orders_id'] .= $key.',';
	  }
	}
    if(isset($_SESSION['tmp_add_to_pdf_o'])){
        unset($_SESSION['tmp_add_to_pdf_o']);
    }
    if(isset($_SESSION['add_to_pdf_o'])){
        unset($_SESSION['add_to_pdf_o']);
    }
	$in['orders_id'] = rtrim($in['orders_id'],',');
	if(!$in['orders_id']){
	  $in['orders_id']= '0';
	}

	$headers = array("SERIAL NUMBER",
	    "CUSTOMER NAME",
	    "DELIVERY ADDRESS",
	    "DELIVERY ZIP",
	    "DELIVERY CITY",
	    "DELIVERY COUNTRY",
	    "DATE",
	    "DELIVERY DATE",
	    "OUR REFERENCE",
	    "YOUR REFERENCE",
	    "ARTICLE CODE",
	    "ARTICLE DESCRIPTION",
	    "QUANTITY",
	    "DELIVERED QUANTITY",
	    "UNIT PRICE",
	    "TOTAL",
	    "EMAIL",
	    "PHONE",
	    "PURCHASE PRICE",
        "PURCHASE ORDERS",
	    "DEFAULT_SUPPLIER",
	    "ARTICLE SERIAL NUMBERS",
	    "CONTACT EMAIL",
	    "CONTACT PHONE"
	);
	$db = new sqldb();
    $filename ="export_orders.csv";
	$contacts=array();
	$final_data=array();
	$order_steps = $db->field("SELECT value FROM settings WHERE constant_name='ORDER_DELIVERY_STEPS'");

	$order_data=$db->query("SELECT pim_order_articles.*,pim_orders.order_id,pim_orders.serial_number,pim_orders.customer_name,pim_orders.date,pim_orders.del_date,pim_orders.our_ref,pim_orders.your_ref,pim_articles.item_code,
		SUM(pim_orders_delivery.quantity) as delivered_q,pim_order_deliveries.delivery_done,customer_addresses.address,customer_addresses.zip,customer_addresses.city,country.name AS country_name,pim_orders.customer_id,pim_orders.contact_id,pim_article_prices.purchase_price,pim_articles.supplier_name,GROUP_CONCAT(pim_order_deliveries.delivery_id) as delivery_ids,pim_articles.use_serial_no
		FROM pim_orders 
		LEFT JOIN pim_order_articles ON pim_orders.order_id=pim_order_articles.order_id 
		LEFT JOIN pim_articles ON pim_order_articles.article_id=pim_articles.article_id
		LEFT JOIN pim_orders_delivery ON pim_order_articles.order_id=pim_orders_delivery.order_id AND pim_order_articles.order_articles_id=pim_orders_delivery.order_articles_id
		LEFT JOIN pim_order_deliveries ON pim_orders_delivery.delivery_id=pim_order_deliveries.delivery_id
		LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.base_price='1'
		LEFT JOIN customer_addresses ON customer_addresses.address_id=(CASE WHEN pim_orders.delivery_address_id>'0' THEN pim_orders.delivery_address_id ELSE pim_orders.main_address_id END)
		LEFT JOIN country ON customer_addresses.country_id=country.country_id
		WHERE pim_order_articles.article_id!='0' AND pim_order_articles.tax_for_article_id='0' AND pim_order_articles.order_id IN (".$in['orders_id'].") GROUP BY pim_orders.order_id,pim_order_articles.order_articles_id ");
	
	while($order_data->next()){
		$contact_email="";
		$contact_phone="";
		if($order_data->f('customer_id') && $order_data->f('contact_id')){
			if(array_key_exists($order_data->f('customer_id').'-'.$order_data->f('contact_id'), $contacts) === false){
				$contacts[$order_data->f('customer_id').'-'.$order_data->f('contact_id')]=array('contact_email'=>'','contact_phone'=>'');
				$contacts_data=$db->query("SELECT * FROM customer_contactsIds WHERE customer_id='".$order_data->f('customer_id')."' AND contact_id='".$order_data->f('contact_id')."' ");
				$contacts_data->next();
				if($contacts_data->f('email')){
					$contact_email=$contacts_data->f('email');
					$contacts[$order_data->f('customer_id').'-'.$order_data->f('contact_id')]['contact_email']=$contacts_data->f('email');
				}
				if($contacts_data->f('phone')){
					$contact_phone=$contacts_data->f('phone');
					$contacts[$order_data->f('customer_id').'-'.$order_data->f('contact_id')]['contact_phone']=$contacts_data->f('phone');
				}
			}else{
				$contact_email=$contacts[$order_data->f('customer_id').'-'.$order_data->f('contact_id')]['contact_email'];
				$contact_phone=$contacts[$order_data->f('customer_id').'-'.$order_data->f('contact_id')]['contact_phone'];
			}		
		}
		$company_email="";
		$company_phone="";
		if($order_data->f('customer_id')){
			$customer_data=$db->query("SELECT c_email, comp_phone FROM customers WHERE customer_id='".$order_data->f('customer_id')."' ");	
			$company_email=$customer_data->f('c_email');
			$company_phone=$customer_data->f('comp_phone');
		}		

		$delivered_q = (($order_data->f('delivery_done') && $order_steps=='2' ) || $order_steps!='2') ? display_number($order_data->f('delivered_q'), 2, '.', '') : 0;

		$art_serial_number_list = '';
		if($order_data->f('delivery_ids') && $order_data->f('use_serial_no')){
			$serial_number_data = $db->query("SELECT serial_number FROM serial_numbers WHERE delivery_id IN (".$order_data->f('delivery_ids').") AND article_id = '".$order_data->f('article_id')."' ");
            while ($serial_number_data->next()) {
                $art_serial_number_list .=$serial_number_data->f('serial_number').",";
            }
            $art_serial_number_list=rtrim($art_serial_number_list,",");
		}
        $purchase_order_s_nr="";

        $purchase_orders = $db->query("SELECT pim_p_orders.serial_number FROM tracking_line 
                INNER JOIN tracking ON tracking_line.trace_id = tracking.trace_id 
                INNER JOIN pim_p_orders ON tracking.target_id = pim_p_orders.p_order_id 
                LEFT JOIN pim_p_order_deliveries ON pim_p_orders.p_order_id = pim_p_order_deliveries.p_order_id 
                WHERE tracking_line.origin_id ='".$order_data->f('order_id')."' AND tracking_line.origin_type ='4' AND tracking.target_type ='6' GROUP BY pim_p_orders.p_order_id")->getAll();
        foreach($purchase_orders as $purchase_order){
            $purchase_order_s_nr.=$purchase_order['serial_number']." ";
        }
        $purchase_order_s_nr=rtrim($purchase_order_s_nr);
		$tmp_line=array(
			$order_data->f('serial_number'),
			stripslashes($order_data->f('customer_name')),
			stripslashes(preg_replace("/[\n\r]/"," ",$order_data->f('address'))),
			"\t".stripslashes($order_data->f('zip')),
			stripslashes($order_data->f('city')),
			stripslashes($order_data->f('country_name')),
			date(ACCOUNT_DATE_FORMAT,$order_data->f('date')),
			($order_data->f('del_date')?date(ACCOUNT_DATE_FORMAT,$order_data->f('del_date')):''),
			stripslashes(preg_replace("/[\n\r]/"," ",$order_data->f('our_ref'))),
			stripslashes(preg_replace("/[\n\r]/"," ",$order_data->f('your_ref'))),
			stripslashes($order_data->f('item_code')),
			stripslashes(preg_replace("/[\n\r]/"," ",$order_data->f('article'))),
            display_number_exclude_thousand($order_data->f('quantity')),
			$delivered_q,
            display_number_exclude_thousand($order_data->f('price')),
            display_number_exclude_thousand($order_data->f('quantity')*$order_data->f('price')),
			"\t".$company_email,
			"\t".$company_phone,
            display_number_exclude_thousand($order_data->f('purchase_price')),
            $purchase_order_s_nr,
			stripslashes(preg_replace("/[\n\r]/"," ",$order_data->f('supplier_name'))),
			$art_serial_number_list,
			"\t".$contact_email,
			"\t".$contact_phone
		);
		array_push($final_data,$tmp_line);
	}

    //modified in 25.11.2021
    header('Content-Type: application/excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
	//$from_location=INSTALLPATH.'upload/'.DATABASE_NAME.'/tmp_export_orders.csv';
    $fp = fopen("php://output", 'w');
    fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    doQueryLog();
    exit();

    // set column width to auto
    foreach(range('A','W') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
    }

    $objPHPExcel->getActiveSheet()->getStyle('G1:G'.$xlsRow)->getAlignment()->setWrapText(true);
    $rows_format='A1:A'.$xlsRow;
    $objPHPExcel->getActiveSheet()->getStyle($rows_format)
        ->getNumberFormat()
        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
    $objPHPExcel->getActiveSheet()->setTitle('Invoices list export');
    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('upload/'.$filename);





    define('ALLOWED_REFERRER', '');
    define('BASE_DIR','upload/');
    define('LOG_DOWNLOADS',false);
    define('LOG_FILE','downloads.log');
    $allowed_ext = array (

        // archives
        'zip' => 'application/zip',

        // documents
        'pdf' => 'application/pdf',
        'doc' => 'application/msword',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        'csv' => 'text/csv',

        // executables
        //'exe' => 'application/octet-stream',

        // images
        'gif' => 'image/gif',
        'png' => 'image/png',
        'jpg' => 'image/jpeg',
        'jpeg' => 'image/jpeg',

        // audio
        'mp3' => 'audio/mpeg',
        'wav' => 'audio/x-wav',

        // video
        'mpeg' => 'video/mpeg',
        'mpg' => 'video/mpeg',
        'mpe' => 'video/mpeg',
        'mov' => 'video/quicktime',
        'avi' => 'video/x-msvideo'
    );
    ####################################################################
    ###  DO NOT CHANGE BELOW
    ####################################################################

    // If hotlinking not allowed then make hackers think there are some server problems
    if (ALLOWED_REFERRER !== ''
        && (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
    ) {
        die("Internal server error. Please contact system administrator.");
    }
    $fname = basename($filename);
    function find_file ($dirname, $fname, &$file_path) {
        $dir = opendir($dirname);
        while ($file = readdir($dir)) {
            if (empty($file_path) && $file != '.' && $file != '..') {
                if (is_dir($dirname.'/'.$file)) {
                    find_file($dirname.'/'.$file, $fname, $file_path);
                }
                else {
                    if (file_exists($dirname.'/'.$fname)) {
                        $file_path = $dirname.'/'.$fname;
                        return;
                    }
                }
            }
        }

    } // find_file
    // get full file path (including subfolders)
    $file_path = '';
    find_file('upload', $fname, $file_path);
    if (!is_file($file_path)) {
        die("File does not exist. Make sure you specified correct file name.");
    }
    // file size in bytes
    $fsize = filesize($file_path);
    // file extension
    $fext = strtolower(substr(strrchr($fname,"."),1));
    // check if allowed extension
    if (!array_key_exists($fext, $allowed_ext)) {
        die("Not allowed file type.");
    }
    // get mime type
    if ($allowed_ext[$fext] == '') {
        $mtype = '';
        // mime type is not set, get from server settings
        if (function_exists('mime_content_type')) {
            $mtype = mime_content_type($file_path);
        }
        else if (function_exists('finfo_file')) {
            $finfo = finfo_open(FILEINFO_MIME); // return mime type
            $mtype = finfo_file($finfo, $file_path);
            finfo_close($finfo);
        }
        if ($mtype == '') {
            $mtype = "application/force-download";
        }
    }
    else {
        // get mime type defined by admin
        $mtype = $allowed_ext[$fext];
    }
    doQueryLog();
    // set headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Type: $mtype");
    header("Content-Disposition: attachment; filename=\"$fname\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . $fsize);
    // download
    //@readfile($file_path);
    $file = @fopen($file_path,"rb");
    if ($file) {
        while(!feof($file)) {
            print(fread($file, 1024*8));
            flush();
            if (connection_status()!=0) {
                @fclose($file);

                die();
            }
        }
        @fclose($file);
    }

    /*
    ark::loadLibraries(array('PHPExcel'));
	$objReader = PHPExcel_IOFactory::createReader('CSV');

	$objPHPExcel = $objReader->load($from_location);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	unlink($from_location);

	doQueryLog();

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="export_orders.xls"');
	header('Cache-Control: max-age=0');
	$objWriter->save('php://output');   
	exit();*/
?>