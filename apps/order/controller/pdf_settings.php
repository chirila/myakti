<?php
/* * **********************************************************************
 * @Author: MedeeaWeb Works                                              *
 * ********************************************************************** */
if(!defined('BASEPATH')){ exit('No direct script access allowed'); }
$result = array( 'logo'=>array());
if(!$in['porder']){
    $tblinvoice = $db->query("SELECT pdf_logo, pdf_layout,email_language, identity_id FROM pim_orders WHERE order_id='".$in['order_id']."'");
    $tblinvoice->move_next();
    $result['order_id']=$in['order_id'];
    $set_logo = $tblinvoice->f('pdf_logo');
    if(!$set_logo){
        $set_logo = ACCOUNT_LOGO_ORDER;
    }

    $next_function = 'order-order-order-pdf_settings';

    if(ACCOUNT_LOGO_ORDER == '') {
        /*$view->assign('account_logo','../img/no-logo.png','logo');
        $view->assign('default','CHECKED','logo');
        $view->loop('logo');*/
        array_push($result['logo'], array(
            'account_logo'=>'images/no-logo.png',
            'default'=>'images/no-logo.png'
        ));
    }
    else {
        $patt = '{o_logo_img_}';
        $search = strpos(ACCOUNT_LOGO_ORDER,'o_logo_img_');
        if($search === false){
            $patt = '{'.str_replace('upload/'.DATABASE_NAME.'/','',ACCOUNT_LOGO_ORDER).',o_logo_img_}';
        }
        $logos = glob(__DIR__.'/../../../upload/'.DATABASE_NAME.'/'.$patt.'*',GLOB_BRACE);

        foreach ($logos as $v) {
            $logo = preg_replace('/\/(.*)\//', 'upload/'.DATABASE_NAME.'/', $v);
            $size = getimagesize($logo);
            $ratio = 250 / 77;
            if($size[0]/$size[1] > $ratio ){
                // $view->assign('attr', 'width="250"','logo');
                $attr = 'width="250"';
            }else{
                // $view->assign('attr', 'height="77"','logo');
                $attr = 'height="77"';
            }

            /*$view->assign(array(
                'account_logo'  => $logo,
                'default'       => $logo == $tblinvoice->f('pdf_logo') ? 'CHECKED' : '',
            ),'logo');

            if($tblinvoice->f('pdf_logo')=='0' ){
                $view->assign(array(
                    'default'       => $logo == ACCOUNT_LOGO_ORDER ? 'CHECKED' : ''
                ),'logo');
            }

            $view->loop('logo');*/
            array_push($result['logo'], array(
                'account_logo'=>$logo,
                'default'=>$set_logo,
                'attr' => $attr
            ));
        }
    }
    if(!$tblinvoice->f('pdf_layout')) {
      $selected=ACCOUNT_ORDER_PDF_FORMAT ? ACCOUNT_ORDER_PDF_FORMAT : '1';
    }else{
     $selected=$tblinvoice->f('pdf_layout');
    }
}else{
    $tblinvoice = $db->query("SELECT pdf_logo,p_order_type,pdf_layout, email_language,identity_id FROM pim_p_orders WHERE p_order_id='".$in['p_order_id']."'");
    $next_function = 'order-po_order-order-pdf_settings';
    $result['p_order_id']=$in['p_order_id'];

    if(ACCOUNT_LOGO_PO_ORDER == '') {
        $view->assign('account_logo','images/no-logo.png','logo');
        $view->assign('default','CHECKED','logo');
        $view->loop('logo');
    }
    else {
        $patt = '{po_logo_img_}';
        $search = strpos(ACCOUNT_LOGO_PO_ORDER,'po_logo_img_');
        if($search === false){
            $patt = '{'.str_replace('upload/'.DATABASE_NAME.'/','',ACCOUNT_LOGO_PO_ORDER).',po_logo_img_}';
        }
        $logos = glob(__DIR__.'/../../../upload/'.DATABASE_NAME.'/'.$patt.'*',GLOB_BRACE);



        foreach ($logos as $v) {
            $logo = preg_replace('/\/(.*)\//', 'upload/'.DATABASE_NAME.'/', $v);
            $size = getimagesize($logo);
            $ratio = 250 / 77;
            if($size[0]/$size[1] > $ratio ){
                $view->assign('attr', 'width="250"','logo');
            }else{
                $view->assign('attr', 'height="77"','logo');
            }

            $view->assign(array(
                'account_logo'  => $logo,
                'default'       => $logo == $tblinvoice->f('pdf_logo') ? 'CHECKED' : '',
            ),'logo');

            if($tblinvoice->f('pdf_logo')=='0' ){
                $view->assign(array(
                    'default'       => $logo == ACCOUNT_LOGO_PO_ORDER ? 'CHECKED' : ''
                ),'logo');
            }

            $view->loop('logo');
        }
    }
    if(!$tblinvoice->f('p_order_type')) {
      $selected=ACCOUNT_P_ORDER_PDF_FORMAT ? ACCOUNT_P_ORDER_PDF_FORMAT : '1';
    }else{
     $selected=$tblinvoice->f('p_order_type');
    }
    //console::log($tblinvoice->f('pdf_layout'));
}


$lng = $tblinvoice->f('email_language');
if(!$lng){ $lng = 1; }

if(!$tblinvoice->f('identity_id')) {
  $selected_id='0';
}else{
 $selected_id=$tblinvoice->f('identity_id');
}

// $view->assign(array(
$result['order_id']             = $in['order_id'];
$result['language_dd']          = build_language_admin_dd($lng);
$result['language_id']          = $lng;
$result['pdf_type_dd']          = build_pdf_type_dd($selected,false,false,false,true);
$result['pdf_type_id']          = $selected;
$result['multiple_identity']    = build_identity_dd($selected_id);
$result['multiple_identity_id'] = $selected_id;
$result['identity_id']          = $tblinvoice->f('identity_id');
$result['do_next']              = $next_function;
$result['default_logo']         = $set_logo;
$result['xls']                  = $in['xls'];
    // ));
json_out($result);
// return $view->fetch();