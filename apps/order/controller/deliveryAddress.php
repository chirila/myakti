<?php 
error_reporting(0);
if (!defined('INSTALLPATH')) {
	define('INSTALLPATH', '../../../');
}
if (!defined('BASEPATH')) {
	define('BASEPATH', INSTALLPATH . 'core/');
}
session_start();

include_once (INSTALLPATH . 'config/config.php');
include_once (BASEPATH . '/library/sqldb.php');
include_once (BASEPATH . '/library/sqldbResult.php');
include_once (INSTALLPATH . 'config/database.php');
include_once (BASEPATH . 'startup.php');

ini_set('display_errors', 1);
  ini_set('error_reporting', 1);
  // error_reporting(E_ALL);
  error_reporting(E_ALL ^ E_NOTICE);



$q = strtolower($in["term"]);
$filter = '';
if($q){
	$filter =" AND address LIKE '%".addslashes($q)."%'";
}

if($in['customer_id']){
$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
						 FROM customer_addresses
						 LEFT JOIN country ON country.country_id=customer_addresses.country_id
						 LEFT JOIN state ON state.state_id=customer_addresses.state_id
						 WHERE customer_addresses.customer_id='".$in['customer_id']."' {$filter}
						 ORDER BY customer_addresses.address_id limit 5");
}
else if($in['contact_id']){
   $arguments.="&contact_id=".$in['contact_id'];
   $address= $db->query("SELECT customer_contact_address.*,country.name AS country 
						 FROM customer_contact_address 
						 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
						 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  {$filter}
						 AND customer_contact_address.delivery='1' limit 5");
}

$addresses=array();
while($address->next() ){
  	$a = array(
	  	'address_id'	    => $address->f('address_id'),
	  	'address'			=> nl2br($address->f('address')),
	  	'zip'			    => $address->f('zip'),
	  	'city'			    => $address->f('city'),
	  	'state'			    => $address->f('state'),
	  	'country'			=> $address->f('country')
  	);
	array_push($addresses, $a);
}
array_push($addresses,array('address_id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));

json_out($addresses);
