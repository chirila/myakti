<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('memory_limit','786M');
setcookie('Akti-Order-Export','6-0-0',time()+3600,'/');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

global $config;

	$headers = array("SERIAL NUMBER",
	    "CUSTOMER NAME",
	    "DELIVERY ADDRESS",
	    "DELIVERY ZIP",
	    "DELIVERY CITY",
	    "DELIVERY COUNTRY",
	    "DATE",
	    "DELIVERY DATE",
	    "OUR REFERENCE",
	    "YOUR REFERENCE",
	    "ARTICLE CODE",
	    "ARTICLE DESCRIPTION",
	    "QUANTITY",
	    "DELIVERED QUANTITY",
	    "UNIT PRICE",
	    "TOTAL",
	    "EMAIL",
	    "PHONE"
	);
	$db = new sqldb();

	$contacts=array();
	$final_data=array();
	$order_steps = $db->field("SELECT value FROM settings WHERE constant_name='ORDER_DELIVERY_STEPS'");

	$l_r =ROW_PER_PAGE;

	$arguments = '';
	$filter = " WHERE pim_order_articles.article_id!='0' AND pim_order_articles.tax_for_article_id='0' ";

	$filter_article='';
	$filter_your_ref = '';
	//$order_by = " ORDER BY t_date DESC, iii DESC ";
	$order_by = " ORDER BY iii DESC, t_date DESC "; // requested by akti-3854

	if(!$in['archived']){
		$filter.= " AND pim_orders.active=1";
	}else{
		$filter.= " AND pim_orders.active=0";
		$arguments.="&archived=".$in['archived'];
	}
	if($in['customer_id']){
		$filter.= " AND pim_orders.customer_id='".$in['customer_id']."'";
		$arguments_c .= '&customer_id='.$in['customer_id'];
	}
	if($in['filter']){
		$arguments.="&filter=".$in['filter'];
	}
	if($in['search']){
		$filter.=" and (pim_orders.serial_number like '%".$in['search']."%' OR pim_orders.customer_name like '%".$in['search']."%' OR pim_orders.our_ref like '%".$in['search']."%' )";
		$arguments.="&search=".$in['search'];
	}

	if($in['article_name_search']){
		$filter_article.=" INNER JOIN pim_order_articles ON pim_order_articles.order_id=pim_orders.order_id
	                       INNER JOIN pim_articles ON pim_articles.article_id=pim_order_articles.article_id AND (pim_articles.internal_name like '%".$in['article_name_search']."%' OR pim_order_articles.article_code like '%".$in['article_name_search']."%')";

		$arguments.="&article_name_search=".$in['article_name_search'];
	}else{
		$filter_article.=" LEFT JOIN pim_order_articles ON pim_order_articles.order_id=pim_orders.order_id
	                       LEFT JOIN pim_articles ON pim_articles.article_id=pim_order_articles.article_id AND (pim_articles.internal_name like '%".$in['article_name_search']."%' OR pim_order_articles.article_code like '%".$in['article_name_search']."%')";

		$arguments.="&article_name_search=".$in['article_name_search'];
	}
	if($in['your_reference_search']){
		$filter_your_ref =" AND pim_orders.your_ref LIKE '%".$in['your_reference_search']."%' ";
		$arguments.="&your_reference_search=".$in['your_reference_search'];
	}
	if($in['order_by']){
		if(in_array($in['order_by'], $order_by_array)){
			$order = " ASC ";
			if($in['desc'] == '1' || $in['desc']=='true'){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
			$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		}
	}

	if(!isset($in['view'])){
		$in['view'] = 4;
	}
	$show_only_my = true;
	if(defined('SHOW_MY_ORDERS_ONLY') && SHOW_MY_ORDERS_ONLY == 1){
		if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])){
			$show_only_my = false;
			if($in['view'] == 0){
				$in['view'] = 4;
			}
			$filter.=" and ( pim_orders.created_by = '".$_SESSION['u_id']."' OR pim_orders.author_id='".$_SESSION['u_id']."' ) ";
		}
	}
		switch ($in['view']) {
			case '1':
				$filter.=" and pim_orders.rdy_invoice = '0' and pim_orders.sent='0' ";
				$arguments.="&view=1";
				break;
			case '2':
				$filter.=" and pim_orders.rdy_invoice != '1' and pim_orders.sent='1'";
				$arguments.="&view=2";
				break;
			case '3':
				$filter.=" and pim_orders.rdy_invoice = '1' ";
				$arguments.="&view=3";
				break;
			case '4':
				if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])) {
					$filter.=" and ( pim_orders.created_by = '".$_SESSION['u_id']."' OR pim_orders.author_id='".$_SESSION['u_id']."' ) ";
				}
				$arguments.="&view=4";
				break;
			case '5':
				$filter.=" and pim_orders.rdy_invoice = '2' ";
				$arguments.="&view=5";
			break;
			default:
				break;
		}

	if($in['start_date'] && $in['stop_date']){
		$filter.=" and pim_orders.date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
		$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
		$filter_link = 'none';
	}
	else if($in['start_date']){
		$filter.=" and cast(pim_orders.date as signed) > ".$in['start_date']." ";
		$arguments.="&start_date=".$in['start_date'];
	}
	else if($in['stop_date']){
		$filter.=" and cast(pim_orders.date as signed) < ".$in['stop_date']." ";
		$arguments.="&stop_date=".$in['stop_date'];
		$filter_link = 'none';
	}

	if($in['d_start_date'] && $in['d_stop_date']){
		$filter.=" and pim_orders.del_date BETWEEN '".$in['d_start_date']."' and '".$in['d_stop_date']."' ";
		$arguments.="&d_start_date=".$in['d_start_date']."&d_stop_date=".$in['d_stop_date'];
		$filter_link = 'none';
	}
	else if($in['d_start_date']){
		$filter.=" and cast(pim_orders.del_date as signed) > ".$in['d_start_date']." ";
		$arguments.="&d_start_date=".$in['d_start_date'];
	}
	else if($in['d_stop_date']){
		$filter.=" and cast(pim_orders.del_date as signed) < ".$in['d_stop_date']." ";
		$arguments.="&d_stop_date=".$in['d_stop_date'];
		$filter_link = 'none';
	}

	if($in['new_orders']==1) {
		$days_30 = time() - 30*3600*24;
		$now = time();
		$filter .= " AND pim_orders.rdy_invoice!=1 AND date BETWEEN ".$days_30." AND ".$now." ";
	}

	$arguments = $arguments.$arguments_c;
	
	$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;


	

	$order_data = $db->query("SELECT pim_order_articles.*,pim_orders.serial_number,pim_orders.customer_name,pim_orders.date,pim_orders.del_date,pim_orders.our_ref,pim_orders.your_ref,pim_articles.item_code, pim_orders.serial_number as iii, cast( FROM_UNIXTIME( pim_orders.`date` ) AS DATE ) AS t_date, SUM(pim_orders_delivery.quantity) as delivered_q,pim_order_deliveries.delivery_done,customer_addresses.address,customer_addresses.zip,customer_addresses.city,country.name AS country_name,pim_orders.customer_id,pim_orders.contact_id
		FROM pim_orders 
		".$filter_article."
		LEFT JOIN pim_orders_delivery ON pim_order_articles.order_id=pim_orders_delivery.order_id AND pim_order_articles.order_articles_id=pim_orders_delivery.order_articles_id
		LEFT JOIN pim_order_deliveries ON pim_orders_delivery.delivery_id=pim_order_deliveries.delivery_id
		LEFT JOIN customer_addresses ON customer_addresses.address_id=(CASE WHEN pim_orders.delivery_address_id>'0' THEN pim_orders.delivery_address_id ELSE pim_orders.main_address_id END)
		LEFT JOIN country ON customer_addresses.country_id=country.country_id
		
				 ".$filter."
				 ".$filter_your_ref." 
		 GROUP BY pim_orders.order_id,pim_order_articles.order_articles_id ".$order_by);
	
	while($order_data->next()){
		$contact_email="";
		$contact_phone="";
		if($order_data->f('customer_id') && $order_data->f('contact_id')){
			if(array_key_exists($order_data->f('customer_id').'-'.$order_data->f('contact_id'), $contacts) === false){
				$contacts[$order_data->f('customer_id').'-'.$order_data->f('contact_id')]=array('contact_email'=>'','contact_phone'=>'');
				$contacts_data=$db->query("SELECT * FROM customer_contactsIds WHERE customer_id='".$order_data->f('customer_id')."' AND contact_id='".$order_data->f('contact_id')."' ");
				$contacts_data->next();
				if($contacts_data->f('email')){
					$contact_email=$contacts_data->f('email');
					$contacts[$order_data->f('customer_id').'-'.$order_data->f('contact_id')]['contact_email']=$contacts_data->f('email');
				}
				if($contacts_data->f('phone')){
					$contact_phone=$contacts_data->f('phone');
					$contacts[$order_data->f('customer_id').'-'.$order_data->f('contact_id')]['contact_phone']=$contacts_data->f('phone');
				}
			}else{
				$contact_email=$contacts[$order_data->f('customer_id').'-'.$order_data->f('contact_id')]['contact_email'];
				$contact_phone=$contacts[$order_data->f('customer_id').'-'.$order_data->f('contact_id')]['contact_phone'];
			}		
		}
		$company_email="";
		$company_phone="";
		if($order_data->f('customer_id')){
			$customer_data=$db->query("SELECT c_email, comp_phone FROM customers WHERE customer_id='".$order_data->f('customer_id')."' ");	
			$company_email=$customer_data->f('c_email');
			$company_phone=$customer_data->f('comp_phone');
		}		

		$delivered_q = (($order_data->f('delivery_done') && $order_steps=='2' ) || $order_steps!='2') ? number_format($order_data->f('delivered_q'), 2, '.', '') : 0;
		
		$tmp_line=array(
			$order_data->f('serial_number'),
			stripslashes($order_data->f('customer_name')),
			stripslashes(preg_replace("/[\n\r]/"," ",$order_data->f('address'))),
			"\t".stripslashes($order_data->f('zip')),
			stripslashes($order_data->f('city')),
			stripslashes($order_data->f('country_name')),
			date(ACCOUNT_DATE_FORMAT,$order_data->f('date')),
			($order_data->f('del_date')?date(ACCOUNT_DATE_FORMAT,$order_data->f('del_date')):''),
			stripslashes(preg_replace("/[\n\r]/"," ",$order_data->f('our_ref'))),
			stripslashes(preg_replace("/[\n\r]/"," ",$order_data->f('your_ref'))),
			stripslashes($order_data->f('item_code')),
			stripslashes(preg_replace("/[\n\r]/"," ",$order_data->f('article'))),
			number_format($order_data->f('quantity'), 2, '.', ''),
			$delivered_q,
			number_format($order_data->f('price'), 2, '.', ''),
			number_format($order_data->f('quantity')*$order_data->f('price'), 2, '.', ''),
			"\t".$company_email,
			"\t".$company_phone
		);
		array_push($final_data,$tmp_line);
	}

	$filename = 'export_orders.csv';
	doQueryLog();
   $from_location='upload/'.DATABASE_NAME.'/export_orders_'.time().'.csv';

    header('Content-Type: application/excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $fp = fopen('php://output', 'w');
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);
    exit();

	/*$from_location=INSTALLPATH.'upload/'.DATABASE_NAME.'/tmp_export_orders.csv';
    $fp = fopen($from_location, 'w');
    fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    fputcsv($fp, $headers);
    foreach ( $final_data as $line ) {
        fputcsv($fp, $line);
    }
    fclose($fp);

    ark::loadLibraries(array('PHPExcel'));
	$objReader = PHPExcel_IOFactory::createReader('CSV');

	$objPHPExcel = $objReader->load($from_location);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	unlink($from_location);

	doQueryLog();

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="export_orders.xls"');
	header('Cache-Control: max-age=0');
	$objWriter->save('php://output');  
	exit();*/ 

?>