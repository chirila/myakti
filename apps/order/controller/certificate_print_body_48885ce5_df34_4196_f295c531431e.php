<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$html='certificate_print_3.html';

$db2=new sqldb();
$db3 = new sqldb(); 
$db4 = new sqldb();
$db_date=new sqldb();

global $database_config,$p_access;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'], 
);

$db_users = new sqldb($db_config); 

$view = new at(ark::$viewpath.$html);
if(!$in['order_id']){ //sample
	//assign dummy data
$height = 251;
	$factur_date = date(ACCOUNT_DATE_FORMAT,  strtotime('now'));

	$pdf->setQuotePageLabel(geto_label_txt('page',$in['lid']));

	$view->assign(array(
	'account_logo'        => '../../img/no-logo.jpg',
	'billing_address_txt' => geto_label_txt('billing_address',$in['lid']),
	'order_note_txt'      => geto_label_txt('order_note',$in['lid']),
	'order_txt'           => $in['deliv_preview'] ? geto_label_txt('delivery_note',$in['lid']) : geto_label_txt('order',$in['lid']),
	'invoice_txt'         => geto_label_txt('invoice',$in['lid']),
	'date_txt'            => geto_label_txt('date',$in['lid']),
	'customer_txt'        => geto_label_txt('customer',$in['lid']),
	'article_txt'         => geto_label_txt('article',$in['lid']),
	'unitmeasure_txt'     => geto_label_txt('unitmeasure',$in['lid']),
	'quantity_txt'        => geto_label_txt('quantity',$in['lid']),
	'unit_price_txt'      => geto_label_txt('unit_price',$in['lid']),
	'amount_txt'          => geto_label_txt('amount',$in['lid']),
	'subtotal_txt'        => geto_label_txt('subtotal',$in['lid']),
	'discount_txt'        => geto_label_txt('discount',$in['lid']),
	'vat_txt'             => geto_label_txt('vat',$in['lid']),
	'payments_txt'        => geto_label_txt('payments',$in['lid']),
	'amount_due_txt'      => geto_label_txt('amount_due',$in['lid']),
	'notes_txt'           => geto_label_txt('notes',$in['lid']),
	//	'duedate'			  => geto_label_txt('duedate',$in['lid']),
	'bankd_txt'			  => geto_label_txt('bank_details',$in['lid']),
	'bank_txt'			  => geto_label_txt('bank_name',$in['lid']),
	'bic_txt'			  => geto_label_txt('bic_code',$in['lid']),
	'iban_txt'			  => geto_label_txt('iban',$in['lid']),
	'our_ref_txt'		  => geto_label_txt('our_ref',$in['lid']),
	'your_ref_txt'		  => geto_label_txt('your_ref',$in['lid']),
	'phone_txt'			  => geto_label_txt('phone',$in['lid']),
	'fax_txt'			  => geto_label_txt('fax',$in['lid']),
	'url_txt'			  => geto_label_txt('url',$in['lid']),
	'email_txt'			  => geto_label_txt('email',$in['lid']),
	'vat_number_txt' 	  => geto_label_txt('vat_number',$in['lid']),
	'vat_number_txt' 	  => geto_label_txt('vat_number',$in['lid']),
	'shipping_price_txt'  => geto_label_txt('shipping_price',$in['lid']),
	'article_code_txt'    => geto_label_txt('article_code',$in['lid']),
	'delivery_note_txt'    => geto_label_txt('delivery_note',$in['lid']),
	'delivery_note'		=> '[Delivery note]',
	'hide_our_ref'		  => '',
	'hide_your_ref'		  => '',

	'seller_name'         => '[Account Name]',
	'seller_d_country'    => '[Country]',
	'seller_d_state'      => '[State]',
	'seller_d_CITY'       => '[City]',
	'seller_d_zip'        => '[Zip]',
	'seller_d_address'    => '[Address]',
	'seller_b_country'    => '[Billing Country]',
	'seller_b_state'      => '[Billing State]',
	'seller_b_city'       => '[Billing City]',
	'seller_b_zip'        => '[Billing Zip]',
	'seller_b_address'    => '[Billing Address]',
	'serial_number'       => '####',
	'order_date'          => $factur_date,
	'order_vat'           => '##',
	'order_vat_no'		  => '[Vat Number]',
	'buyer_name'       	  => '[Customer Name]',
	'buyer_country'    	  => '[Customer Country]<br>',
	'buyer_state'      	  => '[Customer State]',
	'buyer_city'       	  => '[Customer City]<br>',
	'buyer_zip'           => '[Customer ZIP]',
	'buyer_address'    	  => '[Customer Address]<br>',
	'seller_d_country'    => '[Country]',
	'seller_d_state'      => '[State]',
	'seller_d_city'       => '[City]',
	'seller_d_zip'        => '[Zip]',
	'seller_d_address'    => '[Address]',
	'delivery_address'	  => '[Delivery Address]',
	'your_ref'       	  => '[Your Reference]',
	'our_ref' 			  => '[Our reference]',
	'notes'            	  => '[NOTES]',
	'bank'				  => '[Bank name]',
	'bic'				  => '[BIC]',
	'iban'				  => '[IBAN]',
	//'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	'seller_b_fax'        => '[Account Fax]',
	'seller_b_email'      => '[Account Email]',
	'seller_b_phone'      => '[Account Phone]',
	'seller_b_url'        => '[Account URL]',

	'hide_f'			  => true,
	'hide_e'			  => true,
	'hide_p'			  => true,
	'hide_u'			  => true,
	'is_delivery'		  => true,
	'sh_discount'		  => 'hide',
	'is_delivery_date'	  => true,
	'delivery_date' 		  => date(ACCOUNT_DATE_FORMAT,  (time()+(3600*24*30))),
	'delivery_date_txt'   	  => geto_label_txt('delivery_date',$in['lid']),
	'width_if_del'		  => 40,
	'address_info'	      => '[Address]<br>[Zip][City]<br>[Country]<br>',
	'buyer_email'		=> '[Email]<br>',
	'buyer_phone'		=> '[Phone]<br>',
	'buyer_fax'			=> '[Fax]',
	));
	$i=0;
	while ($i<3)
	{
		$shipping_price = 9;
		$vat_total = 5;
		$total = 63;
		$total_with_vat = 68+$shipping_price;
		$view->assign(array(
			'article'  			    	=> 'Article Name',
			'quantity'      			=> display_number(2),
			'price_vat'      			=> display_number(21),
			'price'         			=> display_number_var_dec(20),
			'percent'					=> display_number(5),
			'content'					=> true,
			'vat_value'					=> display_number(1),
			'is_article_code'	  		=> true,
			'delivery'				=> $in['deliv_preview'] ? false : true,
			'q_class'			  => $in['deliv_preview'] ? 'last right' : '',

		),'order_row');

		if(in_array($in['type'], $pdf_type)){
			if($in['deliv_preview']){
				$view->assign('article_width',68,'order_row');
			}else{
				$view->assign('article_width',41,'order_row');
			}
		}

		$view->loop('order_row');
		$i++;

	}
	$view->assign(array(
		'vat_total'				=> display_number($vat_total),
		'total'					=> display_number($total),
		'total_vat'				=> display_number($total_with_vat),
		'delivery'				=> $in['deliv_preview'] ? false : true,
		'hide_currency2'		=> ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide',
		'hide_currency1'		=> ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide',
		'currency_type'			=> ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$',
		'hide_default_total'	=> hide,
		'is_free_delivery'		=> true,
		'shipping_price'		=> display_number($shipping_price),
	));




}else{
	$db = new sqldb();
	$db->query("SELECT pim_orders.*, multiple_identity.*,pim_order_articles.vat_percent FROM pim_orders
			LEFT JOIN multiple_identity ON multiple_identity.identity_id=pim_orders.identity_id
			LEFT JOIN pim_order_articles ON pim_orders.order_id=pim_order_articles.order_id
			WHERE pim_orders.order_id='".$in['order_id']."'");
	$db->move_next();

	$serial_number = $db->f('serial_number');
   
	$factur_date = date(ACCOUNT_DATE_FORMAT,  $db->f('date'));
	
	//$img = '../../img/no-logo.png';
	
	$img = 'upload/'.DATABASE_NAME.'/raymakers-logo.jpg';
	
	$db_users->query("SELECT * from users WHERE user_id='".$db->f('acc_manager_id')."'");
	$db_users->move_next();

	$img_sign= 'upload/'.DATABASE_NAME.'/signature.jpg';

	$buyer_country = get_country_name($db->f('customer_country_id'));
	
	$pdf->setQuotePageLabel('salesassist_2');

	$text = getc_label_txt('text',$in['lid']);
	//$manager_name = $db->f('acc_manager_name');
	$manager_name = 'I. De Vroey';
	$seller_name = $db->f('identity_id')? $db->f('company_name') : $db->f('seller_name');
	$seller_address = $db->f('identity_id')? nl2br($db->f('company_address')) : nl2br(utf8_decode($db->f('seller_d_address')));
	$text = str_replace('{manager_name}', $manager_name, $text);
	$text = str_replace('{seller_name}', $seller_name, $text);
	$text = str_replace('{seller_address}', $seller_address, $text);

	if ($in['lid']==1){ 
		$seller_d_city='Antwerp';
		}elseif($in['lid']==2) {
				$seller_d_city='Anvers';
			}else{
					$seller_d_city='Antwerpen';
				}

	$view->assign(array(
	'account_logo'        => $img,
	'attr'				  => $attr,
	'signature'			  => $img_sign,
	'attr_sign'			  =>$attr_sign,
	
	'billing_address_txt' => geto_label_txt('billing_address',$in['lid']),
	'order_note_txt'      => geto_label_txt('order_note',$in['lid']),
	'order_txt'           => $in['delivery_id'] ? geto_label_txt('delivery_note',$in['lid']) : geto_label_txt('order',$in['lid']),
	'date_txt'            => geto_label_txt('date',$in['lid']),
	'customer_txt'        => geto_label_txt('customer',$in['lid']),
	'article_txt'         => geto_label_txt('article',$in['lid']),
	'unitmeasure_txt'     => geto_label_txt('unitmeasure',$in['lid']),
	'quantity_txt'        => geto_label_txt('quantity',$in['lid']),
	'unit_price_txt'      => geto_label_txt('unit_price',$in['lid']),
	'amount_txt'          => geto_label_txt('amount',$in['lid']),
	'subtotal_txt'        => geto_label_txt('subtotal',$in['lid']),
	'discount_txt'        => geto_label_txt('discount',$in['lid']),
	'vat_txt'             => geto_label_txt('vat',$in['lid']),
	'payments_txt'        => geto_label_txt('payments',$in['lid']),
	'total_txt'      	  => geto_label_txt('total',$in['lid']),
	'notes_txt'           => $notes ? geto_label_txt('notes',$in['lid']) : '',
	'duedate_txt'		  => geto_label_txt('duedate',$in['lid']),
	'bankd_txt'			  => geto_label_txt('bank_details',$in['lid']),
	'bank_txt'			  => geto_label_txt('bank_name',$in['lid']),
	'bic_txt'			  => geto_label_txt('bic_code',$in['lid']),
	'iban_txt'			  => geto_label_txt('iban',$in['lid']),
	'gov_taxes_txt'       => geto_label_txt('gov_taxes',$in['lid']),
	'gov_taxes_code_txt'  => geto_label_txt('gov_taxes_code',$in['lid']),
	'gov_taxes_type_txt'  => geto_label_txt('gov_taxes_type',$in['lid']),
	'sale_unit_txt'		  => wrap(geto_label_txt('sale_unit',$in['lid']), 9, ' '),
	'package_txt'		  => wrap(geto_label_txt('package',$in['lid']), 9, ' '),
	'our_ref_txt'		  => geto_label_txt('our_ref',$in['lid']),
	'your_ref_txt'		  => geto_label_txt('your_ref',$in['lid']),
	'phone_txt'			  => get_label_txt('phone',$in['lid']),
	'fax_txt'			  => get_label_txt('fax',$in['lid']),
	'url_txt'			  => get_label_txt('url',$in['lid']),
	'email_txt'			  => get_label_txt('email',$in['lid']),
	'vat_number_txt' 	  => get_label_txt('vat_number',$in['lid']),
	'delivery_note_txt'	  => geto_label_txt('delivery_note',$in['lid']),
	'shipping_price_txt'  => geto_label_txt('shipping_price',$in['lid']),
	'article_code_txt'    => geto_label_txt('article_code',$in['lid']),
	'delivery_date_txt'   => geto_label_txt('delivery_date',$in['lid']),
	'pick_up_from_store_txt' => geto_label_txt('pick_up_from_store',$in['lid']),
	
	
	// 'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	'serial_number'		  => $serial_number,
	'order_date'          => $factur_date,
	'buyer_name'       	  => $contact_title2.$db->f('customer_name'),
	'buyer_country'    	  => $buyer_country? $buyer_country."<br/>":'',
	'buyer_state'      	  => $db->f('customer_state'),
	'buyer_city'       	  => $db->f('customer_city')? $db->f('customer_city')."<br/>":'',
	'buyer_zip'        	  => $db->f('customer_zip'),
	'buyer_address'    	  => $db->f('customer_address')? $db->f('customer_address')."<br/>":'',
	'buyer_email'		  => $db->f('customer_email')? $db->f('customer_email')."<br/>":'',
	'buyer_fax'           => $db->f('customer_fax')? $db->f('customer_fax')."<br/>":'',
	'buyer_phone'         => $db->f('customer_phone')? $db->f('customer_phone')."<br/>":'',
	'seller_name'         => $db->f('identity_id')? $db->f('company_name') : $db->f('seller_name'),
	'seller_d_country'    => $db->f('identity_id')? get_country_name($db->f('country_id')) : get_country_name($db->f('seller_d_country_id')),
	'seller_d_state'      => get_state_name($db->f('seller_d_state_id')),
	//'seller_d_city'       => $db->f('identity_id')? $db->f('city_name') : utf8_decode($db->f('seller_d_city')),
	'seller_d_city'		  =>$seller_d_city,
	'seller_d_zip'        => $db->f('identity_id')? $db->f('company_zip') : $db->f('seller_d_zip'),
	'seller_d_address'    => $db->f('identity_id')? nl2br($db->f('company_address')) : nl2br(utf8_decode($db->f('seller_d_address'))),
	'order_contact_name'  => $contact_title.$db->f('contact_name'),
	'field_customer_id'   => $db->f('field')=='customer_id'?true:false,
	//	'INVOICE_BUYER_VAT'   => ACCOUNT_VAT_NUMBER,
	'bank'				  => $db->f('identity_id')? $db->f('bank_name_identity') : ACCOUNT_BANK_NAME,
	'bic'				  => $db->f('identity_id')? $db->f('bank_bic_code_identity') : ACCOUNT_BIC_CODE,
	'iban'				  => $db->f('identity_id')? $db->f('bank_iban_identity') : ACCOUNT_IBAN,
	// 'hide_b_d'			  => !defined('ACCOUNT_BANK_NAME') || !ACCOUNT_BANK_NAME ? 'hide' : '',
	'sh_discount'		  => $apply_discount < 2 ? false :true,
	'discount_percent'    => ' ( '.$db->f('discount').'% )',
	'notes'               =>  $notes ? nl2br($notes) : '',
	'view_notes'          => $notes ? '' : 'hide' ,
	'hide_default_total'  => 'hide',//$currency_type ? ($currency_type == ACCOUNT_CURRENCY_TYPE ? 'hide' : '') : 'hide',
	'def_currency'		  => build_currency_name_list(ACCOUNT_CURRENCY_TYPE),
	'order_vat_no'		  => $db->f('seller_bwt_nr'),
	'your_ref'       	  => $db->f('your_ref'),
	'our_ref' 			  => $db->f('our_ref'),
	'hide_our_ref'		  => $db->f('our_ref')? '':'hide',
	'hide_your_ref'		  => $db->f('your_ref')? '':'hide',

	'manager_name'		  => $manager_name,
	//	'INVOICE_BUYER_VAT'   => ACCOUNT_VAT_NUMBER,
	
	'customer_ref'		=> $db->f('your_ref'), 

	'seller_b_fax'        => $db->f('identity_id')? $db->f('company_fax') : ACCOUNT_FAX,
	'seller_b_email'      => $db->f('identity_id')? $db->f('company_email') : ACCOUNT_EMAIL,
	'seller_b_phone'      => $db->f('identity_id')? $db->f('company_phone') : ACCOUNT_PHONE,
	'seller_b_url'        => $db->f('identity_id')? $db->f('company_url') : ACCOUNT_URL,
	'address_info'	        => nl2br($db->f('address_info'))."<br/>",
	'delivery_date'		  => $delivery_date,
	'is_delivery_date'	  => ($in['delivery_id'] && $delivery_date) ? false : true,
	'delivery_del_date'	  => $delivery_del_date,
	'is_delivery_del_date'=> ($in['delivery_id'] && $delivery_del_date) ? true : false,
	'is_pickup'           => $db->f('delivery_type')==2 ? true : false,

	'title_txt'        => getc_label_txt('title',$in['lid']),
	'directive_txt'        => getc_label_txt('directive',$in['lid']),
	'text_txt'        => $text,
	'place_txt'        => getc_label_txt('place',$in['lid']),
	'date_prefix_txt'	=>getc_label_txt('date_prefix',$in['lid']),
	'signature_txt'        => getc_label_txt('signature',$in['lid']),
	'function_txt'        => getc_label_txt('function',$in['lid']),
	'description_txt'        => getc_label_txt('description',$in['lid']),
	'purchase_ref_txt'        => getc_label_txt('purchase_ref',$in['lid']),
	'customer_ref_txt'        => getc_label_txt('customer_ref',$in['lid']),
	'delivery_note_txt'        => getc_label_txt('delivery_note',$in['lid']),
	'particularities_txt'       => getc_label_txt('particularities',$in['lid']),
	'attention_text_txt'        => getc_label_txt('attention_text',$in['lid']),
	'genummerd_txt'				=> getc_label_txt('genummerd',$in['lid']),
	'certificate_note_txt'		=> getc_label_txt('certificate_note',$in['lid']),
	'delivered_to_txt'		=> getc_label_txt('delivered_to',$in['lid']),
	'stamp_txt'        => getc_label_txt('stamp',$in['lid'])


	));
	//$height = 220;
	
	$delivery_address_id = $db_date->field("SELECT delivery_address_id  FROM pim_order_deliveries WHERE order_id = '" . $in['order_id'] . "' AND delivery_id = '" . $in['delivery_id'] . "' AND TRIM(delivery_address) <>' '");

    if($delivery_address_id){
        $delivery_address_data = $db_date->query("SELECT address, zip, city, country_id  FROM customer_addresses WHERE address_id = '" .$delivery_address_id. "' ");
        if($delivery_address_data->next()) {
             $delivery_address =  $delivery_address_data->f('address')."<br>".$delivery_address_data->f('zip').'  '.$delivery_address_data->f('city')."<br>".get_country_name($delivery_address_data->f('country_id'));
        }else{
            $delivery_address = $db_date->field("SELECT delivery_address  FROM pim_order_deliveries WHERE order_id = '" . $in['order_id'] . "' AND delivery_id = '" . $in['delivery_id'] . "' AND TRIM(delivery_address) <>' '");
        }

    }else{
        $delivery_address = $db_date->field("SELECT delivery_address  FROM pim_order_deliveries WHERE order_id = '" . $in['order_id'] . "' AND delivery_id = '" . $in['delivery_id'] . "' AND TRIM(delivery_address) <>' '");
    }

    if (trim($delivery_address)) {
        $view->assign(array(
            //'delivery_address' => nl2br($db->f('address_info')),
            'delivery_address' => nl2br($delivery_address),
            'is_delivery' => true
        ));
    } else {
        if ($db->f('delivery_address')) {
            $view->assign(array(
                'delivery_address' => nl2br($db->f('address_info')),
                'is_delivery' => true
            ));
        } else {
            $view->assign(array(
                'delivery_address' => nl2br($db->f('address_info')),
                'is_delivery' => false
            ));
        }
    }

	$i=0;
	
	$db->query("SELECT pim_certificates.*, SUM( pim_certificates.quantity ) AS total_quantity, pim_order_articles.article, pim_order_articles.article_code FROM pim_certificates
					INNER JOIN pim_order_articles ON pim_certificates.article_id = pim_order_articles.order_articles_id
					 WHERE certificate_id='".$in['certificate_id']."' GROUP BY order_articles_id ORDER BY order_articles_id ASC");
	$db4->query("SELECT * FROM pim_order_certificate WHERE certificate_id='".$in['certificate_id']."'");
	
	$view->assign(array('certificate_note'			=> $db4->f('certificate_note'),
						'checked_signature'			=>$db4->f('use_sign_img')? true : false, 
						'certificate_date'			=>  date('j/n/Y',  $db4->f('date'))
						));
		
	$genummerd_txt=	getc_label_txt('genummerd',$in['lid']);

	while ($db->move_next())
	{	
		$view->assign(array(
		'linetext'					=> $db->f('total_quantity')==0? true: false,
		'article'  			    	=> $db->f('total_quantity')==0? strip_tags(htmlspecialchars_decode(html_entity_decode($db->f('article')))) : $db->f('article'),
		'quantity'      			=> number_format($db->f('total_quantity'),0),
		'article_code'				=> $db->f('article_code'),
		'genummerd_txt'				=> $genummerd_txt
		
		
		),'certificate_row');

		$view->loop('certificate_row');

		$i++;

	}

	

}


return $view->fetch();