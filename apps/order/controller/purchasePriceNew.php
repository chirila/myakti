<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$prPrice=array();

if(!$in['purchase_price_other_currency_id'] || ($in['purchase_price_other_currency_id'] == ACCOUNT_CURRENCY_TYPE)){
	$prPrice['purchase_price']=display_number_var_dec($in['purchase_price']);
	$prPrice['currency_id']	= ACCOUNT_CURRENCY_TYPE;
	$prPrice['purchase_price_new']=display_number_var_dec(0);
}else{
	$prPrice['purchase_price']=display_number_var_dec($in['purchase_price_other_currency']);
	$prPrice['currency_id']	= $in['purchase_price_other_currency_id'];
	$prPrice['purchase_price_new']=display_number_var_dec($in['purchase_price']);
}
$prPrice['article_id']	= $in['article_id'];
$prPrice['order_id']	= $in['order_id'];
$prPrice['tr_id']	= $in['tr_id'];
$prPrice['currencies']	= build_currency_list_translated();
$prPrice['default_currency_id']	= ACCOUNT_CURRENCY_TYPE;
$prPrice['default_currency_name']	= currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
//$prPrice['purchase_price_other_currency_id']=$in['purchase_price_other_currency_id'];
//$prPrice['purchase_price_other_currency']=$in['purchase_price_other_currency'];
// $prPrice['account_number_format']         = ACCOUNT_NUMBER_FORMAT;
// $prPrice['nr_decimals']					= ARTICLE_PRICE_COMMA_DIGITS;
// $prPrice['do']					= 'order-edit_purchase_price-order-update_purchase_price';


json_out($prPrice);