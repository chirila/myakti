
<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}
/*
$db2 = new sqldb();
$db3 = new sqldb();*/

$l_r =10;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$def_lang = DEFAULT_LANG_ID;
if($in['lang_id']){
	$def_lang= $in['lang_id'];
}
if($in['lang_id']>=1000) {
	$def_lang = DEFAULT_LANG_ID;
}

switch ($def_lang) {
	case '1':
		$text = gm('Name');
		break;
	case '2':
		$text = gm('Name fr');
		break;
	case '3':
		$text = gm('Name du');
		break;
	default:
		$text = gm('Name');
		break;
}

$cat_id = $in['cat_id'];
$filter=" 1=1 ";
if(!$in['from_address_id']) {
	$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id  AND pim_article_prices.base_price=1
						   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id
					 AND pim_articles_lang.lang_id=\''.$def_lang.'\' ';
	$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
				pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
				pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
				pim_article_prices.price, pim_articles.internal_name,
				pim_articles_lang.description AS description,
				pim_articles_lang.name2 AS item_name2,
				pim_articles_lang.name AS item_name,
				pim_articles_lang.lang_id,
				pim_articles.vat_id,
				pim_articles.block_discount,
				pim_articles.is_service,
				pim_articles.supplier_reference';

}else{
	$table = 'pim_articles  LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=1
	                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
					   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'';

	$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
				pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
				pim_articles.sale_unit,pim_articles.packing,
				pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

				pim_articles_lang.description AS description,
				pim_articles_lang.name2 AS item_name2,
				pim_articles_lang.name AS item_name,
				pim_articles_lang.lang_id,
				pim_articles.vat_id,
				pim_articles.block_discount,
				pim_articles.is_service,
				pim_articles.supplier_reference';
}

//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

if ($in['search'])
{
	$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%')";
	// $arguments.="&search=".$in['search'];
}
if ($in['hide_article_ids'])
{
	$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
	// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
}
// if ($in['lang_id'])
// {

// 	$arguments.="&lang_id=".$in['lang_id'];
// }
// if ($in['is_purchase_order'])
// {

	// $arguments.="&is_purchase_order=".$in['is_purchase_order'];
// }
if ($in['show_stock'])
{
	$filter.=" AND pim_articles.hide_stock=0";
	// $arguments.="&show_stock=".$in['show_stock'];
}
if ($in['from_customer_id1'])
{
	$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
	// $arguments.="&from_customer_id=".$in['from_customer_id'];
}
if ($in['from_address_id1'])
{
	$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
	// $arguments.="&from_address_id=".$in['from_address_id'];
}

if($in['only_articles']=='1'){
	$filter.=" AND pim_articles.is_service='0' ";
}

$articles= array( 'lines' => array());
$articles['max_rows']= (int)$db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");


$article = $db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1'  ORDER BY pim_articles.item_code  LIMIT ".$offset*$l_r.",".$l_r);

$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='ORDER_FIELD_LABEL'");

if($in['is_purchase_order']){
	$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='P_ORDER_FIELD_LABEL'");
}

$time = time();

$j=0;
while($article->next()){
	$vat = $db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");

	$values = $article->next_array();
	$tags = array_map(function($field){
		return '/\[\!'.strtoupper($field).'\!\]/';
	},array_keys($values));

	$label = preg_replace($tags, $values, $fieldFormat);

	if($article->f('price_type')==1){

	    $price_value_custom_fam=$db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

        $pim_article_price_category_custom=$db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
            $price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

        }else{
       	   	$price_value=$price_value_custom_fam;

         	 //we have to apply to the base price the category spec
    	 	$cat_price_type=$db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
    	    $cat_type=$db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
    	    $price_value_type=$db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

    	    if($cat_price_type==2){
                $article_base_price=get_article_calc_price($article->f('article_id'),3);
            }else{
                $article_base_price=get_article_calc_price($article->f('article_id'),1);
            }

       		switch ($cat_type) {
				case 1:                  //discount
					if($price_value_type==1){  // %
						$price = $article_base_price - $price_value * $article_base_price / 100;
					}else{ //fix
						$price = $article_base_price - $price_value;
					}
					break;
				case 2:                 //profit margin
					if($price_value_type==1){  // %
						$price = $article_base_price + $price_value * $article_base_price / 100;
					}else{ //fix
						$price =$article_base_price + $price_value;
					}
					break;
			}
        }

	    if(!$price || $article->f('block_discount')==1 ){
        	$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
        }
    }else{
    	$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
        if(!$price || $article->f('block_discount')==1 ){
        	$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
        }
    }

    $pending_articles=$db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
  	$base_price = $db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");


  	if($article->f('is_service') == 1){
  		$price=$base_price;
  	}

    $start= mktime(0, 0, 0);
    $end= mktime(23, 59, 59);
    $promo_price=$db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
    if($promo_price->move_next()){
    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

        }else{
            $price=$promo_price->f('price');
            $base_price = $price;
        }
    }
 	if($in['customer_id']){
  		$customer_custom_article_price=$db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['customer_id']."'");
    	if($customer_custom_article_price->move_next()){

            $price = $customer_custom_article_price->f('price');

            $base_price = $price;
       	}
   	}

   	$code = $article->f('item_code');

	if($in['app']=='po_order'){
		$purchase_price = $db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

		if($in['customer_id']){
	      $supplier_article = $db->query("SELECT purchasing_price, supplier_ref FROM pim_article_references WHERE article_id='".$article->f('article_id')."' AND supplier_id='".$in['customer_id']."' ");
	      if($supplier_article->move_next()){
	      	if($supplier_article->f('purchasing_price')>0){
	      		$purchase_price = $supplier_article->f('purchasing_price');
	      	}
	        $code = $supplier_article->f('supplier_ref');
	      }
	    }
		$base_price = $purchase_price;
		$price = $purchase_price;

	}else{
		$purchase_price = $db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");
	}
	
	$nr_taxes = $db->field("SELECT COUNT(article_tax_id) FROM pim_articles_taxes WHERE article_id='".$article->f('article_id')."' ");

	$linie = array(
	  	'article_id'				=> $article->f('article_id'),
	  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
	  	'name'						=> $article->f('internal_name'),
	  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
	    'stock'						=> $article->f('stock'),
	    'stock2'					=> remove_zero_decimals_dn(display_number($article->f('stock'))),
	    'quantity'		    		=> 1,
	    'pending_articles'  		=> intval($pending_articles),
	    'threshold_value'   		=> $article->f('article_threshold_value'),
	  	'sale_unit'					=> $article->f('sale_unit'),
	  	'percent'           		=> $vat_percent,
		'percent_x'         		=> display_number($vat_percent),
	    'packing'					=> remove_zero_decimals($article->f('packing')),
	  	'code'		  	    		=> $code,
		'price'						=> $price,
		'price_vat'					=> $in['remove_vat'] == 1 ? $price : $price + (($price*$vat)/100),
		'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
		'purchase_price'			=> $purchase_price,
		'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
		'quoteformat'    			=> gfn($label),
		'base_price'				=> place_currency(display_number_var_dec($base_price)),
		'show_stock'				=> $article->f('hide_stock') ? false:true,
		'hide_stock'				=> $article->f('hide_stock'),
		'is_service'				=> $article->f('is_service'),
		'allow_stock'               =>ALLOW_STOCK == 1 ? true : false,

	);
	array_push($articles['lines'], $linie);
  	// $j++;
}

$articles['customer_id'] 		= $in['customer_id'];
$articles['lang_id'] 				= $in['lang_id'];
$articles['cat_id'] 				= $in['cat_id'];
$articles['txt_name']			  = $text;
$articles['lr']			  = $l_r;
$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;


if($in['from_order_page']===true){
	return json_out($articles,true,false);
}
json_out($articles);

function get_taxes($in,$showin=true,$exit=true){
	$taxes = array( 'lines'=>array());
	$db = new sqldb();
/*	$db->query("SELECT pim_articles.vat_id,pim_articles.article_id,vats.value
	                       FROM pim_articles
	                       INNER JOIN vats ON vats.vat_id=pim_articles.vat_id
	                       WHERE pim_articles.article_id='".$in['article_id']."' AND pim_articles.active='1' ");
	$vat_percent=$db->f('value');*/

	$gov_tax=array();
	if(!$in['quantity']){
		$in['quantity']==1;
	}

	$get_article_taxes=$db->query("SELECT tax_id FROM pim_articles_taxes WHERE article_id='".$in['article_id']."'");
	while ($get_article_taxes->next()){
		$gov_tax[$get_article_taxes->f('tax_id')]+=$in['quantity'];
	}

	$is_gov_taxes=false;
	$total_gov_taxes=0;
	$i=0;

	foreach($gov_tax as $tax_id => $quantity){
		$gov_tax = $db->query("SELECT pim_article_tax.* ,pim_article_tax_type.name as type_name, vats.value
		                       FROM pim_article_tax
		                       LEFT JOIN pim_article_tax_type ON pim_article_tax_type.id=pim_article_tax.type_id
		                       LEFT JOIN vats ON vats.vat_id=pim_article_tax.vat_id
		                       WHERE pim_article_tax.tax_id='".$tax_id."'");
		$vat_percent=$gov_tax->f('value');
		if($vat_regime==2){
		  	$vat_percent=0;
		}

		$vat_value= $gov_tax->f('amount')*($vat_percent/100);

		$linie = array(
			'tr_id'             			=> 'tmp'.$i.strtotime('now'),
			'is_vat'						=> $in['remove_vat'] == 1 ? false : true,
			'tax_id'            			=> $tax_id,
			'tax_for_article_id'			=> $in['article_id'],
			'quantity_old'      			=> $quantity,
			'quantity'          			=> display_number($quantity),
			'quantity_component'      		=> 0,
			'percent'           			=> $vat_percent,
			'percent_x'         			=> display_number($vat_percent),
			'vat_value'         			=> $vat_value,
			'vat_value_x'       			=> display_number($vat_value),
			'tax_id'            			=> $tax_id,
			//'tax_name'          			=> $gov_tax->f('type_name'),
			'tax_name'          			=> $gov_tax->f('code'),
			'tax_quantity'      			=> display_number($quantity),
			'disc_val'          			=> display_number(0),
			'price'             			=> $in['exchange']==1 ? display_number_var_dec($gov_tax->f('amount')/return_value($in['ex_rate'])) : display_number_var_dec($gov_tax->f('amount')),
			'price_vat'         			=> $in['exchange']==1 ? display_number(($gov_tax->f('amount')/return_value($in['ex_rate'])) * ($vat_percent/100) ) : display_number($gov_tax->f('amount') * ($vat_percent/100)),
			'line_total'        			=> $in['exchange']==1 ? display_number(($gov_tax->f('amount')/return_value($in['ex_rate'])) * $quantity ) : display_number($gov_tax->f('amount') * $quantity),
			//'tax_code'						=> $gov_tax->f('code'),
			'tax_code'						=> $gov_tax->f('type_name'),
			'td_width'						=> ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px',
			'input_width'					=> ALLOW_ARTICLE_PACKING ? 'width:164px' : 'width:223px',
			'hide_disc'						=> $in['apply_discount'] ==0 || $in['apply_discount'] == 2 ? 'hide' : '',
			'allow_article_packing' 		=> $in['allow_article_packing'],
			'allow_article_sale_unit'		=>$in['allow_article_sale_unit'],
			'apply_to'						=> $gov_tax->f('apply_to'),
		);

		$total_gov_taxes+= $gov_tax->f('amount') * $quantity;

		array_push($taxes['lines'], $linie);
		$is_gov_taxes=true;

		$i++;
	}
	$taxes['is_gov_taxes'] = $is_gov_taxes;
	$taxes['gov_taxes_value']= display_number($total_gov_taxes);

	return json_out($taxes, $showin, $exit);
}

function get_components($in,$showin=true,$exit=true){
  $lines = array( 'components'=>array());
  $db = new sqldb();

  $components=$db->query("SELECT pim_articles_combined.*, pim_articles.internal_name, pim_articles.item_code FROM pim_articles_combined 
                          LEFT JOIN pim_articles on pim_articles.article_id = pim_articles_combined.article_id
                          WHERE parent_article_id='".$in['article_id']."'  ORDER BY pim_articles_combined.`sort_order` ASC");
  if(!$in['quantity']){
		$in['quantity']=1;
	}
  $is_components=false;
  $total_components=0;
  $i=0;

  while ($components->next()){
  	$purchase_price = $db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$components->f('article_id')."' AND pim_article_prices.base_price='1'");

     $linie = array(
      'tr_id'                   => 'tmp'.$i.strtotime('now'),
      'is_vat'                  => false,
      'component_id'            => $components->f('pim_articles_combined_id'),
      'component_article_id'    => $components->f('article_id'),
      'component_name'          => $components->f('internal_name'),
      'component_code'          => $components->f('item_code'),
      'parent_article_id'       => $in['article_id'],
      'quantity_old'            => $components->f('quantity')*$in['quantity'],
      'quantity'                => display_number($components->f('quantity')*$in['quantity']),
      'quantity_component'      => display_number($components->f('quantity')),
      'purchase_price' 			=> $purchase_price,
      'percent'                 => 0,
      'percent_x'               => display_number(0),
      'vat_value'               => 0,
      'vat_value_x'             => display_number(0),
      'disc_val'                => display_number(0),
      'price'                   => display_number_var_dec(0),
      'price_vat'               => display_number_var_dec(0),
      'line_total'              => display_number(0),
      'td_width'            => ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px',
      'input_width'         => ALLOW_ARTICLE_PACKING ? 'width:164px' : 'width:223px',
      'hide_disc'           => $in['apply_discount'] ==0 || $in['apply_discount'] == 2 ? 'hide' : '',
      'visible'          	=> $components->f('visible')? true: false,
    );

    array_push($lines['components'], $linie);
    $is_components=true;
    $i++;
  }
  $lines['is_components'] = $is_components;
  return json_out($lines, $showin, $exit);
}