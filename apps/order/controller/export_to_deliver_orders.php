<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('memory_limit', '1000M');

$in['articles_id'] ='';
if($_SESSION['add_to_pdf_otd']){
    foreach ($_SESSION['add_to_pdf_otd'] as $key => $value) {
        $in['articles_id'] .= $key.',';
    }
}
if(isset($_SESSION['tmp_add_to_pdf_otd'])){
    unset($_SESSION['tmp_add_to_pdf_otd']);
}
if(isset($_SESSION['add_to_pdf_otd'])){
    unset($_SESSION['add_to_pdf_otd']);
}

$in['articles_id'] = rtrim($in['articles_id'],',');
if(!$in['articles_id']){
    $in['articles_id']= '0';
}

$headers = array("ARTICLE CODE",
    "INTERNAL NAME",
    "TO DELIVER",
    "SALES ORDER",
    "DATE",
    "CUSTOMER"
);

$db = new sqldb();
$filename ="export_orders_to_be_delivered.csv";
$final_data=array();
$response = array();
$general_orders=array();
$general_orders_rdy=array();

$qty_ord = $db->query("SELECT pim_articles.item_code, pim_articles.internal_name, pim_order_articles.order_articles_id, pim_articles.article_id, pim_articles.packing as article_packing, pim_articles.sale_unit as article_sale_unit, SUM( pim_order_articles.quantity ) as total_order , COUNT(distinct pim_orders_delivery.delivery_id) as nr_del ,SUM( pim_orders_delivery.quantity) as total_deliver, SUM(CASE WHEN pim_order_deliveries.delivery_done = '1' THEN pim_orders_delivery.quantity ELSE 0 END) as total_deliver2,  pim_order_articles.sale_unit, pim_order_articles.packing,pim_orders.order_id,pim_orders.customer_name,pim_orders.serial_number,pim_orders.date,pim_orders.del_date
		    FROM pim_order_articles
		    LEFT JOIN pim_articles ON pim_order_articles.article_id=pim_articles.article_id
		    LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
		    LEFT JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
		    LEFT JOIN pim_order_deliveries ON pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id
		    WHERE pim_order_articles.article_id!='0' AND pim_articles.article_id IN (".$in['articles_id'].") AND pim_articles.is_service='0' AND pim_orders.sent =1 AND pim_orders.active =1 GROUP BY pim_order_articles.order_articles_id,pim_orders.order_id");

while($qty_ord->move_next()) {
    $nr_del = $qty_ord->f('nr_del');
    if($nr_del==0) {
        $nr_del = 1;
    }

    $total_del=0;

    $stock_packing_ord = $qty_ord->f("packing");
    if(!$stock_packing_ord){
        $stock_packing_ord = $diff->f("article_packing");
    }
    if(!ALLOW_ARTICLE_PACKING || !$stock_packing_ord){
        $stock_packing_ord = 1;
    }

    $stock_sale_unit_ord = $qty_ord->f("sale_unit");
    if(!$stock_sale_unit_ord){
        $stock_sale_unit_ord = $qty_ord->f("article_sale_unit");
    }
    if(!ALLOW_ARTICLE_SALE_UNIT || !$stock_sale_unit_ord){
        $stock_sale_unit_ord = 1;
    }

    if(ORDER_DELIVERY_STEPS == '2'){
        $total_del = $qty_ord->f('total_deliver2');
    }else{
        $total_del = $qty_ord->f('total_deliver');
    }

    if($qty_ord->f('total_order')/$nr_del < $total_del){

        $total_deliver = ( $qty_ord->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del;
    }else{
        $total_deliver = $total_del*$stock_packing_ord/$stock_sale_unit_ord;
    }

    if(!array_key_exists($qty_ord->f('order_articles_id'), $general_orders)){
        $general_orders[$qty_ord->f('order_articles_id')]=array();
    }
    if(!array_key_exists($qty_ord->f('article_id'),$general_orders[$qty_ord->f('order_articles_id')])){
        $general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')]=array();
    }
    if(!array_key_exists($qty_ord->f('order_id'),$general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')])){
        $general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]=array();
        $general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['quantity']=0;
        $general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['serial_number']=$qty_ord->f('serial_number');
        $general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['customer_name']=$qty_ord->f('customer_name');
        $general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['date']=$qty_ord->f('date');
        $general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['del_date']=$qty_ord->f('del_date');
        $general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['item_code']=$qty_ord->f('item_code');
        $general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['internal_name']=$qty_ord->f('internal_name');
    }
    $general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['quantity']+=( ( $qty_ord->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del - $total_deliver );

}

$diff = $db->query("SELECT pim_order_articles.order_articles_id, pim_articles.article_id, pim_articles.packing as article_packing, pim_articles.sale_unit as article_sale_unit, SUM( pim_order_articles.quantity ) as total_order , COUNT(distinct pim_orders_delivery.delivery_id) as nr_del, SUM( pim_orders_delivery.quantity) as total_deliver, SUM(CASE WHEN pim_order_deliveries.delivery_done = '1' THEN pim_orders_delivery.quantity ELSE 0 END) as total_deliver2 , pim_order_articles.sale_unit, pim_order_articles.packing,pim_orders.order_id
		        FROM pim_order_articles
		        LEFT JOIN pim_articles ON pim_order_articles.article_id=pim_articles.article_id
		        LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
		        LEFT JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
		        LEFT JOIN pim_order_deliveries ON pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id
		        WHERE pim_order_articles.article_id!='0' AND pim_articles.article_id IN (".$in['articles_id'].") AND pim_articles.is_service='0' AND pim_orders.sent =1 AND pim_orders.rdy_invoice =  '1' AND pim_orders.active =1 GROUP BY pim_order_articles.order_articles_id,pim_orders.order_id");

$total_deliver=0;
while($diff->move_next()) {

    $nr_del = $diff->f('nr_del');
    if($nr_del==0) {
        $nr_del = 1;
    }

    $stock_packing_ord = $diff->f("packing");
    if(!$stock_packing_ord){
        $stock_packing_ord = $diff->f("article_packing");
    }
    if(!ALLOW_ARTICLE_PACKING || !$stock_packing_ord){
        $stock_packing_ord = 1;
    }

    $stock_sale_unit_ord = $diff->f("sale_unit");
    if(!$stock_sale_unit_ord){
        $stock_sale_unit_ord = $diff->f("article_sale_unit");
    }
    if(!ALLOW_ARTICLE_SALE_UNIT || !$stock_sale_unit_ord){
        $stock_sale_unit_ord = 1;
    }

    if(ORDER_DELIVERY_STEPS == '2'){
        $total_del = $diff->f('total_deliver2');
    }else{
        $total_del = $diff->f('total_deliver');
    }
    if($diff->f('total_order')/$nr_del < $total_del){
        $total_deliver =($diff->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del;
    }else{
        $total_deliver = $total_del*$stock_packing_ord/$stock_sale_unit_ord ;
    }

    if(!array_key_exists($diff->f('order_articles_id'), $general_orders_rdy)){
        $general_orders_rdy[$diff->f('order_articles_id')]=array();
    }
    if(!array_key_exists($diff->f('article_id'),$general_orders_rdy[$diff->f('order_articles_id')])){
        $general_orders_rdy[$diff->f('order_articles_id')][$diff->f('article_id')]=array();
    }
    if(!array_key_exists($diff->f('order_id'),$general_orders_rdy[$diff->f('order_articles_id')][$diff->f('article_id')])){
        $general_orders_rdy[$diff->f('order_articles_id')][$diff->f('article_id')][$diff->f('order_id')]=array();
        $general_orders_rdy[$diff->f('order_articles_id')][$diff->f('article_id')][$diff->f('order_id')]['quantity']=0;
    }
    $general_orders_rdy[$diff->f('order_articles_id')][$diff->f('article_id')][$diff->f('order_id')]['quantity']+=( ( $diff->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del - $total_deliver );
}

foreach($general_orders as $order_articles_id=>$data){
    if(array_key_exists($order_articles_id, $general_orders_rdy)){
        foreach($data as $article_id=>$article_data){
            if(!array_key_exists($article_id, $response)){
                $response[$article_id]=array('list'=>array());
            }
            foreach($article_data as $order_id=>$order_data){
                if(array_key_exists($article_id,$general_orders_rdy[$order_articles_id]) && array_key_exists($order_id, $general_orders_rdy[$order_articles_id][$article_id])){
                    if($order_data['quantity']-$general_orders_rdy[$order_articles_id][$article_id][$order_id]['quantity'] > 0){
                        $item_order=array(
                            'id'                    => $order_id,
                            'item_code'             => $order_data['item_code'],
                            'internal_name'         => $order_data['internal_name'],
                            'serial_number'         => $order_data['serial_number'],
                            'customer'              => $order_data['customer_name'],
                            'created'               => date(ACCOUNT_DATE_FORMAT,$order_data['date']),
                            'delivery_date'         => $order_data['del_date'] ? date(ACCOUNT_DATE_FORMAT,$order_data['del_date']):'',
                            'quantity'              => display_number($order_data['quantity']-$general_orders_rdy[$order_articles_id][$article_id][$order_id]['quantity'])
                        );
                        array_push($response[$article_id]['list'], $item_order);
                    }
                }else{
                    if($order_data['quantity'] > 0){
                        $item_order=array(
                            'id'                    => $order_id,
                            'item_code'             => $order_data['item_code'],
                            'internal_name'         => $order_data['internal_name'],
                            'serial_number'         => $order_data['serial_number'],
                            'customer'              => $order_data['customer_name'],
                            'created'               => date(ACCOUNT_DATE_FORMAT,$order_data['date']),
                            'delivery_date'         => $order_data['del_date'] ? date(ACCOUNT_DATE_FORMAT,$order_data['del_date']):'',
                            'quantity'              => display_number($order_data['quantity'])
                        );
                        array_push($response[$article_id]['list'], $item_order);
                    }
                }
            }
        }
    }else{
        foreach($data as $article_id=>$article_data){
            if(!array_key_exists($article_id, $response)){
                $response[$article_id]=array('list'=>array());
            }
            foreach($article_data as $order_id=>$order_data){
                if($order_data['quantity'] > 0){
                    $item_order=array(
                        'id'                    => $order_id,
                        'item_code'             => $order_data['item_code'],
                        'internal_name'         => $order_data['internal_name'],
                        'serial_number'         => $order_data['serial_number'],
                        'customer'              => $order_data['customer_name'],
                        'created'               => date(ACCOUNT_DATE_FORMAT,$order_data['date']),
                        'delivery_date'         => $order_data['del_date'] ? date(ACCOUNT_DATE_FORMAT,$order_data['del_date']):'',
                        'quantity'              => display_number($order_data['quantity'])
                    );
                    array_push($response[$article_id]['list'], $item_order);
                }
            }
        }
    }
}

$explode_ids = explode(',', $in['articles_id']);
foreach ($explode_ids as $key => $article_id)
{
    foreach($response[$article_id]['list'] as $item) {
        $code = $item['item_code'];
        $internal_name = $item['internal_name'];
        $to_deliver = $item['quantity'];
        $sales_order = $item['serial_number'];
        $date = $item['created'];
        $customer = $item['customer'];
        $tmp_line = array(
            $code,
            $internal_name,
            display_number_exclude_thousand($to_deliver),
            $sales_order,
            $date,
            $customer,
        );
        array_push($final_data, $tmp_line);
    }
}

/*
$from_location=INSTALLPATH.'upload/'.DATABASE_NAME.'/tmp_export_orders_to_be_delivered.csv';
$fp = fopen($from_location, 'w');
fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
fputcsv($fp, $headers);
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}
fclose($fp);

ark::loadLibraries(array('PHPExcel'));
$objReader = PHPExcel_IOFactory::createReader('CSV');

$objPHPExcel = $objReader->load($from_location);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
unlink($from_location);

doQueryLog();

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="export_orders_to_be_delivered.xls"');
header('Cache-Control: max-age=0');
$objWriter->save('php://output');
exit();*/
header('Content-Type: application/excel');
header('Content-Disposition: attachment;filename="'.$filename.'"');

$fp = fopen("php://output", 'w');
fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
fputcsv($fp, $headers);
foreach ( $final_data as $line ) {
    fputcsv($fp, $line);
}
fclose($fp);
doQueryLog();
exit();

// set column width to auto
foreach(range('A','W') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$objPHPExcel->getActiveSheet()->getStyle('G1:G'.$xlsRow)->getAlignment()->setWrapText(true);
$rows_format='A1:A'.$xlsRow;
$objPHPExcel->getActiveSheet()->getStyle($rows_format)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->setTitle('Invoices list export');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.$filename);


define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',
  'csv' => 'text/csv',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload', $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}
doQueryLog();
// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);
// download
//@readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);

      die();
    }
  }
  @fclose($file);
}
?>