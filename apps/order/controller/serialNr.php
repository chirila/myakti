<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$is_data = false;
$serial = array('serial_nr_row'=>array(),'sel_serial_nr_row'=>array(),'view_sel_serial_nr_row'=>array());

if(($in['order_id'] && $in['order_articles_id']) && ($in['count_serial_nr'] > 0)){
	$article_id = $db->field("SELECT article_id FROM pim_order_articles WHERE order_articles_id = '".$in['order_articles_id']."' ");
	if($in['selected_s_n']){
		$is_data = true;
		if(is_array($in['selected_s_n'])){
			$selected_s_n = count($in['selected_s_n']);
			$arr = array();
			$selected_list='';
			foreach($in['selected_s_n'] as $key => $value){
				$selected_list.=$value['serial_nr_id'].',';
			}
			$selected_list=rtrim($selected_list, ',');

			
			$serial_numbers = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' AND id NOT IN(".$selected_list.") ORDER BY serial_number ASC ");
			$sel_serial_no = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' AND id IN(".$selected_list.") ORDER BY serial_number ASC ");
			if($in['delivery_approved']){
				$sel_serial_no = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND id IN(".$selected_list.") ORDER BY serial_number ASC ");
			}
		}else{
			$selected_s_n = count(explode(',', $in['selected_s_n']));
			$serial_numbers = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' AND id NOT IN(".$in['selected_s_n'].") ORDER BY serial_number ASC ");
			$sel_serial_no = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' AND id IN(".$in['selected_s_n'].") ORDER BY serial_number ASC ");
			
			if($in['delivery_approved']){
				$sel_serial_no = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND id IN(".$in['selected_s_n'].") ORDER BY serial_number ASC ");
				
			}
		}
		
	}else{
		$selected_s_n = 0;
		if($in['return']){
			$delivery_list ='';
			$deliveries = $db->query("SELECT delivery_id FROM pim_order_deliveries WHERE order_id = '".$in['order_id']."' ");
			while($deliveries->next()){
				$delivery_list .= $deliveries->f('delivery_id').',';
			}
			$delivery_list = rtrim($delivery_list,','); 
		
			$filter2 = " AND serial_numbers.status_id!='1' ";
			$filter1 = " AND serial_numbers_from_orders.return_id='".$in['return_id']."' ";
			if($in['return_id']){
				

				$serial_numbers = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."'  AND delivery_id IN (".$delivery_list.") $filter2 ORDER BY serial_number ASC ");

				$sel_serial_no = $db->query("SELECT * FROM serial_numbers_from_orders
				LEFT JOIN serial_numbers ON serial_numbers.id = serial_numbers_from_orders.serial_number_id
				 WHERE serial_numbers_from_orders.article_id = '".$article_id."'  AND serial_numbers_from_orders.order_id ='".$in['order_id']."' $filter1 ORDER BY serial_number ASC ");
				
			} else{
				
				$serial_numbers = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."'  AND delivery_id IN (".$delivery_list.") $filter2 ORDER BY serial_number ASC ");
				$sel_serial_no = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."'  AND id < '0' AND delivery_id IN ('".$delivery_list."') $filter2 ");
			}

			

			/**/
		}else{
			$serial_numbers = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' ORDER BY serial_number ASC ");
			$sel_serial_no = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND status_id = '1' AND id < '0' ");
		}

	}
	$i=0;
	$locations_data=array();
	while($serial_numbers->next()){
		$location_name="";
		if($serial_numbers->f('address_id')){
			if(array_key_exists($serial_numbers->f('address_id'), $locations_data) === false){
				$location_name=$db->field("SELECT naming FROM dispatch_stock_address WHERE address_id='".$serial_numbers->f('address_id')."' ");
				$locations_data[$serial_numbers->f('address_id')]=$location_name;
			}else{
				$location_name=$locations_data[$serial_numbers->f('address_id')];
			}
		}
		array_push($serial['serial_nr_row'],array(
			'order_articles_id'			=> $in['order_articles_id'],
			'serial_number'		=> $serial_numbers->f('serial_number'),
			'serial_nr_id'		=> $serial_numbers->f('id'),
			'location_name'		=> $location_name
		));
		$i++;
	}


	while($sel_serial_no->next()){
		array_push($serial['sel_serial_nr_row'],array(
			'order_articles_id'			=> $in['order_articles_id'],
			'serial_number'		=> $sel_serial_no->f('serial_number'),
			'serial_nr_id'		=> $sel_serial_no->f('id'),
		));
		$j++;
	}

	if($in['return_id']){
		$selected_s_n =$j;
	}

	$serial['order_articles_id']		= $in['order_articles_id'];
	$serial['overflow_y']						= $i > 9 ?  'style="overflow-y:scroll;"' : '';
	$serial['sel_overflow_y']				= $j > 9 ?  'style="overflow-y:scroll;"' : '';
	$serial['all_green']						= $selected_s_n == $in['count_serial_nr'] ? 'all_green' : '';
	$serial['sel_s_n']							= $selected_s_n ? $selected_s_n : 0;
	$serial['total_s_n']						= $in['count_serial_nr'];
	$serial['not_delivery_id']			= true;
	$serial['hide_if_delivery']			= true;
	$serial['column']								= '6';
	if($in['delivery_id']){
		if($in['delivery_approved'] !=1){
			$serial['hide_if_delivery']		= false;
			$serial['column']							= '12';
			$serial['view_sn']						= 'view_sn';
			$serial['not_delivery_id']		= false;
			$sel_serial_numbers = $db->query("SELECT * FROM serial_numbers WHERE article_id = '".$article_id."' AND delivery_id = '".$in['delivery_id']."' ORDER BY serial_number ASC ");
			while($sel_serial_numbers->next()){
				array_push($serial['sel_serial_nr_row'],array(
					'order_articles_id'			=> $in['order_articles_id'],
					'serial_nr_id'	=> $sel_serial_numbers->f('id'),
					'serial_number'	=> $sel_serial_numbers->f('serial_number'),
				));
			}
		}
	}
}
if($i>0){
	$is_data = true;
}

$serial['is_data']				= $is_data;

json_out($serial);
?>