<?php

error_reporting(0);
if (!defined('INSTALLPATH')) {
	define('INSTALLPATH', '../../../');
}
if (!defined('BASEPATH')) {
	define('BASEPATH', INSTALLPATH . 'core/');
}
session_start();

$is_admin = strtolower($in["is_admin"]);


include_once (INSTALLPATH . 'config/config.php');
include_once (BASEPATH . '/library/sqldb.php');
include_once (BASEPATH . '/library/sqldbResult.php');
include_once (INSTALLPATH . 'config/database.php');
include_once (BASEPATH . 'startup.php');

ini_set('display_errors', 1);
  ini_set('error_reporting', 1);
  // error_reporting(E_ALL);
  error_reporting(E_ALL ^ E_NOTICE);


$q = strtolower($in["term"]);
global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($database_users);
$filter =" is_admin='0' AND active=1 ";
$filter_contact = ' customer_contacts.active=1 ';
if($q){
	$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
	$filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
}

//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
	$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
}
$db= new sqldb();

$cust = $db->query("SELECT customer_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip_name,city_name
	FROM customers
	WHERE $filter
	UNION 
	SELECT customer_contacts.customer_id, CONCAT_WS(' ',firstname, lastname) AS name, customer_contacts.contact_id, position_n as acc_manager_name, country.name AS country_name, customer_contact_address.zip AS zip_name, customer_contact_address.city AS city_name
	FROM customer_contacts
	LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
	LEFT JOIN country ON country.country_id=customer_contact_address.country_id
	WHERE $filter_contact
	ORDER BY name
	LIMIT 5")->getAll();

$result = array();
foreach ($cust as $key => $value) {
	$cname = trim($value['name']);

	$result[]=array(
		"id"					=> $value['customer_id'].'-'.$value['contact_id'],
		"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
		"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
		"ref" 					=> $value['our_reference'],
		"currency_id"			=> $value['currency_id'],
		"lang_id" 				=> $value['internal_language'],
		"identity_id" 			=> $value['identity_id'],
		'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
		'country'				=> $value['country_name'] ? $value['country_name'] : '',
		'zip'					=> $value['zip_name'] ? $value['zip_name'] : '',
		'city'					=> $value['city_name'] ? $value['city_name'] : '',
	);
	/*if (count($result) > 100)
		break;*/
}
array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
json_out($result);

?>