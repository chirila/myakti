<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}

$data = false;
$o=array('lines'=>array());

if($in['order_id'] && !$in['certificate_id']){


	if(!$in['date_cert_line_h']){
		$in['date_cert_line'] = date(ACCOUNT_DATE_FORMAT, mktime(0, 0, 0, date("n"), date("j"), date("Y")));
		$in['date_cert_line_h'] = time();
	  }

	$o['order_id'] =$in['order_id'];
	$o['is_add'] = true;
	$o['is_edit'] = false;
	$o['page_title'] = gm('Add Certificate');
	$o['delivery_id'] =$in['delivery_id'];

	$o['date_cert_line'] = $in['date_cert_line'];
	$o['date_cert_line_h'] = $in['date_cert_line_h'];

	$i = 0;  
	$j = 0;
	$k = 0;

	$o['lines'] = get_lines($in);
	//$o['lines'] = array();

				

}elseif($in['certificate_id']){   //edit certificate note
	$i = 0;
	$in['edit_cnote']=1;

    $db->query("SELECT pim_order_certificate.* FROM pim_order_certificate WHERE certificate_id='".$in['certificate_id']."'");
    $db->move_next();
    $in['date_cert_line_h']  = $db->gf('date');
    $in['date_cert_line']    = date(ACCOUNT_DATE_FORMAT,$db->gf('date'));
    $o['date_cert_line'] = $in['date_cert_line'];
	$o['date_cert_line_h'] = $in['date_cert_line_h'];


	$o['certificate_note']= $db->gf('certificate_note');
	$o['checked'] 	   = $db->gf('use_sign_img')==1? 'checked':'';
	$o['is_add']       =  0;
	$o['is_edit']      =  1;
	$o['language_dd2'] = build_cert_language_dd($db->gf('lang'),1);
	$o['page_title'] = gm('Edit Certificate');
   
}

$customer_id = $db->field("SELECT customer_id FROM pim_orders WHERE order_id='".$in['order_id']."'");
$customer_language = $db->field("SELECT internal_language FROM customers WHERE customer_id = '".$customer_id."'");


$o['do_me']			= $in['certificate_id'] ? 'order--order-edit_certificate' : 'order--order-make_certificate';
$o['style']     	= ACCOUNT_NUMBER_FORMAT;
$o['is_data']		= $data;
$o['order_id']					= $in['order_id'];
$o['pick_date_format']          = pick_date_format(ACCOUNT_DATE_FORMAT);
$o['add_signature_text']		= gm('Add signature to certificate');
$o['language_dd'] 				= build_cert_language_dd($customer_language,1);
$o['deliveries_dd']				= build_deliveries_dd($in['order_id'], $in['delivery_id']);



json_out($o);


function get_lines(&$in){

	$db= new sqldb(); 
 	$result = array();

 	 if ($in['order_id'] && is_numeric($in['delivery_id']) ) {
		$line = $db->query("SELECT pim_orders_delivery.* , pim_order_articles.article, pim_order_articles.article_code, pim_order_articles.sort_order FROM  `pim_orders_delivery` 
							INNER JOIN pim_order_articles ON pim_orders_delivery.order_articles_id = pim_order_articles.order_articles_id 
	 						WHERE pim_orders_delivery.order_id='".$in['order_id']."' AND pim_orders_delivery.delivery_id='".$in['delivery_id']."' AND pim_orders_delivery.certified!=1");
		
		while ($line->next()) {

			array_push($result, array(
				'article'				=> $line->f('article'),
				'article_id'			=> $line->f('article_id'),
				'code'					=> $line->f('article_code'),
	            'quantity_value'		=> $line->f('quantity'),
				'quantity'				=> display_number($line->f('quantity')),
				'order_articles_id' 	=> $line->f('order_articles_id'),
				'sort_order' 			=> $line->f('sort_order'),					
				
			));

			$i++;
		}

		if($i>0){
			$textline = $db->query("SELECT * from  pim_order_articles WHERE order_id='".$in['order_id']."' AND article_id=0 AND content =1 ");
				while ($textline->next()) {
					array_push($result, array(
					'article'				=> strip_tags(htmlspecialchars_decode(html_entity_decode($textline->f('article')))),
					'article_id'			=> $textline->f('article_id'),
					'code'					=> $textline->f('article_code'),
		            'order_articles_id' 	=> $textline->f('order_articles_id'),
		            'sort_order' 			=> $textline->f('sort_order'),	
									
					));

					$i++;

				}
		}

	}

	$exo =  array_sort($result, 'sort_order', SORT_ASC); 
	$result=array();
       foreach ($exo as $key => $value) {
           array_push($result, $value);
       }   

	return $result;
}
