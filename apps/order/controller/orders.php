<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['reset_list']){
	if(isset($_SESSION['tmp_add_to_pdf_o'])){
		unset($_SESSION['tmp_add_to_pdf_o']);
	}
	if(isset($_SESSION['add_to_pdf_o'])){
		unset($_SESSION['add_to_pdf_o']);
	}	
}

global $config;
$db2 = new sqldb();
$db3 = new sqldb();
$l_r =ROW_PER_PAGE;

if(!empty($in['start_date_js'])){
	$in['start_date'] =strtotime($in['start_date_js']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['stop_date_js'])){
	$in['stop_date'] =strtotime($in['stop_date_js']);
	$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}
if(!empty($in['d_start_date_js'])){
	$in['d_start_date'] =strtotime($in['d_start_date_js']);
	$in['d_start_date'] = mktime(0,0,0,date('n',$in['d_start_date']),date('j',$in['d_start_date']),date('y',$in['d_start_date']));
}
if(!empty($in['d_stop_date_js'])){
	$in['d_stop_date'] =strtotime($in['d_stop_date_js']);
	$in['d_stop_date'] = mktime(23,59,59,date('n',$in['d_stop_date']),date('j',$in['d_stop_date']),date('y',$in['d_stop_date']));
}

if(($in['ofs']) || (is_numeric($in['ofs'])))
{
	$in['offset']=$in['ofs'];
}
$order_by_array = array('serial_number','t_date','del_date','our_ref','customer_name','amount','acc_manager_name','author_name','subject','your_ref');
$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$arguments = '';
$filter = 'WHERE 1=1 ';
$filter_link = 'block';
$filter_article='';
$filter_your_ref = '';
$filter_commercial_name = '';
$order_by = " ORDER BY t_date DESC, iii DESC ";
//$order_by = " ORDER BY iii DESC, t_date DESC "; // requested by akti-3854

if(!$in['archived']){
	$filter.= " AND pim_orders.active=1";
}else{
	$filter.= " AND pim_orders.active=0";
	$arguments.="&archived=".$in['archived'];
}
if($in['customer_id']){
	$filter.= " AND pim_orders.customer_id='".$in['customer_id']."'";
	$arguments_c .= '&customer_id='.$in['customer_id'];
}
if($in['filter']){
	$arguments.="&filter=".$in['filter'];
}
if($in['search']){
	$filter.=" and (pim_orders.serial_number like '%".$in['search']."%' OR pim_orders.customer_name like '%".$in['search']."%' OR pim_orders.our_ref like '%".$in['search']."%' )";
	$arguments.="&search=".$in['search'];
}

if($in['article_name_search']){
	$filter_article.=" INNER JOIN pim_order_articles ON pim_order_articles.order_id=pim_orders.order_id
                       INNER JOIN pim_articles ON pim_articles.article_id=pim_order_articles.article_id AND (pim_articles.internal_name like '%".$in['article_name_search']."%' OR pim_order_articles.article_code like '%".$in['article_name_search']."%')";

	$arguments.="&article_name_search=".$in['article_name_search'];
}
if($in['your_reference_search']){
	$filter_your_ref =" AND pim_orders.your_ref LIKE '%".$in['your_reference_search']."%' ";
	$arguments.="&your_reference_search=".$in['your_reference_search'];
}
if ($in['commercial_name_search']) {
    $filter_commercial_name.= "LEFT JOIN customers ON customers.customer_id = pim_orders.customer_id";
    $filter.= " AND customers.commercial_name LIKE '%".$in['commercial_name_search']."%'";
}
if($in['order_by']){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == '1' || $in['desc']=='true'){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	}
}

if(!isset($in['view'])){
	$in['view'] = 4;
}
$dynamic_join="";
$dynamic_cols="";
$having_filter="";
$tmp_having_filter="";
$show_only_my = true;
if(defined('SHOW_MY_ORDERS_ONLY') && SHOW_MY_ORDERS_ONLY == 1){
	if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])){
		$show_only_my = false;
		if($in['view'] == 0){
			$in['view'] = 4;
		}
		$filter.=" and ( pim_orders.created_by = '".$_SESSION['u_id']."' OR pim_orders.author_id='".$_SESSION['u_id']."' ) ";
	}
}
	switch ($in['view']) {
		case '1':
			$filter.=" and pim_orders.rdy_invoice = '0' and pim_orders.sent='0' ";
			$arguments.="&view=1";
			break;
		case '2':
			$filter.=" and pim_orders.rdy_invoice != '1' and pim_orders.rdy_invoice != '2' and pim_orders.sent='1'";
			$arguments.="&view=2";
			break;
		case '3':
			$filter.=" and pim_orders.rdy_invoice = '1' ";
			$arguments.="&view=3";
			break;
		case '4':
			if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])) {
				$filter.=" and ( pim_orders.created_by = '".$_SESSION['u_id']."' OR pim_orders.author_id='".$_SESSION['u_id']."' ) ";
			}
			$arguments.="&view=4";
			break;
		case '5':
			$filter.=" and pim_orders.rdy_invoice = '2' ";
			$arguments.="&view=5";
		break;
		case '6':
			$dynamic_join.=" LEFT JOIN pim_order_deliveries AS order_deliveries_not_done ON order_deliveries_not_done.order_id=pim_orders.order_id AND order_deliveries_not_done.delivery_done='0' ";
			$dynamic_cols.=",COALESCE(COUNT(order_deliveries_not_done.delivery_id),0) AS total_deliveries_not_done";
			$filter.=" and pim_orders.rdy_invoice = '2' ";
			$tmp_having_filter.="AND total_deliveries_not_done>0 ";
			$arguments.="&view=6";
		break;
		default:
			break;
	}

switch($in['invoice_status_id']){
	case '1':
		$dynamic_join.=" LEFT JOIN tbldownpayments ON tbldownpayments.order_id=pim_orders.order_id LEFT JOIN pim_orders_delivery ON pim_orders_delivery.order_id=pim_orders.order_id ";
			$dynamic_cols.=",COALESCE(COUNT(tbldownpayments.id),0) AS downpayments_nr,SUM(pim_orders_delivery.invoiced) AS invoices_sum";
			$tmp_having_filter.="AND downpayments_nr=0 AND invoices_sum=0 ";
			$filter.=" AND pim_orders.invoiced!='1' ";
		break;
	case '2':
		$dynamic_join.=" LEFT JOIN tbldownpayments ON tbldownpayments.order_id=pim_orders.order_id ";
			$dynamic_cols.=",COALESCE(COUNT(tbldownpayments.id),0) AS downpayments_nr";
			$tmp_having_filter.="AND downpayments_nr>0 ";
		break;
	case '3':
		$dynamic_join.=" LEFT JOIN pim_orders_delivery ON pim_orders_delivery.order_id=pim_orders.order_id ";
			$dynamic_cols.=",SUM(pim_orders_delivery.invoiced) AS invoices_sum";
			$filter.=" AND pim_orders.invoiced!='1' ";
			$tmp_having_filter.="AND invoices_sum>0 ";
		break;
	case '4':
		$filter.=" AND pim_orders.invoiced='1' ";
		break;
}
if($tmp_having_filter){
    $having_filter=" HAVING ".ltrim($tmp_having_filter,"AND");
}

if($in['start_date'] && $in['stop_date']){
	$filter.=" and pim_orders.date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
	$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}
else if($in['start_date']){
	$filter.=" and cast(pim_orders.date as signed) > ".$in['start_date']." ";
	$arguments.="&start_date=".$in['start_date'];
}
else if($in['stop_date']){
	$filter.=" and cast(pim_orders.date as signed) < ".$in['stop_date']." ";
	$arguments.="&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}

if($in['d_start_date'] && $in['d_stop_date']){
	$filter.=" and pim_orders.del_date BETWEEN '".$in['d_start_date']."' and '".$in['d_stop_date']."' ";
	$arguments.="&d_start_date=".$in['d_start_date']."&d_stop_date=".$in['d_stop_date'];
	$filter_link = 'none';
}
else if($in['d_start_date']){
	$filter.=" and cast(pim_orders.del_date as signed) > ".$in['d_start_date']." ";
	$arguments.="&d_start_date=".$in['d_start_date'];
}
else if($in['d_stop_date']){
	$filter.=" and cast(pim_orders.del_date as signed) < ".$in['d_stop_date']." ";
	$arguments.="&d_stop_date=".$in['d_stop_date'];
	$filter_link = 'none';
}

if($in['new_orders']==1) {
	$days_30 = time() - 30*3600*24;
	$now = time();
	$filter .= " AND pim_orders.rdy_invoice!=1 AND date BETWEEN ".$days_30." AND ".$now." ";
}

$nav = array();

$arguments = $arguments.$arguments_c;
$max_rows_data = $db->query("SELECT serial_number as iii, cast( FROM_UNIXTIME( pim_orders.date ) AS DATE ) AS t_date, pim_orders.order_id".$dynamic_cols."
			FROM pim_orders
			".$dynamic_join."
			".$filter_article."
			".$filter_commercial_name." 
			".$filter."
			".$filter_your_ref." 
			GROUP BY pim_orders.order_id
			".$having_filter." ".$order_by);
$max_rows=$max_rows_data->records_count();

if(!$_SESSION['tmp_add_to_pdf_o'] || ($_SESSION['tmp_add_to_pdf_o'] && empty($_SESSION['tmp_add_to_pdf_o']))){
	while($max_rows_data->next()){	
		$_SESSION['tmp_add_to_pdf_o'][$max_rows_data->f('order_id')]=1;
		array_push($nav, (object)['order_id'=> $max_rows_data->f('order_id') ]);
	}

}else{
	while($max_rows_data->next()){	
		array_push($nav, (object)['order_id'=> $max_rows_data->f('order_id') ]);
	}
}

$all_pages_selected=false;
$minimum_selected=false;
if($_SESSION['add_to_pdf_o']){
	if($max_rows>0 && count($_SESSION['add_to_pdf_o']) == $max_rows){
		$all_pages_selected=true;
	}else if(count($_SESSION['add_to_pdf_o'])){
		$minimum_selected=true;
	}	
}
$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;
/*$orders = $db->query("SELECT SUM( pim_orders_delivery.invoiced ) AS invoiced_del, pim_orders.*, serial_number as iii, cast( FROM_UNIXTIME( `date` ) AS DATE ) AS t_date
			FROM pim_orders
			LEFT JOIN pim_orders_delivery ON pim_orders_delivery.order_id = pim_orders.order_id
			".$filter_article."
			 ".$filter." GROUP BY pim_orders.order_id ".$order_by );*/
$orders = $db->query("SELECT pim_orders.order_id,pim_orders.customer_id,pim_orders.author_id,pim_orders.buyer_ref,pim_orders.serial_number,pim_orders.del_date,pim_orders.your_ref,pim_orders.subject,pim_orders.address_info,
					pim_orders.date,pim_orders.amount,pim_orders.customer_name,pim_orders.rdy_invoice,pim_orders.sent,pim_orders.our_ref,pim_orders.invoiced, pim_orders.postgreen_id,serial_number as iii, cast( FROM_UNIXTIME( pim_orders.date ) AS DATE ) AS t_date,
					pim_orders.order_email,pim_orders.your_ref,pim_orders.trace_id,pim_orders.active,pim_orders.currency_type,multiple_identity.identity_name,pim_orders.acc_manager_name,CONCAT_WS(' ',".$config['db_users'].".users.first_name, ".$config['db_users'].".users.last_name) AS author_name,vat_new.description AS vat_regime".$dynamic_cols."
			FROM pim_orders
			LEFT JOIN multiple_identity ON pim_orders.identity_id=multiple_identity.identity_id
			LEFT JOIN vat_new ON pim_orders.vat_regime_id=vat_new.id
			LEFT JOIN ".$config['db_users'].".users ON pim_orders.author_id=".$config['db_users'].".users.user_id
			".$dynamic_join."
			".$filter_article."
			".$filter_commercial_name." 
            ".$filter."
            ".$filter_your_ref." 		
		    GROUP BY pim_orders.order_id ".$having_filter.$order_by." LIMIT ".$offset*$l_r.",".$l_r);

// $max_rows=$orders->records_count();
// $orders->move_to($offset*$l_r);
$j=0;
$color = '';

$result = array('query'=>array(),'max_rows'=>$max_rows,'all_pages_selected'=> $in['exported'] ? false : $all_pages_selected,'minimum_selected'=> $in['exported'] ? false :$minimum_selected, 'nav' => $nav);
while($orders->next()){

	$is_admin = getItemPerm(array('module'=>'order','item'=>$orders->f('author_id')));

	$ref = '';
	if(ACCOUNT_ORDER_REF && $orders->f('buyer_ref')){
		$ref = $orders->f('buyer_ref');

	}

	if($orders->f('sent')==0 ){
		$status = gm('Draft');
	}else if($orders->f('invoiced')){
		if($orders->f('rdy_invoice') == 1){
			$status = gm('Delivered');
		}else{
			$status = gm('Confirmed');
		}
	}else{
		if($orders->f('rdy_invoice') == 2){
			$status = gm('Confirmed');
		}else{
			if($orders->f('rdy_invoice') == 1){
				$status = gm('Delivered');
			}else{
				if($orders->f('sent') == 1){
					$status = gm('Confirmed');
				}else{
					$status = gm('Draft');
				}
			}
		}
	}

	if(!$orders->f('active')){
		$status = gm('Archived');
	}

	$deal_id="";
	$deal_stage="-";
	if($orders->f('trace_id')){
		$deal_data=$db->query("SELECT tblopportunity.opportunity_id,tblopportunity_stage.name AS stage_name FROM tracking_line 
			INNER JOIN tblopportunity ON tracking_line.origin_id=tblopportunity.opportunity_id
			INNER JOIN tblopportunity_stage ON tblopportunity.stage_id=tblopportunity_stage.id
			WHERE tracking_line.trace_id='".$orders->f('trace_id')."' AND tracking_line.origin_type='11' ")->getAll();
		if(!empty($deal_data)){
			$deal_id=$deal_data[0]['opportunity_id'];
			$deal_stage=$deal_data[0]['stage_name'];
		}
	}

  //$order_email=$db->field("SELECT order_email FROM pim_orders WHERE order_id='".$orders->f('order_id')."'");
	$order_email= $orders->f('order_email');
  $has_downpayment=$db->field("SELECT invoice_id FROM tbldownpayments WHERE order_id='".$orders->f('order_id')."'");
  $invoice_del=$db->field("SELECT SUM(invoiced) FROM pim_orders_delivery WHERE order_id='".$orders->f('order_id')."'");
	$inv_ord = $invoice_del;

	$is_delivery_planned=false;
   if(ORDER_DELIVERY_STEPS == '2'){
   		$is_partial_deliver = false;
   		if($orders->f('rdy_invoice')==2){
   			$dels=$db->query("SELECT * FROM pim_order_deliveries WHERE order_id='".$orders->f('order_id')."'");
	   		while($dels->next()){
	   			//daca cel putin una din livrarile partiale sunt confirmate, atunci is_partial_deliver = true
	   			if($dels->f('delivery_done')=='1'){
	   				$is_partial_deliver = true;
	   				$is_delivery_planned=false;
	   				break;
	   			}else{
	   				$is_delivery_planned=true;
	   			}
	   		}
   		}
   		

    }else{
    	 $is_partial_deliver= $orders->f('rdy_invoice')==2?true:false;
    }
	
	if($is_partial_deliver && !$is_delivery_planned && $orders->f('sent') == 1){
		$status = gm('Partially Delivered');
	}else if($is_delivery_planned && $orders->f('sent') == 1){
		$status = gm('Delivery planned');
	}

	$delivery_country='';
	$address_info =  nl2br($orders->f('address_info'));
	$delivery_arr=explode('<br />',$address_info );
	$last = count($delivery_arr)-1;
	$delivery_country =$delivery_arr[$last];

	$item =array(
		'info_link'    	 				=> 'index.php?do=order-order&order_id='.$orders->f('order_id').$arguments,
		'edit_link'     				=> 'index.php?do=order-norder&order_id='.$orders->f('order_id').$arguments,
		// 'delete_link'  					=> 'index.php?do=order-orders_list-order-delete_order&order_id='.$orders->f('order_id').$arguments,
		'delete_link'  					=> array('do'=>'order-orders-order-delete_order','order_id'=>$orders->f('order_id')),
		// 'undo_link' 	 					=> 'index.php?do=order-orders_list-order-activate_order&order_id='.$orders->f('order_id').$arguments,
		'undo_link' 	 					=> array('do'=>'order-orders-order-activate_order','order_id'=>$orders->f('order_id')),
		// 'archice_link'  				=> 'index.php?do=order-orders_list-order-archive_order&order_id='.$orders->f('order_id').$arguments ,
		'archive_link'  				=> array('do'=>'order-orders-order-archive_order','order_id'=>$orders->f('order_id')),
		// 'view_delete_link2'			=> $_SESSION['access_level'] == 1 ? ($in['archived'] ? '' : 'hide') : 'hide',
		// 'hide_on_a'							=> $in['archived'] == 1 ? 'hide' : '',
		'id'              	  	=> $orders->f('order_id'),
		'serial_number'   	  	=> $orders->f('serial_number') ? $ref.$orders->f('serial_number') : '',
		'created'         	  	=> date(ACCOUNT_DATE_FORMAT,$orders->f('date')),
		'delivery_date'   	   	=> $orders->f('del_date')?date(ACCOUNT_DATE_FORMAT,$orders->f('del_date')):'',
		'amount'								=> place_currency(display_number($orders->f('amount')), get_commission_type_list($orders->f('currency_type'))),
		'buyer_name'      	  	=> $orders->f('customer_name'),
		// 'is_editable'				=> $orders->f('rdy_invoice') ? false : true,
		'is_editable'						=> $orders->f('rdy_invoice') ? false : ($orders->f('sent') == 1 ? false : true),
		'is_archived'     	  	=> $in['archived'] ? true : false,
		//'path'								=> $config['site_url'],
		'status'				=> $orders->f('sent') == 1 && $orders->f('rdy_invoice') ? ($orders->f('rdy_invoice')==1 ? 'green' : 'orange') : ($orders->f('sent') == 1 ? 'orange' : 'gray'),
		'status2'								=> $status,
		'our_ref'       	 			=> $orders->f('our_ref'),
		'your_ref'        		=> $orders->f('your_ref'),
		// 'alt_for_delete_icon'		=> $in['archived'] == 1 ? 'Delete' : 'Archive',
		'order_id'							=> $orders->f('order_id'),
		'check_add_to_product'	=> $in['exported'] ? false : ($_SESSION['add_to_pdf_o'][$orders->f('order_id')] == 1 ? true : false),
		// 'is_invoice'						=> empty($inv_ord) ? false : true,
		'is_invoice'						=> (!empty($inv_ord) || $orders->f('invoiced') == 1 ) ? true : false,
		/*'invAll'								=> $orders->f('rdy_invoice')==1 ? 'inv_ordGreenIcons' : '',
		'invAll_info'						=> $orders->f('rdy_invoice')==1 ? gm('Invoiced') : gm('Partially Invoiced'),*/
		'invAll'								=> ($orders->f('invoiced')==1 ) ? 'inv_ordGreenIcons' : '',
		'invAll_info'						=> ($orders->f('invoiced')==1  )?gm('Invoiced') : (  !empty($inv_ord) ? gm('Partially Invoiced'):''),

		'is_admin'							=> $is_admin,
		'is_front'							=> $order_email?true:false,
		//'is_partial_deliver'    => $orders->f('rdy_invoice')==2?true:false,
		'is_partial_deliver'    => $is_partial_deliver,
		'confirm'				=> gm('Confirm'),
		'ok'					=> gm('Ok'),
		'cancel'				=> gm('Cancel'),
		'partialDEL_info'       => gm('Partially Delivered'),
		'post_order'		    => $orders->f('postgreen_id')!='' ? true : false,
		'has_downpayment'    	=> $has_downpayment? true:false,
		'downpayment_title'       => gm('A downpayment has been invoiced from this order'),
		//'deal_linked'        => $orders->f('traking_deal_line_id') ? true : false
		'deal_linked'        => $deal_id ? true : false,
		'deal_stage'		 =>  $deal_stage,
		'identity_name'				=> $orders->f('identity_name') ? stripslashes($orders->f('identity_name')) : gm('Main Identity'),
		'acc_manager_name'	=> stripslashes($orders->f('acc_manager_name')),
		'author_name'		=> utf8_decode(stripslashes($orders->f('author_name'))),
		// 'status'								=> $orders->f('rdy_invoice') ? ($orders->f('rdy_invoice')==1 ? 'green' : 'orange') : ($orders->f('sent') == 1 ? 'orange' : 'gray'),
		'vat_regime'				=> stripslashes($orders->f('vat_regime')),
		'subject'					=> stripslashes($orders->f('subject')),
		'delivery_country'					=> $delivery_country
	);

	if(!$orders->f('active')){
		$item['status'] = 'gray';
	}

	/*if(ORDER_DELIVERY_STEPS==2){
		$count_del = $db3->field("SELECT COUNT(delivery_id) FROM pim_order_deliveries WHERE order_id = '".$orders->f('order_id')."' AND delivery_done = '0' ");
		$count_done_del = $db3->field("SELECT COUNT(delivery_id) FROM pim_order_deliveries WHERE order_id = '".$orders->f('order_id')."' AND delivery_done = '1' ");
		/*$view_list->assign(array(
			'status'				=> $orders->f('rdy_invoice') ? ($orders->f('rdy_invoice')==1 ? ($count_del == 0 ? 'green' : 'orange' ) : 'orange') : ($orders->f('sent') == 1 ? 'orange' : 'gray'),
		),'order_row');* /
	}*/
	array_push($result['query'], $item);
	// $view_list->loop('order_row');
	$j++;
}
$result['all_current_page_selected']=$all_current_page_selected;

$post_active=$db->field("SELECT active FROM apps WHERE name='PostGreen' AND main_app_id='0' AND type='main' ");

$default_lang_id = $db->field("SELECT lang_id FROM pim_lang WHERE default_lang = '1' ");
$default_pdf_type = ACCOUNT_ORDER_PDF_FORMAT;

$result['lid']				= $default_lang_id;
$result['pdf_type']		= $default_pdf_type;
$result['lr']		= $l_r;
$result['post_active']= !$post_active || $post_active==0 ? false : true;
$result['export_args'] = ltrim($arguments,'&');

$result['columns']=array();
$cols_default=default_columns_dd(array('list'=>'orders'));
$cols_order_dd=default_columns_order_dd(array('list'=>'orders'));
$cols_selected=$db->query("SELECT * FROM column_settings WHERE list_name='orders' AND user_id='".$_SESSION['u_id']."' ORDER BY sort_order")->getAll();
if(!count($cols_selected)){
	$i=1;
    $default_selected_columns_dd = default_selected_columns_dd(array('list'=>'orders'));
    foreach($cols_default as $key=>$value){
		$tmp_item=array(
			'id'				=> $i,
			'column_name'		=> $key,
			'name'				=> $value,
			'order_by'			=> $cols_order_dd[$key]
		);
        if ($default_selected_columns_dd[$key]) {
            array_push($result['columns'],$tmp_item);
        }		$i++;
	}
}else{
	foreach($cols_selected as $key=>$value){
		$tmp_item=array(
			'id'				=> $value['id'],
			'column_name'		=> $value['column_name'],
			'name'				=> $cols_default[$value['column_name']],
			'order_by'			=> $cols_order_dd[$value['column_name']]
		);
		array_push($result['columns'],$tmp_item);
	}
}
$result['views']=[[
        'name' => gm('All Statuses'),
        'id'=> 0,
        'active' => ''
    ], 
    [
        'name' => gm('My orders'),
        'id' => 4,
        'active' => 'active'
    ], 
    [
        'name' => gm('Draft'),
        'id' => 1,
        'active' => ''
    ], 
    [
        'name' => gm('Ready To Deliver'),
        'id' => 2,
        'active' => ''
    ], 
    [
        'name' => gm('Backorders'),
        'id' => 5,
        'active' => ''
    ], 
    [
        'name' => gm('Fully Delivered'),
        'id' => 3,
        'active' => ''
    ]];
if(defined('ORDER_DELIVERY_STEPS') && ORDER_DELIVERY_STEPS == '2'){
	array_push($result['views'],[
		'name' => gm('Delivery planned'),
        'id' => 6,
        'active' => ''
	]);
}
$result['invoice_statuses']=[[
        'name' => gm('All Invoice Statuses'),
        'id'=> 0,
        'active' => ''
    ], 
    [
        'name' => gm('Not Invoiced'),
        'id' => 1,
        'active' => ''
    ], 
    [
        'name' => gm('Downpayment Invoiced'),
        'id' => 2,
        'active' => ''
    ], 
    [
        'name' => gm('Partially Invoiced'),
        'id' => 3,
        'active' => ''
    ], 
    [
        'name' => gm('Invoiced'),
        'id' => 4,
        'active' => ''
    ]];

json_out($result);
