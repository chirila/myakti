<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$view_list = new at(ark::$viewpath.'return_article.html');
//add
$return = array('lines'=>array());
if(!$in['return_id']){
   if(!$in['date']){
		$return['date_r'] = date(ACCOUNT_DATE_FORMAT, mktime(0, 0, 0, date("n"), date("j"), date("Y")));
		$return['date'] = time()*1000;
	}
}else{
	$return['return_id'] = $in['return_id'];
	$db->query("SELECT pim_orders_return.* FROM pim_orders_return WHERE  pim_orders_return.return_id='".$in['return_id']."'");

    $db->move_next();

    $return['date']  = $db->gf('date')*1000;
    $return['date_r']    = date(ACCOUNT_DATE_FORMAT,$db->gf('date'));
    $return['note'] = $db->gf('note');


}
$return['order_id'] = $in['order_id'];
$order =  $db->query("SELECT pim_orders.use_package,pim_orders.use_sale_unit
	                  FROM pim_orders
	                  WHERE pim_orders.order_id='".$in['order_id']."'");
$order->move_next();
$allow_packing = $order->f('use_package');
$allow_sale_unit = $order->f('use_sale_unit');

// $view_list->assign(array(
	$return['allow_article_packing']       = $allow_packing;
	$return['allow_article_sale_unit']     = $allow_sale_unit;
// ));
$i = 0;
	$line = $db->query("SELECT pim_order_articles.*, pim_orders_delivery.*
		                FROM pim_order_articles
					    INNER JOIN pim_orders_delivery ON pim_orders_delivery.order_articles_id=pim_order_articles.order_articles_id
					    WHERE pim_orders_delivery.order_id='".$in['order_id']."'
                        GROUP BY pim_order_articles.article_id
					    ORDER BY sort_order ASC");
	$k=0;
	while ($line->next()) {
		$delivered = 0;

		$use_serial_no = $db->field("SELECT use_serial_no FROM pim_articles WHERE article_id = '".$line->f('article_id')."' ");
		if($use_serial_no==1){
			$j++;
		}
		
		$use_batch_no = $db->field("SELECT COUNT(id) FROM batches_from_orders WHERE article_id = '".$line->f('article_id')."' AND order_id='".$in['order_id']."' ");
		if($use_batch_no>0){
			$k++;
		}
		if(ORDER_DELIVERY_STEPS==2){
		$delivered = $db->field("SELECT SUM(pim_orders_delivery.quantity)
			                     FROM pim_orders_delivery
			                     INNER JOIN  pim_order_deliveries ON  pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id AND pim_order_deliveries.delivery_done=1
			                     WHERE pim_orders_delivery.order_id='".$in['order_id']."' AND pim_orders_delivery.order_articles_id='".$line->f('order_articles_id')."' ");
		}else{
				$delivered = $db->field("SELECT SUM(pim_orders_delivery.quantity)
			                     FROM pim_orders_delivery

			                     WHERE pim_orders_delivery.order_id='".$in['order_id']."' AND pim_orders_delivery.order_articles_id='".$line->f('order_articles_id')."' ");
		}
		$returned=$db->field("SELECT pim_articles_return.quantity FROM pim_articles_return WHERE return_id='".$in['return_id']."' AND article_id='".$line->f('article_id')."' ");
    $quantity_already_ret=$db->field("SELECT sum(pim_articles_return.quantity)
        	                              FROM pim_articles_return
        	                              WHERE return_id!='".$in['return_id']."' AND order_id='".$in['order_id']."' AND article_id='".$line->f('article_id')."' ");

    $packing   = $line->f('packing');
    $sale_unit = $line->f('sale_unit');

	$linie = array(
			'article'								=> $line->f('article'),
			'article_id'						=> $line->f('article_id'),
			'article_code'		    	=> $line->f('article_code'),
			'quantity'							=> display_number($delivered),
			'quantity_rem'					=> display_number($delivered-$quantity_already_ret),
			'order_articles_id' 		=> $line->f('order_articles_id'),
      'packing'               => remove_zero_decimals($packing),
      'sale_unit'             => $sale_unit,
      //'ret_quantity'          => display_number($returned),
      //'ret_quantity'          => $returned!=0? display_number($returned) : display_number($delivered-$quantity_already_ret),
      'ret_quantity'          => $returned!=0? display_number($returned) : ($in['return_id']? display_number($returned):display_number($delivered-$quantity_already_ret)),
      'quantity_already_ret'  => display_number($quantity_already_ret),
      'old_ret_quantity'          => $returned,
      'use_serial_no_art'	  => $use_serial_no >0 ? true : false,
      'use_batch_no_art'	  => $use_batch_no>0 ? true : false,
      'is_error'			  => false,
      'batchOk'				  => $in['return_id']? true : false,
      'no_quantity'			  => ($in['return_id'] && $returned==0)? true : false,
		);
	// $view_list->loop('view_order_line');
		array_push($return['lines'], $linie);
		$i++;

	}
	if($i>0){
		$data = true;
	}


// $view_list->assign(array(
	$return['do_next']			     = $in['return_id'] ? 'order-order-order-edit_return' : 'order-order-order-make_return';
	//$return['use_batch_no']       = $k > 0 && ALLOW_STOCK ? true : false;
	$return['use_batch_no']       = $k > 0 ? true : false;
	$return['use_serial_no'] 	= $j > 0 ? true : false;
	// 'style'     	     => ACCOUNT_NUMBER_FORMAT,
	// 'is_data'		     => $data,
	// 'pick_date_format'   => pick_date_format(ACCOUNT_DATE_FORMAT)
// ));

json_out($return);
// return $view_list->fetch();
?>