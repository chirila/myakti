<?php

$result = array('lines'=>array());

$linie=array(
	'name'				=> ACCOUNT_COMPANY,
	'address'			=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
	'zip'				=> ACCOUNT_DELIVERY_ZIP,
	'city'				=> ACCOUNT_DELIVERY_CITY,
	'country'			=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
	'phone'				=> ACCOUNT_PHONE,
	'fax'				=> ACCOUNT_FAX,
	'email'				=> ACCOUNT_EMAIL,
	'url'				=> ACCOUNT_URL,
	'tmp_id'			=> ACCOUNT_ORDER_PDF_FORMAT,
	//'logo'				=> '../'.ACCOUNT_LOGO_ORDER,
	'logo'				=> ACCOUNT_LOGO_ORDER,
	'identity_id'		=> '0',
);
array_push($result['lines'],$linie);

$q = $db->query("SELECT * FROM multiple_identity")->getAll();
foreach ($q as $key => $value) {
	$template = $db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$value['identity_id']."' and module='order'");
	$linie = array(
		'name'			=> $value['identity_name'],
		'address'		=> nl2br($value['company_address']),
		'zip'			=> $value['company_zip'],
		'city'			=> $value['city_name'],
		'country'		=> get_country_name($value['country_id']),
		'phone'			=> $value['company_phone'],
		'fax'			=> $value['company_fax'],
		'email'			=> $value['company_email'],
		'url'			=> $value['company_url'],
		'tmp_id'		=> $template ? $template : "1",
		'logo'			=> $value['company_logo'],
		'identity_id'	=> $value['identity_id'],		
	);
	array_push($result['lines'],$linie);
}
if(!$in['identity_id']){
	$in['identity_id']=0;
}
$result['identity_id'] = $in['identity_id'];
json_out($result);


?>