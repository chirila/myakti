<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}


/*	function get_logos($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'logos'=>array(), 'logo'=>array());
		$default_logo = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_LOGO_ORDER' ");
		if($default_logo == '' || $default_logo == 'pim/img/no-logo.png')   {
		   	array_push($data['logo'], array(
        'account_logo'=>'images/no-logo.png',
        'default'=>'images/no-logo.png'
        ));
		}
		else {
			$logos = glob(__DIR__.'/../../../upload/'.DATABASE_NAME.'/{o_logo_img}_*',GLOB_BRACE);
		    foreach ($logos as $v) {
		        $logo = preg_replace('/\/(.*)\//', 'upload/'.DATABASE_NAME.'/', $v);
		        $size = @getimagesize($logo);
		        $ratio = 250 / 77;
		        if($size[0]/$size[1] > $ratio ){
		            $attr = 'width="250"';
		        }else{
		            $attr = 'height="77"';
		        }
		        array_push($data['logos'], array(
		        'account_logo'=>$logo,
		        'default'=>$default_logo,
		        'attr' => $attr
		        ));
		    }
		}
		$data['default_logo'] = $default_logo;


			
		return json_out($data, $showin,$exit);
	}*/
	function get_PDFlayout($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'layout_header'=>array(),'custom_layout'=>array(),'layout_body'=>array(),'layout_footer'=>array(),'layout_header_delivery'=>array(),'custom_layout_delivery'=>array(),'layout_body_delivery'=>array(),'layout_footer_delivery'=>array());
		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		$def = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_PDF_FORMAT' ");
		$def_delivery = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_DELIVERY_PDF_FORMAT' ");
		$use = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_ORDER_PDF' ");
		$use_delivery = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_DELIVERY_PDF' ");
		if(!$in['identity_id']){
			$in['identity_id']='0';
		}

		$existIdentity = $db->query("SELECT identity_id FROM identity_layout  ");
		$data['multiple_identity']				= build_identity_dd($in['identity_id']);
		$data['identity']						= $in['identity_id'];

		$data['show_backorders']				= $db->field("SELECT value FROM settings WHERE constant_name='SHOW_BACKORDERS_DELIVERY' ")? true:false;

		$data['has_custom_layout']=false;
		if(DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='SalesAssist_11' || DATABASE_NAME=='03a0fb2c_c144_cda3_adda851c8e4b' || DATABASE_NAME=='caff4267_64a4_1db2_d49f2ab3760c' || DATABASE_NAME=='c4482c81_7d14_c57e_2068878ad2f4'){
			$data['has_custom_layout']=true;
		}

		$data['use_custom_layout']= ($use ==1)? true:false;
		$data['use_custom_layout_delivery']= ($use_delivery==1)? true:false;

		for ($j=1; $j <= 1; $j++) {
		array_push($data['custom_layout'], array(
				'view_invoice_custom_pdf' 						=>'index.php?do=order-print&custom_type='.$j.'&lid='.$language,
				'img_href_custom'								=> 'images/custom_type-'.$j.'.jpg',
				'custom_type'									=> $j,
				//'action_href'									=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$j.'&tab=6',
				'selected_custom'								=> $def == $j && $use == 1 ? 'active' : '',
			
			));
		}

		$language=$db->field("SELECT lang_id FROM pim_lang where default_lang=1");
		$data['lang_id']=$language;

		$data['lang_id']=$_SESSION['lang_id'];
		$data['header']='header';
		$data['body']='body';
		$data['footer']='footer';
		//order
		$def_header = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_HEADER_PDF_FORMAT' ");
		$use_header = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_ORDER_PDF' ");
		$def_body = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_BODY_PDF_FORMAT' ");
		$use_body = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_ORDER_PDF' ");
		$def_footer = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_FOOTER_PDF_FORMAT' ");
		$use_footer = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_ORDER_PDF' ");
		//delivery
		$def_header_delivery = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_DELIVERY_HEADER_PDF_FORMAT' ");
		$use_header_delivery = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_DELIVERY_PDF' ");
		$def_body_delivery = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_DELIVERY_BODY_PDF_FORMAT' ");
		$use_body_delivery = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_DELIVERY_PDF' ");
		$def_footer_delivery = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_DELIVERY_FOOTER_PDF_FORMAT' ");
		$use_footer_delivery = $db->field("SELECT value FROM settings WHERE constant_name='USE_CUSTOME_DELIVERY_PDF' ");
		$number_type = $_SESSION['u_id']<11464 ? 2 : 1;
		for ($z=1; $z <= 1; $z++) {
				array_push($data['layout_header'], array(
					'view_invoice_pdf' 				=>'index.php?do=order-order_print&header='.$data['header'].'&layout='.$z.'&lid='.$language.'&header_id='.$def_header.'&save_as=preview',
					'img_href'						=> 'images/type_header-'.$number_type.'.jpg',
					'type'							=> $z,
					'header'						=> 'header',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_header == $z && $use_header == 0 ? 'active' : '',
					'identity_id'					=> $in['identity_id'],
				));
		}
		for ($y=1; $y <= 1; $y++) {
				array_push($data['layout_body'], array(
					'view_invoice_pdf' 				=>'index.php?do=order-order_print&layout='.$y.'&lid='.$language.'&body='.$data['body'].'&save_as=preview',
					'img_href'						=> 'images/type_body-'.$number_type.'.jpg',
					'type'							=> $y,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_body == $y && $use_body == 0 ? 'active' : '',
					'identity_id'					=> $in['identity_id'],
				));
		}
		for ($i=1; $i <= 2; $i++) {
				array_push($data['layout_footer'], array(
					'view_invoice_pdf' 				=>'index.php?do=order-order_print&footer='.$data['footer'].'&layout='.$i.'&lid='.$language.'&footer_id='.$i.'&save_as=preview',
					'img_href'						=> 'images/type_footer-'.$i.'.jpg',
					'type'							=> $i,
					'footer'						=> 'footer',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_footer == $i && $use_footer == 0 ? 'active' : '',
					'identity_id'					=> $in['identity_id'],
				));
		}

		for ($j=1; $j <= 1; $j++) {
		array_push($data['custom_layout_delivery'], array(
				'view_invoice_custom_pdf' 						=>'index.php?do=order-order_delivery_print&custom_type='.$j.'&lid='.$language,
				'img_href_custom'								=> 'images/custom_type-'.$j.'.jpg',
				'custom_type_delivery'									=> $j,
				//'action_href'									=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$j.'&tab=6',
				'selected_custom_delivery'								=> $def_delivery == $j && $use_delivery == 1 ? 'active' : '',
				'identity_id'					=> $in['identity_id'],
			));
		}

		for ($z=1; $z <= 1; $z++) {
				array_push($data['layout_header_delivery'], array(
					'view_invoice_pdf' 				=>'index.php?do=order-order_delivery_print&header='.$data['header'].'&layout='.$z.'&lid='.$language.'&header_id='.$def_header.'&save_as=preview',
					'img_href'						=> 'images/type_header-'.$number_type.'.jpg',
					'type'							=> $z,
					'header'						=> 'header',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_header_delivery == $z && $use_header_delivery == 0 ? 'active' : '',
					'identity_id'					=> $in['identity_id'],
				));
		}
		for ($y=1; $y <= 1; $y++) {
				array_push($data['layout_body_delivery'], array(
					'view_invoice_pdf' 				=>'index.php?do=order-order_delivery_print&layout='.$y.'&lid='.$language.'&body='.$data['body'].'&save_as=preview',
					'img_href'						=> 'images/type_body-'.$number_type.'.jpg',
					'type'							=> $y,
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_body_delivery == $y && $use_body_delivery == 0 ? 'active' : '',
					'identity_id'					=> $in['identity_id'],
				));
		}
		for ($i=1; $i <= 2; $i++) {
				array_push($data['layout_footer_delivery'], array(
					'view_invoice_pdf' 				=>'index.php?do=order-order_delivery_print&footer='.$data['footer'].'&layout='.$i.'&lid='.$language.'&footer_id='.$i.'&save_as=preview',
					'img_href'						=> 'images/type_footer-'.$i.'.jpg',
					'type'							=> $i,
					'footer'						=> 'footer',
					//'action_href'					=> 'index.php?do=maintenance-maintenance_settings-maintenance-set_pdf&invoice_pdf_format='.$i.'&tab=6',
					'selected'						=> $def_footer_delivery == $i && $use_footer_delivery == 0 ? 'active' : '',
					'identity_id'					=> $in['identity_id'],
				));
		}

		$data['selected_header'] = $def_header;
		$data['selected_body'] = $def_body;
		$data['selected_footer'] = $def_footer;

		$data['selected_header_delivery'] = $def_header_delivery;
		$data['selected_body_delivery'] = $def_body_delivery;
		$data['selected_footer_delivery'] = $def_footer_delivery;

		//

		return json_out($data, $showin,$exit);
	}
	function get_Convention($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('convention'=>array());
		$db->query("SELECT * FROM settings WHERE type=1");
		while($db->move_next()){
			/*	$view->assign( $db->f('constant_name'), $db->f('value'));*/
			if($db->f('constant_name') == 'ACCOUNT_ORDER_START'){
				$order_start = $db->f('value');
			}
			if($db->f('constant_name') == 'ACCOUNT_ORDER_DIGIT_NR'){
				$order_digit = $db->f('value');
			}
			if($db->f('constant_name') == 'ACCOUNT_ORDER_DEL'){
				$order_del = $db->f('value');
			}
			if($db->f('constant_name') == 'ACCOUNT_ORDER_REF'){
				$order_ref = $db->f('value');
			}
			if($db->f('constant_name') == 'ACCOUNT_ORDER_WEIGHT'){
				$order_weight = $db->f('value');
			}
		}
		$db->query("SELECT * from settings where type=2");
		while($db->move_next()){
			// $view->assign( $db->f('constant_name'), $db->f('long_value'));
		}

		$show_for_one_user = true;
		if(defined('PAYMILL_CLIENT_ID') && (defined('HOW_MANY') && HOW_MANY == 0) ) {
		    $show_for_one_user = false;
		}


		$data['convention'] = array(
			'ACCOUNT_ORDER_START'=>$order_start,
			'ACCOUNT_ORDER_DIGIT_NR'=>$order_digit,
			'CHECKED' => $order_ref ? true : false,
			'WEIGHT_CHECKED' => $order_weight ? true : false,
			'ACCOUNT_ORDER_DEL'	=> $order_del,
			'EXAMPLE'=>ACCOUNT_ORDER_START.str_pad(1,ACCOUNT_ORDER_DIGIT_NR,"0",STR_PAD_LEFT),

		); 
/*			'ACTIVE_TABB'		=> $in['tabbo'],
			'show_for_one_user' => $show_for_one_user*/
		return json_out($data, $showin,$exit);
	}
	function get_general($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('general_condition'=>array());
		$filter = '';
		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
			if($in['languages'] != 1){
					$filter = '_'.$in['languages'];
			}
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
				$filter = '_'.$in['languages'];
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
				if($in['languages'] != 1){
					$filter = '_'.$in['languages'];
				}
			}
		}
		$default_table = $db->field("SELECT value FROM default_data WHERE default_name='order_note' AND default_main_id='0' AND type='order_note".$filter."' ");
		$data['general_condition']= array(
			'page'			=> $page == 1 ? true : false,
			'notes'			=> $default_table,
			'translate_cls'	=> 'form-language-'.$code,
			'languages'		=>  $in['languages'],
			'do'			=> 'order-settings-order-default_language',
			'xget'			=> 'general',
			'language_dd' 	=>  build_language_dd_new($in['languages']),
		);
		return json_out($data, $showin,$exit);
	}
	function get_customLabel($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'customLabel'=>array());

		$pim_lang = $db->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order");
		while($pim_lang->move_next()){

	    array_push($data['customLabel'], array(
			'name'	                 		=> gm($pim_lang->f('language')),
			'do'							=> 'order-settings',
			'xget'							=> 'labels',
			'label_language_id'	     		=> $pim_lang->f('lang_id'),
			'label_custom_language_id'	=> '',
			));
		}
		$pim_custom_lang = $db->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
		while($pim_custom_lang->move_next()){

	    array_push($data['customLabel'], array(
			'name'	                 			=> $pim_custom_lang->f('language'),
			'do'								=> 'order-settings',
			'xget'								=> 'labels',
			'label_custom_language_id'	    	=> $pim_custom_lang->f('lang_id'),
			'label_language_id'	     			=> '',
			));
		}

		return json_out($data, $showin,$exit);
	}
	function get_labels($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array();
		$const = array();


		$table = 'label_language_order';

		if($in['label_language_id'])
		{
			$filter = "WHERE label_language_id='".$in['label_language_id']."'";
			$id = $in['label_language_id'];
		}elseif($in['label_custom_language_id'])
		{
			$filter = "WHERE lang_code='".$in['label_custom_language_id']."'";
			$id = $in['label_custom_language_id'];
		}

		$db->query("SELECT * FROM $table $filter");

		$data['labels']=array(
				'company_name'						=> $db->f('company_name'),
				'comp_reg_number'					=> $db->f('comp_reg_number'),
				'LABEL_LANGUAGE_ID'  				=> $db->f('label_language_id'),
				'STOCK_DISPATCHING_NOTE'		 	=> $db->f('stock_dispatching_note'),     
				'STOCK_DISPATCHING'	         		=> $db->f('stock_dispatching'),
				'INVOICE_NOTE'		 				=> $db->f('invoice_note'),     
				'INVOICE'	         				=> $db->f('invoice'),
				'QUOTE_NOTE'		 				=> $db->f('quote_note'),     
				'QUOTE'	             				=> $db->f('quote'),
				'ORDER_NOTE'		 				=> $db->f('order_note'),     
				'ORDER'	             				=> $db->f('order'),
				'P_ORDER_NOTE'		 				=> $db->f('p_order_note'),     
				'P_ORDER'	         				=> $db->f('p_order'),
				'ARTICLE'	         				=> $db->f('article'),
				'REFERENCE'	         				=> $db->f('reference'),
				'BILLING_ADDRESS'	 				=> $db->f('billing_address'),
				'DATE'	             				=> $db->f('date'),
				'CUSTOMER'	         				=> $db->f('customer'),
				'ITEM'	             				=> $db->f('item'),
				'QUANTITY'	         				=> $db->f('quantity'),
				'UNITMEASURE'	     				=> $db->f('unitmeasure'),
				'UNIT_PRICE'	     				=> $db->f('unit_price'),
				'AMOUNT'	         				=> $db->f('amount'),
				'SUBTOTAL'	         				=> $db->f('subtotal'),
				'DISCOUNT'	         				=> $db->f('discount'),
				'VAT'	             				=> $db->f('vat'),
				'PAYMENTS'	         				=> $db->f('payments'),
				'AMOUNT_DUE'	     				=> $db->f('amount_due'),
				'GRAND_TOTAL'	     				=> $db->f('grand_total'),
				'NOTES'	             				=> $db->f('notes'),
				'TYPE'				 				=> $in['type'],
				'BANKD'				 				=> $db->f('bank_details'),
				'DUEDATE'			 				=> $db->f('duedate'),
				'BANK_NAME'			 				=> $db->f('bank_name'),
				'IBAN'				 				=> $db->f('iban'),
				'BIC_CODE'			 				=> $db->f('bic_code'),
				'PHONE'				 				=> $db->f('phone'),
				'FAX'				 				=> $db->f('fax'),
				'URL'				 				=> $db->f('url'),
				'EMAIL'				 				=> $db->f('email'),
				'OUR_REF'							=> $db->f('our_ref'),
				'YOUR_REF'			 				=> $db->f('your_ref'),
				'VAT_NUMBER'		 				=> $db->f('vat_number'),
				'CUSTOMER_REF'		 				=> $db->f('CUSTOMER_REF'),
				'GOV_TAXES'			 				=> $db->f('gov_taxes'),
				'GOV_TAXES_CODE'	 				=> $db->f('gov_taxes_code'),
				'GOV_TAXES_TYPE'	 				=> $db->f('gov_taxes_type'),
				'SALE_UNIT'	         				=> $db->f('sale_unit'),
				'PACKAGE'	         				=> $db->f('package'),
				'SOURCE'	         				=> $db->f('source'),
				'QUOTE_TYPE'		 				=> $db->f('type'),
				'AUTHOR'	         				=> $db->f('author'),
				'SHIPPING_PRICE'	 				=> $db->f('shipping_price'),
				'ARTICLE_CODE'	     				=> $db->f('article_code'),
				'DELIVERY_DATE'	     				=> $db->f('delivery_date'),
				'ENTRY_DATE'	     				=> $db->f('entry_date'),
				'DELIVERY_NOTE'	 	 				=> $db->f('delivery_note'),
				'ENTRY_NOTE'	 	 				=> $db->f('entry_note'),
				'PICK_UP_FROM_STORE' 				=> $db->f('pick_up_from_store'),
				'PAGE'				 				=> $db->f('page'),
				'download_webl'	     				=> $db->f('download_webl'),
				'pdfPrint_webl'		    			=> $db->f('pdfPrint_webl'),
				'name_webl'	     	 				=> $db->f('name_webl'),
				'email_webl'	     				=> $db->f('email_webl'),
				'addCommentLabel_webl'				=> $db->f('addCommentLabel_webl'),
				'submit_webl'	     				=> $db->f('submit_webl'),
				'p_order_webl'	     				=> $db->f('p_order_webl'),
				'date_webl'	     					=> $db->f('date_webl'),
				'accept_webl'	     				=> $db->f('accept_webl'),
				'requestNewVersion_webl'			=> $db->f('requestNewVersion_webl'),
				'reject_webl'	    				=> $db->f('reject_webl'),
				'status_webl'	     				=> $db->f('status_webl'),
				'bankDetails_webl'	    			=> $db->f('bankDetails_webl'),
				'payWithIcepay_webl'				=> $db->f('payWithIcepay_webl'),
				'cancel_webl'						=> $db->f('cancel_webl'),
				'regularInvoiceProForma_webl'		=> $db->f('regularInvoiceProForma_webl'),
				'bicCode_webl'						=> $db->f('bicCode_webl'),
				'ibanCode_webl'						=> $db->f('ibanCode_webl'),
				'bank_webl'							=> $db->f('bank_webl'),
				'noData_webl'						=> $db->f('noData_webl'),
				'pickup_address'					=> $db->f('pickup_address'),
				'downpayment_paid'					=> $db->f('downpayment_paid'),
				'downpayment_amount_vat'			=> $db->f('downpayment_amount_vat'),
				'downpayment_balance'				=> $db->f('downpayment_balance'),
				'signature'							=> $db->f('signature'),
				'amount_order_vat'					=> $db->f('amount_order_vat'),
				'serial_numbers'					=> $db->f('serial_numbers'),
				'batches'							=> $db->f('batches'),
				'batch_expiration_date'				=> $db->f('batch_expiration_date'),
				'origin_code'						=> $db->f('origin_code'),
				'do'								=> 'order-settings-order-label_update',
				'xget'								=> 'labels',
				'label_language_id'					=> $id
		);
		return json_out($data, $showin,$exit);
	}
	function get_emailMessage($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('message'=>array(),'message_labels'=>array(),'message_po'=>array());

		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
			}
		}

		$array_values=array();
		$array_values['ACCOUNT_COMPANY']=gm("Account Name");
		$array_values['SERIAL_NUMBER']=gm("Serial Number");
		$array_values['DATE']=gm("Date");
		$array_values['CUSTOMER']=gm("Company Name");
		$array_values['DISCOUNT']=gm("Discount Percent");
		$array_values['DISCOUNT_VALUE']=gm("Discount Value");
		$array_values['PAYMENTS']=gm("Payments");
		$array_values['AMOUNT_DUE']=gm("Amount due");
		$array_values['WEB_LINK']=gm("Web Link");
		$array_values['WEB_LINK_2']=gm("Short Weblink");
		/*$array_values['DUE_DATE']=gm("Due Date");*/
		$array_values['DELIVERY_DATE']=gm("Delivery Date");
		$array_values['INVOICE_TYPE']=gm("Invoice type");
		$array_values['SIGNATURE']=gm("Signature");
		$array_values['LOGO']=gm("Logo");
		$array_values['SALUTATION']=gm("Salutation");
		$array_values['CONTACT_FIRST_NAME']=gm("Contact first name");
		$array_values['CONTACT_LAST_NAME']=gm("Contact Last Name");
		$array_values['YOUR_REFERENCE']=gm("Your Reference");
		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id_value'     => $key,
				'name'	   => $value,
			));
		}


		$data['detail_id']=$in['detail_id'];
		if($data['detail_id']!='0'){
			$detail_id='[!'.$in['detail_id'].'!]';
		}

		$data['message_labels'] = array(
			'detail'    				=> $detail,	
			'detail_id'					=> $in['detail_id'],
			'selected_detail'			=> $detail_id
		);


$is_normal = true;
if($code=='nl'){
	$code='du';
}
$in['lang_code'] = $code;
$name = 'ordmess';
$text = '-----------------------------------
Order Summary
-----------------------------------
Order ID: [!SERIAL_NUMBER!]
Date: [!ORDER_DATE!]
Customer: [!CUSTOMER!]
Discount ([!DISCOUNT!]):  [!DISCOUNT_VALUE!]
Amount Due:  [!AMOUNT_DUE!]

The detailed order is attached as a PDF.

Thank you!
-----------------------------------';
$subject = 'Order #  [!SERIAL_NUMBER!] from [!ACCOUNT_COMPANY!] ';
$sys_message_invmess = $db->query("SELECT * FROM sys_message WHERE name='".$name."' AND lang_code='".$code."' ");
		if(!$sys_message_invmess->move_next()){
			$insert = $db->query("INSERT INTO sys_message SET text='".$text."', subject='".$subject."', name='".$name."', lang_code='".$code."' ");
			$data['message']= array(
				'subject'        			=> $subject,
				'text'           			=> $text,
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> '',
				'use_html'					=> false,
				'translate_cls'				=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'order-settings-order-default_message',
				'xget'						=> 'emailMessage',
				'name'						=> $name,
				'lang_code'					=> $code,
				'detail'    				=> $detail,	
				'detail_id'					=> $in['detail_id'],
				'selected_detail'			=> $detail_id,
				'message_po'				=>  array(),
			);
		}else{
			$data['message']= array(
				'subject'        			=> $in['skip_action'] ? $sys_message_invmess->f('subject') : $sys_message_invmess->f('subject'),
				'text'           			=> $sys_message_invmess->f('text'),
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> $sys_message_invmess->f('use_html') == 1 ? 'CHECKED' : '',
				'use_html'					=> $sys_message_invmess->f('use_html') == 1 ? true : false,
				'html_content'				=> $sys_message_invmess->f('html_content'),
				'translate_cls' 			=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'order-settings-order-default_message',
				'xget'						=> 'emailMessage',
				'name'						=> $name,
				'lang_code'					=> $code,
				'detail'    				=> $detail,	
				'detail_id'					=> $in['detail_id'],
				'selected_detail'			=> $detail_id,
				'message_po'				=>  array(),
			);
		}
$is_normal_po = true;
$in['lang_code_po'] = $code;
$name_po = 'pordmess';
$text_po = '-----------------------------------
Order Summary
-----------------------------------
Order ID: [!SERIAL_NUMBER!]
Date: [!ORDER_DATE!]
Customer: [!CUSTOMER!]
Discount ([!DISCOUNT!]):  [!DISCOUNT_VALUE!]
Amount Due:  [!AMOUNT_DUE!]

The detailed order is attached as a PDF.

Thank you!
-----------------------------------';
	$subject_po = 'Order #  [!SERIAL_NUMBER!] from [!ACCOUNT_COMPANY!] ';
$sys_message_invmess_po = $db->query("SELECT * FROM sys_message WHERE name='".$name_po."' AND lang_code='".$code."' ");
		if(!$sys_message_invmess_po->move_next()){
			$insert_po = $db->query("INSERT INTO sys_message SET text='".$text_po."', subject='".$subject_po."', name='".$name_po."', lang_code='".$code."' ");
			array_push($data['message']['message_po'] = array(
				'subject_po'        			=> $subject_po,
				'text_po'           			=> $text_po,
				'language_dd_po' 				=> build_language_dd_new($in['languages']),
				'translate_cls_po'				=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages_po'					=> $in['languages'],
				'do'							=> 'order-settings-order-default_message',
				'xget'							=> 'emailMessage',
				'name_po'						=> $name_po,
				'lang_code'						=> $code,
				'detail'    					=> $detail,	
				'detail_id'						=> $in['detail_id'],
				'selected_detail'				=> $detail_id
			));
		}else{
			array_push($data['message']['message_po'] = array(
				'subject_po'        			=> $in['skip_action'] ? $sys_message_invmess_po->f('subject') : $sys_message_invmess_po->f('subject'),
				'text_po'           			=> $sys_message_invmess_po->f('text'),
				'language_dd_po' 				=> build_language_dd_new($in['languages']),
				'html_content_po'				=> $sys_message_invmess_po->f('html_content'),
				'translate_cls_po' 				=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages_po'						=> $in['languages'],
				'do'							=> 'order-settings-order-default_message',
				'xget'							=> 'emailMessage',
				'name_po'							=> $name_po,
				'lang_code_po'						=> $code,
				'detail'    					=> $detail,	
				'detail_id'						=> $in['detail_id'],
				'selected_detail'				=> $detail_id
			));
		}


		return json_out($data, $showin,$exit);
	}

		function get_emailMessageDelivery($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array('message'=>array(),'message_labels'=>array(),'message_po'=>array());

		if(!$in['languages']){
			$lng = $db->query("SELECT code,lang_id FROM pim_lang WHERE active='1' AND sort_order != '' ORDER BY sort_order limit 1");
			$code=$lng->f('code');
			$in['languages']=$lng->f('lang_id');
		}else{
			if($in['languages']>=1000) {
				$code = $db->field("SELECT code FROM pim_custom_lang WHERE active='1' AND lang_id = '".$in['languages']."' ");
			} else {
				$code = $db->field("SELECT code FROM pim_lang WHERE active='1' AND lang_id='".$in['languages']."' ");
			}
		}

		$array_values=array();
		$array_values['ACCOUNT_COMPANY']=gm("Account Name");
		$array_values['SERIAL_NUMBER']=gm("Serial Number");
		$array_values['DATE']=gm("Date");
		$array_values['CUSTOMER']=gm("Company Name");
		$array_values['DISCOUNT']=gm("Discount Percent");
		$array_values['DISCOUNT_VALUE']=gm("Discount Value");
		$array_values['PAYMENTS']=gm("Payments");
		$array_values['AMOUNT_DUE']=gm("Amount due");
		$array_values['WEB_LINK']=gm("Web Link");
		$array_values['WEB_LINK_2']=gm("Short Weblink");
		/*$array_values['DUE_DATE']=gm("Due Date");*/
		$array_values['DELIVERY_DATE']=gm("Delivery Date");
		$array_values['INVOICE_TYPE']=gm("Invoice type");
		$array_values['SIGNATURE']=gm("Signature");
		$array_values['LOGO']=gm("Logo");
		$array_values['SALUTATION']=gm("Salutation");
		$array_values['CONTACT_FIRST_NAME']=gm("Contact first name");
		$array_values['CONTACT_LAST_NAME']=gm("Contact Last Name");
		$array_values['YOUR_REFERENCE']=gm("Your Reference");
		$detail=array();
		if(!$in['detail_id']){
			$in['detail_id']='0';
		}

		foreach ($array_values as $key => $value) {
			array_push($detail, array(
				'id_value'     => $key,
				'name'	   => $value,
			));
		}


		$data['detail_id']=$in['detail_id'];
		if($data['detail_id']!='0'){
			$detail_id='[!'.$in['detail_id'].'!]';
		}

		$data['message_labels'] = array(
			'detail'    				=> $detail,	
			'detail_id'					=> $in['detail_id'],
			'selected_detail'			=> $detail_id
		);


$is_normal = true;
if($code=='nl'){
	$code='du';
}
$in['lang_code'] = $code;
$name = 'orddelmess';
$text = '-----------------------------------
Delivery Summary
-----------------------------------
Order ID: [!SERIAL_NUMBER!]
Date: [!ORDER_DATE!]
Customer: [!CUSTOMER!]
Discount ([!DISCOUNT!]):  [!DISCOUNT_VALUE!]
Amount Due:  [!AMOUNT_DUE!]

The detailed order is attached as a PDF.

Thank you!
-----------------------------------';
$subject = 'Delivery Note for Order #  [!SERIAL_NUMBER!] from [!ACCOUNT_COMPANY!] ';
$sys_message_invmess = $db->query("SELECT * FROM sys_message WHERE name='".$name."' AND lang_code='".$code."' ");
		if(!$sys_message_invmess->move_next()){
			$insert = $db->query("INSERT INTO sys_message SET text='".$text."', subject='".$subject."', name='".$name."', lang_code='".$code."' ");
			$data['message']= array(
				'subject'        			=> $subject,
				'text'           			=> $text,
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> '',
				'use_html'					=> false,
				'translate_cls'				=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'order-settings-order-default_message',
				'xget'						=> 'emailMessageDelivery',
				'name'						=> $name,
				'lang_code'					=> $code,
				'detail'    				=> $detail,	
				'detail_id'					=> $in['detail_id'],
				'selected_detail'			=> $detail_id,
				'message_po'				=>  array(),
			);
		}else{
			$data['message']= array(
				'subject'        			=> $in['skip_action'] ? $sys_message_invmess->f('subject') : $sys_message_invmess->f('subject'),
				'text'           			=> $sys_message_invmess->f('text'),
				'language_dd' 				=> build_language_dd_new($in['languages']),
				'active_tabb'				=> $in['tabb'],
				'CHECKED'					=> $sys_message_invmess->f('use_html') == 1 ? 'CHECKED' : '',
				'use_html'					=> $sys_message_invmess->f('use_html') == 1 ? true : false,
				'html_content'				=> $sys_message_invmess->f('html_content'),
				'translate_cls' 			=> 'form-language-'.$code,
				'translate_special'			=> 'form-'.$code,
				'languages'					=> $in['languages'],
				'do'						=> 'order-settings-order-default_message',
				'xget'						=> 'emailMessageDelivery',
				'name'						=> $name,
				'lang_code'					=> $code,
				'detail'    				=> $detail,	
				'detail_id'					=> $in['detail_id'],
				'selected_detail'			=> $detail_id,
				'message_po'				=>  array(),
			);
		}

		return json_out($data, $showin,$exit);
	}

	function get_defaultEmail($in,$showin=true,$exit=true){
			$db = new sqldb();
			$email = $db->query("SELECT * FROM default_data WHERE type='order_email' ");
			$po_email = $db->query("SELECT * FROM default_data WHERE type = 'p_order_email' ");
			$default_email_bcc = $db->query("SELECT * FROM default_data WHERE type = 'bcc_order_email' ");
			$default_email_bcc_delivery = $db->query("SELECT * FROM default_data WHERE type = 'bcc_delivery_email' ");
			$mail_type1 = $db->field("SELECT value FROM default_data WHERE  type = 'order_email_type' ");

			$data['email_default']= array(
				'default_name'				=> $email->f('default_name'),
				'email_value'				=> $email->f('value'),
				'po_default_name'			=> $po_email->f('default_name'),
				'po_email_value'			=> $po_email->f('value'),
				'bcc_email'					=> $default_email_bcc->gf('value'),
				'bcc_delivery_email'		=> $default_email_bcc_delivery->gf('value'),
				'mail_type_1'				=> $mail_type1,
				'do'						=> 'order-settings-order-update_default_email',
				'xget'						=> 'defaultEmail',
			);
			return json_out($data, $showin,$exit);
	}
	function get_articlefield($in,$showin=true,$exit=true){
			$db = new sqldb();
			$data = array('articlefields'=>array());
			$not_copy_article_info = $db->field("SELECT value FROM settings WHERE constant_name='NOT_COPY_ARTICLE_ORD'");
			$text = $db->field("SELECT long_value FROM settings WHERE constant_name='ORDER_FIELD_LABEL'");
				$data['articlefields'] = array(
					'not_copy_article_info'  				=> $not_copy_article_info == 1 ? true : false,
					'text'  								=> $text,
					'xget'									=> 'articlefield',
					'do'									=> 'order-settings-order-label',
					);

			return json_out($data, $showin,$exit);
	}
	function get_attachments($in,$showin=true,$exit=true){
			$db = new sqldb();
			//1-quotes,2-invoices,3-orders
			$data = array('atachment'=>array(),'checkbox_allow'=>array());
			$f = $db->query("SELECT * FROM attached_files WHERE type='3' ");
			$allow = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ORDER_DROP_BOX' ") == '1' ? true : false;
			$data['checkbox_allow'] = array(
				'allow'									=> $allow
			);
			while ($f->next()) {
				array_push($data['atachment'], array(
					'file'		=> $f->f('name'),
					'checked'	=> $f->f('default') == 1 ? true : false,
					'file_id'	=> $f->f('file_id'),
					'path'		=> INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/orders/'.$f->f('name'),
					));
			}

			return json_out($data, $showin,$exit);
	}
	function get_delivery($in,$showin=true,$exit=true){
			$db = new sqldb();
			//1-quotes,2-invoices,3-orders
			$data = array('delivery'=>array());
			$delivery = $db->field("SELECT value FROM settings WHERE constant_name='ORDER_DELIVERY_STEPS'");
				$data['delivery'] = array(
					'delivery_type'   		=> $delivery,
					'xget'					=> 'delivery',
					'do'					=> 'order-settings-order-timesheetup',
					);

			return json_out($data, $showin,$exit);
	}

	function get_mobile($in,$showin=true,$exit=true){
			$db = new sqldb();
			global $database_config;
			$db_config = array(
				'hostname' => $database_config['mysql']['hostname'],
				'username' => $database_config['mysql']['username'],
				'password' => $database_config['mysql']['password'],
				'database' => $database_config['user_db'],
			);

			$dbu = new sqldb($db_config);
			//1-quotes,2-invoices,3-orders
			$data = array('mobile'=>array());
			
			$act = $db->field("SELECT value FROM settings WHERE constant_name='ACTIVATE_NOTIFICATION_MOBILE_ORDER' ");
			$user = $db->field("SELECT value FROM settings WHERE constant_name='USER_NOTIFICATION_MOBILE_ORDER' ");
			$users = $dbu->query("SELECT user_id,first_name FROM users ");
			$u = $dbu->query("SELECT first_name,last_name FROM users WHERE user_id='".$user."'");
			$data['mobile'] = array(
					'active'   				=> $act == 1 ? true : false,
					'user_exist'			=> $user,
					'username'				=> get_users($in),
					'user_name'				=> $u ? utf8_encode($u->f("first_name")).' '.utf8_encode($u->f('last_name')) : '',
					'xget'					=> 'chargevat',
			);

			return json_out($data, $showin,$exit);
	}


 
	function get_listSettings($in,$showin=true,$exit=true){
			$db = new sqldb();
			$data = array('listSettings'=>array()); 
			$act = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_MY_ORDERS_ONLY' ");
			$data['listSettings'] = array(
					'list'  								=> $act == 1 ? true : false,
					'xget'									=> 'listSettings',
					);
			return json_out($data, $showin,$exit);
	}
	$result = array(
	/*	'logos'				=> get_logos($in,true,false),*/
		'PDFlayout'			=> get_PDFlayout($in,true,false),
		'Convention' 		=> get_Convention($in,true,false),
	);
	

	json_out($result);
	function get_users ($in){
		$q = strtolower(trim($in["term"]));
		if($q){
			$filter = " first_name LIKE '%".$q."%' ";
		}
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_user = new sqldb($database_users);

		$result = array();
		$db= new sqldb();
		$func = $db_user->query("SELECT users.first_name,users.user_id FROM users
					WHERE users.active=1 $filter ORDER BY users.first_name ")->getAll();
		foreach ($func as $key => $value) {
			$result[] = array('id'=>$value['user_id'], 'value'=>$value['first_name']);
		}
		return $result;
	}
	function get_orderTerm($in,$showin=true,$exit=true){
		$db = new sqldb();
		$apply_discount = $db->field("SELECT value FROM settings WHERE constant_name = 'ORDER_APPLY_DISCOUNT' ");
		$delivery = $db->field("SELECT value FROM settings WHERE constant_name='ORDER_DELIVERY_STEPS'");
		$delivery_term = $db->field("SELECT value FROM settings WHERE constant_name='ORDER_DELIVERY_TERM'");
		$edit_confirmed_orders = $db->field("SELECT value FROM settings WHERE constant_name = 'EDIT_CONFIRMED_ORDERS' ");
		$data = array('orderTerm'=>array(
			'apply_discount' 			=> $apply_discount==1 ? true : false,
			'edit_confirmed_orders'		=> $edit_confirmed_orders==1 ? true : false,
			'do'						=> 'order-settings-order-saveterm',
			'xget'						=> 'orderTerm',
			'delivery'					=> array(
										'delivery_type'   		=> $delivery,
										'delivery_term'   		=> $delivery_term,
										'xget'					=> 'orderTerm',
										'do'					=> 'order-settings-order-timesheetup',
										)
		));

		//from get_delivery
		//1-quotes,2-invoices,3-orders
	
		
		/*$data['delivery'] = array(
			'delivery_type'   		=> $delivery,
			'xget'					=> 'orderTerm',
			'do'					=> 'order-settings-order-timesheetup',
			);*/
		
		return json_out($data, $showin,$exit);
	}
?>
