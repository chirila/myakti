<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$pdf_type =array(4,5);
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetFont('dejavusans', '', 10, '', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Order');
$pdf->SetSubject('Order');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);

if(!$_SESSION['add_to_pdf_o']){
	header("Location:index.php?do=order-orders");
}
foreach ($_SESSION['add_to_pdf_o'] as $key => $value) {
	$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
	$pdf->setHtmlVSpace($tagvs);
	unset($in['logo']);
	$in['order_id']= $key;
	$in['type'] = ACCOUNT_ORDER_PDF_FORMAT;
	$pdf_layout = $db->field("SELECT pdf_layout FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
	if($pdf_layout){
		$in['type'] = $pdf_layout;
	}
	$pdf_logo = $db->field("SELECT pdf_logo FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
	if($pdf_logo){
		$in['logo'] = $pdf_logo;
	}
	$lid = $db->field("SELECT email_language FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
	if($lid){
		$in['lid'] = $lid;
	}
	if($in['type'] == 4){
		$pdf->SetMargins(10, 5, 10);
		$pdf->SetFooterMargin(0);
		$pdf->SetAutoPageBreak(TRUE, 50);
		$pdf->setPrintFooter(false);
		$hide_all = 2;
	}

	$pdf->startPageGroup();

	$pdf->AddPage();

	$html = include('order_print_body.php');
	// print($html);exit();
	if(isset($in['print'])){
		print_r($html);exit();
	}
	$pdf->writeHTML($html, true, false, true, false, '');

	$pdf->SetY($height);
	if($in['type'] == 4){
		$hide_all = 1;
		$pdf->SetAutoPageBreak(TRUE, 0);

		$htmls = 2;

		$pdf->writeHTML($htmls, true, false, true, false, '');
	}
}
$pdf->lastPage();
doQueryLog();
$pdf->Output('orders.pdf');
exit();
