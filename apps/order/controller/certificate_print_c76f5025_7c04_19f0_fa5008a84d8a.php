<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$db5 = new sqldb();
$custom_lng = $db5->field("SELECT lang_id FROM pim_custom_lang WHERE lang_id='".$in['lid']."' ");


ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));


$pdf_type =array(4,5);

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetFont('helvetica', '', 10, '', false);
if($custom_lng){
	#we can use both font types but dejavusans looks better
	$pdf->SetFont('dejavusans', '', 9, '', false);
	// $pdf->SetFont('freeserif', '', 10, '', false);
} 
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Certificate');
$pdf->SetSubject('Certificate');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(20, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, 30);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);

if(in_array($in['type'], $pdf_type)){
	$pdf->SetMargins(10, 5, 10);
	$pdf->SetFooterMargin(0);
	$pdf->SetAutoPageBreak(TRUE, 50); 
	$pdf->setPrintFooter(false);
	$hide_all = 2;
}
$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);

$pdf->AddPage();

$html = include('certificate_print_body_c76f5025_7c04_19f0_fa5008a84d8a.php');
// file_put_contents('print4v2', $html);
// print($html);exit();
if(isset($in['print'])){
	print_r($html);exit();
}
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->SetY($height);
if(in_array($in['type'], $pdf_type)){
	$hide_all = 1;
	$pdf->SetAutoPageBreak(TRUE, 0);

	$htmls = include('certificate_print_body_c76f5025_7c04_19f0_fa5008a84d8a.php');

	$pdf->writeHTML($htmls, true, false, true, false, '');
}
$pdf->lastPage();

if($in['do']=='order-certificate_print'){
   $pdf->Output("c".$serial_number.'.pdf','I');
}if($in['base64']){
	$date_reversed = date('Ymd');
	$str = $pdf->Output('Certificate '.$date_reversed.'.pdf','E');
}else{
	$date_reversed = date('Ymd');
   $pdf->Output(__DIR__.'/../../../upload/'.DATABASE_NAME.'/Certificate_'.$in['delivery_id'].'_'.$date_reversed.'.pdf', 'F');
}