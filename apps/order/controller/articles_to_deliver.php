<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit','512M');

if($in['reset_list']){
    if(isset($_SESSION['tmp_add_to_pdf_otd'])){
        unset($_SESSION['tmp_add_to_pdf_otd']);
    }
    if(isset($_SESSION['add_to_pdf_otd'])){
        unset($_SESSION['add_to_pdf_otd']);
    }
}

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}

$db = new sqldb();
$db2 = new sqldb();

$general_orders=array();
$general_orders_rdy=array();

$search = '';

if($in['search'] && !empty($in['search'])){
    $quotation_mark = false;
    $pos = strpos($in['search'], '"');
    if($pos){
        $quotation_mark = true;
    }
    if($quotation_mark){
        $in['search'] = str_replace('\\"','',$in['search']);
        $search .= " AND ( pim_articles.item_code LIKE '".rtrim(ltrim($in['search']))."%'  OR pim_articles.internal_name LIKE '".rtrim(ltrim($in['search']))."%' OR pim_orders.customer_name LIKE '". $in['search'] ."%')";
    }else{
        $search_words = explode(" ", $in['search']);
        $query_string ='';
        $query_string1 ='';
        $query_string2 ='';
        $query_string3 ='';
        $all_tcl=1;
        $tcl=0;
        $len = count($search_words);

        for ($s = 0; $s < $len; $s++) {
            if ($search_words[$s]) {
                $query_string8 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.internal_name ";
                $query_string9 .= "LIKE '%" . $search_words[$s] . "%' OR pim_articles.item_code ";
                $query_string10 .= "LIKE '%" . $search_words[$s] . "%' OR pim_orders.customer_name ";
                if(strlen($search_words[$s])>3){
                    $all_tcl=0;
                }else{
                    $tcl=1;
                }
                $query_string4 .= "+".$search_words[$s]."* ";
                $query_string5 .= "+".$search_words[$s]." ";
                if($s==0){
                    $query_string6 .= "+".$search_words[$s]." ";
                }else{
                    $query_string6 .= "+".$search_words[$s]."* ";
                }

            }
        }
        $query_string7 =SUBSTR($query_string4, 0, STRLEN($query_string4) - 2);

        if($all_tcl || $tcl ){
            $size = $len - 1;

            if($size){
                $perm = range(0, $size);
                $j = 0;

                do {
                    foreach ($perm as $i) { $perms[$j][] = $search_words[$i]; }
                } while ($perm = pc_next_permutation($perm, $size) and ++$j);

                foreach ($perms as $p) {
                    $query_string1 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.item_code ";
                    $query_string2 .= "LIKE '%" . join(' ', $p) . "%' OR pim_articles.internal_name ";
                    $query_string3 .= "LIKE '%" . join(' ', $p) . "%' OR pim_orders.customer_name ";
                }

                $query_string1 = SUBSTR($query_string1, 0, STRLEN($query_string1) - 27);
                $query_string2 = SUBSTR($query_string2, 0, STRLEN($query_string2) - 31);
                $query_string3 = SUBSTR($query_string2, 0, STRLEN($query_string2) - 31);
            }else{
                $query_string1 .= "LIKE '%" . $search_words[0] . "%' ";
                $query_string2 .= "LIKE '%" . $search_words[0] . "%' ";
                $query_string3 .= "LIKE '%" . $search_words[0] . "%' ";
            }

            $query_string8 =SUBSTR($query_string8, 0, STRLEN($query_string8) - 31);
            $query_string9 =SUBSTR($query_string9, 0, STRLEN($query_string9) - 27);
            $query_string10 =SUBSTR($query_string10, 0, STRLEN($query_string10) - 29);
        }

        $query_string = SUBSTR($query_string, 0, STRLEN($query_string) - 31);

        if ($len>1){
            if($all_tcl || $tcl){
                if($tcl){
                    $search .= " AND ( pim_articles.item_code ".$query_string9."  OR pim_articles.internal_name ".$query_string8." OR pim_orders.customer_name ". $query_string10 .") ";
                }else{
                    $search .= " AND ( pim_articles.item_code ".$query_string1."  OR pim_articles.internal_name ".$query_string2." OR pim_orders.customer_name ". $query_string3 .") ";
                }
            }else{
                $search .= " AND (match(pim_articles.internal_name) against ('".$query_string4."' in boolean mode)
	                      OR match(pim_articles.internal_name) against ('".$query_string5."' in boolean mode)
	                      OR match(pim_articles.internal_name) against ('".$query_string6."' in boolean mode)   
	                      OR match(pim_articles.internal_name) against ('".$query_string7."' in boolean mode)    
	                      OR match(pim_articles.item_code) against ('".$query_string4."' in boolean mode)
	                      OR match(pim_orders.customer_name) against ('".$query_string4."' in boolean mode)
	                      OR match(pim_orders.customer_name) against ('".$query_string5."' in boolean mode)
	                      OR match(pim_orders.customer_name) against ('".$query_string6."' in boolean mode)
	                      OR match(pim_orders.customer_name) against ('".$query_string7."' in boolean mode) )";
            }

        }else{
            $search .= " AND (pim_articles.internal_name LIKE '%".rtrim(ltrim($in['search']))."%'
	                        OR pim_articles.item_code LIKE '%".rtrim(ltrim($in['search']))."%'
	                         OR pim_orders.customer_name LIKE '". $in['search'] ."%')";
        }
    }
}

$qty_ord = $db->query("SELECT pim_order_articles.order_articles_id, pim_order_articles.article_id, pim_articles.packing as article_packing, pim_articles.sale_unit as article_sale_unit, SUM( pim_order_articles.quantity ) as total_order , COUNT(distinct pim_orders_delivery.delivery_id) as nr_del ,SUM( pim_orders_delivery.quantity) as total_deliver, SUM(CASE WHEN pim_order_deliveries.delivery_done = '1' THEN pim_orders_delivery.quantity ELSE 0 END) as total_deliver2,  pim_order_articles.sale_unit, pim_order_articles.packing
    FROM pim_order_articles
    LEFT JOIN pim_articles ON pim_order_articles.article_id=pim_articles.article_id
    LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
    LEFT JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
    LEFT JOIN pim_order_deliveries ON pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id
    WHERE pim_order_articles.article_id!='0' AND pim_articles.is_service='0' AND pim_orders.sent =1 AND pim_orders.active =1 ". $search ." GROUP BY pim_order_articles.order_articles_id,pim_orders.order_id");

    $total_deliver=0;
    while($qty_ord->move_next()) {
        $nr_del = $qty_ord->f('nr_del');
        if($nr_del==0) {
          $nr_del = 1;
        }

        $total_del=0;

        $stock_packing_ord = $qty_ord->f("packing");
        if(!$stock_packing_ord){
          $stock_packing_ord = $diff->f("article_packing");
        }
        if(!ALLOW_ARTICLE_PACKING || !$stock_packing_ord){
           $stock_packing_ord = 1;
         }
         
        $stock_sale_unit_ord = $qty_ord->f("sale_unit");
        if(!$stock_sale_unit_ord){
          $stock_sale_unit_ord = $qty_ord->f("article_sale_unit");
        }
        if(!ALLOW_ARTICLE_SALE_UNIT || !$stock_sale_unit_ord){
           $stock_sale_unit_ord = 1;
        } 

        if(ORDER_DELIVERY_STEPS == '2'){
            $total_del = $qty_ord->f('total_deliver2');
         }else{
            $total_del = $qty_ord->f('total_deliver');
        }

        if($qty_ord->f('total_order')/$nr_del < $total_del){

            $total_deliver = ( $qty_ord->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del;
        }else{
            $total_deliver = $total_del*$stock_packing_ord/$stock_sale_unit_ord;
        }
 
        if(!array_key_exists($qty_ord->f('order_articles_id'), $general_orders)){
        	$general_orders[$qty_ord->f('order_articles_id')]=array();
        }
        $general_orders[$qty_ord->f('order_articles_id')]['article_id']=$qty_ord->f('article_id');
        $general_orders[$qty_ord->f('order_articles_id')]['quantity']=( ( $qty_ord->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del - $total_deliver );
    }

    $diff = $db->query("SELECT pim_order_articles.order_articles_id, pim_order_articles.article_id, pim_articles.packing as article_packing, pim_articles.sale_unit as article_sale_unit, SUM( pim_order_articles.quantity ) as total_order , COUNT(distinct pim_orders_delivery.delivery_id) as nr_del, SUM( pim_orders_delivery.quantity) as total_deliver, SUM(CASE WHEN pim_order_deliveries.delivery_done = '1' THEN pim_orders_delivery.quantity ELSE 0 END) as total_deliver2 , pim_order_articles.sale_unit, pim_order_articles.packing
        FROM pim_order_articles
        LEFT JOIN pim_articles ON pim_order_articles.article_id=pim_articles.article_id
        LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
        LEFT JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
        LEFT JOIN pim_order_deliveries ON pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id
        WHERE pim_order_articles.article_id!='0' AND pim_articles.is_service='0' AND pim_orders.sent =1 AND pim_orders.rdy_invoice =  '1' AND pim_orders.active =1 GROUP BY pim_order_articles.order_articles_id,pim_orders.order_id");

    $total_deliver=0;
    while($diff->move_next()) {

      	$nr_del = $diff->f('nr_del');
      	if($nr_del==0) {
          $nr_del = 1;
        }

        $stock_packing_ord = $diff->f("packing");
        if(!$stock_packing_ord){
          $stock_packing_ord = $diff->f("article_packing");
        }
        if(!ALLOW_ARTICLE_PACKING || !$stock_packing_ord){
           $stock_packing_ord = 1;
         }
         
        $stock_sale_unit_ord = $diff->f("sale_unit");
        if(!$stock_sale_unit_ord){
          $stock_sale_unit_ord = $diff->f("article_sale_unit");
        }
        if(!ALLOW_ARTICLE_SALE_UNIT || !$stock_sale_unit_ord){
           $stock_sale_unit_ord = 1;
        } 

        if(ORDER_DELIVERY_STEPS == '2'){
            $total_del = $diff->f('total_deliver2');
        }else{
            $total_del = $diff->f('total_deliver');
        }
        if($diff->f('total_order')/$nr_del < $total_del){
            $total_deliver =($diff->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del;
        }else{
            $total_deliver = $total_del*$stock_packing_ord/$stock_sale_unit_ord ;
        }

     	if(!array_key_exists($diff->f('order_articles_id'), $general_orders_rdy)){
        	$general_orders_rdy[$diff->f('order_articles_id')]=array();
        }
        $general_orders_rdy[$diff->f('order_articles_id')]['article_id']=$diff->f('article_id');
        $general_orders_rdy[$diff->f('order_articles_id')]['quantity']=(($diff->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del - $total_deliver);
    }

    $articles_ar=array();

    foreach($general_orders as $order_articles_id=>$data){
    	if(array_key_exists($order_articles_id, $general_orders_rdy)){
    		if($data['quantity'] - $general_orders_rdy[$order_articles_id]['quantity'] > 0){
    			if(!array_key_exists($data['article_id'], $articles_ar)){
    				$articles_ar[$data['article_id']]=0;
    			}
    			$articles_ar[$data['article_id']]+=($data['quantity'] - $general_orders_rdy[$order_articles_id]['quantity']);
    		}
    	}else{
    		if($data['quantity'] > 0){
    			if(!array_key_exists($data['article_id'], $articles_ar)){
    				$articles_ar[$data['article_id']]=0;
    			}
    			$articles_ar[$data['article_id']]+=$data['quantity'];
    		}
    	}
    }

    unset($general_orders);
    unset($general_orders_rdy);

    $order_by_array = array('item_code','internal_name','stock','deliver_nr');

    $l_r =ROW_PER_PAGE;
    $result = array('query'=>array(),'max_rows'=>0,'lr'=>$l_r);
	
	$order_by = " ORDER BY item_code  ";

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}
	$filter=" 1=1 ";



	if(isset($in['view']) && $in['view']=='1'){
		$filter .=" AND pim_articles.stock>'0' ";
	}
	if(isset($in['view']) && $in['view']=='2'){
		$filter .=" AND pim_articles.stock<='0' ";
	}

	if($in['order_by']){
	    if(in_array($in['order_by'], $order_by_array)){
		    $order = " ASC ";
		    if($in['desc'] == '1' || $in['desc']=='true'){
		        $order = " DESC ";
		    }
		    if($in['order_by']=='deliver_nr' ){
		    	//
		    }else{
		       $order_by =" ORDER BY ".$in['order_by']." ".$order;
		    }
	    }
	}

	if($in['order_by']=='deliver_nr'){
	    $filter_limit =' ';
	    $order_by ='';
	}else{
	     $filter_limit =" LIMIT ".$offset*$l_r.",".$l_r."  ";
	}
	if(!empty($articles_ar)){
		$articles_str="";
		foreach($articles_ar as $article_id=>$article_deliver_nr){
			$articles_str.="'".$article_id."',";
		}
		$articles_str=rtrim($articles_str,",");

        $session_articles = $db2->query("SELECT * FROM pim_articles WHERE ".$filter." AND article_id IN (".$articles_str.") ".$order_by.$filter_limit." ");

        $nav = array();
        if(!$_SESSION['tmp_add_to_pdf_otd'] || ($_SESSION['tmp_add_to_pdf_otd'] && empty($_SESSION['tmp_add_to_pdf_otd']))){
            while($session_articles->next()){
                $_SESSION['tmp_add_to_pdf_otd'][$session_articles->f('article_id')]=1;
                array_push($nav, (object)['article_id'=> $session_articles->f('article_id') ]);
            }
        }else{
            while($session_articles->next()){
                array_push($nav, (object)['article_id'=> $session_articles->f('article_id') ]);
            }
        }

        $all_pages_selected=false;
        $minimum_selected=false;
        if($_SESSION['add_to_pdf_otd']){
            if($session_articles->records_count()>0 && count($_SESSION['add_to_pdf_otd']) == $session_articles){
                $all_pages_selected=true;
            }else if(count($_SESSION['add_to_pdf_otd'])){
                $minimum_selected=true;
            }
        }


        $result['max_rows']=$db->field("SELECT COUNT(article_id) FROM pim_articles WHERE ".$filter." AND article_id IN (".$articles_str.") ");

        $result = array('query'=>array(),'max_rows'=>$result['max_rows'],'all_pages_selected'=> $in['exported'] ? false : $all_pages_selected,'minimum_selected'=> $in['exported'] ? false : $minimum_selected, 'nav' => $nav);

        $articles=$db->query("SELECT * FROM pim_articles WHERE ".$filter." AND article_id IN (".$articles_str.") ".$order_by.$filter_limit." ");

        while($articles->next()){

			if($articles->f('stock') <= 0){
           		$stock_status='text-danger';        
		    }elseif($articles->f('stock') > 0  && $articles->f('stock') <= $articles->f('article_threshold_value') ){
		        $stock_status='text-warning';
		    }else{
		        $stock_status='text-success';
		    }

			$item=array(
				'article_id'	=> $articles->f('article_id'),
				'code'			=> $articles->f('item_code'),
                'check_add_to_product'	=> $in['exported'] ? false : ($_SESSION['add_to_pdf_otd'][$articles->f('article_id')] == 1 ? true : false),
                'id'              	  	=> $articles->f('article_id'),
                'internal_name'	=> htmlspecialchars_decode($articles->f('internal_name')),
				'stock'			=> remove_zero_decimals_dn(display_number($articles->f('stock'))),
				'stock_status'	=> $stock_status,
				'deliver_nr'	=> display_number($articles_ar[$articles->f('article_id')]),
				'deliver_nr_ord'	=> $articles_ar[$articles->f('article_id')]
			);
			array_push($result['query'], $item);
		}

		if($in['order_by']=='deliver_nr'){
		    if($order ==' ASC '){
		       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_ASC);    
		    }else{
		       $exo = array_sort($result['query'], $in['order_by'].'_ord', SORT_DESC);
		    }
		    $exo = array_slice( $exo, $offset*$l_r, $l_r);
		    $result['query']=array();
	       foreach ($exo as $key => $value) {
	           array_push($result['query'], $value);
	       }
		}
	}
    $result['view_dd'] = array( array('id'=>'1', 'value'=>gm('In stock articles')), array('id'=>'2', 'value'=>gm('Not in stock articles')));
    $result['lr'] = $l_r;
    json_out($result);

	function get_orders_list($in,$showin=true,$exit=true){
		$response = array();
		$db = new sqldb();
		$v=new validation($in);
		$v->field("article_ids","Ids","required");
		if(!$v->run()){
			return json_out($response, $showin,$exit);
		}
		if(!is_array($in['article_ids']) || (is_array($in['article_ids']) && empty($in['article_ids']))){
			return json_out($response, $showin,$exit);
		}
		foreach($in['article_ids'] as $article_id){
			if(!is_numeric($article_id)){
				return json_out($response, $showin,$exit);
				break;
			}
			$article_exists=$db->field("SELECT article_id FROM pim_articles WHERE article_id='".$article_id."' ");
			if(!$article_exists){
				return json_out($response, $showin,$exit);
				break;
			};
		}

		$general_orders=array();
		$general_orders_rdy=array();

		$articles_str="";
		foreach($in['article_ids'] as $article_id){
			$articles_str.="'".$article_id."',";
		}
		$articles_str=rtrim($articles_str,",");

		$qty_ord = $db->query("SELECT pim_order_articles.order_articles_id, pim_articles.article_id, pim_articles.packing as article_packing, pim_articles.sale_unit as article_sale_unit, SUM( pim_order_articles.quantity ) as total_order , COUNT(distinct pim_orders_delivery.delivery_id) as nr_del ,SUM( pim_orders_delivery.quantity) as total_deliver, SUM(CASE WHEN pim_order_deliveries.delivery_done = '1' THEN pim_orders_delivery.quantity ELSE 0 END) as total_deliver2,  pim_order_articles.sale_unit, pim_order_articles.packing,pim_orders.order_id,pim_orders.customer_name,pim_orders.serial_number,pim_orders.date,pim_orders.del_date
		    FROM pim_order_articles
		    LEFT JOIN pim_articles ON pim_order_articles.article_id=pim_articles.article_id
		    LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
		    LEFT JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
		    LEFT JOIN pim_order_deliveries ON pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id
		    WHERE pim_order_articles.article_id!='0' AND pim_articles.article_id IN (".$articles_str.") AND pim_articles.is_service='0' AND pim_orders.sent =1 AND pim_orders.active =1 GROUP BY pim_order_articles.order_articles_id,pim_orders.order_id");

	    $total_deliver=0;
	    while($qty_ord->move_next()) {
	        $nr_del = $qty_ord->f('nr_del');
	        if($nr_del==0) {
	          $nr_del = 1;
	        }

	        $total_del=0;

	        $stock_packing_ord = $qty_ord->f("packing");
	        if(!$stock_packing_ord){
	          $stock_packing_ord = $diff->f("article_packing");
	        }
	        if(!ALLOW_ARTICLE_PACKING || !$stock_packing_ord){
	           $stock_packing_ord = 1;
	         }
	         
	        $stock_sale_unit_ord = $qty_ord->f("sale_unit");
	        if(!$stock_sale_unit_ord){
	          $stock_sale_unit_ord = $qty_ord->f("article_sale_unit");
	        }
	        if(!ALLOW_ARTICLE_SALE_UNIT || !$stock_sale_unit_ord){
	           $stock_sale_unit_ord = 1;
	        } 

	        if(ORDER_DELIVERY_STEPS == '2'){
	            $total_del = $qty_ord->f('total_deliver2');
	         }else{
	            $total_del = $qty_ord->f('total_deliver');
	        }

	        if($qty_ord->f('total_order')/$nr_del < $total_del){

	            $total_deliver = ( $qty_ord->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del;
	        }else{
	            $total_deliver = $total_del*$stock_packing_ord/$stock_sale_unit_ord;
	        }

	        if(!array_key_exists($qty_ord->f('order_articles_id'), $general_orders)){
	        	$general_orders[$qty_ord->f('order_articles_id')]=array();        	
	        }
	        if(!array_key_exists($qty_ord->f('article_id'),$general_orders[$qty_ord->f('order_articles_id')])){
	        	$general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')]=array();  
	        }
	        if(!array_key_exists($qty_ord->f('order_id'),$general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')])){
	        	$general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]=array();
	        	$general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['quantity']=0;
	        	$general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['serial_number']=$qty_ord->f('serial_number');
	        	$general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['customer_name']=$qty_ord->f('customer_name');
	        	$general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['date']=$qty_ord->f('date');
	        	$general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['del_date']=$qty_ord->f('del_date');
	        }
	        $general_orders[$qty_ord->f('order_articles_id')][$qty_ord->f('article_id')][$qty_ord->f('order_id')]['quantity']+=( ( $qty_ord->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del - $total_deliver );	    
        
	    }

		$diff = $db->query("SELECT pim_order_articles.order_articles_id, pim_articles.article_id, pim_articles.packing as article_packing, pim_articles.sale_unit as article_sale_unit, SUM( pim_order_articles.quantity ) as total_order , COUNT(distinct pim_orders_delivery.delivery_id) as nr_del, SUM( pim_orders_delivery.quantity) as total_deliver, SUM(CASE WHEN pim_order_deliveries.delivery_done = '1' THEN pim_orders_delivery.quantity ELSE 0 END) as total_deliver2 , pim_order_articles.sale_unit, pim_order_articles.packing,pim_orders.order_id
		        FROM pim_order_articles
		        LEFT JOIN pim_articles ON pim_order_articles.article_id=pim_articles.article_id
		        LEFT JOIN pim_orders_delivery ON pim_order_articles.order_articles_id = pim_orders_delivery.order_articles_id
		        LEFT JOIN pim_orders ON pim_order_articles.order_id = pim_orders.order_id
		        LEFT JOIN pim_order_deliveries ON pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id
		        WHERE pim_order_articles.article_id!='0' AND pim_articles.article_id IN (".$articles_str.") AND pim_articles.is_service='0' AND pim_orders.sent =1 AND pim_orders.rdy_invoice =  '1' AND pim_orders.active =1 GROUP BY pim_order_articles.order_articles_id,pim_orders.order_id");

	    $total_deliver=0;
	    while($diff->move_next()) {

	      	$nr_del = $diff->f('nr_del');
	      	if($nr_del==0) {
	          $nr_del = 1;
	        }

	        $stock_packing_ord = $diff->f("packing");
	        if(!$stock_packing_ord){
	          $stock_packing_ord = $diff->f("article_packing");
	        }
	        if(!ALLOW_ARTICLE_PACKING || !$stock_packing_ord){
	           $stock_packing_ord = 1;
	         }
	         
	        $stock_sale_unit_ord = $diff->f("sale_unit");
	        if(!$stock_sale_unit_ord){
	          $stock_sale_unit_ord = $diff->f("article_sale_unit");
	        }
	        if(!ALLOW_ARTICLE_SALE_UNIT || !$stock_sale_unit_ord){
	           $stock_sale_unit_ord = 1;
	        } 

	        if(ORDER_DELIVERY_STEPS == '2'){
	            $total_del = $diff->f('total_deliver2');
	        }else{
	            $total_del = $diff->f('total_deliver');
	        }
	        if($diff->f('total_order')/$nr_del < $total_del){
	            $total_deliver =($diff->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del;
	        }else{
	            $total_deliver = $total_del*$stock_packing_ord/$stock_sale_unit_ord ;
	        }

	        if(!array_key_exists($diff->f('order_articles_id'), $general_orders_rdy)){
	        	$general_orders_rdy[$diff->f('order_articles_id')]=array();        	
	        }
	        if(!array_key_exists($diff->f('article_id'),$general_orders_rdy[$diff->f('order_articles_id')])){
	        	$general_orders_rdy[$diff->f('order_articles_id')][$diff->f('article_id')]=array();  
	        }
	        if(!array_key_exists($diff->f('order_id'),$general_orders_rdy[$diff->f('order_articles_id')][$diff->f('article_id')])){
	        	$general_orders_rdy[$diff->f('order_articles_id')][$diff->f('article_id')][$diff->f('order_id')]=array();
	        	$general_orders_rdy[$diff->f('order_articles_id')][$diff->f('article_id')][$diff->f('order_id')]['quantity']=0;
	        }
	        $general_orders_rdy[$diff->f('order_articles_id')][$diff->f('article_id')][$diff->f('order_id')]['quantity']+=( ( $diff->f('total_order')*$stock_packing_ord/$stock_sale_unit_ord )/$nr_del - $total_deliver );	
	    }

	    foreach($general_orders as $order_articles_id=>$data){
	    	if(array_key_exists($order_articles_id, $general_orders_rdy)){
	    		foreach($data as $article_id=>$article_data){
	    			if(!array_key_exists($article_id, $response)){
	    				$response[$article_id]=array('list'=>array());
	    			}	    			
	    			foreach($article_data as $order_id=>$order_data){
	    				if(array_key_exists($article_id,$general_orders_rdy[$order_articles_id]) && array_key_exists($order_id, $general_orders_rdy[$order_articles_id][$article_id])){
	    					if($order_data['quantity']-$general_orders_rdy[$order_articles_id][$article_id][$order_id]['quantity'] > 0){
	    						$item_order=array(
						            'id'                    => $order_id,  
						            'serial_number'         => $order_data['serial_number'],
						            'customer'              => $order_data['customer_name'],
						            'created'               => date(ACCOUNT_DATE_FORMAT,$order_data['date']),
						            'delivery_date'         => $order_data['del_date'] ? date(ACCOUNT_DATE_FORMAT,$order_data['del_date']):'',
						            'quantity'              => display_number($order_data['quantity']-$general_orders_rdy[$order_articles_id][$article_id][$order_id]['quantity'])
						        );   
						       	array_push($response[$article_id]['list'], $item_order);
	    					}
	    				}else{
	    					if($order_data['quantity'] > 0){
				    			$item_order=array(
						            'id'                    => $order_id,  
						            'serial_number'         => $order_data['serial_number'],
						            'customer'              => $order_data['customer_name'],
						            'created'               => date(ACCOUNT_DATE_FORMAT,$order_data['date']),
						            'delivery_date'         => $order_data['del_date'] ? date(ACCOUNT_DATE_FORMAT,$order_data['del_date']):'',
						            'quantity'              => display_number($order_data['quantity'])
						        );   
						       	array_push($response[$article_id]['list'], $item_order);
				    		}
	    				}  				
	    			}
	    		}
	    	}else{
	    		foreach($data as $article_id=>$article_data){
	    			if(!array_key_exists($article_id, $response)){
	    				$response[$article_id]=array('list'=>array());
	    			}
	    			foreach($article_data as $order_id=>$order_data){
	    				if($order_data['quantity'] > 0){
			    			$item_order=array(
					            'id'                    => $order_id,  
					            'serial_number'         => $order_data['serial_number'],
					            'customer'              => $order_data['customer_name'],
					            'created'               => date(ACCOUNT_DATE_FORMAT,$order_data['date']),
					            'delivery_date'         => $order_data['del_date'] ? date(ACCOUNT_DATE_FORMAT,$order_data['del_date']):'',
					            'quantity'              => display_number($order_data['quantity'])
					        );   
					       	array_push($response[$article_id]['list'], $item_order);
			    		}
	    			}	
	    		}   		
	    	}
	    }
		return json_out($response,$showin,$exit);
	}

?>