<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
}

global $config,$database_config;

	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$dbusers = new sqldb($database_users);
	$to_edit=false;
	//$perm_admin = $dbusers->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='admin_6' ");
	$perm_admin = $dbusers->field("SELECT value FROM user_meta WHERE user_id= :user_id AND name= :name ",['user_id'=>$_SESSION['u_id'],'name'=>'admin_6']);
	switch (true) {
		case $_SESSION['group'] == 'admin':
		case $perm_admin:
			$to_edit=true;
			break;
	}

// $view->assign('jspath',ark::$viewpath.'js/'.ark::$controller.'.js?'.$config['cache_version']);
$db2 = new sqldb();
$db3 = new sqldb();
$db_lines=new sqldb();
$o = array(
	'main_comp_info'=>array(
		'name'		=> ACCOUNT_COMPANY,
		'address'	=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
		'zip'		=> ACCOUNT_DELIVERY_ZIP,
		'city'		=> ACCOUNT_DELIVERY_CITY,
		'country'	=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
		'phone'		=> ACCOUNT_PHONE,
		'fax'		=> ACCOUNT_FAX,
		'email'		=> ACCOUNT_EMAIL,
		'url'		=> ACCOUNT_URL,
		'logo'		=> defined('ACCOUNT_LOGO') && ACCOUNT_LOGO!='' ? ACCOUNT_LOGO : 'images/no-logo.png',
		)
);
$in['main_comp_info']=array(
	'name'		=> ACCOUNT_COMPANY,
	'address'	=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
	'zip'		=> ACCOUNT_DELIVERY_ZIP,
	'city'		=> ACCOUNT_DELIVERY_CITY,
	'country'	=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
	'phone'		=> ACCOUNT_PHONE,
	'fax'		=> ACCOUNT_FAX,
	'email'		=> ACCOUNT_EMAIL,
	'url'		=> ACCOUNT_URL,
	'logo'		=> defined('ACCOUNT_LOGO') && ACCOUNT_LOGO!='' ? ACCOUNT_LOGO : 'images/no-logo.png',
	);

$o['default_currency']     	= ACCOUNT_CURRENCY_TYPE;
$o['default_currency_name']    = build_currency_name_list(ACCOUNT_CURRENCY_TYPE);

$o['is_admin']			=   $_SESSION['access_level'] == 1 ? true : false;
$in['is_admin']     	=   $_SESSION['access_level'] == 1 ? true : false;

$o['show_articles_pdf']     	= defined('SHOW_ARTICLES_PDF') && SHOW_ARTICLES_PDF=='1' ? true : false;
$o['show_stock_warning']        = defined('SHOW_STOCK_WARNING') && SHOW_STOCK_WARNING=='1' ? true : false;
$o['allow_stock']               = defined('ALLOW_STOCK') && ALLOW_STOCK=='1' ? true : false;

$o['search_by']=$in['search_by'];
if(!$in['search_by']){
	$o['search_by']=1;
}
// $view->assign(array(
$o['search_by_name_check']		        = $in['search_by'] == 1 ? 'CHECKED' : '';
$o['search_by_company_number_check']	= $in['search_by'] == 2 ? 'CHECKED' : '';
// ));

$o['payment_term']=$in['payment_term'];
if(!$in['payment_term']){
	$o['payment_term']=15;
}
$cancel_link = "index.php?do=order-orders";

if(( ( !$in['order_id'] && !$in['duplicate_order_id']) ||  $in['c_quote_id'] ) || $in['quote_id'] || $in['order_id'] == 'tmp' ){
	$o['do_request'] = 'order-norder-order-updateCustomerData';
	if($in['deal_id']){
			$o['deal_id']=$in['deal_id'];
			$deal_data=$db->query("SELECT * FROM tblopportunity WHERE opportunity_id='".$in['deal_id']."' ");
			if($deal_data->next()){
				if($in['buyer_id']=='tmp' && $deal_data->f('buyer_id')){
					$in['buyer_id']=$deal_data->f('buyer_id');
					$o['add_customer']=false;
				}
				if(!$in['contact_id'] && $deal_data->f('contact_id')){
					$in['contact_id']=$deal_data->f('contact_id');
				}
				$in['subject']=$deal_data->f('subject');
				$in['main_address_id']=$deal_data->f('main_address_id');
				$in['delivery_address_id']=$deal_data->f('same_address');
				$in['identity_id']=$deal_data->f('identity_id');
				$in['source_id']=$deal_data->f('source_id');
				$in['type_id']=$deal_data->f('type_id');
				$in['segment_id']=$deal_data->f('segment_id');
			}		
	}
	if($in['vat_regime_id']){
		if($in['vat_regime_id']<10000){
			$in['vat_regime_id']=$db->field("SELECT id FROM vat_new WHERE vat_regime_id='".$in['vat_regime_id']."' ");
		}	
		$in['remove_vat'] = $db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");     
	}

	$default_email_language = $db->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");

	if($in['buyer_id'] == 'tmp'){
		//$in['add_customer']				= true;
		$in['add_customer']				= false;
		$in['cc']						= get_cc($in,true,false);
		$in['country_dd']				= build_country_list();
		$in['language_dd']				= build_language_dd();
		$in['gender_dd']				= gender_dd();
		$in['title_dd']					= build_contact_title_type_dd(0);
		$in['title_id']					= '0';
		$in['gender_id']				= '0';
		$in['language_id']				= '0';
		$in['main_country_id']			= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';
		$in['email_language'] 			= $in['email_language'] ? $in['email_language'] : $default_email_language;
		$in['APPLY_DISCOUNT_LIST']   	= build_apply_discount_list($in['apply_discount']);
		$in['apply_discount'] 			= '0';
		$in['apply_discount_line'] 		= ORDER_APPLY_DISCOUNT? true :false;
		$in['apply_discount_global'] 	= false;
		$in['payment_type_dd']     		= build_payment_type_dd($in['payment_type']);
		$in['payment_type']				= '0';
		$in['language_dd'] 				= build_language_dd_new($in['email_language']);
		$in['currency_id']				= ACCOUNT_CURRENCY_TYPE;
		$in['identity_id'] 				= get_user_customer_identity($_SESSION['u_id']);
		$in['articles_list']			= get_articles_list($in,true,false);
		$in['authors']					= get_author($in,true,false);
		$in['accmanager']				= get_accmanager($in,true,false);
		$in['contacts']					= get_contacts($in,true,false);
		$in['customers']				= get_customers($in,true,false);
		$in['addresses']				= get_addresses($in,true,false);
		$in['site_add']										= true;
		$in['site_addresses']								= get_addresses($in,true,false);
		$in['is_vat']					= $in['remove_vat'] == 1 ? false : true;
		$in['languages']				= $in['email_language'];
		$in['do_request']				= 'order-norder-order-updateCustomerData';
		$in['remove_vat'] ? $vat_column = 0 : $vat_column = 1 ;
		if(!ALLOW_ARTICLE_PACKING){
			$in['allow_article_packing']=0;
		}else{
			$in['allow_article_packing']=1;
		}
		if(!ALLOW_ARTICLE_SALE_UNIT){
			$in['allow_article_sale_unit']=0;
		}else{
			$in['allow_article_sale_unit']=1;
		}
		$colspan_code = $vat_column.'-'.$in['allow_article_packing'].'-'.$in['apply_discount'];
		switch ($colspan_code) {
			case '0-0-0':
			case '0-0-2':
			$cols = 6;
			$col = 6;
				break;
			case '1-0-0':
			case '0-0-1':
			case '0-0-3':
			case '1-0-2':
			case '0-1-0':
			case '0-1-2':
				$cols = 7;
				$col = 5;
				break;
			case '1-0-1':
			case '1-0-3':
			case '0-1-1':
			case '0-1-3':
			case '1-1-0':
			case '1-1-2':
				$cols = 8;
				$col = 4;
				break;
			case '1-1-1':
			case '1-1-3':
				$cols = 9;
				$col = 3;
				break;
		}
		$in['colum'] = $col;

		$default_del_term =$db->field("SELECT value FROM settings WHERE constant_name='ORDER_DELIVERY_TERM'"); 
		if($default_del_term){
			$str = strtotime('+'.$default_del_term.' day');
		}else{
			$str=time();
		}
   
		$in['delivery_date'] = date(ACCOUNT_DATE_FORMAT, mktime(0, 0, 0, date('m', $str), date('d', $str), date('Y', $str)) );
		$in['del_date'] = $str*1000;
		$in['can_change']			= $to_edit;
		$in['shipping_price']							= display_number(0);

		//print_r($in);
		json_out($in);
	}elseif ($in['buyer_id'] && !is_numeric($in['buyer_id'])) {
		$o = array('redirect'=>'orders');
		json_out($o);
		
	}
	// console::log('ba da2 '.DEFAULT_LANG_ID);
	$in['email_language'] 		= $in['language']? $in['language']:($in['email_language'] ? $in['email_language'] : $default_email_language);

	$in['languages']			= $in['email_language'];
	$in['currency_id']			= ACCOUNT_CURRENCY_TYPE;
	$in['identity_id']			= $in['identity_id'] ? $in['identity_id'] : get_user_customer_identity($_SESSION['u_id'],$in['buyer_id']);
	//ADD
	$cancel_link = "index.php?do=order-orders";
    // $page_title = gm('Add Order');
  	if($in['quote_id']){ // we ignore
  		$view->assign('quote_id',$in['quote_id']);
  	}
  	$do_next = 'order-norder-order-add_order';

  	$apply_discount = 0;

  	if($in['c_quote_id'] ){ // we ignore
  		// $view->assign('quote_id',$in['c_quote_id']);
  		$o['c_quote_id'] = $in['c_quote_id'];
  		$o['quote_id'] = $in['c_quote_id'];
  		$in['our_ref'] = $in['a_quote_id'];

		if(isset($in['version_id']) && is_numeric($in['version_id'])){
			$quote_filter = 'AND tblquote_version.version_id = '.$in['version_id'];
		}else{
			$quote_filter = 'AND tblquote_version.active = 1';
		}
        $quote = $db->query("SELECT tblquote.*,tblquote_version.active,tblquote_version.version_code,tblquote_version.version_id,tblquote_version.active AS version_active, tblquote_version.version_date,tblquote_version.version_own_reference,tblquote_version.version_subject
                    FROM tblquote
                    INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
                    WHERE tblquote.id='".$in['c_quote_id']."' AND 1=1 ".$quote_filter);
		/*if(!$db->move_next()){
		    exit();
		}*/
		if($quote->f('delivery_address')){
			$in['delivery_address']=$quote->f('delivery_address');
		}
		$version_id =   $quote->f('version_id');

		$in['subject']=$in['subject'] ? $in['subject'] : $quote->f('version_subject');
		$q_line_discount = $quote->f('discount_line_gen');
		$in['apply_discount']= $quote->f('apply_discount');
		$o['apply_discount'] = (string)$in['apply_discount'];
        $in['discount']= display_number($quote->f('discount'));

        if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
					$o['apply_discount_line'] = false;
				}else{
					$o['apply_discount_line'] = true;
				}

		if($in['apply_discount'] < 2){
					$o['apply_discount_global'] = false;
				}else{
					$o['apply_discount_global'] = true;
				}
        
        $in['your_ref']= $quote->gf('version_own_reference');
        $in['currency_id'] = $quote->f('currency_type') ? $quote->f('currency_type') : ACCOUNT_CURRENCY_TYPE;
        $in['delivery_address_id'] = $quote->f('delivery_address_id');
        $o['main_address_id'] = $quote->f('main_address_id');
		//$q_apply_fix_disc = $db->f('apply_fix_disc');
		//$q_apply_line_disc = $db->f('apply_line_disc');
		$in['identity_id']= $quote->f('identity_id');
		$in['email_language'] = $quote->f('email_language');
		$in['yuki_project_id']= $quote->f('yuki_project_id');
		$o['yuki_project_id'] = $in['yuki_project_id'];
		if($quote->f('trace_id')){
			$o['deal_id']=$db->field("SELECT tracking_line.origin_id FROM tracking_line
				INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
				LEFT JOIN tblopportunity ON tracking_line.origin_id = tblopportunity.opportunity_id AND tblopportunity.f_archived='0'
				WHERE tracking_line.origin_type='11' AND tracking.target_type='2' AND tracking_line.trace_id='".$quote->f('trace_id')."' ");
		}
  	}

  	if(!$in['date_h']){
		$in['date'] = date(ACCOUNT_DATE_FORMAT, mktime(0, 0, 0, date("n"), date("j"), date("Y")));
		$in['date_h'] = time()*1000;
	}
	$o['date'] = $in['date'];
	$o['date_h'] = $in['date_h'];
	$o['subject']=stripslashes($in['subject']);

	if(!$in['del_date']){
		$default_del_term =$db->field("SELECT value FROM settings WHERE constant_name='ORDER_DELIVERY_TERM'"); 
		if($default_del_term){
			$str = strtotime('+'.$default_del_term.' day');
		}else{
			$str=time();
		}
   
		$in['delivery_date'] = date(ACCOUNT_DATE_FORMAT, mktime(0, 0, 0, date('m', $str), date('d', $str), date('Y', $str)) );
		//$in['del_date'] = time()*1000;
		$in['del_date'] = $str*1000;
	}
	$o['delivery_date'] = $in['delivery_date'];
	$o['del_date'] = $in['del_date'];

	/*if (!$in['serial_number']){
		$in['serial_number'] = generate_order_serial_number(DATABASE_NAME);
	}*/
	$o['serial_number'] = $in['serial_number'];


	if(!ALLOW_ARTICLE_PACKING){
		$in['allow_article_packing']=0;
	}else{
		$in['allow_article_packing']=1;
	}
	if(!ALLOW_ARTICLE_SALE_UNIT){
		$in['allow_article_sale_unit']=0;
	}else{
		$in['allow_article_sale_unit']=1;
	}
	$o['buyer_id'] = $in['buyer_id'];
	$o['customer_id'] = $in['buyer_id'];
	$o['contact_id'] = $in['contact_id'] ? $in['contact_id'] : '';

	$details = array('table' 		=> $in['buyer_id'] ? 'customer_addresses' : 'customer_contact_address' ,
					 'field'		=> $in['buyer_id'] ? 'customer_id' : 'contact_id',
					 'value'		=> $in['buyer_id'] ? $in['buyer_id'] : $in['contact_id'],
					 'name'			=> $in['buyer_id'] ? stripslashes($in['s_buyer_id']) : stripslashes(substr($in['s_customer_id'], 0, strpos($in['s_customer_id'],'>'))) ,
	);

	$ref = '';
	$o['customer_ref'] = '';
	
	$customer_notes="";
	if(!$in['buyer_id']){
		$details['cat'] = '0';

		$o['is_company']=false;
		$buyer_info = $db->query("SELECT phone, cell, email FROM customer_contacts WHERE ".$details['field']."='".$details['value']."' ");
		$buyer_info->next();
	    if(!$details['name']){
	    	$contact_name = $db2->query("SELECT firstname,lastname,contact_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
	    	$details['name'] = $contact_name->f('firstname').' '.$contact_name->f('lastname');
	    }
	}else {
		$customer_id = $in['buyer_id'];
		//$o['do_request']= 'order-norder-order-add_order';
		$o['is_company']=true;
		$buyer_info = $db->query("SELECT deliv_disp_note, cat_id, c_email, our_reference ,comp_phone,name, customers.no_vat,customers.btw_nr, customers.internal_language,customers.vat_regime_id,
								customers.line_discount, customers.apply_fix_disc,customers.fixed_discount, customers.currency_id, customers.apply_line_disc, customers.identity_id,customers.customer_notes
								FROM customers WHERE ".$details['field']."='".$details['value']."' ");
		$buyer_info->next();


		$customer_notes=stripslashes($buyer_info->f('customer_notes'));
		//$o['discount_line_gen']= display_number($buyer_info->f('line_discount'));
		if($buyer_info->f('apply_line_disc')==1){
				$o['discount_line_gen']= display_number($buyer_info->f('line_discount'));
			} else {
				$o['discount_line_gen']= display_number(0);
			}
		if(!$in['vat_regime_id']){
			$in['vat_regime_id'] = $buyer_info->f('vat_regime_id');
		}
		if(!$in['vat_regime_id']){
			$vat_regime_id = $db->field("SELECT id FROM vat_new WHERE `default`='1'");

		    if(empty($vat_regime_id)){
		      $vat_regime_id = 1;
		    }
			$in['vat_regime_id'] = $vat_regime_id;
		}
		if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
			$apply_discount = 3;
		}else{
			if($buyer_info->f('apply_fix_disc')){
				$apply_discount = 2;
			}
			if($buyer_info->f('apply_line_disc')){
				$apply_discount = 1;
			}
		}
		if(!$in['c_quote_id']){
		   	$in['apply_discount'] = $apply_discount;
		   	//$o['apply_discount'] = (string)$apply_discount;
		   	$o['apply_discount'] = $apply_discount;
	    }
	    //var_dump($o['apply_discount']);exit;
			if(!$details['name'] ){
	    	$details['name']=$buyer_info->f('name');
	    }
	    $details['cat'] = $buyer_info->f('cat_id');
		//$in['remove_vat'] = $buyer_info->f('no_vat');
		if(is_null($in['remove_vat'])){
			$in['remove_vat'] = $db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
		}
		$o['customer_ref']=$buyer_info->f('our_reference');
		if(ACCOUNT_ORDER_REF && $buyer_info->f('our_reference')){
			$ref = $buyer_info->f('our_reference').ACCOUNT_ORDER_DEL;
		}
		// var_dump($buyer_info->f('internal_language'));

		 $in['email_language'] 		= $buyer_info->f('language') ? $buyer_info->f('language') : $in['email_language'];
		 $in['currency_id']			= $in['c_quote_id']? $in['currency_id']: ($buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE);
		 
		 $in['del_note']			= $in['del_note'] ? $in['del_note'] : $buyer_info->f('deliv_disp_note');
		 $in['languages']			= $in['email_language'];
		 $buyer_details = $db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND is_primary='1' ");
			$main_address_id			= $in['main_address_id'] ? $in['main_address_id'] : $buyer_details->f('address_id');
			$sameAddress = false;
		/*	if($buyer_details->f('billing')==1){
				$sameAddress = true;
			}*/
			if($buyer_details->f('delivery')==1){
				$sameAddress = true;
			}

		if($in['change_currency']){
			$in['currency_id'] 				=  $in['currency_type'] ;
			$o['currency_type'] 			= $in['currency_type'] ;
		}

		if($o['currency_type'] != ACCOUNT_CURRENCY_TYPE){
        	$params = array(
        		'currency' 					=> currency::get_currency($o['currency_type'],'code'),
				'acc' 						=> ACCOUNT_NUMBER_FORMAT,
				'into' 						=> currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code')
			);
        	$in['currency_rate'] 		= get_currency_convertor($params);
        }

	}
	$o['currency_txt'] 			= currency::get_currency($in['currency_id']);
	$o['customer_notes']=$customer_notes;
	$contact_title = $db->field("SELECT customer_contact_title.name FROM customer_contact_title
								INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
								WHERE customer_contacts.contact_id='".$in['contact_id']."'");
	if($contact_title)
	{
		$contact_title .= " ";
	}


	$contact_info=$db->query("SELECT firstname,lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."'");
	$contact_info->next();
	if($contact_info->f('lastname')) {
	   $contact_last_name = " ".$contact_info->f('lastname');
	}
	$in['contact_name']= $contact_title.$contact_info->f('firstname').$contact_last_name;
	if(!$in['apply_discount']){
		$in['apply_discount']=0;
	}
	//$in['apply_discount']=1;
	
	if($in['buyer_id'] && $in['main_address_id']){
		$buyer = $db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' AND address_id='".$in['main_address_id']."'");
	}else{
		$buyer = $db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' ORDER BY is_primary DESC");
	}
	
	$buyer->next();
	if($in['buyer_id'] && $buyer->f('delivery') == 0){
		$buyer_del_address = $db->query("SELECT * FROM ".$details['table']." WHERE ".$details['field']."='".$details['value']."' AND delivery=1 limit 1 ");
		if($buyer_del_address){
				$o['delivery_address'] = $buyer_del_address->f('address').'
'.$buyer_del_address->f('zip').'  '.$buyer_del_address->f('city').'
'.get_country_name($buyer_del_address->f('country_id'));
		}
	}

	if(!$in['c_quote_id']){ // we don't care
		//if($in['buyer_id'] && $buyer_info->f('apply_line_disc')){
		if($buyer_info->f('apply_line_disc')){
			$in['apply_discount'] = 1;
			if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
				$in['apply_discount'] = 3;
			}else{
				if($buyer_info->f('apply_fix_disc')){
					$in['apply_discount'] = 2;
				}
			}
		}else {
			$in['apply_discount']= ORDER_APPLY_DISCOUNT? 1 :0;
			if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
				$in['apply_discount'] = 3;
			}else{
				if($buyer_info->f('apply_fix_disc')){
					$in['apply_discount'] = 2;
				}
			}	
		}
	  	$discount=0;
	  	if($in['buyer_id']){
	  		if($buyer_info->f('apply_line_disc')){
	  			$discount_fix= $db2->field('SELECT line_discount FROM customers WHERE customer_id ='.$in['buyer_id']);
	  		//} else {
	  		}
	  		if($buyer_info->f('apply_fix_disc')){
	  			$discount= $db2->field('SELECT fixed_discount FROM customers WHERE customer_id ='.$in['buyer_id']);
	  		}
    	}
    	$in['discount']	    		= display_number($discount);
    	$in['discount_fix']			= display_number($discount_fix);
  	}

	$in['customer_id'] 		= $details['value'];
	$in['field'] 			= $details['field'];
	$o['field'] 			= $details['field'];
	$o['price_category_id'] = $details['cat'];
	$in['price_category_id'] = $details['cat'];
	
  	$o['buyer_ref']	    	= $ref;
  	$o['currency_name']    	= build_currency_name_list($in['currency_id']);
  	$o['currency_rate']    	= $in['currency_rate'];


  	$o['show_vat'] = $in['show_vat'];
  	if(!isset($in['show_vat'])){
  		$in['show_vat']=1;
  		$o['show_vat']=1;
  	}
    if($in['buyer_id']){
$in['address_info'] = $buyer->f('address').'
'.$buyer->f('zip').'  '.$buyer->f('city').'
'.get_country_name($buyer->f('country_id'));

    }else{
$in['address_info'] = $buyer->f('address').'
'.$buyer->f('zip').'  '.$buyer->f('city').'
'.get_country_name($buyer->f('country_id'));

    }

	$existIdentity = $db2->field("SELECT COUNT(identity_id) FROM multiple_identity ");

  	$acc_manager=$db->field("SELECT acc_manager_name FROM customers WHERE customer_id = '".$in['buyer_id']."' ");
	$acc_manager = explode(',',$acc_manager);
	$acc_manager_id=$db->field("SELECT user_id FROM customers WHERE customer_id = '".$in['buyer_id']."' ");
	$acc_manager_id = explode(',',$acc_manager_id);
	if(!$in['discount']){
		$in['discount'] = '0';
	}

	$in['remove_vat'] ? $vat_column = 0 : $vat_column = 1 ;
	$colspan_code = $vat_column.'-'.$in['allow_article_packing'].'-'.$in['apply_discount'];
	switch ($colspan_code) {
		case '0-0-0':
		case '0-0-2':
		$cols = 6;
		$col = 6;
			break;
		case '1-0-0':
		case '0-0-1':
		case '0-0-3':
		case '1-0-2':
		case '0-1-0':
		case '0-1-2':
			$cols = 7;
			$col = 5;
			break;
		case '1-0-1':
		case '1-0-3':
		case '0-1-1':
		case '0-1-3':
		case '1-1-0':
		case '1-1-2':
			$cols = 8;
			$col = 4;
			break;
		case '1-1-1':
		case '1-1-3':
			$cols = 9;
			$col = 3;
			break;
	}
	$o['colum'] = $col;
	if(!$in['delivery_address_id']){
		$in['delivery_address_id']=$in['main_address_id'];
	}
	if($in['delivery_address_id'] && $in['delivery_address_id']!=$in['main_address_id']){
		$sameAddress=false;
	}
	$o['sameAddress'] = $sameAddress;
	
    
  $buyer_adr=$db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' and address_id='".$in['main_address_id']."'");
if($in['delivery_address_id']){
	$buyer_adr=$db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' and address_id='".$in['delivery_address_id']."'");
}
 
    $in['address_info']=$buyer_adr->f('address').'
'.$buyer_adr->f('zip').'  '.$buyer_adr->f('city').'
'.get_country_name($buyer_adr->f('country_id'));

	if($in['c_quote_id']){
		$in['address_info'] = $quote->f('free_field');
		$main_address_id = $quote->f('main_address_id');
		if($in['delivery_address_id']){
			$buyer_adr=$db->query("SELECT * FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' and address_id='".$in['delivery_address_id']."'");
			$in['address_info']=$buyer_adr->f('address')."\n".$buyer_adr->f('zip').'  '.$buyer_adr->f('city')."\n".get_country_name($buyer_adr->f('country_id'));
		}
	}
   	/*if($in['currency_id'] != ACCOUNT_CURRENCY_TYPE){
   		
    	$params = array(
    		'currency' 					=> currency::get_currency($in['currency_id'],'code'),
			'acc' 						=> ACCOUNT_NUMBER_FORMAT,
			'into' 						=> currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code')
		);
		
    	$o['currency_rate'] 			= get_currency_convertor($params);
    }*/

    $in['vat']=$db->field("SELECT vats.value FROM vat_new 
									LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
									WHERE vat_new.id='".$in['vat_regime_id']."' ");



  $o['main_address_id'] = $main_address_id;
/*  	$o['apply_discount_line']      						= ORDER_APPLY_DISCOUNT;
  	$o['show_discount']									= ORDER_APPLY_DISCOUNT==1 ? true : false;
  	*/
  	$o['apply_discount']								= $in['apply_discount'];
	$o['our_ref'] = $in['our_ref'];
	$o['your_ref'] = $in['your_ref'];
	$o['add_customer']							= false;
	// $view->assign(array(
	$o['notes']									= stripslashes(get_notes($in));
	$o['form_mode']                    			= 0; //0-add 1-edi;
	$o['customer_vat_dd']        				= build_vat_dd($in['customer_vat_id']);
	$o['customer_vat_id']        				= $in['customer_vat_id'];
	$o['payment_type_dd']      				 	= build_payment_type_dd($in['payment_type']);
	$o['payment_type_txt']       				= build_payment_type_dd($in['payment_type'],true);
	$o['payment_type']							= $db->f('payment_type') ? (string)$db->f('payment_type') : '';
	$o['vat_total']	                    		= number_format(0,2);
	$o['discount_percent']      				= '( '.$in['discount'].'% )';
	$o['del_note']           					= $in['del_note'];

	$o['discount_total']						= number_format(0,2);
	$o['total']									= number_format(0,2);
	$o['total_vat']								= number_format(0,2);
	$o['hide_currency2']						= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
	$o['hide_currency1']						= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
	$o['currency_type']							= get_commission_type_list($in['currency_id']);#ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$';
	$o['default_currency_val']  				= get_commission_type_list(ACCOUNT_CURRENCY_TYPE);
	$o['currency_type_val']						= $in['currency_id'];
	$o['checked']								= $in['rdy_invoice'] ? '"checked"':'';
	$o['name_txt']								= !$in['buyer_id'] ? 'Name:' : 'Company Name:';
	$o['customer_name']							= stripslashes($details['name']);
	$o['contact_name']							= $in['contact_name'];
	$o['ref_serial_number']						= $ref.$in['serial_number'];
	$o['ref_var']								= $ref ? " var ref= '".$ref."';" : " var ref;";
	$o['delivery_address']      				= $in['delivery_address'];
	$o['delivery_address_txt']     				= nl2br($in['delivery_address']);
	$o['delivery_address_id']     				= $in['delivery_address_id'];
	$o['customer_address']						= $buyer->f('address');
	$o['customer_zip']							= $buyer->f('zip');
	$o['customer_vat_number']					= $buyer_info->f('btw_nr');
	$o['customer_city']							= $buyer->f('city');
	$o['customer_country_id']					= $buyer->f('country_id');
	$o['customer_cell']							= !$in['buyer_id'] ? $buyer_info->f('cell') : '';
	$o['customer_phone']						= !$in['buyer_id'] ? $buyer_info->f('phone') : $buyer_info->f('comp_phone');
	$o['customer_email']						= !$in['buyer_id'] ? $buyer_info->f('email') : $buyer_info->f('c_email');
	$o['buyer_country']							= get_country_name($buyer->f('country_id'));
	$o['comp_name']								= $in['s_buyer_id'];
	$o['is_buyer_id']							= $in['s_buyer_id'] ? true : false;
	$o['remove_vat']							= $in['remove_vat'];
	$o['is_vat']								= $in['remove_vat'] == 1 ? false : true;
	$o['is_buyer']			    				= $in['buyer_id'] ? true : false;
	$o['show_vat_2']							= $in['remove_vat'] == 1 ? false : true;
	$o['show_vat']				      			= $in['show_vat'] == 1 ? true : false;
	$o['show_vat_txt']							= $in['show_vat'] == 1 ? gm('Yes') : gm('No');
	$o['address_info_txt']						= nl2br(trim($in['address_info']));
	$o['address_info']							= $in['address_info'];
	$o['APPLY_DISCOUNT_LIST']   				= build_apply_discount_list($in['apply_discount']);
	$o['APPLY_DISCOUNT_LIST_TXT']   			= build_apply_discount_list($in['apply_discount'],true);
	$o['DISCOUNT']              				= display_number($in['discount']);
	$o['discount']              				= display_number($in['discount']);
	$o['discount_fix']              			= display_number($in['discount_fix']);
	$o['hide_global_discount']					= $in['apply_discount'] < 2 ? false : true;
	$o['hide_line_global_discount']				= $in['apply_discount'] == 1 ? '' : 'hide';
	$o['hide_disc']								= $in['apply_discount'] == 1 || $in['apply_discount'] == 3 ? true : false;
	$o['total_currency_hide']			= 		$in['currency_type'] ? ($in['currency_type'] == ACCOUNT_CURRENCY_TYPE ? false : true) : false;
	$o['disc_line']								= display_number($buyer_info->f('line_discount'));
	$o['author_id']								= isset($in['author_id']) ? $in['author_id'] : $_SESSION['u_id'];
	$o['acc_manager']							= isset($in['author_id']) ? get_user_name($db->gf('author_id')) : get_user_name($_SESSION['u_id']);

	$o['acc_manager_txt']    					= $acc_manager[0];
  	$o['acc_manager_id']     					= $acc_manager_id[0];
  	$o['acc_manager_name']   					= $acc_manager[0];

	$o['multiple_identity_txt'] 				= $db->field("SELECT identity_name from multiple_identity WHERE identity_id='".$in['identity_id']."'");
	$o['multiple_identity_dd'] 					= build_identity_dd($in['identity_id']);
	$o['identity_id'] 							= $in['identity_id'] ? $in['identity_id'] : '0';
	$o['existIdentity']							= $existIdentity >0 ? true : false;
	$o['sameAddress']							= isset($in['sameAddress']) ? ($in['sameAddress'] ? true :false ) : $sameAddress;
	// ));

	$o['gender_dd']					= gender_dd();
	$o['title_dd']					= build_contact_title_type_dd(0);
	$o['language_dd'] 				= build_language_dd_new($in['email_language']);

	$o['currency_type']		   		= $in['currency_id'] ? $in['currency_id'] : ACCOUNT_CURRENCY_TYPE;
	$o['currency_val']		   		= get_commission_type_list($o['currency_type']);
	$o['currency_type_list']    	= build_currency_list($o['currency_type']);
	$o['CURRENCY_TXT']		    	= currency::get_currency($o['currency_type'],'name');
	$o['vat_regim_dd']		= build_vat_regime_dd();
	$o['vat_regime_id']		= $in['vat_regime_id'] ? $in['vat_regime_id'] : '';
	$o['vat'] 				= $in['vat'];
	$o['can_change']			= $to_edit;
	$o['generalvats']=build_vat_dd();
	$o['source_dd']					= get_categorisation_source();
	$o['source_id']					= $in['source_id'];
	$o['type_dd']					= get_categorisation_type();
	$o['type_id']					= $in['type_id'];
	$o['segment_dd']= get_categorisation_segment();
	$o['segment_id']=$in['segment_id'];
	$o['copy_free_text']=$in['copy_free_text'] ? '1' : '';

	$o['price_category_dd']= get_price_categories();
	$o['cat_id']=$in['cat_id']? $in['cat_id']: $details['cat'];

	$yuki_active = $db->field("SELECT active FROM apps WHERE name='Yuki' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
	$o['yuki_project_enabled']				= defined('YUKI_PROJECTS') && YUKI_PROJECTS == 1 && $yuki_active ? true : false;
	$o['yuki_projects']						= get_yuki_projects();
	$o['yuki_project_id']					= $in['yuki_project_id'];

	if($in['vat_regime_id'] && $in['vat_regime_id']>=1000){
		$extra_eu=$db->field("SELECT regime_type FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
		if($extra_eu==2){
			$o['block_vat']=true;
		}
		if($extra_eu=='1'){
			$o['vat_regular']=true;
		}
		$contracting_partner=$db->field("SELECT vat_regime_id FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
		if($contracting_partner==4){
			$o['block_vat']=true;
		}
	}
	$o['deals']				= get_deals($in);
	$o['ADV_CRM'] 			= defined('ADV_CRM') && ADV_CRM == 1 ? true : false;
	$o['ACCESS_BARCODE_SCAN'] 			= defined('ACCESS_BARCODE_SCAN') && ACCESS_BARCODE_SCAN == 1 ? true : false;
	$o['is_advanced_subscription'] 	= get_subscription_plan($_SESSION['u_id'])== '5' ? true : false;
	$o['show_barcode_scan'] = $o['ACCESS_BARCODE_SCAN'] || $o['is_advanced_subscription'] || (DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='Salesassist_11') ? true : false;

	$separator = $db->field("SELECT value from settings where constant_name='ACCOUNT_NUMBER_FORMAT'");
	$currency = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
	//$o['currency_rate'] = currency::getCurrency(currency::get_currency($in['currency_type'],'code'),$currency, 1,$separator);

	if($o['identity_id'] != '0'){
		$mm = $db->query("SELECT * FROM multiple_identity WHERE identity_id='".$o['identity_id']."'")->getAll();
		$value = $mm[0];
		$template = $db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$o['identity_id']."' and module='order'");
		
		$o['main_comp_info']=array(
				'name'		=> $value['identity_name'],
				'address'	=> nl2br($value['company_address']),
				'zip'		=> $value['company_zip'],
				'city'		=> $value['city_name'],
				'country'	=> get_country_name($value['country_id']),
				'phone'		=> $value['company_phone'],
				'fax'		=> $value['company_fax'],
				'email'		=> $value['company_email'],
				'url'		=> $value['company_url'],
				'logo'		=> $value['company_logo'],
				);
		
	}

	$i=0;
	$total = 0;
	$total_with_vat = 0;
	$discount_total = 0;
	$vat_total = 0;
	$last_tr ='';
	$o['tr_id']=array();
	$global_disc = $in['discount'];
	$vat_percent = array();
	$sub_t = array();
	$sub_discount = array();
	$o['vat_line']  = array();

  	if($in['c_quote_id']){ //we don't care
	    
	    $in['purchase_price']=array();$in['article']=array();	 $in['article_code']=array(); $in['article_id']=array(); $in['quantity']=array();$in['quantity_old']=array(); $in['quantity_component']=array(); $in['is_tax']=array();$in['disc']=array();$in['content']=array();$in['tax_id']=array();$in['tax_for_article_id']=array();
	    $in['packing']=array();  $in['sale_unit']=array(); $in['vat_percent']=array();$in['price']=array();$in['price_vat']=array();$in['vat_value']=array();$in['has_variants']=array();$in['has_variants_done']=array();$in['is_variant_for']=array();$in['variant_type']=array();$in['is_combined']=array();$in['component_for']=array();$in['visible']=array();$in['has_taxes']=array();$in['purchase_price_other_currency']=array();$in['purchase_price_other_currency_id']=array(); $in['show_stock_warning_line']=array();

	    $quote_line_type_sql=$in['copy_free_text']=='1' ? "(1,2,3)" : "(1,2)";

	    if(isset($in['version_id']) && is_numeric($in['version_id'])){
			$quote_filter = 'AND tblquote_version.version_id = '.$in['version_id'];
		}else{
			$quote_filter = 'AND tblquote_version.active = 1';
		}
	    $db_lines->query("SELECT tblquote_line.*, tblquote_version.active FROM tblquote_line
				            INNER JOIN tblquote_version ON tblquote_line.version_id=tblquote_version.version_id
				           WHERE tblquote_line.quote_id='".$in['c_quote_id']."'  AND line_type IN ".$quote_line_type_sql." AND quantity!='0' ".$quote_filter." ORDER BY id ASC");

	    while($db_lines->move_next()){

	    	if($db_lines->f('has_variants') && !$db_lines->f('has_variants_done')){
	    		continue;
	    	}else if($db_lines->f('has_variants') && $db_lines->f('has_variants_done')){
	    		$child_variant_quantity=$db3->field("SELECT quantity FROM tblquote_line WHERE quote_id='".$db_lines->f('quote_id')."' AND version_id='".$db_lines->f('version_id')."' AND group_id='".$db_lines->f('group_id')."' AND has_variants='0' AND is_variant_for='".$db_lines->f('article_id')."' AND id>'".$db_lines->f('id')."' ORDER BY id ASC LIMIT 1");
	    		if(!$child_variant_quantity){
	    			continue;
	    		}
	    	}

	    	$quantity_component = 0;
	    	if($db_lines->f('component_for')){
	    		$parent_article_id = $db->field("SELECT article_id FROM tblquote_line where id ='".$db_lines->f('component_for')."' ");
	    		$quantity_component = get_quantity_component($parent_article_id, $db_lines->f('article_id'));
	    	}
	    	if(NOT_COPY_ARTICLE_ORD==1 && $db_lines->f('article_id')){
	    		array_push($in['article'],get_article_label($db_lines->f('article_id'),$in['languages'],'order'));
	    	}else if($db_lines->f('line_type')=='3'){
				array_push($in['article'],html_entity_decode($db_lines->f('name')));
	    	}else if($db_lines->f('line_type')=='1' || $db_lines->f('line_type')=='2'){
				array_push($in['article'],strip_tags(html_entity_decode($db_lines->f('name'))));
	    	}else{
	    		array_push($in['article'],$db_lines->f('name'));
	    	}
			//array_push($in['article'],  (NOT_COPY_ARTICLE_ORD==1 && $db_lines->f('article_id')) ? get_article_label($db_lines->f('article_id'),$in['languages'],'order'):(($db_lines->f('line_type')=='3' || $db_lines->f('line_type')=='1') ? strip_tags(html_entity_decode($db_lines->f('name'))): $db_lines->f('name')));
			array_push($in['article_code'],  $db_lines->f('article_code'));
			array_push($in['quantity'],  $db_lines->f('quantity'));
			array_push($in['quantity_old'],  $db_lines->f('quantity'));
			array_push($in['quantity_component'],  $quantity_component);
			array_push($in['disc'],  $db_lines->f('line_discount'));

			array_push($in['sale_unit'],  $db_lines->f('sale_unit'));
			array_push($in['packing'],  $db_lines->f('package'));
			array_push($in['is_tax'], $db_lines->f('is_tax'));
			if ($db_lines->f('is_tax')){
				$tax_for_article_id = $db2->field("SELECT article_id FROM  tblquote_line WHERE id='".$db_lines->f('tax_for_line_id')." ' ");
				$tax_id = $db_lines->f('article_id');
			}else {
				$tax_for_article_id=0;
				$tax_id = 0;
			}
			array_push($in['tax_id'], $tax_id);
			array_push($in['tax_for_article_id'], $tax_for_article_id);
			array_push($in['content'], ($db_lines->f('line_type')=='3' ? '1': $db_lines->f('content')));
			$price=$db_lines->f('price');
			$db2->query("SELECT  pim_articles.article_id ,vat_id, is_service, stock, article_threshold_value  FROM  pim_articles WHERE article_id='".addslashes($db_lines->f('article_id'))." ' ");
			$db2->move_next();

				/*$purchase_price= $db3->field("SELECT pim_article_prices.purchase_price FROM pim_article_prices WHERE pim_article_prices.base_price=1  AND  pim_article_prices.article_id='".$db2->f('article_id')."'");*/
			$purchase_price = $db_lines->f('purchase_price');
			array_push($in['purchase_price'], $purchase_price);
			array_push($in['purchase_price_other_currency'], '0');
			array_push($in['purchase_price_other_currency_id'], ACCOUNT_CURRENCY_TYPE);

			array_push($in['article_id'],  $db2->f('article_id'));
			array_push($in['vat_percent'],$db_lines->f('vat'));
				array_push($in['price'], $price);

			if($in['remove_vat']) {
				array_push($in['price_vat'], $price);
			}else{
				array_push($in['price_vat'], $price + ($price * get_vat_val($db2->f('vat_id'))/100));
			}
			array_push($in['vat_value'],$price * (get_vat_val($db2->f('vat_id'))/100));
			array_push($in['has_variants'], $db_lines->f('has_variants'));
			array_push($in['has_variants_done'], $db_lines->f('has_variants_done'));
			array_push($in['is_variant_for'], $db_lines->f('is_variant_for'));
			array_push($in['variant_type'], $db_lines->f('variant_type'));
			array_push($in['is_combined'], $db_lines->f('is_combined'));
			array_push($in['component_for'], $db_lines->f('component_for')?$db_lines->f('component_for') :'');
			array_push($in['visible'], $db_lines->f('visible'));

			$show_stock_warning_line = false;
			if($o['show_stock_warning'] && $o['allow_stock'] && $db2->f('is_service')==0 && $db_lines->f('is_combined')==0 && $db2->f('stock') - $db_lines->f('quantity') < $db2->f('article_threshold_value')){
				$show_stock_warning_line = true;
			} 
			array_push($in['show_stock_warning_line'], $show_stock_warning_line);

			$nr_taxes =  $db2->field("SELECT COUNT(article_tax_id) FROM pim_articles_taxes WHERE article_id='".$db_lines->f('article_id')."' ");
			array_push($in['has_taxes'], $nr_taxes? 1: 0);
	    }

	    $in['discount_line_gen']= display_number($q_line_discount);
	    $o['discount_line_gen']= display_number($q_line_discount);
	    $view->assign(array(
	      'disc_line'				=> display_number($q_line_discount),
	    ));

	    if($in['article']){
			$in['remove_vat'] ? $vat_column = 0 : $vat_column = 1 ;
			$colspan_code = $vat_column.'-'.$in['allow_article_packing'].'-'.$in['apply_discount'];
			switch ($colspan_code) {
				case '0-0-0':
				case '0-0-2':
				$cols = 6;
				$col = 6;
					break;
				case '1-0-0':
				case '0-0-1':
				case '0-0-3':
				case '1-0-2':
				case '0-1-0':
				case '0-1-2':
					$cols = 7;
					$col = 5;
					break;
				case '1-0-1':
				case '1-0-3':
				case '0-1-1':
				case '0-1-3':
				case '1-1-0':
				case '1-1-2':
					$cols = 8;
					$col = 4;
					break;
				case '1-1-1':
				case '1-1-3':
					$cols = 9;
					$col = 3;
					break;
			}

			foreach ($in['article'] as $row){
				$row_id = 'tmp'.$i;
				if(!ALLOW_ARTICLE_SALE_UNIT){
					$in['sale_unit'][$i]=1;
				}

				if(!ALLOW_ARTICLE_PACKING){
					$in['packing'][$i]=1;
					$in['sale_unit'][$i]=1;
				}

				$discount_line = $in['disc'][$i];
				if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
					$discount_line = 0;
				}
				if($in['apply_discount'] < 2){
					$global_disc = 0;
				}
				$q = $in['quantity'][$i] * $in['packing'][$i] / $in['sale_unit'][$i];

				$price_line = $in['price'][$i] - ($in['price'][$i] * $discount_line /100);

				$discount_total += $price_line * $global_disc / 100 * $q;
				
				$vat_total += ($price_line - $price_line * $global_disc /100) * $in['vat_percent'][$i] / 100 * $q;
				$line_total=$price_line * $q;

				$vat_value_line =($price_line - $price_line * $global_disc /100) *$in['vat_percent'][$i] / 100 * $q;
				$vat_percent[$in['vat_percent'][$i]] += round($vat_value_line,2);

				$sub_t[$in['vat_percent'][$i]] += round( $line_total,ARTICLE_PRICE_COMMA_DIGITS );
				$sub_discount[$in['vat_percent'][$i]] += ($line_total * $global_disc /100);

				$total += $line_total;

				if(!($in['is_tax'][$i])){
					 $in['is_tax'][$i]=0;
					 $last_tr = $row_id;
				}
				if(!$in['packing'][$i]){
					$in['packing'][$i]=1;
				}
				if(!$in['packing'][$i]){
					$in['sale_unit'][$i]=1;
				}
				$stock=$db2->field("SELECT stock from pim_articles WHERE article_id='".$in['article_id'][$i]."' ");
				$hide_stock=$db2->field("SELECT hide_stock from pim_articles WHERE article_id='".$in['article_id'][$i]."' ");
			    $threshold_value=$db2->field("SELECT article_threshold_value from pim_articles WHERE article_id='".$in['article_id'][$i]."' ");
			 	$pending_articles=$db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$in['article_id'][$i]."' AND delivered=1");

			 	$nr_dec_q = strlen(rtrim(substr(strrchr($in['quantity'][$i], "."), 1),'0'));
			 	if($in['is_combined'][$i]){
					$last_combined = $row_id;
				}

				if($in['has_variants'][$i] && $in['has_variants_done'][$i]){
					$last_variant_parent = $row_id;
				}

				$linie= array(
				'tr_id'         			=> $row_id,
				'article'  			    	=> $in['article'][$i],
				'content'  			    	=> $in['content'][$i],
				'colspan'					=> $in['content'][$i] ==1 ? 'colspan="8" class="last"' : '',
				'article_id'				=> $in['article_id'][$i],
				'is_article'      	      	=> $in['article_id'][$i] ? true : false,
				'tax_id'				    => $in['tax_id'][$i],
				'tax_for_article_id'	    => $in['is_tax'][$i]==1?$in['tax_for_article_id'][$i]:'',
				'is_tax'      	            => $in['is_tax'][$i],
				'quantity_component'      	=> display_number($in['quantity_component'][$i], $nr_dec_q),
				'quantity_old'      	    => display_number($in['quantity_old'][$i], $nr_dec_q),
				'quantity'      			=> display_number($in['quantity'][$i], $nr_dec_q),
				'price_vat'      			=> display_number_var_dec($in['price_vat'][$i]),
				'price'         			=> display_number_var_dec($in['price'][$i]),
				'purchase_price'         	=> $in['purchase_price'][$i],
				'purchase_price_other_currency'         	=> $in['purchase_price_other_currency'][$i],
				'purchase_price_other_currency_id'         	=> $in['purchase_price_other_currency_id'][$i],
				'percent'					=> $in['vat_percent'][$i],
				'vat_value'					=> $in['vat_value'][$i],
				'percent_x'					=> display_number($in['vat_percent'][$i]),
				'vat_value_x'			    => display_number($in['vat_value'][$i]),
				'sale_unit_x'				=> $in['sale_unit'][$i],
				'packing_x'					=> remove_zero_decimals($in['packing'][$i]),
				'sale_unit'				    => $in['sale_unit'][$i],
				'packing'					=> remove_zero_decimals($in['packing'][$i]),
				'stock'				    	=> $stock,
				'pending_articles'          => intval($pending_articles),
				'threshold_value'           => intval($threshold_value),
				'hide_stock'           		=>  $hide_stock,
				'disc'				        => display_number($in['disc'][$i]),
				'line_total'				=> display_number($line_total),
				'td_width'					=> ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px',
				'input_width'				=> ALLOW_ARTICLE_PACKING ? 'width:164px' : 'width:223px',
				'article_code'				=> $in['article_code'][$i],
				'is_article_code'			=> $in['article_code'][$i] ? true : false,
				'colum'						=> !$in['article_code'][$i] ? $col+2 : $col,
				'for_article'				=> $in['is_tax'][$i]==1? $last_tr :'',
				'has_variants' 				=> $in['has_variants'][$i],
			 	'has_variants_done' 		=> $in['has_variants_done'][$i],
			 	'is_variant_for' 			=> $in['is_variant_for'][$i],
			 	'is_variant_for_line' 		=> $in['is_variant_for'][$i]? $last_variant_parent :'',
			 	'variant_type' 				=> $in['variant_type'][$i], 
			 	'is_combined' 				=> $in['is_combined'][$i],
			 	'component_for' 			=> $in['component_for'][$i]? $last_combined :'' ,
			 	'visible' 					=> $in['visible'][$i], 
			 	'has_taxes' 				=> $in['has_taxes'][$i],
			 	'show_stock_warning_line' 		=> $in['show_stock_warning_line'][$i],
				);
				// $row['colum'] = $o['colum'];
				// $linie = $row;

				array_push($o['tr_id'], $linie);
				$i++;
			}
		}

	}else{

		 //console::log($in['article']);
		if($in['article']){
			$in['remove_vat'] ? $vat_column = 0 : $vat_column = 1 ;
			$colspan_code = $vat_column.'-'.$use_package.'-'.$in['apply_discount'];
			switch ($colspan_code) {
				case '0-0-0':
				case '0-0-2':
				$cols = 6;
				$col = 6;
					break;
				case '1-0-0':
				case '0-0-1':
				case '0-0-3':
				case '1-0-2':
				case '0-1-0':
				case '0-1-2':
					$cols = 7;
					$col = 5;
					break;
				case '1-0-1':
				case '1-0-3':
				case '0-1-1':
				case '0-1-3':
				case '1-1-0':
				case '1-1-2':
					$cols = 8;
					$col = 4;
					break;
				case '1-1-1':
				case '1-1-3':
					$cols = 9;
					$col = 3;
					break;
			}
		console::log($in['article']);
			foreach ($in['article'] as $row){
				$row_id = 'tmp'.$i;
				if(!ALLOW_ARTICLE_SALE_UNIT){
					$row['sale_unit']=1;
				}

				if(!ALLOW_ARTICLE_PACKING){
					$row['packing']=1;
					$row['sale_unit']=1;
				}

				$discount_line = $in['changePrices'] ? return_value($o['disc_line']) : $row['disc'];

				$row['price'] =return_value($row['price']) ;
				$row['price_vat'] = return_value($row['price_vat']) ;


				if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
					$discount_line = 0;
				}
				if($in['apply_discount'] < 2){
					$global_disc = 0;
				}
				$q = $row['quantity'] * $row['packing'] / $row['sale_unit'];

				$price_line = $row['price'] - ($row['price'] * $discount_line /100);

				$discount_total += $price_line * $global_disc / 100 * $q;
				
				$vat_total += ($price_line - $price_line * $global_disc /100) * $row['percent'] / 100 * $q;
				$line_total=$price_line * $q;

				$vat_value_line =($price_line - $price_line * $global_disc /100) *$row['percent'] / 100 * $q;
				$vat_percent[$row['percent']] += $vat_value_line;

				$sub_t[$row['percent']] +=$line_total;
				$sub_discount[$row['percent']] += ($line_total * $global_disc /100);

				$total += $line_total;

				if(!($row['is_tax'])){
					 $row['is_tax']=0;
					 $last_tr = $row_id;
				}
				if(!$row['packing']){
					$row['packing']=1;
				}
				if(!$row['packing']){
					$row['sale_unit']=1;
				}

				if($row['article_id']){
					$art = $db2->query("SELECT stock, hide_stock, article_threshold_value, is_service from pim_articles WHERE article_id='".$row['article_id']."' ");
					$stock = $art->f('stock');
					$is_service = $art->f('is_service');
					$hide_stock = $art->f('hide_stock');
			    	$threshold_value = $art->f('article_threshold_value');
			 		$pending_articles=$db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$row['article_id']."' AND delivered=1");

				}
				
			 	$nr_dec_q = strlen(rtrim(substr(strrchr($row['quantity'], "."), 1),'0'));
			 	if($row['is_combined']){
					$last_combined = $row_id;
				}

				if($row['has_variants'] && $row['has_variants_done']){
					$last_variant_parent = $row_id;
				}

				$show_stock_warning_line = false;
				if($o['show_stock_warning'] && $o['allow_stock'] && $is_service==0 && $row['is_combined']==0 && $stock - $row['quantity'] < $threshold_value){
					$show_stock_warning_line = true;
				} 

				$colum=$col;
				if(!$row['article_code'] && !$row['article_id'] && !$row['content'] ){
					$colum+=2;
				}
			 	
				$linie= array(
				'tr_id'         			=> $row_id,
				'article'  			    	=> $row['article'],
				'content'  			    	=> $row['content']?$row['content']:false,
				'colspan'					=> $row['content'] ==1 ? 'colspan="8" class="last"' : '',
				'article_id'				=> $row['article_id'],
				'is_article'      	      	=> $row['article_id'] ? true : false,
				'tax_id'				    => $row['tax_id'],
				'tax_for_article_id'	    => $row['is_tax']==1?$row['tax_for_article_id']:'',
				'is_tax'      	            => $row['is_tax'],
				'quantity_component'      	=> display_number($row['quantity_component'],$nr_dec_q),
				'quantity_old'      	    => display_number($row['quantity_old'],$nr_dec_q),
				'quantity'      			=> display_number($row['quantity'],$nr_dec_q),
				'price_vat'      			=> display_number_var_dec($row['price_vat']),
				'price'         			=> display_number_var_dec($row['price']),
				'purchase_price'         	=> $row['purchase_price'],
				'purchase_price_other_currency'         	=> $row['purchase_price_other_currency'],
				'purchase_price_other_currency_id'         	=> $row['purchase_price_other_currency_id'],
				'percent'					=> $row['percent'],
				'vat_value'					=> $row['vat_value'],
				'percent_x'					=> display_number($row['percent_x']),
				'vat_value_x'			    => display_number($row['vat_value']),
				'sale_unit_x'				=> $row['sale_unit'],
				'packing_x'					=> remove_zero_decimals($row['packing']),
				'sale_unit'				    => $row['sale_unit'],
				'packing'					=> remove_zero_decimals($row['packing']),
				'stock'				    	=> $stock,
				'pending_articles'          => intval($pending_articles),
				'threshold_value'           => intval($threshold_value),
				'hide_stock'           		=>  $hide_stock,
				'disc'				        => $in['changePrices']=='1' ? $o['disc_line'] : display_number($row['disc']),
				'line_total'				=> display_number($line_total),
				'td_width'					=> ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px',
				'input_width'				=> ALLOW_ARTICLE_PACKING ? 'width:164px' : 'width:223px',
				'article_code'				=> $row['article_code'],
				'is_article_code'			=> $row['article_code'] ? true : false,
				'colum'						=> $colum,
				'for_article'				=> $row['is_tax']==1? $last_tr :'',
				'has_variants' 				=> $row['has_variants'],
			 	'has_variants_done' 		=> $row['has_variants_done'],
			 	'is_variant_for' 			=> $row['is_variant_for'],
			 	'is_variant_for_line' 		=> $row['is_variant_for']? $last_variant_parent:'',
			 	'variant_type' 				=> $row['variant_type'],  
			 	'is_combined' 				=> $row['is_combined'],
			 	'component_for' 			=> $row['component_for']? $last_combined:'',
			 	'visible' 					=> $row['visible'],  
			 	'has_variants' 				=> $row['has_taxes'],
			 	'show_stock_warning_line' 	=> $show_stock_warning_line,
				);
				// $row['colum'] = $o['colum'];
				// $linie = $row;

				array_push($o['tr_id'], $linie);
				$i++;
			}
		}

	}

	$t=0;
	//$o['hide_total'] = true;
	foreach ($vat_percent as $key => $val){

		if($sub_t[$key] - $sub_discount[$key]){
			 		
	 		// $val = $sub_t[$key]*return_value($key)/100;
			//$total_vat += $val;
			$vat_line=array(
				'vat_percent'   			=> display_number($key),
				'vat_value'	   			=> display_number(round($val,ARTICLE_PRICE_COMMA_DIGITS)),
				'subtotal'				=> display_number(round($sub_t[$key],ARTICLE_PRICE_COMMA_DIGITS)),
				'total_novat'	   	 	=> display_number(round($sub_t[$key],ARTICLE_PRICE_COMMA_DIGITS)),
				'discount_value'			=> display_number($sub_discount[$key]),
				'is_ord'				=> $is_ord,
				'view_discount'			=> ($discount || $is_ord) ? true : false,
				'net_amount'			=> display_number($sub_t[$key] - $sub_discount[$key]),
			);
			array_push($o['vat_line'], $vat_line);
		}
		$t++;
	} 	

	$total_with_vat=$vat_total+$total-$discount_total;
	if($in['remove_vat']){
		$total_with_vat = $total-$discount_total;
	}
	if($discount_total){
		$show_discount_total = true;
	}
	$total_currency = $total_with_vat * return_value($o['currency_rate']);
	// $view->assign(array(
	$o['discount_total']        					= display_number($discount_total);
	$o['vat_total']									= display_number($vat_total);
	$o['total']				    					= display_number($total);
	$o['total_vat']									= display_number($total_with_vat);
	$o['total_default_currency']							= display_number($total_currency);
	$o['allow_article_packing']     				= ALLOW_ARTICLE_PACKING == 1 ? true : false;
  	$o['allow_article_sale_unit']   				= ALLOW_ARTICLE_SALE_UNIT == 1 ? true : false;
	$o['hide_add']				 					= 'hide';
	//$o['view_delivery']          					= trim($in['delivery_address']) ? true : false;
	/*$o['th_width']									= ALLOW_ARTICLE_PACKING ? 'width:208px' : 'width:267px';
	$o['td_width']									= ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px';
	$o['input_width']								= ALLOW_ARTICLE_PACKING ? 'width:164px' : 'width:223px';*/
	$o['shipping_price']							= display_number($shipping_price);
	$o['is_delivery_cost']							= $shipping_price > 0 ? true : false;
	$o['order_id'] 								= $in['order_id'];

	$customer_notes="";
	if($in['buyer_id'] && is_numeric($in['buyer_id'])){
		$buyer_credit_limit_info=$db2->query("SELECT credit_limit,currency_id,customer_notes FROM customers WHERE customer_id='".$in['buyer_id']."' ");
		if($buyer_credit_limit_info->f('credit_limit')){
			$invoices_customer_due=customer_credit_limit($in['buyer_id']);
			if($invoices_customer_due && $invoices_customer_due>$buyer_credit_limit_info->f('credit_limit')){
				$buyer_currency_id=$buyer_credit_limit_info->f('currency_id') ? $buyer_credit_limit_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
				$o['customer_credit_limit']=place_currency(display_number($buyer_credit_limit_info->f('credit_limit')),get_commission_type_list($buyer_currency_id));
			}
		}
		$customer_notes=stripslashes($buyer_credit_limit_info->f('customer_notes'));	
	}
	$o['customer_notes']=$customer_notes;
	// ));

}else {
	//EDIT
	$page_title = gm('Edit Order');

 	$do_next = 'order-norder-order-update_order';
	if($in['duplicate_order_id']){
	
		if(!$in['is_duplicate'] || $in['changePrices']){
			$in['order_id'] = $in['duplicate_order_id'];
		};
		
		$o['duplicate_order_id']=$in['duplicate_order_id'];
		$page_title = gm('Duplicate Order');
		$do_next = 'order-norder-order-add_order';
		$type_title = gm('Draft');
		$nr_status = 1;
	}
	$o['order_id'] = $in['order_id'];
  	$order_query = $db->query("SELECT pim_orders.* ,customer_contacts.firstname,customer_contacts.lastname, customer_contacts.customer_id as cust_id
	            FROM pim_orders
	            LEFT JOIN customer_contacts ON customer_contacts.contact_id=pim_orders.customer_id
	            WHERE pim_orders.order_id='".$in['order_id']."'
	           ");
  	$cancel_link = "index.php?do=order-order&order_id=".$in['order_id'];
  	if($order_query->f('shipping_price')){
		$shipping_price = $order_query->f('shipping_price');
	}else{
		$shipping_price = 0;
	}

	if(!$order_query->move_next()){
		$o = array('redirect'=>'orders');
		json_out($o);
		// return ark::run('order-orders');
	}
	if(!getItemPerm(array('module'=>'order','item'=>$order_query->f('author_id')))){
		$o = array('redirect'=>'orders');
		json_out($o);
		// return ark::run('order-orders');
	}

	$in['vat_regime_id'] = $in['vat_regime_id'] ? $in['vat_regime_id'] : $order_query->f('vat_regime_id');
	if($in['vat_regime_id'] && $in['vat_regime_id']<10000){
		     $in['vat_regime_id']=$db->field("SELECT id FROM vat_new WHERE vat_regime_id='".$in['vat_regime_id']."' ");
	}
	if($in['vat_regime_id']){
		$in['remove_vat']=$db->field("SELECT no_vat FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
	}

	if($order_query->f('field') == 'customer_id'){
		$customer_id = $order_query->f('customer_id');
		$o['is_company']=true;
		$o['is_buyer']=true;
		$in['buyer_id']      	= $in['buyer_id'] && is_numeric($in['buyer_id']) ? $in['buyer_id'] : $order_query->f('customer_id');
		$o['buyer_id'] = $in['buyer_id'];
	}else{
		$o['is_company']=false;
		$o['is_buyer']=false;
	}

	if ($order_query->f('customer_id'))
	{
		$price_category_id = $db2->field('SELECT cat_id FROM customers WHERE customer_id ='.($in['buyer_id'] && is_numeric($in['buyer_id']) ? $in['buyer_id'] : $order_query->f('customer_id')) );
	}
	else
	{
		$price_category_id =0;
	}

	$ref='';
	if(ACCOUNT_ORDER_REF && $order_query->gf('buyer_ref')){
		$ref = $order_query->gf('buyer_ref');
	}

	if($order_query->f('use_package')){
		$in['allow_article_packing']=1;
	}else{
		$in['allow_article_packing']=0;
		$in['allow_article_sale_unit']=0;
	}
	if($order_query->f('use_sale_unit')){
	   $in['allow_article_sale_unit']=1;
	}else{
	   $in['allow_article_sale_unit']=0;
	}
	$o['show_discount']					= $order_query->gf('apply_discount');
	$o['main_address_id']      				= $order_query->gf('main_address_id');
	$in['contact_id']      				= $order_query->gf('contact_id');
	$o['contact_id']      				= $order_query->gf('contact_id');
  	$in['delivery_id']      			= $order_query->gf('delivery_id');
  	$o['delivery_id']      				= $order_query->gf('delivery_id');
	$in['contact_name']      			= $order_query->gf('contact_name');
	$in['customer_id']      			= $in['buyer_id'] && is_numeric($in['buyer_id']) ? $in['buyer_id'] : $order_query->gf('customer_id');
	$o['customer_id']      				= $in['buyer_id'] && is_numeric($in['buyer_id']) ? $in['buyer_id'] : $order_query->gf('customer_id');
	//$in['price_category_id']  			= $price_category_id;
	//$o['price_category_id']   			= $price_category_id;
	$in['price_category_id']			= $order_query->f('cat_id');
	$o['price_category_id']				= $order_query->f('cat_id');
	$in['s_customer_name']  			= $order_query->f('firstname')." ".$order_query->f('lastname');
	$o['s_customer_name']  				= $order_query->f('firstname')." ".$order_query->f('lastname');
	$in['serial_number']    			= $order_query->gf('serial_number');

	$in['payment_term']     			= $order_query->gf('payment_term');
	$o['payment_term']     				= $order_query->gf('payment_term');
	$in['date']             			= date(ACCOUNT_DATE_FORMAT,$order_query->f('date'));
	$o['date']             				= date(ACCOUNT_DATE_FORMAT,$order_query->f('date'));
	$in['date_h']           			= $order_query->f('date');
	$o['date_h']           				= $order_query->f('date')*1000;
	$in['delivery_date']        		= $order_query->f('del_date') ? date(ACCOUNT_DATE_FORMAT,$order_query->f('del_date')) : "";
	$o['delivery_date']        			= $order_query->f('del_date') ? date(ACCOUNT_DATE_FORMAT,$order_query->f('del_date')) : "";
	$in['del_date']           			= $order_query->f('del_date') ? $order_query->f('del_date') : '';
	$o['del_date']           			= $order_query->f('del_date') ? $order_query->f('del_date')*1000 : '';
	$in['del_note']           			= $order_query->f('del_note');
	$o['del_note']           			= $order_query->f('del_note');
	$in['field']	           			= $order_query->f('field');
	$o['field']	           				= $order_query->f('field');
	$o['subject']						= stripslashes($order_query->f('subject'));

	if($in['buyer_changed'] || $in['buyer_changed_save']){
		//	
	}else{
		$in['discount']	           			= display_number($order_query->f('discount'));
	}
	
	$o['discount']	           			= $in['discount'];
	$in['currency_name']    			= build_currency_name_list($order_query->f('currency_type'));
	$o['currency_name']    				= build_currency_name_list($order_query->f('currency_type'));
	$in['buyer_ref']					= $order_query->gf('buyer_ref');
	$o['buyer_ref']						= $order_query->gf('buyer_ref');
	$in['currency_rate']    			= $order_query->f('currency_rate');
	$o['currency_rate']    				= $order_query->f('currency_rate');
	$o['currency_type_val']				= $in['change_currency'] ? $in['currency_type'] : $order_query->f('currency_type');
	// $in['notes']		        = $order_query->f('notes');
	$in['our_ref']								= $order_query->f('our_ref');
	$o['our_ref']									= $order_query->f('our_ref');
	$in['your_ref']								= $order_query->f('your_ref');
	$o['your_ref']								= $order_query->f('your_ref');
	//$in['remove_vat']							= $order_query->f('remove_vat');
	//$o['remove_vat']							= $order_query->f('remove_vat');
	$o['remove_vat']							= $in['remove_vat'];
	$in['languages']            	= $order_query->f('email_language');
	$o['languages']            		= $order_query->f('email_language');

	if($in['buyer_changed'] || $in['buyer_changed_save']){
		$o['apply_discount']=$in['apply_discount'];
		$o['discount_line_gen']			=	 display_number($in['discount_line_gen']);
	}else{
		$in['apply_discount']					= $order_query->f('apply_discount');
		$o['apply_discount']					= $order_query->f('apply_discount');
		$in['discount_line_gen']			= display_number($order_query->gf('discount_line_gen'));
		$o['discount_line_gen']			=	 display_number($order_query->gf('discount_line_gen'));
	}
	
	if($in['duplicate_order_id']){
		$in['serial_number'] = generate_order_serial_number(DATABASE_NAME);
	}
	$o['serial_number']    				= $order_query->gf('serial_number');

	$existIdentity = $db2->field("SELECT COUNT(identity_id) FROM multiple_identity ");

	$delivery_addr = $db2->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND delivery=1 LIMIT 1 ");
		if($delivery_addr){
			$delivery_address = $delivery_addr->f('address')."\n".$delivery_addr->f('zip').'  '.$delivery_addr->f('city')."\n".get_country_name($delivery_addr->f('country_id'));
			$delivery_address_id =$delivery_addr->f('address_id');
		}

	$in['vat']=$db2->field("SELECT vats.value FROM vat_new 
									LEFT JOIN vats ON vat_new.vat_id=vats.vat_id
									WHERE vat_new.id='".$in['vat_regime_id']."' ");

	// $view->assign(array(
	$o['form_mode']             				= 1; //0-add 1-edi;
	$o['customer_vat_dd']       				= build_vat_dd($order_query->f('customer_vat_id'));
	$o['payment_type_dd']       				= build_payment_type_dd($order_query->f('payment_type'));
	$o['payment_type_txt']       				= build_payment_type_dd($order_query->f('payment_type'),true);
	$o['payment_type']							= $order_query->f('payment_type');
	$o['hide_currency2']						= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
	$o['hide_currency1']						= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
	//$o['currency_type']							= get_commission_type_list($order_query->f('currency_type'));#ACCOUNT_CURRENCY_TYPE == 1 ? '&euro;':'$';
	$o['default_currency_val']  				= get_commission_type_list(ACCOUNT_CURRENCY_TYPE);
	$o['checked']								= $order_query->f('rdy_invoice') ? 'checked="checked"':'';

	$o['customer_name']							= $in['buyer_id'] && is_numeric($in['buyer_id']) ? get_customer_name($in['buyer_id']) : $order_query->f('customer_name');
	$o['customer_email']     					= $order_query->f('customer_email');
	$o['contact_name']							= $in['contact_id'] ? $db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$in['contact_id']}' ") : $order_query->f('contact_name');
	$o['customer_city']     					= $order_query->f('customer_city');
	$o['customer_zip']      					= $order_query->f('customer_zip');
	$o['customer_address']  					= $order_query->f('customer_address');
	$o['customer_phone']    					= $order_query->f('customer_phone');
	$o['customer_ref']    						= $order_query->f('customer_ref');
	$o['customer_cell']     	 				= $order_query->f('customer_cell');
	$o['buyer_country']								= get_country_name($order_query->f('customer_country_id'));
	$o['customer_country_id']       	= $order_query->f('customer_country_id');
	$o['delivery_address_txt']     		= $order_query->f('delivery_address')? nl2br($order_query->f('delivery_address')):nl2br($delivery_address);
	$o['delivery_address']     				= $order_query->f('delivery_address')? $order_query->f('delivery_address'):$delivery_address;
	
	//$o['view_delivery']             	= trim($order_query->gf('delivery_address')) ? true : false;
	$o['customer_vat_number']					= $order_query->gf('customer_vat_number');
	$o['name_txt']										= 'Name:';
	$o['ref_serial_number']						= $ref.$order_query->gf('serial_number');
	$o['ref_var']											= $order_query->gf('buyer_ref') ? " var ref= '".$order_query->gf('buyer_ref')."';" : " var ref;";
	$o['discount_percent']         		= '( '.$in['discount'].'% )';
	// 'sh_discount'		        => ($order_query->f('discount')==0.00)?'hide':'',
	$o['allow_article_packing']     	= $order_query->f('use_package') == 1 ? true :false ;
	$o['allow_article_sale_unit']   	= $order_query->f('use_sale_unit') == 1 ? true : false;
	$o['is_vat']											= $in['remove_vat'] == 1 ? false : true;
	$o['show_vat_2']									= $in['remove_vat'] == 1 ? 'hide' : '';
	$o['show_vat']										= $order_query->gf('show_vat') == 1 ? true : false;
	$o['show_vat_checked']          	= $order_query->gf('show_vat') == 1 ? 'CHECKED' : '';
	$o['show_vat_txt']								= $order_query->gf('show_vat') == 1 ? gm('Yes') : gm('No');

	$o['gender_dd']					= gender_dd();
	$o['title_dd']					= build_contact_title_type_dd(0);
	$o['language_dd'] 				= build_language_dd_new($in['email_language']);

	$o['currency_type']		   	= $in['change_currency'] ? $in['currency_type'] : ($order_query->f('currency_type') ? $order_query->f('currency_type') : ACCOUNT_CURRENCY_TYPE);
	$o['currency_val']		   	= get_commission_type_list($o['currency_type']);
	$o['currency_type_list']    		= build_currency_list($o['currency_type']);
	$o['CURRENCY_TXT']		    		= currency::get_currency($o['currency_type'],'name');
	$o['currency_txt']		        	= $in['change_currency']? currency::get_currency($in['currency_type']) : currency::get_currency($order_query->f('currency_type'));
	$o['vat_regim_dd']			= build_vat_regime_dd();
	//$o['vat_regime_id']			= $order_query->f('vat_regime_id') ? $order_query->f('vat_regime_id') : '';
	$o['vat_regime_id']			= $in['vat_regime_id'] ? $in['vat_regime_id'] : '';
	$o['vat'] 					= $in['vat'];
	$o['can_change']			= $to_edit;
	$o['generalvats'			]=build_vat_dd();
	$o['source_dd']					= get_categorisation_source();
	$o['source_id']					= $order_query->f('source_id');
	$o['type_dd']					= get_categorisation_type();
	$o['type_id']					=  $order_query->f('type_id');
	$o['segment_dd']				= get_categorisation_segment();
	$o['segment_id']				=$order_query->f('segment_id');
	$o['deals']						= get_deals($in);
	$o['ADV_CRM'] 					= defined('ADV_CRM') && ADV_CRM == 1 ? true : false;
	$o['ACCESS_BARCODE_SCAN'] 		= defined('ACCESS_BARCODE_SCAN') && ACCESS_BARCODE_SCAN == 1 ? true : false;
	$o['is_advanced_subscription'] 	= get_subscription_plan($_SESSION['u_id'])== '5' ? true : false;
	$o['show_barcode_scan'] = $o['ACCESS_BARCODE_SCAN'] || $o['is_advanced_subscription'] || (DATABASE_NAME=='salesassist_2' || DATABASE_NAME=='Salesassist_11') ? true : false;

	$o['price_category_dd']			= get_price_categories();
	$o['cat_id']					= $order_query->f('cat_id');

	$has_admin_rights 				= getHasAdminRights(array('module'=>'order'));
	$o['hide_margin']				= defined('HIDE_PROFIT_MARGIN') && HIDE_PROFIT_MARGIN == 1 && !$has_admin_rights ? true : false; 

	$customer_notes="";
	if($in['buyer_id']){
		$buyer_credit_limit_info=$db2->query("SELECT credit_limit,currency_id,customer_notes FROM customers WHERE customer_id='".$in['buyer_id']."' ");
		if($buyer_credit_limit_info->f('credit_limit')){
			$invoices_customer_due=customer_credit_limit($in['buyer_id']);
			if($invoices_customer_due && $invoices_customer_due>$buyer_credit_limit_info->f('credit_limit')){
				$buyer_currency_id=$buyer_credit_limit_info->f('currency_id') ? $buyer_credit_limit_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
				$o['customer_credit_limit']=place_currency(display_number($buyer_credit_limit_info->f('credit_limit')),get_commission_type_list($buyer_currency_id));
			}
		}
		$customer_notes=stripslashes($buyer_credit_limit_info->f('customer_notes'));	
	}
	$o['customer_notes']=$customer_notes;

	if($order_query->f('trace_id')){
		$linked_doc=$db->field("SELECT tracking_line.origin_id FROM tracking_line
			INNER JOIN tracking ON tracking_line.trace_id=tracking.trace_id
				WHERE tracking_line.origin_type='11' AND tracking_line.trace_id='".$order_query->f('trace_id')."' ");
		if($linked_doc){
			$o['deal_id']=$linked_doc;	
		}
	}

	if($in['vat_regime_id'] && $in['vat_regime_id']>=1000){
		$extra_eu=$db->field("SELECT regime_type FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
		if($extra_eu==2){
			$o['block_vat']=true;
		}
		if($extra_eu=='1'){
			$o['vat_regular']=true;
		}
		$contracting_partner=$db->field("SELECT vat_regime_id FROM vat_new WHERE id='".$in['vat_regime_id']."' ");
		if($contracting_partner==4){
			$o['block_vat']=true;
		}
	}

	$separator = $db->field("SELECT value from settings where constant_name='ACCOUNT_NUMBER_FORMAT'");
	$currency = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
	$o['currency_rate'] = currency::getCurrency(currency::get_currency($o['currency_type'],'code'),$currency, 1,$separator);

	// $o['address_info']          			= nl2br($order_query->gf('address_info'));
	 /*$address_info = $order_query->f('customer_address').'
		'.$order_query->f('customer_zip').' '.$order_query->f('customer_city').'
		'.get_country_name($order_query->f('customer_country_id'));*/
	if($order_query->f('delivery_address')){
		$address_info =$order_query->f('delivery_address');
	}else{
		$address_info =$order_query->f('address_info');
	}
	
	if($in['contact_id'] && $in['change']){
		$contact_title = $db->field("SELECT customer_contact_title.name FROM customer_contact_title
								INNER JOIN customer_contacts ON customer_contacts.title=customer_contact_title.id
								WHERE customer_contacts.contact_id='".$in['contact_id']."'");
		if($contact_title)
		{
			$contact_title .= " ";
		}
		$buyer = $db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' ORDER BY is_primary DESC");
		$buyer->next();
		$address_info  = $buyer->f('address')."\n".$buyer->f('zip').'  '.$buyer->f('city')."\n".get_country_name($buyer->f('country_id'));
		
    }

	if( $in['delivery_address_id']){

		$buyer = $db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND address_id='".$in['delivery_address_id']."'");
		$buyer->next();
		$address_info  = $buyer->f('address')."\n".$buyer->f('zip').'  '.$buyer->f('city')."\n".get_country_name($buyer->f('country_id'));

		
    }

    if( $in['address_id']){
		$buyer = $db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND address_id='".$in['address_id']."'");
		$buyer->next();
		$address_info  = $buyer->f('address')."\n".$buyer->f('zip').'  '.$buyer->f('city')."\n".get_country_name($buyer->f('country_id'));
	
    }

    if( $in['sameAddress']){
		$buyer = $db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND address_id='".$in['main_address_id']."'");
		$buyer->next();
		$address_info  = $buyer->f('address')."\n".$buyer->f('zip').'  '.$buyer->f('city')."\n".get_country_name($buyer->f('country_id'));
	
    }
    	if($in['buyer_changed'] || $in['buyer_changed_save']){
    		if($in['apply_discount']==0){
				$discount_line=false;
				$discount_global=false;
			}elseif($in['apply_discount']==1){
				$discount_line=true;
				$discount_global=false;
			}elseif($in['apply_discount']==2){
				$discount_line=false;
				$discount_global=true;
			}elseif($in['apply_discount']==3){
				$discount_line=true;
				$discount_global=true;
			}
    	}else{
    		if($order_query->f('apply_discount')==0){
				$discount_line=false;
				$discount_global=false;
			}elseif($order_query->f('apply_discount')==1){
				$discount_line=true;
				$discount_global=false;
			}elseif($order_query->f('apply_discount')==2){
				$discount_line=false;
				$discount_global=true;
			}elseif($order_query->f('apply_discount')==3){
				$discount_line=true;
				$discount_global=true;
			}
    	}

		/*if($order_query->f('apply_discount')==0){
			$discount_line=0;
			$discount_global=0;
		}elseif($order_query->f('apply_discount')==1){
			$discount_line=1;
			$discount_global=0;
		}elseif($order_query->f('apply_discount')==2){
			$discount_line=0;
			$discount_global=1;
		}elseif($order_query->f('apply_discount')==3){
			$discount_line=1;
			$discount_global=1;
		}
   		$customer_disc = $db->query("SELECT apply_fix_disc FROM customers WHERE customer_id='".$in['buyer_id']."' ");
		if($customer_disc->f('apply_fix_disc')==1){
			$discount_global = 1;
		}*/
		//var_dump($discount_global);exit;

	$o['address_info_txt']						= nl2br($address_info); 
	$o['address_info']							= trim($order_query->f('address_info')) ? nl2br($order_query->f('address_info')) : nl2br($address_info);

	$o['apply_discount_line']      						= $discount_line;
	$o['apply_discount_global']     					= $discount_global;
	$o['th_width']										= $in['allow_article_sale_unit'] ? 'width:208px' : 'width:267px';
	$o['td_width']										= $in['allow_article_sale_unit'] ? 'width:218px' : 'width:277px';
	$o['input_width']									= $in['allow_article_sale_unit'] ? 'width:164px' : 'width:223px';
	$o['author_id']										= $order_query->gf('author_id') ? $order_query->gf('author_id') : '';
	$o['acc_manager']									= $order_query->gf('author_id') ? get_user_name($order_query->gf('author_id')) : '';
	$o['APPLY_DISCOUNT_LIST']       					= build_apply_discount_list($in['apply_discount']);
	$o['APPLY_DISCOUNT_LIST_TXT']     					= build_apply_discount_list($in['apply_discount'],true);
	$o['DISCOUNT']              						= display_number($in['discount']);
	$o['hide_disc']										= $in['apply_discount'] ==0 || $in['apply_discount'] == 2 ? false : true;
	$o['hide_global_discount']							= $in['apply_discount'] < 2 ? false : true;
	$o['hide_line_global_discount']						= $in['apply_discount'] == 1 ? '' : 'hide';
	$o['total_currency_hide']							= $in['currency_rate'] ? true : false;
	$o['disc_line']										= display_number($order_query->f('discount_line_gen'));
	$o['is_duplicate']			 						= $in['duplicate_order_id']? true: false;
	$o['acc_manager_id']     							= $order_query->gf('acc_manager_id');
	$o['acc_manager_name']   							= $order_query->gf('acc_manager_name');

	$o['identity_id'] 									= $order_query->gf('identity_id') ? $order_query->gf('identity_id') : get_user_customer_identity($_SESSION['u_id'], $order_query->gf('customer_id') );
	$o['multiple_identity_txt'] 						= $o['identity_id'] ? $db2->field("SELECT identity_name from multiple_identity WHERE identity_id='".$o['identity_id']."'") :'';
	$o['multiple_identity_dd'] 							= build_identity_dd($o['identity_id']);
	$o['existIdentity']									= $existIdentity >0 ? true : false;

	$yuki_active = $db->field("SELECT active FROM apps WHERE name='Yuki' AND app_type='accountancy' AND main_app_id=0 AND type='main' ");
	$o['yuki_project_enabled']				= defined('YUKI_PROJECTS') && YUKI_PROJECTS == 1 && $yuki_active ? true : false;
	$o['yuki_projects']						= get_yuki_projects();
	$o['yuki_project_id']					= $order_query->gf('yuki_project_id');
  	
  	
  	$o['sameAddress']			= !$order_query->f('same_address') ? true : false;
  	$in['delivery_address_id']							= $order_query->gf('delivery_address_id');
  	//$o['delivery_address_id']							= $order_query->gf('delivery_address_id');
  	$o['delivery_address_id']				= $in['delivery_address_id']? $in['delivery_address_id']:$delivery_address_id;

  	if($o['identity_id'] != '0'){
		$mm = $db->query("SELECT * FROM multiple_identity WHERE identity_id='".$o['identity_id']."'")->getAll();
		$value = $mm[0];
		$template = $db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$o['identity_id']."' and module='order'");
		
		$o['main_comp_info']=array(
				'name'		=> $value['identity_name'],
				'address'	=> nl2br($value['company_address']),
				'zip'		=> $value['company_zip'],
				'city'		=> $value['city_name'],
				'country'	=> get_country_name($value['country_id']),
				'phone'		=> $value['company_phone'],
				'fax'		=> $value['company_fax'],
				'email'		=> $value['company_email'],
				'url'		=> $value['company_url'],
				'logo'		=> $value['company_logo'],
				);
		
	}

	// ));
	$use_package = $order_query->f('use_package');

	$show_shipping_price = $order_query->gf('show_shipping_price');

	$i=0;
	$discount_total = 0;
	$show_discount_total = false;
	$vat_value = 0;
	$global_disc = $in['discount'];
	$old_currency_type = $in['currency_type_val'];

	// $use_package 			= $order_query->f('use_package');

	$in['remove_vat'] ? $vat_column = 0 : $vat_column = 1 ;
	$colspan_code = $vat_column.'-'.$use_package.'-'.$in['apply_discount'];
	switch ($colspan_code) {
		case '0-0-0':
		case '0-0-2':
		$cols = 6;
		$col = 6;
			break;
		case '1-0-0':
		case '0-0-1':
		case '0-0-3':
		case '1-0-2':
		case '0-1-0':
		case '0-1-2':
			$cols = 7;
			$col = 5;
			break;
		case '1-0-1':
		case '1-0-3':
		case '0-1-1':
		case '0-1-3':
		case '1-1-0':
		case '1-1-2':
			$cols = 8;
			$col = 4;
			break;
		case '1-1-1':
		case '1-1-3':
			$cols = 9;
			$col = 3;
			break;
	}

	//console::log($col);

	$o['show_articles_pdf']         = defined('SHOW_ARTICLES_PDF') && SHOW_ARTICLES_PDF=='1' ? true : false;
	/*if($o['show_articles_pdf']){
		$col = $col-1;
	}*/
	$o['colum'] = $col;


	$o['tr_id']=array();
	$vat_percent = array();
	$sub_t = array();
	$sub_discount = array();
	$o['vat_line']  = array();

	if($in['buyer_changed']){
        $o['buyer_changed']=$in['buyer_changed'];
    }
    if($in['buyer_changed_save']){
        $o['buyer_changed_save']=$in['buyer_changed_save'];
    }

	$last_tr = '';
	$last_combined = '';
	$last_variant_parent ='';
	$db->query("SELECT pim_order_articles.* FROM pim_order_articles WHERE order_id='".$in['order_id']."' ORDER BY sort_order ASC");
	while ($db->move_next())
	{
		if($in['duplicate_order_id']){

			if($in['change']){
				$in['buyer_id'] = $in['customer_id'];
				$customer_id = $in['buyer_id'];
			}else if($in['buyer_changed'] || $in['buyer_changed_save']){
				$customer_id = $in['customer_id'];
			}
			// echo $db->f('article_id').' '.$customer_id.' '.$contact_id;
			$contact_id = $in['contact_id'];


			$article_data = get_customer_article_data($db->f('article_id'),$customer_id,$contact_id,$old_currency_type);
			
			//console::log($article_data);
			if($in['change'] || $in['buyer_changed'] || $in['buyer_changed_save']){
				$in['languages']			= $article_data['languages'];
			} // requested by akti-4271		
			$in['currency_rate']    	= $article_data['currency_rate'];
			// $in['currency_type']		= $article_data['currency_type'];
			$in['remove_vat'] 		= $article_data['remove_vat'];

			if($article_data['global_discount']){
				$global_disc = $article_data['global_discount'];	
			}
			
			if($article_data['line_discount']){
				$discount_line = $article_data['line_discount'];
			}else{
				$discount_line = $db->f('discount');
			}

			if(!$show_discount_total && $discount_line){
				$show_discount_total = true;
			}
			if($article_data['apply_discount']){
				$in['apply_discount'] = $article_data['apply_discount'];
			}
			
			if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
				$discount_line = 0;
			}
			if($in['apply_discount'] < 2){
				$global_disc = 0;
			}

			//$in['discount_line_gen'] = display_number($discount_line);
			//$in['discount'] = display_number($global_disc);

			/*$q = $db->f('quantity') * $article_data['packing'] / $article_data['sale_unit'];
			$row_id = 'tmp'.$i;
			$price_line = $article_data['price'] - ($article_data['price'] * $discount_line /100);
			$discount_total += $price_line * $global_disc / 100 * $q;

			$vat_total += ($price_line - $price_line * $global_disc /100) *$article_data['vat_value'] / 100 * $q;
			$line_total=$price_line * $q;

			$vat_value_line =($price_line - $price_line * $global_disc /100) *$article_data['vat_value'] / 100 * $q;
			$vat_percent[$article_data['vat_value']] += round($vat_value_line,2);

			$sub_t[$article_data['vat_value']] += round( $line_total,ARTICLE_PRICE_COMMA_DIGITS );
			$sub_discount[$article_data['vat_value']] += ($line_total * $global_disc /100);
			// echo $price_line.'<br>';
			$total += $line_total;*/

			$q = $db->f('packing') && $db->f('sale_unit') ? @($db->f('quantity') * $db->f('packing') / $db->f('sale_unit')) : 0;
			$row_id = 'tmp'.$i;
			//$price_line = $db->f('price') - ($db->f('price') * $discount_line /100);
			$v_percent=$db->f('vat_percent');
			$row_price=$db->f('price');
			$row_price_w_vat=$db->f('price_with_vat');
			if($in['changePrices']=='1'){
				if($db->f('article_id')){
					$b_category_id = $db2->field('SELECT cat_id FROM customers WHERE customer_id ='.$in['buyer_id']);
					require_once(__DIR__.'/../model/order.php');
					$order_obj=new order();
					$row_price=$order_obj->getArticlePrice(array(
							'article_id' => $db->f('article_id'), 
							'price' => $db->f('price'), 
							'quantity' => $db->f('quantity'), 
							'customer_id' => $in['buyer_id'],
							'cat_id' => $b_category_id,
							'asString' => true
						));
					$price_line = $row_price - ($row_price * $discount_line /100);

					$article_vat=$db2->field("SELECT vat_id FROM pim_articles WHERE article_id='".$db->f('article_id')."' ");
		            $vat = $db2->field("SELECT value FROM vats WHERE vat_id='".$article_vat."'");

		            if($in['vat_regime_id']){
		                if($in['vat_regime_id']<10000){
		                  if($in['vat_regime_id']==2){
		                    $vat=0;
		                  }
		                }else{
		                  $vat_regime=$db2->field("SELECT vats.value FROM vat_new 
		                    LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
		                    WHERE vat_new.id='".$in['vat_regime_id']."'");
		                  if(!$vat_regime){
		                    $vat_regime=0;
		                  }
		                  if($vat>$vat_regime){
		                    $vat=$vat_regime;
		                  }
		                }     
		            }    
		            $v_percent=$in['remove_vat'] == 1 ? 0: $vat;
		            $row_price_w_vat=$row_price+($row_price*$v_percent/100);
				}else{
					$price_line = $db->f('price') - ($db->f('price') * $discount_line /100);
					if($in['vat_regime_id']){
		              if($in['vat_regime_id']<10000){
		                if($in['vat_regime_id']==2){
		                  $vat=0;
		                }
		              }else{
		                $vat_regime=$db2->field("SELECT vats.value FROM vat_new 
		                  LEFT JOIN vats ON vat_new.vat_id=vats.vat_id 
		                  WHERE vat_new.id='".$in['vat_regime_id']."'");
		                if(!$vat_regime){
		                  $vat_regime=0;
		                }
		                if($vat>$vat_regime){
		                  $vat=$vat_regime;
		                }
		              }     
		            }
		            $v_percent=$in['remove_vat'] == 1 ? 0: $vat;
				}			
			}else if($in['tr_id'] && !empty($in['tr_id']) && is_array($in['tr_id']) && array_key_exists($i, $in['tr_id']) !== false){
				$v_percent=$in['tr_id'][$i]['percent'];
				$row_price=return_value($in['tr_id'][$i]['price']);
				$row_price_w_vat=return_value($in['tr_id'][$i]['price_vat']);
				$price_line = $row_price - ($row_price * $discount_line /100);
			}else{
				$price_line = $db->f('price') - ($db->f('price') * $discount_line /100);
			}

			$discount_total += $price_line * $global_disc / 100 * $q;

			$vat_total += ($price_line - $price_line * $global_disc /100) *$v_percent / 100 * $q;
			$line_total=$price_line * $q;

			$vat_value_line =($price_line - $price_line * $global_disc /100) *$v_percent / 100 * $q;
			$vat_percent[$v_percent] += round($vat_value_line,2);

			$sub_t[$v_percent] += round( $line_total,ARTICLE_PRICE_COMMA_DIGITS );
			$sub_discount[$v_percent] += ($line_total * $global_disc /100);
			$total += $line_total;

		}else{
			$v_percent=$in['remove_vat'] == 1 ? 0: $db->f('vat_percent');
			$row_price=$db->f('price');
			$row_price_w_vat=$db->f('price_with_vat');
			if(!$show_discount_total && $db->f('discount') !=0){
				$show_discount_total = true;
			}
			$discount_line = $in['changePrices']=='1' ? $order_query->f('discount_line_gen') : $db->f('discount');
			if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
				$discount_line = 0;
			}
			if($in['apply_discount'] < 2){
				$global_disc = 0;
			}
			$q = $db->f('packing') && $db->f('sale_unit') ? @($db->f('quantity') * $db->f('packing') / $db->f('sale_unit')) : 0;
			$row_id = 'tmp'.$i;
			$price_line = $db->f('price') - ($db->f('price') * $discount_line /100);

			$discount_total += $price_line * $global_disc / 100 * $q;

			$vat_total += ($price_line - $price_line * $global_disc /100) *$v_percent / 100 * $q;
			$line_total=$price_line * $q;

			$vat_value_line =($price_line - $price_line * $global_disc /100) *$v_percent / 100 * $q;
			$vat_percent[$v_percent] += $vat_value_line;

			$sub_t[$v_percent] += $line_total;
			$sub_discount[$v_percent] += ($line_total * $global_disc /100);
			$total += $line_total;
		}

		$show_stock_warning_line = false;
		if($db->f('article_id')){
			$art = $db2->query("SELECT stock, article_threshold_value, hide_stock, is_service from pim_articles WHERE article_id='".$db->f('article_id')."' ");
			$stock = $art->f('stock');
			$is_service = $art->f('is_service');
			$hide_stock = $art->f('hide_stock');
	    	$threshold_value = $art->f('article_threshold_value');
			
	 		$pending_articles = $db2->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$db->f('article_id')."' AND delivered=0");
	 		if($o['show_stock_warning'] && $o['allow_stock'] && $is_service==0 && $db->f('is_combined')==0 && $stock - $db->f('quantity')< $threshold_value){
				$show_stock_warning_line = true;
			} 
		}

		
		// echo $in['remove_vat'].','.$use_package.'<br>';

		if(!$db->f('tax_id')){
			$last_tr = $row_id;
		}

		if($db->f('is_combined')){
			$last_combined = $row_id;
		}

		if($db->f('has_variants') && $db->f('has_variants_done')){
			$last_variant_parent = $row_id;
		}

		$colum=$col;
		if(!$db->f('article_code') && !$db->f('article_id') && !$db->f('content') ){
			$colum+=2;
		}

		$quantity_component =0;
		if($db->f('component_for')){
	    		$parent_article_id = $db2->field("SELECT article_id FROM pim_order_articles where order_articles_id ='".$db->f('component_for')."' ");
	    		$quantity_component = get_quantity_component($parent_article_id, $db->f('article_id'));
	    	}
			
		$nr_dec_q = strlen(rtrim(substr(strrchr($db->f('quantity'), "."), 1),'0'));	

		$nr_taxes =  $db2->field("SELECT COUNT(article_tax_id) FROM pim_articles_taxes WHERE article_id='".$db->f('article_id')."' "); 	

		$linie = array(
			'tr_id'         					=> $row_id,
			'article'  			    			=> stripslashes(htmlspecialchars_decode($db->f('article'),ENT_QUOTES)),
	    	'article_code'						=> $db->f('article_code'),
	    	'is_article_code'					=> ($db->f('article_code') && $db->f('article_id'))? true : false,
			'article_id'							=> $db->f('article_id')?$db->f('article_id'):$db->f('tax_id'),
			'tax_for_article_id'	    			=> $db->f('tax_for_article_id'),
			'is_article'      	      				=> $db->f('article_id') ? true : false,
			'is_article1'      	      				=> $db->f('article_id') ? true : false,
			'is_combined'      	 	  				=> $db->f('is_combined') ? true : false,
			'is_component'      	  				=> $db->f('component_for') ? true : false,
			'is_tax'      	          				=> $db->f('tax_id')?1:0,
			'for_article'							=> $db->f('tax_id') ? $last_tr : '',
			'component_for'							=> $db->f('component_for')? $last_combined : '',
			'is_variant_for_line'					=> $db->f('is_variant_for')? $last_variant_parent : '',
			'quantity_component'      	   			=> display_number($quantity_component, $nr_dec_q ),
			'quantity_old'      	   				=> display_number($db->f('quantity'), $nr_dec_q ),
			'quantity'      							=> display_number($db->f('quantity'), $nr_dec_q ),
			'price_vat'      							=> display_number_var_dec($db->f('price_with_vat')),
			'price'         							=> display_number_var_dec($db->f('price')),
			'purchase_price'         					=> $db->f('purchase_price'),
			'purchase_price_other_currency'         					=> $db->f('purchase_price_other_currency'),
			'purchase_price_other_currency_id'         					=> $db->f('purchase_price_other_currency_id'),
			'percent_x'									=> display_number($db->f('vat_percent')),
			'percent'									=> $db->f('vat_percent'),
			'vat_value_x'								=> display_number($db->f('vat_value')),
			'vat_value'									=> $db->f('vat_value'),
		  	'sale_unit_x'								=> $db->f('sale_unit'),
			'packing_x'									=> remove_zero_decimals($db->f('packing')),
			'sale_unit'				    				=> $db->f('sale_unit'),
			'stock'				    					=> $stock,
			'pending_articles'        					=> intval($pending_articles),
			'threshold_value'         					=> intval($threshold_value),
			'hide_stock'             					=> $hide_stock,
			'packing'									=> remove_zero_decimals($db->f('packing')),
			'content'									=> $db->f('content') ? true : false,
			'colspan'									=> $db->f('content') ? ' colspan='.$cols.' class="last" ' : '',
			'disc'										=> $in['duplicate_order_id'] ? display_number($discount_line) : ($in['changePrices']=='1' ? display_number($order_query->f('discount_line_gen')) :  display_number($db->f('discount'))),
			'content_class'								=> $db->f('content') ? ' type_content ' : '',
	    	'line_total'								=> display_number($line_total),
	    	'th_width'									=> $in['allow_article_sale_unit'] ? 'width:208px' : 'width:267px',
			'td_width'									=> $in['allow_article_sale_unit'] ? 'width:218px' : 'width:277px',
			'input_width'								=> $in['allow_article_sale_unit'] ? 'width:164px' : 'width:223px',
			'colum'										=> $colum,
			'has_variants' 								=> $db->f('has_variants'),
		 	'has_variants_done' 						=> $db->f('has_variants_done'),
		 	'is_variant_for'							=> $db->f('is_variant_for'),
		 	'variant_type' 								=> $db->f('variant_type'),  
		 	'visible'									=> $db->f('visible')? true: false,
		 	'order_articles_id'							=> $db->f('order_articles_id'),
		 	'has_taxes'									=> $nr_taxes ? true : false,
		 	'show_stock_warning_line'					=> $show_stock_warning_line,
		);
		//reset prices for duplicate order depending of new article prices and price category of the company
		/*if($in['duplicate_order_id']){
			$view->assign(array(
				'tr_id'         			=> $row_id,
				'article'  			    	=> $article_data['content'] == 1 ? htmlspecialchars(html_entity_decode($db->f('article'))) : htmlspecialchars(html_entity_decode($article_data['article'])),
				'colspan'					=> $article_data['content'] == 1 ? ' colspan='.$cols.' class="last" ' : '',
		        'article_code'				=> $article_data['article_code'],
		        'is_article_code'			=> $article_data['article'] ? true : false,
				'price_vat'      			=> display_number($article_data['price_with_vat']),
				'price'         			=> display_number_var_dec($article_data['price']),
				'vat_value_x'				=> display_number($article_data['vat_value']),
				'vat_value'					=> $article_data['vat_value'],
			    'sale_unit_x'				=> $article_data['sale_unit'],
				'packing_x'					=> remove_zero_decimals($article_data['packing']),
				'sale_unit'					=> $article_data['sale_unit'],
				'stock'						=> $article_data['stock'],
				'pending_articles'         	=> intval($article_data['pending_articles']),
				'hide_stock'               	=> $article_data['hide_stock'],
				'packing'					=> remove_zero_decimals($article_data['packing']),
				'line_total'				=> display_number($line_total),
				'threshold_value'          	=> intval($article_data['threshold_value']),
				'disc'						=> display_number($article_data['line_discount']),
			),'article_line');

			$view->assign(array(
					'discount_percent'      => '( '.$article_data['global_discount'].'% )',
					'disc_line'				=> display_number($article_data['line_discount']),
					'is_duplicate'			=> true,
					'currency_type'			=> get_commission_type_list($in['currency_type']),
					'total_currency_hide'		=> $in['currency_rate'] ? '' : 'hide',
					'customer_name'			=> $article_data['customer_name'],
					'address_info'          	=> $article_data['address_info'],
					'APPLY_DISCOUNT_LIST'       	=> build_apply_discount_list($article_data['apply_discount']),
					'APPLY_DISCOUNT_LIST_TXT'       	=> build_apply_discount_list($article_data['apply_discount'],true),
					'DISCOUNT'              	=> display_number($in['discount']),
					'discount_line_gen'		=> $article_data['line_discount'],
					'is_vat'				=> $in['remove_vat'] == 1 ? false : true,
					'hide_disc'				=> $in['apply_discount'] ==0 || $in['apply_discount'] == 2 ? 'hide' : '',
					'show_vat'					=> $in['remove_vat'] == 1 ? 'hide' : '',
			));
		}*/
		// $view->loop('article_line');
		array_push($o['tr_id'], $linie);
		$i++;

	}

	$t=0;
	//$o['hide_total'] = true;
	foreach ($vat_percent as $key => $val){

		if($sub_t[$key] - $sub_discount[$key]){
			 		
	 		// $val = $sub_t[$key]*return_value($key)/100;
			//$total_vat += $val;
			$vat_line=array(
				'vat_percent'   			=> display_number($key),
				'vat_value'	   			=> display_number(round($val,ARTICLE_PRICE_COMMA_DIGITS)),
				'subtotal'				=> display_number(round($sub_t[$key],ARTICLE_PRICE_COMMA_DIGITS)),
				'total_novat'	   	 	=> display_number(round($sub_t[$key],ARTICLE_PRICE_COMMA_DIGITS)),
				'discount_value'			=> display_number($sub_discount[$key]),
				'is_ord'				=> $is_ord,
				'view_discount'			=> ($discount || $is_ord) ? true : false,
				'net_amount'			=> display_number($sub_t[$key] - $sub_discount[$key]),
			);
			array_push($o['vat_line'], $vat_line);
		}
		$t++;
	} 

	
	$total = round($total,ARTICLE_PRICE_COMMA_DIGITS);
	$vat_total = round($vat_total,ARTICLE_PRICE_COMMA_DIGITS);
	$total_with_vat=$vat_total+$total-$discount_total+$shipping_price;

	$total_currency = $total_with_vat * return_value($o['currency_rate']);
	// $view->assign(array(
	$o['discount_total']        	= display_number($discount_total);
	$o['vat_total']					= display_number($vat_total);
	$o['total']				    	= display_number($total);
	$o['total_vat']					= $in['remove_vat'] ? display_number($total-$discount_total+$shipping_price) : display_number($total_with_vat);
	$o['total_default_currency']			= display_number($total_currency);
	$o['is_delivery_cost']			= $shipping_price > 0 ? true : false;
	$o['shipping_price']			= display_number($shipping_price);
	$o['show_shipping_price']		= $show_shipping_price;
	$o['shipping_price_txt'] 		= $show_shipping_price == 1 ? display_number($shipping_price) : '-';
	$o['show_if_1']					= $show_shipping_price == 2 ? 'hide' : '';
	$o['hide_shipping']				= $show_shipping_price == 2 ? 'hide' : '';
	$o['show_shipping']				= $show_shipping_price == 1 ? 'hide' : '';
	// ));

}
$default_note = $db->field("SELECT value FROM default_data WHERE type='order_note'");

$is_extra = false;
if($customer_id){
	$c_details = $db->query("SELECT * FROM customers WHERE customer_id ='".$customer_id."' ");

	if($c_details->f('bank_name') || $c_details->f('bank_bic_code') || $c_details->f('bank_iban') ){
		$extra_info .= '<h4 >'.gm('Bank Details').'</h4>
	<p><strong>'.gm('Name').': </strong> <span>'.$c_details->f('bank_name').'</span></p>
	<p><strong>'.gm('BIC Code').': </strong> <span>'.$c_details->f('bank_bic_code').'</span></p>
	<p><strong>'.gm('IBAN').': </strong> <span>'.$c_details->f('bank_iban').'</span></p>
	';
	}
	if($extra_info){
		$is_extra = true;
		$extra_info = '<div style="width: 924px;" >'.$extra_info.'</div>';
	}
}


	$o['notes']                     = stripslashes(get_notes($in));
	$o['pick_date_format']          = pick_date_format(ACCOUNT_DATE_FORMAT);
	$o['style']                     = ACCOUNT_NUMBER_FORMAT;
	$o['page_title']			    = $page_title;
	$o['type_title']			    = $type_title;
	$o['nr_status']			    	= $nr_status;
	$o['do_next']	    		    = $do_next;
	// 'total_currency_hide'		=> 'hide',
	$o['extra_info']				= $extra_info;
	$o['is_extra']					= $is_extra;
	$o['stock_multiple_location']   = STOCK_MULTIPLE_LOCATIONS;
	$o['CURRENCY_TYPE_LIST']   = build_currency_list(ACCOUNT_CURRENCY_TYPE);



////////// multilanguage for order note ////////////////

$transl_lang_id_active 		= $in['languages'];
if(!$transl_lang_id_active){
	$transl_lang_id_active = $in['email_language'];
}
// console::log('lang_d2 '.$transl_lang_id_active);
$o['translate_loop'] = array();
$langs = $db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");
while($langs->next()){
	if((!$in['order_id']  ||  $in['c_quote_id']) || $in['quote_id'] || $in['order_id'] =='tmp'){
		if($langs->f('lang_id')==1){
			$note_type = 'order_note';
		}else{
			$note_type = 'order_note_'.$langs->f('lang_id');
		}
		$order_note = $db3->field("SELECT value FROM default_data
								   WHERE default_name = 'order note'
								   AND type = '".$note_type."' ");
		if($in['c_quote_id']){
			$transl_lang_id_active = $db3->field("SELECT lang_id FROM note_fields WHERE item_id = '".$in['c_quote_id']."' AND version_id = '".$version_id."' AND active = 1 AND item_type = 'quote' ");
		}
	}else{
			$order_note = $db3->field("SELECT item_value FROM note_fields
								   WHERE item_type 	= 'order'
								   AND 	item_name 	= 'notes'
								   AND 	item_id 	= '".$in['order_id']."'
								   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

			// $transl_lang_id_active = $db3->field("SELECT lang_id FROM note_fields
			// 					   WHERE item_type 	= 'order'
			// 					   AND 	item_name 	= 'notes'
			// 					   AND 	active 		= '1'
			// 					   AND 	item_id 	= '".$in['order_id']."' ");

			if($in['duplicate_order_id']){
				$transl_lang_id_active = $in['languages'];
			}
			if(!$order_note && !$transl_lang_id_active){
				$order_from_front = $db3->field("SELECT order_email FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
			}
			if($order_from_front){
				if($langs->f('lang_id')==1){
					$note_type = 'order_note';
				}else{
					$note_type = 'order_note_'.$langs->f('lang_id');
				}
				$order_note = $db3->field("SELECT value FROM default_data
										   WHERE default_name = 'order note'
										   AND type = '".$note_type."' ");
				$transl_lang_id_active = $db3->field("SELECT lang_id FROM pim_lang WHERE sort_order = 1");
				$in['order_from_front'] = 1;

			}


	}
	$translate_lang = 'form-language-'.$langs->f('code');
	if($langs->f('code')=='du'){
		$translate_lang = 'form-language-nl';
	}

	if($transl_lang_id_active != $langs->f('lang_id')){
		$translate_lang = $translate_lang.' hidden';
	}

	array_push($o['translate_loop'], array(
		'translate_cls' 	=> 		$translate_lang,
		'lang_id' 			=> 		$langs->f('lang_id'),
		'notes'				=> 		$order_note,
	));

}
$transl_lang_id_active_custom 		= $in['languages'];
if(!$transl_lang_id_active_custom){
	$transl_lang_id_active_custom = $in['email_language'];
}
$o['translate_loop_custom'] = array();
#custom extra languages
$custom_langs = $db->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
while($custom_langs->next()) {
	if(!$in['order_id'] || $in['c_quote_id'] || $in['quote_id'] || $in['order_id'] =='tmp') {
		$custom_note_type = 'order_note_'.$custom_langs->f('lang_id');
		$order_note_custom = $db3->field("SELECT value FROM default_data WHERE default_name = 'order note' AND type = '".$custom_note_type."' ");
		if($in['c_quote_id']) {
			$transl_lang_id_active_custom = $db3->field("SELECT lang_id FROM note_fields WHERE item_id = '".$in['c_quote_id']."' AND version_id = '".$version_id."' AND active = 1 AND item_type = 'quote'");
		}
	} else {
		$order_note_custom = $db3->field("SELECT item_value FROM note_fields
										WHERE item_type 	= 'order'
										AND item_name 		= 'notes'
										AND item_id			= '".$in['order_id']."'
										AND lang_id 		= '".$custom_langs->f('lang_id')."'");
		// $transl_lang_id_active_custom = $db3->field("SELECT lang_id FROM note_fields
		// 											WHERE item_type 	= 'order'
		// 											AND item_name 		= 'notes'
		// 											AND active 			= '1'
		// 											AND item_id 		= '".$in['order_id']."' ");
		if($in['duplicate_order_id']) {
			$transl_lang_id_active_custom = $in['languages'];
		}

		if(!$order_note_custom && $transl_lang_id_active_custom) {
			$order_from_front_cust = $db3->field("SELECT order_email FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
		}
		if($order_from_front_cust) {
			$custom_note_type = 'order_note_'.$custom_langs->f('lang_id');
			$order_note_custom = $db3->field("SELECT value FROM default_data WHERE default_name = 'order note' AND type = '".$custom_note_type."' ");
			if(!$transl_lang_id_active_custom){
				$transl_lang_id_active_custom = $db3->field("SELECT lang_id FROM pim_custom_lang WHERE sort_order = 1");
			}
			$in['order_from_front'] = 1;
		}
	}
	$translate_lang_custom = 'form-language-'.$custom_langs->f('code');

	if($transl_lang_id_active_custom != $custom_langs->f('lang_id')) {
		$translate_lang_custom = $translate_lang_custom.' hidden';
	}

	array_push($o['translate_loop_custom'], array(
		'translate_cls'		=> $translate_lang_custom,
		'lang_id'			=> $custom_langs->f('lang_id'),
		'notes'				=> $order_note_custom
	));
}

/*$translate_lang_active = $db->field("SELECT code FROM pim_lang WHERE lang_id = '".$transl_lang_id_active."'");
$translate_lang_active = 'form-language-'.$translate_lang_active;
if($translate_lang_active=='form-language-du'){
	$translate_lang_active = 'form-language-nl';
}*/

if($transl_lang_id_active>=1000) {
	$lang_code = $db->field("SELECT language FROM pim_custom_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
	$code = $db->field("SELECT code FROM pim_custom_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
} else {
	$lang_code = $db->field("SELECT language FROM pim_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
	$code = $db->field("SELECT code FROM pim_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
}
 	if($in['c_quote_id'] && $in['a_quote_id']){
 		$transl_lang_id_active = $quote->f('email_language');
 	}

$in['only_contact_name'] = 1;
$in['title'] = 1;
//console::log('lang_d '.$transl_lang_id_active);
$o['translate_lang_active']							= 'form-language-'.$code;
$o['language_dd'] 									= build_language_dd_new($transl_lang_id_active);
$o['email_language'] 								= $transl_lang_id_active;
$o['langs'] 										= $transl_lang_id_active;
$o['order_from_front']								= $in['order_from_front'];
$o['nr_decimals'] 									= ARTICLE_PRICE_COMMA_DIGITS;
$o['cancel_link']									= $cancel_link;
$o['language_txt']									= gm($lang_code);
$o['style']  										= ACCOUNT_NUMBER_FORMAT;
$o['authors']										= get_author($in,true,false);
$o['accmanager']									= get_accmanager($in,true,false);
$o['contacts']										= get_contacts($in,true,false);
$o['customers']										= get_customers($in,true,false);
$o['addresses']										= get_addresses($in,true,false);
$o['site_add']										= true;
$o['site_addresses']								= get_addresses($in,true,false);
$o['cc']											= get_cc($in,true,false);
$o['articles_list']									= get_articles_list($in,true,false);
$o['country_dd']									= build_country_list();
$o['main_country_id']								= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';



json_out($o);

function get_author($in,$showin=true,$exit=true){

	$q = strtolower($in["term"]);

	global $database_config;

	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$db_users = new sqldb($database_users);

	$filter = '';

	if($q){
	  $filter .=" AND users.first_name LIKE '".$q."%'";
	}
	$db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role
									FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
									WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ");

	$items = array();
	while($db_users->move_next()){
		array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>$db_users->f('first_name').' '.$db_users->f('last_name') ) );
	}
	return json_out($items, $showin,$exit);

}
function get_accmanager($in,$showin=true,$exit=true){

	$q = strtolower($in["term"]);

	global $database_config;

	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$db_users = new sqldb($database_users);

	$filter = '';

	if($q){
	  $filter .=" AND users.first_name LIKE '".$q."%'";
	}

	$db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type
										FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
										WHERE database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY first_name ");

	$items = array();
	while($db_users->move_next()){
		array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>$db_users->f('first_name').' '.$db_users->f('last_name') ) );
	}
	return json_out($items, $showin,$exit);

}
function get_contacts($in,$showin=true,$exit=true){

	if(!$in['buyer_id'] || ($in['buyer_id'] && !is_numeric($in['buyer_id']))){
		return array();
	}
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db2 = new sqldb();
	$db_user = new sqldb($database_users);
	$filter .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";

	if($in['current_id']){
		$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
	}

	if($in['customer_id']){
		$filter .= " AND customers.customer_id='".$in['customer_id']."'";
	}
	if($in['buyer_id']){
		$filter .= " AND customers.customer_id='".$in['buyer_id']."'";
	}

	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){

		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$items = array();
	$db= new sqldb();

	$title = array();
	$titles = $db2->query("SELECT * FROM customer_contact_title ")->getAll();
	foreach ($titles as $key => $value) {
		$title[$value['id']] = $value['name'];
	}

	$contacts = $db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, customer_contacts.language,
				customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
				FROM customer_contacts
				INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
			 	LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
				LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
				LEFT JOIN country ON country.country_id=customer_contact_address.country_id
				WHERE customer_contacts.active=1 $filter ORDER BY lastname limit 9")->getAll();
	$result = array();
	foreach ($contacts as $key => $value) {
		$contact_title = $title[$value['title']];
    	if($contact_title){
			$contact_title .= " ";
    	}		

	    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
	    $price_category = "1";
	    if ($value['customer_id']){
	  		$price_category = $value['cat_id'];
	  	}

		$result[]=array(
			'symbol'				=> '',
			"id"					=> $value['contact_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
			"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
			"email"					=> $value['email'],
			"price_category_id"		=> $price_category, 
			'customer_id'	 		=> $value['customer_id'], 
			'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
			'currency_id' 			=> $value['currency_id'], 
			"lang_id" 				=> $value['internal_language'], 
			'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
			'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
			//'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
			'right' 				=> '', 
			'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
			//'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']) 
			'bottom' 				=> '',
			'email_language'		=> $value['language']

		);

	}

	$added = false;
	if(count($result)==9){
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999'));
		}
		$added = true;
	}
	if($in['contact_id']){
		$cust = $db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, customer_contacts.language,
				customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
				FROM customer_contacts
				INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
 				LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
				LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
				LEFT JOIN country ON country.country_id=customer_contact_address.country_id
				WHERE customer_contacts.active=1 AND customer_contacts.contact_id='".$in['contact_id']."' ORDER BY lastname ")->getAll();
		$value = $cust[0];
		$contact_title = $title[$value['title']];
    	if($contact_title){
			$contact_title .= " ";
    	}		

	    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
	  	$price_category = "1";
		if ($value['customer_id']){
	  		$price_category = $value['cat_id'];
	  	}
	  	
		$result[]=array(
			"id"					=> $value['contact_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
			"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
			"email"					=> $value['email'],
			"price_category_id"		=> $price_category, 
			'customer_id'	 		=> $value['customer_id'], 
			'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
			'currency_id' 			=> $value['currency_id'], 
			"lang_id" 				=> $value['internal_language'], 
			'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
			'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
			//'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
			'right' 				=> '', 
			'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
			//'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']) 
			'bottom' 				=> '' ,
			'email_language'		=> $value['language']
		);
	}
	if(!$added){
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999'));
		}
	}
	
	return json_out($result, $showin,$exit);
}

	function  get_notes($in){
	/*$db=new sqldb();
	$o = array();
	$transl_lang_id_active 		= $in['languages'];
	if(!$transl_lang_id_active){
		$transl_lang_id_active = $in['email_language'];
	}
	$o['translate_loop'] = array();
	$langs = $db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");
	while($langs->next()){
		if((!$in['order_id']  ||  $in['c_quote_id']) || $in['quote_id']){
			if($langs->f('lang_id')==1){
				$note_type = 'order_note';
			}else{
				$note_type = 'order_note_'.$langs->f('lang_id');
			}
			$order_note = $db->field("SELECT value FROM default_data
									   WHERE default_name = 'order note'
									   AND type = '".$note_type."' ");
			if($in['c_quote_id']){
				$transl_lang_id_active = $db->field("SELECT lang_id FROM note_fields WHERE item_id = '".$in['c_quote_id']."' AND version_id = '".$version_id."' AND active = 1 AND item_type = 'quote' ");
			}
		}else{
				$order_note = $db->field("SELECT item_value FROM note_fields
									   WHERE item_type 	= 'order'
									   AND 	item_name 	= 'notes'
									   AND 	item_id 	= '".$in['order_id']."'
									   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

				if($in['duplicate_order_id']){
					$transl_lang_id_active = $in['languages'];
				}
				if(!$order_note && !$transl_lang_id_active){
					$order_from_front = $db->field("SELECT order_email FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
				}
				if($order_from_front){
					if($langs->f('lang_id')==1){
						$note_type = 'order_note';
					}else{
						$note_type = 'order_note_'.$langs->f('lang_id');
					}
					$order_note = $db->field("SELECT value FROM default_data
											   WHERE default_name = 'order note'
											   AND type = '".$note_type."' ");
					// $transl_lang_id_active = $db->field("SELECT lang_id FROM pim_lang WHERE sort_order = 1");
					$in['order_from_front'] = 1;

				}
		}
		$translate_lang = 'form-language-'.$langs->f('code');
		if($langs->f('code')=='du'){
			$translate_lang = 'form-language-nl';
		}

		if($transl_lang_id_active != $langs->f('lang_id')){
			$translate_lang = $translate_lang.' hidden';
		}

		array_push($o['translate_loop'], array(
			'translate_cls' 	=> 		$translate_lang,
			'lang_id' 			=> 		$langs->f('lang_id'),
			'notes'				=> 		$order_note,
			));

	}
	$transl_lang_id_active_custom 		= $in['languages'];
	if(!$transl_lang_id_active_custom){
		$transl_lang_id_active_custom = $in['email_language'];
	}
	$o['translate_loop_custom'] = array();
	$custom_langs = $db->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
	while($custom_langs->next()) {
		if(!$in['order_id'] || $in['c_quote_id'] || $in['quote_id']) {
			$custom_note_type = 'order_note_'.$custom_langs->f('lang_id');
			$order_note_custom = $db->field("SELECT value FROM default_data WHERE default_name = 'order note' AND type = '".$custom_note_type."' ");
			if($in['c_quote_id']) {
				$transl_lang_id_active_custom = $db->field("SELECT lang_id FROM note_fields WHERE item_id = '".$in['c_quote_id']."' AND version_id = '".$version_id."' AND active = 1 AND item_type = 'quote'");
			}
		} else {
			$order_note_custom = $db->field("SELECT item_value FROM note_fields
											WHERE item_type 	= 'order'
											AND item_name 		= 'notes'
											AND item_id			= '".$in['order_id']."'
											AND lang_id 		= '".$custom_langs->f('lang_id')."'");
			if($in['duplicate_order_id']) {
				$transl_lang_id_active_custom = $in['languages'];
			}

			if(!$order_note_custom && $transl_lang_id_active_custom) {
				$order_from_front_cust = $db->field("SELECT order_email FROM pim_orders WHERE order_id = '".$in['order_id']."' ");
			}
			if($order_from_front_cust) {
				$custom_note_type = 'order_note_'.$custom_langs->f('lang_id');
				$order_note_custom = $db->field("SELECT value FROM default_data WHERE default_name = 'order note' AND type = '".$custom_note_type."' ");
				if(!$transl_lang_id_active_custom){
				}
				$in['order_from_front'] = 1;
			}
		}
		$translate_lang_custom = 'form-language-'.$custom_langs->f('code');

		if($transl_lang_id_active_custom != $custom_langs->f('lang_id')) {
			$translate_lang_custom = $translate_lang_custom.' hidden';
		}

		array_push($o['translate_loop_custom'], array(
			'translate_cls'		=> $translate_lang_custom,
			'lang_id'			=> $custom_langs->f('lang_id'),
			'notes'				=> $order_note_custom
		));
	}

	if($transl_lang_id_active >= 1000) {
		$lang_code = $db->field("SELECT language FROM pim_custom_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
		$code = $db->field("SELECT code FROM pim_custom_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
	} else {
		$lang_code = $db->field("SELECT language FROM pim_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
		$code = $db->field("SELECT code FROM pim_lang WHERE lang_id='".$transl_lang_id_active."' GROUP BY code ");
	}

	$in['only_contact_name'] = 1;
	$in['title'] = 1;

	$o['translate_lang_active']				= 'form-language-'.$code;
	$o['language_txt']								= gm($lang_code);*/
	$db=new sqldb();
		$terms = '';
		if($in['email_language']!=1){		
			$terms = '_'.$in['email_language'];
		}
		$quote_note = $db->field("SELECT value FROM default_data
												   WHERE default_name = 'order_note'
												   AND type = 'order_note".$terms."'  ");
		$free_text_content = $db->field("SELECT value FROM default_data
												   WHERE default_name = 'quote_term'
												   AND type = 'quote_terms".$terms."'  ");
		if($in['order_id']){ // the function is called from inside the class
			if($in['order_id'] == 'tmp'){
				$quote_note = $db->field("SELECT value FROM default_data
												   WHERE default_name = 'order_note'
												   AND type = 'order_note".$terms."'  ");
				$quote_notes ='';
				if($in['c_quote_id']){
					//order from quote
					$quote_notes = $db->field("SELECT item_value FROM note_fields
								 WHERE item_id= '".$in['c_quote_id']."' AND lang_id ='".$in['languages']."' AND item_type='quote' AND item_name='notes' AND active='1'");
				}
				
				if($quote_notes){
					$array=$quote_notes;
				}else{
					$array = $quote_note;
					if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['email_language']){
						$acc_langs=array('en','fr','nl','de');
						$lang_c=$db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['email_language']."' ");
						if($lang_c){
							if($lang_c=='du'){
									$lang_c='nl';
							}
							if(in_array($lang_c, $acc_langs)){
								$in['vat_notes']=stripslashes($db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
								if($in['vat_notes']){
									if($array){
										$array=$array."\n".$in['vat_notes'];
									}else{
										$array=$in['vat_notes'];
									}
								}
							}
						}		
					}
				}	
			//	$array['free_text_content'] = $free_text_content;
			}else{
				$quote_note = $db->field("SELECT value FROM default_data
												   WHERE default_name = 'order_note'
												   AND type = 'order_note".$terms."'  ");
				$quote_notes = $db->field("SELECT item_value FROM note_fields
								 WHERE item_id= '".$in['order_id']."' AND lang_id ='".$in['languages']."' AND item_type='order' AND item_name='notes' AND active='1'");
				if($quote_notes){
					$array=$quote_notes;
				}else{
					$array=$quote_note;
					if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['email_language']){
						$acc_langs=array('en','fr','nl','de');
						$lang_c=$db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['email_language']."' ");
						if($lang_c){
							if($lang_c=='du'){
									$lang_c='nl';
							}
							if(in_array($lang_c, $acc_langs)){
								$in['vat_notes']=stripslashes($db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
								if($in['vat_notes']){
									if($array){
										$array=$array."\n".$in['vat_notes'];
									}else{
										$array=$in['vat_notes'];
									}
								}
							}
						}		
					}
				}
				//$array=$quote_notes ? $quote_notes : $quote_note;

				$free_text_content1 = $db->field("SELECT item_value FROM note_fields
									   WHERE item_type 	= 'quote'
									   AND 	item_name 	= 'free_text_content'
									   AND 	version_id 	= '".$version_id."'
									   AND 	item_id 	= '".$quote_id."'
									   AND 	buyer_id 	= '".$in['buyer_id']."' ");

			//	$array['free_text_content'] = $free_text_content1 ? $free_text_content1 : $free_text_content;

			}
		}else{ // the function is called when the user changed the language
			$quote_note = $db->field("SELECT value FROM default_data
												   WHERE default_name = 'order_note'
												   AND type = 'order_note".$terms."'  ");
			$array = $quote_note;
			if($in['vat_regime_id'] && $in['vat_regime_id']>=10000 && $in['email_language']){
				$acc_langs=array('en','fr','nl','de');
				$lang_c=$db->field("SELECT code FROM pim_lang WHERE lang_id='".$in['email_language']."' ");
				if($lang_c){
					if($lang_c=='du'){
							$lang_c='nl';
					}
					if(in_array($lang_c, $acc_langs)){
						$in['vat_notes']=stripslashes($db->field("SELECT ".$lang_c." FROM vat_lang WHERE id='".$in['vat_regime_id']."' "));
						if($in['vat_notes']){
							if($array){
								$array=$array."\n".$in['vat_notes'];
							}else{
								$array=$in['vat_notes'];
							}
						}
					}
				}		
			}
			//$array['free_text_content'] = $free_text_content;

		}

	return $array;
}
function get_customers($in,$showin=true,$exit=true){
	
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_user = new sqldb($database_users);
	$filter =" AND is_admin='0' ";
	$filter .=" AND ((customers.is_supplier='' AND customers.is_customer='') OR ( customers.is_customer='1')) " ;

	if($q){
		$filter .=" AND name LIKE '%".addslashes($q)."%'";
	}

	if($is_admin){
		$filter =" AND is_admin='".$is_admin."' ";
	}

	if($in['current_id']){
		$filter .= " AND customer_id !='".$in['current_id']."'";
	}
	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$db= new sqldb();

	$cust = $db->query("SELECT customers.name, customers.customer_id, customers.active, our_reference, currency_id, internal_language, identity_id, 
					acc_manager_name, customers.country_name
					FROM customers 
					WHERE customers.active=1 $filter GROUP BY customers.customer_id ORDER BY customers.name limit 5 ")->getAll();

	# foreach e 2x mai rapid decat while-ul
	$result = array();

	foreach ($cust as $key => $value) {
		$cname = trim($value['name']);
		$result[]=array(
			"id"					=> $value['customer_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"ref" 					=> $value['our_reference'],
			"currency_id"			=> $value['currency_id'],
			"lang_id" 				=> $value['internal_language'],
			"identity_id"	 		=> $value['identity_id'],
			'contact_name'			=> $value['acc_manager_name'],
			'country'				=> $value['country_name']
		);
		
	}
	$added = false;
	if(count($result)==5){
		array_push($result,array('id'=>'99999999999'));
		$added = true;
	}
	
	if($in['buyer_id']){
		$cust = $db->query("SELECT customers.name, customers.customer_id, customers.active, our_reference, currency_id, internal_language, identity_id, 
					acc_manager_name, customers.country_name
					FROM customers 
					WHERE customers.active=1 AND customer_id='".$in['buyer_id']."' GROUP BY customers.customer_id ORDER BY customers.name ")->getAll();
		$value = $cust[0];
		$cname = trim($value['name']);
		$result[]=array(
			"id"					=> $value['customer_id'],
			"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
			"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
			"ref" 					=> $value['our_reference'],
			"currency_id"			=> $value['currency_id'],
			"lang_id" 				=> $value['internal_language'],
			"identity_id"	 		=> $value['identity_id'],
			'contact_name'			=> $value['acc_manager_name'],
			'country'				=> $value['country_name']
		);
	}
	
	if(!$added){
		array_push($result,array('id'=>'99999999999'));		
	}
	return json_out($result, $showin,$exit);

}
function get_cc($in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_user = new sqldb($database_users);
	$filter =" is_admin='0' AND customers.active=1 ";
	$filter .=" AND ((customers.is_supplier='' AND customers.is_customer='') OR ( customers.is_customer='1')) " ;
	// $filter_contact = ' 1=1 ';
	if($q){
		$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
		// $filter_contact .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
	}
	if($in['buyer_id'] && is_numeric($in['buyer_id'])){
		$filter .=" AND customers.customer_id='".$in['buyer_id']."'";
	}

	//$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
	$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = :user_id ",['user_id'=>$_SESSION['u_id']]);
	if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
		$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
	}
	$db= new sqldb();
// UNION 
// 		SELECT customer_contacts.customer_id, CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) AS name, customer_contacts.contact_id, position_n as acc_manager_name, country.name AS country_name, customer_contact_address.zip AS zip_name, customer_contact_address.city AS city_name
// 		FROM customer_contacts
// 		LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
// 		LEFT JOIN country ON country.country_id=customer_contact_address.country_id
// 		WHERE $filter_contact
// 		ORDER BY name
	$cust = $db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,type
			FROM customers
			
			WHERE $filter
			GROUP BY customers.customer_id			
			ORDER BY name
			LIMIT 50")->getAll();

	$result = array();
	foreach ($cust as $key => $value) {
		$cname = trim($value['name']);
		if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}

			$address = $db->query("SELECT zip,city,address FROM customer_addresses
										WHERE customer_addresses.is_primary ='1' AND customer_addresses.customer_id ='".$value['cust_id']."'");

		$result[]=array(
				//"id"					=> $value['cust_id'].'-'.$value['contact_id'],
				"id"					=> $value['cust_id'],
				'symbol'				=> '',
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				/*'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],*/
				'zip'					=> $address->f('zip') ? $address->f('zip') : '',
				'city'					=> $address->f('city') ? $address->f('city') : '',
				/*"bottom"				=> $address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']*/
				"bottom"				=> '',
				"right"					=> ''
		);
		/*if (count($result) > 100)
			break;*/
	}
	if($q){
		array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
	}else{
		array_push($result,array('id'=>'99999999999','value'=>''));
	}
	
	return json_out($result, $showin,$exit);

}
function get_addresses($in,$showin=true,$exit=true){
	if(!$in['buyer_id'] || ($in['buyer_id'] && !is_numeric($in['buyer_id']))){
		return array();
	}
	$db= new sqldb();
	$q = strtolower($in["term"]);
	$filter=" AND customer_addresses.delivery='1' ";
	if($q){
		$filter .=" AND (customer_addresses.address LIKE '%".addslashes($q)."%' OR customer_addresses.zip LIKE '%".addslashes($q)."%' OR customer_addresses.city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
	}

	if($in['buyer_id'] && is_numeric($in['buyer_id'])){
		$in['customer_id'] = $in['buyer_id'];
	}
	
	// array_push($result,array('id'=>'99999999999','value'=>''));
	if($in['field'] == 'customer_id' || $in['customer_id'] ){
		$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
								 FROM customer_addresses
								 LEFT JOIN country ON country.country_id=customer_addresses.country_id
								 LEFT JOIN state ON state.state_id=customer_addresses.state_id
								 WHERE customer_addresses.customer_id='".$in['customer_id']."' $filter
								 ORDER BY customer_addresses.address_id limit 9");
	}
	else if($in['field'] == 'contact_id' || $in['contact_id']){
	   $address= $db->query("SELECT customer_contact_address.*,country.name AS country 
							 FROM customer_contact_address 
							 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
							 WHERE customer_contact_address.contact_id='".$in['contact_id']."' $filter 
							 AND customer_contact_address.delivery='1' limit 9");
	}

	// $max_rows=$db->records_count();
	// $db->move_to($offset*$l_r);
	// $j=0;
	$addresses=array();
	if($address){
		while($address->next()){
		  	$a = array(
		  		'symbol'				=> '',
			  	'address_id'	    => $address->f('address_id'),
			  	'id'			    => $address->f('address_id'),
			  	'address'			=> nl2br($address->f('address')),
			  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
			  	'zip'			    => $address->f('zip'),
			  	'city'			    => $address->f('city'),
			  	'state'			    => $address->f('state'),
			  	'country'			=> $address->f('country'),
			  	/*'right'				=> $address->f('country'),
			  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
			  	*/
			  	'right'				=> '',
			  	'bottom'			=> '',
		  	);
			array_push($addresses, $a);
		}
	}
	$added = false;
	if(count($addresses)==9){
		if($q){
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
		}
		$added = true;
	}
	if($in['delivery_address_id']){
		if($in['field'] == 'customer_id'){
			$address= $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['customer_id']."' AND customer_addresses.address_id='".$in['delivery_address_id']."'
									 ORDER BY customer_addresses.address_id ");
		}
		else if($in['field'] == 'contact_id'){
		   $address= $db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  AND customer_contact_address.address_id='".$in['delivery_address_id']."'
								 AND customer_contact_address.delivery='1' ");
		}
		$a = array(
		  		'address_id'	    => $address->f('address_id'),
			  	'id'			    => $address->f('address_id'),
			  	'address'			=> nl2br($address->f('address')),
			  	'top'				=> strip_tags($address->f('address').' '.$address->f('zip').' '.$address->f('city').' '.$address->f('country')),
			  	'zip'			    => $address->f('zip'),
			  	'city'			    => $address->f('city'),
			  	'state'			    => $address->f('state'),
			  	'country'			=> $address->f('country'),
			  	/*'right'				=> $address->f('country'),
			  	'bottom'			=> $address->f('zip').' '.$address->f('city'),*/
			  	'right'				=> '',
			  	'bottom'			=> '',
		  	);
			array_push($addresses, $a);
	}
	if(!$added){
		if($q){
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
		}
	}
	
	return json_out($addresses, $showin,$exit);

}
function get_articles_list(&$in,$showin=true,$exit=true){
	$in['lang_id'] = $in['languages'];
	$in['cat_id'] = $in['price_category_id'];
	$in['from_order_page'] = true;
	unset($in['xget']);
	// return false;
	return ark::run('order-addArticle');
}

function get_currency_convertor($in)
{
	$currency = $in['currency'];
	if(empty($currency)){
		$currency = "USD";
	}
	$separator = $in['acc'];
	$into = $in['into'];
	return currency::getCurrency($currency, $into, 1,$separator);
}
function get_deals(&$in,$showin=true,$exit=true){
	$db= new sqldb();
	$q = strtolower($in["term"]);

	$filter = '';

	if($q){
		  $filter .=" AND subject LIKE '".$q."%'";
	}
	$deals=$db->query("SELECT * FROM tblopportunity 
			WHERE buyer_id='".$in['buyer_id']."' $filter ORDER BY subject ASC");
	$items = array();
	while($deals->next()){
		array_push( $items, array('deal_id'=>$deals->f('opportunity_id'),'name'=>stripslashes($deals->f('subject'))));
	}

	if(defined('ADV_CRM') && ADV_CRM == 1){
		if($q){
	        array_push($items,array('deal_id'=>'99999999999','name'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
	    }else{
	        array_push($items,array('deal_id'=>'99999999999','name'=>''));
	    }
	}
	return $items;
}
function get_countries(&$in,$showin=true,$exit=true){
	$items=array('countries'=>build_country_list());
	return json_out($items, $showin,$exit);
}
