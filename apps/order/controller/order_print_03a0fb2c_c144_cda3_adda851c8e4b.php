<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$db5 = new sqldb();
$custom_lng = $db5->field("SELECT lang_id FROM pim_custom_lang WHERE lang_id='".$in['lid']."' ");

ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));

$pdf_type =array(4,5);
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetFont('dejavusans', '', 9, '', false);
if($custom_lng){
	#we can use both font types but dejavusans looks better
	$pdf->SetFont('dejavusans', '', 9, '', false);
	// $pdf->SetFont('freeserif', '', 10, '', false);
}
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Order');
$pdf->SetSubject('Order');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);

$pdf->SetMargins(15, 10, 10);
$pdf->SetAutoPageBreak(TRUE, 30);
$hide_all = 2;

$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);

$pdf->AddPage();

$html = include('order_print_body_03a0fb2c_c144_cda3_adda851c8e4b.php');
// file_put_contents('print4v2', $html);
// print($html);exit();
if(isset($in['print'])){
	print_r($html);exit();
}
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->SetY($height);

	$hide_all = 1;
	$pdf->SetAutoPageBreak(TRUE, 0);

	$htmls = include('order_print_body_03a0fb2c_c144_cda3_adda851c8e4b.php');

	$pdf->writeHTML($htmls, true, false, true, false, '');

$pdf->lastPage();

if($in['save_as'] == 'F'){
	$in['order_pdf_name'] = 'order_'.unique_id(32).'.pdf';
	$pdf->Output(__DIR__.'/../../../'.$in['order_pdf_name'], 'F');
	ark::loadLibraries(array('aws'));
	$a = new awsWrap(DATABASE_NAME);
	$pdfPath = INSTALLPATH.$in['order_pdf_name'];
	$pdfFile = $in['delivery_id'] ? 'order/order_delivery_'.$in['delivery_id'].".pdf" : 'order/order_'.$in['order_id'].".pdf";
	$e = $a->uploadFile($pdfPath,$pdfFile);
	unlink($in['order_pdf_name']);
}else if($in['do']=='order-order_print'){
	doQueryLog();
	$pdf_file_name=$serial_number;
	if($in['delivery_id']){
		$pdf_file_name=$labels['delivery_note'].'_'.$serial_number;
	}
   $pdf->Output($pdf_file_name.'.pdf','I');
}else{
   $pdf->Output(__DIR__.'/../../../../admin/order.pdf', 'F');
}