<?php
define('INSTALLPATH',__DIR__.'/');
define('LAYOUT',__DIR__.'/layout/');
define('BASEPATH', INSTALLPATH.'core/');
define('APPSPATH', INSTALLPATH.'apps/');
//define('DATABASE_NAME','445e3208_dd94_fdd4_c53c135a2422');

define('AT_CACHE', 'cache/');

/************************************************************************
* @Author: Tinu Coman
***********************************************************************/

set_time_limit(0);
include_once(__DIR__."/core/startup_cron.php");
ini_set('display_errors', 1);
ini_set('error_reporting', 1);
error_reporting(E_ALL ^ E_NOTICE);
ini_set('error_log','cron_project.log');

global $database_config,$config;

$db_config_1 = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
);
$db_users = new sqldb($db_config_1);
$const=array();
$events=array('delivered','dropped','bounce','open','click','spamreport');

$response_init=@file_get_contents("php://input");
$db_users->query("INSERT INTO sendgrid_log SET response='".addslashes($response_init)."', date='".time()."' ");
//$response_init=$db_users->field("SELECT response FROM sendgrid_log WHERE id='126' ");


$response = json_decode($response_init);
 
if(is_array($response) && !empty($response) && in_array($response[0]->event,$events )){

	$x_message_id="";
    $x_message_id_exploded=explode('.',$response[0]->sg_message_id);
    if(!empty($x_message_id_exploded)){
    	$x_message_id=$x_message_id_exploded[0];
    }

    $event=$response[0]->event;
    $email=$response[0]->email;
    $reason=$response[0]->reason;
    $url=$response[0]->url;
    $timestamp=$response[0]->timestamp;

    if(strpos($url, '/weblink/')){
    	$url = 'weblink';
    }

	$info=$db_users->query("SELECT database_name, module, module_id, module_multiple_ids FROM sendgrid_database WHERE x_message_id='".$x_message_id."' ");
	$database_user = $info->f('database_name');
	$module = $info->f('module');
	$module_id = $info->f('module_id');
	$module_multiple_ids = $info->f('module_multiple_ids');

    if($database_user && $module && ($module_id || $module_multiple_ids ) ){
    	$db_user = array(
		    'hostname' => $database_config['mysql']['hostname'],
		    'username' => $database_config['mysql']['username'],
		    'password' => $database_config['mysql']['password'],
		    'database' => $database_user,
		); 
		$dbq= new sqldb($db_user);
		$dbq->query("SET names utf8");
		$dbq->query("SET sql_mode= ''");
		$dbq->query("SELECT constant_name, value, long_value FROM settings");

		while($dbq->next()){
		    if($dbq->f('constant_name')){
		        $const[$dbq->f('constant_name')] = $dbq->f('value') ? addslashes($dbq->f('value')) : addslashes(utf8_decode($dbq->f('long_value')));
		    }
		}

		sendgrid_insert_data($dbq,
			array(
                'sent_date'         => $timestamp,
                'webservice_name'   => '',
                'webservice_id'     => '',
                'module'            => '',
                'module_id'         => '',
                'webservice_incomming'   => $response_init,
                'from_webhook'		=> '1',
                'x_message_id'		=> $x_message_id
            )
        );



        switch($module){
			case 'invoice':
				$invoice_data=$dbq->query("SELECT id FROM tblinvoice WHERE id='".$module_id."' ")->getAll();
				if(!empty($invoice_data)){
					$message=get_event_message($event, $email, $reason, $url);
					/*switch ($event) {
							case 'delivered': 							
								exceptionListRemove($dbq,
									array(
										'module'	=> 'invoice',
										'module_id'	=> $invoice_data[0]['id'],
										'destination'	=> $email
									)
								);
								break;
							case 'bounce': 
								addToExceptionList(array(
					              'destination' => $email,
					              'location_id' => '',
					              'module'      => 'invoice',
					              'module_id'   =>  $invoice_data[0]['id'],
					              'source'      => 'sendgrid',
					              'message'     => 'Invoice was bounced for '.$email
					            ),$database_user);
								break;
							case 'spamreport': 
								exceptionListRemove($dbq,
									array(
										'module'	=> 'invoice',
										'module_id'	=> $invoice_data[0]['id'],
										'destination'	=> $email
									)
								);
								addToExceptionList(array(
					              'destination' => $email,
					              'location_id' => '',
					              'module'      => 'invoice',
					              'module_id'   =>  $invoice_data[0]['id'],
					              'source'      => 'sendgrid',
					              'message'     => 'Invoice was marked as spam by '.$email
					            ),$database_user);
								break;
					}*/ // end switch event

					insert_data_log($dbq,
						array(
							'pag'			=> 'invoice',
							'field_name'	=> 'invoice_id',
							'field_value'	=> $invoice_data[0]['id'],
							'message'		=> $message,
							'timestamp'		=> $timestamp
						)
					);

				}
			break;
			case 'invoice_reminder':
				if($module_multiple_ids){
					$invoiceIds = explode(',',$module_multiple_ids);
				    foreach ($invoiceIds as $invoiceId) {  
				    $message=get_event_message($event, $email, $reason, $url); 	         
			           insert_data_log($dbq,
							array(
								'pag'			=> 'invoice',
								'field_name'	=> 'invoice_id',
								'field_value'	=> $invoiceId,
								'message'		=> $message,
								'timestamp'		=> $timestamp
							)
						); 
			        }
				}	
			break;
			case 'purchase_invoice':
				$invoice_data=$dbq->query("SELECT invoice_id FROM tblinvoice_incomming WHERE invoice_id='".$module_id."' ")->getAll();
				if(!empty($invoice_data)){
					$message=get_event_message($event, $email, $reason, $url);
					insert_data_log($dbq,
						array(
							'pag'			=> 'purchase_invoice',
							'field_name'	=> 'purchase_invoice_id',
							'field_value'	=> $invoice_data[0]['invoice_id'],
							'message'		=> $message,
							'timestamp'		=> $timestamp
						)
					);

				}
			break;
			case 'order':
				$order_data=$dbq->query("SELECT order_id FROM pim_orders WHERE order_id='".$module_id."' ")->getAll();
				if(!empty($order_data)){
					$message=get_event_message($event, $email, $reason, $url);

					insert_data_log($dbq,
						array(
							'pag'			=> 'order',
							'field_name'	=> 'order_id',
							'field_value'	=> $order_data[0]['order_id'],
							'message'		=> $message,
							'timestamp'		=> $timestamp
						)
					);

				}
			break;
			case 'p_order':
				$order_data=$dbq->query("SELECT p_order_id FROM pim_p_orders WHERE p_order_id='".$module_id."' ")->getAll();
				if(!empty($order_data)){
					$message=get_event_message($event, $email, $reason, $url);

					insert_data_log($dbq,
						array(
							'pag'			=> 'p_order',
							'field_name'	=> 'p_order_id',
							'field_value'	=> $order_data[0]['p_order_id'],
							'message'		=> $message,
							'timestamp'		=> $timestamp
						)
					);

				}
			break;
			case 'quote':
				$quote_data=$dbq->query("SELECT id FROM tblquote WHERE id='".$module_id."' ")->getAll();
				if(!empty($quote_data)){
					$message=get_event_message($event, $email, $reason, $url);

					insert_data_log($dbq,
						array(
							'pag'			=> 'quote',
							'field_name'	=> 'quote_id',
							'field_value'	=> $quote_data[0]['id'],
							'message'		=> $message,
							'timestamp'		=> $timestamp
						)
					);

				}
			break;
			case 'contract':
				$contract_data=$dbq->query("SELECT contract_id FROM contracts WHERE contract_id='".$module_id."' ")->getAll();
				if(!empty($contract_data)){
					$message=get_event_message($event, $email, $reason, $url);

					insert_data_log($dbq,
						array(
							'pag'			=> 'contract',
							'field_name'	=> 'contract_id',
							'field_value'	=> $contract_data[0]['contract_id'],
							'message'		=> $message,
							'timestamp'		=> $timestamp
						)
					);

				}
			break;
			case 'service':
				$service_data=$dbq->query("SELECT service_id FROM servicing_support WHERE service_id='".$module_id."' ")->getAll();
				if(!empty($service_data)){
					$message=get_event_message($event, $email, $reason, $url);

					insert_data_log($dbq,
						array(
							'pag'			=> 'service',
							'field_name'	=> 'service_id',
							'field_value'	=> $service_data[0]['service_id'],
							'message'		=> $message,
							'timestamp'		=> $timestamp
						)
					);

				}
			break;
			case 'project':
				$project_data=$dbq->query("SELECT project_id FROM projects WHERE project_id='".$module_id."' ")->getAll();
				if(!empty($project_data)){
					$message=get_event_message($event, $email, $reason, $url);

					insert_data_log($dbq,
						array(
							'pag'			=> 'project',
							'field_name'	=> 'project_id',
							'field_value'	=> $project_data[0]['project_id'],
							'message'		=> $message,
							'timestamp'		=> $timestamp
						)
					);

				}
			break;
		}// end switch module



		$dbq->killConn();
    }



}

//Group unsubscribe / resubscribe
/*$group_sub_array = array('group_unsubscribe_email'=>array(),'group_resubscribe_email'=>array());	
foreach ($response as $k => $v) {
	$group_tmp = array();
	$group_tmp['email'] = $v->email;
	$group_tmp['timestamp'] = $v->timestamp;
	if($v->event == 'group_unsubscribe'){
		array_push($group_sub_array['group_unsubscribe_email'], $group_tmp);
	} elseif($v->event == 'group_resubscribe'){
		array_push($group_sub_array['group_resubscribe_email'], $group_tmp);
	}
}
groupSuppressionsContacts($dbq,$const,$group_sub_array);*/
//End Group unsubscribe / resubscribe

exit;

function insert_data_log($dbq,$data){
    $dbq->query("INSERT INTO logging SET pag='".$data['pag']."', message='".addslashes($data['message'])."', field_name='".$data['field_name']."', field_value='".$data['field_value']."', date='".$data['timestamp']."', type='0', reminder_date='".$data['timestamp']."' ");
}

function get_event_message($event, $email, $reason='',$url='')
{
	$event_message='';
    switch($event){
    	case 'delivered':
    		$event_message='{l}Email to{endl} '.$email.' {l}was confirmed to be delivered.{endl}';
		break;
		case 'dropped':
			$event_message='{l}Email to{endl} '.$email.' {l}was dropped on{endl}';
		break;
		case 'bounce':
			$event_message='{l}Email to{endl} '.$email.' {l}was confirmed to be bounced. Reason: {endl} '.$reason;
		break;
		case 'open':
			$event_message='{l}Email to{endl} '.$email.' {l}was opened on{endl}';
		break;
		case 'click':
			$event_message='{l}Email to{endl} '.$email.' {l}was clicked on{endl} '.$url;
		break;
		case 'spamreport':
			$event_message='{l}Email to{endl} '.$email.' {l}was confirmed to be registered as spam. Reason: {endl} '.$reason;
		break;
    }

    return $event_message;
}

/*function changeInvoiceSent($dbq,$invoice_data,$const){
	$sent_date= $invoice_data['sent_date'] ? $invoice_data['sent_date'] : time();
	$dbq->query("UPDATE tblinvoice SET sent='1',sent_date='".$sent_date."' WHERE id='".$invoice_data['id']."'");
	$p = $dbq->field("SELECT proforma_id FROM tblinvoice WHERE id='".$invoice_data['id']."' ");
	if($p){
	    $proformaInv = $dbq->query("SELECT req_payment,status,paid FROM tblinvoice WHERE id='".$p."' ");
	    $dbq->query("UPDATE tblinvoice SET sent='1', sent_date='".$sent_date."' WHERE id='".$p."' ");
	    $status = $proformaInv->f('status');
	    if($proformaInv->f('req_payment')==100){
	      $paid=$proformaInv->f('paid');
	    }else{
	      $paid=2;
	      $status=0;
	    }
	    $dbq->query("UPDATE tblinvoice SET status='".$status."', paid='".$paid."' WHERE id='".$invoice_data['id']."' ");
	}
	if($invoice_data['type'] == '2'){
      $dbq->query("UPDATE tblinvoice SET status='1', paid='1' WHERE id='".$invoice_data['id']."'");
      $amount_c = $dbq->query("SELECT amount, amount_vat FROM tblinvoice WHERE id='".$invoice_data['id']."' ");
      $amount = $dbq->field("SELECT amount FROM tblinvoice WHERE id='".$invoice_data['c_invoice_id']."' ");
      $info = '<a href="index.php?do=invoice-invoice&invoice_id='.$invoice_data['id'].'" >'.gm('Credit Invoice').'</a>';
      $payment_date = date('Y-m-d');
      $negative = 1;
      if($const['USE_NEGATIVE_CREDIT'] && $const['USE_NEGATIVE_CREDIT']==1){
        $negative = -1;
      }
      if($negative*$amount_c->f('amount') >= $amount){
        $dbq->query("UPDATE tblinvoice SET paid='1',status='1' WHERE id='".$invoice_data['c_invoice_id']."' ");

      }else{
        $dbq->query("UPDATE tblinvoice SET paid='2' WHERE id='".$invoice_data['c_invoice_id']."' ");
      }
      $already_inserted=$dbq->field("SELECT payment_id FROM tblinvoice_payments WHERE invoice_id='".$invoice_data['c_invoice_id']."' AND info='".$info."' AND credit_payment='1' ");
      if(!$already_inserted){
        $dbq->query("INSERT INTO tblinvoice_payments SET
                          invoice_id  = '".$invoice_data['c_invoice_id']."',
                          date    = '".$payment_date."',
                          amount    = '".$amount_c->f('amount_vat')."',
                          info        = '".$info."',
                          credit_payment='1'  ");
      }
    }
}*/
function exceptionListRemove($dbq,$data){
	$exception_ids=$dbq->query("SELECT id FROM exception_list WHERE `module`='".$data['module']."' AND module_id='".$data['module_id']."' AND FIND_IN_SET('".$data['destination']."',`destination`) ")->getAll();
	$exception_ids_list="";
	foreach($exception_ids as $key=>$value){
		$exception_ids_list.="'".$value['id']."',";
	}
	$exception_ids_list=rtrim($exception_ids_list,",");
	if($exception_ids_list){
		$dbq->query("DELETE FROM exception_list WHERE id IN(".$exception_ids_list.") ");
	}	
}
function sendgrid_insert_data($dbq,$data){
	$sent_date=new DateTime();
    $sent_date->setTimestamp($data['sent_date']);     
    $sent_date->setTimezone(new DateTimeZone('Europe/Brussels'));
    $sentDate=$sent_date->format('Y-m-d H:i:s');

    $dbq->query("INSERT INTO sendgrid_history SET 
        `sent_date` = '".$sentDate."',
        webservice_name = '".$data['webservice_name']."',
        webservice_id ='".$data['webservice_id']."',
        `module`       ='".$data['module']."',  
        `module_id`    ='".$data['module_id']."',     
        webservice_incomming='".addslashes(serialize($data['webservice_incomming']))."',
        from_webhook='".$data['from_webhook']."',
        webservice_response_code='200',
        x_message_id='".$data['x_message_id']."' ");
}


/*function unsubscribeContacts($dbq,$const,$response,$data){

	$contact_email_data = getInvoiceContactEmailData($dbq,$data[0]);
	$contact_email = $contact_email_data['contact_email'];
	$contact_email_list = $contact_email_data['contact_email_list'];

	$uns_group_id = $const['SENDGRID_SETTING_UNS_GROUP_ID'];

	//Only if email is a match
	if($contact_email == $response[0]->email){
		$send_email_method = 'POST';
		$webservice_name = 'asm/groups/{group_id}/suppressions';
		$call_link = '/asm/groups/'.$uns_group_id.'/suppressions';
		$sendgrid_msg = 'SendGrid: '.$response[0]->email.' unsubscribed';//gm('Agrees to be on Mailing-checkbox was unchecked');
		$recipient_emails = array('recipient_emails'=>array($contact_email));

		$contact_email_data = array('webservice_name' => $webservice_name,
        							'webservice_id' => $data[0]['contact_id'],
				                    'module' => 'contact',
				                    'module_id' => $data[0]['customer_id'], 
				                    'method'  => $send_email_method,
				                    'call_link' => $call_link,
				                    'webservice_outgoing' => $recipient_emails
		);

		$contactDataResponse = sendgridCall($contact_email_data,DATABASE_NAME);
		
		$contactData = json_decode($contactDataResponse['response']);
        if($contactDataResponse['headers']['http_code'] >= 200 && $contactDataResponse['headers']['http_code'] < 300){
        	//Unsubscribe all contacts that have the same email
        	foreach ($contact_email_list as $con) {
				$dbq->query("UPDATE customer_contactsIds SET s_email='0'
            	WHERE contact_id='".$con['contact_id']."' ");

				insert_data_log($dbq,
					array(
						'pag'			=> 'xcustomer_contact',
						'field_name'	=> 'contact_id',
						'field_value'	=> $con['contact_id'],
						'message'		=> $sendgrid_msg,
						'timestamp'		=> $response[0]->timestamp
					)
				);
			}	
        }else{
          insert_error_call(DATABASE_NAME,"sendgrid","contact",$data[0]['contact_id'],$contactDataResponse['response']);
        }
	}
}


function getInvoiceContactEmailData($dbq,$data){

	$result = array();
	$contact_email = '';

	if($data['contact_id'] && $data['buyer_id']){
		$contact_email = $dbq->field(" SELECT email FROM customer_contactsIds	WHERE contact_id='".$data['contact_id']."' and customer_id = '".$data['buyer_id']."'");
		if(!empty($contact_email)){
			$contact_email_list = $dbq->query("SELECT contact_id FROM customer_contactsIds WHERE email='".$contact_email."'")->getAll();	
		}
	}
	if($data['contact_id']){
		if(!$contact_email){
			$contact_email = $dbq->field("SELECT email FROM customer_contacts	WHERE contact_id='".$data['contact_id']."' ");
			$contact_email_list = $dbq->field("SELECT contact_id FROM customer_contacts WHERE email='".$contact_email."' ");
		}
	}

	$result['contact_email'] = $contact_email;
	$result['contact_email_list'] = $contact_email_list;

	return $result;
}

function getContactsEmailList($dbq,$contact_email){

	$contact_list = array();
	$contact_list_tmp = $dbq->query("SELECT contact_id FROM customer_contactsIds WHERE email='".$contact_email."'")->getAll();

	foreach ($contact_list_tmp as $key => $value) {
		//Get distinct contact email addresses
		if(isset($value['contact_id']) && !in_array($value['contact_id'],$contact_list)){ 
			$tmp_arr = $value['contact_id'];
        	array_push($contact_list, $tmp_arr);
		}
	}

	return $contact_list;
}


function groupSuppressionsContacts($dbq,$const,$data){

	if(!empty($data['group_unsubscribe_email'])){
		foreach ($data['group_unsubscribe_email'] as $key => $value) {
			$group_unsubscribe_list = getContactsEmailList($dbq,$value['email']);

			if(!empty($group_unsubscribe_list)){
				$data_to_send = array('list'=>$group_unsubscribe_list,
									'event'=>'group_unsubscribe',
									'contact_email'=>$value['email'],
									'tmst'=>$value['timestamp']);	
				groupSuppressionsContactsCall($dbq,$const,$data_to_send);
			}
		}
	}

	if(!empty($data['group_resubscribe_email'])){
		foreach ($data['group_resubscribe_email'] as $key => $value) {
			$group_resubscribe_list = getContactsEmailList($dbq,$value['email']);	

			if(!empty($group_resubscribe_list)){
				$data_to_send = array('list'=>$group_resubscribe_list,
									'event'=>'group_resubscribe ',
									'contact_email'=>$value['email'],
									'tmst'=>$value['timestamp']);	
				groupSuppressionsContactsCall($dbq,$const,$data_to_send);
			}	
		}
	}
}

function groupSuppressionsContactsCall($dbq,$const,$data){
	$uns_group_id = $const['SENDGRID_SETTING_UNS_GROUP_ID'];

	$contact_email = $data['contact_email'];
	$tmst = $data['tmst'];

	foreach ($data['list'] as $val) {
		$con_id = $val;

		if($data['event'] == 'group_unsubscribe'){
			$send_email_method = 'POST';
			$webservice_name = 'asm/groups/{group_id}/suppressions';
			$call_link = '/asm/groups/'.$uns_group_id.'/suppressions';
			$sendgrid_msg = 'SendGrid: '.$contact_email.' unsubscribed';
			$recipient_emails = array('recipient_emails'=>array($contact_email));
			$s_email = '0';
		} else {
			$send_email_method = 'DELETE';
			$webservice_name = 'asm/groups/{group_id}/suppressions/{email}';
			$call_link = '/asm/groups/'.$uns_group_id.'/suppressions/'.$contact_email;
			$sendgrid_msg = 'SendGrid: '.$contact_email.' resubscribed';
			$recipient_emails = array();
			$s_email = '1';
		}
		
		$contact_email_data = array('webservice_name' => $webservice_name,
        							'webservice_id' => $con_id,
				                    'module' => 'groupsuppressionscontact',
				                    'module_id' => $con_id, 
				                    'method'  => $send_email_method,
				                    'call_link' => $call_link,
				                    'webservice_outgoing' => $recipient_emails
		);

		$contactDataResponse = sendgridCall($contact_email_data,DATABASE_NAME);
		
		$contactData = json_decode($contactDataResponse['response']);
        if($contactDataResponse['headers']['http_code'] >= 200 && $contactDataResponse['headers']['http_code'] < 300){
        	//Unsubscribe all contacts that have the same email
			$dbq->query("UPDATE customer_contactsIds SET s_email='".$s_email."'
        	WHERE contact_id='".$con_id."' ");

			insert_data_log($dbq,
				array(
					'pag'			=> 'xcustomer_contact',
					'field_name'	=> 'contact_id',
					'field_value'	=> $con_id,
					'message'		=> $sendgrid_msg,
					'timestamp'		=> $tmst
				)
			);
        }else{
          insert_error_call(DATABASE_NAME,"sendgrid","groupsuppressionscontact",$con_id,$contactDataResponse['response']);
        }	
	}	*/
//}

?>


