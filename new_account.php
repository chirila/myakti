<?php
define('INSTALLPATH',__DIR__.'/');
define('BASEPATH',1);
ini_set('memory_limit','512M');
ignore_user_abort(true);
set_time_limit(0);

/************************************************************************
* @Author: Tinu Coman
***********************************************************************/
include_once(__DIR__."/core/startup_cron.php");
ini_set('display_errors', 1);
ini_set('error_reporting', 1);
error_reporting(E_ALL ^ E_NOTICE);
ini_set('error_log','cron_error.log');

global $database_config;
$db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
); 
$db_users= new sqldb($db_config);

	$response = json_decode( @file_get_contents( "php://input" ), true );
	$db_users->query("INSERT INTO account_log SET response='".addslashes(serialize($response))."', date='".time()."' ");
	
	if(!$in['lang_code']){
		$in['lang_code']='en';
	}
	$v = new validation($response);
	$v->field('trial-email', gm('Email'), 'required:min_length[6]:email:unique[users.email]',false,$db_config);
	if(!$v->run()){
		$err_messsages=array("This field is required.",
			"This field must contain at least 6 characters.",
			"This field must contain a valid email address.",
			"This value is already in use.");
        $err_en=array("We are having an issue sending a confirmation email to this email address. Make sure that this is an existing email address.",
        	"We are having an issue sending a confirmation email to this email address. Make sure that this is an existing email address.",
        	"We are having an issue sending a confirmation email to this email address. Make sure that this is an existing email address.",
        	"You already have an account for Akti in this email address, please use a different email address or contact us via phone or via support@akti.com so that we can set the account up for you.");
        $err_fr=array("Nous rencontrons un problème lors de l’envoi d’un mail de confirmation à cette adresse mail. Vérifiez si il n’y a pas de erreur dans l’adresse que vous avez introduite.",
        	"Nous rencontrons un problème lors de l’envoi d’un mail de confirmation à cette adresse mail. Vérifiez si il n’y a pas de erreur dans l’adresse que vous avez introduite.",
        	"Nous rencontrons un problème lors de l’envoi d’un mail de confirmation à cette adresse mail. Vérifiez si il n’y a pas de erreur dans l’adresse que vous avez introduite.",
        	"Vous avez déjà un compte Akti associé à cette adresse mail, introduisez une autre adresse mail ou contactez-nous pas téléphone ou sur l’adresse mail support@akti.com pour que nous puissions vous aider.");
        $err_nl=array("We ondervinden een probleem om u een bevestigingsmail te versturen naar dit mail adres. Kijk even na of er geen schrijffouten staan in het adres.",
        	"We ondervinden een probleem om u een bevestigingsmail te versturen naar dit mail adres. Kijk even na of er geen schrijffouten staan in het adres.",
        	"We ondervinden een probleem om u een bevestigingsmail te versturen naar dit mail adres. Kijk even na of er geen schrijffouten staan in het adres.",
        	"U hebt reeds een account op Akti met dit mailadres, gelieve dan een ander mailadres in te vullen of contacteer ons telefonisch of via email op support@akti.com zodat we u verder kunnen helpen.");

        $err_msg=msg::get_errors();
        $err_final="";
        if(in_array($err_msg['trial-email'], $err_messsages)){
        	switch($in['lang_code']){
	        	case 'nl':
				case 'du':
					$err_final=$err_nl[array_search($err_msg['trial-email'], $err_messsages)];
					break;
				case 'fr':
					$err_final=$err_fr[array_search($err_msg['trial-email'], $err_messsages)];
					break;
				default:
					$err_final=$err_en[array_search($err_msg['trial-email'], $err_messsages)];
					break;
	        }
        }else{
        	$err_final=$err_msg['trial-email'];
        }
        $data=array('data'=>$response,'error'=>array('trial-email'=>$err_final));
		echo json_encode($data);
		exit();
	};
	$in['email']=$response['trial-email'];
	switch($in['lang_code']){
		case 'nl':
		case 'du':
			$in['lang_id']=1;
			break;
		case 'fr':
			$in['lang_id']=3;
			break;
		case 'de':
		case 'ge':
			$in['lang_id']=4;
			break;
		default:
			$in['lang_id']=2;
			break;
	}
	if($response['accountant']){
		$in['accountant_id'] = $response['accountant'];
	}
	require_once (__DIR__.'/libraries/class.phpmailer.php');
    include_once (__DIR__.'/libraries/class.smtp.php');
	$mail = new PHPMailer();
	$mail->WordWrap = 50;
	$fromMail='noreply@akti.com';
	$mail->SetFrom($fromMail, 'Akti');	
	$forgot_email = include_once(__DIR__.'/apps/misc/controller/activation_email.php');
	$body=$forgot_email;
    $body=str_replace('[!FIRSTNAME!]',$response['trial-firstname'],$body);
	$body=str_replace('[!LASTNAME!]',$response['trial-lastname'],$body);
    $body=str_replace('[!LINK_VALIDATE!]',"<a href='".$site."'>".$site."</a>",$body);
	$mail->Subject = $mail_subject;
	$mail->MsgHTML($body);
	$mail->AddAddress($in['email']);
	$mail->Send();
	switch($in['lang_code']){
		case 'nl':
		case 'du':
			$success_msg='Een bevestigingsemail is verstuurd naar '.$in['email'].'. Vanuit deze email zal u uw account kunnen opstarten.';
			break;
		case 'fr':
			$success_msg='Un mail de confirmation a été envoyé à '.$in['email'].'. A partir de cette email, vous pouvez initialiser votre compte.';
			break;
		default:
			$success_msg='A confirmation email has been sent to '.$in['email'].'. From this email you will be able to set up your account.';
			break;
	}
	$data=array('data'=>$response,'success'=>array('success'=>$success_msg));
	echo json_encode($data);
	exit();

?>