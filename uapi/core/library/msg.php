<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class msg {
	
	// Field Related Messages
	public static $msg = array();
	
	// All Error Messages occured during execution
    public static $error;
    public static $errors = array();

    // All Notice Messages occured during execution
	public static $notice;
    public static $notices = array();

    // All Success Messages occured during execution
	public static $success;
    public static $successes = array();

    public static function error($msg, $field) {
		self::$error .= '<div>'.$msg.'</div>';
        self::$errors[$field] = $msg;
	}

	public static function notice($msg, $field) {
		self::$notice .= '<div>'.$msg.'</div>';
        self::$notices[$field] = $msg;
	}

	public static function success($msg, $field) {
		self::$success .= '<div>'.$msg.'</div>';
        self::$successes[$field] = $msg;
	}
	
	public static function set($field, $msg, $type='error') {
		self::$msg[$field] = $msg;
		switch ($type) {
			case 'error': self::error($msg, $field); break;
			case 'notice': self::notice($msg, $field); break;
			case 'success': self::success($msg, $field); break;
		}
	}
	
	public static function get_messages($as_string = false){
		$tmp = array();
		if(count(self::$msg) > 0)
		{
			$tmp['msg'] = self::$msg;
		}
		
		if(count(self::$errors) > 0){
            if(!$as_string)
                $tmp['error'] = self::$errors;
            else
                $tmp['error'] = self::$error;
		}

        if(count(self::$notices) > 0){
            if(!$as_string)
                $tmp['notice'] = self::$notices;
            else
                $tmp['notice'] = self::$notice;
        }

        if(count(self::$successes) > 0){
            if(!$as_string)
                $tmp['success'] = self::$successes;
            else
                $tmp['success'] = self::$success;
        }

        return $tmp;
	}

    public static function get_errors($as_string = false){
        if(count(self::$errors) > 0){
            if(!$as_string)
                return self::$errors;
            else
                return self::$error;
        } else {
            return false;
        }
    }

    public static function get_error($as_string = false){
        return self::get_errors($as_string);
    }

    public static function get_notices($as_string = false){
        if(count(self::$notices) > 0){
            if(!$as_string)
                return self::$notices;
            else
                return self::$notice;
        } else {
            return false;
        }
    }

    public static function get_notice($as_string = false){
        return self::get_notices($as_string);
    }

    public static function get_successes($as_string = false){
        if(count(self::$successes) > 0){
            if(!$as_string)
                return self::$successes;
            else
                return self::$success;
        } else {
            return false;
        }
    }

    public static function get_success($as_string = false){
        return self::get_successes($as_string);
    }

}
