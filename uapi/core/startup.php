<?php   if(!defined('BASEPATH')) exit('No direct script access allowed');

require(INSTALLPATH.'libraries/gen_lib.php');

date_default_timezone_set('Europe/Bucharest');
session_start();

$request_method = $_SERVER['REQUEST_METHOD'];
$allowd_methods = array("GET","POST","PUT","DELETE","OPTIONS");

if(!in_array($request_method,$allowd_methods)){
    http_response_code(401);
    exit();
    // block unwanted request methods
}

// build the $in array
if(is_array($_GET)){
    $in = cleanInputs($_GET);
    unset($_GET);
}

if(is_array($_POST)){
    $in = array_merge($in,cleanInputs($_POST));
    unset($_POST);
}

$json_vars = json_decode( @file_get_contents( "php://input" ), true );

if($json_vars){
    $in = array_merge($in,cleanInputs($json_vars));
}

$header = getallheaders();
if(isset($header['token'])){
    $in['token'] =$header['token'];
}

if(isset($header['auth_key'])){
    $in['auth_key'] =$header['auth_key'];
}

if(isset($header['auth-key'])){
    $in['auth_key'] =$header['auth-key'];
}


if(isset($header['language'])){
    $in['language'] =$header['language'];
}

require(BASEPATH.'common.php');

if(isset($in['lang_id'])){
    $_SESSION['lang_id'] = $in['lang_id'];
}

function cleanInputs($data){
    $clean_input = array();
    if(is_array($data)){
        foreach($data as $k => $v){
            $clean_input[$k] = cleanInputs($v);
        }
    }elseif(is_object($data)){
        $data = (array)$data;
        foreach($data as $k => $v){
            $clean_input[$k] = cleanInputs($v);
        }
    }
    else{
        if(get_magic_quotes_gpc()){
            $data = trim(stripslashes($data));
        }
        $clean_input = addslashes(trim($data));
    }
    return $clean_input;
}
define("LOG_QUERYS", 'NO');

$_db=new sqldb();
$_db->query("SET names utf8");
$_db->query("SET sql_mode= ''");
$_db->query("SELECT constant_name, value, type, long_value FROM settings");
while($_db->move_next())
{
    if($_db->f('constant_name') && $_db->f('type')  == 1)
    {
        if(!defined($_db->f('constant_name'))){
            if($_db->f('value')==''){
                define($_db->f('constant_name'),'0');
            }else{
                define($_db->f('constant_name'),$_db->f('value'));
            }           
        }
    }else{
        if(!defined($_db->f('constant_name'))){
            define($_db->f('constant_name'),$_db->f('long_value'));
        }
    }
}

### apps ###
$_db->query("SELECT * FROM apps WHERE type='main' ");
while ($_db->move_next()) {
    if($_db->f('name') && $_db->f('api')){
        define(str_replace(' ',"_",strtoupper($_db->f('name'))),$_db->f('api'));
        define(str_replace(' ',"_",strtoupper($_db->f('name')))."_ACTIVE",$_db->f('active'));
    }
}
$def_lang = $_db->field("SELECT lang_id FROM pim_lang WHERE default_lang=1");
if(!$def_lang){
    $user_lang = $_SESSION['l'];
    if ($user_lang=='nl')
    {
        $user_lang='du';
    }
    switch ($user_lang) {
        case 'en':
            $user_lang_id=1;
            break;
        case 'du':
            $user_lang_id=3;
            break;
        case 'fr':
            $user_lang_id=2;
            break;
        default:
            $user_lang_id=2;
            break;
    }
    $def_lang = $user_lang_id;
}

// $_db->move_next();
define('DEFAULT_LANG_ID',$def_lang);

# load the language translations
switch (true) {
    case isset($_SESSION['l']):
        $language = $_SESSION['l'];
        break;
    case isset($_COOKIE["l"]):
        $language = $_COOKIE["l"];
        break;
    default:
        $language = LANGUAGE_TR;
        break;
}
$lang = array();
$db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
);
$db_users = new sqldb($db_config);
$languageLabels = $db_users->query("SELECT id, ".strtolower($language)." FROM langs ")->getAll();
foreach ($languageLabels as $key => $value) {
    $lang[$value['id']] = $value[strtolower($language)];
}
//JWT start
ark::loadLibraries(array('jwt/JWT'));
$JWT_PSWD=$db_users->field("SELECT value FROM settings WHERE constant_name='JWT_PSWD'");
$AWS_FACTORY_DATA=$db_users->field("SELECT long_value FROM settings WHERE constant_name='AWS_FACTORY'");
try{
    $AWS_FACTORY=JWT::decode($AWS_FACTORY_DATA, $JWT_PSWD, array('HS256'));
    define('AWS_FACTORY_KEY',$AWS_FACTORY->data->key);
    define('AWS_FACTORY_SECRET',$AWS_FACTORY->data->secret);
}catch(ExpiredException $e){
    //
}catch(Exception $e){
    //    
}
//JWT end

$p_access = array();
$user_plan = 0;
$user_start=0;


/* End of file startup.php */