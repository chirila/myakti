<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

global $config;
if(!empty($in['start'])){
	$in['start_date'] =strtotime($in['start']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['end'])){
	$in['stop_date'] =strtotime($in['end']);
	$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}

$l_r = ROW_PER_PAGE;

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
$order_by_array = array('serial_number','t_date','buyer_name','our_ref','amount_vat','amount');
$arguments_o='';
$arguments_s='';
//print_r($_SESSION); exit();

if($_SESSION['customer_id']){
    $in['customer_id'] = $_SESSION['customer_id'];
    $in['buyer_id'] = $_SESSION['customer_id'];
}

if($_SESSION['contact_id']){
    $in['contact_id'] = $_SESSION['contact_id'];
} else {
    exit();
}

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$arguments='';
$filter = "WHERE 1=1 AND tblinvoice.contact_id='".$in['contact_id']."' ";
$filter_link = 'block';
$order_by = " ORDER BY t_date DESC, iii DESC ";
if($in['buyer_id']){
	$filter .=" AND tblinvoice.buyer_id='".$in['buyer_id']."' ";
	$arguments .='&buyer_id='.$in['buyer_id'];
}
if($in['view'] != 2){
	if(empty($in['archived'])){
		$filter.= " AND f_archived='0' ";
	}else{
		$filter.= " AND f_archived='1' ";
		$arguments.="&archived=".$in['archived'];
	}
}

if(!empty($in['filter'])){
	$arguments.="&filter=".$in['filter'];
}
if(!empty($in['c_invoice_id'])){
	$filter.=" and (tblinvoice.c_invoice_id = '".$in['c_invoice_id']."')";
	$arguments.="&c_invoice_id=".$in['c_invoice_id'];

}
if(!empty($in['search'])){
	$filter.=" AND (tblinvoice.serial_number LIKE '%".$in['search']."%' OR tblinvoice.our_ref LIKE '%".$in['search']."%' )";
	$arguments_s.="&search=".$in['search'];
}
if(!empty($in['order_by'])){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == 'true'){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		/*$view_list->assign(array(
			'on_'.strtolower($in['order_by']) 	=> $in['desc'] ? 'on_asc' : 'on_desc',
		));*/
	}
}
if(!isset($in['view'])){
	$in['view'] = 0;
}
	if($in['view'] == 0){
		// $view_list->assign('selected_all','class="selected"');

	}
	if($in['view'] == 1){
		$filter.=" and tblinvoice.not_paid = '0' and tblinvoice.status = '0' AND sent='0' ";
		$arguments.="&view=1";
		// $view_list->assign('selected_draft','class="selected"');
	}
	if($in['view'] == 2){
		$filter.=" and tblinvoice.not_paid = '0' and tblinvoice.type = '1' ";
		$arguments.="&view=2";
		// $view_list->assign('selected_proforma','class="selected"');
		if(empty($in['archived'])){
			$filter.= " AND (f_archived='0' OR f_archived='3')";
		}else{
			$filter.= " AND (f_archived='1' OR f_archived='3') ";
			$arguments.="&archived=".$in['archived'];
		}
	}
	/*if($in['view'] == 3){
		$filter.=" and tblinvoice.type = '0' ";
		$arguments.="&view=3";
		$view_list->assign('selected_regular','class="selected"');
	}*/
	if($in['view'] == 3){
		$filter.=" and tblinvoice.not_paid = '0' and tblinvoice.type = '2' ";
		$arguments.="&view=3";
		// $view_list->assign('selected_credit','class="selected"');
	}
	/*if($in['view'] == 5){
		$filter.=" and tblinvoice.sent = '1' ";
		$arguments.="&view=5";
		$view_list->assign('selected_sent','class="selected"');
	}*/
	if($in['view'] == 4){
		$filter.=" and tblinvoice.not_paid = '0' AND tblinvoice.status = '0' and tblinvoice.sent = '1' and tblinvoice.due_date < ".time();
		$arguments.="&view=4";
		// $view_list->assign('selected_late','class="selected"');
	}
	if($in['view'] == 7){
		$filter.=" and tblinvoice.not_paid = '1' ";
		$arguments.="&view=7";
		// $view_list->assign('selected_not_paid','class="selected"');
	}
	if($in['view'] == 5){
		$filter.=" and tblinvoice.not_paid = '0' and tblinvoice.status = '0' and tblinvoice.sent = '1' ";
		$arguments.="&view=5";
		// $view_list->assign('selected_open','class="selected"');
	}
	if($in['view'] == 8){
		$filter.=" and tblinvoice.not_paid = '0' and tblinvoice.paid = '1' and tblinvoice.status = '1' and tblinvoice.sent = '1' ";
		$arguments.="&view=5";
		// $view_list->assign('selected_open','class="selected"');
	}
	if($in['view'] == 6){
		$filter.=" and tblinvoice.not_paid = '0' and tblinvoice.sent != '0' ";
		$arguments.="&view=6";
		// $view_list->assign('selected_final','class="selected"');
	}
// }
if(!empty($in['start_date']) && !empty($in['stop_date'])){
	/*list($d, $m, $y) = explode($spliter,  $in['start_date']);
	$start_date= mktime(0, 0, 0, $m, $d, $y);
	list($d, $m, $y) = explode($spliter,  $in['stop_date']);
	$stop_date= mktime(23, 59, 59, $m, $d, $y);*/
	$filter.=" and tblinvoice.invoice_date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
	$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
	// $view_list->assign('start_date',$in['start_date']);
	// $view_list->assign('stop_date',$in['stop_date']);
	$filter_link = 'none';
}
else if(!empty($in['start_date'])){
	/*list($d, $m, $y) = explode($spliter,  $in['start_date']);
	$start_date= mktime(0, 0, 0, $m, $d, $y);
	$stop_date= mktime(23, 59, 59, $m, $d, $y);*/
//	$filter.=" and tblinvoice.invoice_date BETWEEN '".$start_date."' and '".$stop_date."' ";
	$filter.=" and cast(tblinvoice.invoice_date as signed) > ".$in['start_date']." ";
	$arguments.="&start_date=".$in['start_date'];
	// $view_list->assign('start_date',$in['start_date']);
}
else if(!empty($in['stop_date'])){
	/*list($d, $m, $y) = explode($spliter,  $in['stop_date']);
	$start_date= mktime(0, 0, 0, $m, $d, $y);
	$stop_date= mktime(23, 59, 59, $m, $d, $y);*/
//	$filter.=" and tblinvoice.invoice_date BETWEEN '".$start_date."' and '".$stop_date."' ";
	$filter.=" and cast(tblinvoice.invoice_date as signed) < ".$in['stop_date']." ";
	$arguments.="&stop_date=".$in['stop_date'];
	// $view_list->assign('stop_date',$in['stop_date']);
	$filter_link = 'none';
}
$arguments = $arguments.$arguments_s;
$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;
//$db->query("SELECT tblinvoice.*, CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) as iii

$tblinvoice = $db->query("SELECT tblinvoice.*, serial_number as iii, cast( FROM_UNIXTIME( `invoice_date` ) AS DATE ) AS t_date, mandate.sepa_number
			FROM tblinvoice
			LEFT JOIN mandate ON tblinvoice.mandate_id = mandate.mandate_id ".$filter.$order_by );
//pana aici
$max_rows=$tblinvoice->records_count();
$result = array('invoice'=>array(),'results'=>$max_rows);
$tblinvoice->move_to($offset*$l_r);
$j=0;
$color = '';
$all_invoices_id = '';
while($tblinvoice->move_next() && $j<$l_r){
	$status='';
	$status_title='';
	$type='';
	$type_title='';
	$color='';
	switch ($tblinvoice->f('type')){
		case '0':
			$type = 'regular_invoice';
			$type_title = gm('Regular invoice');
			break;
		case '1':
			$type = 'pro-forma_invoice';
			$type_title = gm('Proforma invoice');
			break;
		case '2':
			$type = 'credit_invoice';
			$type_title = gm('Credit Invoice');
			break;
	}

	switch ($tblinvoice->f('status')){
		case '0':
			if($tblinvoice->f('sent') == '0'){
				$status_b = 'draft';
			}
			else{

				if($tblinvoice->f('paid') == '2' ){
					$status_b = 'partial';
				}else {
					$status_b = 'final';
				}
			}
			break;
		case '1':
			$tblinvoice->f('type')=='2'? $status_b = 'final': $status_b = 'fully';
			break;
		default:
			$status_b = 'draft';
			break;
	}

	if($tblinvoice->f('sent') == '0' && $tblinvoice->f('type')!='2'){
				// $status = 'Draft';
					/*if($tblinvoice->f('type')=='2'){
						$type='credit_draft';
						$type_title = gm('Credit Invoice');
					}else{*/
						$type = 'draft_invoice';
						$type_title = gm('Draft');
						$nr_status=1;
					/*}*/
					$status='';
					$status_title= '';
					$color='';
					$class='';
	}else if($tblinvoice->f('status')=='1'){
		if($tblinvoice->f('type')==2){
			$type_title = gm('Credit Invoice');
			$nr_status=3;	
		}else{
			$type_title = gm('Paid');
			$nr_status=3;	
		}

	}else if($tblinvoice->f('sent')!='0' && $tblinvoice->f('not_paid')=='0'){
		$type_title = gm('Final');
		$nr_status=2;
	}
	if($tblinvoice->f('sent') == '0' && $tblinvoice->f('type')=='2'){
				// $status = 'Draft';
					/*if($tblinvoice->f('type')=='2'){
						$type='credit_draft';
						$type_title = gm('Credit Invoice');
					}else{*/
						$type = 'draft_invoice';
						$type_title = gm('Credit Invoice');
						$nr_status=1;
					/*}*/
					$status='';
					$status_title= '';
					$color='';
					$class='';
	}

	if($tblinvoice->f('created_by') == 'System'){
		$color .= ' recurring';
	}
	if($tblinvoice->f('pdf_layout')){
		$link_end='&type='.$tblinvoice->f('pdf_layout').'&logo='.$tblinvoice->f('pdf_logo');
	}else{
		$link_end = '&type='.ACCOUNT_INVOICE_PDF_FORMAT;
	}
	#if we are using a customer pdf template
	if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $tblinvoice->f('pdf_layout') == 0){
		$link_end = '&custom_type='.ACCOUNT_INVOICE_PDF_FORMAT;
	}

	$tblinvoice2 = $db->query("SELECT id,c_invoice_id from tblinvoice where c_invoice_id='".$tblinvoice->f('id')."'");
	$proforma_has_invoice = $db->field("SELECT id FROM tblinvoice WHERE proforma_id='".$tblinvoice->f('id')."' ");

	// and tblinvoice.not_paid = '0' AND tblinvoice.status = '0' and tblinvoice.sent = '1' and tblinvoice.due_date < ".time()
	$late = false;
	if($tblinvoice->f('not_paid') == '0' && $tblinvoice->f('status') == '0' && $tblinvoice->f('sent') == '1' && $tblinvoice->f('due_date') < time()){
		$late = true;
	}

	// $view_list->assign(array(
	$item=array(
		'sent'				    => ($tblinvoice->f('sent') == 1 ? false : true),
		'credited'				=> $tblinvoice2->move_next() ? true : false,
		//'can_archive'			=> $tblinvoice->f('f_archived')==1 ? false : ($tblinvoice->f('sent') == 1 ? false : true),
		//'can_activate'			=> $tblinvoice->f('f_archived')==1 ? true : false,
		'can_pay'				=> $tblinvoice->f('f_archived')==1 ? false : ((isset($in['archived']) && $in['archived'] == 1) ? false : ($tblinvoice->f('sent') == 1 ? ($tblinvoice->f('paid') == 1 && $tblinvoice->f('status') == 1 ? false : ( !$proforma_has_invoice ? true : false)) : false)),
		'type'					=> $type,
		'invoice_type'			=> $tblinvoice->f('type'),
		'c_invoice_id'			=> $tblinvoice->f('c_invoice_id'),
		'type_title'			=> $type_title,
		'nr_status'				=> $nr_status,
		'status'				=> $status,
		'class'					=> $class,
		'email_language'		=> $tblinvoice->f('email_language'),
		'status_title'			=> $status_title,
		'invoice_property'		=> $color,
		'fully'					=> $status_b != 'draft' && $status_b != 'fully' ? true : false,
		'paid'					=> $status_b == 'fully' ? true : false,
		'bpaid'					=> $tblinvoice->f('not_paid') == '0' && $tblinvoice->f('status') < '2' && $tblinvoice->f('sent') == '1' && $tblinvoice->f('due_date') < time(),
		'no_regular_invoice'	=> $tblinvoice->f('proforma_id') ? false : true,
		'no_credit'				=> $tblinvoice->f('type')== 2 ? false : true,
		'bePaid_debt_done'		=> $tblinvoice->f('bPaid_debt_id')!='' ? true : false,
		'amount'				=> place_currency(display_number($tblinvoice->f('amount'))),
		'amount_vat'			=> place_currency(display_number($tblinvoice->f('amount_vat'))),
		'our_reference'			=> $tblinvoice->f('our_ref'),
		'mandate_reference'		=> $tblinvoice->f('sepa_number'),
		'id'         	    	=> $tblinvoice->f('id'),
		'inv_type'			    => $in['view']?'':'all',
		'number'		     	=> $tblinvoice->f('iii') ? $tblinvoice->f('iii') : '',
		'created'         		=> date(ACCOUNT_DATE_FORMAT,$tblinvoice->f('invoice_date')),
		'seller_name'      		=> $tblinvoice->f('seller_name'),
		'buyer_name'       		=> $tblinvoice->f('buyer_name'),
		'late'					=> $late,
		//'edebex_show'			=> ($tblinvoice->f('amount_vat')>=5000 && $tblinvoice->f('due_days')>=30) ? true : false,
		//'edebex_status'			=> $tblinvoice->f('edebex_id') !='' ? true : false
	);
	array_push($result['invoice'], $item);
	$all_invoices_id = $all_invoices_id.','.$tblinvoice->f('id');
	// $view_list->loop('invoice_row');
	$j++;

}

	$result['seller_data']=array(
		'invoice_seller_b_address'	=> ACCOUNT_DELIVERY_ADDRESS,
		'invoice_seller_b_city'		=> ACCOUNT_DELIVERY_CITY,
		'invoice_seller_b_zip'		=> ACCOUNT_DELIVERY_ZIP,
		'invoice_seller_b_country_id'	=> ACCOUNT_DELIVERY_COUNTRY_ID,
		'invoice_seller_name'		=> ACCOUNT_COMPANY,
		'invoice_vat_no'			=> ACCOUNT_VAT_NUMBER,
		'invoice_seller_iban'		=> ACCOUNT_IBAN,
		'invoice_seller_swift'		=> ACCOUNT_BIC_CODE,
		'invoice_seller_email'		=> ACCOUNT_EMAIL
	);


json_out($result);
?>