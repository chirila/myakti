<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $apps;
global $p_access;

perm::model('all','all', 'admin',in_array($apps['invoice'], $p_access));
perm::model('all','all', 'user',in_array($apps['invoice'], $p_access));

if(defined('WIZZARD_COMPLETE') && WIZZARD_COMPLETE=='0'){
	// echo "string";
	perm::controller('all','all', 'admin',false);
	perm::controller('all','all', 'user',false);
}