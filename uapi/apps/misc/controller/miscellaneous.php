<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
$result =array();

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}
$data = explode(',', $in['data']);
foreach ($data as $value) {
	$func = 'get_'.$value;
	if(function_exists($func)){
		$result[$value] = $func();
	}
}

json_out($result);

function get_country_dd($in){
    return build_country_list();
}

function get_user_dd($in){
    return getAccountManager();
}

function get_sales_dd($in){
    return getSales();
}

function get_c_type_dd($in){
    return getCustomerType();
}

function get_lead_source_dd($in){
    return build_l_source_dd();
}

function get_legal_type_dd($in){
    return build_l_type_dd();
}

function get_sector_dd($in){
    return build_s_type_dd();
}

function get_activity_dd($in){
    return build_c_activity_dd();
}

function get_language_dd($in){
    return build_language_dd_new();
}
