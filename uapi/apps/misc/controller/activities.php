<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
        json_out($fname($in));
    }else{
        msg::error('Function does not exist','error');
        json_out($in);
    }
}

/**
 * Activities
 *
 * @return array
 * @author PM
 **/
function get_Activity($in)
{
    $db = new sqldb();
    global $config;

    if($in['from_date']){
        $in['from_date'] = strtotime($in['from_date']);
    }
    if($in['to_date']){
        $in['to_date'] = strtotime($in['to_date']);
    }

    if(!$in['user_id'])
    {
        $in['user_id'] = $_SESSION['u_id'];
    }

    $result = array( 'item' => array() );

    $now = time();

    $limit = "";

    if( isset($in['offset']) ){
        $limit = " LIMIT ".($in['offset']*5)." ,5";
    }

    $filter_u = ' 1=1 ';
    $filter_u2 = ' ';
    if($in['user_id']){
        $filter_u2 = " AND user_id='".$in['user_id']."' ";
    }
    $filter_c = '';
    $filter_q = '';
    $join = '';


    if($in['customer2_id']){
        $in['customer_id'] = $in['customer2_id'];
    }

    switch ($in['filter_type']) {
        case 'date':
            if($in['from_date'] && !$in['to_date'])
            {
                $filter_c = "AND logging.due_date>'".$in['from_date']."'";
            }elseif(!$in['from_date'] && $in['to_date'])
            {
                $filter_c = "AND logging.due_date<'".$in['to_date']."'";
            }elseif($in['from_date'] && $in['to_date'])
            {
                $filter_c = "AND logging.due_date<'".$in['to_date']."' AND logging.due_date>'".$in['from_date']."'";
            }else
            {
                $filter_c = '';
            }
            if($in['customer_id']){
                $filter_c .= " AND customer_contact_activity.customer_id='".$in['customer_id']."'";
            }
            break;
        case 'event':
            if($in['event_type'])
            {
                $filter_c = "AND contact_activity_type='".$in['event_type']."'";
                if($in['event_type']!=7)
                {
                }
            }
            if($in['c_id']){
                $filter_c .= "AND customer_contact_activity.customer_id='".$in['c_id']."'";
            }
            break;
        case 'customer':
            if($in['customer_id'])
            {
                $filter_c = "AND customer_contact_activity.customer_id='".$in['customer_id']."'";
            }
            break;
        case 'user_timeline':
            if($in['from_date'] && !$in['to_date']){
                $filter_c = " AND logging.due_date>'".$in['from_date']."' ";
            }elseif(!$in['from_date'] && $in['to_date']){
                $filter_c = " AND logging.due_date<'".$in['to_date']."' ";
            }elseif($in['from_date'] && $in['to_date']){
                $filter_c = " AND logging.due_date<'".$in['to_date']."' AND logging.due_date>'".$in['from_date']."' ";
            }
            if($in['customer_id']){
                $filter_c .= " AND customer_contact_activity.customer_id='".$in['customer_id']."' ";
            }
            $filter_u = " email_to='".$in['user_id']."' ";
            break;
    }

    if($in['contact_id2']){
        $in['contact_id'] = $in['contact_id2'];
    }

    if($in['contact_id']){
        $filter_c .= " AND customer_contact_activity.contact_id='".$in['contact_id']."' OR (
					customer_contact_activity.contact_id IS NULL
					AND customer_contact_activity_contacts.contact_id =".$in['contact_id']."
					) ";
        $join = 'LEFT JOIN customer_contact_activity_contacts ON customer_contact_activity.`customer_contact_activity_id` = customer_contact_activity_contacts.activity_id AND action_type = 0';
    }

    // $result['page_title']=get_user_name($in['user_id']);

    $i=0;
    $customers_id = array();
    if( isset($in['offset']) ){
        $result['max_rows'] = $db->field("SELECT COUNT( DISTINCT customer_contact_activity_id )  FROM customer_contact_activity
						{$join}
						INNER JOIN logging_tracked ON customer_contact_activity.customer_contact_activity_id=logging_tracked.activity_id
						LEFT JOIN logging ON logging_tracked.log_id=logging.log_id
						WHERE {$filter_u} {$filter_c} ");
    }
    $comments = $db->query("SELECT customer_contact_activity.* FROM customer_contact_activity
					{$join}
					INNER JOIN logging_tracked ON customer_contact_activity.customer_contact_activity_id=logging_tracked.activity_id
					LEFT JOIN logging ON logging_tracked.log_id=logging.log_id
					WHERE {$filter_u} {$filter_c}
					GROUP BY customer_contact_activity.customer_contact_activity_id ORDER BY logging.due_date DESC {$limit} ")->getAll();

    // $q_ids = '';
    //
    // $q = array(); //for quotes

    // $id = rtrim($id,',');

    /*function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = strtolower($row[$col]);
        }
        array_multisort($sort_col, $dir, $arr,SORT_STRING);
    }*/

    // array_sort_by_column($result,'date',SORT_DESC);

    $days = array();
    $contact_name = array();
    // $user_name = array();
    $users_id = array();
    $w = 0;
    foreach ($comments as $key => $value) {
        $new_event = '';
        $source='-';
        $stage = '-';
        $type='-';
        $side = 'left';
        $activity_status='';
        $color='';
        $assigned = '';
        $type_of_call='';
        $type_of_call_id='';
        $status_change = 0;
        if($value['customer_contact_activity_id'])
        {
            $db->query("SELECT * FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' ".$filter_u2." ");
            $db->move_next();
            $un_log_id = $db->f('log_id');
            if($db->f('seen')=='0')
            {
                $new_event = 'new_event';
                $db->query("UPDATE logging_tracked SET seen='1' WHERE activity_id='".$value['customer_contact_activity_id']."' ".$filter_u2." ");
            }
            $log_assigned=$db->query("SELECT finished, due_date, to_user_id FROM logging WHERE log_id='".$un_log_id."' ");
            if(!array_key_exists($log_assigned->f('to_user_id'), $users_id)){
                $users_id[$log_assigned->f('to_user_id')] = get_user_name($log_assigned->f('to_user_id'));
            }
            $assigned = $users_id[$log_assigned->f('to_user_id')];
        }
        elseif($value['log_id']){
            $db->query("SELECT * FROM logging_tracked WHERE log_id='".$value['log_id']."' ".$filter_u2." ");
            $db->move_next();
            if($db->f('seen')=='0')
            {
                $new_event = 'new_event';
                $db->query("UPDATE logging_tracked SET seen='1' WHERE log_id='".$value['log_id']."' ".$filter_u2." ");
            }
        }
        else{
            if($value['sent_date']){
                $log_id = $db->field("SELECT log_id FROM logging WHERE field_name='quote_id' AND field_value='".$value['quote_id']."' AND date='".$value['sent_date']."'");
                if($log_id){
                    $db->query("SELECT * FROM logging_tracked WHERE log_id='".$log_id."' ".$filter_u2." ");
                    $db->move_next();
                    if($db->f('seen')=='0')
                    {
                        $new_event = 'new_event';
                        $db->query("UPDATE logging_tracked SET seen='1' WHERE log_id='".$log_id."' ".$filter_u2." ");
                    }
                }
            }
        }

        switch ($value['contact_activity_type']) {
            case '1':
                $action = 'email';
                $status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
                $status_values->move_next();
                $db->query("SELECT log_id,finished, due_date,status_other FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
                if($db->f('finished') == 1){
                    $activity_status = gm('Completed');
                    $color = 'green_status';
                    $status_change = 1;
                }else{
                    $activity_status = gm('Scheduled');
                    $color = 'opened_status';
                    $status_change = 0;
                }
                $log_code=$db->f('log_id');
                break;
            case '2':
                $action = 'events';
                $status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
                $status_values->move_next();
                $db->query("SELECT log_id,finished, due_date,status_other FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
                if($db->f('finished') == 1){
                    $activity_status = gm('Completed');
                    $color = 'green_status';
                    $status_change = 1;
                }else{
                    $activity_status = gm('Scheduled');
                    $color = 'opened_status';
                    $status_change = 0;
                }
                $log_code=$db->f('log_id');
                break;
            case '3':
                $action = 'fax';
                break;
            case '4':
                $action = 'meeting';
                $status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
                $status_values->move_next();
                $db->query("SELECT log_id,finished, due_date,status_other FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
                if($db->f('finished') == 1){
                    $activity_status = gm('Completed');
                    $color = 'green_status';
                    $status_change = 1;
                }else{
                    $activity_status = gm('Scheduled');
                    $color = 'opened_status';
                    $status_change = 0;
                }
                $log_code=$db->f('log_id');
                break;
            case '5':
                $action = 'phone';
                $status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
                $status_values->move_next();
                $db->query("SELECT log_id,finished, due_date,status_other,type_of_call FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
                if($db->f('finished') == 1){
                    $activity_status = gm('Completed');
                    $color = 'green_status';
                    $status_change = 1;
                }else{
                    $activity_status = gm('Scheduled');
                    $color = 'opened_status';
                    $status_change = 0;
                }
                if($db->f('type_of_call') == 1){
                    $type_of_call = gm('incoming call');
                    $type_of_call_id = 1;

                }else{
                    $type_of_call = gm('outgoing call');
                    $type_of_call_id = 0;

                }
                $log_code=$db->f('log_id');
                break;
            default:
                $action = 'other';
                $status_values=$db->query("SELECT log_id FROM logging_tracked WHERE activity_id='".$value['customer_contact_activity_id']."' AND user_id='".$value['email_to']."'");
                $db->query("SELECT log_id, finished, reminder_date,status_other FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
                $status_change = 0;
                if($db->f('finished') == 1){
                    $activity_status = gm('Completed');
                    $color = 'green_status';
                    $status_change = 1;
                }else if($db->f('reminder_date') < time()){
                    $activity_status = gm('Late');
                    $color = 'red_status';
                }else if($db->f('status_other') == 3){
                    $activity_status = gm('On hold');
                    $color = 'opened_status';
                }else if($db->f('status_other') == 2){
                    $activity_status = gm('In progress');
                    $color = 'opened_status';
                }else{
                    $activity_status = gm('New');
                    $color = 'opened_status';
                }
                $log_code=$db->f('log_id');
                break;
        }

        $message = nl2br($value['contact_activity_note']);
        // $long_message = $value['contact_activity_note'];
        if($value['email_to'] && !array_key_exists($value['email_to'], $user_id)) {
            $user_id[$value['email_to']] = get_user_name($value['email_to']);
        }
        $name = $user_id[$value['email_to']];
        /*if($value['version_id']){
            $name = '';
            $side = 'left';
            if($value['date'] == '0'){
                continue;
            }

            $action = 'quote_sent';
            $message = gm('Quote').': <a href="index.php?do=quote-quote&quote_id='.$value['quote_id'].'">'.$q[$value['quote_id']]['serial_number'].'['.$value['version_code'].']</a> '.gm('was sent');
            if($q[$value['quote_id']]['stage']){
                $stage = $db->field("SELECT name FROM tblquote_stage WHERE id = '".$q[$value['quote_id']]['stage']."' ");
            }
            if($q[$value['quote_id']]['source']){
                $source = $db->field("SELECT name FROM tblquote_source WHERE id = '".$q[$value['quote_id']]['source']."' ");
            }
            if($q[$value['quote_id']]['type']){
                $type = $db->field("SELECT name FROM tblquote_type WHERE id = '".$q[$value['quote_id']]['type']."' ");
            }
            if($q[$value['quote_id']]['autor']){
                $name =  get_user_name($q[$value['quote_id']]['autor']);
            }
        }*/

        /*if($value['log_id']){
            $name = '';
            $side = 'left';
            $action = 'quote_sent';
            $status = 'accepted';
            if(strpos($value['message'], 'Rejected') !== false ){
                $status = 'rejected';
            }
            $version_text = '';
            $version_code = $db->field("SELECT message FROM tblquote_history WHERE quote_id='".$value['field_value']."' AND date='".$value['date']."' ");
            if(!is_null($version_code)){
                $version_text = '['.$version_code.']';
            }
            $message = gm('Quote').': <a href="index.php?do=quote-quote&quote_id='.$value['field_value'].'">'.$q[$value['field_value']]['serial_number'].$version_text.'</a> '.gm('was '.$status);
            if($q[$value['field_value']]['stage']){
                $stage = $db->field("SELECT name FROM tblquote_stage WHERE id = '".$q[$value['field_value']]['stage']."' ");
            }
            if($q[$value['field_value']]['source']){
                $source = $db->field("SELECT name FROM tblquote_source WHERE id = '".$q[$value['field_value']]['source']."' ");
            }
            if($q[$value['field_value']]['type']){
                $type = $db->field("SELECT name FROM tblquote_type WHERE id = '".$q[$value['field_value']]['type']."' ");
            }
            if($q[$value['field_value']]['autor']){
                $name =  get_user_name($q[$value['field_value']]['autor']);
            }
        }*/
        $contacts_array = array();
        $assign_to_str = '';
        if($value['contact_id'])
        {
            if(!array_key_exists($value['contact_id'], $contact_name)){
                $contact_name[$value['contact_id']] = get_contact_first_and_name($value['contact_id']);
            }

            // $view->assign(array(
            $assign_to_str	= $value['contact_id'];
            // ),'item');
            // $view->assign(array(
            $contacts_array[]['contact_name']= $contact_name[$value['contact_id']];
            // ),'contacts');
            // $view->loop('contacts','item');
        }else
        {
            $contacts= '';
            $db->query("SELECT * FROM customer_contact_activity_contacts WHERE activity_id='".$value['customer_contact_activity_id']."' AND action_type='0'");
            while($db->move_next())
            {
                if(!array_key_exists($db->f('contact_id'), $contact_name))
                {
                    $contact_name[$db->f('contact_id')] = get_contact_first_and_name($db->f('contact_id'));
                }
                $contacts = $contact_name[$db->f('contact_id')];
                // $view->assign(array(
                // 'assign_to'	=> $db->f('contact_id'),
                $assign_to_str	= $db->f('contact_id');
                // ),'item');
                $contacts_array[]['contact_name']= $contacts;
                //     $view->assign(array(
                // 		'contact_name'=> $contacts,
                // 	),'contacts');

                // $view->loop('contacts','item');
            }

        }

        if(!$value['contact_activity_type'])
        {
            if($value['quote_id'])
            {
                $customer = $q[$value['quote_id']]['customer'];
                if(!in_array($q[$value['quote_id']]['customer_id'], $customers_id) && $q[$value['quote_id']]['customer_id']!='0')
                {
                    array_push($customers_id, $q[$value['quote_id']]['customer_id']);
                }
            }else
            {
                $customer = $q[$value['field_value']]['customer'];
                if(!in_array($q[$value['field_value']]['customer_id'], $customers_id) && $q[$value['field_value']]['customer_id']!='0')
                {
                    array_push($customers_id, $q[$value['field_value']]['customer_id']);
                }
            }
            $events['7'] = 'quotes';
        }else
        {
            if($value['customer_id'])
            {
                $customer = get_customer_name($value['customer_id']);
                if(!in_array($value['customer_id'], $customers_id))
                {
                    array_push($customers_id, $value['customer_id']);
                }
            }
            $events[$value['contact_activity_type']]=$action;
        }

        $user_assigned = $db->query("SELECT logging.user_id, logging.to_user_id, logging.due_date FROM logging_tracked
			INNER JOIN logging ON logging_tracked.log_id=logging.log_id WHERE activity_id='".$value['customer_contact_activity_id']."' ");
        $date = $user_assigned->f('due_date');

        if(is_null($date)){
            $date = $value['date'];
        }
        $day = date('d-m-Y',$date);

        $value['date'] = $value['date']+$_SESSION['user_timezone_offset'];

        $item_line = array(
            'contacts'				=> $contacts_array,
            'assign_to'				=> $assign_to_str,
            // "side"				=> $side,
            'i'						=> $value['message'],
            'action'				=> $action,
            'message'				=> $message,

            // 'long_message'		=> nl2br($long_message),
            // 'short_message'		=> $short_message,
            // 'show_show_more'		=> strlen($long_message) > 20 ? true : false,

            'time'					=> date('H:i',$date),
            'time2'					=> date('G:i',$date),
            'day_tmpstmp'			=> $date,
            'day'					=> $day,
            'type_of_call'			=> $type_of_call,
            'customer'				=> $customer,
            'show_day'				=> in_array($day, $days) ? false : true,
            'commented'				=> $value['log_comment'] ? 'commented' : '',
            'add_note'				=> $value['log_comment'] ? gm('Edit note') : gm('Add Note'),
            'hide_note_if_exist'	=> $value['log_comment'] ? 'hide' : '',
            'comments'				=> $value['log_comment'],
            'is_comments'			=> $value['log_comment'] ? '': 'hide',
            'by'					=> $name,
            'name'					=> $name,
            // 'contact_name'		=> get_contact_name($value['contact_id']),
            //'assign_to'			=> $value['contact_id'],
            'log_id'				=> $value['customer_contact_activity_id'],
            'stage'					=> $stage,
            'source'				=> $source,
            'type'					=> $type,
            'new_event'				=> $new_event,
            'activity_status'		=> $activity_status,
            'color'					=> $color,
            'to_user'				=> $assigned,
            // 'assigned'			=> $assigned,
            'to'					=> $assigned ? true : false,
            'reminder_tmpstmp'		=> $value['reminder_date'],
            'reminder_time'			=> date('G:i',$value['reminder_date']),
            'reminder_date' 		=> date(ACCOUNT_DATE_FORMAT,$value['reminder_date']).', '.date('H:i',$value['reminder_date']),
            'is_reminder_date' 		=> $value['reminder_date'] > 0 ? true : false,
            'not_task'				=> $value['contact_activity_type']  < 6 ? true : false,
            'log_comment_date' 		=> date(ACCOUNT_DATE_FORMAT,$value['log_comment_date']).', '.date('H:i',$value['log_comment_date']),
            'is_log_comment_date' 	=> $value['log_comment_date'] ? true : false,
            'log_code'				=> $log_code ? $log_code : '',
            'reminder_assign'		=> $value['reminder_date'] > 0 ?true : false,
            'assigned_to_me'		=> ($user_assigned->f('user_id') == $_SESSION['u_id'] || $user_assigned->f('to_user_id')==$_SESSION['u_id'] || $_SESSION['group']=='admin') ? true : false,
            'allowed_delete'		=> ($user_assigned->f('user_id') == $_SESSION['u_id'] ||  $_SESSION['group']=='admin') ? true : false,
            // 'day_hours_time'			=> build_day_hours(date('H:i',$date)),
            'type_of_call_dd'		=> build_simple_dropdown(array(0=>gm('outgoing'),1=>gm('incoming')),$type_of_call_id),
            'type_of_call_id'		=> $type_of_call_id,
            // 'reminder_date_day_hours_time'			=> build_day_hours(date('H:i',$value['reminder_date'])),
            'reminder_date_day'		=> date(ACCOUNT_DATE_FORMAT,$value['reminder_date']),
            'status_change'			=> $status_change,

        );
        $result['item'][] = $item_line;

        // $view->loop('item');
        if(!in_array($day, $days)){
            array_push($days, $day);
        }
        $w++;
    }
    // console::log($days);
    ksort($events);
    $in['ev_type']	= '<option value=0>'.gm('Select type').'</option>'.build_simple_dropdown($events,$selected);
    // $in['customer_dd'] = '<option value=0>'.gm('Select type').'</option>'.build_customers_dd($customers_id);

    if($status_values){
        $status=$db->field("SELECT finished FROM logging WHERE log_id='".$status_values->f('log_id')."' ");
    }
    if($in['filter_type'] == 'customer'){
        $customer_data=$db->query("SELECT customers.name,customers.country_name,customers.city_name,addresses_coord.location_lat,addresses_coord.location_lng FROM customers
					LEFT JOIN addresses_coord ON customers.customer_id=addresses_coord.customer_id
					WHERE customers.customer_id='".$in['customer_id']."' AND customers.active=1 ORDER BY customers.name ");

        $result['location_meet'] = $customer_data->f('city_name').','.$customer_data->f('country_name');
        $result['coord_lat'] = $customer_data->f('location_lat');
        $result['coord_lang'] = $customer_data->f('location_lng');
    }
    $result['log_id'] = $status_values ? $status_values->f('log_id') : '';
    $result['scheduled_selected'] = $status == '0' ? 'selected' : '';
    $result['finished_selected'] = $status == '1' ? 'selected' : '';
    $result['is_data'] = $w > 0 ? true : false;
    $result['user_id'] = $in['user_id'];
    $result2 = array('activities' => $result);
    return $result2;

}