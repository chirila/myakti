<?php

class customers {

    function __construct() {
        $this->db = new sqldb();
        $this->db2 = new sqldb();
        $this->db_import = new sqldb;
        global $database_config;
        $database_users = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database_config['user_db'],
        );
        $this->db_users = new sqldb($database_users);
        $this->field_n='customer_id';
        $this->pag = 'customer';
        $this->pagc = 'xcustomer_contact';
        $this->field_nc = 'contact_id';
    }

    function delete_activity(&$in)
    {
        // if(!$this->CanEdit($in)){
        // 	return false;
        // }
        $activity_type=$this->db->field("SELECT contact_activity_type FROM customer_contact_activity WHERE customer_contact_activity_id='".$in['id']."'");
        $this->db->query("DELETE FROM customer_contact_activity WHERE customer_contact_activity_id='".$in['id']."' ");
        $this->db->query("DELETE FROM customer_contact_activity_contacts WHERE activity_id='".$in['id']."' ");
        $this->db->query("DELETE FROM logging_tracked WHERE activity_id='".$in['id']."' ");
        if($activity_type == 4){
            $this->db->query("DELETE FROM customer_meetings WHERE activity_id='".$in['id']."' ");
        }
        msg::success ( gm('Activity deleted'),'success');
        json_out($in);
        return true;
    }

    function update_status(&$in){
        $reminder_date=strtotimeActivity($in['reminder_event']);

        if($in['reminder_event']){
            $this->db->query("UPDATE logging SET reminder_date='".$reminder_date."' WHERE log_id='".$in['log_code']."'");

            $activity_id=$this->db->field("SELECT logging_tracked.activity_id FROM logging INNER JOIN logging_tracked ON logging.log_id=logging_tracked.log_id
         		WHERE logging.log_id='".$in['log_code']."'
                GROUP BY logging_tracked.activity_id
         		");

            $this->db->query("UPDATE customer_contact_activity SET reminder_date='".$reminder_date."' WHERE customer_contact_activity_id='".$activity_id."'");
        }
        if(!$in['not_status_change']){
            if($in['status_change'] == '0'){
                $this->db->query("UPDATE logging SET status_other='1',finished='0',finish_date='0' WHERE log_id='".$in['log_code']."'");
            }else {
                $this->db->query("UPDATE logging SET finished='1',finish_date='".time()."' WHERE log_id='".$in['log_code']."'");
            }
        }

        msg::success ( gm("Status changed succesfully!"),'success');
        return true;
    }

    function add_activity_validate(&$in){
        $v=new validation($in);
        $v->field('task_type', 'task_type', 'required', gm('Task type required'));
        $v->field('comment', 'comment', 'required', gm('Comment required'));
        $v->field('description', 'description', 'required', gm('Description required'));
        $v->field('date', 'date', 'required', gm('Date required'));
        if($in['task_type']=='6' || $in['reminder_active']){
            $v->field('reminder_date', 'reminder_date', 'required', gm('Reminder Date required'));
        }
        return $v->run();
    }

    function add_activity(&$in){

        if(!$_SESSION['customer_id']){
            json_out(array('error'=>'Invalid ID!'));
            http_response_code(400);
        }
        if(!$this->add_activity_validate($in)){
            http_response_code(400);
            json_out(array('validation_error' => msg::get_errors()));
            return false;
        }

        $users = $this->db_users->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."'")->getAll();

        $in['customer_id'] = $_SESSION['customer_id'];
        $in['contact_id'] = $_SESSION['contact_id'];
        $in['user_id'] = $users[0]['user_id'];
        $in['log_comment'] = $in['description'];
        $in['user_to_id']= $users[0]['user_id'];
        $in['reminder_event']=$in['reminder_date'];
        $in['hd_events']=$in['reminder_active'];

        switch($in['task_type']){
            case '1':
                $this->email_add($in);
                break;
            case '2':
                $this->event_add($in);
                break;
            case '4':
                $this->meeting_add($in);
                break;
            case '5':
                $this->call_add($in);
                break;
            default:
                $this->task_add($in);
                break;
        }

        $this->db->query("INSERT INTO customer_contact_activity_contacts SET activity_id='".$in['some_id']."',
																		 contact_id = '".$in['contact_id']."',
																		 action_type = '0'");


        //$users = $this->db_users->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."'")->getAll();
        foreach ($users as $key => $value) {
            $this->db->query("INSERT INTO logging_tracked SET
									activity_id='".$in['some_id']."',
									log_id='".$in['log_id']."',
									customer_id='".$in['customer_id']."',
									user_id='".$value['user_id']."'");
        }
        $this->db->query("UPDATE logging_tracked SET seen='0' WHERE user_id='".$in['user_id']."'AND activity_id='".$in['some_id']."' AND log_id='".$in['log_id']."'");


        msg::success ( gm("Activity added").".",'success');
        http_response_code(200);
        json_out(array('customer_id'=>$in['customer_id'], 'contact_id'=>$in['contact_id'], 'activity_id'=>$in['some_id']));

        return true;
    }

    function task_add(&$in){
        if($in['user_to_id'] ){
            $user_id=$in['user_to_id'];
        } else {
            $user_id = 0;
        }
        $date = strtotimeActivity($in['date']);
        $date_reminder = strtotimeActivity($in['reminder_event']);

        $in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														status_other='0',
														reminder_date='".$date_reminder."' ");



        $in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
																	contact_activity_note='".$in['comment']."',
																	log_comment='".$in['log_comment']."',
																	log_comment_date='".time()."',
																	date='".time()."',
																	customer_id='".$in['customer_id']."',
																	reminder_date='".$date_reminder."',
																	email_to='".$in['user_id']."' ");


    }

    function event_add(&$in){
        $date = strtotimeActivity($in['date']);

        if($in['hd_events']){
            $date_reminder = strtotimeActivity($in['reminder_event']);
            if($in['user_to_id']){
                $user_id=$in['user_to_id'];
            } else {
                $user_id = 0;
            }
        }else{
            $date_reminder=0;
            $user_id = $in['user_id'];
        }
        $in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														not_task='1',
														reminder_date='".$date_reminder."' ");
        $in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
														contact_activity_note='".$in['comment']."',
														log_comment='".$in['log_comment']."',
														log_comment_date='".time()."',
														date='".time()."',
														customer_id='".$in['customer_id']."',
														reminder_date='".$date_reminder."',
														email_to='".$in['user_id']."' ");
    }

    function call_add(&$in){
        $date = strtotimeActivity($in['date']);
        if($in['hd_events']){
            $reminder_date = strtotimeActivity($in['reminder_event']);
            if($in['user_to_id'] ){
                $user_id=$in['user_to_id'];
            } else {
                $user_id = 0;
            }
        } else {
            $reminder_date=0;
            $user_id = $in['user_id'];
        }



        $in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														status_other='1',
														not_task='1',
														reminder_date='".$reminder_date."',
														type_of_call='".$in['type_of_call']."'
														 ");
        $in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
																	contact_activity_note='".$in['comment']."',
																	log_comment='".$in['log_comment']."',
																	log_comment_date='".time()."',
																	date='".time()."',
																	customer_id='".$in['customer_id']."',
																	reminder_date='".$reminder_date."',
																	type_of_call='".$in['type_of_call']."',
																	email_to='".$in['user_id']."' ");
    }

    function email_add(&$in){
        $date = strtotimeActivity($in['date']);
        if($in['hd_events']){
            $reminder_date = strtotimeActivity($in['reminder_event']);
            if($in['user_to_id'] && $in['assigned_email']){
                $user_id=$in['user_to_id'];
            } else {
                $user_id = 0;
            }
        } else {
            $reminder_date=0;
            $user_id = $in['user_id'];
        }
        $in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														status_other='1',
														not_task='1',
														reminder_date='".$reminder_date."' ");
        $in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
																	contact_activity_note='".$in['comment']."',
																	log_comment='".$in['log_comment']."',
																	log_comment_date='".time()."',
																	date='".time()."',
																	customer_id='".$in['customer_id']."',
																	reminder_date='".$reminder_date."',
																	email_to='".$in['user_id']."' ");
    }

    function meeting_add(&$in){

        if($in['hd_events']){
            $reminder_date=strtotimeActivity($in['reminder_event']);
            if($in['user_to_id']){
                $user_id=$in['user_to_id'];
            } else {
                $user_id = 0;
            }
        } else {
            $reminder_date=0;
            $user_id = $in['user_id'];
        }
        $due_date = strtotimeActivity($in['date']);
        $in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$due_date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														not_task='1',
														reminder_date='".$reminder_date."' ");
        $in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
																	contact_activity_note='".$in['comment']."',
																	log_comment='".$in['log_comment']."',
																	log_comment_date='".time()."',
																	date='".time()."',
																	customer_id='".$in['customer_id']."',
																	reminder_date='".$reminder_date."',
																	email_to='".$in['user_id']."' ");
        $my_name = $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
        $my_name->next();
        if($in['all_day_meet'])
        {
            $duration = 0;
            $all_day = 1;
        } else {
            switch ($in['duration_meet']) {
                case '0-15':
                    $duration = '900';
                    break;
                case '0-30':
                    $duration = '1800';
                    break;
                case '1-0':
                    $duration = '3600';
                    break;
                case '2-0':
                    $duration = '7200';
                    break;
                case '3-0':
                    $duration = '10800';
                    break;
                default:
                    $duration = '';
                    break;
            }

            $all_day = 0;
        }
        if(!$in['user_id'])
        {
            $in['user_id'] = $_SESSION['u_id'];
        }
        $start_date= $due_date;
        //$end_date = strtotime($in['end_d_meet']);
        $end_date=$due_date+$duration;

        $in['meeting_id'] = $this->db->insert("INSERT INTO customer_meetings SET
															   subject='".$in['comment']."',
															   location='".$in['location_meet']."',
															   start_date='".$start_date."',
															   end_date='".$end_date."',
															   duration='".$duration."',
															   all_day='".$all_day."',
															   message='".strip_tags($in['log_comment'])."',
															   from_c='".utf8_encode($my_name->f('first_name'))." ".utf8_encode($my_name->f('last_name'))."',
															   type='1',
															   customer_id='".$in['customer_id']."',
															   customer_name='".$in['customer_name']."',
															   user_id='".$in['user_id']."',
															   activity_id='".$in['some_id']."'
															");
        if(!empty($in['contact_ids'])){
            foreach ($in['contact_ids'] as $contact_id) {
                $this->db->query("INSERT INTO customer_contact_activity_contacts SET activity_id='".$in['meeting_id']."',
																				 contact_id = '".$contact_id."',
																				 action_type = '1'");
            }
        }
        $coordinates=$this->db->field("SELECT coord_id FROM addresses_coord WHERE customer_id='".$in['customer_id']."'");
        if(!$coordinates){
            $this->db->query("INSERT INTO addresses_coord SET customer_id='".$in['customer_id']."',
										location_lat='".$in['latitude_customer']."',
										location_lng='".$in['longitude_customer']."' ");
        }
    }

    function sendToGoogle(&$in)
    {
        ### google calendar ###
        ark::loadLibraries(array('gCalendar'));
        $calendar = new gCalendar($in);
        $calendar->sendToGoogle($in);
        ### google calendar ###
    }

    function modify_activity_date(&$in){

        $date_activity=strtotimeActivity($in['date']);

        $this->db->query("UPDATE logging SET type_of_call='".$in['type_of_call_id']."' , due_date='".$date_activity."'  WHERE  log_id = '".$in['log_code']."' ");

        if($in['type_of_call_id']){
            $in['type_of_call_view']=gm('incoming call');
        }else{
            $in['type_of_call_view']=gm('outgoing call');
        }
        return true;
    }

    /**
     * undocumented function
     *
     * @return bool
     * @author Mp
     **/
    function log_comment(&$in)
    {
        $date = time();
        $in['log_comment_date'] = date(ACCOUNT_DATE_FORMAT,$date).', '.date('H:i',$date);
        $this->db->query("UPDATE customer_contact_activity SET log_comment='".$in['comments']."', log_comment_date='".$date."' WHERE customer_contact_activity_id='".$in['log_id']."' ");
        $log_id=$this->db->field("SELECT log_id FROM logging_tracked WHERE activity_id='".$in['log_id']."' GROUP BY activity_id ");
        $this->db->query("UPDATE logging SET message='".$in['comments']."' WHERE log_id='".$log_id."' ");
        $in['comments'] = stripslashes($in['comments']);
        return true;
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    function UpdateActivity(&$in)
    {
        $this->modify_activity_date($in);
        $this->log_comment($in);
        $this->update_status($in);
        // json_out($in);
    }

}
?>