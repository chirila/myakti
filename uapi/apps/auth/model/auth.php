<?php

/************************************************************************
 * @Author: MedeeaWeb Works                                                   *
 ************************************************************************/
class auth
{

    public function __construct()
    {
        $this->db = new sqldb();
    }
    /****************************************************************
     * function login(&$in)                                          *
     ****************************************************************/

    function login(&$in)
    {
        $v = new  validation($in);
        $v->field('username', gm('Username'), 'required', gm('Please fill the username'));
        $v->field('password', gm('Password'), 'required', gm('Please fill the password'));

        if (!$v->run()) {
            http_response_code(400);
            json_out(array('validation_error' => msg::get_errors()));
            return false;
        }
        // $this->db->query("SELECT * FROM customer_contacts WHERE email = '" . $in['email'] . "' ");

        $db = $this->db->query("SELECT * FROM customer_contacts WHERE email = '" . $in['username'] . "' OR username = '" . $in['username'] . "'");

        if($db->move_next()) {

            if (md5($in['password']) == $db->f('password')) {
            //if ($in['password'] == '123456') {
                session_unset();
                session_start();

                // TO DO: add function to check if the master account is valid
                $in['auth_key'] = md5(uniqid(mt_rand(), true));
                $this->db->query("delete from customer_contact_auth where contact_id = ".$db->f('contact_id'));
                $this->db->query("
    INSERT into customer_contact_auth SET
	auth_key = '".$in['auth_key']."',
	contact_id = '".$db->f('contact_id')."',
	customer_id = '".$db->f('customer_id')."',
	date = '".strtotime('now')."',
	exp_date = '".(strtotime('now') + 31556926)."'

");

                $lang_code = $this->db->field("SELECT code FROM lang WHERE lang_id='" . $db->f('language') . "' ");
                if(!$lang_code){
                    $lang_code = $this->db->field("SELECT code FROM lang WHERE is_main=1 ");
                }

                //json_out(array('auth_key' => $in['auth_key'], 'lang_id' => $db->f('language'), 'lang_code' => $lang_code, 'language' => $lang_name));
                json_out(array('auth_key' => $in['auth_key'], 'lang' => $lang_code));

                return true;
            } else {
                http_response_code(400);
                msg::error(gm("Wrong Username or Password!"), 'error');
                json_out(array('validation_error' => msg::get_errors()));
                return false;
            }
        }
        http_response_code(400);
        msg::error(gm("Wrong Username or Password!"), 'error');
        json_out(array('validation_error' => msg::get_errors()));
        return false;
    }

    /****************************************************************
     * function reset(&$in)                                         *
     ****************************************************************/
    function reset(&$in)
    {
        if(!isset($in['username']) && !isset($in['email'])){
            http_response_code(400);
            msg::error(gm("Please provide the username or email address!"), 'error');
            json_out(array('validation_error' => msg::get_errors()));
            return false;
        }

        $v = new  validation($in);
        $v->field('callback_url', gm('Callback Url'), 'required', gm('Please provide the callback URL'));

        if (!$v->run()) {
            http_response_code(400);
            json_out(array('validation_error' => msg::get_errors()));
            return false;
        }

        if(isset($in['username'])){
            $db = $this->db->query("SELECT * FROM customer_contacts WHERE username='" . $in['username'] . "'");
        } else {
            if(isset($in['email'])){
                $db = $this->db->query("SELECT * FROM customer_contacts WHERE email='" . $in['email'] . "'");
            }
        }

        if($db->next()){
            $lang_code = $this->db->field("SELECT code FROM lang WHERE lang_id='" . $db->f('language') . "' ");
            if(!$lang_code){
                $lang_code = $this->db->field("SELECT code FROM lang WHERE is_main=1 ");
            }
            $in['reset_key'] = md5(uniqid(mt_rand(), true));
            $this->db->query("
    INSERT into customer_contact_auth SET
	reset_key = '".$in['reset_key']."',
	contact_id = '".$db->f('contact_id')."',
	customer_id = '".$db->f('customer_id')."',
	reset_date = '".strtotime('now')."',
	callback_url = '".$in['callback_url']."'

");
            // to do: calls for callback. To send out: reset_key, customer email, customer name, language
            $ch = new clientREST();
            $ch->setAcceptType('application/json');
            $response = $ch->execRequest($in['callback_url'].'?reset_key='.$in['reset_key'].'&firstname='.$db->f('firstname').'&lastname='.$db->f('lastname').'&email='.$db->f('email').'&language='.$lang_code, 'get');

            http_response_code(200);
            json_out(array('success' => 'Success', 'response'=>$response));
            /*
            json_out(array(
                'success' => 'Success',
                'language' => $lang_code,
                'firstname' => $db->f('firstname'),
                'lastname' => $db->f('lastname'),
                'email' => $db->f('email'),
                'reset_key' => $in['reset_key']
            ));
            */
            return true;

        } else {
            http_response_code(400);
            json_out(array('validation_error' => gm('We can not find the user in the database')));
            return false;
        }
    }


    /****************************************************************
     * function change_password(&$in)                                         *
     ****************************************************************/
    function change_password(&$in)
    {

        $v = new  validation($in);
        $v->field('reset_key', gm('Reset Key'), 'required', gm('Please provide the Reset Key'));
        $v->field('password', gm('Password'), 'required', gm('Please provide the Password'));
        $v->field('password2', gm('Verify Password'), 'required', gm('Please verify the Password'));
        $v->field('password', gm('Password'), 'match[password2]', gm('Password does not match'));
        if (!$v->run()) {
            http_response_code(400);
            json_out(array('validation_error' => msg::get_errors()));
            return false;
        }

        $db = $this->db->query("select * from customer_contact_auth where reset_key = '".$in['reset_key']."'");
        if($db->next()){

            $this->db->query("
            UPDATE customer_contacts SET
            password = '".md5($in['password'])."'
            where
            contact_id = '".$db->f('contact_id')."'

            ");

            $this->db->query("DELETE from customer_contact_auth where reset_key = '".$in['reset_key']."'");

            http_response_code(200);
            //json_out(array('success' => 'Success'));
            json_out(array(
                'success' => 'Password Updated'
            ));
            return true;

        } else {
            http_response_code(400);
            json_out(array('validation_error' => gm('Invalid Reset Key')));
            return false;
        }
    }


    /****************************************************************
     * function logout(&$in)                                         *
     ****************************************************************/
    function logout(&$in)
    {
        $this->db->query("delete from customer_contact_auth where auth_key = '".$in['auth_key']."'");
        session_unset();
        session_destroy();
        unset($_SESSION);
        msg::success(gm("Success!"), 'success');
        http_response_code(200);
        json_out(msg::get_success());
        return true;
    }

    function set_session(&$in)
    {
        $this->db->query("select * from customer_contact_auth where auth_key = '".$in['auth_key']."'");
        if($this->db->next()){
            $_SESSION['u_id'] = $this->db->f('contact_id');
            $_SESSION['c_id'] = $this->db->f('customer_id');
            $_SESSION['contact_id'] = $this->db->f('contact_id');
            $_SESSION['customer_id'] = $this->db->f('customer_id');
            $_SESSION['group'] = 'user';
            $_SESSION['access_level'] = 1;

            $lang_id = $this->db->field("SELECT language FROM customer_contacts WHERE contact_id='" . $this->db->f('contact_id') . "'");
            if($lang_id){
                $lang_code = $this->db->field("SELECT code FROM lang WHERE lang_id='" . $lang_id . "' ");
            } else {
                $lang = $this->db->query("SELECT code, lang_id FROM lang WHERE is_main=1");
                $lang_id = $lang->f('lang_id');
                $lang_code =  $lang->f('code');
            }

            if($in['language']){
                $lang_code = $in['language'];
                $lang_id = $this->db->field("SELECT lang_id FROM lang WHERE code='" . $lang_code . "' ");
            }
            $_SESSION['l'] = $lang_code;
            $_SESSION['l_id'] = $lang_id;


            return true;
        } else {
            http_response_code(400);
            msg::error(gm("Invalid Auth Key"), 'error');
            json_out(msg::get_errors());
        }

    }



    function reset_final(&$in)
    {
        if (!$this->validate_reset($in)) {
            return false;
        }
        // $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
        $this->db->query("UPDATE users SET password='" . md5($in['password']) . "' WHERE username='" . $in['username'] . "'");
        $this->db->query("UPDATE user_info SET reset_request='' WHERE user_id in (SELECT user_id FROM users WHERE username='" . $in['username'] . "')");
        msg::success(gm("Password change succesfully."), 'success');
        $in['pag'] = "login";
        return true;
    }

    function validate_reset(&$in)
    {
        $v = new validation($in);
        $v->field('password', gm('Password'), 'required:min_length[6]');
        $v->field('password2', gm('Confirm Password'), 'match[password]');
        // if(!$in['password']){
        // 	msg::$error = gm("Password").' '.gm('is required').'<br>';
        // 	$iss_ok = false;
        // }
        if (!$in['password2']) {
            msg::error(gm("Confirm Password") . ' ' . gm('is required') . '<br>', 'error');
            $iss_ok = false;
        }
        if ($in['password2'] != $in['password']) {
            msg::error(gm("Password") . ' ' . gm('Field') . ' ' . gm("don't match with") . ' ' . gm("Confirm Password") . ' ' . gm('Field') . '<br>', 'error');
            $iss_ok = false;
        }

        if ($iss_ok === false) {
            return false;
        }
        $crypt = $this->db->field("SELECT reset_request FROM user_info WHERE user_info.reset_request='" . $in['crypt'] . "' ");
        if (!$crypt) {
            msg::error(gm('Wrong Username or Password!'), 'error');
            return false;
        }


        /*	$crypt = $this->db->field("SELECT reset_request FROM user_info INNER JOIN users ON user_info.user_id = users.user_id WHERE users.username='".$in['username']."' ");
            if($crypt != $in['crypt']){
                msg::error ( gm('Wrong Username or Password!'),'error');
                return false;
            }*/
        return $v->run();
    }

    function impersonate(&$in)
    {
        if (!$in['user_id']) {
            header("Location: /");
            return false;
        }
        $db = $this->db->query("SELECT * FROM users
                        INNER JOIN lang ON lang.lang_id = users.lang_id
                        WHERE user_id = '" . $in['user_id'] . "' ");

        $code = md5($this->db->f('user_id') . '-' . $this->db->f('username') . '-' . date('Y-m-d'));

        if ($code == $in['code']) {
            @session_start();
            $this->set_session($db, $in);
            header("Location: /invoices/");
            // header("Location: /bianca/accountant/index.php");
            // header("Location: ".$_SERVER['PHP_SELF']);

        } else {
            msg::error(gm('Wrong Username or Password!'), 'error');
            console::log($code);
            header("Location: /");
            return false;
        }
    }

}//end class
?>