<?php

/**
* 
*/
class quote
{
	
	private $db;
	protected $pag = 'quote'; 						# used for log messages
	protected $field_n = 'quote_id';					# used for log messages

	function __construct()
	{
		$this->db = new sqldb();
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($db_config);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->updateCustomer_validate($in)){
			return false;
		}
        $query_sync=array();

		// $c_type = explode(',', $in['c_type']);
		
		foreach ($in['c_type'] as $key) {
			if($key){
				$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
				$c_type_name .= $type.',';
			}
		}
		$c_types = implode(',', $in['c_type']);
		$c_type_name = rtrim($c_type_name,',');


		if($in['user_id']){
			foreach ($in['user_id'] as $key => $value) {
				$manager_id = $this->db_users->field("SELECT first_name,last_name FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
					$acc_manager .= $manager_id.',';
			}			
			$in['acc_manager'] = rtrim($acc_manager,',');
			$acc_manager_ids = implode(',', $in['user_id']);
		}else{
			$acc_manager_ids = '';
			$in['acc_manager'] = '';
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET name 				= '".$in['name']."',
											serial_number_customer  =   '".$in['serial_number_customer']."',
											commercial_name 		= '".$in['commercial_name']."',
											active					= '".$in['active']."',
											legal_type				= '".$in['legal_type']."',
											user_id					= '".$acc_manager_ids."',
											c_type					= '".$c_types."',
											website					= '".$in['website']."',
											language				= '".$in['language']."',
											c_email					= '".$in['c_email']."',
											sector					= '".$in['sector']."',
											comp_phone				= '".$in['comp_phone']."',
											comp_fax				= '".$in['comp_fax']."',
											activity				= '".$in['activity']."',
											comp_size				= '".$in['comp_size']."',
											lead_source				= '".$in['lead_source']."',
											c_type_name				= '".$c_type_name."',
											various1				= '".$in['various1']."',
											various2				= '".$in['various2']."',
											acc_manager_name		= '".$in['acc_manager']."',
											sales_rep				= '".$in['sales_rep']."',
											is_supplier				= '".$in['is_supplier']."',
											our_reference			= '".$in['our_reference']."'
					  	WHERE customer_id='".$in['customer_id']."' ");

		if($in['customer_id_linked']){
			$link_id = $this->db->insert("INSERT INTO customer_link SET customer_id='".$in['customer_id']."',
														customer_id_linked='".$in['customer_id_linked']."',
														link_type='".$in['link_type']."'");
			if($in['link_type'] == 1){
				$type = 2;
			}
			else{
				$type = 1;
			}
			$this->db->query("INSERT INTO customer_link SET customer_id='".$in['customer_id_linked']."',
														customer_id_linked='".$in['customer_id']."',
														link_type='".$type."',
														link_id='".$link_id."'");
		}
		$this->db->query("UPDATE customer_contacts SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$this->db->query("UPDATE projects SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$legal_type = $this->db->field("SELECT name FROM customer_legal_type WHERE id='".$in['legal_type']."' ");
		$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['name'].' '.$legal_type."'	WHERE buyer_id='".$in['customer_id']."' ");

		
		Sync::end($in['customer_id']);

		set_first_letter('customers',$in['name'],'customer_id',$in['customer_id']);

		//save extra fields values
		$this->update_custom_field($in);

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		// $in['pagl'] = $this->pag;
		self::sendCustomerToZintra($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer_validate(&$in)
	{

		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		if($in['customer_link']){
			$v->field('link_type', 'Type of link', 'required');
		}
		return $this -> addCustomer_validate($in);

	}

	function CanEdit(&$in){
		if(ONLY_IF_ACC_MANAG ==0 && ONLY_IF_ACC_MANAG2==0){
			return true;
		}
		$c_id = $in['customer_id'];
		if(!$in['customer_id'] && $in['contact_id']) {
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
		}
		if($c_id){
			if($_SESSION['group'] == 'user' && !in_array('company', $_SESSION['admin_sett']) ){
				$u = $this->db->field("SELECT user_id FROM customers WHERE customer_id='".$c_id."' ");
				if($u){
					$u = explode(',', $u);
					if(in_array($_SESSION['u_id'], $u)){
						return true;
					}
					else{
						msg::$warning = gm("You don't have enought privileges");
						return false;
					}
				}else{
					msg::$warning = gm("You don't have enought privileges");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Update contact data
	 * @param  array $in
	 * @return bool
	 * @author PM
	 */
	function contact_update(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->contact_update_validate($in)){
			return false;
		}
		if(isset($in['birthdate']) && !empty($in['birthdate']) ){
			$in['birthdate'] = strtotime($in['birthdate']);
		}
		$position = implode(',', $in['position']);
		$pos = '';
		foreach ($in['position_n'] as $key => $value) {
			$pos .= $value['name'].',';
		}
		$pos = addslashes(rtrim($pos,','));
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customer_contacts SET
                                            customer_id	=	'".$in['customer_id']."',
                                            firstname	=	'".$in['firstname']."',
                                            lastname	=	'".$in['lastname']."',
                                            position	=	'".$position."',
                                            department	=	'".$in['department']."',
                                            email		=	'".$in['email']."',
                                            birthdate	=	'".$in['birthdate']."',
					    					phone		=	'".$in['phone']."',
										    cell		=	'".$in['cell']."',
										    note		=	'".$in['note']."',
										    sex			=	'".$in['sex']."',
										    language	=	'".$in['language']."',
										    title		=	'".$in['title']."',
										    e_title		=	'".$in['e_title']."',
											fax			=	'".$in['fax']."',
											company_name=	'".$in['customer']."',
											title_name	=	'".$in['title_name']."',
											position_n  = 	'".$pos."',
										    last_update =	'".time()."'
				WHERE
			 	contact_id	=	'".$in['contact_id']."'");
		$this->db->query("UPDATE customer_contacts SET s_email='0' WHERE contact_id='".$in['contact_id']."' ");
		if($in['s_email']){
			$this->db->query("UPDATE customer_contacts SET s_email='1', unsubscribe='' WHERE contact_id='".$in['contact_id']."' ");
		}

		if ($in['password'])
		{
			$this->db->query("UPDATE customer_contacts SET password='".md5($in['password'])."' WHERE contact_id='".$in['contact_id']."'");
		}

		Sync::end($in['contact_id']);

		$this->db->query("SELECT * FROM recurring_invoice WHERE contact_id='".$in['contact_id']."' ");
		while ($this->db->move_next()) {
			$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['firstname'].' '.$in['lastname']."',
																							buyer_email='".$in['email']."',
																							buyer_phone='".$in['phone']."'
												WHERE contact_id='".$in['contact_id']."' ");
		}
		if(!$in['customer_id']){
			$this->removeFromAddress($in);
		}

		$project_company_name = $in['firstname'].' '.$in['lastname'];
		$this->db->query("UPDATE projects SET company_name = '".$project_company_name."' WHERE customer_id = '".$in['contact_id']."' AND is_contact = '1' ");

		set_first_letter('customer_contacts',$in['lastname'],'contact_id',$in['contact_id']);

		//save extra fields values
		$this->update_custom_contact_field($in);

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pagc,'{l}Contact updated by{endl} '.get_user_name($_SESSION['u_id']),'contact_id',$in['contact_id']);
		// $in['pagl'] = $this->pagc;
		return true;
	}

	/**
	 * Validate update contact data
	 * @param  array $in
	 * @return [type]     [description]
	 */
	function contact_update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', gm('Invalid ID'));
		$is_ok = $v->run();

		if(!$this->contact_add_validate($in)){
			$is_ok = false;
		}
		if($in['customer_id']){
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			if($c_id){
				if($c_id != $in['customer_id']){
					$this->removeFromAddress($in);
				}
			}
		}

		return $is_ok;
	}

	function select_edit(&$in){

		$query_sync=array();
		$i=0;
		foreach ($in['lines'] as $key => $value){
			if(!empty($value)){
				$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$value['id']."' ");
				if(!$id){
					$filter ='';
					if ($in['table']=='customer_extrafield_dd' && is_numeric($in['extra_id']))
					{
						$filter = ", extra_field_id='".$in['extra_id']."'";
					}
					if ($in['table']=='contact_extrafield_dd' && is_numeric($in['extra_id']))
					{
						$filter = ", extra_field_id='".$in['extra_id']."'";
					}
					$inserted_id=$this->db->insert("INSERT INTO ".$in['table']." SET name='".addslashes($value['name'])."', sort_order='".($key+1)."' ".$filter." ");
					$query_sync[$i]="INSERT INTO ".$in['table']." SET id='".$inserted_id."',name='".addslashes($value['name'])."', sort_order='".($key+1)."' ".$filter." ";
					$i++;
				}
			}
		}

		Sync::start(ark::$model.'-'.ark::$method);

        foreach ($in['lines'] as $key=>$value){
			/*if(!empty($value)){
				$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$value['id']."' ");
				if($id){
					$this->db->query("UPDATE ".$in['table']." SET name='".addslashes($value['name'])."', sort_order='".($key+1)."' WHERE id='".$id."' ");

				}
			}*/
			if(!empty($value)){
				if($in['table']=='customer_type'){
					$info = $this->db->query("SELECT id,name FROM ".$in['table']." WHERE id='".$value['id']."' ");
					$id = $info->f('id');
					$old_value = $info->f('name');
				}else{
					$id = $this->db->field("SELECT id FROM ".$in['table']." WHERE id='".$value['id']."' ");
				}
				if($id){
					$this->db->query("UPDATE ".$in['table']." SET name='".addslashes($value['name'])."', sort_order='".($key+1)."' WHERE id='".$id."' ");

					# begin update type of relationship on every customer if has this type.
					if($in['table']=='customer_type' && $old_value != $value){
						$all_c_t = array();
						$all_c_type = $this->db->query("SELECT id,name FROM customer_type ");
						while ($all_c_type->next()) {
							$all_c_t[$all_c_type->f('id')] = $all_c_type->f('name');
						}
						$cust_with_this_rel = $this->db->query("SELECT customer_id, c_type, c_type_name FROM customers");
						while($cust_with_this_rel->next()){
							$update_customer = false;
							$c_type = $cust_with_this_rel->f('c_type');
							$c_type = explode(',', $c_type);
							$type_names = '';
							foreach ($c_type as $key => $value2) {
								if(array_key_exists($value2, $all_c_t)){
									// $type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$value2."' ");
									$type_names .= $all_c_t[$value2].', ';
								}
								if($id == $value2){
									$update_customer = true;
								}
							}
							if($update_customer==true){

								$type_names = addslashes(rtrim(rtrim($type_names,' '),','));
								$this->db->query("UPDATE customers SET c_type_name = '".$type_names."' WHERE customer_id = '".$cust_with_this_rel->f('customer_id')."' ");
							}
						}
					}
					# end update

				}
			}
		}
		$in['lines'] = build_some_dd($in['table'],$in['extra_id']);
		Sync::end(0,$query_sync);
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
		return true;
	}

	/****************************************************************
	* function delete_select(&$in)                                  *
	****************************************************************/
	function delete_select(&$in){
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("DELETE FROM ".$in['table']." WHERE id='".$in['id']."' ");
		Sync::end();
		msg::success ( gm('Changes saved'),'success');
		json_out($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->update_financial_valid($in)){
			return false;
		}
		$apply_fix_disc = 0;
		if($in['apply_fix_disc']){$apply_fix_disc = 1;}
		$apply_line_disc = 0;
		if($in['apply_line_disc']){$apply_line_disc = 1;}
		$check_vat_number = $this->db->field("SELECT check_vat_number FROM customers WHERE customer_id = '".$in['customer_id']."' ");
		if($in['btw_nr']!=$check_vat_number){
			$check_vat_number = '';
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET
											btw_nr								=	'".$in['btw_nr']."',
											bank_name							=	'".$in['bank_name']."',
											payment_term					=	'".$in['payment_term']."',
											payment_term_type 		= '".$in['payment_term_start']."',
											bank_bic_code					=	'".$in['bank_bic_code']."',
											fixed_discount				=	'".return_value($in['fixed_discount'])."',
											bank_iban							=	'".$in['bank_iban']."',
                      						vat_id								=	'".$in['vat_id']."',
											cat_id								=	'".$in['cat_id']."',
											no_vat								=	'".$in['no_vat']."',
											currency_id						= '".$in['currency_id']."',
											invoice_email					=	'".$in['invoice_email']."',
											attention_of_invoice		   =	'".$in['attention_of_invoice']."',
											invoice_note2					=	'".$in['invoice_note2']."',
											deliv_disp_note				=	'".$in['deliv_disp_note']."',
											external_id						=	'".$in['external_id']."',
											internal_language			=	'".$in['internal_language']."',
											apply_fix_disc				= '".$apply_fix_disc."',
											apply_line_disc				= '".$apply_line_disc."',
											line_discount 				= '".return_value($in['line_discount'])."',
											comp_reg_number 				= '".$in['comp_reg_number']."',
											vat_regime_id				= '".$in['vat_regime_id']."',
											siret						= '".$in['siret']."',
											check_vat_number 			= '".$check_vat_number."',
											identity_id                 = '".$in['identity_id']."'
											WHERE customer_id			= '".$in['customer_id']."' ");
		//quote_reference			=	'".$in['quote_reference']."',

		Sync::end($in['customer_id']);
		// msg::$success = gm("Changes have been saved.");
		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		self::sendCustomerToZintra($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial_valid(&$in)
	{
		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
		if($in['invoice_email']){
			$v->field('invoice_email', 'Type of link', 'email');
		}

		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function sendCustomerToZintra(&$in)
	{
		if(!defined("ZINTRA_ACTIVE") || ZINTRA_ACTIVE != 1){
			return true;
		}
		$vars_post = array();
		$data = $this->db->query("SELECT * FROM customers WHERE customer_id='".$in['customer_id']."' ");
		if($data){
			$bar_code_start = null;
			$bar_code_end = null;
			$custom_fields = $this->db->query("SELECT * FROM customer_field WHERE customer_id='".$in['customer_id']."' ");
			while($custom_fields->next()){
				if($custom_fields->f('field_id') == 1){
					$bar_code_start = $custom_fields->f('value');
				}
				if($custom_fields->f('field_id') == 2){
					$bar_code_end = $custom_fields->f('value');
				}
			}
			//asdkauas
			$primary = array();
			$address = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
			if($address) {
				$primary = array(
					"Unformatted"=> null,
			    "Label"=> null,
			    "Building"=> null,
			    "Street"=> $address->f('address'),
			    "PostalCode"=> $address->f('zip'),
			    "City"=> $address->f('city'),
			    "State"=> $address->f('state_id'),
			    "Country"=> get_country_name($address->f('country_id')),
			    "CountryCode"=> get_country_code($address->f('country_id'))
			  );
			}
			$test = array();
			$vars_post = array(
				'Id'											=> (string)$in['customer_id'],
				"Name"										=> $data->f('name'),
			  "VatNumber"								=> $data->f('btw_nr') ? $data->f('btw_nr') : null,
			  "TimeZone"								=> null,
			  "Note"										=> null,
			  "PictureURI"							=> null,
			  "Importance"							=> null,
			  "Blog"										=> null,
			  "WebSite"									=> $data->f('website') ? $data->f('website') : null,
			  "FixedPhoneNumber"				=> $data->f('comp_phone') ? $data->f('comp_phone') : null,
			  "MobilePhoneNumber"				=> $data->f('other_phone') ? $data->f('other_phone') : null,
			  "FaxNumber"								=> $data->f('comp_fax') ? $data->f('comp_fax') : null,
			  "EMailAddress"						=> $data->f('c_email') ? $data->f('c_email') : null,
			  "DeliveryPostalAddress"		=> null,
			  "InvoicePostalAddress"		=> null,
			  "PostalAddress"						=> $primary,
			  "Fields"									=> array(
					array(
					  "Name"								=> "Reference",
					  "Value"								=> $data->f('our_reference')
					),
					array(
					  "Name"								=> "BarcodeFrom",
					  "Value"								=> $bar_code_start
					),
					array(
					  "Name"								=> "BarcodeTo",
					  "Value"								=> $bar_code_end
					)
			  ),
			  "Links"										=> $test,
			  "Categories"							=> null
			);

			$put = $this->c_rest->execRequest('https://app.zintra.eu/datas/Organization/'.$in['customer_id'],'put',$vars_post);
			console::log($put);
			$this->db->query("UPDATE customers SET zintraadded='1' WHERE customer_id='".$in['customer_id']."' ");
		}
		return true;
	}

	function check_vies_vat_number(&$in){
		$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

		if(!$in['value'] || $in['value']==''){			
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		$value=trim($in['value']," ");
		$value=str_replace(" ","",$value);
		$value=str_replace(".","",$value);
		$value=strtoupper($value);

		$vat_numeric=is_numeric(substr($value,0,2));

		/*if($vat_numeric || substr($value,0,2)=="BE"){
			$trends_access_token=$this->db->field("SELECT api FROM apps WHERE type='trends_access_token' ");
			$trends_expiration=$this->db->field("SELECT api FROM apps WHERE type='trends_expiration' ");
			$trends_refresh_token=$this->db->field("SELECT api FROM apps WHERE type='trends_refresh_token' ");

			if(!$trends_access_token || $trends_expiration<time()){
				$ch = curl_init();
				$headers=array(
					"Content-Type: x-www-form-urlencoded",
					"Authorization: Basic ". base64_encode("801df338bf4b46a6af4f2c850419c098:B4BDDEF6F33C4CA2B0F484")
					);
				/*$trends_data=!$trends_access_token ? "grant_type=password&username=akti_api&password=akti_api" : "grant_type=refresh_token&&refresh_token=".$trends_refresh_token;* /
				$trends_data="grant_type=password&username=akti_api&password=akti_api";

			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    curl_setopt($ch, CURLOPT_POST, true);
		       	curl_setopt($ch, CURLOPT_POSTFIELDS, $trends_data);
			    curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/oauth2/token');

			    $put = json_decode(curl_exec($ch));
			 	$info = curl_getinfo($ch);

			 	/*console::log($put);
			 	console::log('aaaaa');
			 	console::log($info);* /

			 	if($info['http_code']==400){
			 		if(ark::$method == 'check_vies_vat_number'){
				 		msg::error ( $put->error,'error');
				 		json_out($in);
				 	}
					return false;
			 	}else{
			 		if(!$trends_access_token){
			 			$this->db->query("INSERT INTO apps SET api='".$put->access_token."', type='trends_access_token' ");
					 	$this->db->query("INSERT INTO apps SET api='".(time()+1800)."', type='trends_expiration' ");
					 	$this->db->query("INSERT INTO apps SET api='".$put->refresh_token."', type='trends_refresh_token' ");
			 		}else{
			 			$this->db->query("UPDATE apps SET api='".$put->access_token."' WHERE type='trends_access_token' ");
			 			$this->db->query("UPDATE apps SET api='".(time()+1800)."' WHERE type='trends_expiration' ");
			 			$this->db->query("UPDATE apps SET api='".$put->refresh_token."' WHERE type='trends_refresh_token' ");
			 		}

			 		$ch = curl_init();
					$headers=array(
						"Authorization: Bearer ".$put->access_token
						);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				    if($vat_numeric){
				      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
				    }else{
				      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
				    }
				    $put = json_decode(curl_exec($ch));
				 	$info = curl_getinfo($ch);

				 	/*console::log($put);
				 	console::log('bbbbb');
				 	console::log($info);* /

				    if($info['http_code']==400 || $info['http_code']==429){
				    	if(ark::$method == 'check_vies_vat_number'){
					 		msg::error ( $put->error,'error');
					 		json_out($in);
					 	}
						return false;
				 	}else if($info['http_code']==404){
				 		if($vat_numeric){
				 			if(ark::$method == 'check_vies_vat_number'){
					 			msg::error ( gm("Not a valid vat number"),'error');
					 			json_out($in);
					 		}
							return false;
				 		}
				 	}else{
				 		if($in['customer_id']){
				 			$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				 			if($country_id != 26){
				 				$this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
				 				$in['remove_v']=1;
				 			}else if($country_id == 26){
					 			$this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
					 			$in['add_v']=1;
					 		}
				 		}
				 		$in['comp_name']=$put->officialName;
				 		$in['comp_address']=$put->street.' '.$put->houseNumber;
						$in['comp_zip']=$put->postalCode;
						$in['comp_city']=$put->city;
				 		$in['comp_country']='26';
				 		$in['trends_ok']=true;
				 		$in['trends_lang']=$_SESSION['l'];
				 		$in['full_details']=$put;
				 		if(ark::$method == 'check_vies_vat_number'){
					 		msg::success(gm('Success'),'success');
					 		json_out($in);
					 	}
				 		return false;
				 	}
			 	}
			}else{
				$ch = curl_init();
				$headers=array(
					"Authorization: Bearer ".$trends_access_token
					);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			    if($vat_numeric){
			      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.$value);
			    }else{
			      	curl_setopt($ch, CURLOPT_URL, 'https://webapi.trendstop.be/api2/company/'.substr($value,2));
			    }
			    $put = json_decode(curl_exec($ch));
			 	$info = curl_getinfo($ch);

			 	/*console::log($put);
				console::log('ccccc');
				console::log($info);* /

			    if($info['http_code']==400 || $info['http_code']==429){
			    	if(ark::$method == 'check_vies_vat_number'){
				 		msg::error ( $put->error,'error');
				 		json_out($in);
				 	}
					return false;
			 	}else if($info['http_code']==404){
			 		if($vat_numeric){
			 			if(ark::$method == 'check_vies_vat_number'){
				 			msg::error (gm("Not a valid vat number"),'error');
				 			json_out($in);
				 		}
						return false;
			 		}
			 	}else{
			 		if($in['customer_id']){
			 			$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				 		if($country_id != 26){
				 			$this->db->query("UPDATE customers SET no_vat='1' WHERE customer_id='".$in['customer_id']."' ");
				 			$in['remove_v']=1;
				 		}else if($country_id == 26){
				 			$this->db->query("UPDATE customers SET no_vat='0' WHERE customer_id='".$in['customer_id']."' ");
				 			$in['add_v']=1;
				 		}
				 	}
			 		$in['comp_name']=$put->officialName;
			 		$in['comp_address']=$put->street.' '.$put->houseNumber;
					$in['comp_zip']=$put->postalCode;
					$in['comp_city']=$put->city;
				 	$in['comp_country']='26';
			 		$in['trends_ok']=true;
			 		$in['trends_lang']=$_SESSION['l'];
			 		$in['full_details']=$put;
			 		if(ark::$method == 'check_vies_vat_number'){
				 		msg::success(gm('Success'),'success');
				 		json_out($in);
				 	}
			 		return false;
			 	}
			}
		}*/


		if(!in_array(substr($value,0,2), $eu_countries)){
			$value='BE'.$value;
		}
		if(in_array(substr($value,0,2), $eu_countries)){
			$search   = array(" ", ".");
			$vat = str_replace($search, "", $value);
			$_GET['a'] = substr($vat,0,2);
			$_GET['b'] = substr($vat,2);
			$_GET['c'] = '1';
			$dd = include('valid_vat.php');
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		if(isset($response) && $response == 'invalid'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'error'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'valid'){
			$full_address=explode("\n",$result->address);
			switch($result->countryCode){
				case "RO":
					$in['comp_address']=$full_address[1];
					$in['comp_city']=$full_address[0];
					$in['comp_zip']=" ";
					break;
				case "NL":
					$zip=explode(" ",$full_address[2],2);
					$in['comp_address']=$full_address[1];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
				default:
					$zip=explode(" ",$full_address[1],2);
					$in['comp_address']=$full_address[0];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
			}

			$in['comp_name']=$result->name;

			$in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
			$in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
			$in['full_details']=$result;
			$in['vies_ok']=1;
			if($in['customer_id']){
				$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				if($in['comp_country'] != $country_id){
				 	$this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='2' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['remove_v']=1;
				}else if($in['comp_country'] == $country_id){
					$this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='1' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['add_v']=1;
				}
			}
			if(ark::$method == 'check_vies_vat_number'){
				msg::success ( gm('VAT Number is valid'),'success');
				json_out($in);
			}
			return true;
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				json_out($in);
			}
			return false;
		}
		if(ark::$method == 'check_vies_vat_number'){
			json_out($in);
		}
	}

	/**
	* undocumented function
	*
	* @return void
	* @author PM
	**/
	function tryAddC(&$in){
		if(!$this->tryAddCValid($in)){ 
			json_out($in);
			return false; 
		}
		$in['quote_id'] = $in['item_id'];

		$name_user= $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id='".$_SESSION['u_id']."'");

		if($in['add_customer']){
			$payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
       		$payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
		  	$in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
		                                        name='".$in['name']."',
		                                        btw_nr='".$in['btw_nr']."',
		                                        city_name = '".$in['city']."',
		                                        user_id = '".$_SESSION['u_id']."',
		                                        acc_manager_name = '".$name_user."',
		                                        country_name ='".get_country_name($in['country_id'])."',
		                                        active='1',
		                                        payment_term 			= '".$payment_term."',
												payment_term_type 		= '".$payment_term_type."',
												type 					= '0',
		                                        zip_name  = '".$in['zip']."'");
		  
		  	$this->db->query("INSERT INTO customer_addresses SET
		                    address='".$in['address']."',
		                    zip='".$in['zip']."',
		                    city='".$in['city']."',
		                    country_id='".$in['country_id']."',
		                    customer_id='".$in['buyer_id']."',
		                    is_primary='1',
		                    delivery='1',
		                    billing='1' ");
		  	$in['customer_name'] = $in['name'];

		  // include_once('../apps/company/admin/model/customer.php');
		  	$in['value']=$in['btw_nr'];
		  // $comp=new customer();       
		  // $this->check_vies_vat_number($in);
		  	if($this->check_vies_vat_number($in) != false){
		    	$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
		  	}else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
		    	$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
		  	}

		  	$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
		                            AND name    = 'company-customers_show_info' ");
		  	if(!$show_info->move_next()) {
		    	$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
		                        name    = 'company-customers_show_info',
		                        value   = '1' ");
		  	} else {
		    	$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
		                            AND name    = 'company-customers_show_info' ");
		 	}
		  	$count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
		  	if($count == 1){
		    	doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
		  	}

		  	if($in['item_id'] && is_numeric($in['item_id'])){
		  		$address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
		  		$this->db->query("UPDATE tblquote SET 
		  			buyer_id='".$in['buyer_id']."', 
		  			buyer_name='".$in['name']."',
		  			acc_manager_id='".$_SESSION['u_id']."',
		  			acc_manager_name='".$name_user."',
		  			contact_id='',
		  			delivery_address='', 
		  			delivery_address_id='', 
		  			free_field = '".$address."'
		  			WHERE id='".$in['item_id']."' ");		  		
		  	}
		  	msg::success (gm('Success'),'success');
		  	return true;
		}
		if($in['add_individual']){
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
                                            name='".$in['lastname']."',
                                            firstname = '".$in['firstname']."',
                                            user_id = '".$_SESSION['u_id']."',
                                            acc_manager_name = '".$name_user."',
                                            btw_nr='".$in['btw_nr']."',
                                            city_name = '".$in['city']."',
                                            c_email = '".$in['email']."',
                                            type = 1,
                                            country_name ='".get_country_name($in['country_id'])."',
                                            active='1',
                                            payment_term      = '".$payment_term."',
                                            payment_term_type     = '".$payment_term_type."',
                                            zip_name  = '".$in['zip']."'");
      
        $this->db->query("INSERT INTO customer_addresses SET
                        address='".$in['address']."',
                        zip='".$in['zip']."',
                        city='".$in['city']."',
                        country_id='".$in['country_id']."',
                        customer_id='".$in['buyer_id']."',
                        is_primary='1',
                        delivery='1',
                        billing='1' ");
        $in['customer_name'] = $in['name'];

      // include_once('../apps/company/admin/model/customer.php');
        $in['value']=$in['btw_nr'];
      // $comp=new customer();       
      // $this->check_vies_vat_number($in);
        if($this->check_vies_vat_number($in) != false){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
        }

        $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");
        if(!$show_info->move_next()) {
          $this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
                            name    = 'company-customers_show_info',
                            value   = '1' ");
        } else {
          $this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
                                AND name    = 'company-customers_show_info' ");
      }
        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
        if($count == 1){
          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
        }
        if($in['item_id'] && is_numeric($in['item_id'])){
          $address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".get_country_name($in['country_id']);
          $this->db->query("UPDATE tblquote SET 
            buyer_id='".$in['buyer_id']."', 
            buyer_name='".$in['name']."',
            acc_manager_id='".$_SESSION['u_id']."',
		  	acc_manager_name='".$name_user."',
            contact_id='',
            free_field = '".$address."'
            WHERE id='".$in['item_id']."' ");         
        }
        msg::success (gm('Success'),'success');
        return true;
    }
		if($in['add_contact']){
			$customer_name='';
			if($in['buyer_id']){
				$customer_name = addslashes($this->db->field("SELECT name FROM customers WHERE customer_id ='".$in['buyer_id']."' "));
			}
		  	$in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
		                                          firstname='".$in['firstname']."',
		                                          lastname='".$in['lastname']."',
		                                          `create`  = '".time()."',
		                                          customer_id = '".$in['buyer_id']."',
		                                          company_name=	'".$customer_name."',
		                                          email='".$in['email']."' ");
		  	$this->db->query("INSERT INTO customer_contactsIds SET customer_id='".$in['buyer_id']."', contact_id='".$in['contact_id']."' ");
		  	$in['contact_name']=$in['firstname'].' '.$in['lastname'].' >';
		  	if($in['country_id']){
		    	$this->db->query("INSERT INTO customer_contact_address SET
		                      address='".$in['address']."',
		                      zip='".$in['zip']."',
		                      city='".$in['city']."',
		                      country_id='".$in['country_id']."',
		                      contact_id='".$in['contact_id']."',
		                      is_primary='1',
		                      delivery='1' ");
		  	}
		  	$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
		                                AND name    = 'company-contacts_show_info'  ");
		  	if(!$show_info->move_next()) {
		    	$this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
		                              name    = 'company-contacts_show_info',
		                              value   = '1' ");
		  	} else {
		    	$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
		                                  AND name    = 'company-contacts_show_info' ");
		  	}
		  	$count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
		  	if($count == 1){
		    	doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
		  	}
		  	if($in['item_id'] && is_numeric($in['item_id'])){
		  		$this->db->query("UPDATE tblquote SET contact_id='".$in['contact_id']."' WHERE id='".$in['item_id']."' ");	
		  	}
		  	msg::success (gm('Success'),'success');
		}
		
		return true;
	}

	/**
	* undocumented function
	*
	* @return void
	* @author PM
	**/
	function tryAddCValid(&$in)
	{
		if($in['add_customer']){
		  	$v = new validation($in);
			$v->field('name', 'name', 'required:unique[customers.name]');
			$v->field('country_id', 'country_id', 'required');
			return $v->run();  
		}
		if($in['add_contact']){
			$v = new validation($in);
			$v->field('firstname', 'firstname', 'required');
			$v->field('lastname', 'lastname', 'required');
			$v->field('email', 'Email', 'required:email:unique[customer_contacts.email]');
			$v->field('country_id', 'country_id', 'required');
			return $v->run();  
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return boolean
	 * @author PM
	 **/
	function updateCustomerData(&$in)
	{

		if($in['field'] == 'contact_id'){
			$in['buyer_id'] ='';
		}
		if($in['quote_id']){
		    $buyer_exist = $this->db->field("SELECT buyer_id FROM tblquote WHERE id='".$in['quote_id']."'");
		    if($buyer_exist!=$in['buyer_id']){
		      $in['save_final']=1;
		    }elseif($buyer_exist==$in['buyer_id']){
		      $in['save_final']=0;
		    }
		}elseif($in['duplicate_quote_id']){
			$buyer_exist = $this->db->field("SELECT buyer_id FROM tblquote WHERE id='".$in['duplicate_quote_id']."'");
		    if($buyer_exist!=$in['buyer_id']){
		      $in['save_final']=1;
		    }elseif($buyer_exist==$in['buyer_id']){
		      $in['save_final']=0;
		    }
		}

		$sql = "UPDATE tblquote SET ";
		if($in['buyer_id']){
			$buyer_info = $this->db->query("SELECT customers.cat_id, customers.c_email, customers.our_reference, customers.comp_phone, customers.name, customers.no_vat, customers.btw_nr, 
									customers.internal_language, customers.line_discount, customers.apply_fix_disc, customers.currency_id, customers.apply_line_disc, customers.identity_id, 
									customer_addresses.address,customer_addresses.zip,customer_addresses.city,customer_addresses.country_id,customers.fixed_discount, customer_addresses.address_id
									FROM customers
									LEFT JOIN customer_addresses ON customers.customer_id = customer_addresses.customer_id
									AND customer_addresses.is_primary=1
									WHERE customers.customer_id='".$in['buyer_id']."' ");
			$buyer_info->next();
			/*if(!$in['delivery_address']){
				$delivery_addr = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['buyer_id']."' AND delivery=1 LIMIT 1 ");
				if($delivery_addr){
					$in['delivery_address'] = $delivery_addr->f('address')."\n".$delivery_addr->f('zip').'  '.$delivery_addr->f('city')."\n".get_country_name($delivery_addr->f('country_id'));
					$in['delivery_address_id']=$delivery_addr->f('address_id');
				}
			}*/

			if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
				$apply_discount = 3;
			}else{
				if($buyer_info->f('apply_fix_disc')){
					$apply_discount = 2;
				}
				if($buyer_info->f('apply_line_disc')){
					$apply_discount = 1;
				}
			}
			if($in['duplicate_quote_id']==''){
				$in['main_address_id'] = $buyer_info->f('address_id');
			}

			$in['apply_discount'] = $apply_discount;
		    
			if(ACCOUNT_ORDER_REF && $buyer_info->f('our_reference')){
				$ref = $buyer_info->f('our_reference').ACCOUNT_ORDER_DEL;
			}
			$in['currency_rate']=0;
			$in['currency_id']	= $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
			if($in['currency_id'] != ACCOUNT_CURRENCY_TYPE){
				$in['currency_rate'] = $this->currencyRate(currency::get_currency($in['currency_id'],'code'));
			}
			$in['contact_name'] ='';
			
			$sql .= " buyer_id = '".$in['buyer_id']."', ";
			$sql .= " buyer_name = '".addslashes($buyer_info->f('name'))."', ";
			$sql .= " buyer_email = '".$buyer_info->f('c_email')."', ";
			$sql .= " buyer_country_id = '".$buyer_info->f('country_id')."', ";
			$sql .= " buyer_city = '".addslashes($buyer_info->f('city'))."', ";
			$sql .= " buyer_zip = '".$buyer_info->f('zip')."', ";
			$sql .= " buyer_phone = '".$buyer_info->f('comp_phone')."', ";
			$sql .= " buyer_address = '".addslashes($buyer_info->f('address'))."', ";
			$sql .= " main_address_id = '".$in['main_address_id']."', ";
			$sql .= " email_language = '".($buyer_info->f('internal_language') ? $buyer_info->f('internal_language') : DEFAULT_LANG_ID )."', ";
			$sql .= " delivery_address = '".addslashes($in['delivery_address'])."', ";
			$sql .= " delivery_address_id = '".$in['delivery_address_id']."', ";
			$sql .= " currency_type = '".$in['currency_id']."', ";
			$sql .= " currency_rate = '".$in['currency_rate']."', ";
			$sql .= " vat = '".get_customer_vat($in['buyer_id'])."', ";
			if($in['sameAddress']==1){
				$sql .= " delivery_address = '', ";
				$sql .= " delivery_address_id = '', ";
			}
			$sql .= " identity_id = '".$buyer_info->f('identity_id')."', ";
			// $sql .= " customer_ref = '".addslashes($buyer_info->f('our_reference'))."', ";
			$sql .= " buyer_reference = '".addslashes($ref)."', ";
			
			if($in['contact_id']){
				$contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
				// $sql .= " contact_name = '".( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
				$sql .= " contact_id = '".$in['contact_id']."', ";
				$in['contact_name'] = $contact_info->f('firstname').' '.$contact_info->f('lastname')."\n";
			}else{
				// $sql .= " contact_name = '', ";
				$sql .= " contact_id = '', ";
			}
			$in['address_info'] = $buyer_info->f('address')."\n".$buyer_info->f('zip').'  '.$buyer_info->f('city')."\n".get_country_name($buyer_info->f('country_id'));
			if($buyer_info->f('address_id') != $in['main_address_id']){
				$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
				$in['address_info'] = $buyer_addr->f('address')."\n".$buyer_addr->f('zip').'  '.$buyer_addr->f('city')."\n".get_country_name($buyer_addr->f('country_id'));
			}

			$sql .= " free_field = '".addslashes($in['address_info'])."' ";
			// $sql .= " field = 'customer_id' ";
			if($in['changePrices']==1){
				$sql .= ",  apply_discount = '".$in['apply_discount']."' ";
				// $sql .= ",  remove_vat = '".$buyer_info->f('no_vat')."' ";
				$sql .= ",  discount_line_gen = '".$buyer_info->f('line_discount')."' ";
				$sql .= ",  discount = '".$buyer_info->f('fixed_discount')."' ";
				foreach ($in['article'] as $key => $value) {
					$params = array(
						'article_id' => $value['article_id'], 
						'price' => $value['price'], 
						'quantity' => $value['quantity'], 
						'customer_id' => $in['buyer_id'],
						'cat_id' => $buyer_info->f('cat_id'),
						'asString' => true
					);
					$in['article'][$key]['price'] = display_number($this->getArticlePrice($params));
				}
			}
		}else{
			if(!$in['contact_id']){
				msg::error ( gm('Please select a company or a contact'),'error');
				return false;
			}
			$contact_info = $this->db->query("SELECT  phone, cell, email, firstname, lastname FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			$contact_address = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='".$in['contact_id']."' ORDER BY is_primary DESC ");
			$sql .= " buyer_id = '', ";
			$sql .= " buyer_name = '".addslashes( $contact_info->f('firstname').' '.$contact_info->f('lastname') )."', ";
			// $sql .= " contact_name = '', ";
			$sql .= " contact_id = '".$in['contact_id']."', ";
			$sql .= " buyer_email = '".$contact_info->f('email')."', ";
			// $sql .= " customer_vat_number = '', ";
			$sql .= " buyer_country_id = '".$contact_address->f('country_id')."', ";
			$sql .= " buyer_city = '".addslashes($contact_address->f('city'))."', ";
			$sql .= " buyer_zip = '".$contact_address->f('zip')."', ";
			$sql .= " buyer_phone = '".$contact_info->f('phone')."', ";
			$sql .= " buyer_address = '".addslashes($contact_address->f('address'))."', ";
			$sql .= " email_language = '".DEFAULT_LANG_ID."', ";
			// $sql .= " delivery_address = '', ";
			// $sql .= " delivery_address_id = '', ";
			$sql .= " identity_id = '".$in['identity_id']."', ";
			$sql .= " currency_type = '".ACCOUNT_CURRENCY_TYPE."', ";
			$sql .= " currency_rate = '0', ";
			// $sql .= " customer_ref = '', ";
			$sql .= " buyer_reference = '', ";
			$in['address_info'] = $contact_address->f('address')."\n".$contact_address->f('zip').'  '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
			$sql .= " free_field = '".addslashes($in['address_info'])."' ";
			// $sql .= " field = 'contact_id' ";
			if($in['changePrices']==1){
				$sql .= ",  apply_discount = '0' ";
				// $sql .= ",  remove_vat = '0' ";
				$sql .= ",  discount_line_gen = '0' ";
				$sql .= ",  discount = '0' ";
				foreach ($in['article'] as $key => $value) {
					$params = array(
						'article_id' => $value['article_id'], 
						'price' => return_value($value['price']), 
						'quantity' => return_value($value['quantity']),
						'cat_id' => '0',
						'asString' => true
					);
					$in['article'][$key]['price'] = display_number($this->getArticlePrice($params));
				}
			}
		}
		if($in['duplicate_quote_id']){
			$sql .=" WHERE id ='".$in['duplicate_quote_id']."' ";
		}else{
			$sql .=" WHERE id ='".$in['item_id']."' ";
		}
		if(!$in['isAdd']){
			$this->db->query($sql);			
		}
		if($in['duplicate_quote_id']){
			$in['quote_id'] = $in['duplicate_quote_id'];
		}else{
			$in['quote_id'] = $in['item_id'];
		}

		msg::$success = gm('Sync successfull.');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function tryAddAddress(&$in){
		if(!$this->CanEdit($in)){
			json_out($in);
			return false;
		}
		if(!$this->tryAddAddressValidate($in)){
			json_out($in);
			return false;
		}
		$country_n = get_country_name($in['country_id']);
		if($in['field']=='customer_id'){
	 		Sync::start(ark::$model.'-'.ark::$method);

			$address_id = $this->db->insert("INSERT INTO customer_addresses SET
																				customer_id			=	'".$in['customer_id']."',
																				country_id			=	'".$in['country_id']."',
																				state_id			=	'".$in['state_id']."',
																				city				=	'".$in['city']."',
																				zip					=	'".$in['zip']."',
																				address				=	'".$in['address']."',
																				billing				=	'".$in['billing']."',
																				is_primary			=	'".$in['primary']."',
																				delivery			=	'".$in['delivery']."'");
			if($in['billing']){
				$this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
			}

			Sync::end($address_id);

			/*if($in['delivery']){
				$this->db->query("UPDATE customer_addresses SET delivery=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
			}*/
			if($in['primary']){
				if($in['address'])
				{
					if(!$in['zip'] && $in['city'])
					{
						$address = $in['address'].', '.$in['city'].', '.$country_n;
					}elseif(!$in['city'] && $in['zip'])
					{
						$address = $in['address'].', '.$in['zip'].', '.$country_n;
					}elseif(!$in['zip'] && !$in['city'])
					{
						$address = $in['address'].', '.$country_n;
					}else
					{
						$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
					}
					$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
					$output= json_decode($geocode);
					$lat = $output->results[0]->geometry->location->lat;
					$long = $output->results[0]->geometry->location->lng;
				}
				$this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
				$this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
				$this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
			}
		}else{			
			$address_id = $this->db->insert("INSERT INTO customer_contact_address SET
																				contact_id			=	'".$in['contact_id']."',
																				country_id			=	'".$in['country_id']."',																				city				=	'".$in['city']."',
																				zip					=	'".$in['zip']."',
																				address				=	'".$in['address']."',
																				is_primary			=	'".$in['primary']."',
																				delivery			=	'".$in['delivery']."'");
		}
		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer address added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['address_id'] = $address_id;
		$in['country'] = $country_n;
		$in['quote_id'] = $in['item_id'];
		if($in['quote_id'] && is_numeric($in['quote_id'])){
			$delivery_address = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
			$this->db->query("UPDATE tblquote SET 
	  			delivery_address='".$delivery_address."', 
	  			delivery_address_id='".$address_id."' WHERE id='".$in['quote_id']."'  ");	
	  	}elseif ($in['quote_id'] == 'tmp') {
	  		$in['delivery_address'] = $in['address']."\n".$in['zip'].'  '.$in['city']."\n".$country_n;
	  		$in['delivery_address_id'] = $address_id;
	  	}
	  	$in['buyer_id'] = $in['customer_id'];
		// $in['pagl'] = $this->pag;
		
		 //json_out($in);
		return true;

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function tryAddAddressValidate(&$in)
	{
		$v = new validation($in);
		if($in['customer_id']){
			$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
		}else if($in['contact_id']){
			$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
		}else{
			msg::error(gm('Invalid ID'),'error');
			return false;
		}
		$v->field('country_id', 'Country', 'required:exist[country.country_id]');

		return $v->run();
	}

	/**
	 * add quote function
	 *
	 * @return void
	 * @author PM
	 **/
	function add(&$in)
	{

		$f_archived = '0';
		$serial_number=$in['serial_number'];
		if($in['template']){
			$f_archived='2';
			if(!$in['template_name']){
				msg::error (gm("Template name required"),'success');
				return false;
			}
			$serial_number=$in['template_name'];
		}else{
			if(!$this->add_validate($in))
			{
				json_out($in);
				return false;
			}
			$serial_number = $in['serial_number'];
		}

		$created_date= date(ACCOUNT_DATE_FORMAT);
		if(!$in['pdf_layout'] && $in['identity_name']){
	      $in['pdf_layout'] = $this->db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$in['identity_name']."' and module='quote'");
	    }

    	if( $in['main_address_id'] ){
			$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
			$in['free_field'] = addslashes($buyer_addr->f('address'))."\n".addslashes($buyer_addr->f('zip')).'  '.addslashes($buyer_addr->f('city'))."\n".get_country_name($buyer_addr->f('country_id'));
		}
	    
	    $in['validity_date2'] = strtotime($in['validity_date']);
	    $in['quote_date'] = strtotime($in['quote_date']);
		//insert into quote
		$in['quote_id']=$this->db->insert("INSERT INTO tblquote SET
                                      serial_number       		=   '".$serial_number."',
                                      own_reference           	=   '".$in['own_reference']."',
                                      quote_date       			=   '".$in['quote_date']."',
                                      discount      	    	=   '".return_value($in['discount'])."',
                                      discount_line_gen      	=   '".return_value($in['discount_line_gen'])."',
                                      apply_discount            =   '".$in['apply_discount']."',
                                      currency_type       		=   '".$in['currency_type']."',
                                      vat						=   '".return_value($in['vat'])."',
                                      type                		=   '".$in['type']."',
                                      status              		=   '0',
                                      f_archived          		=   '".$f_archived."',
                                      buyer_id       	      	=   '".$in['buyer_id']."',
                                      contact_id				=   '".$in['contact_id']."',
                                      buyer_reference     		=   '".$in['buyer_reference']."',
                                      buyer_name       			=   '".$in['buyer_name']."',
                                      buyer_email       		=   '".$in['buyer_email']."',
                                      buyer_address       		=   '".$in['buyer_address']."',
                                      buyer_zip       			=   '".$in['buyer_zip']."',
                                      buyer_city       			=   '".$in['buyer_city']."',
                                      buyer_country_id      	=   '".$in['buyer_country_id']."',
                                      buyer_state_id       		=   '".$in['buyer_state_id']."',
			                          seller_id             	=   '".$in['seller_id']."',
                                      seller_name       		=   '".$in['seller_name']."',
                                      seller_d_address    	 	=   '".$in['seller_d_address']."',
                                      seller_d_zip       		=   '".$in['seller_d_zip']."',
                                      seller_d_city         	=   '".$in['seller_d_city']."',
                                      seller_d_country_id   	=   '".$in['seller_d_country_id']."',
                                      seller_d_state_id     	=   '".$in['seller_d_state_id']."',
                                      seller_b_address     		=   '".$in['seller_b_address']."',
                                      seller_b_zip       		=   '".$in['seller_b_zip']."',
                                      seller_b_city       		=   '".$in['seller_b_city']."',
                                      seller_b_country_id   	=   '".$in['seller_b_country_id']."',
                                      seller_b_state_id     	=   '".$in['seller_b_state_id']."',
                                      seller_bwt_nr         	=   '".$in['seller_bwt_nr']."',
                                      delivery_address 			=   '".$in['delivery_address']."',
                                      delivery_address_id		=   '".$in['delivery_address_id']."',
                                      use_package             	=   '".ALLOW_QUOTE_PACKING."',
                                      use_sale_unit           	=   '".ALLOW_QUOTE_SALE_UNIT."',
                                      created             		=   '".$created_date."',
                                      created_by          		=   '".$_SESSION['u_id']."',
                                      last_upd            		=   '".$created_date."',
                                      last_upd_by         		=   '".$_SESSION['u_id']."',
                                      author_id					= 	'".$_SESSION['u_id']."',
                                      email_language			= 	'".$in['email_language']."',
                                      show_vat					= 	'".$in['show_vat']."',
                                      show_vat_pdf				= 	'".$in['show_vat_pdf']."',
                                      show_grand				= 	'".$in['show_grand']."',
                                      currency_rate         	=   '".$in['currency_rate']."',
                                      subject         			=   '".$in['subject']."',
                                      validity_date      		=   '".$in['validity_date2']."',
                                      preview 					=	'0',
                                      show_img_q				=   '".$in['show_img_q']."',
                                      acc_manager_id           	=	'".$in['acc_manager_id']."',
                                      acc_manager_name          =	'".$in['acc_manager_name']."',
                                      free_field 				=	'".$in['free_field']."',
                                      identity_id 				=   '".$in['identity_id']."',
                                      show_QC					=	'".$in['show_QC']."',
                                      show_UP					=	'".$in['show_UP']."',
                                      show_V					=	'".$in['show_V']."',
                                      show_PS					=	'".$in['show_PS']."',
                                      show_d					=	'".$in['show_d']."',
                                      main_address_id			= 	'".$in['main_address_id']."',
                                      pdf_layout				=   '".$in['pdf_layout']."' ");


		/*if($in['author_id']){
			$this->db->query("UPDATE tblquote SET created_by='".$in['author_id']."' WHERE id='".$in['quote_id']."' ");
		}*/

		if($in['duplicate_quote_id']) {
			$this->db->query("UPDATE tblquote SET author_id = '".$_SESSION['u_id']."' ");
		}
		$in['duplicate_quote_id'] = null;
		if($in['general_conditions_new_page']){
			$this->db->query("UPDATE tblquote SET general_conditions_new_page='1' WHERE id='".$in['quote_id']."' ");
		}

		//version
		include_once(__DIR__.'/quote_version.php');
		$version = new quote_version();
		$version->add($in);
		$this->quote_lines($in);

        $lang_note_id = $in['langs'];
		// add multilanguage quote notes in note_fields  DEFAULT_LANGS
		        $lang_note_id = $in['email_language'];
          if($lang_note_id == $in['email_language']){
            $active_lang_note = 1;
          }else{
            $active_lang_note = 0;
          }
          $this->db->insert("INSERT INTO note_fields SET
                            active 			=	'1',
                            item_id         = 	'".$in['quote_id']."',
                            lang_id         =   '".$in['email_language']."',
                            item_type       =   'quote',
                            item_name       =   'notes',
                            version_id 		= 	'".$in['version_id']."',
                            item_value      =   '".$in['notes']."'
        ");
/*            $this->db->insert("INSERT INTO note_fields SET
                            item_id         = '".$in['quote_id']."',
                            lang_id         =   '".$in['email_language']."',
                            item_type         =   'quote',
                            item_name         =   'notes',
                            item_value        =   '".$in['NOTE']."'
            ");*/
/*        $langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
        while($langs->next()){
        	if($lang_note_id == $langs->f('lang_id')){
        		$active_lang_note = 1;
        	}else{
        		$active_lang_note = 0;
        	}
        	$this->db->insert("INSERT INTO note_fields SET
        										item_id 				=	'".$in['quote_id']."',
        										lang_id 				= 	'".$langs->f('lang_id')."',
        										active 					= 	'".$active_lang_note."',
        										item_type 				= 	'quote',
        										item_name 				= 	'notes',
        										version_id 				= 	'".$in['version_id']."',
        										item_value				= 	'".$in['notes_'.$langs->f('lang_id')]."'
        	");
        }*/

        // add multilanguage quote notes in note_fields  CUSTOM LANGS
/*        $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
        while($custom_langs->next()) {
        	if($lang_note_id == $custom_langs->f('lang_id')) {
        		$active_lang_note = 1;
        	} else {
        		$active_lang_note = 0;
        	}
        	$this->db->query("INSERT INTO note_fields SET 	item_id 		=	'".$in['quote_id']."',
        													lang_id 		= 	'".$custom_langs->f('lang_id')."',
        													active 			= 	'".$active_lang_note."',
        													item_type 		= 	'quote',
        													item_name 		=   'notes',
        													version_id		= 	'".$in['version_id']."',
        													item_value 		= 	'".$in['notes_.'.$custom_langs->f('lang_id')]."'
        	");
        }*/


        if($in['free_text_content']){

			$this->db->query("INSERT INTO note_fields SET
				item_value				= 	'".$in['free_text_content']."',
				item_type 				= 	'quote',
				item_name 				= 	'free_text_content',
				version_id 				= 	'".$in['version_id']."',
				item_id 				= 	'".$in['quote_id']."' ");
		}
		$in['pdfPreview'] = 'index.php?do=quote-quote_print&id='.$in['quote_id'].'&version_id='.$in['version_id'].'&lid='.$in['email_language'].'&type='.ACCOUNT_QUOTE_PDF_FORMAT;
		$in['begin_pdf_link'] = 'index.php?do=quote-quote_print&id='.$in['quote_id'].'&version_id='.$in['version_id'].'&lid=';
		$in['end_pdf_link'] = '&type='.ACCOUNT_QUOTE_PDF_FORMAT;
		msg::success (gm("Quote was created"),'success');
		// ark::$controller = $in['controller'];

		$in['add_problem'] = $this->pag;
		insert_message_log($this->pag,'{l}Quote was created by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['quote_id'],false,$_SESSION['u_id']);
		$in['pagl'] = $this->pag;

		if($in['opportunity_id']){
			$this->db->query("INSERT INTO tblopportunity_quotes SET opportunity_id = '".$in['opportunity_id']."', quote_id = '".$in['quote_id']."' ");
			$source_id = $this->db->field("SELECT source_id FROM tblopportunity WHERE opportunity_id = '".$in['opportunity_id']."' ");
			$t_id = $this->db->field("SELECT t_id FROM tblopportunity WHERE opportunity_id = '".$in['opportunity_id']."' ");
			$this->db->query("UPDATE tblquote SET source_id = '".$source_id."', t_id = '".$t_id."' WHERE id = '".$in['quote_id']."' ");
		}

		// $in['dropbox'] = new drop('quotes',$in['buyer_id'],$in['quote_id']);
		
		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
																		AND name	= 'quote-quotes_show_info'	");
		if(!$show_info->move_next()) {
			$this->db_users->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
																name = 'quote-quotes_show_info',
																value = '1' ");
		} else {
			$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id = '".$_SESSION['u_id']."'
															   AND name 	 = 'quote-quotes_show_info' ");
		}

		if($in['template']){
			msg::success (gm("Saved"),'success');
			// ark::$controller = 'nquote';
			$in['template_id'] = $in['quote_id'];
			// $in['quote_id'] = null;
		}
		$count = $this->db->field("SELECT COUNT(id) FROM tblquote ");
		if($count == 1){
			doManageLog('Created the first quote.','',array( array("property"=>'first_module_usage',"value"=>'Quote') ));
		}

		//ark::$controller = 'nquote';
		//header("Location:index.php?do=quote-nquote&quote_template_id=".$in['quote_id']."&template=1");
		return true;
	}

	private function quote_lines(&$in){
		
		//INSERT LINES
		// $lines_type = array('6','7');
		$use_package=$this->db->field("SELECT use_package FROM tblquote WHERE id='".$in['quote_id']."'");
		$use_sale_unit=$this->db->field("SELECT use_sale_unit FROM tblquote WHERE id='".$in['quote_id']."'");
		$margin_total=0;
		$quote_total=0;
		$quote_total_articles=0;
		$quote_total_vat=0;
		if(is_array($in['quote_group']) && !empty($in['quote_group'])){
			$group_order = 0;
			foreach($in['quote_group'] as $group_code => $group){
				$group_id = $this->db->insert("INSERT INTO tblquote_group SET quote_id = '".$in['quote_id']."',
																				title = '".addslashes($group['title'])."',
																				version_id 	=   '".$in['version_id']."',
																				sort_order = '".$group_order."',
																				show_chapter_total 	=   '".$group['show_chapter_total']."',
																				pagebreak_ch='".$group['pagebreak_ch']."',
																				show_QC = '".$group['show_QC']."' ,
																				show_UP = '".$group['show_UP']."' ,
																				show_PS = '".$group['show_PS']."' ,
																				show_d = '".$group['show_d']."'  ");
				$group_order += 1;
				if(is_array($group['table']) || !empty($group['table'])){
					$i = 0;
					foreach($group['table'] as $table_id => $table){
						$sort_order=0;
						if($table['q_table'] == true){
							foreach($table['quote_line'] as $index => $val){
								// console::log()
								if(empty($val['description']) ){
									continue;
								}
								if(ark::$method=='add'){
									if(!ALLOW_QUOTE_PACKING){
										$val['package']=1;
									}
									if(!ALLOW_QUOTE_SALE_UNIT){
										$val['sale_unit']=1;
									}
								}else{ //update
									if(!$use_package){
										$val['package']=1;
									}
									if(!$use_sale_unit){
										$val['sale_unit']=1;
									}
								}
								$line_total = return_value($val['quantity']) * return_value($val['price']) * ($val['package']/ $val['sale_unit']);
								$line_total=$line_total- ($line_total* return_value($val['line_discount'])/100);
								if(!is_numeric($val['line_type'])) {
									$val['line_type']=1;
								}
								$quote_total += $line_total;
				                if($val['article_id']){
				                    $quote_total_articles += $line_total;
				                }
					            $quote_total_vat += ($line_total) * (return_value($val['vat']) / 100);
								if($val['article_id']){
                     				$margin_total+=($line_total)-($val['purchase_price']*return_value($val['quantity']));
			                	}
			                	if(!$val['purchase_price']){
			                		$purchase_price1 = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$val['article_id']."' AND pim_article_prices.base_price='1'");
			                		$val['purchase_price']=$purchase_price1;

			                	}
								$this->db->query("INSERT INTO tblquote_line SET
								                name       				=   '".htmlspecialchars($val['description'],ENT_COMPAT | ENT_HTML401,"UTF-8")."',
								                quote_id   	    		=   '".$in['quote_id']."',
								                version_id 	    		=   '".$in['version_id']."',
								                content_type        	=   '1',
								                line_type           	=   '".$val['line_type']."',
								                group_id            	=   '".$group_id."',
								                table_id            	=   '".$table['group_table_id']."',
								                article_code        	=   '".addslashes($val['article_code'])."',
	                              				article_id          	=   '".$val['article_id']."',    
	                              				is_tax					=	'".$val['is_tax']."',
	                              				quantity            	=   '".return_value($val['quantity'])."',
	                              				line_discount       	=   '".return_value($val['line_discount'])."',
								                package             	=   '".return_value2($val['package'])."',
								                sale_unit           	=   '".return_value($val['sale_unit'])."',
								                price               	=   '".return_value($val['price'])."',
								                purchase_price			=	'".$val['purchase_price']."',
								                amount              	=   '".$line_total."',
								                f_archived          	= 	'0',
								                created             	= 	'".$created_date."',
								                created_by          	= 	'".$_SESSION['u_id']."',
								                last_upd            	= 	'".$created_date."',
								                last_upd_by         	= 	'".$_SESSION['u_id']."'  ,
								                vat						=	'".$val['line_vat']."',
								                line_order          	=   '".$index."',
							            		sort_order				=	'".$table['sort_order']."',
							            		show_block_total		=	'".$val['show_block_total']."',
							            		hide_AC					=	'".$val['hide_AC']."',
							            		hide_QC					=	'".($in['show_QC'] == '1' ? '0': '1')."',
							            		hide_UP 				= 	'".($in['show_UP'] == '1' ? '0': '1')."',
							            		hide_DISC 				= 	'".($in['show_d'] == '1' ? '0': '1')."',
							            		show_block_vat			=	'".$val['show_lVATv']."',
							            		service_bloc_title		=	'".addslashes($table['item_text'])."'
								");
								$i += 1;
								$sort_order++;
							}
						}
						elseif($table['q_content'] == true){
							$content = str_replace('?','&#63;', $table['quote_content']);
							$this->db->query("INSERT INTO tblquote_line SET quote_id   	    	=   '".$in['quote_id']."',
																			version_id 	    	=   '".$in['version_id']."',
																			content_type        =   '2',
																			group_id            =   '".$group_id."',
																			table_id            =   '".$table['group_content_id']."',
																			content             =   '".addslashes($content)."',
																			f_archived          = 	'0',
																			created             = 	'".$created_date."',
																			created_by          = 	'".$_SESSION['u_id']."',
																			last_upd            = 	'".$created_date."',
																			last_upd_by         = 	'".$_SESSION['u_id']."',
                                                							sort_order			=	'".$table['sort_order']."'");
						}
						elseif($table['pagebreak'] == true){
							$this->db->query("INSERT INTO tblquote_line SET quote_id   	    =   '".$in['quote_id']."',
																		 version_id 	    =   '".$in['version_id']."',
																		content_type        =   '3',
																		group_id            =   '".$group_id."',
																		table_id            =   '".$table['group_break_id']."',
																		content             =   '<br pagebreak=\"true\"/>',
																		f_archived          = 	'0',
																		created             = 	'".$created_date."',
																		created_by          = 	'".$_SESSION['u_id']."',
																		last_upd            = 	'".$created_date."',
																		last_upd_by         = 	'".$_SESSION['u_id']."',
                                                						sort_order			=	'".$table['sort_order']."'");
						}
					}
				}
			}
		}
		    $quote_total=round($quote_total,2);
		    $quote_total_vat=round($quote_total_vat,2);
		    $quote_total_vat += $quote_total;
		    $margin_total=$margin_total;
		    $margin_percent_total=($margin_total/$quote_total_articles)*100;

		    if($in['currency_type'] != ACCOUNT_CURRENCY_TYPE){
		      if($in['currency_rate']){
		        $quote_total = $quote_total*return_value($in['currency_rate']);
		        $quote_total_articles = $quote_total_articles*return_value($in['currency_rate']);
		        $quote_total_vat = $quote_total_vat*return_value($in['currency_rate']);

		      }
		    }

		    $this->db->query("UPDATE tblquote SET margin='".$margin_total."', margin_percent='".$margin_percent_total."',amount='".$quote_total."', amount_vat='".$quote_total_vat."' WHERE id='".$in['quote_id']."'");
	}

	/**
	 * add quote validate function
	 *
	 * @return void
	 * @author PM
	 **/
	function add_validate(&$in)
	{
		// $in['quote_date'] = strtotime($in['quote_ts']);
		$is_ok=true;

		$v = new validation($in);

		$v->field('serial_number','Quote Nr','required','Quote Nr. is required');
		$v->field('quote_date','Date','required','Date is required');

		$is_ok = $v->run();

		if($in['main_quote_id']){
			$filter .=" AND tblquote_version.main_quote_id!= '".$in['main_quote_id']."' ";
		}
		if(ark::$method == 'update') {
			$filter = " AND tblquote.id !='".$in['quote_id']."' AND f_archived='0' ";
		}
		// console::log('ff');
		$this->db->query("SELECT tblquote.id FROM tblquote INNER JOIN tblquote_version ON tblquote.id=tblquote_version.quote_id WHERE tblquote.serial_number='".$in['serial_number']."' and tblquote.serial_number!='' $filter");
		if($this->db->move_next()){
			msg::error(gm("Quote Nr. already used"),'error');
			// console::log('u');
			if(ark::$method == 'add'){
				// console::log('add');
				$in['serial_number'] = generate_quote_number(DATABASE_NAME);
				// console::log($in['serial_number']);
			}else{
				// console::log('update');
				$is_ok=false;
			}
		}

		return $is_ok;
	}

	/**
	 * update quote validate function
	 *
	 * @return void
	 * @author PM
	 **/
	function update(&$in)
	{
		$initial_quote = $this->get($in['quote_id']);
		if($in['template']){
			$f_archived='2';
			$in['serial_number'] = $in['template_name'];
		}else{
			if(!$this->update_validate($in))
			{
				return false;
			}
		}
		$created_date= date(ACCOUNT_DATE_FORMAT.' H:i:s');
		$updatedd_date= date(ACCOUNT_DATE_FORMAT.' H:i:s');

		//update quote

		if($in['buyer_id']){
			$in['buyer_name'] = addslashes($this->db->field("SELECT name FROM customers WHERE customer_id = '".$in['buyer_id']."'"));
		} else {
			$contact_name = $this->db->query("SELECT firstname, lastname FROM  customer_contacts WHERE contact_id = '".$in['contact_id']."' ");
			$contact_name->next();
			$in['buyer_name'] = addslashes($contact_name->f('firstname').' '.$contact_name->f('lastname'));
		}

		$can_do = false;
		$buyer_id_old = $this->db->field("SELECT buyer_id FROM tblquote WHERE id = '".$in['quote_id']."' ");
		if(($in['buyer_id'] != $buyer_id_old || $in['customer_changed'])) {
			$can_do = true;
		}
		$contact_id_old = $this->db->field("SELECT contact_id FROM tblquote WHERE id = '".$in['quote_id']."' ");
		$can_do_contact = false;
		if($in['contact_id'] != $contact_id_old || $in['contact_changed']) {
			$can_do_contact = true;
		}
		if( $in['main_address_id']){
			$buyer_addr = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$in['main_address_id']."' ");
			$in['free_field'] = addslashes($buyer_addr->f('address'))."\n".addslashes($buyer_addr->f('zip')).'  '.addslashes($buyer_addr->f('city'))."\n".addslashes(get_country_name($buyer_addr->f('country_id')));
		}
		$in['validity_date2'] = strtotime($in['validity_date']);
		$in['quote_date2'] = strtotime($in['quote_date']);
	
		$this->db->query("UPDATE tblquote SET
                                                serial_number       				=   '".$in['serial_number']."',
                                                quote_date       					=   '".$in['quote_date2']."',
                                                own_reference           			=   '".$in['own_reference']."',
                                                discount      	    				=   '".return_value($in['discount'])."',
                                                discount_line_gen      	    		=   '".return_value($in['discount_line_gen'])."',
                                                apply_discount        		  		=   '".$in['apply_discount']."',
                                                currency_type      				 	=   '".$in['currency_type']."',
                                                vat									=   '".return_value($in['vat'])."',
                                                type             			 	  	=   '".$in['type']."',
                                                buyer_id       	  		    		=   '".$in['buyer_id']."',
                                                contact_id							=   '".$in['contact_id']."',
                                                buyer_reference   			  		=   '".$in['buyer_reference']."',
                                                buyer_name       					=   '".$in['buyer_name']."',
                                                buyer_address     			  		=   '".$in['buyer_address']."',
                                                buyer_zip       					=   '".$in['buyer_zip']."',
                                                buyer_city       					=   '".$in['buyer_city']."',
                                                buyer_country_id    		  		=   '".$in['buyer_country_id']."',
                                                buyer_state_id     			  		=   '".$in['buyer_state_id']."',
			                        			seller_id          					=   '".$in['seller_id']."',
                                                seller_name       					=   '".$in['seller_name']."',
                                                seller_d_address    		  		=   '".$in['seller_d_address']."',
                                                seller_d_zip       					=   '".$in['seller_d_zip']."',
                                                seller_d_city       				=   '".$in['seller_d_city']."',
                                                seller_d_country_id  			 	=   '".$in['seller_d_country_id']."',
                                                seller_d_state_id    		 		=   '".$in['seller_d_state_id']."',
                                                seller_b_address      				=   '".$in['seller_b_address']."',
                                                seller_b_zip       					=   '".$in['seller_b_zip']."',
                                                seller_b_city      				 	=   '".$in['seller_b_city']."',
                                                seller_b_country_id   				=   '".$in['seller_b_country_id']."',
                                                seller_b_state_id     				=   '".$in['seller_b_state_id']."',
                                                seller_bwt_nr       	 	 		=   '".$in['seller_bwt_nr']."',
                                                delivery_address 					=   '".$in['delivery_address']."',
                                                delivery_address_id					=   '".$in['delivery_address_id']."',
                                                last_upd          			  		=   '".$updatedd_date."',
                                                author_id							= 	'".$in['author_id']."',
                                                show_vat							= 	'".$in['show_vat']."',
                                                show_vat_pdf						= 	'".$in['show_vat_pdf']."',
                                                show_grand							= 	'".$in['show_grand']."',
                                                last_upd_by   			      		=   '".$_SESSION['u_id']."',
                                                general_conditions_new_page			=	'0',
                                                email_language						= 	'".$in['email_language']."',
                                                free_field							=	'".$in['free_field']."',
                                                validity_date      					=   '".$in['validity_date2']."',
                                                show_img_q							=   '".$in['show_img_q']."',
                                                acc_manager_id           			=	'".$in['acc_manager_id']."',
                                                acc_manager_name           			=	'".$in['acc_manager_name']."',
                                                subject								= 	'".$in['subject']."',
                                                identity_id 						=	'".$in['identity_name']."',
                                                show_QC								=	'".$in['show_QC']."',
                                                show_UP								=	'".$in['show_UP']."',
                                                show_V								=	'".$in['show_V']."',
                                                show_PS								=	'".$in['show_PS']."',
                                                show_d								=	'".$in['show_d']."',
                                                main_address_id						=	'".$in['main_address_id']."',
                                                downpayment_val						=	'".return_value($in['quote_downpayment'])."'
                           WHERE id = '".$in['quote_id']."' ");

		// source_id 				=	'".$in['source_id']."',
		// t_id 					=	'".$in['t_id']."',
		// stage_id         		=   '".$in['stage_id']."',
		// notes                 	=   '".utf8_encode($in['notes'])."',

		/*if($in['author_id']){
			$this->db->query("UPDATE tblquote SET created_by='".$in['author_id']."' WHERE id='".$in['quote_id']."' ");
		}*/


		if($in['general_conditions_new_page']){
			$this->db->query("UPDATE tblquote SET general_conditions_new_page='1' WHERE id='".$in['quote_id']."' ");
		}
		if($in['buyer_id']) {
			if($can_do == true ) {
				$address = $this->db->query("SELECT city, zip, address, country_id FROM customer_addresses WHERE customer_id = '".$in['buyer_id']."' AND is_primary=1 ");
				if($address->next()) {
					$address_field = $address->f('address')."\n".$address->f('zip').' '.$address->f('city')."\n".get_country_name($address->f('country_id'));
					$this->db->query("UPDATE tblquote SET free_field = '".addslashes($address_field)."' WHERE id = '".$in['quote_id']."' ");
					$this->db->query("UPDATE tblquote SET 	buyer_address 		= '".addslashes($address->f('address'))."',
															buyer_zip    	 	= '".$address->f('zip')."',
															buyer_city    		= '".addslashes($address->f('city'))."',
															buyer_country_id 	= '".$address->f('country_id')."' WHERE id = '".$in['quote_id']."' ");
				}


			}
		}

		if($in['contact_id']) {
			if($can_do_contact == true) {
				$this->db->query("UPDATE tblquote SET contact_id = '".$in['contact_id']."' where id = '".$in['quote_id']."' ");
				if(!$in['buyer_id']) {
					$contact_address = $this->db->query("SELECT city, zip, address, country_id FROM customer_contact_address WHERE contact_id = '".$in['contact_id']."' ");
					if($contact_address->move_next()) {
						$address_field_contact = $contact_address->f('address')."\n".$contact_address->f('zip').' '.$contact_address->f('city')."\n".get_country_name($contact_address->f('country_id'));
						$this->db->query("UPDATE tblquote SET free_field = '".addslashes($address_field_contact)."' WHERE id = '".$in['quote_id']."' ");
						$this->db->query("UPDATE tblquote SET 	buyer_address 		= '".addslashes($contact_address->f('address'))."',
															buyer_zip    	 	= '".$contact_address->f('zip')."',
															buyer_city    		= '".addslashes($contact_address->f('city'))."',
															buyer_country_id 	= '".$contact_address->f('country_id')."' WHERE id = '".$in['quote_id']."' ");
					}

				}
			}

		}

		// update multilanguage quote note in note_fields
		$lang_note_id = $in['email_language'];

		//if we select a custom_lang we update email_language from tblquote.

		// $this->db->query("UPDATE tblquote SET email_language = '".$in['langs']."' WHERE id = '".$in['quote_id']."' ");

		#default langs
        $langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
        // add multilanguage quote notes in note_fields  DEFAULT_LANGS
		        $lang_note_id = $in['email_language'];
        if($lang_note_id == $in['email_language']){
            $active_lang_note = 1;
        }else{
            $active_lang_note = 0;
        }
        $this->db->query("DELETE FROM note_fields WHERE item_id = '".$in['quote_id']."' AND item_type = 'quote' AND item_name = 'notes' AND version_id = '".$in['version_id']."' AND lang_id = '".$in['email_language']."' ");

        $this->db->insert("INSERT INTO note_fields SET
                            active 			=	'1',
                            item_id         = 	'".$in['quote_id']."',
                            lang_id         =   '".$in['email_language']."',
                            item_type       =   'quote',
                            item_name       =   'notes',
                            version_id 		= 	'".$in['version_id']."',
                            item_value      =   '".$in['notes']."'
        ");
       /* while($langs->next()){
        	foreach ($in['translate_loop'] as $key => $value) {
	    		if($value['lang_id'] == $langs->f('lang_id')){
	    			$in['nota'] = $value['notes'];
	    			break;
	    		}
	    	}
        	if($lang_note_id == $langs->f('lang_id')){
        		$active_lang_note = 1;
        	}else{
        		$active_lang_note = 0;
        	}
        	$this->db->query("UPDATE note_fields SET
        										active 					= 	'".$active_lang_note."',
        										item_value				= 	'".$in['nota']."'
        								WHERE 	item_type 				= 	'quote'
        								AND		item_name 				= 	'notes'
        								AND 	version_id 				= 	'".$in['version_id']."'
        								AND		lang_id 				= 	'".$langs->f('lang_id')."'
        								AND		item_id 				= 	'".$in['quote_id']."'

        	");
        }*/
        #custom langs
/*        $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
        while($custom_langs->next()) {
        	foreach ($in['translate_loop_custom'] as $key => $value) {
	    		if($value['lang_id'] == $custom_langs->f('lang_id')){
	    			$in['nota'] = $value['notes'];
	    			break;
	    		}
	    	}
        	if($lang_note_id == $custom_langs->f('lang_id')) {
        		$active_lang_note_custom = 1;
        	} else {
        		$active_lang_note_custom = 0;
        	}
        	$this->db->query("UPDATE note_fields SET 	active 		=  	'".$active_lang_note_custom."',
        												item_value 	=  	'".$in['nota']."'
        										 WHERE  item_type 	=  	'quote'
        										 AND 	item_name 	=  	'notes'
        										 AND 	version_id	= 	'".$in['version_id']."'
        										 AND 	lang_id 	= 	'".$custom_langs->f('lang_id')."'
        										 AND 	item_id 	= 	'".$in['quote_id']."'
        	");
        }
*/
		$this->db->query("DELETE FROM note_fields
						  	WHERE 	item_type 	= 'quote'
						  	AND 	item_name 	= 'free_text_content'
						  	AND 	version_id 	= '".$in['version_id']."'
						  	AND 	item_id 	= '".$in['quote_id']."' ");

		$this->db->query("INSERT INTO note_fields SET
			item_value				= 	'".$in['free_text_content']."',
			item_type 				= 	'quote',
			item_name 				= 	'free_text_content',
			version_id 				= 	'".$in['version_id']."',
			item_id 				= 	'".$in['quote_id']."' ");

		//INSERT LINES
		$this->db->query("DELETE FROM tblquote_line WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");
		$this->db->query("DELETE FROM tblquote_group WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");
		$this->db->query("UPDATE tblquote_version SET version_date='".$in['quote_date2']."' ,
																									discount      	    				=   '".return_value($in['discount'])."',
                                                	discount_line_gen      	    =   '".return_value($in['discount_line_gen'])."',
                                                	apply_discount        		  =   '".$in['apply_discount']."'
											WHERE version_id='".$in['version_id']."' ");
		$this->quote_lines($in);
		
		if($in['success_msg']){
			// msg::$success = gm($in['success_msg']);
			msg::success ($in['success_msg'],'success');
		}else{
			msg::success (gm("Quote has been updated"),'success');	
		}
		// ark::$controller=$in['controller'];
		$tmpstp = $this->db->field("SELECT tmpstp FROM tblquote WHERE id='".$in['quote_id']."' ");
		if($in['tmpstp'] != $tmpstp){
			insert_message_log($this->pag,'{l}Quote has been updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['quote_id']);
			$this->db->query("UPDATE tblquote SET tmpstp='".$in['tmpstp']."' WHERE id='".$in['quote_id']."' ");
		}
		$in['pagl'] = $this->pag;
		if($in['template']){
			$in['quote_template_id'] = $in['quote_id'];
			$in['quote_id']='';
			// ark::$controller = 'nquote';
		}
		$in['add_problem'] = 'quote';
		$updated_quote = $this->get($in['quote_id']);
		// console::log($updated_quote, $initial_quote);
		if(!$this->check_diff($updated_quote, $initial_quote)){
			global $config;
			$this->db->query("UPDATE tblquote_version SET preview='0' WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."' ");
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_".$in['quote_id']."_".$in['version_id']."_%' ");
		}
		return true;
	}

	/**
	 * update quote validate function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_validate(&$in)
	{
		$is_ok = true;
		$v = new validation($in);
		$v->field('quote_id', 'ID', 'required:exist[tblquote.id]', "Invalid Id");
		$is_ok = $v->run();

		if (!$this->add_validate($in))
		{
			$is_ok = false;
		}
		return $is_ok;
	}

	/**
	 * get quote info function
	 *
	 * @return void
	 * @author PM
	 **/
	function get($id){
		if(!$id){
			return;
		}
		# main tables
		$q = $this->db->query("SELECT * FROM tblquote WHERE id='".$id."'")->getAll();
		$quote = $this->parse_results($q[0]);

		$qgroup = $this->db->query("SELECT * FROM tblquote_group WHERE quote_id='".$id."' order by group_id asc ")->getAll();
		$groups = $this->parse_results($qgroup,'version_id',true);

		$qhistory = $this->db->query("SELECT *  FROM  `tblquote_history` WHERE quote_id='".$id."' ")->getAll();
		$history = $this->parse_results($qhistory);

		$qlines = $this->db->query("SELECT * FROM  `tblquote_line` WHERE quote_id='".$id."' order by id asc ")->getAll();
		$lines = $this->parse_results($qlines,'version_id',true);

		$qversion = $this->db->query("SELECT * FROM  `tblquote_version` WHERE quote_id='".$id."' order by version_id asc ")->getAll();
		$version = $this->parse_results($qversion,'version_id');

		$notes = $this->db->query("SELECT * FROM `note_fields` WHERE `item_type`='quote' AND `item_id`='".$id."' order by note_fields_id asc ")->getAll();
		$notes = $this->parse_results($notes,'version_id',true);

		# adjacent tables
		$tblquote_lost_reason = $this->db->query("SELECT * FROM  `tblquote_lost_reason` ")->getAll();
		$tblquote_lost_reason = $this->parse_results($tblquote_lost_reason,'id');

		$tblquote_source = $this->db->query("SELECT * FROM  `tblquote_source` ")->getAll();
		$tblquote_source = $this->parse_results($tblquote_source,'id');

		$tblquote_type = $this->db->query("SELECT * FROM  `tblquote_type` ")->getAll();
		$tblquote_type = $this->parse_results($tblquote_type,'id');

		$result = array('tblquote'=>$quote,
										'tblquote_version'=>$version,
										'tblquote_line'=>$lines,
										'tblquote_group'=>$groups,
										'note_fields'=>$notes,
										// 'tblquote_history'=>$history
										);

		// console::log($notes);
		return $result;
	}

	/**
	 * parse initial quote function
	 *
	 * @return void
	 * @author PM
	 **/
	function parse_results($array,$primary_key = '',$group_by_primary = false){
		$ar = array();
		foreach ($array as $key => $value) {
			if(is_array($value)){
				$arr = array();
				$p_k = '';
				foreach ($value as $k => $val) {
					if(is_numeric($k)){
						continue;
					}
					if($k == $primary_key){
						$p_k = $val;
					}
					$arr[$k] = $val;
				}
				if($primary_key && $p_k && !$group_by_primary){
					$ar[$p_k] = $arr;
				}elseif($primary_key && $p_k && $group_by_primary){
					if(!is_array($ar[$p_k])){
						$ar[$p_k] = array();
					}
					array_push($ar[$p_k], $arr);
				}else{
					array_push($ar, $arr);
				}
			}else{
				if(is_numeric($key)){
					continue;
				}
				$ar[$key] = $value;
			}
		}
		return $ar;
	}

	/**
	 * compare initial quote with new function
	 *
	 * @return void
	 * @author PM
	 **/
	function check_diff($arr1,$arr2){
		/*
			what is $igonre for?
			ex: when we update a quote we delete all the lines and add new lines with new ids
			that is why we need to ignore ids fields changes.
		*/
		$is_ok = true;
		$ignore = array('last_upd','id','group_id','preview','note_fields_id');
		foreach ($arr1 as $key => $value) {
			if(is_array($value)){
				if(count($value) != count($arr2[$key])){
					$is_ok = false;
					break;
				}
				if(!$this->check_diff($value,$arr2[$key])){
					$is_ok = false;
					break;
				}
			}else{
				if(in_array($key, $ignore)){
					continue;
				}
				if(trim($value) != trim($arr2[$key])){
					$is_ok = false;
					break;
				}
			}
		}
		return $is_ok;
	}

	# servicing and support
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function uploadify(&$in)
	{
		$response=array();
		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
			$targetPath = 'upload/'.DATABASE_NAME;
			$in['name'] = 'q_logo_img_'.DATABASE_NAME.'_'.time().'.jpg';
			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];
			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			@mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {

				move_uploaded_file($tempFile,$targetFile);
				global $database_config;

				$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => DATABASE_NAME,
				);
				$db_upload = new sqldb($database_2);
				$logo = $db_upload->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_QUOTE_LOGO' ");
				if($logo == ''){
					if(defined('ACCOUNT_QUOTE_LOGO')){
						$db_upload->query("UPDATE settings SET
		                           value = 'upload/".DATABASE_NAME."/".$in['name']."'
		                           WHERE constant_name='ACCOUNT_QUOTE_LOGO' ");
					}else{
						$db_upload->query("INSERT INTO settings SET
		                           value = 'upload/".DATABASE_NAME."/".$in['name']."',
		                           constant_name='ACCOUNT_QUOTE_LOGO' ");
					}
				}
				$response['success'] = 'success';
        echo json_encode($response);
				// ob_clean();
			} else {
				$response['error'] = gm('Invalid file type.');
        echo json_encode($response);
				// echo gm('Invalid file type.');
			}
		}
	}
	function atach(&$in)
	{

		$response=array();
		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			// $targetPath = $_SERVER['DOCUMENT_ROOT']. $in['last_part'] . $in['path'];
/*			$targetPath = $in['path'];
*/			$in['name'] = $_FILES['Filedata']['name'];
			$targetPath = INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/quotes';

			$targetFile = rtrim($targetPath,'/') . '/' . $in['name'];

			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif','pdf','docx'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			@mkdir(str_replace('//','/',$targetPath), 0775, true);
			if (in_array(strtolower($fileParts['extension']),$fileTypes)) {

				move_uploaded_file($tempFile,$targetFile);
				global $database_config;

				$database_2 = array(
					'hostname' => $database_config['mysql']['hostname'],
					'username' => $database_config['mysql']['username'],
					'password' => $database_config['mysql']['password'],
					'database' => DATABASE_NAME,
				);
				$db_upload = new sqldb($database_2);
				$file_id = $db_upload->insert("INSERT INTO attached_files SET `path` = 'upload/".DATABASE_NAME."/quotes/', name = '".$in['name']."', type='1' ");
				$response['success'] = 'success';
        echo json_encode($response);
				// ob_clean();
			} else {
				$response['error'] = gm('Invalid file type.');
        echo json_encode($response);
				// echo gm('Invalid file type.');
			}
		}
	}
			
				
	function delete_logo(&$in)
	{
	    if($in['name'] == ACCOUNT_LOGO_QUOTE){
	      msg::error ( gm('You cannot delete the default logo'),'error');
	      json_out($in);
	      return true;
	    }
	    if($in['name'] == '../img/no-logo.png'){
	      msg::error ( gm('You cannot delete the default logo'),'error');
	      json_out($in);
	      return true;
	    }
	    $exists = $this->db->field("SELECT COUNT(service_id) FROM servicing_support WHERE pdf_logo='".$in['name']."' ");
	    if($exists>=1){
	      msg::error ( gm('This logo is set for one ore more invoices.'),'error');
	      json_out($in);
	      return false;
	    }
	    @unlink($in['name']);
	    msg::success ( gm('File deleted'),'success');
	    return true;
	}

	function set_default_logo(&$in)
	{
	    $this->db->query("UPDATE settings SET value = '".$in['name']."' WHERE constant_name='ACCOUNT_LOGO_QUOTE' ");
	    msg::success (gm("Default logo set."),'success');
			json_out($in);
	    return true;
	}

	function settings(&$in){

		$this->db->query("UPDATE settings SET value='".(int)$in['use_page_numbering']."' WHERE constant_name='USE_QUOTE_PAGE_NUMBERING' ");
		$this->db->query("UPDATE settings SET value='".(int)$in['show_stamp']."' WHERE constant_name='SHOW_QUOTE_STAMP_SIGNATURE' ");

		msg::success ( gm('Changes saved'),'success');
		/*json_out($in);*/
		return true;
	}

	function default_pdf_format(&$in)
	{

		if(!$in['type']){
			msg::$error = gm('Invalid ID');
			return false;
		}

		if($in['identity_id']>0){
	      $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='quote' ");
	      $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='quote' ");

	    }else{

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
			$this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_QUOTE_PDF_FORMAT'");
			$this->db->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
			$this->db->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_QUOTE_PDF_MODULE'");
		}
			global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}
	function default_pdf_format_header(&$in)
	{

/*		if(!$in['type']){
			msg::$error = gm('Invalid ID');
			return false;
		}*/

		if($in['identity_id']>0){
	      $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='quote' ");
	      $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='quote' ");

	    }else{

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
			$this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_QUOTE_HEADER_PDF_FORMAT'");
			$this->db->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
			$this->db->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_QUOTE_PDF_MODULE'");
		}
			global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}
	function default_pdf_format_body(&$in)
	{

/*		if(!$in['type']){
			msg::$error = gm('Invalid ID');
			return false;
		}*/

		if($in['identity_id']>0){
	      $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='quote' ");
	      $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='quote' ");

	    }else{

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
			$this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_QUOTE_BODY_PDF_FORMAT'");
			$this->db->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
			$this->db->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_QUOTE_PDF_MODULE'");
		}
			global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}
	function default_pdf_format_footer(&$in)
	{

/*		if(!$in['type']){
			msg::$error = gm('Invalid ID');
			return false;
		}*/

		if($in['identity_id']>0){
	      $this->db->query("DELETE FROM identity_layout WHERE identity_id ='".$in['identity_id']."' AND module ='quote' ");
	      $this->db->query("INSERT INTO identity_layout SET identity_id='".$in['identity_id']."',pdf_layout ='".$in['type']."' , module='quote' ");

	    }else{

			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
			$this->db->query("UPDATE settings set value='".$in['type']."' where constant_name='ACCOUNT_QUOTE_FOOTER_PDF_FORMAT'");
			$this->db->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
			$this->db->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

			$this->db->query("UPDATE settings SET value='1' WHERE constant_name = 'ACCOUNT_QUOTE_PDF_MODULE'");
		}
			global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}

	function set_custom_pdf(&$in)
	{
		if(!$in['custom_type']){
			msg::$error = gm('Invalid ID');
			return false;
		}
		$this->db->query("DELETE FROM settings WHERE constant_name='USE_CUSTOME_QUOTE_PDF' ");
		$this->db->query("INSERT INTO settings SET value='1', constant_name='USE_CUSTOME_QUOTE_PDF' ");
		$this->db->query("UPDATE settings SET value='".$in['custom_type']."' where constant_name='ACCOUNT_QUOTE_PDF_FORMAT'");
		$this->db->query("UPDATE tblquote SET preview='0' WHERE pdf_layout='0' OR pdf_logo='0'");
		$this->db->query("UPDATE tblquote_version SET preview='0'  WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

		global $config;
			$this->db->query("DELETE FROM s3_links WHERE item LIKE '".$config['awsBucket'].DATABASE_NAME."/quote_cache/quote_%' ");

			msg::success ( gm('Changes saved'),'success');
			// json_out($in);

		return true;
	}

	function PdfAcitve(&$in){
		
		$this->db->query("UPDATE settings SET value='".(int)$in['pdf_active']."' WHERE constant_name='CUSTOMIZABLE_QUOTE_PDF' ");
		$this->db->query("UPDATE settings SET value='".(int)$in['pdf_note']."' WHERE constant_name='CUSTOMIZABLE_QUOTE_NOTE' ");
		$this->db->query("UPDATE settings SET value='".(int)$in['pdf_condition']."' WHERE constant_name='CUSTOMIZABLE_QUOTE_CONDITIONS' ");
		$this->db->query("UPDATE settings SET value='".(int)$in['pdf_stamp']."' WHERE constant_name='CUSTOMIZABLE_QUOTE_SIGNATURE' ");
		//$this->db->query("UPDATE settings SET value='".(int)$in['show_stamp']."' WHERE constant_name='SHOW_QUOTE_STAMP_SIGNATURE' ");

		msg::success ( gm('Changes saved'),'success');
		/*json_out($in);*/
		return true;
	}
	function PageActive(&$in){
		
		$this->db->query("UPDATE settings SET value='".(int)$in['page']."' WHERE constant_name='QUOTE_GENERAL_CONDITION_PAGE' ");
		msg::success ( gm('Changes saved'),'success');
		return true;
	}

	function reset_data(&$in){
		if($in['header']){
			$variable_data=$this->db->field("SELECT content FROM tblquote_pdf_data WHERE master='quote' AND type='".$in['header']."' AND initial='0' AND layout='".$in['layout']."'");
		}elseif($in['footer']){
			$variable_data=$this->db->field("SELECT content FROM tblquote_pdf_data WHERE master='quote' AND type='".$in['footer']."' AND initial='0' AND layout='".$in['layout']."'");
		}
		$result=array("var_data" => $variable_data);

		json_out($result);
		return true;
	}

	function pdfSaveData(&$in){

		if($in['header']=='header'){
			$this->db->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='quote' AND type='".$in['header']."' AND initial='1' ");
			$exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='quote' AND type='".$in['header']."' AND initial='1' AND layout='".$in['layout']."' ");
			if($exist){
				$this->db->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
			}else{
				$this->db->query("INSERT INTO tblquote_pdf_data SET master='quote',
													type='".$in['header']."',
													content='".$in['variable_data']."',
													initial='1', 
													layout='".$in['layout']."',
													`default`='1' ");
			}
		}else{
			$this->db->query("UPDATE tblquote_pdf_data SET `default`='0' WHERE master='quote' AND type='".$in['footer']."' AND initial='1' ");
			$exist=$this->db->field("SELECT id FROM tblquote_pdf_data WHERE master='quote' AND type='".$in['footer']."' AND initial='1' AND layout='".$in['layout']."' ");

			if($exist){
				$this->db->query("UPDATE tblquote_pdf_data SET content='".$in['variable_data']."', `default`='1' WHERE id='".$exist."' ");
			}else{
				$this->db->query("INSERT INTO tblquote_pdf_data SET master='quote',
													type='".$in['footer']."',
													content='".$in['variable_data']."',
													initial='1', 
													layout='".$in['layout']."',
													`default`='1' ");
			}
		}
		msg::success ( gm('Data saved'),'success');
		return true;
	}

	function Convention(&$in){
			if($in['account_number']){
				$this->db->query("UPDATE settings SET value='".$in['account_number']."' where constant_name='ACCOUNT_QUOTE_START'");
			}
			if($in['account_digits']){
				if(!defined('ACCOUNT_QUOTE_DIGIT_NR')){
					$this->db->query("INSERT INTO settings SET value='".$in['account_digits']."', constant_name='ACCOUNT_QUOTE_DIGIT_NR', module='billing' ");
					// msg::$success = gm("Setting added.");
				}else{
					$this->db->query("UPDATE settings SET value='".$in['account_digits']."' WHERE constant_name='ACCOUNT_QUOTE_DIGIT_NR'");
					// msg::$success = gm("Settings updated").'.';
				}
			}
			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ACCOUNT_QUOTE_REF' ");
			if((int)$in['reference']){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ACCOUNT_QUOTE_REF' ");
			}
			$this->db->query("UPDATE settings SET value='".$in['del']."' WHERE constant_name='ACCOUNT_QUOTE_DEL' ");
			

			msg::success ( gm('Data saved'),'success');
			return true;

	}
	function default_language(&$in){
		msg::success ( gm("Changes have been saved."),'success');
		return true;

	}
	function default_message(&$in)
	{
		$set = "text	= '".$in['text']."', ";
		/*if($in['use_html']){*/
			$set1 = "html_content	= '".$in['html_content']."' ";
		/*}*/
		$this->db->query("UPDATE sys_message SET
					subject = '".$in['subject']."',
					".$set."
					".$set1."
					WHERE name='".$in['name']."' AND lang_code='".$in['lang_code']."' ");
		/*$this->db->query("UPDATE sys_message SET use_html='".(int)$in['use_html']."' WHERE name='".$in['name']."' ");*/
		msg::success( gm('Message has been successfully updated'),'success');
		// json_out($in);
		return true;
	}

	function save_language(&$in){
		switch ($in['languages']){
			case '1':
				$lang = '';
				break;
			case '2':
				$lang = '_2';
				break;
			case '3':
				$lang = '_3';
				break;
			case '4':
				$lang = '_4';
				break;
			default:
				$lang = '';
				break;
		}

		# extra check for custom_langs
		if($in['languages']>=1000) {
			$lang = '_'.$in['languages'];
		}
		if($this->db->field("SELECT default_id FROM default_data WHERE type='quote_note".$lang."' AND default_main_id='0' ")){
			$this->db->query("UPDATE default_data SET value='".$in['notes']."' WHERE type='quote_note".$lang."' AND default_main_id='0' ");
		}else{
			$this->db->query("INSERT INTO default_data SET value='".$in['notes']."', type='quote_note".$lang."', default_main_id='0', default_name='quote_note' ");
		}
		if($in['terms']){
			$this->db->query("DELETE FROM default_data WHERE type='quote_terms".$lang."' AND default_main_id='0' ");
			$this->db->query("INSERT INTO default_data SET value='".$in['terms']."', type='quote_terms".$lang."',  default_name='quote_term'  ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function label_update(&$in){

	    $fields = '';
	    $table = 'label_language_quote';

	    $exist = $this->db->query("SELECT label_language_id FROM $table WHERE label_language_id='".$in['label_language_id']."' ");
	    if($exist->next()){
    		$this->db->query("UPDATE $table SET
    			quote						='".$in['quote_number']."',
    			quote_note 					='".$in['quote']."',
				billing_address 			='".$in['billing_address']."',
				buyer_delivery_address 		='".$in['buyer_delivery_address']."',
				buyer_delivery_zip 			='".$in['buyer_delivery_zip']."',
				buyer_delivery_city 		='".$in['buyer_delivery_city']."',
				buyer_delivery_country 		='".$in['buyer_delivery_country']."',
				buyer_main_address 			='".$in['buyer_main_address']."',
				buyer_main_zip 				='".$in['buyer_main_zip']."',
				buyer_main_city 			='".$in['buyer_main_city']."',
				buyer_main_country 			='".$in['buyer_main_country']."',
				bank_details 				='".$in['bank_details']."',
				item 						='".$in['item']."',
				subtotal 					='".$in['subtotal']."',
				unit_price 					='".$in['unit_price']."',
				vat 						='".$in['vat']."',
				amount_due 					='".$in['amount_due']."',
				notes 						='".$in['notes']."',
				bic_code 					='".$in['bic_code']."',
				phone 						='".$in['phone']."',
				email 						='".$in['email']."',
				our_ref 					='".$in['our_ref']."',
				vat_number 					='".$in['vat_number']."',
				package 					='".$in['package']."',
				article_code 				='".$in['article_code']."',
				chapter_total 				='".$in['chapter_total']."',
				subtotal_2 					='".$in['subtotal_2']."',
				reference 					='".$in['reference']."',
				date 						='".$in['date']."',
				customer 					='".$in['customer']."',
				quantity 					='".$in['quantity']."',
				amount 						='".$in['amount']."',
				discount 					='".$in['discount']."',
				payments 					='".$in['payments']."',
				grand_total 				='".$in['grand_total']."',
				bank_name 					='".$in['bank_name']."',
				iban 						='".$in['iban']."',
				fax 						='".$in['fax']."',
				url 						='".$in['url']."',
				your_ref 					='".$in['your_ref']."',
				sale_unit 					='".$in['sale_unit']."',
				general_conditions 			='".$in['general_conditions']."',
				page 						='".$in['page']."',
				valid_until 				='".$in['valid_until']."',
				stamp_signature 			='".$in['stamp_signature']."',
				download_webl 				='".$in['download_webl']."',
				name_webl 					='".$in['name_webl']."',
				addCommentLabel_webl 		='".$in['addCommentLabel_webl']."',
				quote_webl 					='".$in['quote_webl']."',
				accept_webl 				='".$in['accept_webl']."',
				reject_webl 				='".$in['reject_webl']."',
				bankDetails_webl 			='".$in['bankDetails_webl']."',
				cancel_webl 				='".$in['cancel_webl']."',
				bicCode_webl 				='".$in['bicCode_webl']."',
				bank_webl 					='".$in['bank_webl']."',
				pdfPrint_webl 				='".$in['pdfPrint_webl']."',
				email_webl 					='".$in['email_webl']."',
				submit_webl 				='".$in['submit_webl']."',
				date_webl 					='".$in['date_webl']."',
				requestNewVersion_webl 		='".$in['requestNewVersion_webl']."',
				status_webl 				='".$in['status_webl']."',
				payWithIcepay_webl 			='".$in['payWithIcepay_webl']."',
				regularInvoiceProForma_webl ='".$in['regularInvoiceProForma_webl']."',
				ibanCode_webl 				='".$in['ibanCode_webl']."',
				noData_webl 				='".$in['noData_webl']."'
                WHERE label_language_id='".$in['label_language_id']."'");
  		}else{
  			$this->db->query("UPDATE $table SET
  				quote						='".$in['quote_number']."',
    			quote_note 					='".$in['quote']."',
				billing_address 			='".$in['billing_address']."',
				bank_details 				='".$in['bank_details']."',
				item 						='".$in['item']."',
				subtotal 					='".$in['subtotal']."',
				unit_price 					='".$in['unit_price']."',
				vat 						='".$in['vat']."',
				amount_due 					='".$in['amount_due']."',
				notes 						='".$in['notes']."',
				bic_code 					='".$in['bic_code']."',
				phone 						='".$in['phone']."',
				email 						='".$in['email']."',
				our_ref 					='".$in['our_ref']."',
				vat_number 					='".$in['vat_number']."',
				package 					='".$in['package']."',
				article_code 				='".$in['article_code']."',
				chapter_total 				='".$in['chapter_total']."',
				subtotal_2 					='".$in['subtotal_2']."',
				reference 					='".$in['reference']."',
				date 						='".$in['date']."',
				customer 					='".$in['customer']."',
				quantity 					='".$in['quantity']."',
				amount 						='".$in['amount']."',
				discount 					='".$in['discount']."',
				payments 					='".$in['payments']."',
				grand_total 				='".$in['grand_total']."',
				bank_name 					='".$in['bank_name']."',
				iban 						='".$in['iban']."',
				fax 						='".$in['fax']."',
				url 						='".$in['url']."',
				your_ref 					='".$in['your_ref']."',
				sale_unit 					='".$in['sale_unit']."',
				general_conditions 			='".$in['general_conditions']."',
				page 						='".$in['page']."',
				valid_until 				='".$in['valid_until']."',
				stamp_signature 			='".$in['stamp_signature']."',
				download_webl 				='".$in['download_webl']."',
				name_webl 					='".$in['name_webl']."',
				addCommentLabel_webl 		='".$in['addCommentLabel_webl']."',
				quote_webl 					='".$in['quote_webl']."',
				accept_webl 				='".$in['accept_webl']."',
				reject_webl 				='".$in['reject_webl']."',
				bankDetails_webl 			='".$in['bankDetails_webl']."',
				cancel_webl 				='".$in['cancel_webl']."',
				bicCode_webl 				='".$in['bicCode_webl']."',
				bank_webl 					='".$in['bank_webl']."',
				pdfPrint_webl 				='".$in['pdfPrint_webl']."',
				email_webl 					='".$in['email_webl']."',
				submit_webl 				='".$in['submit_webl']."',
				date_webl 					='".$in['date_webl']."',
				requestNewVersion_webl 		='".$in['requestNewVersion_webl']."',
				status_webl 				='".$in['status_webl']."',
				payWithIcepay_webl 			='".$in['payWithIcepay_webl']."',
				regularInvoiceProForma_webl ='".$in['regularInvoiceProForma_webl']."',
				ibanCode_webl 				='".$in['ibanCode_webl']."',
				noData_webl 				='".$in['noData_webl']."'
                WHERE label_language_id='".$in['label_language_id']."'");
 		}
    	msg::success ( gm('Account has been successfully updated'),'success');
    	// $in['failure']=false;

    	return true;
  	}

  	function update_default_email(&$in)
	{
			if(!$this->validate_default_email($in)){
				return false;
			}

			$mail_type = $in['mail_type_1'];
			$this->db->query("UPDATE default_data SET default_name='".$in['default_name']."', value='".$in['email_value']."' WHERE type='quote_email' ");
			$this->db->query("UPDATE default_data SET value='".$mail_type."' WHERE type = 'quote_email_type' ");
	    	
	    	msg::success ( gm('Default email updated'),'success');
	    	// $in['failure']=false;
	    
	    	return true;
	}
	function validate_default_email(&$in)
	{
		$v = new validation($in);
		$v->field('mail_type_1', 'Email', 'required');
		if($in['mail_type_1'] == 2){
			$v->field('default_name', 'Email', 'required');
			$v->field('email_value', 'Email', 'email:required');
		}
		return $v->run();
	}
	function web_link(&$in)
	{

		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='USE_QUOTE_WEB_LINK' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_QUOTE_ACCEPT' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_QUOTE_COMMENTS' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='WEB_INCLUDE_PDF_Q' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_QUOTE_REJECT' ");
		$this->db->query("UPDATE settings SET value='0' WHERE constant_name='ALLOW_QUOTE_NEWV' ");
		if($in['quote_link']=='1'){
			$this->db->query("UPDATE settings SET value='1' WHERE constant_name='USE_QUOTE_WEB_LINK' ");
			if($in['quote_accept']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_QUOTE_ACCEPT' ");
			}
			if($in['quote_comment']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_QUOTE_COMMENTS' ");
			}
			if($in['quote_pdf']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='WEB_INCLUDE_PDF_Q' ");
			}
			if($in['quote_reject']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_QUOTE_REJECT' ");
			}
			if($in['quote_new']==1){
				$this->db->query("UPDATE settings SET value='1' WHERE constant_name='ALLOW_QUOTE_NEWV' ");
			}
		}else{
			return false;
		}


		msg::success ( gm("Changes have been saved."),'success');
    	// $in['failure']=false;
    	return true;
    }

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function sendNewEmail(&$in)
	{

		if(!$this->sendNewEmailValidate($in)){
			msg::error( 'Invalid email address','error');
			json_out($in);
			return false;
		}

		global $config;
		$in['weblink_url'] = $this->external_id($in);
		$this->db->query("SELECT tblquote.acc_manager_id,tblquote.id,tblquote.serial_number,tblquote.pdf_layout,tblquote.pdf_logo,tblquote.discount,tblquote.quote_date,tblquote.buyer_name,tblquote.currency_type, SUM(tblquote_line.amount) AS total, tblquote.use_custom_template, customers.quote_reference, customers.customer_id,tblquote.email_language,tblquote.author_id, tblquote.created_by,tblquote.buyer_id,tblquote.contact_id
                           FROM tblquote
                           LEFT JOIN tblquote_line ON tblquote_line.quote_id=tblquote.id
                           LEFT JOIN customers ON tblquote.buyer_id=customers.customer_id
                           WHERE tblquote.id='".$in['id']."'");

		$this->db->move_next();
		$cust_id = $this->db->f('customer_id');
		$acc_manager_id=$this->db->f('acc_manager_id');
		$e_lang = $this->db->f('email_language');
		$created_by = $this->db->f('created_by');
		$author = $this->db->f('author_id');
		$buyer_id = $this->db->f('buyer_id');
		$contact_id = $this->db->f('contact_id');

		$pdf_layout = $this->db->f('pdf_layout');
		if($this->db->f('use_custom_template')==1){
			$in['custom_type'] = $pdf_layout;
			$in['logo']=$this->db->f('pdf_logo');
		}elseif(defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $pdf_layout == 0){
			$in['custom_type'] = ACCOUNT_QUOTE_PDF_FORMAT;
		}else{
			if($pdf_layout){
				$in['type']=$pdf_layout;
				$in['logo']=$this->db->f('pdf_logo');
			}else{
				$in['type'] = ACCOUNT_QUOTE_PDF_FORMAT;
			}
		}

		$this->generate_pdf($in);

		ark::loadLibraries(array('class.phpmailer','class.smtp'));
		// $date = explode('-',$this->dbu->f('quote_date'));
		// $factur_date = $date[2].'/'.$date[1].'/'.$date[0];
		$total=$this->db->f('total');
		$currency=get_commission_type_list($this->db->f('currency_type'));

		$discount_value = $total*$this->db->f('discount')/100;
		$total -= $discount_value;

		$amount_due = round($total,2);

		$mail = new PHPMailer();
		$mail->WordWrap = 50;
		if(!$e_lang || $e_lang > 4){
	    	$e_lang=1;
	    }
		$text_array = array('1' => array('simple' => array('1' => 'Your offer', '2'=> 'Check your offer by clicking on the above link','3'=>"Following this link",'4'=>'Web Link'),
        								   'pay' => array('1' => 'INVOICE WEB LINK', '2'=> 'Check your offer by clicking on the above link.','3'=>"HERE")
        								   ),
        					'2' => array('simple' => array('1' => 'Votre Offre', '2'=> 'Visualisez votre offre en cliquant sur le lien ci-dessus','3'=>'Cliquez sur ce lien','4'=>'Lien web'),
        								   'pay' => array('1' => 'LIEN WEB FACTURE', '2'=> 'Vous pouvez tÃ©lÃ©charger votre facture ICI et la payer en ligne','3'=>'ICI')
        								   ),
        					'3' => array('simple' => array('1' => 'Offerte', '2'=> 'Bekijk uw aanbod door op de link hierboven te klikken','3'=>'Via deze link','4'=>'Weblink'),
        								   'pay' => array('1' => 'WEB LINK FACTUUR', '2'=> 'Bekijk uw aanbod door op de link hierboven te klikken','3'=>'HIER')
        								   ),
        					'4' => array('simple' => array('1' => 'ANGEBOT WIE WEB-LINK', '2'=> 'Sie kÃ¶nnen Ihr Angebot HIER downloaden','3'=>'HIER','4'=>'WEB-LINK'),
        								   'pay' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie kÃ¶nnen Ihre Rechnung HIER downloaden und online bezahlen','3'=>'HIER')
        								   )
        );
		$in['e_message'] = stripslashes($in['e_message']);
		$body= htmlentities($in['e_message'],ENT_COMPAT | ENT_HTML401,'UTF-8');

		$def_email = $this->default_email();

    	/* 1 - use user logged email
    	   2 - use default email
    	   3 - use author email
    	*/
    	$mail_sender_type = $this->db->field("SELECT value FROM default_data WHERE type='quote_email_type' ");
    	if($mail_sender_type == 1 || $mail_sender_type == 3) {
    		if($mail_sender_type == 1) {
    			$this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
    			$mail->SetFrom($def_email['from']['email'], utf8_decode( htmlspecialchars($this->db_users->f('first_name')).' '.htmlspecialchars($this->db_users->f('last_name'))),0);
    			$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( htmlspecialchars($this->db_users->f('first_name')).' '.htmlspecialchars($this->db_users->f('last_name')) ) );
    		} else {
    			$this->db_users->query("SELECT first_name, last_name, email FROM users WHERE user_id = '".$author."' ");
    			$mail->SetFrom($def_email['from']['email'], utf8_decode( htmlspecialchars($this->db_users->f('first_name')).' '.htmlspecialchars($this->db_users->f('last_name'))),0);
    			$mail->AddReplyTo($this->db_users->f('email'), utf8_decode( htmlspecialchars($this->db_users->f('first_name')).' '.htmlspecialchars($this->db_users->f('last_name'))));
    		}
    	}else{
    		$mail->SetFrom($def_email['from']['email'], $def_email['from']['name'],0);
    		$mail->AddReplyTo($def_email['reply']['email'], $def_email['reply']['name']);
    	}

		$subject=utf8_decode( htmlspecialchars($in['e_subject']) );

		$mail->Subject = $subject;

		if($in['include_pdf']){
			$tmp_file_name = 'quote_.pdf';
			if($in['attach_file_name']){
				$tmp_file_name = $in['attach_file_name'];
			}
			$mail->AddAttachment($tmp_file_name, $this->db->f('quote_reference').$in['serial_number'].'.pdf'); // attach files/quote-user-1234.pdf, and rename it to quote.pdf
		}
		if($in['files']){
	        foreach ($in['files'] as $key => $value) {
	        	if($value['checked'] == 1){
		          $mime = mime_content_type($value['path'].$value['file']);
		          $mail->AddAttachment($value['path'].$value['file'], $value['file']); // attach files/quote-user-1234.pdf, and rename it to quote.pdf
		        }
	        }
	    }

		$mail->WordWrap = 50;

		$body=str_replace('[!DROP_BOX!]', '', $body);
			 // console::log($body);
		    if($in['file_d_id']){
		    	$body.="\n\n";
			    foreach ($in['file_d_id'] as $key => $value) {
					$body.="<p><a href=\"".$in['file_d_path'][$key]."\">".$in['file_d_name'][$key]."</a></p>\n";
				}
			}
		$tblinvoice = $this->db->query("SELECT * FROM tblquote WHERE id='".$in['quote_id']."'  ");	
		$contact = $this->db->query("SELECT firstname,lastname FROM customer_contacts WHERE contact_id='".$tblinvoice->f('contact_id')."'  ");
		$title_cont = $this->db->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'  ");
		$customer = $this->db->field("SELECT name FROM customers WHERE customer_id='".$tblinvoice->f('buyer_id')."'  ");


	    //$mail->MsgHTML(nl2br($body));
		if($in['use_html']){
			
			$head = '<style>p {	margin: 0px; padding: 0px; }</style>';

	      	$mail->IsHTML(true);
	      	$body_style='<style>body{background-color: #f2f2f2;}</style><div style="margin: 35px auto; max-width: 600px; border-collapse: collapse; border-spacing: 0; font: inherit; vertical-align: baseline; background-color: #fff; padding: 30px;">'.utf8_decode($in['e_message']).'</div>';

	     	$body = stripslashes($body_style);
			$body=str_replace('[!CONTACT_FIRST_NAME!]', "".$contact->f('firstname')."", $body);
			$body=str_replace('[!CONTACT_LAST_NAME!]', "".$contact->f('lastname')."", $body);
			$body=str_replace('[!SALUTATION!]', "".$title_cont."", $body);	
			$body=str_replace('[!YOUR_REFERENCE!]',"".$tblinvoice->f('your_ref')."",$body);
	      	if(defined('USE_QUOTE_WEB_LINK') && USE_QUOTE_WEB_LINK == 1){
				$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['id']."' AND `version_id`='".$in['version_id']."' AND `type`='q'  ");

		     	// $extra = "<p style=\"background: #dedede; width: 500px; text-align:center; padding-bottom: 15px;\"><br />";
		     	// $extra .="<b>".$text_array[$e_lang]['simple']['1']."</b><br />";
		    	// $extra .= str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2'])."<br /></p>";

		     	$extra = "<p style=\"background: #e6e6e6; max-width: 90%; text-align:center; margin:0 auto; padding-bottom: 15px; border: 2px solid #e6e6e6; color:#868d91; border-radius:8px;\">";
	        	
	    		$extra .="<b style=\"color: #5199b7; text-align:center; font-size:32px;\"><a style=\"text-decoration:none; text-transform: lowercase; color:#6399c6;\" href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['1']."</a></b><br />";
	        	
	    		$extra .=$text_array[$e_lang]['simple']['2']."<br /></p>";

		     	// $body .=$extra;
	    		if($in['copy']){
	    			$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_QUOTE."\">",$body);
		     	}elseif($in['copy_acc']){
					$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url']."images/akti-logo.png\">",$body);
		     	}else{
		     		$body=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_QUOTE."\">",$body);
		     	}
		     	
		     	$body=str_replace('[!WEB_LINK!]', $extra, $body);
		     	$body=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $body);

		    }else{
			    $body=str_replace('[!WEB_LINK!]', '', $body);
			    $body=str_replace('[!WEB_LINK_2!]', '', $body);
			}
			/*sif(defined('DROPBOX') && DROPBOX != ''){
			    if(strpos($body, '[!DROP_BOX!]')){
			        $body=str_replace('[!DROP_BOX!]', '', $body);
			        $id = $buyer_id;
			        $is_contact = 0;
			        if(!$id){
			            $id = $contact_id;
			            $is_contact = 1;
			        }
			        $d = new drop('quotes', $id, $in['id'],true,'',$is_contact, $in['serial_number']);
			        $files = $d->getContent();
			        if(!empty($files['contents'])){
			            $body .="<br><br>";
			            foreach ($files['contents']  as $key => $value) {
			                $l = $d->getLink(urldecode($value['path']));
			                $link = $l;
			                $file = $in['serial_number'].'/'.str_replace($files['path'].'/', '', $value['path']);
			                $body.="<p><a href=".$link.">".$file."</a></p>";
			            }
			        }
			    }
			}*/
			$body=str_replace('[!DROP_BOX!]', '', $body);
// console::log($body);
			if($in['file_d_id']){
				$body .="<br><br>";
				foreach ($in['file_d_id'] as $key => $value) {
					$body.="<p><a href=".$in['file_d_path'][$key].">".$in['file_d_name'][$key]."</a></p>";
				}
			}
		    $mail->Body    = $head.$body;
	    }else{

	    	if(defined('USE_QUOTE_WEB_LINK') && USE_QUOTE_WEB_LINK == 1){console::log('use weblink');
				$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['id']."' AND `version_id`='".$in['version_id']."' AND `type`='q'  ");

		      	$extra = "\n\n-----------------------------------";
		      	$extra .="\n".$text_array[$e_lang]['simple']['1'];
		      	$extra .="\n-----------------------------------";
		      	$extra .="\n". str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2']);
						// $body .=$extra;
		      	$body=str_replace('[!WEB_LINK!]', $extra, $body);
		      	$body=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $body);
		    }else{
		      	$body=str_replace('[!WEB_LINK!]', '', $body);
		      	$body=str_replace('[!WEB_LINK_2!]', '', $body);
		    }
		    if(defined('DROPBOX') && DROPBOX != ''){
			    if(strpos($body, '[!DROP_BOX!]')){

			        $id = $buyer_id;
			        $is_contact = 0;
			        if(!$id){
			            $id = $contact_id;
			            $is_contact = 1;
			        }
			        $d = new drop('quotes', $id, $in['id'],true,'',$is_contact, $in['serial_number']);
			        $files = $d->getContent();
			        if(!empty($files['contents'])){
			            $body.="\n\n";
			            foreach ($files['contents']  as $key => $value) {
			                $l = $d->getLink(urldecode($value['path']));
			                $link = $l;
			                $file = $in['serial_number'].'/'.str_replace($files['path'].'/', '', $value['path']);
			                // $body.="<p><a href=".$link.">".$file."</a></p>";
			                $body.="<p><a href=\"".$link."\">".$file."</a></p>\n";
			            }
			        }
			    }
			}

			// $body=str_replace('[!DROP_BOX!]', '', $body);
			// console::log($body);
		    /*if($in['file_d_id']){
		    	$body.="\n\n";
			    foreach ($in['file_d_id'] as $key => $value) {
					$body.="<p><a href=\"".$in['file_d_path'][$key]."\">".$in['file_d_name'][$key]."</a></p>\n";
				}
			}*/
	      	$mail->MsgHTML(nl2br($body));
	    }
    // console::log($body);

		if($in['copy']){
			$u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."' ");
			if($u->f('email')){
				$mail->AddCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
			}
		}
		if($in['copy_acc']){
			$u = $this->db_users->query("SELECT email, first_name, last_name FROM users WHERE user_id='".$acc_manager_id."' ");
			if($u->f('email')){
				$mail->AddBCC($u->f('email'),$u->f('last_name').' '.$u->f('first_name'));
			}
		}


		/*$this->db->query("SELECT customer_contacts.*,tblquote.id,tblquote.buyer_id,tblquote.serial_number
                           FROM tblquote
                           INNER JOIN customer_contacts ON customer_contacts.customer_id=tblquote.buyer_id
                           WHERE tblquote.id ='".$in['id']."'");
		while ($this->db->move_next()) {
			if($in['recipients'][$this->db->f('contact_id')]){
				$mail->AddAddress($this->db->f('email'),$this->db->f('firstname').' '.$this->db->f('lastname'));
			}
		};*/

		/*if($in['send_to']){
			$mail->AddAddress($in['send_to']);
		}*/

		$mail->AddAddress(trim($in['email']));
		if($in['mark_as_sent'] == 1){
			$sent_date= time();
			$this->db->query("UPDATE tblquote_version SET sent='1',sent_date='".$sent_date."' WHERE quote_id='".$in['id']."' AND version_id='".$in['version_id']."' ");
			$this->db->query("UPDATE tblquote SET sent='1', sent_date='".$sent_date."' WHERE id='".$in['id']."' ");
			$this->db->query("UPDATE tblquote SET pending = '1' WHERE id = '".$in['id']."' ");
			$in['external_url']				= $exist_url;
			$in['web_url']					= $config['web_link_url'].'?q=';
		}

		$mail->Send();
		console::log($mail);
		if($in['attach_file_name']){
			unlink($in['attach_file_name']);
		}

		if($mail->IsError()){
			msg::error ( $mail->ErrorInfo,'error');
        	json_out($in);
			return false;
		}

		if($mail->ErrorInfo){
			msg::notice( $mail->ErrorInfo,'notice');
		}

		// msg::$success= gm("Email sent");
		if(isset($in['logging_id']) && is_numeric($in['logging_id'])){
			update_log_message($in['logging_id'],' '.$in['email']);
		}else{
			$in['logging_id'] =insert_message_log($this->pag,'{l}Quote has been sent by{endl} '.get_user_name($_SESSION['u_id']).' {l}to{endl}: '.$in['email'],$this->field_n,$in['id'],false,$_SESSION['u_id'],true,$cust_id,$sent_date);
		}

		msg::success( gm('Email sent').'.','success');

      	json_out($in);
		return true;

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function sendNewEmailValidate(&$in)
	{
		$v = new validation($in);
		$in['email'] = trim($in['email']);
		$v->field('email', 'Email', 'email');
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function external_id(&$in)
	{
		if(defined('USE_QUOTE_WEB_LINK') && USE_QUOTE_WEB_LINK == 1){
			$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['id']."' AND `type`='q' AND version_id='".$in['version_id']."' ");
			if(!$exist_url){
				$url = generate_chars();
				while (!isUnique($url)) {
					$url = generate_chars();
				}
				$this->db_users->query("INSERT INTO urls SET `url_code`='".$url."', `database`='".DATABASE_NAME."', `item_id`='".$in['id']."', `type`='q', version_id='".$in['version_id']."' ");
				$exist_url = $url;
			}
			return $exist_url;
		}
	}

	/**
	 * return the default email address
	 *
	 * @return array
	 * @author PM
	 **/
	function generate_pdf(&$in)
	{
		include_once(__DIR__.'/../controller/print.php');
	}

	/**
	 * return the default email address
	 *
	 * @return array
	 * @author PM
	 **/
	function default_email()
	{
		$array = array();
		$array['from']['name'] = ACCOUNT_COMPANY;
		$array['from']['email'] = 'noreply@akti.com';
		$array['reply']['name'] = ACCOUNT_COMPANY;
		$array['reply']['email'] = ACCOUNT_EMAIL;
		$this->db->query("SELECT * FROM default_data WHERE type='quote_email' ");
		if($this->db->move_next()){
			$array['reply']['name'] = $this->db->f('default_name');
			$array['reply']['email'] = $this->db->f('value');
			$array['from']['name'] = $this->db->f('default_name');
		}
		return $array;
	}

	function mark_sent(&$in){
		if(!$this->mark_sent_validate($in))
		{
			return false;
		}
		$in['s_date'] = strtotime($in['sent_date']);
		$this->db->query("UPDATE tblquote_version SET sent='1',sent_date='".$in['s_date']."' WHERE version_id='".$in['version_id']."' AND quote_id='".$in['quote_id']."' ");
		$this->db->query("UPDATE tblquote SET sent='1', sent_date='".$in['s_date']."' WHERE id='".$in['quote_id']."' ");
		$this->db->query("UPDATE tblquote SET pending= '1' WHERE id = '".$in['quote_id']."' ");

		msg::success(gm("Quote has been successfully marked as sent"),'success');
		insert_message_log($this->pag,'{l}Quote has been manually marked as sent by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['quote_id']);
		$in['id'] = $in['quote_id'];
		$this->external_id($in);
		return true;
	}

	/****************************************************************
	* function mark_sent_validate(&$in)                                *
	****************************************************************/
	function mark_sent_validate(&$in)
	{
		$is_ok = true;
		$v = new validation($in);
		$v->field('quote_id', 'ID', 'required:exist[tblquote.id]', "Invalid Id");
		$v->field('sent_date', 'ID', 'required');
		$is_ok = $v->run();
		return $is_ok;
	}

	  /**
	 * Send order by email
	 *
	 * @return bool
	 * @author PM
	 **/
	function pdf_settings(&$in){
   		if(!($in['quote_id']&&$in['pdf_layout']&&$in['logo']) )
		{
			msg::error(gm('Invalid ID'),'error');
			json_out($in);
			return false;
		}
    	
		if($in['logo'] == '../img/no-logo.png'){
			$in['logo'] = 'img/no-logo.png';
		}
    
	    $this->db->query("UPDATE tblquote SET pdf_layout='".$in['pdf_layout']."',pdf_logo='".$in['logo']."',email_language='".$in['email_language']."', identity_id='".$in['identity_id']."' WHERE id = '".$in['quote_id']."' ");

	    $this->db->query("UPDATE tblquote_version SET preview='0' WHERE quote_id = '".$in['quote_id']."' AND version_id = '".$in['version_id']."' ");

	  	msg::success ( gm("Pdf settings have been updated"),'success');
	  	return true;
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author PM
	 **/
	function revert(&$in)
	{
		if(!$this->delete_validate($in)){
			json_out($in);
			return false;
		}

		$this->db->query("UPDATE tblquote SET sent='0' WHERE id='".$in['quote_id']."' ");
		$this->db->query("UPDATE tblquote SET pending = '0' WHERE id= '".$in['quote_id']."' ");
		$this->db->query("UPDATE tblquote_version SET sent='0' WHERE quote_id='".$in['quote_id']."' AND version_id='".$in['version_id']."' ");
		// $in['quote_id'] = $in['id'];
		msg::success(gm("Quote  has been return to draft"),'success');
		insert_message_log($this->pag,'{l}Quote  has been return to draft by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['quote_id']);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author PM
	 **/
	function delete_validate(&$in)
	{
		$v = new validation($in);
		$v->field('quote_id', 'ID', 'required:exist[tblquote.id]', "Invalid Id");

		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author PM
	 **/
	function status(&$in){
		if($in['id']){ $in['quote_id'] = $in['id']; }
		$q = $this->db->query("select id from tblquote where id=".$in['quote_id']);
		$cust_id = $this->db->field("SELECT buyer_id FROM tblquote WHERE id='".$in['quote_id']."'");
		if(!$q->next()){
			$in['error'] = gm("Invalid ID");
			msg::error(gm("Invalid ID"),'error');
			json_out($in);
			return false;
		}
		$time = time();

		$this->db->query("UPDATE tblquote SET status_customer='".$in['status_customer']."', status_c_date='".$time."' WHERE id=".$in['quote_id']);
		/*$this->db->query("UPDATE tblquote SET lost_id = 0 WHERE id = '".$in['quote_id']."' ");*/
		// if($in['status_customer'] > 1 ){
			$this->db->query("UPDATE tblquote_version SET active = 0 WHERE quote_id='".$in['quote_id']."'");
			$this->db->query("UPDATE tblquote_version SET active = 1 WHERE quote_id='".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

			// msg::$success = gm("Quote Version was selected");
			// insert_message_log($this->pag,'{l}Quote Version was selected{endl} '.get_user_name($_SESSION['u_id']),'quote_id',$in['quote_id'],false,$_SESSION['u_id'],true,$cust_id);
		// }else
		if ($in['status_customer'] == 1) {
				$exist=$this->db->field("SELECT id FROM tblquote_lost_reason WHERE id='".$in['lost_id']."'");
				if($exist){
					$this->db->query("UPDATE tblquote SET lost_id='".$exist."' WHERE id=".$in['quote_id']);
				}
		}

		$in['date'] = date(ACCOUNT_DATE_FORMAT,$time);
		$opts = array('Pending','Rejected', 'Accepted', 'Accepted', 'Accepted', 'Draft');
		$the_time = time();
		$this->db->query("INSERT INTO tblquote_history SET quote_id='".$in['quote_id']."', date='".$the_time."', message=(SELECT version_code FROM tblquote_version WHERE version_id='".$in['version_id']."' ) ");
		insert_message_log($this->pag,'{l}Approval status changed to{endl} :'.$opts[$in['status_customer']].' {l}by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['quote_id'],false,$_SESSION['u_id'],true,$cust_id,$the_time);
		msg::success ( gm("Quote has been updated"),'success');
		return true;
	}

	function DeleteField(&$in){

		$this->db->query("DELETE FROM tblquote_".$in['table']." WHERE id='".$in['field_id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	/**
	 * add new custom field
	 *
	 * @return bool
	 * @author Mp
	 **/
	function add_field(&$in)
	{
		$in['field_id'] = $this->db->insert("INSERT INTO tblquote_".$in['table']." SET name='".$in['name']."' ");
		msg::success(gm('Field added.'),'success');
		return true;
	}

	 function UpdateField(&$in){
	 	$this->db->insert("UPDATE tblquote_".$in['table']." SET name='".$in['value']."' WHERE id='".$in['id']."' ");
		msg::success(gm('Field updated.'),'success');
		return true;
	 }
	function saveArticleField(&$in){
		$v = new validation($in);
		$v->field('text', 'Field', 'required', gm("Invalid Field Label"));

		if(!$v->run()){
			return false;
		}
	 	$this->db->insert("UPDATE settings SET long_value='".$in['text']."' WHERE constant_name='QUOTE_FIELD_LABEL' ");
	 	msg::success(gm('Field updated.'),'success');
		return true;
	}
	function savepacking(&$in){
		if(!ALLOW_ARTICLE_SALE_UNIT  && $in['sale']){
			msg::error ( gm("There are no support for article Sale Unit.Please check first that option on Article Settings."),'success');
			return false;
		}

		if(!$in['pack']){
			$in['pack']=0;
			$in['sale']=0;
		}

		if(!$in['sale']){
			$in['sale']=0;
		}
	 	$this->db->insert("UPDATE settings SET value='".$in['pack']."' WHERE constant_name='ALLOW_QUOTE_PACKING' ");
	 	$this->db->insert("UPDATE settings SET value='".$in['sale']."' WHERE constant_name='ALLOW_QUOTE_SALE_UNIT' ");
	 	msg::success(gm('Field updated.'),'success');
		return true;
	}
	function deleteatach(&$in){

		$this->db->query("DELETE FROM attached_files WHERE file_id='".$in['id']."' AND name='".$in['name']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function defaultcheck(&$in){

		$this->db->query("UPDATE `attached_files` SET `default`='".$in['default_id']."' WHERE `file_id`='".$in['id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function setact(&$in){

		$this->db->insert("UPDATE settings SET value='".$in['id']."' WHERE constant_name='SHORT_QUOTES_BY_SERIAL' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	/**
	 * Archive quote
	 *
	 * @param array $in
	 */
	function archive(&$in){
		if(!$this->archive_validate($in)){
			return false;
		}

		$this->db->query("UPDATE tblquote SET f_archived='1' WHERE id='".$in['id']."' ");
		msg::success ( gm('Quote archived'),'success');
		insert_message_log($this->pag,'{l}Quote archived by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['id']);
		return true;

	}

	/**
	 * Archive validation
	 * We check to see if we have the quote with this id
	 *
	 * @param array $in
	 * @return true or false
	 */
	function archive_validate(&$in){
		$v = new validation($in);
		$v->f('id', 'Id', 'required:exist[tblquote.id]',gm('Invalid ID'));
		return $v->run();
	}

	/**
	 * Delete a quote
	 *
	 * @param array $in
	 * @return true
	 */
	function delete(&$in){
		if(!$this->archive_validate($in)){
			return false;
		}

		/*$img = glob(__DIR__.'/../../../../upload/'.DATABASE_NAME.'/quote_cache/quote_'.$in['id'].'_*.png');
		foreach ($img as $filename) {
			@unlink($filename);
		}*/
		$this->db->query("DELETE FROM tblquote_group WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblquote_line WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblquote_history WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblquote_version WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM logging WHERE field_name='quote_id' AND field_value='".$in['id']."' ");
		$this->db->query("DELETE FROM tblquote WHERE id = '".$in['id']."'");
		$this->db->query("DELETE FROM note_fields WHERE item_type = 'quote' AND item_name = 'notes' AND item_id = '".$in['id']."'");
		$this->db->query("DELETE FROM note_fields WHERE item_type = 'quote' AND item_name = 'free_text_content' AND item_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblopportunity_quotes WHERE quote_id = '".$in['id']."'");

	  	msg::success (gm("Quote deleted."),'success');

	  	return true;
	}

	/**
	 * Activate a quote
	 *
	 * @param array $in
	 * @return true
	 */
	function activate(&$in){
		if(!$this->archive_validate($in)){
			return false;
		}

		$this->db->query("UPDATE tblquote SET f_archived='0' WHERE id='".$in['id']."' ");

	  	msg::success ( gm("Quote activated"),'success');
	  	insert_message_log($this->pag,'{l}Quote activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['id']);
	  	return true;
	}

	function currencyRate($currency=''){
		if(empty($currency)){
			$currency = "USD";
		}
		$separator = ACCOUNT_NUMBER_FORMAT;
		$into = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
		return currency::getCurrency($currency, $into, 1,$separator);
	}

	/**
	 * Delete a quote template
	 *
	 * @return bool
	 * @author Mp
	 **/
	function delete_template(&$in)
	{
		if(!$this->archive_validate($in)){
			return false;
		}

		$this->db->query("DELETE FROM tblquote_group WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblquote_line WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblquote_history WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM tblquote_version WHERE quote_id = '".$in['id']."'");
		$this->db->query("DELETE FROM logging WHERE field_name='quote_id' AND field_value='".$in['id']."' ");
		$this->db->query("DELETE FROM tblquote WHERE id = '".$in['id']."'");

	  	msg::success ( gm("Template deleted."),'success');

	  	return true;
	}
	  function saveAddToPdf(&$in)
	  {

	    if($in['all']){
	      if($in['value'] == 1){
	        foreach ($in['item'] as $key => $value) {
	          $_SESSION['add_to_quote'][$value]= $in['value'];
	        }
	      }else{
	        foreach ($in['item'] as $key => $value) {
	          unset($_SESSION['add_to_quote'][$value]);
	        }
	      }
	    }else{
	      if($in['value'] == 1){
	        $_SESSION['add_to_quote'][$in['item']]= $in['value'];
	      }else{
	        unset($_SESSION['add_to_quote'][$in['item']]);
	      }
	    }
	      json_out($_SESSION['add_to_quote']);
	    return true;
	  }
}

?>