<?php

class quote_version{

	private $dbu;
	var $pag = 'quote'; 						# used for log messages
	var $field_n = 'quote_id';					# used for log messages

	public function __construct(){
		$this->dbu = new sqldb();
		$this->db = new sqldb();
	}

	public function add(&$in){
		//we need to get the quote and duplicate the lines
		$now = $in['quote_date'] ? $in['quote_date'] : time();
		$this->dbu->query("UPDATE tblquote_version SET active = 0 WHERE quote_id='".$in['quote_id']."'");
		$in['version_id'] = $this->dbu->insert("INSERT INTO tblquote_version SET
												                                    quote_id         =	'".$in['quote_id']."',
												                                    version_code     =	'".$this->next_version_code($in['quote_id'])."',
												                                    active           =	'1',
												                                    discount      	    	=   '".$in['discount']."',
                                      												discount_line_gen      	=   '".$in['discount_line_gen']."',
                                      												apply_discount            =   '".$in['apply_discount']."',
												                                    version_date	 =	'".$now."' ");
		return true;

	}

	public function next(&$in){
		if(!$this->next_validate($in)){
			return false;
		}

		//we add a new quote version
		if(!$this->add($in)){
			return false;
		}
		//get all the lines and move them over
		$groups = $this->dbu->query("SELECT * FROM tblquote_group WHERE version_id = '".$in['current_version_id']."' AND quote_id = '".$in['quote_id']."'");
		while ($groups->next()) {
			$fields = $groups->next_array();
			//remove old id
			unset($fields['group_id']);

			$fields['version_id'] = $in['version_id'];

			$sql = $this->gen_sql($fields);
			$group_id = $this->dbu->insert("INSERT INTO tblquote_group SET ".$sql);

			//update lines of this group;
			$lines = $this->dbu->query("SELECT * FROM tblquote_line WHERE group_id = '".$groups->f('group_id')."' AND quote_id = '".$in['quote_id']."'");
			while ($lines->next()) {
				$fields = $lines->next_array();
				//remove old id
				unset($fields['id']);

				$fields['version_id'] = $in['version_id'];
				$fields['group_id'] = $group_id;

				$sql = $this->gen_sql($fields);

				$this->dbu->query("INSERT INTO tblquote_line SET ".$sql);
			}
		}
		// insert quote note for specific version of quote
			$note_data = $this->db->query("SELECT item_value, lang_id, active 	FROM 	note_fields
																				WHERE 	item_id 	= 	'".$in['quote_id']."'
																				AND 	item_type 	= 	'quote'
																				AND 	item_name 	= 	'notes'
																				AND 	version_id 	= 	'".$in['current_version_id']."'

				");
			while($note_data->next()){
				$this->db->query("INSERT INTO note_fields 	SET 	item_value 	= 	'".addslashes($note_data->f('item_value'))."',
																	lang_id 	= 	'".$note_data->f('lang_id')."',
																	active 		= 	'".$note_data->f('active')."',
																	version_id 	= 	'".$in['version_id']."',
																	item_id 	= 	'".$in['quote_id']."',
																	item_type 	= 	'quote',
																	item_name 	= 	'notes'

					");
			}
		// insert the general notes
		$gen = $this->db->query("SELECT * FROM note_fields WHERE item_type='quote' AND item_name='free_text_content' AND version_id='{$in['current_version_id']}' AND item_id='{$in['quote_id']}' ");
		$this->db->query("INSERT INTO note_fields SET
				item_value				= 	'".addslashes($gen->f('item_value'))."',
				item_type 				= 	'quote',
				item_name 				= 	'free_text_content',
				version_id 				= 	'".$in['version_id']."',
				item_id 				= 	'".$in['quote_id']."' ");

		msg::success ( gm("Quote Version was created"),'success');
		insert_message_log($this->pag,'{l}Quote Version was created by{endl} '.get_user_name($_SESSION['u_id']),$field_n,$in['quote_id']);
		// ark::$controller = 'nquote';
		json_out($in);
		return true;
	}

	public function activate(&$in){
		if(!$this->activate_validate($in)){
			return false;
		}

		$this->dbu->query("UPDATE tblquote_version SET active = 0 WHERE quote_id='".$in['quote_id']."'");
		$this->dbu->query("UPDATE tblquote_version SET active = 1 WHERE quote_id='".$in['quote_id']."' AND version_id = '".$in['version_id']."'");

		msg::$success = gm("Quote Version was selected");
		insert_message_log($this->pag,'{l}Quote Version was selected{endl} '.get_user_name($_SESSION['u_id']),'quote_id',$in['quote_id']);
		return true;
	}

	private function next_validate(&$in){
		$v = new validation($in);
		//quote_id, current_version_id
		$v->field('quote_id','Quote','required');
		$v->field('current_version_id','Quote Version','required');

		return $v->run();
	}

	private function activate_validate(&$in){
		$v = new validation($in);
		//quote_id, current_version_id
		$v->field('quote_id','Quote','required');
		$v->field('version_id','Quote Version','required');

		return $v->run();
	}

	private function gen_sql($fields){
		$sql = '';
		foreach ($fields as $field => $value) {
			$sql .= $field . " = '".addslashes($value)."',";
		}
		$sql = trim($sql,',');
		return $sql;
	}

	private function next_version_code($id){
		$alphas = range('A', 'Z');
		$pos = array_flip($alphas);

		$this->dbu->query("SELECT tblquote_version.* FROM tblquote_version WHERE quote_id = '".$id."' ORDER BY version_id DESC LIMIT 1");
		if($this->dbu->move_next() ){
			$next_version_code = $alphas[$pos[$this->dbu->f('version_code')]+1];
		}
		else{
		   $next_version_code='A';
		}

		return $next_version_code;
	}

	public function delete_validate(&$in) {
		$v = new validation($in);
		$v->f('id', 'Id', 'required:exist[tblquote.id]', gm('Invalid Id'));
		$v->f('version_id', 'Id', 'required:exist[tblquote_version.version_id]', gm('Invalid version Id'));
		return $v->run();
	}

	public function delete(&$in) {
		if(!$this->delete_validate($in)) {
			return false;
		}

		$current_version = $this->db->field("SELECT version_id FROM tblquote_version WHERE quote_id = '".$in['id']."' AND version_id != '".$in['version_id']."' ");
		if(!$current_version) {
			// msg::$error = gm("Last quote version can't be deleted! ");
			msg::error ( gm("Last quote version can't be deleted! "),'error');
			return false;
		}

		$this->db->query("DELETE FROM tblquote_group WHERE quote_id = '".$in['id']."' AND version_id = '".$in['version_id']."' ");
		$this->db->query("DELETE FROM tblquote_line WHERE quote_id = '".$in['id']."' AND version_id = '".$in['version_id']."' ");
		$this->db->query("DELETE FROM tblquote_version WHERE quote_id = '".$in['id']."' AND version_id = '".$in['version_id']."' ");
		$this->db->query("DELETE FROM note_fields WHERE item_type = 'quote' AND item_name = 'notes' AND item_id = '".$in['id']."' AND version_id = '".$in['version_id']."' ");
		$this->db->query("DELETE FROM note_fields WHERE item_type = 'quote' AND item_name = 'free_text_content' AND item_id = '".$in['id']."' AND version_id = '".$in['version_id']."' ");

		$last_quote = $this->db->query("SELECT version_id FROM tblquote_version WHERE quote_id = '".$in['id']."' ORDER BY version_id DESC LIMIT 1");
		$this->db->query("UPDATE tblquote_version SET active= 1 WHERE quote_id = '".$in['id']."' AND version_id = '".$last_quote->f('version_id')."' ");

		// msg::$success=gm("Quote version delete.");
		msg::success ( gm("Quote version delete."),'success');
		$in['quote_id'] = $in['id'];
		return true;


	}
}