<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
// $starty = console::getMicroTime();

global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($database_users);

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}

// $view_list = new at(ark::$viewpath.'quotes_list.html');
$result = array('list'=>array(),'max_rows'=>0);
if(!empty($in['start_date_js'])){
	$in['start_date'] =strtotime($in['start_date_js']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['stop_date_js'])){
	$in['stop_date'] =strtotime($in['stop_date_js']);
	$in['stop_date'] = mktime(23,59,59,date('n',$in['stop_date']),date('j',$in['stop_date']),date('y',$in['stop_date']));
}
$order_by_array = array('serial_number','quote_date','subject','buyer_name','amount');
// $db = new sqldb();
$today = mktime(0,0,0,date('n'),date('j'),date('Y'));

$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

$l_r = ROW_PER_PAGE;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$filter_q = 'WHERE 1=1 ';
$order_by = " ORDER BY tblquote.quote_date DESC ";
if(defined('SHORT_QUOTES_BY_SERIAL') && SHORT_QUOTES_BY_SERIAL == 1){
	$order_by = " ORDER BY tblquote.serial_number DESC ";
}
$arguments = '';
if($in['customer_id']){
	$arguments_c .= '&customer_id='.$in['customer_id'];
}
if(!$in['archived']){
	$filter_q.= " AND f_archived = '0' ";
}else{
	$filter_q.= " AND f_archived = '1' ";
	$arguments.="&archived=".$in['archived'];
	$arguments_o.="&archived=".$in['archived'];
}
if($in['search']){
	$filter_q.=" AND (tblquote.serial_number like '%".$in['search']."%' OR tblquote.buyer_name like '%".$in['search']."%'  )";
	$arguments.="&search=".$in['search'];
}
if($in['order_by']){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == 'true'){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];		
	}
}

if($in['start_date'] && $in['stop_date']){
	$filter_q.=" and tblquote.quote_date BETWEEN '".$in['start_date']."' and '".$in['stop_date']."' ";
	$arguments.="&start_date=".$in['start_date']."&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}
else if($in['start_date']){
	$filter_q.=" and cast(tblquote.quote_date as signed) > ".$in['start_date']." ";
	$arguments.="&start_date=".$in['start_date'];
}
else if($in['stop_date']){
	$filter_q.=" and cast(tblquote.quote_date as signed) < ".$in['stop_date']." ";
	$arguments.="&stop_date=".$in['stop_date'];
	$filter_link = 'none';
}
if(!isset($in['view'])){
	$in['view'] = 0;
}
if($in['view'] == 0){
	# do nothing
}
if($in['view'] == 1){
	$filter_q.=" AND tblquote.status_customer='0' AND tblquote_version.sent='0' ";
	$arguments.="&view=1";
}
if($in['view'] == 2){
	$filter_q.=" AND tblquote.status_customer='0' AND tblquote_version.sent = '1' ";
	$arguments.="&view=2";
}
if($in['view'] == 3){
	$filter_q.=" AND tblquote.status_customer > '1' ";
	$arguments.="&view=3";
}
if($in['view'] == 4){
	$filter_q.=" AND tblquote.status_customer = '1' ";
	$arguments.="&view=4";
}
if($in['view'] == 5){
	$filter_q.=" AND tblquote.created_by = '".$_SESSION['u_id']."' ";
	$arguments.="&view=5";
}
if($in['view'] == 6 ) {
	$filter_q .= " AND tblquote.pending = '1' AND tblquote.status_customer > '1' AND tblquote.transformation = '0' ";
	$arguments.="&view=6";
}

$filter_c = " AND 1=1 ";
$cust_id = '';
if($in['customer_id']){
	$cust_id=$in['customer_id'];
	$filter_c = " AND buyer_id='".$in['customer_id']."' ";
	$page_title_q='<h2>'.gm('List quotes').'<a href="index.php?do=quote-quotes&add=true" title="'.gm('Add').'" class="add">'.gm('Add').'</a>';
}
if($in['opportunity_id']){
	$quote_ids = "";
	$opportunity_quotes = $db->query("SELECT quote_id FROM  tblopportunity_quotes WHERE opportunity_id = '".$in['opportunity_id']."' ORDER BY id DESC");
	$j=0;
	while($opportunity_quotes->next()){
		$quote_ids .= "'".$opportunity_quotes->f('quote_id')."',";
		$j++;
	}
	$quote_ids = rtrim($quote_ids,',');
	$filter_q.=" AND tblquote.id IN (".$quote_ids.") ";
	$arguments_c.="&opportunity_id=".$in['opportunity_id'];
}
$arguments = $arguments.$arguments_c;
// LEFT JOIN tblquote_stage ON tblquote_stage.id = tblquote.stage_id
// ,tblquote_stage.name as stage_name
// LEFT JOIN tblquote_stage ON tblquote_stage.id = tblquote.stage_id

$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");

if($admin_licence != '3' && ONLY_IF_ACC_MANAG4 == '1'){
	$filter_q.= " AND CONCAT( ',', acc_manager_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
}


$max_rows=$db->field("SELECT count(tblquote.id) FROM tblquote INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id  ".$filter_q." ".$filter_c);
$result['max_rows'] = $max_rows;
$quote = $db->query("SELECT tblquote.*,tblquote_version.*, tblquote_version.sent as sent, tblquote_version.sent_date as sent_date, tblquote.sent as sent_q
            FROM tblquote
            INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
            ".$filter_q." AND active=1 ".$filter_c.$order_by ." LIMIT ".$offset*$l_r.",".$l_r);

/* normal user privileges */
$u_id = $_SESSION['u_id'];											// $user_credential - 1 (admin)
if($_SESSION['group']=='admin') {									//                  - 2 (quote_admin)
	$user_credential = 1;											//					- 3 (none => check if is author or creator)
} else {
	if(in_array('quote', $_SESSION['admin_sett'])) {
		$user_credential = 2;
	} else {
		$user_credential = 3;
	}
}

$i=0;
while($quote->next()){
	$is_admin = true;
	if($user_credential == 3) {
		$created_by = $db->field("SELECT created_by FROM tblquote WHERE id = '".$quote->f('id')."'");
		$author_id = $db->field("SELECT author_id FROM tblquote WHERE id = '".$quote->f('id')."'");
		if($created_by == $u_id || $author_id == $u_id) {
			$is_admin = true;
			// $view_list->assign('is_admin', true,'template_row');
		} else {
			$is_admin = false;
			// $view_list->assign('is_admin', false,'template_row');
		}
	} /*else {
		$view_list->assign('is_admin', true,'template_row');
	}*/

	$ref = '';
	if(ACCOUNT_QUOTE_REF && $quote->f('buyer_reference')){
		$ref = $quote->f('buyer_reference').ACCOUNT_QUOTE_DEL;
	}

	$amount = $db->field("SELECT SUM(amount) FROM tblquote_line WHERE quote_id='".$quote->f('id')."' AND version_id='".$quote->f('version_id')."' AND show_block_total='0' ");

	if($quote->f('discount') && $quote->f('apply_discount') > 1){
		$amount = $amount - ($amount*$quote->f('discount')/100);
	}

	if($quote->f('currency_rate')){
		$amount = $amount*return_value($quote->f('currency_rate'));
	}
	$status = '';
	if($quote->f('validity_date') && $quote->f('status_customer') == 0){
		$valid = mktime(0,0,0,date('n',$quote->f('validity_date')),date('j',$quote->f('validity_date')),date('Y',$quote->f('validity_date')));
		if($valid < $today){
			$status = gm('Expired');
			// $text = gm('Expired');
		}
	}
	$link_end = '&type='.ACCOUNT_QUOTE_PDF_FORMAT;
	if($quote->f('pdf_layout') && $quote->f('use_custom_template')==0){
		$link_end='&type='.$quote->f('pdf_layout').'&logo='.$quote->f('pdf_logo');
	}elseif($quote->f('pdf_layout') && $quote->f('use_custom_template')==1){
		$link_end = '&custom_type='.$quote->f('pdf_layout').'&logo='.$quote->f('pdf_logo');
	}
	#if we are using a customer pdf template
	if(defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $quote->f('pdf_layout') == 0){
		$link_end = '&custom_type='.ACCOUNT_QUOTE_PDF_FORMAT.'&logo='.$quote->f('pdf_logo');
		$in['use_custom'] = 1;
	}
	$lng = 1;
	if($quote->f('email_language')){
		$lng = $quote->f('email_language');
	}

	$was_sent = $db->field("SELECT count(version_id) FROM tblquote_version WHERE sent='1' AND quote_id='".$quote->f('id')."' ");
	$item=array(
		'info_link'     			=> ('index.php?do=quote-quote&quote_id='.$quote->f('id')).$arguments,
		'edit_link'     			=> ('index.php?do=quote-nquote&quote_id='.$quote->f('id')).$arguments,
		'archive_link' 		 		=> array('do'=>'quote-quotes-quote-archive', 'id'=>$quote->f('id')),
		'delete_link' 		 		=> array('do'=>'quote-quotes-quote-delete', 'id'=>$quote->f('id')),
		'activate_link' 		 	=> array('do'=>'quote-quotes-quote-activate', 'id'=>$quote->f('id')),
		'pdf_link'					=> 'index.php?do=quote-print&customer_id='.$in['customer_id'].'&id='.$quote->f('id').'&version_id='.$quote->f('version_id').'&lid='.$lng.$link_end,
		'check_add_to_product'		=> $_SESSION['add_to_quote'][$quote->f('id')] == 1 ? true : false,
		'view_delete_link2'			=> $_SESSION['access_level'] == 1 ? '' : 'hide',
		'hide_on_a'					=> $in['archived'] == 1 ? 'hide' : '',
		'archived'					=> $quote->f('f_archived') == 1 ? true : false,
		'status'					=> $quote->f('sent') > 0 ? false : true,
		'status2'					=> $quote->f('status_customer'),
		'id'            	    	=> $quote->f('id'),
		'serial_number' 	    	=> $ref.$quote->f('serial_number'),
		'quote_version' 	    	=> $in['archived'] == 1 ? $db->field("SELECT version_code FROM tblquote_version WHERE active=1 AND quote_id='".$quote->f('id')."' ") : $quote->f('version_code'),
		'created'       	   	 	=> $quote->f('version_date') ? date(ACCOUNT_DATE_FORMAT,$quote->f('version_date')) : date(ACCOUNT_DATE_FORMAT,$quote->f('quote_date')),
		'seller_name'   	    	=> $quote->f('seller_name'),
		'buyer_name'    	    	=> $quote->f('buyer_name'),
		'amount'					=> place_currency(display_number($amount)),
		// 'stage'					=> $quote->f('stage_name'),
		'template_link'				=> 'index.php?do=quote-nquote&quote_id='.$quote->f('id')."&template=1".$arguments,
		'alt_for_delete_icon'		=> $in['archived'] == 1 ? gm('Delete') : gm('Archive'),
		'subject'					=> htmlspecialchars($quote->f('subject')),
		'if_subject'				=> $quote->f('subject') ? true : false,
		'valid'						=> $status ? true : false,
		'validity'					=> $status,
		// 'status_title'				=> $text,
		'confirm'					=> gm('Confirm'),
		'ok'						=> gm('Ok'),
		'cancel'					=> gm('Cancel'),
		'is_admin'					=> $is_admin
	);

	$opts = array(gm('Draft'),gm('Rejected'),gm('Accepted'),gm('Accepted'),gm('Accepted'),gm('Sent'));
	$field = 'status_customer';
	// if($quote->f('sent')){
		// foreach ($fields as $field){
			// $view_list->assign(array(strtoupper($field),$opts[$quote->f($field)]),'template_row');
			$item[strtolower($field)]=$opts[$quote->f($field)];
		// }
	/*}else{
		$view_list->assign('status_customer',$opts[5],'template_row');
	}*/

	// if($was_sent && $quote->f('status_customer') == 0){
	if($quote->f('sent') == 1 && $quote->f('status_customer') == 0){
		$item['status_customer']=$opts[5];
		$item['status2']='5';
	}

	// if($status){
		// $item['status_customer']=$status;
		// $item['status2']='-1';
		// $view_list->assign('status_customer',$status,'template_row');
	// }
	array_push($result['list'], $item);
	// $view_list->loop('template_row');
	$i++;
}
$result['lr'] = $l_r;
$result['adv_quotes'] = defined('NEW_SUBSCRIPTION') ? (ADV_QUOTE == 1 ? true: false ) : true;
json_out($result);

function get_quote_list($in){
	$db = new sqldb();
	if(!isset($in['quote_id']) || !is_numeric($in['quote_id'])){
		return array();
	}
	$result = array( 'list' => array());
	$today = mktime(0,0,0,date('n'),date('j'),date('Y'));

	$l_r = ROW_PER_PAGE;

	$order_by = " ORDER BY tblquote_version.version_code ASC ";

	$quote = $db->query("SELECT tblquote.*,tblquote_version.*, tblquote_version.sent as sent, tblquote_version.sent_date as sent_date, tblquote.sent as sent_q,tblquote_version.active AS activeV
	            FROM tblquote
	            INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
	            WHERE quote_id='".$in['quote_id']."'
	            ".$order_by)->getAll();
	$row = count($quote);

	$u_id = $_SESSION['u_id'];											// $user_credential - 1 (admin)
	if($_SESSION['group']=='admin') {									//                  - 2 (quote_admin)
		$user_credential = 1;											//					- 3 (none => check if is author or creator)
	} else {
		if(in_array('quote', $_SESSION['admin_sett'])) {
			$user_credential = 2;
		} else {
			$user_credential = 3;
		}
	}
	if($user_credential == 3) {
		$created_by = $db->field("SELECT created_by FROM tblquote WHERE id = '".$in['quote_id']."' ");
		$author_id = $db->field("SELECT author_id FROM tblquote WHERE id = '".$in['quote_id']."' ");
		if($created_by == $u_id || $author_id == $u_id) {
			$is_admin = true;
			// $view->assign('is_admin', true,'template_row');
		} else {
			$is_admin = false;
			// $view->assign('is_admin', false,'template_row');
		}
	} else {
		$is_admin = true;
		// $view->assign('is_admin', true,'template_row');
	}

	$i=0;
	foreach ($quote as $key => $value) {
	// while($quote->next()){

		$amount = $db->field("SELECT SUM(amount) FROM tblquote_line WHERE quote_id='".$value['id']."' AND version_id='".$value['version_id']."' AND show_block_total='0' ");

		if($value['discount'] && $value['apply_discount'] > 1){
			$amount = $amount - ($amount*$value['discount']/100);
		}

		if($value['currency_rate']){
			$amount = $amount*return_value($value['currency_rate']);
		}
		$status = '';
		$text = '';
		if($value['validity_date'] && $value['status_customer'] == 0){
			$valid = mktime(0,0,0,date('n',$value['validity_date']),date('j',$value['validity_date']),date('Y',$value['validity_date']));
			if($valid < $today){
				$status = 'late';
				$text = 'Expired';
			}
		}

		$link_end = '&type='.ACCOUNT_QUOTE_PDF_FORMAT;
		if($value['pdf_layout'] && $value['use_custom_template']==0){
			$link_end='&type='.$value['pdf_layout'].'&logo='.$value['pdf_logo'];
		}elseif($value['pdf_layout'] && $value['use_custom_template']==1){
			$link_end = '&custom_type='.$value['pdf_layout'].'&logo='.$value['pdf_logo'];
		}
		$lng = 1;
		if($value['email_language']){
			$lng = $value['email_language'];
		}

		$was_sent = $db->field("SELECT count(version_id) FROM tblquote_version WHERE sent='1' AND quote_id='".$value['id']."' ");
		$item = array(
			'is_admin'				=> $is_admin,
		  	'info_link'    			=> 'index.php?do=quote-quote&quote_id='.$value['id'].'&version_id='.$value['version_id'],
		  	'delete_link' 			=> array('do'=>'quote-quotes-quote_version-delete', 'id'=>$value['id'],'xget'=>'quote_list','version_id'=>$value['version_id']),		  	
		  	'pdf_link'				=> 'index.php?do=quote-print&id='.$value['id'].'&version_id='.$value['version_id'].'&lid='.$lng.$link_end,
		  	'status'				=> $value['status_customer'] > 0 ? false :($value['sent'] > 0 ? false : ($key+1 == $row) ? true : false),
		  	'id'               		=> $value['id'],
			'quote_version'    		=> $value['version_code'],
			'created'          		=> date(ACCOUNT_DATE_FORMAT,$value['version_date']),
			'amount'				=> place_currency(display_number($amount)),
			'alt_for_delete_icon'	=> $in['archived'] == 1 ? gm('Delete') : gm('Archive'),
			'status_title'			=> $text,
			'version_id'			=> $value['version_id'],
			'archived'				=> $value['v_archived'] == 0 ? false : true,
		);

		$opts = array(gm('Draft'),gm('Rejected'),gm('Accepted'),gm('Accepted'),gm('Accepted'),gm('Sent'));
		// $opts = array('gray','red','green','green','green','orange');
		$fields = 'status_customer';
			// foreach ($fields as $field){
		if($value['sent'] == 0){
				$item[strtolower($fields)]=$opts[0];
				$item['status2']='0';
		}else{
			if($value['activeV']==1){
				switch ($value['status_customer']) {
					case '1':
						$item[strtolower($fields)]=$opts[1];
						$item['status2']='1';
						break;
					case '2':
						$item[strtolower($fields)]=$opts[2];
						$item['status2']='2';
						break;
					default:
						$item[strtolower($fields)]=$opts[5];
						$item['status2']='5';
						break;
				}
			}else{
				$item[strtolower($fields)]=$opts[5];
				$item['status2']='5';
			}
		}
		if($value['status_customer']=='2' && $value['sent']=='0'){
			$item['status2']='9874125486355841568522487568963214'; // when the quote is accepted, no more edit
		}

		array_push($result['list'], $item);
		$i++;
	}

	return $result;
}


function get_quotesTemplates(&$in)
{
	$db = new sqldb();
	$result = array('list'=>array());

	$arguments_s = '';
	$arguments='';
	$arguments_o='';
	$arguments_a='';
	$l_r =ROW_PER_PAGE;

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}

	$filter = " f_archived='2' ";

	$filter_c=' ';

	if(!empty($in['search'])){
		$filter .= " AND serial_number LIKE '%".$in['search']."%' ";
		$arguments_s.="&search=".$in['search'];
	}

	$arguments = $arguments.$arguments_s;

	$max_rows =  $db->field("SELECT count(id) FROM tblquote WHERE ".$filter );
	$result['max_rows'] = $max_rows;
	$db->query("SELECT tblquote.*,tblquote_version.*,tblquote_stage.name as stage_name
	            FROM tblquote
	            INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
	            LEFT JOIN tblquote_stage ON tblquote_stage.id = tblquote.stage_id
	            WHERE ".$filter." ORDER BY tblquote.serial_number LIMIT ".$offset*$l_r.",".$l_r );
	$j=0;
	while($db->move_next()){

		$list=array(
			'name'						=> $db->f('serial_number'),
			'id'						=> $db->f('id'),
			// 'edit_link'					=> 'index.php?do=quote-nquote&quote_template_id='.$db->f('id').'&template=1',
			// 'delete_link'				=> 'index.php?do=quote-quote_templates_list-quote-delete_template&id='.$db->f('id').$arguments.$arguments_o,
			'delete_link' 		 		=> array('do'=>'quote-quotes-quote-delete_template', 'id'=>$db->f('id'),'xget'=>'quotesTemplates'),
		);
		$result['list'][]=$list;
	}
	if($in['index']){
		$result['index'] = $in['index'];
	}
	return $result;	
}