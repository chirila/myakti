<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	$o=array(
		'purchase_price'			=> display_number_var_dec($in['purchase_price']),
		'account_number_format'       => ACCOUNT_NUMBER_FORMAT,
		'nr_decimals'			=> ARTICLE_PRICE_COMMA_DIGITS,
		'index'				=> $in['index']
	);

 	json_out($o);

 ?>