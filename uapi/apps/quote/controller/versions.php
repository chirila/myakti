<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($_SESSION['customer_id']){
	$in['customer_id'] = $_SESSION['customer_id'];
	$in['buyer_id'] = $_SESSION['customer_id'];
}

if($_SESSION['contact_id']){
	$in['contact_id'] = $_SESSION['contact_id'];
} else {
	exit();
}


	$db = new sqldb();
	if(!isset($in['id']) || !is_numeric($in['id'])){
		return array();
	} else {
		$in['quote_id'] = $in['id'];
	}
	$result = array( 'list' => array());
	$today = mktime(0,0,0,date('n'),date('j'),date('Y'));

	$l_r = ROW_PER_PAGE;

	$order_by = " ORDER BY tblquote_version.version_code ASC ";

	$quote = $db->query("SELECT tblquote.*,tblquote_version.*, tblquote_version.sent as sent, tblquote_version.sent_date as sent_date, tblquote.sent as sent_q,tblquote_version.active AS activeV
	            FROM tblquote
	            INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
	            WHERE quote_id='".$in['quote_id']."' and tblquote.buyer_id = '".$in['buyer_id']."' and tblquote.contact_id = '".$in['contact_id']."'
	            ".$order_by)->getAll();
	$row = count($quote);

	$i=0;
	foreach ($quote as $key => $value) {
	// while($quote->next()){

		$amount = $db->field("SELECT SUM(amount) FROM tblquote_line WHERE quote_id='".$value['id']."' AND version_id='".$value['version_id']."' AND show_block_total='0' ");

		if($value['discount'] && $value['apply_discount'] > 1){
			$amount = $amount - ($amount*$value['discount']/100);
		}

		if($value['currency_rate']){
			$amount = $amount*return_value($value['currency_rate']);
		}
		$status = '';
		$text = '';
		if($value['validity_date'] && $value['status_customer'] == 0){
			$valid = mktime(0,0,0,date('n',$value['validity_date']),date('j',$value['validity_date']),date('Y',$value['validity_date']));
			if($valid < $today){
				$status = 'late';
				$text = 'Expired';
			}
		}

		$lng = 1;
		if($value['email_language']){
			$lng = $value['email_language'];
		}

		$was_sent = $db->field("SELECT count(version_id) FROM tblquote_version WHERE sent='1' AND quote_id='".$value['id']."' ");
		$item = array(
			'status'				=> $value['status_customer'] > 0 ? false :($value['sent'] > 0 ? false : ($key+1 == $row) ? true : false),
		  	'id'               		=> $value['id'],
			'quote_version'    		=> $value['version_code'],
			'created'          		=> date(ACCOUNT_DATE_FORMAT,$value['version_date']),
			'amount'				=> display_number($amount),
			'amount2'				=> place_currency(display_number($amount)),
			'status_title'			=> $text,
			'version_id'			=> $value['version_id'],
			'archived'				=> $value['v_archived'] == 0 ? false : true,
		);

		$opts = array(gm('Draft'),gm('Rejected'),gm('Accepted'),gm('Accepted'),gm('Accepted'),gm('Sent'));
		// $opts = array('gray','red','green','green','green','orange');
		$fields = 'status_customer';
			// foreach ($fields as $field){
		if($value['sent'] == 0){
				$item[strtolower($fields)]=$opts[0];
				$item['status2']='0';
		}else{
			if($value['activeV']==1){
				switch ($value['status_customer']) {
					case '1':
						$item[strtolower($fields)]=$opts[1];
						$item['status2']='1';
						break;
					case '2':
						$item[strtolower($fields)]=$opts[2];
						$item['status2']='2';
						break;
					default:
						$item[strtolower($fields)]=$opts[5];
						$item['status2']='5';
						break;
				}
			}else{
				$item[strtolower($fields)]=$opts[5];
				$item['status2']='5';
			}
		}
		if($value['status_customer']=='2' && $value['sent']=='0'){
			$item['status2']='9874125486355841568522487568963214'; // when the quote is accepted, no more edit
		}

		array_push($result['list'], $item);
		$i++;
	}

json_out($result);
