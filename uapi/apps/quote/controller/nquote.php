<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');

class QuoteCtrl extends Controller{

	private $quote;
	private $quote_id;
	private $discount;
	private $grand_total;
	private $total_vat;
	private $grand_total_vat;
	private $currency_type;
	private $currency_rate;
	private $default_total;
	private $allowPacking;
	private $allowSaleUnit;
	private $is_discount;
	private $is_vat;
	private $version_id;

	function __construct($in,$db = null)
	{

		parent::__construct($in,$db = null);
		if(isset($in['template_id']) && is_numeric($in['template_id']) ){
			$this->in['quote_id']=$in['template_id'];
			unset($in['version_id']);
			unset($this->in['version_id']);
		}
		$this->quote_id=$in['quote_id'];
		$this->version_id=$in['version_id'];
	}

	public function getQuote()
	{
		if($this->in['quote_id'] == 'tmp'){ //add
		// if(!$this->in['quote_id']  && !$this->in['duplicate_quote_id'] && !$this->in['main_quote_id'] && !$this->in['template']){ //add
			$this->getAddQuote();
		}/*elseif ($this->in['template']) { # create quote template
			$this->getTemplateQuote();
		}*/elseif($this->in['quote_id'] || $this->in['duplicate_quote_id']){ # edit
			if($this->in['xview']){
				$this->getViewQuote();
			}else{
				$this->getEditQuote();
			}
		}else{
			$this->output(array('redirect'=>'quotes'));
		}
	}

	private function getQuoteValid()
	{
		
		$v = new validation($this->in);
		if($this->in['quote_id']){
			$v->field('quote_id', 'ID', 'required:exist[tblquote.id]', gm('Invalid ID'));
		}
		if($this->in['duplicate_quote_id']){
			$v->field('duplicate_quote_id', 'ID', 'required:exist[tblquote.id]', gm('Invalid ID'));	
		}
		if(!$v->run()){
			return array('redirect'=>'quotes');
		} else {
			return '';
		}

	}

	private function getAddQuote()
	{
		$user_lang = $_SESSION['l'];
		if ($user_lang=='nl')
		{
			$user_lang='du';
		}
		switch ($user_lang) {
			case 'en':
				$user_lang_id='1';
				break;
			case 'du':
				$user_lang_id='3';
				break;
			case 'fr':
				$user_lang_id='2';
				break;
			case 'de':
				$user_lang_id='4';
				break;
		}
		$in = $this->in;

		if($in['quote_date']){
			$in['quote_date'] = strtotime($in['quote_date'])*1000;
		}else{
			$in['quote_date'] = time()*1000;
		}
		if($in['validity_date']){
			$in['validity_date'] = strtotime($in['validity_date'])*1000;
		}

		$serial = generate_quote_number(DATABASE_NAME);
		$page_quote = $this->db->field("SELECT value FROM settings WHERE constant_name='QUOTE_GENERAL_CONDITION_PAGE'");
		$in['page']=$page_quote;

		/*if($in['buyer_id']){*/
			if($in['save_final']==1){
				$final=true;
			}else{
				$final=false;
			}
		/*}
		*/
		// $default_note = $this->db->field("SELECT value FROM default_data WHERE type='quote_note' ");

		$output = array(
		    'ADV_QUOTE'						=> defined('NEW_SUBSCRIPTION') ? (ADV_QUOTE == 1 ? true: false ) : true,
			'save_final'					=> $final,
			'general_conditions_new_page'	=> $in['page'] == 1 ? true : false,
		    'do_request' 					=> 'quote-nquote-quote-updateCustomerData',
			'CURRENCY_TYPE_LIST'        	=> build_currency_list(ACCOUNT_CURRENCY_TYPE),
			'currency_type'					=> ACCOUNT_CURRENCY_TYPE,
			'default_currency'				=> ACCOUNT_CURRENCY_TYPE,
			'country_dd'					=> build_country_list(0),
			// 'notes'							=> $default_note,
			'country_id'					=> @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26',
			'main_country_id'				=> @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26',
			'APPLY_DISCOUNT_LIST'      		=> build_apply_discount_list(0),
			'apply_discount'				=> '0',
			'language_dd' 					=> build_language_dd_new(0),
			'email_language'				=> $user_lang_id,
			'authors'						=> $this->get_author($in),
			'accmanager'					=> $this->get_accmanager($in),
			'cc'							=> $this->get_cc($in),
			'contacts'						=> $in['contact_id'] ? array() : $this->get_contacts($in),
			'sameAddress'					=> true,
			'main_comp_info' 				=> $this->getQuoteIdentity('0'), # get identity
			'identity_id'					=> '0',
			'add_customer'					=> !$in['buyer_id'] && !$in['contact_id'] ? true : false,
			'serial_number'					=> $serial,
			'seller_name'               	=> ACCOUNT_COMPANY,
			'quote_date'					=> $in['quote_date'],
			'discount'                  	=> display_number($in['discount']),
			'delivery_address' 	    		=> $in['delivery_address'],
	  		'delivery_address_txt'	     	=> nl2br($in['delivery_address']),
	  		'delivery_address_id'			=> $in['delivery_address_id'],
	  		'view_delivery'					=> $in['delivery_address'] ? true : false,
			'seller_d_address'	    		=> ACCOUNT_DELIVERY_ADDRESS,
			'seller_d_zip'   				=> ACCOUNT_DELIVERY_ZIP,
			'seller_d_city'   				=> ACCOUNT_DELIVERY_CITY,
			//Seller Billing Details
			'seller_b_address'    			=> ACCOUNT_BILLING_ADDRESS,
			'seller_b_zip'   		 		=> ACCOUNT_BILLING_ZIP,
			'seller_b_city'   				=> ACCOUNT_BILLING_CITY,
			'seller_bwt_nr'   				=> ACCOUNT_VAT_NUMBER,
			'author_id'						=> $in['author_id'] ? $in['author_id'] : $_SESSION['u_id'],
			'subject'						=> $in['subject'],
			'downpayment'					=> $in['downpayment'] ? $in['downpayment'] : display_number(0),
			'vat'							=> $in['vat'] ? $in['vat'] : display_number(0),
			'own_reference'					=> $in['own_reference'],
			'validity_date'					=> $in['validity_date'],
			'show_vat'						=> isset($in['show_vat']) ? $in['show_vat'] : '1',
			'show_vat_pdf'					=> isset($in['show_vat_pdf']) ? $in['show_vat_pdf'] : '1',
			'show_grand'					=> isset($in['show_grand']) ? $in['show_grand'] : '1',
			'show_d'						=> isset($in['show_d']) ? $in['show_d'] : '1',
			'currency_rate'					=> $in['currency_rate'],
			'show_img_q'					=> $in['show_img_q'],
			'free_field' 					=> '',
			'pdf_layout'					=> $in['pdf_layout'],
			'type'							=> '0',
			'VAT_TOTAL'						=> display_number(0),
			'GRAND_TOTAL'					=> display_number(0),
			'total_wo_vat' 					=> display_number(0),
			'TOTAL_DEFAULT_CURRENCY'		=> display_number(0),
			'TOTAL_WITHOUT'					=> display_number(0),
			'currency_txt'		        	=> currency::get_currency(ACCOUNT_CURRENCY_TYPE),
			'hide_currency2' 				=> ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
			'hide_currency1' 				=> ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
			'show_QC'						=> true,
			'show_UP'						=> true,
			'show_PS'						=> true,			
		);
		
		// $output['show_vat']						= $in['show_vat'] ? ' ' : 'hide';
		$output['TOTAL_DISCOUNT']				= display_number(-$this->grand_total * $this->discount / 100);
		$apply_discount = '0';
		if($in['buyer_id']){
			$output['buyer_id'] = $in['buyer_id'];
		    $buyer_info = $this->db->query("SELECT customer_legal_type.name as l_name, customers.c_email, customers.acc_manager_name, customers.user_id,
		                                   customers.our_reference, customers.fixed_discount, customers.no_vat, customers.currency_id, customers.line_discount, 
		                                   customers.apply_fix_disc, customers.apply_line_disc, customers.name, customers.identity_id, customers.internal_language
		                                   FROM customers
									  LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
									  WHERE customers.customer_id = '".$in['buyer_id']."' ");
	      	$acc_manager_id = explode(',',$buyer_info->f('user_id'));
	      	$acc_manager_name = explode(',',$buyer_info->f('acc_manager_name'));
		  	$output['acc_manager_id'] = $acc_manager_id[0];
		  	$output['acc_manager_name']	= $acc_manager_name[0];
	      
			$output['buyer_name'] = $buyer_info->f('l_name') ? stripslashes($buyer_info->f('name')).' '.$buyer_info->f('l_name') : stripslashes($buyer_info->f('name'));
			$text = gm('Customer Name');
			
			$$output['buyer_reference'] = $buyer_info->f('our_reference');
			$q_ref = $buyer_info->f('our_reference');
			
			if($q_ref){
				$q_ref = $q_ref.ACCOUNT_QUOTE_DEL;
			}
			$output['REF_SERIAL_NUMBER'] = $q_ref.$output['serial_number'];

			if( !return_value($in['discount']) ){
	            $output['discount'] = display_number($buyer_info->f("fixed_discount"));
	        }

	        $output['discount_line_gen']= display_number($buyer_info->f('line_discount'));
	        if($buyer_info->f('apply_fix_disc') && $buyer_info->f('apply_line_disc')){
				$apply_discount = '3';
			}else{
				if($buyer_info->f('apply_fix_disc')){
					$apply_discount = '2';
				}
				if($buyer_info->f('apply_line_disc')){
					$apply_discount = '1';
				}
			}
			// if(!isset($in['var'])){
			$output['vat'] = get_customer_vat($in['buyer_id']);
			// }
			
			$output['apply_discount'] 			= $apply_discount;
	        $output['BUYER_EMAIL'] 				= $buyer_info->f('c_email');
	        $output['no_buyer_vat']	 			= $buyer_info->f("no_vat");
	        $output['currency_type'] 			= $buyer_info->f('currency_id') ? $buyer_info->f('currency_id') : ACCOUNT_CURRENCY_TYPE;

	        if($output['currency_type'] != ACCOUNT_CURRENCY_TYPE){
	        	$params = array(
	        		'currency' 					=> currency::get_currency($output['currency_type'],'code'),
					'acc' 						=> ACCOUNT_NUMBER_FORMAT,
					'into' 						=> currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code')
				);
	        	$output['currency_rate'] 		= $this->get_currency_convertor($params);
	        }

	        $output['email_language']	 		= $buyer_info->f('internal_language') ? $buyer_info->f('internal_language') : $user_lang_id;

	        $buyer_details = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='{$in['buyer_id']}' AND is_primary='1' ");
	        $output['buyer_country_id']         = $buyer_details->f('country_id');
			$output['BUYER_COUNTRY_NAME']       = get_country_name($buyer_details->f('country_id'));
			$output['buyer_state_id']           = $buyer_details->f('state_id');
			$output['BUYER_STATE_NAME']         = get_state_name($buyer_details->f('state_id'));
			$output['buyer_city']               = $buyer_details->f('city');
			$output['buyer_zip']                = $buyer_details->f('zip');
			$output['buyer_address']            = $buyer_details->f('address');
			$output['free_field'] 				= $buyer_details->f('address')."\n".$buyer_details->f('zip').' '.$buyer_details->f('city')."\n".get_country_name($buyer_details->f('country_id'));
			$output['free_field_txt']           = nl2br($output['free_field']);
	        $output['field']                    = 'customer_id';
	        $output['addresses']				= $this->get_addresses($in);
	        $output['identity_id']				= $buyer_details->f('identity_id');
	        $output['main_comp_info'] 			= $this->getQuoteIdentity($buyer_info->f('identity_id')); # get identity
			$output['do_request'] 				= 'quote-nquote-quote-add';
			$output['main_address_id']			= $buyer_details->f('address_id');
			
			if($in['contact_id']){
				$output['contact_id'] = $in['contact_id'];
				$output['contact_name'] = $this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE contact_id='{$in['contact_id']}' ");
			}

		}else if($in['contact_id']){
			$buyer_info = $this->db->query("SELECT phone, cell, email, CONCAT_WS(' ',firstname, lastname) as name FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			$buyer_details = $this->db->query("SELECT * FROM customer_contact_address WHERE contact_id='{$in['contact_id']}' AND is_primary='1' ");
			$text =gm('Name');
			$q_ref = ACCOUNT_QUOTE_DEL;
			$output['REF_SERIAL_NUMBER'] 		= $q_ref.$output['serial_number'];
			$output['contact_id'] 				= $in['contact_id'];
			$output['BUYER_EMAIL'] 				= $buyer_info->f('email');
			$output['CONTACT_NAME'] 			= '';
			$output['buyer_name'] 				= $buyer_info->f('name');
			$output['field']                    = 'contact_id';
			$output['do_request'] 				= 'quote-nquote-quote-add';
			$output['buyer_city']               = $buyer_details->f('city');
			$output['buyer_zip']                = $buyer_details->f('zip');
			$output['buyer_address']            = $buyer_details->f('address');
			$output['free_field'] 				= $buyer_details->f('address')."\n".$buyer_details->f('zip').' '.$buyer_details->f('city')."\n".get_country_name($buyer_details->f('country_id'));
			$output['free_field_txt']           = nl2br($output['free_field']);
	        $output['addresses']				= $this->get_addresses($in);
		}
		$output['NAME_TEXT']					= $text;

		$in['lang_id'] = $output['email_language'];
		/*$notes = $this->get_notes($in);*/
		
		$notes = $this->get_notes($in);
		$output['quote_group'] = array();

		$output['free_text_content']				= $notes['free_text_content'];
		$output['notes'] 							= $notes['notes'];
		$output['translate_loop']					= $notes['translate_loop'];
		$output['translate_loop_custom'] 			= $notes['translate_loop_custom'];
		$output['lang_cls'] 						= $notes['lang_cls'];

		$this->out = $output;
	}

	private function getTemplateQuote()
	{
		# code...
	}

	private function getEditQuote()
	{

		$valid = $this->getQuoteValid();
		if($valid){
			$this->output($valid);
		}

		$output = array();
		$output['ADV_QUOTE'] = defined('NEW_SUBSCRIPTION') ? (ADV_QUOTE == 1 ? true: false ) : true;
		$in = $this->in;
		// $view->assign('HIDE_LINE', '','quote_table');
		if($in['duplicate_quote_id']){
			$page_title=gm("Duplicate Quote");
			$next_function='quote-nquote-quote-add';
			$this->quote_id=$in['duplicate_quote_id'];
		}
		else{
			$is_edit = true;
			$page_title=gm("Edit Quote");
			$next_function='quote-nquote-quote-update';
			$this->quote_id=$in['quote_id'];
		}
		if(isset($in['version_id']) && is_numeric($in['version_id'])){
			$filter = 'AND tblquote_version.version_id = '.$in['version_id'];

		}else{
			$filter = 'AND tblquote_version.active = 1';
		}
		$quote = $this->db->query("SELECT tblquote.*,tblquote_version.active,tblquote_version.version_code,tblquote_version.version_id,tblquote_version.active AS version_active,
								tblquote_version.version_date, tblquote_version.discount,tblquote_version.discount_line_gen,tblquote_version.apply_discount
	            FROM tblquote
	            INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
	            WHERE tblquote.id='".$this->quote_id."' AND 1=1 ".$filter);
		if(!$quote->next()){
			$this->output(array('redirect'=>'quotes'));
			return true;
		}
		if(!getItemPerm( array( 'module' => 'quote' , 'item' => array( $quote->f('author_id'),$quote->f('created_by') ) ) ) && !isset($in['template_id']) ) {
			$this->output(array('redirect'=>'quotes'));
			return true;
		}		
		$this->quote = $quote;

		$output['main_comp_info'] = $this->getQuoteIdentity($quote->f('identity_id')); # get identity
		$output['identity_id'] = $quote->f('identity_id');
		$page_quote = $this->db->field("SELECT value FROM settings WHERE constant_name='QUOTE_GENERAL_CONDITION_PAGE'");
		$in['page']=$page_quote;
		$in['general_conditions_new_page']=$quote->f('general_conditions_new_page');

		$in['show_vat'] = $quote->f('show_vat');
		$in['show_grand'] = $quote->f('show_grand');
		
		$this->is_discount=$quote->gf('apply_discount');
		$cols = 3;
		$hcols = 5;
		if($this->is_discount == 0 || $this->is_discount == 2){
			$cols++;
			$hcols++;
		}
	   	$this->is_vat=$quote->gf('show_vat');
		if($quote->f('use_package')){
			$output['ALLOW_QUOTE_PACKING']=1;
			$this->allowPacking = true;
			// $use = true;
		}else{
			$output['ALLOW_QUOTE_PACKING']=0;
			$this->allowPacking = false;
			$cols++;
			$hcols++;
			// $use = false;
		}
		if($quote->f('use_sale_unit')){
		    $output['ALLOW_QUOTE_SALE_UNIT']=1;
		    $this->allowSaleUnit = true;
		    // $use2 = true;
		}else{
			// $use2 = false;
			$output['ALLOW_QUOTE_SALE_UNIT']=0;
			$this->allowSaleUnit = false;
		}
		$output['colum'] = $cols;
		$in['colum'] = $cols;
		$output['hcolum'] = $hcols;
		$customer_id = $quote->f('buyer_id');
		$vat = display_number($quote->f('vat'));
		$contact_name = $this->db->query("SELECT firstname,lastname,customer_contacts.contact_id FROM customer_contacts
		                                 INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
									WHERE customer_contacts.contact_id='".$quote->f('contact_id')."' AND customer_contactsIds.customer_id='".$quote->f('buyer_id')."'");
		// $contact_name = $this->db->query("SELECT firstname,lastname,contact_id FROM customer_contacts
		// 							WHERE contact_id='".$quote->f('contact_id')."' AND customer_id='".$quote->f('buyer_id')."'");
		$contact_name->next();
		$q_ref = $quote->f('buyer_reference');
		// console::log($q_ref);

		$cat ='0';
		if($quote->f('buyer_id')){
			//get buyer details
			$buyer_details = $this->db->query("SELECT customers.customer_id as buyer_id,customers.name, customers.our_reference, customers.cat_id, customer_addresses.*
				                    	FROM customers
				                    	LEFT JOIN customer_addresses ON customer_addresses.customer_id=customers.customer_id AND customer_addresses.is_primary=1
		                            	WHERE customers.customer_id='".$quote->f('buyer_id')."'");
			$q_ref = $buyer_details->f('our_reference');
			$cat = $buyer_details->f('cat_id') ? $buyer_details->f('cat_id') : $cat;
			// $output['main_address_id']			= $buyer_details->f('address_id');
		}
		// console::log($q_ref);
		$text = gm('Company');
		if(!$quote->f('buyer_id')){
			$text = gm('Name');
		}

		$quote_date =$quote->f('version_date') ? $quote->f('version_date')*1000 : time()*1000;
		$q_time = $quote->f('version_date') ? $quote->f('version_date') : time();
		if($in['duplicate_quote_id']){
			$quote_date=  time()*1000;
			$q_time = time();
		}


		$this->discount = $quote->f('discount');
		if($this->is_discount < 2){
			$this->discount = 0;
		}

		$ref = '';
		if(ACCOUNT_QUOTE_REF && $q_ref){
			$ref =  $q_ref;
			if($ref){
				$ref = $ref.ACCOUNT_QUOTE_DEL;
			}
		}

		$rel = ($quote->f('buyer_id') ? 'customer_addresses' : 'customer_contact_address').'.'.($quote->f('buyer_id') ? 'customer_id' : 'contact_id').'.'.($quote->f('buyer_id') ? $quote->f('buyer_id') : $quote->f('contact_id'));
		$this->currency_type = $quote->f('currency_type');
		$this->currency_rate = $quote->f('currency_rate');

		$in['discount'] = $in['discount'] ? $in['discount'] : $quote->f('discount');

		$free_field = $quote->f('buyer_address')."\n".$quote->f('buyer_zip').' '.$quote->f('buyer_city')."\n".get_country_name($quote->f('buyer_country_id'));

		
		$in['show_img_q'] = $quote->f('show_img_q');

		if($quote->f('pdf_layout') && $quote->f('use_custom_template')==0){
			$link_end='&type='.$quote->f('pdf_layout').'&logo='.$quote->f('pdf_logo');
		}elseif($quote->f('pdf_layout') && $quote->f('use_custom_template')==1){
			$link_end = '&custom_type='.$quote->f('pdf_layout').'&logo='.$quote->f('pdf_logo');
		}else{
			$link_end = '&type='.ACCOUNT_QUOTE_PDF_FORMAT;
		}
		if(defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $quote->f('pdf_layout') == 0){
			$link_end = '&custom_type='.ACCOUNT_QUOTE_PDF_FORMAT;
			// $in['use_custom'] = 1;
		}
		//console::log('f','apply_discount: '.$quote->f('apply_discount').' discount: '.$quote->f('discount').' discount_line_gen: '.$quote->f('discount_line_gen'));

		if($in['buyer_id']){
			if($in['save_final']==1){
				$final=true;
			}else{
				$final=false;
			}
		}
		// $default_note = $this->db->field("SELECT value FROM default_data WHERE type='quote_note' ");

		$in['cat_id'] = $cat;
		$version_code=$quote->f('version_code');
		if($quote->f('apply_discount')==0){
			$discount_line=0;
			$discount_global=0;
		}elseif($quote->f('apply_discount')==1){
			$discount_line=1;
			$discount_global=0;
		}elseif($quote->f('apply_discount')==2){
			$discount_line=0;
			$discount_global=1;
		}elseif($quote->f('apply_discount')==3){
			$discount_line=1;
			$discount_global=1;
		}


		// $output['notes']						= $default_note;
		$output['save_final']					= $final;

		$output['main_address_id']				= $quote->f('main_address_id');
		$output['default_currency']				= ACCOUNT_CURRENCY_TYPE;
		$output['default_currency_code']		= currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
		$output['default_currency_value']		= currency::get_currency(ACCOUNT_CURRENCY_TYPE);
		$output['cat_id']						= $cat;
		$output['pdf_layout']					= $quote->f('pdf_layout');
		$output['SHOW_PRODUCT_SAVE']         	= 0;
		$output['HIDE_VERSION']              	= $in['main_quote_id']?false:true;
		$output['VIEW_VERSION']              	= $in['main_quote_id']?true:false;
		$output['own_reference']           		= $quote->gf('own_reference');
		$output['CURRENT_VERSION']           	= $quote->f('version_code');
		$output['LAST_SERIAL_NUMBER']        	= '['.$quote->f('version_code').']';
		$output['version_id']                  	= $quote->f('version_id');
		$output['type']                      	= $quote->f('type');
		$output['seller_name']              	= $quote->f('seller_name');
		$output['quote_date']                	= $quote_date;
		$output['QUOTE_TS']						= $q_time;
		$output['discount']                  	= display_number($quote->f('discount'));
		$output['vat']                      	= $vat;
		// 'NOTES'                     	=> utf8_decode($in['notes']),
		$output['CURRENCY_TYPE_LIST']        	= build_currency_list($quote->f('currency_type'));
		$output['currency_txt']		        	= currency::get_currency($quote->f('currency_type'));
	  	// $output['APPLY_DISCOUNT_LIST']      	= build_apply_discount_list($quote->f('apply_discount') );
	  	$output['apply_discount']      			= $quote->gf('apply_discount');
	  	$output['apply_discount_line']      	= $discount_line;
		$output['apply_discount_global']      	= $discount_global;

	  	// $output['APPLY_DISCOUNT_LIST_TXT']		= build_apply_discount_list($quote->f('apply_discount'),true);
	  	$output['email_language']				= $quote->f('email_language');
		//Buyer Details
		$output['buyer_id']                  	= $quote->f('buyer_id');
		$output['buyer_name']                	= $quote->f('buyer_name');
		$output['contact_id']                	= $quote->f('contact_id');
		$output['contact_name']              	= $quote->f('contact_id') && !$quote->f('buyer_id')?$quote->f('buyer_name'): trim($contact_name->f('firstname').' '.$contact_name->f('lastname'));
		
		$output['is_contact']					= $quote->f('contact_id') ? true : false;
		$output['buyer_reference']           	= $q_ref;
		$output['buyer_country_id']          	= $quote->f('buyer_country_id');
		$output['BUYER_COUNTRY_NAME']        	= get_country_name($quote->f('buyer_country_id'));
		$output['buyer_state_id']            	= $quote->f('buyer_state_id');
		$output['BUYER_STATE_NAME']          	= get_state_name($quote->f('buyer_state_id'));
		$output['buyer_city']                	= $quote->f('buyer_city');
		$output['buyer_zip']                 	= $quote->f('buyer_zip');
		$output['buyer_address']             	= $quote->f('buyer_address');
		$output['NAME_TEXT']					= $text;
		$output['currency_type']				= $quote->f('currency_type');
	  	$output['delivery_address']     		= $quote->f('delivery_address');
	  	$output['delivery_address_txt']     	= nl2br($quote->f('delivery_address'));
	  	$output['delivery_address_id']			= $quote->f('delivery_address_id');
	  	$output['view_delivery']				= $quote->f('delivery_address') ? true : false; 
		$output['VIEW_DELIVERY']             	= $quote->f('delivery_address')?'':'hide';
		$output['country_dd']					= build_country_list($quote->f('buyer_country_id'));
		//Seller Delivery Details

		// $output['SELLER_NAME']               	= $quote->gf('seller_name');
		$output['seller_d_address']    			= $quote->gf('seller_d_address');
		$output['seller_d_zip']   		  		= $quote->gf('seller_d_zip');
		$output['seller_d_city']   		  	  	= $quote->gf('seller_d_city');
		// $output['SELLER_D_COUNTRY_DD']	  	  	= build_country_list($quote->gf('seller_d_country_id'));
		// 'SELLER_D_STATE_DD'			   	 	=> build_state_list($quote->gf('seller_d_state_id'),$quote->gf('seller_d_country_id')),
		$output['free_field']					= $in['change'] && $in['buyer_id'] ?  $free_field : ( $quote->f('free_field') ? $quote->f('free_field') : $free_field);
		$output['free_field_txt']				= $in['change'] && $in['buyer_id'] ?  nl2br($free_field) : ( $quote->f('free_field') ? nl2br($quote->f('free_field')) : nl2br($free_field));
		//Seller Billing Details

		$output['seller_b_address']  	  		= $quote->gf('seller_b_address');
		$output['seller_b_zip']   				= $quote->gf('seller_b_zip');
		$output['seller_b_city']   			    = $quote->gf('seller_b_city');
		// $output['SELLER_B_COUNTRY_DD']		    = build_country_list($quote->gf('seller_b_country_id'));
		// 'SELLER_B_STATE_DD'			    	=> build_state_list($quote->gf('seller_b_state_id'),$quote->gf('seller_b_country_id')),
		$output['seller_bwt_nr']   			    = $quote->gf('seller_bwt_nr');
		$output['PICK_DATE_FORMAT']				= pick_date_format();
		$output['STAGE_DD']                     = build_data_dd('tblquote_stage',$quote->gf('stage_id'),'name',array('cond'=> 'ORDER BY sort_order','id' =>'id'));
		$output['SOURCE_DD']                    = build_data_dd('tblquote_source',$quote->gf('source_id'),'name',array('cond'=> 'ORDER BY sort_order','id' =>'id'));
		$output['TYPE_DD']                	  	= build_data_dd('tblquote_type',$quote->gf('t_id'),'name',array('cond'=> 'ORDER BY sort_order','id' =>'id'));
		$output['REL']							= $rel.".".$in['quote_id'];
		$output['currency_rate']				= $quote->f('currency_rate');
		$output['author_id']					= $in['duplicate_quote_id'] ? $_SESSION['u_id'] : $quote->gf('author_id');
		$output['acc_manager_id']				= $quote->gf('acc_manager_id');
		$output['acc_manager_name']				= $quote->gf('acc_manager_name');
		$output['ACC_MANAGER']					= $in['duplicate_quote_id'] ? get_user_name( $_SESSION['u_id']) :($quote->gf('author_id') ? get_user_name($quote->gf('author_id')) : '');
		$output['show_vat']						= $in['show_vat'] == 1 ? true : false;
		$output['checked_no']					= $in['show_vat'] == 1 ? '' : 'CHECKED';
		$output['show_grand']					= $in['show_grand'] == 1 ? true : false;
		// $output['show_vat_txt']					= $in['show_vat'] == 1 ? gm('Yes') : gm('No');
		$output['hide_grand_t']					= $in['show_grand'] == 1 ? '' : 'hide';
		$output['show_vat_pdf']					= $quote->f('show_vat_pdf') == 1 ? true : false;
		$output['show_vat_inp']					= $in['show_grand'] == 1 ? '' : 'hide';
		$output['hide_vat_input']				= $in['show_vat'] == 1 ? '' : 'hide';
		$output['hide_contact_name']			= !$quote->f('buyer_id') ? 'hide' : '';
		$output['hide_global_discount']			= $this->is_discount < 2 ? false : true;
		$output['hide_line_global_discount']	= $this->is_discount == 1 ? '' : 'hide';
		$output['not_template']					= true;
		// $output['free_text_content']			= $free_text_content;
		$output['general_conditions_new_page']	= $in['general_conditions_new_page'] == 1 ? true : false;
		$output['subject']						= $quote->gf('subject');
		$output['field']                        = $quote->f('buyer_id')?'customer_id':'contact_id';
		$output['show_img_q']					= $quote->f('show_img_q') == 1 ? true : false;
		// $output['checked_show_img_q']			= $quote->f('show_img_q') == 1 ? 'CHECKED' : '';
		$output['save_txt']						= gm('Saved');
		$output['PDF_LINK']          			= 'index.php?do=quote-quote_print&id='.$this->quote_id.'&version_id='.$quote->f('version_id').'&lid='.$quote->f('email_language').$link_end;
		$output['begin_pdf_link']          		= 'index.php?do=quote-quote_print&id='.$this->quote_id.'&version_id='.$quote->f('version_id').'&lid=';
		$output['end_pdf_link']					= $link_end;
		$output['downpayment']					= $quote->f('downpayment_val') ? display_number($quote->f('downpayment_val')) : display_number(0);
		$output['main_country_id']				= @constant("ACCOUNT_BILLING_COUNTRY_ID") ? (string)@constant("ACCOUNT_BILLING_COUNTRY_ID") : '26';
		$output['hide_currency2'] 				= ACCOUNT_CURRENCY_FORMAT == 0 ? true : false;
		$output['hide_currency1'] 				= ACCOUNT_CURRENCY_FORMAT == 1 ? true : false;
		$output['show_QC']						= $quote->gf('show_QC') == 1 ? true : false;
		$output['show_UP']						= $quote->gf('show_UP') == 1 ? true : false;
		$output['show_V']						= $quote->gf('show_V') == 1 ? true : false;
		$output['show_PS']						= $quote->gf('show_PS') == 1 ? true : false;
		$output['show_d']						= $quote->gf('show_d') == 1 ? true : false;

		// ));
		$show_img_q = $quote->f('show_img_q');
	    $output['discount_line_gen']				= display_number($quote->gf('discount_line_gen'));

		// $in['validity_d'] = $quote->gf('validity_date') ? date(ACCOUNT_DATE_FORMAT,$quote->gf('validity_date')) : '';
		$output['validity_date'] = $quote->gf('validity_date') ? $quote->gf('validity_date')*1000 : '';
		$transl_lang_id_active = $quote->f('email_language');
		$language_id = $quote->f('email_language');
		$this->version_id = $quote->f('version_id');
		if($in['duplicate_quote_id']){
			$in['validity_d'] = '';
			$in['validity_date'] = '';
			$output['validity_date'] = '';
			// $view->assign(array(
			$output['serial_number']				= generate_quote_number(DATABASE_NAME);
			$output['REF_SERIAL_NUMBER']			= $ref.generate_quote_number(DATABASE_NAME);
			$output['REF']							= $ref ? " var ref= '".$ref."';":' var ref;';
			// ));
		}else{
			// $view->assign(array(
			$output['REF_SERIAL_NUMBER']			= $ref.$quote->f('serial_number');
			$output['serial_number']				= $quote->f('serial_number');
			$output['REF']							= $ref ? " var ref= '".$ref."';":' var ref;';
			// ));
		}
		
		$lines = $this->getQuoteLines($in);
		
		$output['sort_order']= $lines['sort_order'];
		$output['quote_group'] = $lines['quote_group'];
		$output['vat_lines'] = $lines['vat_lines'];
		
		if($this->is_discount == 1 || $this->is_discount == 3){
			$output['VIEW_DISCOUNT']= true;
		}else{
			$output['VIEW_DISCOUNT']= false;
		}

		if(isset($in['template_id']) && is_numeric($in['template_id']) ){
			$output['template_name'] = $quote->f('serial_number');
		}

		// $view->assign(array(
		$output['VAT_TOTAL']					= display_number($this->total_vat);
		$output['GRAND_TOTAL'] 					= display_number($this->grand_total_vat);
		$output['total_wo_vat'] 				= display_number($this->grand_total);
		$output['TOTAL_DEFAULT_CURRENCY']		= display_number($this->default_total);
		$output['TOTAL_WITHOUT']				= display_number($this->grand_total);
		// $output['show_vat']						= $in['show_vat'] ? ' ' : 'hide';
		$output['TOTAL_DISCOUNT']				= display_number(-$this->grand_total * $this->discount / 100);
		// ));
		if($customer_id){
			$in['no_buyer_vat'] = $this->db->field("SELECT no_vat FROM customers WHERE customer_id='".$customer_id."' ");
		}

		$in['buyer_id'] = $quote->f('buyer_id');
		$in['contact_id'] = $quote->f('contact_id');
		$in['delivery_address_id'] = $quote->f('delivery_address_id');
		$in['lang_id'] = $quote->f('email_language');
		$notes = $this->get_notes($in);
		$output['notes'] 			= $notes['notes'];
		$output['free_text_content']= $notes['free_text_content'];
		/*		$notes = $this->get_notes($in);
		
		$output['translate_loop']					= $notes['translate_loop'];
		$output['translate_loop_custom'] 			= $notes['translate_loop_custom'];
		$output['lang_cls'] 						= $notes['lang_cls'];*/

		$output['authors']							= $this->get_author($in);
		$output['accmanager']						= $this->get_accmanager($in);
		$output['contacts']							= $this->get_contacts($in);
		$output['addresses']						= $this->get_addresses($in);
		$output['cc']								= $this->get_cc($in);
		$output['articles_list']					= $this->get_articles_list($in);
		$output['language_dd'] 						= build_language_dd_new($in['email_language']);
		$output['APPLY_DISCOUNT_LIST']   			= build_apply_discount_list($in['apply_discount']);
		$output['sameAddress']						= $quote->f('delivery_address') ? false : true;
		$output['quote_id']							= $in['quote_id'];
		$output['duplicate_quote_id']				= $in['duplicate_quote_id'];
		$output['version_id']						= $this->version_id;
		if($this->hide_total==1){
			$hide_tot=true;
		}else{
			$hide_tot=true;
		}
		$output['hide_total'] = $hide_tot;
		$this->out = $output;

	}

	private function getQuoteEditLines(&$in)
	{
		$groups = $this->db->query("SELECT * FROM tblquote_group WHERE quote_id = '".$this->quote_id."' AND version_id = '".$this->quote->f('version_id')."' ORDER BY sort_order ASC");
		$in['quote_group'] = array();//clear error data
		$in['quote_group_order'] = array();
		$hide_total = 0;
		$this->grand_total = 0;
		$this->total_vat = 0;
		$purchase_price=0;
		while ($groups->next()) {
			$group_code = unique_id();
			$group_content = '';
			$groupI=0;
			$quoteLine = $this->db->query("SELECT * FROM tblquote_line WHERE quote_id='".$this->quote_id."' AND group_id = '".$groups->f('group_id')."' ORDER BY sort_order,line_order ASC");
			while($quoteLine->next()){

				$purchase_price1 = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$quoteLine->f('article_id')."' AND pim_article_prices.base_price='1'");
					$purchase_price=$quoteLine->f('purchase_price');

				if(!isset($in['quote_group'][$group_code])){
					console::log('here');
					$in['quote_group'][$group_code] = array(
					    'title' => $groups->f('title'),
					    'show_chapter_total'=>$groups->f('show_chapter_total'),
					    'pagebreak_ch' => $groups->f('pagebreak_ch'),
					    'show_QC' => $groups->f('show_QC'),
						'show_UP' => $groups->f('show_UP'),
						'show_PS' => $groups->f('show_PS'),
						'show_d' => $groups->f('show_d'));
				}
				if($quoteLine->f('content_type') == 1 ){
					if(!isset($in['quote_group'][$group_code]['table'])){
						$in['quote_group'][$group_code]['table'] = array(
						$quoteLine->f('table_id') => array(
						'description' => array($quoteLine->f('name')),
	          			'article_code' => array($quoteLine->f('article_code')),
	          			'article_id' => array($quoteLine->f('article_id')),
	          			'is_tax' => array($quoteLine->f('is_tax')),
	          			// 'for_article' => array( $quoteLine->f('is_tax') == 1 ? $this->db->field("SELECT article_id FROM  `pim_articles_taxes`  WHERE tax_id='".$quoteLine->f('is_tax')."'") : '' ),
						'quantity' => array($quoteLine->f('quantity')),
	          			'line_discount' => array($quoteLine->f('line_discount')),
	          			'line_vat' => array($quoteLine->f('vat')),
						'sale_unit' => array($quoteLine->f('sale_unit')),
						'package' => array($quoteLine->f('package')),
						'price' => array($quoteLine->f('price')),
						'vat_val' => array($quoteLine->f('vat')),
						'purchase_price' => array($purchase_price),
						'line_type' => array($quoteLine->f('line_type')),
						'show_block_total' => array($quoteLine->f('show_block_total')),
						'hide_QC' => array($quoteLine->f('hide_QC')),
						'hide_AC' => array($quoteLine->f('hide_AC')),
						'hide_UP' => array($quoteLine->f('hide_UP')),
						'hide_DISC' => array($quoteLine->f('hide_DISC')),
						'show_block_vat' => array($quoteLine->f('show_block_vat')),
						'service_bloc_title' => array($quoteLine->f('service_bloc_title'))						
						));
					}else{
						//if it's set we check for the table if it's set
						if(!isset($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')])){
							$in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')] = array(
							'description' => array($quoteLine->f('name')),
	            			'article_code' => array($quoteLine->f('article_code')),
	            			'article_id' => array($quoteLine->f('article_id')),
	            			'is_tax' => array($quoteLine->f('is_tax')),
	            			// 'for_article' => array( $quoteLine->f('is_tax') == 1 ? $this->db->field("SELECT article_id FROM  `pim_articles_taxes`  WHERE tax_id='".$quoteLine->f('is_tax')."'") : '' ),
							'quantity' => array($quoteLine->f('quantity')),
	            			'line_discount' => array($quoteLine->f('line_discount')),
	            			'line_vat' => array($quoteLine->f('vat')),
							'sale_unit' => array($quoteLine->f('sale_unit')),
							'package' => array($quoteLine->f('package')),
							'price' => array($quoteLine->f('price')),
							'vat_val' => array($quoteLine->f('vat')),
							'purchase_price' => array($purchase_price),
							'line_type' => array($quoteLine->f('line_type')),
							'show_block_total' => array($quoteLine->f('show_block_total')),
							'hide_QC' => array($quoteLine->f('hide_QC')),
							'hide_AC' => array($quoteLine->f('hide_AC')),
							'hide_UP' => array($quoteLine->f('hide_UP')),
							'hide_DISC' => array($quoteLine->f('hide_DISC')),
							'show_block_vat' => array($quoteLine->f('show_block_vat')),
							'service_bloc_title' => array($quoteLine->f('service_bloc_title'))
							);
						}else{
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['description'],$quoteLine->f('name'));
	            			array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['article_code'],$quoteLine->f('article_code'));
	            			array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['article_id'],$quoteLine->f('article_id'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['quantity'],$quoteLine->f('quantity'));
	            			array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['line_discount'],$quoteLine->f('line_discount'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['line_vat'],$quoteLine->f('vat'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['sale_unit'],$quoteLine->f('sale_unit'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['package'],$quoteLine->f('package'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['price'],$quoteLine->f('price'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['vat_val'],$quoteLine->f('vat'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['purchase_price'],$purchase_price);
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['line_type'],$quoteLine->f('line_type'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['show_block_total'],$quoteLine->f('show_block_total'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['hide_AC'],$quoteLine->f('hide_AC'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['hide_QC'],$quoteLine->f('hide_QC'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['hide_UP'],$quoteLine->f('hide_UP'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['show_block_vat'],$quoteLine->f('show_block_vat'));
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['is_tax'],$quoteLine->f('is_tax'));
							// array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['for_article'], $quoteLine->f('is_tax') == 1 ? $this->db->field("SELECT article_id FROM  `pim_articles_taxes`  WHERE tax_id='".$quoteLine->f('is_tax')."'") : '' );
							array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['hide_DISC'],$quoteLine->f('hide_DISC'));

							// array_push($in['quote_group'][$group_code]['table'][$quoteLine->f('table_id')]['service_bloc_title'],$quoteLine->f('service_bloc_title'));
						}
					}
					$line_discount = $quoteLine->f('line_discount');
					if($this->is_discount == 0 || $this->is_discount == 2){
						$line_discount = 0;
					}
					$price = ($quoteLine->f('price')-($quoteLine->f('price')*$line_discount/100));
					if($this->is_discount > 1){
						$price = $price - $price*$this->discount/100;
					}
					if($quoteLine->f('show_block_total') == 0){
						$this->grand_total += ($quoteLine->f('price')-($quoteLine->f('price')*$quoteLine->f('line_discount')/100))*$quoteLine->f('quantity')*$quoteLine->f('package')/$quoteLine->f('sale_unit');
						// if($in['show_vat']){
							$this->total_vat += $price*($quoteLine->f('quantity')*$quoteLine->f('package')/$quoteLine->f('sale_unit'))* $quoteLine->f('vat')/100;
						// }
					}


				}

				if($quoteLine->f('content_type') == 2){
					if(!isset($in['quote_group'][$group_code]['content'])){
						$in['quote_group'][$group_code]['content'] = array($quoteLine->f('table_id') => $quoteLine->f('content'));
						$in['quote_group'][$group_code]['content_id'] = array($quoteLine->f('table_id') => $quoteLine->f('id'));
					}else{
						$in['quote_group'][$group_code]['content'][$quoteLine->f('table_id')] = $quoteLine->f('content');
						$in['quote_group'][$group_code]['content_id'][$quoteLine->f('table_id')] = $quoteLine->f('id');
					}
				}

				if($quoteLine->f('content_type') == 3){
					if(!isset($in['quote_group'][$group_code]['pagebreak'])){
						$in['quote_group'][$group_code]['pagebreak'] = array($quoteLine->f('table_id') => '');
					}else{
						$in['quote_group'][$group_code]['pagebreak'][$quoteLine->f('table_id')] = '';
					}
				}
				$in['quote_group_order'][$group_code][$quoteLine->f('table_id')] = $quoteLine->f('sort_order');
				$groupI++;
				$hide_total++;
			}
			$this->hide_total = $hide_total;
			// console::log($groupI);
			if($groupI==0){
				if(!isset($in['quote_group'][$group_code])){
					
					$in['quote_group'][$group_code] = array('title' => $groups->f('title'),'show_chapter_total'=>$groups->f('show_chapter_total'),'pagebreak_ch' => $groups->f('pagebreak_ch'),
					    'show_QC' => $groups->f('show_QC'),
						'show_UP' => $groups->f('show_UP'),
						'show_PS' => $groups->f('show_PS'),
						'show_d' => $groups->f('show_d'));
					// $default_group_id2 = unique_id();
					$default_table_id2 = unique_id();
					// $default_sort2 = array($default_group_id2 => array($default_table_id2 => 0));
					$in['quote_group_order'][$group_code][$default_table_id2] = 0;
				}
			}

		}		
	}

	private function getQuoteLines($in)
	{
		
		$this->getQuoteEditLines($in);
		list($default_group,$default_sort) = $this->setDefaultData();
		
		if(empty($in['quote_group'])){
			//empty array we need to init with something
			$in['quote_group'] = $default_group;
		}

		if(empty($in['quote_group_order'])){
			$in['quote_group_order'] = $default_sort;
		}
		$array = array('sort_order' => array(), 'quote_group' => array() );
		// console::log($in['quote_group_order'],$in['quote_group']);
		if($in['quote_group_order']){

			foreach ($in['quote_group_order'] as $group_id => $group_items) {
				$chapter_total = 0;
				$quote_group_line = array('table'=>array());
				foreach($group_items as $item => $sort_order){
					$quote_table_line = array('quote_line'=>array());
					//check to see if it's a table or content
					$content_type = 0;//don't know what it is yet
					$table = array(); $content = '';
					if(isset($in['quote_group'][$group_id]['table']) && isset($in['quote_group'][$group_id]['table'][$item])){
						$content_type = 1;//it's a table; congrats
						$table = $in['quote_group'][$group_id]['table'][$item];
					}

					if(isset($in['quote_group'][$group_id]['content']) && isset($in['quote_group'][$group_id]['content'][$item])){
						$content_type = 2;//it's content
						$content = $in['quote_group'][$group_id]['content'][$item];
						$content_id = $in['quote_group'][$group_id]['content_id'][$item];
					}

					if(isset($in['quote_group'][$group_id]['pagebreak']) && isset($in['quote_group'][$group_id]['pagebreak'][$item])){
						$content_type = 3;//page break; yeye
					}

					if($content_type == 0){
						//could not find out what it is so just ignore it;
						continue;
					}

					if(!empty($table) && $content_type == 1){
						$i = 0;
						$tableTotal = 0;
						$vat_line = 0;
						foreach($table['description'] as $index => $val){
							if(empty($val)){
								continue;
							}
							// $view->assign('QUOTE_GROUP_ID',$group_id,'quote_group');
							$quote_group_line['quote_group_id'] = $group_id;
							$image = '';
							$show_block_total = true;
							if($table['show_block_total'][$index] == 1){
								$show_block_total = false;
							}
							$show_block_vat = false;
							if($table['show_block_vat'][$index] == 1){
								$show_block_vat = true;
							}
							$hide_QC=false;
							$hide_AC=false;
							$hide_UP=false;
							$hide_DISC=false;
							if($table['hide_AC'][$index] == 1){
								$hide_AC = true;
							}
							if($table['hide_QC'][$index] == 1){
								$hide_QC = true;
							}
							if($table['hide_UP'][$index] == 1){
								$hide_UP = true;
							}
							if($table['hide_DISC'][$index] == 1){
								$hide_DISC = true;
							}
							// $delete_r ='';
							if(!$table['article_code'][$index])  { //is_service
		          				$service_bloc_title = $table['service_bloc_title'][0];
		          				// $delete_r ='delete_more_to_the_left';
		          				// $view->assign(array(
			    		    	$quote_table_line['ONLY_ARTICLE']			= 'hide';
			    		    	// $quote_table_line['item_txt']				= '<span class="bloc">'.gm('Service bloc title').'</span><input name="quote_group['.$group_id.'][table]['.$item.'][service_bloc_title]" value="'.$service_bloc_title.'" type="text" class="large" style="width:250px;margin:0px 0px 0px 10px"/>';
			    		    	$quote_table_line['item_text']				= $service_bloc_title;
			    		    	// $quote_table_line['add_a_line']   	 		= gm('Add line');
			    	    		// ),'quote_table');
		        			}else{
					            // $view->assign(array(
					            $quote_table_line['ONLY_ARTICLE']			= '';
					    //         $quote_table_line['item_txt']					= '<span class="bloc" style="vertical-align:middle;">'.gm('Article bloc').'</span><span class="question_mark atip show_tip_info" style="padding-top:0px;">
									// <a class="tooltip" title="Show tip"  href="#"></a>
									// <div class="tip_info">'.gm('The article content can be changed in the').' <a href="index.php?do=quote-quote_note&tabbq=9" style="text-decoration:underline;">'.gm('Configure Module').'</a> '.gm('section').'</div>
									// </span>';
								$quote_table_line['item_text']				= '';
					            // $quote_table_line['add_a_line']    		=  gm('Add an article');
					            $quote_table_line['hide_not_service']	= 'hide';
					            // ),'quote_table');
		            			$image_q = $this->db->query("SELECT pim_article_photo.file_name,pim_article_photo.name, pim_article_photo.upload_amazon FROM pim_article_photo
		            											INNER JOIN pim_articles ON pim_article_photo.parent_id=pim_articles.article_id
		             								WHERE pim_article_photo.parent_id='".$table['article_id'][$index]."' ORDER BY pim_article_photo.photo_id ASC LIMIT 1 ");
								if($image_q->next() && $show_img_q==1){
									if($image_q->f('upload_amazon')==1){
										$image ='<img src="'.$image_q->f('file_name').'" alt="'.$image_q->f('name').'" width="50" />';
									}else{
										$image ='<img src="'.$config['real_path'].'phpthumb/phpThumb.php?src=../upload/'.$database_config['mysql']['database'].'/pim_article_photo/'.$image_q->f('file_name').'&w=50&h=50'.'" alt="'.$image_q->f('name').'"  width="50"  />';
									}
								}
		          			}
				          	if(!$table['package'][$index]){
				              	$table['package'][$index]=1;
				          	}
		          			if(!$table['sale_unit'][$index]){
		              			$table['sale_unit'][$index]=1;
		          			}
		          			if(!$this->allowPacking){
		          				// $view->assign(array(
								$quote_table_line['USE_SALE_UNIT']= false;
								$quote_table_line['USE_PACKAGE']= false;
								// ),'quote_table');
		          			}else{
		          				$quote_table_line['USE_SALE_UNIT']= true;
								$quote_table_line['USE_PACKAGE']= true;
		          			}


		          			if(!$this->allowSaleUnit){
		          				// $view->assign(array('USE_SALE_UNIT'=> 'class="hide"'),'.quote_table');
		          				$quote_table_line['USE_SALE_UNIT']= false;
		          			}
		          			
		           			/*if($this->is_vat== 1){
		              			// $view->assign(array('VIEW_VAT'=>''),'quote_table');
		              			$quote_table_line['VIEW_VAT']= true;
		          			}else{
		              			// $view->assign(array('VIEW_VAT'=>'hide'),'quote_table');
		              			$quote_table_line['VIEW_VAT']= false;
		          			}*/
		          			$line_total=$table['quantity'][$index] * $table['price'][$index] * ($table['package'][$index]/ $table['sale_unit'][$index]);

				          	$quote_line_line = array(
								'tr_id'         			=> $item,
								'line_type'      			=> $table['line_type'][$index],
								'description'  				=> html_entity_decode($val),
								'quantity'      			=> display_number($table['quantity'][$index]),
								'article_code'      		=> $table['article_code'][$index],
								'article_id'      			=> $table['article_id'][$index],
								'is_tax'					=> $table['is_tax'][$index],
								'for_article'				=> $table['for_article'][$index],
						      	'line_discount'      		=> display_number($table['line_discount'][$index]),
						      	'line_vat'      			=> display_number($table['line_vat'][$index]),
						      	'sale_unit'      			=> $table['sale_unit'][$index],
								'package'      			  	=> remove_zero_decimals($table['package'][$index]),
								'price'         			=> display_number_var_dec($table['price'][$index]),
								'price_vat'					=> display_number($table['price'][$index] + (($table['price'][$index]*$table['line_vat'][$index])/100)),
								'line_total'          		=> display_number($line_total - ($line_total* $table['line_discount'][$index]/100)),
								'hide_currency2'			=> ACCOUNT_CURRENCY_FORMAT == 0 ? true : false,
								'hide_currency1'			=> ACCOUNT_CURRENCY_FORMAT == 1 ? true : false,
								'vat_val'					=> $table['vat_val'][$index],
								'purchase_price'			=> $table['purchase_price'][$index],
								'art_img'					=> $image,
								'show_block_total'			=> $table['show_block_total'][$index],
								'hide_QCv'					=> $table['hide_QC'][$index],
								'hide_ACv'					=> $table['hide_AC'][$index],
								'hide_UPv'					=> $table['hide_UP'][$index],
								'hide_DISCv'				=> $table['hide_DISC'][$index],
								'show_lVATv'				=> $table['show_block_vat'][$index],
								'colum'						=> $table['line_type'][$index] == 1 ? $in['colum'] : $in['colum']+2,
								// 'delete_r'						=> $delete_r,
								// 'width'								=> !$table['article_code'][$index] ? '370' : '313'
							);
							// ,'quote_line');
							$tableTotal +=$line_total - ($line_total* $table['line_discount'][$index]/100);
							if($this->is_discount > 1){
								$p = ($line_total - ($line_total* $table['line_discount'][$index]/100));
								$p = $p - $p * $this->discount / 100;
								$vat_line += $p * $table['vat_val'][$index]/100;
							}else{
								$vat_line += ($line_total - ($line_total* $table['line_discount'][$index]/100))*$table['vat_val'][$index]/100;
							}
							// $view->loop('quote_line','quote_table');
							array_push($quote_table_line['quote_line'], $quote_line_line);
							$i++;
						}
		        		// $view->assign('LINE_TYPE'  , $table['line_type'][$index],'quote_table');
						$quote_table_line['line_type']=$table['line_type'][$index];
		        		// $view->assign(array('GROUP_TYPE' => $table['article_code'][$index]?'1':'2'),'quote_table');
		        		$quote_table_line['group_type']= $table['article_code'][$index]?'1':'2';
						if($i > 0){
							// $view->assign(array(
							$quote_table_line['total'] 				= display_number($tableTotal);
							$quote_table_line['hide_currency2'] 	= ACCOUNT_CURRENCY_FORMAT == 0 ? '' : 'hide';
							$quote_table_line['hide_currency1'] 	= ACCOUNT_CURRENCY_FORMAT == 1 ? '' : 'hide';
							$quote_table_line['group_table_id'] 	= $item;
							$quote_table_line['total_vat']			= $vat_line;
							$quote_table_line['total_vat_span']		= display_number($vat_line);
							$quote_table_line['q_table']			= true;
							$quote_table_line['block_total_hide']	= $show_block_total ? '' : 'hide';
							$quote_table_line['checkedBAD']			= $show_block_total ? '' : 'CHECKED';
							$quote_table_line['checkedQC']			= $hide_QC ? 'CHECKED' : '';
							$quote_table_line['checkedAC']			= $hide_AC ? 'CHECKED' : '';
							$quote_table_line['checkedUPC']			= $hide_UP ? 'CHECKED' : '';
							$quote_table_line['checkedDISC']		= $hide_DISC ? 'CHECKED' : '';
							$quote_table_line['hide_QC']			= $hide_QC ? 'hide' : "";
							$quote_table_line['hide_AC']			= $hide_AC ? 'hide' : "";
							$quote_table_line['hide_UP']			= $hide_UP ? 'hide' : "";
							$quote_table_line['hide_DISC']			= $hide_DISC ? 'hide' : "";
							$quote_table_line['show_lVAT']			= $show_block_vat && $show_block_total ? '' : 'hide';
							$quote_table_line['checkedVAT']			= $show_block_vat ? 'CHECKED' : '';
							$quote_table_line['sort_order']			= $sort_order;
							// ),'quote_table');
							// $view->loop('quote_table','quote_group');
							array_push($quote_group_line['table'], $quote_table_line);
						}
						if($show_block_total === true){
							$chapter_total += $tableTotal;
						}
					}
					if(!empty($content) && $content_type == 2){
						// $view->assign(array(
							$quote_table_line['quote_group_id']		= $group_id;
							$quote_table_line['group_content_id']	= $item;
							$quote_table_line['quote_content']		= $content;
							$quote_table_line['content_id']			= $content_id;
							$quote_table_line['q_content']			= true;
							$quote_table_line['sort_order']			= $sort_order;
						// ),'quote_table');
						if(!$use){
		         			// $view->assign(array(
								// $quote_table_line['USE_SALE_UNIT']	= 'class="hide"';
								// $quote_table_line['USE_PACKAGE']	= 'class="hide"';
							// ),'quote_table');
		        		}
				        if(!$use2){
				         	// $view->assign(array('USE_SALE_UNIT'=> 'class="hide"'),'.quote_table');
				         	// $quote_table_line['USE_SALE_UNIT'] = 'class="hide"';
				        }
						// $view->loop('quote_table','quote_group');
						array_push($quote_group_line['table'], $quote_table_line);
					}

					if($content_type == 3){
						// $view->assign(array(
							$quote_table_line['quote_group_id']		= $group_id;
							$quote_table_line['group_break_id']		= $item;
							$quote_table_line['pagebreak']			= true;
							$quote_table_line['sort_order']			= $sort_order;
						// ),'quote_table');
						if(!$use){
		          			// $view->assign(array(
								// $quote_table_line['USE_SALE_UNIT']	= 'class="hide"';
								// $quote_table_line['USE_PACKAGE']	= 'class="hide"';
							// ),'quote_table');
		        		}
				        if(!$use2){
				         	// $view->assign(array('USE_SALE_UNIT'=> 'class="hide"'),'.quote_table');
				         	// $quote_table_line['USE_SALE_UNIT']		= 'class="hide"';
				        }
						// $view->loop('quote_table','quote_group');
						array_push($quote_group_line['table'], $quote_table_line);
					}

					array_push($array['sort_order'], array(
						'group_id' => $group_id,
						'item_id' => $item,
						'order' => $sort_order
					));
					// ,'sort_order');
					// $view->loop('sort_order');

				}

				$quote_group_line['chapter_total'] = display_number($chapter_total);
				$quote_group_line['group_id'] = $group_id;

				$quote_group_line['title'] = htmlspecialchars($in['quote_group'][$group_id]['title']);
				$quote_group_line['show_QC'] = $in['quote_group'][$group_id]['show_QC'] == '1' ? true : false;
				$quote_group_line['show_UP'] = $in['quote_group'][$group_id]['show_UP'] == '1' ? true : false;
				$quote_group_line['show_PS'] = $in['quote_group'][$group_id]['show_PS'] == '1' ? true : false;
				$quote_group_line['show_d'] = $in['quote_group'][$group_id]['show_d'] == '1' ? true : false;
				$quote_group_line['quote_group_pb'] = $in['quote_group'][$group_id]['pagebreak_ch'];

				$show_chapter_total = '0';
				$show_chapter_subtotal = 'hide';

				// console::log($in['quote_group'][$group_id]);
				if($in['quote_group'][$group_id]['show_chapter_total']==1){
					$show_chapter_total = '1';
					$show_chapter_subtotal = '';
				}

				$quote_group_line['show_chapter_total']=$show_chapter_total == 1 ? true : false;
				$quote_group_line['show_chapter_subtotal']=$show_chapter_subtotal;
				$show_chapter_pb = $in['quote_group'][$group_id]['pagebreak_ch'] == 1 ? '' :'hide';
				$quote_group_line['show_chapter_pb']=$show_chapter_pb;
				$quote_group_line['active'] = count($array['quote_group']) == 0 ? true : false;
				// $quote_group_line['active'] = true;

				// $view->loop('quote_group');
				array_push($array['quote_group'], $quote_group_line);
				
			}
		}

		$this->grand_total = round($this->grand_total,2);
		$this->total_vat = round($this->total_vat,2);
		$this->grand_total_vat = ($this->grand_total - $this->grand_total * $this->discount / 100 ) + $this->total_vat;
		if(($this->currency_type != ACCOUNT_CURRENCY_TYPE) && $this->currency_rate){
			$this->default_total = return_value($this->currency_rate)*$this->grand_total_vat;
		}

		$array['vat_lines'] = $this->get_vatLines($array['quote_group']);


		// $array['VAT_TOTAL']					= display_number($total_vat);
		// $array['GRAND_TOTAL'] 				= display_number($grand_total_vat);
		// $array['TOTAL_DEFAULT_CURRENCY']	= display_number($default_total);
		// $array['TOTAL_WITHOUT']				= display_number($grand_total);		
		// $array['TOTAL_DISCOUNT']			= display_number(-$grand_total * $this->discount / 100);
		return $array;
	}

	/**
	 * @return array=>
	 * '21' => array('vat-value'=>'10','subtotal'=>'100')
	 * '6' => array('vat-value'=>'6','subtotal'=>'6')
	*/
	private function get_vatLines($array){
		// console::log($array);
		$result = array();
		foreach ($array as $key => $value) { // chapter
			foreach ($value['table'] as $tkey => $tvalue) { // table
				foreach ($tvalue['quote_line'] as $lkey => $lvalue) { // line
					//console::log($lvalue['vat_val']);
					if(!$result[$lvalue['vat_val']]){
						$result[$lvalue['vat_val']] = array('vat_value'=>0,'subtotal'=>0);
					}
					$result[$lvalue['vat_val']]['vat_value']+=($lvalue['price']*$lvalue['vat_val']/100);
					$result[$lvalue['vat_val']]['subtotal']+=$lvalue['price'];
					$result[$lvalue['vat_val']]['vat_percent']=display_number($lvalue['vat_val']);
					$result[$lvalue['vat_val']]['vat_value']=display_number($result[$lvalue['vat_val']]['vat_value']);
					$result[$lvalue['vat_val']]['subtotal']=display_number($result[$lvalue['vat_val']]['subtotal']);
				}
			}
		}

		return $result;
	}

	private function setDefaultData()
	{

		$default_group_id = unique_id();
		$default_table_id = unique_id();

		$default_group = array(
			$default_group_id => array(
				'title' => '',
				'show_QC' => true,
				'show_UP' => true,
				'show_PS' => true,
				'show_d' => true,
				'table' => array(
					$default_table_id => array(
						'description' => array(''),
						'quantity' => array(1),
		                'article_code' => array(''),
		                'article_id' => array(''),
						'package' => array(1),
						'sale_unit' => array(1),
						'price' => array(0)
					)
				)
			)
		);

		$default_sort = array($default_group_id => array($default_table_id => 0));

		return array($default_group, $default_sort);
	}

	public function getQuoteIdentity($identity_id)
	{
		$array = array(			
				'name'		=> ACCOUNT_COMPANY,
				'address'	=> nl2br(ACCOUNT_DELIVERY_ADDRESS),
				'zip'		=> ACCOUNT_DELIVERY_ZIP,
				'city'		=> ACCOUNT_DELIVERY_CITY,
				'country'	=> get_country_name(ACCOUNT_DELIVERY_COUNTRY_ID),
				'phone'		=> ACCOUNT_PHONE,
				'fax'		=> ACCOUNT_FAX,
				'email'		=> ACCOUNT_EMAIL,
				'url'		=> ACCOUNT_URL,
				'logo'		=> '../'.ACCOUNT_LOGO_QUOTE,			
		);
		if($identity_id != '0'){
			$mm = $this->db->query("SELECT * FROM multiple_identity WHERE identity_id='".$identity_id."'")->getAll();
			$value = $mm[0];
			
			$array=array(
				'name'		=> $value['identity_name'],
				'address'	=> nl2br($value['company_address']),
				'zip'		=> $value['company_zip'],
				'city'		=> $value['city_name'],
				'country'	=> get_country_name($value['country_id']),
				'phone'		=> $value['company_phone'],
				'fax'		=> $value['company_fax'],
				'email'		=> $value['company_email'],
				'url'		=> $value['company_url'],
				'logo'		=> '../pim/admin/'.$value['company_logo'],
			);
			
		}
		return $array;
	}

	public function get_author($in)
	{
		$q = strtolower($in["term"]);

		$filter = '';

		if($q){
		  $filter .=" AND users.first_name LIKE '".$q."%'";
		}
		$db_users = $this->db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role
										FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
										WHERE  database_name='".DATABASE_NAME."' AND users.active='1' $filter  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ");

		$items = array();
		while($db_users->next()){
			array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>htmlspecialchars($db_users->f('first_name').' '.$db_users->f('last_name')) ) );
		}
		return $items;
	}

	public function get_accmanager($in)
	{
		$q = strtolower($in["term"]);		

		$filter = '';

		if($q){
		  $filter .=" AND users.first_name LIKE '".$q."%'";
		}

		$db_users = $this->db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type
											FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'
											WHERE database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1' $filter AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY first_name ");

		$items = array();
		while($db_users->next()){
			array_push( $items, array( 'id'=>$db_users->f('user_id'),'value'=>htmlspecialchars($db_users->f('first_name').' '.$db_users->f('last_name')) ) );
		}
		return $items;
	}

	public function get_contacts($in)
	{
		$q = strtolower($in["term"]);
		$filter = " ";
		if($q){
			$filter .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
		}

		if($in['current_id']){
			$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
		}

		if($in['buyer_id']){
			$filter .= " AND customers.customer_id='".$in['buyer_id']."'";
		}
		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		$items = array();

		$title = array();
		$titles = $this->db->query("SELECT * FROM customer_contact_title ")->getAll();
		foreach ($titles as $key => $value) {
			$title[$value['id']] = $value['name'];
		}

		$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id,customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id 
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE 1=1 $filter ORDER BY lastname limit 5")->getAll();
		$result = array();
		foreach ($contacts as $key => $value) {
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		    $price_category = "1";
		    if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}

			$result[]=array(
				"symbol"				=> '',
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"email"					=> $value['email'],
				"price_category_id"		=> $price_category, 
				'customer_id'	 		=> $value['customer_id'], 
				'c_name' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				'currency_id' 			=> $value['currency_id'], 
				"lang_id" 				=> $value['internal_language'], 
				'contact_name' 			=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['firstname'].' '.$value['lastname']), 
				'country' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				'right' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['country_name']), 
				'title' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']), 
				'bottom' 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['position_n']),
			);

		}

		$added = false;
		if(count($result)==5){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
			$added = true;
		}
		if( $in['contact_id']){
			$cust = $this->db->query("SELECT customer_contacts.contact_id,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id,customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email,customer_contactsIds.title,country.name AS country_name, customer_contacts.position_n
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id 
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
					LEFT JOIN country ON country.country_id=customer_contact_address.country_id
					WHERE 1=1 AND customer_contacts.contact_id='".$in['contact_id']."' ORDER BY lastname ")->getAll();
			$value = $cust[0];
			$contact_title = $title[$value['title']];
	    	if($contact_title){
				$contact_title .= " ";
	    	}		

		    $name = $contact_title.$value['firstname'].' '.$value['lastname'];
		  	$price_category = "1";
			if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}
		  	
			$result[]=array(
				"symbol"				=> '',
				"id"					=> $value['contact_id'],
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				"top" 					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
				'email'					=> $value['email'],
				"price_category_id"		=> $price_category_id, 
				"customer_id" 			=> $value['customer_id'],
				'c_name'				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$value['name']), 
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id"	 		=> $value['identity_id'],
				'contact_name'			=> $value['firstname'].' '.$value['lastname'],
				'country'				=> $value['country_name'],
				'right'					=> $value['country_name'],
				'title'					=> $value['position_n'],
				'bottom'				=> $value['position_n'],
			);
		}
		if(!$added){
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999'));
			}
		}
		
		return $result;
	}

	public function get_addresses($in)
	{
		$q = strtolower($in["term"]);
		if($q){
			$filter .=" AND (address LIKE '%".addslashes($q)."%' OR zip LIKE '%".addslashes($q)."%' OR city LIKE '%".addslashes($q)."%' or country.name LIKE '%".addslashes($q)."%' ) ";
		}
		
		// array_push($result,array('id'=>'99999999999','value'=>''));
		if( $in['buyer_id']){
			$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.customer_id='".$in['buyer_id']."' $filter
									 ORDER BY customer_addresses.address_id limit 5");
		}
		else if( $in['contact_id'] ){
		   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
								 FROM customer_contact_address 
								 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
								 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  $filter
								 AND customer_contact_address.delivery='1' limit 5");
		}

		// $max_rows=$db->records_count();
		// $db->move_to($offset*$l_r);
		// $j=0;
		$addresses=array();
		if($address){
			while($address->next()){
			  	$a = array(
			  		'symbol'				=> '',
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> ($address->f('address')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
			  	);
				array_push($addresses, $a);
			}
		}
		$added = false;
		if(count($addresses)==5){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));				
			}

			$added = true;
		}
		if( $in['delivery_address_id'] ){
			if($in['buyer_id']){
				$address= $this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
										 FROM customer_addresses
										 LEFT JOIN country ON country.country_id=customer_addresses.country_id
										 LEFT JOIN state ON state.state_id=customer_addresses.state_id
										 WHERE customer_addresses.customer_id='".$in['buyer_id']."' AND customer_addresses.address_id='". $in['delivery_address_id']."'
										 ORDER BY customer_addresses.address_id ");
			}
			else if($in['contact_id']){
			   $address= $this->db->query("SELECT customer_contact_address.*,country.name AS country 
									 FROM customer_contact_address 
									 LEFT JOIN country ON country.country_id=customer_contact_address.country_id 
									 WHERE customer_contact_address.contact_id='".$in['contact_id']."'  AND customer_contact_address.address_id='". $in['delivery_address_id']."'
									 AND customer_contact_address.delivery='1' ");
			}
			$a = array(
					'symbol'				=> '',
				  	'address_id'	    => $address->f('address_id'),
				  	'id'			    => $address->f('address_id'),
				  	'address'			=> ($address->f('address')),
				  	'top'				=> ($address->f('address')),
				  	'zip'			    => $address->f('zip'),
				  	'city'			    => $address->f('city'),
				  	'state'			    => $address->f('state'),
				  	'country'			=> $address->f('country'),
				  	'right'				=> $address->f('country'),
				  	'bottom'			=> $address->f('zip').' '.$address->f('city'),
			  	);
				array_push($addresses, $a);
		}
		if(!$added){
			if($q){
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999','address'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($addresses,array('address_id'=>'99999999999','id'=>'99999999999'));
			}
		}
		return $addresses;

	}

	public function get_cc($in)
	{
		$q = strtolower($in["term"]);
				
		$filter =" is_admin='0' AND customers.active=1 ";
		// $filter_contact = ' 1=1 ';
		if($q){
			$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
			// $filter_contact .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
		if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
			$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}
		
		/*UNION 
			SELECT customer_contacts.customer_id, CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) AS name, customer_contacts.contact_id, position_n as acc_manager_name, country.name AS country_name, customer_contact_address.zip AS zip_name, customer_contact_address.city AS city_name
			FROM customer_contacts
			LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id AND customer_contact_address.is_primary=1
			LEFT JOIN country ON country.country_id=customer_contact_address.country_id
			WHERE $filter_contact*/

		$cust = $this->db->query("SELECT customers.customer_id as cust_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip,city,address,type
			FROM customers
			INNER JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			WHERE $filter AND is_primary=1			
			ORDER BY name
			LIMIT 5")->getAll();

		$result = array();
		foreach ($cust as $key => $value) {
			$cname = trim($value['name']);
			if($value['type']==0){
				$symbol = '<i class="fa fa-building" aria-hidden="true" uib-tooltip="Company" ></i>';
			}elseif($value['type']==1){
				$symbol = '<i class="fa fa-users" aria-hidden="true" uib-tooltip="Individual" ></i>';
			}else{
				$symbol ='';
			}


			$result[]=array(
				"id"					=> $value['cust_id'].'-'.$value['contact_id'],
				'symbol'				=> $symbol,
				"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
				"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
				"ref" 					=> $value['our_reference'],
				"currency_id"			=> $value['currency_id'],
				"lang_id" 				=> $value['internal_language'],
				"identity_id" 			=> $value['identity_id'],
				'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
				'country'				=> $value['country_name'] ? $value['country_name'] : '',
				'zip'					=> $value['zip'] ? $value['zip'] : '',
				'city'					=> $value['city'] ? $value['city'] : '',
				"bottom"				=> $value['address'].' '.$value['zip'].' '.$value['city'].' '.$value['country_name'],
				"right"					=> $value['acc_manager_name']
			);
		}
		if($q){
			array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
		}else{
			array_push($result,array('id'=>'99999999999','value'=>''));
		}
		
		return $result;
	}

	public function get_articles_list($in)
	{
		$def_lang = DEFAULT_LANG_ID;
		if($in['lang_id']){
			$def_lang= $in['lang_id'];
		}
		//if custom language is selected we show default lang data
		if($def_lang>4){
			$def_lang = DEFAULT_LANG_ID;
		}
		
		switch ($def_lang) {
			case '1':
				$text = gm('Name');
				break;
			case '2':
				$text = gm('Name fr');
				break;
			case '3':
				$text = gm('Name du');
				break;
			default:
				$text = gm('Name');
				break;
		}

		$cat_id = $in['cat_id'];
		if(!$in['from_address_id']) {
			$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=\'1\'
								   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
								   LEFT JOIN pim_article_brands ON pim_articles.article_brand_id = pim_article_brands.id 
								   LEFT JOIN pim_article_categories ON pim_articles.article_category_id = pim_article_categories.id ';
			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
						pim_article_prices.price, pim_articles.internal_name,
						pim_article_brands.name AS article_brand,
						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_article_categories.name AS family,
						pim_articles.supplier_reference,
						pim_articles.price AS unit_price,
						pim_articles.is_service,
						pim_articles.block_discount';

		}else{
			$table = 'pim_articles  INNER JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
			                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
							   INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id
							   LEFT JOIN pim_article_categories ON pim_articles.article_category_id = pim_article_categories.id';

			$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,
						pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.supplier_reference,
						pim_article_categories.name AS family,
						pim_articles.price AS unit_price,
						pim_articles.is_service,
						pim_articles.block_discount';
		}

		$filter.=" 1=1 ";

		//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

		if ($in['search'])
		{
			$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%' OR pim_articles.supplier_reference LIKE '%".$in['search']."%' OR pim_article_categories.name LIKE '%".$in['search']."%')";
			// $arguments.="&search=".$in['search'];
		}
		if ($in['hide_article_ids'])
		{
			$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
			// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
		}
		// if ($in['lang_id'])
		// {

		// 	$arguments.="&lang_id=".$in['lang_id'];
		// }
		// if ($in['is_purchase_order'])
		// {

			// $arguments.="&is_purchase_order=".$in['is_purchase_order'];
		// }
		if ($in['show_stock'])
		{
			$filter.=" AND pim_articles.hide_stock=0";
			// $arguments.="&show_stock=".$in['show_stock'];
		}
		if ($in['from_customer_id'])
		{
			$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
			// $arguments.="&from_customer_id=".$in['from_customer_id'];
		}
		if ($in['from_address_id'])
		{
			$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
			// $arguments.="&from_address_id=".$in['from_address_id'];
		}
		if($in['article_id']){
			$filter.=" AND  pim_articles.article_id=".$in['article_id'];
		}

		$articles= array( 'lines' => array());
		// $articles['max_rows']= $db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");

		$article = $this->db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1'  ORDER BY pim_articles.item_code LIMIT 5");
		
		

		$fieldFormat = $this->db->field("SELECT long_value FROM settings WHERE constant_name='QUOTE_FIELD_LABEL'");

		$time = time();

		$j=0;
		while($article->next()){
			$vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");
			
			if($in['buyer_id']){
				$vat_regime=$this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
				if($vat_regime==2){
					$vat=0;
				}
			}

			$values = $article->next_array();
			$tags = array_map(function($field){
				return '/\[\!'.strtoupper($field).'\!\]/';
			},array_keys($values));

			$label = preg_replace($tags, $values, $fieldFormat);

			if($article->f('price_type')==1){

			    $price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

		        $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

		       	if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
		            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

		        }else{
		       	   	$price_value=$price_value_custom_fam;

		         	 //we have to apply to the base price the category spec
		    	 	$cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    $price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

		    	    if($cat_price_type==2){
		                $article_base_price=get_article_calc_price($article->f('article_id'),3);
		            }else{
		                $article_base_price=get_article_calc_price($article->f('article_id'),1);
		            }

		       		switch ($cat_type) {
						case 1:                  //discount
							if($price_value_type==1){  // %
								$price = $article_base_price - $price_value * $article_base_price / 100;
							}else{ //fix
								$price = $article_base_price - $price_value;
							}
							break;
						case 2:                 //profit margin
							if($price_value_type==1){  // %
								$price = $article_base_price + $price_value * $article_base_price / 100;
							}else{ //fix
								$price =$article_base_price + $price_value;
							}
							break;
					}
		        }

			    if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }else{
		    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
		        if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
		        }
		    }

		    $pending_articles=$this->db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
		  	$base_price = $this->db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

		    $start= mktime(0, 0, 0);
		    $end= mktime(23, 59, 59);
		    $promo_price=$this->db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
		    if($promo_price->move_next()){
		    	if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

		        }else{
		            $price=$promo_price->f('price');
		            $base_price = $price;
		        }
		    }
		 	if($in['buyer_id']){
		 		// $customer_vat_id=$this->db->field("SELECT vat_id FROM customers WHERE customer_id='".$in['buyer_id']."'");
		  		$customer_custom_article_price=$this->db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['buyer_id']."'");
		    	if($customer_custom_article_price->move_next()){

		            $price = $customer_custom_article_price->f('price');

		            $base_price = $price;
		       	}
		       	// $vat = $this->db->field("SELECT value FROM vats WHERE vat_id='".$customer_vat_id."'");
		   	}

			$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");

			$linie = array(
			  	'article_id'				=> $article->f('article_id'),
			  	'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
			  	'name'						=> $article->f('internal_name'),
			  	'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
			    'stock'						=> $article->f('stock'),
			    'supplier_reference'		=> $article->f('supplier_reference'),
			    'family'					=> $article->f('family') ? $article->f('family') : '',
			    'stock2'					=> remove_zero_decimals($article->f('stock')),
			    'quantity'		    		=> 1,
			    'pending_articles'  		=> intval($pending_articles),
			    'threshold_value'   		=> $article->f('article_threshold_value'),
			  	'sale_unit'					=> $article->f('sale_unit'),
			  	'percent'           		=> $vat_percent,
				'percent_x'         		=> display_number($vat_percent),
			    'packing'					=> remove_zero_decimals($article->f('packing')),
			  	'code'		  	    		=> $article->f('item_code'),
				'price'						=> $article->f('is_service') == 1 ? $article->f('unit_price') : $price,
				'price_vat'					=> $in['remove_vat'] == 1 ? $price : $price + (($price*$vat)/100),
				'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
				'purchase_price'			=> $purchase_price,
				'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
				'quoteformat'    			=> html_entity_decode(gfn($label)),
				'base_price'				=> $article->f('is_service') == 1 ? place_currency(display_number_var_dec($article->f('unit_price'))) : place_currency(display_number_var_dec($base_price)),
				'show_stock'				=> $article->f('hide_stock') ? false:true,
				'hide_stock'				=> $article->f('hide_stock'),
				'is_service'				=> $article->f('is_service'),
			);
			array_push($articles['lines'], $linie);
		  	
		}

		$articles['buyer_id'] 		= $in['buyer_id'];
		$articles['lang_id'] 				= $in['lang_id'];
		$articles['cat_id'] 				= $in['cat_id'];
		$articles['txt_name']			  = $text;
		$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;

		array_push($articles['lines'],array('article_id'=>'99999999999','name'=>'############################################################################################################################################################ '.$in['search'].' ############################################################################################################################################################'));

		return $articles;
	}

	public function get_taxes($in)
	{
		$taxes = array( 'lines'=>array());
		
		$this->db->query("SELECT pim_articles.vat_id,pim_articles.article_id,vats.value
		                       FROM pim_articles
		                       INNER JOIN vats ON vats.vat_id=pim_articles.vat_id
		                       WHERE pim_articles.article_id='".$in['article_id']."' AND pim_articles.active='1' ");
		$vat_percent=$this->db->f('value');

		$gov_tax=array();
		if(!$in['quantity']){
			$in['quantity']==1;
		}

		$get_article_taxes=$this->db->query("SELECT tax_id FROM pim_articles_taxes WHERE article_id='".$in['article_id']."'");
		while ($get_article_taxes->next()){
			$gov_tax[$get_article_taxes->f('tax_id')]+=$in['quantity'];
		}

		$is_gov_taxes=false;
		$total_gov_taxes=0;
		$i=0;

		if($in['customer_id']){
			$vat_regime=$this->db->field("SELECT vat_regime_id FROM customers WHERE customer_id='".$in['customer_id']."'");
			if($vat_regime==2){
			  	$vat_percent=0;
			}
		}

		foreach($gov_tax as $tax_id => $quantity){
			$gov_tax = $this->db->query("SELECT pim_article_tax.* ,pim_article_tax_type.name as type_name
			                       FROM pim_article_tax
			                       INNER JOIN pim_article_tax_type ON pim_article_tax_type.id=pim_article_tax.type_id
			                       WHERE pim_article_tax.tax_id='".$tax_id."'");

			$vat_value= $gov_tax->f('amount')*($vat_percent/100);

			$linie = array(
				'tr_id'             			=> 'tmp'.$i.strtotime('now'),
				'is_vat'						=> $in['remove_vat'] == 1 ? false : true,
				'tax_id'            			=> $tax_id,
				'tax_for_article_id'			=> $in['article_id'],
				'quantity_old'      			=> $quantity,
				'quantity'          			=> display_number($quantity),
				'percent'           			=> $vat_percent,
				'percent_x'         			=> display_number($vat_percent),
				'vat_value'         			=> $vat_value,
				'vat_value_x'       			=> display_number($vat_value),
				'tax_id'            			=> $tax_id,
				'tax_name'          			=> $gov_tax->f('type_name'),
				'tax_quantity'      			=> display_number($quantity),
				'disc_val'          			=> display_number(0),
				'price'             			=> $in['exchange']==1 ? display_number_var_dec($gov_tax->f('amount')/return_value($in['ex_rate'])) : display_number_var_dec($gov_tax->f('amount')),
				'price_vat'         			=> $in['exchange']==1 ? display_number(($gov_tax->f('amount')/return_value($in['ex_rate'])) * ($vat_percent/100) ) : display_number($gov_tax->f('amount') * ($vat_percent/100)),
				'line_total'        			=> $in['exchange']==1 ? display_number(($gov_tax->f('amount')/return_value($in['ex_rate'])) * $quantity ) : display_number($gov_tax->f('amount') * $quantity),
				'tax_code'						=> $gov_tax->f('code'),
				// 'td_width'						=> ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px',
				// 'input_width'					=> ALLOW_ARTICLE_PACKING ? 'width:164px' : 'width:223px',
				'hide_disc'						=> $in['apply_discount'] ==0 || $in['apply_discount'] == 2 ? 'hide' : '',
				'allow_article_packing' 		=> $in['allow_article_packing'],
				'allow_article_sale_unit'		=> $in['allow_article_sale_unit'],
			);
			// $view->assign('allow_article_packing',$in['allow_article_packing'],'tax_line');
			// $view->assign('allow_article_sale_unit',$in['allow_article_sale_unit'],'tax_line');
			$total_gov_taxes+= $gov_tax->f('amount') * $quantity;

			array_push($taxes['lines'], $linie);
			$is_gov_taxes=true;
			// $view->loop('tax_line');
			$i++;
		}
		$taxes['is_gov_taxes'] = $is_gov_taxes;
		$taxes['gov_taxes_value']= display_number($total_gov_taxes);
		// $view->assign(array(
		// 	'td_width'			=> ALLOW_ARTICLE_PACKING ? 'width:218px' : 'width:277px',
		// ));
		return $taxes;
	}

	public function get_notes($in)
	{
		
		/*$transl_lang_id_active = $in['lang_id'];

		
		$array = array('translate_loop'=>array(),'translate_loop_custom'=>array(),'lang_cls'=>'');
		// $output['translate_loop']=array();
		$langs = $this->db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");
		while($langs->next()){
			// if($in['languages'] || ($in['template']&& !$in['quote_template_id'])){
			if($in['quote_id'] == 'tmp' && !$in['duplicate_quote_id'] && !$in['main_quote_id'] && !$in['template']){
				//if(!$in['template_id']){
					if($langs->f('lang_id')==1){
						$note_type = 'quote_note';
					}else{
						$note_type = 'quote_note_'.$langs->f('lang_id');
					}
					$quote_note = $this->db->field("SELECT value FROM default_data
											   WHERE default_name = 'quote_note'
											   AND type = '".$note_type."' ");
					// if($in['project_id']){
					// 	$transl_lang_id_active = $this->db->field("SELECT lang_id FROM pim_lang WHERE active = '1' AND sort_order = '1' GROUP BY lang_id  ");
					// }
			}else{
				if(!$in['template_id']){
					$quote_note = $this->db->field("SELECT item_value FROM note_fields
										   WHERE item_type 	= 'quote'
										   AND 	item_name 	= 'notes'
										   AND 	version_id 	= '".$this->version_id."'
										   AND 	item_id 	= '".$in['quote_id']."'
										   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

					/*$transl_lang_id_active = $this->db->field("SELECT lang_id FROM note_fields
										   WHERE item_type 	= 'quote'
										   AND 	item_name 	= 'notes'
										   AND 	version_id 	= '".$this->version_id."'
										   AND 	active 		= '1'
										   AND 	item_id 	= '".$in['quote_id']."' ");*1/
				}else{
					$quote_note = $this->db->field("SELECT item_value FROM note_fields
										   WHERE item_type 	= 'quote'
										   AND 	item_name 	= 'notes'
										   AND 	item_id 	= '".$in['template_id']."'
										   AND 	lang_id 	= '".$langs->f('lang_id')."' ");

					/*$transl_lang_id_active = $this->db->field("SELECT lang_id FROM note_fields
										   WHERE item_type 	= 'quote'
										   AND 	item_name 	= 'notes'
										   AND 	active 		= '1'
										   AND 	item_id 	= '".$in['template_id']."' ");*2/
				}

			}

			$translate_lang = 'language-'.$langs->f('code');
			if($langs->f('code')=='du'){
				$translate_lang = 'language-nl';
			}
			if($transl_lang_id_active != $langs->f('lang_id')){
				$translate_lang = $translate_lang.' hidden';
			}else{
				$translate_lang2 = $translate_lang;
			}
			console::log('e');
			$line=array(
				'translate_cls' 	=> 		'form-'.$translate_lang,
				'lang_id' 			=> 		$langs->f('lang_id'),
				'notes'				=> 		$quote_note,
				);
			array_push($array['translate_loop'],$line);
			// $view->loop('translate_loop');
		}
		// $output['translate_loop_custom']=array();
		$custom_langs = $this->db->query("SELECT * FROM pim_custom_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");
		while($custom_langs->next()){
			if(!$in['quote_id']  && !$in['duplicate_quote_id'] && !$in['main_quote_id'] && !$in['template']){
					if($custom_langs->f('lang_id')==1){
						$note_type = 'quote_note';
					}else{
						$note_type = 'quote_note_'.$custom_langs->f('lang_id');
					}
					$quote_note = $this->db->field("SELECT value FROM default_data
											   WHERE default_name = 'quote_note'
											   AND type = '".$note_type."' ");

					/*if($in['project_id']){
						$transl_lang_id_active = $this->db->field("SELECT lang_id FROM pim_custom_lang WHERE active = '1' AND sort_order = '1' GROUP BY lang_id  ");
					}*3/
			}else{
				if(!$in['template_id']){
					$quote_note = $this->db->field("SELECT item_value FROM note_fields
										   WHERE item_type 	= 'quote'
										   AND 	item_name 	= 'notes'
										   AND 	version_id 	= '".$this->version_id."'
										   AND 	item_id 	= '".$in['quote_id']."'
										   AND 	lang_id 	= '".$custom_langs->f('lang_id')."' ");

					/*$transl_lang_id_active = $this->db->field("SELECT lang_id FROM note_fields
										   WHERE item_type 	= 'quote'
										   AND 	item_name 	= 'notes'
										   AND 	version_id 	= '".$this->version_id."'
										   AND 	active 		= '1'
										   AND 	item_id 	= '".$in['quote_id']."' ");*4/
				}else{
					$quote_note = $this->db->field("SELECT item_value FROM note_fields
										   WHERE item_type 	= 'quote'
										   AND 	item_name 	= 'notes'
										   AND 	item_id 	= '".$in['template_id']."'
										   AND 	lang_id 	= '".$custom_langs->f('lang_id')."' ");

					/*$transl_lang_id_active = $this->db->field("SELECT lang_id FROM note_fields
										   WHERE item_type 	= 'quote'
										   AND 	item_name 	= 'notes'
										   AND 	active 		= '1'
										   AND 	item_id 	= '".$in['template_id']."' ");*5/
				}

			}

			$translate_lang = 'language-'.$custom_langs->f('code');
			if($custom_langs->f('code')=='du'){
				$translate_lang = 'language-nl';
			}
			// var_dump($transl_lang_id_active);

			if($transl_lang_id_active != $custom_langs->f('lang_id')){
				$translate_lang = $translate_lang.' hidden';
			}else{
				$translate_lang2 = $translate_lang;
			}
			$line=array(
				'translate_cls' 	=> 		'form-'.$translate_lang,
				'lang_id' 			=> 		$custom_langs->f('lang_id'),
				'notes'				=> 		$quote_note,
				);
			array_push($array['translate_loop_custom'], $line);
			// $view->loop('translate_loop_custom');
		}*/
		$terms = '';
		if($in['lang_id']!=1){		
			$terms = '_'.$in['lang_id'];
		}
		$quote_note = $this->db->field("SELECT value FROM default_data
												   WHERE default_name = 'quote_note'
												   AND type = 'quote_note".$terms."'  ");
		$free_text_content = $this->db->field("SELECT value FROM default_data
												   WHERE default_name = 'quote_term'
												   AND type = 'quote_terms".$terms."'  ");
		if($this->quote_id){ // the function is called from inside the class

			if($this->quote_id == 'tmp'){
				
				$array['notes'] = $quote_note;
				$array['free_text_content'] = $free_text_content;
			}else{
				
				$quote_notes = $this->db->field("SELECT item_value FROM note_fields
								 WHERE item_id= '".$in['quote_id']."' AND lang_id ='".$in['lang_id']."' AND item_type='quote' AND item_name='notes'");
				
				$array['notes']=$quote_notes;

				$free_text_content = $this->db->field("SELECT item_value FROM note_fields
									   WHERE item_type 	= 'quote'
									   AND 	item_name 	= 'free_text_content'
									   AND 	version_id 	= '".$this->version_id."'
									   AND 	item_id 	= '".$this->quote_id."' ");

				$array['free_text_content'] = $free_text_content;

			}
		}else{ // the function is called when the user changed the language

			$array['notes'] = $quote_note;
			$array['free_text_content'] = $free_text_content;

		}

		/*
			if($in['lang_id']==1){
				$note_type = 'quote_terms';
			}else{
				$note_type = 'quote_terms_'.$in['lang_id'];
			}
			$quote_note = $this->db->field("SELECT value FROM default_data
												   WHERE default_name = 'quote_term'
												   AND type = '".$note_type."'  ");
			$quote_notes = '';
			if($in['quote_id'] != 'tmp'){
				$quote_notes = $this->db->field("SELECT item_value FROM note_fields
							 WHERE item_id= '".$in['quote_id']."' AND lang_id ='".$in['lang_id']."' AND item_type='quote' AND item_name='notes'");
			}
		$array['NOTE']=$quote_notes ? $quote_notes : $quote_note;
		$array['lang_cls']='form-'.$translate_lang2;

		if(!isset($in['free_text_content']) && $in['quote_id']=='tmp'){
			$terms = '';
			if($in['lang_id'] != 1){
				$terms = '_'.$in['lang_id'];
			}
			$array['free_text_content'] = $this->db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='quote_terms".$terms."' ");
		}else{
			$array['free_text_content'] = $this->db->field("SELECT item_value FROM note_fields
									   WHERE item_type 	= 'quote'
									   AND 	item_name 	= 'free_text_content'
									   AND 	version_id 	= '".$this->version_id."'
									   AND 	item_id 	= '".$this->quote_id."' ");
		}*/
		 

		return $array;
		// $this->out = $array;
	}

	public function get_currency_convertor($in)
	{
		$currency = $in['currency'];
		if(empty($currency)){
			$currency = "USD";
		}
		$separator = $in['acc'];
		$into = $in['into'];
		return currency::getCurrency($currency, $into, 1,$separator);		
	}

	public function getViewQuote()
	{
		global $config;
		$valid = $this->getQuoteValid();
		if($valid){
			$this->output($valid);
		}

		$in = $this->in;
		$filter = '';
		if(!empty($this->version_id) && is_numeric($this->version_id)){
			$filter = 'AND tblquote_version.version_id = '.$this->version_id;
		}else{
			$filter = 'AND tblquote_version.active = 1';
		}

		$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");

		if($admin_licence != '3' && ONLY_IF_ACC_MANAG4 == '1'){
			$filter.= " AND CONCAT( ',', acc_manager_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
		}

		$data = $this->db->query("SELECT tblquote.*, tblquote.sent as sent_q,
					tblquote_version.active,tblquote_version.version_code,tblquote_version.version_id,tblquote_version.active AS version_active,
					tblquote_version.version_date, tblquote_version.sent as sent, tblquote_version.sent_date as sent_date,tblquote.pdf_logo,tblquote.pdf_layout,tblquote_version.preview
            		FROM tblquote
            		INNER JOIN tblquote_version ON tblquote_version.quote_id=tblquote.id
            		WHERE tblquote.id='".$in['quote_id']."' AND 1=1 ".$filter)->getAll();
		$this->quote = $data[0];
		if(!$this->version_id){
			$this->version_id = $this->quote['version_id'];
		}
		$filter_db2 = '';
		if($this->quote['contact_id'])
		{
			$filter_db2 = " AND customer_contacts.contact_id='".$this->quote['contact_id']."' ";			
		}
		$output = $this->quote;
		
		$u_id = $_SESSION['u_id'];											// $user_credential - 1 (admin)
		if($_SESSION['group']=='admin') {									//                  - 2 (quote_admin)
			$user_credential = 1;											//					- 3 (none => check if is author or creator)
		} else {
			if(in_array('quote', $_SESSION['admin_sett'])) {
				$user_credential = 2;
			} else {
				$user_credential = 3;
			}
		}
		if($user_credential == 3) {
			$created_by = $output['created_by'];
			$author_id = $output['author_id'];
			if($created_by == $u_id || $author_id == $u_id) {
				$output['is_admin_2'] = true;
			} else {
				$output['is_admin_2'] = false;
			}
		} else {
			$output['is_admin_2'] = true;
		}

		$output['apply_discount'] = $this->quote['apply_discount'];
		$output['discount_line_gen'] = $this->quote['discount_line_gen'];
		$output['discount'] = $this->quote['discount'];
		$output['own_reference'] = $this->quote['own_reference'];

		$output['ADV_QUOTE'] = defined('NEW_SUBSCRIPTION') ? (ADV_QUOTE == 1 ? true: false ) : true;

		switch ($_SESSION['group']) {
			case 'user':
				$can_revert = false;
				if(in_array(ark::$app, $_SESSION['admin_sett'])){
					$can_revert = true;
				}elseif($this->quote['created_by'] == $_SESSION['u_id']){
					$can_revert = true;
				}
				break;
			case 'admin':
				$can_revert = true;
				break;
			default:
				$can_revert = false;
				break;
		}
		$output['ACC_EMAIL'] = '';
		if($output['acc_manager_id']){
			$output['ACC_EMAIL'] = $this->db_users->field("SELECT email FROM users WHERE user_id='".$output['acc_manager_id']."' ");
		}

		$output['can_revert']				= $can_revert;
		$output['SQUOTE_DATE']				= date(ACCOUNT_DATE_FORMAT,$this->quote['version_date']);
		$output['sent_date']				= $this->quote['sent_date'] ? $this->quote['sent_date']*1000 : time()*1000;
		$output['sent_date_txt']			= $this->quote['sent']==1 && $this->quote['sent_date'] ? date(ACCOUNT_DATE_FORMAT,$this->quote['sent_date']) : '';
		$output['source'] = '-';
		if($this->quote['source_id']){
			$output['source'] = $this->db->field("SELECT name FROM tblquote_source WHERE id='".$this->quote['source_id']."' ");
		}
		$output['xtype'] = '-';
		if($this->quote['t_id']){
			$output['xtype'] = $this->db->field("SELECT name FROM tblquote_type WHERE id='".$this->quote['t_id']."' ");
		}
		$output['order_author'] = '-';
		if($this->quote['author_id']){
			$output['order_author'] = get_user_name($this->quote['author_id']);
		}
		$output['quote_lost'] = '-';
		if($this->quote['lost_id']){
			$output['quote_lost'] = $this->db->field("SELECT name FROM tblquote_lost_reason WHERE id='".$this->quote['lost_id']."' ");
		}
		$output['is_lost']					= $this->quote['status_customer'] == 1 ? true : false;
		$output['use_weblink']				= defined('USE_QUOTE_WEB_LINK') && USE_QUOTE_WEB_LINK == 1 ? true : false;
		$exist_url='';

		if(!$in['include_pdf'])
		{
			$in['include_pdf'] = 1;
		}
		if(defined('USE_QUOTE_WEB_LINK') && USE_QUOTE_WEB_LINK == 1){
			$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$this->quote_id."' AND `type`='q' AND version_id='".$this->version_id."' ");
			if(defined('WEB_INCLUDE_PDF_Q') && WEB_INCLUDE_PDF_Q == 0){
				$in['include_pdf'] = 0;
			}
		}
		$output['external_url']				= $this->quote['sent'] == '0' ? false : $exist_url;
		$output['web_url']					= $config['web_link_url'].'?q=';
		$output['contact_name'] 			= $this->db->field(" SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts
																WHERE contact_id='".$this->quote['contact_id']."' ");
		$output['free_field_txt']			= nl2br($this->quote['free_field']);
		$output['delivery_address_main']         = nl2br($this->quote['delivery_address']);

		$ver = $this->db->query("SELECT * FROM tblquote_version WHERE quote_id='".$in['quote_id']."' ORDER BY `version_code` ASC  ");
		$output['versions'] = array();
		while($ver->next()){
			$line = array(
				'VERSION_CODE'	=> $ver->f('version_code'),
				'version_id'	=> $ver->f('version_id'),
				'active'	    => $ver->f('active') == 1 ? ($this->quote['status_customer'] > 0 ? ($this->quote['status_customer'] == 1 ? 'lost' : 'active') : ($ver->f('sent') == 1 ? 'sent' : '')) : ($ver->f('sent') == 1 ? 'sent' : ''),
				'selected'		=> $ver->f('version_id') == $this->version_id ? 'selected' : '',
				'VERSION_LINK'  => ('index.php?do=quote-quote&quote_id='.$ver->f('quote_id').'&version_id='.$ver->f('version_id')),
				);
			array_push($output['versions'],$line);			
		}
		$output['WonOrLost']	    = gm('Won');
		$output['lost']				= false;
		if($this->quote['status_customer'] == 0){
			if($this->quote["sent"] == 0){
				$output['selectedDraft']	    ='selected';
			}else{
				$output['selectedSent']	    	='selected';
			}
		}else{
			$output['selectedWon']	    		='selected';
			if($this->quote['status_customer'] == 1){
				$output['WonOrLost']	    = gm('Lost');
				$output['lost']				= true;				
			}
		}
		if($this->quote['pdf_layout'] && $this->quote['use_custom_template']==0){
			$in['logo'] = $this->quote['pdf_logo'];
			$link_end='&type='.$this->quote['pdf_layout'].'&logo='.$this->quote['pdf_logo'];
			$in['template_type'] = $this->quote['pdf_layout'];
		}elseif($this->quote['pdf_layout'] && $this->quote['use_custom_template']==1){
			$link_end = '&custom_type='.$this->quote['pdf_layout'].'&logo='.$this->quote['pdf_logo'];
			$in['template_type'] = $this->quote['pdf_layout'];
			$in['use_custom'] = 1;
		}else{
			$link_end = '&type='.ACCOUNT_QUOTE_PDF_FORMAT;
			$in['template_type'] = ACCOUNT_QUOTE_PDF_FORMAT;
		}
		#if we are using a customer pdf template
		if(defined('USE_CUSTOME_QUOTE_PDF') && USE_CUSTOME_QUOTE_PDF == 1 && $this->quote['pdf_layout'] == 0){
			$link_end = '&custom_type='.ACCOUNT_QUOTE_PDF_FORMAT;
			$in['use_custom'] = 1;
		}
		$output['PDF_LINK2']='index.php?do=quote-print&id='.$this->quote_id.'&version_id='.$this->version_id.'&lid='.$this->quote['email_language'].'&upload='.($in['redo_img'] === true ? 0 : $this->quote['preview']).$link_end;
		$output['pdf_link']='index.php?do=quote-print&id='.$this->quote_id.'&version_id='.$this->version_id.'&lid='.$this->quote['email_language'].$link_end;

		$in['customer_id'] = $this->quote['buyer_id'];
		$in['contact_id'] = $this->quote['contact_id'];
		$output['recipients'] = $this->get_recipients($in);

		# email #
		$email_q = $this->db->query("SELECT customer_contacts.firstname,customer_contacts.lastname,
            SUM(tblquote_line.amount) AS total
            FROM tblquote
            LEFT JOIN tblquote_line ON tblquote_line.quote_id=tblquote.id
            LEFT JOIN customer_contacts ON customer_contacts.customer_id=tblquote.buyer_id
            WHERE tblquote.id='".$this->quote_id."' ".$filter_db2)->getAll();

		$message_data=get_sys_message('quomess',$this->quote['email_language']);
		console::log($message_data,$this->quote['email_language']);
		$subject=$message_data['subject'];
		
		$external_web_link = '<a href="'.$config['web_link_url'].'?q='.$exist_url.'">'.$config['web_link_url'].'?q='.$exist_url.'</a>';
		
		$subject=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $subject );
		$subject=str_replace('[!SERIAL_NUMBER!]',$this->quote['serial_number'].' ['.$this->quote['version_code'].']', $subject);
		$subject=str_replace('[!QUOTE_DATE!]',$quote_date, $subject);
		$subject=str_replace('[!CUSTOMER!]',$this->quote['buyer_name'], $subject);
		$subject=str_replace('[!SUBTOTAL!]',place_currency(display_number($email_q[0]['total']),get_commission_type_list($this->quote['currency_type'])), $subject);
		$subject=str_replace('[!DISCOUNT!]',$this->quote['discount'].'%', $subject);
		$subject=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($discountValue),get_commission_type_list($this->quote['currency_type'])), $subject);
		//$subject=str_replace('[!VAT!]',$this->quote['vat'].'%', $subject);
		$subject=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),get_commission_type_list($this->quote['currency_type'])), $subject);
		$subject=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($amount),get_commission_type_list($this->quote['currency_type'])), $subject);
		$subject=str_replace('[!CONTACT_FIRST_NAME!]',$email_q[0]['firstname'], $subject);
		$subject=str_replace('[!CONTACT_LAST_NAME!]',$email_q[0]['lastname'], $subject);
		$subject=str_replace('[!SUBJECT!]',$this->quote['subject'], $subject);

		$body=$message_data['text'];
		$body=str_replace('[!ACCOUNT_COMPANY!]',ACCOUNT_COMPANY, $body );
		$body=str_replace('[!SERIAL_NUMBER!]',$this->quote['serial_number'].' ['.$this->quote['version_code'].']', $body);
		$body=str_replace('[!QUOTE_DATE!]',$quote_date, $body);
		$body=str_replace('[!CUSTOMER!]',$this->quote['buyer_name'], $body);
		$body=str_replace('[!SUBTOTAL!]',place_currency(display_number($email_q[0]['total']),get_commission_type_list($this->quote['currency_type'])), $body);
		$body=str_replace('[!DISCOUNT!]',$this->quote['discount'].'%', $body);
		$body=str_replace('[!DISCOUNT_VALUE!]',place_currency(display_number($discountValue),get_commission_type_list($this->quote['currency_type'])), $body);
		//$body=str_replace('[!VAT!]',$this->quote['vat'].'%', $body);
		$body=str_replace('[!VAT_VALUE!]',place_currency(display_number($vat_value),get_commission_type_list($this->quote['currency_type'])), $body);
		$body=str_replace('[!AMOUNT_DUE!]',place_currency(display_number($amount),get_commission_type_list($this->quote['currency_type'])), $body);
		$body=str_replace('[!CONTACT_FIRST_NAME!]',$email_q[0]['firstname'], $body);
		$body=str_replace('[!CONTACT_LAST_NAME!]',$email_q[0]['lastname'], $body);
		$body=str_replace('[!SUBJECT!]',$this->quote['subject'], $body);

		$mail_sender_type = $this->db->field("SELECT value FROM default_data WHERE type='quote_email_type' ");
	 	if($mail_sender_type == 1){
					if($message_data['use_html']){
	  					 $body=str_replace('[!SIGNATURE!]',get_signature_new(), $body);
					}else{
		 				 $body=str_replace('[!SIGNATURE!]','', $body);
	 		}
	    }
	    $account_manager_email = $this->db_users->field("SELECT email FROM users WHERE user_id='".$_SESSION['u_id']."'");
	    $account_user_email = $this->db_users->field("SELECT email FROM users WHERE user_id='".$this->quote['acc_manager_id']."'");
		$output['use_html'] = $message_data['use_html'];
		$output['email'] = array(
			'e_subject'			=> $subject,
			'language_email' 	=> $this->quote['email_language'],
			'e_message' 		=> $in['e_message'] ? $in['e_message'] : ($body),
			'lid'				=> $this->quote['email_language'],
			'include_pdf'		=> $in['include_pdf'] ? true : false,
			'id'				=> $this->quote_id,
			'mark_as_sent'		=> $output['selectedDraft']	=='selected' ? '1':'0',
			'version_id'		=> $this->version_id,
			'files'				=> array(),
			'use_html'			=> $message_data['use_html'],
			'serial_number'		=> $this->quote['serial_number'],
			'recipients'		=> array_map(function($field){
										return $field['id'];
									},$output['recipients']),
			'alerts'			=> array(),
			'user_loged_email'	=> $account_manager_email,
			'user_acc_email'	=> $account_user_email,
// mail la userul logat.
		);

		//attached files
		$output['is_file'] = false;
		
		$f = $this->db->query("SELECT * FROM attached_files WHERE type='1'");
		while ($f->next()) {
			$output['email']['files'][]=array(
				'file'		=> $f->f('name'),
				'checked'	=> $f->f('default') == 1 ? true : false,
				'file_id'	=> $f->f('file_id'),
				'path'		=> '../'.$f->f('path'),
				);
			$output['is_file'] = true;
		}
		
		## email ##
		$amount = 0;
		$amount_purchase_price = 0;
		$amount_data = $this->db->query("SELECT * FROM tblquote_line WHERE quote_id='".$this->quote_id."' AND version_id='".$this->version_id."' AND show_block_total='0' ");
		while($amount_data->next()){
			$amount += $amount_data->f('amount');
			if($amount_data->f('article_id')>0){
				$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id = '".$amount_data->f('article_id')."' AND base_price = '1' ");
				$line_purchase_price = $purchase_price*$amount_data->f('quantity')*$amount_data->f('package')/$amount_data->f('sale_unit');
				$amount_purchase_price+=$line_purchase_price;
			}
		}
		if($this->quote['discount'] && $this->quote['apply_discount'] > 1){
			$amount = $amount - ($amount*$this->quote['discount']/100);
		}
		$amount_margin = $amount-$amount_purchase_price;
		$amount2 = 0;
		$amount2_margin = 0;
		if($this->quote['currency_rate']){
			$amount2 = $amount*return_value($this->quote['currency_rate']);
			$amount2_margin = $amount_margin*return_value($this->quote['currency_rate']);
		}

		$output['is_admin']			= $_SESSION['access_level'] == 1 ? true : false;
		$output['id_quote']			= $in['quote_id'];
		$output['language_dd']		= build_pdf_language_dd($this->quote['email_language'],1);
		$output['show_margin']		= SHOW_TOTAL_MARGIN_QUOTE == 1 ? true : false;
		$output['total']			= place_currency(display_number($amount),get_commission_type_list($this->quote['currency_type']));
		$output['total2']			= place_currency(display_number($amount2));
		$output['is_total2']		= $amount2 ? true : false;
		/*$output['total_margin']		= place_currency(display_number($amount_margin),get_commission_type_list($this->quote['currency_type']));*/
		$output['total_margin']		= place_currency(display_number($this->quote['margin']),get_commission_type_list($this->quote['currency_type']));
		$output['total2_margin']	= place_currency(display_number($amount2_margin));
		$output['is_total2_margin']	= $amount2_margin ? true : false;
		$output['default_currency']	= currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');
		$output['downpayment']		= ($this->quote['downpayment_val']>0 && !$this->quote['downpayment']) ? true : false;
		$output['downpayment_made']	= $this->quote['downpayment']>0 ? true : false;
		$output['downpayment_link']	= 'index.php?do=invoice-invoice&invoice_id='.$invoice_id;
		$output['can_do'] 			= $this->db->query("SELECT tblquote_line.*, tblquote_version.active FROM tblquote_line
									   INNER JOIN tblquote_version ON tblquote_line.version_id = tblquote_version.version_id
									   WHERE tblquote_line.quote_id='".$this->quote_id."' AND tblquote_line.content_type='1' AND tblquote_version.active='1' AND tblquote_line.article_id='0' ORDER BY id ASC  ")->records_count();
		$output['LOST_DD']          = build_data_dd('tblquote_lost_reason',$this->quote['lost_id'],'name',array('cond'=> 'ORDER BY sort_order','id' =>'id'));
		$output['LOST_QQ']			= build_data_qq();
		$ref = '';

		if(ACCOUNT_QUOTE_REF && $this->quote['buyer_reference']){
			$ref = $this->quote['buyer_reference'];
			if($ref){
				$ref = $ref.ACCOUNT_QUOTE_DEL;
			}
		}
		$output['quote_nr'] 	    = $ref.$this->quote['serial_number'];
		$output['account_manager']  = get_user_name($this->quote['acc_manager_id']);
		$output['quote_to_project_link']	= array('do'=>'project-project','source_id'=>$this->quote['source_id'],'buyer_id'=>($this->quote['buyer_id'] ? $this->quote['buyer_id'] : $this->quote['contact_id']),'customer'=> $this->quote['buyer_name'],'quote_id'=>$in['quote_id'],'currency_type'=>$this->quote['currency_type'],'budget_type'=>1,'is_contact'=>($this->quote['buyer_id'] ? '0' : '1'));
		$output['quote_to_service_link']	= array('source_id'=>$this->quote['source_id'],'buyer_id'=>($this->quote['buyer_id'] ? $this->quote['buyer_id'] : $this->quote['contact_id']),'customer'=> $this->quote['buyer_name'],'quote_id'=>$in['quote_id'],'currency_type'=>$this->quote['currency_type'],'is_contact'=>($this->quote['buyer_id'] ? '0' : '1'),'languages'=>$this->quote['email_language'],'acc_manager_id'=>$this->quote['acc_manager_id']);		
		//subscription
		global $apps;
		for ($i = 1; $i<=15; $i++){
		    if(in_array($i,perm::$allow_apps)){
		    	if($i==3){
		        	$is_3 = 1;
		       	}
		    	if($i==4){
		    		$is_4 = 1;
		    	}
		    	if($i==13){
		    		$is_13 = 1;
		    	}
		    	if($i==6){
		    		$is_6 = 1;
		    	}
		    }
		}
		$ADV_QUOT = $this->db->field("SELECT value FROM settings WHERE constant_name ='ADV_QUOTE' ");
			if($_SESSION['group']=='admin' && $ADV_QUOT==0){
				$is_3 = 0;
				$is_4 = 1;
				$is_13 = 0;
				$is_6 = 0;
			}

	        $output['is_3']= $is_3 == 0 ? false : true;
	        $output['is_4']= $is_4 == 0 ? false : true;
		    $output['is_13']= $is_13 == 0 ? false : true;
		    $output['is_6']= $is_6 == 0 ? false : true;

		$this->out = $output;



	}

	public function get_recipients($in)
	{
		$q = strtolower($in["term"]);
		if($in['customer_id'] && !$in['contact_id']){

		}else{
			$filter .=" AND CONCAT_WS(' ',customer_contacts.firstname, customer_contacts.lastname) LIKE '%".$q."%'";
		}

		

		if($in['current_id']){
			$filter .= " AND customer_contacts.contact_id !='".$in['current_id']."'";
		}

		if($in['customer_id']){
			$filter .= " AND customers.customer_id='".$in['customer_id']."'";
		}
		if($in['contact_id']){
			$filter .= " AND customer_contacts.contact_id='".$in['contact_id']."'";
		}

		$items = array();
		$items2 = array();
		if($in['customer_id'] && $in['contact_id']){
			$contacts = $this->db->query("SELECT customer_contacts.contact_id,customer_contactsIds.email,customer_contacts.customer_id,customer_contacts.firstname,customer_contacts.lastname,customers.cat_id, customers.name, customers.currency_id,customers.internal_language,customer_contactsIds.email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 $filter ORDER BY lastname ")->getAll();
		}else{
			$contacts='';	
		}
		if($in['customer_id'] && !$in['contact_id']){
			$customer = $this->db->query("SELECT customers.name as name,customers.c_email as email
					FROM customers
					WHERE 1=1 $filter ");
		}else{
			$customer = $this->db->query("SELECT customers.name as name,customers.c_email as email
					FROM customer_contacts
					INNER JOIN customer_contactsIds ON customer_contacts.contact_id = customer_contactsIds.contact_id
					LEFT JOIN customers ON customer_contactsIds.customer_id = customers.customer_id
					WHERE 1=1 $filter ");
		}
		$items2 = array(
			    'id' => $customer->f('email'), 
			    'label' => $customer->f('name'),
			    'value' => $customer->f('name'),
		);


		foreach ($contacts as $key => $value) {

			if($value['name']){
				$name = $value['firstname'].' '.$value['lastname'].' > '.$value['name'];
			}else{
				$name = $value['firstname'].' '.$value['lastname'];
			}
			if($in['only_contact_name']){
				$name = $value['firstname'].' '.$value['lastname'];
			}
			if ($value['customer_id']){
		  		$price_category = $value['cat_id'];
		  	}
		  	else{
		  		$price_category = "1";
		  	}
			$items[] = array(
			    'id' => $value['email'], 
			    'label' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$name),
			    'value' => preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($name)),
			);
		}
		$added = false;
		foreach ($items as $key => $value) {
			if($value['id'] == $items2['id']){
				$added = true;
			}
		}
		if(!$added){
			array_push($items, $items2);
		}
		// $items['name'].= $customer; 
		return $items;
	}

	public function get_pdfSettings($in)
	{
		$result = array( 'logo'=>array());
		$tblinvoice = $this->db->query("SELECT pdf_logo, pdf_layout, use_custom_template,email_language, identity_id FROM tblquote WHERE id='".$in['quote_id']."'");
		$tblinvoice->next();

		$result['quote_id']=$in['quote_id'];
		$result['version_id']=$in['version_id'];
	    $set_logo = $tblinvoice->f('pdf_logo');
	    if(!$set_logo){
	        $set_logo = ACCOUNT_LOGO_QUOTE;
	    }

		if(ACCOUNT_LOGO_QUOTE == '') {
		    array_push($result['logo'], array(
	            'account_logo'=>'images/no-logo.png',
	            'default'=>'images/no-logo.png'
	        ));
		}
		else {
		    $patt = '{q_logo_img_}';
		    $search = strpos(ACCOUNT_LOGO_QUOTE,'q_logo_img_');
		    if($search === false){
		        $patt = '{'.str_replace('upload/'.DATABASE_NAME.'/','',ACCOUNT_LOGO_QUOTE).',q_logo_img_}';
		    }
		    $logos = glob(__DIR__.'/../../../upload/'.DATABASE_NAME.'/'.$patt.'*',GLOB_BRACE);
		    foreach ($logos as $v) {
		        $logo = preg_replace('/\/(.*)\//', 'upload/'.DATABASE_NAME.'/', $v);
		        $size = getimagesize($logo);
		        $ratio = 250 / 77;
		        if($size[0]/$size[1] > $ratio ){
		            $attr = 'width="250"';
		        }else{
		            $attr = 'height="77"';
		        }
		        
		        array_push($result['logo'], array(
	                'account_logo'=>$logo,
	                'default'=>$set_logo,
	                'attr' => $attr
	            ));
		    }
		}

		if(!$tblinvoice->f('pdf_layout')) {
		  $selected=ACCOUNT_QUOTE_PDF_FORMAT ? ACCOUNT_QUOTE_PDF_FORMAT : '1';
		}else{
		 $selected=$tblinvoice->f('pdf_layout');

		 if($tblinvoice->f('use_custom_template')==1){
		    $selected=$tblinvoice->f('pdf_layout')+5;
		 }

		}

		$lng = $tblinvoice->f('email_language');

		if(!$lng){ $lng = 1; }

		if(!$tblinvoice->f('identity_id')) {
		  $selected_id='0';
		}else{
		 $selected_id=$tblinvoice->f('identity_id');
		}
		$link_end = '&type='.ACCOUNT_QUOTE_HEADER_PDF_FORMAT;
		$result['pdf_link']= 'index.php?do=quote-print&id='.$in['quote_id'].'&lid='.$tblinvoice->f('email_language').$link_end;
		$result['language_dd']          = build_language_admin_dd($lng);
		$result['language_id']          = $lng;
		$result['pdf_type_dd']          = build_pdf_type_dd($selected,false,false,false,true);
		$result['pdf_type_id']          = $selected;
		$result['multiple_identity']    = build_identity_dd($selected_id);
		$result['multiple_identity_id'] = $selected_id;
		$result['identity_id']          = $tblinvoice->f('identity_id');
		$result['do_next']              = 'quote-nquote-quote-pdf_settings';
		$result['default_logo']         = $set_logo;
		$result['xget']         		= 'pdfSettings';		


		return $result;
	}
	public function get_weblinkLog($in)
	{
		$l_r = ROW_PER_PAGE;
		$l_r = 10;

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		$result = array( 'weblink_rows'=>array(),'quote_id'=>$in['quote_id']);		

		$max_rows=$this->db->field("SELECT count(item_id) FROM weblink_log WHERE item_id = '".$in['quote_id']."' AND type = 'quote'");
		//$this->db->query("SELECT * FROM weblink_log WHERE item_id = '".$in['quote_id']."' AND type = 'quote' LIMIT ".$offset*$l_r.",".$l_r);
		$this->db->query("SELECT * FROM weblink_log WHERE item_id = '".$in['quote_id']."' AND type = 'quote'LIMIT ".$offset*$l_r.",".$l_r." ");

		while($this->db->move_next()){

		array_push($result['weblink_rows'], array(
			    'code' 		=> $this->db->f('code'), 
			    'ip'		=> $this->db->f('ip'),
				'browser'	=> $this->db->f('browser'),
				'time'		=> date('d-m-Y H:m:s', $this->db->f('time'))
			));

		}
		$result['lr']					= $l_r;
		$result['max_rows']         	= (int)$max_rows;	
		$result['xget']         		= 'weblinkLog';		

		return $result;
	}
	public function get_email_preview($in)
	{
		global $config;
		if(!$in['quote_id'])
		{
			return '';
		}
		$tblinvoice = $this->db->query("SELECT * FROM tblquote WHERE id='".$in['quote_id']."'  ");
		if(!$tblinvoice->next()){
			return '';			
		}
		$in['message'] = stripslashes($in['message']);
		
		$e_lang = $tblinvoice->f('email_language');
		if(!$e_lang || $e_lang > 4){
			$e_lang=1;
		}
		$text_array = array('1' => array('simple' => array('1' => 'Web link to Quote', '2'=> 'Following this link, you can see our quote.','3'=>"Following this link",'4'=>'Web Link'),
		        								   'pay' => array('1' => 'INVOICE WEB LINK', '2'=> 'You can download your invoice HERE and pay it online','3'=>"HERE")
		        								   ),
		        					'2' => array('simple' => array('1' => 'Lien web pour Offre', '2'=> 'Cliquez sur ce lien pour voir notre offre.','3'=>'Cliquez sur ce lien','4'=>'Lien web'),
		        								   'pay' => array('1' => 'LIEN WEB FACTURE', '2'=> 'Vous pouvez tÃ©lÃ©charger votre facture ICI et la payer en ligne','3'=>'ICI')
		        								   ),
		        					'3' => array('simple' => array('1' => 'Weblink naar Offerte', '2'=> 'Via deze link kan u onze offerte bekijken.','3'=>'Via deze link','4'=>'Weblink'),
		        								   'pay' => array('1' => 'WEB LINK FACTUUR', '2'=> 'U kan uw factuur HIER downloaden en online betalen','3'=>'HIER')
		        								   ),
		        					'4' => array('simple' => array('1' => 'ANGEBOT WIE WEB-LINK', '2'=> 'Sie kÃ¶nnen Ihr Angebot HIER downloaden','3'=>'HIER','4'=>'WEB-LINK'),
		        								   'pay' => array('1' => 'RECHNUNGS WEB LINK', '2'=> 'Sie kÃ¶nnen Ihre Rechnung HIER downloaden und online bezahlen','3'=>'HIER')
		        								   )
		        );

		$contact = $this->db->query("SELECT firstname,lastname,title FROM customer_contacts WHERE contact_id='".$tblinvoice->f('contact_id')."'  ");
		$title_cont = $this->db->field("SELECT name FROM customer_contact_title WHERE id='".$contact->f('title')."'  ");
		$customer = $this->db->field("SELECT name FROM customers WHERE customer_id='".$tblinvoice->f('buyer_id')."'  ");

		$in['message']=str_replace('[!CONTACT_FIRST_NAME!]', "".$contact->f('firstname')."", $in['message']);
		$in['message']=str_replace('[!CONTACT_LAST_NAME!]', "".$contact->f('lastname')."", $in['message']);
		$in['message']=str_replace('[!SALUTATION!]', "".$title_cont."", $in['message']);

		if($in['use_html']){
			if(defined('USE_QUOTE_WEB_LINK') && USE_QUOTE_WEB_LINK == 1){
				$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['quote_id']."' AND `version_id`='".$in['version_id']."' AND `type`='q'  ");
				
		    	$extra = "<p style=\"background: #f7f9fa; max-width: 90%; text-align:center; margin:0 auto; padding-bottom: 15px; border: 2px solid #deeaf0; color:#868d91;\"><br />";

		        $extra .="<img src=\"https://app.akti.com/pim/img/email_arrow_left.png\" width=\"18\" height=\"8\" />&nbsp;&nbsp;";
		    	$extra .="<b style=\"color: #5199b7;\"><a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['1']."</a></b>";
		        $extra .="&nbsp;&nbsp;<img src=\"https://app.akti.com/pim/img/email_arrow_right.png\" width=\"18\" height=\"8\" /><br />";
		    	$extra .=$text_array[$e_lang]['simple']['2']."<br /></p>";
				$in['message']=str_replace('[!LOGO!]',"<img style=\"width: 140px; height: auto; margin: 0 auto;\" src=\"".$config['site_url'].ACCOUNT_LOGO_QUOTE."\">",$in['message']);
		    	// $in['message'] .=$extra;
		        $in['message']=str_replace('[!WEB_LINK!]', $extra, $in['message']);
		    	$in['message']=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $in['message']);
		    }else{
		        $in['message']=str_replace('[!WEB_LINK!]', '', $in['message']);
		        $in['message']=str_replace('[!WEB_LINK_2!]', '', $in['message']);
		    }
		    if(defined('DROPBOX') && DROPBOX != ''){
		        if(strpos($in['message'], '[!DROP_BOX!]')){
		            $in['message']=str_replace('[!DROP_BOX!]', '', $in['message']);
		            $id = $tblinvoice->f('buyer_id');
		            $is_contact = 0;
		            if(!$id){
		                $id = $tblinvoice->f('contact_id');
		                $is_contact = 1;
		            }
		            $d = new drop('quotes', $id, $in['quote_id'],true,'',$is_contact, $tblinvoice->f('serial_number'));
		            $files = $d->getContent();
		            if(!empty($files['contents'])){
		                $in['message'] .="<br><br>";
		                foreach ($files['contents']  as $key => $value) {
		                    $l = $d->getLink(urldecode($value['path']));
		                    $link = $l;
		                    $file = $tblinvoice->f('serial_number').'/'.str_replace($files['path'].'/', '', $value['path']);
		                    $in['message'].="<p><a href=".$link.">".$file."</a></p>";
		                }
		            }
		        }
		    }
		}else{
			if(defined('USE_QUOTE_WEB_LINK') && USE_QUOTE_WEB_LINK == 1){
				$exist_url = $this->db_users->field("SELECT url_code FROM urls WHERE `database`='".DATABASE_NAME."' AND `item_id`='".$in['invoice_id']."' AND `type`='i'  ");
				
			   	$extra = "\n\n-----------------------------------";
			   	$extra .="\n".$text_array[$e_lang]['simple']['1'];
			   	$extra .="\n-----------------------------------";
			   	$extra .="\n". str_replace($text_array[$e_lang]['simple']['3'], "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['3']."</a>", $text_array[$e_lang]['simple']['2']);
			   	// $in['message'] .=$extra;
			   	$in['message']=str_replace('[!WEB_LINK!]', $extra, $in['message']);
		        $in['message']=str_replace('[!WEB_LINK_2!]', "<a href=\"".$config['web_link_url']."?q=".$exist_url."\">".$text_array[$e_lang]['simple']['4']."</a>", $in['message']);
			}else{
		        $in['message']=str_replace('[!WEB_LINK!]', '', $in['message']);
		        $in['message']=str_replace('[!WEB_LINK_2!]', '', $in['message']);
		    }
		    if(defined('DROPBOX') && DROPBOX != ''){
		        if(strpos($in['message'], '[!DROP_BOX!]')){
		            $in['message']=str_replace('[!DROP_BOX!]', '', $in['message']);
		            $id = $tblinvoice->f('buyer_id');
		            $is_contact = 0;
		            if(!$id){
		                $id = $tblinvoice->f('contact_id');
		                $is_contact = 1;
		            }
		            $d = new drop('quotes', $id, $in['quote_id'],true,'',$is_contact, $tblinvoice->f('serial_number'));
		            $files = $d->getContent();
		            if(!empty($files['contents'])){
		                $in['message'] .="\n\n";
		                foreach ($files['contents']  as $key => $value) {
		                    $l = $d->getLink(urldecode($value['path']));
		                    $link = $l;
		                    $file = $tblinvoice->f('serial_number').'/'.str_replace($files['path'].'/', '', $value['path']);
		                    $in['message'].="<p><a href=\"".$link."\">".$file."</a></p>\n";
		                }
		            }
		        }
		    }
		}

		
		$result = array( 'e_message' => nl2br($in['message']) );
		
		return $result;
	}

}

$quote = new QuoteCtrl($in,$db);

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if( method_exists($quote, $fname) ){
    	$quote->output( $quote->$fname($in) );	
    }else{
    	msg::error('Method does not exist','error');
    	$quote->output($in);
    }    
}

$quote->getQuote();

$quote->output();

?>