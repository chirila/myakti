<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('post_max_size', '500M');
ini_set('upload_max_filesize', '500M');
// echo ini_get('post_max_size');
global $config;
$db5 = new sqldb();
$quote_data = $db5->query("SELECT preview, show_grand, show_vat, discount, apply_discount, vat FROM tblquote WHERE id = '".$in['id']."' ");
$custom_lng = $db5->field("SELECT lang_id FROM pim_custom_lang WHERE lang_id='".$in['lid']."' ");
$version_data =$db5->field("SELECT preview FROM tblquote_version WHERE quote_id = '".$in['id']."' AND version_id = '".$in['version_id']."' ");

if($version_data == 1 && !$in['save_as']){

	ark::loadLibraries(array('aws'));
	$aws = new awsWrap(DATABASE_NAME);
	if(ark::$model == 'quote' && (ark::$method == 'send' || ark::$method == 'sendNewEmail')){
		$in['attach_file_name'] = 'quote_'.$in['id'].'_'.$in['version_id'].'.pdf';
		$file = $aws->getItem('quote/quote_'.$in['id'].'_'.$in['version_id'].'.pdf',$in['attach_file_name']);
		if($file === true){
			return;
		}else{
			$in['attach_file_name'] = null;
		}
	}else{		
		$link =  $aws->getLink($config['awsBucket'].DATABASE_NAME.'/quote/quote_'.$in['id'].'_'.$in['version_id'].'.pdf');
		$content = file_get_contents($link);
		$q_serial_number=$db->field("SELECT serial_number FROM tblquote WHERE id='{$in['id']}' ");

		if($q_serial_number){
             $name=$q_serial_number.'.pdf';
		}else{
			$name='quote_'.$in['id'].'.pdf';
		}
		if($content){
			header('Content-Type: application/pdf');
			header("Content-Disposition:inline;filename=".$name);
			header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			echo $content;
			exit();
		}else{
			$in['upload']='0';
		}
	}
}

ark::loadLibraries(array('tcpdf2/tcpdf/tcpdf','tcpdf2/tcpdf/examples/lang/eng'));
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetFont('helvetica', '', 10, '', false);
if($custom_lng){
	#we can use both font types but dejavusans looks better
	$pdf->SetFont('dejavusans', '', 9, '', false);
	// $pdf->SetFont('freeserif', '', 10, '', false);
}
$pdf_type =array(4,5);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Akti');
$pdf->SetTitle('Quote');
$pdf->SetSubject('Quote');

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);


/*if(!USE_QUOTE_PAGE_NUMBERING){
	$pdf->setPrintFooter(false);
	$pdf->SetAutoPageBreak(TRUE, 10);
}*/

// echo PDF_MARGIN_BOTTOM; exit();

// find out where to set auto page break

// while($quote_data->next()){
	/*$show_total_p = $quote_data->f('show_grand');
	$show_vat_p = $quote_data->f('show_vat');
	$discount_p = $quote_data->f('discount');
	$apply_discount_p = $quote_data->f('apply_discount');
	$general_vat_p = $quote_data->f('vat');*/
// }
/*$discount_procent = $discount_p ? $discount_p : 0;
if($apply_discount_p < 2){
	$discount_procent = 0;
}*/

/*if($show_total_p){
	$a_custom_page_break=30;
	if(!$discount_procent){
		$a_custom_page_break-=6;
	}
	if(!$show_vat_p){
		$a_custom_page_break-=6;
	}else{
		$nr_of_vats_p = $db5->field("SELECT COUNT( DISTINCT tblquote_line.vat )
		FROM tblquote_group
		LEFT JOIN tblquote_line ON tblquote_group.group_id = tblquote_line.group_id
		WHERE tblquote_group.quote_id = '".$in['id']."'
		AND tblquote_group.version_id = '".$in['version_id']."'
		AND tblquote_line.vat !=0");
		$a_custom_page_break = $a_custom_page_break + ($nr_of_vats_p*6)-6+2;
	}
}else{
	$a_custom_page_break=10;
}*/
// echo $a_custom_page_break; exit();

if(in_array($in['type'], $pdf_type)){
	$pdf->SetMargins(10, 10, 10);
	$pdf->SetFooterMargin(0);
	$pdf->SetAutoPageBreak(TRUE, $a_custom_page_break);
	$hide_all = 2;
}

$tagvs = array('h1' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 0,'n' => 0)),'h2' => array(0 => array('h' => 0,'n' => 0),1 => array('h' => 1,'n' => 2)));
$pdf->setHtmlVSpace($tagvs);

$pdf->AddPage();
$pdf->customTmargin = 15;

$htmll = include('print_body.php');

 //print_r($html); exit();
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM+20);

//console::benchmark('writeHTML');
//echo strlen($htmll);
// $htmll=str_replace("data:image/jpeg;base64,", "@", $htmll);
$pdf->writeHTML($htmll, true, false, true, false, '');
//console::end('writeHTML');
// if($in['height']){
	// echo 'height = '.$height; echo "<br/>";
	// print_r($pdf->GetY());
// }
$height_generated = $pdf->GetY();
if($height_generated-8 > $height && $in['type'] !=3){
	$pdf->AddPage();
}

$pdf->SetY($height);
if(in_array($in['type'], $pdf_type)){
	$hide_all = 1;
	// $pdf->SetAutoPageBreak(TRUE, 0);
	// if($in['type'] == 5){
		$pdf->SetY($height-11);
		$pdf->SetAutoPageBreak(TRUE, 10);

	$htmls = include('print_body.php');
// print_r($htmls);exit();
	$pdf->writeHTML($htmls, true, false, true, false, '');
}
$pdf->lastPage();
$in['last_page'] = $pdf->getPage();

if(isset($in['print'])){
	echo "<pre>";
	echo $htmll;
	exit();
}

if($in['save_as'] == 'F'){
	#we need to delete all the old images
	$img = glob(__DIR__.'/../../../../upload/'.DATABASE_NAME.'/quote_cache/quote_'.$in['id'].'_'.$in['version_id'].'_*.png');
	foreach ($img as $filename) {
		@unlink($filename);
	}
	ark::loadLibraries(array('aws'));
	$a = new awsWrap(DATABASE_NAME);
	$a->deleteItems($config['awsBucket'].DATABASE_NAME.'/quote_cache/quote_'.$in['id'].'_'.$in['version_id'].'_');
	$in['quote_pdf_name'] = 'quote_'.time().'.pdf';
	$pdf->Output(__DIR__.'/../../../'.$in['quote_pdf_name'], 'F');
	unlink(__DIR__.'/../../../'.$in['quote_pdf_name']);
}else if($in['upload'] == '0'){
	$in['quote_pdf_name'] = 'quote_'.unique_id(32).'.pdf';
	$pdf->Output(__DIR__.'/../../../'.$in['quote_pdf_name'], 'F');
	ark::loadLibraries(array('aws'));
	$a = new awsWrap(DATABASE_NAME);
	$pdfPath = INSTALLPATH.$in['quote_pdf_name'];
	$pdfFile = 'quote/quote_'.$in['id'].'_'.$in['version_id'].".pdf";
	$a->uploadFile($pdfPath,$pdfFile);
	unlink($in['quote_pdf_name']);
	$db->query("UPDATE tblquote_version SET preview='1' WHERE quote_id = '".$in['id']."' AND version_id = '".$in['version_id']."' ");
	$pdf->Output($buyer_reference.' '.$serial_number.' ['.$version_code.'].pdf','I');
}else if($in['do']=='quote-print'){
	$pdf->Output($buyer_reference.' '.$serial_number.' ['.$version_code.'].pdf','I');
}else{
	$pdf->Output(__DIR__.'/../../../'.'quote_.pdf', 'F');
}
