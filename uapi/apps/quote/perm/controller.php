<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $apps;
global $p_access;
perm::controller('all', 'admin',in_array($apps['quote'], $p_access));
perm::controller('all', 'user',in_array($apps['quote'], $p_access));
if(isset($_SESSION['admin_sett']) &&  !in_array(ark::$app, $_SESSION['admin_sett'])){
	perm::controller(array('quote_note','xdefault_message','xdefault_format','xlanguage','xquote_field_details','xquote_packing_details','default_email','categ','xlabel'), 'user', false);
}

if(in_array($apps['company'], $p_access)) {
	perm::controller(array('opportunities','opportunities_list','opportunity','nopportunity','stage_bar'),'user',in_array($apps['quote'], $p_access));
	perm::controller(array('opportunities','opportunities_list','opportunity','nopportunity','stage_bar'),'admin',in_array($apps['quote'], $p_access));
}
if(defined('WIZZARD_COMPLETE') && WIZZARD_COMPLETE=='0'){
	// echo "string";
	perm::controller('all', 'admin',false);
	perm::controller('all', 'user',false);
}