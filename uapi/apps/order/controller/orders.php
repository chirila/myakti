<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($_SESSION['customer_id']){
	$in['customer_id'] = $_SESSION['customer_id'];
	$in['buyer_id'] = $_SESSION['customer_id'];
}

if($_SESSION['contact_id']){
	$in['contact_id'] = $_SESSION['contact_id'];
} else {
	exit();
}

$l_r =ROW_PER_PAGE;

if(!empty($in['start_date'])){
	$in['start_date'] =strtotime($in['start_date']);
	$in['start_date'] = mktime(0,0,0,date('n',$in['start_date']),date('j',$in['start_date']),date('y',$in['start_date']));
}
if(!empty($in['end_date'])){
	$in['end_date'] =strtotime($in['end_date']);
	$in['end_date'] = mktime(23,59,59,date('n',$in['end_date']),date('j',$in['end_date']),date('y',$in['end_date']));
}

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}

$order_by_array = array('serial_number','t_date','del_date','our_ref','customer_name','amount');
$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);

$arguments = '';
$filter = 'WHERE 1=1 ';
$filter_link = 'block';
$filter_article='';
$order_by = " ORDER BY t_date DESC, iii DESC ";

if(!$in['archived']){
	$filter.= " AND pim_orders.active=1";
}else{
	$filter.= " AND pim_orders.active=0";
	$arguments.="&archived=".$in['archived'];
}
if($in['customer_id']){
	$filter.= " AND pim_orders.customer_id='".$in['customer_id']."'";
	$arguments_c .= '&customer_id='.$in['customer_id'];
}
if($in['filter']){
	$arguments.="&filter=".$in['filter'];
}
if($in['search']){
	$filter.=" and (pim_orders.serial_number like '%".$in['search']."%' OR pim_orders.customer_name like '%".$in['search']."%' OR pim_orders.our_ref like '%".$in['search']."%' )";
	$arguments.="&search=".$in['search'];
}

if($in['order_by']){
	if(in_array($in['order_by'], $order_by_array)){
		$order = " ASC ";
		if($in['desc'] == '1'){
			$order = " DESC ";
		}
		$order_by =" ORDER BY ".$in['order_by']." ".$order;
		$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
		/*$view_list->assign(array(
			'on_'.$in['order_by'] 	=> $in['desc'] ? 'on_asc' : 'on_desc',
		));*/
	}
}

$show_only_my = true;
if(defined('SHOW_MY_ORDERS_ONLY') && SHOW_MY_ORDERS_ONLY == 1){
	if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])){
		$show_only_my = false;
		if($in['view'] == 0){
			$in['view'] = 4;
		}
		$filter.=" and ( pim_orders.created_by = '".$_SESSION['u_id']."' OR pim_orders.author_id='".$_SESSION['u_id']."' ) ";
	}
}
	switch ($in['view']) {
		case '1':
			$filter.=" and pim_orders.rdy_invoice = '0' and pim_orders.sent='0' ";
			break;
		case '2':
			$filter.=" and pim_orders.rdy_invoice != '1' and pim_orders.sent='1'";
			break;
		case '3':
			$filter.=" and pim_orders.rdy_invoice = '1' ";
			break;
		case '4':
			if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])) {
				$filter.=" and ( pim_orders.created_by = '".$_SESSION['u_id']."' OR pim_orders.author_id='".$_SESSION['u_id']."' ) ";
			}
			break;
		default:
			break;
	}

if($in['start_date'] && $in['end_date']){
	$filter.=" and pim_orders.date BETWEEN '".$in['start_date']."' and '".$in['end_date']."' ";
}
else if($in['start_date']){
	$filter.=" and cast(pim_orders.date as signed) > ".$in['start_date']." ";
}
else if($in['end_date']){
	$filter.=" and cast(pim_orders.date as signed) < ".$in['end_date']." ";
}

if($in['new_orders']==1) {
	$days_30 = time() - 30*3600*24;
	$now = time();
	$filter .= " AND pim_orders.rdy_invoice!=1 AND date BETWEEN ".$days_30." AND ".$now." ";
}

$arguments = $arguments.$arguments_s;
$max_rows = $db->field("SELECT count(pim_orders.order_id)
			FROM pim_orders
			".$filter_article."
			 ".$filter."  ");
$prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;

$orders = $db->query("SELECT SUM( pim_orders_delivery.invoiced ) AS invoiced_del, pim_orders.order_id,pim_orders.author_id,pim_orders.buyer_ref,pim_orders.serial_number,pim_orders.del_date,
					pim_orders.date,pim_orders.amount,pim_orders.customer_name,pim_orders.rdy_invoice,pim_orders.sent,pim_orders.our_ref,pim_orders.invoiced, pim_orders.postgreen_id,serial_number as iii, cast( FROM_UNIXTIME( `date` ) AS DATE ) AS t_date
			FROM pim_orders
			LEFT JOIN pim_orders_delivery ON pim_orders_delivery.order_id = pim_orders.order_id
			".$filter_article."
			 ".$filter." GROUP BY pim_orders.order_id ".$order_by." LIMIT ".$offset*$l_r.",".$l_r)->getAll();


$ints = array();
foreach($orders as $key=>$value){

	$ref = '';
	if(ACCOUNT_ORDER_REF && $value['buyer_ref'] ){
		$ref = $value['buyer_ref'];

	}

	if($value['sent']==0 ){
		$status = gm('Draft');
	}
	if($value['invoiced']){
		if($value['rdy_invoice'] == 1){
			$status = gm('Delivered');
		}else{
			$status = gm('Final');
		}
	}else{
		if($value['rdy_invoice'] == 2){
			$status = gm('Final');
		}else{
			if($value['rdy_invoice'] == 1){
				$status = gm('Delivered');
			}else{
				if($value['sent'] == 1){
					$status = gm('Final');
				}else{
					$status = gm('Draft');
				}
			}
		}
	}

	$inv_ord = $value['invoiced_del'];
	$line =array(
		'id'              	  		=> $value['order_id'],
		'serial_number'   	  		=> $value['serial_number'] ? $ref.$value['serial_number'] : '',
		'date'         	  			=> date(ACCOUNT_DATE_FORMAT,$value['date']),
		'delivery_date'   	   		=> $value['del_date'] ? date(ACCOUNT_DATE_FORMAT,$value['del_date']):'',
		'amount'					=> place_currency(display_number($value['amount'])),
		'status'					=> $status,
		'our_reference'       		=> $value['our_ref'],
		'is_invoiced'				=> (!empty($inv_ord) || $value['invoiced'] == 1 ) ? true : false,
		'is_partially_delivered'    => $value['rdy_invoice']==2 ? true : false,
	);
	array_push($ints, $line);
}

$result = array('order'=>$ints,'orders'=>$max_rows);

json_out($result);
