<?php
 
class order {

 	var $pag = 'order';

	function __construct() {
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db = new sqldb();
    	$this->dbu = new sqldb();
		$this->db2 = new sqldb();
		$this->db_users = new sqldb($db_config);
		$this->db_users2 = new sqldb($db_config);
		$this->field_n = 'order_id';
	}

	/**
	 * order add
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function add_order(&$in)
	{
		if(!$this->add_order_validate($in)){
			http_response_code(400);
			json_out(array('validation_error' => msg::get_errors()));
			return false;
		}
		$in['serial_number'] = generate_order_serial_number(DATABASE_NAME);
		if($in['date']){
			$in['date_h']=strtotime($in['date']);
		}else{
			$in['date_h']=time();
		}
		if($in['delivery_date']){
			$in['del_date']=strtotime($in['delivery_date']);
		}else{
			$in['del_date']=time();
		}

		if ($in['rdy_invoice'])
		{
			$rdy_invoice = 1;
		}
		else
		{
			$rdy_invoice = 0;
		}

		if(!$in['notes']){
			$in['notes'] = $this->db->field("SELECT value FROM default_data WHERE type='order_note'");
   	 	}
	    if(!$in['pdf_layout'] && $in['identity_name']){
	      $in['pdf_layout'] = $this->db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$in['identity_name']."' and module='order'");
	    }

		$buyer_details = $this->db->query("SELECT customers.*,customer_addresses.*
			                    	FROM customers
			                    	LEFT JOIN customer_addresses ON customer_addresses.customer_id=customers.customer_id AND customer_addresses.is_primary=1
	                            	WHERE customers.customer_id='".$_SESSION['customer_id']."'");
	      $in['customer_email']=addslashes($buyer_details->f('c_email'));
	      $in['customer_phone']=addslashes($buyer_details->f('comp_phone'));
	      $in['customer_address']=addslashes($buyer_details->f('address'));
	      $in['customer_zip']=addslashes($buyer_details->f('zip_name'));
	      $in['customer_city']=addslashes($buyer_details->f('city_name'));
	      $in['customer_country_id']=$buyer_details->f('country_id');
	      $in['customer_state']=$buyer_details->f('state');
	      $in['customer_name']=addslashes($buyer_details->f('name'));
	      $in['contact_name']=get_contact_name($_SESSION['contact_id']);
	      $in['customer_vat_number']=$buyer_details->f('btw_nr');
	      $in['customer_cell']=$buyer_details->f('cell');

	    $main_address_id	= $buyer_details->f('address_id');
		$same_address = 0;

		$free_field = addslashes($buyer_details->f('address').'
		'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
		'.get_country_name($buyer_details->f('country_id')));

		if($in['main_address_id']){
			$buyer_m_address=$this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
				FROM customer_addresses
				LEFT JOIN country ON country.country_id=customer_addresses.country_id
				LEFT JOIN state ON state.state_id=customer_addresses.state_id
				WHERE customer_addresses.address_id='".$in['main_address_id']."' ");
			$main_address_id=$in['main_address_id'];
			$free_field = addslashes($buyer_m_address->f('address').'
			'.$buyer_m_address->f('zip').' '.$buyer_m_address->f('city').'
			'.get_country_name($buyer_m_address->f('country_id')));
		}

		if($in['shipping_address_id']){
			$buyer_m_address=$this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
				FROM customer_addresses
				LEFT JOIN country ON country.country_id=customer_addresses.country_id
				LEFT JOIN state ON state.state_id=customer_addresses.state_id
				WHERE customer_addresses.address_id='".$in['shipping_address_id']."' ");
			$same_address=$in['shipping_address_id'];
			$free_field = addslashes($buyer_m_address->f('address').'
			'.$buyer_m_address->f('zip').' '.$buyer_m_address->f('city').'
			'.get_country_name($buyer_m_address->f('country_id')));
		}

		$in['customer_id']=$_SESSION['customer_id'];
		$in['email_language']=$buyer_details->f('internal_language');
		if(!$in['email_language']){
			$in['email_language']=1;
		}
		$in['payment_term']=15;
		$in['field']='customer_id';
		$ref = '';
		$in['customer_ref'] = '';
		$o['customer_ref']=$buyer_details->f('our_reference');
		if(ACCOUNT_ORDER_REF && $buyer_details->f('our_reference')){
			$ref = $buyer_details->f('our_reference').ACCOUNT_ORDER_DEL;
		}
		$in['buyer_ref']= $ref;
		$in['our_ref']=$in['our_reference'];
		$in['your_ref']=$in['your_reference'];
		$in['identity_id']= $buyer_details->f('identity_id');
		$in['del_note']=$buyer_details->f('deliv_disp_note');
		$in['discount']=display_number($buyer_details->f('fixed_discount'));
		$in['currency_id']= $buyer_details->f('currency_id') ? $buyer_details->f('currency_id') : ACCOUNT_CURRENCY_TYPE;
		$in['currency_type_val']= $in['currency_id'];
		if($in['currency_id'] != ACCOUNT_CURRENCY_TYPE){	
	    	$params = array(
	    		'currency' 					=> currency::get_currency($in['currency_id'],'code'),
				'acc' 						=> ACCOUNT_NUMBER_FORMAT,
				'into' 						=> currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code')
			);
			
	    	$in['currency_rate'] 			= get_currency_convertor($params);
	    }
		$in['show_vat']=1;
		$in['apply_discount']=0;
		$in['discount_line_gen']=$buyer_details->f('line_discount');
		if($buyer_details->f('apply_fix_disc') && $buyer_details->f('apply_line_disc')){
			$in['apply_discount'] = 3;
		}else{
			if($buyer_details->f('apply_fix_disc')){
				$in['apply_discount'] = 2;
			}
			if($buyer_details->f('apply_line_disc')){
				$in['apply_discount'] = 1;
			}
		}
		$in['order_id'] = $this->db->insert("INSERT INTO pim_orders SET
										customer_id 						= '".$_SESSION['customer_id']."',
										customer_name 						= '".$in['customer_name']."',
										contact_name 						= '".$in['contact_name']."',
										contact_id							= '".$_SESSION['contact_id']."',
										company_number 						= '".$in['company_number']."',
										customer_vat_id 					= '".$in['customer_vat_id']."',
										customer_email 						= '".$in['customer_email']."',
										customer_bank_name 					= '".$in['customer_bank_name']."',
										customer_bank_iban 					= '".$in['customer_bank_iban']."',
										customer_bic_code 					= '".$in['customer_bic_code']."',
										customer_vat_number 				= '".$in['customer_vat_number']."',
										customer_country_id 				= '".$in['customer_country_id']."',
										customer_state						= '".$in['customer_state']."',
										customer_city 						= '".$in['customer_city']."',
										rdy_invoice 						= '".$rdy_invoice."',
										customer_zip 						= '".$in['customer_zip']."',
										customer_phone 						= '".$in['customer_phone']."',
										customer_cell 						= '".$in['customer_cell']."',
										customer_address 					= '".$in['customer_address']."',
										created_by							= 'API',
										seller_name       					= '".addslashes(utf8_encode(ACCOUNT_COMPANY))."',
                                        seller_d_address     				= '".addslashes(utf8_encode(ACCOUNT_DELIVERY_ADDRESS))."',
                                        seller_d_zip       					= '".addslashes(ACCOUNT_DELIVERY_ZIP)."',
                                        seller_d_city         				= '".addslashes(utf8_encode(ACCOUNT_DELIVERY_CITY))."',
                                        seller_d_country_id   				= '".ACCOUNT_DELIVERY_COUNTRY_ID."',
                                        seller_d_state_id     				= '".ACCOUNT_DELIVERY_STATE_ID."',
                                        seller_bwt_nr         				= '".addslashes(ACCOUNT_VAT_NUMBER)."',
                                        delivery_id 			    		= '".$in['delivery_id']."',
                                        delivery_address 					= '".$in['delivery_address']."',
                                        `date` 								= '".$in['date_h']."',
					    				payment_term 						= '".$in['payment_term']."',
					    				payment_type 						= '".$in['payment_type']."',
										serial_number 						= '".$in['serial_number']."',
										field								= '".$in['field']."',
										active								= '1',
										buyer_ref							= '".$in['buyer_ref']."',
										customer_ref						= '".$in['customer_ref']."',
										discount							= '".return_value($in['discount'])."',
										use_package           				= '".ALLOW_ARTICLE_PACKING."',
                                       	use_sale_unit         				= '".ALLOW_ARTICLE_SALE_UNIT."',
										our_ref								= '".$in['our_ref']."',
										your_ref							= '".$in['your_ref']."',
										del_date							= '".$in['del_date']."',
                                        del_note                    		= '".$in['del_note']."',
										remove_vat							= '".$in['remove_vat']."',
                                        quote_id 							= '".$in['quote_id']."',
                                        email_language						= '".$in['email_language']."',
										currency_type						= '".$in['currency_type_val']."' ,
										show_vat              				= '".$in['show_vat']."',
										address_info        				= '".$free_field."',
										default_delivery_address    		= '".$in['delivery_address']."',
										apply_discount						= '".$in['apply_discount']."',
										author_id							= '".$in['author_id']."',
                                        acc_manager_id           			= '".$in['acc_manager_id']."',
                                        acc_manager_name           			= '".$in['acc_manager_name']."',
										discount_line_gen					= 	'".return_value($in['discount_line_gen'])."',
                                        identity_id             			= '".$in['identity_id']."',
                                        delivery_address_id					= '".$in['shipping_address_id']."',
                                        pdf_layout              			= '".$in['pdf_layout']."',
                                          same_address='".$same_address."',
                                          main_address_id           = '".$in['main_address_id']."'
		                                    ");

		$transl_lang_id_active = $in['email_language'];
		$in['translate_loop'] = array();
		$langs = $this->db->query("SELECT * FROM pim_lang  WHERE active=1 GROUP BY lang_id ORDER BY sort_order");
		while($langs->next()){
			if($langs->f('lang_id')==1){
					$note_type = 'order_note';
			}else{
				$note_type = 'order_note_'.$langs->f('lang_id');
			}
			$order_note = $this->db->field("SELECT value FROM default_data
										   WHERE default_name = 'order note'
										   AND type = '".$note_type."' ");
			$translate_lang = 'form-language-'.$langs->f('code');
			if($langs->f('code')=='du'){
				$translate_lang = 'form-language-nl';
			}
			if($transl_lang_id_active != $langs->f('lang_id')){
				$translate_lang = $translate_lang.' hidden';
			}
			array_push($in['translate_loop'], array(
				'translate_cls' 	=> 		$translate_lang,
				'lang_id' 			=> 		$langs->f('lang_id'),
				'notes'				=> 		$order_note,
			));
		}
		$transl_lang_id_active_custom = $in['email_language'];
		$in['translate_loop_custom'] = array();
		#custom extra languages
		$custom_langs = $this->db->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order ");
		while($custom_langs->next()) {
			$custom_note_type = 'order_note_'.$custom_langs->f('lang_id');
			$order_note_custom = $this->db->field("SELECT value FROM default_data WHERE default_name = 'order note' AND type = '".$custom_note_type."' ");
			$translate_lang_custom = 'form-language-'.$custom_langs->f('code');
			if($transl_lang_id_active_custom != $custom_langs->f('lang_id')) {
				$translate_lang_custom = $translate_lang_custom.' hidden';
			}
			array_push($in['translate_loop_custom'], array(
				'translate_cls'		=> $translate_lang_custom,
				'lang_id'			=> $custom_langs->f('lang_id'),
				'notes'				=> $order_note_custom
			));
		}

		// add multilanguage order notes in note_fields
		$lang_note_id = $in['email_language'];
		
    	$langs = $this->db->query("SELECT lang_id FROM pim_lang WHERE active='1'  ORDER BY sort_order");
    	while($langs->next()){
	    	foreach ($in['translate_loop'] as $key => $value) {
	    		if($value['lang_id'] == $langs->f('lang_id')){
	    			$in['nota'] = $value['notes'];
	    			break;
	    		}
	    	}

	    	if($lang_note_id == $langs->f('lang_id')){
	    		$active_lang_note = 1;
	    	}else{
	    		$active_lang_note = 0;
	    	}
	    	$this->db->insert("INSERT INTO note_fields SET
	    										item_id 					=	'".$in['order_id']."',
	    										lang_id 					= 	'".$langs->f('lang_id')."',
	    										active 						= 	'".$active_lang_note."',
	    										item_type 				= 	'order',
	    										item_name 				= 	'notes',
	    										item_value				= 	'".$in['nota']."'
	    	");
    	
    	}
    	$custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active = '1' ORDER BY sort_order ");
	    while($custom_langs->next()) {
	    	foreach ($in['translate_loop_custom'] as $key => $value) {
	    		if($value['lang_id'] == $custom_langs->f('lang_id')){
	    			$in['nota'] = $value['notes'];
	    			break;
	    		}
	    	}
	    	if($lang_note_id == $custom_langs->f('lang_id')) {
	    		$active_lang_note = 1;
	    	} else {
	    		$active_lang_note = 0;
	    	}
	    	$this->db->insert("INSERT INTO note_fields SET
	    									item_id 		= '".$in['order_id']."',
	    									lang_id 		= '".$custom_langs->f('lang_id')."',
	    									active 			= '".$active_lang_note."',
	    									item_type 		= 'order',
	    									item_name 		= 'notes',
	    									item_value 		= '".$in['nota']."'
	    	");
	    }

		//add articles
		$in['lang_id'] = $in['email_language'];
		$in['price_category_id']=$buyer_details->f('cat_id');
		$in['cat_id'] = $in['price_category_id'];
		$global_disc = return_value($in['discount']);
		if($in['apply_discount'] < 2){
			$global_disc = 0;
		}
   		$order_total=0;
   		$in['tr_id']=array();
   		include_once(INSTALLPATH.'apps/order/controller/addArticle.php');
   		foreach($in['lines'] as $key=>$value){
   			if($value['article_id']){
   				$exist_article=$this->db->field("SELECT article_id FROM pim_articles WHERE article_id='".$value['article_id']."' ");
   				if(!$exist_article){
   					continue;
   				}
   				$in['article_id']=$value['article_id'];
   				$art_obj=new AddArticle();
   				$art_data=$art_obj->getLine($in);
   				$temp_line=$art_data['lines'][0];
   				$temp_line['article']=$value['name'];
   				$temp_line['quantity']=return_value($value['quantity']);
   				$temp_line['price']=return_value($value['unit_price']);
   				$temp_line['price_vat']=return_value($value['unit_price'])+return_value($value['unit_price'])*return_value($value['vat'])/100;
   				$temp_line['percent']=return_value($value['vat']);
   				$temp_line['vat_value']=return_value($value['unit_price'])*return_value($value['vat'])/100;
   				$temp_line['disc']=return_value($value['discount']);
   				$temp_line['content']=0;
   				$temp_line['is_tax']=0;
   				array_push($in['tr_id'], $temp_line);
   				$in['quantity']=return_value($value['quantity']);
   				$tax_data=$art_obj->getTaxes($in);
   				if(!empty($tax_data['lines'])){
   					foreach($tax_data['lines'] as $key1=>$value1){
   						$value1['article']=$value1['tax_name'];
   						$value1['content']=0;
   						$value1['article_code']=$value1['tax_code'];
   						array_push($in['tr_id'], $value1);
   					}				
   				}
   			}else{
   				$temp_line=array('is_tax'=>0,'article'=>$value['name'],'content'=>1);
   				array_push($in['tr_id'], $temp_line);
   			}
   		}

		foreach ($in['tr_id'] as $nr => $row)
		{   

			$line_total=0;

	    	if(!$row['sale_unit'] || !ALLOW_ARTICLE_PACKING){
				$row['sale_unit']=1;
			}
			if(!$row['packing'] ||  !ALLOW_ARTICLE_PACKING){
				$row['packing']=1;
			}

			$discount_line = return_value($row['disc']);
			if($in['apply_discount'] == 0 || $in['apply_discount'] == 2){
				$discount_line = 0;
			}

			$q = return_value($row['quantity']) * return_value2($row['packing']) / $row['sale_unit'];

			$price_line = return_value($row['price']) - (return_value($row['price']) * $discount_line /100);

			$line_total=$price_line * $q;

      		$order_total+= $line_total - ($line_total*$global_disc/100);
      		$is_from_import = $this->db->field("SELECT from_adveo FROM pim_articles WHERE article_id='".$row['article_id']."'");
			if($row['is_tax'] != 1){

							$this->db->query("INSERT INTO pim_order_articles SET
																		 from_import = '".$is_from_import."',
																		 order_id = '".$in['order_id']."',
																		 article_id = '".$row['article_id']."',
																		 discount = '".return_value($row['disc'])."',
																		 quantity = '".return_value($row['quantity'])."',
																		 article = '".htmlentities($row['article'])."',
																		 price = '".return_value($row['price'])."',
                                     									 purchase_price = '".$row['purchase_price']."',
																		 price_with_vat = '".return_value($row['price_vat'])."',
																		 vat_value = '".$row['vat_value']."',
																		 vat_percent = '".$row['percent']."',
																		 sort_order='".$nr."',
																		 sale_unit = '".$row['sale_unit']."',
																		 packing = '".return_value2($row['packing'])."',
																		 content = '".$row['content']."',
																		 article_code = '".addslashes($row['article_code'])."'
															");
			}else{ //is_tax
						$this->db->query("INSERT INTO pim_order_articles SET
																		 from_import = '".$is_from_import."',
																		 order_id = '".$in['order_id']."',
																		 tax_id = '".$row['tax_id']."',
																		 tax_for_article_id = '".$row['tax_for_article_id']."',
																		 quantity = '".return_value($row['quantity'])."',
																		 article = '".htmlentities($row['article'])."',
																		 price = '".return_value($row['price'])."',
																		 price_with_vat = '".return_value($row['price_vat'])."',
																		 vat_value = '".$row['vat_value']."',
																		 vat_percent = '".$row['percent']."',
																		 sort_order='".$nr."',
																		 discount = '".return_value($row['disc'])."',
																		 sale_unit = '".$row['sale_unit']."',
																		 packing = '".return_value2($row['packing'])."',
																		 content = '".$row['content']."',
																		 article_code = '".addslashes($row['article_code'])."'
															");
			}
		}

   		if($in['currency_type_val'] != ACCOUNT_CURRENCY_TYPE){
			$order_total = $order_total*return_value($in['currency_rate']);
		}

		$this->db->query("UPDATE pim_orders SET amount='".$order_total."' WHERE order_id='".$in['order_id']."'");
		insert_message_log($this->pag,'{l}Order successfully added by{endl} API',$this->field_n,$in['order_id']);

    	$count = $this->db->field("SELECT COUNT(order_id) FROM pim_orders ");
	    if($count == 1){
	      doManageLog('Created the first order.','',array( array("property"=>'first_module_usage',"value"=>'Order') ));
	    }
	    msg::success(gm('Order successfully added'),"success");
	    json_out(array('order_id'=>$in['order_id'],'serial_number'=>$in['serial_number']));
	}
	/**
	 * order add validate
	 *
	 * @param array $in
	 * @return true or false
	 * @author PM
	 */
	function add_order_validate(&$in)
	{
		if($in['main_address_id']){
			$main_e=$this->db->field("SELECT address_id FROM customer_addresses WHERE address_id='".$in['main_address_id']."' AND customer_id='".$_SESSION['customer_id']."' ");
			if(!$main_e){
				http_response_code(400);
				json_out(array('validation_error' => gm('Wrong main address')));
			}
		}
		if($in['shipping_address_id']){
			$ship_e=$this->db->field("SELECT address_id FROM customer_addresses WHERE address_id='".$in['shipping_address_id']."' AND customer_id='".$_SESSION['customer_id']."' ");
			if(!$ship_e){
				http_response_code(400);
				json_out(array('validation_error' => gm('Wrong shipping address')));
			}
		}
		$v = new validation($in);
		if($in['date']){
			$v->field('date', 'date', 'dateISO8601', gm('Invalid ISO8601 Date'));
		}
		if($in['delivery_date']){
			$v->field('delivery_date', 'delivery_date', 'dateISO8601', gm('Invalid ISO8601 Delivery Date'));
		}
		return $v->run();
	}

	function pay_validate(&$in){
		$v = new validation($in);
    		$v->field('order_id', 'order_id', 'required:exist[pim_orders.order_id]', gm('Invalid ID'));
    		$v->field('date', 'date', 'dateISO8601', gm('Invalid ISO8601 Date'));
    		$v->field('amount', 'amount', 'required', gm('Amount required'));
    		$is_ok=$v->run();
    		if($is_ok === true){
    			$allowed_data=$this->db->query("SELECT customer_id FROM pim_orders WHERE order_id='".$in['order_id']."' ");
    			if($allowed_data->f('customer_id') != $_SESSION['customer_id']){
				http_response_code(400);
				json_out(array('validation_error' => gm('Wrong order_id')));
				return false;
			}
    		}
    		return $is_ok;
	}

	function pay(&$in){
		if(!$this->pay_validate($in)){
			http_response_code(400);
			json_out(array('validation_error' => msg::get_errors()));
			return false;
		}
		$in['date']=strtotime($in['date']);
		if(date('I',$in['date'])=='1'){
			$in['date']+=3600;
		}
		$this->db->query("INSERT INTO pim_order_payments SET 
					order_id 	= '".$in['order_id']."',
					`date`	= '".$in['date']."',
					amount 	= '".return_value($in['amount'])."',
 					transaction_provider = '".addslashes($in['transaction_provider'])."',
 					transaction_id  	= '".addslashes($in['transaction_id'])."',
 					note  		= '".addslashes($in['notes'])."' ");

		msg::success(('Payment has been successfully recorded'),'success');
    		json_out();
	}

}

?>