<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/

	if($_SESSION['customer_id']){
		$in['customer_id'] = $_SESSION['customer_id'];
	}

	if($_SESSION['contact_id']){
		$in['contact_id'] = $_SESSION['contact_id'];
	} else {
		exit();
	}

	$l_r = ROW_PER_PAGE;
	$l_r = 30;

	if((!$in['offset']) || (!is_numeric($in['offset'])))
	{
	    $offset=0;
	    $in['offset']=1;
	}
	else
	{
	    $offset=$in['offset']-1;
	}
	$filter = 'WHERE 1=1 ';
	$order_by_array=array('name');
	$order_by = " ORDER BY installations.name ASC ";

	if(!empty($in['search'])){
		$filter.=" AND (installations.name LIKE '%".$in['search']."%' OR customers.name LIKE '%".$in['search']."%') ";
	}
	if($in['customer_id']){
		$filter .=" AND installations.customer_id='".$in['customer_id']."' ";
	}
	if(!empty($in['order_by'])){
		if(in_array($in['order_by'], $order_by_array)){
			$order = " ASC ";
			if($in['desc'] == 'true'){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
		}
	}


	$installations=$db->query("SELECT installations.* FROM installations 
		LEFT JOIN customers ON installations.customer_id=customers.customer_id
		".$filter.$order_by);
	$max_rows=$installations->records_count();
	$result = array('query'=>array(),'max_rows'=>$max_rows);
	$installations->move_to($offset*$l_r);
	$j=0;
	$ints=array();
	while($installations->move_next() && $j<$l_r){
		$address=$db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
									 FROM customer_addresses
									 LEFT JOIN country ON country.country_id=customer_addresses.country_id
									 LEFT JOIN state ON state.state_id=customer_addresses.state_id
									 WHERE customer_addresses.address_id='".$installations->f('address_id')."' ");
		$item=array(
			'installation_id'	=> $installations->f('id'),
			'name'				=> stripslashes($installations->f('name')),
			'address'			=> $address->f('address'),
			'city'				=> $address->f('city'),
			'country'			=> $address->f('country_id') ? get_country_name($address->f('country_id')) : ''
		);
		array_push($ints, $item);
		$j++;
	}

	$result = array('installation'=>$ints,'installations'=>$max_rows);
	json_out($result);
?>