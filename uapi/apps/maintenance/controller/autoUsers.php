<?php
	global $database_config;
	$db_config = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$db_users = new sqldb($db_config);

	$team_members=$db->query("SELECT user_id FROM servicing_team_users ")->getAll();
	$db_users->query("SELECT first_name,last_name,users.user_id FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.active='1' AND ( user_meta.value!='1' OR user_meta.value IS NULL ) AND users.group_id!='2' ORDER BY first_name ");
	while($db_users->move_next()){
		foreach($team_members as $key=>$value){
			if(in_array($db_users->f('user_id'), $value)){
				$users[utf8_encode($db_users->f('first_name').' '.$db_users->f('last_name'))]=$db_users->f('user_id');
			}
		}	  
	}

	$result = array('users'=>array());
	if($users){
		foreach ($users as $key=>$value) {
			array_push($result['users'], array("id"=>$value, "name" => strip_tags($key)));
			if (count($result['users']) > 2000)
			break;
		}
	}else {
		array_push($result['users'], array("id"=>'0', "name" => 'No matches' ));
	}
	json_out($result);
 ?>