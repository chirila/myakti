<?php

 if(!defined('BASEPATH')) exit('No direct script access allowed');

//print_r($_SESSION); exit();

if($_SESSION['customer_id']){
	$in['customer_id'] = $_SESSION['customer_id'];
	$in['buyer_id'] = $_SESSION['customer_id'];
}

if($_SESSION['contact_id']){
	$in['contact_id'] = $_SESSION['contact_id'];
} else {
	exit();
}

$l_r = ROW_PER_PAGE;
$l_r = 30;

if((!$in['offset']) || (!is_numeric($in['offset'])))
{
    $offset=0;
    $in['offset']=1;
}
else
{
    $offset=$in['offset']-1;
}
$order_by =" ORDER BY servicing_support.planeddate ASC ";
$filter = ' 1=1 ';

if($in['customer_id']){
	$filter .=" AND customer_id='".$in['customer_id']."' ";
	$arguments .='&customer_id='.$in['customer_id'];
}
if(!$in['archived']){
	$filter.= " AND servicing_support.active=1 ";
}else{
	$filter.= " AND servicing_support.active=0 ";
	$arguments.="&archived=".$in['archived'];
	$arguments_a = "&archived=".$in['archived'];
}

//FILTER LIST

if($in['search']){
	$filter .= " AND ( servicing_support.subject LIKE '%".$in['search']."%' OR servicing_support.serial_number LIKE '%".$in['search']."%' )";
	$arguments_s.="&search=".$in['search'];
}

if(!isset($in['view'])){
	$in['view'] = 0;
	$order_by =" ORDER BY servicing_support.serial_number DESC ";
}
/*if($in['view'] == 1){
	$order_by =" ORDER BY servicing_support.serial_number DESC ";
	$filter.=" and servicing_support.status = '1' ";

	$arguments.="&view=1";
}
if($in['view'] == 0){
	$filter.=" and servicing_support.status = '0' ";
}
if($in['view'] == 2){
	$filter.=" and servicing_support.status = '1' ";
	$arguments.="&view=2";
}
if($in['view'] == 3){
	$filter.=" and servicing_support.status = '2' ";
	$arguments.="&view=3";
}
if($in['view'] == 4){
	$filter.=" and servicing_support.status = '2' AND servicing_support.accept=1 ";
	$arguments.="&view=4";
}*/

if($in['order_by']){
	$order = " ASC ";
	if($in['desc'] == 'true'){
		$order = " DESC ";
	}
	$order_by =" ORDER BY ".$in['order_by']." ".$order;
	$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
	// $view_list->assign(array(
	// 	'on_'.$in['order_by'] 	=> $in['desc'] ? 'on_asc' : 'on_desc',
	// ));
}


$max_rows =  $db->field("SELECT count(DISTINCT servicing_support.service_id) FROM servicing_support
						".$join."
						WHERE ".$filter );

$intervention = $db->query("SELECT service_id,planeddate,customer_name,contact_name,subject,serial_number,rating,accept,status FROM servicing_support
			".$join."
			WHERE ".$filter." ".$order_by." LIMIT ".$offset*$l_r.",".$l_r )->getAll();
$ints = array();
foreach ($intervention as $key => $value) {
	switch($value['status']){
		case '1':
			$status_txt=gm('Planned');
			break;
		case '2':
			$status_txt=gm('Closed');
			break;
		default:
			$status_txt=gm('Draft');
	}
	$line = array(
		'accept' => $value['accept'],
		'contact_name'=> $value['contact_name'],
		'customer_name'=> $value['customer_name'],
		'planneddate'=> $value['planeddate'] ? date(ACCOUNT_DATE_FORMAT,$value['planeddate']) : '',
		'rating'=> $value['rating'],
		'serial_number'=> $value['serial_number'],
		'intervention_id'=> $value['service_id'],
		'status'=> $status_txt,
		'subject'=> $value['subject'],
		);
	array_push($ints, $line);
}

$result = array('intervention'=>$ints,'interventions'=>$max_rows);
//$result['lr'] = $l_r;
json_out($result);
