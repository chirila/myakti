<?php

	$user_ok=false;
	$team_members=$db->query("SELECT user_id FROM servicing_team_users ")->getAll();
	foreach($team_members as $key=>$value){
		if($value['user_id'] == $in['user_id']){
			$user_ok=true;
			break;
		}
	}
	if(!$user_ok){
		http_response_code(400);
		json_out(array('validation_error' => gm('Wrong user or no user selected')));
	}
	if(!$in['days']){
		http_response_code(400);
		json_out(array('validation_error' => gm('Days required')));
	}
	$result=array('interventions'=>array());
	$start_time=mktime(0,0,0,date('n'),date('j'),date('Y'));
	$end_day=mktime(23,59,59,date('n'),date('j'),date('Y'));
	$total_days=86400*($in['days']-1);
	$end_time=$end_day+$total_days;
	$i=0;
	$sheet = $db->query("SELECT servicing_support.planeddate,startdate FROM servicing_support
												 INNER JOIN servicing_support_users ON servicing_support.service_id=servicing_support_users.service_id
												 WHERE servicing_support.planeddate BETWEEN '".$start_time."' AND '".$end_time."'
												 AND servicing_support_users.user_id='".$in['user_id']."' AND servicing_support.active='1' ORDER BY servicing_support.planeddate, servicing_support.startdate ASC ")->getAll();

	$start_day=$start_time;
	while($start_day <= $end_time){
		$start_d=$start_day;
		$end_d=$start_d+86399;
		$service_nr=0;
		$result['interventions'][date('j/n/Y',$start_d)]=0;
		foreach($sheet as $key=>$value){
			if($value['planeddate'] >= $start_d && $value['planeddate'] <= $end_d){
				$service_nr++;
				$result['interventions'][date('j/n/Y',$start_d)]=$service_nr;			
			}
		}
		$start_day=$start_day+86400;
	}
	json_out($result);

?>