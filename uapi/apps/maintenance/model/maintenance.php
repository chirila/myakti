<?php
/**
 * undocumented class
 *
 * @package default
 * @author PM
 **/
class maintenance
{
	private $db;
	private $db_users;

	function maintenance() {
		$this->db = new sqldb();
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);

		$this->db_users = new sqldb($db_config);

	}

	# servicing and support
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addService(&$in)
	{
		if(!$this->addService_validate($in)){
			http_response_code(400);
			json_out(array('validation_error' => msg::get_errors()));
			return false;
		}
		$in['billable']=1;
		if($in['intervention_type']>2){
			$in['billable']=0;
		}
		$in['serial_number']=generate_service_number();
		$buyer_details = $this->db->query("SELECT customers.*,customer_addresses.*
			                    	FROM customers
			                    	LEFT JOIN customer_addresses ON customer_addresses.customer_id=customers.customer_id AND customer_addresses.is_primary=1
	                            	WHERE customers.customer_id='".$_SESSION['customer_id']."'");
	      $in['customer_email']=addslashes($buyer_details->f('c_email'));
	      $in['customer_phone']=addslashes($buyer_details->f('comp_phone'));
	      $in['customer_address']=addslashes($buyer_details->f('address'));
	      $in['customer_zip']=addslashes($buyer_details->f('zip_name'));
	      $in['customer_city']=addslashes($buyer_details->f('city_name'));
	      $in['customer_country_id']=$buyer_details->f('country_id');
	      $in['customer_state']=$buyer_details->f('state');
	      $in['customer_name']=$buyer_details->f('name');
	      $in['contact_name']=get_contact_name($_SESSION['contact_id']);

	      $main_address_id	= $buyer_details->f('address_id');
		$same_address = 0;

		$free_field = $buyer_details->f('address').'
		'.$buyer_details->f('zip').' '.$buyer_details->f('city').'
		'.get_country_name($buyer_details->f('country_id'));
		$name = stripslashes($name);

		if($in['main_address_id']){
			$buyer_m_address=$this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
				FROM customer_addresses
				LEFT JOIN country ON country.country_id=customer_addresses.country_id
				LEFT JOIN state ON state.state_id=customer_addresses.state_id
				WHERE customer_addresses.address_id='".$in['main_address_id']."' ");
			$main_address_id=$in['main_address_id'];
			$free_field = $buyer_m_address->f('address').'
			'.$buyer_m_address->f('zip').' '.$buyer_m_address->f('city').'
			'.get_country_name($buyer_m_address->f('country_id'));
		}

		if($in['site_address_id']){
			$buyer_m_address=$this->db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country
				FROM customer_addresses
				LEFT JOIN country ON country.country_id=customer_addresses.country_id
				LEFT JOIN state ON state.state_id=customer_addresses.state_id
				WHERE customer_addresses.address_id='".$in['site_address_id']."' ");
			$same_address=$in['site_address_id'];
			$free_field = $buyer_m_address->f('address').'
			'.$buyer_m_address->f('zip').' '.$buyer_m_address->f('city').'
			'.get_country_name($buyer_m_address->f('country_id'));
		}

		$in['email_language']=$buyer_details->f('internal_language');
		if(!$in['email_language']){
			$in['email_language']=1;
		}	
		$in['rate'] = $in['rate'] ? return_value($in['rate']) : (defined('MAINTENANCE_RATE') && MAINTENANCE_RATE ? MAINTENANCE_RATE : 0);
		if($in['planneddate']){
			$in['planneddate']=mktime(0,0,0,date('n',strtotime($in['planneddate'])),date('j',strtotime($in['planneddate'])),date('Y',strtotime($in['planneddate'])));
		}

		$in['intervention_id'] = $this->db->insert("INSERT INTO servicing_support SET
				service_type					='".($in['intervention_type']-1)."',
				customer_id						='".$_SESSION['customer_id']."',
				contact_id 						='".$_SESSION['contact_id']."',
				project_id 						='".$in['project_id']."',
				quote_id 						='".$in['quote_id']."',
				task_id 						='".$in['task_id']."',
				contract_id 					='".$in['contract_id']."',
				subject 						='".addslashes($in['subject'])."',
				planeddate 						='".$in['planneddate']."',
				date 							='".$in['date_ts']."',
				startdate 						='".correct_val($in['startdate'])."',
				enddate 						='".$in['enddate_ts']."',
				duration 						='".correct_val($in['duration'])."',
				report 						='".$in['report']."',
				type 							='".$in['type']."',
				customer_name 				 	='".$in['customer_name']."',
				customer_email       				='".$in['customer_email']."',
				customer_phone       				='".$in['customer_phone']."',
                   	customer_address       				='".$in['customer_address']."',
                    	customer_zip     	  				='".$in['customer_zip']."',
                   	customer_city    	   				='".$in['customer_city']."',
                   	customer_country_id    				='".$in['customer_country_id']."',
                    	customer_state       				='".$in['customer_state']."',
				contact_name					='".$in['contact_name']."',
				project_name					='".$in['project_name']."',
				task_name						='".$in['task_name']."',
				contract_name					='".$in['contract_name']."',
				user_name						='".$in['user_name']."',
				serial_number  					='".$in['serial_number']."',
				email_language					='".$in['email_language']."',
				billable               				='".$in['billable']."',
				rate 							='".$in['rate']."',
				price    						='".$in['price']."',
				active 						='1' ,
				free_field 						='".$free_field."',
				update_pdf						=1,
				identity_id 					='".$in['identity_id']."',
				same_address					='".$same_address."',
				main_address_id				='".$main_address_id."' ");

		if($in['user']){
			$in['user_id']=$in['user'];
			$in['name'] = get_user_name($in['user_id']);
			$this->addServiceUser($in);
		}
		if($in['task']){
			$this->serviceAddTask($in);
		}
		if($in['supply']){
			$this->serviceAddSupply($in);
		}
		if($in['article_id']){
			$this->serviceAddArticle($in);
		}
		msg::success(gm("Intervention created successfully"),'success');
		json_out(array('intervention_id'=>$in['intervention_id']));
	}

	function addService_validate(&$in){
		if(!$in['intervention_type'] || $in['intervention_type']>3 || $in['intervention_type']<0){
			http_response_code(400);
			json_out(array('validation_error' => gm('Intervention type required')));
		}
		if($in['main_address_id']){
			$main_e=$this->db->field("SELECT address_id FROM customer_addresses WHERE address_id='".$in['main_address_id']."' AND customer_id='".$_SESSION['customer_id']."' ");
			if(!$main_e){
				http_response_code(400);
				json_out(array('validation_error' => gm('Wrong main address')));
			}
		}
		if($in['site_address_id']){
			$site_e=$this->db->field("SELECT address_id FROM customer_addresses WHERE address_id='".$in['site_address_id']."' AND customer_id='".$_SESSION['customer_id']."' ");
			if(!$site_e){
				http_response_code(400);
				json_out(array('validation_error' => gm('Wrong site address')));
			}
		}
		$v = new validation($in);
		if($in['rate']){
			$v->field('rate', 'rate', 'numeric', gm('Invalid Rate'));
		}
		if($in['planneddate']){
			$v->field('planneddate', 'planneddate', 'dateISO8601', gm('Invalid ISO8601 Date'));
		}
		return $v->run();
	}

	/**
	 * Add user to an intervention
	 *
	 * @return void
	 * @author PM
	 **/
	function addServiceUser(&$in)
	{
		if(!$this->addServiceUser_validate($in)){
			if(ark::$method=='addService'){
				//nothing
			}else{
				http_response_code(400);
				json_out(array('validation_error' => msg::get_errors()));
			}	
			return false;
		}
		$this->db->insert("INSERT INTO servicing_support_users SET user_id='".$in['user_id']."', service_id='".$in['intervention_id']."', name='".$in['name']."' ");
		$service = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['intervention_id']."' ");
		if($service->next()){
			$in['startdate'] = $service->f('startdate');
			$in['planeddate_ts'] = $service->f('planeddate');
			$in['serial_number'] = $service->f('serial_number');
			if($in['startdate'] && $in['planeddate_ts']){
				$this->updateCalendar($in);
			}
		}
		msg::success( gm('User added'), 'success');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addServiceUser_validate(&$in)
	{	
		$user_exist=$this->db->field("SELECT user_id FROM servicing_team_users WHERE user_id='".$in['user_id']."' ");
		if(!$user_exist){
			if(ark::$method=='addService'){
				msg::notice(gm("User with id").' '.$in['user_id'].' '.gm('does not exist'),'notice');
				return false;
			}else{
				http_response_code(400);
				json_out(array('validation_error' => gm("User with id").' '.$in['user_id'].' '.gm('does not exist')));
			}		
		}
		$v = new validation($in);
		$v->f('intervention_id', 'intervention_id', 'required');
		return $v->run();
	}
	
	function updateCalendar(&$in)
	{
		if(isset($in['planeddate_ts']) && !empty($in['planeddate_ts'])){
			$in['planeddate_ts'] = mktime(0,0,0,date('n',$in['planeddate_ts']),date('j',$in['planeddate_ts']),date('Y',$in['planeddate_ts']));
			$in['planeddate'] = date(ACCOUNT_DATE_FORMAT,$in['planeddate_ts']);
		}
		$in['comment_due_date'] = $in['planeddate_ts']+round($in['startdate']*3600);
		$mtime = $in['planeddate_ts'];

		if($in['startdate']){
			$mtime += $in['startdate']*3600;
		}
		$end_t=$mtime;
		if($in['duration']){
			$end_t+=$in['duration']*3600;
		}
		if($in['plannedtime']){
			$mtime += $in['plannedtime']*3600;
		}

		$srv = $this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['intervention_id']."' ");
		$cust = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$srv->f('customer_id')."' ");
		$in['comment'] = 'Intervention - ' . $in['serial_number'];
		$in['comment'] .="\n".$srv->f('customer_name')."\n".$srv->f('report')."\n".$cust->f('address')."\n".$cust->f('city').' '.$cust->f('zip')."\n".get_country_name($cust->f('country_id'));
		$users = $this->db->query("SELECT * FROM servicing_support_users WHERE service_id='".$in['intervention_id']."' ");

		while ($users->next()) {

			$is_meet = $this->db->query("SELECT * FROM customer_meetings WHERE user_id='".$users->f('user_id')."' AND service_id='".$in['intervention_id']."' ");

			if($is_meet->next()){
				$in['meeting_id'] = $is_meet->f('customer_meeting_id');
				$this->db->insert("UPDATE customer_meetings SET start_date='".$mtime."',end_date='".$end_t."',message='".addslashes($in['comment'])."' WHERE user_id='".$users->f('user_id')."' AND service_id='".$in['intervention_id']."' ");
			}else{

				$in['meeting_id'] = $this->db->insert("INSERT INTO customer_meetings SET
															   subject='".addslashes($srv->f('subject'))."',
															   location='',
															   start_date='".$mtime."',
															   end_date='".$end_t."',
															   duration='0',
															   all_day='',
															   message='".addslashes($in['comment'])."',
															   from_c='".utf8_encode(get_contact_name($_SESSION['contact_id']))."',
															   type='3',
															   customer_id='".$srv->f('customer_id')."',
															   customer_name='".addslashes($srv->f('customer_name'))."',
															   user_id='".$users->f('user_id')."',
															   service_id='".$in['intervention_id']."',
															   notify = '1'
															");
			}
			$c = $this->db->query("SELECT * FROM customer_contact_activity_contacts WHERE activity_id='".$in['meeting_id']."' AND action_type IN (1,2) ");
			if(!$c->next()){
				$this->db->insert("INSERT INTO customer_contact_activity_contacts SET activity_id='".$in['meeting_id']."', contact_id='".$_SESSION['contact_id']."', action_type=1 ");
			}
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function serviceAddTask(&$in)
	{
		if(!$this->serviceAddTask_validate($in)){
			if(ark::$method=='addService'){
				//nothing
			}else{
				http_response_code(400);
				json_out(array('validation_error' => msg::get_errors()));
			}
			return false;
		}
		$task_budget=$this->db->field("SELECT price FROM pim_articles WHERE internal_name='".addslashes($in['task'])."' AND is_service='1' ");
		if(!$task_budget){
			$task_budget=0;
		}
		$this->db->insert("INSERT INTO servicing_support_tasks SET service_id='".$in['intervention_id']."', task_name='".addslashes($in['task'])."', task_budget='".$task_budget."' ");
		msg::success( gm('Changes saved'), 'success');
		$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['intervention_id']."'");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function serviceAddTask_validate(&$in)
	{
		$v = new validation($in);
		$v->f('task', 'task', 'required');
		$v->f('intervention_id', 'intervention_id', 'required');
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function serviceAddSupply(&$in)
	{
		if(!$this->serviceAddSupply_validate($in)){
			if(ark::$method=='addService'){
				//nothing
			}else{
				http_response_code(400);
				json_out(array('validation_error' => msg::get_errors()));
			}
			return false;
		}
		$this->db->insert("INSERT INTO servicing_support_articles SET service_id='".$in['intervention_id']."', name='".addslashes($in['supply'])."',billable='1' ");
		msg::success( gm('Changes saved'), 'success');
		$this->db->query("UPDATE servicing_support SET update_pdf=1 WHERE service_id='".$in['intervention_id']."'");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function serviceAddSupply_validate(&$in)
	{
		$v = new validation($in);
		$v->f('supply', 'supply', 'required');
		$v->f('intervention_id', 'intervention_id', 'required');
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function serviceAddArticle(&$in)
	{
		if(!$this->serviceAddArticle_validate($in)){
			if(ark::$method=='addService'){
				//nothing
			}else{
				http_response_code(400);
				json_out(array('validation_error' => msg::get_errors()));
			}
			return false;
		}
		$name=$this->db->field("SELECT internal_name FROM pim_articles WHERE article_id='".$in['article_id']."' ");
		$purchase_price = $this->db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$in['article_id']."' AND pim_article_prices.base_price='1'");
		$cat_id=$this->db->field("SELECT cat_id FROM customers WHERE customer_id ='".$_SESSION['customer_id']."' ");
		$article=$this->db->query("SELECT pim_articles.price_type,pim_articles.article_category_id,pim_articles.block_discount
		 	FROM pim_articles WHERE pim_articles.article_id='".$in['article_id']."' ");
		$price=0;
		if($article->f('price_type')==1){
			$price_value_custom_fam=$this->db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");
		      $pim_article_price_category_custom=$this->db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$in['article_id']."' and category_id='".$cat_id."' ");
		      if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
		            $price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$in['article_id']."'");

		      }else{
		       	$price_value=$price_value_custom_fam;
		         	 //we have to apply to the base price the category spec
		    	 	$cat_price_type=$this->db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    	$cat_type=$this->db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    	$price_value_type=$this->db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
		    	    	if($cat_price_type==2){
		                $article_base_price=get_article_calc_price($in['article_id'],3);
		            }else{
		                $article_base_price=get_article_calc_price($in['article_id'],1);
		            }
		       	switch ($cat_type) {
					case 1:                  //discount
						if($price_value_type==1){  // %
							$price = $article_base_price - $price_value * $article_base_price / 100;
						}else{ //fix
							$price = $article_base_price - $price_value;
						}
						break;
					case 2:                 //profit margin
						if($price_value_type==1){  // %
							$price = $article_base_price + $price_value * $article_base_price / 100;
						}else{ //fix
							$price =$article_base_price + $price_value;
						}
						break;
				}
		      }
			if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$in['article_id']."' AND base_price=1");
		      }
		}else{
		    	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$in['article_id']."'");
		      if(!$price || $article->f('block_discount')==1 ){
		        	$price=$this->db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$in['article_id']."' AND base_price=1");
		      }
		}

		$margin_nr=$price-$purchase_price;
		$margin_percent=($price-$purchase_price)/$price*100;
		$this->db->insert("INSERT INTO servicing_support_articles SET 
									service_id='".$in['intervention_id']."',
									name='".$name."',
									article_id='".$in['article_id']."',
									quantity='1',
									price='".$price."',
									purchase_price='".$purchase_price."',
									margin='".$margin_nr."',
									margin_per='".$margin_percent."',
									billable='1' ");
		msg::success(gm('Changes saved'),"success");
		return true;
	}

	function serviceAddArticle_validate(&$in)
	{
		$v = new validation($in);
		$v->f('intervention_id', 'intervention_id', 'required');
		$v->f('article_id', 'article_id', 'required');
		$is_ok=$v->run();
		if($is_ok){
			$article_e=$this->db->field("SELECT article_id FROM pim_articles WHERE article_id='".$in['article_id']."' AND is_service='0' ");
			if(!$article_e){
				if(ark::$method=='addService'){
					msg::notice(gm("Article with id").' '.$in['article_id'].' '.gm('does not exist'),'notice');
					return false;
				}else{
					http_response_code(400);
					json_out(array('validation_error' => gm("Article with id").' '.$in['article_id'].' '.gm('does not exist')));
				}
			}
			$unique_art=$this->db->field("SELECT a_id FROM servicing_support_articles WHERE service_id='".$in['intervention_id']."' AND article_id='".$in['article_id']."' ");
			if($unique_art){
				http_response_code(400);
				json_out(array('validation_error' => gm('Article already added')));
				return false;
			}
		}	
		return $is_ok;
	}

	function reschedule(&$in){
		if(!$this->reschedule_validate($in)){
			http_response_code(400);
			json_out(array('validation_error' => msg::get_errors()));
		}
		$this->db->query("UPDATE servicing_support SET status='0',planeddate='0',startdate='0',duration='0' WHERE service_id='".$in['intervention_id']."' ");
		$this->db->query("DELETE FROM servicing_support_users WHERE service_id='".$in['intervention_id']."' ");
		msg::success(gm("Intervention successfully updated"),'success');
		json_out();
	}

	function reschedule_validate(&$in){
		$v = new validation($in);
		$v->f('intervention_id', 'intervention_id', 'required');
		$is_ok=$v->run();
		if($is_ok){
			$allowed_data=$this->db->query("SELECT customer_id,status FROM servicing_support WHERE service_id='".$in['intervention_id']."'");
			if($allowed_data->f('customer_id') != $_SESSION['customer_id']){
				http_response_code(400);
				json_out(array('validation_error' => gm('Wrong intervention_id')));
				return false;
			}
			if($allowed_data->f('status') == 2){
				http_response_code(400);
				json_out(array('validation_error' => gm('Intervention closed')));
				return false;
			}
		}
		return $is_ok;
	}

	function downpaymentInvoice_validate(&$in){
		$v = new validation($in);
		$v->f('intervention_id', 'intervention_id', 'required');
		$v->f('amount', 'amount', 'required');
		$is_ok=$v->run();
		if($is_ok){
			$allowed_data=$this->db->query("SELECT customer_id,status FROM servicing_support WHERE service_id='".$in['intervention_id']."'");
			if($allowed_data->f('customer_id') != $_SESSION['customer_id']){
				http_response_code(400);
				json_out(array('validation_error' => gm('Wrong intervention_id')));
				return false;
			}
			if($allowed_data->f('status') == 2){
				http_response_code(400);
				json_out(array('validation_error' => gm('Intervention closed')));
				return false;
			}
			$downpayment_data=$this->db->query("SELECT tbldownpayments.invoice_id,tblinvoice.serial_number FROM tbldownpayments 
			INNER JOIN tblinvoice ON tbldownpayments.invoice_id=tblinvoice.id
			WHERE tbldownpayments.service_id='".$in['intervention_id']."' AND tblinvoice.f_archived='0' ");
			if($downpayment_data->f('invoice_id')){
				http_response_code(400);
				json_out(array('validation_error' => gm('Downpayment already done')));
				return false;
			}
		}
		return $is_ok;
	}

	function downpaymentInvoice(&$in){
		if(!$this->downpaymentInvoice_validate($in)){
			http_response_code(400);
			json_out(array('validation_error' => msg::get_errors()));
		}
		$in['downpayment_fixed']=$in['amount'];
		$service_data=$this->db->query("SELECT * FROM servicing_support WHERE service_id='".$in['intervention_id']."' ");
		$price=return_value($in['downpayment_fixed']);
		
		$buyer_details = $this->db->query("SELECT customers.*,customer_legal_type.name as l_name FROM customers
                        LEFT JOIN customer_legal_type ON customers.legal_type = customer_legal_type.id
                        WHERE customer_id='".$service_data->f('customer_id')."' ");
      	$buyer_address = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$service_data->f('customer_id')."' AND billing ='1' ");
      	$address_info = $buyer_address->f('address')."\n".$buyer_address->f('zip').'  '.$buyer_address->f('city')."\n".get_country_name($buyer_address->f('country_id'));
        $free_field=$service_data->f('free_field');

	    $email_language = $service_data->f('email_language');
	    $langs = $service_data->f('email_language');
	    if(!$email_language){
	    	$email_language = $buyer_details->f('internal_language');
      	$langs = $buyer_details->f('internal_language');      
	    }
	    if(!$email_language){
	    	$user_lang = $_SESSION['l'];
		if ($user_lang=='nl')
		{
			$user_lang='du';
		}
		switch ($user_lang) {
			case 'en':
				$user_lang_id='1';
				break;
			case 'du':
				$user_lang_id='3';
				break;
			case 'fr':
				$user_lang_id='2';
				break;
		}
	      $email_language = $user_lang_id;
	      $langs = $user_lang_id;
	    }

	    $quote_ts=time();
    	$due_days = $buyer_details->f('payment_term');
	    $invoice_due_date = $buyer_details->f('payment_term') ? $quote_ts + ($buyer_details->f('payment_term') * (60*60*24)) : time();
	    $payment_term_type = $buyer_details->f('payment_term_type') ? $buyer_details->f('payment_term_type') : PAYMENT_TERM_TYPE;
	    $payment_type_choose = $buyer_details->f('payment_term_type') ? $buyer_details->f('payment_term_type') : PAYMENT_TERM_TYPE;
	    if($payment_term_type == 2){
	      $curMonth = date('n',$quote_ts);
	      $curYear  = date('Y',$quote_ts);
	        $firstDayNextMonth = mktime(0, 0, 0,$curMonth+1, 1,$curYear);
	      $invoice_due_date = $buyer_details->f('payment_term') ? $firstDayNextMonth + ($buyer_details->f('payment_term') * (60*60*24)-1) : time();
	    }

	    $currency_type = $service_data->f('currency_type') ? $service_data->f('currency_type') : ($buyer_details->f('currency_type') ? $buyer_details->f('currency_type') : ACCOUNT_CURRENCY_TYPE);

	    $invoice_note_lang_id = $this->db->field("SELECT lang_id FROM pim_lang WHERE sort_order='1' AND active='1'");
	    // multilanguage notes
	    if($invoice_note_lang_id==1){
	      $invoice_note = 'invoice_note';
	    }else{
	      $invoice_note = 'invoice_note_'.$invoice_note_lang_id;
	    }

	    $notes_1 = addslashes($this->db->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note' "));
	    $notes_2 = addslashes($this->db->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note_2' "));
	    $notes_3 = addslashes($this->db->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note_3' "));
	    $notes_4 = addslashes($this->db->field("SELECT value FROM default_data WHERE  default_name='invoice note' AND type='invoice_note_4' "));

	    $custom_translate_loop=array();
	    $custom_langs_inv = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ");
	    while($custom_langs_inv->next()) {
	      $cust_translate_loop=array(
	        'lang_id'           =>    $custom_langs_inv->f('lang_id'),
	        'NOTES'             =>    addslashes($this->db->field("SELECT value FROM default_data WHERE default_name = 'invoice note' AND type='invoice_note_".$custom_langs_inv->f('lang_id')."' ")),
	      );
	      array_push($custom_translate_loop, $cust_translate_loop);
	    }

	    $NOTES=$notes_1;
	    $notes2 =  addslashes($buyer_details->f('invoice_note2'));
	    $contact_id = $service_data->f('contact_id');
	    $buyer_id = $service_data->f('customer_id');
	    $buyer_name = $service_data->f('customer_id') ? addslashes($buyer_details->f('name')).' '.addslashes($buyer_details->f('l_name')) : addslashes($buyer_details->f('firstname')).' '.addslashes($buyer_details->f('lastname'));
	    $customer_address = addslashes($buyer_address->f('address'));
	    $buyer_email = $buyer_id ? addslashes($buyer_details->f('c_email')) : addslashes($buyer_details->f('email'));
	    $buyer_phone = $buyer_id ? $buyer_details->f('comp_phone') : $buyer_details->f('phone');
	    $buyer_fax = $buyer_id ? $buyer_details->f('comp_fax') : '';
	    $buyer_zip = $buyer_address->f('zip');
	    $buyer_city = addslashes($buyer_address->f('city'));
	    $buyer_country_id = $buyer_address->f('country_id');
	    $buyer_state_id = $buyer_address->f('state_id');

	    $currency_rate = $currency_type != ACCOUNT_CURRENCY_TYPE ? $this->get_rate($currency_type) : 1;

	    $acc_manager=$this->db->field("SELECT acc_manager_name FROM customers WHERE customer_id = '".$buyer_id."'");
	    $acc_manager = explode(',',$acc_manager);
	    $acc_manager_id=$this->db->field("SELECT user_id FROM customers WHERE customer_id = '".$buyer_id."'");
	    $acc_manager_id = explode(',',$acc_manager_id);

	    $ac_manager_id=$acc_manager_id[0];
	    $ac_manager_name=$acc_manager[0];
	    $pdf_layout = $this->db->field("SELECT pdf_layout FROM identity_layout where identity_id='".$buyer_details->f('identity_id')."' and module='inv'");
	    if($contact_id) {
	      $contact_name=addslashes($this->db->field("SELECT CONCAT_WS(' ',firstname, lastname) FROM customer_contacts WHERE customer_contacts.contact_id='".$contact_id."'"));
	    }

	    $invoice_total = $price*$currency_rate;
	    $invoice_total_vat=$invoice_total+($invoice_total*get_customer_vat($buyer_id)/100);
	    $inv_sn=generate_invoice_number(DATABASE_NAME);
	    $invoice_id=$this->db->insert("INSERT INTO tblinvoice SET
                            serial_number           =   '".addslashes($inv_sn)."',
                            invoice_date            =   '".$quote_ts."',
                            due_date          		= '".$invoice_due_date."',
                            due_days          		=   '".$due_days."',
                            payment_term_type     	=   '".$payment_type_choose."',
                            req_payment             =   '100',
                            currency_type           =   '".$currency_type."',
                            notes2                  =   '".utf8_encode($notes2)."',
                            type                    =   '0',
                            contact_id          	=   '".$contact_id."',
                            contact_name         	=   '".$contact_name."',
                            status                  =   '0',
                            f_archived              =   '0',
                            paid                  	=   '0',
                            buyer_id                =   '".$buyer_id."',
                            buyer_name            	=   '".$buyer_name."',
                            buyer_address           =   '".$customer_address."',
                            buyer_email             =   '".$buyer_email."',
                            buyer_phone             =   '".$buyer_phone."',
                            buyer_fax             	=   '".$buyer_fax."',
                            buyer_zip             	=   '".$buyer_zip."',
                            buyer_city            	=   '".$buyer_city."',
                            buyer_country_id        =   '".$buyer_country_id."',
                            buyer_state_id          =   '".$buyer_state_id."',
                            seller_id             	=   '".$in['seller_id']."',
                            seller_name             =   '".addslashes(ACCOUNT_COMPANY)."',
                            seller_d_address        =   '".addslashes(ACCOUNT_DELIVERY_ADDRESS)."',
                            seller_d_zip            =   '".ACCOUNT_DELIVERY_ZIP."',
                            seller_d_city           =   '".addslashes(ACCOUNT_DELIVERY_CITY)."',
                            seller_d_country_id     =   '".ACCOUNT_DELIVERY_COUNTRY_ID."',
                            seller_d_state_id       =   '".ACCOUNT_DELIVERY_STATE_ID."',
                            seller_b_address        =   '".addslashes(ACCOUNT_BILLING_ADDRESS)."',
                            seller_b_zip            =   '".ACCOUNT_BILLING_ZIP."',
                            seller_b_city           =   '".addslashes(ACCOUNT_BILLING_CITY)."',
                            seller_b_country_id     =   '".ACCOUNT_BILLING_COUNTRY_ID."',
                            seller_b_state_id       =   '".ACCOUNT_BILLING_STATE_ID."',
                            seller_bwt_nr           =   '".$buyer_details->f('btw_nr')."',
                            created                 =   '".date(ACCOUNT_DATE_FORMAT,$quote_ts)."',
                            created_by              =   'API',
                            last_upd                =   '".date(ACCOUNT_DATE_FORMAT,$quote_ts)."',
                            last_upd_by             =   '0',
                            vat             		=   '".get_customer_vat($buyer_id)."',
                            our_ref           		=   '".$service_data->f('serial_number')."',
                            remove_vat          	=   '".$buyer_details->f('no_vat')."',
                            discount 				= '0',
                            currency_rate       	= '".$currency_rate."',
                            quote_id          		= '0',
                            contract_id         	= '0',
                            email_language        	= '".$email_language."',
                            order_id          		= '0',
                            free_field          	= '".$free_field."',
                            same_invoice        	= '0',
                            apply_discount        	= '0',
                            service_id          	= '".$in['service_id']."',
                            attach_timesheet        =   '0',
                            attach_intervention     =     '0',
                            vat_regime_id           ='".$buyer_details->f('vat_regime_id')."',
                            acc_manager_id          ='".$ac_manager_id."',
                            acc_manager_name        ='".$ac_manager_name."',
                            identity_id 			='".$buyer_details->f('identity_id')."',
                            pdf_layout				='".$pdf_layout."',
                            same_address			='1',
                            amount                  ='".$invoice_total."', 
                            amount_vat              ='".$invoice_total_vat."',
                            margin                  ='".$invoice_total."', 
                            margin_percent          ='0' ");

		$ogm = generate_ogm();
	    $this->db->query("UPDATE tblinvoice SET ogm = '".$ogm."' WHERE id='".$invoice_id."' ");

	    $lang_note_id = $email_language;
        if($lang_note_id == $email_language){
            $active_lang_note = 1;
        }else{
            $active_lang_note = 0;
        }

        $this->db->insert("INSERT INTO note_fields SET
                            item_id         = '".$invoice_id."',
                            lang_id         =   '".$email_language."',
                            active          =   '".$active_lang_note."',
                            item_type         =   'invoice',
                            item_name         =   'notes',
                            item_value        =   '".$NOTES."' ");
        $custom_lang_note_id = $email_language;

        $custom_langs = $this->db->query("SELECT lang_id FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
        while($custom_langs->next()) {
          foreach ($custom_translate_loop as $key => $value) {
            if($value['lang_id'] == $custom_langs->f('lang_id')){
              $nota = $value['NOTES'];
              break;
            }
          }
          if($custom_lang_note_id == $custom_langs->f('lang_id')) {
            $custom_active_lang_note = 1;
          } else {
            $custom_active_lang_note = 0;
          }
          $this->db->insert("INSERT INTO note_fields SET   item_id     = '".$invoice_id."',
                                      lang_id     = '".$custom_langs->f('lang_id')."',
                                      active      = '".$custom_active_lang_note."',
                                      item_type     = 'invoice',
                                      item_name     = 'notes',
                                      item_value    = '".$nota."' ");
        }

		$this->db->query("INSERT INTO tblinvoice_line SET
                                    name                =   '".(gm('Downpayment').' '.$service_data->f('subject'))."', 
                                    invoice_id          =   '".$invoice_id."',
                                    quantity            =   '1',
                                    price               =   '".$invoice_total."',
                                    amount              =   '".$invoice_total."',
                                    f_archived          =   '0',
                                    created             =   '".date(ACCOUNT_DATE_FORMAT,$quote_ts)."',
                                    created_by          =   'API',
                                    last_upd            =   '".$created_date."',
                                    last_upd_by         =   '0',
                                    vat                 = '".return_value(get_customer_vat($buyer_id))."',
                                    discount            = '0' ");
                
  		insert_message_log('invoice','{l}Invoice was created by{endl} API','invoice_id',$in['invoice_id']);

  		$down_exist=$this->db->field("SELECT id FROM tbldownpayments WHERE service_id='".$in['service_id']."' ");
  		if($down_exist){
  			$this->db->query("UPDATE tbldownpayments SET
  									invoice_id  = '".$invoice_id."',
  									date        = '".$quote_ts."',
  									user_id 	= '0',
  									value_fixed = '".return_value($in['downpayment_fixed'])."',
  									value_percent='".return_value($in['downpayment_percent'])."'
  									WHERE service_id='".$in['intervention_id']."' ");
  		}else{
  			$this->db->query("INSERT INTO tbldownpayments SET
  									invoice_id  = '".$invoice_id."',
  									date        = '".$quote_ts."',
  									user_id 	= '0',
  									value_fixed = '".return_value($in['downpayment_fixed'])."',
  									value_percent='".return_value($in['downpayment_percent'])."',
  									service_id   = '".$in['intervention_id']."' ");
  		}		
	    # for pdf

	      $tracking_data=array(
	            'target_id'         => $invoice_id,
	            'target_type'       => '1',
	            'target_buyer_id'   => $buyer_id,
	            'lines'             => array(
	                                          array('origin_id'=>$in['service_id'],'origin_type'=>'5')
	                                    )
	          );
	      addTracking($tracking_data);
	    msg::success(gm('Downpayment invoice created'),"success");
	    json_out(array('invoice_id'=>$invoice_id,'serial_number'=>$inv_sn));	
	}

	function get_rate($from)
  	{
	    $currency = currency::get_currency($from,'code');
	    if(empty($currency)){
	      $currency = "USD";
	    }
	    $separator = ACCOUNT_NUMBER_FORMAT;
	    $into = currency::get_currency(ACCOUNT_CURRENCY_TYPE,'code');

	    return currency::getCurrency($currency, $into, 1,$separator);
  	}

}

?>