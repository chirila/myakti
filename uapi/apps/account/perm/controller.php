<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

perm::controller('customerfields', 'all');
perm::controller('contactfields', 'all');
perm::controller('countries', 'all');
perm::controller('titles', 'all');
perm::controller('sectors', 'all');
perm::controller('all', 'user');

// var_dump(perm::$ctrl);