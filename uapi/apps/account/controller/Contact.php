<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
$result =array();

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}
if($in['viewMod']){
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db_users = new sqldb($database_users);

	$users_auto = $db_users->query("SELECT users.first_name, users.last_name, users.user_id, users.main_user_id, users.user_role, users.user_type FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active'  WHERE  database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1'  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) GROUP BY users.user_id ORDER BY first_name ")->getAll();
	$user_auto = array();
	foreach ($users_auto as $key => $value) {
		$user_auto[] = array('id'=>$value['user_id'], 'value'=>utf8_encode(strip_tags($value['first_name'] . ' ' . $value['last_name'])) );
	}

	$result['user_auto'] = $user_auto;
}

$contact = $db->query("SELECT * FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ")->getAll();
$contact = $contact[0];

$contact_new = $db->query("SELECT * FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' ")->getAll();
$contact_new = $contact_new[0];
$contact['position'] = $db->field("SELECT name FROM customer_contact_job_title WHERE id='".$contact_new['position']."'");
$contact['department_new'] = $db->field("SELECT name FROM customer_contact_dep WHERE id='".$contact_new['department']."'");
$contact['phone'] = $contact_new['phone'];
$contact['fax'] = $contact_new['fax'];

$contact['exact_title']=$contact_new['e_title'];
$contact['function']=$contact_new['e_title'];
$contact['title_txt'] = $db->field("SELECT name FROM customer_contact_title  WHERE id='".$contact['title']."' ");
if(!is_numeric($contact['title'])){
	$contact['title']='1';
}

if(!is_numeric($contact['sex']) || !$contact['sex'] ){
	$contact['sex']='1';
}
switch ($contact['sex']) {
	case '2':
		$contact['sex_txt'] = gm('Female');
		break;	
	default:
		$contact['sex_txt'] = gm('Male');
		break;
}

if(!is_numeric($contact['language'])){
	$contact['language']='1';
}
$contact['language_txt'] = $db->field("SELECT name FROM customer_contact_language WHERE id='".$contact['language']."' ");
$contact['exact_title_txt'] = $db->field("SELECT e_title FROM customer_contactsIds  WHERE contact_id='".$in['contact_id']."' ");

if(!is_numeric($contact['department'])){
	$contact['department']='1';
}
$contact['department_txt'] = $db->field("SELECT name FROM customer_contact_dep WHERE id='".$contact['department']."' ");

if(!is_numeric($contact['country_id'])){
	$contact['country_id']=ACCOUNT_BILLING_COUNTRY_ID;
}

if($contact['birthdate']){
	$contact['birthdate_txt'] = date(ACCOUNT_DATE_FORMAT,$contact['birthdate']);
	$contact['birthdate'] = $contact['birthdate']*1000;
}


if($contact['s_email'] == '1'){
	$contact['s_email'] = true;
	$contact['s_email_txt'] = gm('Yes');
}else{
	$contact['s_email'] = false;
	$contact['s_email_txt'] = gm('No');
}


if($contact['create']){
	$contact['create'] = date(ACCOUNT_DATE_FORMAT,$contact['create']);
}

if( isset($contact['position']) && !is_numeric($contact['position']) ){
	$contact['position']='1';
}
$contact['position_txt'] = $db->field("SELECT name FROM customer_contact_job_title WHERE id='".$contact['position']."' ");

if($contact['position_n']){
	$contact['position_n'] = explode(',', $contact['position_n']);
}

$label = $db->query("SELECT * FROM contact_fields WHERE value = '1' ORDER BY field_id ");
$all = $label->getAll();
$contact['extra']=array();
$result['extra'] = array();
$result['extra_name'] = array();
foreach ($all as $key => $value) {
	$f_value = $db->field("SELECT value FROM contact_field WHERE customer_id='".$in['contact_id']."' AND field_id='".$value['field_id']."' ");
	if($value['is_dd']==1){
		$result['extra'][$value['field_id']]=build_extra_contact_field_dd('',$value['field_id']);
		if($in['viewMod']){
			$f_value = $db->field("SELECT name FROM contact_extrafield_dd WHERE extra_field_id='".$value['field_id']."' ");
		}
		if(!$f_value){
			$f_value = '0';
		}
	}
	$result['extra_name'][$value['field_id']] = $value['label'];
	
	$contact['extra'][$value['field_id']]=$f_value;
}

/*$label1 = $db->query("SELECT * FROM contact_fields WHERE value = '1' AND default_value='1' ORDER BY field_id ");
$all1 = $label1->getAll();
$contact['extra1']=array();
$result['extra1'] = array();
$result['extra_name1'] = array();
foreach ($all1 as $key1 => $value1) {
	$f_value1 = $db->field("SELECT value FROM contact_field WHERE customer_id='".$in['contact_id']."' AND field_id='".$value1['field_id']."' ");
	if($value1['is_dd']==1){
		$result['extra1'][$value1['field_id']]=build_extra_contact_field_dd('',$value1['field_id']);
		if($in['viewMod']){
			$f_value1 = $db->field("SELECT name FROM contact_extrafield_dd WHERE extra_field_id='".$value1['field_id']."' ");
		}
		if(!$f_value1){
			$f_value1 = '0';
		}
	}
	$result['extra_name1'][$value1['field_id']] = $value1['label'];
	
	$contact['extra1'][$value1['field_id']]=$f_value1;
}
*/
$i=0;
$default_show=array();
$def=$db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact' AND value=1 ");
while ($def->next()) {
	$line = array();
	/*$line[$def->f('field')] = $def->f('value') == 1 ? true : false;*/
	$result['hide'][$def->f('field').'_if'] = $def->f('creation_value') == 1 ? true : false;
}



if($in['contact_id'] == 'tmp' || !$in['contact_id']){
	$do_next ='customers-Contact-customers-contact_add';
	$is_add = true;
	$result['contact_id'] = 'tmp';
}else{
	$do_next ='customers-Contact-customers-contact_update';
	$is_add = false;
	$address = $db->query("SELECT * FROM  customer_contact_address WHERE contact_id='".$in['contact_id']."' and is_primary=1 ")->getAll();
	foreach ($address[0] as $key => $value) {
		$contact[$key] = $value;
	}
	$add_id = $db->query("SELECT * FROM customer_contact_addresses WHERE contact_id='".$in['contact_id']."' ");
	$title = $db->field("SELECT title FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' ");
	$title_contact = $db->field("SELECT title FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
	if($add_id->next()){
		$c_address = $db->query("SELECT customer_addresses.*,state.name AS state,country.name AS country  FROM customer_addresses
								 LEFT JOIN country ON country.country_id=customer_addresses.country_id
						 		 LEFT JOIN state ON state.state_id=customer_addresses.state_id
						  		 WHERE address_id='".$add_id->f('address_id')."'");
		$contact['attachlocation'] = array(
			'caddress'			=> nl2br($c_address->f('address')),
	  		'czip'			    => $c_address->f('zip'),
	  		'ccity'			    => $c_address->f('city'),
	  		'cstate'			=> $c_address->f('state'),
	  		'ccountry'			=> $c_address->f('country')
		);
		
	}

	$result['accounts'] = get_contactAccounts($in);

}
$contact['do'] = $do_next;

$result['is_admin']					= $_SESSION['access_level'] == 1 ? true : false;
$contact['title_id']				= $title;
$contact['title_contact_id']		= $title_contact;
$result['title']					= build_contact_title_type_dd('');
$result['department']				= build_contact_dep_type_dd('');
$result['language']					= build_language_dd('');
$result['country_dd']				= build_country_list('');
$result['position']					= build_contact_function('');
$result['customer_dd']				= get_customers($in);
$result['format']					= 'dd/MM/yyyy';
$result['showLinkAccount']			= false;
$result['dateOptions'] 				= array(
							            'formatYear'	=> 'YYYY',
							            'startingDay' 	=> 1
							        );

$result['viewMod']				= $in['viewMod'];
if($in['viewMod']){
	$result['contact_id']			= $in['contact_id'];
}

$result['obj']						= $contact;
if(!$in['viewMod']){
	$result['obj']['customer_id']	= isset($in['customer_id']) ? $in['customer_id'] : '';
}

foreach ($in as $key => $value) {
	if(!array_key_exists($key, $result['obj'])) {
		$result['obj'][$key] = $in[$key];
	}
}
$result['obj']['contact_name'] = $result['obj']['firstname'].' '.$result['obj']['lastname'];

$result['ADV_CRM']					= defined('ADV_CRM') && ADV_CRM == 1 ? true : false;

json_out($result);

function get_function($name){
	$q = strtolower(trim($in["term"]));
	$q = $name;
	$filter = " 1=1 ";
	if($q){
		$filter = " name LIKE '%".$q."%' ";
	}
	$result = array();
	$db= new sqldb();
	$func = $db->query("SELECT * FROM customer_contact_job_title WHERE $filter ORDER BY sort_order ")->getAll();
	foreach ($func as $key => $value) {
		$result[] = array('id'=>$value['id'], 'name'=>$value['name']);
	}
	return $result;
}

function get_customers($in){
	$q = strtolower(trim($in["term"]));
	$filter =" AND is_admin='0' ";
	if($q){
		$filter = " name LIKE '%".$q."%' ";
	}
	$result = array();
	$db= new sqldb();
	$func = $db->query("SELECT customers.name,customers.customer_id FROM customers
				WHERE customers.active=1 $filter ORDER BY customers.name ")->getAll();
	foreach ($func as $key => $value) {
		$result[] = array('id'=>$value['customer_id'], 'name'=>$value['name']);
	}
	return $result;
}

function get_contactAccounts($in){
	$db= new sqldb();
	$position 				= build_contact_function('',true);
	$department				= build_contact_dep_type_dd('',true);
	$title					= build_contact_title_type_dd('',true);
	$result = $db->query("SELECT customers.name, customer_contactsIds.* FROM customers INNER JOIN customer_contactsIds ON customers.customer_id=customer_contactsIds.customer_id WHERE customer_contactsIds.contact_id='".$in['contact_id']."' ORDER BY customer_contactsIds.primary DESC , customers.name ASC  ")->getAll();
	$i = 0;
	foreach ($result as $key => $value) {
		$result[$key]['s_email'] = $result[$key]['s_email'] == 1 ? true : false;
		/*foreach ($position as $k => $v) {
			if($value['position'] == $v['id']){
				$result[$key]['position_txt'] = $v['name'];
			}
		}*/
		$result[$key]['position_txt'] 		= $position[$value['position']];
		$result[$key]['department_txt'] 	= $department[$value['department']];
		$result[$key]['title_txt'] 			= $title[$value['title']];
		/*foreach ($department as $k => $v) {
			if($value['department'] == $v['id']){
				$result[$key]['department_txt'] = $v['name'];
			}
		}*/

		$result[$key]['s_email_txt'] = $result[$key]['s_email'] == 1 ? gm('Yes') : gm('No');
		// $result[$key]['s_email'] = $result[$key]['s_email'] == 1 ? true : false;
		if($i==0){
			$result[$key]['collapse'] = true;
		}
		$i++;
	}
	return $result;
}


// pentru popularea noii tabele customer_contactsIds
// insert into customer_contactsIds (customer_id,contact_id) select customer_id, contact_id FROM customer_contacts WHERE customer_id<>'0'

?>