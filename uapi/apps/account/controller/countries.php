<?php

$db->query("SELECT * FROM country ORDER BY name");

$out_str=array();
while($db->move_next()){
    array_push($out_str, array(
        'id'=>$db->f('country_id'),
        'code'=>$db->f('code'),
        'name'=>$db->f('name')
    ));
}
json_out($out_str);
