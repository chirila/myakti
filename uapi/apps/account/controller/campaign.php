<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/ 

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
        json_out($fname($in));
    }else{
        msg::error('Function does not exist','error');
        json_out($in);
    }
}

json_out($in);


function get_camp($in){
    if(!defined('CAMPAIGN_MONITOR_ACTIVE') || CAMPAIGN_MONITOR_ACTIVE != 1){
        return array();
    }
    
    global $config;
    $db = new sqldb();
    $data = array('list'=>array());
    ark::loadLibraries(array('campaignmonitor/campaign_all'));
    
    $wrap = new CS_REST_Clients(ACCOUNT_CAMPAIGN_CLIENT,ACCOUNT_CAMPAIGN_APY);
    $wrap_list = new CS_REST_Lists(NULL, ACCOUNT_CAMPAIGN_APY);
    $result = $wrap->get_lists();

    if($result->was_successful()) {
    	$i=0;
        foreach ($result->response as $key){
            $list = array();
        	// $view->assign(array(
        		$list['NAME']			= $key->Name;
        		$list['LIST_ID']		= $key->ListID;
                $list['delete_link']    = array('do'=>'customers-campaign-campaign-delete_list', 'xget'=> 'camp', 'list_id'=>$key->ListID );
                $list['refresh_link']   = array('do'=>'customers-campaign-campaign-refresh', 'xget'=> 'camp', 'list_id'=>$key->ListID );
        	// ),'lists');
        	
        	# get the stats of the list    	
            $wrap_list->set_list_id($key->ListID);
        	$result_list = $wrap_list->get_stats();
    		if($result_list->was_successful()) {
    			// $view->assign(array(
    				$list['SUBSCRIBERS'] = $result_list->response->TotalActiveSubscribers;
    			// ),'lists');		    
    		} else {
    		    msg::$error = $result_list->response;
    		}
            # end get the stats of the list
        	// $view->loop('lists');
            array_push($data['list'], $list);
        	// $i++;
        }
       //  if($i==0){
       //  	$view->assign('HIDE_SUB','hide');
    			// $view->assign("MSG_NOTICE", '<div class="info info_big">[!L!]No data to display[!/L!]</div>');
       //  }
    } else {
        msg::error ( $result->response->Message,'error');
    }
    $count_contact_list = $db->field("SELECT COUNT(contact_list_id) FROM contact_list");
    $is_contact_list = 0;
    if($count_contact_list>0){
        $is_contact_list = 1;    
    }


    // $view->assign(array(
    	$data['CLIENT_ID']	 	    = ACCOUNT_CAMPAIGN_CLIENT;
        $data['is_data']           = $i > 0 ? true : false;
        $data['is_contact_list']   = $is_contact_list;
        $data['confirm']  =    gm('Confirm');
        $data['ok']   =    gm('Ok');
        $data['cancel']=    gm('Cancel');
        $data['no_delete'] = true;
        $data['model']      = 'campaign';
    // ));

    return $data;
}

function get_CampaignEditList($in){
    $db = new sqldb();
    $data = array('list'=>array());

    // ark::loadLibraries(array('campaignmonitor/campaign_all'));

    // $wrap = new CS_REST_Lists($in['list_id'], ACCOUNT_CAMPAIGN_APY);
    // $result = $wrap->get();

    $name = $in['name'];
    $act = 'company-progress_bar-campaign-add_subscriber';
    $email = $in['email'];
    $hide_e = 'hide';
    $hide_a = 'hide';
    // $view_add->assign(array(    
        $data['list_name']     = $result->response->Title;
    // ));
    // if($in['email_sub']){
    //     $view_add->assign('HIDE_A','');
    //     $wrap = new CS_REST_Subscribers($in['list_id'], ACCOUNT_CAMPAIGN_APY);  
        
    //     $result = $wrap->get($in['email_sub']); 
    //     if($result->was_successful()) {
    // //      print_r($result->response);
    //         $name = $result->response->Name;
    //         $act = 'campaign-update_subscriber';
    //         $email = $result->response->EmailAddress;
    //     } else {
    //         $in['error'] = $result->response->Message;
    //         $act = '';
    //     }
    // }else{
        // $view_add->assign('HIDE_E','');
        $hide_e = '';
        $hide_e = '';
        $i = 0;
        $eee = $db->query("SELECT * FROM contact_list");        
        while ($eee->next()) {
            $data['list'][] = array(
                'NAME2'         => $eee->f('name'),
                'LISTS_ID'      => $eee->f('contact_list_id'),
                'CONTACTS'      => $eee->f('contacts'),
                // 'CHECKED'       => $company == $eee->f('contact_list_id') ? 'CHECKED' : '',
                'TYPE'          => $eee->f('list_type') == 1 ? gm('Static list') : gm('Dynamic list'),
            );
            // $view_add->loop('subscribers');
            $i++;
        }
        /*if($i == 0 ){
            $view_add->assign('HIDE_SUB','hide');
            $view_add->assign("MSG_NOTICE", '<div class="info info_big">[!L!]No date to display[!/L!]</div>');  
        }*/
    // }
        switch ($in['app']) {
            case 'chimp':
                $app = 'chimp';
                break;
            
            default:
                $app = 'campaign';
                break;
        }

    // $view_add->assign(array(
        $data['HIDE_E']        = $hide_e;
        $data['HIDE_A']        = $hide_a;
        $data['NAME1']         = $name;
        $data['ACT']           = $act;
        $data['EMAIL']         = $email;
        $data['list_id']       = $in['list_id'];
        $data['app']           = $app;
        $data['client_id']     = ACCOUNT_CAMPAIGN_CLIENT;
        $data['EMAIL_SUB']     = $in['email_sub'];
        $data['is_data']       = $i > 0 ? true : false;
        $data['lists_id']      =  $db->field("SELECT contact_list_id FROM campaign_list WHERE campaign_list_id='".$in['list_id']."' ");
    // ));


    return $data;
}

function get_campCampaigns($in){
    if(!defined('CAMPAIGN_MONITOR_ACTIVE') || CAMPAIGN_MONITOR_ACTIVE != 1){
        return array();
    }
    global $config;
    $db = new sqldb();
    $data = array('listC'=>array());
    
    ark::loadLibraries(array('campaignmonitor/campaign_all'));

    $wrap = new CS_REST_Clients(ACCOUNT_CAMPAIGN_CLIENT,ACCOUNT_CAMPAIGN_APY);
    $wrap_camp = new CS_REST_Campaigns(NULL, ACCOUNT_CAMPAIGN_APY);
    $result = $wrap->get_campaigns();

    if($result->was_successful()) {
    //    print_r($result->response);
        // console::log($result->response);
            $i = 0;
        foreach ($result->response as $key){
            $data['listC'][] = array(
                'NAME'              => $key->Name,
                'SENT'              => date(ACCOUNT_DATE_FORMAT,strtotime($key->SentDate)),
                'RECIPIENTS'        => $key->TotalRecipients,
                'campaign_id'       => $key->CampaignID,
                'save_link'         => array('do'=>'customers-campaign-campaign-sync_campaign_monitor', 'xget'=> 'campCampaigns', 'campaign_id'=>$key->CampaignID, 'chimp'=>'false' ),
            );
    //      $campaign_id = $key->CampaignID;
            # get the campaign summary
        /*  $wrap_camp->set_campaign_id($key->CampaignID);
                $result_camp = $wrap_camp->get_summary();
            // console::log($result_camp->response);
                if($result_camp->was_successful()) {
                    $view->assign(array(
                        'OPEN'  => $result_camp->response->UniqueOpened,
                        'CLICK' => $result_camp->response->Clicks
                    ),'campaigns');
            //          print_r($result_camp->response);
                } else {
                    msg::$error = $result_camp->response;
                }*/
            # end get the campaign summary
            
            // $i++;
        }
    } else {
        msg::error ( $result->response->Message,'error');
    }

    /*$view->assign(array(
        'CLIENT_ID'     => ACCOUNT_CAMPAIGN_CLIENT,
        'HIDE'          => 'hide',
        'is_data'       => $i > 0 ? true : false,
    ));*/
    $data['loadedCampaigns']=true;
    return $data;
}

function get_recipient_activitycamp($in){
    global $config;
    $db = new sqldb();
    $data = array('list'=>array());

    ark::loadLibraries(array('campaignmonitor/campaign_all'));

    $wrap = new CS_REST_Campaigns($in['campaign_id'], ACCOUNT_CAMPAIGN_APY);
    $result = $wrap->get_opens(date('Y-m-d', 0), 1, 300, 'email', 'asc');
    $result_b = $wrap->get_bounces(date('Y-m-d', 0), 1, 300, 'email', 'asc');
    $result_u = $wrap->get_unsubscribes(date('Y-m-d', 0), 1, 300, 'email', 'asc');
    // console::log($result_u->response);
    //$result = $wrap->get_opens(date('Y-m-d', strtotime('-30 days')), page, page size, order field, order direction);

    $result_list = $wrap->get_lists_and_segments();
    // console::log($result_list->response);
    $list = array();
    if($result_list->was_successful()) {
        // console::log('list',$result_list->response);
        if(count($result_list->response->Lists) > 0){
            foreach ($result_list->response->Lists as $key){
                $wrap_list = new CS_REST_Lists($key->ListID, ACCOUNT_CAMPAIGN_APY);
                $result_list_a = $wrap_list->get_active_subscribers(date('Y-m-d', 0));
                // console::log($result_list_a->response);
                if($result_list_a->was_successful()) {
                    // console::log('active',$result_list_a->response);
                    foreach ($result_list_a->response->Results as $keys){
                        $list[$keys->EmailAddress]=$keys->Name;     
                    }
                }       
            }
        }else if (count($result_list->response->Segments) > 0) {
            foreach ($result_list->response->Segments as $key){
                $wrap_list = new CS_REST_Lists($key->ListID, ACCOUNT_CAMPAIGN_APY);
                $result_list_a = $wrap_list->get_active_subscribers(date('Y-m-d', 0));
                // console::log($result_list_a);
                if($result_list_a->was_successful()) {
                    // console::log('active',$result_list_a->response);
                    foreach ($result_list_a->response->Results as $keys){
                        $list[$keys->EmailAddress]=$keys->Name;     
                    }
                }       
            }
        }
    }
    // console::log($list);
    #glob['opened']
    # undefined - i have no idea
    # 1 - opened
    # 2 - bounces
    # 3 - unsubscribed


    $email = array();
    $email_b = array();
    $email_u = array();
    $i=0;
    if($result->was_successful()) {
       // console::log('opened',$result->response);
        foreach ($result->response->Results as $key){
            if(array_key_exists($key->EmailAddress,$email)){
                $email[$key->EmailAddress] = $email[$key->EmailAddress]+1;
            }else{
                $email[$key->EmailAddress] = 1;
            }
        }
        #for bounces
        foreach ($result_b->response->Results as $key){
            $email_b[$key->EmailAddress] = $key->Reason;            
        }
        #for unsubscribs
        foreach ($result_u->response->Results as $key ) {
            $email_u[$key->EmailAddress] = $key->Date;
        }
        // console::log($email_b);
        if($in['opened'] == 1){
            foreach ($email as $key => $value){         
                $data['list'][] = array(
                    'EMAIL'     => $key,
                    'OPENS'     => $value,
                    'NAME'      => $list[$key]
                );
                // $view->loop('activity');
                // $i++;
            }
            $data['TEXT']=gm('Opened');
        }elseif ($in['opened'] == 2){           
            foreach ($email_b as $key => $value){
                $data['list'][] = array(
                    'EMAIL'     => $key,
                    'OPENS'     => $value,
                    'NAME'      => $list[$key]
                );
                // $view->loop('activity');
                // $i++;
            }
            $data['TEXT']=gm('Reason');
        }elseif ($in['opened'] == 3) {
            foreach ($email_u as $key => $value){
                $data['list'][] = array(
                    'EMAIL'     => $key,
                    'OPENS'     => $value,
                    'NAME'      => $list[$key]
                );
                // $view->loop('activity');
                // $i++;
            }
            $data['TEXT']=gm('Date');
        }
        else{
            $campaign = array();
                $result_camp = $wrap->get_recipients();
                if($result_camp->was_successful()) {
                    foreach ($result_camp->response->Results as $key){
                        $campaign[$key->EmailAddress] = 1;
                    }
                //    print_r($result_camp->response);
                }
            $email = array_merge($email,$email_b);
            $diff = array_diff_key($campaign, $email);
            foreach ($diff as $key => $value){
                $data['list'][] = array(
                    'EMAIL'     => $key,
                    'OPENS'     => 0,
                    'NAME'      => $list[$key]
                );
                // $view->loop('activity');
                // $i++;
            }
            $data['TEXT']=gm('Opened');
        }
    } else {
        msg::$error = $result->response;
    }

    // $view->assign(array(
    //     'CLIENT_ID'     => ACCOUNT_CAMPAIGN_CLIENT,
    //     'is_data'       => $i > 0 ? true : false,
    // ));
    $data['CLIENT_ID'] = ACCOUNT_CAMPAIGN_CLIENT;

    return $data;
}

function get_recipient_activitychimp($in){
    ark::loadLibraries(array('MCAPI.class'));
    $data = array('list'=>array());
    $chimp_c = new MCAPI(MAIL_CHIMP);
    $i=0;
    if($in['opened']){
        if($in['opened']=='1')
        {
            $result = $chimp_c->campaignOpenedAIM($in['campaign_id']);
            foreach ($result['data'] as $key){
                $id = $chimp_c->listsForEmail($key['email']);
                $name = $chimp_c->listMemberInfo($id[0],array($key['email']));
                $data['list'][] =array(
                        'EMAIL'             => $key['email'],
                        'OPENS'             => $key['open_count'],
                        'NAME'              => $name['data'][0]['merges']['FNAME'].' '.$name['data'][0]['merges']['LNAME']
                );
                // $view->loop('activity');
                $i++;
            }
        }else
        {
            $result = $chimp_c->campaignBounceMessages($in['campaign_id']);
            foreach ($result['data'] as $key){
                $id = $chimp_c->listsForEmail($key['email']);
                $name = $chimp_c->listMemberInfo($id[0],array($key['email']));
                $data['list'][] =array(
                        'EMAIL'             => $key['email'],
                        'OPENS'             => 0,
                        'NAME'              => $name['data'][0]['merges']['FNAME'].' '.$name['data'][0]['merges']['LNAME']
                );
                // $view->loop('activity');
                $i++;
            }
        }
    }else {
        $result = $chimp_c->campaignNotOpenedAIM($in['campaign_id']);
        foreach ($result['data'] as $key){
            $id = $chimp_c->listsForEmail($key);
            $name = $chimp_c->listMemberInfo($id[0],array($key));
            $data['list'][] = array(
                    'EMAIL'             => $key,
                    'OPENS'             => 0,
                    'NAME'              => $name['data'][0]['merges']['FNAME'].' '.$name['data'][0]['merges']['LNAME']
            );
            // $view->loop('activity');
            $i++;
        }
    }
    return $data;
}

function get_chimp($in){
    if(!defined('MAIL_CHIMP_ACTIVE') || MAIL_CHIMP_ACTIVE != 1){
        return array();
    }
    
    global $config;
    $data = array('list'=>array());
    ark::loadLibraries(array('MCAPI.class'));

    $chimp = new MCAPI(MAIL_CHIMP);

    $list = $chimp->lists();

    if ($chimp->errorCode){
        // $view->assign('HIDE_SUB','hide');
        msg::notice ($chimp->errorMessage,'notice');
        // $view->assign("MSG_NOTICE", '<div class="info info_big">'.$chimp->errorMessage.'</div>');
    }else{
        $i = 0;
        foreach ($list['data'] as $key){
            $data['list'][] = array(
                'NAME'              => $key['name'],
                'LIST_ID'           => $key['id'],
                'SUBSCRIBERS'       => $key['stats']['member_count'],
                'refresh_link'      => array('do'=>'customers-campaign-chimp-refresh', 'xget'=> 'chimp', 'list_id'=>$key['id'] )
            );
            // $view->loop('lists');
            $i++;
        }
        if($i==0){
            // $view->assign('HIDE_SUB','hide');
            msg::notice (gm('No data to display'),'notice');
            // $view->assign("MSG_NOTICE", '<div class="info info_big">'.gm('No data to display').'</div>');
        }
    }
    $data['no_delete']  = false;
    $data['confirm']    = gm('Confirm');
    $data['ok']         = gm('Ok');
    $data['cancel']     = gm('Cancel');
    $data['model']      = 'chimp';

    return $data;
}
function get_chimpCampaigns($in){
    if(!defined('MAIL_CHIMP_ACTIVE') || MAIL_CHIMP_ACTIVE != 1){
        return array();
    }
    global $config;
    $db = new sqldb();
    $data = array('listC'=>array());
    
    ark::loadLibraries(array('MCAPI.class','Mailchimp'));

    $chimp_c2 = new Mailchimp(MAIL_CHIMP);
    $result2 = $chimp_c2->campaigns->getList();
    console::log($result2);
    if(empty($result2['error'])){
      foreach ($result2['data'] as $c) {
        $data['listC'][] = array(
          'NAME'          => $c['title'],
          'SENT'          => $c['send_time'] ? $c['send_time'] : '&nbsp;',
          'RECIPIENTS'    => $c['emails_sent'] ? $c['emails_sent'] : '0',
          'campaign_id'   => $c['id'],
          'OPEN'          => $c['summary']['opens'] ? $c['summary']['opens'] : '0',
          'CLICK'         => $c['summary']['clicks'] ? $c['summary']['clicks'] : '0',
          // 'json'          => htmlentities(json_encode($c['summary'])),
        );
        // $view->loop('campaigns');
        $i++;
      }
    }else{
      // $view->assign('HIDE_T','hide');
        msg::notice($result2['error'],'notice');
      // $view->assign("MSG_NOTICE", '<div class="info info_big">'.$result2['error'].'</div>');
    }
   
    $data['loadedCampaigns']=true;
    return $data;
}