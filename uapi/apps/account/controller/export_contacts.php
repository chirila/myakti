<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('memory_limit','786M');
setcookie('Akti-Contact-Export','6-0-0',time()+3600,'/');
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

ark::loadLibraries(array('PHPExcel'));

$filter = "1=1";
$filename ="contacts_report.xls";
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
               ->setLastModifiedBy("Akti")
               ->setTitle("Contacts report")
               ->setSubject("Contacts report")
               ->setDescription("Contacts export")
               ->setKeywords("office PHPExcel php")
               ->setCategory("Contacts");

if($in['search']){
  $filter .= " AND (firstname LIKE '%".$in['search']."%' OR lastname LIKE '%".$in['search']."%' OR company_name LIKE '%".$in['search']."%' OR phone LIKE '%".$in['search']."%' OR cell LIKE '%".$in['search']."%' OR email LIKE '%".$in['search']."%')";
  $arguments_s.="&search=".$in['search'];
}

if($in['first'] && $in['first'] == '[0-9]'){
  $filter.=" AND SUBSTRING(LOWER(lastname),1,1) BETWEEN '0' AND '9'";
  $arguments.="&first=".$in['first'];
} elseif ($in['first']){
  $filter .= " AND lastname LIKE '".$in['first']."%'  ";
  $arguments.="&first=".$in['first'];
}

if(!$in['archived']){
  $filter.= " AND customer_contacts.active=1";
}else{
  $filter.= " AND customer_contacts.active=0";
  $arguments.="&archived=".$in['archived'];
}
if($in['front_register']){
  $filter.= " AND customer_contacts.front_register=1";
}else{
  $filter.= " AND customer_contacts.front_register=0";
  $arguments.="&front_register=".$in['front_register'];
}
$arguments =$arguments.$arguments_s;
$language_array = array();
$job_title_array= array();
$job_dep_array= array();
$language='';
$c_types_name='';
$department_names='';
$language = $db->query("SELECT * FROM lang ORDER BY name");
while($language->move_next()){
  $language_array[$language->f('lang_id')] = $language->f('name');
}
$language = $language_array['2'];


$c_types_name = $db->query("SELECT * FROM customer_contact_job_title");
while($c_types_name->move_next()){
  $job_title_array[$c_types_name->f('id')] = $c_types_name->f('name');
}
$department_names = $db->query("SELECT * FROM customer_contact_dep");
while($department_names->move_next()){
  $job_dep_array[$department_names->f('id')] = $department_names->f('name');
}




$info = $db->query("SELECT customer_contacts.firstname,customer_contacts.lastname,customer_contacts.company_name,
                            customer_contacts.cell,customer_contacts.active,customer_contacts.front_register,
                            customer_contacts.sex,customer_contacts.note,customer_contacts.birthdate,customer_contacts.language,
                            customer_contactsIds.position,customer_contactsIds.department,customer_contactsIds.title,
                            customer_contactsIds.email,customer_contactsIds.phone,customer_contactsIds.fax,customer_contacts.contact_id,customers.name as Cname
                    FROM customer_contacts
                    INNER JOIN customer_contactsIds ON customer_contacts.contact_id=customer_contactsIds.contact_id
                    LEFT JOIN customers ON customer_contactsIds.customer_id=customers.customer_id
                    WHERE ".$filter." ORDER BY customer_contacts.contact_id");



$i=0;
$objPHPExcel->getActiveSheet(0)->getStyle('A1:N1')->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)
          ->setCellValue('A1',gm("Contact id"))
      ->setCellValue('B1',gm("First Name"))
      ->setCellValue('C1',gm("Last Name"))
      ->setCellValue('D1',gm("Gender"))
      ->setCellValue('E1',gm("Birthdate"))
      ->setCellValue('F1',gm("Address"))
      ->setCellValue('G1',gm("Company"))
      ->setCellValue('H1',gm("Department"))
      ->setCellValue('I1',gm("Function"))
      ->setCellValue('J1',gm("Email"))
      ->setCellValue('K1',gm("Phone"))
      ->setCellValue('L1',gm("Cell"))
      ->setCellValue('M1',gm("Language"))
      ->setCellValue('N1',gm("Note"));
$xlsRow = 2;
while ($info->next())
{
  if($info->f('sex')==0)
  {
    $sex = '';
  }
  elseif ($info->f('sex')==1)
  {
    $sex = 'male';
  }
  else
  {
    $sex = 'female';
  }
  if($info->f('language')!=0)
  {
    /*$language = $db->field("SELECT name FROM lang WHERE lang_id=".$info->f('language'));*/
    $language = $lang_array[$info->f('language')];
  }
  $date = '';
  if($info->f('birthdate')){
    $date = date('d/m/Y',$info->f('birthdate'));
  }
  $c_types = '';
  if($info->f('position')){
    /*$pos = explode(',', $info->f('position'));*/
/*    foreach ($pos as $key) {
      
      $c_types .= $c_types_name.', ';
    }*/
    $c_types = $job_title_array[$info->f('position')];
  }
  $address_info = $db->query("SELECT * FROM customer_contact_address WHERE contact_id = '".$info->f('contact_id')."' AND is_primary = '1' ");
  $address = '';
  if($address_info->next()){
    $address = $address_info->f('address').'
'.$address_info->f('zip').'  '.$address_info->f('city').'
'.get_country_name($address_info->f('country_id'));
  }
  $department_name = '';
  if($info->gf('department')){
   /* $department_name = $db->field("SELECT name FROM customer_contact_dep WHERE id = '".$info->gf('department')."'");*/
   $department_name = $job_dep_array[$info->f('department')];
  }
  
  $objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $info->f('contact_id'))
        ->setCellValue('B'.$xlsRow, $info->f('firstname'))
        ->setCellValue('C'.$xlsRow, $info->f('lastname'))
        ->setCellValue('D'.$xlsRow, $sex)
        ->setCellValue('E'.$xlsRow, $date)
        ->setCellValue('F'.$xlsRow, $address)
        ->setCellValue('G'.$xlsRow, $info->f('Cname'))
        ->setCellValue('H'.$xlsRow, $department_name)
        ->setCellValue('I'.$xlsRow, $c_types)
        ->setCellValue('J'.$xlsRow, $info->f('email'))
        ->setCellValue('K'.$xlsRow, $info->f('phone'))
        ->setCellValue('L'.$xlsRow, $info->f('cell'))
        ->setCellValue('M'.$xlsRow, $language)
        ->setCellValue('N'.$xlsRow, $info->f('note'));
        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($xlsRow)->setRowHeight(-1);
    $xlsRow++;
}

// set column width to auto
foreach(range('A','N') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

@mkdir('upload/'.DATABASE_NAME."/",0755);

$objPHPExcel->getActiveSheet()->setTitle('Contacts report');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.DATABASE_NAME."/".$filename);
define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}

// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    setcookie('Akti-Contact-Export','9-0-0',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();
?>