<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
$result =array();
if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}

json_out($result);

function get_customerFields($in){
	
	$result = array('customerFields'=>array('extraList'=>array()));
	$db = new sqldb();
	$label = $db->query("SELECT * FROM customer_fields ORDER BY field_id ")->getAll();
	$all = $label;

	foreach ($all as $key => $value) {
		$loop = 'extra_field';

		$f_value = $db->field("SELECT value FROM customer_field WHERE field_id='".$value['field_id']."' ");

		$result['customerFields']['extraList'][] = array(
			'field_id'			=> $value['field_id'],
			'label'				=> $value['label'],
			'value'				=> $value['value'] == 1 ? true : false,
			'value_default'			=> $value['default_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
			'value_creation'			=> $value['creation_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
			// 'value_def'			=> $value['default_value'] == 1 ? 'checked' :($value['value'] == 0 ? 'disabled' : ''),

		);		
	}

	$db->query("SELECT * FROM customise_field WHERE controller='company-customer' ");

	while ($db->next()) {
		$result['customerFields'][$db->f('field')]	 		= $db->f('value') == 1 ? true : false;
		$result['customerFields'][$db->f('field').'_d']		= $db->f('default_value') == 1 ? true : ($db->f('value') == 0 ? 'disabled' : false);
		$result['customerFields'][$db->f('field').'_c']		= $db->f('creation_value') == 1 ? true : ($db->f('value') == 0 ? 'disabled' : false);
	}
	
	$in['various1'] = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 1' ");
	$in['various2'] = $db->field("SELECT value FROM default_data WHERE default_main_id='0' AND type='customer_field' AND default_name='Variuos 2' ");

	$result['customerFields']['various1_text']			= $in['various1'] ? $in['various1'] : gm('Various 1');
	$result['customerFields']['various2_text']			= $in['various2'] ? $in['various2'] : gm('Various 2');
	$result['customerFields']['xget'] = 'customerFields';
	$result['customerFields']['controller'] = 'company-customer';

	
	return $result;
}

function get_contactFields($in){
	
	$result = array('contactFields'=>array('extraList'=>array()));
	$db = new sqldb();
	$label = $db->query("SELECT * FROM contact_fields ORDER BY field_id ")->getAll();
	$all = $label;

	foreach ($all as $key => $value) {
		$loop = 'extra_field';

		$f_value = $db->field("SELECT value FROM contact_field WHERE field_id='".$value['field_id']."' ");

		$result['contactFields']['extraList'][] = array(
			'field_id'			=> $value['field_id'],
			'label'				=> $value['label'],
			'value'				=> $value['value'] == 1 ? true : false,
			'value_default'			=> $value['default_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
			'value_creation'			=> $value['creation_value'] == 1 ? true : ($value['value'] == 0 ? 'disabled' : false),
			// 'value_def'			=> $value['default_value'] == 1 ? 'checked' :($value['value'] == 0 ? 'disabled' : ''),

		);		
	}

	$db->query("SELECT * FROM customise_field WHERE controller='company-xcustomer_contact' ");

	while ($db->next()) {
		$result['contactFields'][$db->f('field')]	 		= $db->f('value') == 1 ? true : false;
		$result['contactFields'][$db->f('field').'_d']		= $db->f('default_value') == 1 ? true : ($db->f('value') == 0 ? 'disabled' : false);
		$result['contactFields'][$db->f('field').'_c']		= $db->f('creation_value') == 1 ? true : ($db->f('value') == 0 ? 'disabled' : false);
	}	
	
	return $result;
}

function get_extraFields($in){
	
	$result = array('extraFields'=>array('customer_fields'=>array(),'contact_fields'=>array()));
	$db = new sqldb();
	$i=0;
	$j=0;
	$c_fields = $db->query("SELECT * FROM customer_fields ORDER BY field_id ");
	while ($c_fields->next()) {
		$i++;
		if($c_fields->f('is_dd'))
		{
			$has_values = false;
			$values = $db->query("SELECT name FROM customer_extrafield_dd WHERE extra_field_id='".$c_fields->f('field_id')."'");
			if($values->next())
			{
				$has_values=true;
			}
		}

		$result['extraFields']['customer_fields'][]=array(
			'field_id'	=> $c_fields->f('field_id'),
			'label'		=> gm('Field').' '.$i,
			'value'		=> $c_fields->f('label'),
			'checked'	=> $c_fields->f('is_dd')? true: false,
			'warn'		=> $has_values ? '1': '0',
		);		
	
	}
	$c_fields = $db->query("SELECT * FROM contact_fields ORDER BY field_id ");
	while ($c_fields->next()) {
		$j++;
		if($c_fields->f('is_dd'))
		{
			$has_values = false;
			$values = $db->query("SELECT name FROM customer_extrafield_dd WHERE extra_field_id='".$c_fields->f('field_id')."'");
			if($values->next())
			{
				$has_values=true;
			}
		}
		$result['extraFields']['contact_fields'][]=array(
			'field_id'			=> $c_fields->f('field_id'),
			'label'			=> gm('Field').' '.$j,
			'value'			=> $c_fields->f('label'),
			'checked'		=> $c_fields->f('is_dd')? true: false,
			'warn'			=> $has_values ? '1': '0',
		);
		
	}	
	
	return $result;
}

function get_custRef($in){
	$db = new sqldb();
	$use = $db->field("SELECT value FROM settings WHERE constant_name = 'USE_COMPANY_NUMBER'");
	$customers = $db->field("SELECT count(customer_id) FROM customers WHERE our_reference='' ORDER BY `customers`.`creation_date` ");
	$result = array('custRef'=>array(
		'inc_part_c'			=> $db->field("SELECT value FROM settings WHERE constant_name = 'ACCOUNT_COMPANY_DIGIT_NR'"),
		'first_reference_c' 	=> $db->field("SELECT value FROM settings WHERE constant_name = 'FIRST_REFERENCE_C'"),
		'use_company_number' 	=> $use == 1 ? true : false,
		'totalcustomers'		=> $customers,
	));

	return $result;
}

function get_paymentTerm($in){
	$db = new sqldb();
	
	$result = array('paymentTerm'=>array(
		'payment_term'  			=> $db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' "),
		'choose_payment_term_type' 	=> $db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' "),
	));

	return $result;
}

function get_restore($in){
	$db = new sqldb();


	set_time_limit(0);
    ini_set('memory_limit', '-1');   
		$db = new sqldb();
		$db2 = new sqldb();
		$in['crm'] = '0';
		$in['crm2'] = '0';
		$file1=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup1.xml';
		$file2=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup2.xml';
		$file3=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup3.xml';
		$file_fill1 = file_get_contents($file1);
		$file_fill2 = file_get_contents($file2);
		$file_fill3 = file_get_contents($file3);

		if($file_fill1==true&&$file_fill2==true&&$file_fill3==true){
			$show_file=1;
		}
		$result = array('erase'=>array(
			'file'				=>    backupcrm2($in['crm']),
			'filebackup'		=>    backupcrm3($in['crm']),
			'file_id'			=>	  $in['crm'],
			'file2_id'			=>	  $in['crm2'],
			'show_file'			=>    $show_file==1?true:false,
			'class1'			=> 	  $class1,
			'class2'			=> 	  $class2,
			'class3'			=> 	  $class3,
			'history'			=>    array(),
			'confirm'			=>    gm('Confirm'),
			'ok'				=>    gm('Ok'),
			'cancel'			=> 	  gm('Cancel'),
		));


	return $result;
}


// INSERT INTO `customise_field` (`controller`, `field`, `value`, `default_value`, `creation_value`) VALUES ('company-xcustomer_contact', 'create', '1', '', '');

?>