<?php
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
if(!defined('BASEPATH')) exit('No direct script access allowed');

$output=array();

global $database_config;
$db_config = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_users = new sqldb($db_config);
$filter = " is_admin='0' AND front_register=0 ";
// if we have a regular user and ONLY_IF_ACC_MANAG == 1, then we show to the user only the
// companies where he is account manager
$admin_licence = $db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1' && !in_array('company', $_SESSION['admin_sett'])){
	$filter.= " AND CONCAT( ',', user_id,  ',' ) LIKE  '%%,".$_SESSION['u_id'].",%%' "; // folosind functia sprintf pentru formarea query-ului trebuie sa ii dam escape la caracterul % deaia sunt doua %%
}
#time
$sixMonths = strtotime('6 months ago');
$twelveMonths = strtotime('12 months ago');
#filter
$filterCustomer = $filter . ' AND %3$s > '.$sixMonths.' ';
$filterCustomer12 = $filter . ' AND %3$s between ' . $twelveMonths . ' AND ' . $sixMonths . ' ';

$output['kpi_number'] = array();

$modules = array(
    array(
    	'text' 		=> gm('quote based clients'),
    	'table'		=> 'tblquote',
    	'date'		=> 'quote_date',
    	'field'		=> 'buyer_id',
    	'filters'	=> ''
    ),
	array(
		'text'		=> gm('order based clients'),
		'table'		=> 'pim_orders',
    	'date'		=> 'date',
    	'field'		=> 'customer_id',
    	'filters'	=> ''
	),
	array(
		'text'		=> gm('project based clients'),
		'table'		=> 'projects',
    	'date'		=> 'start_date',
    	'field'		=> 'customer_id',
    	'filters'	=> ''
	),
	array(
		'text'		=> gm('invoice based clients'),
		'table'		=> 'tblinvoice',
   	 	'date'		=> 'invoice_date',
   	 	'field'		=> 'buyer_id',
    	'filters'	=> ''
	));

foreach ($modules as $key => $value) {
	
	$format = 	'SELECT count(DISTINCT customers.customer_id)
				FROM customers
				INNER JOIN %1$s ON customers.customer_id=%1$s . %2$s
				WHERE '.$filterCustomer;

	$sql = sprintf($format,$value['table'],$value['field'],$value['date']);
	
	$c = $db->field($sql);

	$format = 	'SELECT count(DISTINCT customers.customer_id)
				FROM customers
				INNER JOIN %1$s ON customers.customer_id=%1$s.%2$s
				WHERE '.$filterCustomer12;

	$sql = sprintf($format,$value['table'],$value['field'],$value['date']);

	$c12 = $db->field($sql);

	$output['kpi_number'][] = array(
		    'text' => $value['text'],
		    'clients' => $c,
		    'diff' => abs($c - $c12), 
		    'class' => $c - $c12 > 0 ? 'text-success' : ( $c - $c12 == 0 ? 'text-muted' : 'text-danger' ),
		    'faclass' => $c - $c12 > 0 ? 'fa-arrow-up' : ( $c - $c12 == 0 ? 'fa-circle' : 'fa-arrow-down' ),
		);
}

$turnover = get_turnover();

$output['account_managers']	= build_acc_manager();
$output['c_type_dd']		= build_c_type_name_dd();
$output['countries'] 		= company_country_list();
$output['lead_source_dd']	= build_l_source_dd();
$output['sector_dd']		= build_s_type_dd();
$output['topCustomers']		= get_topCustomers();
$output['type_dd']			= array( array('id'=>'2', 'value'=>gm('Company')), array('id'=>'1', 'value'=>gm('Individual')) );
$output['type_dd2']			= array( array('id'=>'2', 'value'=>gm('Customer')), array('id'=>'1', 'value'=>gm('Supplier')), array('id'=>'3', 'value' =>gm('Customer')." & ".gm('Supplier')), array('id'=>'4', 'value'=>gm('Undefined')) );
$output['turnover']			= $turnover['fix'];
$output['turnoverCumulate']	= $turnover['cumulate'];

$YodaDashboardGraph = $db_users->field("SELECT value FROM user_meta WHERE name='YodaDashboardGraph' AND user_id='".$_SESSION['u_id']."' ");
console::log($YodaDashboardGraph);
$YodaDashboardGraph = explode(';', $YodaDashboardGraph);
console::log($YodaDashboardGraph);

$graphs = array('1'=>gm('Top 10 companies'),'2'=>gm('Turnover'),'3'=>gm('Cumulative'));
$graphs_option = array('1'=>'topCustomers','2'=>'turnover','3'=>'turnoverCumulate');
$output['graphs'] = array();
foreach ($graphs as $key => $value) {
	$show=false;
	if(!in_array($key, $YodaDashboardGraph)){
		$show=true;
	}
	$output['graphs'][] = array('name'=>$value,'show'=>$show,'option'=>$graphs_option[$key],'key'=>$key);
}

json_out($output);

function get_topCustomers()
{	
	$db= new sqldb();
	$negative = 1;
	if(defined('USE_NEGATIVE_CREDIT') && USE_NEGATIVE_CREDIT==1){
		$negative = -1;
	}
	$time = mktime(0,0,0,date('m'),date('j'),date('Y')-1);
	$data = array();
	$color = array('#90c3e1','#51c38b','#ffd58b','#ed82ab','#bce9f7','#51e9ed','#f9cfe6','#ffeee3','#b4e7ea','#beb2a9');
	$db->query("SELECT customers.customer_id,customers.name,customers.active,customers.is_admin,tblinvoice.f_archived,
	            SUM( CASE WHEN tblinvoice.type =2 THEN tblinvoice.amount ELSE 0 END ) AS credit,
	            SUM( CASE WHEN tblinvoice.type =2 THEN 0 ELSE tblinvoice.amount END ) AS invoice_value
	            FROM customers
	            INNER JOIN tblinvoice ON tblinvoice.buyer_id=customers.customer_id
		       	WHERE customers.is_admin=0 AND tblinvoice.f_archived=0 AND tblinvoice.invoice_date > {$time}
	            AND sent != '0'
	            GROUP BY customers.customer_id ORDER BY invoice_value DESC LIMIT 0, 10 ");
	 // customers.active=1 AND
	$i=0;
	$array = array('labels'=>array(),'data'=>array(),'opt'=>array('legend'=>false),'colors'=>array(),'chart_type'=>'doughnut');
	$backgroundColor=array("#5d97b9","#9ec1d5","#cee0ea","#eef4f8","#ebf8f2","#c3ebd7","#88d7b0","#38bc7b","#cbd4d7","#eaeeef");
	while($db->move_next()){
		if ($db->f('invoice_value') != "") {
			// echo $db->f('invoice_value').' - '.$db->f('credit')."<br>";
			$array['labels'][]=$db->f('name');
			$array['data'][]=(float)number_format( $db->f('invoice_value')-($negative*$db->f('credit')),2,'.', '' );
			$array['colors'][] = $backgroundColor[$i];
			$i++; 
		}
	}

	return $array;	
}
function get_turnover()
{	
	$db= new sqldb();
	$negative = 1;
	if(defined('USE_NEGATIVE_CREDIT') && USE_NEGATIVE_CREDIT==1){
		$negative = -1;
	}
	$inv = array();
	$ina = array(date('Y')=>array(),'cumulated '.date('Y')=>array(),date('Y')-1=>array(),'cumulated '.date('Y')-1=>array());
	$i=1;
	//current_year
	$months=array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);
	$months_credit=array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);
	$months_cumulated = array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);
	$current_year = $db->query("SELECT tblinvoice.id,tblinvoice.sent,tblinvoice.f_archived,count(tblinvoice.id) as nr, MONTH( FROM_UNIXTIME( `invoice_date` ) ) AS `month` , YEAR( FROM_UNIXTIME( `invoice_date` ) ) AS `year`,
	SUM(tblinvoice.amount) as sum_amount
	FROM `tblinvoice`
	WHERE YEAR( FROM_UNIXTIME( `invoice_date` ) )=YEAR(CURDATE()) AND not_paid ='0' AND sent='1' AND f_archived='0' AND type='0' AND c_invoice_id='0'
	GROUP BY `year` , `month` ASC");
	while ($current_year->move_next()){
		if($current_year->f('month') <= date('m')){
			$months[$current_year->f('month')]= number_format($current_year->f('sum_amount'),2,'.','');
		}
	}
	#credit note
	$current_year_credit = $db->query("SELECT tblinvoice.id,tblinvoice.sent,tblinvoice.f_archived,count(tblinvoice.id) as nr, MONTH( FROM_UNIXTIME( `invoice_date` ) ) AS `month` , YEAR( FROM_UNIXTIME( `invoice_date` ) ) AS `year`,
	SUM(tblinvoice.amount) as sum_amount
	FROM `tblinvoice`
	WHERE YEAR( FROM_UNIXTIME( `invoice_date` ) )=YEAR(CURDATE()) AND sent='1' AND f_archived='0' AND type='2' AND c_invoice_id!='0'
	GROUP BY `year` , `month` ASC");
	while ($current_year_credit->move_next()){
		$months_credit[$current_year_credit->f('month')]= number_format($current_year_credit->f('sum_amount'),2,'.','');
	}
	for ($i = 1; $i <= 12; $i++) {
		if($i <= date('m')){
			$months[$i] = $months[$i]-($negative*$months_credit[$i]);
			$months_cumulate+=$months[$i];
		}else{
			$months_cumulate = 0;
		}
		$months_cumulated[$i] = $months_cumulate;
		
	}
	//last year
	$months_last_year=array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);
	$months_last_year_credit=array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);
	$months_cumulated_last_year=array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);

	$j=0;
	$last_year = $db->query("SELECT tblinvoice.id,tblinvoice.sent,tblinvoice.f_archived,count(tblinvoice.id) as nr, MONTH( FROM_UNIXTIME( `invoice_date` ) ) AS `month` , YEAR( FROM_UNIXTIME( `invoice_date` ) ) AS `year`,
	SUM(tblinvoice.amount) as sum_amount
	FROM `tblinvoice`
	WHERE YEAR( FROM_UNIXTIME( `invoice_date` ) )=YEAR(CURDATE()- INTERVAL 1 YEAR)  AND sent='1' AND f_archived='0' AND type='0' AND c_invoice_id='0'
	GROUP BY `year` , `month` ASC");
	while ($last_year->move_next()){
		$months_last_year[$last_year->f('month')]=number_format($last_year->f('sum_amount'),2,',','');
		$j++;
	}

	if($j){
		$last_year_credit = $db->query("SELECT tblinvoice.id,tblinvoice.sent,tblinvoice.f_archived,count(tblinvoice.id) as nr, MONTH( FROM_UNIXTIME( `invoice_date` ) ) AS `month` , YEAR( FROM_UNIXTIME( `invoice_date` ) ) AS `year`,
		SUM(tblinvoice.amount) as sum_amount
		FROM `tblinvoice`
		WHERE YEAR( FROM_UNIXTIME( `invoice_date` ) )=YEAR(CURDATE()- INTERVAL 1 YEAR)  AND sent='1' AND f_archived='0' AND type='2' AND c_invoice_id!='0'
		GROUP BY `year` , `month` ASC");
		while ($last_year_credit->move_next()){
			$months_last_year_credit[$last_year_credit->f('month')]=number_format($last_year_credit->f('sum_amount'),2,',','');
		}
	}

	for ($i = 1; $i <= 12; $i++) {
		$months_last_year[$i] = $months_last_year[$i]-($negative*$months_last_year_credit[$i]);
		$months_cumulate_last_year+=$months_last_year[$i];
		$months_cumulated_last_year[$i] = $months_cumulate_last_year;

	}
	//two years ago
	$months_last_2year=array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);
	$months_last_2year_credit=array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);
	$months_cumulated_last_2year=array(1=>null,2=>null,3=>null,4=>null,5=>null,6=>null,7=>null,8=>null,9=>null,10=>null,11=>null,12=>null);

	$j=0;
	$last_2year = $db->query("SELECT tblinvoice.id,tblinvoice.sent,tblinvoice.f_archived,count(tblinvoice.id) as nr, MONTH( FROM_UNIXTIME( `invoice_date` ) ) AS `month` , YEAR( FROM_UNIXTIME( `invoice_date` ) ) AS `year`,
	SUM(tblinvoice.amount) as sum_amount
	FROM `tblinvoice`
	WHERE YEAR( FROM_UNIXTIME( `invoice_date` ) )=YEAR(CURDATE()- INTERVAL 2 YEAR)  AND sent='1' AND f_archived='0' AND type='0' AND c_invoice_id='0'
	GROUP BY `year` , `month` ASC");
	while ($last_2year->move_next()){
		$months_last_2year[$last_2year->f('month')]=number_format($last_2year->f('sum_amount'),2,',','');
		$j++;
	}

	if($j){
		$last_2year_credit = $db->query("SELECT tblinvoice.id,tblinvoice.sent,tblinvoice.f_archived,count(tblinvoice.id) as nr, MONTH( FROM_UNIXTIME( `invoice_date` ) ) AS `month` , YEAR( FROM_UNIXTIME( `invoice_date` ) ) AS `year`,
		SUM(tblinvoice.amount) as sum_amount
		FROM `tblinvoice`
		WHERE YEAR( FROM_UNIXTIME( `invoice_date` ) )=YEAR(CURDATE()- INTERVAL 2 YEAR)  AND sent='1' AND f_archived='0' AND type='2' AND c_invoice_id!='0'
		GROUP BY `year` , `month` ASC");
		while ($last_2year_credit->move_next()){
			$months_last_2year_credit[$last_2year_credit->f('month')]=number_format($last_2year_credit->f('sum_amount'),2,',','');
		}
	}

	for ($i = 1; $i <= 12; $i++) {
		$months_last_2year[$i] = $months_last_2year[$i]-($negative*$months_last_2year_credit[$i]);
		$months_cumulate_last_2year+=$months_last_2year[$i];
		$months_cumulated_last_2year[$i] = $months_cumulate_last_2year;
	}

	$data = array(
	    'labels'=>array(),
	    'data'=>array(),
	    'series'=>array(gm('Turnover').' 2016',gm('Turnover').' 2015',gm('Turnover').' 2014'),
	    'colors'=>array('#5d97b9','#9ec1d5','#c0c9cc'),
	    'opt'=>array(
	        'responsive'=> true,
            'legend'=>array(
                'display'=> true,
                'position'=> 'bottom',
                'labels'=> array(
                    'boxWidth'=> 10
                    )
                ),
            'hover'=> array(
                'mode'=> 'label'
            	)
        	),
        'datasetOverride' => array( 'lineTension'=> 0 , 'fill'=> false, 'pointBorderWidth'=> 0, 'pointRadius'=> 4, 'pointHoverRadius'=> 5 ),
	    'chart_type'=>'line');
	$dataCumulate = array(
	    'labels'=>array(),
	    'data'=>array(),
	    'series'=>array(gm('Cumulative').' 2016',gm('Cumulative').' 2015',gm('Cumulative'),' 2014'),
	    'colors'=>array('#38bc7b','#91e5bb','#dcdfe0'),
	    'opt'=>array(
	        'responsive'=> true,
            'legend'=>array(
                'display'=> true,
                'position'=> 'bottom',
                'labels'=> array(
                    'boxWidth'=> 10
                    )
                ),
            'hover'=> array(
                'mode'=> 'label'
            )
        ),
        'datasetOverride' => array( 'lineTension'=> 0 , 'fill'=> false, 'pointBorderWidth'=> 0, 'pointRadius'=> 4, 'pointHoverRadius'=> 5 ),
	    'chart_type'=>'line');
	#year
	$first = array();
	foreach ($months as $key => $value) {
		$data['labels'][] = str_pad($key,2,"0",STR_PAD_LEFT);
		$first[] = $value;
	}
	array_push($data['data'], $first);
	
	$firstC = array();
	foreach ($months_cumulated as $key => $value) {
		$dataCumulate['labels'][] = str_pad($key,2,"0",STR_PAD_LEFT);
		$firstC[] = $value;
	}
	array_push($dataCumulate['data'], $firstC);
	
	$second = array();
	foreach ($months_last_year as $key => $value) {
		$second[] = $value;
	}
	array_push($data['data'], $second);

	$secondC = array();
	foreach ($months_cumulated_last_year as $key => $value) {
		$secondC[] = $value;
	}
	array_push($dataCumulate['data'], $secondC);

	$third = array();
	foreach ($months_last_2year as $key => $value) {
		$third[] = $value;
	}
	array_push($data['data'], $third);

	$thirdC = array();
	foreach ($months_cumulated_last_2year as $key => $value) {
		$thirdC[] = $value;
	}
	array_push($dataCumulate['data'], $thirdC);
	
	return array('fix'=>$data,'cumulate'=>$dataCumulate);
}
