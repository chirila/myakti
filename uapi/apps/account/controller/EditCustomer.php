<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
$result =array();

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}
if($in['contact_id']){
	$customer = $db->query("SELECT * FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ")->getAll();
	$customer[0]['name']		=$customer[0]['lastname'];
	$customer[0]['comp_phone']	=$customer[0]['phone'];
	$customer[0]['comp_fax']	=$customer[0]['fax'];
	$customer[0]['c_email']		=$customer[0]['email'];
}else{
	$customer = $db->query("SELECT * FROM customers WHERE customer_id='".$in['customer_id']."' ")->getAll();
}


if(!is_numeric($customer[0]['lead_source'])){
	$customer[0]['lead_source']='0';
}
if(!is_numeric($customer[0]['sector'])){
	$customer[0]['sector']='0';
}
if(!is_numeric($customer[0]['language'])){
	$customer[0]['language']='0';
}
if(!is_numeric($customer[0]['lead_source'])){
	$customer[0]['lead_source']='0';
}
if(!is_numeric($customer[0]['activity'])){
	$customer[0]['activity']='0';
}
if(!is_numeric($customer[0]['various1'])){
	$customer[0]['various1']='0';
}
if(!is_numeric($customer[0]['various2'])){
	$customer[0]['various2']='0';
}
if(!is_numeric($customer[0]['legal_type'])){
	$customer[0]['legal_type']='0';
}
if($customer[0]['is_customer']==1){
	$customer[0]['is_customer']=true;
}else{
	$customer[0]['is_customer']=false;
}

if($customer[0]['is_supplier']==1){
	$customer[0]['is_supplier']=true;
}else{
	$customer[0]['is_supplier']=false;
}
if($customer[0]['acc_manager_name']){
	$customer[0]['acc_manager'] = explode(',', $customer[0]['acc_manager_name']);
	$customer[0]['user_id'] = explode(',', $customer[0]['user_id']);
}
if($customer[0]['c_type']){
	$customer[0]['c_type'] = explode(',', $customer[0]['c_type']);
}
// var_dump($customer[0]['vat_id']);
if(!$customer[0]['vat_id']){
	$vat = $db->field("SELECT vat_id FROM vats WHERE value='".ACCOUNT_VAT."' ");
	$customer[0]['vat_id'] = $vat;
}

if(!$customer[0]['currency_id']){
	$customer[0]['currency_id'] = ACCOUNT_CURRENCY_TYPE;
}

if($customer[0]['creation_date']){
	$customer[0]['creation_date'] = date(ACCOUNT_DATE_FORMAT,$customer[0]['creation_date']);
}

if($customer[0]['apply_fix_disc']=='1'){
	$customer[0]['apply_fix_disc']=true;
}

if($customer[0]['apply_line_disc']=='1'){
	$customer[0]['apply_line_disc']=true;
}

$customer[0]['payment_term_start'] = $customer[0]['payment_term_type'];

$is_siret = false;
	$country_id = $db->field("SELECT country_id FROM customer_addresses WHERE customer_id = '".$in['customer_id']."' ");
	if( $country_id=='79' || $country_id=='132' ){ //france and luxembourg
		$is_siret = true;
	}	


$label = $db->query("SELECT * FROM customer_fields WHERE value = '1' ORDER BY field_id ");
$all = $label->getAll();

$customer[0]['extra']=array();
$result['extra'] = array();
foreach ($all as $key => $value) {
	$f_value = $db->field("SELECT value FROM customer_field WHERE customer_id='".$in['customer_id']."' AND field_id='".$value['field_id']."' ");	
	if($value['is_dd']==1){
		$result['extra'][$value['field_id']]=build_extra_field_dd('',$value['field_id']);
		if(!$f_value){
			$f_value = '0';
		}
	}
	$customer[0]['extra'][$value['field_id']]=$f_value;
}
if($in['customer_id'] == 'tmp'){
	$do_next ='customers-EditCustomer-customers-addCustomer';
	$is_add = true;
}else{
	$do_next ='customers-EditCustomer-customers-updateCustomer';
	$is_add = false;
	$address = $db->query("SELECT * FROM  customer_addresses WHERE customer_id='".$in['customer_id']."' and is_primary=1 ")->getAll();
	foreach ($address[0] as $key => $value) {
		$customer[0][$key] = $value;
	}
}
if($in['contact_id']){
	$address = $db->query("SELECT * FROM  customer_contact_address WHERE contact_id='".$in['contact_id']."'")->getAll();
		$customer[0]['address'] = $address[0]['address'];
		$customer[0]['zip'] 	= $address[0]['zip'];
		$customer[0]['city'] 	= $address[0]['city'];
}


$result['vat_0'] 					= $db->field("SELECT vat_id FROM vats where value='0.00'");
if($customer[0]['internal_language']>1000){
  $result['internal_language_table']='pim_custom_lang';
}else{
  $result['internal_language_table']='pim_lang';
}
$result['invoice_email_type']		= $customer[0]['invoice_email_type'];
$result['invoice_email']			= $customer[0]['invoice_email'];
$result['attention_of_invoice']		= $customer[0]['attention_of_invoice'];
$result['contact_id']				= $in['contact_id'];
$result['language']					= build_language_dd($in['language']);
$result['lead_source']				= build_l_source_dd($in['lead_source']);
$result['activity']					= build_c_activity_dd($in['activity']);
$result['various1']					= build_various_dd($in['various1'],'1');
$result['various2']					= build_various_dd($in['various2'],'2');
$result['legal_type']				= build_l_type_dd($in['legal_type']);
$result['sector']					= build_s_type_dd($in['sector']);
$result['c_type']					= getCustomerType();
$result['accountManager']			= getAccountManager();
$result['sales_rep']				= getSales();
$result['vat_dd']					= build_vat_dd();
$result['vat_regim_dd']				= build_vat_regime_dd();
$result['currency_dd']				= build_currency_list();
$result['cat_dd']					= build_cat_dd();
$result['lang_dd']					= build_language_dd_new();
$result['multiple_dd'] 				= build_identity_dd();
$result['is_siret']					= $is_siret;
$result['is_add']					= $is_add;
$result['is_admin']					= $_SESSION['access_level'] == 1 ? true : false;
$result['obj']						= $customer[0];
if(!$result['obj']['type']){
	$result['obj']['type'] 			= $in['types']==1 ? '1' : '0';
}
/*if($in['updateFromFinancial']==false){
	$in['updateFromFinancial']=false;
}elseif($in['updateFromFinancial']==""){
	$in['updateFromFinancial']=true;
}elseif($in['updateFromFinancial']==true){
	$in['updateFromFinancial']=true;
}*/

$result['obj']['add_customer']		= $in['updateFromFinancial'] == false ? true : false;
$result['obj']['add_contact']		= $in['updateFromFinancial1'] == true ? true : false;
$result['obj']['add_notes']			= $in['updateFromFinancial2'] == true ? true : false;
$result['obj']['country_dd']		= build_country_list(0);
$result['obj']['country_id']		= $result['obj']['country_id'] ? $result['obj']['country_id'] : ACCOUNT_BILLING_COUNTRY_ID;
$result['obj']['do']				= $do_next;
$result['obj']['vies_ok'] 			= $customer[0]['check_vat_number'] && ($customer[0]['check_vat_number'] ==  $customer[0]['btw_nr']) && !$customer[0]['vat_form'] ? true : false;
$result['obj']['do_financial']		= 'customers-EditCustomer-customers-update_financial';
$result['obj']['do_notes']					= 'customers-EditCustomer-customers-customer_notes';
$result['obj']['customer_notes']			= $customer[0]['customer_notes'];

$i=0;
$default_show=array();
if($in['customer_id']=='tmp'){
	$def=$db->query("SELECT * FROM customise_field WHERE controller='company-customer' AND value=1 ");
	while ($def->next()) {
		$line = array();
		/*$line[$def->f('field')] = $def->f('value') == 1 ? true : false;*/
		$result['obj'][$def->f('field').'_if'] = $def->f('creation_value') == 1 ? true : false;
	}
}else{
	$def=$db->query("SELECT * FROM customise_field WHERE controller='company-customer' ");
	while ($def->next()) {
		$line = array();
		/*$line[$def->f('field')] = $def->f('value') == 1 ? true : false;*/
		/*$result['obj'][$def->f('field').'_if'] =  true;*/
		$result['obj'][$def->f('field').'_if'] = $def->f('value') == 1 ? true : false;
	}
}

/*$default_show[$def->f('field')] = $line;*/

/*$result['obj']['acc_manager']      		=  ($def->f("field")=='acc_manager' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['language_if']      		=  ($def->f("field")=='language' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['sales_rep_if']      	=  ($def->f("field")=='sales_rep' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['lead_source_if']    	=  ($def->f("field")=='lead_source' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['comp_size_if'] 		    =  ($def->f("field")=='comp_size' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['activity_if']      		=  ($def->f("field")=='activity' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['various1_if']   	   	=  ($def->f("field")=='various1' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['various2_if'] 	     	=  ($def->f("field")=='various2' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['is_supplier_if']    	=  ($def->f("field")=='is_supplier' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['website_if']    	  	=  ($def->f("field")=='website' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['phone']      			=  ($def->f("field")=='phone' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['fax']      				=  ($def->f("field")=='fax' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['c_email_if']      		=  ($def->f("field")=='c_email' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['our_reference_if']  	=  ($def->f("field")=='our_reference' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['sector_if']     		=  ($def->f("field")=='sector' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['legal_type_if']     	=  ($def->f("field")=='legal_type' &&  $def->f("creation_value")==1) ? true : false;
$result['obj']['commercial_name_if']    =  ($def->f("field")=='legal_type' &&  $def->f("creation_value")==1) ? true : false;*/




/*$label = $db->query("SELECT * FROM customer_fields WHERE value = '1' AND is_dd = 1 ORDER BY field_id ");
$all = $label->getAll();

foreach ($all as $key => $value) {
	
}*/

json_out($result);

function get_message_intra_eu($in){
	$db=new sqldb();
	$c = $db->query("SELECT message_intra_eu FROM  ".$in['table']."  WHERE lang_id='".$in['lang_id']."'");
	return array('message'=>$c->f('message_intra_eu'));
}

function get_check_for_btw($in){
	$db=new sqldb();
	$db->query("SELECT btw_nr, name FROM customers WHERE customer_id!='".$in['customer_id']."' AND btw_nr='".$in['btw_nr']."' ");
	if($db->next()){
		msg::notice ( $db->f('name').' '.gm("has the same VAT number"),'notice');
		return false;
	}
	return true;
}

?>