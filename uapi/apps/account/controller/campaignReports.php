<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/ 

if($in['xget']){
    $fname = 'get_'.$in['xget'];
    if(function_exists($fname)){
    	json_out($fname($in));
    }else{
    	msg::error('Function does not exist','error');
    	json_out($in);
    }
}

$result = array(
	'CLIENT_ID'	 	=> ACCOUNT_CAMPAIGN_CLIENT,
	'HIDE2'			=> 'hide',
	'PAG'			=> ark::$controller,
	'list'			=> array()
);

$camp = $db->query("SELECT * FROM campaign");

while ($camp->next()) {
	$result['list'][] = array(
		'NAME'			=> $camp->f('campaign_name'),
		'SENT'			=> date(ACCOUNT_DATE_FORMAT,$camp->f('date')),
		'RECIPIENTS'	=> $camp->f('recipients'),
		'CAMPAIGN_ID'	=> $camp->f('campaign_id'),
		'OPEN'  		=> $camp->f('t_open'),
		'CLICK'			=> $camp->f('click'),
		'HIDE2'			=> 'hide',
  	);	
}

json_out($result);

function get_xcampaign2($in){
	$db = new sqldb();
	$camp = $db->query("SELECT * FROM campaign WHERE campaign_id='".$in['campaign_id']."' ");
	$camp->next();

	$not_opened = $camp->f('recipients') - $camp->f('uniq_open') -$camp->f('bounce');
	$open_procent = round(($camp->f('uniq_open')*100)/$camp->f('recipients'));
	$not_open_procent = round(($not_opened*100)/$camp->f('recipients'));
	$bounce_procent = round(($camp->f('bounce')*100)/$camp->f('recipients'));

	$result = array(
		'DATE'							=> date(ACCOUNT_DATE_FORMAT,$camp->f('date')),
		'RECIPIENTS'					=> $camp->f('recipients'),
		'OPENED_UNIQ' 					=> $camp->f('uniq_open'),
		'OPENED_UNIQ_TEXT'				=> $camp->f('uniq_open') == 0 ? "Unique opens" : "<a href=\"#\" class=\"opened\" rev=\"".$in['campaign_id']."\" rel=\"1\">Unique opens</a>",
		'TOTAL_OPENED'					=> $camp->f('t_open'),
		'BOUNCED'						=> $camp->f('bounce'),
		'BOUNCED_TEXT'					=> $camp->f('bounce') == 0 ? 'All emails appear to be delivered':"<a href=\"#\" class=\"opened\" rel=\"2\">Bounced</a>",
		'NOT_OPENED'					=> $not_opened,
		'NOT_OPENED_TEXT'				=> $not_opened == 0 ? "Not Opened" : "<a href=\"#\" class=\"opened\" rel=\"0\">Not Opened</a>",
		'OPENED_PROCENT'				=> $open_procent,
		'OPENED_PROCENT_TEXT'   		=> $open_procent == 0 ? "opened so far" : "<a href=\"#\" class=\"opened\" rel=\"1\">opened so far</a>",
		'CLICK_LINK_PROCENT'			=> round(($camp->f('click')*100)/$camp->f('recipients')),
		'UNSUBSCRIBED'					=> $camp->f('unsubscribed'),
		'UNSUBSCRIBED_LINK'				=> $camp->f('unsubscribed') == 0 ? 'people' : "<a href=\"#\" class=\"opened\" rel=\"3\">people</a>",
	//		'SPAM'					=> '?',
		'LIKES'							=> $camp->f('likes'),
		'ID'							=> $in['campaign_id'],
		'CLICKED'						=> $camp->f('click'),
		'UNSUBSCRIBED_PROCENT'			=> round(($camp->f('unsubscribed')*100)/$camp->f('recipients')),
	);
	$result['recepients'] = $camp->f('recipients');
	$result['unique_open'] = $camp->f('uniq_open');
	$result['bounce']	= $camp->f('bounce');
	$result['CAMPAIGN_ID']	= $in['campaign_id'];
	$result['chart'] = array(
		'chart_type'=> 'doughnut',
		'colors' => array('#5bc0de','#d9534f','#5cb85c'),
		'labels' => array('Not Opened','Bounced','Unique Opened'),
		'opt' => array('legend'=>false),
		'data' => array( (int)$not_open_procent, (int)$bounce_procent, (int)$open_procent ) 
	);

	return $result;

}

function get_recipient_activity2($in){
	$db = new sqldb();
	$filter = " AND open='0' ";
	if($in['opened'] == 1){
		$filter = " AND open>'0' ";
	}elseif ($in['opened'] == 2){
		$filter = " AND open='-1' ";
	}elseif ($in['opened'] == 3){
		$filter = " AND open='-2' ";
	}
	$j=0;
	$emm = $db->query("SELECT * FROM campaign_email WHERE campaign_id='".$in['campaign_id']."' $filter GROUP BY email ");
	$result = array(
	    'list'=>array(),
	    'CLIENT_ID'	 	=> ACCOUNT_CAMPAIGN_CLIENT,
		'TEXT'			=> $in['opened'] == 1 || $in['opened'] == 0 ? gm('Opened') : gm('Reason'),		
	);
	while ($emm->next()) {
		switch($in['opened']){
			case '0':
				$text = '0';
				break;
			case '1':
				$text = $emm->f('open');
				break;
			case '2':
				$text = 'Mail Block';
				break;
			case '3':
				$text = 'unsubscribed';
				break;
			default: 
				$text = '0';
				break;
		}
		$result['list'][] =array(
			'EMAIL'		=> $emm->f('email'),
			'OPENS'		=> $text,
			'NAME'		=> $emm->f('name'),
		);
		$j++;
	}

	return $result;

}

function get_xcampaigncamp($in){
	
	ark::loadLibraries(array('campaignmonitor/campaign_all'));
	// require_once("campaignmonitor/campaign_all.php");

	$wrap = new CS_REST_Clients(ACCOUNT_CAMPAIGN_CLIENT, ACCOUNT_CAMPAIGN_APY);
	console::log($wrap);
	$result = $wrap->get_campaigns();

	if($result->was_successful()) {
	//    print_r($result->response);
	    foreach ($result->response AS $key ){
			if($key->CampaignID == $in['campaign_id']){
				$date = $key->SentDate;
				break;
			}
		}
	} else {
	    msg::$error = $result->response->Message;
	}

	$wrap = new CS_REST_Campaigns($in['campaign_id'], ACCOUNT_CAMPAIGN_APY);
	$result = $wrap->get_summary();

	if($result->was_successful()) {
	   // console::log($result->response);
		$not_opened = $result->response->Recipients - $result->response->UniqueOpened - $result->response->Bounced;
		$open_procent = round(($result->response->UniqueOpened*100)/$result->response->Recipients);
		
		$not_open_procent = round(($not_opened*100)/ $result->response->Recipients);
		$bounce_procent = round(($result->response->Bounced*100)/ $result->response->Recipients);

		$data =array(
			'DATE'							=> date(ACCOUNT_DATE_FORMAT,strtotime($date)),
			'RECIPIENTS'					=> $result->response->Recipients,
			'OPENED_UNIQ' 					=> $result->response->UniqueOpened,
			'OPENED_UNIQ_TEXT'				=> $result->response->UniqueOpened == 0 ? "Unique opens" : "<a href=\"#\" class=\"opened\" rev=\"".$in['campaign_id']."\" rel=\"1\">Unique opens</a>",
			'TOTAL_OPENED'					=> $result->response->TotalOpened,
			'BOUNCED'						=> $result->response->Bounced,
			'BOUNCED_TEXT'					=> $result->response->Bounced == 0 ? 'All emails appear to be delivered':"<a href=\"#\" class=\"opened\" rel=\"2\">Bounced</a>",
			'NOT_OPENED'					=> $not_opened,
			'NOT_OPENED_TEXT'				=> $not_opened == 0 ? "Not Opened" : "<a href=\"#\" class=\"opened\" rel=\"0\">Not Opened</a>",
			'OPENED_PROCENT'				=> $open_procent,
			'OPENED_PROCENT_TEXT'   		=> $open_procent == 0 ? "opened so far" : "<a href=\"#\" class=\"opened\" rel=\"1\">opened so far</a>",
			'CLICK_LINK_PROCENT'			=> round(($result->response->Clicks*100)/$result->response->Recipients),
			'UNSUBSCRIBED'					=> $result->response->Unsubscribed,
			'UNSUBSCRIBED_LINK'				=> $result->response->Unsubscribed == 0 ? 'people' : "<a href=\"#\" class=\"opened\" rel=\"3\">people</a>",
	//		'SPAM'					=> '?',
			'LIKES'							=> $result->response->Likes,
			'ID'							=> $in['campaign_id'],
			'CLICKED'						=> $result->response->Clicks,
			'UNSUBSCRIBED_PROCENT'			=> round(($result->response->Unsubscribed*100)/$result->response->Recipients),
		);
		$data['recepients'] = $result->response->Recipients;
		$data['unique_open'] = $result->response->UniqueOpened;
		$data['bounce']	= $result->response->Bounced;
		
	} else {
		console::log($result->response);
	    // msg::error ( $result->response,'error');
	}

	// $view->assign(array(
		#mesaj
	$data['campaign_id']		= $in['campaign_id'];
	// ));
	$data['chart'] = array(
		'chart_type'=> 'doughnut',
		'colors' => array('#5bc0de','#d9534f','#5cb85c'),
		'labels' => array('Not Opened','Bounced','Unique Opened'),
		'opt' => array('legend'=>false),
		'data' => array( (int)$not_open_procent, (int)$bounce_procent, (int)$open_procent ) 
	);

	return $data;

}


function get_xcampaignchimp($in){
	
	// ark::loadLibraries(array('campaignmonitor/campaign_all'));
	// require_once("campaignmonitor/campaign_all.php");
	$data = array();
	ark::loadLibraries(array('MCAPI.class','Mailchimp'));

    $chimp_c2 = new Mailchimp(MAIL_CHIMP);
    $result2 = $chimp_c2->campaigns->getList(array('campaign_id'=> $in['campaign_id']));
    console::log($result2);
    $result = '';
    if(empty($result2['error'])){
    	$result = $result2['data'][0]['summary'];
	}

    if(!empty($result)) {
		$bounces = $result['hard_bounces'] + $result['soft_bounces'];
		$not_opened = $result['emails_sent'] - $result['unique_opens'] - $bounces;
		$open_procent = round(($result['unique_opens']*100)/$result['emails_sent']);

		$not_open_procent = round(($not_opened*100)/$result['emails_sent']);
		$bounce_procent = round(($bounces*100)/$result['emails_sent']);

		$data = array(
	//		'DATE'					=> date(ACCOUNT_DATE_FORMAT,strtotime($date)),
			'RECIPIENTS'						=> $result['emails_sent'],
			'OPENED_UNIQ' 					=> $result['unique_opens'],
			'OPENED_UNIQ_TEXT'			=> $result['unique_opens'] == 0 ? "Unique opens" : "<a href=\"#\" class=\"opened\" rev=\"".$in['campaign']."\" rel=\"1\">Unique opens</a>",
			'TOTAL_OPENED'					=> $result['opens'],
			'BOUNCED'								=> $bounces,
			'BOUNCED_TEXT'					=> $bounces == 0 ? 'All emails appear to be delivered':"<a href=\"#\" class=\"opened\" rel=\"2\">Bounced</a>",
			'NOT_OPENED'						=> $not_opened,
			'NOT_OPENED_TEXT'				=> $not_opened == 0 ? "Not Opened" : "<a href=\"#\" class=\"opened\" rel=\"0\">Not Opened</a>",
			'OPENED_PROCENT'				=> $open_procent,
			'OPENED_PROCENT_TEXT'   => $open_procent == 0 ? "opened so far" : "<a href=\"#\" class=\"opened\" rel=\"1\">opened so far</a>",
			'CLICK_LINK_PROCENT'		=> round(($result['clicks']*100)/$result['emails_sent']),
			'UNSUBSCRIBED'					=> $result['unsubscribes'],
			'LIKES'									=> $result['unique_likes'],
			'ID'										=> $in['campaign'],
			'CLICKED'								=> $result['clicks'],
			'UNSUBSCRIBED_PROCENT'	=> round(($result['unsubscribes']*100)/$result['emails_sent']),
		);
		$data['recepients'] = $result['emails_sent'];
		$data['unique_open'] = $result['unique_opens'];
		$data['bounce'] = $bounces;
		
	} else {
		msg::notice(gm('No data to display'),'notice');
	}

	$data['campaign_id']		= $in['campaign_id'];
	$data['chart'] = array(
		'chart_type'=> 'doughnut',
		'colors' => array('#5bc0de','#d9534f','#5cb85c'),
		'labels' => array('Not Opened','Bounced','Unique Opened'),
		'opt' => array('legend'=>false),
		'data' => array( (int)$not_open_procent, (int)$bounce_procent, (int)$open_procent ) 
	);

	return $data;

}