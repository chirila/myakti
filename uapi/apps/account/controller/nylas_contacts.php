<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/************************************************************************
* @Author: MedeeaWeb Works                                              *
************************************************************************/
	class NylasContacts extends Controller{

		function __construct($in,$db = null)
		{
			parent::__construct($in,$db = null);		
		}

		public function get_init(){
			$in=$this->in;
			$output=array();
			/*$l_r = ROW_PER_PAGE;

			if((!$in['offset']) || (!is_numeric($in['offset'])))
			{
			    $offset=0;
			    $in['offset']=1;
			}
			else
			{
			    $offset=$in['offset']-1;
			}

			$contacts=$this->db->query("SELECT customer_contacts.*,customers.name FROM customer_contacts 
				LEFT JOIN customers ON customer_contacts.customer_id=customers.customer_id
				WHERE customer_contacts.nylas_contact_id!='' ORDER BY lastname");
			$max_rows=$contacts->records_count();
			$output['max_rows'] =  $max_rows;
			$contacts->move_to($offset*$l_r);
			while($contacts->next()){
				$item=array(
					'cus_name'			=> $contacts->f('name'),
					'customer_id'		=> $contacts->f('customer_id'),
					'contact_id'		=> $contacts->f('contact_id'),
					'firstname'			=> $contacts->f('firstname'),
					'lastname'			=> $contacts->f('lastname'),
					'email'				=> $contacts->f('email'),
					'cell'				=> $contacts->f('cell'),
					'phone'				=> $contacts->f('phone'),
				);
				array_push($output['list'], $item);
			}*/

			$user_nylas=$this->db_users->field("SELECT active FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
			$output['nylas_active']=(defined('NYLAS_ACTIVE') &&  NYLAS_ACTIVE==1 && $user_nylas) ? true : false;
			//$output['lr']=$l_r;
			$in['crm'] = '0';
			$file1=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup1.xml';
			$file2=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup2.xml';
			$file3=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup3.xml';
			$file_fill1 = file_get_contents($file1);
			$file_fill2 = file_get_contents($file2);
			$file_fill3 = file_get_contents($file3);

			if($file_fill1==true&&$file_fill2==true&&$file_fill3==true){
				$show_file=1;
			}
			$output['step']=$in['step'] ? $in['step'] : 1;
			$output['file']		=    backupcrm($in['crm']);
			$output['file_id']	=	  $in['crm'];
			$output['show_file']	=    $show_file==1?true:false;
			$output['markAll']=false;
			$output['markSync']=false;
			$this->out = $output;
		}

		public function get_contacts(){
			global $config;
			$in=$this->in;

			$result=array('list'=>array());
			$ch = curl_init();
			$token_id=$this->db_users->field("SELECT token FROM nylas_data WHERE user_id='".$_SESSION['u_id']."' ");
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		    curl_setopt($ch, CURLOPT_USERPWD, $token_id.':');
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_URL, $config['nylas_base_url'].'/contacts');

	        $put = curl_exec($ch);
		    $info = curl_getinfo($ch);

		    if($info['http_code']>300 || $info['http_code']==0){
		    	$err=json_decode($put);
	    		msg::error($err->message,"error");
		    }else{
		    	$data=json_decode($put);
		    	foreach($data as $key=>$value){
		    		$contact_exist=$this->db->field("SELECT contact_id FROM customer_contacts WHERE nylas_contact_id='".$value->id."' ");
		    		if(!$contact_exist){
		    			$name_array=explode(" ", $value->name);
		    			$last_name=$name_array[count($name_array)-1];
		    			$first_name='';
		    			for($i=0;$i<count($name_array)-1;$i++){
							$first_name.=$name_array[$i]." ";
		    			}	    			
		    			$first_name=rtrim($first_name," ");
		    			$phone='';
		    			foreach($data->phone_numbers as $key=>$value){
		    				if($value['type']=='Mobile'){
		    					$phone=$value['number'];
		    					break;
		    				}
		    			}
		    			$temp=array(
		    				'firstname'		=> $first_name,
		    				'lastname'		=> $last_name,
		    				'id'			=> $value->id,
		    				'email'			=> $value->email,
		    				'customer_dd'	=> $this->get_cc(),
		    				'phone'			=> $phone,
		    				'disabled'		=> false
		    			);
		    			array_push($result['list'], $temp);
		    		}
		    	}
		    }
		    if(count($result['list'])==0){
		    	msg::notice(gm('No contacts to sync'),'notice');
		    }
		    $result['country_dd']=build_country_list(ACCOUNT_BILLING_COUNTRY_ID);
		    $result['country_id']=ACCOUNT_BILLING_COUNTRY_ID;
		    $this->out = $result;
		}

		public function get_cc($in)
		{
			$q = strtolower($in["term"]);
					
			$filter =" is_admin='0' AND customers.active=1 ";
			// $filter_contact = ' 1=1 ';
			if($q){
				$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
				// $filter_contact .=" AND CONCAT_WS(' ',firstname, lastname) LIKE '%".$q."%'";
			}

			$admin_licence = $this->db_users->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
			if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
				$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
			}
			$cust = $this->db->query("SELECT customer_id, name, CONCAT(  '',  '' ) as contact_id,acc_manager_name,country_name,zip_name,city_name
				FROM customers
				WHERE $filter			
				ORDER BY name
				LIMIT 5")->getAll();

			$result = array();
			foreach ($cust as $key => $value) {
				$cname = trim($value['name']);

				$result[]=array(
					"id"					=> $value['customer_id'],
					"name"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
					"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"top"	 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
					"ref" 					=> $value['our_reference'],
					"currency_id"			=> $value['currency_id'],
					"lang_id" 				=> $value['internal_language'],
					"identity_id" 			=> $value['identity_id'],
					'contact_name'			=> $value['acc_manager_name'] ? $value['acc_manager_name'] : '',
					'country'				=> $value['country_name'] ? $value['country_name'] : '',
					'zip'					=> $value['zip_name'] ? $value['zip_name'] : '',
					'city'					=> $value['city_name'] ? $value['city_name'] : '',
					"bottom"				=> $value['zip_name'].' '.$value['city'].' '.$value['country_name'],
					"right"					=> $value['acc_manager_name']
				);
			}
			if($q){
				array_push($result,array('id'=>'99999999999','value'=>'############################################################################################################################################################ '.$q.' ############################################################################################################################################################'));
			}else{
				array_push($result,array('id'=>'99999999999','value'=>''));
			}
			
			return $result;
		}

		public function get_created(){
			$in=$this->in;
			$in['term']=$this->db->field("SELECT name FROM customers WHERE customer_id='".$in['buyer_id']."' ");
			$customer_dd=$this->get_cc($in);
			$result=array('customer_id'=>$in['buyer_id'],'customer_dd'=>$customer_dd);
			return $result;
		}
	}

	$contacts = new NylasContacts($in,$db);

	if($in['xget']){
		$fname = 'get_'.$in['xget'];
		$contacts->output($contacts->$fname($in));
	}

	$contacts->get_init();
	$contacts->output();
?>