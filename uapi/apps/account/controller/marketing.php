<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$apps = $db->query("SELECT * FROM apps WHERE main_app_id='0' AND type='main' AND active='1' AND app_type='mail' AND template_file!='nylas' ");
$result=array();
while ($apps->next()) {
	$result['list'][] = array(
		'app'		=> $apps->f('template_file'),
		'not'		=> $apps->f('active') ? ($apps->f('active') == 2 ? 'bad-activated' : '' ) : 'not-activated',
		'pag'		=> $apps->f('template_file') == 'chimp' ? 'mailchimp' : 'campaigns',
	);
}

json_out($result);