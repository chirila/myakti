<?php

$customer  = $db->query("SELECT
firstname,
name,
website,
language,
c_email as email,
sector,
comp_phone as phone,
comp_fax as fax,
zip_name as zip,
country_name as country,
city_name as city,
btw_nr
from customers
where customer_id = '".$_SESSION['customer_id']."'
");
if($customer->next()){
    $sector = $db->field("select name from customer_sector where id = '".$customer->f('sector')."'");
    $customer = array(
        'firstname' => $customer->f('firstname'),
        'name' => $customer->f('name'),
        'website' => $customer->f('website'),
        'language' => $customer->f('language'),
        'email' => $customer->f('email'),
        'sector' => $customer->f('sector'),
        'sector_name' => $sector,
        'phone' => $customer->f('phone'),
        'fax' => $customer->f('fax'),
        'zip' => $customer->f('zip'),
        'city' => $customer->f('city'),
        'country' => $customer->f('country'),
        'btw_nr' => $customer->f('btw_nr')
    );
}

$address  = $db->query("SELECT
country_id,
city,
zip,
address,
delivery, billing, is_primary as `primary`
from customer_addresses
where  customer_id='".$_SESSION['customer_id']."' and is_primary	= '1'
");

if($address->next()){
    $address = array(
        'country_id' => $address->f('country_id'),
        'city' => $address->f('city'),
        'zip' => $address->f('zip'),
        'address' => $address->f('address'),
        'delivery' => $address->f('delivery'),
        'billing' => $address->f('billing'),
        'primary' => $address->f('primary'),
    );

}

$contact  = $db->query("SELECT
firstname,
lastname,
position,
email,
birthdate,
phone,
cell,
sex,
language,
title,
fax,
company_name,
username
from customer_contacts
where  customer_id='".$_SESSION['customer_id']."' and contact_id = '".$_SESSION['contact_id']."'
");

if($contact->next()){
    $contact = array(
        'firstname' => $contact->f('firstname'),
        'lastname' => $contact->f('lastname'),
        'position' => $contact->f('position'),
        'email' => $contact->f('email'),
        'birthdate' => $contact->f('birthdate'),
        'phone' => $contact->f('phone'),
        'cell' => $contact->f('cell'),
        'sex' => $contact->f('sex'),
        'language' => $contact->f('language'),
        'title' => $contact->f('title'),
        'fax' => $contact->f('fax'),
        'company_name' => $contact->f('company_name'),
        'username' => $contact->f('username'),

    );

}


json_out(array('customer'=>$customer, 'address'=>$address, 'contact' => $contact));
