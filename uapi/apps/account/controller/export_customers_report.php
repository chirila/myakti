<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('memory_limit','786M');
setcookie('Akti-Customer-Export','6-0-0',time()+3600,'/');
$filename ="customers_report.xls";

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

ark::loadLibraries(array('PHPExcel'));
/** Include PHPExcel */

global $database_config;

$database_users = array(
  'hostname' => $database_config['mysql']['hostname'],
  'username' => $database_config['mysql']['username'],
  'password' => $database_config['mysql']['password'],
  'database' => $database_config['user_db'],
);

$dbu = new sqldb($database_users);

$filter = " is_admin='0' ";
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akti")
               ->setLastModifiedBy("Akti")
               ->setTitle("Customers report")
               ->setSubject("Customers report")
               ->setDescription("Customers export")
               ->setKeywords("office PHPExcel php")
               ->setCategory("Customers");
$filter_c = '';
// if($in['search']){
//   /*$filter .= " AND ( name LIKE '%".$in['search']."%'  OR country_name LIKE '%".$in['search']."%' OR c_type_name LIKE '%".$in['search']."%' OR city_name LIKE '%".$in['search']."%') ";
//   $arguments_s.="&search=".$in['search'];*/
//   $filter_x = '';
//   // $search_arr = str_split($in['search']);
//   $search_arr = str_split(stripslashes($in['search']));
//   foreach ($search_arr as $value) {
//     $filter_x .= $value.".?";
//   }
//   $filter .= " AND ( name regexp '".addslashes($filter_x)."' OR country_name LIKE '%".$in['search']."%' OR c_type_name LIKE '%".$in['search']."%' OR city_name LIKE '%".$in['search']."%' ) ";
//   // $filter .= " AND ( name LIKE '%".$in['search']."%' OR country_name LIKE '%".$in['search']."%' OR c_type_name LIKE '%".$in['search']."%' OR city_name LIKE '%".$in['search']."%' ) ";
//   $arguments_s.="&search=".$in['search'];
// }
$escape_chars = array('(','?','*','+');

if(!empty($in['search'])){
  $filter_x = '';
  // $search_arr = str_split($in['search']);
  $search_arr = str_split(stripslashes($in['search']));
  foreach ($search_arr as $value) {
    if(in_array($value, $escape_chars)){
      $value = '\\'.$value;
    }
    $filter_x .= $value."[^a-zA-Z0-9]?";
  }
  $filter .= " AND ( name regexp '".addslashes($filter_x)."' OR country_name LIKE '%".$in['search']."%' OR c_type_name LIKE '%".$in['search']."%' OR city_name LIKE '%".$in['search']."%' ) ";
  // $filter .= " AND ( name LIKE '%".$in['search']."%' OR country_name LIKE '%".$in['search']."%' OR c_type_name LIKE '%".$in['search']."%' OR city_name LIKE '%".$in['search']."%' ) ";
  $arguments_s.="&search=".$in['search'];
}

if(!$in['archived']){
  $filter.= " AND active=1";
}else{
  $filter.= " AND active=0";
  $arguments.="&archived=".$in['archived'];
  $arguments_a = "&archived=".$in['archived'];
}

if(!empty($in['acc_manager_name'])){
  $filter .= " AND ( name LIKE '%".$in['acc_manager_name']."%'  OR country_name LIKE '%".$in['acc_manager_name']."%' OR c_type_name LIKE '%".$in['acc_manager_name']."%' OR city_name LIKE '%".$in['acc_manager_name']."%' OR acc_manager_name LIKE '%".$in['acc_manager_name']."%' ) ";
  $arguments_s.="&acc_manager_name=".$in['acc_manager_name'];
}elseif(!empty($in['acc'])){
  $filter .= " AND user_id='0' ";
  $arguments_s.="&acc=".$in['acc'];
}
if(!empty($in['zip_from']) && empty($in['zip_to'])){
  $zip_from = preg_replace('/[^0-9]/', '', $in['zip_from']);
  $filter .= " AND ( zip_name LIKE '%".$zip_from."%' ) ";
  $arguments_s.="&zip_from=".$in['zip_from'];
}
if(!empty($in['zip_to'])){
  $zip_from = preg_replace('/[^0-9]/', '', $in['zip_from']);
  $zip_to = preg_replace('/[^0-9]/', '', $in['zip_to']);
  if(empty($zip_from)){
    $zip_from = '0';
  }
  $filter .= " AND ( STRIP_NON_DIGIT(zip_name) BETWEEN '".$zip_from."' AND '".$zip_to."' ) ";
  $arguments_s.="&zip_to=".$zip_to."&zip_from=".$zip_from;
}
if(!empty($in['country_id'])){
  $filter .= " AND country_name = '".$in['country_id']."' ";
  $arguments_s.="&country_id=".$in['country_id'];
}
if(!empty($in['our_reference'])){
  $filter .= " AND ( our_reference LIKE '%".$in['our_reference']."%' ) ";
  $arguments_s.="&our_reference=".$in['our_reference'];
}
if(!empty($in['btw_nr'])){
  $filter .= " AND ( btw_nr LIKE '%".$in['btw_nr']."%' ) ";
  $arguments_s.="&btw_nr=".$in['btw_nr'];
}
if(!empty($in['commercial_name'])){
  $filter_x = '';
  $search_arr = str_split(stripslashes($in['search']));
  foreach ($search_arr as $value) {
    $filter_x .= $value.".?";
  }
  $filter .= " AND ( commercial_name regexp '".addslashes($filter_x)."' ) ";
  $arguments_s.="&commercial_name=".$in['commercial_name'];
}
if (isset($in['type'])){
  $filter .= " AND customers.type='".$in['type']."'  ";
  $arguments.="&type=".$in['type'];
}
if (isset($in['is_supplier'])){
  $filter .= " AND customers.is_supplier='".$in['is_supplier']."'  ";
  $arguments.="&is_supplier=".$in['is_supplier'];
}
if(!empty($in['lead_source'])){
  $filter .= " AND lead_source='".$in['lead_source']."' ";
  $arguments_s.="&lead_source=".$in['lead_source'];
}
if($in['first'] && $in['first'] == '[0-9]'){
  $filter.=" AND SUBSTRING(LOWER(customers.name),1,1) BETWEEN '0' AND '9'";
  $arguments.="&first=".$in['first'];
} elseif ($in['first']){
  $filter .= " AND customers.name LIKE '".$in['first']."%'  ";
  $arguments.="&first=".$in['first'];
}
if(!isset($in['front_register'])){
  $in['front_register'] = 0;
}
  $filter .= " AND front_register= '".$in['front_register']."'  ";
  $arguments.="&front_register=".$in['front_register'];

setcookie('Akti-Customer-Export','6-0-88',time()+3600,'/');
$arguments = $arguments.$arguments_s;
$info = $db->query("SELECT * FROM customers WHERE $filter ");
$i = 0;
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', gm("Company id"))
      ->setCellValue('B1', gm("Name"))
      ->setCellValue('C1', gm("Account Manager"))
      ->setCellValue('D1', gm("Type of relationship"))
      ->setCellValue('E1', gm("VAT"))
      ->setCellValue('F1', gm("VAT number"))
      ->setCellValue('G1', gm("Reference"))
      ->setCellValue('H1', gm("Country"))
      ->setCellValue('I1', gm("City"))
      ->setCellValue('J1', gm("ZIP"))
      ->setCellValue('K1', gm("Address"))
      ->setCellValue('L1', gm("No. contacts"))
      ->setCellValue('M1', gm("Phone"))
      ->setCellValue('N1', gm("Price Category"))
      ->setCellValue('O1', gm("Language"))
      ->setCellValue('P1', gm("Legal Type"))
      ->setCellValue('Q1', gm("Website"))
      ->setCellValue('R1', gm("Activity Sector"))
      ->setCellValue('S1', gm("Company email"))
      ->setCellValue('T1', gm("Lead Source"))
      ->setCellValue('U1', gm("Commercial Name"))
      ->setCellValue('V1', gm("Activity"))
      ->setCellValue('W1', gm("Payment Term"))
      ->setCellValue('X1', gm("Discount"))
      ->setCellValue('Y1', gm("Currency"))
      ->setCellValue('Z1', gm("External id"))
      ->setCellValue('AA1', gm("Delivery and dispatch notes"))
      ->setCellValue('AB1', gm("Bank Name"))
      ->setCellValue('AC1', gm("BIC Code"))
      ->setCellValue('AD1', gm("IBAN"))
      ->setCellValue('AE1', gm("Invoice email"))
      ->setCellValue('AF1', gm("Document Language"))
      ->setCellValue('AG1', gm("Company size"));
$first_letter = 0;
$second_letter = 7;
$letters = range('A', 'Z');
$extra_field = $db->query("SELECT * FROM customer_fields ")->getAll();
      foreach ($extra_field as $key => $value) {
        $cell_name = (string)$letters[$first_letter].$letters[$second_letter].'1';
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_name, utf8_decode($value['label']));
        if($second_letter ==25){
          $second_letter = 0;
          $first_letter ++;
        }else{
          $second_letter++;
        }

      }

 $xlsRow = 2;

while ($info->next())
{
  
  $add = $db->field("SELECT address FROM customer_addresses WHERE customer_id='".$info->f('customer_id')."' ");
  $vat = '';
  if($info->f('vat_id')){
    $vat = $db->field("SELECT value FROM vats WHERE vat_id=".$info->f('vat_id'));
  }
  $cat_name = '';
  if($info->f('cat_id')){
    $cat_name = $db->field("SELECT name FROM pim_article_price_category WHERE category_id='".$info->f('cat_id')."' ");
  }
  $language = '';
  if($info->f('language')){
    $language = $db->field("SELECT name FROM customer_contact_language WHERE id='".$info->f('language')."' ");
  }
  $legal_type ='';
  if($info->f('legal_type')){
    $legal_type = $db->field("SELECT name FROM customer_legal_type WHERE id='".$info->f('legal_type')."' ");
  }
  $sector = '';
  if($info->f('sector')){
    $sector = $db->field("SELECT name FROM customer_sector WHERE id='".$info->f('sector')."' ");
  }
  $lead_source = '';
  if($info->f('lead_source')){
    $lead_source = $db->field("SELECT name FROM customer_lead_source WHERE id='".$info->f('lead_source')."' ");
  }
  $activity = '';
  if($info->f('activity')){
    $activity = $db->field("SELECT name FROM customer_activity WHERE id='".$info->f('activity')."' ");
  }
  $currency_code = '';
  if($info->f('currency_id')){
    $currency_code = $db->field("SELECT code FROM currency WHERE id='".$info->f('currency_id')."' ");
  }
  $internal_language = '';
  if($info->f('internal_language')){
    $internal_language = $db->field("SELECT language FROM pim_lang WHERE lang_id='".$info->f('internal_language')."' GROUP BY lang_id ");
    if($internal_language){
      $internal_language = gm($internal_language);
    }
  }

  $objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A'.$xlsRow, $info->f('customer_id'))
        ->setCellValue('B'.$xlsRow, $info->f('name'))
        ->setCellValue('C'.$xlsRow, $info->f('acc_manager_name'))
        ->setCellValue('D'.$xlsRow, $info->f('c_type_name'))
        ->setCellValue('E'.$xlsRow, $vat)
        ->setCellValue('F'.$xlsRow, $info->f('btw_nr'))
        ->setCellValue('G'.$xlsRow, $info->f('our_reference'))
        ->setCellValue('H'.$xlsRow, $info->f('country_name'))
        ->setCellValue('I'.$xlsRow, $info->f('city_name'))
        ->setCellValue('J'.$xlsRow, $info->f('zip_name'))
        ->setCellValue('K'.$xlsRow, $add)
        ->setCellValue('L'.$xlsRow, get_contact_nr($info->f('customer_id')))
        ->setCellValue('M'.$xlsRow, $info->f('comp_phone'))
        ->setCellValue('N'.$xlsRow, $cat_name)
        ->setCellValue('O'.$xlsRow, $language)
        ->setCellValue('P'.$xlsRow, $legal_type)
        ->setCellValue('Q'.$xlsRow, $info->f('website'))
        ->setCellValue('R'.$xlsRow, $sector)
        ->setCellValue('S'.$xlsRow, $info->f('c_email'))
        ->setCellValue('T'.$xlsRow, $lead_source)
        ->setCellValue('U'.$xlsRow, $info->f('commercial_name'))
        ->setCellValue('V'.$xlsRow, $activity)

        ->setCellValue('W'.$xlsRow, $info->f('payment_term'))
        ->setCellValue('X'.$xlsRow, $info->f('fixed_discount'))
        ->setCellValue('Y'.$xlsRow, $currency_code)
        ->setCellValue('Z'.$xlsRow, $info->f('external_id'))
        ->setCellValue('AA'.$xlsRow, $info->f('deliv_disp_note'))
        ->setCellValue('AB'.$xlsRow, $info->f('bank_name'))
        ->setCellValue('AC'.$xlsRow, $info->f('bank_bic_code'))
        ->setCellValue('AD'.$xlsRow, $info->f('bank_iban'))
        ->setCellValue('AE'.$xlsRow, $info->f('invoice_email'))
        ->setCellValue('AF'.$xlsRow, $internal_language)
        ->setCellValue('AG'.$xlsRow, $info->f('comp_size'));
    $first_letter = 0;
    $second_letter = 7;
    $letters = range('A', 'Z');
    foreach ($extra_field as $key => $value) {
      $value_extra = $db->field("SELECT customer_field.value FROM customer_field 
                               INNER JOIN customer_fields ON customer_field.field_id = customer_fields.field_id
                               WHERE customer_id = '".$info->f('customer_id')."' AND customer_field.field_id = '".$value['field_id']."' ");
      $cell_name = (string)$letters[$first_letter].$letters[$second_letter].$xlsRow;
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_name, utf8_decode($value_extra));
      if($second_letter ==25){
        $second_letter = 0;
        $first_letter ++;
      }else{
        $second_letter++;
      }
    }

    $xlsRow++;
    $i++;
}

@mkdir('upload/'.DATABASE_NAME."/",0755);

$objPHPExcel->getActiveSheet()->setTitle('Customers report');
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('upload/'.DATABASE_NAME."/".$filename);

define('ALLOWED_REFERRER', '');
define('BASE_DIR','upload/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE','downloads.log');
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  //'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);
####################################################################
###  DO NOT CHANGE BELOW
####################################################################

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {
  die("Internal server error. Please contact system administrator.");
}
set_time_limit(0);
$fname = basename($filename);
function find_file ($dirname, $fname, &$file_path) {
  $dir = opendir($dirname);
  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file
// get full file path (including subfolders)
$file_path = '';
find_file('upload/'.DATABASE_NAME, $fname, $file_path);
if (!is_file($file_path)) {
  die("File does not exist. Make sure you specified correct file name.");
}
// file size in bytes
$fsize = filesize($file_path);
// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));
// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}
// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}

// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$fname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    setcookie('Akti-Customer-Export','9-0-0',time()+3600,'/');
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}
exit();
?>