<?php

class customer {

	function __construct() {
		$this->db = new sqldb();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author Tinu Coman
	 **/
	function add(&$in)
	{

		if(!$in['type']){
			$in['type'] = 0;
		}

		if(!$this->add_validate($in)){
			return false;
		}



		if(!$in['language']){
			$in['language'] = 1;
		}

		if($in['lang']){
			$this->db->query("SELECT lang_id from lang where code='".$in['lang']."'");
			if($this->db->next()){
				$in['language']=$this->db->f('lang_id');
			}
		}

		if($in['country']){
			$country = get_country_name($in['country']);
		}
		if(!$in['name']){
			$in['name'] = $in['lastname'];
		}
        $in['customer_id'] = $this->db->insert("INSERT INTO customers SET
                                                type                    = '".$in['type']."',
                                                firstname 				= '".$in['firstname'].' '.$in['lastname']."',
												name 					= '".$in['name']."',
												commercial_name 		= '".$in['commercial_name']."',
												active					= '1',
												legal_type				= '".$in['legal_type']."',
												website					= '".$in['website']."',
												language				= '".$in['language']."',
												c_email					= '".$in['email']."',
												sector					= '".$in['sector']."',
												comp_phone				= '".$in['phone']."',
												comp_fax				= '".$in['fax']."',
												lead_source				= '".$in['lead_source']."',
												zip_name				= '".$in['zip']."',
												city_name 				= '".$in['city']."',
												country_name 			= '".$country."',
												vat_regime_id			= '1',
												btw_nr					= '".$in['btw_nr']."',
												creation_date			= '".time()."' ");

		$address_id = $this->db->insert("INSERT INTO customer_addresses SET
												customer_id			=	'".$in['customer_id']."',
												country_id			=	'".$in['country']."',
												state_id			=	'".$in['state']."',
												city				=	'".$in['city']."',
												zip					=	'".$in['zip']."',
												address				=	'".$in['address']."',
												billing				=	'1',
												is_primary			=	'1',
												delivery			=	'1'");

			if(!$in['zip'] && $in['city']){
				$address = $in['address'].', '.$in['city'].', '.$country;
			}elseif(!$in['city'] && $in['zip']){
				$address = $in['address'].', '.$in['zip'].', '.$country;
			}elseif(!$in['zip'] && !$in['city']){
				$address = $in['address'].', '.$country;
			}else{
				$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country;
			}

			$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
			$output= json_decode($geocode);
			$in['lat'] = $output->results[0]->geometry->location->lat;
			$in['lng'] = $output->results[0]->geometry->location->lng;
			$this->save_location($in);

		$in['value']=$in['btw_nr'];
		$check_valid_vat=$this->check_vies_vat_number($in);
		if($check_valid_vat != false){
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['customer_id']."' ");
		}else if($check_valid_vat == false && $in['trends_ok']){
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['customer_id']."' ");
		}else{
			$in['value']='';
		}

		set_first_letter('customers',$in['name'],'customer_id',$in['customer_id']);

		//save extra fields values
		$this->update_custom_field($in);
		
		$in['skip_c_validate'] = true;
		$this->contact_add($in);

		http_response_code(200);
		json_out(array('customer_id'=>$in['customer_id'], 'contact_id'=>$in['contact_id']));

		return true;
	}


	/**
	 * undocumented function
	 *
	 * @return void
	 * @author Tinu Coman
	 **/
	function update(&$in)
	{
		if($_SESSION['customer_id']){
			$in['customer_id'] = $_SESSION['customer_id'];
		}

		if(!$this->update_validate($in)){
			return false;
		}

		if(!$in['language']){
			$in['language'] = 1;
		}

		if($in['lang']){
			$this->db->query("SELECT lang_id from lang where code='".$in['lang']."'");
			if($this->db->next()){
				$in['language']=$this->db->f('lang_id');
			}
		}

		if($in['country']){
			$country = get_country_name($in['country']);
		}
		if(!$in['name']){
			$in['name'] = $in['lastname'];
		}

		$this->db->query("UPDATE customers SET
											firstname 				= '".$in['firstname'].' '.$in['lastname']."',
											name 					= '".$in['name']."',
											serial_number_customer  = '".$in['serial_number_customer']."',
											commercial_name 		= '".$in['commercial_name']."',
											legal_type				= '".$in['legal_type']."',
											website					= '".$in['website']."',
											language				= '".$in['language']."',
											c_email					= '".$in['email']."',
											sector					= '".$in['sector']."',
											comp_phone				= '".$in['phone']."',
											comp_fax				= '".$in['fax']."',
											lead_source				= '".$in['lead_source']."',
											zip_name				= '".$in['zip']."',
											city_name 				= '".$in['city']."',
											country_name 			= '".$country."',
											vat_regime_id			= '1',
											btw_nr					= '".$in['btw_nr']."'

					  	WHERE customer_id='".$_SESSION['customer_id']."' ");

		$this->db->query("UPDATE customer_addresses SET
												country_id			=	'".$in['country']."',
												state_id			=	'".$in['state_id']."',
												city				=	'".$in['city']."',
												zip					=	'".$in['zip']."',
												address				=	'".$in['address']."'
												WHERE customer_id='".$_SESSION['customer_id']."' and is_primary	= '1' ");

		if(!$in['zip'] && $in['city']){
			$address = $in['address'].', '.$in['city'].', '.$country;
		}elseif(!$in['city'] && $in['zip']){
			$address = $in['address'].', '.$in['zip'].', '.$country;
		}elseif(!$in['zip'] && !$in['city']){
			$address = $in['address'].', '.$country;
		}else{
			$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country;
		}

		$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
		$output= json_decode($geocode);
		$lat = $output->results[0]->geometry->location->lat;
		$long = $output->results[0]->geometry->location->lng;
		$this->db->query("SELECT coord_id FROM addresses_coord WHERE customer_id='".$in['customer_id']."'");
		if($this->db->move_next())
		{
			$this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
		}else
		{
			$this->db->query("INSERT INTO addresses_coord SET location_lat='".$lat."', location_lng='".$long."', customer_id='".$in['customer_id']."'");
		}


		$this->db->query("UPDATE customer_contacts SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$this->db->query("UPDATE projects SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");

		if($in['legal_type']){
			$legal_type = $this->db->field("SELECT name FROM customer_legal_type WHERE id='".$in['legal_type']."' ");
		}
		$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['name'].' '.$legal_type."'	WHERE buyer_id='".$in['customer_id']."' ");

		set_first_letter('customers',$in['name'],'customer_id',$in['customer_id']);

		//save extra fields values
		$this->update_custom_field($in);

		$in['skip_c_validate'] = true;
		$this->contact_update($in);

		msg::success ( gm("Changes have been saved."),'success');
		http_response_code(200);
		json_out(array('success' => msg::get_success()));

		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author Tinu Coman
	 **/
	function add_validate(&$in, $nopass = 0){
		$v = new  validation($in);
		$v->field('firstname', gm('firstname'), 'required', gm('Please fill in the First Name field'));
		$v->field('lastname', gm('lastname'), 'required', gm('Please fill in the Last Name field'));
		$v->field('email', gm('Email'), 'required:email', gm('Please provide a valid email address'));
		$v->field('phone', gm('Phone'), 'required', gm('Please fill in the Phone field'));
		$v->field('zip', gm('Postalcode'), 'required', gm('Please fill in the Postalcode field'));
		$v->field('city', gm('City'), 'required', gm('Please fill in the City field'));
		$v->field('country', gm('Country'), 'required', gm('Please select the Country'));

		if(!$nopass){
			$v->field('password', gm('Password'), 'required', gm('Please provide the Password'));
			if($in['password']){
				$v->field('password2', gm('Verify Password'), 'required', gm('Please verify the Password'));
				$v->field('password', gm('Password'), 'match[password2]', gm('Password does not match'));
			}
		}else{
			if($in['password']){
				$v->field('password2', gm('Verify Password'), 'required', gm('Please verify the Password'));
				$v->field('password', gm('Password'), 'match[password2]', gm('Password does not match'));
			}
		}


		if (!$v->run()) {
			http_response_code(400);
			json_out(array('validation_error' => msg::get_errors()));
			return false;
		}
		return true;

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author Tinu Coman
	 **/
	function update_validate(&$in)
	{

		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		if (!$v->run()) {
			http_response_code(400);
			json_out(array('validation_error' => msg::get_errors()));
			return false;
		}
		return $this->add_validate($in, 1);

	}


	/**
	 * undocumented function
	 *
	 * @return void
	 * @author Tinu Coman
	 **/
	function contact_add(&$in)
	{
		// if we come from add, we have allready validated
		if(!$in['skip_c_validate']){
			if(!$this->add_validate($in)){
				return false;
			}
			if($_SESSION['customer_id']){
				$in['customer_id'] = $_SESSION['customer_id'];
			} else {
				http_response_code(401);
				json_out();
			}
		} else {

		}

		if($in['birthdate']){
			$in['birthdate'] = strtotime($in['birthdate']);
		}
		if(!$in['username']){
			$in['username'] = $in['email'];
		}

		$in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
						customer_id	=	'".$in['customer_id']."',
						firstname	=	'".$in['firstname']."',
						lastname	=	'".$in['lastname']."',
						`position`	=	'".$in['position']."',
						department	=	'".$in['department']."',
						email		=	'".$in['email']."',
						birthdate	=	'".$in['birthdate']."',
						phone		=	'".$in['phone']."',
						cell		=	'".$in['cell']."',
						note		=	'".$in['note']."',
						sex			=	'".$in['sex']."',
						title		=	'".$in['title']."',
						`language`	=	'".$in['language']."',
						fax			=	'".$in['fax']."',
						company_name=	'".$in['name']."',
						username   =	'".$in['username']."',
						password   =	'".md5($in['password'])."',
						title_name	=	'".$in['title_name']."',
						`create`	=	'".time()."',
						s_email = 1,
						last_update = 	'".time()."'");

		set_first_letter('customer_contacts',$in['lastname'],'contact_id',$in['contact_id']);
		//save extra fields values
		$this->update_custom_contact_field($in);

		//$this->add_contact_address($in);

		$this->addContactToCustomer($in);

		$this->setAccountAsPrimary($in);

		if(!$in['skip_c_validate']){
			// output_data
			unset($in['skip_c_validate']);
			http_response_code(200);
			json_out(array('customer_id'=>$in['customer_id'], 'contact_id'=>$in['contact_id']));


		}

		return true;
	}



	/**
	 * Update contact data
	 * @param  array $in
	 * @return bool
	 */
	function contact_update(&$in)
	{
		if($_SESSION['customer_id']){
			$in['customer_id'] = $_SESSION['customer_id'];
		} else {
			http_response_code(401);
			json_out();
		}

		if($_SESSION['contact_id']){
			if(!$in['contact_id']){
				$in['contact_id'] = $_SESSION['contact_id'];
			}
		} else {
			http_response_code(401);
			json_out();
		}
		$this->db->query("select customer_id from customer_contacts where contact_id = '".$in['contact_id']."'");
		if(!$this->db->next()){
			http_response_code(401);
			json_out();
		} else {
			if($this->db->f('customer_id')!= $_SESSION['customer_id']){
				http_response_code(401);
				json_out();
			}
		}
		// if we come from update, we have allready validated
		if(!$in['skip_c_validate']){
			if(!$this->update_validate($in)){
				return false;
			}
		}
		//print_r($_SESSION); exit();

		if($in['birthdate']){
			$in['birthdate'] = strtotime($in['birthdate']);
		}

		if(!$in['username']){
			$in['username'] = $in['email'];
		}

		$this->db->query("UPDATE customer_contacts SET

                                            firstname	=	'".$in['firstname']."',
                                            lastname	=	'".$in['lastname']."',
                                            position	=	'".$in['position']."',
                                            department	=	'".$in['department']."',
                                            email		=	'".$in['email']."',
                                            birthdate	=	'".$in['birthdate']."',
					    					phone		=	'".$in['phone']."',
										    cell		=	'".$in['cell']."',
										    note		=	'".$in['note']."',
										    sex			=	'".$in['sex']."',
										    language	=	'".$in['language']."',
										    title		=	'".$in['title']."',
										    fax			=	'".$in['fax']."',
											company_name=	'".$in['name']."',
											username	=	'".$in['username']."',
											last_update =	'".time()."'
				WHERE
			 	contact_id	=	'".$in['contact_id']."' and customer_id	='".$in['customer_id']."'");

		$this->db->query("UPDATE customer_contacts SET s_email='0' WHERE contact_id='".$in['contact_id']."' and customer_id	='".$in['customer_id']."'");
		if($in['s_email']){
			$this->db->query("UPDATE customer_contacts SET s_email='1', unsubscribe='' WHERE contact_id='".$in['contact_id']."' ");
		}

		if ($in['password'])
		{
			$this->db->query("UPDATE customer_contacts SET password='".md5($in['password'])."' WHERE contact_id='".$in['contact_id']."'");
		}

		$this->db->query("SELECT * FROM recurring_invoice WHERE contact_id='".$in['contact_id']."' ");
		while ($this->db->move_next()) {
			$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['firstname'].' '.$in['lastname']."',
																							buyer_email='".$in['email']."',
																							buyer_phone='".$in['phone']."'
												WHERE contact_id='".$in['contact_id']."' ");
		}

		$project_company_name = $in['firstname'].' '.$in['lastname'];
		$this->db->query("UPDATE projects SET company_name = '".$project_company_name."' WHERE customer_id = '".$in['contact_id']."' AND is_contact = '1' ");

		set_first_letter('customer_contacts',$in['lastname'],'contact_id',$in['contact_id']);
		//save extra fields values
		$this->update_custom_contact_field($in);

		if(!$in['skip_c_validate']){
			// output_data
			unset($in['skip_c_validate']);
			http_response_code(200);
			json_out(array('customer_id'=>$in['customer_id'], 'contact_id'=>$in['contact_id']));


		}

		return true;
	}
	/**
	 * Archive a contact
	 * @param  array $in
	 * @return bool
	 */
	function archive_contact(&$in)
	{
		if(!$this->archive_contact_validate($in)){
			return false;
		}
		// Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customer_contacts SET active='0'  where contact_id='".$in['contact_id']."' ");
		// Sync::end();
		msg::success (gm('Contact has archived'),'success');
		http_response_code(200);
		json_out(array('success' => msg::get_success()));

		return true;
	}

	/**
	 * Activate a contact
	 * @param  array $in
	 * @return bool
	 */
	function activate_contact(&$in)
	{
		if(!$this->archive_contact_validate($in)){
			return false;
		}
		// Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customer_contacts SET active='1'  where contact_id='".$in['contact_id']."' ");
		// Sync::end();
		msg::success ( gm('Contact has activated'),'success');
		http_response_code(200);
		json_out(array('success' => msg::get_success()));
		return true;
	}

	/**
	 * Archive a contact validate
	 * @param  array $in
	 * @return bool
	 */
	function archive_contact_validate(&$in)
	{
		if($_SESSION['customer_id']){
			$in['customer_id'] = $_SESSION['customer_id'];
		} else {
			http_response_code(401);
			json_out();
		}

		if(!$_SESSION['contact_id']){
			http_response_code(401);
			json_out();
		} else {
			if($_SESSION['contact_id'] == $in['contact_id']){
				http_response_code(400);
				json_out(array('validation_error' => gm('You can not activate or archive your self')));
			}
		}
		$this->db->query("select customer_id from customer_contacts where contact_id = '".$in['contact_id']."'");
		if(!$this->db->next()){
			http_response_code(401);
			json_out();
		} else {
			if($this->db->f('customer_id')!= $_SESSION['customer_id']){
				http_response_code(401);
				json_out();
			}
		}

		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', gm('Invalid ID'));
		if (!$v->run()) {
			http_response_code(400);
			json_out(array('validation_error' => msg::get_errors()));
			return false;
		}

		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author Tinu Coman
	 **/
	function save_location(&$in)
	{
		$this->db->query("INSERT INTO addresses_coord SET
							customer_id='".$in['customer_id']."',
							location_lat='".$in['lat']."',
							location_lng='".$in['lng']."'");
		return true;
	}

	function check_vies_vat_number(&$in){
		$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

		if(!$in['value'] || $in['value']==''){			
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = 'error';
				json_out($in);
			}
			return false;
		}
		$value=trim($in['value']," ");
		$value=str_replace(" ","",$value);
		$value=str_replace(".","",$value);
		$value=strtoupper($value);

		$vat_numeric=is_numeric(substr($value,0,2));

		if(!in_array(substr($value,0,2), $eu_countries)){
			$value='BE'.$value;
		}
		if(in_array(substr($value,0,2), $eu_countries)){
			$search   = array(" ", ".");
			$vat = str_replace($search, "", $value);
			$_GET['a'] = substr($vat,0,2);
			$_GET['b'] = substr($vat,2);
			$_GET['c'] = '1';
			$dd = include('valid_vat.php');
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = gm('Error');
				json_out($in);
			}
			return false;
		}
		if(isset($response) && $response == 'invalid'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = gm('Error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'error'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = gm('Error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'valid'){
			$full_address=explode("\n",$result->address);
			switch($result->countryCode){
				case "RO":
					$in['comp_address']=$full_address[1];
					$in['comp_city']=$full_address[0];
					$in['comp_zip']=" ";
					break;
				case "NL":
					$zip=explode(" ",$full_address[2],2);
					$in['comp_address']=$full_address[1];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
				default:
					$zip=explode(" ",$full_address[1],2);
					$in['comp_address']=$full_address[0];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
			}

			$in['comp_name']=$result->name;

			$in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
			$in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
			$in['full_details']=$result;
			$in['vies_ok']=1;
			$in['crm']['country_dd']		= build_country_list(0);
			$in['crm']['country_id']		= $in['comp_country'] ? $in['comp_country'] : '26';
			$in['crm']['address']			= $in['comp_address'];
			$in['crm']['city']				= $in['comp_city'];
			$in['crm']['zip']				= $in['comp_zip'];
			$in['crm']['customer_id']		= $in['customer_id'];
			if($in['customer_id']){
				$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				if($in['comp_country'] != $country_id){
				 	$this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='2' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['remove_v']=1;
				}else if($in['comp_country'] == $country_id){
					$this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='1' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['add_v']=1;
				}
			}
			if(ark::$method == 'check_vies_vat_number'){
				msg::success ( gm('VAT Number is valid'),'success');
				$in['success'] = gm('VAT Number is valid');
				json_out($in);
			}
			return true;
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = gm('Error');
				json_out($in);
			}
			return false;
		}
		if(ark::$method == 'check_vies_vat_number'){
			json_out($in);
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author Tinu Coman
	 **/
	function update_custom_field(&$in)
	{
		$this->db->query("DELETE FROM customer_field WHERE customer_id='".$in['customer_id']."' ");
		if($in['custom']){
			foreach ($in['custom'] as $key => $value) {
				$this->db->query("INSERT INTO customer_field SET customer_id='".$in['customer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
			}
		}

		return true;
	}



	/**
	 * Validate update contact data
	 * @param  array $in
	 * @return [type]     [description]
	 */
	function contact_update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', gm('Invalid ID'));
		$is_ok = $v->run();

		if(!$this->contact_add_validate($in)){
			$is_ok = false;
		}
		/*if($in['customer_id']){
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			if($c_id){
				if($c_id != $in['customer_id']){
					$this->removeFromAddress($in);
				}
			}
		}*/

		return $is_ok;
	}

	/**
	 * undocumented function
	 *
	 * @return true
	 * @param array $in
	 * @author Marius Pop
	 **/
	function contact_add_validate(&$in)
	{
		$in['email'] = trim($in['email']);
		$v = new validation($in);
		$v->field('firstname', 'First name', 'required');
		$v->field('lastname', 'Last name', 'required');
		if(ark::$method=='contact_add'){
			$v->field('customer_id', 'Customer', 'required');
		}

		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function update_custom_contact_field(&$in)
	{
		$this->db->query("DELETE FROM contact_field WHERE customer_id='".$in['contact_id']."' ");
		if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO contact_field SET customer_id='".$in['contact_id']."', value='".addslashes($value)."', field_id='".$key."' ");
			}
		}

		return true;
	}



	/****************************************************************
	* function update_contact_address(&$in)                         *
	****************************************************************/
	function update_contact_address(&$in){

		if(!$in['country_id'] && !$in['state'] && !$in['city'] && !$in['zip'] && !$in['address'] ){
			msg::error (gm('Please fill at least one of the fields'),'success');
			return false;
		}

        // Sync::start(ark::$model.'-'.ark::$method);
        $delivery = 0;
        if($in['delivery']){
        	$delivery = 1;
        }
        $this->db->query("UPDATE customer_contact_address SET country_id 	=	'".$in['country_id']."',
														  		   state 	=	'".$in['state']."',
														           city 	=	'".$in['city']."',
														           zip 		=	'".$in['zip']."',
														           address 	=	'".$in['address']."',
														           delivery =	'".$delivery."'
													      WHERE contact_id 	=	'".$in['contact_id']."'
													      AND  	address_id 	= 	'".$in['address_id']."' ");
        if($in['primary']){
        	$this->db->query("UPDATE customer_contact_address SET is_primary='0' WHERE contact_id='".$in['contact_id']."' ");
        	$this->db->query("UPDATE customer_contact_address SET is_primary='1' WHERE contact_id='".$in['contact_id']."' AND address_id='".$in['address_id']."' ");
        	#updating existing recurring invoices
        	$this->db->query("SELECT * FROM recurring_invoice WHERE contact_id='".$in['contact_id']."' ");
			while ($this->db->move_next()) {
				$this->db->query("UPDATE recurring_invoice SET buyer_address	= '".$in['address']."',
																	buyer_zip	= '".$in['zip']."',
																	buyer_city	= '".$in['city']."',
																buyer_country_id= '".$in['country_id']."'
																WHERE contact_id= '".$in['contact_id']."' ");
			}
        }
		// Sync::end($in['address_id']);
		msg::success ( gm('Address updated'),'success');
		insert_message_log($this->pagc,'{l}Contact updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_nc,$in['contact_id']);
		$in['pagl'] = $this->pagc;
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author Tinu Coman
	 **/
	function attackToAddress(&$in)
	{
		$this->db->query("DELETE FROM customer_contact_addresses WHERE contact_id='".$in['contact_id']."' ");
		$this->db->query("INSERT INTO customer_contact_addresses SET address_id='".$in['address_id']."', contact_id='".$in['contact_id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	/**
	 * Add contact to customer
	 * @param  array $in
	 * @return bool
	 */

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function addContactToCustomer(&$in){
		if(!$this->addContactToCustomer_validate($in)){
			return false;
		}
		$this->db->query("INSERT INTO
						  customer_contactsIds SET
						  customer_id='".$in['customer_id']."',
						  contact_id='".$in['contact_id']."',
						  position = '".$in['position']."',
						  department = '".$in['department']."',
						  title = '".$in['title']."',
						  email = '".$in['email']."',
						  phone = '".$in['phone']."',
						  fax = '".$in['fax']."'
						  ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}


	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function setAccountAsPrimary(&$in)
	{
		if(!$this->removeContactFromCustomer_validate($in)){
			return false;
		}
		$this->db->query("UPDATE customer_contactsIds SET `primary`='0' WHERE contact_id='".$in['contact_id']."' ");
		$this->db->query("UPDATE customer_contactsIds SET `primary`='1' WHERE contact_id='".$in['contact_id']."' AND customer_id='".$in['customer_id']."' ");
		$this->updateContactCustomerName($in);
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}


	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function removeContactFromCustomer_validate(&$in){
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
		$v->field('customer_id', 'ID', 'required:exist[customer_contactsIds.customer_id.contact_id=\''.$in['contact_id'].'\']', "Invalid Id");
		return $v->run();
	}


	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function updateContactCustomerName(&$in)
	{
		$name = $this->db->field("SELECT name FROM customers INNER JOIN customer_contactsIds ON customers.customer_id=customer_contactsIds.customer_id WHERE customer_contactsIds.contact_id='".$in['contact_id']."' AND customer_contactsIds.`primary`=1");
		$info = $this->db->query("SELECT phone, email,customer_id FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' AND `primary`=1 ");

		$this->db->query("UPDATE customer_contacts SET company_name='".addslashes($name)."', phone='".$info->f('phone')."', email='".$info->f('email')."', customer_id='".$info->f('customer_id')."' WHERE contact_id='".$in['contact_id']."' ");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function addContactToCustomer_validate(&$in){
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]:unique[customer_contactsIds.customer_id.contact_id=\''.$in['contact_id'].'\']', "Invalid Id");
		return $v->run();
	}

	function check_valid_vat_number(&$in){

		$in['value']=$in['account_vat_number'];
		$check_vies_number=$this->check_vies_vat_number($in);
		console::log($check_vies_number);
		if($check_vies_number != false){
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['account_vat_number']."', btw_nr='".$in['account_vat_number']."', vat_form='0' where customer_id = '".$in['customer_id']."' ");
			msg::$success = gm('VAT Number is valid');
			$in['success'] = gm('VAT Number is valid');
		}else if($check_vies_number == false && $in['trends_ok']){
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['account_vat_number']."', btw_nr='".$in['account_vat_number']."', vat_form='1' where customer_id = '".$in['customer_id']."' ");
			msg::$success = gm('VAT Number is valid');
			$in['success'] = gm('VAT Number is valid');
		}else{
			msg::$error = gm('Sorry, but we could not verify your VAT. Please make sure it works with').' <a href="http://ec.europa.eu/taxation_customs/vies/" >http://ec.europa.eu/</a> '.gm('and try again');
			$in['error'] = gm('Sorry, but we could not verify your VAT. Please make sure it works with').' <a href="http://ec.europa.eu/taxation_customs/vies/" >http://ec.europa.eu/</a> '.gm('and try again');
		}
		json_out($in);
		return true;
	}



	//ADDRESSES
	/****************************************************************
	* function address_add(&$in)                                    *
	****************************************************************/
	function address_add(&$in)
	{

		if(!$this->address_add_validate($in)){
			return false;
		}

		if($in['billing']){
			$in['billing']=strtolower($in['billing'])=='true' ? 1 : 0;
		}
		if($in['primary']){
			$in['primary']=strtolower($in['primary'])=='true' ? 1 : 0;
		}
		if($in['delivery']){
			$in['delivery']=strtolower($in['delivery'])=='true' ? 1 : 0;
		}
		if($in['site']){
			$in['site']=strtolower($in['site'])=='true' ? 1 : 0;
		}
		$country_n = get_country_name($in['country_id']);

 		$address_id = $this->db->insert("INSERT INTO customer_addresses SET
																			customer_id			=	'".$in['customer_id']."',
																			contact_id			=	'".$in['contact_id']."',
																			country_id			=	'".$in['country_id']."',
																			state_id			=	'".$in['state_id']."',
																			city				=	'".$in['city']."',
																			zip					=	'".$in['zip']."',
																			address				=	'".$in['address']."',
																			billing				=	'".$in['billing']."',
																			is_primary			=	'".$in['primary']."',
																			delivery			=	'".$in['delivery']."',
																			site                    =     '".$in['site']."' ");
		if($in['billing']){
			$this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
		}


		if($in['primary']){
			if($in['address'])
			{
				if(!$in['zip'] && $in['city'])
				{
					$address = $in['address'].', '.$in['city'].', '.$country_n;
				}elseif(!$in['city'] && $in['zip'])
				{
					$address = $in['address'].', '.$in['zip'].', '.$country_n;
				}elseif(!$in['zip'] && !$in['city'])
				{
					$address = $in['address'].', '.$country_n;
				}else
				{
					$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
				}
				$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
				$output= json_decode($geocode);
				$lat = $output->results[0]->geometry->location->lat;
				$long = $output->results[0]->geometry->location->lng;
			}
			$this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
			$this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
			$this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
		}

		http_response_code(200);
		json_out(array('customer_id'=>$_SESSION['customer_id'], 'contact_id'=>$_SESSION['contact_id'], 'address_id' => $address_id));

		return true;
	}

	/****************************************************************
	* function address_add_validate                                 *
	****************************************************************/
	function address_add_validate(&$in)
	{
		if($in['country']){
			$in['country_id'] = $in['country'];
		}
		if($_SESSION['contact_id']){
			$in['contact_id'] = $_SESSION['contact_id'];
		}
		if($_SESSION['customer_id']){
			$in['customer_id'] = $_SESSION['customer_id'];
		}


		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
		$v->field('country_id', 'Country', 'required:exist[country.country_id]');
		$v->field('city', 'city', 'required');
		$v->field('zip', 'zip', 'required');

		if(!$v->run()){
			http_response_code(400);
			json_out(array('validation_error' => msg::get_errors()));
		}

		return true;
	}

	/****************************************************************
	* function address_delete(&$in)                                 *
	****************************************************************/
	function address_delete(&$in)
	{
		if($_SESSION['customer_id']){
			$in['customer_id'] = $_SESSION['customer_id'];
		}

		if(!$this->address_delete_validate($in)){
			return false;
		}

		$this->db->query("DELETE FROM customer_addresses
					  WHERE customer_id	=	'".$in['customer_id']."'
					  AND 	address_id	=	'".$in['address_id']."' ");
		$this->db->query("DELETE FROM article_threshold_dispatch where address_id='".$in['address_id']."' AND customer_id	=	'".$in['customer_id']."'");

		http_response_code(200);
		json_out();
		return true;
	}

	/****************************************************************
	* function address_delete_validate(&$in)                                          *
	****************************************************************/
	function address_delete_validate(&$in)
	{
		$v = new validation($in);
		$v->field('address_id', 'ID', 'required:exist[customer_addresses.address_id]', "Invalid Id");
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		if(!$v->run()){
			http_response_code(400);
			json_out(array('validation_error' => msg::get_errors()));
		}
		return true;
	}

	/****************************************************************
	* function address_update(&$in)                                 *
	****************************************************************/
	function address_update(&$in)
	{
		if($in['country']){
			$in['country_id'] = $in['country'];
		}
		if($_SESSION['contact_id']){
			$in['contact_id'] = $_SESSION['contact_id'];
		}
		if($_SESSION['customer_id']){
			$in['customer_id'] = $_SESSION['customer_id'];
		}

		if(!$this->address_update_validate($in)){
			return false;
		}
		if($in['billing']){
			$in['billing']=strtolower($in['billing'])=='true' ? 1 : 0;
		}
		if($in['primary']){
			$in['primary']=strtolower($in['primary'])=='true' ? 1 : 0;
		}
		if($in['delivery']){
			$in['delivery']=strtolower($in['delivery'])=='true' ? 1 : 0;
		}
		if($in['site']){
			$in['site']=strtolower($in['site'])=='true' ? 1 : 0;
		}
		if(!$in['primary']) {
			$exist_primary = $this->db->field("SELECT address_id FROM customer_addresses WHERE customer_id = '".$in['customer_id']."' AND address_id!= '".$in['address_id']."' AND is_primary = '1' ");
			if(!$exist_primary) {
				$in['primary'] =1 ;
			}
		}
		$country_n = get_country_name($in['country_id']);
		
		$this->db->query("UPDATE customer_addresses SET
											country_id		=	'".$in['country_id']."',
											state_id		=	'".$in['state_id']."',
											city			=	'".$in['city']."',
											zip				=	'".$in['zip']."',
											address			=	'".$in['address']."',
											billing			=	'".$in['billing']."',
											is_primary		=	'".$in['primary']."',
											delivery		=	'".$in['delivery']."',
											site 			= '".$in['site']."'
										WHERE customer_id	=	'".$in['customer_id']."' AND address_id='".$in['address_id']."' ");

		if($in['billing']){
			$this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id='".$in['customer_id']."' AND address_id!='".$in['address_id']."'");
		}

		
		if($in['primary']){
			if($in['address'])
			{
				if(!$in['zip'] && $in['city'])
				{
					$address = $in['address'].', '.$in['city'].', '.$country_n;
				}elseif(!$in['city'] && $in['zip'])
				{
					$address = $in['address'].', '.$in['zip'].', '.$country_n;
				}elseif(!$in['zip'] && !$in['city'])
				{
					$address = $in['address'].', '.$country_n;
				}else
				{
					$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
				}
				$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
				$output= json_decode($geocode);
				$lat = $output->results[0]->geometry->location->lat;
				$long = $output->results[0]->geometry->location->lng;
			}
			$this->db->query("SELECT coord_id FROM addresses_coord WHERE customer_id='".$in['customer_id']."'");
			if($this->db->move_next())
			{
				$this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
			}else
			{
				$this->db->query("INSERT INTO addresses_coord SET location_lat='".$lat."', location_lng='".$long."', customer_id='".$in['customer_id']."'");
			}
			$this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id='".$in['customer_id']."' AND address_id!='".$in['address_id']."'");
			$this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		http_response_code(200);
		json_out(array('success' => msg::get_success()));

		return true;
	}
	/****************************************************************
	* function address_update_validate(&$in)                                          *
	****************************************************************/
	function address_update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('address_id', 'ID', 'required:exist[customer_addresses.address_id]', "Invalid Id");
		if(!$v->run()){
			http_response_code(400);
			json_out(array('validation_error' => msg::get_errors()));
		} else {
			return $this->address_add_validate($in);
		}
	}

	/****************************************************************
	 * function address_status_update(&$in)                         *
	 ****************************************************************/
	function address_status_update(&$in)
	{
		if($in['country']){
			$in['country_id'] = $in['country'];
		}
		if($_SESSION['contact_id']){
			$in['contact_id'] = $_SESSION['contact_id'];
		}
		if($_SESSION['customer_id']){
			$in['customer_id'] = $_SESSION['customer_id'];
		}

		$v = new validation($in);
		$v->field('address_id', 'ID', 'required:exist[customer_addresses.address_id]', "Invalid Id");
		if(!$v->run()){
			http_response_code(400);
			json_out(array('validation_error' => msg::get_errors()));
		}

		if($in['primary'] != 1){
			$in['primary'] = 0;
		}
		if($in['billing'] != 1){
			$in['billing'] = 0;
		}
		if($in['delivery'] != 1){
			$in['delivery'] = 0;
		}

		if(!$in['primary']) {
			$exist_primary = $this->db->field("SELECT address_id FROM customer_addresses WHERE customer_id = '".$in['customer_id']."' AND address_id!= '".$in['address_id']."' AND is_primary = '1' ");
			if(!$exist_primary) {
				$in['primary'] =1 ;
			}
		}



		$this->db->query("UPDATE customer_addresses SET
											billing			=	'".$in['billing']."',
											is_primary		=	'".$in['primary']."',
											delivery		=	'".$in['delivery']."'
										WHERE customer_id	=	'".$in['customer_id']."' AND address_id='".$in['address_id']."' ");

		if($in['billing']){
			$this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id='".$in['customer_id']."' AND address_id!='".$in['address_id']."'");
		}


		if($in['primary']){

			$address = $this->db->query("SELECT address_id FROM customer_addresses WHERE customer_id = '".$in['customer_id']."' AND address_id!= '".$in['address_id']."' AND is_primary = '1' ");
			$address->next();


			$this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id='".$in['customer_id']."' AND address_id!='".$in['address_id']."'");
			$this->db->query("UPDATE customers SET city_name='".$address->f('city')."', country_name='".get_country_name($address->f('country_id'))."', zip_name='".$address->f('zip')."' WHERE customer_id='".$in['customer_id']."' ");
		}
		msg::success ( gm("Changes have been saved."),'success');
		http_response_code(200);
		json_out(array('success' => msg::get_success()));

		return true;
	}


}
?>