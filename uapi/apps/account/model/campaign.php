<?php
/**
 * Email campaign class
 *
 * @author      Arkweb SRL
 * @link        https://go.salesassist.eu
 * @copyright   [description]
 * @package 	campaign monitor
 */
class campaign{

	var $campaign;
	var $list;
	var $segment;
	var $db;

	/**
	 * [campaign description]
	 * @return [type] [description]
	 */
	function campaign(){
		ark::loadLibraries(array('campaignmonitor/campaign_all'));
		// require_once("campaignmonitor/campaign_all.php");
		$this->campaign = new CS_REST_Campaigns(null, ACCOUNT_CAMPAIGN_APY);
		$this->list = new CS_REST_Lists(NULL, ACCOUNT_CAMPAIGN_APY);
		$this->subscriber = new CS_REST_Subscribers(NULL, ACCOUNT_CAMPAIGN_APY);
		$this->segment = new CS_REST_Segments(NULL, ACCOUNT_CAMPAIGN_APY);
		$this->db = new sqldb();
	}

	/**
	 * Add campaign
	 * @param array $in
	 * @return bool
	 */
	function add_campaign(&$in){

		$result = $this->campaign->create($in['client_id'], array(
		    'Subject' 	=> $in['subject'],
		    'Name' 		=> $in['name'],
		    'FromName' 	=> $in['fromname'],
		    'FromEmail' => $in['fromemail'],
		    'ReplyTo' 	=> $in['replay'],
		    'HtmlUrl' 	=> $in['htmlurl'],
		    'TextUrl' 	=> $in['txturl'],
		    'ListIDs' 	=> $in['list'],
		    //'SegmentIDs' => array('')
		));

		if($result->was_successful()) {
		    msg::success ( $result->response,'success');
		    return true;
		} else {
			msg::error ( $result->response->Message, 'error');
		    return false;
		}
	}

	/**
	 * Add campaign list
	 * @param array $in
	 * @return bool
	 */
	function add_list(&$in){
		$result = $this->list->create($in['client_id'], array(
		    'Title' => $in['name'],
		));
		if($result->was_successful()) {
			$this->db->query("INSERT INTO campaign_list SET campaign_list_id='".$result->response."' ");
		    msg::success ( $result->response,'success');
		    ark::$controller = 'get_clients_lists';
		    return true;
		} else {
		    // msg::$error = $result->response->Message;
		    msg::set('name',$result->response->Message);
		    return false;
		}
	}

	/**
	 * Delete campaign list
	 * @param  array $in
	 * @return bool
	 */
	function delete_list(&$in){
		$wrap = $this->list;
		$wrap->list_id = $in['list_id'];
		$wrap->set_list_id($wrap->list_id);

		$result = $wrap->delete();

		if($result->was_successful()) {
			$this->db->query("DELETE FROM campaign_list WHERE campaign_list_id='".$in['list_id']."' ");
		    msg::success ( gm("List deleted").'.', 'success');
		    return true;
		} else {
		    msg::error ( $result->response->Message, 'success');
		    return false;
		}
	}

	/**
	 * Update campaign list
	 * @param  array $in
	 * @return bool
	 */
	function update_list(&$in){
		$wrap = $this->list;
		$wrap->list_id = $in['list_id'];
		$wrap->set_list_id($wrap->list_id);
		$result = $wrap->update(array(
		    'Title' => $in['name'],
//		    'UnsubscribePage' => 'List unsubscribe page',
//		    'ConfirmedOptIn' => true,
//		    'ConfirmationSuccessPage' => 'List confirmation success page'
		));

		if($result->was_successful()) {
		    msg::success ( gm("List Updated"), 'success');
		    return true;
		} else {
		    msg::error ( $result->response->Message, 'error');
		    return false;
		}
	}

	/**
	 * Add subscriber to list
	 * @param array $in
	 * @return bool
	 */
	function add_subscriber(&$in){
		$db = new sqldb();
		$wrap = $this->subscriber;
		$wrap->list_id = $in['list_id'];
		$wrap->set_list_id($wrap->list_id);
		#delete all the existing subscribers
		$wrap_list = $this->list;
		$wrap_list->list_id = $in['list_id'];
		$wrap_list->set_list_id($wrap_list->list_id);
		$result1 = $wrap_list->get();
		$result1 = $wrap_list->get_active_subscribers(date('Y-m-d',0));
		$list_email = array();
		if($result1->was_successful()) {
			foreach ($result1->response->Results as $key){
				// $in['email'] = $key->EmailAddress;
				array_push($list_email, $key->EmailAddress);
				// $this->delete_sub($in);
		  }
		}
		// return true;
		#create an arrays with the new subscribers
		$contacts_list = $db->query("SELECT * FROM contact_list WHERE contact_list_id='".$in['lists_id']."' ");
		$contacts_list->next();
		$list_for = $contacts_list->f('list_for');
		if($contacts_list->f('list_type')){
			$cont = $db->query("SELECT contact_id FROM contact_list_contacts WHERE contact_list_id='".$in['lists_id']."' AND active='1'");
		}else{
			$filtru = unserialize($contacts_list->f('filter'));
			if($filtru){
				$filter = '';
				foreach ($filtru as $field => $value){
					if($value){
						if(is_array($value)){
							$val = '';
							foreach ($value as $key){
								$val .= $key.',';
							}
							$value = $val;
						}
						$value = rtrim($value,',');
						if($value){
							if(substr(trim($field), 0, 13)=='custom_dd_id_'){
								$filter.= " AND ";
								$v = explode(',', $value);
								foreach ($v as $k => $v2) {
									$filter .= "customer_field.value = '".addslashes($v2)."' OR ";
								}
								$filter = rtrim($filter,"OR ");
							}elseif($field == 'c_type '){
								$filter .= " AND ( ";
								$v = explode(',', $value);
								foreach ($v as $k => $v2) {
									$filter .= "c_type_name like '%".addslashes($v2)."%' OR ";
								}
								$filter = rtrim($filter,"OR ");
								$filter .= " ) ";
							}elseif ($field == 'c_type') {
								$filter .= " AND ( ";
								$v = explode(',', $value);
								foreach ($v as $k => $v2) {
									// console::log($v2);
									// $c_v = $db->field("SELECT name FROM customer_type WHERE id='".$v2."' ");
									$filter .= "c_type_name like '%".addslashes($v2)."%' OR ";
								}
								$filter = rtrim($filter,"OR ");
								$filter .= " ) ";
							}
							elseif(trim($field) == 'position'){
								$filter .= " AND ( ";
								$v = explode(',', $value);
								foreach ($v as $k => $v2) {
									$filter .= "position_n like '%".addslashes($v2)."%' OR ";
								}
								$filter = rtrim($filter,"OR ");
								$filter .= " ) ";
							}elseif(trim($field) == 'customer_contacts.email'){
								$filter.=" AND customer_contacts.email='' ";
							}elseif(trim($field) == 'customers.c_email'){
								$filter.=" AND customers.c_email='' ";
							}
							else{
								$filter .= " AND ".$field." IN (".addslashes($value).") ";
							}
						}elseif(trim($field) == 'customers.c_email'){
							$filter.= " AND customers.c_email!='' ";
						}elseif(trim($field) == 'customer_contacts.email'){
							$filter.= " AND customer_contacts.email!='' ";
						}elseif(trim($field) == 'customers.s_emails'){
							$filter.= " AND customers.s_emails='1' ";
						}
					}
				}
			}
			if(!$filter){
				$filter = '1=1';
			}
			$filter = ltrim($filter," AND");

			if(!$list_for){
				$filter .= " AND customer_contacts.active='1' ";
				$cont = $db->query("SELECT customer_contacts.contact_id
							    FROM customer_contacts
							    LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
							    LEFT JOIN customers ON customer_contacts.customer_id=customers.customer_id
							    LEFT JOIN customer_addresses ON customer_contacts.customer_id=customer_addresses.customer_id
							    LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
							    LEFT JOIN customer_type ON customers.c_type=customer_type.id
							    LEFT JOIN customer_sector ON customers.sector=customer_sector.id
							    LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
							    LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
							    LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
            			LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
            			LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
							    WHERE ".$filter." ");
			}else{
				$filter .= " AND customers.active='1'  AND is_admin='0' ";
				$cont =  $db->query("SELECT DISTINCT customers.customer_id AS contact_id
			        FROM customers
			        LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
			        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
			        LEFT JOIN customer_type ON customers.c_type=customer_type.id
			        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
			        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
			        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
			        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
							LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
							LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
            		WHERE ".$filter." ");
			}
		}
		$m=0;

		while ($cont->next()) {
			if(!in_array($cont->f('contact_id'),$contacts)){
				$contacts[] = $cont->f('contact_id');
			}
			$m++;
		}

		$in['total_contacts'] = $contacts_list->f('contacts');
		$in['contacts_ids'] = $contacts;
		// return true;
		if ($in['refresh'])
		{
			// $contacts = explode(',',$contacts);
			$i=0;
			$in['email'] = array();
			foreach ($contacts as $key){
				if(!$list_for){
					$contact = $db->query("SELECT * FROM customer_contacts WHERE contact_id='".$key."' ");
					$contact->next();
					$has_email  = $db->field("SELECT email from customer_contacts WHERE contact_id='".$key."' ");
					if($has_email){
						$in['email'][$i]	= $contact->f('email');
						$in['ids'][$i] 		= $key;
						$in['names'][$i] 	= $contact->f('firstname')." ".$contact->f('lastname');
					}else{
						if(!$in['no_error']){
							msg::notice ( gm('There were some contact with no email address that were not imported.'),'notice');
						}
					}
				}else{
					$has_email  = $db->query("SELECT c_email, name from customers WHERE customer_id='".$key."' ");
					if($has_email->f('c_email')){
						$in['email'][$i]	= $has_email->f('c_email');
						$in['ids'][$i] 		= $key;
						$in['names'][$i] 	= $has_email->f('name');
					}else{
						if(!$in['no_error']){
							msg::notice ( gm('There were some contact with no email address that were not imported.'), 'notice');
						}
					}
				}
				$i++;
			}

			$new_email = array();
			foreach ($in['email'] as $key => $value ){
				$email = array(
					'EmailAddress' => $value,
				  	'Name' => $in['names'][$key],
				);
				array_push($new_email,$email);
			}

			$toSendEmail = array_chunk($new_email, 999);
			foreach ($toSendEmail as $key ) {
				$result = $wrap->import($key, true);

				if($result->was_successful()) {
					if(!$list_for){
					    foreach ($in['email'] as $key => $value){
					    	$db->query("UPDATE customer_contacts SET list_id=CONCAT(list_id,',','".$in['list_id']."') WHERE contact_id='".$in['ids'][$key]."'");
					    	$db->query("INSERT INTO customer_contact_activity SET contact_id='".$in['ids'][$key]."', contact_activity_type='6', contact_activity_note='Subscribed to ".$in['list_name'].".', date='".time()."' ");
					    }
					}
				    $db->query("SELECT campaign_list_id FROM campaign_list WHERE campaign_list_id='".$in['list_id']."' ");
				    if(!$db->move_next()){
				    	$db->query("INSERT INTO campaign_list SET contact_list_id='".$in['lists_id']."', campaign_list_id='".$in['list_id']."' ");
				    }else{
				    	$db->query("UPDATE campaign_list SET contact_list_id='".$in['lists_id']."' WHERE campaign_list_id='".$in['list_id']."' ");
				    }
				    if(!$in['no_error']){
						msg::success ( gm("Subscribers added"), 'success');
					}
				    // return true;
				} else {
					if(!$in['no_error']){
						msg::error ( $result->response->Message, 'error');
					    msg::error ( $result->response->ResultData->FailureDetails[0]->Message,'error');

					    if($result->response->ResultData->TotalExistingSubscribers > 0) {
					        msg::error ( 'Updated '.$result->response->ResultData->TotalExistingSubscribers.' existing subscribers in the list', 'error');
					    } else if($result->response->ResultData->TotalNewSubscribers > 0) {
					        msg::error ( 'Added '.$result->response->ResultData->TotalNewSubscribers.' to the list','error');
					    } else if(count($result->response->ResultData->DuplicateEmailsInSubmission) > 0) {
					        msg::error ( $result->response->ResultData->DuplicateEmailsInSubmission.' were duplicated in the provided array.','error');
					    }
				    }
		//		    msg::$error .= 'The following emails failed to import correctly.';
				    // return true;
				}
			}
			$del_email = array_diff($list_email,$in['email']);
			foreach ($del_email as $key => $value) {
				$in['email'] = $value;
				$this->delete_sub($in);
			}
		}
		return true;
	}

	/**
	 * Insert subscriber in list
	 * @param  array $in
	 * @return bool
	 */
	function insert_subscriber(&$in)
	{
		$db = new sqldb();
		$wrap = $this->subscriber;
		$wrap->list_id = $in['list_id'];
		$wrap->set_list_id($wrap->list_id);

		$contacts_list = $db->query("SELECT * FROM contact_list WHERE contact_list_id='".$in['lists_id']."' ");
		$contacts_list->next();
		$list_for = $contacts_list->f('list_for');
		if(!$list_for){
			$contact = $db->query("SELECT * FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			$contact->next();
			$has_email  = $db->field("SELECT email from customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			if($has_email){
				$in['email']	= $contact->f('email')? $contact->f('email'):'';
				$in['ids']		= $in['contact_id'];
				$in['names'] 	= $contact->f('firstname')." ".$contact->f('lastname');
			}else{
				// msg::$notice = $l->lookup('There were some contact with no email address that were not imported.');
				msg::notice ( $contact->f('firstname')." ".$contact->f('lastname'),'notice');
				$in['failure']=true;
				// return false;
				json_out($in);
			}
		}else{
			$has_email  = $db->query("SELECT c_email, name from customers WHERE customer_id='".$in['contact_id']."' ");
			if($has_email->f('c_email')){
				$in['email']	= $has_email->f('c_email');
				$in['ids'] 		= $key;
				$in['names'] 	= $has_email->f('name');
			}else{
				if(!$in['no_error']){
					msg::notice ( gm('There were some contact with no email address that were not imported.'),'notice');
				}
			}
		}

		$email = array(
			'EmailAddress' => $in['email'],
		  	'Name' => $in['names'],
		  	'Resubscribe' => true,
		);

		$result = $wrap->add($email);
		if($result->response->Code == '205')
		{
			$result = $wrap->update($email['EmailAddress'],$email);
		}
		// print_r($result);
		if($result->was_successful()) {
			if(!$list_for){
		    	$db->query("UPDATE customer_contacts SET list_id=CONCAT(list_id,',','".$in['list_id']."') WHERE contact_id='".$in['ids']."'");
		    	$db->query("INSERT INTO customer_contact_activity SET contact_id='".$in['ids']."', contact_activity_type='6', contact_activity_note='Subscribed to ".$in['list_name'].".', date='".time()."' ");
		    }
		    $db->query("SELECT campaign_list_id FROM campaign_list WHERE campaign_list_id='".$in['list_id']."' ");
		    if(!$db->move_next()){
		    	$db->query("INSERT INTO campaign_list SET contact_list_id='".$in['lists_id']."', campaign_list_id='".$in['list_id']."' ");
		    }else{
		    	$db->query("UPDATE campaign_list SET contact_list_id='".$in['lists_id']."' WHERE campaign_list_id='".$in['list_id']."' ");
		    }
			msg::success ( gm("Subscribers added"),'success');
			json_out($in);
		    // return true;
		} else {
			$in['failure']=true;
			msg::error ( $result->response->Message.'<br />For '.$email['Name'].": ".$email['EmailAddress'], 'error');

		   /* if($result->response->ResultData->TotalExistingSubscribers > 0) {
		        msg::$error .= 'Updated '.$result->response->ResultData->TotalExistingSubscribers.' existing subscribers in the list'."<br>";
		    } else if($result->response->ResultData->TotalNewSubscribers > 0) {
		        msg::$error .= 'Added '.$result->response->ResultData->TotalNewSubscribers.' to the list'."<br>";
		    } else if(count($result->response->ResultData->DuplicateEmailsInSubmission) > 0) {
		        msg::$error .= $result->response->ResultData->DuplicateEmailsInSubmission.' were duplicated in the provided array.'."<br>";
		    }*/

//		    msg::$error .= 'The following emails failed to import correctly.';
			// return false;
			json_out($in);
		}
		// return true;
		json_out($in);
	}

	/**
	 * Update subscriber
	 * @param  array $in
	 * @return bool
	 */
	function update_subscriber(&$in){
		$wrap = $this->subscriber;
		$wrap->list_id = $in['list_id'];
		$wrap->set_list_id($wrap->list_id);

		$result = $wrap->update($in['email_sub'], array(
		    'EmailAddress' 	=> $in['email'],
		    'Name' 			=> $in['name'],
		    'Resubscribe' 	=> true
		));

		if($result->was_successful()) {
		    msg::success ( gm("Subscriber updated"),'success');
		    return true;
		} else {
//			print_r($result->response);
		    msg::error ( $result->response->Message, 'error');
		    return false;
		}
	}

	/**
	 * Delete subscriber
	 * @param  array $in
	 * @return bool
	 */
	function delete_sub(&$in){
		$wrap = $this->subscriber;
		$wrap->list_id = $in['list_id'];
		$wrap->set_list_id($wrap->list_id);

		$result = $wrap->delete($in['email']);

		$db = new sqldb();
		$contacts_list = $db->query("SELECT * FROM contact_list WHERE contact_list_id='".$in['lists_id']."' ");
		$contacts_list->next();
		$list_for = $contacts_list->f('list_for');

		$list_id = $db->field("SELECT list_id FROM customer_contacts WHERE email='".$in['email']."'");
		$new_list = explode(',',$list_id);
		foreach ($new_list as $key){
			if($key != $in['list_id'] && $key){
				$list .=$key.',';
			}
		}
		$list = rtrim($list,',');
		if(!$list_for){
			$db->query("UPDATE customer_contacts SET list_id='".$list."' WHERE email='".$in['email']."' ");
			$contact_id = $db->field("SELECT contact_id FROM customer_contacts WHERE email='".$in['email']."' ");
			$db->query("INSERT INTO customer_contact_activity SET contact_id='".$contact_id."', contact_activity_type='6', contact_activity_note='Unsubscribed from ".$in['list_name'].".', date='".time()."' ");
		}
	    msg::success ( gm("Subscriber deleted"),'success');

		return true;
	}

	/**
	 * Add filter rule for subscriber
	 * @param array $in
	 * @return bool
	 */
	function add_rule(&$in){
		$result = $this->segment->create($in['list_id'], array(
		    'Title' => $in['name'],
		    'Rules' => array(
		        array(
		            'Subject' => $in['rule_type'],
		            'Clauses' => array($in['rule_eval'].' '.$in['rule_text'])
		        )
		    )
		));

		if($result->was_successful()) {
		    msg::success ( gm("Rule added"),'success');
		    return true;
		} else {
		    msg::error ( $result->response->Message,'error');
		    return false;
		}
	}

	/**
	 * [delete_seg description]
	 * @param  array $in
	 * @return bool
	 */
	function delete_seg(&$in){
		$wrap = $this->segment;
		$wrap->segment_id = $in['segment_id'];
		$wrap->set_segment_id($wrap->segment_id);

		$result = $wrap->delete();

		if($result->was_successful()) {
		    msg::success ( gm('Segment deleted'),'success');
		    return true;
		} else {
		    msg::error ( $result->response->Message,'error');
		    return false;
		}
	}

	/**
	 * Update subscribers
	 * @param  array $in
	 * @return bool
	 */
	function refresh(&$in){
		$list = $this->db->field("SELECT contact_list_id FROM campaign_list WHERE campaign_list_id='".$in['list_id']."' ");
		if($list){
			$in['lists_id'] =$list;
			$in['refresh'] = '1';
			$this->add_subscriber($in);
			if(!$in['no_error']){
				msg::success ( gm("Subscribers updated"),'success');
			}
		}else {
			msg::notice("Please update your campaign manualy",'notice');
		}
		return true;
	}

	/**
	 * Syncronize campaigns
	 * @param  array $in
	 * @return bool
	 */
	function sync_campaign_monitor(&$in){
		if($in['chimp'] == "false"){
			$wrap = new CS_REST_Clients(ACCOUNT_CAMPAIGN_CLIENT, ACCOUNT_CAMPAIGN_APY);

			$result = $wrap->get_campaigns();

			if($result->was_successful()) {
				foreach ($result->response as $key){
			    	if($key->CampaignID == $in['campaign_id']){
						$date = $key->SentDate;
				    	$campaign_id = $key->CampaignID;
				    	# get the campaign summary
				    	$wrap_camp = new CS_REST_Campaigns($campaign_id, ACCOUNT_CAMPAIGN_APY);
						$result_camp = $wrap_camp->get_summary();
						$eee = $wrap_camp->get_lists_and_segments();
						$campaign_list_id = $eee->response->Lists[0]->ListID;
						$contact_list = $this->db->query("SELECT * FROM contact_list WHERE contact_list_id=(SELECT contact_list_id FROM campaign_list WHERE campaign_list_id='".$campaign_list_id."') ");
						$list_for = $contact_list->f('list_for');

						$camp_name = $key->Name;
						if($result_camp->was_successful()) {
				//		    print_r($result_camp->response);
							$camp = $this->db->field("SELECT campaign_id FROM campaign WHERE campaign_id='".$key->CampaignID."' ");

							if(!$camp){
								$this->db->query("INSERT INTO campaign SET campaign_id='".$key->CampaignID."',
																	 campaign_name='".addslashes($key->Name)."',
																	 date='".strtotime($date)."',
																	 recipients='".$result_camp->response->Recipients."',
																	 uniq_open='".$result_camp->response->UniqueOpened."',
																	 t_open='".$result_camp->response->TotalOpened."',
																	 bounce='".$result_camp->response->Bounced."',
																	 click='".$result_camp->response->Clicks."',
																	 unsubscribed='".$result_camp->response->Unsubscribed."',
																	 likes='".$result_camp->response->Likes."',
																	 list_id='".$campaign_list_id."',
																	 type='campaign_monitor' ");
							}else{
								$this->db->query("UPDATE campaign SET campaign_name='".addslashes($key->Name)."',
																date='".strtotime($date)."',
																recipients='".$result_camp->response->Recipients."',
																uniq_open='".$result_camp->response->UniqueOpened."',
																t_open='".$result_camp->response->TotalOpened."',
																bounce='".$result_camp->response->Bounced."',
																click='".$result_camp->response->Clicks."',
																unsubscribed='".$result_camp->response->Unsubscribed."',
																likes='".$result_camp->response->Likes."',
																list_id='".$campaign_list_id."'
														    WHERE campaign_id='".$key->CampaignID."' ");

							}
							$campaign_n = $key->Name;
							$result_unsub= $wrap_camp->get_unsubscribes();
					    	foreach ($result_unsub->response->Results as $mykey => $myvalue) {
					    		$unsub_mail = $myvalue->EmailAddress;
					    		if(!$list_for){
						    		$this->db->query("UPDATE customer_contacts SET s_email=0, unsubscribe='".strtotime($myvalue->Date)."' WHERE email='".$unsub_mail."'");
						    	}else{
						    		$this->db->query("UPDATE customers SET s_emails=0, unsubscribe='".strtotime($myvalue->Date)."' WHERE c_email='".$unsub_mail."'");
						    	}
					    	}

							$result_e = $wrap_camp->get_opens(date('Y-m-d', 0), 1, 100, 'email', 'asc');
							$result_b = $wrap_camp->get_bounces(date('Y-m-d', 0), 1, 300, 'email', 'asc');
							$result_u = $wrap_camp->get_unsubscribes(date('Y-m-d', 0), 1, 300, 'email', 'asc');
							$result_c = $wrap_camp->get_clicks(date('Y-m-d', 0), 1, 300, 'email', 'asc');
							$result_list = $wrap_camp->get_lists_and_segments();
							$list = array();
							if($result_list->was_successful()) {
								if(count($result_list->response->Lists) > 0){
								    foreach ($result_list->response->Lists as $keyy){
								    	$wrap_list = new CS_REST_Lists($keyy->ListID, ACCOUNT_CAMPAIGN_APY);
								    	$result_list_a = $wrap_list->get_active_subscribers(date('Y-m-d', 0));
											if($result_list_a->was_successful()) {
											    foreach ($result_list_a->response->Results as $keys){
													$list[$keys->EmailAddress]=$keys->Name;
											    }
											}
								    }
								}else if (count($result_list->response->Segments) > 0) {
									foreach ($result_list->response->Segments as $keyy){
								    	$wrap_list = new CS_REST_Lists($keyy->ListID, ACCOUNT_CAMPAIGN_APY);
								    	$result_list_a = $wrap_list->get_active_subscribers(date('Y-m-d', 0));
								    	if($result_list_a->was_successful()) {
										    foreach ($result_list_a->response->Results as $keys){
												$list[$keys->EmailAddress]=$keys->Name;
										    }
										}
								    }
								}
							}

							$campaign = array();
							$result_camp = $wrap_camp->get_recipients();
							if($result_camp->was_successful()) {
								foreach ($result_camp->response->Results as $keyy){
									$campaign[$keyy->EmailAddress] = 1;
								}
							}

							$email = array();
							$email_b = array();
							$email_u = array();
							$email_c = array();
							$added_emails = array();
							if($result_e->was_successful()) {
							//    print_r($result->response);
								$this->db->query("DELETE FROM campaign_data WHERE campaign_id='".$campaign_id."' ");
							    foreach ($result_e->response->Results as $keyy){
							    	if(array_key_exists($keyy->EmailAddress,$email)){
							    		$email[$keyy->EmailAddress] = $email[$keyy->EmailAddress]+1;
							    	}else{
							    		$email[$keyy->EmailAddress] = 1;
							    	}
							    	if(!$list_for){
								    	$contact = $this->db->field("SELECT contact_id FROM customer_contacts WHERE email='".$keyy->EmailAddress."' ");
								    }else{
								    	$contact = $this->db->field("SELECT customer_id FROM customers WHERE c_email='".$keyy->EmailAddress."' ");
								    }
							    	if($contact){
							    		$this->save_activity($contact, 'open', $campaign_id, $date, $camp_name, $keyy->Date);
							    	}
							    }

							    foreach ($result_b->response->Results as $key){
							    	$email_b[$key->EmailAddress] = $key->Reason;
							    }

							    foreach ($result_u->response->Results as $key ) {
							    	$email_u[$key->EmailAddress] = $key->Date;
							    	if(!$list_for){
								    	$contact = $this->db->field("SELECT contact_id FROM customer_contacts WHERE email='".$key->EmailAddress."' ");
								    }else{
								    	$contact = $this->db->field("SELECT customer_id FROM customers WHERE c_email='".$key->EmailAddress."' ");
								    }
							    	if($contact){
							    		$this->save_activity($contact, 'unsub', $campaign_id, $date, $camp_name, $keyy->Date);
							    	}
							    }
							    foreach ($result_c->response->Results as $key ) {
							    	$email_c[$key->EmailAddress] = 1;
							    	if(!$list_for){
								    	$contact = $this->db->field("SELECT contact_id FROM customer_contacts WHERE email='".$key->EmailAddress."' ");
								    }else{
								    	$contact = $this->db->field("SELECT customer_id FROM customers WHERE c_email='".$key->EmailAddress."' ");
								    }
							    	if($contact){
							    		$this->save_activity($contact, 'click', $campaign_id, $date, $camp_name, $keyy->Date);
							    	}
							    }
							    $total_emails = count(array_merge($email,$email_b,$email_u,$email_c));
							    $this->db->query("DELETE FROM campaign_email WHERE campaign_id='".$campaign_id."' ");
							    $in['campaign'] = $campaign;
							    $in['email'] = $email;
							    $in['email_b'] = $email_b;
							    $in['email_c'] = $email_c;
							    $in['email_u'] = $email_u;
							    $in['list'] = $list;
							    $in['campaign_id'] = $campaign_id;
							    $in['total_emails'] = $total_emails;
							    foreach ($email as $keyy => $value){
							    	$this->db->query("INSERT INTO campaign_email SET campaign_id='".$campaign_id."', name='".addslashes($list[$keyy])."', email='".$keyy."', open='".$value."' ");
							    	array_push($added_emails, $keyy);
							    }

							    foreach ($email_b as $keyy => $value){
							    	if(!in_array($keyy, $added_emails))
							    	{
							    		$this->db->query("INSERT INTO campaign_email SET campaign_id='".$campaign_id."', name='".addslashes($list[$keyy])."', email='".$keyy."', bounce='".$value."' ");
							    		array_push($added_emails, $keyy);
							    	}else
							    	{
							    		$this->db->query("UPDATE campaign_email SET bounce='".$value."' WHERE email='".$keyy."' AND campaign_id='".$campaign_id."'");
							    	}
							    }
							    foreach ($email_u as $keyy => $value){
							    	if(!in_array($keyy, $added_emails))
							    	{
							    		$this->db->query("INSERT INTO campaign_email SET campaign_id='".$campaign_id."', name='".addslashes($list[$keyy])."', email='".$keyy."', unsubscribe='1' ");
							    		array_push($added_emails, $keyy);
							    	}else
							    	{
							    		$this->db->query("UPDATE campaign_email SET unsubscribe='1' WHERE email='".$keyy."' AND campaign_id='".$campaign_id."'");
							    	}
							    }
							    foreach ($email_c as $keyy => $value){
							    	if(!in_array($keyy, $added_emails))
							    	{
							    		$this->db->query("INSERT INTO campaign_email SET campaign_id='".$campaign_id."', name='".addslashes($list[$keyy])."', email='".$keyy."', click='1' ");
							    		array_push($added_emails, $keyy);
							    	}else
							    	{
							    		$this->db->query("UPDATE campaign_email SET click='1' WHERE email='".$keyy."' AND campaign_id='".$campaign_id."'");
							    	}
							    }
						    	$email = array_merge($email,$email_b,$email_u,$email_c);
						    	$diff = array_diff_key($campaign, $email);
						    	foreach ($diff as $keyy => $value){
							       	$this->db->query("INSERT INTO campaign_email SET campaign_id='".$campaign_id."', name='".addslashes($list[$keyy])."', email='".$keyy."', open='0', bounce='', unsubscribe='0', click='0' ");
							    }
							}
							msg::success ( gm('Data stored succesfully'),'success');
							return true;
						} else {
						    msg::error ( $result_camp->response,'error');
						    return false;
						}
				    }else{
				    	continue;
				    }
			    }
			} else {
			    msg::error ( $result->response->Message,'error');
			    return false;
			}
		}else{
			ark::loadLibraries(array('MCAPI.class'));
			$api = $this->db->field("SELECT api FROM apps WHERE type='main' AND template_file='chimp' ");

			$chimp_c = new MCAPI($api);

			$retval = $chimp_c->campaigns();

			if ($chimp_c->errorCode){
				msg::error ( $chimp_c->errorMessage,'error');
				return false;
			} else {
			    foreach($retval['data'] as $c){
			      	if($c['id'] == $in['campaign_id']){
			      		$contact_list = $this->db->query("SELECT * FROM contact_list WHERE contact_list_id=(SELECT contact_list_id FROM chimp_list WHERE campaign_list_id='".$c['list_id']."') ");
						$list_for = $contact_list->f('list_for');
				    	$retval2 = $chimp_c->campaignStats($c['id']);

				    	$camp = $this->db->field("SELECT campaign_id FROM campaign WHERE campaign_id='".$in['campaign_id']."' ");
						if(!$chimp_c->errorCode) {
							if(!$camp){
								$this->db->query("INSERT INTO campaign SET campaign_id='".$in['campaign_id']."',
																	 campaign_name='".addslashes($c['title'])."',
																	 date='".strtotime($c['send_time'])."',
																	 recipients='".$c['emails_sent']."',
																	 uniq_open='".$retval2['unique_opens']."',
																	 t_open='".$retval2['opens']."',
																	 bounce='".$retval2['hard_bounces']."',
																	 click='".$retval2['clicks']."',
																	 unsubscribed='".$retval2['unsubscribes']."',
																	 likes='".$retval2['unique_likes']."',
																	 type='chimp',
																	 list_id='".$c['list_id']."' ");
							}else{
								$this->db->query("UPDATE campaign SET campaign_name='".addslashes($c['title'])."',
																date='".strtotime($c['send_time'])."',
																recipients='".$c['emails_sent']."',
																uniq_open='".$retval2['unique_opens']."',
																t_open='".$retval2['opens']."',
																bounce='".$retval2['hard_bounces']."',
																click='".$retval2['clicks']."',
																unsubscribed='".$retval2['unsubscribes']."',
																likes='".$retval2['unique_likes']."',
																list_id='".$c['list_id']."'
														    WHERE campaign_id='".$in['campaign_id']."' ");
							}

							# save data for clicks and opens end
							$this->db->query("DELETE FROM campaign_email WHERE campaign_id='".$in['campaign_id']."' ");
							$result = $chimp_c->campaignOpenedAIM($in['campaign_id']); #opened
							$result_b = $chimp_c->campaignMembers($in['campaign_id']); #sent
							// $result_c = $chimp_c->campaignClickDetailAIM($in['campaign_id']); #clicked
							//$result_bounce = $chimp_c->campaignBounceMessages($in['campaign_id']);
							$result_u = $chimp_c->campaignUnsubscribes($in['campaign_id']); #unsub
							$email_b = array();
							$k=0;
							foreach ($result_b['data'] as $key){
								//if($key['status'] != 'sent'){
									$id = $chimp_c->listsForEmail($key['email']);
									$name = $chimp_c->listMemberInfo($id[0],array($key['email']));
									$name = $name['data'][0]['merges']['FNAME'].' '.$name['data'][0]['merges']['LNAME'];
									$this->db->query("INSERT INTO campaign_email SET campaign_id='".$in['campaign_id']."', email='".$key['email']."', name='".addslashes($name)."' ");
									$email_b[] = $key['email'];
									if($key['status'] != 'sent'){
										$this->db->query("UPDATE campaign_email SET open='0', bounce='".$key['status']."' WHERE campaign_id='".$in['campaign_id']."' AND email='".$key['email']."'");
									}
								//}
							}
							$eee = $chimp_c->campaignEmailStatsAIMAll($in['campaign_id']);
							# save data for clicks and opens
							$this->db->query("DELETE FROM campaign_data WHERE campaign_id='".$in['campaign_id']."' ");
							foreach ($eee['data'] as $key => $value) {
								if(empty($value)){
									continue;
								}
								if(!$list_for){
									$contact = $this->db->field("SELECT contact_id FROM customer_contacts WHERE email='".$key."' ");
								}else{
									$contact = $this->db->field("SELECT customer_id FROM customers WHERE c_email='".$key."' ");
								}
								foreach ($value as $k => $v) {
									$this->save_activity($contact, $v['action'], $in['campaign_id'], 0, 0, $v['timestamp']);
									if($v['action'] == 'click'){
										 $this->db->query("UPDATE campaign_email SET click='1' WHERE campaign_id='".$in['campaign_id']."' AND email='".$key."'");
									}
								}
							}
							foreach ($result['data'] as $key){
								$this->db->query("UPDATE campaign_email SET open='".$key['open_count']."' WHERE campaign_id='".$in['campaign_id']."' AND email='".$key['email']."'");
							}
							$resulto = $chimp_c->campaignNotOpenedAIM($in['campaign_id']);
							foreach ($resulto['data'] as $key){
								$this->db->query("UPDATE campaign_email SET open='0' WHERE campaign_id='".$in['campaign_id']."' AND email='".$key."'");
							}
							/*foreach ($result_c['data'] as $key){
								  $this->db->query("UPDATE campaign_email SET click='{$key['clicks']}' WHERE campaign_id='".$in['campaign_id']."' AND email='".$key['email']."'");
							}*/
							foreach ($result_u['data'] as $key){
								  $this->db->query("UPDATE campaign_email SET unsubscribe='1' WHERE campaign_id='".$in['campaign_id']."' AND email='".$key['email']."'");
							}

							msg::success ( gm('Data stored succesfully'),'success');
							return true;
						}else{
							msg::error ( $chimp_c->errorCode,'error');
							return false;
						}

	   				}
				}
			}
		}
	}

	/**
	 * Save the activity type
	 *
	 * @return Boolean
	 * @author Mp
	 **/
	function save_activity($contact_id, $type, $list_id, $date, $name, $date_action)
	{
		switch ($type) {
			case 'open':
				$text = "Campaign opened on: ".$date_action.".";
				break;
			case 'click':
				$text = "Campaign clicked on: ".$date_action.".";
				break;
			case 'unsub':
				$text = "Unsubscribed from campaign on: ".$date_action.".";
				break;
			default:
				$text ='';
				break;
		}
		$this->db->query("INSERT INTO campaign_data SET message='".$text."', contact_id='".$contact_id."', campaign_id='".$list_id."' ");

		return true;
	}

}
?>