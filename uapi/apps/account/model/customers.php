<?php

class customers {

	function __construct() {
		$this->db = new sqldb();
		$this->db2 = new sqldb();
		$this->db_import = new sqldb;
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($database_users);
		$this->field_n='customer_id';
		$this->pag = 'customer';
		$this->pagc = 'xcustomer_contact';
		$this->field_nc = 'contact_id';
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addCustomer(&$in)
	{
		if(!$this->addCustomer_validate($in)){
			return false;
		}

		if(!$in['active']){
			$in['active'] = 1;
		}
		
		$c_types = '';
		$c_type_name = '';
		if($in['c_type']){
			/*foreach ($in['c_type'] as $key) {
				if($key){
					$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
					$c_type_name .= $type.',';
				}
			}
			$c_types = implode(',', $in['c_type']);
			$c_type_name = rtrim($c_type_name,',');*/
			if(is_array($in['c_type'])){
				foreach ($in['c_type'] as $key) {
					if($key){
						$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
						$c_type_name .= $type.',';
					}
				}
				$c_types = implode(',', $in['c_type']);
				$c_type_name = rtrim($c_type_name,',');
			}else{
				$c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");				
				$c_types = $in['c_type'];
			}
		}


		if($in['user_id']){
			/*foreach ($in['user_id'] as $key => $value) {
				$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
					$acc_manager .= $manager_id.',';
			}			
			$in['acc_manager'] = rtrim($acc_manager,',');
			$acc_manager_ids = implode(',', $in['user_id']);*/
			if(is_array($in['user_id'])){
				foreach ($in['user_id'] as $key => $value) {
					$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
						$acc_manager .= $manager_id.',';
				}			
				$in['acc_manager'] = rtrim($acc_manager,',');
				$acc_manager_ids = implode(',', $in['user_id']);
			}else{
				$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
				$acc_manager_ids = $in['user_id'];
			}
		}else{
			$acc_manager_ids = '';
			$in['acc_manager'] = '';
		}

		if(SET_DEF_PRICE_CAT == 1){
			$in['cat_id'] = $this->db->field("SELECT category_id FROM pim_article_price_category WHERE default_category=1");
		}
		if($in['contact_id']){
			$indiv_created = 1;
			$this->db->query("UPDATE customer_contacts SET indiv_created = '".$indiv_created."' WHERE contact_id = '".$in['contact_id']."' ");
		}


		$country_n = get_country_name($in['country_id']);
        // $query_sync=array();
        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
       	$payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
        $in['customer_id'] = $this->db->insert("INSERT INTO customers SET
                                                type                    = '".$in['type']."',
                                                firstname 				= '".$in['firstname']."',
												serial_number_customer  = '".$in['serial_number_customer']."',
												name 					= '".$in['name']."',
												commercial_name 		= '".$in['commercial_name']."',
												active					= '".$in['active']."',
												legal_type				= '".$in['legal_type']."',
												user_id					= '".$acc_manager_ids."',
												c_type					= '".$c_types."',
												website					= '".$in['website']."',
												language				= '".$in['language']."',
												c_email					= '".$in['c_email']."',
												sector					= '".$in['sector']."',
												comp_phone				= '".$in['comp_phone']."',
												comp_fax				= '".$in['comp_fax']."',
												activity				= '".$in['activity']."',
												comp_size				= '".$in['comp_size']."',
												lead_source				= '".$in['lead_source']."',
												various1				= '".$in['various1']."',
												various2				= '".$in['various2']."',
												c_type_name				= '".$in['c_type']."',
												zip_name				= '".$in['zip']."',
												our_reference			= '".$in['our_reference']."',
												invoice_email_type		= '1',
												acc_manager_name		= '".$in['acc_manager']."',
												sales_rep				= '".$in['sales_rep_id']."',
												city_name 				= '".$in['city']."',
												country_name 			= '".get_country_name($in['country_id'])."',
												payment_term 			= '".$payment_term."',
												payment_term_type 		= '".$payment_term_type."',
												cat_id					= '".$in['cat_id']."',
												vat_regime_id			= '1',
												btw_nr					= '".$in['btw_nr']."',
												internal_language		= '".$in['internal_language']."',
												is_supplier				= '".$in['is_supplier']."',
												is_customer				= '".$in['is_customer']."',
												creation_date			= '".time()."' ");

		$address_id = $this->db->insert("INSERT INTO customer_addresses SET
												customer_id			=	'".$in['customer_id']."',
												country_id			=	'".$in['country_id']."',
												state_id			=	'".$in['state_id']."',
												city				=	'".$in['city']."',
												zip					=	'".$in['zip']."',
												address				=	'".$in['address']."',
												billing				=	'1',
												is_primary			=	'1',
												delivery			=	'1'");
		// if($in['address']){
			if(!$in['zip'] && $in['city']){
				$address = $in['address'].', '.$in['city'].', '.$country_n;
			}elseif(!$in['city'] && $in['zip']){
				$address = $in['address'].', '.$in['zip'].', '.$country_n;
			}elseif(!$in['zip'] && !$in['city']){
				$address = $in['address'].', '.$country_n;
			}else{
				$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
			}
			$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
			$output= json_decode($geocode);
			$in['lat'] = $output->results[0]->geometry->location->lat;
			$in['lng'] = $output->results[0]->geometry->location->lng;
			$this->save_location($in);

		$in['value']=$in['btw_nr'];
		$check_valid_vat=$this->check_vies_vat_number($in);
		if($check_valid_vat != false){
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['customer_id']."' ");
		}else if($check_valid_vat == false && $in['trends_ok']){
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['customer_id']."' ");
		}else{
			$in['value']='';
		}
		// }
		// $this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");

        /*$query_sync[0]="INSERT INTO customers SET	                        customer_id       		= '".$in['customer_id']."',
        																	serial_number_customer           =   '".$in['serial_number_customer']."',
																			name 					= '".$in['name']."',
																			commercial_name 		= '".$in['commercial_name']."',
																			active					= '".$in['active']."',
																			legal_type				= '".$in['legal_type']."',
																			user_id					= '".$in['acc_manager_id']."',
																			c_type					= '".$c_types."',
																			website					= '".$in['website']."',
																			language				= '".$in['language']."',
																			c_email					= '".$in['c_email']."',
																			sector					= '".$in['sector']."',
																			comp_phone				= '".$in['comp_phone']."',
																			comp_fax				= '".$in['comp_fax']."',
																			activity				= '".$in['activity']."',
																			comp_size				= '".$in['comp_size']."',
																			lead_source				= '".$in['lead_source']."',
																			various1				= '".$in['various1']."',
																			various2				= '".$in['various2']."',
																			c_type_name				= '".$in['c_type']."',
																			zip_name				= '".$in['zip']."',
																			our_reference			= '".$in['our_reference']."',
																			acc_manager_name		= '".$in['acc_manager']."',
																			sales_rep				= '".$in['sales_rep']."',
																			city_name 				= '".$in['city']."',
																			country_name 			= '".get_country_name($in['country_id'])."',
																			payment_term 			= '".$payment_term."',
																			btw_nr				= '".$in['btw_nr']."',
																			check_vat_number 			= '".$in['value']."',
																			creation_date			= '".time()."'";
		$query_sync[1]="INSERT INTO customer_addresses SET
																			customer_id			=	'".$in['customer_id']."',
																			country_id			=	'".$in['country_id']."',
																			state_id			=	'".$in['state_id']."',
																			city				=	'".$in['city']."',
																			zip					=	'".$in['zip']."',
																			address				=	'".$in['address']."',
																			billing				=	'1',
																			is_primary			=	'1',
																			delivery			=	'1'";*/

        // Sync::start(ark::$model.'-'.ark::$method);

		// Sync::end($in['customer_id'],$query_sync);
		set_first_letter('customers',$in['name'],$this->field_n,$in['customer_id']);

		//save extra fields values
		$this->update_custom_field($in);

		// msg::$success = gm("Changes have been saved.");
		insert_message_log($this->pag,'{l}Customer added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['pagl'] = $this->pag;

		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
										 								AND name 		= 'company-customers_show_info'	");
		if(!$show_info->move_next()) {
			$this->db_users->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
																name    = 'company-customers_show_info',
																value   = '1' ");
		} else {
			$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
																	  AND name 		= 'company-customers_show_info' ");
		}
		self::sendCustomerToZintra($in);

		$count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
		if($count == 1){
			doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
		}

		// ark::$controller = 'customer';
		msg::success ( gm("Changes have been saved."),'success');

		// header("Location: index.php?do=company-customer&customer_id=".$in['customer_id']."");
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer(&$in)
	{

		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->updateCustomer_validate($in)){
			return false;
		}
        // $query_sync=array();
		
		// $c_type = explode(',', $in['c_type']);
		$c_types = '';
		$c_type_name = '';
		if($in['c_type']){
			if(is_array($in['c_type'])){
				foreach ($in['c_type'] as $key) {
					if($key){
						$type = $this->db->field("SELECT name FROM customer_type WHERE id='".$key."' ");
						$c_type_name .= $type.',';
					}
				}
				$c_types = implode(',', $in['c_type']);
				$c_type_name = rtrim($c_type_name,',');
			}else{
				$c_type_name = $this->db->field("SELECT name FROM customer_type WHERE id='".$in['c_type']."' ");				
				$c_types = $in['c_type'];
			}
		}


		if($in['user_id']){
			if(is_array($in['user_id'])){
				foreach ($in['user_id'] as $key => $value) {
					$manager_id = $this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$value."' AND database_name = '".DATABASE_NAME."' ");
						$acc_manager .= $manager_id.',';
				}			
				$in['acc_manager'] = rtrim($acc_manager,',');
				$acc_manager_ids = implode(',', $in['user_id']);
			}else{
				$in['acc_manager'] =$this->db_users->field("SELECT CONCAT_WS(' ',first_name, last_name) FROM users WHERE user_id ='".$in['user_id']."' AND database_name = '".DATABASE_NAME."' ");
				$acc_manager_ids = $in['user_id'];
			}
		}else{
			$acc_manager_ids = '';
			$in['acc_manager'] = '';
		}

		// Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET name 				= '".$in['name']."',
		                 					firstname 				= '".$in['firstname']."',
											serial_number_customer  = '".$in['serial_number_customer']."',
											commercial_name 		= '".$in['commercial_name']."',
											active					= '".$in['active']."',
											legal_type				= '".$in['legal_type']."',
											user_id					= '".$acc_manager_ids."',
											c_type					= '".$c_types."',
											website					= '".$in['website']."',
											language				= '".$in['language']."',
											c_email					= '".$in['c_email']."',
											sector					= '".$in['sector']."',
											comp_phone				= '".$in['comp_phone']."',
											comp_fax				= '".$in['comp_fax']."',
											activity				= '".$in['activity']."',
											comp_size				= '".$in['comp_size']."',
											lead_source				= '".$in['lead_source']."',
											c_type_name				= '".$c_type_name."',
											various1				= '".$in['various1']."',
											various2				= '".$in['various2']."',
											acc_manager_name		= '".$in['acc_manager']."',
											sales_rep				= '".$in['sales_rep']."',
											is_supplier				= '".$in['is_supplier']."',
											is_customer				= '".$in['is_customer']."',
											our_reference			= '".$in['our_reference']."'
					  	WHERE customer_id='".$in['customer_id']."' ");

		$this->db->query("UPDATE customer_addresses SET
												country_id			=	'".$in['country_id']."',
												state_id			=	'".$in['state_id']."',
												city				=	'".$in['city']."',
												zip					=	'".$in['zip']."',
												address				=	'".$in['address']."'
												WHERE address_id 	=	'".$in['address_id']."'												
												");

		if($in['customer_id_linked']){
			$link_id = $this->db->insert("INSERT INTO customer_link SET customer_id='".$in['customer_id']."',
														customer_id_linked='".$in['customer_id_linked']."',
														link_type='".$in['link_type']."'");
			if($in['link_type'] == 1){
				$type = 2;
			}
			else{
				$type = 1;
			}
			$this->db->query("INSERT INTO customer_link SET customer_id='".$in['customer_id_linked']."',
														customer_id_linked='".$in['customer_id']."',
														link_type='".$type."',
														link_id='".$link_id."'");
		}

		$this->db->query("UPDATE customer_contacts SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$this->db->query("UPDATE projects SET company_name='".$in['name']."' WHERE customer_id='".$in['customer_id']."' ");
		$legal_type = $this->db->field("SELECT name FROM customer_legal_type WHERE id='".$in['legal_type']."' ");
		$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['name'].' '.$legal_type."'	WHERE buyer_id='".$in['customer_id']."' ");

		
		// Sync::end($in['customer_id']);

		set_first_letter('customers',$in['name'],'customer_id',$in['customer_id']);

		//save extra fields values
		$this->update_custom_field($in);

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		// $in['pagl'] = $this->pag;
		self::sendCustomerToZintra($in);
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function updateCustomer_validate(&$in)
	{

		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		if($in['customer_link']){
			$v->field('link_type', 'Type of link', 'required');
		}
		return $this -> addCustomer_validate($in);

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function addCustomer_validate(&$in)
	{
		$in['c_email'] = trim($in['c_email']);
		$v = new validation($in);
		$v->field('name', 'Name', 'required');

		if (ark::$method == 'addCustomer') {
			$v->field('country_id', 'Country', 'required:exist[country.country_id]');
		}
		if (ark::$method == 'updateCustomer') {
        	$our_ref_val=$this->db->field("SELECT value FROM settings WHERE constant_name='USE_COMPANY_NUMBER' ");
		  	if($in['our_reference'] && $our_ref_val){
		 			$v->field('our_reference','Company Nr.',"unique[customers.our_reference.(customer_id!='".$in['customer_id']."')]");
		  	}
	  	}


		if($in['c_email']){
			if (ark::$method == 'addCustomer')
			{
				$v->field('c_email', 'Email', 'email:unique[customers.c_email]');
			}
			else
			{
				$v->field('c_email','Email',"email:unique[customers.c_email.(customer_id!='".$in['customer_id']."')]");
			}
		}
		if($in['do']=='customers-EditCustomer-customers-addCustomer'){
			if(USE_COMPANY_NUMBER == 1){
         		$in['our_reference']= generate_customer_number(DATABASE_NAME);
         	}
      	}


		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function CanEdit(&$in){
		if(ONLY_IF_ACC_MANAG ==0 && ONLY_IF_ACC_MANAG2==0){
			return true;
		}
		$c_id = $in['customer_id'];
		if(!$in['customer_id'] && $in['contact_id']) {
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
		}
		if($c_id){
			if($_SESSION['group'] == 'user' && !in_array('company', $_SESSION['admin_sett']) ){
				$u = $this->db->field("SELECT user_id FROM customers WHERE customer_id='".$c_id."' ");
				if($u){
					$u = explode(',', $u);
					if(in_array($_SESSION['u_id'], $u)){
						return true;
					}
					else{
						msg::$warning = gm("You don't have enought privileges");
						return false;
					}
				}else{
					msg::$warning = gm("You don't have enought privileges");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function sendCustomerToZintra(&$in)
	{
		if(!defined("ZINTRA_ACTIVE") || ZINTRA_ACTIVE != 1){
			return true;
		}
		$vars_post = array();
		$data = $this->db->query("SELECT * FROM customers WHERE customer_id='".$in['customer_id']."' ");
		if($data){
			$bar_code_start = null;
			$bar_code_end = null;
			$custom_fields = $this->db->query("SELECT * FROM customer_field WHERE customer_id='".$in['customer_id']."' ");
			while($custom_fields->next()){
				if($custom_fields->f('field_id') == 1){
					$bar_code_start = $custom_fields->f('value');
				}
				if($custom_fields->f('field_id') == 2){
					$bar_code_end = $custom_fields->f('value');
				}
			}
			//asdkauas
			$primary = array();
			$address = $this->db->query("SELECT * FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
			if($address) {
				$primary = array(
					"Unformatted"=> null,
			    "Label"=> null,
			    "Building"=> null,
			    "Street"=> $address->f('address'),
			    "PostalCode"=> $address->f('zip'),
			    "City"=> $address->f('city'),
			    "State"=> $address->f('state_id'),
			    "Country"=> get_country_name($address->f('country_id')),
			    "CountryCode"=> get_country_code($address->f('country_id'))
			  );
			}
			$test = array();
			$vars_post = array(
				'Id'											=> (string)$in['customer_id'],
				"Name"										=> $data->f('name'),
			  "VatNumber"								=> $data->f('btw_nr') ? $data->f('btw_nr') : null,
			  "TimeZone"								=> null,
			  "Note"										=> null,
			  "PictureURI"							=> null,
			  "Importance"							=> null,
			  "Blog"										=> null,
			  "WebSite"									=> $data->f('website') ? $data->f('website') : null,
			  "FixedPhoneNumber"				=> $data->f('comp_phone') ? $data->f('comp_phone') : null,
			  "MobilePhoneNumber"				=> $data->f('other_phone') ? $data->f('other_phone') : null,
			  "FaxNumber"								=> $data->f('comp_fax') ? $data->f('comp_fax') : null,
			  "EMailAddress"						=> $data->f('c_email') ? $data->f('c_email') : null,
			  "DeliveryPostalAddress"		=> null,
			  "InvoicePostalAddress"		=> null,
			  "PostalAddress"						=> $primary,
			  "Fields"									=> array(
					array(
					  "Name"								=> "Reference",
					  "Value"								=> $data->f('our_reference')
					),
					array(
					  "Name"								=> "BarcodeFrom",
					  "Value"								=> $bar_code_start
					),
					array(
					  "Name"								=> "BarcodeTo",
					  "Value"								=> $bar_code_end
					)
			  ),
			  "Links"										=> $test,
			  "Categories"							=> null
			);

			$put = $this->c_rest->execRequest('https://app.zintra.eu/datas/Organization/'.$in['customer_id'],'put',$vars_post);
			console::log($put);
			$this->db->query("UPDATE customers SET zintraadded='1' WHERE customer_id='".$in['customer_id']."' ");
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->update_financial_valid($in)){
			return false;
		}
		$apply_fix_disc = 0;
		if($in['apply_fix_disc']){$apply_fix_disc = 1;}
		$apply_line_disc = 0;
		if($in['apply_line_disc']){$apply_line_disc = 1;}
		$check_vat_number = $this->db->field("SELECT check_vat_number FROM customers WHERE customer_id = '".$in['customer_id']."' ");
		if($in['btw_nr']!=$check_vat_number){
			$check_vat_number = '';
		}
		if($in['invoice_email_type']==1){

		    $in['invoice_email']='';
		    $in['attention_of_invoice']='';
		}
		// Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET
											btw_nr								=	'".$in['btw_nr']."',
											bank_name							=	'".$in['bank_name']."',
											payment_term					=	'".$in['payment_term']."',
											payment_term_type 		= '".$in['payment_term_start']."',
											bank_bic_code					=	'".$in['bank_bic_code']."',
											fixed_discount				=	'".return_value($in['fixed_discount'])."',
											bank_iban							=	'".$in['bank_iban']."',
                      						vat_id								=	'".$in['vat_id']."',
											cat_id								=	'".$in['cat_id']."',
											no_vat								=	'".$in['no_vat']."',
											currency_id						= '".$in['currency_id']."',
											invoice_email					=	'".$in['invoice_email']."',
											invoice_email_type					=	'".$in['invoice_email_type']."',
											attention_of_invoice		   =	'".$in['attention_of_invoice']."',
											invoice_note2					=	'".$in['invoice_note2']."',
											deliv_disp_note				=	'".$in['deliv_disp_note']."',
											external_id						=	'".$in['external_id']."',
											internal_language			=	'".$in['internal_language']."',
											apply_fix_disc				= '".$apply_fix_disc."',
											apply_line_disc				= '".$apply_line_disc."',
											line_discount 				= '".return_value($in['line_discount'])."',
											comp_reg_number 				= '".$in['comp_reg_number']."',
											vat_regime_id				= '".$in['vat_regime_id']."',
											siret						= '".$in['siret']."',
											check_vat_number 			= '".$check_vat_number."',
											identity_id                 = '".$in['identity_id']."'
											WHERE customer_id			= '".$in['customer_id']."' ");
		//quote_reference			=	'".$in['quote_reference']."',

		// Sync::end($in['customer_id']);
		// msg::$success = gm("Changes have been saved.");
		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer updated by{endl} '.get_user_name($_SESSION['u_id']),'customer_id',$in['customer_id']);
		self::sendCustomerToZintra($in);
		$in['updateFromFinancial1'] = true;
		$in['updateFromFinancial'] = true;
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_financial_valid(&$in)
	{
		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
		if($in['invoice_email']){
			$v->field('invoice_email', 'Type of link', 'email');
		}

		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function save_location(&$in)
	{
		$this->db->query("INSERT INTO addresses_coord SET
							customer_id='".$in['customer_id']."',
							location_lat='".$in['lat']."',
							location_lng='".$in['lng']."'");
		return true;
	}

	function check_vies_vat_number(&$in){
		$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');

		if(!$in['value'] || $in['value']==''){			
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = 'error';
				json_out($in);
			}
			return false;
		}
		$value=trim($in['value']," ");
		$value=str_replace(" ","",$value);
		$value=str_replace(".","",$value);
		$value=strtoupper($value);

		$vat_numeric=is_numeric(substr($value,0,2));

		if(!in_array(substr($value,0,2), $eu_countries)){
			$value='BE'.$value;
		}
		if(in_array(substr($value,0,2), $eu_countries)){
			$search   = array(" ", ".");
			$vat = str_replace($search, "", $value);
			$_GET['a'] = substr($vat,0,2);
			$_GET['b'] = substr($vat,2);
			$_GET['c'] = '1';
			$dd = include('valid_vat.php');
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = gm('Error');
				json_out($in);
			}
			return false;
		}
		if(isset($response) && $response == 'invalid'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = gm('Error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'error'){
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = gm('Error');
				json_out($in);
			}
			return false;
		}else if(isset($response) && $response == 'valid'){
			$full_address=explode("\n",$result->address);
			switch($result->countryCode){
				case "RO":
					$in['comp_address']=$full_address[1];
					$in['comp_city']=$full_address[0];
					$in['comp_zip']=" ";
					break;
				case "NL":
					$zip=explode(" ",$full_address[2],2);
					$in['comp_address']=$full_address[1];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
				default:
					$zip=explode(" ",$full_address[1],2);
					$in['comp_address']=$full_address[0];
					$in['comp_zip']=$zip[0];
					$in['comp_city']=$zip[1];
					break;
			}

			$in['comp_name']=$result->name;

			$in['comp_country']=(string)$this->db->field("SELECT country_id FROM country WHERE code='".$result->countryCode."' ");
			$in['comp_country_name']=gm($this->db->field("SELECT name FROM country WHERE code='".$result->countryCode."' "));
			$in['full_details']=$result;
			$in['vies_ok']=1;
			$in['crm']['country_dd']		= build_country_list(0);
			$in['crm']['country_id']		= $in['comp_country'] ? $in['comp_country'] : '26';
			$in['crm']['address']			= $in['comp_address'];
			$in['crm']['city']				= $in['comp_city'];
			$in['crm']['zip']				= $in['comp_zip'];
			$in['crm']['customer_id']		= $in['customer_id'];
			if($in['customer_id']){
				$country_id=$this->db->field("SELECT country_id FROM customer_addresses WHERE customer_id='".$in['customer_id']."' AND is_primary='1' ");
				if($in['comp_country'] != $country_id){
				 	$this->db->query("UPDATE customers SET no_vat='1',vat_regime_id='2' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['remove_v']=1;
				}else if($in['comp_country'] == $country_id){
					$this->db->query("UPDATE customers SET no_vat='0',vat_regime_id='1' WHERE customer_id='".$in['customer_id']."' ");
				 	$in['add_v']=1;
				}
			}
			if(ark::$method == 'check_vies_vat_number'){
				msg::success ( gm('VAT Number is valid'),'success');
				$in['success'] = gm('VAT Number is valid');
				json_out($in);
			}
			return true;
		}else{
			if(ark::$method == 'check_vies_vat_number'){
				msg::error ( gm('Error'),'error');
				$in['error'] = gm('Error');
				json_out($in);
			}
			return false;
		}
		if(ark::$method == 'check_vies_vat_number'){
			json_out($in);
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function update_custom_field(&$in)
	{
		$this->db->query("DELETE FROM customer_field WHERE customer_id='".$in['customer_id']."' ");
		if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO customer_field SET customer_id='".$in['customer_id']."', value='".addslashes($value)."', field_id='".$key."' ");
			}
		}
		// msg::$success = gm("Customer updated");
		if(ark::$model == 'update_custom_field'){
			self::sendCustomerToZintra($in);
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function archive(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->archive_validate($in)){
			return false;
		}
        // Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET active='0'  where customer_id='".$in['customer_id']."' ");
		$this->db->query("UPDATE customer_contacts SET active='0'  where customer_id='".$in['customer_id']."' ");

		// Sync::end($in['customer_id']);
		msg::success ( gm("Company has archived"),'success');

		insert_message_log($this->pag,'{l}Customer archived by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['pagl'] = $this->pag;
		$this->deleteCustomerFromZintra($in);
		/*if($in['front_register']){
			return ark::run('company-new_companies_list');
		}*/
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function activate(&$in)
	{
		if(!$this->archive_validate($in)){
			return false;
		}
        // Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customers SET active='1'  where customer_id='".$in['customer_id']."' ");

		// Sync::end($in['customer_id']);
		msg::success ( gm("Company has activated"),'success');
		insert_message_log($this->pag,'{l}Customer activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['pagl'] = $this->pag;
		self::sendCustomerToZintra($in);
		/*if($in['front_register']){
			return ark::run('company-new_companies_list');
		}*/
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function archive_validate(&$in)
	{
		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function deleteCustomerFromZintra(&$in)
	{
		if(!defined("ZINTRA_ACTIVE") || ZINTRA_ACTIVE != 1){
			return true;
		}
		$put = $this->c_rest->execRequest('https://app.zintra.eu/datas/Organization/'.$in['customer_id'],'delete');
		console::log($put);
		$this->db->query("UPDATE customers SET zintraadded='1' WHERE customer_id='".$in['customer_id']."' ");
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function showGraph(&$in)
	{
		// $this
		$sql_txt = "UPDATE ";
		$YodaDashboardGraph = $this->db_users->field("SELECT value FROM user_meta WHERE name='YodaDashboardGraph' AND user_id='".$_SESSION['u_id']."' ");
		if($YodaDashboardGraph == null){
			$sql_txt = "INSERT INTO ";
		}
		$YodaDashboardGraph = explode(';', $YodaDashboardGraph);
		if($in['show']==''){			
			array_push($YodaDashboardGraph,$in['key']);
			$value = implode(';', $YodaDashboardGraph);
			$this->db_users->field($sql_txt." user_meta SET value='".$value."' WHERE name='YodaDashboardGraph' AND user_id='".$_SESSION['u_id']."' ");
		}else{
			$diff = array_diff($YodaDashboardGraph, $in['key'] );
			$value = implode(';', $diff);
			$this->db_users->field($sql_txt." user_meta SET value='".$value."' WHERE name='YodaDashboardGraph' AND user_id='".$_SESSION['u_id']."' ");
		}
		return true;
	}

	/**
	 * Activate a contact
	 * @param  array $in
	 * @return bool
	 */
	function activate_contact(&$in)
	{
		if(!$this->archive_contact_validate($in)){
			return false;
		}
         // Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customer_contacts SET active='1'  where contact_id='".$in['contact_id']."' ");
		// Sync::end();
		msg::success ( gm('Contact has activated'),'success');
		insert_message_log($this->pagc,'{l}Contact activated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['contact_id']);
		return true;
	}

	/**
	 * Archive a contact
	 * @param  array $in
	 * @return bool
	 */
	function archive_contact(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->archive_contact_validate($in)){
			return false;
		}
        // Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("UPDATE customer_contacts SET active='0'  where contact_id='".$in['contact_id']."' ");
        // Sync::end();
		msg::success (gm('Contact has archived'),'success');

		insert_message_log($this->pagc,'{l}Contact archived by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['contact_id']);
		return true;
	}

	/**
	 * Archive a contact validate
	 * @param  array $in
	 * @return bool
	 */
	function archive_contact_validate(&$in)
	{
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', gm('Invalid ID'));
		return $v->run();
	}
	
	function contact_unsubscribe(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		$in['time'] = time();
		$this->db->query("UPDATE customer_contacts SET unsubscribe='".$in['time']."', s_email='0' WHERE contact_id='".$in['contact_id']."' ");
		msg::success ( gm("Contact Updated").".",'success');
		return true;
	}

	function save_view_list(&$in){
		$view='';
		if($in['view']==1){
			$view = '1';
		}elseif($in['view']==2){
			$view = '2';
		}else{
			$view = '0';
		}
		$this->db->query("UPDATE settings SET value = '".$view."' WHERE constant_name = 'CUSTOMER_VIEW_LIST' ");
		msg::success (gm('Changes have been saved.'),'success');
		json_out($in);
		return true;
	}

	/**
	 * Update contact data
	 * @param  array $in
	 * @return bool
	 */
	function contact_update(&$in)
	{

		if(!$this->CanEdit($in)){
			return false;
		}

		if(!$this->contact_update_validate($in)){
			return false;
		}
		if($in['birthdate']){
			$in['birthdate'] = strtotime($in['birthdate']);
		}

		// $pos = addslashes(preg_replace('/:[0-9]+/', '', $in['position_n']));
/*		$pos = array();
		foreach ($in['position'] as $key => $value) {
			$pos[] = addslashes($this->db->field("SELECT name FROM customer_contact_job_title WHERE id='".$value."' "));
		}
		if(!empty($pos)){
			$pos = implode(',', $pos);
		}else{
			$pos = '';
		}
		$in['position'] = implode(',', $in['position']);*/
		// Sync::start(ark::$model.'-'.ark::$method);

		$this->db->query("UPDATE customer_contacts SET
                                            customer_id	=	'".$in['customer_id']."',
                                            firstname	=	'".$in['firstname']."',
                                            lastname	=	'".$in['lastname']."',
                                            position	=	'".$in['position']."',
                                            department	=	'".$in['department']."',
                                            email		=	'".$in['email']."',
                                            birthdate	=	'".$in['birthdate']."',
					    					phone		=	'".$in['phone']."',
										    cell		=	'".$in['cell']."',
										    note		=	'".$in['note']."',
										    sex			=	'".$in['sex']."',
										    language	=	'".$in['language']."',
										    title		=	'".$in['title_contact_id']."',
										    e_title		=	'".$in['e_title']."',
											fax			=	'".$in['fax']."',
											company_name=	'".$in['customer']."',
											title_name	=	'".$in['title_name']."',
											position_n  = 	'".$pos."',
										    last_update =	'".time()."'
				WHERE
			 	contact_id	=	'".$in['contact_id']."'");
		$this->db->query("UPDATE customer_contacts SET s_email='0' WHERE contact_id='".$in['contact_id']."' ");
		if($in['s_email']){
			$this->db->query("UPDATE customer_contacts SET s_email='1', unsubscribe='' WHERE contact_id='".$in['contact_id']."' ");
		}

		if ($in['password'])
		{
			$this->db->query("UPDATE customer_contacts SET password='".md5($in['password'])."' WHERE contact_id='".$in['contact_id']."'");
		}

		$this->updateContactAccounts($in);

		$this->updateContactCustomerName($in); 

		// Sync::end($in['contact_id']);

		$this->db->query("SELECT * FROM recurring_invoice WHERE contact_id='".$in['contact_id']."' ");
		while ($this->db->move_next()) {
			$this->db->query("UPDATE recurring_invoice SET buyer_name='".$in['firstname'].' '.$in['lastname']."',
																							buyer_email='".$in['email']."',
																							buyer_phone='".$in['phone']."'
												WHERE contact_id='".$in['contact_id']."' ");
		}
		if(!$in['customer_id']){
			$this->removeFromAddress($in);
		}

		$project_company_name = $in['firstname'].' '.$in['lastname'];
		$this->db->query("UPDATE projects SET company_name = '".$project_company_name."' WHERE customer_id = '".$in['contact_id']."' AND is_contact = '1' ");

		set_first_letter('customer_contacts',$in['lastname'],$this->field_nc,$in['contact_id']);

		//save extra fields values
		$this->update_custom_contact_field($in);

		msg::success ( gm('Contact Updated').'.','success');
		insert_message_log($this->pagc,'{l}Contact updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_nc,$in['contact_id']);
		$in['pagl'] = $this->pagc;
		$this->contact_address($in);
		return true;
	}

	/**
	 * Validate update contact data
	 * @param  array $in
	 * @return [type]     [description]
	 */
	function contact_update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', gm('Invalid ID'));
		$is_ok = $v->run();

		if(!$this->contact_add_validate($in)){
			$is_ok = false;
		}
		/*if($in['customer_id']){
			$c_id = $this->db->field("SELECT customer_id FROM customer_contacts WHERE contact_id='".$in['contact_id']."' ");
			if($c_id){
				if($c_id != $in['customer_id']){
					$this->removeFromAddress($in);
				}
			}
		}*/

		return $is_ok;
	}

	/**
	 * undocumented function
	 *
	 * @return true
	 * @param array $in
	 * @author Marius Pop
	 **/
	function contact_add_validate(&$in)
	{
		$in['email'] = trim($in['email']);
		$v = new validation($in);
		$v->field('firstname', 'First name', 'required');
		$v->field('lastname', 'Last name', 'required');
		if(ark::$method=='contact_add'){
			$v->field('customer_id', 'Customer', 'required');
		}
/*		if ($in['do']=='company-xcustomer_contact-customer-contact_add')
		{
			$v->field('email', 'Email', 'email:unique[customer_contacts.email]');
		}
		else
		{
			$v->field('email','Email',"email:unique[customer_contacts.email.(contact_id!='".$in['contact_id']."')]");
		}*/
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function removeFromAddress(&$in)
	{
		$this->db->query("DELETE FROM customer_contact_addresses WHERE contact_id='".$in['contact_id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function update_custom_contact_field(&$in)
	{
		$this->db->query("DELETE FROM contact_field WHERE customer_id='".$in['contact_id']."' ");
		if($in['extra']){
			foreach ($in['extra'] as $key => $value) {
				$this->db->query("INSERT INTO contact_field SET customer_id='".$in['contact_id']."', value='".addslashes($value)."', field_id='".$key."' ");
			}
		}
		// msg::$success = gm("Customer updated");
		if(ark::$model == 'update_custom_field'){
			self::sendCustomerToZintra($in);
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function contact_address(&$in)
	{
		$in['primary']=1;
		$exist = $this->db->field("SELECT address_id FROM customer_contact_address WHERE is_primary='1' AND contact_id='".$in['contact_id']."' ");
		if($exist){
			$in['address_id'] = $exist;
			$this->update_contact_address($in);
		}else{
			$this->add_contact_address($in);
		}
	}

	/****************************************************************
	* function add_contact_address(&$in)                         *
	****************************************************************/
	function add_contact_address(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$in['country_id'] && !$in['state'] && !$in['city'] && !$in['zip'] && !$in['address'] ){
			msg::error (  gm('Please fill at least one of the fields'),'error');
			return false;
		}
        // Sync::start(ark::$model.'-'.ark::$method);
        $delivery = 0;
        $primary = 0;
        if($in['delivery']){
        	$delivery = 1;
        }
        if($in['primary']){
        	$primary = 1;
        }
        if($in['primary']){
        	$this->db->query("UPDATE customer_contact_address SET is_primary='0' WHERE contact_id='".$in['contact_id']."' ");
        	#updating existing recurring invoices
        	$this->db->query("SELECT * FROM recurring_invoice WHERE contact_id='".$in['contact_id']."' ");
			while ($this->db->move_next()) {
				$this->db->query("UPDATE recurring_invoice SET buyer_address	= '".$in['address']."',
																	buyer_zip	= '".$in['zip']."',
																	buyer_city	= '".$in['city']."',
																buyer_country_id= '".$in['country_id']."'
																WHERE contact_id= '".$in['contact_id']."' ");
			}
        }
        $this->db->query("INSERT INTO customer_contact_address SET country_id 	=	'".$in['country_id']."',
														  		   	state 		=	'".$in['state']."',
														           	city 		=	'".$in['city']."',
															        zip 		=	'".$in['zip']."',
															        address 	=	'".$in['address']."',
															        delivery 	=	'".$delivery."',
															        is_primary 	=	'".$primary."',
														      		contact_id 	=	'".$in['contact_id']."' ");
		// Sync::end($in['address_id']);
		msg::success (gm('Address added'),'success');
		insert_message_log($this->pagc,'{l}Contact updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_nc,$in['contact_id']);
		$in['pagl'] = $this->pagc;
		return true;
	}

	/****************************************************************
	* function update_contact_address(&$in)                         *
	****************************************************************/
	function update_contact_address(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$in['country_id'] && !$in['state'] && !$in['city'] && !$in['zip'] && !$in['address'] ){
			msg::error (gm('Please fill at least one of the fields'),'success');
			return false;
		}

        // Sync::start(ark::$model.'-'.ark::$method);
        $delivery = 0;
        if($in['delivery']){
        	$delivery = 1;
        }
        $this->db->query("UPDATE customer_contact_address SET country_id 	=	'".$in['country_id']."',
														  		   state 	=	'".$in['state']."',
														           city 	=	'".$in['city']."',
														           zip 		=	'".$in['zip']."',
														           address 	=	'".$in['address']."',
														           delivery =	'".$delivery."'
													      WHERE contact_id 	=	'".$in['contact_id']."'
													      AND  	address_id 	= 	'".$in['address_id']."' ");
        if($in['primary']){
        	$this->db->query("UPDATE customer_contact_address SET is_primary='0' WHERE contact_id='".$in['contact_id']."' ");
        	$this->db->query("UPDATE customer_contact_address SET is_primary='1' WHERE contact_id='".$in['contact_id']."' AND address_id='".$in['address_id']."' ");
        	#updating existing recurring invoices
        	$this->db->query("SELECT * FROM recurring_invoice WHERE contact_id='".$in['contact_id']."' ");
			while ($this->db->move_next()) {
				$this->db->query("UPDATE recurring_invoice SET buyer_address	= '".$in['address']."',
																	buyer_zip	= '".$in['zip']."',
																	buyer_city	= '".$in['city']."',
																buyer_country_id= '".$in['country_id']."'
																WHERE contact_id= '".$in['contact_id']."' ");
			}
        }
		// Sync::end($in['address_id']);
		msg::success ( gm('Address updated'),'success');
		insert_message_log($this->pagc,'{l}Contact updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_nc,$in['contact_id']);
		$in['pagl'] = $this->pagc;
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author PM
	 **/
	function attackToAddress(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		$this->db->query("DELETE FROM customer_contact_addresses WHERE contact_id='".$in['contact_id']."' ");
		$this->db->query("INSERT INTO customer_contact_addresses SET address_id='".$in['address_id']."', contact_id='".$in['contact_id']."' ");
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	/**
	 * Add contact to customer
	 * @param  array $in
	 * @return bool
	 */
	function contact_add(&$in)
	{

		if(!$this->CanEdit($in)){
			return false;
		}
		if(!$this->contact_add_validate($in)){
			return false;
		}

		if($in['birthdate']){
			$in['birthdate'] = strtotime($in['birthdate']);
		}

		// $pos = addslashes(preg_replace('/:[0-9]+/', '', $in['position_n']));
/*		$pos = array();
		foreach ($in['position'] as $key => $value) {
			$pos[] = addslashes($this->db->field("SELECT name FROM customer_contact_job_title WHERE id='".$value."' "));
		}
		if(!empty($pos)){
			$pos = implode(',', $pos);
		}else{
			$pos = '';
		}
		$in['position'] = implode(',', $in['position']);*/

        // $query_sync=array();
		// $pos = addslashes(preg_replace('/:[0-9]+/', '', $in['position_n']));

		$in['contact_id'] = $this->db->insert("INSERT INTO customer_contacts SET
						customer_id	=	'".$in['customer_id']."',
						firstname	=	'".$in['firstname']."',
						lastname	=	'".$in['lastname']."',
						position	=	'".$in['position']."',
						department	=	'".$in['department']."',
						email		=	'".$in['email']."',
						birthdate	=	'".$in['birthdate']."',
						phone		=	'".$in['phone']."',
						cell		=	'".$in['cell']."',
						note		=	'".$in['note']."',
						sex			=	'".$in['sex']."',
						title		=	'".$in['title_contact_id']."',
						language	=	'".$in['language']."',
						e_title		=	'".$in['e_title']."',
						fax			=	'".$in['fax']."',
						company_name=	'".$in['customer']."',
						title_name	=	'".$in['title_name']."',
						`create`	=	'".time()."',
						position_n  = 	'".$pos."',
						last_update = 	'".time()."'");

        /*$query_sync[0]="INSERT INTO customer_contacts SET
						contact_id	=	'".$in['contact_id']."',
                        customer_id	=	'".$in['customer_id']."',
						firstname	=	'".$in['firstname']."',
						lastname	=	'".$in['lastname']."',
						position	=	'".$in['position']."',
						department	=	'".$in['department']."',
						email		=	'".$in['email']."',
						birthdate	=	'".$in['birthdate']."',
						phone		=	'".$in['phone']."',
						cell		=	'".$in['cell']."',
						note		=	'".$in['note']."',
						sex			=	'".$in['sex']."',
						title		=	'".$in['title']."',
						language	=	'".$in['language']."',
						company_name=	'".$in['customer']."',
						title_name	=	'".$in['title_name']."',
						`create`	=	'".time()."',
						position_n  = 	'".$pos."',
						last_update = 	'".time()."'";*/

		// Sync::start(ark::$model.'-'.ark::$method);

		// if($in['s_email']){
			$this->db->query("UPDATE customer_contacts SET s_email='1' WHERE contact_id='".$in['contact_id']."' ");
		// }

        // Sync::end($in['contact_id'],$query_sync);

		// $this->updateContactAccounts($in);

		set_first_letter('customer_contacts',$in['lastname'],$this->field_nc,$in['contact_id']);
		//save extra fields values
		$this->update_custom_contact_field($in);

		
		insert_message_log($this->pagc,'{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_nc,$in['contact_id']);
		$in['pagl'] = $this->pagc;

		$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
										 								AND name 		= 'company-contacts_show_info'	");
		if(!$show_info->move_next()) {
			$this->db_users->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
																name    = 'company-contacts_show_info',
																value   = '1' ");
		} else {
			$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
																	  AND name 		= 'company-contacts_show_info' ");
		}
		$this->add_contact_address($in);
		/*if(!$in['add_contact_from_company']){
			// header("Location: index.php?do=company-xcustomer_contact&contact_id=".$in['contact_id']."");
			ark::$controller = 'xcustomer_contact';
		}else{
			//$in['view']=0;
			ark::$controller = 'customer_contacts';
		}*/
		$count = $this->db->field("SELECT COUNT(contact_id) FROM customer_contacts ");
		if($count == 1){
			doManageLog('Created the first contact.','',array( array("property"=>'first_module_usage',"value"=>'Contact') ));
		}

		$this->addContactToCustomer($in);

		$this->setAccountAsPrimary($in);

		msg::success ( gm('Contact Added'),'success');
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function customise(&$in)
	{
		$in['debug'] = true;
		$is_extra_field = substr($in['field'] ,0 ,12);

		if($is_extra_field =='extra_field_'){

			$extra_field_id = str_replace($is_extra_field , '', $in['field']);
			$this->db->query("UPDATE customer_fields SET value = '".$in['value']."' WHERE field_id = '".$extra_field_id."' ");

		}else{

			$this->db->query("SELECT * FROM customise_field WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			if($this->db->next()){
				#update
				$this->db->query("UPDATE customise_field SET value='".$in['value']."' WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			}else{
				#insert
				$this->db->query("INSERT INTO customise_field SET value='".$in['value']."', controller='".$in['controller']."', field='".$in['field']."' ");
			}

		}

		msg::success (gm("Setting updated"),'success');
		return true;
	}
	function default_customise(&$in)
	{
		$in['debug'] = true;
		$is_extra_field = substr($in['field'] ,0 ,12);

		if($is_extra_field =='extra_field_'){

			$extra_field_id = str_replace($is_extra_field , '', $in['field']);
			$this->db->query("UPDATE customer_fields SET default_value = '".$in['value']."' WHERE field_id = '".$extra_field_id."' ");

		}else{
			$this->db->query("SELECT * FROM customise_field WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			if($this->db->next()){
				#update
				$this->db->query("UPDATE customise_field SET default_value='".$in['value']."' WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			}
		}
		msg::success (gm("Setting updated"),'success');
		return true;
	}
	function creation_customise(&$in)
	{
		$in['debug'] = true;
		$is_extra_field = substr($in['field'] ,0 ,12);

		if($is_extra_field =='extra_field_'){

			$extra_field_id = str_replace($is_extra_field , '', $in['field']);
			$this->db->query("UPDATE customer_fields SET creation_value = '".$in['value']."' WHERE field_id = '".$extra_field_id."' ");

		}else{
			$this->db->query("SELECT * FROM customise_field WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			if($this->db->next()){
				#update
				$this->db->query("UPDATE customise_field SET creation_value='".$in['value']."' WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			}
		}
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	function customise_contact(&$in)
	{
		$in['debug'] = true;
		$is_extra_field = substr($in['field'] ,0 ,12);
		if($is_extra_field =='extra_field_'){
			$extra_field_id = str_replace($is_extra_field , '', $in['field']);
			$this->db->query("UPDATE contact_fields SET value = '".$in['value']."' WHERE field_id = '".$extra_field_id."' ");
		}else{
			$this->db->query("SELECT * FROM customise_field WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			//if($this->db->next()){
				#update
				$this->db->query("UPDATE customise_field SET value='".$in['value']."' WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			//}else{
				#insert
			//	$this->db->query("INSERT INTO customise_field SET value='".$in['value']."', controller='".$in['controller']."', field='".$in['field']."' ");
			//}	
		}
		msg::success (gm("Setting updated"),'success');
		return true;
	}
	function default_customise_contact(&$in)
	{
		$in['debug'] = true;
		$is_extra_field = substr($in['field'] ,0 ,12);
		if($is_extra_field =='extra_field_'){
			$extra_field_id = str_replace($is_extra_field , '', $in['field']);
			$this->db->query("UPDATE contact_fields SET default_value = '".$in['value']."' WHERE field_id = '".$extra_field_id."' ");
		}else{
			$this->db->query("SELECT * FROM customise_field WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			if($this->db->next()){
				#update
				$this->db->query("UPDATE customise_field SET default_value = '".$in['value']."' WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			}
		}
		
		msg::success (gm("Setting updated"),'success');
		return true;
	}
	function creation_customise_contact(&$in)
	{
		$in['debug'] = true;
		$is_extra_field = substr($in['field'] ,0 ,12);
		if($is_extra_field =='extra_field_'){
			$extra_field_id = str_replace($is_extra_field , '', $in['field']);
			$this->db->query("UPDATE contact_fields SET creation_value = '".$in['value']."' WHERE field_id = '".$extra_field_id."' ");
		}else{
			$this->db->query("SELECT * FROM customise_field WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			if($this->db->next()){
				#update
				$this->db->query("UPDATE customise_field SET creation_value = '".$in['value']."' WHERE controller='".$in['controller']."' AND field='".$in['field']."' ");
			}
		}	
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	/**
	 * update custom field
	 *
	 * @return bool
	 * @author Mp
	 **/
	function update_field(&$in)
	{
		if($in['is_dd']=='1'){
			$bulk_type = '2';
			$bulk_table_name = 'customer_extrafield_dd';
			$bulk_field_name = 'name';
			$bulk_field_id_name = 'extra_field_id';
			$bulk_dd = 'build_extra_field_dd';
		}else{
			$bulk_type = '1';
			$bulk_table_name = 'customer_field';
			$bulk_field_name = 'value';
			$bulk_field_id_name = 'field_id';
			$bulk_dd = '';
		}
		$this->db->query("UPDATE customer_fields SET label='".$in['value_input']."', is_dd='".$in['is_dd']."' WHERE field_id='".$in['field_id']."' ");
		$this->db->query("UPDATE import_labels SET	label_name='".$in['value_input']."',
													is_custom_dd='".$in['is_dd']."',
													bulk_type='".$bulk_type."',
													bulk_table_name='".$bulk_table_name."',
													bulk_field_name='".$bulk_field_name."',
													bulk_field_id_name='".$bulk_field_id_name."',
													bulk_dd='".$bulk_dd."'
												WHERE field_name='".$in['field_id']."' ");
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	/**
	 * delete a custom field
	 *
	 * @return bool
	 * @author Mp
	 **/
	function delete_field(&$in)
	{
		$this->db->query("DELETE FROM customer_fields WHERE field_id='".$in['field_id']."' ");
		$this->db->query("DELETE FROM import_labels WHERE field_name='".$in['field_id']."' ");
		$this->db->query("DELETE FROM customer_extrafield_dd WHERE extra_field_id='".$in['field_id']."' ");
		msg::success (gm("Setting updated"),'success');
		return true;
	}


	/**
	 * add new custom field
	 *
	 * @return bool
	 * @author Mp
	 **/
	function add_field(&$in)
	{
		$value_label = gm("Label");
		$in['field_id'] = $this->db->insert("INSERT INTO customer_fields SET label='".$value_label."', value='1' ");
		$bulk_type = '1';
		$bulk_table_name = 'customer_field';
		$bulk_field_name = 'value';
		$bulk_field_id_name = 'field_id';
		$bulk_dd = '';

		$this->db->insert("INSERT INTO import_labels SET 	label_name='Label',
															field_name='".$in['field_id']."',
															tabel_name='customer_field',
															type='customer',
															custom_field='1',
															bulk_type='".$bulk_type."',
															bulk_table_name='".$bulk_table_name."',
															bulk_field_name='".$bulk_field_name."',
															bulk_field_id_name='".$bulk_field_id_name."',
															bulk_dd='".$bulk_dd."'
															");
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	function update_contact_field(&$in)
	{
		if($in['is_dd']=='1'){
			$bulk_type = '2';
			$bulk_table_name = 'contact_extrafield_dd';
			$bulk_field_name = 'name';
			$bulk_field_id_name = 'extra_field_id';
			$bulk_dd = 'build_extra_field_dd';
		}else{
			$bulk_type = '1';
			$bulk_table_name = 'contact_field';
			$bulk_field_name = 'value';
			$bulk_field_id_name = 'field_id';
			$bulk_dd = '';
		}
		$this->db->query("UPDATE contact_fields SET label='".$in['value_input']."', is_dd='".$in['is_dd']."' WHERE field_id='".$in['field_id']."' ");
		$this->db->query("UPDATE import_labels SET	label_name='".$in['value_input']."',
													is_custom_dd='".$in['is_dd']."',
													bulk_type='".$bulk_type."',
													bulk_table_name='".$bulk_table_name."',
													bulk_field_name='".$bulk_field_name."',
													bulk_field_id_name='".$bulk_field_id_name."',
													bulk_dd='".$bulk_dd."'
												WHERE field_name='".$in['field_id']."' ");
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	function delete_contact_field(&$in)
	{
		$this->db->query("DELETE FROM contact_fields WHERE field_id='".$in['field_id']."' ");
		$this->db->query("DELETE FROM import_labels WHERE field_name='".$in['field_id']."' ");
		$this->db->query("DELETE FROM contact_extrafield_dd WHERE extra_field_id='".$in['field_id']."' ");
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	function add_contact_field(&$in)
	{
		$value_label = gm("Label");
		$in['field_id'] = $this->db->insert("INSERT INTO contact_fields SET label='".$value_label."', value='1' ");
		$bulk_type = '1';
		$bulk_table_name = 'contact_field';
		$bulk_field_name = 'value';
		$bulk_field_id_name = 'field_id';
		$bulk_dd = '';

		$this->db->insert("INSERT INTO import_labels SET 	label_name='Label contact',
															field_name='".$in['field_id']."',
															tabel_name='contact_field',
															type='customer_contacts',
															custom_field='1',
															bulk_type='".$bulk_type."',
															bulk_table_name='".$bulk_table_name."',
															bulk_field_name='".$bulk_field_name."',
															bulk_field_id_name='".$bulk_field_id_name."',
															bulk_dd='".$bulk_dd."'
															");
		msg::success (gm("Setting updated"),'success');
		return true;
	}

	/**
	 * various function
	 *
	 * @return bool
	 * @author Mp
	 **/
	function custRef(&$in)
	{
	
		if(!defined('USE_COMPANY_NUMBER')){
			$this->db->query("INSERT INTO settings SET value='".$in['use_company_number']."', constant_name='USE_COMPANY_NUMBER' ");
		}else{
			$this->db->query("UPDATE settings SET value='".$in['use_company_number']."' WHERE constant_name='USE_COMPANY_NUMBER'");
		}
		if($in['inc_part_c']){
			if(!defined('ACCOUNT_COMPANY_DIGIT_NR')){
				$this->db->query("INSERT INTO settings SET value='".$in['inc_part_c']."', constant_name='ACCOUNT_COMPANY_DIGIT_NR', module='billing' ");
			}else{
				$this->db->query("UPDATE settings SET value='".$in['inc_part_c']."' WHERE constant_name='ACCOUNT_COMPANY_DIGIT_NR'");
			}
		}
		if($in['first_reference_c']){
			if(!defined('FIRST_REFERENCE_C')){
				$this->db->query("INSERT INTO settings SET value='".$in['first_reference_c']."', constant_name='FIRST_REFERENCE_C', module='billing' ");
			}else{
				$this->db->query("UPDATE settings SET value='".$in['first_reference_c']."' WHERE constant_name='FIRST_REFERENCE_C'");
			}
		}
		
		msg::success ( gm("Changes saved"),'success');

		return true;
	}

	function update_exist_customer(&$in)
	{
		$customers = $this->db->query("SELECT customer_id, our_reference FROM customers WHERE our_reference='' ORDER BY `customers`.`creation_date`  ASC  ");
		if($in['first_reference_c']!=''){
/*			for ($k=$in['k'];$k<$in['totalcustomers'];$k++)
			{*/
				while ($customers->next()) {
				 	$in['first_reference']= generate_customer_number(DATABASE_NAME,$in['first_reference_c']);
					$this->db->query("UPDATE customers SET our_reference ='".$in['first_reference']."' WHERE customer_id='".$customers->f('customer_id')."' ");
				}
			/*}*/
		}
	}

	function paymentTerm(&$in)
	{

		// Sync::start(ark::$model.'-'.ark::$method);

		$v = new validation($in);
		$v -> f('payment_term', 'Payment term', 'integer');

		if(!$v->run())
		{
			return false;
		}

		$this->db->query("UPDATE settings SET value='".$in['payment_term']."' WHERE constant_name='PAYMENT_TERM'");
		$this->db->query("UPDATE settings SET value='".$in['choose_payment_term_type']."' WHERE constant_name = 'PAYMENT_TERM_TYPE' ");

		// Sync::end();

		msg::$success = gm("Payment term updated");
		return true;
	}

	//ADDRESSES
	/****************************************************************
	* function address_add(&$in)                                          *
	****************************************************************/
	function address_add(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		$in['failure']=false;
		if(!$this->address_add_validate($in)){
			$in['failure']=true;
			return false;
		}

		$country_n = get_country_name($in['country_id']);

 		$address_id = $this->db->insert("INSERT INTO customer_addresses SET
																			customer_id			=	'".$in['customer_id']."',
																			country_id			=	'".$in['country_id']."',
																			state_id			=	'".$in['state_id']."',
																			city				=	'".$in['city']."',
																			zip					=	'".$in['zip']."',
																			address				=	'".$in['address']."',
																			billing				=	'".$in['billing']."',
																			is_primary			=	'".$in['primary']."',
																			delivery			=	'".$in['delivery']."'");
		if($in['billing']){
			$this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
		}

		/*if($in['delivery']){
			$this->db->query("UPDATE customer_addresses SET delivery=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
		}*/
		if($in['primary']){
			if($in['address'])
			{
				if(!$in['zip'] && $in['city'])
				{
					$address = $in['address'].', '.$in['city'].', '.$country_n;
				}elseif(!$in['city'] && $in['zip'])
				{
					$address = $in['address'].', '.$in['zip'].', '.$country_n;
				}elseif(!$in['zip'] && !$in['city'])
				{
					$address = $in['address'].', '.$country_n;
				}else
				{
					$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
				}
				$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
				$output= json_decode($geocode);
				$lat = $output->results[0]->geometry->location->lat;
				$long = $output->results[0]->geometry->location->lng;
			}
			$this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
			$this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id ='".$in['customer_id']."' AND address_id!='".$address_id."'");
			$this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
		}

		msg::success (gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer address added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['pagl'] = $this->pag;
		$in['address_id'] = $address_id;
		return true;
	}

	/****************************************************************
	* function address_add_validate(&$in)                                          *
	****************************************************************/
	function address_add_validate(&$in)
	{
		$v = new validation($in);
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");
		$v->field('country_id', 'Country', 'required:exist[country.country_id]');

		return $v->run();
	}

	function check_valid_vat_number(&$in){
		/*$eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');
		if(in_array(get_country_code($in['account_billing_country_id']), $eu_countries) && $in['account_vat_number']){
			$search   = array(" ", ".");
			$vat = str_replace($search, "", $in['account_vat_number']);
			$_GET['a'] = substr($vat,0,2);
			$_GET['b'] = substr($vat,2);
			$_GET['c'] = '1';
			$dd = include('../valid_vat.php');
		}else{
			msg::$error = gm('Country is not in EU');
			return false;
		}
		if(isset($response) && $response == 'invalid'){
			msg::$error = gm('Incorrect VAT number or format');
			return false;
		}else if(isset($response) && $response == 'error'){
			msg::$error = gm('Sorry, but we could not verify your VAT. Please make sure it works with').' <a href="http://ec.europa.eu/taxation_customs/vies/" >http://ec.europa.eu/</a> '.gm('and try again');
			return false;
		}else if(isset($response) && $response == 'valid'){
			$in['result_data']=json_encode($result);
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['account_vat_number']."', btw_nr='".$in['account_vat_number']."' where customer_id = '".$in['customer_id']."' ");
			msg::$success = gm('VAT Number is valid');
		}else{
			msg::$error = gm('Sorry, but we could not verify your VAT. Please make sure it works with').' <a href="http://ec.europa.eu/taxation_customs/vies/" >http://ec.europa.eu/</a> '.gm('and try again');
			return false;
		}*/
		$in['value']=$in['account_vat_number'];
		$check_vies_number=$this->check_vies_vat_number($in);
		console::log($check_vies_number);
		if($check_vies_number != false){
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['account_vat_number']."', btw_nr='".$in['account_vat_number']."', vat_form='0' where customer_id = '".$in['customer_id']."' ");
			msg::$success = gm('VAT Number is valid');
			$in['success'] = gm('VAT Number is valid');
		}else if($check_vies_number == false && $in['trends_ok']){
			$this->db->query("UPDATE customers SET check_vat_number = '".$in['account_vat_number']."', btw_nr='".$in['account_vat_number']."', vat_form='1' where customer_id = '".$in['customer_id']."' ");
			msg::$success = gm('VAT Number is valid');
			$in['success'] = gm('VAT Number is valid');
		}else{
			msg::$error = gm('Sorry, but we could not verify your VAT. Please make sure it works with').' <a href="http://ec.europa.eu/taxation_customs/vies/" >http://ec.europa.eu/</a> '.gm('and try again');
			$in['error'] = gm('Sorry, but we could not verify your VAT. Please make sure it works with').' <a href="http://ec.europa.eu/taxation_customs/vies/" >http://ec.europa.eu/</a> '.gm('and try again');
		}
		json_out($in);
		return true;
	}
	function backup_data(&$in)
	{
		set_time_limit(0);
	    ini_set('memory_limit', '-1');   
	    $folder=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm';
	    $folder_exist=file_get_contents($folder);

	    if($folder_exist==false){
	    	@mkdir($folder,0755,true);
	    }
	    $array_exclude = array('customer_import');
		$file1=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup1.xml';
		$file2=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup2.xml';
		$file3=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup3.xml';
		$file_fill1 = file_get_contents($file1);
		$file_fill2 = file_get_contents($file2);
		$file_fill3 = file_get_contents($file3);
		if($file_fill1==true&&$file_fill2==true&&$file_fill3==true){
			if($in['file_id']!=0){
					ini_set('memory_limit', '-1');
					$this->db->query("show tables");
					$table_names=array();

					while ($this->db->move_next()){
						if(strpos($this->db->f("Tables_in_".DATABASE_NAME), 'customer')!==FALSE && strpos($this->db->f("Tables_in_".DATABASE_NAME), 'backup')===FALSE)
						{
							if(!in_array($this->db->f("Tables_in_".DATABASE_NAME), $array_exclude)){
								$table_names[]=$this->db->f("Tables_in_".DATABASE_NAME);
							}
						}
						
					}
					$xml = new SimpleXMLElement('<?xml version="1.0" encoding="ISO-8859-1"?><RECORDS/>');	

				    for($i=0;$i<count($table_names);$i++){
				    	// $cus = $xml->addChild('customers-data');
			            	 $tbl=$xml->addChild($table_names[$i],''); 
			                 //$this->db->query("SHOW COLUMNS FROM ".$table_names[$i]."");
			                $cols = $this->db2->query("SHOW COLUMNS FROM ".$table_names[$i]."")->getAll();
			                $this->db->query("SELECT * FROM  ".$table_names[$i]."");
			                while($this->db->move_next()){
			                 	    $chese=$tbl->addChild('line',''); 
			                 	    foreach ($cols as $key => $value) {
			                 	   		$chese->addChild($value['Field'], htmlspecialchars($this->db->f($value['Field']))) ;
			                 	   	}
			                 	    /*$this->db2->query("SHOW COLUMNS FROM ".$table_names[$i]."");
						            while ($this->db2->move_next()) {
			                 	  	  $chese->addChild($this->db2->f('Field'), $this->db->f($this->db2->f('Field'))) ;
			 	               	  	}*/
			                }
			    	}
			    	$time=time();
		    		$xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup'.$in['file_id'].'.xml');
		    		$this->db->query("UPDATE backup_data SET backup_time='".$time."' WHERE backup_id='".$in['file_id']."'");
		    	msg::success ( gm('Data successfully saved'),'success');
		    	$in['step']=2;
		    	return true;
			}else{
				msg::error ( gm('Select a file'),'error');
				json_out($in);
	    		return false;
			}
		}else{
			if($in['file_id']!=0){
				ini_set('memory_limit', '-1');
				$this->db->query("show tables");
				$table_names=array();

				while ($this->db->move_next()){
					if(strpos($this->db->f("Tables_in_".DATABASE_NAME), 'customer')!==FALSE && strpos($this->db->f("Tables_in_".DATABASE_NAME), 'backup')===FALSE)
					{
						if(!in_array($this->db->f("Tables_in_".DATABASE_NAME), $array_exclude)){
							$table_names[]=$this->db->f("Tables_in_".DATABASE_NAME);
						}
					}
					
				}
				$xml = new SimpleXMLElement('<?xml version="1.0" encoding="ISO-8859-1"?><RECORDS/>');	
			    for($i=0;$i<count($table_names);$i++){
			    	// $cus = $xml->addChild('customers-data');
		            	 $tbl=$xml->addChild($table_names[$i],''); 
		                 //$this->db->query("SHOW COLUMNS FROM ".$table_names[$i]."");
		                $cols = $this->db2->query("SHOW COLUMNS FROM ".$table_names[$i]."")->getAll();
		                $this->db->query("SELECT * FROM  ".$table_names[$i]."");
		                while($this->db->move_next()){
		                 	    $chese=$tbl->addChild('line',''); 
		                 	   	foreach ($cols as $key => $value) {
		                 	   		$chese->addChild($value['Field'], htmlspecialchars($this->db->f($value['Field']))) ;
		                 	   	}
					            /*while ($this->db2->move_next()) {
		                 	  	  $chese->addChild($this->db2->f('Field'), $this->db->f($this->db2->f('Field'))) ;
		 	               	  	}*/
		                }
		    	}
		    	$time=time();
		    	if($file_fill1==false){
		    		$xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup1.xml');
		    		$this->db->query("UPDATE backup_data SET backup_time='".$time."' WHERE backup_id='1'");
		    	}elseif($file_fill1==true&&$file_fill2==false){
		    		$xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup2.xml');
		    		$this->db->query("UPDATE backup_data SET backup_time='".$time."' WHERE backup_id='2'");
		    	}elseif ($file_fill2==true&&$file_fill3==false) {
		    		$xml->asXML(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup3.xml');
		    		$this->db->query("UPDATE backup_data SET backup_time='".$time."' WHERE backup_id='3'");
		    	}
		    }else{
		    	msg::error ( gm('Select a file'),'error');
		    	json_out($in);
	    		return false;
		    }
			
		}
    	/*msg::success ( gm('Data successfully saved'),'success');*/
    	$in['step']=2;
    	return true;
	}
	function uploadify(&$in)
	{
		$response=array();
		// Define a destination
		$targetFolder = 'upload/'.DATABASE_NAME.'/crm/'; // Relative to the root
		@mkdir($targetFolder,0755,true);
		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$targetPath = $targetFolder;
			$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];

			// Validate the file type
			$fileTypes = array('xml','xls','xlsx'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			if (in_array($fileParts['extension'],$fileTypes)) {
				move_uploaded_file($tempFile,$targetFile);
				// $response['success'] = $_FILES['Filedata']['name'];
				msg::success($_FILES['Filedata']['name'],'success');
				$in['xls_name'] = $_FILES['Filedata']['name'];

				json_out($in);
				// $this->import_company($in);
				/*echo json_encode($response);*/
			} else {
				// $response['error'] = 'Invalid file type.';
				msg::error( 'Invalid file type.','error');
				// echo json_encode($response);
			}

		}
	}

	/**
	 * Import customers from Excel files
	 * @param  array $in
	 * @return bool
	 */
	function import_company(&$in){

		ini_set('memory_limit', '2048M');
		ini_set('max_execution_time', 0);
		$in['timestamp'] = time();
		// $in['debug'] = true;
		global $_FILES;
		global $database_config;
		$database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$db_users = new sqldb($database_users);
		require_once 'libraries/PHPExcel.php';		
		$inputFileName = INSTALLPATH.UPLOAD_PATH.DATABASE_NAME."/crm/".$in['xls_name'];
		
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($inputFileName);
		//$objPHPExcel->setActiveSheetIndex(0);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		$highestRow = $objWorksheet->getHighestRow(); // e.g. 10
		$highestColumn = $objWorksheet->getHighestColumn();
		$i=0;
		$x=0;
		$z=1;
		$sdsd = 0;
		$nr_rows=0;
		$nr_cols=0;
		$blabla = 0;
		$headers = array();
		$values = array();
		$final_arr = array();
		// $arrayy = array();
		// print_r($objWorksheet->getRowIterator());
		foreach ($objWorksheet->getRowIterator() as $row) {
			if($i>$highestRow)
			{
				break;
			}
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false);
			/*if($i!=0)
			{
				$nr_rows++;
			}*/
			$k=0;
			$row_array = array();
			foreach ($cellIterator as $cell) {
				$val = $cell->getValue();
				array_push($row_array, $val);
				if($i==0)
				{	if($val)
					{
						$val = str_replace("'", " ", $val);
						array_push($headers, $val);
						$nr_cols++;
					}else{
						break;
					}
				}else
				{
					if($k!=$nr_cols)
					{
						$values[$k] = $cell->getValue();
						$k++;
					}else
					{
						break;
					}
				}
			}
			if($i!=0)
			{
				if($i < 5){
					for ($j=0;$j<$k;$j++)
					{
						$final_arr[$headers[$j]][$x] = $values[$j];
					}
				}
				$x++;
				$t = 0;
				$nr_rows++;
			}else{
				$t = 1;
			}
			// array_push($arrayy, $row_array);
			$i++;
			// console::memory(addslashes(serialize($row_array)),'line');
			$this->db->query("INSERT INTO customer_import SET data='".addslashes(serialize($row_array))."', timestamp='".$in['timestamp']."', line='".$z."',type='".$t."' ");
			$j++;
			$z++;
		}

		$in['file_content']=$final_arr;
		$dark = str_replace(array("\r"), "", $in['file_content']);
		foreach ($dark as $header => $values) {
				$i=0;
				$in['match_field_id'] = '0';
				$in['match_field'] = '0';
				$header = trim($header);
				$header_select = str_replace(array("\r\n","\n","\r"," "), '_', $header);
				/*$header_select1 = str_replace(array("\r\n","\n","\r"," "), '.', $header);*/
				$in['match_field_company'] = build_match_field_dd_company($in[$header_select]);
				$in['match_field_contacts'] = build_match_field_dd_contacts($in[$header_select]);
				
				$in['match_field_individual'] = build_match_field_dd_individual($in[$header_select]);
				$in['names_id'][]=$header_select;
					if($header_select == 'First_Name'){
						$header_select = 'First_Name_Contact';
					}elseif ($header_select == 'Last_Name') {
						$header_select = 'Last_Name_Contact';
					}
				$in['names_id1'][]=$header_select;
		}
		$final_arr = null;
		$in['total_rows']=$nr_rows;
		$in['step']=3;
		$in['indi']=0;
		$in['indicontact']=0;
		$in['hideconts']=1;
		$in['hidecont']=1;
		$in['namecomp']=gm('Company');
		$in['changefield']=0;
		$in['class3']='text-primary';
		@unlink(INSTALLPATH.UPLOAD_PATH.DATABASE_NAME."/crm/".$in['xls_name']);
		//header("Location: index.php?do=company-save_imports");
		// var_dump($in['file_content']); exit();
		
		json_out($in);
		return true;
	}
	function save_imports(&$in)
	{

		$file_content = array();
		$tmp_header = array();
		$q = $this->db->query("SELECT * FROM customer_import WHERE `timestamp`='".$in['timestamp']."' ORDER BY line ASC ");
		while ($q->next()) {
			$line = unserialize($q->f('data'));
			foreach ($line as $key => $value) {
				if($q->f('type') == 1 ){
					$file_content[$value] = array();
					$tmp_header[$key] = $value;
					if( $value == 'First Name'){
						$file_content['First Name Contact'] = array();
					}
					if( $value == 'Last Name'){
						$file_content['Last Name Contact'] = array();
					}
				}else{
					array_push($file_content[$tmp_header[$key]],$value);
					if($tmp_header[$key] == 'First Name'){
						array_push($file_content['First Name Contact'],$value);
					}
					if($tmp_header[$key] == 'Last Name'){
						array_push($file_content['Last Name Contact'],$value);
					}
					
				}
			}
		}
		// var_dump($file_content);
		$insert_values = "";
		$values_info = array();

		$j=0;
		$nr_headers=0;
		$headers_ids= array();
		$inserted_cust = 0;
		$inserted_cust_arr = array();
		$update_cust = 0;
		$update_cust_arr = array();
		$inserted_contacts = 0;
		$inserted_contacts_arr = array();
		$update_cont = 0;
		$update_cont_arr = array();
		$filename = $in['xls_name'];
							
					
		for ($k=$in['k'];$k<$in['total_rows'];$k++)
		{

			$no_action_customer = true;
			$no_action_contact = true;
			$no_action_contact_addr = false;// set true to import the address only if there is a country id
			$no_action_cust_addr = false;
			$has_first_name = false;
			$has_last_name = false;
			$first_name="";
			$last_name="";
			$begin_query = "";
			$end_query = "";
			$cmon_id ="";
			$begin_contact_query = "INSERT INTO ";
			$insert_cont = true;
			unset($values_info);

		
			foreach ($file_content as $headers => $value) {
				$headers = trim($headers);
				$headers = str_replace(array("\r\n","\n","\r"," "), '_', $headers);
				/*$headers = explode(' ', $headers);
				$headers = implode('_', $headers);*/
				$headers = addslashes($headers);

				$value[$k] = addslashes($value[$k]);

				if($in[$headers])
				{

					if($k==0)
					{	
						if(in_array($in[$headers], $headers_ids))
						{
							
							msg::error ( gm("You cannot select the same field twice"),'error');
							$in['error'] = 'error';
							json_out($in);
							//ark::$controller = 'save_imports';
							return false;
						}else
						{
							array_push($headers_ids, $in[$headers]);
						}
					}
					$nr_headers++;
					$labels_info = $this->db_import->query("SELECT field_name, tabel_name, is_custom_dd, type, custom_field FROM import_labels WHERE import_label_id='".$in[$headers]."'");
					$labels_info->next();
					if($labels_info->f('type')=='customer')
					{

						$tabel_name = 'customers';
						
					}else
					{
						$tabel_name = 'customer_contacts';

					}
					if($labels_info->f('is_custom_dd')==1 && $labels_info->f('custom_field')==0)
					{

						if(trim($value[$k]))
						{
							if($labels_info->f('field_name')=='internal_language'){

								$lang_names = $this->db->query("SELECT language, lang_id FROM ".$labels_info->f('tabel_name')." WHERE active = '1'");
								while($lang_names->next()){
									if(gm($lang_names->f('language'))==$value[$k]){
										$internal_language_id = $lang_names->f('lang_id');
									}
								}
								if(!$internal_language_id){
									$internal_language_id = 0;
								}
								$values_info[$j]['field_name'] = $labels_info->f('field_name');
								$values_info[$j]['tabel_name'] = $tabel_name;
								$values_info[$j]['value'] = $internal_language_id;
							}elseif($labels_info->f('field_name')=='customer_id'){

									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $value[$k];
						
							}elseif($labels_info->f('field_name')=='cat_id'){

								$this->db->query("SELECT * FROM ".$labels_info->f('tabel_name')." WHERE name='".$value[$k]."'");
								if($this->db->move_next())
								{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $this->db->f('category_id');
								}else
								{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = '0';
								}
							}elseif($labels_info->f('field_name')=='invoice_email'){

									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $value[$k];
							
							}elseif($labels_info->f('field_name')=='currency_id'){

								$this->db->query("SELECT * FROM ".$labels_info->f('tabel_name')." WHERE code='".$value[$k]."'");
								if($this->db->move_next())
								{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $this->db->f('id');
								}else
								{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = '1';
								}
							}elseif($labels_info->f('field_name')=='vat_id')
							{
								$this->db->query("SELECT * FROM ".$labels_info->f('tabel_name')." WHERE value='".return_value($value[$k])."'");
								if($this->db->move_next())
								{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $this->db->f('vat_id');
								}else
								{
									$vat_id = $this->db2->insert("INSERT INTO ".$labels_info->f('tabel_name')." SET value='".return_value($value[$k])."'");
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $vat_id;
								}
							}else
							{
								$this->db->query("SELECT * FROM ".$labels_info->f('tabel_name')." WHERE name='".$value[$k]."'");
								if($this->db->move_next())
								{
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $this->db->f('id');
									if($labels_info->f('field_name')=='position')
									{
										$j++;
										$values_info[$j]['field_name'] = 'position_n';
										$values_info[$j]['tabel_name'] = $tabel_name;
										$values_info[$j]['value'] = addslashes($this->db->f('name'));
									}
								}else
								{
									$sort_order = $this->db2->field("SELECT MAX(sort_order) FROM ".$labels_info->f('tabel_name'));
									$sort_order = $sort_order+1;
									$val_id = $this->db2->insert("INSERT INTO ".$labels_info->f('tabel_name')." SET name='".$value[$k]."', sort_order='".$sort_order."'");
									$values_info[$j]['field_name'] = $labels_info->f('field_name');
									$values_info[$j]['tabel_name'] = $tabel_name;
									$values_info[$j]['value'] = $val_id;
									if($labels_info->f('field_name')=='position')
									{
										$j++;
										$values_info[$j]['field_name'] = 'position_n';
										$values_info[$j]['tabel_name'] = $tabel_name;
										$values_info[$j]['value'] = $value[$k];
									}
								}
							}
						}
					}elseif ($labels_info->f('custom_field')==1 && $labels_info->f('is_custom_dd')==1) {
						if(trim($value[$k]))
						{
							$this->db->query("SELECT * FROM customer_extrafield_dd WHERE name='".$value[$k]."' AND extra_field_id='".$labels_info->f('field_name')."'");
							if($this->db->move_next())
							{
								$values_info[$j]['field_name'] = 'value';
								$values_info[$j]['tabel_name'] = 'customer_field';
								$values_info[$j]['field_id'] = $labels_info->f('field_name');
								$values_info[$j]['value'] = $this->db->f('id');
							}else
							{
								$sort_order = $this->db2->field("SELECT MAX(sort_order) FROM customer_extrafield_dd WHERE extra_field_id='".$labels_info->f('field_name')."'");
								$sort_order = $sort_order+1;
								$val_id = $this->db2->insert("INSERT INTO customer_extrafield_dd SET name='".$value[$k]."', sort_order='".$sort_order."', extra_field_id='".$labels_info->f('field_name')."'");
								$values_info[$j]['field_name'] = 'value';
								$values_info[$j]['tabel_name'] = 'customer_field';
								$values_info[$j]['field_id'] = $labels_info->f('field_name');
								$values_info[$j]['value'] = $val_id;
							}
						}
					}
					elseif($labels_info->f('custom_field')==1 && $labels_info->f('is_custom_dd')==0)
					{
						if(trim($value[$k]))
						{
							$values_info[$j]['field_name'] = 'value';
							$values_info[$j]['tabel_name'] = 'customer_field';
							$values_info[$j]['field_id'] = $labels_info->f('field_name');
							$values_info[$j]['value'] = $value[$k];
						}
					}else
					{
						if($labels_info->f('field_name')=='fixed_discount')
						{
							if(trim($value[$k]))
							{
								$value[$k] = format_number_import($value[$k]);
								$values_info[$j]['field_name'] = 'apply_fix_disc';
								$values_info[$j]['tabel_name'] = 'customers';
								$values_info[$j]['value'] = 1;
								$j++;
							}
						}
						if($labels_info->f('field_name')=='c_type_name')
						{
							if(trim($value[$k]))
							{
								$all_vals = explode(',',$value[$k]);
								foreach ($all_vals as $c_type) {
									if(trim($c_type)!='')
									{
										$this->db->query("SELECT * FROM customer_type WHERE name='".trim($c_type)."'");
										if($this->db->move_next())
										{
											//$values_info[$j]['field_name'] = 'c_type';
											//$values_info[$j]['tabel_name'] = 'customers';
											//$values_info[$j]['value'] = $this->db->f('id');
											$val_id = $this->db->f('id');
										}else
										{
											$sort_order = $this->db2->field("SELECT MAX(sort_order) FROM customer_type");
											$sort_order = $sort_order+1;
											$val_id = $this->db2->insert("INSERT INTO customer_type SET name='".trim($c_type)."', sort_order='".$sort_order."'");
											//$values_info[$j]['field_name'] = 'c_type';
											//$values_info[$j]['tabel_name'] = 'customers';
											//$values_info[$j]['value'] = $val_id;
										}
										$values_info[$j]['value'] .= $val_id.",";
									}
								}
								$values_info[$j]['field_name'] = 'c_type';
								$values_info[$j]['tabel_name'] = 'customers';
								$values_info[$j]['value'] = rtrim($values_info[$j]['value'],',');
								$j++;
							}
						}
						if($labels_info->f('field_name')=='sex')
						{
							if(trim($value[$k]))
							{
								if(strtolower($value[$k])=='m')
								{
									$value[$k] = '1';
								}elseif(strtolower($value[$k])=='f')
								{
									$value[$k] = '2';
								}else
								{
									$value[$k] = '0';
								}
							}
						}
						if($labels_info->f('field_name')=='acc_manager_name')
						{
							$user_ids = '';
							$acc_manags = '';
							if(trim($value[$k]))
							{
								$this->db_users->query("SELECT * FROM users WHERE database_name='".DATABASE_NAME."'");
								while ($this->db_users->move_next()) {
									$user_name = utf8_encode($this->db_users->f('first_name')).utf8_encode($this->db_users->f('last_name'));
									$user_name = strtolower($user_name);
									$acc_name = strtolower(str_replace(" ","",$value[$k]));
									$acc_name = explode(',', $acc_name);
									foreach ($acc_name as $key => $acc_name) {
										if($user_name==$acc_name){
											$user_ids .= $this->db_users->f('user_id').',';
											$acc_manags .= $this->db_users->f('first_name').' '.$this->db_users->f('last_name').', ';
										}
									}
								}
								$values_info[$j]['field_name'] = 'user_id';
								$values_info[$j]['tabel_name'] = 'customers';
								$values_info[$j]['value'] = rtrim($user_ids,',');
								$j++;
								$values_info[$j]['field_name'] = $labels_info->f('field_name');
								$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
								$values_info[$j]['value'] = rtrim($acc_manags,', ');
							}
						}else
						{
							$values_info[$j]['field_name'] = $labels_info->f('field_name');
							$values_info[$j]['tabel_name'] = $labels_info->f('tabel_name');
							$values_info[$j]['value'] = $value[$k];
						}
						if($in['indi'] == 1 && $in['indicontact'] == 1 && $in[$headers]==20){
							$j++;
							$values_info[$j]['field_name'] = 'firstname';
							$values_info[$j]['tabel_name'] = 'customers';
							$values_info[$j]['value'] = $value[$k];
						}
						if($in['indi'] == 1 && $in['indicontact'] == 1 && $in[$headers]==21){
							$j++;
							$values_info[$j]['field_name'] = 'name';
							$values_info[$j]['tabel_name'] = 'customers';
							$values_info[$j]['value'] = $value[$k];
						}
							if($labels_info->f('tabel_name')=='customers' && $labels_info->f('field_name')=='customer_id' && trim($value[$k])!=''){
									$customer_id = $this->db->field("SELECT customer_id FROM customers WHERE customer_id='".$value[$k]."'");
									if($customer_id)
									{
										$begin_query = "UPDATE ";
										$insert_cust = false;
										$end_query = " WHERE customer_id='".$customer_id."'";
									}else
									{
										$insert_cust = true;
										$begin_query = "INSERT INTO ";
									}
									$no_action_customer = false;
							}else{
								if($labels_info->f('tabel_name')=='customers' && $labels_info->f('field_name')=='name' && trim($value[$k])!='')
								{
									$customer_id = $this->db->field("SELECT customer_id FROM customers WHERE name='".$value[$k]."'");
									if($customer_id)
									{

										$begin_query = "UPDATE ";
										$insert_cust = false;
										$end_query = " WHERE customer_id='".$customer_id."'";
									}else
									{
										
										$insert_cust = true;
										$begin_query = "INSERT INTO ";
									}
									//$customer_id = 1;
									$no_action_customer = false;
								}
								if($values_info[$j]['tabel_name']=='customers' && $labels_info->f('field_name')=='firstname' && trim($value[$k])!='')
								{
									$customer_id = $this->db->field("SELECT customer_id FROM customers WHERE firstname='".$value[$k]."'");
									if($customer_id)
									{

										$begin_query = "UPDATE ";
										$insert_cust = false;
										$end_query = " WHERE customer_id='".$customer_id."'";
									}else
									{
										
										$insert_cust = true;
										$begin_query = "INSERT INTO ";
									}
									//$customer_id = 1;
									$no_action_customer = false;
								}
								if($values_info[$j]['tabel_name']=='customers' && $labels_info->f('field_name')=='lastname' && trim($value[$k])!='')
								{
									$customer_id = $this->db->field("SELECT customer_id FROM customers WHERE name='".$value[$k]."'");
									if($customer_id)
									{

										$begin_query = "UPDATE ";
										$insert_cust = false;
										$end_query = " WHERE customer_id='".$customer_id."'";
									}else
									{
										
										$insert_cust = true;
										$begin_query = "INSERT INTO ";
									}
									//$customer_id = 1;
									$no_action_customer = false;
								}
							}
						if($labels_info->f('tabel_name')=='customer_contacts'  && trim($value[$k])!='')
						{

						}
						if($labels_info->f('tabel_name')=='customer_contacts' && $labels_info->f('field_name')=='firstname' && trim($value[$k])!='')
						{
							$has_first_name = true;
							$first_name = $value[$k];
						}
						if($labels_info->f('tabel_name')=='customer_contacts' && $labels_info->f('field_name')=='lastname' && trim($value[$k])!='')
						{
							$has_last_name = true;
							$last_name = $value[$k];
						}
						if($labels_info->f('tabel_name')=='customer_contacts' && $has_first_name && $has_last_name)
						{
							$no_action_contact = false;
						}
						if($labels_info->f('tabel_name')=='customer_addresses' && $labels_info->f('field_name')=='country_id' && trim($value[$k])!='')
						{
							$country_id = $this->db->field("SELECT country_id FROM country WHERE name='".ucfirst(strtolower($value[$k]))."' OR code='".strtoupper($value[$k])."'");
							if($country_id)
							{
								$no_action_cust_addr = false;
								$values_info[$j]['value'] = $country_id;
							}
						}
						if($labels_info->f('tabel_name')=='customer_contact_address' && $labels_info->f('field_name')=='country_id' && trim($value[$k])!='')
						{
							$country_id = $this->db->field("SELECT country_id FROM country WHERE name='".ucfirst(strtolower($value[$k]))."' OR code='".strtoupper($value[$k])."'");
							if($country_id)
							{
								$no_action_contact_addr = false;
								$values_info[$j]['value'] = $country_id;
							}
						}
						
						if($in['indi'] == 1 && $labels_info->f('field_name')=='firstname' && $in[$headers.'_C'] ){
							$has_first_name = true;
							$first_name = $value[$k];
						}
						if($in['indi'] == 1 && $labels_info->f('field_name')=='name' && $in[$headers.'_C'] ){
							$has_last_name = true;
							$last_name = $value[$k];
						}
						if($in['indi'] && $has_first_name && $has_last_name){
							$no_action_contact = false;
						}

					}
				}
				$j++;
			}
			if($nr_headers==0)
			{
				msg::error ( gm("No field selected"),'error');
				//ark::$controller = 'save_imports';
				$in['step']=3;
				return false;
			}
			// var_dump($values_info);
			unset($insert_values);
			foreach ($values_info as $key => $val) {
				if($val['value']){
					if($val['tabel_name']=='customers' && $no_action_customer==false)
					{
						/*var_dump('geeee');*/
						$insert_values[$val['tabel_name']] .= $val['field_name']."='".($val['value'])."',";
					}
					if($val['tabel_name']=='customer_contacts' && $no_action_contact==false)
					{
						/*var_dump('gee123');*/
						$insert_values[$val['tabel_name']] .= $val['field_name']."='".($val['value'])."',";
					}
					if($val['tabel_name']=='customer_contact_address' && $no_action_contact_addr==false)
					{
						$insert_values[$val['tabel_name']] .= $val['field_name']."='".($val['value'])."',";
					}
					if($val['tabel_name']=='customer_addresses' && $no_action_cust_addr==false)
					{
						$insert_values[$val['tabel_name']] .= $val['field_name']."='".($val['value'])."',";
					}
					if($val['tabel_name']=='customer_field')
					{
						$insert_values[$val['tabel_name']] .= $val['field_name']."='".($val['value'])."', field_id='".($val['field_id'])."',%!%!";
					}
				}
			}
			krsort($insert_values);
			// echo "<pre>";
			// print_r($insert_values);
			// echo "</pre>";
			// exit();
			foreach ($insert_values as $db_name => $query_string) {//db_name = numele tabelei;
				if ($db_name!='customer_field')
				{
					$query_string=rtrim($query_string,',');
				}

				if ($db_name=='customers')
				{
					if($insert_cust==true)
					{
						$customer_id = $this->db->insert($begin_query.$db_name." SET ".$query_string.", s_emails='1', active='1',type='".$in['indi']."'");
						$inserted_cust_arr[$inserted_cust] = $customer_id;
						$inserted_cust++;
						$first_letter = $this->db->field("SELECT SUBSTRING( name , 1, 1 ) AS first_letter FROM customers WHERE customer_id='".$customer_id."'");
						$this->db->query("UPDATE customers SET first_letter='".addslashes($first_letter)."' WHERE customer_id='".$customer_id."'");
						$comp_payment_term = $this->db->field("SELECT payment_term FROM customers WHERE customer_id='".$customer_id."'");
						if(!$comp_payment_term){
							$payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
							$this->db->query("UPDATE customers SET payment_term ='".$payment_term."' WHERE customer_id='".$customer_id."'");
						}
						//echo $begin_query.$db_name." SET ".$query_string.", active='1'"."<br>";
					}else
					{
						$this->db->query($begin_query.$db_name." SET ".$query_string.$end_query);
						$update_cust_arr[$update_cust] = $customer_id;
						$update_cust++;
						//echo $begin_query.$db_name." SET ".$query_string.$end_query."<br>";
					}
				}
				if ($db_name=='customer_contacts')
				{
					$contact_id = $this->db->field("SELECT contact_id FROM customer_contacts WHERE firstname='".$first_name."' AND lastname='".$last_name."' AND is_primary='1'");

					if($contact_id)
					{

						$begin_contact_query = "UPDATE ";
						$insert_cont = false;
						$end_contact_query = " WHERE contact_id='".$contact_id."'";							

					}else
					{
						$insert_cont = true;
						$begin_contact_query = "INSERT INTO ";
					}


					$company_name = $this->db->field("SELECT name FROM customers WHERE customer_id='".$customer_id."'");
					if($insert_cont==true)
					{
						$contact_id = $this->db->insert($begin_contact_query.$db_name." SET ".$query_string.", is_primary='1', s_email='1', customer_id='".$customer_id."', company_name='".addslashes($company_name)."'");
						
						$inserted_contacts_arr[$inserted_contacts] = $contact_id;
						$inserted_contacts++;
						$first_letter = $this->db->field("SELECT SUBSTRING( lastname , 1, 1 ) AS first_letter FROM customer_contacts WHERE contact_id='".$contact_id."'");
						$this->db->query("UPDATE customer_contacts SET first_letter='".addslashes($first_letter)."' WHERE contact_id='".$contact_id."'");
						$all_mass = $this->db->query("SELECT * FROM customer_contacts WHERE contact_id='".$contact_id."' AND customer_id='".$customer_id."'");
						$ids_exist = $this->db->query("SELECT id FROM customer_contactsIds WHERE contact_id='".$contact_id."' AND customer_id='".$customer_id."'");
						/*$title_intro = $this->db->query("INSERT INTO customer_contact_title SET name='".."' ")*/     
						if($ids_exist){
								$contact_ids = $this->db->query("UPDATE customer_contactsIds SET
														 position='".$all_mass->f('position')."',
														 department='".$all_mass->f('department')."',
														 title='".$all_mass->f('title')."',
														 e_title='".addslashes($all_mass->f('e_title'))."',
														 email='".$all_mass->f('email')."',
														 phone='".$all_mass->f('phone')."',
														 fax='".$all_mass->f('fax')."',
														 s_email='".$all_mass->f('s_email')."' 
														 WHERE contact_id='".$contact_id."' AND customer_id='".$customer_id."' AND `primary`='1'
														 ");						
						}
						$contact_ids = $this->db->insert("INSERT INTO  customer_contactsIds SET
													 contact_id='".$contact_id."',
													 customer_id='".$customer_id."',
													 position='".$all_mass->f('position')."',
													 department='".$all_mass->f('department')."',
													 title='".$all_mass->f('title')."',
													 e_title='".addslashes($all_mass->f('e_title'))."',
													 email='".$all_mass->f('email')."',
													 phone='".$all_mass->f('phone')."',
													 fax='".$all_mass->f('fax')."',
													 `primary`='".$all_mass->f('is_primary')."',
													 s_email='".$all_mass->f('s_email')."' "); 
						//echo "INSERT INTO ".$db_name." SET ".$query_string.", s_email='1', customer_id='".$customer_id."', company_name='".$company_name."'"."<br>";
					}else
					{
						/*$cust = $this->db->field("SELECT contact_id FROM customer_contacts WHERE firstname='".$first_name."' AND lastname='".$last_name."' AND is_primary='1'");*/

						$this->db->query($begin_contact_query.$db_name." SET ".$query_string.$end_contact_query);
						$update_cont_arr[$update_cont] = $contact_id;
						$customer_primary = $this->db->query("SELECT * FROM customer_contacts WHERE contact_id='".$contact_id."' AND is_primary='1'");
						$all_mass = $this->db->query("SELECT * FROM customer_contacts WHERE contact_id='".$contact_id."' AND customer_id='".$customer_primary->f('customer_id')."'");
						// if($all_mass->f('is_primary')==1){
						$ids_exist = $this->db->query("SELECT id FROM customer_contactsIds WHERE contact_id='".$contact_id."' AND customer_id='".$customer_id."'");
						if($ids_exist){
							$contact_ids = $this->db->query("UPDATE customer_contactsIds SET
													 position='".$all_mass->f('position')."',
													 department='".$all_mass->f('department')."',
													 title='".$all_mass->f('title')."',
													 e_title='".addslashes($all_mass->f('e_title'))."',
													 email='".$all_mass->f('email')."',
													 phone='".$all_mass->f('phone')."',
													 fax='".$all_mass->f('fax')."',
													 s_email='".$all_mass->f('s_email')."' 
													 WHERE contact_id='".$contact_id."' AND customer_id='".$customer_id."' AND `primary`='1'
													 ");
						}else{
							$contact_ids = $this->db->insert("INSERT INTO  customer_contactsIds SET
													 contact_id='".$contact_id."',
													 customer_id='".$customer_id."',
													 position='".$customer_primary->f('position')."',
													 department='".$customer_primary->f('department')."',
													 title='".$customer_primary->f('title')."',
													 e_title='".addslashes($customer_primary->f('e_title'))."',
													 email='".$customer_primary->f('email')."',
													 phone='".$customer_primary->f('phone')."',
													 fax='".$customer_primary->f('fax')."',
													 s_email='".$customer_primary->f('s_email')."' ");	
						}

						/*}*/
						if($customer_primary->f('is_primary')==1){
							$contact_ids = $this->db->query("UPDATE customer_contactsIds SET
													 position='".$all_mass->f('position')."',
													 department='".$all_mass->f('department')."',
													 title='".$all_mass->f('title')."',
													 e_title='".addslashes($all_mass->f('e_title'))."',
													 email='".$all_mass->f('email')."',
													 phone='".$all_mass->f('phone')."',
													 fax='".$all_mass->f('fax')."',
													 s_email='".$all_mass->f('s_email')."' 
													 WHERE contact_id='".$contact_id."' AND customer_id='".$customer_id."' AND `primary`='1'
													 ");						
						}
 


						$update_cont++;
						//echo $begin_contact_query.$db_name." SET ".$query_string.$end_contact_query."<br>";
					}

				}
				if ($db_name=='customer_addresses')
				{
					if($customer_id)
					{
						if($insert_cust==true)
						{
							$addr_id = $this->db->insert("INSERT INTO ".$db_name." SET ".$query_string.", customer_id='".$customer_id."', delivery='1', billing='1', is_primary='1'");
						}else
						{
							/*$addr_id = $this->db->insert("INSERT INTO ".$db_name." SET ".$query_string.", customer_id='".$customer_id."'");*/
						}
						$addr_info = $this->db->query("SELECT * FROM customer_addresses WHERE address_id='".$addr_id."'");
						$country_name = $this->db->field("SELECT name FROM country WHERE country_id='".$addr_info->f('country_id')."'");
						$this->db->query("UPDATE customers SET zip_name='".addslashes($addr_info->f('zip'))."', country_name='".addslashes($country_name)."', city_name='".addslashes($addr_info->f('city'))."' WHERE customer_id='".$customer_id."'");
						//echo "INSERT INTO ".$db_name." SET ".$query_string.", customer_id='".$customer_id."', delivery='1', billing='1', is_primary='1'<br>";
					}
				}
				if ($db_name=='customer_contact_address')
				{
					if($contact_id)
					{
						$addr_id = $this->db2->field("SELECT address_id FROM ".$db_name." WHERE contact_id='".$contact_id."'");
						if(!$addr_id)
						{
							$addr_id = $this->db->insert("INSERT INTO ".$db_name." SET ".$query_string.", contact_id='".$contact_id."'");
						}else
						{
							$this->db->query("UPDATE ".$db_name." SET ".$query_string." WHERE address_id='".$addr_id."'");
						}
						//echo "INSERT INTO ".$db_name." SET ".$query_string.", contact_id='".$contact_id."'<br>";
					}
				}
				if ($db_name=='customer_field')
				{
					$query_string = explode("%!%!", $query_string);
					foreach ($query_string as $nr => $query_field) {
						if($query_field)
						{
							$pos = strpos($query_field, 'field_id=');
							$pos2 = strpos($query_field, ',');
							$pos3 = strpos($query_field, ',', $pos2+1);
							$len = $pos3-$pos;
							$row_field_id = substr($query_field, $pos, $len);
							$query_field=rtrim($query_field,',');
							if($customer_id)
							{
								$this->db->query("SELECT * FROM customer_field WHERE customer_id='".$customer_id."' AND ".$row_field_id);
								if($this->db->move_next())
								{
									$this->db2->query("UPDATE ".$db_name." SET ".$query_field." WHERE customer_id='".$customer_id."' AND ".$row_field_id);
									//echo "UPDATE ".$db_name." SET ".$query_field." WHERE customer_id='".$customer_id."' AND ".$row_field_id."<br>";
								}else
								{
									$this->db2->insert("INSERT INTO ".$db_name." SET ".$query_field.", customer_id='".$customer_id."'");
									//echo "INSERT INTO ".$db_name." SET ".$query_field.", customer_id='".$customer_id."'<br>";
								}
							}
						}
					}
				}
			}
		}

		if($in['k'] == 0){
		//header("Location: index.php?do=company-import_accounts&succ='Import completed successfully'");
		$user_info = $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$user_info->next();
		$this->db->query("INSERT INTO company_import_log SET
							`date`='".$in['timestamp']."', filename='".$filename."',
							company_added='".$inserted_cust."',
							company_updated='".$update_cust."',
							contact_added='".$inserted_contacts."',
							contact_updated='".$update_cont."',
							companies_add='".serialize($inserted_cust_arr)."',
							companies_update='".serialize($update_cust_arr)."',
							contacts_add='".serialize($inserted_contacts_arr)."',
							contacts_update='".serialize($update_cont_arr)."',
							username='".$user_info->f('first_name')." ".$user_info->f('last_name')."'");
		}else{
			$a = $this->db->query("SELECT * FROM company_import_log WHERE `date` = '".$in['timestamp']."' ");
			$ca = unserialize($a->f('companies_add'));
			foreach ($ca as $key => $value) {
				array_push($inserted_cust_arr,$value);
			}
			$cu = unserialize($a->f('companies_update'));
			foreach ($cu as $key => $value) {
				array_push($update_cust_arr,$value);
			}
			$cu = unserialize($a->f('contacts_add'));
			foreach ($cu as $key => $value) {
				array_push($inserted_contacts_arr,$value);
			}
			$cu = unserialize($a->f('contacts_update'));
			foreach ($cu as $key => $value) {
				array_push($update_cont_arr,$value);
			}
			$this->db->query("UPDATE company_import_log SET
							company_added=company_added+'".$inserted_cust."',
							company_updated=company_updated+'".$update_cust."',
							contact_added=contact_added+'".$inserted_contacts."',
							contact_updated=contact_updated+'".$update_cont."',
							companies_add='".serialize($inserted_cust_arr)."',
							companies_update='".serialize($update_cust_arr)."',
							contacts_add='".serialize($inserted_contacts_arr)."',
							contacts_update='".serialize($update_cont_arr)."'
							WHERE `date`='".$in['timestamp']."' ");
		}
		$in['done'] = '1';
		$in['class3']='text-primary';
		$in['step']=3;
		msg::success ( gm('Import completed successfully'),'success');
		return true;
	}
	function delete_import_company(&$in)
	{
		if(!$this->delete_import__company_validate($in)){
			return false;
		}

		$this->db->query("DELETE FROM company_import_log WHERE company_import_log_id='".$in['company_import_log_id']."' ");

		msg::success ( gm('Import Log deleted').'.','success');

		return true;
	}
	/****************************************************************
	* function delete_import_validate(&$in)                         *
	****************************************************************/
	function delete_import__company_validate(&$in)
	{
		$is_ok = true;
		if(!$in['company_import_log_id']){
			msg::error ( gm('Invalid ID').'.','error');
			return false;
		}
		else{
			$this->db->query("SELECT company_import_log_id FROM company_import_log WHERE company_import_log_id='".$in['company_import_log_id']."'");
			if(!$this->db->move_next()){
				msg::error ( gm('Invalid ID').'.','error');
				return false;
			}
		}

		return $is_ok;
	}
	function get_backup_tables(&$in)
	{
		if(!$in['no_backup']){
			if($in['remove']){
				$columns = $this->db->query("SHOW COLUMNS FROM ".$in['table_name'])->getAll();
				$cols_array = array();
				$cols_info = array();
				foreach ($columns as $key => $value) {
					array_push($cols_array, $value['Field']);
					array_push($cols_info,$value);
				}
				$is_table = $this->db->query("SELECT COUNT(*) as table_name FROM information_schema.tables WHERE table_schema = '".DATABASE_NAME."' AND table_name = 'backup_".$in['table_name']."'");
				$is_table->next();
				if($is_table->f('table_name'))
				{
					$backup_columns = $this->db->query("SHOW COLUMNS FROM backup_".$in['table_name'])->getAll();
					$backup_cols_array = array();
					foreach ($backup_columns as $key => $value) {
						array_push($backup_cols_array, $value['Field']);
					}
					$res = array_diff($cols_array,$backup_cols_array);
					if($res)
					{
						foreach ($res as $key => $value) {
							$query = "ALTER TABLE backup_".$in['table_name']." ADD `".$cols_info[$key]['Field']."` ".$cols_info[$key]['Type'];
							if($cols_info[$key]['Null']=='NO')
							{
								$query .= " NOT NULL";
							}else
							{
								$query .= " NULL";
							}
							if($cols_info[$key]['Default'])
							{
								$query .= " DEFAULT '".$cols_info[$key]['Default']."'";
							}
							// echo $query."<br>";
							console::log("Database: ".DATABASE_NAME."\n\tTable: ".$in['table_name']."<br>\t".$query);
							$this->db->query($query);
						}
					}
				}

				$this->db->query("TRUNCATE ".$in['table_name']);
				// $this->db->query('ALTER TABLE backup_'.$in['table_name'].'  RENAME TO '.$in['table_name'].' ');
				$this->db->query("INSERT ".$in['table_name']." SELECT * FROM backup_".$in['table_name']."");
				$this->db->query("DROP TABLE backup_".$in['table_name']."");
			}
			$this->db->query("show tables");
			$table_names=array();
			while ($this->db->move_next()){
				if(strpos($this->db->f("Tables_in_".DATABASE_NAME), 'backup') !== FALSE )
				{
					$table_names[]=str_replace('backup_','',$this->db->f("Tables_in_".DATABASE_NAME));
				}
			}
			if($table_names){
				$in['table_names'] = $table_names;
			}
			$in['table_nr'] = count($table_names);
		}else{
			$this->db->query("UPDATE settings SET value='0' WHERE constant_name='DATA_BACKEDUP' ");
			msg::success ( gm('Data successfully restored'),'success');
		}
    	return true;
	}



	function removeContactFromCustomer(&$in){
		if(!$this->removeContactFromCustomer_validate($in)){
			return false;
		}else{
			$count = $this->db->field("SELECT count(customer_id) FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' ");
			if($count<=1){
				msg::notice ( gm("There must be at least one account."),'notice');
				return false;
			}
			$primary = $this->db->field("SELECT `primary` FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' AND customer_id='".$in['customer_id']."'  ");
			if($primary == 1){
				msg::notice ( gm("You cannot delete the primary account"),'notice');
				return false;	
			}
		}
		$this->db->query("DELETE FROM customer_contactsIds WHERE customer_id='".$in['customer_id']."' AND contact_id='".$in['contact_id']."' ");
		$this->updateContactCustomerName($in);
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}

	function removeContactFromCustomer_validate(&$in){
		$v = new validation($in);
		$v->field('contact_id', 'ID', 'required:exist[customer_contacts.contact_id]', "Invalid Id");
		$v->field('customer_id', 'ID', 'required:exist[customer_contactsIds.customer_id.contact_id=\''.$in['contact_id'].'\']', "Invalid Id");
		return $v->run();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function updateContactCustomerName(&$in)
	{
		$name = $this->db->field("SELECT name FROM customers INNER JOIN customer_contactsIds ON customers.customer_id=customer_contactsIds.customer_id WHERE customer_contactsIds.contact_id='".$in['contact_id']."' AND customer_contactsIds.`primary`=1");
		$info = $this->db->query("SELECT phone, email,customer_id FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."' AND `primary`=1 ");
		
		$count = $this->db->field("SELECT count(customer_id) FROM customer_contactsIds WHERE contact_id='".$in['contact_id']."'  ");
		
		// $name .= " (".$count.")";
		$this->db->query("UPDATE customer_contacts SET company_name='".addslashes($name)."', phone='".$info->f('phone')."', email='".$info->f('email')."', customer_id='".$info->f('customer_id')."' WHERE contact_id='".$in['contact_id']."' ");
		return true;
	}

	/****************************************************************
	* function address_delete(&$in)                                          *
	****************************************************************/
	function address_delete(&$in)
	{
		// $in['failure']=false;
		if(!$this->address_delete_validate($in)){
			// $in['failure']=true;
			return false;
		}

		$this->db->query("DELETE FROM customer_addresses
					  WHERE customer_id	=	'".$in['customer_id']."'
					  AND 	address_id	=	'".$in['address_id']."' ");
		$this->db->query("DELETE FROM article_threshold_dispatch where address_id='".$in['address_id']."' AND customer_id	=	'".$in['customer_id']."'");

		msg::success ( gm("Address deleted."),'success');
		insert_message_log($this->pag,'{l}Customer address deleted by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		return true;
	}
	/****************************************************************
	* function address_delete_validate(&$in)                                          *
	****************************************************************/
	function address_delete_validate(&$in)
	{
		$v = new validation($in);
		$v->field('address_id', 'ID', 'required:exist[customer_addresses.address_id]', "Invalid Id");
		$v->field('customer_id', 'ID', 'required:exist[customers.customer_id]', "Invalid Id");

		return $v->run();

	}

	/****************************************************************
	* function address_update(&$in)                                          *
	****************************************************************/
	function address_update(&$in)
	{
		if(!$this->CanEdit($in)){
			return false;
		}
		$in['failure']=false;
		if(!$this->address_update_validate($in)){
			// $in['failure']=true;
			return false;
		}
		if(!$in['primary']) {
			$exist_primary = $this->db->field("SELECT address_id FROM customer_addresses WHERE customer_id = '".$in['customer_id']."' AND address_id!= '".$in['address_id']."' AND is_primary = '1' ");
			if(!$exist_primary) {
				$in['primary'] =1 ;
			}
		}
		$country_n = get_country_name($in['country_id']);
		
		$this->db->query("UPDATE customer_addresses SET
											country_id		=	'".$in['country_id']."',
											state_id		=	'".$in['state_id']."',
											city			=	'".$in['city']."',
											zip				=	'".$in['zip']."',
											address			=	'".$in['address']."',
											billing			=	'".$in['billing']."',
											is_primary		=	'".$in['primary']."',
											delivery		=	'".$in['delivery']."'
										WHERE customer_id	=	'".$in['customer_id']."' AND address_id='".$in['address_id']."' ");

		if($in['billing']){
			$this->db->query("UPDATE customer_addresses SET billing=0 WHERE customer_id='".$in['customer_id']."' AND address_id!='".$in['address_id']."'");
		}
		/*if($in['delivery']){
			$this->db->query("UPDATE customer_addresses SET delivery=0 WHERE customer_id='".$in['customer_id']."' AND address_id!='".$in['address_id']."'");
		}*/
		
		if($in['primary']){
			if($in['address'])
			{
				if(!$in['zip'] && $in['city'])
				{
					$address = $in['address'].', '.$in['city'].', '.$country_n;
				}elseif(!$in['city'] && $in['zip'])
				{
					$address = $in['address'].', '.$in['zip'].', '.$country_n;
				}elseif(!$in['zip'] && !$in['city'])
				{
					$address = $in['address'].', '.$country_n;
				}else
				{
					$address = $in['address'].', '.$in['zip'].' '.$in['city'].', '.$country_n;
				}
				$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
				$output= json_decode($geocode);
				$lat = $output->results[0]->geometry->location->lat;
				$long = $output->results[0]->geometry->location->lng;
			}
			$this->db->query("SELECT coord_id FROM addresses_coord WHERE customer_id='".$in['customer_id']."'");
			if($this->db->move_next())
			{
				$this->db->query("UPDATE addresses_coord SET location_lat='".$lat."', location_lng='".$long."' WHERE customer_id='".$in['customer_id']."'");
			}else
			{
				$this->db->query("INSERT INTO addresses_coord SET location_lat='".$lat."', location_lng='".$long."', customer_id='".$in['customer_id']."'");
			}
			$this->db->query("UPDATE customer_addresses SET is_primary=0 WHERE customer_id='".$in['customer_id']."' AND address_id!='".$in['address_id']."'");
			$this->db->query("UPDATE customers SET city_name='".$in['city']."', country_name='".get_country_name($in['country_id'])."', zip_name='".$in['zip']."' WHERE customer_id='".$in['customer_id']."' ");
		}

		msg::success ( gm("Changes have been saved."),'success');
		insert_message_log($this->pag,'{l}Customer address updated by{endl} '.get_user_name($_SESSION['u_id']),$this->field_n,$in['customer_id']);
		$in['pagl'] = $this->pag;
		return true;
	}
	/****************************************************************
	* function address_update_validate(&$in)                                          *
	****************************************************************/
	function address_update_validate(&$in)
	{
		$v = new validation($in);
		$v->field('address_id', 'ID', 'required:exist[customer_addresses.address_id]', "Invalid Id");
		if(!$v->run()){
			return false;
		} else {
			return $this->address_add_validate($in);
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function updateContactAccounts(&$in)
	{
		foreach ($in['accounts'] as $key => $value) {
			$this->db->query("UPDATE customer_contactsIds SET position='".$value['position']."',
			                 								department='".$value['department']."',
			                 								title='".$value['title']."',
			                 								e_title='".$value['e_title']."',
			                 								email='".$value['email']."',
			                 								phone='".$value['phone']."',
			                 								fax='".$value['fax']."',
			                 								s_email='".$value['s_email']."'
			                WHERE id='".$value['id']."' ");
		}
		return true;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function setAccountAsPrimary(&$in)
	{
		if(!$this->removeContactFromCustomer_validate($in)){
			return false;
		}
		$this->db->query("UPDATE customer_contactsIds SET `primary`='0' WHERE contact_id='".$in['contact_id']."' ");
		$this->db->query("UPDATE customer_contactsIds SET `primary`='1' WHERE contact_id='".$in['contact_id']."' AND customer_id='".$in['customer_id']."' ");
		$this->updateContactCustomerName($in);
		msg::success ( gm("Changes have been saved."),'success');
		return true;
	}
	function customer_notes(&$in){
		if(!$this->CanEdit($in)){
			return false;
		}
		$this->db->query("UPDATE customers SET customer_notes='".$in['customer_notes']."' WHERE customer_id='".$in['customer_id']."'");
		// msg::$success = gm("Notes updated");
		msg::success ( gm("Changes have been saved."),'success');
		$in['updateFromFinancial'] = true;
		$in['updateFromFinancial1'] = false;
		$in['updateFromFinancial2'] = true;
		return true;
	}

	function delete_list(&$in)
	{
		$this->db->query("DELETE FROM contact_list WHERE contact_list_id='".$in['contact_list_id']."' ");
		$this->db->query("DELETE FROM contact_list_contacts WHERE contact_list_id='".$in['contact_list_id']."' ");
		msg::success ( gm('List deleted').'.','success');
		return true;
	}

	function refresh(&$in){
		$contacts_list = $this->db->query("SELECT * FROM contact_list WHERE contact_list_id='".$in['contact_list_id']."' ");
		$contacts_list->next();
		$list_for = $contacts_list->f('list_for');
		$filtru = unserialize($contacts_list->f('filter'));
		if($filtru){
			$filter = '';
			foreach ($filtru as $field => $value){
				if($value){
					if(is_array($value)){
						$val = '';
						foreach ($value as $key){
							$val .= $key.',';
						}
						$value = $val;
					}
					$value = rtrim($value,',');
					if($value){
						if(substr(trim($field), 0, 13)=='custom_dd_id_'){
							$filter.= " AND ";
							$v = explode(',', $value);
							foreach ($v as $k => $v2) {
								$filter .= "customer_field.value = '".addslashes($v2)."' OR ";
							}
							$filter = rtrim($filter,"OR ");
						}elseif($field == 'c_type '){
							$filter .= " AND ( ";
							$v = explode(',', $value);
							foreach ($v as $k => $v2) {
								$filter .= "c_type_name like '%".addslashes($v2)."%' OR ";
							}
							$filter = rtrim($filter,"OR ");
							$filter .= " ) ";
						}elseif ($field == 'c_type') {
							$filter .= " AND ( ";
							$v = explode(',', $value);
							foreach ($v as $k => $v2) {
								$filter .= "c_type_name like '%".addslashes($v2)."%' OR ";
							}
							$filter = rtrim($filter,"OR ");
							$filter .= " ) ";
						}
						elseif(trim($field) == 'position'){
							$filter .= " AND ( ";
							$v = explode(',', $value);
							foreach ($v as $k => $v2) {
								$filter .= "position_n like '%".addslashes($v2)."%' OR ";
							}
							$filter = rtrim($filter,"OR ");
							$filter .= " ) ";
						}elseif(trim($field) == 'customer_contacts.email'){
							$filter.=" AND customer_contacts.email='' ";
						}
						elseif(trim($field) == 'customers.c_email'){
							$filter.=" AND customers.c_email='' ";
						}
						else{
							$filter .= " AND ".$field." IN (".addslashes($value).") ";
						}
					}elseif(trim($field) == 'customers.c_email'){
						$filter.= " AND customers.c_email!='' ";
					}elseif(trim($field) == 'customer_contacts.email'){
						$filter.= " AND customer_contacts.email!='' ";
					}elseif(trim($field) == 'customers.s_emails'){
						$filter.= " AND customers.s_emails='1' ";
					}
				}
			}
		}
		if(!$filter){
			$filter = '1=1';
		}
		$filter = ltrim($filter," AND");

		if(!$list_for){
			$filter .= " AND customer_contacts.active='1' ";
			$cont = $this->db->field("SELECT count(DISTINCT customer_contacts.contact_id)
						    FROM customer_contacts
						    LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
						    LEFT JOIN customers ON customer_contacts.customer_id=customers.customer_id
						    LEFT JOIN customer_addresses ON customer_contacts.customer_id=customer_addresses.customer_id
						    LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
						    LEFT JOIN customer_type ON customers.c_type=customer_type.id
						    LEFT JOIN customer_sector ON customers.sector=customer_sector.id
						    LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
						    LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
						    LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
            		LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
            		LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
						    WHERE ".$filter." ");
		}else{
			$filter .= " AND customers.active='1' AND is_admin='0' ";
			$cont =  $this->db->field("SELECT COUNT( DISTINCT customers.customer_id)
			        FROM customers
			        LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
			        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
			        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
			        LEFT JOIN customer_type ON customers.c_type=customer_type.id
			        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
			        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
			        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
			        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
							LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
							LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
            		WHERE ".$filter." ");
		}

		$this->db->query("UPDATE contact_list SET contacts='".$cont."' WHERE contact_list_id='".$in['contact_list_id']."' ");
		msg::success ( gm('List Updated'),'success');
		return true;
	}

	function list_add(&$in){
		if(!$this->list_add_validate($in)){
			json_out($in);
			return false;
		}

		$in['contact_list_id'] = $this->db->insert("INSERT INTO contact_list SET name='".$in['list_name']."', list_type='".$in['list_type']."', contacts='".$in['nr_contacts']."', list_for='".$in['list_for']."' ");

		if($in['list_type'] == 1 && $in['list_for'] == 0){
			$contacts = $this->db->query("SELECT DISTINCT customer_contacts.contact_id
							        FROM customer_contacts
							        LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
							        LEFT JOIN customers ON customer_contacts.customer_id=customers.customer_id
							        LEFT JOIN customer_addresses ON customer_contacts.customer_id=customer_addresses.customer_id
							        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
							        LEFT JOIN customer_type ON customers.c_type=customer_type.id
							        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
							        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
							        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
							        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
            						LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
							        WHERE customer_contacts.active='1'");

			while ($contacts->next()) {
				$this->db->query("INSERT INTO contact_list_contacts SET contact_list_id='".$in['contact_list_id']."', contact_id='".$contacts->f('contact_id')."' ");
			}
		}
		if($in['list_type'] == 1 && $in['list_for'] == 1){
			$contacts = $this->db->query("SELECT DISTINCT customers.customer_id
							        FROM customers
							        LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
							        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
							        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
							        LEFT JOIN customer_type ON customers.c_type=customer_type.id
							        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
							        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
							        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
							        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
            						LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
							        WHERE customers.active='1'");

			while ($contacts->next()) {
				$this->db->query("INSERT INTO contact_list_contacts SET contact_list_id='".$in['contact_list_id']."', contact_id='".$contacts->f('customer_id')."' ");
			}
		}
		$in['state'] = 'smartListEdit';
		if($in['list_for'] == 1){
			// $in['state'] = 'crm_lv';
			// ark::$controller = 'crm_lv';
		}

		msg::success ( gm('List added'),'success');
		json_out($in);
		return true;
	}

	function list_add_validate(&$in){
		$v = new validation($in);
		$v->field('list_name', 'List name', "required:unique[contact_list.name.(contact_list_id!='".$in['contact_list_id']."')]");
		return $v->run();
	}

	function list_update(&$in){
		if(!$this->list_add_validate($in)){
			return false;
		}
		/*if($in['list_type'])
		{
			$k =  $this->db->field("SELECT COUNT(contact_id) FROM contact_list_contacts WHERE active='1' AND contact_list_id='".$in['contact_list_id']."' ");
		}else
		{*/
			$k = $in['nr_contacts'];
		// }
		$this->db->query("UPDATE contact_list SET contacts='".$k."',filter='".addslashes( serialize($in['filter']))."', name='".$in['list_name']."' WHERE contact_list_id='".$in['contact_list_id']."' ");
		if($in['filter'] && $in['list_type'] && $in['list_for'] == 0){ //echo 'a';exit();
			$this->db->query("UPDATE contact_list_contacts SET is_part='0' WHERE contact_list_id='".$in['contact_list_id']."' ");
			$contacts = $this->db->query("SELECT DISTINCT customer_contacts.contact_id
							        FROM customer_contacts
							        LEFT JOIN customer_contact_address ON customer_contacts.contact_id=customer_contact_address.contact_id
							        LEFT JOIN customers ON customer_contacts.customer_id=customers.customer_id
							        LEFT JOIN customer_addresses ON customer_contacts.customer_id=customer_addresses.customer_id
							        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
							        LEFT JOIN customer_type ON customers.c_type=customer_type.id
							        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
							        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
							        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
							        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
            						LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
            						LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
							        WHERE ".stripslashes($in['filter_a']));
			$i = 0;
			while ($contacts->next()) {
				$exist = $this->db->query("SELECT contact_id FROM contact_list_contacts WHERE contact_id='".$contacts->f('contact_id')."' AND contact_list_id='".$in['contact_list_id']."' ");
				if($exist->next()){
					$this->db->query("UPDATE contact_list_contacts SET is_part='1' WHERE contact_id='".$contacts->f('contact_id')."' AND contact_list_id='".$in['contact_list_id']."' ");
				}
				else{
					$this->db->query("INSERT INTO contact_list_contacts SET contact_list_id='".$in['contact_list_id']."', contact_id='".$contacts->f('contact_id')."' ");
				}
				$i++;
			}
			$this->db->query("UPDATE contact_list SET contacts='".$i."' WHERE contact_list_id='".$in['contact_list_id']."' ");
		}elseif ($in['filter'] && $in['list_type'] && $in['list_for'] == 1) { //echo 'b';exit();
			$this->db->query("UPDATE contact_list_contacts SET is_part='0' WHERE contact_list_id='".$in['contact_list_id']."' ");
			$contacts = $this->db->query("SELECT DISTINCT customers.customer_id
							        FROM customers
							        LEFT JOIN customer_contact_language ON customers.language=customer_contact_language.id
							        LEFT JOIN customer_addresses ON customers.customer_id=customer_addresses.customer_id
							        LEFT JOIN customer_legal_type ON customers.legal_type=customer_legal_type.id
							        LEFT JOIN customer_type ON customers.c_type=customer_type.id
							        LEFT JOIN customer_sector ON customers.sector=customer_sector.id
							        LEFT JOIN article_price_category ON customers.cat_id=article_price_category.category_id
							        LEFT JOIN customer_lead_source ON customers.lead_source=customer_lead_source.id
							        LEFT JOIN customer_various1 ON customers.various1=customer_various1.id
            						LEFT JOIN customer_various2 ON customers.various2=customer_various2.id
            						LEFT JOIN customer_field ON customers.customer_id=customer_field.customer_id
							        WHERE ".stripslashes($in['filter_a']));
			$i = 0;
			while ($contacts->next()) {
				$exist = $this->db->query("SELECT contact_id FROM contact_list_contacts WHERE contact_id='".$contacts->f('customer_id')."' AND contact_list_id='".$in['contact_list_id']."' ");
				if($exist->next()){
					$this->db->query("UPDATE contact_list_contacts SET is_part='1' WHERE contact_id='".$contacts->f('customer_id')."' AND contact_list_id='".$in['contact_list_id']."' ");
				}
				else{
					$this->db->query("INSERT INTO contact_list_contacts SET contact_list_id='".$in['contact_list_id']."', contact_id='".$contacts->f('customer_id')."' ");
				}
				$i++;
			}
			$this->db->query("UPDATE contact_list SET contacts='".$i."' WHERE contact_list_id='".$in['contact_list_id']."' ");
		} //print_r($in['list_for']);exit();
		// if($in['list_for'] == 1){
		// 	ark::$controller = 'crm_lv';
		// }


		$this->db->query("DELETE FROM contact_list_contacts WHERE contact_list_id='".$in['contact_list_id']."' AND is_part='0' ");
		msg::success ( gm('List Updated'),'success');
		return true;
	}

	function list_deactivate_c(&$in){
		$exist = $this->db->query("SELECT contact_id FROM contact_list_contacts WHERE contact_id='".$in['id']."' AND contact_list_id='".$in['contact_list_id']."' ");
		if(!$exist->next()){
			$this->db->query("INSERT INTO contact_list_contacts SET active='".$in['active']."', contact_id='".$in['id']."', contact_list_id='".$in['contact_list_id']."' ");
		}else{
			$this->db->query("UPDATE contact_list_contacts SET active='".$in['active']."' WHERE contact_id='".$in['id']."' AND contact_list_id='".$in['contact_list_id']."' ");
		}
		$this->db->query("UPDATE contact_list SET contacts=(SELECT COUNT(contact_id) FROM contact_list_contacts WHERE contact_list_id='".$in['contact_list_id']."' AND active='1' ) WHERE contact_list_id='".$in['contact_list_id']."' ");
		msg::success ( gm('List Updated'),'success');
		return true;
	}

	function list_deactivate_co(&$in){
		$exist = $this->db->query("SELECT customer_id FROM company_reports_lists WHERE customer_id='{$in['id']}' AND list_id='{$in['list_id']}' ");
		if(!$exist->next()){
			$this->db->query("INSERT INTO company_reports_lists SET active='{$in['active']}', customer_id='{$in['id']}', list_id='{$in['list_id']}' ");
		}else{
			$this->db->query("UPDATE company_reports_lists SET active='{$in['active']}' WHERE customer_id='{$in['id']}' AND list_id='{$in['list_id']}' ");
		}
		$this->db->query("UPDATE company_report_list SET contacts=(SELECT COUNT(customer_id) FROM company_reports_lists WHERE list_id='{$in['list_id']}' AND active='1' ) WHERE list_id='{$in['list_id']}' ");
		msg::success ( gm('List Updated'),'success');
		return true;
	}

	/*function delete_activity(&$in)
	{
		// if(!$this->CanEdit($in)){
		// 	return false;
		// }
		$this->db->query("DELETE FROM customer_contact_activity WHERE customer_contact_activity_id='".$in['id']."' ");
		$this->db->query("DELETE FROM customer_contact_activity_contacts WHERE activity_id='".$in['id']."' ");
		$this->db->query("DELETE FROM logging_tracked WHERE activity_id='".$in['id']."' ");
		msg::success ( gm('Activity deleted'),'success');
		json_out($in);
		return true;
	}*/

	/*function update_status(&$in){
		// $date_init=$in['date1'].' '.$in['date2'];
		// $reminder_date=$this->time_format($date_init);
        $reminder_date=strtotime($in['reminder_event']);

        if($in['reminder_event']){
         	$this->db->query("UPDATE logging SET reminder_date='".$reminder_date."' WHERE log_id='".$in['log_code']."'");

         	$activity_id=$this->db->field("SELECT logging_tracked.activity_id FROM logging INNER JOIN logging_tracked ON logging.log_id=logging_tracked.log_id 
         		WHERE logging.log_id='".$in['log_code']."'
                GROUP BY logging_tracked.activity_id
         		");

         	$this->db->query("UPDATE customer_contact_activity SET reminder_date='".$reminder_date."' WHERE customer_contact_activity_id='".$activity_id."'");
        }
	    if(!$in['not_status_change']){
			if($in['status_change'] == '0'){
				$this->db->query("UPDATE logging SET status_other='1',finished='0',finish_date='0' WHERE log_id='".$in['log_code']."'");
			}else {
				$this->db->query("UPDATE logging SET finished='1',finish_date='".time()."' WHERE log_id='".$in['log_code']."'");
			}
		}	
        

		$log = $this->db->query("SELECT finished, due_date,status_other FROM logging WHERE log_id='".$in['log_id']."' "); 
		$activity = $this->db->field("SELECT activity_id FROM logging_tracked WHERE log_id='".$in['log_id']."' ");
		switch ($activity) {
		case '1':
			if($log->f('finished') == 1){
				$color = 'green_status';
			}else{
				$color = 'opened_status';
			}
			break;
		case '2':
			if($log->f('finished') == 1){
				$color = 'green_status';
			}else{
				$color = 'opened_status';
			}
			break;
		case '4':
			if($log->f('finished') == 1){
				$color = 'green_status';
			}else{
				$color = 'opened_status';
			}
			break;
		case '5':
			if($log->f('finished') == 1){
				$color = 'green_status';
			}else{
				$color = 'opened_status';
			}
			break;
		default:
			if($log->f('finished') == 1){
				$color = 'green_status';
			}else {
				$color = 'opened_status';
			
			}
			break;
		}
		$in['color'] = $color;
		if(strtotime('now')>$reminder_date && $in['status_change'] == 0){
			$in['color'] ='red_status';
			$in['late']=true;
		}
		//console::log($in['color']);
		msg::success ( gm("Status changed succesfully!"),'success');
		return true;
	}*/

	/*function time_format($date_init){
		if(strpos($date_init,'/') !== false){
			$date=strtotime(str_replace('/', '-', $date_init));
		}else if(strpos($date_init,'.') !== false){
			$date=strtotime(str_replace('.', '-', $date_init));
		}else{
			$date=strtotime($date_init);
		}
		return $date;
	}*/

	/*function add_activity(&$in){
		



		switch($in['task_type']){
			case '1':
				$this->email_add($in);
				break;
			case '2':
				$this->event_add($in);
				break;
			case '4':
				$this->meeting_add($in);
				break;
			case '5':
				$this->call_add($in);
				break;
			default:
				$this->task_add($in);
				break;
		}
		foreach ($in['contact_ids'] as $contact_id) {
			$this->db->query("INSERT INTO customer_contact_activity_contacts SET activity_id='".$in['some_id']."',
																		 contact_id = '".$contact_id."',
																		 action_type = '0'");
		}
		$users = $this->db_users->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."'")->getAll();
		foreach ($users as $key => $value) {
				$this->db->query("INSERT INTO logging_tracked SET
									activity_id='".$in['some_id']."',
									log_id='".$in['log_id']."',
									customer_id='".$in['customer_id']."',
									user_id='".$value['user_id']."'");
		}
		$this->db->query("UPDATE logging_tracked SET seen='0' WHERE user_id='".$_SESSION['u_id']."'AND activity_id='".$in['some_id']."' AND log_id='".$in['log_id']."'");
		msg::success ( gm("Activity added").".",'success');
		
		return true;
	}*/

	/*function task_add(&$in){
		// if($in['user_to_id'] && $in['assigned_task']){
		if($in['user_to_id'] ){
			$user_id=$in['user_to_id'];
		} else {
			$user_id = 0;
		}
		// $date_init=$in['start_d_task'].' '.$in['start_d_task_sel'];
		// $date=$this->time_format($date_init);
		$date = strtotime($in['date']);
		//if($in['hd_task']){
			// $date_reminder_init=$in['reminder'].' '.$in['reminder_time_task'];
			// $date_reminder=$this->time_format($date_reminder_init);
			$date_reminder = strtotime($in['reminder_event']);
		//}else{
		//	$date_reminder=0;
		//}
		$in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														status_other='0',
														reminder_date='".$date_reminder."' ");

		$in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
																	contact_activity_note='".$in['comment']."',
																	log_comment='".$in['log_comment']."',
																	log_comment_date='".time()."',
																	date='".time()."',
																	customer_id='".$in['customer_id']."',
																	reminder_date='".$date_reminder."',
																	email_to='".$in['user_id']."' ");
		
	}*/

	/*function event_add(&$in){
		$date = strtotime($in['date']);
		
		// $date_init=$in['date'].' '.$in['dateHours'];
		// $date=$this->time_format($date_init);
		if($in['hd_events']){
			// $date_reminder_init=$in['reminder_event'].' '.$in['reminder_time_event'];
			// $date_reminder=$this->time_format($date_reminder_init);
			$date_reminder = strtotime($in['reminder_event']);
			// if($in['user_to_id'] && $in['assigned_event']){
			if($in['user_to_id']){
				$user_id=$in['user_to_id'];
			} else {
				$user_id = 0;
			}
		}else{
			$date_reminder=0;
			$user_id = $in['user_id'];
		}
		$in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														not_task='1',
														reminder_date='".$date_reminder."' ");
		$in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
														contact_activity_note='".$in['comment']."',
														log_comment='".$in['log_comment']."',
														log_comment_date='".time()."',
														date='".time()."',
														customer_id='".$in['customer_id']."',
														reminder_date='".$date_reminder."',
														email_to='".$in['user_id']."' ");
	}*/

	/*function call_add(&$in){
		$date = strtotime($in['date']);
		// $date_init=$in['start_d_call'].' '.$in['start_d_call_sel'];
		// $date=$this->time_format($date_init);
		if($in['hd_events']){
			// $reminder_value=$in['hd_call_select'] == '0' ? ('-'.$in['hd_input_call'].' '.'hours') : ('-'.$in['hd_input_call'].' '.'days') ;
			// $reminder_date=strtotime($reminder_value,$date);
			// $reminder_value=$in['reminder_call'].' '.$in['reminder_time_call'];
			// $reminder_date=$this->time_format($reminder_value);
			$reminder_date = strtotime($in['reminder_event']);
			// if($in['user_to_id'] && $in['assigned_call']){
			if($in['user_to_id'] ){
				$user_id=$in['user_to_id'];
			} else {
				$user_id = 0;
			}
		} else {
			$reminder_date=0;
			$user_id = $in['user_id'];
		}
		
						 

		$in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														status_other='1',
														not_task='1',
														reminder_date='".$reminder_date."',
														type_of_call='".$in['type_of_call']."'
														 ");
		$in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
																	contact_activity_note='".$in['comment']."',
																	log_comment='".$in['log_comment']."',
																	log_comment_date='".time()."',
																	date='".time()."',
																	customer_id='".$in['customer_id']."',
																	reminder_date='".$reminder_date."',
																	type_of_call='".$in['type_of_call']."',
																	email_to='".$in['user_id']."' ");
	}*/

	/*function email_add(&$in){
		// $date_init=$in['start_d_email'].' '.$in['start_d_email_sel'];
		// $date=$this->time_format($date_init);
		$date = strtotime($in['date']);
		if($in['hd_events']){
			// $reminder_value=$in['hd_email_select'] == '0' ? ('-'.$in['hd_input_email'].' '.'hours') : ('-'.$in['hd_input_email'].' '.'days') ;
			// $reminder_date=strtotime($reminder_value,$date);
			// $reminder_value=$in['reminder_email'].' '.$in['reminder_time_email'];
			// $reminder_date=$this->time_format($reminder_value);
			$reminder_date = strtotime($in['reminder_event']);
			if($in['user_to_id'] && $in['assigned_email']){
				$user_id=$in['user_to_id'];
			} else {
				$user_id = 0;
			}
		} else {
			$reminder_date=0;
			$user_id = $in['user_id'];
		}
		$in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														status_other='1',
														not_task='1',
														reminder_date='".$reminder_date."' ");
		$in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
																	contact_activity_note='".$in['comment']."',
																	log_comment='".$in['log_comment']."',
																	log_comment_date='".time()."',
																	date='".time()."',
																	customer_id='".$in['customer_id']."',
																	reminder_date='".$reminder_date."',
																	email_to='".$in['user_id']."' ");
	}*/

	/*function meeting_add(&$in){

		if($in['hd_events']){
			// $date_init=$in['start_d_meet'].' '.$in['start_d_meet_sel'];
			// $date=$this->time_format($date_init);
			
			// $reminder_value=$in['hd_meet_select'] == '0' ? ('-'.$in['hd_input_meet'].' '.'hours') : ('-'.$in['hd_input_meet'].' '.'days') ;
			// $reminder_date=strtotime($reminder_value,$date);
			// $reminder_value=$in['reminder_meet'].' '.$in['reminder_time_meet'];
			// $reminder_date=$this->time_format($reminder_value);
			$reminder_date=strtotime($in['reminder_event']);
			// if($in['user_to_id'] && $in['assigned_meeting']){
			if($in['user_to_id']){
				$user_id=$in['user_to_id'];
			} else {
				$user_id = 0;
			}
		} else {
			$reminder_date=0;
			$user_id = $in['user_id'];
		}
		// $due_date_init=$in['start_d_meet'].' '.$in['start_d_meet_sel'];
		// $due_date=$this->time_format($due_date_init);
		$due_date = strtotime($in['date']);
		$in['log_id'] = $this->db->insert("INSERT INTO logging SET field_name='customer_id',
														pag='customer',
														field_value='".$in['customer_id']."',
														date='".time()."',
														type='2',
														user_id='".$in['user_id']."',
														to_user_id='".$user_id."',
														done='0',
														due_date='".$due_date."',
														log_comment='".$in['comment']."',
														message='".$in['log_comment']."',
														not_task='1',
														reminder_date='".$reminder_date."' ");
		$in['some_id'] = $this->db->insert("INSERT INTO customer_contact_activity SET contact_activity_type='".$in['task_type']."',
																	contact_activity_note='".$in['comment']."',
																	log_comment='".$in['log_comment']."',
																	log_comment_date='".time()."',
																	date='".time()."',
																	customer_id='".$in['customer_id']."',
																	reminder_date='".$reminder_date."',
																	email_to='".$in['user_id']."' ");
		$my_name = $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
		$my_name->next();
		if($in['all_day_meet'])
		{
			$duration = 0;
			$all_day = 1;
		} else {
			switch ($in['duration_meet']) {
				case '0-15':
					$duration = '900';
					break;
				case '0-30':
					$duration = '1800';
					break;
				case '1-0':
					$duration = '3600';
					break;
				case '2-0':
					$duration = '7200';
					break;
				case '3-0':
					$duration = '10800';
					break;
				default:
					$duration = '';
					break;
			}
			
			$all_day = 0;
		}
		if(!$in['user_id'])
		{
			$in['user_id'] = $_SESSION['u_id'];
		}
		// $start_date_init=$in['start_d_meet'].' '.$in['start_d_meet_sel'];
		$start_date= $due_date;
		// $end_date_init=$in['end_d_meet'].' '.$in['end_d_meet_sel'];
		// $end_date=$this->time_format($end_date_init);
		$end_date = strtotime($in['end_d_meet']);
		
		$in['meeting_id'] = $this->db->insert("INSERT INTO customer_meetings SET
															   subject='".$in['comment']."',
															   location='".$in['location_meet']."',
															   start_date='".$start_date."',
															   end_date='".$end_date."',
															   duration='".$duration."',
															   all_day='".$all_day."',
															   message='".strip_tags($in['log_comment'])."',
															   from_c='".utf8_encode($my_name->f('first_name'))." ".utf8_encode($my_name->f('last_name'))."',
															   type='1',
															   customer_id='".$in['customer_id']."',
															   customer_name='".$in['customer_name']."',
															   user_id='".$in['user_id']."'
															");
		if(!empty($in['contact_ids'])){
			foreach ($in['contact_ids'] as $contact_id) {
				$this->db->query("INSERT INTO customer_contact_activity_contacts SET activity_id='".$in['meeting_id']."',
																				 contact_id = '".$contact_id."',
																				 action_type = '1'");
			}
		}
		$coordinates=$this->db->field("SELECT coord_id FROM addresses_coord WHERE customer_id='".$in['customer_id']."'");
		if(!$coordinates){
			$this->db->query("INSERT INTO addresses_coord SET customer_id='".$in['customer_id']."',
										location_lat='".$in['latitude_customer']."',
										location_lng='".$in['longitude_customer']."' ");
		}
		$calendar = $this->db->field("SELECT app_id FROM apps WHERE name='Google Calendar' AND main_app_id='0' AND type='main' AND active='1' ");
		$google_sync=$this->db->field("SELECT api FROM apps WHERE main_app_id='".$calendar."' AND type='outgoing' ");
		if($google_sync==1){
			$this->sendToGoogle($in);
		}
	}*/

	function sendToGoogle(&$in)
	{
		### google calendar ###
		ark::loadLibraries(array('gCalendar'));
		$calendar = new gCalendar($in);
		$calendar->sendToGoogle($in);
		### google calendar ###
	}

	/*function modify_activity_date(&$in){


		// $date_init=$in['date1'].' '.$in['date2'];
		$date_activity=strtotime($in['date']);
		// $date_activity=$this->time_format($date_init);

		$this->db->query("UPDATE logging SET type_of_call='".$in['type_of_call_id']."' , due_date='".$date_activity."'  WHERE  log_id = '".$in['log_code']."' ");

		if($in['type_of_call_id']){
            $in['type_of_call_view']=gm('incoming call');
		}else{
            $in['type_of_call_view']=gm('outgoing call');
		} 
		return true;
	}*/

	/**
	 * undocumented function
	 *
	 * @return bool
	 * @author Mp
	 **/
	/*function log_comment(&$in)
	{
		// if(!$this->CanEdit($in)){
		// 	return false;
		// }
		$date = time();
		$in['log_comment_date'] = date(ACCOUNT_DATE_FORMAT,$date).', '.date('H:i',$date);
		$this->db->query("UPDATE customer_contact_activity SET log_comment='".$in['comments']."', log_comment_date='".$date."' WHERE customer_contact_activity_id='".$in['log_id']."' ");
		$log_id=$this->db->field("SELECT log_id FROM logging_tracked WHERE activity_id='".$in['log_id']."' GROUP BY activity_id ");
		$this->db->query("UPDATE logging SET message='".$in['comments']."' WHERE log_id='".$log_id."' ");
		$in['comments'] = stripslashes($in['comments']);
		return true;
	}*/

	/*function update_status(&$in){
		// $date_init=$in['date1'].' '.$in['date2'];
		// $reminder_date=$this->time_format($date_init);
        $reminder_date=strtotime($in['reminder_event']);
        if($in['reminder_event']){
         	$this->db->query("UPDATE logging SET reminder_date='".$reminder_date."' WHERE log_id='".$in['log_id']."'");

         	$activity_id=$this->db->field("SELECT logging_tracked.activity_id FROM logging INNER JOIN logging_tracked ON logging.log_id=logging_tracked.log_id 
         		WHERE logging.log_id='".$in['log_id']."'
                GROUP BY logging_tracked.activity_id
         		");

         	$this->db->query("UPDATE customer_contact_activity SET reminder_date='".$reminder_date."' WHERE customer_contact_activity_id='".$activity_id."'");
        }
     	if(!$in['not_status_change']){
			if($in['status_change'] == '0'){
				$this->db->query("UPDATE logging SET status_other='1',finished='0',finish_date='0' WHERE log_id='".$in['log_id']."'");
			}else {
				$this->db->query("UPDATE logging SET finished='1',finish_date='".time()."' WHERE log_id='".$in['log_id']."'");
			}
	 	}        

		$log = $this->db->query("SELECT finished, due_date,status_other FROM logging WHERE log_id='".$in['log_id']."' "); 
		$activity = $this->db->field("SELECT activity_id FROM logging_tracked WHERE log_id='".$in['log_id']."' ");
		switch ($activity) {
		case '1':
			if($log->f('finished') == 1){
				$color = 'green_status';
			}else{
				$color = 'opened_status';
			}
			break;
		case '2':
			if($log->f('finished') == 1){
				$color = 'green_status';
			}else{
				$color = 'opened_status';
			}
			break;
		case '4':
			if($log->f('finished') == 1){
				$color = 'green_status';
			}else{
				$color = 'opened_status';
			}
			break;
		case '5':
			if($log->f('finished') == 1){
				$color = 'green_status';
			}else{
				$color = 'opened_status';
			}
			break;
		default:
			if($log->f('finished') == 1){
				$color = 'green_status';
			}else {
				$color = 'opened_status';
			
			}
			break;
		}
		$in['color'] = $color;
		if(strtotime('now')>$reminder_date && $in['status_change'] == 0){
			$in['color'] ='red_status';
			$in['late']=true;
		}
		//console::log($in['color']);
		msg::success ( gm("Status changed succesfully!"),'success');
		return true;
	}*/

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	/*function UpdateActivity(&$in)
	{
		$this->modify_activity_date($in);
		$this->log_comment($in);
		$this->update_status($in);
	}*/

	function erase_data(&$in)
	{

			$array_table = array();
			$tables_row = array();
			$column_row = array();
			$tables = $this->db->query("SHOW TABLES WHERE Tables_in_".DATABASE_NAME." LIKE 'customer%'")->getAll();
			foreach ($tables as $query => $table) {
				array_push($array_table, $table);
					foreach ($table as $querys => $tab) {
						array_push($tables_row, $tab);
					}
			}
			for($z=0;$z<31;$z++){
				$this->db->query("TRUNCATE TABLE ".$tables_row[$z]."");
			}
			msg::success ( gm("The data has been erased."),'success');
			return true;

	}

	function restore_data(&$in)
	{
		
		if($in['backup_id']!=0){
			$this->erase_data($in);	
			set_time_limit(0);
		    ini_set('memory_limit', '-1');   
			$file1=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/crm/backup'.$in['backup_id'].'.xml';
			$xmlDoc = simplexml_load_file($file1);
			$i=0;
			foreach ($xmlDoc as $key => $value) {
				foreach ($value as $k => $v) {
					$add = false;
					$sql   = "INSERT INTO {$key} SET ";
					foreach ($v as $field => $val) {
						$add=true;
						$sql .= " `".$field."`='".addslashes($val)."',";
					}
					$sql = rtrim($sql,',');
					if($add){
			  			$this->db->insert($sql);
					}
				}
				$i++;
			}
			msg::success ( gm('Restore completed successfully'),'success');
			json_out($in);
			return true;
		}else{
			msg::error ( gm('Are you sure you want restore the data, Please select the file of the data you want to restore'),'error');
			json_out($in);
	    	return false;
		}
	
	}


	/**
	  * undocumented function
	  *
	  * @return void
	  * @author PM
	  **/
	  function tryAddC(&$in){
	    if(!$this->tryAddCValid($in)){ 
	      json_out($in);
	      return false; 
	    }
	    if($in['add_customer']){
	        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
	        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
	        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
	                                            name='".$in['name']."',
	                                            btw_nr='".$in['btw_nr']."',
	                                            city_name = '".$in['city']."',
	                                            country_name ='".get_country_name($in['country_id'])."',
	                                            active='1',
	                                            type='0',
	                                            payment_term      = '".$payment_term."',
	                                            payment_term_type     = '".$payment_term_type."',
	                                            zip_name  = '".$in['zip']."'");
	      
	        $this->db->query("INSERT INTO customer_addresses SET
	                        address='".$in['address']."',
	                        zip='".$in['zip']."',
	                        city='".$in['city']."',
	                        country_id='".$in['country_id']."',
	                        customer_id='".$in['buyer_id']."',
	                        is_primary='1',
	                        delivery='1',
	                        billing='1' ");
	        $in['customer_name'] = $in['name'];

	        $in['value']=$in['btw_nr'];
	        if($this->check_vies_vat_number($in) != false){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
	        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
	        }

	        $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");
	        if(!$show_info->move_next()) {
	          $this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
	                            name    = 'company-customers_show_info',
	                            value   = '1' ");
	        } else {
	          $this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");
	      }
	        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
	        if($count == 1){
	          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
	        }
	        msg::success (gm('Success'),'success');
	        return true;
	    }
	    if($in['add_individual']){
	        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
	        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
	        $in['buyer_id'] = $this->db->insert("INSERT INTO customers SET
	                                            name='".$in['lastname']."',
	                                            firstname = '".$in['firstname']."',
	                                            btw_nr='".$in['btw_nr']."',
	                                            city_name = '".$in['city']."',
	                                            c_email = '".$in['email']."',
	                                            type = 1,
	                                            country_name ='".get_country_name($in['country_id'])."',
	                                            active='1',
	                                            payment_term      = '".$payment_term."',
	                                            payment_term_type     = '".$payment_term_type."',
	                                            zip_name  = '".$in['zip']."'");
	      
	        $this->db->query("INSERT INTO customer_addresses SET
	                        address='".$in['address']."',
	                        zip='".$in['zip']."',
	                        city='".$in['city']."',
	                        country_id='".$in['country_id']."',
	                        customer_id='".$in['buyer_id']."',
	                        is_primary='1',
	                        delivery='1',
	                        billing='1' ");
	        $in['customer_name'] = $in['name'];

	        $in['value']=$in['btw_nr'];
	        if($this->check_vies_vat_number($in) != false){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='0' where customer_id = '".$in['buyer_id']."' ");
	        }else if($this->check_vies_vat_number($in) == false && $in['trends_ok']){
	          $this->db->query("UPDATE customers SET check_vat_number = '".$in['btw_nr']."', vat_form='1' where customer_id = '".$in['buyer_id']."' ");
	        }

	        $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");
	        if(!$show_info->move_next()) {
	          $this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
	                            name    = 'company-customers_show_info',
	                            value   = '1' ");
	        } else {
	          $this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
	                                AND name    = 'company-customers_show_info' ");
	      }
	        $count = $this->db->field("SELECT COUNT(customer_id) FROM customers WHERE is_admin='0' ");
	        if($count == 1){
	          doManageLog('Created the first company.','',array( array("property"=>'first_module_usage',"value"=>'Company') ));
	        }
	        msg::success (gm('Success'),'success');
	        return true;
	    }
	    return true;
	  }

	  /**
	  * undocumented function
	  *
	  * @return void
	  * @author PM
	  **/
	  function tryAddCValid(&$in)
	  {
	    if($in['add_customer']){
	      $v = new validation($in);
	      $v->field('name', 'name', 'required:unique[customers.name]');
	      $v->field('country_id', 'country_id', 'required');
	      return $v->run();  
	    }
	    if($in['add_individual']){
	      $v = new validation($in);
	      $v->field('firstname', 'firstname', 'required');
	      $v->field('lastname', 'lastname', 'required');
	      $v->field('email', 'Email', 'required:email:unique[customers.c_email]');
	      $v->field('country_id', 'country_id', 'required');
	      return $v->run();  
	    }
	    return true;
	  }

	function addEmailContacts(&$in){
	  	if(!$this->CanEdit($in)){
	  		json_out($in);
			return false;
		}
		if(!$this->addEmailContacts_validate($in)){
			json_out($in);
		}
		$i=0;
		$inserted_cust=0;
		$inserted_cust_arr = array();
		$update_cust_arr=array();
		$inserted_contacts_arr=array();
		$update_cont_arr=array();
	  	foreach($in['list'] as $key=>$value){
	  		if(!$value['sync']){
		  		if($value['individual']){
			        $payment_term = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM' ");
			        $payment_term_type = $this->db->field("SELECT value FROM settings WHERE constant_name = 'PAYMENT_TERM_TYPE' ");
			        $buyer_id = $this->db->insert("INSERT INTO customers SET
			                                            name='".$value['lastname']."',
			                                            firstname = '".$value['firstname']."',
			                                            c_email = '".$value['email']."',
			                                            type = 1,
			                                            country_name ='".get_country_name($value['country_id'])."',
			                                            active='1',
			                                            payment_term      = '".$payment_term."',
			                                            payment_term_type     = '".$payment_term_type."',
			                                            zip_name  = '".$value['zip']."'");    
			        $this->db->query("INSERT INTO customer_addresses SET
			                        address='".$value['address']."',
			                        zip='".$value['zip']."',
			                        city='".$value['city']."',
			                        country_id='".$value['country_id']."',
			                        customer_id='".$buyer_id."',
			                        is_primary='1',
			                        delivery='1',
			                        billing='1' ");
			        $show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id   = '".$_SESSION['u_id']."'
			                                AND name    = 'company-customers_show_info' ");
			        if(!$show_info->move_next()) {
			          $this->db_users->query("INSERT INTO user_meta SET  user_id = '".$_SESSION['u_id']."',
			                            name    = 'company-customers_show_info',
			                            value   = '1' ");
			        } else {
			          $this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
			                                AND name    = 'company-customers_show_info' ");
			        }
			        $inserted_cust_arr[$inserted_cust] = $buyer_id;
			        $inserted_cust++;
			    }
			    $customer_id=$buyer_id ? $buyer_id : $value['customer_id'];
			    $company_name=$this->db->field("SELECT name FROM customers WHERE customer_id='".$customer_id."' ");
			    $contact_id = $this->db->insert("INSERT INTO customer_contacts SET
							customer_id	=	'".$customer_id."',
							firstname	=	'".$value['firstname']."',
							lastname	=	'".$value['lastname']."',
							email		=	'".$value['email']."',
							cell		=	'".$value['phone']."',
							company_name=	'".addslashes($company_name)."',
							nylas_contact_id='".$value['id']."',
							`create`	=	'".time()."',
							last_update = 	'".time()."'");

				$this->db->query("UPDATE customer_contacts SET s_email='1' WHERE contact_id='".$contact_id."' ");

				set_first_letter('customer_contacts',$value['lastname'],$this->field_nc,$contact_id);
			
				insert_message_log($this->pagc,'{l}Contact added by{endl} '.get_user_name($_SESSION['u_id']),$this->field_nc,$contact_id);

				$show_info=$this->db_users->query("SELECT value FROM user_meta WHERE user_id 	= '".$_SESSION['u_id']."'
												 								AND name 		= 'company-contacts_show_info'	");
				if(!$show_info->move_next()) {
					$this->db_users->query("INSERT INTO user_meta SET 	user_id = '".$_SESSION['u_id']."',
																		name    = 'company-contacts_show_info',
																		value   = '1' ");
				} else {
					$this->db_users->query("UPDATE user_meta set value = '1' WHERE user_id  = '".$_SESSION['u_id']."'
																			  AND name 		= 'company-contacts_show_info' ");
				}

				$this->db->query("INSERT INTO customer_contactsIds SET customer_id='".$customer_id."', contact_id='".$contact_id."' ");
				$this->db->query("UPDATE customer_contactsIds SET `primary`='0' WHERE contact_id='".$contact_id."' ");
				$this->db->query("UPDATE customer_contactsIds SET `primary`='1' WHERE contact_id='".$contact_id."' AND customer_id='".$customer_id."' ");
				$info = $this->db->query("SELECT phone, email,customer_id FROM customer_contactsIds WHERE contact_id='".$contact_id."' AND `primary`=1 ");	
				$this->db->query("UPDATE customer_contacts SET phone='".$info->f('phone')."' WHERE contact_id='".$contact_id."' ");		
	  			$inserted_contacts_arr[$i] = $contact_id;
	  			$i++;
	  		}
	  	}
	  	if($i>0){
	  		$user_info = $this->db_users->query("SELECT first_name, last_name FROM users WHERE user_id='".$_SESSION['u_id']."'");
			$user_info->next();
			$this->db->query("INSERT INTO company_import_log SET
								`date`='".time()."',
								company_added='".$inserted_cust."',
								company_updated='0',
								contact_added='".$i."',
								contact_updated='0',
								companies_add='".serialize($inserted_cust_arr)."',
								companies_update='".serialize($update_cust_arr)."',
								contacts_add='".serialize($inserted_contacts_arr)."',
								contacts_update='".serialize($update_cont_arr)."',
								username='".$user_info->f('first_name')." ".$user_info->f('last_name')."'");
	  	}
	  	msg::success ( gm('Contacts Added'),'success');
		json_out($in);
	}

	function addEmailContacts_validate(&$in){
		$err=true;
		$keys=array('firstname','lastname','email','customer_id','country_id','phone');
		foreach($in['list'] as $key=>$value){
			if(!$value['sync']){
                if($value['individual']) {
                  $new_keys=$keys;
                  $key_index=array_search('customer_id',$new_keys);
                  if($key_index>=0){
                  	array_splice($new_keys,$key_index,1);
                  }
                  foreach($new_keys as $key1=>$value1){
                    if(!$value[$value1]){
                    	msg::error(gm('This field is required'),$value1.'_'.$key);                   
                      $err=false;
                    }
                  }
                }else{
                  $new_keys=$keys;
                  $key_index=array_search('country_id',$new_keys);
                  if($key_index>=0){
                    array_splice($new_keys,$key_index,1);
                  }
                  foreach($new_keys as $key1=>$value1){           
                    if(!$value[$value1]){
                      msg::error(gm('This field is required'),$value1.'_'.$key);
                     $err=false;
                    }
                  }
                }
            }
        }
        return $err;
	}

}
?>