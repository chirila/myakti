<?php
/**
 * Article price category class
 *
 * @author      Arkweb SRL
 * @link        https://go.salesassist.eu
 * @copyright   [description]
 * @package article
 */
class article_price_category
{

	function article_price_category()
	{
		$this->db = new sqldb;
		$this->db2 = new sqldb;
	}

	/**
	 * Add price category
	 * @param array $in
	 * @return boolean
	 */
	function add(&$in)
	{
		$in['failure'] = false;
		if(!$this->add_validate($in)){
			$in['failure'] = true;
			return false;
		}
		
		$query_sync=array();
		
		$in['category_id'] = $this->db->insert("INSERT INTO pim_article_price_category SET name='".$in['name']."',
		                                                                                   type='".$in['type']."',
		                                                                                   price_type='".$in['price_type']."' ,
		                                                                                   price_value='".return_value($in['price_value'])."',
		                                                                                   price_value_type='".$in['price_value_type']."' 
		                                       ");
       
		$query_sync[0]="INSERT INTO pim_article_price_category SET                         
		                                                                                   category_id='".$in['category_id']."', 
		                                                                                   name='".$in['name']."',
		                                                                                   type='".$in['type']."',
		                                                                                   price_type='".$in['price_type']."',
		                                                                                   price_value='".return_value($in['price_value'])."',
		                                                                                   price_value_type='".$in['price_value_type']."' 
		                                       ";
		
		Sync::start(ark::$model.'-'.ark::$method);
		

		  $article_prices = $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices WHERE base_price=1");
		

		while ($article_prices->next()){
             if($in['price_type']==2){
               $article_base_price=$article_prices->f('purchase_price');
             }else{
               $article_base_price=$article_prices->f('price');
             }
			//insert the article prices
			switch ($in['type']) {
				case 1:                  //discount
				if($in['price_value_type']==1){  // %
					$total_price = $article_base_price - (return_value($in['price_value']) * $article_base_price / 100);
				}else{ //fix
					$total_price = $article_base_price - $in['price_value'];
				}

				break;
				case 2:                 //mark up
				if($in['price_value_type']==1){  // %
					$total_price = $article_base_price + (return_value($in['price_value']) * $article_base_price / 100);
				}else{ //fix
					$total_price =$article_base_price + return_value($in['price_value']);
				}
				break;
				case 3:                 //profit margin
				if($in['price_value_type']==1){  // %
					$total_price = $article_base_price / (1 - return_value($in['price_value']) / 100);
				}else{ //fix
					

					//$total_price =$article_base_price + return_value($in['price_value']);
				}

				break;

			}
			$article_vat_id = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$article_prices->f('article_id')."'");
			$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$article_vat_id."'");
			$total_with_vat = $total_price+($total_price*$vat_value/100);

			
		    $this->db->query("INSERT INTO pim_article_prices SET
				article_id			= '".$article_prices->f('article_id')."',
				price_category_id	= '".$in['category_id']."',
				price				= '".$total_price."',
				total_price			= '".$total_with_vat."'
				");

		}
		
 		Sync::end($in['category_id'],$query_sync);
		msg::$success = "Article Price Category added.";

		return true;
	}

	/**
	 * Update price category
	 * @param  array $in
	 * @return boolean
	 */
	function update(&$in)
	{   
		$in['failure'] = false;
		if(!$this->update_validate($in)){
			$in['failure'] = true;
			return false;
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("SELECT pim_article_price_category.* FROM pim_article_price_category WHERE category_id='".$in['category_id']."' ");
		$this->db->move_next();
		$price_value = $this->db->f('price_value');
		$type = $this->db->f('type');
		$price_type = $this->db->f('price_type');
		$price_value_type = $this->db->f('price_value_type');

		$this->db->query("UPDATE pim_article_price_category SET     name='".$in['name']."',
		                                                            type='".$in['type']."',
		                                                            price_type='".$in['price_type']."',
		                                                            price_value='".return_value($in['price_value'])."',
		                                                            price_value_type='".$in['price_value_type']."'
		                  WHERE category_id='".$in['category_id']."'");

       $this->db2->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom WHERE category_id='".$in['category_id']."'");
   			while($this->db2->move_next()){
           	$article_ids.=$this->db2->f('article_id').',';
   		}
        $article_ids=rtrim($article_ids,',');
        $article_ids_array=explode(',',$article_ids);

		if(($in['type'] != $type) || ($in['price_type'] != $price_type) || (return_value($in['price_value']) != $price_value) || ($in['price_value_type'] != $price_value_type)) {
		
         
           if(!$article_ids){   
			 $this->db->query("DELETE FROM pim_article_prices WHERE price_category_id='".$in['category_id']."' and base_price!=1");
			}
			else{
			 $this->db->query("DELETE FROM pim_article_prices WHERE price_category_id='".$in['category_id']."' AND article_id NOT IN (".$article_ids.") and base_price!=1");	
			}
			
			$article_prices = $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices WHERE base_price=1");

			while ($article_prices->next()){
                     if($in['price_type']==2){
                       $article_base_price=$article_prices->f('purchase_price');
                     }else{
                       $article_base_price=$article_prices->f('price');
                      }

				//insert the article prices
				switch ($in['type']) {
					case 1:                  //discount
					if($in['price_value_type']==1){  // %
						$total_price =$article_base_price - (return_value($in['price_value']) * $article_base_price / 100);
					}else{ //fix
						$total_price = $article_base_price - return_value($in['price_value']);
					}

					break;
					case 2:                 //mark-up margin
					if($in['price_value_type']==1){  // %
						$total_price = $article_base_price + (return_value($in['price_value']) * $article_base_price / 100);
					}else{ //fix
						$total_price =$article_base_price + return_value($in['price_value']);
					}

					break;

					case 3:                 //profit margin
				if($in['price_value_type']==1){  // %
					$total_price = $article_base_price / (1- return_value($in['price_value']) / 100);
				}else{ //fix
					

					//$total_price =$article_base_price + return_value($in['price_value']);
				}

				break;

				}
				$article_vat_id = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$article_prices->f('article_id')."'");
				$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$article_vat_id."'");
				$total_with_vat = $total_price+($total_price*$vat_value/100);
				
            if(!in_array($article_prices->f('article_id'),  $article_ids_array)){
				$this->db->query("INSERT INTO pim_article_prices SET
				article_id			= '".$article_prices->f('article_id')."',
				price_category_id	= '".$in['category_id']."',
				price				= '".$total_price."',
				total_price			= '".$total_with_vat."'
				");
              }
            
			}

		}
		Sync::end($in['category_id']);
		msg::$success = "Article Price Category edited.";
		return true;
	}

	/**
	 * Add add_volume category
	 * @param array $in
	 * @return boolean
	 */
	function add_volume(&$in)
	{
		$in['failure'] = false;
		if(!$this->add_volume_validate($in)){
			$in['failure'] = true;
			return false;
		}
		
		$query_sync=array();
		
		$in['id'] = $this->db->insert("INSERT INTO  article_price_volume_discount SET `from_q`='".$in['from_q']."',
		                                                                                   `to_q`='".$in['to_q']."',
		                                                                                   percent='".return_value($in['percent'])."' 
		                                                                                 
		                                       ");
       
		$query_sync[0]="INSERT INTO article_price_volume_discount SET                         
		                                                                                  `from_q`='".$in['from_q']."',
		                                                                                   `to_q`='".$in['to_q']."',
		                                                                                   percent='".return_value($in['percent'])."' 
		                                       ";
		
		Sync::start(ark::$model.'-'.ark::$method);
		

		
 		Sync::end($in['id'],$query_sync);
		msg::$success = gm("Volume Discount added.");

		return true;
	}
	/**
	 * Update update_volume category
	 * @param  array $in
	 * @return boolean
	 */
	function update_volume(&$in)
	{   
		$in['failure'] = false;
		if(!$this->update_volume_validate($in)){
			$in['failure'] = true;
			return false;
		}

		Sync::start(ark::$model.'-'.ark::$method);
		

		$this->db->query("UPDATE  article_price_volume_discount SET  `from_q`='".$in['from_q']."',
		                                                             to_q='".$in['to_q']."',
		                                                             percent='".return_value($in['percent'])."'
		                                                           
		                  WHERE id='".$in['id']."'");

      
		Sync::end($in['id']);
		msg::$success = gm("Volume Discount edited.");
		return true;
	}


	/**
	 * [update_for_article description]
	 * @param  [type] $in [description]
	 * @return [type]     [description]
	 */
	function update_for_article(&$in)
	{   
		$in['failure'] = false;
		if(!$this->update_for_article_validate($in)){
			$in['failure'] = true;
			return false;
		}
	    
		$this->db->query("DELETE FROM  pim_article_price_category_custom WHERE category_id='".$in['category_id']."' and article_id='".$in['article_id']."'");
        $in['id'] = $this->db->insert("INSERT INTO pim_article_price_category_custom SET   category_id='".$in['category_id']."',
        	                                                                               article_id='".$in['article_id']."',
		                                                                                   type='".$in['category_type']."',
		                                                                                    price_type='".$in['price_type']."',
		                                                                                   price_value='".return_value($in['price_value'])."',
		                                                                                   price_value_type='".$in['price_value_type']."' 
		                                       ");
      
        
		$this->db->query("DELETE FROM pim_article_prices WHERE price_category_id='".$in['category_id']."' AND article_id='".$in['article_id']."' and base_price!=1");
        $article_prices = $this->db->query("SELECT pim_article_prices.* FROM pim_article_prices WHERE base_price=1 AND article_id='".$in['article_id']."'");
        if($in['price_type']==2){
                       $article_base_price=$article_prices->f('purchase_price');
                     }else{
                       $article_base_price=$article_prices->f('price');
                      }

        //insert the article prices
		switch ($in['category_type']) {
			case 1:                  //discount
			if($in['price_value_type']==1){  // %
				$total_price = $article_base_price - (return_value($in['price_value']) *$article_base_price / 100);
			}else{ //fix
				$total_price = $article_base_price - return_value($in['price_value']);
			}

			break;
			case 2:                 //markup margin
			if($in['price_value_type']==1){  // %
				$total_price = $article_base_price + (return_value($in['price_value']) * $article_base_price / 100);
			}else{ //fix
				$total_price =$article_base_price + return_value($in['price_value']);
			}

            break;

			case 3:                 //profit margin
				if($in['price_value_type']==1){  // %
					$total_price = $article_base_price / (1- return_value($in['price_value']) / 100);
				}else{ //fix
					

					//$total_price =$article_base_price + return_value($in['price_value']);
				}
              break;

		}
		$article_vat_id = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$article_prices->f('article_id')."'");
		$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$article_vat_id."'");
		$total_with_vat = $total_price+($total_price*$vat_value/100);
		

		$this->db->query("INSERT INTO pim_article_prices SET
		article_id			= '".$in['article_id']."',
		price_category_id	= '".$in['category_id']."',
		price				= '".$total_price."',
		total_price			= '".$total_with_vat."'
		");

        msg::success(gm("Article Price Category edited"),'success');
		
		return true;
	}

	/**
	 * Revert price category
	 * @param  array $in
	 * @return boolean
	 */
	function revert_price_category(&$in)
	{   
		$in['failure'] = false;
		if(!$this->revert_price_category_validate($in)){
			$in['failure'] = true;
			return false;
		}
	  
		
		$this->db->query("DELETE FROM  pim_article_price_category_custom WHERE category_id='".$in['category_id']."' and article_id='".$in['article_id']."'");
        $this->db->query("DELETE FROM pim_article_prices WHERE price_category_id='".$in['category_id']."' AND article_id='".$in['article_id']."'");

        
         $prices = $this->db->query("SELECT * FROM pim_article_price_category where category_id='".$in['category_id']."'");
				while ($prices->next()){
				//insert the default price
                if($prices->f('price_type')==2){
                      $base_price=$in['purchase_price'];
                 }else{
                         $base_price=$in['price'];
                 }

				switch ($prices->f('type')){
					case 1:                  //discount
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) - ($prices->f('price_value') * return_value($base_price)/ 100);
							}else{ //fix
								$total_price = return_value($base_price) - $prices->f('price_value');
							}

					break;
					case 2:                 //profit margin
							if($prices->f('price_value_type')==1){  // %
								$total_price = return_value($base_price) + ($prices->f('price_value') * return_value($base_price) / 100);
							}else{ //fix
								$total_price = return_value($base_price) + $prices->f('price_value');
							}

					break;
				}

					$article_vat_id = $this->db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$in['article_id']."'");
					$vat_value   = $this->db->field("SELECT value from vats WHERE vat_id='".$article_vat_id."'");					
					$total_with_vat = $total_price+($total_price*$vat_value/100);

					$this->db->query("INSERT INTO pim_article_prices SET
							article_id			= '".$in['article_id']."',
							price_category_id	= '".$prices->f('category_id')."',
							price				= '".$total_price."',
							total_price			= '".$total_with_vat."'
						");

            }

      
		
		msg::success(gm("Article Price Category reverted."),'success');
		return true;
	}

	/**
	 * Delete price category
	 * @param  array $in
	 * @return boolean
	 */
	function delete(&$in)	{
		
		if(!$this->delete_validate($in)){
			
			return false;
		}
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("DELETE FROM pim_article_price_category WHERE category_id='".$in['category_id']."' ");
		$this->db->query("DELETE FROM fam_custom_price WHERE category_id='".$in['category_id']."' ");
		$this->db->query("DELETE FROM pim_article_prices
					      WHERE price_category_id='".$in['category_id']."'");
		Sync::end($in['category_id']);
		msg::$success = "Article Price Category deleted.";
		
		return true;
	}

	/**
	 * Delete price category
	 * @param  array $in
	 * @return boolean
	 */
	function delete_volume(&$in)	{
		
		if(!$this->delete_volume_validate($in)){
			
			return false;
		}
		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("DELETE FROM article_price_volume_discount WHERE id='".$in['id']."' ");
		
		Sync::end($in['id']);
		msg::$success = gm("Article Volume Discount deleted.");
		
		return true;
	}

	/**
	 * Validate add data
	 * @param array $in
	 * @return boolean
	 */
	function add_validate(&$in){

		$v = new validation($in);
		 $is_ok = true;

		if (ark::$method == 'update') {
			$option = ".(category_id != '".$in['category_id']."')";
		} else {
			$option = "";
		}

		$v->field('name','Name','required:unique[pim_article_price_category.name'.$option.']');
		$v->field('type','Type','required');
		//$v->field('price_value','Price Value','required:numeric');
		$v->field('price_value_type','Price Value Type','required');
        
        
if(!$v->run()){
			$is_ok = false;
		}else{
			 if($in['type']==3 && $in['price_value_type']==2){
				msg::$error = gm('Profit Margin can be set only as percent value');
				 $is_ok=false;
				
 			}
		}


        


		return $is_ok;


		

	}

	/**
	 * Validate update data
	 * @param  array $in
	 * @return boolean
	 */
	function update_validate(&$in){
		$v=new validation($in);
		$v->f('category_id','Category Id','required:exist[pim_article_price_category.category_id]');
		if(!$v->run()){
			return false;
		}else{
			return $this->add_validate($in);
		}
	}


/**
	 * Validate add data
	 * @param array $in
	 * @return boolean
	 */
	function add_volume_validate(&$in){

		$v = new validation($in);
		if (ark::$method == 'update_volume') {
			$option = ".(id != '".$in['id']."')";
		} else {
			$option = "";
		}
          // biggest to quantity
		$biggest_to_quantity=$this->db->field("SELECT  article_price_volume_discount.to_q FROM  article_price_volume_discount  ORDER BY to_q DESC  LIMIT 1");
		$biggest_from_quantity=$this->db->field("SELECT article_price_volume_discount.from_q FROM article_price_volume_discount  ORDER BY from_q DESC  LIMIT 1");
		//$is_infinite=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.to_q=0 AND  pim_article_prices.from_q!=0");

        if((return_value($in['from_q']) > return_value($in['to_q'])) && return_value($in['to_q']) ){
		   $in['price_error']=gm('To Qty value should be bigger than From Qty');
		   return false;
		}
		 if($in['from_q']){
        	//check if we allready have an interval for these value
         $this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE '".return_value($in['from_q'])."' BETWEEN article_price_volume_discount.from_q AND article_price_volume_discount.to_q
         	               ");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
		     return false;
          }

        }
        if($in['to_q']){
        	$this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE '".return_value($in['to_q'])."' BETWEEN article_price_volume_discount.from_q AND article_price_volume_discount.to_q
         	               ");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
		     return false;
          }
           	//check if we allready have an interval for these value
         $this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE  article_price_volume_discount.from_q < '".return_value($in['to_q'])."'  AND    article_price_volume_discount.to_q < '".return_value($in['to_q'])."'
         	               AND article_price_volume_discount.to_q > '".return_value($in['from_q'])."'
         	               ");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
		     return false;
          }

        $this->db->query("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE  article_price_volume_discount.from_q < '".return_value($in['to_q'])."' and  article_price_volume_discount.to_q=0 LIMIT 1");
            if($this->db->move_next()){
            	 $in['price_error']=gm('Invalid Interval');
		         return false;
            }



        }else{

            $this->db->query("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE    article_price_volume_discount.to_q=0 LIMIT 1");
            if($this->db->move_next()){
            	 $in['price_error']=gm('Invalid Interval');
		         return false;
            }

        }



	
		$v -> f('from_q', 'Min Qty', 'required:greater_than[1]:numeric');
		$v -> f('to_q', 'Max Qty', 'numeric:greater_than[1]');
		$v->field('percent','Percent','required');

		return $v->run();

	}

	/**
	 * Validate update data
	 * @param  array $in
	 * @return boolean
	 */
	function update_volume_validate(&$in){
		$v=new validation($in);
		$v->f('id',' Id','required:exist[article_price_volume_discount.id]');
			
		$biggest_to_quantity=$this->db->field("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE  id!='".$in['id']."' ORDER BY to_q DESC  LIMIT 1");
		$biggest_from_quantity=$this->db->field("SELECT article_price_volume_discount.from_q FROM article_price_volume_discount WHERE  id!='".$in['id']."' ORDER BY from_q DESC  LIMIT 1");
		//$is_infinite=$this->db->field("SELECT pim_article_prices.to_q FROM pim_article_prices WHERE pim_article_prices.article_id='".$in['article_id']."' AND pim_article_prices.to_q=0 AND  pim_article_prices.from_q!=0");

        if((return_value($in['from_q']) > return_value($in['to_q'])) && return_value($in['to_q']) ){
		   $in['price_error']=gm('To Qty value should be bigger than From Qty');
		   return false;
		}
        if($in['from_q']){
        	//check if we allready have an interval for these value
         $this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE '".return_value($in['from_q'])."' BETWEEN article_price_volume_discount.from_q AND article_price_volume_discount.to_q
         	               AND id!='".$in['id']."'");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
		     return false;
          }

        }
        if($in['to_q']){
        	$this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE '".return_value($in['to_q'])."' BETWEEN article_price_volume_discount.from_q AND article_price_volume_discount.to_q
         	                 AND id!='".$in['id']."'");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
		     return false;
          }

        	//check if we allready have an interval for these value
         $this->db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount
         	               WHERE  article_price_volume_discount.from_q < '".return_value($in['to_q'])."'  AND    article_price_volume_discount.to_q < '".return_value($in['to_q'])."'
         	               AND article_price_volume_discount.to_q > '".return_value($in['from_q'])."'
         	                AND id!='".$in['id']."'");
         if($this->db->move_next()){
         	 $in['price_error']=gm('Invalid Interval');
		     return false;
          }

          $this->db->query("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE  article_price_volume_discount.from_q < '".return_value($in['to_q'])."'  AND  article_price_volume_discount.to_q=0  AND id!='".$in['id']."' LIMIT 1");
            if($this->db->move_next()){
            	 $in['price_error']=gm('Invalid Interval');
		         return false;
            }



        }else{
           //return_value($in['from_q']) >

            $this->db->query("SELECT article_price_volume_discount.to_q FROM article_price_volume_discount WHERE    article_price_volume_discount.to_q=0  AND id!='".$in['id']."' LIMIT 1");
            if($this->db->move_next()){
            	 $in['price_error']=gm('Invalid Interval');
		         return false;
            }

        }

		$v -> f('from_q', 'Min Qty', 'required:greater_than[1]');

return $v->run();
	}
	/**
	 * Validate update for article
	 * @param  array $in
	 * @return boolean
	 */
	function update_for_article_validate(&$in){
		$v = new validation($in);
	    $is_ok = true;

	    $v->field('category_type','Type','required');
		$v->field('price_value','Price Value','required:numeric');
		$v->field('price_value_type','Price Value Type','required');


		if(!$v->run()){
			$is_ok = false;
		}else{
			 if($in['category_type']==3 && $in['price_value_type']==2){
				msg::$error = gm('Profit Margin can be set only as percent value');
				 $is_ok=false;
				
 			}
		}


        


		return $is_ok;


       
     }

    /**
     * Validate revert data for price category
     * @param  array $in
     * @return boolean
     */
	function revert_price_category_validate(&$in){
		$v = new validation($in);
	    $v->f('category_id','Category Id','required:exist[pim_article_price_category_custom.category_id]');

        return $v->run();
    }

	/**
	 * validate delete price category
	 * @param  array $in
	 * @return boolean
	 */
	function delete_validate(&$in){
		$v=new validation($in);
		$v->f('category_id','Category Id','required:exist[pim_article_price_category.category_id]');

		return $v->run();

	}
	function delete_volume_validate(&$in){
		$v=new validation($in);
		$v->f('id',' Id','required:exist[article_price_volume_discount.id]');

		return $v->run();

	}

	/**
	 * [article_ready description]
	 * @param  [type] $in [description]
	 * @return [type]     [description]
	 */
	function article_ready(&$in)
	{
		$this->db->query("UPDATE setup SET value=1 WHERE name='article_price_ready'");
		$article_settings_ready = $this->db->field("SELECT value FROM setup WHERE name='article_settings_ready'");
		$article_price_ready = $this->db->field("SELECT value FROM setup WHERE name='article_price_ready'");
		$article_taxes_ready = $this->db->field("SELECT value FROM setup WHERE name='article_taxes_ready'");
		if ($article_settings_ready==1 && $article_price_ready==1 && $article_taxes_ready==1)
		{
			$this->db->query("UPDATE setup SET value=1 WHERE name='articles_ready'");
			header("Location: index.php?do=home-account_configure");
		}
	}

	    /**
     * Update price list for article
     * @param  array $in
     * @return boolean
     */
	function update_fam_price_list(&$in)
	{  
		if(!$this->update_fam_price_list_validate($in)){
           return false;
		}

		Sync::start(ark::$model.'-'.ark::$method);
		$this->db->query("DELETE FROM  fam_custom_price WHERE fam_id = '".$in['fam_id']."' AND category_id = '".$in['category_id']."'");
        $this->db->query("INSERT INTO fam_custom_price SET fam_id 		= '".$in['fam_id']."',
									category_id 			= '".$in['category_id']."',
									value 			= '".return_value($in['value'])."'");		
   
       //we have tu update all prices for these category and article with these fam
    /*   $this->db->query("SELECT  pim_articles.category_id, pim_articles.article_id FROM pim_articles WHERE pim_articles.category_id='".$in['fam_id']."'");
        while ( $this->db->move_next()) {
 
         	    

         	    $this->db2->query("UPDATE  pim_article_prices SET  price='".$price."' ,total_price='".$total_price."'
        	                       
        	                       WHERE article_id = '".$this->db->f('article_id')."'
        	             AND price_category_id = '".$in['category_id']."'");
         } */
       
	   


        Sync::end($in['fam_custom_price_id']);

		msg::$success = gm('Changes saved');
		return true;
	}

	/**
	 * validate update price list data
	 * @param  array $in
	 * @return boolean
	 */
	function update_fam_price_list_validate(&$in)
	{
		$v = new validation($in);
		$v -> f('fam_id', 'fam_id', 'required');
	
		return $v -> run();

	}

}//end class
?>