<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if($in['xget']){
	$fname = 'get_'.$in['xget'];
	if(function_exists($fname)){
		json_out($fname($in));
	}else{
		msg::error('Function does not exist','error');
		json_out($in);
	}
}
//echo generate_purchase_price($in['article_id']);

global $config;
$db2 = new sqldb();
$db3 = new sqldb();

$o=array();

if(!isset($in['tab'])){
	$in['tab']=0;
}

$in['lang_id'] = $_SESSION['l_id'];
$lang = $db->query("SELECT lang_id,code,default_lang FROM pim_lang WHERE lang_id='".$in['lang_id']."' GROUP BY lang_id");


$in['lang_id']= $lang->f('lang_id');
$in['code']   = $lang->f('code');

if($in['id']){
	$in['article_id'] = $in['id'];
}

if(!$in['article_id'] && !$in['duplicate_article_id'] ){
	json_out(array('message' => gm('Invalid ID')));
}
else { //edit

	$article=$db->query("SELECT pim_articles.*, pim_articles.price AS unit_price,vats.value AS vat_value,pim_articles_lang.name,pim_articles_lang.name2,pim_articles_lang.description,
	                            pim_article_prices.default_price, pim_article_prices.price, pim_article_prices.total_price, pim_article_prices.purchase_price
				         FROM pim_articles
				         LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='".$in['lang_id']."'
						 LEFT JOIN vats ON pim_articles.vat_id=vats.vat_id
						 LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.base_price='1'
				         WHERE pim_articles.article_id='".$in['article_id']."' ");

	$in['internal_name']	= 	gfn($article->gf('internal_name'));

	$in['item_code']   		= 	gfn($article->gf('item_code'));


	$in['purchase_price']   = 	display_number_var_dec($article->f('purchase_price'));
	$in['total_price'] 		= 	display_number_var_dec($article->f('total_price'));


	$in['category_id'] 		= 	$article->gf('category_id');

	$in['supplier_reference'] 	= 	$article->gf('supplier_reference');

	$in['price_type']       =   $article->gf('price_type');

	//$in['supplier_id']   		= 	$article->f('supplier_id');
	//$in['supplier_name']   		= 	$article->f('supplier_name');

	$in['article_category_id'] = $article->gf('article_category_id');

	// $o['category_dd']     = 	build_category_list($db->gf('category_id'),0,1);
	$o['article_id']       = 	$article->gf('article_id');
	$o['lang_id']        	= 	$article->gf('lang_id');
	$o['name']        		= 	gfn($article->gf('name'));
	$o['description'] 		= 	gfn($article->gf('description'));
	$o['name2']       		= 	gfn($article->gf('name2'));
	$o['unit_price']    	= 	display_number_var_dec($article->f('unit_price'));
	$o['item_code']        = 	gfn($article->gf('item_code'));
	$o['price']       		= 	display_number_var_dec($article->f('price'));
	$o['total_price']   = 	display_number_var_dec($article->f('total_price'));
	$o['supplier_reference'] 	= 	$article->gf('supplier_reference');

	$o['supplier_id']   		= 	(string)$article->f('supplier_id');
	//$o['supplier_name']   		= 	(string)$article->f('supplier_name');
	$o['stock']	   		= 	remove_zero_decimals($article->gf('stock'));
	$o['weight']   		= 	 display_number($article->gf('weight'));
	$o['sale_unit']     	= 	$article->gf('sale_unit')?$article->gf('sale_unit'):'';
	$o['packing']       	= 	$article->gf('packing')? remove_zero_decimals($article->f('packing')):'';
	$o['ean_code']    		= 	$article->gf('ean_code');
	$o['article_brand'] 		= 	$article->gf('article_brand_id');
	$o['vat_value'] 			= 	$article->gf('vat_value');
	$o['artPhoto']=   $db->field("SELECT file_name FROM pim_article_photo WHERE parent_id='".$in['article_id']."'");

	if($article->f('price_type')==1){
		$o['price_type_1'] 	= true;
		$o['price_type_2'] 	= false;

	}else{
		$o['price_type_1'] 	= false;
		$o['price_type_2'] 	= true;

	}
	$price_type  = $article->f('price_type');

	$db->query("SELECT pim_article_tax.*,pim_articles_taxes.article_id,pim_articles_taxes.tax_id AS active_tax
            FROM pim_article_tax
            INNER JOIN pim_articles_taxes on pim_articles_taxes.tax_id=pim_article_tax.tax_id AND pim_articles_taxes.article_id='".$in['article_id']."'
           ");

	$j=0;

	$o['taxes']=array();
	while($db->move_next()){
		$taxe_row=array(
			'taxe_code' =>$db->f('code'),
			'tax_id'    =>$db->f('tax_id'),
			'tax_value' =>display_number($db->f('amount'))
		);




		array_push($o['taxes'], $taxe_row);
		$j++;
	}
	$o['has_taxes']=$j;

	if( $price_type==1){
		$prices = get_articlePrice($in);
		// Get article prices
		//$o['price_categories']=$prices['price_categories'];
		//$o['si_vat_value']=$prices['si_vat_value'];
	}else{
		$prices = get_articlePriceVol($in);


		$o['price_categories_vol']=$prices['price_categories_vol'];

	}
	unset($o['price_type_1']);
	unset($o['price_type_2']);





}



if($in['product_id']){
	$o['is_product']=1;
	$o['product_link']="index.php?do=pim-product&product_id=".$in['product_id'];


}else{
	$o['is_product']=0;

}

json_out($o);
function get_articlePriceVol($in){
	$db = new sqldb();
	$db2 = new sqldb();
	$result = array('price_categories_vol'=>array(),
	);
	$db->query("SELECT pim_article_prices.*
			FROM pim_article_prices 
			WHERE pim_article_prices.article_id = '".$in['article_id']."' AND pim_article_prices.base_price!=1
			ORDER BY pim_article_prices.from_q");
	$i=0;
	while ($db->move_next()){

		$price = $db->f('price');
		$vat   =$db->f('total_price')- $db->f('price');
		$total_price = $db->f('total_price') ;
		$vat_percent=(($db->f('total_price')*100)/$db->f('price')) -100;

		$price_row = array(
			'min_qty'	    	=> $db->f('from_q'),
			'max_qty'	    	=> return_value($db->f('to_q'))!=0? $db->f('to_q') : '8',
			'si_price'			=> display_number_var_dec($price),
			'si_percent'		=> display_number($db->f('percent')),
			'si_vat'			=> display_number($vat),
			'si_vat_percent'    => display_number($vat_percent),
			'si_total_price'	=> display_number_var_dec($total_price),
			'article_price_id'  => $db->f('article_price_id'),
			'article_id'  => $in['article_id'],
		);
		array_push($result['price_categories_vol'], $price_row);
		$i++;
	}


	if($i==0){
		$result['is_prices_vol']=false;
	}else{
		$result['is_prices_vol']=true;
	}

	return $result;
}


function get_articlePrice($in){
	$db = new sqldb();
	$db2 = new sqldb();
	$db3 = new sqldb();
	$result = array('price_categories'=>array(),
		'si_vat_value'=>'');

	$db->query("SELECT vats.value,vats.vat_id
			FROM vats
			INNER JOIN pim_articles ON pim_articles.vat_id = vats.vat_id
			WHERE pim_articles.article_id = '".$in['article_id']."' AND pim_articles.active='1' ");
	$db->move_next();
	$article_vat_value = $db->f('value');
	$result['si_vat_value']=$article_vat_value;

	$db->query("SELECT pim_article_price_category.*, pim_article_prices.price, pim_article_prices.total_price
			FROM pim_article_price_category 
			RIGHT JOIN pim_article_prices ON pim_article_price_category.category_id=pim_article_prices.price_category_id
			WHERE pim_article_prices.article_id = '".$in['article_id']."' AND base_price!=1
			ORDER BY pim_article_price_category.category_id");

// to do: check against user price category
	// if to be replaced with while
	if ($db->move_next()) {
		$db2->query("SELECT pim_article_price_category_custom.* FROM pim_article_price_category_custom WHERE category_id='".$db->f('category_id')."' AND article_id='".$in['article_id']."'");
		if($db2->move_next()){
			$is_custom=true;


			$type = $db2->f('type');
			$price_value = $db2->f('price_value');
			$price_value_type = $db2->f('price_value_type');
		}else{
			$is_custom=false;

			$type = $db->f('type');

			$price_value_custom_fam=$db3->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$in['article_category_id']."' AND category_id='".$db->f('category_id')."'");
			if($price_value_custom_fam==NULL){
				$price_value = $db->f('price_value');
				$custom_fam=0;
			}else{
				$price_value = $price_value_custom_fam;
				$custom_fam=1;
			}

			$price_value_type = $db->f('price_value_type');
		}
		if($custom_fam==0){
			$price = $db->f('price');
			$vat   = $article_vat_value * $price / 100;
			$total_price = $db->f('total_price') ;
		}else{
			//we have to apply to the base price the category spec
			$cat_price_type=$db3->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$db->f('category_id')."'");
			$cat_type=$db3->field("SELECT type FROM pim_article_price_category WHERE category_id='".$db->f('category_id')."'");
			$price_value_type=$db3->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$db->f('category_id')."'");

			if($cat_price_type==2){
				$article_base_price=get_article_calc_price($in['article_id'],3);
			}else{
				$article_base_price=get_article_calc_price($in['article_id'],1);
			}

			switch ($cat_type) {
				case 1:                  //discount
					if($price_value_type==1){  // %
						$price = $article_base_price - $price_value * $article_base_price / 100;
					}else{ //fix
						$price = $article_base_price - $price_value;
					}

					break;

				case 2:                 //profit margin
					if($price_value_type==1){  // %
						$price = $article_base_price + $price_value * $article_base_price / 100;
					}else{ //fix
						$price =$article_base_price + $price_value;
					}

					break;

			}


			//$price = $db->f('price');
			$vat   = $article_vat_value * $price / 100;
			$total_price = $vat+$price ;



		}


		$price_row = array(
			'is_custom'	             => $is_custom,
			'si_price_cat_name'	            => $db->f('name'),
			'si_type'	        			=> get_price_category_type($type),
			'si_price_value'				=> display_number_var_dec($price_value),
			'si_price_value_type'			=> get_price_value_type($price_value_type),
			'si_sup_cat_id'		    		=> $db->f('category_id'),
			'si_price'			    		=> display_number_var_dec($price),
			'si_vat'			    		=> display_number($vat),
			'si_total_price'				=> display_number_var_dec($total_price),
			'price_cat_id'               => 'price_cat_'.$in['article_id'].'_'.$db->f('category_id'),
			'article_id'                 => $in['article_id'],
			'article_category_id'        => $in['article_category_id'],
		);



		array_push($result['price_categories'], $price_row);
	}
	return $result;

}

function get_articleDispatch($in){
	$db = new sqldb();
	$db2 = new sqldb();
	$result = array('customer_stocks'=>array(),
	);
	$is_data2=false;
	$i=0;
//first we select own locations
	$db->query("SELECT dispatch_stock.*,dispatch_stock_address.*
            FROM   dispatch_stock
            INNER JOIN  dispatch_stock_address ON  dispatch_stock_address.address_id=dispatch_stock.address_id
            WHERE dispatch_stock.article_id = '".$in['article_id']."' and dispatch_stock.main_address='1'
	        ");
	$i=0;
	while ($db->move_next()) {


		$data_row=array(
			'customer_name' => $db->f('naming'),
			'current_quantity'      => remove_zero_decimals($db->f('stock')),
			'address'=>  $db->f('address'),
			'city'=>  $db->f('city'),
			'zip'=> $db->f('zip'),
			'country'=> get_country_name($db->f('country_id'))
		);


		array_push($result['customer_stocks'], $data_row);




		$i++;
	}
//then customer locations
	$db->query("SELECT dispatch_stock.*, customer_addresses.*,customers.name
            FROM   dispatch_stock
            INNER JOIN customers ON customers.customer_id=dispatch_stock.customer_id
            LEFT JOIN  customer_addresses ON  dispatch_stock.address_id=customer_addresses.address_id AND dispatch_stock.customer_id=customer_addresses.customer_id
            WHERE dispatch_stock.article_id = '".$in['article_id']."' and dispatch_stock.main_address='0'
            ");

	while ($db->move_next()) {


		$data_row_c=array(
			'customer_name' => $db->f('name'),
			'current_quantity'      => remove_zero_decimals($db->f('stock')),
			'address'=> $db->f('address'),
			'city'=> $db->f('city'),
			'zip'=> $db->f('zip'),
			'country'=> get_country_name($db->f('country_id'))
		);


		array_push($result['customer_stocks'], $data_row_c);
		$i++;
	}



	return $result;
}

function get_articleTranslation($in){
	$db = new sqldb();
	$db2 = new sqldb();
	$result = array('translation'=>array(),
	);
	$internal_name=$db->field("SELECT internal_name FROM pim_articles where article_id='".$in['article_id']."'");
	$active_langs = $db->query("SELECT language, lang_id, default_lang,code FROM pim_lang WHERE active = '1' GROUP BY lang_id ORDER BY sort_order ASC");
	$i=1;
	while($active_langs->next()){
		$article_data = $db->query(" SELECT * FROM pim_articles_lang WHERE item_id ='".$in['article_id']."' AND lang_id='".$active_langs->f('lang_id')."' ");

		while($article_data->next()){
			$data_row_l=array(

				'lang_id'			=>	$active_langs->f('lang_id'),
				'lang_name'			=>	$active_langs->f('language'),
				'lang_img'          =>	'images/geo/'.$active_langs->f('code').'.png',

				'internal_name'		=>	$internal_name,
				'name'				=>	gfn($article_data->f('name')),
				'name2'				=>	gfn($article_data->f('name2')),
				'description'		=>	$article_data->f('description'),
				'block_data'        => $active_langs->f('lang_id')==$default_lang ?'readonly="readonly" class="block_edit" ' :''

			);




			array_push($result['translation'], $data_row_l);

		}
	}



	return $result;
}
function get_articleBatch($in){
	$db = new sqldb();
	$db2 = new sqldb();
	$result = array('batch'=>array(
		'use_batch_no'=>false,
		'items'=>array())
	);
	if($in['parent_article_id']){
		$in['article_id']=$in['parent_article_id'];
	}
	$db->query("SELECT pim_articles.use_batch_no FROM pim_articles WHERE article_id = '".$in['article_id']."'");
	if($db->f('use_batch_no')){
		$result['batch']['use_batch_no']=true;
	}
	$art_no_used = $db->field("SELECT COUNT(id) FROM batches WHERE article_id = '".$in['article_id']."' ");


	$result['batch']['disable_use_batch_no']=($result['batch']['use_batch_no'] == 1 && $art_no_used > 0) ? true : false;

	$filter = " article_id='".$in['article_id']."' ";


	$db->query("SELECT batches.*,batch_number_status.name
			FROM batches
			LEFT JOIN batch_number_status
			ON batches.status_id = batch_number_status.id
			WHERE ".$filter." ");
	$j=0;
	while($db->move_next()){
		$in_details = $db->f('status_details_1');
		if($db->f('p_delivery_id')){
			$p_order_serial_number = $db->f('status_details_1');
			$p_order_id = $db2->field("SELECT p_order_id FROM pim_p_order_deliveries WHERE delivery_id = '".$db->f('p_delivery_id')."' ");
			$in_details = '<a href="index.php?do=order-p_order&p_order_id='.$p_order_id.'" target="_blank" >'.$p_order_serial_number.'</a>';
		}
		$out_details = $db->f('status_details_2');
		if($db->f('delivery_id')){
			$order_serial_number = $db->f('status_details_2');
			$order_id = $db2->field("SELECT order_id FROM pim_order_deliveries WHERE delivery_id = '".$db->f('delivery_id')."' ");
			$out_details = '<a href="index.php?do=order-order&order_id='.$order_id.'" target="_blank" >'.$order_serial_number.'</a>';
		}
		$drop_info = array('drop_folder' => 'articles', 'item_id' =>  $db->f('id'), 'is_batch'=>1);

		$data_row=array(
			'batch_number'		=> $db->f('batch_number'),
			'id'			=> $db->f('id'),
			'status_name'		=> $db->f('name'),
			'status_id'		    => $db->f('status_id'),
			'status'			=> $db->f('status_id') == 1 ? 'green' : ($db->f('status_id') == 2 ? 'red' : 'orange' ),
			'date_in'			=> $db->f('date_in') < 10000 ? '' : date(ACCOUNT_DATE_FORMAT,$db->f('date_in')),
			'date_out'			=> $db->f('date_out') < 10000 ? '' : date(ACCOUNT_DATE_FORMAT,$db->f('date_out')),
			'date_exp'			=> $db->f('date_exp') < 10000 ? '' : date(ACCOUNT_DATE_FORMAT,$db->f('date_exp')),
			'in_details'		=> $in_details,
			'out_details'		=> $out_details,
			'quantity'			=> $db->f('quantity'),
			'in_stock'			=> $db->f('in_stock'),
			'show_edit'			=> $db->f('in_stock')>0? true : false,
			'drop_info'			=> htmlentities(json_encode($drop_info)),
		);

		array_push($result['batch']['items'], $data_row);
	}



	return $result;
}



function get_articleSerial($in){
	$db = new sqldb();
	$db2 = new sqldb();
	$result = array('serial'=>array(
		'use_serial_no'=>false,
		'items'=>array())
	);
	if($in['parent_article_id']){
		$in['article_id']=$in['parent_article_id'];
	}
	$db->query("SELECT pim_articles.use_serial_no FROM pim_articles WHERE article_id = '".$in['article_id']."'");
	if($db->f('use_serial_no')){
		$result['serial']['use_serial_no']=true;
	}
	$art_no_used = $db->field("SELECT COUNT(id) FROM serial_numbers WHERE article_id = '".$in['article_id']."' ");


	$result['serial']['disable_use_serial_no']=($result['serial']['use_serial_no'] == 1 && $art_no_used > 0) ? true : false;

	$filter = " article_id='".$in['article_id']."' ";

	$db->query("SELECT serial_numbers.*,serial_number_status.name
			FROM serial_numbers
			LEFT JOIN serial_number_status
			ON serial_numbers.status_id = serial_number_status.id
			WHERE ".$filter." ");
	while($db->move_next()){

		$in_details = $db->f('status_details_1');
		if($db->f('p_delivery_id')){
			$p_order_serial_number = $db->f('status_details_1');
			$p_order_id = $db2->field("SELECT p_order_id FROM pim_p_order_deliveries WHERE delivery_id = '".$db->f('p_delivery_id')."' ");
			$in_details = '<a href="index.php?do=order-p_order&p_order_id='.$p_order_id.'" target="_blank" >'.$p_order_serial_number.'</a>';
		}
		$out_details = $db->f('status_details_2');
		if($db->f('delivery_id')){
			$order_serial_number = $db->f('status_details_2');
			$order_id = $db2->field("SELECT order_id FROM pim_order_deliveries WHERE delivery_id = '".$db->f('delivery_id')."' ");
			$out_details = '<a href="index.php?do=order-order&order_id='.$order_id.'" target="_blank" >'.$order_serial_number.'</a>';
		}
		if($db->f('project_id')){
			$out_details = '<a href="index.php?do=project-project&project_id='.$db->f('project_id').'" target="_blank" >'.$db->f('status_details_2').'</a>';
		}
		if($db->f('service_id')){
			$out_details = '<a href="index.php?do=maintenance-services&service_id='.$db->f('service_id').'" target="_blank" >'.$db->f('status_details_2').'</a>';
		}
		$data_row=array(
			'serial_number'		=> $db->f('serial_number'),
			'id'		=> $db->f('id'),
			'status_name'		=> gm($db->f('name')),
			'status_id'         => $db->f('status_id'),
			'status'			=> $db->f('status_id') == 1 ? 'green' : ($db->f('status_id') == 2 ? 'red' : 'orange' ),
			'date_in'			=> $db->f('date_in') < 10000 ? '' : date(ACCOUNT_DATE_FORMAT,$db->f('date_in')),
			'date_out'			=> $db->f('date_out') < 10000 ? '' : date(ACCOUNT_DATE_FORMAT,$db->f('date_out')),
			'in_details'		=> $in_details,
			//'out_details'		=> $out_details,
		);

		array_push($result['serial']['items'], $data_row);

	}



	return $result;
}


function get_articleCombine($in){
	$db = new sqldb();
	$db2 = new sqldb();
	$result = array('composed'=>array(
		'use_combined'=>false,
		'articles'=>array())
	);
	if($in['parent_article_id']){
		$in['article_id']=$in['parent_article_id'];
	}

	$db->query("SELECT pim_articles.use_combined FROM pim_articles WHERE article_id = '".$in['article_id']."'");
	if($db->f('use_combined')){
		$result['composed']['use_combined']=true;
	}

	$result['composed']['articles_list']			=  get_articles_list($in);


	$filter = " parent_article_id='".$in['article_id']."' ";
	$db->query("SELECT pim_articles.article_threshold_value,pim_articles.stock,pim_articles.hide_stock,pim_articles.article_id,pim_articles.item_code,pim_articles.internal_name,pim_articles_combined.parent_article_id,pim_articles_combined.pim_articles_combined_id,pim_articles_combined.quantity
         FROM pim_articles
			INNER JOIN pim_articles_combined ON pim_articles_combined.article_id=pim_articles.article_id
			WHERE $filter
		  
			");
	$potential_stock= $db->f('stock')/$db->f('quantity');

	while($db->move_next()){
		$potential_stock=min($potential_stock, floor($db->f('stock')/$db->f('quantity')));
		$data_row=array(
			'name'		  			=> $db->f('internal_name'),
			'article_id'  		=> $db->f('article_id'),
			'parent_article_id'  		=> $db->f('parent_article_id'),
			'quantity'  		=> remove_zero_decimals($db->f('quantity')),
			'stock'  			=> remove_zero_decimals($db->f('stock')),
			'show_stock'  		=> $db->f('hide_stock') ? false:true,
			'pim_articles_combined_id'  						=> $db->f('pim_articles_combined_id'),
			'code'		  			=> $db->f('item_code'),
			'allow_stock'         			=> ALLOW_STOCK,
			'stock_multiple_locations'=> STOCK_MULTIPLE_LOCATIONS,

		);

		if($db->f('stock') <= 0){
			$data_row['stock_status']='text-danger';

		}elseif($db->f('stock') > 0  && $db->f('stock') <= $db->f('article_threshold_value') ){
			$data_row['stock_status']='text-warning';
		}else{
			$data_row['stock_status']='text-success';

		}


		array_push($result['composed']['articles'], $data_row);
	}
	$result['composed']['potential_stock']=$potential_stock;

	return $result;
}

function get_articles_list($in)
{

	$db = new sqldb();
	$db2 = new sqldb();
	$def_lang = DEFAULT_LANG_ID;
	if($in['lang_id']){
		$def_lang= $in['lang_id'];
	}
	//if custom language is selected we show default lang data
	if($def_lang>4){
		$def_lang = DEFAULT_LANG_ID;
	}

	switch ($def_lang) {
		case '1':
			$text = gm('Name');
			break;
		case '2':
			$text = gm('Name fr');
			break;
		case '3':
			$text = gm('Name du');
			break;
		default:
			$text = gm('Name');
			break;
	}

	$cat_id = $in['cat_id'];
	if(!$in['from_address_id']) {
		$table = 'pim_articles LEFT JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id AND pim_article_prices.base_price=\'1\'
								   LEFT JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id AND pim_articles_lang.lang_id=\''.$def_lang.'\'
								   LEFT JOIN pim_article_brands ON pim_articles.article_brand_id = pim_article_brands.id ';
		$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.stock, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
						pim_article_prices.price, pim_articles.internal_name,
						pim_article_brands.name AS article_brand,
						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount';

	}else{
		$table = 'pim_articles  INNER JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
			                INNER JOIN  dispatch_stock ON  dispatch_stock.article_id = pim_articles.article_id
							   INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id';

		$columns = 'pim_articles.article_id, pim_articles.vat_id, pim_articles.price_type, pim_articles.article_category_id, pim_articles.article_threshold_value,
						pim_articles.item_code, pim_articles.hide_stock, pim_articles.ean_code, pim_articles.origin_number,
						pim_articles.sale_unit,pim_articles.packing,
						pim_articles.internal_name,	dispatch_stock.article_id,dispatch_stock.stock	,

						pim_articles_lang.description AS description,
						pim_articles_lang.name2 AS item_name2,
						pim_articles_lang.name AS item_name,
						pim_articles_lang.lang_id,
						pim_articles.vat_id,
						pim_articles.is_service,
						pim_articles.price AS unit_price,
						pim_articles.block_discount';
	}

	$filter.=" 1=1 ";
	$filter= "pim_articles.is_service = '0'";
	//$filter= "pim_article_prices.price_category_id = '".$cat_id."' AND pim_articles_lang.lang_id='".DEFAULT_LANG_ID."'";

	if ($in['search'])
	{
		$filter.=" AND (pim_articles.item_code LIKE '%".$in['search']."%' OR pim_articles.internal_name LIKE '%".$in['search']."%')";
		// $arguments.="&search=".$in['search'];
	}
	if ($in['hide_article_ids'])
	{
		$filter.=" AND pim_articles.article_id  not in (".$in['hide_article_ids'].")";
		// $arguments.="&hide_article_ids=".$in['hide_article_ids'];
	}

	if ($in['show_stock'])
	{
		$filter.=" AND pim_articles.hide_stock=0";
		// $arguments.="&show_stock=".$in['show_stock'];
	}
	if ($in['from_customer_id'])
	{
		$filter.=" AND  dispatch_stock.customer_id=".$in['from_customer_id'];
		// $arguments.="&from_customer_id=".$in['from_customer_id'];
	}
	if ($in['from_address_id'])
	{
		$filter.=" AND  dispatch_stock.address_id=".$in['from_address_id'];
		// $arguments.="&from_address_id=".$in['from_address_id'];
	}


	$articles= array( 'lines' => array());
	// $articles['max_rows']= $db->field("SELECT count( DISTINCT pim_articles.article_id) FROM $table WHERE $filter AND pim_articles.active='1' ");

	$article = $db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1' ORDER BY pim_articles.item_code LIMIT 5");

	$fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='QUOTE_FIELD_LABEL'");

	$time = time();

	$j=0;
	while($article->next()){
		$vat = $db->field("SELECT value FROM vats WHERE vat_id='".$article->f('vat_id')."'");



		$values = $article->next_array();
		$tags = array_map(function($field){
			return '/\[\!'.strtoupper($field).'\!\]/';
		},array_keys($values));

		$label = preg_replace($tags, $values, $fieldFormat);

		if($article->f('price_type')==1){

			$price_value_custom_fam=$db->field("SELECT value FROM  fam_custom_price WHERE fam_id='".$article->f('article_category_id')."' AND category_id='".$cat_id."'");

			$pim_article_price_category_custom=$db->field("SELECT id from pim_article_price_category_custom WHERE article_id='".$article->f('article_id')."' and category_id='".$cat_id."' ");

			if($price_value_custom_fam==NULL || $pim_article_price_category_custom){
				$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id='".$cat_id."'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");

			}else{
				$price_value=$price_value_custom_fam;

				//we have to apply to the base price the category spec
				$cat_price_type=$db->field("SELECT price_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
				$cat_type=$db->field("SELECT type FROM pim_article_price_category WHERE category_id='".$cat_id."'");
				$price_value_type=$db->field("SELECT price_value_type FROM pim_article_price_category WHERE category_id='".$cat_id."'");

				if($cat_price_type==2){
					$article_base_price=get_article_calc_price($article->f('article_id'),3);
				}else{
					$article_base_price=get_article_calc_price($article->f('article_id'),1);
				}

				switch ($cat_type) {
					case 1:                  //discount
						if($price_value_type==1){  // %
							$price = $article_base_price - $price_value * $article_base_price / 100;
						}else{ //fix
							$price = $article_base_price - $price_value;
						}
						break;
					case 2:                 //profit margin
						if($price_value_type==1){  // %
							$price = $article_base_price + $price_value * $article_base_price / 100;
						}else{ //fix
							$price =$article_base_price + $price_value;
						}
						break;
				}
			}

			if(!$price || $article->f('block_discount')==1 ){
				$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
			}
		}else{
			$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.from_q='1'  AND  pim_article_prices.article_id='".$article->f('article_id')."'");
			if(!$price || $article->f('block_discount')==1 ){
				$price=$db->field("SELECT pim_article_prices.price FROM pim_article_prices WHERE pim_article_prices.price_category_id=0  AND  pim_article_prices.article_id='".$article->f('article_id')."' AND base_price=1");
			}
		}

		$pending_articles=$db->field("SELECT SUM(pim_order_articles.quantity) as pending_articles FROM  pim_order_articles WHERE  pim_order_articles.article_id='".$article->f('article_id')."' AND delivered=0");
		$base_price = $db->field("SELECT price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND base_price='1' ");

		$start= mktime(0, 0, 0);
		$end= mktime(23, 59, 59);
		$promo_price=$db->query("SELECT price,use_price_categ FROM promotions WHERE article_id='".$article->f('article_id')."' AND (promotions.date_start <='".$end."' and promotions.date_end >='".$start."' ) ");
		if($promo_price->move_next()){
			if($promo_price->f('use_price_categ') && $promo_price->f('price')>$price){

			}else{
				$price=$promo_price->f('price');
				$base_price = $price;
			}
		}
		if($in['buyer_id']){

			$customer_custom_article_price=$db->query("SELECT * FROM customer_custom_article_price WHERE article_id='".$article->f('article_id')."' AND customer_id='".$in['buyer_id']."'");
			if($customer_custom_article_price->move_next()){

				$price = $customer_custom_article_price->f('price');

				$base_price = $price;
			}

		}

		$purchase_price = $db->field("SELECT purchase_price FROM pim_article_prices WHERE article_id='".$article->f('article_id')."' AND pim_article_prices.base_price='1'");

		$linie = array(
			'article_id'				=> $article->f('article_id'),
			'checked'					=> $article->f('article_id')==$in['article_id']? 'checked="checked"':'',
			'name'						=> $article->f('internal_name'),
			'name2'						=> $article->f('item_name') ? htmlspecialchars(html_entity_decode($article->f('item_name'))) : htmlspecialchars(html_entity_decode($article->f('item_name'))),
			'stock'						=> $article->f('stock'),
			'stock2'					=> remove_zero_decimals($article->f('stock')),
			'quantity'		    		=> 1,
			'pending_articles'  		=> intval($pending_articles),
			'threshold_value'   		=> $article->f('article_threshold_value'),
			'sale_unit'					=> $article->f('sale_unit'),
			'percent'           		=> $vat_percent,
			'percent_x'         		=> display_number($vat_percent),
			'packing'					=> remove_zero_decimals($article->f('packing')),
			'code'		  	    		=> $article->f('item_code'),
			'price'						=> $article->f('is_service') == 1 ? $article->f('unit_price') : $price,
			'price_vat'					=> $in['remove_vat'] == 1 ? $price : $price + (($price*$vat)/100),
			'vat_value'					=> $in['remove_vat'] == 1 ? 0 : ($price*$vat)/100,
			'purchase_price'			=> $purchase_price,
			'vat'			    		=> $in['remove_vat'] == 1 ? '0' : $vat,
			'quoteformat'    			=> html_entity_decode(gfn($label)),
			'base_price'				=> $article->f('is_service') == 1 ? place_currency(display_number_var_dec($article->f('unit_price'))) : place_currency(display_number_var_dec($base_price)),
			'show_stock'				=> $article->f('hide_stock') ? false:true,
			'hide_stock'				=> $article->f('hide_stock'),
			'is_service'				=> $article->f('is_service'),
		);
		array_push($articles['lines'], $linie);

	}

	$articles['buyer_id'] 		= $in['buyer_id'];
	$articles['lang_id'] 				= $in['lang_id'];
	$articles['cat_id'] 				= $in['cat_id'];
	$articles['txt_name']			  = $text;
	$articles['allow_stock']		= ALLOW_STOCK == 1 ? true : false;

	array_push($articles['lines']);

	return $articles;
}


