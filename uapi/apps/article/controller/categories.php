<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$db = new sqldb();

$db->query("SELECT * FROM pim_article_categories ORDER BY name ");
$result['categories'] = array();
while ($db->move_next()) {
    $families = array(
        'name' => $db->f('name'),
        'id' => $db->f('id')
    );

    array_push($result['categories'], $families);
}
json_out($result);