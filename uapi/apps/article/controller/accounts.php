<?php

error_reporting(0);
if (!defined('INSTALLPATH')) {
	define('INSTALLPATH', '../../../');
}
if (!defined('BASEPATH')) {
	define('BASEPATH', INSTALLPATH . 'core/');
}
session_start();

$is_admin = strtolower($in["is_admin"]);


include_once (INSTALLPATH . 'config/config.php');
include_once (BASEPATH . '/library/sqldb.php');
include_once (BASEPATH . '/library/sqldbResult.php');
include_once (INSTALLPATH . 'config/database.php');
include_once (BASEPATH . 'startup.php');

ini_set('display_errors', 1);
  ini_set('error_reporting', 1);
  // error_reporting(E_ALL);
  error_reporting(E_ALL ^ E_NOTICE);


$q = strtolower($in["term"]);
global $database_config;
$database_users = array(
	'hostname' => $database_config['mysql']['hostname'],
	'username' => $database_config['mysql']['username'],
	'password' => $database_config['mysql']['password'],
	'database' => $database_config['user_db'],
);
$db_user = new sqldb($database_users);
$filter =" AND is_admin='0' ";

if($q){
	$filter .=" AND customers.name LIKE '%".addslashes($q)."%'";
}

if($is_admin){
	$filter =" AND is_admin='".$is_admin."' ";
}
if($in['supplier']){
	$filter .= " AND customers.is_supplier='1'";
}

if($in['current_id']){
	$filter .= " AND customers.customer_id !='".$in['current_id']."'";
}
$admin_licence = $db_user->field("SELECT user_type FROM users WHERE user_id = '".$_SESSION['u_id']."' ");
if($admin_licence != '3' && ONLY_IF_ACC_MANAG == '1'){
	$filter.= " AND CONCAT( ',', customers.user_id,  ',' ) LIKE  '%,".$_SESSION['u_id'].",%' ";
}
$db= new sqldb();

$cust = $db->query("SELECT customers.name, customers.customer_id, customers.active, our_reference, currency_id, internal_language, identity_id, 
					acc_manager_name, customers.country_name
					FROM customers 
					WHERE customers.active=1 $filter GROUP BY customers.customer_id ORDER BY customers.name limit 5 ")->getAll();

$result = array();
foreach ($cust as $key => $value) {
	$cname = trim($value['name']);

	$result[]=array(
		"id"					=> $value['customer_id'],
		"label"					=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',$cname),
		"value" 				=> preg_replace(array('/\r/', '/\n/','/\t/'), ' ',strip_tags($cname)),
		"ref" 					=> $value['our_reference'],
		"currency_id"			=> $value['currency_id'],
		"lang_id" 				=> $value['internal_language'],
		"identity_id" 			=> $value['identity_id'],
		'contact_name'			=> $value['acc_manager_name'],
		'country'				=> $value['country_name']
	);

}


if($in['from_addArticle_page']===true){
	return json_out($result,true,false);
}
json_out($result);

?>