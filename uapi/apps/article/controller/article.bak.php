<?php

$article = array();

// $article['article_brand'] 			= build_article_brand_dd($in['article_brand_id']);
$article['ledger_account'] 	= build_article_ledger_account_dd($in['ledger_account_id']);
$article['article_category'] 		= build_article_category_dd($in['article_category_id']);
$article['supplier']				= get_supplier($in,true,false);
$article['is_admin']				= $_SESSION['access_level'] == 1 ? true : false;

$article['article_brand'] 		= build_article_brand_dd($in['article_brand_id']);
$article['article_ledger_account'] 	= build_article_ledger_account_dd($in['ledger_account_id']);
$article['allow_article_packing'] 		= ALLOW_ARTICLE_PACKING ?true:false;
$article['allow_article_sale_unit'] 	= ALLOW_ARTICLE_SALE_UNIT?true:false;
$article['allow_stock'] 		= ALLOW_STOCK ?true:false;
$article['adv_product'] 	= ADV_PRODUCT?true:false;
$article['vat_dd']					= build_vat_dd();
$article['vat_id']	                = $db->field("SELECT vat_id FROM vats WHERE value='".ACCOUNT_VAT."'");
$article['price_type']	            = $db->field("SELECT value FROM settings WHERE constant_name='PRICE_TYPE'");
$article['lang_code']               = $db->field("SELECT code FROM pim_lang WHERE sort_order='1'");
$article['is_aac']               = $db->field("SELECT value FROM settings WHERE constant_name='AAC'");
$article['is_auto_aac']               = $db->field("SELECT value FROM settings WHERE constant_name='AUTO_AAC'"
	);
$article['tab_class']='disabled';
json_out($article);

function get_supplier(&$in,$showin=true,$exit=true){
	$in['supplier'] = 1;
	$in['from_addArticle_page'] = true;
	$in['noAdd'] = 'false';
	// return false;
	return ark::run('quote-accounts');
}

?>