<?php

	if($in['xget']){
    $fname = 'get_'.$in['xget'];
    $fname($in,false);
	}

	function get_article_data($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'data_article'=>array());
		$is_backup_data = $db->field("SELECT value FROM settings WHERE constant_name='ART_DATA_BACKEDUP'");

		$data['data_article']				= $is_backup_data == 1 ? true : false;

		return json_out($data, $showin,$exit);
	}
	function get_setarticle($in,$showin=true,$exit=true){
		$db = new sqldb();
		$data = array( 'setarticles'=>array());
		$ALLOW_ARTICLE_PACKING = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_PACKING'");
		$ALLOW_ARTICLE_SALE_UNIT = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_ARTICLE_SALE_UNIT'");
		$ALLOW_QUOTE_PACKING = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_PACKING'");
		$ALLOW_QUOTE_SALE_UNIT = $db->field("SELECT value FROM settings WHERE constant_name='ALLOW_QUOTE_SALE_UNIT'");
		$SHOW_TOTAL_MARGIN_QUOTE = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_TOTAL_MARGIN_QUOTE'");
		$WAC = $db->field("SELECT value FROM settings WHERE constant_name='WAC'");
		$ARTICLE_PRICE_COMMA_DIGITS = $db->field("SELECT value FROM settings WHERE constant_name='ARTICLE_PRICE_COMMA_DIGITS'");
		$AAC = $db->field("SELECT value FROM settings WHERE constant_name='AAC'");
		$AUTO_AAC = $db->field("SELECT value FROM settings WHERE constant_name='AUTO_AAC'");

		$data['setarticles'] = array(
			'ALLOW_ARTICLE_PACKING'	 				=> $ALLOW_ARTICLE_PACKING == 1 ? true : false,
			'ALLOW_ARTICLE_SALE_UNIT'	 			=> $ALLOW_ARTICLE_SALE_UNIT == 1 ? true : false,
			'ALLOW_QUOTE_PACKING'	 				=> $ALLOW_QUOTE_PACKING == 1 ? true : false,
			'ALLOW_QUOTE_SALE_UNIT'	 				=> $ALLOW_QUOTE_SALE_UNIT == 1 ? true : false,
			'SHOW_TOTAL_MARGIN_QUOTE'	 			=> $SHOW_TOTAL_MARGIN_QUOTE == 1 ? true : false,
			'ARTICLE_PRICE_COMMA_DIGITS'			=> $ARTICLE_PRICE_COMMA_DIGITS,
			'WAC'	 								=> $WAC == 1 ? true : false,
			'AAC'	 								=> $AAC == 1 ? true : false,
			'AUTO_AAC'	 							=> $AUTO_AAC == 1 ? true : false,
			'do'									=> 'article-settings-article-article_settings',
			'xget'									=> 'setarticle',

		);
		return json_out($data, $showin,$exit);

	}

	function get_pricecateg($in,$showin=true,$exit=true){
		$db = new sqldb();
		$db2 = new sqldb();
/*		$data = array( 'pricescateg'=>array());*/
		$PRICE_TYPE = $db->field("SELECT value FROM settings WHERE constant_name='PRICE_TYPE'");
		$USE_FAM_CUSTOM_PRICE = $db->field("SELECT value FROM settings WHERE constant_name='USE_FAM_CUSTOM_PRICE'");
		$data['pricescateg'] = array(
			'PRICE_TYPE'	 						=> $PRICE_TYPE,
			'USE_FAM_CUSTOM_PRICE'					=> $USE_FAM_CUSTOM_PRICE  == 1 ? true : false,
			'do'									=> 'article-settings-article-use_fam_price',
			'xget'									=> 'pricecateg',
		);
		$data['price_search'] = array(
			'search' 				=>array( 'search'=> $in['search'], 'do'=>'article-settings', 'xget'=>'pricecateg' ),
            'pricelist'				=> array(),
		);
		$l_r =ROW_PER_PAGE;
		if(($in['ofs']) || (is_numeric($in['ofs'])))
		{
			$in['offset']=$in['ofs'];
		}
		$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
			$offset=0;
		}
		else
		{
			$offset=$in['offset'];
		}
		$filter = "1=1";
		if ($in['search'])
		{
			$filter .= " AND name LIKE '%".$in['search']."%' ";
		}
		$order_by = " ORDER BY name ";
		if($in['order_by']){
			$order = " ASC ";
			if($in['desc']){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
			$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];

		}
		$db->query("SELECT pim_article_price_category.* FROM pim_article_price_category WHERE $filter $order_by");
		$use_fam_custom_price = $db2->field("SELECT value FROM settings WHERE constant_name='USE_FAM_CUSTOM_PRICE'");
		$max_rows=$db->records_count();
		$db->move_to($offset*$l_r);
		$j=0;
		$i = 0;
		while($db->move_next() && $j<$l_r){
			$categ_dd = build_price_category_type($db->gf('type'));
			$price_type_dd = build_price_type($db->gf('price_type'));
			$price_value_dd = build_price_value_type_list($db->gf('price_value_type'));	
			$data['price_search']['pricelist'][$i]	= array(
				'name'					=> $db->f('name'),
				'type'	        		=> get_price_category_type($db->f('type')),
				'price_type'	        => get_price_type($db->f('price_type')),
				'price_value'			=> display_number($db->f('price_value')),
				'price_value_type'		=> get_price_value_type($db->f('price_value_type')),
				'id'			        => $db->f('category_id'),
				'category_id'			        => $db->f('category_id'),
				'default_category'      => $db->f('default_category'),
				'not_default'			=> $db->f('default_category')?0:1,
				'page_title' 			=> 	gm('Add Article Price Category'),
		    	'price_type_dd'			=> 	$price_type_dd,
		    	'price_type_id'			=> 	$db->gf('price_type') ? $db->gf('price_type') : (string)$price_type_dd[0]['id'],
		    	'category_type_dd'		=>	$categ_dd,
		    	'category_type_id'		=>	$db->gf('type') ? $db->gf('type') : (string)$categ_dd[0]['id'],
		    	'price_value_type_dd'	=>  $price_value_dd,
		    	'price_value_type_id'	=>  $db->gf('price_value_type') ? $db->gf('price_value_type') : (string)$price_value_dd[0]['id'],
		    	'price_value'			=>	display_number($db->gf('price_value')),
		    	'name'					=> 	$db->gf('name'),
		    	'is_regular'			=>	true,
		    	'show'					=>	false,
		    	'him'					=>  true,
		    	'use_fam_custom_price'	=> 	$use_fam_custom_price  == 1 ? true : false,
		    	'do'					=> 	'article-settings',
		    	'xget'					=> 	'pricecateg',
				'add_price'				=> array(),

		    );
			$j++;
			$i++;
		}

		if(!$in['category_id']){
			$categ_dd = build_price_category_type($in['type']);
			$price_type_dd = build_price_type($in['price_type']);
			$price_value_dd = build_price_value_type_list($in['price_value_type']);
		    $data['price_search']['pricelist']['add_price'][] = array(
		    		'page_title' 			=> 	gm('Add Article Price Category'),
		    		'price_type_dd'			=> 	$price_type_dd,
		    		'price_type_id'			=> 	$in['price_type'] ? $in['price_type'] : (string)$price_type_dd[0]['id'],
		    		'category_type_dd'		=>	$categ_dd,
		    		'category_type_id'		=>	$in['type'] ? $in['type'] : (string)$categ_dd[0]['id'],
		    		'price_value_type_dd'	=>  $price_value_dd,
		    		'price_value_type_id'	=>  $in['price_value_type'] ? $in['prive_value_type'] : (string)$price_value_dd[0]['id'],
		    		'price_value'			=>	display_number($in['price_value']),
		    		'name'					=> 	$in['name'],
		    		'show'					=>	true,
		    		'him'					=>  false,
		    		'is_regular'			=>	true,
		    		'do'					=> 	'article-settings',
		    		'xget'					=> 	'pricecateg',		    		
		    );
			
		}


		$l_r =ROW_PER_PAGE;
		if(($in['ofs']) || (is_numeric($in['ofs'])))
		{
			$in['offset']=$in['ofs'];
		}
		$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
			$offset=0;
		}
		else
		{
			$offset=$in['offset'];
		}

		$filter = "1=1";
		$order_by = " ORDER BY `from_q` ";
		if($in['order_by']){
			$order = " ASC ";
			if($in['desc']){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
			$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
			$view_list->assign(array(
				'on_'.$in['order_by'] 	=> $in['desc'] ? 'on_asc' : 'on_desc',
			));
		}
		$db->query("SELECT article_price_volume_discount.* FROM article_price_volume_discount WHERE $filter $order_by");
		$max_rows=$db->records_count();
		$db->move_to($offset*$l_r);
		$z=0;
		while($db->move_next() && $z<$l_r){
			$data['volume'][]  =  array(
						'from_q'			=> $db->f('from_q'),
					    'to_q'				=> $db->f('to_q')!=0?$db->f('to_q'):'&#8734;',
						'percent'			=> display_number($db->f('percent')),
						'id'			    => $db->f('id'),
						'show'					=>	false,
		    			'him'					=>  true,
					
		              );
						
			$z++;
		}
			$data['add_volume'][]  =  array(
				'title'				=> gm("Add Volume Discount"),
				'from_q'			=> $in['from_q'],
				'to_q'				=> $in['to_q'],
				'percent'			=> '0,00',
				'id'			    => $in['id'],
				'show'					=>	true,
		    	'him'					=>  false,
			);


		return json_out($data, $showin,$exit);
	};

	function get_family($in,$showin=true,$exit=true){
		$db = new sqldb();
		$db2 = new sqldb();
		$filter = "1=1";
		$l_r =15;
		
		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}

		$order_by = " ORDER BY sort_order ASC";
		if($in['order_by']){
			$order = " ASC ";
			if($in['desc']){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
			$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
			$view_list->assign(array(
				'on_'.$in['order_by'] 	=> $in['desc'] ? 'on_asc' : 'on_desc',
			));
		}

		if($in['category_id']){
		  $arguments.="&category_id=".$in['category_id'];
		  

		  $price_value=$db->field("SELECT pim_article_price_category.price_value FROM pim_article_price_category WHERE pim_article_price_category.category_id='".$in['category_id']."'");
		}
		$max_rows=$db->field("SELECT count(pim_article_categories.id) FROM pim_article_categories WHERE $filter $order_by ");
		$db->query("SELECT pim_article_categories.* FROM pim_article_categories WHERE $filter $order_by  LIMIT ".$offset*$l_r.",".$l_r );
		
		$j=0;
		$price_value_line=0;
		while($db->next()){

			$price_value_line=$db2->field("SELECT  fam_custom_price.value FROM fam_custom_price WHERE fam_custom_price.category_id='".$in['category_id']."' AND fam_id='".$db->f('id')."'"); 
			$data['fam'][] = array( 
						'name'			=> $db->f('name'),
						'fam_id'		=> $db->f('id'),
						'fam_price'     => $price_value_line!=NULL ? display_number($price_value_line) : display_number($price_value),
						'category_id'	=> $in['category_id'],
		              );
						
			$j++;

		}
		$data['lr'] = $l_r;
		$data['max_rows'] = $max_rows;
		
		return json_out($data, $showin,$exit);
	};

	function get_tax($in,$showin=true,$exit=true){
		$db = new sqldb();
		$db2 = new sqldb();
		$db3 = new sqldb();
		$data['tax_search'] = array(
			'search' 				=>array( 'search'=> $in['search'], 'do'=>'article-settings', 'xget'=>'tax' ),
            'taxlist'				=> array(),
		);
		$l_r =ROW_PER_PAGE;

		if(($in['ofs']) || (is_numeric($in['ofs'])))
		{
			$in['offset']=$in['ofs'];
		}
		$spliter = substr(ACCOUNT_DATE_FORMAT, 1, 1);
		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
			$offset=0;
		}
		else
		{
			$offset=$in['offset'];
		}

		$filter = "1=1";
		if ($in['search'])
		{
			$filter .= " AND code LIKE '%".$in['search']."%' ";
		}
		$order_by = " ORDER BY code ";
		if($in['order_by']){
			$order = " ASC ";
			if($in['desc']){
				$order = " DESC ";
			}
			$order_by =" ORDER BY ".$in['order_by']." ".$order;
			$arguments.="&order_by=".$in['order_by']."&desc=".$in['desc'];
			$view_list->assign(array(
				'on_'.$in['order_by'] 	=> $in['desc'] ? 'on_asc' : 'on_desc',
			));
		}
		$db->query("SELECT pim_article_tax.* FROM pim_article_tax WHERE $filter $order_by");
		$max_rows=$db->records_count();
		$db->move_to($offset*$l_r);
		$j=0;
		while($db->move_next() && $j<$l_r){
			$type=$db2->field("SELECT name FROM pim_article_tax_type WHERE id='".$db->f('type_id')."'");
			$data['tax_search']['taxlist']['taxes'][]		=   array(
						'code'	=> $db->f('code'),
						'type_name'	=> $type?$type:'-',
						'type'		=> $db->f('type_id'),
						'amount' => display_number_var_dec($db->f('amount')),
						'article_tax_type'	 	=> build_article_tax_type_dd($db->f('type_id')),
						'description' => $db->f('description'),
						'id'	=> $db->f('tax_id'),
						'show'					=>	false,
		    			'him'					=>  true,
		    			'title'					=> gm('Update Tax'),
		    			'title_list'			=> gm('Manage list'),
						'add_tax' => array(),
			);
			$j++;
		}
		$article_tax_type_dd = build_article_tax_type_dd($in['type_id']);
		$data['tax_search']['taxlist']['taxes']['add_tax'][]  =  array(
				'code'					=> $in['code'],
				'type'					=> $in['type'],
				'article_tax_type'	 	=> $article_tax_type_dd,
				'amount' 				=> $in['amount'],
				'description'			=> $in['description'],
				'id'					=> $in['id'],		
				'show'					=>	true,
		    	'him'					=>  false,
		    	'title'					=> gm('Add Tax'),
		    	'title_list'			=> gm('Manage list'),
		);

		$db3->query("SELECT name, id FROM pim_article_tax_type ORDER BY sort_order");
		while($db3->move_next()){
		$data['manage'][]    =  array(
			'id'		=>  $db3->f('id'),
			'name'		=>	$db3->f('name'),

			);
		}

		return json_out($data, $showin,$exit);
	};
	function get_serial($in,$showin=true,$exit=true){
		$db = new sqldb();
		$db2 = new sqldb();
		$data = array( 'numbers'=>array());
		$SHOW_ART_S_N_INVOICE_PDF = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_ART_S_N_INVOICE_PDF'");
		$SHOW_ART_S_N_ORDER_PDF = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_ART_S_N_ORDER_PDF'");

		$data['numbers']  =  array(
			'SHOW_ART_S_N_INVOICE_PDF'		=>    $SHOW_ART_S_N_INVOICE_PDF == 1 ? true : false,
			'SHOW_ART_S_N_ORDER_PDF'		=>    $SHOW_ART_S_N_ORDER_PDF == 1 ? true : false,

			
		);
		return json_out($data, $showin,$exit);
	};
	function get_batches($in,$showin=true,$exit=true){
		$db = new sqldb();
		$db2 = new sqldb();
		$data = array( 'numbers'=>array());
		$SHOW_ART_B_N_ORDER_PDF = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_ART_B_N_ORDER_PDF'");
		$SHOW_ART_B_N_INVOICE_PDF = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_ART_B_N_INVOICE_PDF'");
		$UNIQUE_BATCH_NR = $db->field("SELECT value FROM settings WHERE constant_name='UNIQUE_BATCH_NR'");
		$SHOW_ART_B_ED_ORDER_PDF = $db->field("SELECT value FROM settings WHERE constant_name='SHOW_ART_B_ED_ORDER_PDF'");

		$data['batch']  =  array(
			'SHOW_ART_B_N_ORDER_PDF'		=>    $SHOW_ART_B_N_ORDER_PDF == 1 ? true : false,
			'SHOW_ART_B_N_INVOICE_PDF'		=>    $SHOW_ART_B_N_INVOICE_PDF == 1 ? true : false,
			'UNIQUE_BATCH_NR'				=>    $UNIQUE_BATCH_NR == 1 ? true : false,
			'SHOW_ART_B_ED_ORDER_PDF'		=>    $SHOW_ART_B_ED_ORDER_PDF == 1 ? true : false,
		);
		return json_out($data, $showin,$exit);
	};

	function get_ledger($in,$showin=true,$exit=true){
		$db = new sqldb();
		$db2 = new sqldb();
		/*		$data = array('ledge'=>array());*/
		
		$l_r = ROW_PER_PAGE;
		
		global $cfg;

		if((!$in['offset']) || (!is_numeric($in['offset'])))
		{
		    $offset=0;
		    $in['offset']=1;
		}
		else
		{
		    $offset=$in['offset']-1;
		}
		$data['ledge'] = array(
			'search' 				=>array( 'search'=> $in['search'], 'do'=>'article-settings', 'xget'=>'ledger','offset'=> $in['offset'] ),
            'ledgelist'				=> array(),
		);

		$filter = "1=1 ";
		$max_rows=$db->field("SELECT count(pim_article_categories.id) 
			        FROM  pim_article_categories 
			        LEFT JOIN fam_ledger ON fam_ledger.fam_id=pim_article_categories.id
			        ");
		$db->query("SELECT pim_article_categories.*, fam_ledger.ledger_val 
			        FROM  pim_article_categories 
			        LEFT JOIN fam_ledger ON fam_ledger.fam_id=pim_article_categories.id
			        GROUP BY pim_article_categories.id order by pim_article_categories.sort_order
			         LIMIT ".$offset*$l_r.",".$l_r );
		
		$j=0;
		while($db->next()){
			$data['ledge']['ledgelist'][]  =  array(
						'name'	    			=> $db->f('name'),
						'val'	    			=> $db->f('ledger_val'),
						'fam_id'        		=> $db->f('id'),
						'check_add_to_article'	=> $_SESSION['add_to_article'][$db->f('id')] == 1 ? true : false,
						'ids'					=>   array('name'=>'add_fam['.$db->f('id').']' , 'value' => '1'),
						
		    );
			$j++;
		}

		$data['ledge']['lr'] = $l_r;
		$data['ledge']['max_rows'] = $max_rows;

		return json_out($data, $showin,$exit);
	};
	function get_restore($in,$showin=true,$exit=true){
	$db = new sqldb();
	set_time_limit(0);
    ini_set('memory_limit', '-1');   
		$db = new sqldb();
		$db2 = new sqldb();
		$in['article'] = '0';
		$in['article2'] = '0';
		$file1=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/catalog/backup1.xml';
		$file2=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/catalog/backup2.xml';
		$file3=INSTALLPATH.UPLOAD_PATH.DATABASE_NAME.'/catalog/backup3.xml';
		$file_fill1 = file_get_contents($file1);
		$file_fill2 = file_get_contents($file2);
		$file_fill3 = file_get_contents($file3);

		if($file_fill1==true&&$file_fill2==true&&$file_fill3==true){
			$show_file=1;
		}
		$data = array('restore'=>array(
			'file'				=>    backuparticle2($in['article']),
			'filebackup'		=>    backuparticle3($in['article']),
			'file_id'			=>	  $in['article'],
			'file2_id'			=>	  $in['article2'],
			'show_file'			=>    $show_file==1?true:false,
			'class1'			=> 	  $class1,
			'class2'			=> 	  $class2,
			'class3'			=> 	  $class3,
			'history'			=>    array(),
		));


	return json_out($data, $showin,$exit);
};

	$result = array(
		'setarticle'				=> get_setarticle($in,true,false),
	);
json_out($result);
?>





