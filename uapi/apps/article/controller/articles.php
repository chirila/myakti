<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$db = new sqldb();
$db2 = new sqldb();

$l_r = ROW_PER_PAGE;
$order_by = " ORDER BY  pim_articles.item_code  ";

$order_by_array = array('item_code', 'internal_name', 'article_category', 'price', 'customer_name');
if($in['page']){
    $in['offset'] = $in['page'];
}
if ((!$in['offset']) || (!is_numeric($in['offset']))) {
    $offset = 0;
    $in['offset'] = 1;
} else {
    $offset = $in['offset'] - 1;
}

//FILTER LIST
$selected = array();
$filter = '1=1';
$filter_attach = '';
$filter .= " AND pim_articles.is_service='0' ";

if ($in['archived'] == 0) {
    $filter .= " AND pim_articles.active='1' ";
} else {
    $filter .= " AND pim_articles.active='0' ";
    $arguments .= "&archived=" . $in['archived'];
    $arguments_a = "&archived=" . $in['archived'];
}


if (!empty($in['search'])) {
    $filter .= " AND ( pim_articles.item_code LIKE '%" . $in['search'] . "%'  OR pim_articles.internal_name LIKE '%" . $in['search'] . "%') ";
    $arguments_s .= "&search=" . $in['search'];
}
// supplier
if (!empty($in['supplier_name'])) {
    $filter .= " AND ( customers.name LIKE '%" . $in['supplier_name'] . "%') ";
    $arguments_s .= "&supplier_name=" . $in['supplier_name'];
}
if (!empty($in['supplier_reference'])) {
    $filter .= " AND ( pim_articles.supplier_reference LIKE '%" . $in['supplier_reference'] . "%') ";
    $arguments_s .= "&supplier_reference=" . $in['supplier_reference'];
}


if ($in['has_picture']) {
    if ($in['has_picture'] == 1) {
        // $filter_attach.= " INNER JOIN pim_article_photo ON pim_article_photo.parent_id=pim_articles.article_id ";
        $filter .= " AND pim_articles.parent_id <> 0";
    } else {
        // $filter_attach.= " LEFT JOIN pim_article_photo ON pim_article_photo.parent_id=pim_articles.article_id ";
        $filter .= " AND pim_articles.parent_id = 0";
    }
    $arguments .= "&has_picture=" . $in['has_picture'];
}

if ($in['category_id']) {
    $filter .= " AND pim_articles.article_category_id='" . $in['category_id'] . "' ";
    $arguments .= "&category_id=" . $in['category_id'];
}
if ($in['first'] && $in['first'] == '[0-9]') {
    $filter .= " AND SUBSTRING(LOWER(pim_articles.internal_name),1,1) BETWEEN '0' AND '9'";
    $arguments .= "&first=" . $in['first'];
} elseif ($in['first']) {
    $filter .= " AND pim_articles.internal_name LIKE '" . $in['first'] . "%'  ";
    $arguments .= "&first=" . $in['first'];
}

if ($in['order_by']) {
    if (in_array($in['order_by'], $order_by_array)) {
        $order = " ASC ";
        if ($in['desc'] == '1') {
            $order = " DESC ";
        }
        if ($in['order_by'] == 'customer_name') {
            $in['order_by'] = 'name';
        }
        $order_by = " ORDER BY " . $in['order_by'] . " " . $order;
        $arguments .= "&order_by=" . $in['order_by'] . "&desc=" . $in['desc'];
        /*$view_list->assign(array(
            'on_'.$in['order_by'] 	=> $in['desc'] ? 'on_asc' : 'on_desc',
        ));*/
    }
}


$arguments = $arguments . $arguments_s;
if ($in['order_by'] == 'name') {

} elseif ($in['supplier_name']) {

} else {
    $internal_name = $db->query("SELECT DISTINCT SUBSTR( internal_name, 1, 1 ) AS internal_name
            FROM pim_articles
			WHERE $filter
		     " . $order_by . "
			")->getAll();
}
foreach ($internal_name as $key => $value) {
    array_push($selected, $value['internal_name']);
}
$internal_name = null;
if ($in['order_by'] == 'name') {

} elseif ($in['supplier_name']) {

} else {
    $e = $db->query("SELECT pim_articles.article_id
            FROM pim_articles
			WHERE $filter
		     " . $order_by . "
			")->getAll();
}
$max_rows = count($e);
$_SESSION['articles_id'] = array();
foreach ($e as $key => $value) {
    array_push($_SESSION['articles_id'], $value['article_id']);
}
$e = null;


$result = array('products' => array(), 'results' => $max_rows);
if (!isset($in['lang_id'])) {
    $in['lang_id'] = $_SESSION['l_id'];
}

$articles = $db->query("SELECT pim_articles.use_batch_no,pim_articles.article_id,pim_articles.hide_stock,pim_articles.block_discount,pim_articles.article_threshold_value, pim_articles.stock, pim_articles.item_code,pim_articles.show_front,customers.name,
			                   pim_articles.internal_name,pim_article_prices.price, pim_article_prices.total_price,pim_articles.article_category,
			                   vats.value AS vat_value ,pim_articles_lang.description AS description,pim_articles.ean_code,pim_article_categories.name AS article_category_name

            FROM pim_articles
            LEFT JOIN customers ON pim_articles.supplier_id=customers.customer_id
            LEFT JOIN vats ON vats.vat_id=pim_articles.vat_id
            LEFT JOIN pim_article_categories ON pim_article_categories.id=pim_articles.article_category_id
            LEFT JOIN pim_articles_lang ON pim_articles_lang.item_id=pim_articles.article_id AND pim_articles_lang.lang_id='" . $in['lang_id'] . "'
			LEFT JOIN pim_article_prices ON pim_article_prices.article_id=pim_articles.article_id AND pim_article_prices.base_price='1'
			WHERE $filter
		   " . $order_by . "
			LIMIT " . $offset * $l_r . "," . $l_r . "
			");

$j = 0;
while ($db->move_next()) {
    $item = array(
        'id' => $db->f('article_id'),
        'use_batch_no' => $db->f('use_batch_no') ? true : false,
        'description' => $db->f('description'),
        'ean_code' => $db->f('ean_code'),
        'category_name' => $db->f('article_category_name'),
        'name' => $db->f('internal_name'),
        'supplier_name' => $db->f('name'),
        'article_category' => $db->f('article_category'),
        'article_id' => $db->f('article_id'),
        'stock' => remove_zero_decimals($db->f('stock')),
        'show_stock' => $db->f('hide_stock') ? false : true,
        'block_discount' => $db->f('block_discount') ? true : false,
        //'base_price'  		=> display_number_var_dec($db->f('price')),
        'item_code' => $db->f('item_code'),
        'price'  		=> display_number_var_dec($db->f('price')),
        'vat_value' => $db->f('vat_value'),
        'total_price' => 	display_number_var_dec($db->f('total_price')),
        'is_archived' => $in['archived'] ? true : false,
    );

    array_push($result['products'], $item);
    $j++;
}

$result['per_page'] = $l_r;

json_out($result);