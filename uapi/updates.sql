-- Akti Database


CREATE TABLE `customer_contact_auth` (
  `auth_id` bigint(20) NOT NULL,
  `auth_key`  VARCHAR(255),
  `contact_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `exp_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer_contact_auth`
--
ALTER TABLE `customer_contact_auth`
  ADD PRIMARY KEY (`auth_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer_contact_auth`
--
ALTER TABLE `customer_contact_auth`
  MODIFY `auth_id` bigint(20) NOT NULL AUTO_INCREMENT;