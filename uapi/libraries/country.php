<?php
class country{
	private static $sql;
	private static $country = array();

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author your's truly
	 **/
	function __construct(){}

	/**
	 * use this function to return the currency as you want
	 *
	 * @return multiple
	 * string - can return the value or the code
	 * array - return an array (used for example on drop-downs)
	 * @author me
	 **/
	static public function get_currency($selected,$code=''){
		if(empty(self::$country)){
			self::get();
		}
		if(!$selected){
			$selected = 1;
		}
		switch ($code) {
			case 'code': #code
				return self::$country[$selected]->code;
				break;
			case 'arrays': #code and value all
				$country_as_array = array();
				foreach (self::$country as $key => $value) {
					$country_as_array[$key] = $value->code.' '.$value->name;
				}
				return $country_as_array;
				break;
			case 'name': #code and value
				return self::$country[$selected]->code.' '.self::$country[$selected]->name;
				break;
			case 'array': #code
				$country_as_array = array();
				foreach (self::$country as $key => $value) {
					$country_as_array[$key] = $value->code;
				}
				return $country_as_array;
				break;
			case 'entity':
				return self::$country[$selected]->entity;
				break;
			default: #value
				return self::$country[$selected]->name;
				break;
		}
		// return self::$currency[$selected]->value;
	}

	static private function get(){
		$db = self::set_sql();
		$query = $db->query("SELECT * FROM country")->getAll();
		foreach ($query as $key => $value) {
			self::$country[$value['country_id']] = new stdClass();
			self::$country[$value['country_id']]->code = $value['code'];
			self::$country[$value['country_id']]->name = $value['name'];
			self::$country[$value['country_id']]->is_main = $value['is_main'];
		}		
	}

	/**
	 * use your own class to connect to the database
	 *
	 * @return object that contains the connection to the database
	 * @author me
	 **/
	static private function set_sql(){
		return self::$sql = new sqldb();
	}

}
?>