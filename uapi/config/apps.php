<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * --------------------------------------------------------------------------
 * Website Apps
 * --------------------------------------------------------------------------
 * Every app in the apps folder needs to be registered here, so the framework know where to look for it
 */

define("USE_APPS", true);
define("USE_ENVIRONMENT", false);

$apps = array();
$apps['auth'] = 1; # for auth we don't care
$apps['maintenance'] = 1;
$apps['account'] = 1;
$apps['invoice'] = 1;
$apps['quote'] = 1;
$apps['article'] = 1;
$apps['installation'] = 1;
$apps['order'] = 1;

$apps['misc'] = 1;



// $default_app = 'misc';
// $default_controller = 'home';

// $default_controller['front'] = 'home';
$auth_app = 'auth';
$auth_controller = 'login';

/*
$auth_app = 'auth';
$auth_controller = 'login';

$default_app['front'] = 'home';
$default_controller['front'] = 'home';

$default_app['admin'] = 'home';

*/

# google maps api key for project CRM maps

// AIzaSyCd5onUZQ4N_hZHPY5lbHb_2m8HWSs0sXY