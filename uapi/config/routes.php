<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * --------------------------------------------------------------------------
 * Website Routes
 * --------------------------------------------------------------------------
  */

$routes = array(

    'auth/login' => array(
        'method' => "POST",
        'do' => 'auth-login-auth-login',
        'params' => ''
    ),
    'auth/logout' => array(
        'method' => "POST",
        'do' => 'auth-login-auth-logout',
        'params' => ''
    ),
    'auth/reset' => array(
        'method' => "POST",
        'do' => 'auth-login-auth-reset',
        'params' => ''
    ),
    'auth/change' => array(
        'method' => "POST",
        'do' => 'auth-login-auth-change_password',
        'params' => ''
    ),


    'invoice/list' => array(
        'method' => "GET",
        'do' => 'invoice-invoices',
        'params' => ''
    ),

    'quote/list' => array(
        'method' => "GET",
        'do' => 'quote-quotes',
        'params' => ''
    ),

    'quote/add' => array(
        'method' => "POST",
        'do' => 'quote--quote-add',
        'params' => ''
    ),

    'quote/versions' => array(
        'method' => "GET",
        'do' => 'quote-versions',
        'params' => ''
    ),

    'intervention/list' => array(
        'method' => "GET",
        'do' => 'maintenance-services',
        'params' => ''
    ),

    'intervention/create' => array(
        'method' => "POST",
        'do' => 'maintenance--maintenance-addService',
        'params' => ''
    ),

    'intervention/userSelect' => array(
        'method' => "GET",
        'do' => 'maintenance-autoUsers',
        'params' => ''
    ),

    'intervention/planningUser' => array(
        'method' => "GET",
        'do' => 'maintenance-planningUser',
        'params' => ''
    ),

    'intervention/reschedule' => array(
        'method' => "POST",
        'do' => 'maintenance--maintenance-reschedule',
        'params' => ''
    ),

    'intervention/createDownpayment' => array(
        'method' => "POST",
        'do' => 'maintenance--maintenance-downpaymentInvoice',
        'params' => ''
    ),

    'account/countries' => array(
        'method' => "GET",
        'do' => 'account-countries',
        'params' => ''
    ),

    'account/customerfields' => array(
        'method' => "GET",
        'do' => 'account-customerfields',
        'params' => ''
    ),

    'account/contactfields' => array(
        'method' => "GET",
        'do' => 'account-contactfields',
        'params' => ''
    ),

    'account/titles' => array(
        'method' => "GET",
        'do' => 'account-titles',
        'params' => ''
    ),

    'account/sectors' => array(
        'method' => "GET",
        'do' => 'account-sectors',
        'params' => ''
    ),

    'account/details' => array(
        'method' => "GET",
        'do' => 'account-accountdetails',
        'params' => ''
    ),

    'account/create' => array(
        'method' => "POST",
        'do' => 'account--customer-add',
        'params' => ''
    ),

    'account/update' => array(
        'method' => "POST",
        'do' => 'account--customer-update',
        'params' => ''
    ),

    'account/createcontact' => array(
        'method' => "POST",
        'do' => 'account--customer-contact_add',
        'params' => ''
    ),

    'account/archivecontact' => array(
        'method' => "POST",
        'do' => 'account--customer-archive_contact',
        'params' => ''
    ),

    'account/activatecontact' => array(
        'method' => "POST",
        'do' => 'account--customer-archive_contact',
        'params' => ''
    ),

    'account/updatecontact' => array(
        'method' => "POST",
        'do' => 'account--customer-contact_update',
        'params' => ''
    ),

    'account/createaddress' => array(
        'method' => "POST",
        'do' => 'account--customer-address_add',
        'params' => ''
    ),

    'account/updateaddress' => array(
        'method' => "POST",
        'do' => 'account--customer-address_update',
        'params' => ''
    ),

    'account/deleteaddress' => array(
        'method' => "GET",
        'do' => 'account--customer-address_delete',
        'params' => ''
    ),

    'account/statusaddress' => array(
        'method' => "POST",
        'do' => 'account--customer-address_status_update',
        'params' => ''
    ),

    'account/addresslist' => array(
        'method' => "GET",
        'do' => 'account-addresses',
        'params' => ''
    ),

    'article/products' => array(
        'method' => "GET",
        'do' => 'article-articles',
        'params' => ''
    ),

    'article/product' => array(
        'method' => "GET",
        'do' => 'article-article',
        'params' => ''
    ),

    'article/categories' => array(
        'method' => "GET",
        'do' => 'article-categories',
        'params' => ''
    ),

    'article/brands' => array(
        'method' => "GET",
        'do' => 'article-brands',
        'params' => ''
    ),

    'article/services' => array(
        'method' => "GET",
        'do' => 'article-aservices',
        'params' => ''
    ),
    
     'activity/add' => array(
        'method' => "POST",
        'do' => 'misc--customers-add_activity',
        'params' => ''
    ),

    'installation/list' => array(
        'method' => "GET",
        'do' => 'installation-installations',
        'params' => ''
    ),

    'order/list' => array(
        'method' => "GET",
        'do' => 'order-orders',
        'params' => ''
    ),
	
	'order/create' => array(
        'method' => "POST",
        'do' => 'order--order-add_order',
        'params' => ''
    ),

    'order/updatePayment' => array(
        'method' => "POST",
        'do' => 'order--order-pay',
        'params' => ''
    ),

);

if(INSTALLPATH == '../'){
    $base = $config['install_path'].ENVIRONMENT.'/';
} else {
    $base = $config['install_path'];
}

$router = new arkRouter($routes,$base);

/*
$router->add('myaccount', 'GET', 'home-my_account', 'user_id');
$router->add('user', 'GET', 'home-user', '/user_id/test/');
*/

$router->run();