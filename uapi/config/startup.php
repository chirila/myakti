<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * --------------------------------------------------------------------------
 * STARTUP
 * --------------------------------------------------------------------------
 * This file keeps a list of libraries which should be loaded by default.
 */

/**
 * ------------------------------------------------------------------------
 * Libraries
 * ------------------------------------------------------------------------
 * Specify the list of libraries you'd like the system to load. Please remember that
 * This is meant as a loading mechanism for system wide libraries and not application
 * specific.
 * All libraries should be located in the Libraries folder or in a subfolder of the Libraries folder
 *
 */

$load['libraries'] = array('json','PHPMailerAutoload','currency','sync','tips','controller', 'curls','dropbox-sdk/Dropbox/autoload','dropbox_api','ftp_connection');


/* End of file startup.php */