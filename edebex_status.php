<?php
define('INSTALLPATH',__DIR__.'/');
define('LAYOUT',__DIR__.'/layout/');
define('BASEPATH', INSTALLPATH.'core/');
define('APPSPATH', INSTALLPATH.'apps/');

define('AT_CACHE', 'cache/');

/************************************************************************
* @Author: Tinu Coman
***********************************************************************/
ini_set('display_errors', 1);
ini_set('error_reporting', 1);
error_reporting(E_ALL ^ E_NOTICE);
ini_set('error_log','cron_project.log');
set_time_limit(0);
include_once(__DIR__."/core/startup.php");
global $config,$database_config;
$db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
); 
$db_users= new sqldb($db_config);
$response = json_decode( @file_get_contents( "php://input" ), true );

$db_users->query("INSERT INTO edebex_log SET response='".serialize($response)."', date='".time()."', type='1' ");


?>