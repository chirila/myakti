<?php

$startTime = microtime(true);

define('INSTALLPATH','');
define('ENVIRONMENT','front');
define('REAL_PATH', __DIR__);
define('LAYOUT',INSTALLPATH.'layout/');
define('BASEPATH', INSTALLPATH.'core/');
define('APPSPATH', INSTALLPATH.'apps/');

define('AT_CACHE', 'cache/');
include_once(BASEPATH.'startup.php');

require(BASEPATH.'engine.php');

/* End of file index.php */

