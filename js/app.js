var app_titles={
    'customers':'Accounts',
    'customerView':'Account',
    'contacts':'Contacts',
    'customer':'Account',
    'contact':'Contact',
    'contactView':'Contact',
    'smartList':'Smart Lists',
    'smartListView':'Smart Lists',
    'smartListEdit':'Smart Lists',
    'import_accounts':'Import Accounts',
    'customersSettings':'Account Module',
    'deals':'Deals',
    'dealView':'Deal',
    'dealsSettings':'Deals Settings',
    'quotes':'Quotes',
    'quotesTemplates':'Quote Models',
    'quote.editTemplate':'Quote Models',
    'quote.edit':'Quote',
    'quote.view':'Quote',
    'quoteSettings':'Quote Module',
    'contracts':'Contracts',
    'contractTemplates':'Model Contracts',
    'contract.editTemplate':'Model Contracts',
    'contract.edit':'Contract',
    'contract.view':'Contract',
    'contractSettings':'Contract Module',
    'articles':'Products',
    'aservices':'Services',
    'article.add':'',
    'import_articles':'Import Products',
    'import_prices':'Import of prices',
    'articlesSettings':'Article Module',
    'orders':'Sales Orders',
    'order.view':'Order',
    'order.edit':'Order',
    'order_deliveries':'Deliveries',
    'orders_deliver':'To be delivered',
    'orderSettings':'Order Module',
    'purchase_orders':'Purchase Orders',
    'purchase_order.view':'Purchase Order',
    'purchase_order.edit':'Purchase Order',
    'purchase_order_deliveries':'Deliveries',
    'stock_low':'Products Low in Stock',
    'to_order':'Bulk Orders - To be Ordered',
    'purchaseOrderSettings':'Purchase Order Module',
    'services':'Intervention',
    'service.add':'Intervention',
    'service.edit':'Intervention',
    'service.view':'Intervention',
    'rec_services':'Recurring Interventions',
    'rec_service.add':'Recurring intervention',
    'rec_service.edit':'Recurring intervention',
    'planning':'Planning',
    'teams':'Teams',
    'team.edit':'Teams',
    'serviceModels':'Model Interventions',
    'service.editModel':'Model Interventions',
    'service.addModel':'Model Interventions',
    'serviceSettings':'Interventions Settings',
    'installations':'Installations',
    'installation.edit':'Installation',
    'installation.view':'',
    'install_fields':'Installation Fields',
    'install_templates':'Installation Templates',
    'install_template.edit':'Installation Templates',
    'installationSettings':'Installations Settings',
    'stock_info':'Transaction Based',
    'stock_article':'Article Based',
    'stock_customer':'Customer/Location Based',
    'stock_date':'Stock by Date',
    'stockSettings':'Stock Settings',
    'stock_dispatching':'Stock Dispatching ',
    'stock_dispatch.edit':'Stock Dispatch',
    'stock_dispatch.view':'Stock Dispatch',
    'invoices':'Invoices',
    'invoice.edit':'',
    'invoice.view':'',
    'reminders':'Reminders',
    'recinvoices':'Recurring Invoices',
    'recinvoice.view':'Recurring Invoice',
    'recinvoice.edit':'Recurring Invoice',
    'sepa':'SEPA',
    'payments':'Payments',
    'import_invoices':'Import Invoices',
    'invoiceSettings':'Invoice Module',
    'purchase_invoices':'Purchase Invoices',
    'purchase_invoice.edit':'Purchase Invoice',
    'purchase_invoice.view':'Purchase Invoice',
    'export_apps':'Export',
    'ptimesheets':'Timesheets',
    'timetracker_tasks':'Timesheets',
    'projects':'Projects',
    'project.edit':'Project',
    'project_invoice':'Billable',
    'project_deliver':'To Deliver',
    'projectSettings':'Project Module',
    'project.report':'Projects',
    'report_catalogue':'Insights',
    'report_catalogue_invoices':'Insights',
    'report_quotes':'Insights',
    'report_orders':'Insights',
    'report_p_orders':'Insights',
    'report_projects':'Insights',
    'report_invoices':'Insights',
    'report_client_history':'Insights',
    'settings':'Settings',
    'myActivities':'My activities',
    'calendar':'Calendar',
    'timesheet':'Timesheet',
    'serviceTs':'Interventions',
    'myaccount':'My Preference'
};

var page_title_default=angular.element("title")[0].text;

var app=angular.module('akti', ['ui.router', 'ngAnimate', 'ui.bootstrap', 'selectize', 'ngSanitize', 'angular-confirm', 'colorpicker.module', 'wysiwyg.module', 'chart.js', 'as.sortable', 'ngMessages', 'toggle-switch', 'ui.tinymce', 'mj.scrollingTabs', 'ngScrollbars', 'auth', 'customer', 'invoice', 'maintenance', 'misc', 'project', 'quote', 'settings', 'cashregister', 'installation', 'contracts', 'colorpicker', 'ngFileUpload', 'report', 'bootstrapLightbox', 'RecursionHelper', 'TreeWidget', 'timetrack', 'ngTouch']).config(['$provide', '$urlRouterProvider', '$stateProvider', '$locationProvider', '$compileProvider', '$httpProvider', 'LightboxProvider', function($provide, $urlRouterProvider, $stateProvider, $locationProvider, $compileProvider, $httpProvider, LightboxProvider) {
    $urlRouterProvider.otherwise('/login/');
    $compileProvider.debugInfoEnabled(!1);
    $stateProvider.state('import_articles', {
        url:'/import_articles/', templateUrl:'atemplate/import_articles', controller:'ImportArticlesCtrl as vm', resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'article-import_articles', 'xget': 'import'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
        , data: {
            menu: 'articles', submenu: 'import'
        }
        ,
    }
    ).state('import_prices', {
        url:'/import_prices/', templateUrl:'atemplate/import_prices', controller:'ImportArticlesPricesCtrl as vm', resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'article-import_articles', 'xget': 'import'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
        , data: {
            menu: 'articles', submenu: 'import'
        }
        ,
    }
    ).state('orders', {
        url:'/orders/', templateUrl:'otemplate/orders', controller:'OrdersCtrl', data: {
            menu: 'orders', submenu: 'orders'
        }
        , resolve: {
            data:function(helper) {
                return!0
            }
        }
    }
    ).state('order', {
        abstract:!0, url:'/order/', templateUrl:'otemplate/order', controller:'OrderCtrl as vm', data: {
            menu: 'orders', submenu: 'orders'
        }
        ,
    }
    ).state('order.view', {
        url:'view/:order_id', templateUrl:'otemplate/orderView', controller:'OrderViewCtrl', data: {
            menu: 'orders', submenu: 'orders'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'order-order', 'order_id': $stateParams.order_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('order.edit', {
        url:'edit/:order_id?c_quote_id?buyer_id?contact_id?a_quote_id?currency_type?languages?duplicate_order_id?vat_regime_id?identity_id?source_id?type_id?segment_id?copy_free_text?version_id', templateUrl:'otemplate/orderEdit', controller:'OrderEditCtrl', controllerAs:'edit', data: {
            menu: 'orders', submenu: 'orders'
        }
        , resolve: {
            data:function($cacheFactory, $stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                var initData= {
                    'do': 'order-norder', 'order_id': $stateParams.order_id, 'c_quote_id': $stateParams.c_quote_id, 'buyer_id': $stateParams.buyer_id, 'contact_id': $stateParams.contact_id, 'a_quote_id': $stateParams.a_quote_id, 'currency_type': $stateParams.currency_type, 'languages': $stateParams.languages, 'duplicate_order_id': $stateParams.duplicate_order_id, 'vat_regime_id': $stateParams.vat_regime_id, 'identity_id': $stateParams.identity_id, 'source_id': $stateParams.source_id, 'type_id': $stateParams.type_id, 'segment_id': $stateParams.segment_id, 'copy_free_text': $stateParams.copy_free_text, 'version_id':$stateParams.version_id
                }
                ;
                var cacheDeal=$cacheFactory.get('DealToDocument');
                if(cacheDeal!=undefined) {
                    var cachedDataDeal=cacheDeal.get('data')
                }
                if(cachedDataDeal!=undefined) {
                    for(x in cachedDataDeal) {
                        initData[x]=cachedDataDeal[x]
                    }
                    cacheDeal.remove('data')
                }
                return helper.doRequest('get', 'index.php', initData).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('order.add', {
        url:'add/', templateUrl:'otemplate/orderAdd', controller:'OrderAddCtrl', data: {
            menu: 'orders', submenu: 'orders'
        }
        ,
    }
    ).state('orderSettings', {
        url:'/orderSettings/', templateUrl:'otemplate/orderSettings', controller:'OrderSettingsCtrl as vm', data: {
            menu: 'orders', submenu: 'orders setting', adminSetting: 'order'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
            }
        }
    }
    ).state('orders_deliver', {
        url:'/orders_deliver/', templateUrl:'otemplate/ordersToBeDelivered', controller:'OrdersToBeDeliveredCtrl', data: {
            menu: 'orders', submenu: 'orders_deliver'
        }
        ,
    }
    ).state('order_deliveries', {
        url:'/order_deliveries/', templateUrl:'otemplate/ordersDeliveries', controller:'OrdersDeliveriesCtrl', data: {
            menu: 'orders', submenu: 'order_deliveries'
        }
        ,
    }
    ).state('stock_dispatching', {
        url:'/stock_dispatching/', templateUrl:'stocktemplate/stock_dispatching', controller:'StockdispatchingCtrl', data: {
            menu: 'stock_info', submenu: 'stock_dispatching'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
            }
        }
    }
    ).state('stock_dispatch', {
        abstract:!0, url:'/stock_dispatch/', templateUrl:'stocktemplate/stock_dispatch', controller:'StockdispatchCtrl as vm', data: {
            menu: 'stock_info', submenu: 'stock_dispatching'
        }
        ,
    }
    ).state('stock_dispatch.view', {
        url:'view/:stock_disp_id', templateUrl:'stocktemplate/stockdispatchView', controller:'StockdispatchViewCtrl', data: {
            menu: 'stock_info', submenu: 'stock_dispatching'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'stock-stock_dispatch', 'stock_disp_id': $stateParams.stock_disp_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('stock_dispatch.edit', {
        url:'edit/:stock_disp_id', templateUrl:'stocktemplate/stockdispatchEdit', controller:'StockdispatchEditCtrl', controllerAs:'edit', data: {
            menu: 'stock_info', submenu: 'stock_dispatching'
        }
        , resolve: {
            data:function($stateParams, helper) {
                if($stateParams.stock_disp_id == 'tmp'){
                    return {};
                }else{
                    angular.element('.loading_wrap').removeClass('hidden');
                    return helper.doRequest('get', 'index.php', {
                        'do': 'stock-nstock_dispatch', 'stock_disp_id': $stateParams.stock_disp_id
                    }
                    ).then(function(d) {
                        return d
                    }
                    , function() {
                        return {}
                    }
                    )
                }            
            }
        }
    }
    ).state('stock_dispatch.add', {
        url:'add/', templateUrl:'stocktemplate/stockdispatchAdd', controller:'StockdispatchAddCtrl', data: {
            menu: 'stock_info', submenu: 'stock_dispatching'
        }
        ,
    }
    ).state('stock_date', {
        url:'/stock_date/', templateUrl:'stocktemplate/stock_date', controller:'StockdateCtrl', data: {
            menu: 'stock_info', submenu: 'stock_info'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'stock-stock_date'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('stock_picking', {
        url:'/stock_picking/', templateUrl:'stocktemplate/stock_picking', controller:'StockPickingCtrl', data: {
            menu: 'stock_info', submenu: 'stock_picking'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
            }
        }
    }
    ).state('purchaseOrderSettings', {
        url:'/purchaseOrderSettings/', templateUrl:'potemplate/purchaseOrderSettings', controller:'PurchaseOrderSettingsCtrl as vm', data: {
            menu: 'orders', submenu: 'po_order_setting setting', adminSetting: 'po_order_setting'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
            }
        }
    }
    ).state('stockSettings', {
        url:'/stockSettings/', templateUrl:'stocktemplate/stockSettings', controller:'StockSettingsCtrl as vm', data: {
            menu: 'stock_info', submenu: 'stock_info',
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'stock-settings', 'xget': 'stock'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('stock_customer', {
        url:'/stock_customer/', templateUrl:'stocktemplate/stock_customer', controller:'StockcustomerCtrl', data: {
            menu: 'stock_info', submenu: 'stock_info'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'stock-stock_customer'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('stock_article', {
        url:'/stock_article/', templateUrl:'stocktemplate/stock_article', controller:'StockarticleCtrl', data: {
            menu: 'stock_info', submenu: 'stock_info'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'stock-stock_article'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('stock_batches', {
        url:'/stock_batches/', templateUrl:'stocktemplate/stock_batches', controller:'StockbatchesCtrl', data: {
            menu: 'stock_info', submenu: 'stock_info'
        }
        , resolve: {
            data:function(helper) {
                return!0
            }
        }
    }
    ).state('stock_info', {
        url:'/stock_info/', templateUrl:'stocktemplate/stock_info', controller:'StockinfoCtrl', data: {
            menu: 'stock_info', submenu: 'stock_info'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
            }
        }
    }
    ).state('stock_serial', {
        url:'/stock_serial/', templateUrl:'stocktemplate/stock_serial', controller:'StockSerialCtrl', data: {
            menu: 'stock_info', submenu: 'stock_info'
        }
        , resolve: {
            data:function(helper) {
                return!0
            }
        }
    }
    ).state('purchase_orders', {
        url:'/purchase_orders/', templateUrl:'potemplate/purchase_orders', controller:'PurchaseordersCtrl', data: {
            menu: 'orders', submenu: 'purchase_orders'
        }
        , resolve: {
            data:function(helper) {
                return!0
            }
        }
    }
    ).state('purchase_order_deliveries', {
        url:'/purchase_order_deliveries/', templateUrl:'potemplate/purchaseOrdersDeliveries', controller:'PurchaseOrdersDeliveriesCtrl', data: {
            menu: 'orders', submenu: 'purchase_order_deliveries'
        }
        ,
    }
    ).state('purchase_order', {
        abstract:!0, url:'/purchase_order/', templateUrl:'potemplate/purchase_order', controller:'PurchaseorderCtrl as vm', data: {
            menu: 'orders', submenu: 'purchase_orders'
        }
        ,
    }
    ).state('purchase_order.bulk', {
        url:'bulk/:order_id', templateUrl:'potemplate/purchaseorderBulk', controller:'PurchaseorderBulkCtrl', data: {
            menu: 'orders', submenu: 'purchase_orders'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'po_order-add_bulk', 'order_id': $stateParams.order_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('purchase_order.bulkserv', {
        url:'bulkserv/:service_id?segment_id?type_id?source_id', templateUrl:'potemplate/purchaseorderBulk', controller:'PurchaseorderBulkCtrl', data: {
            menu: 'orders', submenu: 'purchase_orders'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'po_order-add_bulk', 'service_id': $stateParams.service_id, 'source_id': $stateParams.source_id, 'type_id': $stateParams.type_id, 'segment_id': $stateParams.segment_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('purchase_order.bulkfacq', {
        url:'bulkfacq/:quote_id', templateUrl:'potemplate/purchaseorderBulk', controller:'PurchaseorderBulkFacqCtrl', data: {
            menu: 'orders', submenu: 'purchase_orders'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'po_order-add_bulk_facq', 'quote_id': $stateParams.quote_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                
                
                )
            }
        }
    }
    ).state('purchase_order.view', {
        url:'view/:p_order_id', templateUrl:'potemplate/purchaseorderView', controller:'PurchaseorderViewCtrl', data: {
            menu: 'orders', submenu: 'purchase_orders'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'po_order-po_order', 'p_order_id': $stateParams.p_order_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('purchase_order.edit', {
        url:'edit/:p_order_id?buyer_id?duplicate_p_order_id?quote_id', templateUrl:'potemplate/purchaseorderEdit', controller:'PurchaseorderEditCtrl', controllerAs:'edit', data: {
            menu: 'orders', submenu: 'purchase_orders'
        }
        , resolve: {
            data:function($stateParams, helper, $cacheFactory) {
                angular.element('.loading_wrap').removeClass('hidden');
                var cacheOrder=$cacheFactory.get('OrderToPurchase');
                if(cacheOrder!=undefined) {
                    var cachedDataOrder=cacheOrder.get('data')
                }
                var initData= {};
                initData.p_order_id=$stateParams.p_order_id;
                initData.buyer_id=$stateParams.buyer_id;
                initData.duplicate_p_order_id=$stateParams.duplicate_p_order_id;
                initData.quote_id=$stateParams.quote_id;
                if(cachedDataOrder!=undefined) {
                    for(x in cachedDataOrder) {
                        initData[x]=cachedDataOrder[x]
                    }
                    cacheOrder.remove('data')
                }
                return helper.doRequest('get', 'index.php?do=po_order-npo_order', initData).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('purchase_order.add', {
        url:'add/', templateUrl:'potemplate/purchaseorderAdd', controller:'PurchaseorderAddCtrl', data: {
            menu: 'orders', submenu: 'purchase_orders'
        }
        ,
    }
    ).state('stock_low', {
        url:'/stock_low/', templateUrl:'stocktemplate/stock_low', controller:'StocklowCtrl', data: {
            menu: 'orders', submenu: 'stock_low'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'stock-stock_low'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('to_order', {
        url:'/to_order/', templateUrl:'stocktemplate/to_order', controller:'ToOrderCtrl', data: {
            menu: 'orders', submenu: 'stock_low'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'stock-to_order'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('aservices', {
        url:'/aservices/', templateUrl:'atemplate/aservices', controller:'ServicesCtrl', data: {
            menu: 'articles', submenu: 'aservices'
        }
        , resolve: {
            data:function(helper) {
                return!0
            }
        }
    }
    ).state('articles', {
        url:'/articles/', templateUrl:'atemplate/articles', controller:'ArticlesCtrl', data: {
            menu: 'articles', submenu: 'articles'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
            }
        }
    }
    ).state('articlesSettings', {
        url:'/articlesSettings/', templateUrl:'atemplate/articlesSettings', controller:'articlesSettingsCtrl as vm', data: {
            menu: 'articles', submenu: 'articles setting', adminSetting: 'article'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'article-settings', 'xget': 'setarticle'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('article', {
        abstract:!0, url:'/article/', templateUrl:'atemplate/article', controller:'ArticleCtrl as vm', data: {
            menu: 'articles', submenu: 'articles'
        }
    }
    ).state('article.add', {
        url:'article/:article_id?duplicate_article_id?form_type', templateUrl:function($stateParams) {
            return'atemplate/add/articleTemplate/'+$stateParams.form_type
        }
        , controller:'ArticleAddCtrl as add', resolve: {
            data:function(helper,$stateParams) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'article-article', 'form_type':$stateParams.form_type
                }
                ).then(function(d) {
                    return d.data
                }
                , function() {
                    return {}
                }
                )
            }
        }
        , data: {
            menu: 'articles', submenu: 'articles'
        }
        ,
    }
    ).state('groups', {
        url:'/groups/', templateUrl:'miscTemplate/groups', controller:'MyUsersCtrl as vm', resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'settings-settings', 'xget': 'group'
                }
                ).then(function(d) {
                    return d.data
                }
                , function() {
                    return {}
                }
                )
            }
        }
        , data: {
            menu: '#', submenu: '#'
        }
    }
    ).state('board', {
        url:'/board/', templateUrl:'miscTemplate/board', controller:'Board as vm', data: {
            menu: '#', submenu: '#'
        }
    }
    ).state('board_1', {
        url:'/board_1/', templateUrl:'miscTemplate/board_1', controller:'Board as vm', data: {
            menu: '#', submenu: '#'
        }
    }
    ).state('article_families', {
        url:'/article_families/', templateUrl:'atemplate/ArticleFamilies', controller:'ArticleFamiliesCtrl', resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'article-settings', 'xget': 'ledger'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
        , data: {
            menu: 'articles', submenu: 'families'
        }
    }
    );
    $urlRouterProvider.deferIntercept();
    $locationProvider.html5Mode(!0);
    $httpProvider.defaults.headers.common["X-Requested-With"]='XMLHttpRequest';
    $provide.decorator('$templateCache', ['$delegate', function($delegate) {
        var keys=[], origPut=$delegate.put, origRem=$delegate.remove, origRemAll=$delegate.removeAll;
        $delegate.put=function(key, value) {
            origPut(key, value);
            if(keys.indexOf(key)===-1) {
                keys.push(key)
            }
            else {
                keys[key]=value
            }
        }
        ;
        $delegate.remove=function(key) {
            origRem(key);
            keys.splice(keys.indexOf(key), 1)
        }
        ;
        $delegate.removeAll=function() {
            origRemAll();
            keys=[]
        }
        $delegate.getKeys=function() {
            return keys
        }
        ;
        return $delegate
    }
    ]);
    LightboxProvider.templateUrl='miscTemplate/lightbox'
}

]).run(['$http', '$rootScope', '$urlRouter', 'helper', 'UserService', '$state','$timeout', 'CONSTANTS', '$window', '$confirm','$location',function($http, $rootScope, $urlRouter, helper, UserService, $state, $timeout, CONSTANTS, $window,$confirm,$location) {
    
    let edited_documents=['quote_edit'];
    let allowed_to_continue={
        'quote_edit':['quote.view']
    };
    let browser_history=[];

    $rootScope.$on('$stateChangeStart', function(event, nextState, currentState) {
        if(nextState.name && app_titles[nextState.name] !== undefined){
            document.title = page_title_default+(app_titles[nextState.name].length ? ' - '+helper.getLanguage(app_titles[nextState.name]) : '');
        }else{
            document.title=page_title_default;
        }
        if(!$rootScope.reset_lists){
            $rootScope.reset_lists={};
        }
        if(!$rootScope.opened_lists){
            $rootScope.opened_lists={};
        }
        if(!$rootScope.edited_data){
            $rootScope.edited_data=[];
        }
        let doc_edited = $rootScope.edited_data.find( val => edited_documents.includes(val));
        if(doc_edited !== undefined && allowed_to_continue[doc_edited] && allowed_to_continue[doc_edited].indexOf(nextState.name) === -1){
            event.preventDefault();
            $rootScope.disable_menu_click=true;         
            $confirm({
                title: helper.getLanguage('Confirm'),
                ok: helper.getLanguage('Leave'),
                cancel: helper.getLanguage('Stay'),
                text: helper.getLanguage("Are you sure you want to leave? You might lose any changes you have made on this page.")
            }).then(function() {
                $rootScope.disable_menu_click=false;
                $rootScope.edited_data.splice($rootScope.edited_data.indexOf(doc_edited), 1);            
                $state.go(nextState, currentState);
            }, function() {
               //
            });
            if(browser_history.length){
                $location.url(browser_history[browser_history.length-1]);
            }
            return false;         
        }

        $rootScope.nextState=nextState;
        if($rootScope.opened_lists.orders && nextState.name != 'orders'){
            $rootScope.reset_lists.orders=true;
        }else if($rootScope.opened_lists.invoices && nextState.name != 'invoices'){
            $rootScope.reset_lists.invoices=true;
        }else if($rootScope.opened_lists.recinvoices && nextState.name != 'recinvoices'){
            $rootScope.reset_lists.recinvoices=true;
        }else if($rootScope.opened_lists.purchase_orders && nextState.name != 'purchase_orders'){
            $rootScope.reset_lists.purchase_orders=true;
        }else if($rootScope.opened_lists.purchase_invoices && nextState.name != 'purchase_invoices'){
            $rootScope.reset_lists.purchase_invoices=true;
        }else if($rootScope.opened_lists.services && nextState.name != 'services'){
            $rootScope.reset_lists.services=true;
        }else if($rootScope.opened_lists.installations && nextState.name != 'installations'){
            $rootScope.reset_lists.installations=true;
        }else if($rootScope.opened_lists.export_btb && nextState.name != 'export_btb'){
            $rootScope.reset_lists.export_btb=true;
        }else if($rootScope.opened_lists.stock_batches && nextState.name != 'stock_batches'){
            $rootScope.reset_lists.stock_batches=true;
        }else if($rootScope.opened_lists.customers && nextState.name != 'customers'){
            $rootScope.reset_lists.customers=true;
        }else if($rootScope.opened_lists.articles && nextState.name != 'articles') {
            $rootScope.reset_lists.articles=true;
        }else if($rootScope.opened_lists.quotes && nextState.name != 'quotes'){
            $rootScope.reset_lists.quotes=true;
        }else if($rootScope.opened_lists.aservices && nextState.name != 'aservices'){
            $rootScope.reset_lists.aservices=true;
        }else if($rootScope.opened_lists.deals && nextState.name != 'deals'){
            $rootScope.reset_lists.deals=true;
        }

        if(angular.element("meta[name='version']")[0]) {
            var browserVersion=angular.element("meta[name='version']")[0].content;
            helper.doRequest('get', 'index.php', { 'do': 'auth-getVersion'}, function(r) {
             
                if(browserVersion!=r.data.version) {
                    browserVersion=r.data.version;
                    var ref=angular.element("base")[0].href;
                    ref=ref.substring(0, ref.length-1);
                    $window.location.replace(ref+nextState.url)
                }else if(r.data.show_banner =='1'){

                    $timeout(function() {
                        $('.text_from_snap').html(r.data.banner_message);
                        $('.urgent_message').removeClass('hide');
                    },200);
                }else{
                    $('.urgent_message').addClass('hide');
                }
              })
        }
        if(UserService.isLoggedIn===!0&&UserService.two_done===!1&&(nextState.name!='login'||nextState.name!='login_two')) {
            if(nextState.name=='login'||nextState.name=='logout'||nextState.name=='login_two') {}
            else {
                event.preventDefault();
                //angular.element('.loading_wrap').addClass('hidden');
                return!1
            }
        }
        else if(UserService.isLoggedIn===!0&&UserService.must_pay===!0&&(nextState.name!='subscription'||nextState.name!='subscription_new'||nextState.name!='login'||nextState.name!='settings')) {
            if(nextState.name=='subscription'||nextState.name=='subscription_new'||nextState.name=='login'||nextState.name=='logout'||nextState.name=='settings') {}
            else {
                event.preventDefault();
                //angular.element('.loading_wrap').addClass('hidden');
                return!1
            }
        }
        var data= {}
        , e=event, newDate=new Date().getTime();
        if(nextState.data) {
            if(nextState.data.menu) {
                data= {
                    menu: nextState.data.menu, submenu: nextState.data.submenu
                }
                if(currentState.form_type=='service') {
                    data= {
                        menu: nextState.data.menu, submenu: 'aservices'
                    }
                }
                helper.menu=data
            }
            if(nextState.data.group&&nextState.data.group!=UserService.group) {
                event.preventDefault()
            }
            else if(nextState.data.adminSetting&&UserService.group!='admin'&&UserService.adminSetting.indexOf(nextState.data.adminSetting)===-1) {
                event.preventDefault()
            }
            else if(helper.settings.WIZZARD_COMPLETE=='0'&&nextState.name!='board') {
                event.preventDefault()
            }
        }
        if(nextState.name=='login') {
            helper.doRequest('get', 'index.php', {
                'do': 'auth-isLoged'
            }
            , function(r) {
                if (r.data != undefined) {
                    if (r.data.isLoggedIn) {
                        if (r.data.must_pay === !0) {
                            $state.go(r.data.pag)
                        }
                        else {
                            if (r.data.remember_url) {
                                $window.location.replace(r.data.default_pag)
                            } else {
                                $state.go(r.data.default_pag)
                            }
                        }
                    }
                } else {
                    $window.location.replace('login');
                }
            }
            )
        }
        if(UserService.isLoggedIn===undefined) {
            event.preventDefault();
            angular.element('.loading_wrap').addClass('hidden')
        }
        angular.element('body').scrollTop(0);
        if(CONSTANTS.noAuth.indexOf(nextState.name)==-1) {
            if(UserService.isLoggedIn===!0&&UserService.isLoged<newDate||UserService.isLoggedIn===!1||UserService.isLoggedIn===undefined) {
                UserService.checkAuthAsync().then(function(r) {
                    if(r&&r.data) {
                        for(x in r.data) {
                            UserService[x]=r.data[x]
                        }
                        UserService.isLoged=new Date().getTime()+(30*60)*1000;
                        if(UserService.isLoggedIn===!1) {
                            $state.go('login');
                            return!1
                        }
                        if(UserService.isLoggedIn===!0&&UserService.two_done===!1&&(nextState.name!='login'||nextState.name!='login_two')) {
                            if(nextState.name=='login'||nextState.name=='logout'||nextState.name=='login_two') {
                                $urlRouter.sync()
                            }
                            else {
                                if(UserService.remember_url){
                                    $window.location.replace(UserService.default_pag)
                                }else{
                                    $state.go(UserService.default_pag)
                                }
                                return!1
                            }
                        }
                        else if(UserService.isLoggedIn===!0&&UserService.must_pay===!0&&(nextState.name!='subscription'||nextState.name!='subscription_new'||nextState.name!='login'||nextState.name!='settings')) {
                            if(nextState.name=='subscription'||nextState.name=='subscription_new'||nextState.name=='login'||nextState.name=='logout'||nextState.name=='settings') {
                                $urlRouter.sync()
                            }
                            else {
                                $state.go(UserService.pag);
                                return!1
                            }
                        }
                        if(nextState.data) {
                            if(nextState.data.group&&nextState.data.group!=UserService.group) {
                                if(UserService.isLoggedIn===!0) {
                                    if(UserService.remember_url){
                                        $window.location.replace(UserService.default_pag)
                                    }else{
                                        $state.go(UserService.default_pag)
                                    }
                                }
                                else {
                                    $state.go('login')
                                }
                                return!1
                            }
                            else if(nextState.data.adminSetting&&UserService.group!='admin'&&UserService.adminSetting.indexOf(nextState.data.adminSetting)==-1) {
                                if(UserService.isLoggedIn===!0) {
                                    if(UserService.remember_url){
                                        $window.location.replace(UserService.default_pag)
                                    }else{
                                        $state.go(UserService.default_pag)
                                    }
                                }
                                else {
                                    $state.go('login')
                                }
                                return!1
                            }
                            else if(helper.settings.WIZZARD_COMPLETE=='0') {
                                //angular.element('.loading_wrap').addClass('hidden');
                                return!1
                            }
                            else {
                                if(event.defaultPrevented===!0) {
                                    $urlRouter.sync()
                                }
                            }
                        }
                    }
                    else {
                        UserService.isLoggedIn=!1;
                        $state.go('login');
                        return!1
                    }
                }
                , function() {
                    UserService.isLoggedIn=!1;
                    $state.go('login');
                    return!1
                }
                )
            }
            else {
                if(helper.settings.WIZZARD_COMPLETE=='0') {
                    //angular.element('.loading_wrap').addClass('hidden');
                    return!1
                }
            }
        }
        else {
            if(event.defaultPrevented===!0) {
                UserService.reset();
                $urlRouter.sync()
            }
        }
        if(event.defaultPrevented===!1) {
            angular.element('body').removeClass('overflow_hidden');
            if(angular.isDefined($rootScope.haveGeneralSettings)&&UserService.isLoggedIn===!0) {
                return!0
            }
            helper.getSettings().then(function(r) {
                console.log('waiting for the settings');
                $rootScope.haveGeneralSettings=!0
            }
            )
        }
    }
    );
    $rootScope.$on('$stateChangeSuccess', function(event, nextState, currentState){ 
        angular.element('.content').scrollTop(0);
        if(browser_history.length>10){
            browser_history=[];
        }
        browser_history.push($location.$$path);
    });
    $urlRouter.listen()
}

])