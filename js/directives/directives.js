angular.module('akti').directive('pagCustom1', ['helper', function(helper) {
    return {
        restrict: "E",
        templateUrl: 'pager',
        link: function(scope, element, attr) {
            if (!scope.pagination) {
                scope.pagination = {}
            }
            if (!scope.activeView) {
                scope.activeView = 0
            }
            scope.pagination.pagination = !1;
            scope.pagination.page = [];
            scope.pagination.pages = 1;
            scope.pagination.row = 30;
            if (scope.pagination.offset == undefined) {
                scope.pagination.offset = 0
            }
            scope.pagination.currentPage = scope.pagination.offset + 1;
            scope.pagination.setPage = function(pageNo) {
                if (pageNo < 1) {
                    return !1
                }
                if (pageNo > scope.pagination.pages || pageNo == scope.pagination.currentPage) {
                    return !1
                }
                scope.pagination.currentPage = pageNo;
                scope.pagination.offset = (scope.pagination.currentPage - 1);
                scope.pagination.createPages();
                var params = {};
                for (x in scope.extraParams) {
                    params[x] = scope.extraParams[x]
                }
                params.do = attr.doit;
                params.offset = scope.pagination.offset;
                params.view = scope.activeView;
                params.search = scope.search.search;
                params.xget = attr.xge;
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', params, scope.renderList)
            };
            scope.pagination.renderPagination = function() {
                scope.pagination.pages = Math.ceil(scope.max_rows / scope.pagination.row);
                if (scope.pagination.pages <= 1) {
                    scope.pagination.pagination = !1;
                    return !0
                }
                scope.pagination.createPages();
                scope.pagination.pagination = !0
            };
            scope.pagination.createPages = function() {
                scope.pagination.page = [];
                var iteration = 1;
                for (var i = 1; i <= scope.pagination.pages; i++) {
                    if (scope.pagination.pages > 5) {
                        if (scope.pagination.currentPage - 2 > 1 && scope.pagination.currentPage - 2 > i && i + 5 <= scope.pagination.pages) {
                            continue
                        }
                    }
                    if (iteration > 5) {
                        break
                    }
                    scope.pagination.page.push(i);
                    iteration++
                }
            }
            scope.$watch('max_rows', function(value) {
                scope.pagination.renderPagination()
            })
        }
    }
}]).directive('searchIt', ['$timeout', 'helper', function($timeout, helper) {
    return {
        restrict: "A",
        scope: {
            search: "=doit",
            callbk: "="
        },
        link: function(scope, element, attr) {
            element.on('keyup', function() {
                if (helper.searchPromise) {
                    $timeout.cancel(helper.searchPromise)
                }
                var type = 'get';
                if (attr.typemethod != undefined) {
                    type = attr.typemethod
                }
                
                helper.searchPromise = $timeout(function() {
                    if (scope.search.search != '') {
                        scope.search.offset = 1
                    }
                    var params=angular.copy(scope.search);
                    if (attr.resetlist != undefined) {
                        params.reset_list='1';
                    }
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest(type, 'index.php', params, scope.callbk)
                }, 500)
            })
        }
    }
}]).directive('displayNr', ['$window', 'helper', function($window, helper) {
    return {
        restrict: "A",
        scope: {
            val: "=ngModel"
        },
        link: function(scope, element, attr) {
            element.on('blur', function() {
                var style = helper.settings.ACCOUNT_NUMBER_FORMAT,
                    number = helper.return_value(scope.val),
                    unit_price_digits = helper.settings.ARTICLE_PRICE_COMMA_DIGITS,
                    digits = 2;
                if (attr.useDigits) {
                    digits = unit_price_digits
                }
                var val = number_format(number, digits, style[1], style[0]);
                element.val(val);
                scope.val = val
            }).on('focus', function() {
                var windowWidth = $window.innerWidth;
                if (windowWidth < 1200 && attr.selectTabletAll == "true") {
                    this.select()
                }
            })
        }
    }
}]).directive('displayOgm', ['helper', function(helper) {
    return {
        restrict: "A",
        scope: {
            val: "=ngModel",
            err: "=displayError"
        },
        link: function(scope, element, attr) {
            element.on('keyup', function() {
                var elem = scope.val.trim();
                var elem_length = elem.length;
                scope.err ='';
                if (elem_length>0 && elem_length != 12 && elem_length != 14) {
                    //element.val('');
                    //scope.val = '';
                    scope.err = helper.getLanguage("This is not a valid OGM");
                    return !1
                }
                var is_ok = !0;
                if (elem_length == 12) {
                    for (var i = 0; i < elem_length; i++) {
                        if (parseInt(elem.charAt(i)) == NaN) {
                            is_ok = !1;
                            break
                        }
                    }
                }
                if (elem_length == 14) {
                    for (var i = 0; i < elem_length; i++) {
                        if (parseInt(elem.charAt(i)) == NaN) {
                            if ((i == 3 || i == 8) && i == '/') {
                                continue
                            } else {
                                is_ok = !1;
                                break
                            }
                        }
                    }
                }
                if (!is_ok) {
                   // element.val('');
                    //scope.val = '';
                    scope.err = helper.getLanguage("This is not a valid OGM");
                    return !1
                } else {
                    if (elem_length == 12) {
                        var new_el = '';
                        for (var i = 0; i < elem_length; i++) {
                            new_el += (i == 3 || i == 7 ? '/' + elem.charAt(i) : elem.charAt(i))
                        }
                        element.val(new_el);
                        scope.val = new_el
                    }
                }
            })
        }
    }
}]).directive('corectVal', ['helper', function(helper) {
    return {
        restrict: "A",
        scope: {
            val: "=ngModel"
        },
        link: function(scope, element, attr) {
            element.on('blur', function() {
                scope.val = corect_val(scope.val)
            })
        }
    }
}]).directive('goTo', ['$state', '$window', 'helper', function($state, $window, helper) {
    return {
        restrict: "A",
        scope: {
            state: '@',
            params: '@',
            list: '@'
        },
        link: function(scope, element, attr) {
            var w = $(window);
            $window.scrollTo(0,0);
            element.on('click', function(e) {
                e.preventDefault();
                var current_width = w.width();
                if ($(this).hasClass('mainLink') && current_width < 1200) {
                    return !1
                }
                if (scope.state == "article.add") {
                    var click_disabled = attr.clickdisabled;
                    if (click_disabled == '1') {
                        return !1
                    }
                    var view_disabled = helper.settings.NO_ACCESS_VIEW_ARTICLE;
                    var is_admin_article = 0;
                    if (helper.is_admin_for.indexOf('12') > -1) {
                        is_admin_article = 1
                    }
                    if (view_disabled == '1' && !is_admin_article) {
                        return !1
                    }
                }
                if (scope.params == undefined) {
                    scope.params = {}
                }
                if (typeof(scope.params) == 'string') {
                    scope.params = JSON.parse(scope.params)
                }
                if (scope.list) {
                    if (typeof(scope.list) == 'string') {
                        scope.list = JSON.parse(scope.list)
                    }
                    helper.setItem(scope.list.type, scope.list.obj)
                }
                angular.element('.page').removeClass('showing-menu');
                if (e.button == 1) {
                    $window.open($state.href(scope.state, scope.params, {
                        absolute: !0
                    }), '_blank')
                } else {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    $state.go(scope.state, scope.params, {
                        reload: !0
                    })
                }
            })
        }
    }
}]).directive('footerDir', ['$interval', 'UserService', 'helper', function($interval, UserService, helper) {
    return {
        restrict: 'E',
        scope: {},
        link: function(scope, element, attrs) {
            scope.isLoged = !1;
            $interval(function() {
                scope.isLoged = UserService.isLoggedIn
            }, 250, !1);
            helper.doRequest('get', 'index.php?do=misc-footer&xget=foot', function(r) {
                scope.footer = r.data.footer
            })
        },
        templateUrl: 'miscTemplate/footer'
    }
}]).directive('headerDir', ['$state', '$compile', '$rootScope', '$state', 'editItem','$interval', 'helper', 'UserService', '$uibModal','modalFc', function($state, $compile, $rootScope, $state, editItem, $interval, helper, UserService, $uibModal, modalFc) {
    return {
        restrict: 'E',
        scope: {},
        transclude: !0,
        link: function(scope, element, attrs) {
            var w = $(window);
            scope.menu = helper.menu;
            scope.main_menu = helper.main_menu;
            scope.intNr = 0;
            scope.messages = 0;
            scope.taskNr = 0;
            scope.switchList = [];
            scope.userName = UserService.name;
            scope.isLoged = !1;
            scope.switchAcc = !1;
            scope.show = {
                profile: !1,
                quickAdd: !1
            };
            helper.Loading++;
            scope.is_trial = !1;
            scope.wizzard_complete = !1;
            scope.days_left = '';
            scope.app="general";
            scope.showTxt={};
            helper.doRequest('get', 'index.php?do=misc-notice', function(r) {
                if (r.data) {
                    scope.intNr = r.data.topInfo.info;
                    scope.messages = r.data.topInfo.nr;
                    scope.taskNr = r.data.topInfo.nr;
                    scope.switchList = r.data.switchInfo.user_dd;
                    scope.switchAcc = r.data.switchInfo.data;
                    scope.companyName = r.data.accountName;
                    scope.ADV_CRM = r.data.ADV_CRM;
                    scope.is_accountant = r.data.switchInfo.is_accountant;
                    scope.lang_page = r.data.lang_page;
                    scope.is_trial = r.data.switchInfo.is_trial;
                    scope.wizzard_complete = r.data.switchInfo.wizzard_complete;
                    scope.days_left = r.data.switchInfo.days_left
                }
            });
            scope.switchAccount = function(id) {
                var params = {
                    user_id: id,
                    'do': 'misc--misc-switchUser',
                    is_accountant: scope.is_accountant
                };
                helper.doRequest('post', 'index.php', params, function(r) {
                    if (r.success && r.success.success == 'Success') {
                        window.location.reload()
                    }
                })
            }
            angular.element('body').on('click', function(e) {
                if (angular.element(e.target).parents('.header').length == 0) {
                    scope.show = {
                        profile: !1,
                        quickAdd: !1
                    }
                } else {
                    if (angular.element(e.target).parents('.uo-quick_add').length == 0) {
                        scope.show.quickAdd = !1
                    }
                    if (angular.element(e.target).parents('.user_options-menu').length == 0) {
                        scope.show.profile = !1
                    }
                    if (angular.element(e.target).parents('.support-page').length == 0) {
                        scope.show.support = !1
                    }
                }
            });
            element.on('touchstart click', function(e) {
                if (angular.element(e.target).hasClass('header')) {
                    angular.element('.page').removeClass('showing-menu')
                }
                if($rootScope.disable_menu_click){
                  e.preventDefault();
                  return false;  
                }
                if(angular.element(e.target).closest('li').hasClass('navigation-link')){
                    link_parent = angular.element(e.target).closest('li');
                    if (!link_parent.hasClass('navigation-current')) {
                        angular.element(e.target).parents('#navigation-menu').find('li').removeClass('navigation-current');
                        link_parent.addClass('navigation-current');
                    }
                }
            });
            scope.show_dropdown = function() {
                var current_width = w.width();
                if (current_width < 1200) {
                    angular.element('.page').toggleClass('showing-menu')
                }
            }, $interval(function() {
                scope.menu = helper.menu;
                scope.main_menu = helper.main_menu;
                scope.userName = UserService.name;
                scope.isLoged = UserService.isLoggedIn
            }, 250, !1);
            scope.changeLang = function(lang) {
                if (scope.lang_page) {
                    scope.lang_change = lang
                } else {
                    scope.lang_change = lang
                }
                helper.Loading++;
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php?do=misc-isLoged-user-change_language&lang=' + scope.lang_change, function(r) {
                    if (!r.error) {
                        UserService.name = r.data.name ? r.data.name : '';
                        helper.removeCachedTemplates();
                        angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                        angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                        $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                        $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                        UserService.default_pag = r.data.default_pag;
                        helper.doRequest('get', 'index.php?do=misc-lang', function(d) {
                            LANG = d.data;
                            $state.reload()
                        }, function(r) {
                            $state.reload()
                        })
                    } else {
                        //angular.element('.loading_wrap').addClass('hidden')
                    }
                })
            }
            scope.headerModal = function(type, item, size) {
                if (type == 'HeaderModal') {
                    var params = {
                        template: 'miscTemplate/' + type,
                        ctrl: type + 'Ctrl',
                        size: 'md',
                        windowClass: 'app-modal-window'
                    }
                }
                if (type == 'InviteModal') {
                    var params = {
                        template: 'miscTemplate/' + type,
                        ctrl: type + 'Ctrl',
                        size: size,
                        backdrop: 'static',
                       
                    }

                }
                modalFc.open(params)
            }

            scope.$watch(function() {
                if (angular.element('.beamer_icon').length == 2) {
                    angular.element('.beamer_icon:eq(1)').remove()
                }
            })

            scope.createCustomer=function(){
                scope.show.quickAdd = false;
                var main_obj = {
                    country_dd: [],
                    country_id: 0,
                    add_customer: !0,
                    'do': 'misc--misc-tryAddC',
                };
                editItem.openModal(scope, 'AddCustomer', main_obj, 'md');
            }

            scope.createContact=function(){
                scope.show.quickAdd = false;
                var main_obj = {
                    country_dd: [],
                    country_id: 0,
                    add_contact: !0,
                    contact_only: !0,
                    'do': 'misc--misc-tryAddC',
                };
                editItem.openModal(scope, 'AddCustomer', main_obj, 'md');
            }

        },
        templateUrl: 'miscTemplate/header'
    }
}]).directive('console', ['$timeout', 'helper', function($timeout, helper) {
    return {
        restrict: 'E',
        scope: {},
        link: function(scope, element, attrs) {
            scope.console = helper.console
        },
        templateUrl: 'miscTemplate/console'
    }
}]).directive('logging', ['helper','$rootScope','$timeout', function(helper, $rootScope,$timeout) {
    return {
        restrict: 'E',
        scope: {},
        link: function(scope, element, attrs) {
            scope.advanced = attrs.advanced || "true";
            scope.isContactView = attrs.do == 'customers-xcustomer_contact' ? !0 : !1;
            scope.seeMore = !1;
            scope.seeCmts = !0;
            var date = new Date();
            scope.log = {
                format: 'dd/MM/yyyy'
            };
            scope.datePickOpen = {
                comment_due_date: !1
            };
            scope.dateOptions = {
                dateFormat: '',
            };
            var params = {};
            params.pagl = attrs.do;
            params[attrs.type] = attrs.ref;
            var reference = attrs.type;
            scope.renderPage = function(d) {
                scope.log = d.data;
                if (scope.log.comments_list) {
                    helper.doRequest('post', 'index.php?do=misc-customer_activity_comment_list', scope.log, scope.renderComment)
                }
            }
            scope.renderComment = function(d) {
                scope.comment = d.data;
                if (scope.comment.add_comment == !1) {
                    scope.log.hide_comment_form = !1
                }
            }
            scope.show_form = function(el) {
                scope.comment.add_comment = !1;
                scope.log.hide_comment_form = !1
            }
            scope.dataComment = function(form, func, item) {
                if (func != 'delete_comment') {
                    if (form != null && (Object.keys(form.$error).length > 0 || scope.log.to_user_id == '0')) {
                        return !1
                    }
                }
                if (item) {
                    item.user_id = scope.log.user_id;
                    item.log_id = item.id;
                    var data = item
                } else {
                    var data = scope.log
                }
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('post', 'index.php?do=misc-customer_activity_comment_list-misc-' + func, data, function(r) {
                    //angular.element('.loading_wrap').addClass('hidden');
                    scope.datePickOpen.comment_due_date = !1;
                    scope.log.comment = '';
                    if (func == 'delete_comment') {
                        form.comment = {};
                        form.comment_due_date = {};
                        form.to_user_id = {}
                    }
                    scope.log.to_user_id = '';
                    scope.log.comment_due_date = '';
                    scope.log.hide_comment_form = !0;
                    scope.seeCmts = !0;
                    scope.renderComment(r)
                })
            }
            scope.loadActions = function() {
                //angular.element('.loading_wrap').removeClass('hidden');
                scope.seeMore = scope.seeMore == !0 ? !1 : !0;
                var prms = {
                    more: scope.seeMore,
                    pag: scope.log.page,
                    field_name: scope.log.field_name,
                    field_value: scope.log.field_value
                };
                helper.doRequest('get', 'index.php?do=misc-customer_activity_list', prms, function(r) {
                    //angular.element('.loading_wrap').addClass('hidden');
                    scope.log.messages = r.data.messages
                })
            }
            scope.loadComments = function() {
                //angular.element('.loading_wrap').removeClass('hidden');
                scope.seeCmts = scope.seeCmts == !0 ? !1 : !0;
                var data = scope.log;
                data.moreCtms = scope.seeCmts;
                helper.doRequest('get', 'index.php?do=misc-customer_activity_comment_list', data, function(r) {
                    //angular.element('.loading_wrap').addClass('hidden');
                    scope.comment.comments = r.data.comments
                })
            }

            scope.notesTinymceOpts = {
                selector: ".variable_assign",
                resize: "both",
                width: "auto",
                height: 192,
                menubar: !1,
                autoresize_bottom_margin: 0,
                relative_urls: !1,
                trusted: !0,
                debouncetime: 5000,
                onblur: !0,
                plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
                statusbar: !1,
                toolbar: ["bold italic | bullist numlist "]
            };

            scope.editNotes = function() {
                if (helper.searchPromise) {
                    $timeout.cancel(helper.searchPromise)
                }
                helper.searchPromise = $timeout(function() {
                    var tmp_obj = {
                        'do': 'misc--misc-updateInternalNotes',
                        'module': scope.log.pag,
                        'field_name': scope.log.field_name,
                        'field_value': scope.log.field_value,
                        'notes': scope.log.notes
                    };
                    helper.doRequest('post', 'index.php', tmp_obj);
                }, 500);            
            }

            if (attrs.ref != '') {
                helper.doRequest('get', 'index.php?do=misc-loging', params, scope.renderPage)
            }

            var refreshListener=$rootScope.$on('refreshLogging', function(event, data) {
                if (attrs.ref != '') {
                    helper.doRequest('get', 'index.php?do=misc-loging', params, scope.renderPage)
                }
            });
            scope.$on('$destroy', refreshListener);
        },
        templateUrl: 'miscTemplate/loging'
    }
}]).directive('resizeTxt', ['$timeout', function($timeout) {
    return {
        restrict: "A",
        scope: {},
        priority: 100,
        link: function(scope, element, attr) {
            var calcheight = calcheight,
                rowHeight = element[0].offsetHeight;
            element.on('keyup', function() {
                var _this = angular.element(this);
                _this = _this[0];
                calcheight(_this)
            }).addClass('noscroll');
            $timeout(function() {
                rowHeight = element[0].offsetHeight;
                calcheight(element[0])
            });

            function calcheight(elem) {
                elem.rows = 1;
                while (elem.scrollHeight > elem.offsetHeight) {
                    elem.rows++
                }
                $timeout(function() {})
            }
        }
    }
}]).directive('timepick', ['helper', function(helper) {
    return {
        restrict: 'E',
        link: function(scope, element, attrs) {
            var clickedElement = !1;
            scope.timeH = [];
            scope.timeM = [];
            scope.temp = {};
            scope.temp.align = attrs.nalign ? attrs.nalign : '';
            create_hours();
            if (scope.note.obj.rows[attrs.nindex][attrs.nmodel] == '') {
                scope.timeH[0].primaryH = !0;
                scope.timeM[0].primaryM = !0
            } else {
                var res = scope.note.obj.rows[attrs.nindex][attrs.nmodel].split(":");
                for (x in scope.timeH) {
                    if (scope.timeH[x].id == parseInt(res[0])) {
                        scope.timeH[x].primaryH = !0;
                        break
                    }
                }
                for (x in scope.timeM) {
                    if (scope.timeM[x].id == parseInt(res[1])) {
                        scope.timeM[x].primaryM = !0;
                        break
                    }
                }
            }
            element.on("mousedown", function(e) {
                clickedElement = !0;
                e.stopPropagation()
            });
            element.on('mouseup', function(e) {
                clickedElement = !1
            });
            angular.element(document).mousedown(function(e) {
                if (!clickedElement) {
                    angular.element('.timepicker').find('timepick').remove()
                }
            });

            function create_hours() {
                scope.timeH = [];
                scope.timeM = [];
                for (var i = 0; i < 24; i++) {
                    var line = {
                        'id': i,
                        'name': i
                    };
                    scope.timeH.push(line)
                }
                for (var i = 0; i < 60; i = i + 5) {
                    var line = {
                        'id': i,
                        'name': i
                    };
                    scope.timeM.push(line)
                }
            }
            scope.setHours = function(item) {
                if (attrs.nmodel != 'line_h') {
                    scope.note.obj.rows[attrs.nindex].line_h = '0:00';
                    scope.note.obj.rows[attrs.nindex].line_h_wo = '0';
                    scope.note.obj.rows[attrs.nindex].is_total_h = !1
                }
                for (x in scope.timeH) {
                    if (scope.timeH[x].primaryH) {
                        scope.timeH[x].primaryH = !1
                    }
                }
                item.primaryH = !0;
                if (scope.note.obj.rows[attrs.nindex][attrs.nmodel] == '') {
                    scope.note.obj.rows[attrs.nindex][attrs.nmodel] = item.id + ":00"
                } else {
                    var splitted = scope.note.obj.rows[attrs.nindex][attrs.nmodel].split(":");
                    scope.note.obj.rows[attrs.nindex][attrs.nmodel] = item.id + ":" + splitted[1]
                }
                if (attrs.nmodel == 'line_h') {
                    scope.note.changeTotalHoursLine(attrs.nindex)
                } else {
                    scope.note.savepickerData(attrs.nindex)
                }
            }
            scope.setMinutes = function(item) {
                if (attrs.nmodel != 'line_h') {
                    scope.note.obj.rows[attrs.nindex].line_h = '0:00';
                    scope.note.obj.rows[attrs.nindex].line_h_wo = '0';
                    scope.note.obj.rows[attrs.nindex].is_total_h = !1
                }
                for (x in scope.timeM) {
                    if (scope.timeM[x].primaryM) {
                        scope.timeM[x].primaryM = !1
                    }
                }
                item.primaryM = !0;
                if (scope.note.obj.rows[attrs.nindex][attrs.nmodel] == '') {
                    if (item.id < 10) {
                        scope.note.obj.rows[attrs.nindex][attrs.nmodel] = '00:0' + item.id
                    } else {
                        scope.note.obj.rows[attrs.nindex][attrs.nmodel] = '00:' + item.id
                    }
                } else {
                    var splitted = scope.note.obj.rows[attrs.nindex][attrs.nmodel].split(":");
                    if (item.id < 10) {
                        scope.note.obj.rows[attrs.nindex][attrs.nmodel] = splitted[0] + ":0" + item.id
                    } else {
                        scope.note.obj.rows[attrs.nindex][attrs.nmodel] = splitted[0] + ":" + item.id
                    }
                }
                if (attrs.nmodel == 'line_h') {
                    scope.note.changeTotalHoursLine(attrs.nindex)
                } else {
                    scope.note.savepickerData(attrs.nindex)
                }
            }
        },
        templateUrl: 'miscTemplate/timepick'
    }
}]).directive('timepicker', ['helper', function(helper) {
    return {
        restrict: 'E',
        link: function(scope, element, attrs) {
            var clickedElement = !1;
            scope.timeH = [];
            scope.timeM = [];
            scope.temp = {};
            scope.temp.align = attrs.nalign ? attrs.nalign : '';
            create_hours();
            if (scope.item.rows[attrs.nindex][attrs.nmodel] == '') {
                scope.timeH[0].primaryH = !0;
                scope.timeM[0].primaryM = !0
            } else {
                var res = scope.item.rows[attrs.nindex][attrs.nmodel].split(":");
                for (x in scope.timeH) {
                    if (scope.timeH[x].id == parseInt(res[0])) {
                        scope.timeH[x].primaryH = !0;
                        break
                    }
                }
                for (x in scope.timeM) {
                    if (scope.timeM[x].id == parseInt(res[1])) {
                        scope.timeM[x].primaryM = !0;
                        break
                    }
                }
            }
            element.on("mousedown", function(e) {
                clickedElement = !0;
                e.stopPropagation()
            });
            element.on('mouseup', function(e) {
                clickedElement = !1
            });
            angular.element(document).mousedown(function(e) {
                if (!clickedElement) {
                    angular.element('.timesheet_day').find('timepicker').remove()
                }
            });

            function create_hours() {
                scope.timeH = [];
                scope.timeM = [];
                for (var i = 0; i < 24; i++) {
                    var line = {
                        'id': i,
                        'name': i
                    };
                    scope.timeH.push(line)
                }
                for (var i = 0; i < 60; i = i + 5) {
                    var line = {
                        'id': i,
                        'name': i
                    };
                    scope.timeM.push(line)
                }
            }
            scope.setHours = function(item) {
                if (attrs.nmodel != 'line_h') {
                    scope.item.rows[attrs.nindex].line_h = '0:00';
                    scope.item.rows[attrs.nindex].line_h_wo = '0';
                    scope.item.rows[attrs.nindex].is_total_h = !1
                }
                for (x in scope.timeH) {
                    if (scope.timeH[x].primaryH) {
                        scope.timeH[x].primaryH = !1
                    }
                }
                item.primaryH = !0;
                if (scope.item.rows[attrs.nindex][attrs.nmodel] == '') {
                    scope.item.rows[attrs.nindex][attrs.nmodel] = item.id + ":00"
                } else {
                    var splitted = scope.item.rows[attrs.nindex][attrs.nmodel].split(":");
                    scope.item.rows[attrs.nindex][attrs.nmodel] = item.id + ":" + splitted[1]
                }
                if (attrs.nmodel == 'line_h') {
                    scope.changeTotalHoursLine(attrs.nindex)
                } else {
                    scope.savepickerData(attrs.nindex)
                }
            }
            scope.setMinutes = function(item) {
                if (attrs.nmodel != 'line_h') {
                    scope.item.rows[attrs.nindex].line_h = '0:00';
                    scope.item.rows[attrs.nindex].line_h_wo = '0';
                    scope.item.rows[attrs.nindex].is_total_h = !1
                }
                for (x in scope.timeM) {
                    if (scope.timeM[x].primaryM) {
                        scope.timeM[x].primaryM = !1
                    }
                }
                item.primaryM = !0;
                if (scope.item.rows[attrs.nindex][attrs.nmodel] == '') {
                    if (item.id < 10) {
                        scope.item.rows[attrs.nindex][attrs.nmodel] = '00:0' + item.id
                    } else {
                        scope.item.rows[attrs.nindex][attrs.nmodel] = '00:' + item.id
                    }
                } else {
                    var splitted = scope.item.rows[attrs.nindex][attrs.nmodel].split(":");
                    if (item.id < 10) {
                        scope.item.rows[attrs.nindex][attrs.nmodel] = splitted[0] + ":0" + item.id
                    } else {
                        scope.item.rows[attrs.nindex][attrs.nmodel] = splitted[0] + ":" + item.id
                    }
                }
                if (attrs.nmodel == 'line_h') {
                    scope.changeTotalHoursLine(attrs.nindex)
                } else {
                    scope.savepickerData(attrs.nindex)
                }
            }
        },
        templateUrl: 'miscTemplate/timepick'
    }
}]).directive('expandText', ['$timeout', function($timeout) {
    return {
        restrict: "A",
        scope: {
            show: '&onShow',
            ngBindHtml: '='
        },
        link: function(scope, element, attr) {
            scope.$watch('ngBindHtml', function(newValue, oldValue) {
                if (element[0].scrollHeight > element[0].clientHeight) {
                    scope.show()
                }
            })
        }
    }
}]).directive('timepicknew', ['helper', function(helper) {
    return {
        restrict: 'E',
        link: function(scope, element, attrs) {
            var clickedElement = !1;
            scope.timeH = [];
            scope.timeM = [];
            scope.temp = {};
            scope.temp.align = attrs.nalign ? attrs.nalign : '';
            create_hours();
            if (scope.edit[attrs.nparent][attrs.nmodel] == '' || scope.edit[attrs.nparent][attrs.nmodel] == undefined) {
                scope.timeH[0].primaryH = !0;
                scope.timeM[0].primaryM = !0
            } else {
                var res = scope.edit[attrs.nparent][attrs.nmodel].split(":");
                for (x in scope.timeH) {
                    if (scope.timeH[x].id == parseInt(res[0])) {
                        scope.timeH[x].primaryH = !0;
                        break
                    }
                }
                for (x in scope.timeM) {
                    if (scope.timeM[x].id == parseInt(res[1])) {
                        scope.timeM[x].primaryM = !0;
                        break
                    }
                }
            }
            element.on("mousedown", function(e) {
                clickedElement = !0;
                e.stopPropagation()
            });
            element.on('mouseup', function(e) {
                clickedElement = !1
            });
            angular.element(document).mousedown(function(e) {
                if (!clickedElement) {
                    angular.element('body').find('timepicknew').remove()
                }
            });

            function create_hours() {
                scope.timeH = [];
                scope.timeM = [];
                for (var i = 0; i < 24; i++) {
                    var line = {
                        'id': i,
                        'name': i
                    };
                    scope.timeH.push(line)
                }
                for (var i = 0; i < 60; i = i + 5) {
                    var line = {
                        'id': i,
                        'name': i
                    };
                    scope.timeM.push(line)
                }
            }
            scope.setHours = function(item) {
                for (x in scope.timeH) {
                    if (scope.timeH[x].primaryH) {
                        scope.timeH[x].primaryH = !1
                    }
                }
                item.primaryH = !0;
                if (scope.edit[attrs.nparent][attrs.nmodel] == '' || scope.edit[attrs.nparent][attrs.nmodel] == undefined) {
                    scope.edit[attrs.nparent][attrs.nmodel] = item.id + ":00"
                } else {
                    var splitted = scope.edit[attrs.nparent][attrs.nmodel].split(":");
                    scope.edit[attrs.nparent][attrs.nmodel] = item.id + ":" + splitted[1]
                }
            }
            scope.setMinutes = function(item) {
                for (x in scope.timeM) {
                    if (scope.timeM[x].primaryM) {
                        scope.timeM[x].primaryM = !1
                    }
                }
                item.primaryM = !0;
                if (scope.edit[attrs.nparent][attrs.nmodel] == '' || scope.edit[attrs.nparent][attrs.nmodel] == undefined) {
                    if (item.id < 10) {
                        scope.edit[attrs.nparent][attrs.nmodel] = '00:0' + item.id
                    } else {
                        scope.edit[attrs.nparent][attrs.nmodel] = '00:' + item.id
                    }
                } else {
                    var splitted = scope.edit[attrs.nparent][attrs.nmodel].split(":");
                    if (item.id < 10) {
                        scope.edit[attrs.nparent][attrs.nmodel] = splitted[0] + ":0" + item.id
                    } else {
                        scope.edit[attrs.nparent][attrs.nmodel] = splitted[0] + ":" + item.id
                    }
                }
            }
        },
        templateUrl: 'miscTemplate/timepicker'
    }
}]).directive('planning', ['helper', function(helper) {
    return {
        restrict: 'E',
        link: function(scope, element, attrs) {
            scope.temp_plan = {};
            scope.temp_plan.title = scope.activeType == 3 ? scope.planning.user_data[attrs.pindex].data[attrs.cindex].day_txt + ' ' + scope.planning.month_txt : scope.planning.days_nr[attrs.cindex].day_name + ' ' + scope.planning.days_nr[attrs.cindex].date_month;
            scope.temp_plan.block_day = scope.planning.user_data[attrs.pindex].data[attrs.cindex].block_day;
            if (scope.activeType == 3) {
                scope.temp_plan.align = parseInt(attrs.cindex) + 1 < 15 ? 'right_align' : 'left_align'
            } else {
                scope.temp_plan.align = parseInt(attrs.cindex) < 4 ? 'right_align' : 'left_align'
            }
            scope.temp_plan.for_user = attrs.nuser;
            var clickedElement = !1;
            element.on("mousedown", function(e) {
                clickedElement = !0;
                e.stopPropagation()
            });
            element.on('mouseup', function(e) {
                clickedElement = !1
            });
            angular.element(document).mousedown(function(e) {
                if (!clickedElement) {
                    angular.element('body').find('planning').remove()
                }
            });
            scope.updateBlockDay = function() {
                scope.planning.user_data[attrs.pindex].data[attrs.cindex].block_day = scope.temp_plan.block_day;
                var obj = {};
                obj.do = 'maintenance--team-blockDay';
                obj.date = scope.planning.user_data[attrs.pindex].data[attrs.cindex].tmsp;
                obj.user_id = scope.planning.user_data[attrs.pindex].user_id;
                obj.value = scope.temp_plan.block_day;
                helper.doRequest('post', 'index.php', obj, function(r) {
                    scope.showAlerts(r)
                })
            }
        },
        templateUrl: 'miscTemplate/resourceblock'
    }
}]).directive('myDragg', [function() {
    return {
        restrict: 'A',
        link: function(scope, el, attrs) {
            if (attrs.draggable == 'true') {
                el.bind("dragstart", function(e) {
                    e.originalEvent.dataTransfer.setData('id', e.target.id);
                    e.originalEvent.dataTransfer.setData('line', attrs.nuser);
                    e.originalEvent.dataTransfer.setData('day', attrs.nday);
                    e.originalEvent.dataTransfer.setData('origin_id', angular.element('#' + e.target.id).parent('.planning_cell').attr('id'))
                })
            }
        }
    }
}]).directive('myDrop', [function() {
    return {
        restrict: 'A',
        scope: {
            onDrop: '&'
        },
        link: function(scope, el, attrs) {
            el.bind("dragover", function(e) {
                if (e.preventDefault) {
                    e.preventDefault()
                }
                e.originalEvent.dataTransfer.dropEffect = 'move'
            });
            el.bind("drop", function(e) {
                var origin = angular.element('#' + e.originalEvent.dataTransfer.getData("origin_id"));
                var target = angular.element('#' + e.delegateTarget.id);
                var line = e.originalEvent.dataTransfer.getData("line");
                if (line != attrs.nuser || e.delegateTarget.id == e.originalEvent.dataTransfer.getData("origin_id")) {
                    e.preventDefault();
                    e.stopPropagation();
                    return !1
                }
                var id = e.originalEvent.dataTransfer.getData("id");
                var src = document.getElementById(id);
                if (target.children().length) {
                    target.find('div:first-child').addClass('hidden')
                }
                target.append(src);
                if (origin.children().length) {
                    origin.find('div:first-child').removeClass('hidden')
                }
                var origin_tmsp = parseInt(e.originalEvent.dataTransfer.getData("origin_id").substring((e.originalEvent.dataTransfer.getData("origin_id").search("_")) + 1));
                var dest_tmsp = parseInt(e.delegateTarget.id.substring((e.delegateTarget.id.search("_")) + 1));
                scope.onDrop({
                    line: parseInt(line),
                    origin_tmsp: origin_tmsp,
                    dest_tmsp: dest_tmsp
                })
            })
        }
    }
}]).directive('generateLink', ['helper', function(helper) {
    return {
        restrict: "A",
        scope: {
            val: "=ngModel"
        },
        link: function(scope, element, attr) {
            element.on('blur', function() {
                var elem = scope.val.trim();
                if (elem.indexOf('https') != -1) {
                    var new_val = elem
                } else if (elem.indexOf('http') == -1 && elem.length) {
                    var new_val = 'http://' + elem
                } else {
                    var new_val = elem
                }
                scope.val = new_val
            })
        }
    }
}]).directive('ngClickCopy',['$window','$timeout','helper',function ($window,$timeout,helper){
        return{
            restrict: "A",
            link: function(scope,element,attr){
                element.on('click',function(){
                    angular.element(element[0]).closest('table').find('i.fa-check.text-success').remove();
                    var body = angular.element($window.document.body);
                    var textarea = angular.element('<textarea/>');
                    textarea.css({
                        position: 'fixed',
                        opacity: '0'
                    });
                    textarea.val(attr.ngClickCopy);
                    body.append(textarea);
                    textarea[0].select();
                    try {
                        var successful = document.execCommand('copy');
                        angular.element(element[0]).after(' <i class="fa fa-check text-success copy-clipboard-message">'+helper.getLanguage('<span>Copied to clipboard</span>')+'</i>');
                        $timeout(function() {
                            angular.element(element[0]).next().remove();
                        }, 3000);
                    } catch (err) {
                        //
                    }
                    textarea.remove();
                });
            }
        }
    }]).directive('ngMouseWheelUp', function() {
    return function(scope, element, attrs) {
        element.bind("mousewheel onmousewheel", function(event) {
            var delta = Math.max(-1, Math.min(1, (event.deltaY || -event.detail)));
            if (delta > 0) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngMouseWheelUp)
                });
                event.returnValue = !1;
                if (event.preventDefault) {
                    event.preventDefault()
                }
            }
        })
    }
}).directive('ngMouseWheelDown', function() {
    return function(scope, element, attrs) {
        element.bind("mousewheel onmousewheel", function(event) {
            var delta = Math.max(-1, Math.min(1, (event.deltaY || -event.detail)));
            if (delta < 0) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngMouseWheelDown)
                });
                event.returnValue = !1;
                if (event.preventDefault) {
                    event.preventDefault()
                }
            }
        })
    }
}).directive('navigator', ['$state', 'helper', 'list', function($state, helper, list) {
    return {
        restrict: 'E',
        scope: {},
        link: function(scope, element, attrs) {
            scope.showNavigation = !1;
            scope.total_items = 1;
            scope.start_item = 1;
            scope.changeIndex = !1;
            scope.isDealView=attrs.originalpage=="dealView" ? true : false;
			scope.dealStageName="";
            var list = angular.copy(helper.getItemSimple(attrs.pag)),
                params = {};
            var l_r = attrs.pag == 'customers-contacs' ? 28 : 30;
            if (list === undefined) {
                if (attrs.originalpage == 'article.add') {
                    params.original_page = attrs.pag == 'article-articles' ? 'article.add' : 'service.add'
                } else {
                    params.original_page = attrs.originalpage
                }
            } else {
                var excludes = ['quote-quotes'];
                if (list.total_items !== undefined) {
                    scope.total_items = list.total_items
                }
                if (list && list.search) {
                    for (x in list.search) {
                        if (x.indexOf('date_js') > -1) {
                            list.search[x] = new Date(list.search[x])
                        }
                        params[x] = list.search[x]
                    }
                }
                if (list.cindex !== undefined) {
                    scope.changeIndex = !0;
                    params.cindex = list.cindex + 1;
                    scope.start_item = (params.offset - 1) * l_r + list.cindex + 1
                }
                if (attrs.originalpage == 'article.add') {
                    params.original_page = attrs.pag == 'article-articles' ? 'article.add' : 'service.add'
                } else {
                    params.original_page = attrs.originalpage
                }
            }
            params.do = 'misc-navigator';
            params.original_id = attrs.itemid;

            if(sessionStorage.getItem(params.original_page + '_list')){
                params.nav_list = JSON.parse(sessionStorage.getItem(params.original_page + '_list'));
                params.nav_total = sessionStorage.getItem(params.original_page + '_total');
                 
                helper.doRequest('post', 'index.php', params, function(r) {
                    if (r.data && r.data.total_items) {

                        scope.showNavigation = !0;
                        scope.total_items = r.data.total_items;
                        scope.prev_item = r.data.prev_item;
                        scope.next_item = r.data.next_item;
                        scope.start_item = r.data.start_item;
                        if(scope.isDealView){
                            scope.dealStageName=r.data.stage_name;
                        }
                    }
                }); 
            }else{
                scope.showNavigation = 0;
            }

            if(params.original_page=='timesheetView' ){
               helper.doRequest('post', 'index.php', params, function(r) {
                    if (r.data && r.data.total_items) {

                        scope.showNavigation = !0;
                        scope.total_items = r.data.total_items;
                        scope.prev_item = r.data.prev_item;
                        scope.next_item = r.data.next_item;
                        scope.start_item = r.data.start_item;
                        if(scope.isDealView){
                            scope.dealStageName=r.data.stage_name;
                        }
                    }
                });  
            }

            
            scope.goBack = function() {
                if (scope.start_item == 1) {
                    return !1
                }
                if (scope.changeIndex) {
                    helper.setItemSimple(attrs.pag, 'cindex', list.cindex - 1)
                }
                //angular.element('.loading_wrap').removeClass('hidden');
                switch (attrs.originalpage) {
                    case 'customerView':
                    case 'customer':
                        $state.go(attrs.originalpage, {
                            'customer_id': scope.prev_item
                        });
                        break;
                    case 'contactView':
                    case 'contact':
                        $state.go(attrs.originalpage, {
                            'contact_id': scope.prev_item
                        });
                        break;
                    case 'article.add':
                        if (attrs.pag == 'article-articles') {
                            $state.go(attrs.originalpage, {
                                'article_id': scope.prev_item
                            })
                        } else {
                            $state.go(attrs.originalpage, {
                                'article_id': scope.prev_item,
                                'form_type': 'service'
                            })
                        }
                        break;
                    case 'quote.view':
                        $state.go(attrs.originalpage, {
                            'quote_id': scope.prev_item,
                            'version_id': undefined
                        });
                        break;
                    case 'order.view':
                        $state.go(attrs.originalpage, {
                            'order_id': scope.prev_item
                        }, {
                            reload: !0
                        });
                        break;
                    case 'purchase_order.view':
                        $state.go(attrs.originalpage, {
                            'p_order_id': scope.prev_item
                        }, {
                            reload: !0
                        });
                        break;
                    case 'service.edit':
                    case 'service.view':
                        $state.go(attrs.originalpage, {
                            'service_id': scope.prev_item
                        });
                        break;
                    case 'project.edit':
                        $state.go(attrs.originalpage, {
                            'project_id': scope.prev_item
                        });
                        break;
                    case 'installation.view':
                        $state.go(attrs.originalpage, {
                            'installation_id': scope.prev_item
                        });
                        break;
                    case 'contract.view':
                        $state.go(attrs.originalpage, {
                            'contract_id': scope.prev_item,
                            'version_id': undefined
                        });
                        break;
                    case 'invoice.view':
                        $state.go(attrs.originalpage, {
                            'invoice_id': scope.prev_item
                        });
                        break;
                    case 'recinvoice.edit':
                    case 'recinvoice.view':
                        $state.go(attrs.originalpage, {
                            'recurring_invoice_id': scope.prev_item
                        });
                        break;
                    case 'purchase_invoice.view':
                        $state.go(attrs.originalpage, {
                            'invoice_id': scope.prev_item
                        });
                        break;
                    case 'dealView':
                        $state.go(attrs.originalpage, {
                            'opportunity_id': scope.prev_item
                        });
                        break;
                    case 'timesheetView':
                        var fields = scope.prev_item.split('-');
                        $state.go(attrs.originalpage, {
                            'user_id': fields[0],
                            'start': fields[1]
                        });
                        break;
                    case 'user.edit':
                        $state.go('user', {
                            'user_id': scope.prev_item
                        });
                        break;
                }
            }
            scope.goNext = function() {
                if (scope.start_item == scope.total_items) {
                    return !1
                }
                if (scope.changeIndex) {
                    helper.setItemSimple(attrs.pag, 'cindex', list.cindex + 1)
                }
                //angular.element('.loading_wrap').removeClass('hidden');
                switch (attrs.originalpage) {
                    case 'customerView':
                    case 'customer':
                        $state.go(attrs.originalpage, {
                            'customer_id': scope.next_item
                        });
                        break;
                    case 'contactView':
                    case 'contact':
                        $state.go(attrs.originalpage, {
                            'contact_id': scope.next_item
                        });
                        break;
                    case 'article.add':
                        if (attrs.pag == 'article-articles') {
                            $state.go(attrs.originalpage, {
                                'article_id': scope.next_item
                            })
                        } else {
                            $state.go(attrs.originalpage, {
                                'article_id': scope.next_item,
                                'form_type': 'service'
                            })
                        }
                        break;
                    case 'quote.view':
                        $state.go(attrs.originalpage, {
                            'quote_id': scope.next_item,
                            'version_id': undefined
                        });
                        break;
                    case 'order.view':
                        $state.go(attrs.originalpage, {
                            'order_id': scope.next_item
                        }, {
                            reload: !0
                        });
                        break;
                    case 'purchase_order.view':
                        $state.go(attrs.originalpage, {
                            'p_order_id': scope.next_item
                        }, {
                            reload: !0
                        });
                        break;
                    case 'service.edit':
                    case 'service.view':
                        $state.go(attrs.originalpage, {
                            'service_id': scope.next_item
                        });
                        break;
                    case 'project.edit':
                        $state.go(attrs.originalpage, {
                            'project_id': scope.next_item
                        });
                        break;
                    case 'installation.view':
                        $state.go(attrs.originalpage, {
                            'installation_id': scope.next_item
                        });
                        break;
                    case 'contract.view':
                        $state.go(attrs.originalpage, {
                            'contract_id': scope.next_item,
                            'version_id': undefined
                        });
                        break;
                    case 'invoice.view':
                        $state.go(attrs.originalpage, {
                            'invoice_id': scope.next_item
                        });
                        break;
                    case 'recinvoice.edit':
                    case 'recinvoice.view':
                        $state.go(attrs.originalpage, {
                            'recurring_invoice_id': scope.next_item
                        });
                        break;
                    case 'purchase_invoice.view':
                        $state.go(attrs.originalpage, {
                            'invoice_id': scope.next_item
                        });
                        break;
                    case 'dealView':
                        $state.go(attrs.originalpage, {
                            'opportunity_id': scope.next_item
                        });
                        break;
                    case 'timesheetView':
                        var fields = scope.next_item.split('-');
                        $state.go(attrs.originalpage, {
                            'user_id': fields[0],
                            'start': fields[1]
                        });
                        break;
                    case 'user.edit':
                        $state.go('user', {
                            'user_id': scope.next_item
                        });
                        break;
                }
            }
        },
        templateUrl: 'miscTemplate/navigator'
    }
}]).directive('saveSpin', ['$rootScope','helper', function($rootScope, helper) {
    return {
        restrict: "A",
        scope: {},
        link: function(scope, element, attr) {
            setTimeout(function(){
                scope.btn_text=element.text();
                scope.btn_width=element.width();
                scope.btn_pressed=false;
                scope.text_after=attr.afterSave ? attr.afterSave : scope.btn_text;
                element.on('click', function(event) {
                    if(scope.btn_pressed){
                        event.preventDefault();
                        event.stopImmediatePropagation();
                    }else{
                        angular.element('.loading_wrap').removeClass('hidden');
                        scope.btn_pressed=true;
                        element.attr('disabled', true); 
                        element.addClass('spinning disabled_btn');
                        element.text('').append('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>').width(scope.btn_width);
                    }                 
                });
                $rootScope.$on('dataLoaded', function(event, data) {
                    if(scope.btn_pressed){
                        var element_exists=angular.element('.spinning.disabled_btn');
                        if(element_exists.length){
                            //scope.btn_pressed=false;
                            if(data){
                                element_exists.removeClass('spinning');                          
                            }else{
                                element_exists.removeClass('spinning disabled_btn');
                                element.attr('disabled', false);
                                scope.btn_pressed=false;
                            }
                            element_exists.text((data ? scope.text_after : scope.btn_text));
                        }
                    }               
                });
                $rootScope.$on('dataChanged', function(event, data) {
                    if(scope.btn_pressed){
                        var element_exists=angular.element('.disabled_btn');
                        if(element_exists.length){
                            scope.btn_pressed=false;
                            element.attr('disabled', false);
                            element_exists.removeClass('disabled_btn');
                            element_exists.text(helper.getLanguage('Save'));
                        }
                    }               
                });
            },500); 
        }
    }
}]).directive('headerModule', ['$interval', '$rootScope', 'UserService', 'helper', function($interval, $rootScope,UserService, helper) {
    return {
        restrict: 'E',
        scope: {},
        link: function(scope, element, attrs) {
            scope.isLoged = !1;
            scope.show_settings={};
            scope.menu = helper.menu;
            scope.main_menu = helper.main_menu;
            scope.nextState="";
            scope.nr_show=true;
            scope.amount_o="";
            scope.total_amount_p="";
            scope.amount_s="";
            scope.invoice_tab="";
            $interval(function() {
                scope.isLoged = UserService.isLoggedIn;
                scope.show_settings=$rootScope.show_settings;
                scope.menu = helper.menu;
                scope.main_menu = helper.main_menu;
                if($rootScope['nextState'] && $rootScope['nextState']['name']){
                    scope.nextState=$rootScope.nextState.name;
                    if(scope.nextState!='invoices'){
                        scope.invoice_tab='';
                    }
                }
                if($rootScope.nr_show != undefined){
                    scope.nr_show=$rootScope.nr_show;
                }
                if($rootScope.amount_o != undefined){
                    scope.amount_o=$rootScope.amount_o;
                }
                if($rootScope.total_amount_p != undefined){
                    scope.total_amount_p=$rootScope.total_amount_p;
                }
                if($rootScope.amount_s != undefined){
                    scope.amount_s=$rootScope.amount_s;
                }               
            }, 100, !1);
            scope.setInvoiceTab=function(tab){
                scope.invoice_tab=tab;
            }
        },
        templateUrl: 'miscTemplate/headerModule'
    }
}]).directive('inputRed', ['helper', function(helper) {
    return {
        restrict: "A",
        scope: { input : '=inputRed', val: '=ngModel' },
        link: function(scope, element, attr) {
            var inputValue=parseFloat(helper.return_value(scope.val),10);
            if(inputValue == 0){
                scope.input.red_input=true;
            }
            element.on('change', function(event) {
                if(parseFloat(helper.return_value(scope.val),10) == 0){
                    scope.input.red_input=true;
                }else{
                    scope.input.red_input=false;
                }                
            });
        }
    }
}]).directive('focusMe', function($timeout) {
    return {
        scope: { trigger: '=focusMe' },
        link: function(scope, element) {
            scope.$watch('trigger', function(value) {
                if(value === true) { 
                    $timeout(function(){
                        element[0].focus();
                    },100);                
                }
            });
        }
    };
}).directive('nopage', ['$state', 'helper', 'list', function($state, helper, list) {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: 'miscTemplate/nopage'
    }
}]).directive('notification', ['$state', '$rootScope', '$window','$uibModal','$uibModalStack','$timeout','helper', 'modalFc',function($state, $rootScope, $window, $uibModal, $uibModalStack, $timeout, helper, modalFc) {
    return {
        restrict: 'E',
        scope: {},
        transclude: !0,
        link: function(scope, element, attrs) {

            scope.list=[];
            scope.msg_nr=0;

            scope.renderList=function(data){
                if(data && data.data){
                    scope.list = data.data.list;
                    scope.msg_nr=data.data.max_rows;
                }
            }

            scope.getList=function(){
                helper.doRequest('get', 'index.php?do=misc-notifications', scope.renderList);
            }

            scope.initSocket=function(){
                helper.doRequest('get', 'index.php?do=misc-notice&xget=status', function(r) {
                    if (r && r.data && r.data.allow_socket && !$rootScope.ws_connected) {

                        const socket = io(r.data.ws_url,{
                            auth:{'hash':r.data.hash}
                        });

                        socket.on("connect", () => {
                            $rootScope.ws_connected=true;
                        });

                        socket.on("notification_received", (data) => {
                            scope.getList();
                        });

                        socket.on("connect_error", () => {
                            //console.log('connection error');
                        });

                        socket.on("disconnect", () => {
                            //console.log('disconnected');
                        }); 
                    }
                });
            }

            scope.initSocket();
            scope.getList();

            scope.showNotifications=function(){
                let modalInstance = $uibModal.open({
                    animation: !0,
                    templateUrl: 'miscTemplate/notifications',
                    windowClass: 'right',
                    controller: function($scope, $uibModalInstance, data){
                        var vm = this;
                        vm.obj = data;

                        vm.cancel = function() {
                            $uibModalInstance.close()
                        }
                    },
                    controllerAs: 'vm',
                    size: 'lg',
                    resolve: {
                        data: function() {
                            return scope
                        }
                    }
                });
                modalInstance.rendered.then(function() {
                    $timeout(function() {
                        $uibModalStack.getTop().value.modalDomEl.attr('id', 'myModal2');
                    })
                })
            }         

            scope.updateReadState=function(item){
                item.read = !item.read;
                item.selected = item.read ? '' : item.id;
                helper.doRequest('post','index.php',{'do':'misc--notification-updateRead','id':item.id,'msg_read':item.read},function(r){
                    if(r.success){
                        scope.msg_nr = (item.read ? parseInt(scope.msg_nr)-1 : parseInt(scope.msg_nr)+1);
                    }
                });
            }

            scope.deleteNotification=function(index){
                helper.doRequest('post','index.php',{'do':'misc--notification-delete','id':scope.list[index].id},function(r){
                    if(r.success){
                        scope.msg_nr=parseInt(scope.msg_nr)-1;
                        scope.list.splice(index,1);
                    }
                });
            }   

            scope.updateReadAll=function(value){
                helper.doRequest('post','index.php',{'do':'misc--notification-updateReadAll','value':value},function(r){
                    if(r.success){
                        scope.msg_nr=r.data.msg_nr;
                        for(let x in scope.list){
                            scope.list[x].read=value == 1 ? true : false;
                            scope.list[x].selected=value == 1 ? '' :  scope.list[x].id;
                        }
                    }
                });
            }  

            scope.deleteAll=function(){
                helper.doRequest('post','index.php',{'do':'misc--notification-deleteAll'},function(r){
                    if(r.success){
                        scope.msg_nr=0;
                        scope.list=[];
                    }
                });
            }

            scope.showMore=function(item){
                if(item.open_modal){
                    openModal('failed_export', item.modal_params, 'lg')
                }else{
                    $window.open(item.link, '_blank');
                }
            }

            function openModal(type, item, size) {
                switch (type) {
                    case 'failed_export':
                        var iTemplate = 'FailedExport';
                        var ctas = 'vm';
                        break
                }
                var params = {
                    template: 'iTemplate/' + iTemplate + 'Modal',
                    ctrl: iTemplate + 'ModalCtrl',
                    ctrlAs: ctas,
                    size: size
                };
                switch (type) {
                    case 'failed_export':
                        params.backdrop='static';
                        params.params = {
                            'do': 'invoice-export_fail',
                            'app': item.app
                        };
                        break
                }
                modalFc.open(params)
            }
            
        },
        template: '<li class="user_options-link">'+
                    '<div class="text-center">'+
                        '<button type="button" class="btn btn-akti" uib-tooltip="'+helper.getLanguage('Notifications')+'" tooltip-placement="bottom" ng-click="showNotifications()">'+
                            '<i class="fas fa-bell"></i><span class="bubble-holder header_task_number" ng-if="msg_nr > 0"><ins>{{msg_nr}}</ins></span>'+
                        '</button>'+
                    '</div>'+
                '</li>'
    }
}]).directive('signaturePad',['$interval', '$timeout', '$window',
    function ($interval, $timeout, $window) {
        var signaturePad, element, EMPTY_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjgAAADcCAQAAADXNhPAAAACIklEQVR42u3UIQEAAAzDsM+/6UsYG0okFDQHMBIJAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcCQADAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDkQAwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAegeayZAN3dLgwnAAAAAElFTkSuQmCC';

        return {
            restrict: 'EA',
            replace: true,
            template: '<div class="signature" style="width: 100%; max-width:{{width}}px; height: 100%; max-height:{{height}}px;"><canvas style="display: block; margin: 0 auto;" ng-mouseup="onMouseup()" ng-mousedown="notifyDrawing({ drawing: true })"></canvas></div>',
            scope: {
                accept: '=?',
                clear: '=?',
                disabled: '=?',
                dataurl: '=?',
                height: '@',
                width: '@',
                notifyDrawing: '&onDrawing',
            },
            controller: [
                '$scope',
                function ($scope) {
                    $scope.accept = function () {

                        return {
                            isEmpty: $scope.dataurl === EMPTY_IMAGE,
                            dataUrl: $scope.dataurl
                        };
                    };

                    $scope.onMouseup = function () {
                        $scope.updateModel();

                        // notify that drawing has ended
                        $scope.notifyDrawing({ drawing: false });
                    };

                    $scope.updateModel = function () {
                        /*
                         defer handling mouseup event until $scope.signaturePad handles
                         first the same event
                         */
                        $timeout().then(function () {
                            $scope.dataurl = $scope.signaturePad.isEmpty() ? EMPTY_IMAGE : $scope.signaturePad.toDataURL();
                        });
                    };

                    $scope.clear = function () {
                        $scope.signaturePad.clear();
                        $scope.dataurl = EMPTY_IMAGE;
                    };

                    $scope.$watch("dataurl", function (dataUrl) {
                        if (!dataUrl || $scope.signaturePad.toDataURL() === dataUrl) {
                            return;
                        }

                        $scope.setDataUrl(dataUrl);
                    });
                }
            ],
            link: function (scope, element, attrs) {
                var canvas = element.find('canvas')[0];
                var parent = canvas.parentElement;
                var scale = 0;
                var ctx = canvas.getContext('2d');

                var width = parseInt(scope.width, 10);
                var height = parseInt(scope.height, 10);

                canvas.width = width;
                canvas.height = height;

                scope.signaturePad = new SignaturePad(canvas);

                scope.setDataUrl = function(dataUrl) {
                    var ratio = Math.max(window.devicePixelRatio || 1, 1);

                    ctx.setTransform(1, 0, 0, 1, 0, 0);
                    ctx.scale(ratio, ratio);

                    scope.signaturePad.clear();
                    scope.signaturePad.fromDataURL(dataUrl);

                    $timeout().then(function() {
                        ctx.setTransform(1, 0, 0, 1, 0, 0);
                        ctx.scale(1 / scale, 1 / scale);
                    });
                };

                scope.$watch('disabled', function (val) {
                    val ? scope.signaturePad.off() : scope.signaturePad.on();
                });

                var calculateScale = function() {
                    var scaleWidth = Math.min(parent.clientWidth / width, 1);
                    var scaleHeight = Math.min(parent.clientHeight / height, 1);

                    var newScale = Math.min(scaleWidth, scaleHeight);

                    if (newScale === scale) {
                        return;
                    }

                    var newWidth = width * newScale;
                    var newHeight = height * newScale;
                    canvas.style.height = Math.round(newHeight) + "px";
                    canvas.style.width = Math.round(newWidth) + "px";

                    scale = newScale;
                    ctx.setTransform(1, 0, 0, 1, 0, 0);
                    ctx.scale(1 / scale, 1 / scale);
                };

                var resizeIH = $interval(calculateScale, 200);
                scope.$on('$destroy', function () {
                    $interval.cancel(resizeIH);
                    resizeIH = null;
                });

                angular.element($window).bind('resize', calculateScale);
                scope.$on('$destroy', function () {
                    angular.element($window).unbind('resize', calculateScale);
                });

                calculateScale();

                element.on('touchstart', onTouchstart);
                element.on('touchend', onTouchend);

                function onTouchstart(event) {
                    scope.$apply(function () {
                        // notify that drawing has started
                        scope.notifyDrawing({ drawing: true });
                    });
                    event.preventDefault();
                }

                function onTouchend(event) {
                    scope.$apply(function () {
                        // updateModel
                        scope.updateModel();

                        // notify that drawing has ended
                        scope.notifyDrawing({ drawing: false });
                    });
                    event.preventDefault();
                }
            }
        };
    }

]);