var app = angular.module('akti', ['ui.router', 'ngAnimate', 'ui.bootstrap', 'ngSanitize', 'angular-confirm', 'wysiwyg.module', 'ngMessages', 'ui.tinymce', 'auth', 'customer', 'bootstrapLightbox', 'ngTouch']).config(['$provide', '$urlRouterProvider', '$stateProvider', '$locationProvider', '$compileProvider', '$httpProvider', 'LightboxProvider', function($provide, $urlRouterProvider, $stateProvider, $locationProvider, $compileProvider, $httpProvider, LightboxProvider) {
    $urlRouterProvider.otherwise('/login/');
    $compileProvider.debugInfoEnabled(!1);
    $urlRouterProvider.deferIntercept();
    $locationProvider.html5Mode(!0);
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    $provide.decorator('$templateCache', ['$delegate', function($delegate) {
        var keys = [],
            origPut = $delegate.put,
            origRem = $delegate.remove,
            origRemAll = $delegate.removeAll;
        $delegate.put = function(key, value) {
            origPut(key, value);
            if (keys.indexOf(key) === -1) {
                keys.push(key)
            } else {
                keys[key] = value
            }
        };
        $delegate.remove = function(key) {
            origRem(key);
            keys.splice(keys.indexOf(key), 1)
        };
        $delegate.removeAll = function() {
            origRemAll();
            keys = []
        }
        $delegate.getKeys = function() {
            return keys
        };
        return $delegate
    }]);
    LightboxProvider.templateUrl = 'miscTemplate/lightbox'
}]).run(['$http', '$rootScope', '$urlRouter', 'helper', 'UserService', '$state', 'CONSTANTS', '$window', function($http, $rootScope, $urlRouter, helper, UserService, $state, CONSTANTS, $window) {
    $rootScope.$on('$stateChangeStart', function(event, nextState, currentState) {
        if (angular.element("meta[name='version']")[0]) {
            var browserVersion = angular.element("meta[name='version']")[0].content;
            helper.doRequest('get', 'index.php', {
                'do': 'auth-getVersion'
            }, function(r) {
                if (browserVersion != r.data.version) {
                    browserVersion = r.data.version;
                    var ref = angular.element("base")[0].href;
                    ref = ref.substring(0, ref.length - 1);
                    $window.location.replace(ref + nextState.url)
                }
            })
        }
        if (UserService.isLoggedIn === undefined) {
            event.preventDefault();
            //angular.element('.loading_wrap').addClass('hidden')
        }
        angular.element('body').scrollTop(0);
        if (CONSTANTS.noAuth.indexOf(nextState.name) == -1) {
            angular.element('.page.ng-scope').removeClass('page-login');
            if (UserService.isLoggedIn === !0 && UserService.isLoged < newDate || UserService.isLoggedIn === !1 || UserService.isLoggedIn === undefined) {} else {
                if (helper.settings.WIZZARD_COMPLETE == '0') {
                    //angular.element('.loading_wrap').addClass('hidden');
                    return !1
                }
            }
        } else {
            angular.element('.page.ng-scope').addClass('page-login');
            if (event.defaultPrevented === !0) {
                UserService.reset();
                $urlRouter.sync()
            }
        }
        if (event.defaultPrevented === !1) {}
    });
    $urlRouter.listen()
}])