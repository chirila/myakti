angular.module('customer', ['uiGmapgoogle-maps']).config(['$provide', '$urlRouterProvider', '$stateProvider', '$locationProvider', '$compileProvider', '$httpProvider', 'uiGmapGoogleMapApiProvider', function($provide, $urlRouterProvider, $stateProvider, $locationProvider, $compileProvider, $httpProvider, uiGmapGoogleMapApiProvider) {
    $stateProvider.state('customers', {
        url:'/customers/', templateUrl:'cTemplate/customers', controller:'CustomersCtrl as vm', data: {
            menu: 'customers', submenu: 'customers'
        }
        , resolve: {
            data:function(helper) {
                angular.element('body').addClass('overflow_hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-customersDashboard', 'xget': 'dashboard_first'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('customer', {
        url:'/customer/:customer_id?types?contact_id', templateUrl:function($stateParams) {
            return'cTemplate/customer/customer/'+$stateParams.customer_id
        }
        , controller:'CustomerCtrl as ls', resolve: {
            data:function($stateParams, helper) {
                angular.element('body').addClass('overflow_hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-EditCustomer', 'customer_id': $stateParams.customer_id, 'types': $stateParams.types, 'contact_id': $stateParams.contact_id
                }
                ).then(function(d) {
                    return d.data
                }
                , function() {
                    return {}
                }
                )
            }
        }
        , data: {
            menu: 'customers', submenu: 'customers'
        }
        ,
    }
    ).state('customerView', {
        url:'/customerView/:customer_id?tab?site_addr', templateUrl:'cTemplate/customerView/CustomerViewHtml', controller:'CustomerViewCtrl as ls', resolve: {
            data:function($stateParams, helper) {
                angular.element('body').addClass('overflow_hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-ViewCustomer', 'customer_id': $stateParams.customer_id
                }
                ).then(function(d) {
                    return d.data
                }
                , function() {
                    return {}
                }
                )
            }
        }
        , data: {
            menu: 'customers', submenu: 'customers'
        }
        ,
    }
    ).state('contacts', {
        url:'/contacts/', templateUrl:'cTemplate/contacts', controller:'ContactsCtrl as vm', data: {
            menu: 'customers', submenu: 'contacts'
        }
        , resolve: {
            data:function(helper) {
                angular.element('body').addClass('overflow_hidden');
                return!0
            }
        }
    }
    ).state('contact', {
        url:'/contact/:contact_id?customer_id', templateUrl:function($stateParams) {
            return'cTemplate/contact/contactHtml/'+$stateParams.contact_id
        }
        , controller:'ContactCtrl as ls', resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-Contact', 'contact_id': $stateParams.contact_id, 'customer_id': $stateParams.customer_id
                }
                ).then(function(d) {
                    return d.data
                }
                , function() {
                    return {}
                }
                )
            }
        }
        , data: {
            menu: 'customers', submenu: 'contacts'
        }
        ,
    }
    ).state('contactView', {
        url:'/contactView/:contact_id?customer_id', templateUrl:'cTemplate/contactView/ContactViewHtml', controller:'ContactViewCtrl as ls', resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-Contact', 'contact_id': $stateParams.contact_id, 'customer_id': $stateParams.customer_id, 'viewMod': !0
                }
                ).then(function(d) {
                    return d.data
                }
                , function() {
                    return {}
                }
                )
            }
        }
        , data: {
            menu: 'customers', submenu: 'contacts'
        }
        ,
    }
    ).state('import_accounts', {
        url:'/import_accounts/?tab', templateUrl:'cTemplate/import_accounts', controller:'ImportAccountsCtrl as vm', resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-import_accounts', 'xget': 'import'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
        , data: {
            menu: 'customers', submenu: 'import'
        }
        ,
    }
    ).state('customersSettings', {
        url:'/customersSettings/', templateUrl:'cTemplate/customersSettings', controller:'CustomersSettingsCtrl as ls', data: {
            menu: 'customers', submenu: 'setting', adminSetting: 'company'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-customersSettings', 'xget': 'customerFields'
                }
                ).then(function(d) {
                    return d.data
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('smartList', {
        url:'/smartList/', templateUrl:'cTemplate/smartList', controller:'SmartListCtrl as ls', data: {
            menu: 'customers', submenu: 'smartList',
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
            }
        }
    }
    ).state('smartListContact', {
        url:'/smartListContact/', templateUrl:'cTemplate/smartListContact', controller:'SmartListCtrl as ls', data: {
            menu: 'customers', submenu: 'smartListContact',
        }
        , resolve: {
            data:function(helper, $stateParams) {
                $stateParams.isContact = true;
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
            }
        }
    }
    ).state('smartListEdit', {
        url:'/smartListEdit/:contact_list_id', templateUrl:'cTemplate/smartListEdit', controller:'SmartListEditCtrl as ls', data: {
            menu: 'customers', submenu: '',
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-smartListEdit', 'contact_list_id': $stateParams.contact_list_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('smartListAdd', {
        url:'/smartListAdd/?list_for', templateUrl:'cTemplate/smartListEdit', controller:'SmartListAddCtrl as ls', data: {
            menu: 'customers', submenu: '',
        }
        , resolve: {
            data:function($stateParams, helper, $state) {
                if($stateParams.list_for != '0' && $stateParams.list_for != '1'){
                    return $state.go('customers');
                }else{
                    angular.element('.loading_wrap').removeClass('hidden');
                    return helper.doRequest('get', 'index.php', {
                        'do': 'customers-smartListAdd',
                        'list_for':$stateParams.list_for,
                        'list_type':'0'
                    }
                    ).then(function(d) {
                        return d
                    }
                    , function() {
                        return {}
                    }
                    )
                }            
            }
        }
    }
    ).state('smartListView', {
        url:'/smartListView/:contact_list_id', templateUrl:'cTemplate/smartListView', controller:'SmartListViewCtrl as ls', data: {
            menu: 'customers', submenu: '',
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-smartListView', 'contact_list_id': $stateParams.contact_list_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('marketing', {
        url:'/marketing/', templateUrl:'cTemplate/marketing', controller:'MarketingCtrl as ls', data: {
            menu: 'customers', submenu: 'marketing',
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-marketing'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('campaignReports', {
        url:'/campaignReports/', templateUrl:'cTemplate/campaignReports', controller:'CampaignReportsCtrl as ls', data: {
            menu: 'customers', submenu: 'marketing',
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-campaignReports'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('campaigns', {
        url:'/campaigns/:app', templateUrl:'cTemplate/campaigns', controller:'CampaignsCtrl as ls', data: {
            menu: 'customers', submenu: 'marketing',
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return {}
            }
        }
    }
    ).state('quotes', {
        url:'/quotes/', templateUrl:'qTemplate/quotes', controller:'quotesCtrl', data: {
            menu: 'customers', submenu: 'quotes'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
            }
        }
    }
    ).state('quotesTemplates', {
        url:'/quotesTemplates/', templateUrl:'qTemplate/quotesTemplates', controller:'quotesTemplatesCtrl as vm', data: {
            menu: 'customers', submenu: 'quotesTemplates'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'quote-quotes', 'xget': 'quotesTemplates'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('quoteSettings', {
        url:'/quoteSettings/', templateUrl:'qTemplate/quoteSettings', controller:'quoteSettingsCtrl as vm', data: {
            menu: 'customers', submenu: 'quotes setting', adminSetting: 'quote'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
            }
        }
    }
    ).state('quote.view', {
        url:'view/:quote_id?version_id', templateUrl:'qTemplate/quoteView', controller:'QuoteViewCtrl as view', data: {
            menu: 'customers', submenu: 'quotes'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                helper.Loading++;
                return helper.doRequest('get', 'index.php', {
                    'do': 'quote-nquote', 'quote_id': $stateParams.quote_id, 'version_id': $stateParams.version_id, 'xview': 1
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('quote.edit', {
        url:'edit/:quote_id?duplicate_quote_id?version_id?buyer_id?from_customer', templateUrl:'qTemplate/quoteEdit', controller:'QuoteEditCtrl as edit', data: {
            menu: 'customers', submenu: 'quotes'
        }
        , resolve: {
            data:function($cacheFactory, $stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                var initData= {
                    'do': 'quote-nquote', 'quote_id': $stateParams.quote_id, 'duplicate_quote_id': $stateParams.duplicate_quote_id, 'version_id': $stateParams.version_id, 'buyer_id': $stateParams.buyer_id
                }
                ;
                var cacheDeal=$cacheFactory.get('DealToDocument');
                if(cacheDeal!=undefined) {
                    var cachedDataDeal=cacheDeal.get('data')
                }
                if(cachedDataDeal!=undefined) {
                    for(x in cachedDataDeal) {
                        initData[x]=cachedDataDeal[x]
                    }
                    cacheDeal.remove('data')
                }
                return helper.doRequest('get', 'index.php', initData).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('quote.editTemplate', {
        url:'editTemplate/:template_id?quote_id?version_id', templateUrl:'qTemplate/quoteEditTemplate', controller:'QuoteEditTemplateCtrl as edit', data: {
            menu: 'customers', submenu: 'quotesTemplates'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'quote-nquote', 'template_id': $stateParams.template_id, 'version_id': $stateParams.version_id, 'quote_id': $stateParams.quote_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('contracts', {
        url:'/contracts/', templateUrl:'conTemplate/contracts', controller:'contractsCtrl', data: {
            menu: 'customers', submenu: 'contracts'
        }
        , resolve: {
            data:function(helper) {
                return!0
            }
        }
    }
    ).state('contractTemplates', {
        url:'/contractTemplates/', templateUrl:'conTemplate/contractTemplates', controller:'contractTemplatesCtrl as vm', data: {
            menu: 'customers', submenu: 'contractTemplates'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'contract-contracts_templates'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('contract.edit', {
        url:'edit/:contract_id?version_id?buyer_id?quote_id', templateUrl:'conTemplate/contractEdit', controller:'contractEditCtrl as edit', data: {
            menu: 'customers', submenu: 'contracts'
        }
        , resolve: {
            data:function($cacheFactory, $stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                var initData= {
                    'do': 'contract-contract', 'contract_id': $stateParams.contract_id, 'version_id': $stateParams.version_id, 'buyer_id': $stateParams.buyer_id, 'quote_id': $stateParams.quote_id
                }
                ;
                var cacheDeal=$cacheFactory.get('DealToDocument');
                if(cacheDeal!=undefined) {
                    var cachedDataDeal=cacheDeal.get('data')
                }
                if(cachedDataDeal!=undefined) {
                    for(x in cachedDataDeal) {
                        initData[x]=cachedDataDeal[x]
                    }
                    cacheDeal.remove('data')
                }
                return helper.doRequest('get', 'index.php', initData).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('contract.view', {
        url:'view/:contract_id?version_id', templateUrl:'conTemplate/contractView', controller:'contractViewCtrl as view', data: {
            menu: 'customers', submenu: 'contracts'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'contract-contract', 'contract_id': $stateParams.contract_id, 'version_id': $stateParams.version_id, 'xview': 1
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('contract.editTemplate', {
        url:'editTemplate/:template_id?contract_id?version_id', templateUrl:'conTemplate/contractEditTemplate', controller:'ContractEditTemplateCtrl as edit', data: {
            menu: 'customers', submenu: 'contractTemplates'
        }
        , resolve: {
            data:function($stateParams, helper) {
            angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'contract-contract', 'template_id': $stateParams.template_id, 'version_id': $stateParams.version_id, 'contract_id': $stateParams.contract_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('contractSettings', {
        url:'/contractSettings/', templateUrl:'conTemplate/contractSettings', controller:'contractSettingsCtrl as vm', data: {
            menu: 'customers', submenu: 'contracts setting', adminSetting: 'contract'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'contract-settings', 'xget': 'PDFlayout'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('deals', {
        url:'/deals/?board_id?buyer_id?contact_id?add_direct?show_edit?zen_data?from_view?is_add', templateUrl:'cTemplate/deals', controller:'dealsCtrl', data: {
            menu: 'customers', submenu: 'deals',
        }
        , resolve: {
            data:function(helper, $stateParams) {                
                angular.element('.loading_wrap').removeClass('hidden');
                var list=angular.copy(helper.getItemSimple('customers-deals'));
                if(list&&list.search&&list.search.xget) {
                    delete list.search.xget
                }
                if(list&&list.search) {
                    var params= {
                        'do': 'customers-deals', 'board_id': $stateParams.board_id
                    }
                    ;
                    for(x in list.search) {
                        params[x]=list.search[x];
                        if(x=='desc') {
                            if(list.search[x]) {
                                params[x]='1'
                            }
                            else if(!list.search[x]) {
                                params[x]=''
                            }
                        }
                    }
                    return helper.doRequest('get', 'index.php', params).then(function(d) {
                        return d
                    }
                    , function() {
                        return {}
                    }
                    )
                }
                else {
                    return helper.doRequest('get', 'index.php', {
                        'do': 'customers-deals', 'board_id': $stateParams.board_id, 'from_view': $stateParams.from_view
                    }
                    ).then(function(d) {
                        return d
                    }
                    , function() {
                        return {}
                    }
                    )
                }
            }
        }
    }
    ).state('dealView', {
        url:'/dealView/:opportunity_id', templateUrl:'cTemplate/dealView', controller:'DealViewCtrl as edit', data: {
            menu: 'customers', submenu: 'deals'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-deal_edit', 'opportunity_id': $stateParams.opportunity_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('address_field', {
        abstract:!0, url:'/address_field/', templateUrl:'cTemplate/AddressField', controller:'AddressFieldCtrl', data: {
            menu: 'customers', submenu: 'customers'
        }
    }
    ).state('address_field.edit', {
        url:'edit/:address_id?customer_id', templateUrl:'cTemplate/AddressFieldEdit', controller:'AddressFieldEditCtrl as edit', data: {
            menu: 'customers', submenu: 'customers'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-sites', 'address_id': $stateParams.address_id, 'customer_id': $stateParams.customer_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('dealsSettings', {
        url:'/dealsSettings/', templateUrl:'cTemplate/dealsSettings', controller:'DealsSettingsCtrl as ls', data: {
            menu: 'customers', submenu: 'dealsSetting', adminSetting: 'company'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'customers-customersSettings', 'xget': 'dealsRef'
                }
                ).then(function(d) {
                    return d.data
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    );
    uiGmapGoogleMapApiProvider.configure( {
        key: 'AIzaSyCd5onUZQ4N_hZHPY5lbHb_2m8HWSs0sXY', v: '3.25', libraries: 'weather,geometry,visualization'
    }
    )
}

])