angular.module('quote', []).config(['$provide', '$stateProvider', function($provide, $stateProvider) {
    $stateProvider/*.state('quotes', {
        url:'/quotes/', templateUrl:'qTemplate/quotes', controller:'quotesCtrl', data: {
            menu: 'quotes', submenu: 'quotes'
        }
        , resolve: {
            data:function(helper) {
                return!0
            }
        }
    }
    )*//*.state('quotesTemplates', {
        url:'/quotesTemplates/', templateUrl:'qTemplate/quotesTemplates', controller:'quotesTemplatesCtrl as vm', data: {
            menu: 'quotes', submenu: 'quotesTemplates'
        }
        , resolve: {
            data:function(helper) {
                return helper.doRequest('get', 'index.php', {
                    'do': 'quote-quotes', 'xget': 'quotesTemplates'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    )*//*.state('quoteSettings', {
        url:'/quoteSettings/', templateUrl:'qTemplate/quoteSettings', controller:'quoteSettingsCtrl as vm', data: {
            menu: 'quotes', submenu: 'quotes setting', adminSetting: 'quote'
        }
        , resolve: {
            data:function(helper) {
                return!0
            }
        }
    }
    )*/.state('quote', {
        abstract:!0, url:'/quote/', templateUrl:'qTemplate/quote', controller:'QuoteCtrl as vm', data: {
            menu: 'quotes', submenu: 'quotes'
        }
    }
    )/*.state('quote.edit', {
        url:'edit/:quote_id?duplicate_quote_id?version_id?buyer_id', templateUrl:'qTemplate/quoteEdit', controller:'QuoteEditCtrl as edit', data: {
            menu: 'quotes', submenu: 'quotes'
        }
        , resolve: {
            data:function($cacheFactory, $stateParams, helper) {
                var initData= {
                    'do': 'quote-nquote', 'quote_id': $stateParams.quote_id, 'duplicate_quote_id': $stateParams.duplicate_quote_id, 'version_id': $stateParams.version_id, 'buyer_id': $stateParams.buyer_id
                }
                ;
                var cacheDeal=$cacheFactory.get('DealToDocument');
                if(cacheDeal!=undefined) {
                    var cachedDataDeal=cacheDeal.get('data')
                }
                if(cachedDataDeal!=undefined) {
                    for(x in cachedDataDeal) {
                        initData[x]=cachedDataDeal[x]
                    }
                    cacheDeal.remove('data')
                }
                return helper.doRequest('get', 'index.php', initData).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('quote.editTemplate', {
        url:'editTemplate/:template_id?quote_id?version_id', templateUrl:'qTemplate/quoteEditTemplate', controller:'QuoteEditTemplateCtrl as edit', data: {
            menu: 'quotes', submenu: 'quotesTemplates'
        }
        , resolve: {
            data:function($stateParams, helper) {
                return helper.doRequest('get', 'index.php', {
                    'do': 'quote-nquote', 'template_id': $stateParams.template_id, 'version_id': $stateParams.version_id, 'quote_id': $stateParams.quote_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    )*//*.state('quote.view', {
        url:'view/:quote_id?version_id', templateUrl:'qTemplate/quoteView', controller:'QuoteViewCtrl as view', data: {
            menu: 'quotes', submenu: 'quotes'
        }
        , resolve: {
            data:function($stateParams, helper) {
                helper.Loading++;
                return helper.doRequest('get', 'index.php', {
                    'do': 'quote-nquote', 'quote_id': $stateParams.quote_id, 'version_id': $stateParams.version_id, 'xview': 1
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    )*/
}

])