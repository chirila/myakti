angular.module('timetrack', []).config(['$provide', '$stateProvider', function($provide, $stateProvider) {
    $stateProvider.state('timetracker_tasks', {
        url:'/timetracker_tasks/', templateUrl:'tTemplate/tasks', controller:'timetracktasksCtrl', data: {
            menu: 'timetracker', submenu: 'timetracker_tasks'
        }
        , resolve: {
            data:function(helper) {
    			angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'timetracker-tasks',
                    'archived': '1',
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('timetrackerSettings', {
        url:'/timetrackerSettings/', templateUrl:'tTemplate/timetrackSettings', controller:'TimetrackSettingsCtrl as vm', data: {
            menu: 'timetracker', submenu: 'timetracker_setting', adminSetting: 'timetracker'
        }
        , resolve: {
            data:function(helper) {
    			angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'timetracker-settings', 'xget': 'expenses'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('timesheetView', {
        url:'/timesheetView/:user_id?start', templateUrl:'tTemplate/timesheetView', controller:'TimesheetViewCtrl as info', data: {
            menu: 'timetracker', submenu: 'timesheet'
        }
        , resolve: {
            data:function($stateParams, helper) {
    			angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'timetracker-timesheet', 'user_id': $stateParams.user_id, 'start': $stateParams.start
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    )
}

])