angular.module('installation', []).config(['$provide', '$stateProvider', function($provide, $stateProvider) {
    $stateProvider/*.state('installations', {
        url:'/installations/', templateUrl:'insTemplate/installations', controller:'installationsCtrl', data: {
            menu: 'installations', submenu: 'installations'
        }
        , resolve: {
            data:function(helper) {
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-installations'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    )*//*.state('install_fields', {
        url:'/install_fields/', templateUrl:'insTemplate/installationFields', controller:'installationFieldsCtrl', data: {
            menu: 'installations', submenu: 'install_fields'
        }
        , resolve: {
            data:function(helper) {
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-install_fields'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    )*//*.state('install_templates', {
        url:'/install_templates/', templateUrl:'insTemplate/installationTemplates', controller:'installationTemplatesCtrl', data: {
            menu: 'installations', submenu: 'install_templates'
        }
        , resolve: {
            data:function(helper) {
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-install_templates'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    )*//*.state('install_template', {
        abstract:!0, url:'/install_template/', templateUrl:'insTemplate/install_template', controller:'installationTemplateCtrl', data: {
            menu: 'installations', submenu: 'install_templates'
        }
    }
    ).state('install_template.edit', {
        url:'edit/:template_id', templateUrl:'insTemplate/install_templateEdit', controller:'installationTemplateEditCtrl as edit', data: {
            menu: 'installations', submenu: 'install_templates'
        }
        , resolve: {
            data:function($stateParams, helper) {
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-install_template', 'template_id': $stateParams.template_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('installation', {
        abstract:!0, url:'/installation/', templateUrl:'insTemplate/installation', controller:'installationCtrl', data: {
            menu: 'installations', submenu: 'installations'
        }
    }
    ).state('installation.edit', {
        url:'edit/:installation_id?buyer_id', templateUrl:'insTemplate/installationEdit', controller:'installationEditCtrl as edit', data: {
            menu: 'installations', submenu: 'installations'
        }
        , resolve: {
            data:function($stateParams, helper) {
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-installation', 'installation_id': $stateParams.installation_id, 'buyer_id': $stateParams.buyer_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('installation.view', {
        url:'view/:installation_id', templateUrl:'insTemplate/installationView', controller:'installationViewCtrl as edit', data: {
            menu: 'installations', submenu: 'installations'
        }
        , resolve: {
            data:function($stateParams, helper) {
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-ninstallation', 'installation_id': $stateParams.installation_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('installationSettings', {
        url:'/installationSettings/', templateUrl:'insTemplate/installationSettings', controller:'installationSettingsCtrl as ls', data: {
            menu: 'installations', submenu: 'installation setting', adminSetting: 'installation',
        }
        , resolve: {
            data:function(helper) {
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-settings'
                }
                ).then(function(d) {
                    return d.data
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    )*/
}

])