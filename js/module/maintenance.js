angular.module('maintenance', []).config(['$provide', '$stateProvider', function($provide, $stateProvider) {
    $stateProvider.state('services', {
        url:'/services/', templateUrl:'template/services', controller:'servicesCtrl', data: {
            menu: 'interventions', submenu: 'intervention'
        }
        , resolve: {
            data:function(helper) {
                return!0
            }
        }
    }
    ).state('installations', {
        url:'/installations/', templateUrl:'insTemplate/installations', controller:'installationsCtrl', data: {
            menu: 'interventions', submenu: 'installations'
        }
        , resolve: {
            /*data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-installations'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }*/
            data:function(helper) {
                return!0
            }
        }
    }
    ).state('install_fields', {
        url:'/install_fields/', templateUrl:'insTemplate/installationFields', controller:'installationFieldsCtrl', data: {
            menu: 'interventions', submenu: 'install_fields'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-install_fields'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('install_templates', {
        url:'/install_templates/', templateUrl:'insTemplate/installationTemplates', controller:'installationTemplatesCtrl', data: {
            menu: 'interventions', submenu: 'install_templates'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-install_templates'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('install_template', {
        abstract:!0, url:'/install_template/', templateUrl:'insTemplate/install_template', controller:'installationTemplateCtrl', data: {
            menu: 'interventions', submenu: 'install_templates'
        }
    }
    ).state('install_template.edit', {
        url:'edit/:template_id', templateUrl:'insTemplate/install_templateEdit', controller:'installationTemplateEditCtrl as edit', data: {
            menu: 'interventions', submenu: 'install_templates'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-install_template', 'template_id': $stateParams.template_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('installation', {
        abstract:!0, url:'/installation/', templateUrl:'insTemplate/installation', controller:'installationCtrl', data: {
            menu: 'interventions', submenu: 'installations'
        }
    }
    ).state('installation.edit', {
        url:'edit/:installation_id?buyer_id', templateUrl:'insTemplate/installationEdit', controller:'installationEditCtrl as edit', data: {
            menu: 'interventions', submenu: 'installations'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-installation', 'installation_id': $stateParams.installation_id, 'buyer_id': $stateParams.buyer_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('installation.view', {
        url:'view/:installation_id', templateUrl:'insTemplate/installationView', controller:'installationViewCtrl as edit', data: {
            menu: 'interventions', submenu: 'installations'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-ninstallation', 'installation_id': $stateParams.installation_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('installationSettings', {
        url:'/installationSettings/', templateUrl:'insTemplate/installationSettings', controller:'installationSettingsCtrl as ls', data: {
            menu: 'interventions', submenu: 'installation setting', adminSetting: 'installation',
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'installation-settings'
                }
                ).then(function(d) {
                    return d.data
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('service', {
        abstract:!0, url:'/service/', templateUrl:'template/service', controller:'serviceCtrl as vm', data: {
            menu: 'interventions', submenu: 'intervention'
        }
        ,
    }
    ).state('service.edit', {
        url:'edit/:service_id?for_user?buyer_id?duplicate_service_id', templateUrl:'template/serviceEdit', controller:'serviceEditCtrl as edit', data: {
            menu: 'interventions', submenu: 'intervention'
        }
        , resolve: {
            data:function($cacheFactory,$stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                var initData= {
                    'do': 'maintenance-service', 'service_id': $stateParams.service_id, 'duplicate_service_id': $stateParams.duplicate_service_id
                }
                if ($stateParams.for_user) {
                    initData.for_user = $stateParams.for_user
                }
                if ($stateParams.buyer_id) {
                    initData.buyer_id = $stateParams.buyer_id
                }
                if ($stateParams.is_recurring) {
                    initData.is_recurring = $stateParams.is_recurring
                }
                var cacheQuote = $cacheFactory.get('QuoteToService');
                if (cacheQuote != undefined) {
                    var cachedDataQuote = cacheQuote.get('data')
                }
                var cacheMContract = $cacheFactory.get('ContractToService');
                if (cacheMContract != undefined) {
                    var cachedDataMContract = cacheMContract.get('data')
                }
                var cacheDeal = $cacheFactory.get('DealToDocument');
                if (cacheDeal != undefined) {
                    var cachedDataDeal = cacheDeal.get('data')
                }
                if (cachedDataQuote != undefined) {
                    for (x in cachedDataQuote) {
                        initData[x] = cachedDataQuote[x]
                    }
                    cacheQuote.remove('data')
                }
                if (cachedDataMContract != undefined) {
                    for (x in cachedDataMContract) {
                        initData[x] = cachedDataMContract[x]
                    }
                    cacheMContract.remove('data')
                }
                if (cachedDataDeal != undefined) {
                    for (x in cachedDataDeal) {
                        initData[x] = cachedDataDeal[x]
                    }
                    cacheDeal.remove('data')
                }
                return helper.doRequest('get', 'index.php', initData)
                .then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('service.view', {
        url:'view/:service_id', templateUrl:'template/serviceView', controller:'serviceViewCtrl as view', data: {
            menu: 'interventions', submenu: 'intervention'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-service', 'service_id': $stateParams.service_id, 'xview':'1'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('service.add', {
        url:'add/?for_user?buyer_id', templateUrl:'template/serviceAdd', controller:'serviceAddCtrl as edit', data: {
            menu: 'interventions', submenu: 'intervention'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-serviceAdd'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('serviceReport', {
        url:'/serviceReport/:service_id', templateUrl:'template/serviceReport', controller:'serviceReportCtrl', data: {
            menu: 'interventions', submenu: 'intervention'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-service', 'xget': 'report', 'service_id': $stateParams.service_id,
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('serviceSettings', {
        url:'/serviceSettings/', templateUrl:'template/serviceSettings', controller:'serviceSettingsCtrl', data: {
            menu: 'interventions', submenu: 'intervention setting', adminSetting: 'service',
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-settings', 'xget': 'namingConv'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('serviceTs', {
        url:'/serviceTs/', templateUrl:'planning_gb', controller:'serviceTsCtrl', data: {
            menu: 'a', submenu: 'b'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
                /*return helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-planning', 'xget': 'intNotSeen'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )*/
            }
        }
    }
    ).state('planning', {
        url:'/planning/', templateUrl:'template/subplanning', controller:'planningCtrl', data: {
            menu: 'interventions', submenu: 'planning'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-planning_over'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('teams', {
        url:'/teams/', templateUrl:'template/teams', controller:'teamsCtrl', data: {
            menu: 'interventions', submenu: 'teams'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-teams'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('team', {
        abstract:!0, url:'/team/', templateUrl:'template/team', controller:'teamCtrl', data: {
            menu: 'interventions', submenu: 'teams'
        }
        ,
    }
    ).state('team.edit', {
        url:'edit/:team_id', templateUrl:'template/teamEdit', controller:'teamEditCtrl as edit', data: {
            menu: 'interventions', submenu: 'teams'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-team', 'team_id': $stateParams.team_id,
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('rec_services', {
        url:'/rec_services/', templateUrl:'template/rec_services', controller:'rec_servicesCtrl', data: {
            menu: 'interventions', submenu: 'rec_services'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
            }
        }
    }
    ).state('rec_service', {
        abstract:!0, url:'/rec_service/', templateUrl:'template/service', controller:'serviceCtrl as vm', data: {
            menu: 'interventions', submenu: 'rec_services'
        }
        ,
    }
    ).state('rec_service.edit', {
        url:'edit/:service_id?for_user?is_recurring', templateUrl:'template/serviceEdit', controller:'serviceEditCtrl as edit', data: {
            menu: 'interventions', submenu: 'rec_services'
        }
        , resolve: {
            data:function($cacheFactory,$stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                var initData= {
                    'do': 'maintenance-service', 'service_id': $stateParams.service_id, 'for_user':$stateParams.for_user,'is_recurring':$stateParams.is_recurring
                }
                var cacheMRContract = $cacheFactory.get('ContractRecurringToService');
                if (cacheMRContract != undefined) {
                    var cachedDataMRContract = cacheMRContract.get('data')
                }
                if (cachedDataMRContract != undefined) {
                    for (x in cachedDataMRContract) {
                        initData[x] = cachedDataMRContract[x]
                    }
                    cacheMRContract.remove('data')
                }
                return helper.doRequest('get', 'index.php', initData)
                .then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('rec_service.add', {
        url:'add/?for_user?is_recurring', templateUrl:'template/serviceAdd', controller:'serviceAddCtrl as edit', data: {
            menu: 'interventions', submenu: 'rec_services'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-serviceAdd'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('serviceModels', {
        url:'/serviceModels/', templateUrl:'template/serviceModels', controller:'serviceModelsCtrl', data: {
            menu: 'interventions', submenu: 'serviceModels'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-serviceModels'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('service.addModel', {
        url:'addModel/:template_id', templateUrl:'template/serviceEditModel', controller:'serviceAddCtrl as edit', data: {
            menu: 'interventions', submenu: 'serviceModels'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-serviceAdd', 'template_id': $stateParams.template_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('service.editModel', {
        url:'editModel/:template_id', templateUrl:'template/serviceEditModel', controller:'serviceEditCtrl as edit', data: {
            menu: 'interventions', submenu: 'serviceModels'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-service', 'template_id': $stateParams.template_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    )
}

])