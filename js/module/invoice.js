angular.module('invoice', []).config(['$provide', '$stateProvider', function($provide, $stateProvider) {
    $stateProvider.state('invoices', {
        url:'/invoices/?tab', templateUrl:'iTemplate/invoices', controller:'invoicesCtrl', data: {
            menu: 'invoices', submenu: 'invoices'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
            }
        }
    }
    ).state('invoice', {
        abstract:!0, url:'/invoice/', templateUrl:'iTemplate/invoice', controller:'InvoiceCtrl', data: {
            menu: 'invoices', submenu: 'invoices'
        }
    }
    ).state('invoiceSettings', {
        url:'/invoiceSettings/', templateUrl:'iTemplate/invoiceSettings', controller:'InvoiceSettingsCtrl as vm', data: {
            menu: 'invoices', submenu: 'invoices setting', adminSetting: 'invoice'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
            }
        }
    }
    ).state('invoice.view', {
        url:'view/:invoice_id', templateUrl:'iTemplate/invoiceView', controller:'InvoiceViewCtrl as view', data: {
            menu: 'invoices', submenu: 'invoices'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0
                /*return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-invoice', 'invoice_id': $stateParams.invoice_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )*/
            }
        }
    }
    ).state('invoice.edit', {
        url:'edit/:invoice_id?duplicate_invoice_id?buyer_id?c_invoice_id?type?a_invoice_id?base_type?languages?c_quote_id?a_quote_id?track_base?your_ref?copy_free_text', templateUrl:'iTemplate/invoiceEdit', controller:'InvoiceEditCtrl as edit', data: {
            menu: 'invoices', submenu: 'invoices'
        }
        ,
    }
    ).state('purchase_invoices', {
        url:'/purchase_invoices/', templateUrl:'iTemplate/purchase_invoices', controller:'purchase_invoicesCtrl', data: {
            menu: 'invoices', submenu: 'purchase_invoices'
        }
        , resolve: {
            data:function(helper) {
                return!0
            }
        }
    }
    ).state('purchase_invoice', {
        abstract:!0, url:'/purchase_invoice/', templateUrl:'iTemplate/purchase_invoice', controller:'Purchase_invoiceCtrl', data: {
            menu: 'invoices', submenu: 'purchase_invoices'
        }
        ,
    }
    ).state('purchase_invoice.view', {
        url:'view/:invoice_id', templateUrl:'iTemplate/purchase_invoiceView', controller:'Purchase_invoiceViewCtrl as view', data: {
            menu: 'invoices', submenu: 'purchase_invoices'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-purchase_invoice', 'invoice_id': $stateParams.invoice_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('purchase_invoice.edit', {
        url:'edit/:incomming_invoice_id?invoice_id?p_order_id', templateUrl:'iTemplate/purchase_invoiceEdit', controller:'purchaseInvoiceEditCtrl as edit', data: {
            menu: 'invoices', submenu: 'purchase_invoices'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-purchase_ninvoice', 'incomming_invoice_id': $stateParams.incomming_invoice_id, 'invoice_id': $stateParams.invoice_id, 'p_order_id': $stateParams.p_order_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('recinvoices', {
        url:'/recinvoices/', templateUrl:'iTemplate/recinvoices', controller:'recinvoicesCtrl', data: {
            menu: 'invoices', submenu: 'recinvoices'
        }
        , resolve: {
            data:function(helper) {
                return!0
            }
        }
    }
    ).state('recinvoice', {
        abstract:!0, url:'/recinvoice/', templateUrl:'iTemplate/recinvoice', controller:'recInvoiceCtrl', data: {
            menu: 'invoices', submenu: 'recinvoices'
        }
    }
    ).state('recinvoice.edit', {
        url:'edit/:recurring_invoice_id?invoice_id?contract_id?duplicate_recurring_invoice_id', templateUrl:'iTemplate/recinvoiceEdit', controller:'recInvoiceEditCtrl as edit', data: {
            menu: 'invoices', submenu: 'recinvoices'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-recurring_ninvoice', 'recurring_invoice_id': $stateParams.recurring_invoice_id, 'invoice_id': $stateParams.invoice_id, 'contract_id': $stateParams.contract_id, 'duplicate_recurring_invoice_id': $stateParams.duplicate_recurring_invoice_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('recinvoice.view', {
        url:'view/:recurring_invoice_id', templateUrl:'iTemplate/recinvoiceView', controller:'RecurringInvoiceViewCtrl as view', data: {
            menu: 'invoices', submenu: 'invoices'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0;
            }
        }
    }
    ).state('import_invoices', {
        url:'/import_invoices/', templateUrl:'iTemplate/import_invoices', controller:'ImportInvoicesCtrl as vm', data: {
            menu: 'invoices', submenu: 'import'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-import_invoices', 'xget': 'import'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('reminders', {
        url:'/reminders/', templateUrl:'iTemplate/reminders', controller:'remindersCtrl', data: {
            menu: 'invoices', submenu: 'reminders'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return!0;
                /*return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-late_invoices'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )*/
            }
        }
    }
    ).state('remSettings', {
        url:'/remSettings/', templateUrl:'iTemplate/remSettings', controller:'reminderSettingsCtrl as set', data: {
            menu: 'invoices', submenu: 'reminders'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-late_settings'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('export_apps', {
        url:'/export_apps/', templateUrl:'iTemplate/exportApps', controller:'exportAppsCtrl', data: {
            menu: 'invoices', submenu: 'exports'
        }
        , resolve: {
            data:function($state, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-export_apps'
                }
                ).then(function(d) {
                    if(d.data.preferred_acc&&d.data.accountancy.length==1&&d.data.accountancy[0].activated) {
                        $state.go('export_btb', {
                            'app': d.data.preferred_acc
                        }
                        )
                    }
                    else {
                        return d
                    }
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('export_csvs', {
        url:'/export_csvs/', templateUrl:'iTemplate/exportCsvs', controller:'exportCsvsCtrl', data: {
            menu: 'invoices', submenu: 'exports'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-export_csvs'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('export_csv', {
        abstract:!0, url:'/export_csv/', templateUrl:'iTemplate/exportCsv', controller:'exportCsvCtrl', data: {
            menu: 'invoices', submenu: 'exports'
        }
    }
    ).state('export_csv.edit', {
        url:'edit/:list_id', templateUrl:'iTemplate/exportCsvEdit', controller:'exportCsvEditCtrl as edit', data: {
            menu: 'invoices', submenu: 'exports'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-export_csv', 'list_id': $stateParams.list_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('export_view', {
        url:'/export_view/:list_id', templateUrl:'iTemplate/exportView', controller:'exportViewCtrl', data: {
            menu: 'invoices', submenu: 'exports'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-export_view', 'list_id': $stateParams.list_id, 'view': $stateParams.activeView, 'offset': 1, 'type': 0
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('export_btb', {
        url:'/export_btb/:app', templateUrl:'iTemplate/exportBtbApp', controller:'exportBtbAppCtrl', data: {
            menu: 'invoices', submenu: 'exports'
        }
        , resolve: {
            data:function($stateParams, $rootScope, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-export_btb', 'app': $stateParams.app, 'view': $stateParams.activeView, 'offset': 1, 'type': 0, 'reset_list':($rootScope.reset_lists.export_btb ? '1' : '0')
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('payments', {
        url:'/payments/', templateUrl:'iTemplate/payments', controller:'paymentsCtrl', data: {
            menu: 'invoices', submenu: 'payments'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-payments_list'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('sepa', {
        url:'/sepa/', templateUrl:'iTemplate/sepa', controller:'sepaCtrl', data: {
            menu: 'invoices', submenu: 'sepa'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'invoice-sepa', 'view': '1'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    )
}

])