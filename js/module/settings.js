angular.module('settings', []).config(['$provide', '$stateProvider', function($provide, $stateProvider) {
    $stateProvider.state('subscription', {
        url:'/subscription/', templateUrl:'sTemplate/subscriptionNew', controller:'subscriptionNewCtrl', data: {
            menu: 'a', submenu: 'b'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'settings-subscription_new',
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('subscription_new', {
        url:'/subscription_new/', templateUrl:'sTemplate/subscriptionNew', controller:'subscriptionNewCtrl', data: {
            menu: 'a', submenu: 'b'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'settings-subscription_new',
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('nylas', {
        url:'/nylas/?code', templateUrl:'sTemplate/nylasCallback', controller:'nylasCallbackCtrl', data: {
            menu: 'a', submenu: 'b'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'settings-nylas_callback', 'code': $stateParams.code
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('billing', {
        url:'/billing/', templateUrl:'sTemplate/billing', controller:'BillingCtrl', data: {
            menu: ' ', submenu: ' '
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'settings-billing'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('settings', {
        url:'/settings/?tab&cfconn&eoconn', templateUrl:'sTemplate/Settings', controller:'SettingsCtrl as vm', data: {
            menu: '#', submenu: '#', group: 'admin'
        }
        , resolve: {
            data:function($stateParams,helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                angular.forEach(angular.element('#settingsNav li.sidebar-subitem'), function(value, key) {
                    if (angular.element(value)) {
                        angular.element(value).removeClass("active");
                    }
                });
                if($stateParams.tab){
                    return {};    
                }else{
                    return helper.doRequest('get', 'index.php', {
                        'do': 'settings-settings', 'xget': 'company'
                    }
                    ).then(function(d) {
                        return d
                    }
                    , function() {
                        return!0
                    }
                    )   
                }           
            }
        }
    }
    ).state('user', {
        url:'/user/:user_id', templateUrl:'sTemplate/settingsEdit', controller:'UserCtrl as vm', data: {
            group: 'admin'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'settings-settings', 'xget': 'NewUser', 'user_id': $stateParams.user_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('identity', {
        url:'/identity/:identity_id', templateUrl:'sTemplate/identityEdit', controller:'IdentityCtrl as vm', data: {
            group: 'admin'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'settings-settings', 'xget': 'NewIdentity', 'identity_id': $stateParams.identity_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('users', {
        url:'/users/', templateUrl:'sTemplate/users', controller:'MyUsersCtrl as vm', resolve: {
            data:function(helper) {
                /*angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'settings-settings', 'xget': 'Users'
                }
                ).then(function(d) {
                    return d.data
                }
                , function() {
                    return {}
                }
                )*/
                return {}
            }
        }
        , data: {
            menu: '#', submenu: '#'
        }
    }
    ).state('clearfacts', {
        url:'/clearfacts/?code', templateUrl:'sTemplate/clearfactsCallback', controller:'clearfactsCallbackCtrl', data: {
            menu: 'a', submenu: 'b'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'settings-clearfacts_callback', 'code': $stateParams.code
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('exact_online', {
        url:'/exact_online/?code', templateUrl:'sTemplate/exact_onlineCallback', controller:'exact_onlineCallbackCtrl', data: {
            menu: 'a', submenu: 'b'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'settings-exact_online_callback', 'code': $stateParams.code
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('dropbox', {
        url:'/dropbox/?code', templateUrl:'sTemplate/dropboxCallback', controller:'dropboxCallbackCtrl', data: {
            menu: 'a', submenu: 'b'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'settings-dropbox_callback', 'code': $stateParams.code
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('kc', {
        url:'/kc/?code', templateUrl:'sTemplate/kcCallback', controller:'kcCallbackCtrl', data: {
            menu: 'a', submenu: 'b'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'settings-kc_callback', 'code': $stateParams.code
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    )
}

])