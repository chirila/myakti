angular.module('report', []).config(['$provide', '$stateProvider', function($provide, $stateProvider) {
    $stateProvider.state('report_catalogue', {
        url: '/report_catalogue/',
        templateUrl: 'rTemplate/ReportCatalogue',
        controller: 'ReportCatalogueCtrl',
        data: {
            menu: 'reports',
            submenu: 'report_catalogue'
        },
        resolve: {
            data: function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'report-xarticle_report',
                    'period_id': '0'
                }).then(function(d) {
                    return d
                }, function() {
                    return !0
                })
            }
        }
    }).state('report_catalogue_invoices', {
        url: '/report_catalogue_invoices/',
        templateUrl: 'rTemplate/ReportCatalogue_invoices',
        controller: 'ReportCatalogueInvoicesCtrl',
        data: {
            menu: 'reports',
            submenu: 'report_catalogue_invoices'
        },
        resolve: {
            data: function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'report-xarticle_report_invoices',
                    'period_id': '0'
                }).then(function(d) {
                    return d
                }, function() {
                    return !0
                })
            }
        }
    }).state('report_quotes', {
        url: '/report_quotes/',
        templateUrl: 'rTemplate/ReportQuote',
        controller: 'ReportQuoteCtrl',
        data: {
            menu: 'reports',
            submenu: 'report_quotes'
        },
        resolve: {
            data: function($cacheFactory, helper) {
                var initData = {
                    'do': 'report-xquote_report',
                    'period_id': '0'
                };
                var cacheReport = $cacheFactory.get('ReportDigestToQuote');
                if (cacheReport != undefined) {
                    var cachedDataReport = cacheReport.get('data')
                }
                if (cachedDataReport != undefined) {
                    for (x in cachedDataReport) {
                        initData[x] = cachedDataReport[x]
                    }
                }
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', initData).then(function(d) {
                    return d
                }, function() {
                    return !0
                })
            }
        }
    }).state('report_qdigest', {
        url: '/report_qdigest/',
        templateUrl: 'rTemplate/ReportQuoteDigest',
        controller: 'ReportQuoteDigestCtrl',
        data: {
            menu: 'reports',
            submenu: 'report_quotes'
        },
        resolve: {
            data: function($cacheFactory, helper) {
                var initData = {
                    'xget': 'Digest'
                };
                var cacheReport = $cacheFactory.get('ReportQuoteToDigest');
                if (cacheReport != undefined) {
                    var cachedDataReport = cacheReport.get('data')
                }
                if (cachedDataReport != undefined) {
                    for (x in cachedDataReport) {
                        initData[x] = cachedDataReport[x]
                    }
                }
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', initData).then(function(d) {
                    return d
                }, function() {
                    return !0
                })
            }
        }
    }).state('report_orders', {
        url: '/report_orders/',
        templateUrl: 'rTemplate/ReportOrder',
        controller: 'ReportOrderCtrl',
        data: {
            menu: 'reports',
            submenu: 'report_orders'
        },
        resolve: {
            data: function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'report-xorder_report',
                    'period_id': '0'
                }).then(function(d) {
                    return d
                }, function() {
                    return !0
                })
            }
        }
    }).state('report_p_orders', {
        url: '/report_p_orders/',
        templateUrl: 'rTemplate/ReportPOrder',
        controller: 'ReportPOrderCtrl',
        data: {
            menu: 'reports',
            submenu: 'report_p_orders'
        },
        resolve: {
            data: function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'report-xporder_report',
                    'period_id': '0'
                }).then(function(d) {
                    return d
                }, function() {
                    return !0
                })
            }
        }
    }).state('report_projects', {
        url: '/report_projects/',
        templateUrl: 'rTemplate/ReportProject',
        controller: 'ReportProjectCtrl',
        data: {
            menu: 'reports',
            submenu: 'report_projects'
        },
        resolve: {
            data: function($cacheFactory, helper) {
                var initData = {
                    'do': 'report-xproject_report',
                    'period_id': '0'
                };
                var cacheReport = $cacheFactory.get('ReportDetailToProject');
                if (cacheReport != undefined) {
                    var cachedDataReport = cacheReport.get('data')
                }
                if (cachedDataReport != undefined) {
                    for (x in cachedDataReport) {
                        initData[x] = cachedDataReport[x]
                    }
                }
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', initData).then(function(d) {
                    return d
                }, function() {
                    return !0
                })
            }
        }
    }).state('report_pdetail', {
        url: '/report_pdetail/',
        templateUrl: 'rTemplate/ReportProjectDetailed',
        controller: 'ReportProjectDetailedCtrl',
        data: {
            menu: 'reports',
            submenu: 'report_projects'
        },
        resolve: {
            data: function($cacheFactory, helper) {
                var initData = {
                    'do': 'report-xproject_report',
                    'xget': 'ReportDetailed'
                };
                var cacheReport = $cacheFactory.get('ReportProjectToDetail');
                if (cacheReport != undefined) {
                    var cachedDataReport = cacheReport.get('data')
                }
                if (cachedDataReport != undefined) {
                    for (x in cachedDataReport) {
                        initData[x] = cachedDataReport[x]
                    }
                }
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', initData).then(function(d) {
                    return d
                }, function() {
                    return !0
                })
            }
        }
    }).state('report_invoices', {
        url: '/report_invoices/',
        templateUrl: 'rTemplate/ReportInvoice',
        controller: 'ReportInvoiceCtrl',
        data: {
            menu: 'reports',
            submenu: 'report_invoices'
        },
        resolve: {
            data: function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'report-xinvoice_report',
                    'period_id': '0'
                }).then(function(d) {
                    return d
                }, function() {
                    return !0
                })
            }
        }
    }).state('report_client_history', {
        url: '/report_client_history/',
        templateUrl: 'rTemplate/ReportClientHistory',
        controller: 'ReportClientHistoryCtrl',
        data: {
            menu: 'reports',
            submenu: 'report_client_history'
        },
        resolve: {
            data: function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'report-xclient_history_report',
                    'period_id': '0'
                }).then(function(d) {
                    return d
                }, function() {
                    return !0
                })
            }
        }
    })
}])