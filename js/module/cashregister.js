angular.module('cashregister', []).config(['$provide', '$stateProvider', function($provide, $stateProvider) {
    $stateProvider.state('cashregister', {
        url:'/cashregister/', templateUrl:'cashTemplate/cashArticles', controller:'cashArticlesCtrl', data: {
            menu: 'cashregister', submenu: 'cash_articles'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'cashregister-articles'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('cashorder', {
        url:'/cashorder/', templateUrl:'cashTemplate/cashOrders', controller:'cashOrdersCtrl', data: {
            menu: 'cashregister', submenu: 'cash_orders'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'cashregister-orders'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('cashaccount', {
        url:'/cashaccount/', templateUrl:'cashTemplate/cashAccount', controller:'cashAccountCtrl', data: {
            menu: 'cashregister', submenu: 'cash_company'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'cashregister-account'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    ).state('cashcustomer', {
        url:'/cashcustomer/', templateUrl:'cashTemplate/cashCustomer', controller:'cashCustomerCtrl', data: {
            menu: 'cashregister', submenu: 'cash_customer'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'cashregister-customer'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    )
}

])