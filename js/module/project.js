angular.module('project', []).config(['$provide', '$stateProvider', function($provide, $stateProvider) {
    $stateProvider.state('projects', {
        url:'/projects/', templateUrl:'pTemplate/projects', controller:'projectsCtrl', data: {
            menu: 'projects', submenu: 'projects'
        }
        , resolve: {
            data:function(helper) {
                return!0
            }
        }
    }
    ).state('projectSettings', {
        url:'/projectSettings/', templateUrl:'pTemplate/projectSettings', controller:'ProjectSettingsCtrl as vm', data: {
            menu: 'projects', submenu: 'projects setting', adminSetting: 'project'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'project-settings', 'xget': 'name_convention'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('project', {
        abstract:!0, url:'/project/', templateUrl:'pTemplate/project', controller:'projectCtrl', data: {
            menu: 'projects', submenu: 'projects'
        }
    }
    ).state('project.edit', {
        url:'edit/:project_id?buyer_id', templateUrl:'pTemplate/projectEdit', controller:'ProjectEditCtrl as edit', data: {
            menu: 'projects', submenu: 'projects'
        }
        ,
    }
    ).state('project.report', {
        url:'report/:project_id', templateUrl:'pTemplate/projectReport', controller:'ProjectReportCtrl as rep', data: {
            menu: 'projects', submenu: 'projects'
        }
        ,
    }
    ).state('project.report_detailed', {
        url:'report_detailed/:project_id', templateUrl:'pTemplate/projectReportDetailed', controller:'ProjectReportDetailedCtrl as rep', data: {
            menu: 'projects', submenu: 'projects'
        }
        ,
    }
    ).state('pservices', {
        url:'/pservices/', templateUrl:'pTemplate/pservices', controller:'ProjectServicesCtrl', data: {
            menu: 'projects', submenu: 'pservices'
        }
        , resolve: {
            data:function(helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'project-pservices'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('pservice', {
        abstract:!0, url:'/pservice/', templateUrl:'pTemplate/pservice', controller:'ProjectServiceCtrl', data: {
            menu: 'projects', submenu: 'pservices'
        }
    }
    ).state('pservice.edit', {
        url:'edit/:article_id', templateUrl:'pTemplate/serviceEdit', controller:'ProjectServiceEditCtrl as edit', data: {
            menu: 'projects', submenu: 'pservices'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'project-pservice', 'article_id': $stateParams.article_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('project_invoice', {
        url:'/project_invoice/', templateUrl:'pTemplate/projectInvoice', controller:'ProjectInvoiceCtrl', data: {
            menu: 'projects', submenu: 'project_invoice'
        }
        , resolve: {
            data:function($stateParams, helper) {
                angular.element('.loading_wrap').removeClass('hidden');
                return helper.doRequest('get', 'index.php', {
                    'do': 'project-to_be_invoiced'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('project_deliver', {
        url:'/project_deliver/', templateUrl:'pTemplate/projectDeliver', controller:'ProjectDeliverCtrl', data: {
            menu: 'projects', submenu: 'project_deliver'
        }
        ,
    }
    ).state('ptimesheets', {
        url:'/ptimesheets/', templateUrl:'tTemplate/pTimesheets', controller:'ProjectTimesheetCtrl', data: {
            menu: 'timetracker', submenu: 'ptimesheets'
        }
        , resolve: {
            data:function($stateParams, helper) {}
        }
    }
    )
}

])