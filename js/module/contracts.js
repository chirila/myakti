angular.module('contracts', []).config(['$provide', '$stateProvider', function($provide, $stateProvider) {
    $stateProvider/*.state('contracts', {
        url:'/contracts/', templateUrl:'conTemplate/contracts', controller:'contractsCtrl', data: {
            menu: 'contracts', submenu: 'contracts'
        }
        , resolve: {
            data:function(helper) {
                return!0
            }
        }
    }
    )*/.state('contract', {
        abstract:!0, url:'/contract/', templateUrl:'conTemplate/contract', controller:'contractCtrl as vm', data: {
            menu: 'contracts', submenu: 'contracts'
        }
    }
    )/*.state('contract.edit', {
        url:'edit/:contract_id?version_id?buyer_id', templateUrl:'conTemplate/contractEdit', controller:'contractEditCtrl as edit', data: {
            menu: 'contracts', submenu: 'contracts'
        }
        , resolve: {
            data:function($cacheFactory, $stateParams, helper) {
                var initData= {
                    'do': 'contract-contract', 'contract_id': $stateParams.contract_id, 'version_id': $stateParams.version_id, 'buyer_id': $stateParams.buyer_id
                }
                ;
                var cacheDeal=$cacheFactory.get('DealToDocument');
                if(cacheDeal!=undefined) {
                    var cachedDataDeal=cacheDeal.get('data')
                }
                if(cachedDataDeal!=undefined) {
                    for(x in cachedDataDeal) {
                        initData[x]=cachedDataDeal[x]
                    }
                    cacheDeal.remove('data')
                }
                return helper.doRequest('get', 'index.php', initData).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('contract.view', {
        url:'view/:contract_id?version_id', templateUrl:'conTemplate/contractView', controller:'contractViewCtrl as view', data: {
            menu: 'contracts', submenu: 'contracts'
        }
        , resolve: {
            data:function($stateParams, helper) {
                return helper.doRequest('get', 'index.php', {
                    'do': 'contract-contract', 'contract_id': $stateParams.contract_id, 'version_id': $stateParams.version_id, 'xview': 1
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    ).state('contract.editTemplate', {
        url:'editTemplate/:template_id?contract_id?version_id', templateUrl:'conTemplate/contractEditTemplate', controller:'ContractEditTemplateCtrl as edit', data: {
            menu: 'contracts', submenu: 'contractTemplates'
        }
        , resolve: {
            data:function($stateParams, helper) {
                return helper.doRequest('get', 'index.php', {
                    'do': 'contract-contract', 'template_id': $stateParams.template_id, 'version_id': $stateParams.version_id, 'contract_id': $stateParams.contract_id
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return {}
                }
                )
            }
        }
    }
    )*//*.state('contractTemplates', {
        url:'/contractTemplates/', templateUrl:'conTemplate/contractTemplates', controller:'contractTemplatesCtrl as vm', data: {
            menu: 'contracts', submenu: 'contractTemplates'
        }
        , resolve: {
            data:function(helper) {
                return helper.doRequest('get', 'index.php', {
                    'do': 'contract-contracts_templates'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    )*//*.state('contractSettings', {
        url:'/contractSettings/', templateUrl:'conTemplate/contractSettings', controller:'contractSettingsCtrl as vm', data: {
            menu: 'contracts', submenu: 'contracts setting', adminSetting: 'contract'
        }
        , resolve: {
            data:function(helper) {
                return helper.doRequest('get', 'index.php', {
                    'do': 'contract-settings', 'xget': 'PDFlayout'
                }
                ).then(function(d) {
                    return d
                }
                , function() {
                    return!0
                }
                )
            }
        }
    }
    )*/
}

])