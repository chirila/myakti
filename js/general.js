(function () {
    "use strict";
    $("#agree_checkbox").click(function () {
        if(this.checked){
            $("#create_account_button").removeClass("btn-disabled");
        	$("#create_account_button").prop("disabled", false);
        }else{
            $("#create_account_button").addClass("btn-disabled");
        	$("#create_account_button").prop("disabled", true);
        }

    });
})(jQuery);

function explode(delimiter,string,limit){if(arguments.length<2||typeof delimiter=='undefined'||typeof string=='undefined')return null;if(delimiter===''||delimiter===!1||delimiter===null)return!1;if(typeof delimiter=='function'||typeof delimiter=='object'||typeof string=='function'||typeof string=='object'){return{0:''}}
if(delimiter===!0)delimiter='1';delimiter+='';string+='';var s=string.split(delimiter);if(typeof limit==='undefined')return s;if(limit===0)limit=1;if(limit>0){if(limit>=s.length)return s;return s.slice(0,limit-1).concat([s.slice(limit-1).join(delimiter)])}
if(-limit>=s.length)return[];s.splice(s.length+limit);return s}
function getLanguage(str){return str}
function corect_val(number){var what_char=number.toString().search(/\D/),the_char=number.toString().charAt(what_char);if(the_char==':'){var zhe_number=explode(the_char,number);if(isNaN(zhe_number[1])){zhe_number[1]='00'}
if(zhe_number[1]>=60){alert(getLanguage("Invalid format"));value='';return value}
if(zhe_number[1].length==1){zhe_number[1]='0'+zhe_number[1]}
value=zhe_number[0]+':'+zhe_number[1]}
else if(the_char==','){var minutes=number.replace(the_char,'.');if(isNaN(minutes)){minutes=0}
minutes=Math.round(minutes*60);if(minutes>=60){n=Math.floor(minutes/60);minutes=minutes-(60*n);if(minutes.toString().length==1){minutes='0'+minutes}
value=n+":"+minutes}
else{if(minutes.toString().length==1){minutes='0'+minutes}
value="0:"+minutes}}
else if(the_char=='.'){var minutes=number;if(isNaN(minutes)){minutes=0}
minutes=Math.round(minutes*60);if(minutes>=60){n=Math.floor(minutes/60);minutes=minutes-(60*n);if(minutes.toString().length==1){minutes='0'+minutes}
value=n+":"+minutes}
else{if(minutes.toString().length==1){minutes='0'+minutes}
value="0:"+minutes}}else if(!isNaN(number)){value=number+':00';if(!number){value=''}}else{value=''}
return value}
function convert_value(number){if(!number){return'0.00'}
var what_char=number.search(/\D/),the_char=number.charAt(what_char);if(the_char==':'){var zhe_number=explode(the_char,number);value=(parseInt(zhe_number[0])+zhe_number[1]/60).toFixed(4)}
else if(the_char==','){value=number.replace(the_char,'.')}
else if(the_char=='.'){value=number}else{value='0.00'}
return value}
function number_format(number,decimals,dec_point,thousands_sep){number=(number+'').replace(/[^0-9+\-Ee.]/g,'');var n=!isFinite(+number)?0:+number,prec=!isFinite(+decimals)?0:Math.abs(decimals),sep=(typeof thousands_sep==='undefined')?',':thousands_sep,dec=(typeof dec_point==='undefined')?'.':dec_point,s='',toFixedFix=function(n,prec){var k=Math.pow(10,prec);return''+Math.round(n*k)/k};s=(prec?toFixedFix(n,prec):''+Math.round(n)).split('.');if(s[0].length>3){s[0]=s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g,sep)}
if((s[1]||'').length<prec){s[1]=s[1]||'';s[1]+=new Array(prec-s[1].length+1).join('0')}
return s.join(dec)}
function str_replace(search,replace,subject,count){var i=0,j=0,temp='',repl='',sl=0,fl=0,f=[].concat(search),r=[].concat(replace),s=subject,ra=Object.prototype.toString.call(r)==='[object Array]',sa=Object.prototype.toString.call(s)==='[object Array]';s=[].concat(s);if(count){this.window[count]=0}
for(i=0,sl=s.length;i<sl;i++){if(s[i]===''){continue}
for(j=0,fl=f.length;j<fl;j++){temp=s[i]+'';repl=ra?(r[j]!==undefined?r[j]:''):r[0];s[i]=(temp).split(f[j]).join(repl);if(count&&s[i]!==temp){this.window[count]+=(temp.length-s[i].length)/f[j].length}}}
return sa?s:s[0]}
function base64_encode(data){var b64="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";var o1,o2,o3,h1,h2,h3,h4,bits,i=0,ac=0,enc="",tmp_arr=[];if(!data){return data}
data=utf8_encode(data+'');do{o1=data.charCodeAt(i++);o2=data.charCodeAt(i++);o3=data.charCodeAt(i++);bits=o1<<16|o2<<8|o3;h1=bits>>18&0x3f;h2=bits>>12&0x3f;h3=bits>>6&0x3f;h4=bits&0x3f;tmp_arr[ac++]=b64.charAt(h1)+b64.charAt(h2)+b64.charAt(h3)+b64.charAt(h4)}while(i<data.length);enc=tmp_arr.join('');switch(data.length%3){case 1:enc=enc.slice(0,-2)+'==';break;case 2:enc=enc.slice(0,-1)+'=';break}
return enc}
function utf8_encode(argString){if(argString===null||typeof argString==="undefined"){return""}
var string=(argString+'');var utftext="",start,end,stringl=0;start=end=0;stringl=string.length;for(var n=0;n<stringl;n++){var c1=string.charCodeAt(n);var enc=null;if(c1<128){end++}else if(c1>127&&c1<2048){enc=String.fromCharCode((c1>>6)|192)+String.fromCharCode((c1&63)|128)}else{enc=String.fromCharCode((c1>>12)|224)+String.fromCharCode(((c1>>6)&63)|128)+String.fromCharCode((c1&63)|128)}
if(enc!==null){if(end>start){utftext+=string.slice(start,end)}
utftext+=enc;start=end=n+1}}
if(end>start){utftext+=string.slice(start,stringl)}
return utftext}
Date.prototype.addMonths=function(m){var d=this;if(d.getDate()<28){d.setMonth(d.getMonth()+m);return d}else{var new_date=new Date(JSON.parse(JSON.stringify(d)));new_date=new Date(new_date.setDate(1));new_date.setMonth(new_date.getMonth()+m+1);var ld_next_month=new Date(new_date.setDate(-1));if(d.getDate()<=ld_next_month.getDate()){d.setMonth(d.getMonth()+m);return d}else{return ld_next_month}}}
function convert_to_time(txt){let tmp_split_date=txt.split(":");let time_date=new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate(),new Date().getHours(),new Date().getMinutes());time_date.setHours(parseInt(tmp_split_date[0]),parseInt(tmp_split_date[1]));return time_date}