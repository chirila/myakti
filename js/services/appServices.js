angular.module('akti').factory('helper', ['$http', '$rootScope', '$q', '$timeout', '$templateCache', '$state', 'UserService', function($http, $rootScope, $q, $timeout, $templateCache, $state, UserService) {
    var helper = {
            console: [],
            settings: [],
            main_menu: [],
            Loading: 0,
            searchPromise: undefined,
            headers: undefined,
            doRequest: doRequest,
            getSettings: getSettings,
            return_value: return_value,
            displayNr: displayNr,
            searchIt: searchIt,
            setItem: setItem,
            setItemSimple: setItemSimple,
            getItem: getItem,
            getItemSimple: getItemSimple,
            showAlerts: showAlerts,
            showAlertsManual: showAlertsManual,
            unique_id: unique_id,
            clearData: clearData,
            round: round,
            round_n: round_n,
            generatePassword: generatePassword,
            getLanguage: getLanguage,
            removeCachedTemplates: removeCachedTemplates,
            uploadSingleItem: uploadSingleItem,
            uploadDropSepa: uploadDropSepa,
            refreshLogging: refreshLogging,
            changedData: changedData,
            updateTitle: updateTitle
        },
        canceler, item = [];
    return helper;

    function doRequest() {
        switch (arguments.length) {
            case 0:
                console.error('Missing parameters method,url,callback');
                return !1;
                break;
            case 1:
                console.error('Missing parameters url,callback');
                return !1;
                break;
            case 2:
                var data = {},
                    method = arguments[0],
                    url = arguments[1],
                    params = {};
                break;
            default:
                var data = {},
                    method = arguments[0],
                    url = arguments[1];
                switch (typeof(arguments[2])) {
                    case 'object':
                        var params = arguments[2];
                        if (arguments[3] && typeof(arguments[3]) == 'function') {
                            var callback = arguments[3]
                        } else {}
                        break;
                    case 'function':
                        var callback = arguments[2];
                        if (arguments[3] && typeof(arguments[3]) == 'object') {
                            var params = arguments[3]
                        }
                        break;
                    default:
                        if (arguments[3] && typeof(arguments[3]) == 'function') {
                            var callback = arguments[3]
                        } else {}
                        break
                }
                break
        }
        var headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };
        if (method == 'post') {
            data = params;
            params = {}
        }
        if (data && data.__proto__.constructor.name == 'FormData') {
            headers = {
                'Content-Type': undefined
            }
        }
        if (helper.headers) {
            headers = helper.headers;
            helper.headers = undefined
        }
        this.data = $http({
            method: method,
            url: url,
            params: params,
            data: data,
            headers: headers
        }).then(function(response) {
            if (helper.Loading <= 0) {
                angular.element('.loading_wrap').addClass('hidden')
            }
            helper.Loading--;
            if (helper.Loading < 0) {
                helper.Loading = 0
            }
            if (response.data && response.data.notice && response.data.notice.notice == "You don't have permission to access this page") {
                if (helper.settings.WIZZARD_COMPLETE == '0') {
                    $state.go('board');
                    return !1
                } else {
                    UserService.isLoged = 0;
                    $state.go('login');
                    return !1
                }
            }
            $rootScope.$broadcast('dataLoaded', (response.data.success ? true : false));
            if (callback) {
                callback(response.data)
            }
            if (response.data.console) {
                helper.console.unshift(response.data.console)
            }
            return response.data
        }, function(response) {
            if (helper.Loading <= 0) {
                //angular.element('.loading_wrap').addClass('hidden')
            }
            $rootScope.$broadcast('dataLoaded', true);
            if (callback) {
                callback(response)
            }
            helper.Loading--;
            if (helper.Loading < 0) {
                helper.Loading = 0
            }
            console.warn('Error!');
            return
        });
        return this.data
    }

    function getSettings($get_anyway) {
        helper.Loading++;
        var response = helper.doRequest('get', 'index.php', {
            'do': 'misc-generalSettings',
            'get_anyway': $get_anyway
        }, function(r) {
            if (r && r.data) {
                for (x in r.data) {
                    if (x == 'show_settings') {
                        $rootScope.show_settings = r.data[x];
                    } else {
                        helper[x] = r.data[x]
                    }
                }
            }
            return !0
        });
        return response
    }

    function return_value(number) {
        var style = this.settings.ACCOUNT_NUMBER_FORMAT;
        var number1 = str_replace(style[0], '', number);
        var number2 = number1.replace(style[1], '.');
        return number2
    }

    function displayNr(number, digit) {
        var style = this.settings.ACCOUNT_NUMBER_FORMAT,
            digit = digit || 2;
        return number_format(number, digit, style[1], style[0])
    }

    function searchIt(item, callback) {
        if (helper.searchPromise) {
            $timeout.cancel(helper.searchPromise)
        }
        helper.searchPromise = helper.doRequest('post', 'index.php', item, callback)
    }

    function setItem(type, obj) {
        item[type] = obj
    }

    function setItemSimple(type, obj, value) {
        item[type][obj] = value
    }

    function getItem(type) {
        if (item && item[type]) {
            var newItem = angular.copy(item[type]);
            delete item[type];
            return newItem
        }
        return
    }

    function getItemSimple(type) {
        if (item && item[type]) {
            var newItem = angular.copy(item[type]);
            return newItem
        }
        return
    }

    function showAlerts(scope, d, formObj) {
        if (typeof(d) !== 'object' || (d.status && d.status != 200)) {
            if (formObj) {
                formObj.$invalid = !1
            }
            return
        }
        if (!scope.alerts) {
            scope.alerts = []
        }
        if (!scope.alertsField) {
            scope.alertsField = []
        }
        if (d.error && d.error.error) {
            scope.alerts.push({
                type: 'danger',
                msg: d.error.error
            })
        } else if (d.error) {
            for (x in d.error) {
                if (formObj) {
                    formObj[x].$pristine = !1;
                    formObj[x].$invalid = !0;
                    formObj.$invalid = !0;
                    scope.alertsField[x] = d.error[x]
                } else {
                    scope.alerts.push({
                        type: 'danger',
                        msg: d.error[x]
                    })
                }
            }
        }
        if (d.notice && d.notice.notice) {
            scope.alerts.push({
                type: 'info',
                msg: d.notice.notice
            });
            if (formObj) {
                formObj.$invalid = !1
            }
        }
        if (d.success && d.success.success) {
            scope.alerts.push({
                type: 'success',
                msg: d.success.success
            });
            if (formObj) {
                formObj.$invalid = !1
            }
        }
    }

    function showAlertsManual(scope, d) {
        if (!scope.alerts) {
            scope.alerts = []
        }
        if (!scope.alertsField) {
            scope.alertsField = []
        } else {
            scope.alertsField = []
        }
        if (d.error && d.error.error) {
            scope.alerts.push({
                type: 'danger',
                msg: d.error.error
            })
        } else if (d.error) {
            for (x in d.error) {
                scope.alertsField[x] = d.error[x];
            }
        }
        if (d.notice && d.notice.notice) {
            scope.alerts.push({
                type: 'info',
                msg: d.notice.notice
            });
        }
        if (d.success && d.success.success) {
            scope.alerts.push({
                type: 'success',
                msg: d.success.success
            });
        }
    }

    function round(number, precision) {
        var factor = Math.pow(10, precision);
        var tempNumber = number * factor;
        if (tempNumber < 0) {
            var roundedTempNumber = Math.floor(tempNumber)
        } else {
            var roundedTempNumber = Math.round(tempNumber)
        }
        return roundedTempNumber / factor
    }

    function round_n(number, precision) {
        var factor = Math.pow(10, precision);
        var tempNumber = number * factor;
        var roundedTempNumber = Math.round(tempNumber);
        return roundedTempNumber / factor
    }

    function unique_id() {
        return Math.random().toString(36).substring(7)
    }

    function clearData(data, array) {
        for (x in array) {
            if (data[array[x]]) {
                data[array[x]] = undefined
            }
        }
        return data
    }

    function generatePassword(length, special) {
        var iteration = 0;
        var password = "";
        var randomNumber;
        if (special == undefined) {
            var special = !1
        }
        while (iteration < length) {
            randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
            if (!special) {
                if ((randomNumber >= 33) && (randomNumber <= 47)) {
                    continue
                }
                if ((randomNumber >= 58) && (randomNumber <= 64)) {
                    continue
                }
                if ((randomNumber >= 91) && (randomNumber <= 96)) {
                    continue
                }
                if ((randomNumber >= 123) && (randomNumber <= 126)) {
                    continue
                }
            }
            iteration++;
            password += String.fromCharCode(randomNumber)
        }
        if (password.search(/[0-9]/) == -1) {
            return helper.generatePassword(8)
        }
        if (password.search(/[A-Z]/) == -1) {
            return helper.generatePassword(8)
        }
        if (password.search(/[a-z]/) == -1) {
            return helper.generatePassword(8)
        }
        return password
    }

    function getLanguage(text) {
        var base64 = base64_encode(text);
        if (!LANG[base64]) {
            helper.doRequest('post', 'index.php', {
                'do': 'misc--lang-add',
                'word': text
            }, function(r) {});
            LANG[base64] = text;
            return text
        }
        return LANG[base64]
    }

    function removeCachedTemplates() {
        var templates = angular.copy($templateCache.getKeys());
        for (x in templates) {
            if (templates[x].search('uib/template/') === -1) {
                $templateCache.remove(templates[x])
            }
        }
    }

    function uploadSingleItem(scope, data, cover, formObj) {
        x = angular.element('#uploadFile');
        if (x[0].files[0] != undefined && x[0].files[0].size > 2560000) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Error: file is to big.")
                },
                "notice": !1,
                "success": !1
            };
            showAlerts(obj);
            return !1
        }
        var formData = new FormData();
        for (y in data) {
            formData.append(y, data[y])
        }
        formData.append("do", data.do_next);
        if (x[0].files[0] != undefined) {
            formData.append("Filedata", x[0].files[0])
        }
        var oReq = new XMLHttpRequest();
        angular.element('.' + cover).removeClass('hidden');
        oReq.open("POST", "index.php", !0);
        oReq.onload = function(oEvent) {
            var res = JSON.parse(oEvent.target.response);
            angular.element('.' + cover).addClass('hidden');
            if (formObj != undefined) {
                showAlerts(scope, res, formObj)
            } else {
                showAlerts(scope, res)
            }
        };
        oReq.send(formData)
    }

    function refreshLogging() {
        $rootScope.$broadcast('refreshLogging', true);
    }

    function changedData() {
        $rootScope.$broadcast('dataChanged', true);
    }

    function updateTitle(state, word) {
        if (state && app_titles[state] !== undefined) {
            document.title = page_title_default + (app_titles[state].length ? ' - ' + helper.getLanguage(app_titles[state]) : '') + (word ? ' ' + word : '');
        } else {
            document.title = page_title_default;
        }
    }

    function uploadDropSepa(scope, e) {
        angular.element('.newUploadFile5').on('click', function(e) {
            e.stopPropagation()
        }).on('change', function(e) {
            e.preventDefault();
            var _this = $(this),
                p = _this.parents('.upload-box'),
                x = p.find('.newUploadFile5');
            if (x[0].files.length == 0) {
                var obj = {
                    "error": {
                        "error": helper.getLanguage("Error: no file")
                    },
                    "notice": !1,
                    "success": !1
                };
                showAlerts(obj);
                return !1
            }
            if (x[0].files[0] != undefined && x[0].files[0].size > 2560000) {
                var obj = {
                    "error": {
                        "error": helper.getLanguage("Error: file is to big.")
                    },
                    "notice": !1,
                    "success": !1
                };
                showAlerts(obj);
                return !1
            }
            p.find('.btn').addClass('hidden');
            p.find('.nameOfTheFile').text(x[0].files[0].name);
            var formData = new FormData();
            formData.append("Filedata", x[0].files[0]);
            formData.append("do", 'invoice--invoice-uploadPDF_sepa');
            formData.append("mandate_id", scope.item.mandate_id);
            p.find('.upload-progress-bar').css({
                width: '0%'
            });
            p.find('.upload-file').removeClass('hidden');
            var oReq = new XMLHttpRequest();
            //angular.element('.loading_alt').removeClass('hidden');
            oReq.open("POST", "index.php", !0);
            oReq.upload.addEventListener("progress", function(e) {
                var pc = parseInt(e.loaded / e.total * 100);
                p.find('.upload-progress-bar').css({
                    width: pc + '%'
                })
            });
            oReq.onload = function(oEvent) {
                //angular.element('.loading_alt').addClass('hidden');
                var res = JSON.parse(oEvent.target.response);
                p.find('.upload-file').addClass('hidden');
                p.find('.btn').removeClass('hidden');
                if (oReq.status == 200) {
                    showAlerts(scope, res);
                    if (res.success) {
                        scope.item.PDF_hide = !1;
                        scope.item.name_PDF = !0;
                        scope.item.original_name = res.data.original_name
                    }
                } else {
                    var obj = {
                        "error": {
                            "error": helper.getLanguage("Error ") + oReq.status + helper.getLanguage(" occurred when trying to upload your file.")
                        },
                        "notice": !1,
                        "success": !1
                    };
                    showAlerts(obj)
                }
            };
            oReq.send(formData)
        });
        angular.element(e.target).parents('.upload-box').find('.newUploadFile5').click()
    }
}]).factory('UserService', ['$http', function($http) {
    var user = {
        isLoggedIn: undefined,
        isLoged: new Date().getTime() + (30 * 60) * 1000,
        name: '',
        group: '',
        adminSetting: [],
        checkAuthAsync: checkAuthAsync,
        reset: reset
    };
    return user;

    function checkAuthAsync() {
        this.e = $http.get('index.php?do=auth-isLoged').then(function(response) {
            if (response.status == 200) {
                if (response.data.console) {}
                return response.data
            } else {
                return
            }
        });
        return this.e
    }

    function reset() {
        user.isLoggedIn = !1;
        user.name = '';
        user.group = '';
        user.adminSetting = [];
        user.must_pay = !1
    }
}]).factory('list', ['$timeout', 'helper', function($timeout, helper) {
    var list = {
        init: init,
        search: search,
        filter: filter,
        tSearch: toggleSearch,
        action: action,
        orderBy: orderBy,
        saveForPDF: saveForPDF,
        selectItems: selectItems,
        exportFnc: exportFnc,
        markCheckNew: markCheckNew,
        markCheckNewAll: markCheckNewAll
    };
    return list;

    function init(scope, url, callback, reset_list) {
        var list = angular.copy(helper.getItem(url)),
            params = {
                'do': url
            };
        if (reset_list) {
            params.reset_list = '1';
        }
        var excludes = ['quote-quotes'];
        if (list && list.search && list.search.xget && excludes.indexOf(params.do) >= 0) {
            delete list.search.xget
        }
        if (list && list.search) {
            for (x in list.search) {
                if (x.indexOf('date_js') > -1) {
                    list.search[x] = new Date(list.search[x])
                }
                params[x] = list.search[x]
            }
            $timeout(function() {
                for (x in list.search) {
                    scope.search[x] = list.search[x]
                }
                scope.activeView = list.search.view;
                scope.search.skipSearchThing = !0;
                scope.reverse = list.search.desc;
                scope.ord = list.search.order_by;
                if (scope.search.showAdvanced) {
                    scope.search.skipSearchThing = !1
                }
                if (scope.search.do == 'customers-customers') {
                    init(scope, url, callback)
                }
            })
        }
        angular.element('.loading_wrap').removeClass('hidden');
        if (scope.search.do == 'invoice-invoices') {
            helper.doRequest('get', 'index.php', params, function(r) {
                callback(r, !0)
            })
        } else if (scope.search.do == 'invoice-purchase_invoices') {
            helper.doRequest('get', 'index.php', params, function(r) {
                callback(r, !0)
            })
        } else if (scope.search.do == 'invoice-late_invoices') {
            helper.doRequest('get', 'index.php', params, function(r) {
                callback(r, 0, !0)
            })
        } else {
            helper.doRequest('get', 'index.php', params, callback)
        }
    }

    function search(url, callback, scope) {
        //angular.element('.loading_wrap').first().removeClass('hidden');
        helper.searchIt(url, callback)
    }

    function filter(scope, p, callback, reset_list) {
        if (scope.activeView == p) {
            return !1
        }
        scope.activeView = p;
        var params = {};
        for (x in scope.search) {
            params[x] = scope.search[x]
        }
        params.view = p;
        if (reset_list) {
            params.reset_list = '1';
        }
        scope.search.view = scope.activeView;
        scope.search.offset = 1;
        scope.ord = '';
        scope.reverse = !1;
        search(params, callback, scope)
    }

    function toggleSearch(scope, i, v, callback, reset_list) {
        scope.search[i] = v;
        var params = {
            view: scope.activeView,
            offset: scope.search.offset
        };
        for (x in scope.search) {
            params[x] = scope.search[x]
        }
        if (reset_list) {
            params.reset_list = '1';
        }
        search(params, callback, scope)
    }

    function action(scope, item, action, callback, reset_list) {
        var params = {
            view: scope.activeView,
            offset: (scope.search ? scope.search.offset : 0)
        };
        for (x in scope.search) {
            params[x] = scope.search[x]
        }
        for (x in item[action]) {
            params[x] = item[action][x]
        }
        if (reset_list) {
            params.reset_list = '1';
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, callback)
    }

    function orderBy(scope, p, callback) {
        if (p == scope.ord && scope.reverse) {
            scope.ord = '';
            scope.reverse = !1
        } else {
            if (p == scope.ord) {
                scope.reverse = !scope.reverse
            } else {
                scope.reverse = !1
            }
            scope.ord = p
        }
        scope.search.desc = scope.reverse;
        scope.search.order_by = scope.ord;
        var params = {
            view: scope.activeView,
            offset: scope.search.offset,
            desc: scope.reverse,
            order_by: scope.ord
        };
        for (x in scope.search) {
            params[x] = scope.search[x]
        }
        search(params, callback, scope)
    }

    function selectItems(scope, url, item) {
        var items = [];
        if (item == undefined) {
            var li = scope.item.deals_list.list;
            for (y in li) {
                for (x in li[y].data) {
                    items.push(li[y].data[x].id);
                    li[y].data[x].check_add_to_product = scope.listObj.check_add_all
                }
            }
        }
        var params = {
            'do': url,
            'value': item ? item.check_add_to_product : scope.listObj.check_add_all,
            'item': item ? item.id : items,
            'all': item ? undefined : !0
        };
        helper.doRequest('post', 'index.php', params)
    }

    function saveForPDF(scope, url, item) {
        var items = [];
        if (item == undefined) {
            for (x in scope.list) {
                items.push(scope.list[x].id);
                scope.list[x].check_add_to_product = scope.listObj.check_add_all
            }
        }
        var params = {
            'do': url,
            'value': item ? item.check_add_to_product : scope.listObj.check_add_all,
            'item': item ? item.id : items,
            'all': item ? undefined : !0
        };
        helper.doRequest('post', 'index.php', params)
    }

    function exportFnc(scope, e) {
        if (scope.listObj.check_add_all) {
            return !0
        }
        for (x in scope.list) {
            if (scope.list[x].check_add_to_product == !0) {
                return !0;
                break
            }
        }
        e.preventDefault()
    }

    function markCheckNew(scope, url, item) {
        var items = [];
        if (item == undefined) {
            for (x in scope.list) {
                items.push(scope.list[x].id);
                scope.list[x].check_add_to_product = scope.listObj.check_add_all
            }
        }
        var params = {
            'do': url,
            'value': item ? item.check_add_to_product : scope.listObj.check_add_all,
            'item': item ? item.id : items,
            'all': item ? undefined : !0
        };
        helper.doRequest('post', 'index.php', params, function(r) {
            scope.all_pages_selected = r.data.all_pages_selected;
            scope.minimum_selected = r.data.minimum_selected;
        });
    }

    function markCheckNewAll(scope, url, value) {
        for (x in scope.list) {
            scope.list[x].check_add_to_product = (value == '1' ? true : false);
        }
        var params = {
            'do': url,
            'value': value
        };
        helper.doRequest('post', 'index.php', params, function(r) {
            scope.all_pages_selected = r.data.all_pages_selected;
            scope.minimum_selected = r.data.minimum_selected;
        });
    }

}]).factory('editItem', ['$rootScope', '$timeout', '$sce', '$state', 'helper', 'modalFc', '$confirm', 'selectizeFc', function($rootScope, $timeout, $sce, $state, helper, modalFc, $confirm, selectizeFc) {
    var item = {
        init: init,
        addArticle: addArticle,
        addPurchaseOrderArticle: addPurchaseOrderArticle,
        addQuoteArticle: addQuoteArticle,
        calcTotal: calcTotal,
        calcTotalPurchase: calcTotalPurchase,
        invoiceQuantityUpdate: invoiceQuantityUpdate,
        calcInvoiceTotal: calcInvoiceTotal,
        addInvoiceArticle: addInvoiceArticle,
        addInvoiceArticleSpecial: addInvoiceArticleSpecial,
        addInvoiceLine: addInvoiceLine,
        addArticleComposedLine: addArticleComposedLine,
        addFacqLine: addFacqLine,
        addBarcodeLine: addBarcodeLine,
        getScope: getScope,
        openModal: openModal,
        openModalRenamed: openModal,
        scope: {}
    };
    return item;

    function init(scope, $objProps) {
        switch (scope.app) {
            case 'quote':
            case 'order':
                var addr_type = helper.getLanguage('Create a shipping address');
                break;
            case 'invoice':
            case 'contract':
                var addr_type = helper.getLanguage('Create a billing address');
                break;
            case 'project':
            case 'maintenance':
            case 'customers':
                var addr_type = helper.getLanguage('Create a primary address');
                break;
            case 'installation':
                var addr_type = helper.getLanguage('Create a site address');
                break;
            case 'po_order':
                var addr_type = helper.getLanguage('Create a shipping address');
                break;
            default:
                var addr_type = helper.getLanguage('Create a shipping address');
        }
        this.scope = scope;
        var typePromise;
        scope.typePromise = undefined;
        scope.requestAuto = requestAuto;
        scope.renderContactAuto = renderContactAuto;
        if ($objProps && $objProps.renameFunction && $objProps.renameFunction == 'openModal') {
            scope.openModalRenamed = openModal;
        } else {
            scope.openModal = openModal;
        }
        scope.addChapterFromTemplate = addChapterFromTemplate;
        scope.addFacqLine = addFacqLine;
        scope.addBarcodeLine = addBarcodeLine;
        scope.removeContact = removeContact;
        scope.removeDeliveryAddr = removeDeliveryAddr;
        scope.removeMainDeliveryAddr = removeMainDeliveryAddr;
        scope.removeAccount = removeAccount;
        scope.renderAddressAuto = renderAddressAuto;
        scope.renderMainAddressAuto = renderMainAddressAuto;
        scope.renderCCAuto = renderCCAuto;
        scope.renderToCCAuto = renderToCCAuto;
        scope.renderSCAuto = renderSCAuto;
        scope.saveClientData = saveClientData;
        scope.removeCust = removeCust;
        scope.renderArticleListAuto = renderArticleListAuto;
        scope.init = initP;
        scope.postC = postC;
        scope.showEditFnc = showEditFnc;
        scope.showCCSelectize = showCCSelectize;
        scope.cancelEdit = cancelEdit;
        scope.addQuoteContent = addQuoteContent;
        scope.addContractContent = addContractContent;
        scope.addr_type = addr_type;
        scope.app = scope.app;
        scope.calcQuoteTotal = calcQuoteTotal;
        scope.to_trusted = to_trusted;
        scope.addQuoteGroup = addQuoteGroup;
        scope.uploadDrop = uploadDrop;
        scope.showCustomerNotes = showCustomerNotes;
        scope.showPurchasePriceModal = showPurchasePriceModal;
        scope.show = !0;
        scope.autorAutoCfg = selectizeFc.configure({
            placeholder: 'Select author',
            onItemAdd(value, $item) {
                scope.item.acc_manager = $item.html()
            },
            onItemRemove(value) {
                scope.item.acc_manager = ''
            }
        });
        scope.accmanagerAutoCfg = selectizeFc.configure({
            placeholder: helper.getLanguage('Select account manager'),
            onItemAdd(value, $item) {
                if (value) {
                    scope.item.acc_manager_name = $item.html()
                }
            },
            onItemRemove(value) {
                scope.item.acc_manager_name = ''
            }
        });
        scope.contactAutoCfg = selectizeFc.configure({
            placeholder: helper.getLanguage('Select contact'),
            maxOptions: 10,
            onType(str) {
                var selectize = angular.element('#contact_id')[0].selectize;
                selectize.close();
                if (typePromise) {
                    $timeout.cancel(typePromise)
                }
                typePromise = $timeout(function() {
                    var data = {
                        'do': scope.app + '-' + scope.ctrlpag,
                        xget: 'contacts',
                        term: str,
                        buyer_id: scope.item.buyer_id
                    };
                    scope.requestAuto(scope, data, scope.renderContactAuto)
                }, 300)
            },
            onChange(value) {
                if (value == undefined) {
                    scope.removeContact(scope);
                    var selectize = angular.element('#contact_id')[0].selectize;
                    selectize.close();
                    if (typePromise) {
                        $timeout.cancel(typePromise)
                    }
                    typePromise = $timeout(function() {
                        var data = {
                            'do': scope.app + '-' + scope.ctrlpag,
                            xget: 'contacts',
                            buyer_id: scope.item.buyer_id
                        };
                        scope.requestAuto(scope, data, scope.renderContactAuto)
                    })
                } else if (value == '99999999999') {
                    var tmp_contact_obj = angular.copy(scope.contactAddObj);
                    if ((scope.app == 'invoice' && !scope.app_mod) || (scope.app == 'invoice' && scope.app_mod == 'recinvoice') || (scope.app == 'quote' && !scope.app_mod) || scope.app == 'order' || scope.app == 'po_order') {
                        if (scope.app == 'invoice') {
                            tmp_contact_obj.invoice_line = scope.item.invoice_line;
                        }
                        if (scope.app == 'quote') {
                            tmp_contact_obj.quote_group = scope.item.quote_group;
                        }
                        if (scope.app == 'order' || scope.app == 'po_order') {
                            tmp_contact_obj.tr_id = scope.item.tr_id;
                        }
                    }
                    scope.openModal(scope, 'AddCustomer', tmp_contact_obj, 'md');
                    return !1
                }else if(value){
                    if(scope.app == 'order' || (scope.app == 'quote' && !scope.app_mod) || scope.app == 'po_order' || (scope.app == 'invoice' && !scope.app_mod)){             
                        scope.contactUpdated(this.options[value].email_language);
                    }
                }
            },
            onItemAdd(value, $item) {
                if (value == '99999999999') {
                    scope.removeContact(scope)
                }
            }
        }, helper.getLanguage('Create a new Contact'));
        scope.SecondaddressAutoCfg = selectizeFc.configure({
            valueField: 'address_id',
            labelField: 'address',
            searchField: ['address','city'],
            delimiter: '|',
            placeholder: helper.getLanguage('Select or Create a delivery address'),
            create: !1,
            maxOptions: 10,
            onDropdownOpen($dropdown) {
                var id = angular.element($dropdown).parent().parent().find('selectize')[0].id;
                if (scope.app == 'maintenance') {
                    $timeout(function() {
                        if (id == 'main_address') {
                            angular.element($dropdown).find('span.sel_text').text(helper.getLanguage('Create a primary address'));
                            scope.app_serv = 'primary'
                        } else {
                            angular.element($dropdown).find('span.sel_text').text(helper.getLanguage('Create a site address'));
                            scope.app_serv = 'site'
                        }
                    })
                } else if (scope.app == 'customers') {
                    $timeout(function() {
                        if (id == 'main_address') {
                            angular.element($dropdown).find('span.sel_text').text(helper.getLanguage('Create a primary address'));
                            scope.app_serv = 'primary'
                        } else {
                            angular.element($dropdown).find('span.sel_text').text(helper.getLanguage('Create a delivery address'));
                            scope.app_serv = 'delivery'
                        }
                    })
                }
            },
            onChange(value) {
                if (scope.app == 'invoice') {
                    this.$input.parent().find('.selectize-input input').prop('placeholder', (value != undefined && value != '99999999999' ? '' : helper.getLanguage('Select or Create an invoicing address')));
                    //this.$input.parent().find('.selectize-input input').css('width', '100%')
                } else {
                    this.$input.parent().find('.selectize-input input').prop('placeholder', (value != undefined && value != '99999999999' ? '' : helper.getLanguage('Select or Create a delivery address')));
                    //this.$input.parent().find('.selectize-input input').css('width', '100%')
                }
                if (value == undefined) {} else if (value == '99999999999') {
                    var main_obj = {
                        country_dd: scope.item.country_dd,
                        country_id: scope.item.main_country_id,
                        customer_id: scope.item.buyer_id,
                        'do': scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-tryAddAddress',
                        item_id: scope.item_id,
                        article: scope.item.tr_id,
                        identity_id: scope.item.identity_id,
                        field: scope.item.field,
                        contact_id: scope.item.contact_id,
                    };
                    if (scope.app == 'po_order') {
                        scope.item.second_address_id = undefined;
                        main_obj.customer_id = scope.item.sc_id;
                        main_obj.buyer_id = scope.item.buyer_id;
                        main_obj.client_delivery = scope.item.client_delivery;
                    }
                    scope.openModal(scope, 'AddAddress', main_obj, 'md');
                    return !1
                } else {
                    if (scope.app == 'invoice' || scope.app == 'project' || scope.app == 'maintenance' || scope.app == 'contract' || scope.app == 'customers') {
                        for (x in scope.item.addresses) {
                            if (scope.item.addresses[x].address_id == value) {
                                scope.item.free_field = scope.item.addresses[x].address + "\n" + scope.item.addresses[x].zip + ' ' + scope.item.addresses[x].city + "\n" + scope.item.addresses[x].country
                            }
                        }
                    }
                    if (scope.app == 'order') {
                        for (x in scope.item.addresses) {
                            if (scope.item.addresses[x].address_id == value) {
                                scope.item.address_info = scope.item.addresses[x].address + "\n" + scope.item.addresses[x].zip + ' ' + scope.item.addresses[x].city + "\n" + scope.item.addresses[x].country
                            }
                        }
                    }
                }
            },
            onItemAdd(value, $item) {
                var id = this.$input[0].id;
                if (id != 'main_address') {
                    if (value == '99999999999') {
                        scope.item.delivery_address_id = '';
                        scope.item.delivery_address = ''
                    } else if (value) {
                        for (x in scope.item.addresses) {
                            if (scope.item.addresses[x].address_id == value) {
                                scope.item.delivery_address = scope.item.addresses[x].address + "\n" + scope.item.addresses[x].zip + ' ' + scope.item.addresses[x].city + "\n" + scope.item.addresses[x].country;
                                scope.item.sameAddress = !1
                            }
                        }
                    } else {
                        scope.item.delivery_address = '';
                        scope.item.sameAddress = !0
                    }
                } else {
                    if (value == '99999999999') {
                        scope.item.main_address_id = ''
                    }
                }
            },
            onType(str) {
                var id = this.$input[0].id;
                if (id == 'main_address') {
                    var selectize = angular.element('#main_address')[0].selectize
                } else {
                    var selectize = angular.element('#delivery_address')[0].selectize
                }
                selectize.close();
                if (typePromise) {
                    $timeout.cancel(typePromise)
                }
                typePromise = $timeout(function() {
                    if (id == 'main_address') {
                        var data = {
                            'do': scope.app + '-' + scope.ctrlpag,
                            'xget': 'addresses',
                            term: str,
                            buyer_id: scope.item.sc_id,
                            contact_id: scope.item.contact_id
                        };
                        scope.requestAuto(scope, data, scope.renderMainAddressAuto)
                    } else {
                        var data = {
                            'do': scope.app + '-' + scope.ctrlpag,
                            'xget': 'addresses',
                            term: str,
                            buyer_id: scope.item.sc_id,
                            contact_id: scope.item.contact_id,
                            site_add: !0,
                            field: 'customer_id'
                        };
                        if (scope.app == 'po_order') {
                            data.customer_id = scope.item.sc_id;
                        }
                        scope.requestAuto(scope, data, scope.renderAddressAuto)
                    }
                }, 300)
            },
            maxItems: 1
        }, addr_type);
        scope.addressAutoCfg = selectizeFc.configure({
            valueField: 'address_id',
            labelField: 'address',
            searchField: ['address','city'],
            delimiter: '|',
            create: !1,
            maxOptions: 10,
            onInitialize: function(selectize) {
                var id = angular.element(selectize.$dropdown).parent().parent().find('selectize')[0].id;
                if (scope.app == 'invoice') {
                    // selectize.$input.parent().find('.selectize-input input').prop('placeholder', helper.getLanguage('Select or Create an invoicing address'));
                    // selectize.$input.parent().find('.selectize-input input').css('width', '100%')
                    selectize.settings.placeholder = helper.getLanguage('Select or Create an invoicing address');
                    selectize.updatePlaceholder();
                } else if (scope.app == 'quote') {
                    // selectize.$input.parent().find('.selectize-input input').prop('placeholder', helper.getLanguage('Select or Create an primary address'));
                    // selectize.$input.parent().find('.selectize-input input').css('width', '100%')
                    selectize.settings.placeholder = helper.getLanguage('Select or Create an primary address');
                    selectize.updatePlaceholder();
                } else if (scope.app == 'installation') {
                    // selectize.$input.parent().find('.selectize-input input').prop('placeholder', helper.getLanguage('Select or Create a site address'));
                    // selectize.$input.parent().find('.selectize-input input').css('width', '100%')
                    selectize.settings.placeholder = helper.getLanguage('Select or Create a site address');
                    selectize.updatePlaceholder();
                } else if (scope.app == 'maintenance') {
                    if (id == 'main_address') {
                        // selectize.$input.parent().find('.selectize-input input').prop('placeholder', helper.getLanguage('Select or Create an primary address'));
                        // selectize.$input.parent().find('.selectize-input input').css('width', '100%')
                        selectize.settings.placeholder = helper.getLanguage('Select or Create an primary address');
                        selectize.updatePlaceholder();
                    } else {
                        // selectize.$input.parent().find('.selectize-input input').prop('placeholder', helper.getLanguage('Select or Create a site address'));
                        // selectize.$input.parent().find('.selectize-input input').css('width', '100%')
                        selectize.settings.placeholder = helper.getLanguage('Select or Create a site address');
                        selectize.updatePlaceholder();
                    }
                } else {
                    // selectize.$input.parent().find('.selectize-input input').prop('placeholder', helper.getLanguage('Select or Create a delivery address'));
                    // selectize.$input.parent().find('.selectize-input input').css('width', '100%')
                    selectize.settings.placeholder = helper.getLanguage('Select or Create a delivery address');
                    selectize.updatePlaceholder();
                }
            },
            onDropdownOpen($dropdown) {
                var id = angular.element($dropdown).parent().parent().find('selectize')[0].id;
                if (scope.app == 'maintenance') {
                    $timeout(function() {
                        if (id == 'main_address') {
                            angular.element($dropdown).find('span.sel_text').text(helper.getLanguage('Create a primary address'));
                            scope.app_serv = 'primary';
                            scope.addr_type = helper.getLanguage('Create a primary address')
                        } else {
                            angular.element($dropdown).find('span.sel_text').text(helper.getLanguage('Create a site address'));
                            scope.app_serv = 'site';
                            scope.addr_type = helper.getLanguage('Create a site address')
                        }
                    })
                } else if (scope.app == 'quote') {
                    $timeout(function() {
                        if (id == 'main_address') {
                            angular.element($dropdown).find('span.sel_text').text(helper.getLanguage('Create a primary address'));
                            scope.app_serv = 'primary'
                        } else {
                            angular.element($dropdown).find('span.sel_text').text(helper.getLanguage('Create a delivery address'));
                            scope.app_serv = 'delivery'
                        }
                    })
                } else if (scope.app == 'customers') {
                    $timeout(function() {
                        if (id == 'main_address') {
                            angular.element($dropdown).find('span.sel_text').text(helper.getLanguage('Create a primary address'));
                            scope.app_serv = 'primary'
                        } else {
                            angular.element($dropdown).find('span.sel_text').text(helper.getLanguage('Create a delivery address'));
                            scope.app_serv = 'delivery'
                        }
                    })
                }
            },
            onChange(value) {
                if (scope.app == 'invoice') {
                    this.$input.parent().find('.selectize-input input').prop('placeholder', (value != undefined && value != '99999999999' ? '' : helper.getLanguage('Select or Create an invoicing address')));
                    // this.$input.parent().find('.selectize-input input').css('width', '100%')
                } else if (scope.app == 'installation') {
                    this.$input.parent().find('.selectize-input input').prop('placeholder', (value != undefined && value != '99999999999' ? '' : helper.getLanguage('Select or Create a site address')));
                    // this.$input.parent().find('.selectize-input input').css('width', '100%')
                } else if (scope.app == 'maintenance') {
                    if (scope.app_serv == 'primary') {
                        this.$input.parent().find('.selectize-input input').prop('placeholder', (value != undefined && value != '99999999999' ? '' : helper.getLanguage('Select or Create an primary address')));
                        // this.$input.parent().find('.selectize-input input').css('width', '100%')
                    } else {
                        this.$input.parent().find('.selectize-input input').prop('placeholder', (value != undefined && value != '99999999999' ? '' : helper.getLanguage('Select or Create a site address')));
                        // this.$input.parent().find('.selectize-input input').css('width', '100%')
                    }
                } else if (scope.app == 'quote') {
                    if (scope.app_serv == 'primary') {
                        this.$input.parent().find('.selectize-input input').prop('placeholder', (value != undefined && value != '99999999999' ? '' : helper.getLanguage('Select or Create an primary address')));
                        // this.$input.parent().find('.selectize-input input').css('width', '100%')
                    } else {
                        this.$input.parent().find('.selectize-input input').prop('placeholder', (value != undefined && value != '99999999999' ? '' : helper.getLanguage('Select or Create a delivery address')));
                        // this.$input.parent().find('.selectize-input input').css('width', '100%')
                    }
                } else {
                    this.$input.parent().find('.selectize-input input').prop('placeholder', (value != undefined && value != '99999999999' ? '' : helper.getLanguage('Select or Create a delivery address')));
                    // this.$input.parent().find('.selectize-input input').css('width', '100%')
                }
                if (value == undefined) {} else if (value == '99999999999') {
                    var main_obj = {
                        country_dd: scope.item.country_dd,
                        country_id: scope.item.main_country_id,
                        customer_id: scope.item.buyer_id,
                        'do': scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-tryAddAddress',
                        item_id: scope.item_id,
                        article: scope.item.tr_id,
                        identity_id: scope.item.identity_id,
                        field: scope.item.field,
                        contact_id: scope.item.contact_id,
                        app_mod: scope.app_mod,
                        main_address_id: scope.item.main_address_id,
                    };
                    if (scope.app == 'project') {
                        main_obj.name = scope.item.name;
                        main_obj.project_type = scope.item.project_type;
                        main_obj.budget_type = scope.item.budget_type;
                        main_obj.project_rate = scope.item.project_rate;
                        main_obj.billable_type = scope.item.billable_type;
                        main_obj.start_date = scope.item.start_date ? new Date(scope.item.start_date) : '';
                        main_obj.end_date = scope.item.end_date ? new Date(scope.item.end_date) : '';
                        main_obj.t_pr_hour = scope.item.t_pr_hour;
                        main_obj.send_email_alerts = scope.item.send_email_alerts;
                        main_obj.alert_percent = scope.item.alert_percent
                    }
                    if (scope.app == 'maintenance') {
                        main_obj.service_type = scope.item.service_type;
                        main_obj.billable = scope.item.billable;
                        if (scope.item.is_recurring && !scope.item.contract_id) {
                            main_obj.is_recurring = scope.item.is_recurring;
                            main_obj.rec_start_date = scope.item.rec_start_date;
                            main_obj.rec_frequency = scope.item.rec_frequency;
                            main_obj.rec_end_date = scope.item.rec_end_date;
                            main_obj.rec_days = scope.item.rec_days;
                            main_obj.rec_number = scope.item.rec_number;
                            main_obj.recurring_no_end = scope.item.recurring_no_end
                        }
                    }
                    if (scope.app == 'customers' && scope.app_mod && scope.app_mod == 'deal') {
                        main_obj.board_id = scope.item.board_id;
                        main_obj.stage_id = scope.item.stage_id;
                    }
                    scope.openModal(scope, 'AddAddress', main_obj, 'md');
                    return !1
                } else {
                    if (scope.app == 'invoice' || scope.app == 'project' || scope.app == 'maintenance' || scope.app == 'contract' || scope.app == 'customers') {
                        for (x in scope.item.addresses) {
                            if (scope.item.addresses[x].address_id == value) {
                                scope.item.free_field = scope.item.addresses[x].address + "\n" + scope.item.addresses[x].zip + ' ' + scope.item.addresses[x].city + "\n" + scope.item.addresses[x].country
                            }
                        }
                    }
                    if (scope.app == 'order') {
                        for (x in scope.item.addresses) {
                            if (scope.item.addresses[x].address_id == value) {
                                scope.item.address_info = scope.item.addresses[x].address + "\n" + scope.item.addresses[x].zip + ' ' + scope.item.addresses[x].city + "\n" + scope.item.addresses[x].country
                            }
                        }
                    }
                }
            },
            onItemAdd(value, $item) {
                var id = this.$input[0].id;
                if (id != 'main_address') {
                    if (value == '99999999999') {
                        scope.item.delivery_address_id = '';
                        scope.item.delivery_address = ''
                    } else if (value) {
                        for (x in scope.item.addresses) {
                            if (scope.item.addresses[x].address_id == value) {
                                scope.item.delivery_address = scope.item.addresses[x].address + "\n" + scope.item.addresses[x].zip + ' ' + scope.item.addresses[x].city + "\n" + scope.item.addresses[x].country;
                                scope.item.sameAddress = !1
                            }
                        }
                    } else {
                        scope.item.delivery_address = '';
                        scope.item.sameAddress = !0
                    }
                } else {
                    if (value == '99999999999') {
                        scope.item.main_address_id = '';
                        scope.item.sameAddress = !1
                    }
                }
            },
            onType(str) {
                var id = this.$input[0].id;
                if (id == 'main_address') {
                    var selectize = angular.element('#main_address')[0].selectize
                } else {
                    var selectize = angular.element('#delivery_address')[0].selectize
                }
                selectize.close();
                if (typePromise) {
                    $timeout.cancel(typePromise)
                }
                typePromise = $timeout(function() {
                    if (id == 'main_address') {
                        var data = {
                            'do': scope.app + '-' + scope.ctrlpag,
                            'xget': 'addresses',
                            term: str,
                            buyer_id: scope.item.buyer_id,
                            contact_id: scope.item.contact_id
                        };
                        scope.requestAuto(scope, data, scope.renderMainAddressAuto)
                    } else {
                        var data = {
                            'do': scope.app + '-' + scope.ctrlpag,
                            'xget': 'addresses',
                            term: str,
                            buyer_id: scope.item.buyer_id,
                            contact_id: scope.item.contact_id,
                            site_add: !0
                        };
                        scope.requestAuto(scope, data, scope.renderAddressAuto)
                    }
                }, 300)
            },
            maxItems: 1
        }, addr_type);
        scope.secondAutoCfg = selectizeFc.configure({
            placeholder: helper.getLanguage('Select customer'),
            maxOptions: 10,
            onType(str) {
                var selectize = angular.element('#sc_id')[0].selectize;
                selectize.close();
                if (typePromise) {
                    $timeout.cancel(typePromise)
                }
                typePromise = $timeout(function() {
                    var data = {
                        'do': scope.app + '-' + scope.ctrlpag,
                        xget: 'sc',
                        term: str
                    };
                    helper.doRequest('get', 'index.php', data, function(o) {
                        scope.requestAuto(scope, data, scope.renderSCAuto)
                    })
                }, 300)
            },
            onChange(value) {
                if (value == undefined) {} else if (value == '99999999999') {
                    scope.openModal(scope, 'AddCustomer', {
                        buyer_id: scope.item.buyer_id,
                        client_delivery: scope.item.client_delivery,
                        country_dd: scope.item.country_dd,
                        country_id: scope.item.main_country_id,
                        add_customer: !0,
                        article: scope.item.tr_id,
                        'do': scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-tryAddSC',
                        identity_id: scope.item.identity_id,
                        item_id: scope.item_id
                    }, 'md');
                    var data = {
                        'do': scope.app + '-' + scope.ctrlpag,
                        xget: 'addresses',
                        'field': 'customer_id',
                        'customer_id': scope.item.sc_id
                    };
                    helper.doRequest('get', 'index.php', data, function(o) {
                        scope.auto.options.second_addresses = o.data
                    })
                } else if (value != undefined) {
                    var selectize = angular.element('#sc_id')[0].selectize;
                    selectize.close();
                    if (typePromise) {
                        $timeout.cancel(typePromise)
                    }
                    typePromise = $timeout(function() {
                        var data = {
                            'do': scope.app + '-' + scope.ctrlpag,
                            xget: 'addresses',
                            'field': 'customer_id',
                            'customer_id': scope.item.sc_id
                        };
                        helper.doRequest('get', 'index.php', data, function(o) {
                            scope.auto.options.second_addresses = o.data
                        })
                    }, 300)
                }
            },
            onItemAdd(value, $item) {
                if (value == '99999999999') {
                    scope.item.sc_id = ''
                }
            },
            maxItems: 1
        }, helper.getLanguage('Create a new Customer'));
        scope.ccAutoCfg = selectizeFc.configure({
            placeholder: helper.getLanguage('Select customer'),
            maxItems: 51,
            onDropdownOpen($dropdown) {
                var _this = this;
                scope.old_buyer_id = _this.$input[0].attributes.value.value;
            },
            onType(str) {
                var selectize = angular.element('#custandcont')[0].selectize;
                selectize.close();
                if (typePromise) {
                    $timeout.cancel(typePromise)
                }
                typePromise = $timeout(function() {
                    var data = {
                        'do': scope.app + '-' + scope.ctrlpag,
                        'xget': 'cc',
                        term: str,
                        'internal': scope.item.internal_value
                    };
                    scope.requestAuto(scope, data, scope.renderCCAuto)
                }, 300)
            },
            onChange(value) {
                if (value == undefined) {
                    if ((scope.app == 'invoice' && !scope.app_mod) || (scope.app == 'invoice' && scope.app_mod == 'recinvoice') || (scope.app == 'invoice' && scope.app_mod == 'sepa') || (scope.app == 'quote' && !scope.app_mod) || scope.app == 'order' || scope.app == 'customers' || scope.app == 'maintenance' || (scope.app == 'contract' && !scope.app_mod) || scope.app == 'installation') {
                        scope.auto.options.contacts = [];
                        scope.item.contact_id = undefined;
                        if (scope.app == 'quote') {
                            scope.auto.options.secondaryAddresses = [];
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'order') {
                            scope.auto.options.site_addresses = [];
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'customers') {
                            scope.auto.options.addresses = [];
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'maintenance') {
                            scope.auto.options.addresses = [];
                            scope.item.main_address_id = undefined;
                            scope.auto.options.site_addresses = [];
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'invoice' && !scope.app_mod) {
                            scope.item.has_third_party = '';
                            scope.item.third_party_id = '';
                            scope.item.third_party_applied = '';
                            scope.item.third_party_regime_id = '';
                        }
                        if (scope.app == 'project') {
                            scope.auto.options.addresses = [];
                            scope.item.main_address_id = undefined;
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'contract') {
                            scope.auto.options.addresses = [];
                            scope.item.main_address_id = undefined;
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'installation') {
                            scope.auto.options.addresses = [];
                            scope.item.delivery_address_id = undefined;
                        }
                        var data = {
                            'do': scope.app + '-' + scope.ctrlpag,
                            'xget': 'cc'
                        };
                        scope.requestAuto(scope, data, scope.renderCCAuto)
                    }
                } else if (value == '99999999999') {
                    var main_obj = {
                        country_dd: scope.item.country_dd,
                        country_id: scope.item.main_country_id,
                        add_customer: !0,
                        article: scope.item.tr_id,
                        'do': scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-tryAddC',
                        identity_id: scope.item.identity_id,
                        item_id: scope.item_id,
                        board_id: scope.item.board_id,
                        stage_id: scope.item.stage_id,
                        user_id: scope.item.user_id,
                        app_mod: scope.app_mod
                    };
                    if ((scope.app == 'invoice' && !scope.app_mod) || (scope.app == 'invoice' && scope.app_mod == 'recinvoice') || (scope.app == 'invoice' && scope.app_mod == 'sepa') || (scope.app == 'quote' && !scope.app_mod) || scope.app == 'order' || scope.app == 'customers' || scope.app == 'maintenance' || scope.app == 'project' || (scope.app == 'contract' && !scope.app_mod) || scope.app == 'installation') {
                        scope.auto.options.contacts = [];
                        scope.item.contact_id = undefined;
                        scope.item.buyer_id = undefined;
                        if (scope.app == 'invoice') {
                            main_obj.invoice_line = scope.item.invoice_line;
                            if (scope.item.base_type) {
                                main_obj.base_type = scope.item.base_type;
                            }
                            if (scope.item.orders_id) {
                                main_obj.orders_id = scope.item.orders_id;
                            }
                        }
                        if (scope.app == 'quote') {
                            scope.auto.options.secondaryAddresses = [];
                            scope.item.delivery_address_id = undefined;
                            main_obj.quote_group = scope.item.quote_group;
                        }
                        if (scope.app == 'order') {
                            scope.auto.options.site_addresses = [];
                            scope.item.delivery_address_id = undefined;
                            main_obj.tr_id = scope.item.tr_id;
                        }
                        if (scope.app == 'customers') {
                            scope.auto.options.addresses = [];
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'maintenance') {
                            scope.auto.options.addresses = [];
                            scope.item.main_address_id = undefined;
                            scope.auto.options.site_addresses = [];
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'project') {
                            scope.auto.options.addresses = [];
                            scope.item.main_address_id = undefined;
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'contract') {
                            scope.auto.options.addresses = [];
                            scope.item.main_address_id = undefined;
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'installation') {
                            scope.auto.options.addresses = [];
                            scope.item.delivery_address_id = undefined;
                        }
                    }
                    if (scope.app == 'project') {
                        main_obj.project_name = scope.item.name;
                        main_obj.project_type = scope.item.project_type;
                        main_obj.budget_type = scope.item.budget_type;
                        main_obj.project_rate = scope.item.project_rate;
                        main_obj.billable_type = scope.item.billable_type;
                        main_obj.start_date = scope.item.start_date ? new Date(scope.item.start_date) : '';
                        main_obj.end_date = scope.item.end_date ? new Date(scope.item.end_date) : '';
                        main_obj.t_pr_hour = scope.item.t_pr_hour;
                        main_obj.send_email_alerts = scope.item.send_email_alerts;
                        main_obj.alert_percent = scope.item.alert_percent
                    }
                    if (scope.app == 'maintenance') {
                        main_obj.service_type = scope.item.service_type;
                        main_obj.billable = scope.item.billable;
                        if (scope.item.is_recurring && !scope.item.contract_id) {
                            main_obj.is_recurring = scope.item.is_recurring;
                            main_obj.rec_start_date = scope.item.rec_start_date;
                            main_obj.rec_frequency = scope.item.rec_frequency;
                            main_obj.rec_end_date = scope.item.rec_end_date;
                            main_obj.rec_days = scope.item.rec_days;
                            main_obj.rec_number = scope.item.rec_number;
                            main_obj.recurring_no_end = scope.item.recurring_no_end
                        }
                    }
                    scope.openModal(scope, 'AddCustomer', main_obj, 'md');
                    return !1
                } else if (value) {
                    var selectize = angular.element('#custandcont')[0].selectize;
                    if (selectize) {
                        selectize.blur();
                    }
                    var val = explode('-', value);
                    scope.item.buyer_id = val[0];
                    if ((scope.app == 'invoice' && !scope.app_mod) || (scope.app == 'invoice' && scope.app_mod == 'recinvoice') || (scope.app == 'invoice' && scope.app_mod == 'sepa') || (scope.app == 'quote' && !scope.app_mod) || scope.app == 'order' || scope.app == 'customers' || scope.app == 'maintenance' || scope.app == 'project' || (scope.app == 'contract' && !scope.app_mod) || scope.app == 'installation') {
                        scope.auto.options.contacts = [];
                        scope.item.contact_id = undefined;
                        if (scope.app == 'quote') {
                            scope.auto.options.secondaryAddresses = [];
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'order') {
                            scope.auto.options.site_addresses = [];
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'customers') {
                            scope.auto.options.addresses = [];
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'maintenance') {
                            scope.auto.options.addresses = [];
                            scope.item.main_address_id = undefined;
                            scope.auto.options.site_addresses = [];
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'project') {
                            scope.auto.options.addresses = [];
                            scope.item.main_address_id = undefined;
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'contract') {
                            scope.auto.options.addresses = [];
                            scope.item.main_address_id = undefined;
                            scope.item.delivery_address_id = undefined;
                        }
                        if (scope.app == 'installation') {
                            scope.auto.options.addresses = [];
                            scope.item.delivery_address_id = undefined;
                        }
                    } else {
                        scope.item.contact_id = val[1];
                    }
                    if (scope.app == 'project') {
                        scope.item.our_reference = this.options[value].ref
                    }
                    scope.saveClientData(scope)
                }
            },
            onItemAdd(value, $item) {
                if (value == '99999999999') {
                    scope.item.cc_id = ''
                }
            },
            onBlur() {
                if ((scope.app != 'invoice' && scope.app != 'quote' && scope.app != 'order' && scope.app != 'customers' && scope.app != 'maintenance' && scope.app != 'project' && scope.app != 'contract' && scope.app != 'installation') || (scope.app == 'invoice' && scope.app_mod && scope.app_mod != 'recinvoice' && scope.app_mod != 'sepa')) {
                    $timeout(function() {
                        scope.removeCust(scope)
                    })
                }
            },
            maxItems: 1
        }, helper.getLanguage('Create a new Customer'));
        scope.fromccAutoCfg = selectizeFc.configure({
            placeholder: helper.getLanguage('Select From'),
            maxItems: 51,
            onType(str) {
                var selectize = angular.element('#custandcont')[0].selectize;
                selectize.close();
                if (typePromise) {
                    $timeout.cancel(typePromise)
                }
                typePromise = $timeout(function() {
                    var data = {
                        'do': scope.app + '-' + scope.ctrlpag,
                        'xget': 'cc',
                        term: str,
                        'internal': scope.item.internal_value
                    };
                    scope.requestAuto(scope, data, scope.renderCCAuto)
                }, 300)
            },
            onChange(value) {
                if (value == undefined) {
                    scope.auto.options.addresses = [];
                    scope.item.main_address_id = undefined;
                    var data = {
                        'do': scope.app + '-' + scope.ctrlpag,
                        'xget': 'cc'
                    };
                    scope.requestAuto(scope, data, scope.renderCCAuto);
                } else if (value == '99999999999') {
                    scope.auto.options.addresses = [];
                    scope.item.main_address_id = undefined;
                    scope.openModal(scope, 'AddCustomer', {
                        country_dd: scope.item.country_dd,
                        country_id: scope.item.main_country_id,
                        add_customer: !0,
                        article: scope.item.tr_id,
                        'do': scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-tryAddC',
                        item_id: scope.item_id
                    }, 'md');
                    return !1
                } else if (value) {
                    var val = explode('-', value);
                    scope.item.buyer_id = val[0];
                    //scope.item.contact_id = val[1];
                    scope.auto.options.addresses = [];
                    scope.item.main_address_id = undefined;
                    scope.saveClientData(scope)
                }
            },
            onItemAdd(value, $item) {
                if (value == '99999999999') {
                    scope.item.cc_id = ''
                }
            },
            onBlur() {
                /*$timeout(function() {
                    scope.removeCust(scope)
                })*/
            },
            maxItems: 1
        }, helper.getLanguage('Create a new Customer'));
        scope.toccAutoCfg = selectizeFc.configure({
            placeholder: helper.getLanguage('Select To'),
            maxItems: 51,
            onType(str) {
                var selectize = angular.element('#tocustandcont')[0].selectize;
                selectize.close();
                if (typePromise) {
                    $timeout.cancel(typePromise)
                }
                typePromise = $timeout(function() {
                    var data = {
                        'do': scope.app + '-' + scope.ctrlpag,
                        'xget': 'to_cc',
                        term: str,
                        'internal': scope.item.internal_value
                    };
                    scope.requestAuto(scope, data, scope.renderToCCAuto)
                }, 300)
            },
            onChange(value) {
                if (value == undefined) {
                    scope.auto.options.to_addresses = [];
                    scope.item.to_main_address_id = undefined;
                    var data = {
                        'do': scope.app + '-' + scope.ctrlpag,
                        'xget': 'to_cc'
                    };
                    scope.requestAuto(scope, data, scope.renderToCCAuto)
                } else if (value == '99999999999') {
                    scope.auto.options.to_addresses = [];
                    scope.item.to_main_address_id = undefined;
                    scope.openModal(scope, 'AddCustomer', {
                        country_dd: scope.item.country_dd,
                        country_id: scope.item.main_country_id,
                        add_customer: !0,
                        article: scope.item.tr_id,
                        'do': scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-tryAddC',
                        item_id: scope.item_id
                    }, 'md');
                    return !1
                } else if (value) {
                    var val = explode('-', value);
                    scope.item.to_buyer_id = val[0];
                    scope.auto.options.to_addresses = [];
                    scope.item.to_main_address_id = undefined;
                    scope.saveClientData(scope)
                }
            },
            onItemAdd(value, $item) {
                if (value == '99999999999') {
                    scope.item.cc_id = ''
                }
            },
            onBlur() {},
            maxItems: 1
        }, helper.getLanguage('Create a new Customer'));
        scope.sccAutoCfg = selectizeFc.configure({
            placeholder: helper.getLanguage('Select supplier'),
            maxOptions: 10,
            onDropdownOpen($dropdown) {
                var _this = this;
                scope.old_buyer_id = _this.$input[0].attributes.value.value;
            },
            onType(str) {
                var selectize = angular.element('#custandcont')[0].selectize;
                selectize.close();
                if (typePromise) {
                    $timeout.cancel(typePromise)
                }
                typePromise = $timeout(function() {
                    var data = {
                        'do': scope.app + '-' + scope.ctrlpag,
                        'xget': 'cc',
                        term: str,
                        'internal': scope.item.internal_value
                    };
                    scope.requestAuto(scope, data, scope.renderCCAuto)
                }, 300)
            },
            onChange(value) {
                // console.log(value);
                if (value == undefined) {
                    scope.auto.options.contacts = [];
                    scope.item.contact_id = undefined;
                    var data = {
                        'do': scope.app + '-' + scope.ctrlpag,
                        'xget': 'cc'
                    };
                    scope.requestAuto(scope, data, scope.renderCCAuto)
                } else if (value == '99999999999') {
                    var obj = {
                        country_dd: scope.item.country_dd,
                        country_id: scope.item.main_country_id,
                        add_customer: !0,
                        article: scope.item.tr_id,
                        'do': scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-tryAddC',
                        identity_id: scope.item.identity_id,
                        item_id: scope.item_id,
                        app_mod: scope.app_mod,
                    }
                    if (scope.app == 'po_order') {
                        scope.item.buyer_id = undefined;
                        obj.tr_id = scope.item.tr_id;
                    }
                    if (scope.app_mod == 'incomminginvoice') {
                        obj.file_path = scope.item.file_path;
                        obj.linkpath = scope.item.linkpath;
                        obj.link_url = scope.item.link_url
                    }
                    scope.openModal(scope, 'AddCustomer', obj, 'md');
                    return !1
                } else if (value) {
                    var selectize = angular.element('#custandcont')[0].selectize;
                    if (selectize) {
                        selectize.blur();
                    }
                    var val = explode('-', value);
                    scope.item.buyer_id = val[0];
                    if (scope.app == 'po_order') {
                        scope.auto.options.contacts = [];
                        scope.item.contact_id = undefined;
                    } else {
                        scope.item.contact_id = val[1];
                    }
                    if (scope.app == 'project') {
                        scope.item.our_reference = this.options[value].ref
                    }
                    if (scope.app_mod == 'incomminginvoice') {
                        scope.item.save_mode = !0
                    }
                    scope.saveClientData(scope)
                }
            },
            onItemAdd(value, $item) {
                if (value == '99999999999') {
                    scope.item.cc_id = ''
                }
            },
            onBlur() {
                if (scope.app != 'po_order' && scope.app != 'invoice') {
                    $timeout(function() {
                        scope.removeCust(scope)
                    })
                }
            },
            maxItems: 1
        }, helper.getLanguage('Create a new supplier'));
        scope.invAutoCfg = selectizeFc.configure({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            delimiter: '|',
            placeholder: 'Select invoice',
            create: !1,
            maxOptions: 6,
            closeAfterSelect: !0,
            onType(str) {
                var selectize = angular.element('#invoices_list')[0].selectize;
                selectize.close();
                if (typePromise) {
                    $timeout.cancel(typePromise)
                }
                typePromise = $timeout(function() {
                    var data = {
                        'do': 'invoice-ninvoice',
                        'xget': 'invoices_list',
                        'term': str,
                        'buyer_id': scope.item.buyer_id
                    };
                    helper.doRequest('get', 'index.php', data, function(o) {
                        scope.auto.options.invoices = o.data;
                        var selectize = angular.element('#invoices_list')[0].selectize;
                        selectize.open()
                    })
                }, 300)
            },
            onChange(value) {
                if (value == 0) {
                    return !1
                }
                if (scope.item.c_invoice_id == undefined) {
                    var data = {
                        'do': 'invoice-ninvoice',
                        'xget': 'invoices_list',
                        'buyer_id': scope.item.buyer_id
                    };
                    helper.doRequest('get', 'index.php', data, function(o) {
                        scope.auto.options.invoices = o.data;
                        scope.item.our_ref = ''
                    })
                } else {
                    var data = {
                        'do': 'invoice-ninvoice',
                        'c_invoice_id': value,
                        'type': scope.item.type,
                        'invoice_id': scope.item.invoice_id,
                        'serial_number': scope.item.serial_number
                    };
                    helper.doRequest('get', 'index.php', data, function(o) {
                        scope.renderPage(o);
                        for (x in scope.auto.options.invoices) {
                            if (scope.auto.options.invoices[x].id == value) {
                                scope.item.our_ref = scope.auto.options.invoices[x].name;
                                break
                            }
                        }
                    })
                }
            },
            maxItems: 1
        })
    }

    function getScope() {
        return this.scope
    }

    function to_trusted(someHTML) {
        return $sce.trustAsHtml(someHTML)
    }

    function requestAuto(scope, data, renderer) {
        helper.doRequest('get', 'index.php', data, function(d) {
            renderer(scope, d)
        })
    }

    function renderContactAuto(scope, d) {
        if (d.data) {
            scope.auto.options.contacts = d.data;
            var selectize = angular.element('#contact_id')[0].selectize;
            selectize.open()
        }
    }

    function openModal(scope, type, item, size) {
        var params = {
            template: scope.tmpl + '/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'ChangeIdentity':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.params = {
                    'do': 'misc-' + type,
                    identity_id: scope.item.identity_id
                };
                params.callback = function(data) {
                    if (data) {
                        scope.item.main_comp_info = data;
                        scope.item.identity_id = data.identity_id
                    }
                }
                break;
            case 'scanbar':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.params = {
                    'do': 'misc-' + type
                };
                params.callback = function(data) {
                    if (data) {

                        angular.element('.loading_wrap').removeClass('hidden');

                        var tmp_data = {
                            'do': 'misc--misc-get_barcode_articles',
                            'lang': scope.item.email_language,
                            'barcodes': data.barcodes,
                        };
                        helper.Loading = 777777777;
                        helper.doRequest('get', 'index.php', tmp_data, function(o) {

                            if (o.data.lines.length || o.data.not_found.length) {
                                angular.element('.loading_wrap').removeClass('hidden');
                                scope.addBarcodeLine(scope, o.data.lines, o.data.not_found);
                            }

                        });

                    }
                }
                break;
            case 'scanbarWarning':

                params.template = 'miscTemplate/' + type + 'Modal';
                params.params = {
                    'do': 'misc-scanbar',
                    'not_found': item,

                };


                params.callback = function(data) {
                    if (data) {
                        console.log(data);
                        //scope.item.payment_type_dd = data.data.lines
                    }
                }
                break;
            case 'EditCustomer':
                params.params = {
                    'do': 'misc-' + type,
                    customer_id: scope.item.buyer_id
                };
                if (item && item.financial_tab) {
                    params.params.financial_tab = '1';
                }
                params.template = 'EditCustomerModal';
                params.backdrop = "static";
                params.callbackExit = function(data) {
                    if (data && typeof(data) == 'object') {
                        scope.item.buyer_name = data.name;
                        scope.item.customer_name = data.name;
                        if (scope.app == 'invoice') {
                            scope.item.seller_bwt_nr = data.btw_nr;
                            if (data.update_invoice_language) {
                                if (scope.item.email_language != data.language) {
                                    scope.item.email_language = data.language
                                }
                            }
                            if (scope.app_mod == 'recinvoice' && item && item.financial_tab && data.changed_invoice_preference) {
                                scope.saveClientData(scope);
                            }
                        }
                    }
                }
                break;
            case 'EditContact':
                if (scope.app == 'order') {
                    var contact_id = scope.item.field == 'contact_id' ? scope.item.customer_id : scope.item.contact_id
                } else {
                    var contact_id = scope.item.contact_id
                }
                if (scope.item.customer_id) {
                    var c_id = scope.item.customer_id
                } else if (scope.item.buyer_id) {
                    var c_id = scope.item.buyer_id
                } else {
                    var c_id = undefined
                }
                params.params = {
                    'do': 'misc-' + type,
                    contact_id: contact_id,
                    c_id: c_id
                };
                params.template = 'EditContactModal';
                params.callbackExit = function(data) {
                    if (data && typeof(data) == 'object') {
                        if (scope.item.field == 'contact_id') {
                            scope.item.buyer_name = data.firstname + ' ' + data.lastname;
                            scope.item.customer_name = data.firstname + ' ' + data.lastname
                        } else {
                            scope.item.contact_name = data.firstname + ' ' + data.lastname
                        }
                    }
                }
                break;
            case 'AddCustomer':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.initial_item = item;
                params.initial_item.app = scope.app;
                if(scope.app_mod){
                    params.initial_item.app_mod = scope.app_mod;
                }
                var related_for = 'null';
                if (item.add_customer) {
                    related_for = 'add_customer'
                } else if (item.add_contact) {
                    related_for = 'add_contact'
                }
                var related_fields_data = {
                    'do': 'misc--misc-get_related_fields',
                    related_for: related_for
                };
                angular.element('.loading_wrap').removeClass('hidden');
                if (item.add_customer || item.add_individual) {
                    params.size = "lg";
                    params.params = { 'do': 'misc--misc-get_customer_other_fields', related_for: related_for };
                } else {
                    if(item.buyer_id){
                        params.size = "lg";
                        related_fields_data.customer_id=item.buyer_id;
                    }
                    params.params = related_fields_data;
                }
                switch (scope.app) {
                    case 'quote':
                        params.initial_item.main_address_id = scope.item.main_address_id;
                        params.initial_item.delivery_address_id = scope.item.delivery_address_id;
                        params.initial_item.sameAddress = scope.item.sameAddress;
                        break;
                    case 'order':
                        params.initial_item.main_address_id = scope.item.main_address_id;
                        params.initial_item.delivery_address_id = scope.item.delivery_address_id;
                        params.initial_item.sameAddress = scope.item.sameAddress;
                        break;
                    case 'po_order':
                        params.initial_item.main_address_id = scope.item.main_address_id;
                        params.initial_item.delivery_address_id = scope.item.delivery_address_id;
                        params.initial_item.client_delivery = scope.item.client_delivery;
                        params.initial_item.sc_id = scope.item.sc_id;
                        params.initial_item.second_address_id = scope.item.second_address_id;
                        break;
                    case 'project':
                        params.initial_item.main_address_id = scope.item.main_address_id;
                        params.initial_item.delivery_address_id = scope.item.delivery_address_id;
                        params.initial_item.sameAddress = scope.item.sameAddress;
                        break;
                    case 'maintenance':
                        params.initial_item.main_address_id = scope.item.main_address_id;
                        params.initial_item.delivery_address_id = scope.item.delivery_address_id;
                        params.initial_item.sameAddress = scope.item.sameAddress;
                        break;
                    case 'contract':
                        params.initial_item.main_address_id = scope.item.main_address_id;
                        params.initial_item.delivery_address_id = scope.item.delivery_address_id;
                        params.initial_item.sameAddress = scope.item.sameAddress;
                        break;
                    case 'invoice':
                        params.initial_item.main_address_id = scope.item.main_address_id;
                        break
                }
                params.callback = function(data) {
                    if (data) {
                        if (scope.app == 'general') {
                            helper.Loading = 1;
                            angular.element('.loading_wrap').removeClass('hidden');
                            if(params.initial_item.add_contact){
                                $state.go('contactView', {
                                    contact_id: data.data.contact_id
                                });
                            }else{
                                $state.go('customerView', {
                                    customer_id: data.data.buyer_id
                                });
                                if(params.initial_item.create_contact){
                                    var main_obj = {
                                        country_dd: [],
                                        country_id: 0,
                                        add_contact: !0,
                                        contact_only: !0,
                                        buyer_id:data.data.buyer_id,
                                        'do': 'misc--misc-tryAddC',
                                    };
                                    openModal(scope, 'AddCustomer', main_obj, 'md');
                                }
                            }                          
                        } else if(scope.app == 'customers' && scope.app_mod == 'view'){
                            helper.Loading = 1;
                            angular.element('.loading_wrap').removeClass('hidden');
                            $state.reload();
                        }else if(params.initial_item.after_save && params.initial_item.after_save == 'update_data'){
                            scope.updateData(data.data.buyer_id);
                        }else {
                            scope.renderPage(data);
                            if (scope.app_mod == 'incomminginvoice') {
                                scope.item.save_mode = !0;
                                scope.saveClientData(scope)
                            }
                            if (item.add_customer && ((scope.app == 'invoice' && !scope.app_mod) || (scope.app == 'invoice' && scope.app_mod == 'recinvoice') || (scope.app == 'invoice' && scope.app_mod == 'sepa') || (scope.app == 'quote' && !scope.app_mod) || scope.app == 'order' || scope.app == 'po_order' || scope.app == 'maintenance' || scope.app == 'project')) {
                                scope.saveClientData(scope);
                            }
                            if(item.add_customer && params.initial_item.create_contact){
                                var selectize = angular.element('#contact_id')[0].selectize;
                                $timeout(function() {
                                    selectize.addItem('99999999999');
                                });
                            }
                        }
                    }
                    scope.showTxt.address = !1;
                    scope.showTxt.addBtn = !0
                }
                break;
            case 'AddAddress':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.item = item;
                params.item.title = scope.addr_type;
                params.item.app = scope.app;
                if (scope.app_serv) {
                    params.item.app_serv = scope.app_serv
                }
                params.callbackExit = function(r) {
                    var callbackExitData = {};
                    callbackExitData.do = 'misc--customers-get_addresses';
                    if (scope.ctrlpag == 'purchase_ninvoice') {
                        scope.app = 'purchase_ninvoice'
                    }
                    switch (scope.app) {
                        case 'quote':
                            callbackExitData.app = scope.app;
                            callbackExitData.buyer_id = scope.item.buyer_id;
                            callbackExitData.secondaryAddressesCheck = !0;
                            callbackExitData.delivery_address_id = scope.item.delivery_address_id;
                            helper.doRequest('get', 'index.php', callbackExitData, function(r) {
                                if (r.data && r.data.addresses) {
                                    scope.auto.options.addresses = r.data.addresses
                                }
                                if (r.data && r.data.secondaryAddresses) {
                                    scope.auto.options.secondaryAddresses = r.data.secondaryAddresses
                                }
                            });
                            break;
                        case 'order':
                            callbackExitData.app = scope.app;
                            callbackExitData.buyer_id = scope.item.buyer_id;
                            helper.doRequest('get', 'index.php', callbackExitData, function(r) {
                                if (r.data && r.data.addresses) {
                                    scope.auto.options.addresses = r.data.addresses;
                                    scope.auto.options.site_addresses = r.data.addresses
                                }
                            });
                            break;
                        case 'po_order':
                            callbackExitData.app = scope.app;
                            callbackExitData.buyer_id = scope.item.buyer_id;
                            helper.doRequest('get', 'index.php', callbackExitData, function(r) {
                                if (r.data && r.data.addresses) {
                                    scope.auto.options.addresses = r.data.addresses;
                                    scope.auto.options.second_addresses = r.data.addresses
                                }
                            });
                            break;
                        case 'project':
                            callbackExitData.app = scope.app;
                            callbackExitData.buyer_id = scope.item.buyer_id;
                            helper.doRequest('get', 'index.php', callbackExitData, function(r) {
                                if (r.data && r.data.addresses) {
                                    scope.auto.options.addresses = r.data.addresses
                                }
                            });
                            break;
                        case 'maintenance':
                            callbackExitData.app = scope.app;
                            callbackExitData.site_add = !0;
                            callbackExitData.buyer_id = scope.item.buyer_id;
                            helper.doRequest('get', 'index.php', callbackExitData, function(r) {
                                if (r.data && r.data.addresses) {
                                    scope.auto.options.addresses = r.data.addresses
                                }
                                if (r.data && r.data.secondaryAddresses) {
                                    scope.auto.options.site_addresses = r.data.secondaryAddresses
                                }
                            });
                            break;
                        case 'installation':
                            callbackExitData.app = scope.app;
                            callbackExitData.buyer_id = scope.item.buyer_id;
                            helper.doRequest('get', 'index.php', callbackExitData, function(r) {
                                if (r.data && r.data.addresses) {
                                    scope.auto.options.addresses = r.data.addresses
                                }
                            });
                            break;
                        case 'contract':
                            callbackExitData.app = scope.app;
                            callbackExitData.buyer_id = scope.item.buyer_id;
                            helper.doRequest('get', 'index.php', callbackExitData, function(r) {
                                if (r.data && r.data.addresses) {
                                    scope.auto.options.addresses = r.data.addresses
                                }
                            });
                            break;
                        case 'invoice':
                            callbackExitData.app = scope.app;
                            callbackExitData.buyer_id = scope.item.buyer_id;
                            helper.doRequest('get', 'index.php', callbackExitData, function(r) {
                                if (r.data && r.data.addresses) {
                                    scope.auto.options.addresses = r.data.addresses
                                }
                            });
                            break;
                        case 'purchase_ninvoice':
                            callbackExitData.app = scope.app;
                            callbackExitData.buyer_id = scope.item.buyer_id;
                            helper.doRequest('get', 'index.php', callbackExitData, function(r) {
                                if (r.data && r.data.addresses) {
                                    scope.auto.options.addresses = r.data.addresses
                                }
                            });
                            break
                    }
                }
                params.callback = function(data) {
                    if (data) {
                        scope.renderPage(data)
                    }
                    if ((scope.app == 'invoice' && !scope.app_mod) || (scope.app == 'quote' && !scope.app_mod) || scope.app == 'customers' || scope.app == 'order' || scope.app == 'po_order' || scope.app == 'maintenance') {
                        scope.showTxt.address = !1;
                    } else {
                        scope.showTxt.address = !0;
                    }
                    scope.showTxt.addBtn = !0
                }
                break;
            case 'purchasePrice':
                params.params = {
                    'do': 'order-' + type,
                    article_id: item.article_id,
                    order_id: scope.item_id,
                    purchase_price: item.purchase_price,
                    tr_id: item.tr_id
                };
                params.callback = function(data) {
                    if (data) {
                        for (x in scope.item.tr_id) {
                            if (scope.item.tr_id[x].tr_id == data.tr_id) {
                                scope.item.tr_id[x].purchase_price = helper.return_value(data.purchase_price)
                            }
                        }
                    }
                }
                break;
            case 'purchasePriceOrder':
                params.template = scope.tmpl + '/purchasePriceOrderModal';
                params.ctrl = 'purchasePriceOrderModalCtrl';
                params.params = {
                    'do': 'order-purchasePriceNew',
                    article_id: item.article_id,
                    order_id: scope.item_id,
                    purchase_price: item.purchase_price,
                    tr_id: item.tr_id,
                    purchase_price_other_currency: item.purchase_price_other_currency,
                    purchase_price_other_currency_id: item.purchase_price_other_currency_id
                };
                params.callback = function(data) {
                    if (data) {
                        if (item.save_data) {
                            /*item.purchase_price=helper.return_value(data.purchase_price);
                            item.purchase_price_other_currency=helper.return_value(data.purchase_price_other_currency);
                            item.purchase_price_other_currency_id=data.purchase_price_other_currency_id;
                            helper.doRequest('post', 'index.php', {'do':'order-order-order-update_purchase_price','order_id':scope.order_id,'order_articles_id':item.order_articles_id,'purchase_price':data.purchase_price,'purchase_price_other_currency':data.purchase_price_other_currency,'purchase_price_other_currency_id':data.purchase_price_other_currency_id},scope.renderPage);*/
                            scope.edit_purchase_price = false;
                            scope.order.lines[item.c_index].purchase_price = helper.return_value(data.purchase_price);
                            scope.order.lines[item.c_index].purchase_price_other_currency = helper.return_value(data.purchase_price_other_currency);
                            scope.order.lines[item.c_index].purchase_price_other_currency_id = data.purchase_price_other_currency_id;
                        } else {
                            for (x in scope.item.tr_id) {
                                if (scope.item.tr_id[x].tr_id == data.tr_id) {
                                    scope.item.tr_id[x].purchase_price = helper.return_value(data.purchase_price);
                                    scope.item.tr_id[x].purchase_price_other_currency = helper.return_value(data.purchase_price_other_currency);
                                    scope.item.tr_id[x].purchase_price_other_currency_id = data.purchase_price_other_currency_id;
                                }
                            }
                        }
                    }
                }
                params.callbackExit = function() {
                    scope.edit_purchase_price = false;
                }
                break;
            case 'AddAnArticle':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.params = item;
                params.callback = function(data) {
                    if (data) {
                        if (!scope.app) {
                            scope.app = 'order'
                        }
                        switch (scope.app) {
                            case 'quote':
                            case 'contract':
                                addQuoteArticle(scope, data.data.lines[0]);
                                break;
                            case 'invoice':
                                addInvoiceArticle(scope, data.data.lines[0]);
                                break;
                            case 'project':
                                if (data.data.lines[0].is_service == '1') {
                                    scope.item.task_name = data.data.lines[0].name;
                                    scope.AddTask(1);
                                    var data = {
                                        'do': 'project-project',
                                        xget: 'articles_list',
                                        cat_id: scope.item.cat_id,
                                        'only_service': !0
                                    };
                                    helper.doRequest('get', 'index.php', data, function(r) {
                                        scope.item.tasks_list = r.data
                                    })
                                } else {
                                    scope.item.articles_list.push(data.data.lines[0]);
                                    scope.item.article_id = data.data.lines[0].article_id;
                                    scope.addArticle()
                                }
                                break;
                            case 'maintenance':
                                if (data.data.lines[0].is_service == '1') {
                                    var tsmp = Math.floor(Date.now() / 1000);
                                    var tmp_item = {};
                                    tmp_item.name = value;
                                    tmp_item.id = 'tmp_' + tsmp;
                                    tmp_item.checked = false;
                                    tmp_item['no-plus'] = '';
                                    tmp_item.comment = '';
                                    tmp_item.ALLOW_STOCK = helper.settings.ALLOW_STOCK == 1 ? true : false;
                                    tmp_item.readonly = '';
                                    tmp_item.task_budget = helper.displayNr(this.options[value].price);
                                    tmp_item.code = this.options[value].code;
                                    tmp_item.quantity = helper.displayNr(1);
                                    tmp_item.article_id = this.options[value].article_id;
                                    scope.item.services.services.push(tmp_item);
                                    scope.item.services.is_data += 1;
                                    scope.item.task_service_id = null;
                                    scope.item.task_name = '';
                                    scope.item.add_subtask1 = false;
                                    scope.item.add_subtask2 = false;
                                    /*var obj = {};
                                    obj.service_id = scope.item.service_id;
                                    obj.do = 'maintenance-service-maintenance-serviceAddService';
                                    obj.xget = 'serviceSubServices';
                                    obj.task_service_id = data.data.lines[0].article_id;
                                    obj.task_name = data.data.lines[0].name;
                                    obj.task_budget = data.data.lines[0].price;
                                    obj.quantity = data.data.lines[0].quantity;
                                    helper.doRequest('post', 'index.php', obj, function(r) {
                                        if (r && r.data) {
                                            if (r.success) {
                                                scope.item.services = r.data;
                                                scope.item.task_service_id = undefined
                                            }
                                            scope.item.task_name = '';
                                            scope.item.add_subtask1 = !1;
                                            scope.item.add_subtask2 = !1
                                        }
                                    })*/
                                } else {
                                    scope.addArticle(data.data.lines[0])
                                }
                                break;
                            default:
                                addArticle(scope, data.data.lines[0]);
                                break
                        }
                        scope.selected_article_id = '';
                        scope.showTxt.addArticleBtn = !1;
                        var data = {
                            'do': scope.app + '-' + scope.ctrlpag,
                            xget: 'articles_list',
                            customer_id: scope.item.buyer_id,
                            lang_id: scope.item.email_language,
                            cat_id: scope.item.price_category_id,
                            remove_vat: scope.item.remove_vat,
                            vat_regime_id: scope.item.vat_regime_id
                        };
                        scope.requestAuto(scope, data, scope.renderArticleListAuto)
                    }
                }
                break;
            case 'addArticleSpecial':
                params.template = 'miscTemplate/' + type + 'Modal';
                switch (scope.app) {
                    default: params.ctrl = 'addArticleSpecialModalCtrl';
                    var only_articles = '0'
                }
                params.params = {
                    'do': 'misc-addArticleSpecial',
                    app: scope.app,
                    customer_id: scope.item.buyer_id,
                    c_quote_id: scope.c_quote_id
                };
                params.callback = function(data) {
                    if (data) {
                        if (scope.app == 'invoice') {
                            addInvoiceArticleSpecial(scope, data);
                            openModal(scope, 'addArticle', '', 'lg');
                            return !1
                        }
                        var line = {
                                article: data.quoteformat,
                                article_code: data.code,
                                article_id: data.article_id,
                                colspan: "",
                                colum: scope.item.colum,
                                content: !1,
                                content_class: "",
                                disc: "0,00",
                                hide_stock: data.hide_stock,
                                is_article: 1,
                                is_article_code: !0,
                                is_tax: 0,
                                line_total: "0,00",
                                packing: data.packing,
                                packing_x: data.packing,
                                pending_articles: data.pending_articles,
                                percent: data.vat,
                                percent_x: helper.displayNr(data.vat),
                                price: helper.displayNr(scope.app == 'po_order' ? data.purchase_price : data.price),
                                price_vat: helper.displayNr(data.price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                                purchase_price: data.purchase_price,
                                quantity: helper.displayNr(data.quantity),
                                quantity_old: helper.displayNr(data.quantity),
                                sale_unit: data.sale_unit,
                                sale_unit_x: data.sale_unit,
                                stock: data.stock,
                                tax_for_article_id: "0",
                                threshold_value: data.threshold_value,
                                tr_id: "tmp" + (new Date().getTime()),
                                vat_value: data.vat_value,
                                vat_value_x: data.vat_value,
                                allow_stock: data.allow_stock,
                            },
                            exchange = 0;
                        if (data.quantity > 1 && scope.app != 'po_order') {
                            var params = {
                                article_id: data.article_id,
                                price: data.price,
                                quantity: data.quantity,
                                customer_id: scope.item.buyer_id,
                                'do': 'order-order-order-get_article_quantity_price'
                            };
                            helper.doRequest('post', 'index.php', params, function(r) {
                                if (r.data) {
                                    line.price = helper.displayNr(r.data.new_price)
                                }
                                if (scope.item.currency_type_val != scope.item.default_currency) {
                                    line.price = line.price / edit.order.currency_rate;
                                    exchange = 1
                                }
                                scope.item.tr_id.push(line);
                                scope.calcTotal('price', scope.item.tr_id.length - 1);
                                var params = {
                                    article_id: data.article_id,
                                    quantity: data.quantity,
                                    exchange: exchange,
                                    ex_rate: scope.item.currency_rate,
                                    allow_article_sale_unit: scope.item.allow_article_sale_unit,
                                    allow_article_packing: scope.item.allow_article_packing,
                                    remove_vat: scope.item.remove_vat,
                                    apply_discount: scope.item.apply_discount,
                                    'do': 'misc-addArticleList',
                                    xget: 'taxes'
                                };
                                helper.doRequest('get', 'index.php', params, function(res) {
                                    if (res.data && res.data.lines) {
                                        var d = res.data.lines
                                        for (x in d) {
                                            var taxLine = {
                                                article: d[x].tax_name,
                                                article_code: d[x].tax_code,
                                                quantity: d[x].quantity,
                                                quantity_old: d[x].quantity_old,
                                                article_id: d[x].tax_id,
                                                is_tax: 1,
                                                tax_for_article_id: d[x].tax_for_article_id,
                                                vat_percent: d[x].percent,
                                                vat_value: d[x].vat_value,
                                                packing: 1,
                                                packing_x: 1,
                                                stock: d[x].stock,
                                                content: !1,
                                                price: d[x].price,
                                                purchase_price: 0,
                                                percent: d[x].percent,
                                                percent_x: d[x].percent_x,
                                                price_vat: d[x].price_vat,
                                                sale_unit: 1,
                                                sale_unit_x: 1,
                                                disc: "0,00",
                                                colum: scope.item.colum,
                                                line_total: "0,00",
                                                for_article: line.tr_id,
                                                tr_id: "tmp" + (new Date().getTime()),
                                            }
                                            scope.item.tr_id.push(taxLine);
                                            scope.calcTotal('price', scope.item.tr_id.length - 1)
                                        }
                                    }
                                    if (helper.return_value(data.stock2) - data.quantity < parseFloat(data.threshold_value) && data.hide_stock != 1 && data.allow_stock) {
                                        //scope.openModal(scope, 'stockWarning', data, 'sm')
                                    } else {
                                        scope.openModal(scope, 'addArticle')
                                    }
                                })
                            })
                        }
                    }
                }
                break;
            case 'addArticle':
                params.template = 'miscTemplate/' + type + 'Modal';
                switch (scope.app) {
                    case 'maintenance':
                    case 'project':
                        var only_articles = '0';
                        break;
                    case 'stock':
                        params.ctrl = 'addArticlePOModalCtrl';
                        var only_articles = '0';
                        break;
                    case 'po_order':
                        params.ctrl = 'addArticlePOModalCtrl';
                        var only_articles = '0';
                        break;
                    case 'article':
                        params.template = 'miscTemplate/addArticleCombinedModal';
                        var only_articles = '0',
                            hide_article_ids = scope.item.article_id;
                        break;
                    default:
                        params.ctrl = 'addArticleModalCtrl';
                        var only_articles = '0'
                }
                params.params = {
                    'do': 'misc-addArticleList',
                    app: scope.app,
                    customer_id: scope.item.buyer_id,
                    lang_id: scope.item.email_language,
                    cat_id: scope.item.price_category_id,
                    remove_vat: scope.item.remove_vat,
                    only_articles: only_articles,
                    vat_regime_id: scope.item.vat_regime_id,
                    hide_article_ids: hide_article_ids,
                };
                params.callback = function(data) {
                    if (data) {
                        if (scope.app == 'invoice') {
                            addInvoiceArticle(scope, data);
                            openModal(scope, 'addArticle', '', 'lg');
                            return !1
                        } else if (scope.app == 'maintenance') {
                            scope.addArticle(data);
                            openModal(scope, 'addArticle', '', 'lg');
                            return !1
                        } else if (scope.app == 'project') {
                            scope.addArticle(data);
                            openModal(scope, 'addArticle', '', 'lg');
                            return !1
                        }
                        var line = {
                                article: data.quoteformat,
                                article_code: data.code,
                                article_id: data.article_id,
                                colspan: "",
                                colum: scope.item.colum,
                                content: !1,
                                content_class: "",
                                disc: "0,00",
                                hide_stock: data.hide_stock,
                                is_article: 1,
                                is_article_code: !0,
                                is_tax: 0,
                                line_total: "0,00",
                                packing: data.packing,
                                packing_x: data.packing,
                                pending_articles: data.pending_articles,
                                percent: data.vat,
                                percent_x: helper.displayNr(data.vat),
                                price: helper.displayNr(scope.app == 'po_order' ? data.purchase_price : data.price),
                                price_vat: helper.displayNr(data.price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                                purchase_price: data.purchase_price,
                                quantity: helper.displayNr(data.quantity),
                                quantity_old: helper.displayNr(data.quantity),
                                sale_unit: data.sale_unit,
                                sale_unit_x: data.sale_unit,
                                stock: data.stock,
                                tax_for_article_id: "0",
                                threshold_value: data.threshold_value,
                                tr_id: "tmp" + (new Date().getTime()),
                                vat_value: data.vat_value,
                                vat_value_x: data.vat_value,
                                allow_stock: data.allow_stock,
                            },
                            exchange = 0;

                        if (data.quantity > 1 && scope.app != 'po_order') {
                            var params = {
                                article_id: data.article_id,
                                price: data.price,
                                quantity: data.quantity,
                                customer_id: scope.item.buyer_id,
                                'do': 'order-order-order-get_article_quantity_price'
                            };
                            helper.doRequest('post', 'index.php', params, function(r) {
                                if (r.data) {
                                    line.price = helper.displayNr(r.data.new_price)
                                }
                                if (scope.item.currency_type_val != scope.item.default_currency) {
                                    line.price = line.price / edit.order.currency_rate;
                                    exchange = 1
                                }
                                scope.item.tr_id.push(line);
                                scope.calcTotal('price', scope.item.tr_id.length - 1);
                                var params = {
                                    article_id: data.article_id,
                                    quantity: data.quantity,
                                    exchange: exchange,
                                    ex_rate: scope.item.currency_rate,
                                    allow_article_sale_unit: scope.item.allow_article_sale_unit,
                                    allow_article_packing: scope.item.allow_article_packing,
                                    remove_vat: scope.item.remove_vat,
                                    apply_discount: scope.item.apply_discount,
                                    vat_regime_id: scope.item.vat_regime_id,
                                    'do': 'misc-addArticleList',
                                    xget: 'taxes'
                                };
                                helper.doRequest('get', 'index.php', params, function(res) {
                                    if (res.data && res.data.lines) {
                                        var d = res.data.lines
                                        for (x in d) {
                                            var show_line = !1;
                                            if (scope.app == 'invoice' || scope.app == 'order' || scope.app == 'quote' || scope.app == 'maintenance') {
                                                if (d[x].apply_to == '0' || d[x].apply_to == '1') {
                                                    show_line = !0
                                                }
                                            } else if (scope.app == 'po_order') {
                                                if (d[x].apply_to == '0' || d[x].apply_to == '2') {
                                                    show_line = !0
                                                }
                                            }
                                            var taxLine = {
                                                article: d[x].tax_name,
                                                article_code: d[x].tax_code,
                                                quantity: d[x].quantity,
                                                quantity_old: d[x].quantity_old,
                                                article_id: d[x].tax_id,
                                                is_tax: 1,
                                                tax_for_article_id: d[x].tax_for_article_id,
                                                vat_percent: d[x].percent,
                                                vat_value: d[x].vat_value,
                                                packing: 1,
                                                packing_x: 1,
                                                stock: d[x].stock,
                                                content: !1,
                                                price: d[x].price,
                                                purchase_price: 0,
                                                percent: d[x].percent,
                                                percent_x: d[x].percent_x,
                                                price_vat: d[x].price_vat,
                                                sale_unit: 1,
                                                sale_unit_x: 1,
                                                disc: "0,00",
                                                colum: scope.item.colum,
                                                line_total: "0,00",
                                                for_article: line.tr_id,
                                                tr_id: "tmp" + (new Date().getTime()),
                                                apply_to: d[x].apply_to,
                                                show_line: show_line
                                            }
                                            if (show_line) {
                                                scope.item.tr_id.push(taxLine);
                                                scope.calcTotal('price', scope.item.tr_id.length - 1)
                                            }
                                        }
                                    }
                                    if (helper.return_value(data.stock2) - data.quantity < parseFloat(data.threshold_value) && data.hide_stock != 1 && data.allow_stock) {
                                        //scope.openModal(scope, 'stockWarning', data, 'sm')
                                    } else {
                                        scope.openModal(scope, 'addArticle')
                                    }
                                })
                            })
                        } else {
                            scope.item.tr_id.push(line);
                            scope.app == 'po_order' ? scope.calcTotalPurchase() : scope.calcTotal();
                            var params = {
                                article_id: data.article_id,
                                quantity: data.quantity,
                                exchange: exchange,
                                ex_rate: scope.item.currency_rate,
                                allow_article_sale_unit: scope.item.allow_article_sale_unit,
                                allow_article_packing: scope.item.allow_article_packing,
                                remove_vat: scope.item.remove_vat,
                                apply_discount: scope.item.apply_discount,
                                vat_regime_id: scope.item.vat_regime_id,
                                'do': 'misc-addArticleList',
                                xget: 'taxes'
                            };
                            helper.doRequest('get', 'index.php', params, function(res) {
                                if (res.data && res.data.lines) {
                                    var d = res.data.lines
                                    for (x in d) {
                                        var show_line = !1;
                                        if (scope.app == 'invoice' || scope.app == 'order' || scope.app == 'quote' || scope.app == 'maintenance') {
                                            if (d[x].apply_to == '0' || d[x].apply_to == '1') {
                                                show_line = !0
                                            }
                                        } else if (scope.app == 'po_order') {
                                            if (d[x].apply_to == '0' || d[x].apply_to == '2') {
                                                show_line = !0
                                            }
                                        }
                                        var taxLine = {
                                            article: d[x].tax_name,
                                            article_code: d[x].tax_code,
                                            quantity: d[x].quantity,
                                            quantity_old: d[x].quantity_old,
                                            article_id: d[x].tax_id,
                                            is_tax: 1,
                                            tax_for_article_id: d[x].tax_for_article_id,
                                            vat_percent: d[x].vat,
                                            vat_value: d[x].vat_value,
                                            packing: 1,
                                            packing_x: 1,
                                            stock: d[x].stock,
                                            content: !1,
                                            price: d[x].price,
                                            purchase_price: 0,
                                            percent: d[x].vat,
                                            percent_x: d[x].percent_x,
                                            price_vat: d[x].price_vat,
                                            sale_unit: 1,
                                            sale_unit_x: 1,
                                            disc: "0,00",
                                            colum: scope.item.colum,
                                            line_total: "0,00",
                                            for_article: line.tr_id,
                                            tr_id: "tmp" + (new Date().getTime()),
                                            apply_to: d[x].apply_to,
                                            show_line: show_line
                                        }
                                        if (show_line) {
                                            scope.item.tr_id.push(taxLine);
                                            scope.app == 'po_order' ? scope.calcTotalPurchase() : scope.calcTotal()
                                        }
                                    }
                                }


                                if (scope.app != 'stock' && scope.app != 'po_order' && helper.return_value(data.stock2) - data.quantity < parseFloat(data.threshold_value) && data.hide_stock != 1 && data.allow_stock) {
                                    //scope.openModal(scope, 'stockWarning', data, 'sm')
                                } else {
                                    scope.openModal(scope, 'addArticle')
                                }
                            })
                        }
                    }
                }
                break;
            case 'stockWarning':
            case 'Alert':
                params.item = item;
                break;
            case 'selectEdit':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.params = {
                    'do': 'misc-' + type,
                    'table': item
                };
                params.callback = function(data) {
                    if (data) {
                        scope.item.payment_type_dd = data.data.lines
                    }
                }
            case 'quotesTemplates':
                params.params = {
                    'do': 'quote-quotes',
                    xget: type,
                    'index': item,
                    'lang': scope.item.email_language
                };
                params.callback = function(data) {
                    if (data) {
                        var i = 1;
                        //angular.element('.loading_wrap').removeClass('hidden');
                        if(data.list.length){
                            scope.addChapterFromTemplate(scope, data.list, item, i, data.refresh_prices)
                        }                        
                    }
                }
                break;
            case 'ProgressFacqBasket':

                params.callback = function(data) {
                    if (data) {
                        /* console.log(data);
                         var i = 1;
                         //angular.element('.loading_wrap').removeClass('hidden');
                         for(x in data.items){
                             scope.addFacqLine(scope, data.items[x]);
                         }
                        */
                    }
                }
                break;
            case 'contractTemplates':
                params.params = {
                    'do': 'contract-contracts_templates',
                    'index': item
                };
                params.callback = function(data) {
                    if (data) {
                        var i = 1;
                        //angular.element('.loading_wrap').removeClass('hidden');
                        scope.addChapterFromTemplate(scope, data, item, i);
                    }
                }
                break;
            case 'ManageAddresses':
                params.template = 'cTemplate/ManageAddressesModal';
                params.ctrl = 'ManageAddressesModalCtrl';
                params.params = {
                    'do': 'customers-ViewCustomer',
                    'customer_id': item,
                    'show_add': !1,
                    'xget': 'customerAddresses'
                };
                break;
            case 'EditCustomerNotes':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.windowClass = "hidden";
                params.params = { 'do': 'misc--misc-retrieveCustomerNotes', 'buyer_id': item.buyer_id };
                break;
        }
        modalFc.open(params)
    }

    function addChapterFromTemplate(scope, arr, item, i, refresh_prices) {
        if (scope.app == 'quote' && !scope.app_mod && $rootScope.edited_data.indexOf('quote_edit') === -1) {
            $rootScope.edited_data.push('quote_edit');
        }
        var inc = item;
        if (arr.length) {
            helper.Loading++;
            var obj = {};
            obj.do = scope.app == 'contract' ? 'contract-contract' : 'quote-nquote';
            obj.template_id = arr[0];
            obj.refresh_prices=refresh_prices ? '1' : '';
            obj.buyer_id=scope.item.buyer_id;
            cat_id=scope.item.cat_id;
            helper.doRequest('get', 'index.php', obj, function(r) {
                if (scope.app == 'quote') {
                    for (y in r.data.quote_group) {
                        if (typeof scope.item.quote_group[inc] === 'undefined') {
                            scope.item.quote_group.splice(inc, 0, r.data.quote_group[y]);
                        } else {
                            scope.item.quote_group[inc].show_IC = r.data.quote_group[y].show_IC;
                            scope.item.quote_group[inc].show_QC = r.data.quote_group[y].show_QC;
                            scope.item.quote_group[inc].show_UP = r.data.quote_group[y].show_UP;
                            scope.item.quote_group[inc].show_price_vat = r.data.quote_group[y].show_price_vat;
                            scope.item.quote_group[inc].show_PS = r.data.quote_group[y].show_PS;
                            scope.item.quote_group[inc].show_d = r.data.quote_group[y].show_d;
                            scope.item.quote_group[inc].show_AC = r.data.quote_group[y].show_AC;
                            scope.item.quote_group[inc].show_chapter_total = r.data.quote_group[y].show_chapter_total;
                            scope.item.quote_group[inc].show_block_total = r.data.quote_group[y].show_block_total;
                            scope.item.quote_group[inc].title = r.data.quote_group[y].title;
                            if (scope.item.quote_group[inc].table.length == 0) {
                                addQuoteArticle(scope, '', 7);
                            } else if (scope.item.quote_group[inc].table[0].quote_line.length == 0) {
                                addQuoteArticle(scope, '', 7);
                            }
                            for (z in r.data.quote_group[y].table[0].quote_line) {
                                //skip header and chapter subtotal lines
                                if (r.data.quote_group[y].table[0].quote_line[z].line_type == 7 || r.data.quote_group[y].table[0].quote_line[z].line_type == '8') {
                                    continue;
                                }

                                //insert at the last index, not push because there might be subtotal line as last in index
                                scope.item.quote_group[inc].table[0].quote_line.splice(scope.item.quote_group[inc].table[0].quote_line.length, 0, r.data.quote_group[y].table[0].quote_line[z]);
                            }
                        }
                        inc += i;
                    }
                } else {
                    for (y in r.data.quote_group) {
                        inc += i;
                        scope.item.quote_group.splice(inc, 0, r.data.quote_group[y]);
                        i++
                    }
                }
                arr.shift();
                return scope.addChapterFromTemplate(scope, arr, item, i, refresh_prices)
            })
        } else {
            if (scope.app != 'quote') {
                scope.item.quote_group.splice(item, 1);
                scope.save(!1, !0)
            } else {
                scope.calcQuoteTotal(scope);
            }
        }
    }

    function removeContact(scope) {
        scope.item.contact_id = '';
        if (scope.item.free_field) {
            scope.item.free_field_txt = scope.item.free_field_txt.replace(scope.item.contact_name, '');
            scope.item.free_field = scope.item.free_field.replace(scope.item.contact_name, '')
        }
        if (scope.item.address_info) {
            scope.item.address_info_txt = scope.item.address_info_txt.replace(scope.item.contact_name, '');
            scope.item.address_info = scope.item.address_info.replace(scope.item.contact_name, '')
        }
        scope.item.contact_name = ''
    }

    function removeDeliveryAddr(scope) {
        scope.item.delivery_address_id = '';
    }

    function removeMainDeliveryAddr(scope) {
        scope.item.main_address_id = '';
    }
    
    function removeAccount(scope) {
        scope.item.buyer_id = '';
        scope.item.contact_id = '';
        scope.item.delivery_address_id = '';
        scope.item.main_address_id = '';
        var data = {
            'do': scope.app + '-' + scope.ctrlpag,
            'xget': 'cc'
        };
        scope.requestAuto(scope, data, scope.renderCCAuto)
    }

    function renderAddressAuto(scope, d) {
        if (d.data) {
            if (scope.app == 'maintenance' || scope.app == 'order') {
                scope.auto.options.site_addresses = d.data
            } else if (scope.app == 'quote') {
                scope.auto.options.secondaryAddresses = d.data
            } else if (scope.app == 'po_order') {
                scope.auto.options.second_addresses = d.data
            } else {
                scope.auto.options.addresses = d.data
            }
            var selectize = angular.element('#delivery_address')[0].selectize;
            selectize.open()
        }
    }

    function renderMainAddressAuto(scope, d) {
        if (d.data) {
            scope.auto.options.addresses = d.data;
            var selectize = angular.element('#main_address')[0].selectize;
            selectize.open()
        }
    }

    function renderCCAuto(scope, d) {
        if (d.data) {
            scope.auto.options.cc = d.data;
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        }
    }

    function renderToCCAuto(scope, d) {
        if (d.data) {
            scope.auto.options.to_cc = d.data;
            var selectize = angular.element('#tocustandcont')[0].selectize;
            selectize.open()
        }
    }

    function renderSCAuto(scope, d) {
        if (d.data) {
            scope.auto.options.sc = d.data;
            var selectize = angular.element('#sc_id')[0].selectize;
            selectize.open()
        }
    }

    function saveClientData(scope, field, formObj, withoutConfirm) {
        if ((scope.app != 'invoice' && scope.app != 'quote' && scope.app != 'order' && scope.app != 'po_order' && scope.app != 'customers' && scope.app != 'maintenance' && scope.app != 'stock' && scope.app != 'project' && scope.app != 'contract' && scope.app != 'installation') || (scope.app == 'invoice' && scope.app_mod && scope.app_mod != 'recinvoice' && scope.app_mod != 'incomminginvoice' && scope.app_mod != 'sepa')) {
            scope.showTxt.address = !0;
        }
        if (scope.app == 'customers' && field) {
            scope.showTxt.address = !0;
        }
        scope.item.add_customer = !1;
        if (scope.showTxt.addBtn == !1) {
            scope.showTxt.addBtn = !0;
            scope.showTxt.address = !1
        }
        var data = {};
        for (x in scope.item) {
            data[x] = scope.item[x]
        }
        if (scope.app == 'invoice' || scope.app == 'project' || scope.app == 'maintenance') {
            data = helper.clearData(data, ['APPLY_DISCOUNT_LIST', 'accmanager', 'addresses', 'articles_list', 'cc', 'contacts', 'country_dd', 'currency_type_list', 'language_dd', 'main_comp_info', 'multiple_identity_dd', 'seller_b_country_dd', 'seller_d_country_dd', 'vat_regime_dd']);
            data.do_request = data.do_next;
            if (scope.item.sameAddress) {
                data.sameAddress = 1
            }
        } else {
            data = helper.clearData(data, ['APPLY_DISCOUNT_LIST', 'CURRENCY_TYPE_LIST', 'SOURCE_DD', 'STAGE_DD', 'TYPE_DD', 'accmanager', 'addresses', 'authors', 'articles_list', 'authors', 'cc', 'contacts', 'country_dd', 'language_dd', 'main_comp_info', 'sort_order']);
            data.article = scope.item.tr_id;
            data.sameAddress = scope.item.sameAddress
        }
        if (scope.app == 'project') {
            data.our_reference = scope.item.our_reference
        }
        data.buyer_id = scope.item.buyer_id;
        data.customer_id = scope.item.buyer_id;
        data.contact_id = scope.item.contact_id;
        data.item_id = scope.item_id;
        data.identity_id = scope.item.identity_id;
        if (scope.app_mod == 'recinvoice') {
            data['do'] = scope.item_id != 'tmp' ? scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-updateCustomerDataRec' : data.do_request
        } else if (scope.app_mod == 'incomminginvoice') {
            data['do'] = scope.item_id != 'tmp' ? scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-updateCustomerDataInc' : data.do_request
        } else if (scope.app_mod == 'sepa') {
            data['do'] = data.do_next
        } else if (scope.app == 'customers') {
            data['do'] = scope.item_id != 'tmp' ? scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-updateCustomerDataDeal' : data.do_request
        } else if (scope.app == 'stock') {
            data['do'] = data.do_request;
        } else {
            data['do'] = scope.item_id != 'tmp' ? scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-updateCustomerData' : data.do_request
        }
        data.isAdd = scope.item_id != 'tmp' ? !1 : !0;
        data.contact_id = scope.item.contact_id;
        data.delivery_address = scope.item.delivery_address;
        data.delivery_address_id = scope.item.delivery_address_id;
        data.delivery_address_txt = scope.item.delivery_address_txt;
        data.field = field;
        if (scope.app == 'invoice' || scope.app == 'project' || scope.app == 'maintenance' || scope.app == 'installation') {
            if (scope.app_mod && scope.app_mod == 'incomminginvoice') {
                scope.postC(scope, data, 'post', formObj)
            } else {
                if (!withoutConfirm && scope.item.invoice_line && scope.item.invoice_line.length && (scope.removedCustomer == !0 || (scope.app == 'invoice' && !scope.app_mod) || (scope.app == 'invoice' && scope.app_mod == 'recinvoice'))) {
                    $confirm({
                        ok: helper.getLanguage('Yes'),
                        cancel: helper.getLanguage('No'),
                        text: helper.getLanguage('Recalculate prices')
                    }).then(function() {
                        data.changePrices = '1';
                        scope.postC(scope, data, 'post', formObj)
                    }, function(e) {
                        if (e == 'dismiss') {
                            data.buyer_id = scope.old_buyer_id;
                            var selectize = angular.element('#custandcont')[0].selectize;
                            selectize.addItem(scope.old_buyer_id, true);
                        }
                        scope.postC(scope, data, 'post', formObj)
                    })
                } else {
                    scope.postC(scope, data, 'post', formObj)
                }
            }
        } else {
            if (scope.item.tr_id && scope.item.tr_id.length && (scope.removedCustomer == !0 || scope.app == 'order' || scope.app == 'po_order')) {
                $confirm({
                    ok: helper.getLanguage('Yes'),
                    cancel: helper.getLanguage('No'),
                    text: helper.getLanguage('Recalculate prices')
                }).then(function() {
                    data.changePrices = '1';
                    scope.init(scope, data, 'post');
                    scope.removedCustomer = !1
                }, function(e) {
                    if (e == 'dismiss') {
                        data.buyer_id = scope.old_buyer_id;
                        var selectize = angular.element('#custandcont')[0].selectize;
                        selectize.addItem(scope.old_buyer_id, true);
                    }
                    scope.init(scope, data, 'post');
                    scope.removedCustomer = !1
                })
            } else if (scope.app == 'quote' && scope.item.quote_group && scope.item.quote_group.length && scope.item.quote_group[0].table.length && scope.item.quote_group[0].table[0].quote_line.length) {
                $confirm({
                    ok: helper.getLanguage('Yes'),
                    cancel: helper.getLanguage('No'),
                    text: helper.getLanguage('Recalculate prices')
                }).then(function() {
                    data.changePrices = '1';
                    scope.init(scope, data, 'post');
                    scope.removedCustomer = !1
                }, function(e) {
                    if (e == 'dismiss') {
                        data.buyer_id = scope.old_buyer_id;
                        var selectize = angular.element('#custandcont')[0].selectize;
                        selectize.addItem(scope.old_buyer_id, true);
                    }
                    scope.init(scope, data, 'post');
                    scope.removedCustomer = !1
                })
            } else {
                scope.init(scope, data, 'post')
            }
        }
    }

    function removeCust(scope) {
        scope.showTxt.address = !0;
        scope.item.add_customer = !0;
        scope.showTxt.addBtn = !0;
        scope.item.customer_id = '';
        scope.item.buyer_name = '';
        scope.item.customer_name = '';
        scope.item.main_address_id = '';
        scope.item.delivery_address = '';
        scope.item.delivery_address_id = '';
        scope.item.delivery_address_txt = '';
        scope.removedCustomer = !0;
        scope.item.buyer_id = '';
        scope.item.contact_id = '';
        scope.oldObj = {};
        if (scope.app == 'invoice') {
            if (scope.app_mod == 'recinvoice') {
                scope.item.do_next = scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-updateCustomerDataRec'
            } else if (scope.app_mod == 'incomminginvoice') {
                scope.item.do_next = scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-updateCustomerDataInc'
            } else if (scope.app_mod == 'sepa') {
                scope.item.do_next = scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-updateCustomerDataSepa';
                scope.item.bank_iban = '';
                scope.item.bank_bic_code = ''
            } else {
                scope.item.do_next = scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-updateCustomerData'
                scope.item.has_third_party = '';
                scope.item.third_party_id = '';
                scope.item.third_party_applied = '';
                scope.item.third_party_regime_id = '';
            }
            if (scope.type == '2' && !scope.c_invoice_id) {
                var data = {
                    'do': 'invoice-ninvoice',
                    'xget': 'invoices_list'
                };
                helper.doRequest('get', 'index.php', data, function(o) {
                    scope.auto.options.invoices = o.data;
                    scope.item.c_invoice_id = '';
                    scope.item.our_ref = '';
                    calcInvoiceTotal(scope.item)
                })
            }
        } else if (scope.app == 'project' || scope.app == 'maintenance') {
            scope.item.do_next = scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-updateCustomerData';
            if (scope.app == 'maintenance') {
                scope.showTxt.site = !1;
                scope.item.installation_id = undefined;
                var tmp_array = [];
                for (x in scope.item.installations) {
                    if (scope.item.installations[x].installation_id == '99999999999') {
                        tmp_array.push(scope.item.installations[x])
                    }
                }
                scope.item.installations = [];
                scope.item.installations = tmp_array
            }
        } else {
            scope.item.do_request = scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-updateCustomerData'
        }
    }

    function showEditFnc(scope) {
        scope.showTxt.address = !1;
        scope.item.save_final = !0;
        scope.oldObj = {
            buyer_id: angular.copy(scope.item.buyer_id),
            contact_id: angular.copy(scope.item.contact_id),
            contact_name: angular.copy(scope.item.contact_name),
            buyer_name: angular.copy(scope.item.buyer_name),
            customer_name: angular.copy(scope.item.customer_name),
            delivery_address: (scope.item.sameAddress ? !1 : angular.copy(scope.item.delivery_address)),
            delivery_address_id: (scope.item.sameAddress ? !1 : angular.copy(scope.item.delivery_address_id)),
            delivery_address_txt: (scope.item.sameAddress ? !1 : angular.copy(scope.item.delivery_address_txt)),
            view_delivery: (scope.item.sameAddress ? !1 : angular.copy(scope.item.view_delivery)),
            sameAddress: angular.copy(scope.item.sameAddress),
        }
    }

    function showCCSelectize(scope) {
        scope.showTxt.addBtn = !1;
        scope.item.save_final = !0;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }

    function cancelEdit(scope) {
        if (Object.keys(scope.oldObj).length == 0) {
            return scope.removeCust(scope)
        }
        scope.showTxt.address = !0;
        for (x in scope.oldObj) {
            if (scope.item[x]) {
                if (x == 'delivery_address') {
                    scope.item[x] = scope.oldObj[x].trim()
                } else {
                    scope.item[x] = scope.oldObj[x]
                }
            }
        }
        scope.item.buyer_id = scope.oldObj.buyer_id
    }

    function renderArticleListAuto(scope, d) {
        if (d.data) {
            scope.auto.options.articles_list = d.data.lines;
            var elem = angular.element('#article_list_id');
            if (elem[0]) {
                var selectize = elem[0].selectize;
                selectize.open()
            }
        }
    }

    function initP(scope, o, method) {
        //angular.element('.loading_wrap').removeClass('hidden');
        method = method || "get";
        helper.doRequest(method, 'index.php', o, scope.renderPage)
    }

    function postC(scope, o, method, formObj) {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest("post", 'index.php', o, function(r) {
            if (r.success) {
                scope.removedCustomer = !1;
                if (scope.app == 'maintenance' && r.data.service_id && r.data.service_id != 'tmp' && scope.item_id == 'tmp') {
                    /*if (scope.item.is_recurring) {
                        scope.goRecEdit(r.data.service_id)
                    } else {
                        scope.goEdit(r.data.service_id, r.data.customer_credit_limit)
                    }*/
                    scope.renderPage(r);
                } else if (scope.app == 'maintenance' && r.data.service_id && r.data.service_id == 'tmp' && scope.item_id == 'tmp') {
                    scope.renderPage(r);
                    //scope.saveData(scope.project_all);
                } else if (scope.app_mod == 'incomminginvoice' && scope.item.save_mode === !0) {
                    scope.renderPage(r);
                    scope.item.save_mode = !1;
                    //saveClientData(scope)
                } else {
                    scope.renderPage(r)
                }
            } else {
                scope.showTxt.address = !1;
                scope.showAlerts(r, formObj)
            }
        })
    }


    function addFacqLine(scope, data) {

        if (scope.item.quote_group[scope.quote_group_active].table.length == 0) {
            addQuoteArticle(scope, '', 7)
        } else if (scope.item.quote_group[scope.quote_group_active].table[0].quote_line.length == 0) {
            addQuoteArticle(scope, '', 7)
        }
        if (data) {

            var unique_id = helper.unique_id();
            var line = {
                "tr_id": unique_id,
                "line_type": data.line_type,
                "description": data.quoteformat,
                "quantity": helper.displayNr(data.quantity),
                "quantity_old": helper.displayNr(data.quantity),
                "article_code": data.code,
                "article_id": data.article_id,
                "is_tax": 0,
                "line_discount": helper.displayNr(data.line_discount),
                "sale_unit": data.sale_unit,
                "package": data.packing,
                "price": helper.displayNr(data.price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                'price_vat': scope.item.show_vat ? helper.displayNr(data.price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS) : helper.displayNr(data.price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                "line_total": helper.displayNr(data.price),
                "hide_currency2": "hide",
                "hide_currency1": "",
                "vat_val": data.vat_value,
                'line_vat': scope.item.show_vat ? helper.displayNr(data.vat) : helper.displayNr(0),
                'purchase_price': helper.displayNr(data.purchase_price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                "art_img": "",
                "show_block_total": scope.item.quote_group[scope.quote_group_active].show_block_total,
                "hide_QCv": "0",
                "hide_ACv": "0",
                "hide_UPv": "0",
                "hide_DISCv": "0",
                "show_lVATv": "0",
                'colum': (data.line_type == '2') ? (scope.item.colum * 1 + 2) : (scope.item.show_vat || scope.item.is_template ? scope.item.colum : scope.item.colum * 1 + 1),
                'has_variants': data.has_variants,
                'has_variants_done': data.has_variants_done,
                'is_variant_for': data.is_variant_for,
                'is_variant_for_line': data.is_variant_for_line,
                'variant_type': data.variant_type,
                'visible': 1,
                'is_combined': data.is_combined,
                'component_for': '',
                'has_taxes': data.has_taxes,
                'facq': 1,
            }

            tables = scope.item.quote_group[scope.quote_group_active].table.length;
            scope.item.quote_group[scope.quote_group_active].table[0].quote_line.push(line);

            scope.calcQuoteTotal(scope, 'price', tables - 1, line.tr_id)


        }
    }

    function addBarcodeLine(scope, lines, not_found) {
        angular.element('.loading_wrap').removeClass('hidden');

        if (lines.length) {
            helper.Loading++;

            var item = {
                article_id: lines[0].article_id,
                code: lines[0].code,
                ean_code: lines[0].ean_code,
                quoteformat: lines[0].quoteformat,
                name: lines[0].name,
                packing: lines[0].packing,
                sale_unit: lines[0].sale_unit,
                purchase_price: lines[0].purchase_price,
                vat: lines[0].vat,
                vat_value: lines[0].vat_value,
                price: lines[0].price,
                price_vat: lines[0].price_vat,
                quantity: lines[0].quantity,
                quantity_component: lines[0].quantity_component,
                line_discount: lines[0].line_discount,
                show_vat: 1,
                stock: lines[0].stock,
                threshold_value: lines[0].threshold_value,
                pending_articles: lines[0].pending_articles,
                new_stock: lines[0].new_stock,
                hide_stock: lines[0].hide_stock,
                colum: '',
                has_variants: lines[0].has_variants,
                has_variants_done: lines[0].has_variants_done,
                is_variant_for: lines[0].is_variant_for,
                is_variant_for_line: lines[0].is_variant_for_line,
                variant_type: lines[0].variant_type,
                is_combined: lines[0].is_combined,
                has_taxes: lines[0].has_taxes,
                visible: lines[0].visible,
                is_service: lines[0].is_service

            }

            $timeout(function() {
                addArticle(scope, item);
                lines.shift();
                return scope.addBarcodeLine(scope, lines, not_found);
            }, 300);
        } else if (not_found) {

            angular.element('.loading_wrap').addClass('hidden');
            helper.Loading = 0;
            scope.openModal(scope, 'scanbarWarning', not_found, 'md')

        } else {

            angular.element('.loading_wrap').addClass('hidden');
            helper.Loading = 0;
        }


    }

    function addArticle(scope, data, customIndex) {

        if (data) {
            var line = {
                    article: data.quoteformat,
                    article_code: data.code,
                    article_id: data.article_id,
                    colspan: "",
                    colum: data.article_id ? scope.item.colum : scope.item.colum + 2,
                    content: !1,
                    content_class: "",
                    disc: scope.item.discount_line_gen,
                    hide_stock: data.hide_stock,
                    is_article: data.article_id ? 1 : 0,
                    is_article_code: data.article_id ? true : !1,
                    is_tax: 0,
                    is_service: data.is_service,
                    line_total: "0,00",
                    packing: data.packing,
                    packing_x: data.packing,
                    purchase_price: data.purchase_price,
                    pending_articles: data.pending_articles,
                    percent: data.vat,
                    percent_x: helper.displayNr(data.vat),
                    price: helper.displayNr(data.price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                    price_vat: helper.displayNr(data.price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                    quantity: helper.displayNr(data.quantity),
                    quantity_old: helper.displayNr(data.quantity),
                    quantity_component: helper.displayNr(data.quantity_component),
                    sale_unit: data.sale_unit,
                    sale_unit_x: data.sale_unit,
                    stock: data.stock,
                    tax_for_article_id: "0",
                    threshold_value: data.threshold_value,
                    tr_id: "tmp" + (new Date().getTime()),
                    vat_value: data.vat_value,
                    vat_value_x: data.vat_value,
                    'has_variants': data.has_variants,
                    'has_variants_done': data.has_variants_done,
                    'is_variant_for': data.is_variant_for,
                    'is_variant_for_line': data.is_variant_for_line,
                    'variant_type': data.variant_type,
                    'is_combined': data.is_combined,
                    'visible': 1,
                    'has_taxes': data.has_taxes,
                    'show_stock_warning_line': 0
                },
                exchange = 0;

            if (scope.item.currency_type_val != scope.item.default_currency) {
                line.price = helper.displayNr(data.price / helper.return_value(scope.item.currency_rate), helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                exchange = 1
            }

            if (scope.app == "order" && scope.item.show_stock_warning && data.is_service == 0 && scope.item.allow_stock && parseFloat(data.stock) - data.quantity < parseFloat(data.threshold_value) && data.hide_stock != 1 && data.is_combined == '0') {
                line.show_stock_warning_line = 1;
            }

            if (data.quantity > 1) {
                var params = {
                    article_id: data.article_id,
                    price: data.price,
                    quantity: data.quantity,
                    customer_id: scope.item.buyer_id,
                    'do': 'order-order-order-get_article_quantity_price'
                };
                helper.doRequest('post', 'index.php', params, function(r) {
                    if (r.data) {
                        line.price = helper.displayNr(r.data.new_price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS)
                    }
                    if (customIndex) {
                        scope.item.tr_id.splice(customIndex, 0, line);
                    } else {
                        scope.item.tr_id.push(line);
                    }
                    calcTotal(scope.item, 'price', scope.item.tr_id.length - 1);
                    if (data.has_variants == true || data.has_variants == '1') {
                        for (x in scope.item.tr_id) {
                            if (scope.item.tr_id[x].is_variant_for > 0 && scope.item.tr_id[x].variant_type > 0) {
                                var tmp_data = {
                                    'do': 'misc-articleVariants',
                                    'parentVariant': data.article_id,
                                    'is_variant_for_line': scope.item.tr_id[x].tr_id,
                                    buyer_id: scope.item.buyer_id,
                                    lang_id: scope.item.email_language,
                                    cat_id: scope.item.price_category_id,
                                    remove_vat: scope.item.remove_vat,
                                    vat_regime_id: scope.item.vat_regime_id,
                                    variant_type_search: scope.item.tr_id[x].variant_type,
                                    app: scope.app
                                };
                                helper.doRequest('get', 'index.php', tmp_data, function(o) {
                                    if (o.data.lines.length) {
                                        var actual_index = scope.item.tr_id.length - 1;
                                        scope.item.tr_id[actual_index].has_variants_done = '1';
                                        addArticle(scope, o.data.lines[0]);
                                    }
                                });
                                break;
                            }
                        }
                    }
                    var params = {
                        article_id: data.article_id,
                        quantity: data.quantity,
                        exchange: exchange,
                        ex_rate: scope.item.currency_rate,
                        allow_article_sale_unit: scope.item.allow_article_sale_unit,
                        allow_article_packing: scope.item.allow_article_packing,
                        remove_vat: scope.item.remove_vat,
                        apply_discount: scope.item.apply_discount,
                        vat_regime_id: scope.item.vat_regime_id,
                        'do': 'misc-addArticle',
                        xget: 'taxes'
                    };
                    helper.doRequest('get', 'index.php', params, function(res) {
                        if (res.data && res.data.lines) {
                            var d = res.data.lines
                            for (x in d) {
                                var show_line = !1;
                                if (scope.app == 'invoice' || scope.app == 'order' || scope.app == 'quote' || scope.app == 'maintenance') {
                                    if (d[x].apply_to == '0' || d[x].apply_to == '1') {
                                        show_line = !0
                                    }
                                } else if (scope.app == 'po_order') {
                                    if (d[x].apply_to == '0' || d[x].apply_to == '2') {
                                        show_line = !0
                                    }
                                }
                                var taxLine = {
                                    article: d[x].tax_name,
                                    article_code: d[x].tax_code,
                                    quantity: d[x].quantity,
                                    quantity_old: d[x].quantity_old,
                                    article_id: d[x].tax_id,
                                    is_tax: 1,
                                    tax_for_article_id: d[x].tax_for_article_id,
                                    vat_percent: d[x].percent,
                                    vat_value: d[x].vat_value,
                                    packing: 1,
                                    packing_x: 1,
                                    stock: d[x].stock,
                                    content: !1,
                                    price: d[x].price,
                                    purchase_price: 0,
                                    percent: d[x].percent,
                                    percent_x: d[x].percent_x,
                                    price_vat: d[x].price_vat,
                                    sale_unit: 1,
                                    sale_unit_x: 1,
                                    disc: "0,00",
                                    colum: scope.item.colum,
                                    line_total: "0,00",
                                    for_article: line.tr_id,
                                    tr_id: "tmp" + (new Date().getTime()),
                                    apply_to: d[x].apply_to,
                                    show_line: show_line
                                }
                                if (show_line) {
                                    if (!data.has_variants || data.has_variants == '0') {
                                        if (customIndex) {
                                            scope.item.tr_id.splice(customIndex, 0, taxLine);
                                        } else {
                                            scope.item.tr_id.push(taxLine);
                                        }
                                    }
                                }
                                calcTotal(scope.item, 'price', scope.item.tr_id.length - 1)
                            }
                        }
                        if (scope.app == "order" && data.show_stock_warning && data.is_service == 0 && data.allow_stock && helper.return_value(data.stock2) - data.quantity < parseFloat(data.threshold_value) && data.hide_stock != 1 && data.is_combined == '0') {
                            //scope.openModal(scope, 'stockWarning', data, 'sm')
                        }
                    })

                    //add components
                    if (data.article_id) {
                        var params = {
                            article_id: data.article_id,
                            quantity: data.quantity,
                            exchange: exchange,
                            ex_rate: scope.item.currency_rate,
                            allow_article_sale_unit: scope.item.allow_article_sale_unit,
                            allow_article_packing: scope.item.allow_article_packing,
                            remove_vat: 1,
                            apply_discount: scope.item.apply_discount,
                            vat_regime_id: scope.item.vat_regime_id,
                            'do': 'misc-addArticle',
                            xget: 'components'
                        };
                        helper.doRequest('get', 'index.php', params, function(res) {
                            if (res.data && res.data.components) {
                                var d = res.data.components
                                for (x in d) {
                                    var show_line = !1;
                                    if (scope.app == 'invoice' || scope.app == 'order' || scope.app == 'quote' || scope.app == 'maintenance') {
                                        if (d[x].apply_to == '0' || d[x].apply_to == '1') {
                                            show_line = !0
                                        }
                                    } else if (scope.app == 'po_order') {
                                        if (d[x].apply_to == '0' || d[x].apply_to == '2') {
                                            show_line = !0
                                        }
                                    }

                                    show_line = 1;
                                    var componentLine = {
                                        article: d[x].component_name,
                                        article_code: d[x].component_code,
                                        quantity: d[x].quantity,
                                        quantity_old: d[x].quantity_old,
                                        quantity_component: d[x].quantity_component,
                                        article_id: d[x].component_article_id,
                                        component_id: d[x].component_id,
                                        is_tax: 0,
                                        is_article_code: true,
                                        is_component: 1,
                                        parent_article_id: d[x].parent_article_id,
                                        visible: d[x].visible,
                                        vat_percent: '',
                                        vat_value: '',
                                        packing: 1,
                                        packing_x: 1,
                                        stock: d[x].stock,
                                        content: !1,
                                        price: 0,
                                        purchase_price: d[x].purchase_price,
                                        percent: line.percent,
                                        percent_x: line.percent_x,
                                        price_vat: 0,
                                        sale_unit: 1,
                                        sale_unit_x: 1,
                                        disc: "0,00",
                                        colum: scope.item.colum,
                                        line_total: "0,00",
                                        component_for: line.tr_id,
                                        tr_id: "tmp" + (new Date().getTime()),
                                        apply_to: d[x].apply_to,
                                        show_line: 1
                                    }
                                    if (show_line) {
                                        scope.item.tr_id.push(componentLine);
                                        scope.app == 'po_order' ? scope.calcTotalPurchase() : scope.calcTotal()
                                    }
                                }

                            }
                        })
                    }


                })
            } else {
                if (customIndex) {
                    scope.item.tr_id.splice(customIndex, 0, line);
                } else {
                    scope.item.tr_id.push(line);
                }
                calcTotal(scope.item);
                if (data.has_variants == true || data.has_variants == '1') {
                    for (x in scope.item.tr_id) {
                        if (scope.item.tr_id[x].is_variant_for > 0 && scope.item.tr_id[x].variant_type > 0) {
                            var tmp_data = {
                                'do': 'misc-articleVariants',
                                'parentVariant': data.article_id,
                                'is_variant_for_line': scope.item.tr_id[x].tr_id,
                                buyer_id: scope.item.buyer_id,
                                lang_id: scope.item.email_language,
                                cat_id: scope.item.cat_id,
                                remove_vat: (scope.item.show_vat ? '0' : '1'),
                                vat_regime_id: scope.item.vat_regime_id,
                                variant_type_search: scope.item.tr_id[x].variant_type,
                                app: scope.app
                            };
                            helper.doRequest('get', 'index.php', tmp_data, function(o) {
                                if (o.data.lines.length) {
                                    var actual_index = scope.item.tr_id.length - 1;
                                    scope.item.tr_id[actual_index].has_variants_done = '1';
                                    addArticle(scope, o.data.lines[0]);
                                }
                            });
                            break;
                        }
                    }
                }
                var params = {
                    article_id: data.article_id,
                    quantity: data.quantity,
                    exchange: exchange,
                    ex_rate: scope.item.currency_rate,
                    allow_article_sale_unit: scope.item.allow_article_sale_unit,
                    allow_article_packing: scope.item.allow_article_packing,
                    remove_vat: scope.item.remove_vat,
                    apply_discount: scope.item.apply_discount,
                    vat_regime_id: scope.item.vat_regime_id,
                    'do': 'misc-addArticle',
                    xget: 'taxes'
                };
                helper.doRequest('get', 'index.php', params, function(res) {
                    if (res.data && res.data.lines) {
                        var d = res.data.lines
                        for (x in d) {
                            var show_line = !1;
                            if (scope.app == 'invoice' || scope.app == 'order' || scope.app == 'quote' || scope.app == 'maintenance') {
                                if (d[x].apply_to == '0' || d[x].apply_to == '1') {
                                    show_line = !0
                                }
                            } else if (scope.app == 'po_order') {
                                if (d[x].apply_to == '0' || d[x].apply_to == '2') {
                                    show_line = !0
                                }
                            }
                            var taxLine = {
                                article: d[x].tax_name,
                                article_code: d[x].tax_code,
                                quantity: d[x].quantity,
                                quantity_old: d[x].quantity_old,
                                article_id: d[x].tax_id,
                                is_tax: 1,
                                tax_for_article_id: d[x].tax_for_article_id,
                                vat_percent: d[x].vat,
                                vat_value: d[x].vat_value,
                                packing: 1,
                                packing_x: 1,
                                stock: d[x].stock,
                                content: !1,
                                price: d[x].price,
                                purchase_price: 0,
                                percent: d[x].percent,
                                percent_x: d[x].percent_x,
                                price_vat: d[x].price_vat,
                                sale_unit: 1,
                                sale_unit_x: 1,
                                disc: "0,00",
                                colum: scope.item.colum,
                                line_total: "0,00",
                                for_article: line.tr_id,
                                tr_id: "tmp" + (new Date().getTime()),
                                apply_to: d[x].apply_to,
                                show_line: show_line,
                                visible: 1,
                            }
                            if (show_line) {
                                if (!data.has_variants || data.has_variants == '0') {
                                    if (customIndex) {
                                        scope.item.tr_id.splice(customIndex, 0, taxLine);
                                    } else {
                                        scope.item.tr_id.push(taxLine);
                                    }
                                }
                            }
                            calcTotal(scope.item)
                        }
                    }
                    if (scope.app == "order" && data.show_stock_warning && data.is_service == 0 && data.allow_stock && helper.return_value(data.stock2) - data.quantity < parseFloat(data.threshold_value) && data.hide_stock != 1 && data.is_combined == '0') {
                        //scope.openModal(scope, 'stockWarning', data, 'sm')
                    }
                })

                //add components
                if (data.article_id) {
                    var params = {
                        article_id: data.article_id,
                        quantity: data.quantity,
                        exchange: exchange,
                        ex_rate: scope.item.currency_rate,
                        allow_article_sale_unit: scope.item.allow_article_sale_unit,
                        allow_article_packing: scope.item.allow_article_packing,
                        remove_vat: scope.item.remove_vat,
                        apply_discount: scope.item.apply_discount,
                        vat_regime_id: scope.item.vat_regime_id,
                        'do': 'misc-addArticle',
                        xget: 'components'
                    };
                    helper.doRequest('get', 'index.php', params, function(res) {
                        if (res.data && res.data.components) {
                            var d = res.data.components
                            for (x in d) {
                                var show_line = !1;
                                if (scope.app == 'invoice' || scope.app == 'order' || scope.app == 'quote' || scope.app == 'maintenance') {
                                    if (d[x].apply_to == '0' || d[x].apply_to == '1') {
                                        show_line = !0
                                    }
                                } else if (scope.app == 'po_order') {
                                    if (d[x].apply_to == '0' || d[x].apply_to == '2') {
                                        show_line = !0
                                    }
                                }

                                show_line = 1; //de test
                                var componentLine = {
                                    article: d[x].component_name,
                                    article_code: d[x].component_code,
                                    quantity: d[x].quantity,
                                    quantity_old: d[x].quantity_old,
                                    quantity_component: d[x].quantity_component,
                                    article_id: d[x].component_article_id,
                                    component_id: d[x].component_id,
                                    is_tax: 0,
                                    is_component: 1,
                                    is_article_code: true,
                                    parent_article_id: d[x].parent_article_id,
                                    visible: d[x].visible,
                                    vat_percent: 0,
                                    vat_value: 0,
                                    packing: 1,
                                    packing_x: 1,
                                    stock: d[x].stock,
                                    content: !1,
                                    price: 0,
                                    purchase_price: d[x].purchase_price,
                                    percent: line.percent,
                                    percent_x: line.percent_x,
                                    price_vat: 0,
                                    sale_unit: 1,
                                    sale_unit_x: 1,
                                    disc: "0,00",
                                    colum: scope.item.colum,
                                    line_total: "0,00",
                                    component_for: line.tr_id,
                                    tr_id: "tmp" + (new Date().getTime()),
                                    apply_to: d[x].apply_to,
                                    show_line: 1
                                }
                                if (show_line) {
                                    scope.item.tr_id.push(componentLine);
                                    scope.app == 'po_order' ? scope.calcTotalPurchase() : scope.calcTotal()
                                }
                            }
                        }
                    })
                }


                //console.log(scope.item.tr_id);
            }
        }
    }

    function addPurchaseOrderArticle(scope, data, customIndex) {
        if (data) {
            var line = {
                    article: data.quoteformat,
                    article_code: data.code,
                    article_id: data.article_id,
                    colspan: "",
                    colum: data.article_id ? scope.item.colum : (scope.item.hide_disc ? 6 : 7),
                    content: !1,
                    content_class: "",
                    disc: scope.item.discount_line_gen,
                    hide_stock: data.hide_stock,
                    is_article: data.article_id ? 1 : 0,
                    is_article_code: !0,
                    is_tax: 0,
                    line_total: "0,00",
                    packing: data.packing,
                    packing_x: data.packing,
                    pending_articles: data.pending_articles,
                    percent: data.vat,
                    percent_x: helper.displayNr(data.vat),
                    price: helper.displayNr(data.purchase_price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                    price_vat: helper.displayNr(data.price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                    purchase_price: data.purchase_price,
                    quantity: helper.displayNr(data.quantity),
                    quantity_old: helper.displayNr(data.quantity),
                    sale_unit: data.sale_unit,
                    sale_unit_x: data.sale_unit,
                    supplier_reference: data.supplier_reference_x,
                    stock: data.stock,
                    tax_for_article_id: "0",
                    threshold_value: data.threshold_value,
                    tr_id: "tmp" + (new Date().getTime()),
                    vat_value: data.vat_value,
                    vat_value_x: data.vat_value,
                    'has_variants': data.has_variants,
                    'has_variants_done': data.has_variants_done,
                    'is_variant_for': data.is_variant_for,
                    'is_variant_for_line': data.is_variant_for_line,
                    'variant_type': data.variant_type
                },
                exchange = 0;
            if (scope.item.currency_type_val != scope.item.default_currency) {
                line.price = helper.displayNr(data.price / helper.return_value(scope.item.currency_rate), helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                exchange = 1
            }
            if (customIndex) {
                scope.item.tr_id.splice(customIndex, 0, line);
            } else {
                scope.item.tr_id.push(line);
            }
            calcTotalPurchase(scope.item);
            if (data.has_variants == true || data.has_variants == '1') {
                for (x in scope.item.tr_id) {
                    if (scope.item.tr_id[x].is_variant_for > 0 && scope.item.tr_id[x].variant_type > 0) {
                        var tmp_data = {
                            'do': 'misc-articleVariants',
                            'parentVariant': data.article_id,
                            'is_variant_for_line': scope.item.tr_id[x].tr_id,
                            buyer_id: scope.item.buyer_id,
                            lang_id: scope.item.email_language,
                            cat_id: scope.item.price_category_id,
                            remove_vat: scope.item.remove_vat,
                            variant_type_search: scope.item.tr_id[x].variant_type,
                            app: scope.app
                        };
                        helper.doRequest('get', 'index.php', tmp_data, function(o) {
                            if (o.data.lines.length) {
                                var actual_index = scope.item.tr_id.length - 1;
                                scope.item.tr_id[actual_index].has_variants_done = '1';
                                addPurchaseOrderArticle(scope, o.data.lines[0]);
                            }
                        });
                        break;
                    }
                }
            }
            var params = {
                article_id: data.article_id,
                quantity: data.quantity,
                exchange: exchange,
                ex_rate: scope.item.currency_rate,
                allow_article_sale_unit: scope.item.allow_article_sale_unit,
                allow_article_packing: scope.item.allow_article_packing,
                remove_vat: scope.item.remove_vat,
                apply_discount: scope.item.apply_discount,
                'do': 'misc-addArticle',
                xget: 'taxes'
            };
            helper.doRequest('get', 'index.php', params, function(res) {
                if (res.data && res.data.lines) {
                    var d = res.data.lines
                    for (x in d) {
                        var show_line = !1;
                        if (scope.app == 'po_order') {
                            if (d[x].apply_to == '0' || d[x].apply_to == '2') {
                                show_line = !0
                            }
                        }
                        var taxLine = {
                            article: d[x].tax_name,
                            article_code: d[x].tax_code,
                            quantity: d[x].quantity,
                            quantity_old: d[x].quantity_old,
                            article_id: d[x].tax_id,
                            is_tax: 1,
                            tax_for_article_id: d[x].tax_for_article_id,
                            vat_percent: d[x].vat,
                            vat_value: d[x].vat_value,
                            packing: 1,
                            packing_x: 1,
                            stock: d[x].stock,
                            content: !1,
                            price: d[x].price,
                            purchase_price: 0,
                            percent: d[x].percent,
                            percent_x: d[x].percent_x,
                            price_vat: d[x].price_vat,
                            sale_unit: 1,
                            sale_unit_x: 1,
                            disc: "0,00",
                            colum: scope.item.colum,
                            line_total: "0,00",
                            for_article: line.tr_id,
                            tr_id: "tmp" + (new Date().getTime()),
                            apply_to: d[x].apply_to,
                            show_line: show_line
                        }
                        if (show_line) {
                            if (!data.has_variants || data.has_variants == '0') {
                                if (customIndex) {
                                    scope.item.tr_id.splice(customIndex, 0, taxLine)
                                } else {
                                    scope.item.tr_id.push(taxLine)
                                }
                            }
                        }
                        calcTotalPurchase(scope.item)
                    }
                }
            })
        }
    }

    function addQuoteArticle(scope, data, isFreeTxt, customIndex) {
        if (scope.app == 'quote' && !scope.app_mod && $rootScope.edited_data.indexOf('quote_edit') === -1) {
            $rootScope.edited_data.push('quote_edit');
        }
        helper.changedData();
        if (data) {
            if (scope.item.quote_group[scope.quote_group_active].table.length == 0) {
                addQuoteArticle(scope, '', 7)
            } else {
                if (scope.item.quote_group[scope.quote_group_active].table[0].quote_line.length == 0) {
                    addQuoteArticle(scope, '', 7)
                } else if (scope.item.quote_group[scope.quote_group_active].table[0].quote_line.length > 0) {
                    var tableHeaderExist = !1;
                    angular.forEach(scope.item.quote_group[scope.quote_group_active].table[0].quote_line, function(value, key) {
                        if (value.description == 'talbe header') {
                            tableHeaderExist = !0
                        }
                    });
                    if (!tableHeaderExist) {
                        addQuoteArticle(scope, '', 7)
                    }
                }
            }
            //console.log(data);
            var unique_id = helper.unique_id();
            var line = {
                    "tr_id": unique_id,
                    "line_type": "1",
                    "description": data.quoteformat,
                    "quantity": helper.displayNr(data.quantity),
                    "quantity_old": helper.displayNr(data.quantity),
                    "article_code": data.code,
                    "article_id": data.article_id,
                    "is_tax": 0,
                    "line_discount": helper.displayNr(data.line_discount),
                    "sale_unit": data.sale_unit,
                    "package": data.packing,
                    "price": helper.displayNr(data.price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                    'price_vat': scope.item.show_vat ? helper.displayNr(data.price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS) : helper.displayNr(data.price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                    "line_total": helper.displayNr(data.price),
                    "hide_currency2": "hide",
                    "hide_currency1": "",
                    "vat_val": data.vat_value,
                    'line_vat': scope.item.show_vat ? helper.displayNr(data.vat) : helper.displayNr(0),
                    'purchase_price': helper.displayNr(data.purchase_price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                    "art_img": "",
                    "show_block_total": scope.item.quote_group[scope.quote_group_active].show_block_total,
                    "hide_QCv": "0",
                    "hide_ACv": "0",
                    "hide_UPv": "0",
                    "hide_DISCv": "0",
                    "show_lVATv": "0",
                    'colum': (scope.item.show_vat || scope.item.is_template) ? scope.item.colum : scope.item.colum * 1 + 1,
                    'has_variants': data.has_variants,
                    'has_variants_done': data.has_variants_done,
                    'is_variant_for': data.is_variant_for,
                    'is_variant_for_line': data.is_variant_for_line,
                    'variant_type': data.variant_type,
                    'visible': 1,
                    'is_combined': data.is_combined,
                    'component_for': '',
                    'has_taxes': data.has_taxes,
                },
                exchange = 0,
                tables = scope.item.quote_group[scope.quote_group_active].table.length;

            // console.log(line);
            if (scope.item.currency_type != scope.item.default_currency) {
                line.price = helper.displayNr(data.price / helper.return_value(scope.item.currency_rate), helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                exchange = 1
            }
            if (tables > 0) {
                if (scope.app == 'quote') {
                    if (data.quantity > 1) {
                        var params = {
                            article_id: data.article_id,
                            price: data.price,
                            quantity: data.quantity,
                            customer_id: scope.item.buyer_id,
                            'do': 'quote--quote-get_article_quantity_price'
                        };
                        helper.doRequest('post', 'index.php', params, function(r) {
                            if (r.data) {
                                line.price = helper.displayNr(r.data.new_price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS)
                            }
                            if (customIndex) {
                                scope.item.quote_group[scope.quote_group_active].table[0].quote_line.splice(customIndex, 0, line);
                            } else {
                                scope.item.quote_group[scope.quote_group_active].table[0].quote_line.push(line);
                            }
                            scope.calcQuoteTotal(scope, 'price', tables - 1, line.tr_id)
                        })
                    } else {
                        if (customIndex) {
                            scope.item.quote_group[scope.quote_group_active].table[0].quote_line.splice(customIndex, 0, line);
                        } else {
                            scope.item.quote_group[scope.quote_group_active].table[0].quote_line.push(line);
                        }
                        if (data.has_variants == true || data.has_variants == '1') {
                            for (x in scope.item.quote_group[scope.quote_group_active].table[0].quote_line) {
                                if (scope.item.quote_group[scope.quote_group_active].table[0].quote_line[x].is_variant_for > 0 && scope.item.quote_group[scope.quote_group_active].table[0].quote_line[x].variant_type > 0) {
                                    var tmp_data = {
                                        'do': 'misc-articleVariants',
                                        'parentVariant': data.article_id,
                                        'is_variant_for_line': 5555,
                                        buyer_id: scope.item.buyer_id,
                                        lang_id: scope.item.email_language,
                                        cat_id: scope.item.cat_id,
                                        remove_vat: (scope.item.show_vat ? '0' : '1'),
                                        vat_regime_id: scope.item.vat_regime_id,
                                        variant_type_search: scope.item.quote_group[scope.quote_group_active].table[0].quote_line[x].variant_type,
                                        app: scope.app
                                    };
                                    helper.doRequest('get', 'index.php', tmp_data, function(o) {
                                        if (o.data.lines.length) {
                                            var actual_index = scope.item.quote_group[scope.quote_group_active].table[0].quote_line.length - 1;
                                            scope.item.quote_group[scope.quote_group_active].table[0].quote_line[actual_index].has_variants_done = '1';
                                            addQuoteArticle(scope, o.data.lines[0]);
                                        }
                                    });
                                    break;
                                }
                            }
                        }
                    }
                    scope.checkVariants();
                } else {
                    scope.item.quote_group[scope.quote_group_active].table[0].quote_line.push(line)
                }
            } else {
                var tbl = {
                    quote_line: [line],
                    "line_type": "1",
                    "group_type": "1",
                    total: helper.displayNr(0),
                    "group_table_id": helper.unique_id(),
                    "total_vat": 0,
                    "total_vat_span": helper.displayNr(0),
                    "q_table": !0,
                    "ONLY_ARTICLE": "",
                    "hide_not_service": "hide",
                    "USE_SALE_UNIT": (scope.item.ALLOW_QUOTE_SALE_UNIT == 1 ? !0 : !1),
                    "USE_PACKAGE": (scope.item.ALLOW_QUOTE_PACKING == 1 ? !0 : !1),
                    "VIEW_DISCOUNT": (scope.item.apply_discount == '1' || scope.item.apply_discount == '3' ? !0 : !1),
                    "VIEW_VAT": "hide",
                    "hide_currency2": "hide",
                    "hide_currency1": "",
                    "block_total_hide": "",
                    "checkedBAD": "",
                    "checkedQC": "",
                    "checkedAC": "",
                    "checkedUPC": "",
                    "checkedDISC": "",
                    "hide_QC": "",
                    "hide_AC": "",
                    "hide_UP": "",
                    "hide_DISC": "",
                    "show_lVAT": "hide",
                    "checkedVAT": "",
                    "sort_order": "2"
                }
                scope.item.quote_group[scope.quote_group_active].table.push(tbl);
                tables = scope.item.quote_group[scope.quote_group_active].table.length
            }
            scope.calcQuoteTotal(scope, 'price', tables - 1, line.tr_id);
            if (scope.app == 'contract') {
                var params = {
                    article_id: data.article_id,
                    quantity: data.quantity,
                    exchange: exchange,
                    ex_rate: scope.item.currency_rate,
                    allow_article_sale_unit: scope.item.ALLOW_QUOTE_SALE_UNIT,
                    allow_article_packing: scope.item.ALLOW_QUOTE_PACKING,
                    remove_vat: (scope.item.show_vat ? '0' : '1'),
                    apply_discount: scope.item.apply_discount,
                    vat_regime_id: scope.item.vat_regime_id,
                    'do': 'contract-contract',
                    xget: 'taxes',
                    customer_id: scope.item.buyer_id
                }
            } else {
                var params = {
                    article_id: data.article_id,
                    quantity: data.quantity,
                    exchange: exchange,
                    ex_rate: scope.item.currency_rate,
                    allow_article_sale_unit: scope.item.ALLOW_QUOTE_SALE_UNIT,
                    allow_article_packing: scope.item.ALLOW_QUOTE_PACKING,
                    remove_vat: (scope.item.show_vat ? '0' : '1'),
                    apply_discount: scope.item.apply_discount,
                    vat_regime_id: scope.item.vat_regime_id,
                    'do': 'quote-nquote',
                    xget: 'taxes',
                    customer_id: scope.item.buyer_id
                }
            }
            helper.doRequest('get', 'index.php', params, function(res) {
                if (res.data && res.data.lines) {
                    var d = res.data.lines
                    for (x in d) {
                        var show_line = !1;
                        if (scope.app == 'quote') {
                            if (d[x].apply_to == '0' || d[x].apply_to == '1') {
                                show_line = !0
                            }
                        }
                        var taxLine = {
                            "tr_id": helper.unique_id(),
                            "line_type": "1",
                            "description": d[x].tax_name,
                            "quantity": d[x].quantity,
                            "quantity_old": d[x].quantity,
                            "article_code": d[x].tax_code,
                            "article_id": d[x].tax_id,
                            "is_tax": 1,
                            "line_discount": helper.displayNr(0),
                            "line_vat": d[x].percent_x,
                            "sale_unit": 1,
                            "package": 1,
                            "price": d[x].price,
                            'price_vat': d[x].price_vat,
                            "line_total": d[x].line_total,
                            "hide_currency2": "hide",
                            "hide_currency1": "",
                            "vat_val": data.vat_value,
                            "art_img": "",
                            "show_block_total": "0",
                            "hide_QCv": "0",
                            "hide_ACv": "0",
                            "hide_UPv": "0",
                            "hide_DISCv": "0",
                            "show_lVATv": "0",
                            'colum': scope.item.colum,
                            'show_line': show_line,
                            'visible': 1,
                            'for_article': unique_id,
                        }
                        if (show_line) {
                            if (!data.has_variants || data.has_variants == '0') {
                                if (customIndex) {
                                    scope.item.quote_group[scope.quote_group_active].table[0].quote_line.splice(customIndex + 1, 0, taxLine);
                                } else {
                                    scope.item.quote_group[scope.quote_group_active].table[0].quote_line.push(taxLine);
                                }
                                scope.is_add_article = !0;
                                scope.calcQuoteTotal(scope)
                            }
                        }
                    }
                }
            });

            //add components
            var params = {
                article_id: data.article_id,
                quantity: data.quantity,
                exchange: exchange,
                ex_rate: scope.item.currency_rate,
                allow_article_sale_unit: scope.item.ALLOW_QUOTE_SALE_UNIT,
                allow_article_packing: scope.item.ALLOW_QUOTE_PACKING,
                remove_vat: (scope.item.show_vat ? '0' : '1'),
                apply_discount: scope.item.apply_discount,
                vat_regime_id: scope.item.vat_regime_id,
                'do': 'quote-nquote',
                xget: 'components',
                customer_id: scope.item.buyer_id
            }
            helper.doRequest('get', 'index.php', params, function(res) {
                if (res.data && res.data.components) {
                    var d = res.data.components
                    for (x in d) {
                        var show_line = !1;
                        if (scope.app == 'quote') {
                            if (d[x].apply_to == '0' || d[x].apply_to == '1') {
                                show_line = !0
                            }
                        }
                        show_line = !0;
                        var componentLine = {
                            "tr_id": helper.unique_id(),
                            "line_type": "1",
                            "description": d[x].component_name,
                            "quantity": d[x].quantity,
                            "quantity_old": d[x].quantity,
                            "quantity_component": d[x].quantity_component,
                            "article_code": d[x].component_code,
                            "article_id": d[x].component_article_id,
                            "is_tax": 0,
                            "is_article_code": true,
                            "is_component": 1,
                            "line_discount": helper.displayNr(0),
                            "line_vat": 0,
                            "sale_unit": 1,
                            "package": 1,
                            "price": 0,
                            'price_vat': 0,
                            'purchase_price': helper.displayNr(d[x].purchase_price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                            "line_total": 0,
                            "hide_currency2": "hide",
                            "hide_currency1": "",
                            "vat_val": data.vat_value,
                            "art_img": "",
                            "show_block_total": "0",
                            "hide_QCv": "0",
                            "hide_ACv": "0",
                            "hide_UPv": "0",
                            "hide_DISCv": "0",
                            "show_lVATv": "0",
                            'colum': scope.item.colum,
                            'show_line': show_line,
                            'visible': d[x].visible,
                            'component_for': unique_id,
                        }
                        if (show_line) {
                            scope.item.quote_group[scope.quote_group_active].table[0].quote_line.push(componentLine);

                            scope.is_add_article = !0;
                            scope.calcQuoteTotal(scope)

                        }
                        //console.log(scope.item.quote_group[scope.quote_group_active].table[0].quote_line);
                    }
                }
            });

            if (scope.typePromise) {
                $timeout.cancel(scope.typePromise)
            }
            scope.typePromise = $timeout(function() {
                //scope.save()
            }, 2000)
        } else {
            var description = '';
            var colum = scope.item.colum * 1 + 2;
            if (isFreeTxt == 6) {
                description = '<br>'
            }
            if (isFreeTxt == 7) {
                description = 'talbe header';
                colum = scope.item.show_vat ? scope.item.colum * 1 : scope.item.colum * 1 + 1
            }
            if (isFreeTxt == 8) {
                description = helper.getLanguage('Amount Due');
            }
            var line = {
                    "tr_id": helper.unique_id(),
                    "line_type": (isFreeTxt),
                    "description": description,
                    "quantity": helper.displayNr(1),
                    "article_code": '',
                    "article_id": 0,
                    "is_tax": 0,
                    "line_discount": scope.item.discount_line_gen,
                    "sale_unit": 1,
                    "package": 1,
                    "price": helper.displayNr(0, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                    'price_vat': helper.displayNr(0, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                    "line_total": helper.displayNr(0),
                    "hide_currency2": "hide",
                    "hide_currency1": "",
                    "vat_val": 0,
                    'line_vat': (isFreeTxt == 2 ? scope.item.vat : 0),
                    "art_img": "",
                    "show_block_total": "0",
                    "hide_QCv": "0",
                    "hide_ACv": "0",
                    "hide_UPv": "0",
                    "hide_DISCv": "0",
                    "show_lVATv": "0",
                    'colum': colum,
                    'visible': 1,
                },
                tables = scope.item.quote_group[scope.quote_group_active].table.length;
            if (tables > 0) {
                scope.item.quote_group[scope.quote_group_active].table[0].quote_line.push(line)
            } else {
                var tbl = {
                    quote_line: [line],
                    "line_type": "1",
                    "group_type": "1",
                    total: helper.displayNr(0),
                    "group_table_id": helper.unique_id(),
                    "total_vat": 0,
                    "total_vat_span": helper.displayNr(0),
                    "q_table": !0,
                    "ONLY_ARTICLE": "",
                    "hide_not_service": "hide",
                    "USE_SALE_UNIT": (scope.item.ALLOW_QUOTE_SALE_UNIT == 1 ? !0 : !1),
                    "USE_PACKAGE": (scope.item.ALLOW_QUOTE_PACKING == 1 ? !0 : !1),
                    "VIEW_DISCOUNT": (scope.item.apply_discount == '1' || scope.item.apply_discount == '3' ? !0 : !1),
                    "VIEW_VAT": "hide",
                    "hide_currency2": "hide",
                    "hide_currency1": "",
                    "block_total_hide": "",
                    "checkedBAD": "",
                    "checkedQC": "",
                    "checkedAC": "",
                    "checkedUPC": "",
                    "checkedDISC": "",
                    "hide_QC": "",
                    "hide_AC": "",
                    "hide_UP": "",
                    "hide_DISC": "",
                    "show_lVAT": "hide",
                    "checkedVAT": "",
                    "sort_order": "2",
                    'visible': 1,
                }
                scope.item.quote_group[scope.quote_group_active].table.push(tbl);
                tables = scope.item.quote_group[scope.quote_group_active].table.length
            }
            scope.calcQuoteTotal(scope);
        }
        if(isFreeTxt != 7 && scope.app == 'quote'){
            var chapterSubtotalExists = false;
            for (x in scope.item.quote_group[scope.quote_group_active].table[0].quote_line) {
                if (parseInt(scope.item.quote_group[scope.quote_group_active].table[0].quote_line[x].line_type) == 8) {
                    chapterSubtotalExists = true;
                    break;
                }
            }
            if (!chapterSubtotalExists && scope.item.quote_group[scope.quote_group_active].show_chapter_total) {
                addQuoteArticle(scope, '', 8);
            }
        }
        if (scope.typePromise) {
            $timeout.cancel(scope.typePromise)
        }
        scope.typePromise = $timeout(function() {
            //scope.save()
        }, 2000)
    }

    function addQuoteContent(scope, data, isFreeTxt) {
        if (scope.app == 'quote' && !scope.app_mod && $rootScope.edited_data.indexOf('quote_edit') === -1) {
            $rootScope.edited_data.push('quote_edit');
        }
        var quoteElements = [3, 4, 5, 6];
        var existInElements = quoteElements.indexOf(isFreeTxt);
        if (isFreeTxt != 7 && scope.item.ADV_QUOTE) {
            if (scope.item.quote_group[scope.quote_group_active].table.length == 0) {
                if (existInElements == -1) {
                    addQuoteArticle(scope, '', 7)
                }
            } else {
                if (scope.item.quote_group[scope.quote_group_active].table[0].quote_line.length == 0) {
                    if (existInElements == -1) {
                        addQuoteArticle(scope, '', 7)
                    }
                } else if (scope.item.quote_group[scope.quote_group_active].table[0].quote_line.length > 0 && existInElements == -1) {
                    var tableHeaderExist = !1;
                    angular.forEach(scope.item.quote_group[scope.quote_group_active].table[0].quote_line, function(value, key) {
                        if (value.description == 'talbe header') {
                            tableHeaderExist = !0
                        }
                    });
                    if (!tableHeaderExist) {
                        addQuoteArticle(scope, '', 7)
                    }
                }
            }
        }
        addQuoteArticle(scope, data, isFreeTxt)
    }

    function addContractContent(scope, data, isFreeTxt) {
        var contractElements = [3, 4, 5, 6];
        var existInElements = contractElements.indexOf(isFreeTxt);
        if (isFreeTxt != 7) {
            if (scope.item.quote_group[scope.quote_group_active].table.length == 0) {
                if (existInElements == -1) {
                    addQuoteArticle(scope, '', 7)
                }
            } else {
                if (scope.item.quote_group[scope.quote_group_active].table[0].quote_line.length == 0) {
                    if (existInElements == -1) {
                        addQuoteArticle(scope, '', 7)
                    }
                }
            }
        }
        addQuoteArticle(scope, data, isFreeTxt)
    }

    function calcQuoteTotal(scope, type, $index, id) {
        var vat_total = 0,
            total = 0,
            total_vat = 0,
            discount_total = 0,
            total_currency = 0,
            discount_global = parseFloat(helper.return_value(scope.item.discount), 10),
            apply_discount = scope.item.apply_discount;
        scope.item.vat_lines = [];
        var liness = {};
        scope.item.hide_total = '';
        var vats = [];
        var vat_percent = [];
        var count_vat = 0;
        scope.item.total_wo_vat = 0;
        for (x in scope.item.quote_group) {
            var chapter_total = 0,
                table = scope.item.quote_group[x].table;
            if (table.length) {
                for (y in table) {
                    var table_total = 0,
                        table_total_vat = 0,
                        lines = table[y].quote_line;
                    table[y].total = 0;
                    table[y].total_vat_span = 0;
                    if (lines.length) {
                        for (i in lines) {
                            var tempObj = {};
                            var q = parseFloat(helper.return_value(lines[i].quantity), 10),
                                sale_unit = parseFloat(lines[i].sale_unit, 10),
                                packing = parseFloat(lines[i].package, 10),
                                price = parseFloat(helper.return_value(lines[i].price), 10),
                                price_vat = parseFloat(helper.return_value(lines[i].price_vat), 10),
                                vat = scope.item.remove_vat == 1 ? 0 : (lines[i].line_vat ? parseFloat(lines[i].line_vat, 10) : 0),
                                disc_line = lines[i].line_discount ? parseFloat(helper.return_value(lines[i].line_discount), 10) : 0,
                                price1 = 0;
                            if (isNaN(sale_unit)) {
                                sale_unit = 1
                            }
                            if (isNaN(packing)) {
                                packing = 1
                            }
                            var precision = getPrecision(q);
                            if (precision > 4) {
                                q = helper.round_n(q, 4);
                                lines[i].quantity = helper.displayNr(helper.round_n(q, 4), 4)
                            }
                            if (lines[i].line_type != 7 && lines[i].show_block_total != 1) {
                                if (vats[vat]) {
                                    if ((scope.app == 'contract' || scope.app == 'quote') && packing > 0) {
                                        vats[vat] += q * (price - price * disc_line / 100) * vat / 100 * (packing / sale_unit)
                                    } else {
                                        vats[vat] += q * (price - price * disc_line / 100) * vat / 100
                                    }
                                } else {
                                    if ((scope.app == 'contract' || scope.app == 'quote') && packing > 0) {
                                        vats[vat] = q * (price - price * disc_line / 100) * vat / 100 * (packing / sale_unit)
                                    } else {
                                        vats[vat] = q * (price - price * disc_line / 100) * vat / 100
                                    }
                                }
                                if (liness[vat]) {
                                    if ((scope.app == 'contract' || scope.app == 'quote') && packing > 0) {
                                        liness[vat] += q * (price - price * disc_line / 100) * (packing / sale_unit)
                                    } else {
                                        liness[vat] += q * (price - price * disc_line / 100)
                                    }
                                } else {
                                    if ((scope.app == 'contract' || scope.app == 'quote') && packing > 0) {
                                        liness[vat] = q * (price - price * disc_line / 100) * (packing / sale_unit)
                                    } else {
                                        liness[vat] = q * (price - price * disc_line / 100)
                                    }
                                }
                                if (liness[vat]) {
                                    vat_percent[vat] = lines[i].line_vat
                                }
                                if (apply_discount == 0 || apply_discount == 2) {
                                    disc_line = 0
                                }
                                if (apply_discount < 2) {
                                    discount_global = 0
                                }
                                if (id == lines[i].tr_id) {
                                    switch (type) {
                                        case 'price':
                                            lines[i].price_vat = helper.displayNr(parseFloat(price + price * vat / 100, 10), helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                                            break
                                    }
                                }
                                if ($index == i && id == lines[i].tr_id) {
                                    switch (type) {
                                        case 'price':
                                            lines[i].price_vat = helper.displayNr(parseFloat(price + price * vat / 100, 10), helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                                            break;
                                        case 'price_vat':
                                            price = parseFloat(price_vat / (1 + vat / 100), 10)
                                            lines[i].price = helper.displayNr(price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                                            break;
                                        case 'quantity':
                                            if (lines[i].component_for) {
                                                scope.item.quote_group[x].table[y].quote_line[i].quantity = lines[i].quantity_old;
                                            } else {
                                                var params = {
                                                    article_id: lines[i].article_id,
                                                    price: helper.return_value(lines[i].price),
                                                    quantity: q,
                                                    customer_id: scope.item.buyer_id,
                                                    x: x,
                                                    y: y,
                                                    i: i,
                                                    'do': 'quote--quote-get_article_quantity_price'
                                                };
                                                if (lines[i].is_tax != 1 && (lines[i].article_id && lines[i].article_id != '0')) {
                                                    helper.doRequest('post', 'index.php', params, function(r) {
                                                        if (r.data) {
                                                            scope.item.quote_group[r.data.x].table[r.data.y].quote_line[r.data.i].price = helper.displayNr(r.data.new_price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);

                                                            if (lines[r.data.i].is_combined) {
                                                                for (z in lines) {

                                                                    if (lines[r.data.i].line_id) {
                                                                        if (lines[r.data.i].line_id == lines[z].component_for) {
                                                                            //console.log(parseFloat(helper.return_value(lines[z].quantity_component), 10), r.data.quantity );
                                                                            lines[z].quantity = helper.displayNr(parseFloat(helper.return_value(lines[z].quantity_component), 10) * r.data.quantity);
                                                                            lines[z].quantity_old = parseFloat(helper.return_value(lines[z].quantity_component), 10) * r.data.quantity;
                                                                        }
                                                                    } else {
                                                                        if (lines[r.data.i].tr_id == lines[z].component_for) {
                                                                            //console.log(parseFloat(helper.return_value(lines[z].quantity_component), 10), r.data.quantity );
                                                                            lines[z].quantity = helper.displayNr(parseFloat(helper.return_value(lines[z].quantity_component), 10) * r.data.quantity);
                                                                            lines[z].quantity_old = parseFloat(helper.return_value(lines[z].quantity_component), 10) * r.data.quantity;
                                                                        }
                                                                    }

                                                                }
                                                            }

                                                            if (lines[r.data.i].has_taxes) {

                                                                for (z in lines) {

                                                                    if (lines[r.data.i].line_id) {
                                                                        if (lines[r.data.i].line_id == lines[z].for_article) {
                                                                            lines[z].quantity = helper.displayNr(r.data.quantity);
                                                                            lines[z].quantity_old = r.data.quantity;
                                                                        }
                                                                    } else {
                                                                        if (lines[r.data.i].tr_id == lines[z].for_article) {
                                                                            lines[z].quantity = helper.displayNr(r.data.quantity);
                                                                            lines[z].quantity_old = r.data.quantity;
                                                                        }
                                                                    }

                                                                }
                                                            }

                                                            scope.calcQuoteTotal(scope, 'price', $index, lines[i].tr_id)
                                                        }
                                                    })
                                                }
                                            }
                                            break
                                    }
                                }
                                price1 = price - price * disc_line / 100;
                                lines[i].vat_value = price1 * vat / 100;
                                var vat_total_line = (price1 - price1 * discount_global / 100) * vat / 100 * q * (packing / sale_unit);
                                vat_total += vat_total_line;
                                total += price1 * q * (packing / sale_unit);
                                lines[i].line_total = helper.displayNr(parseFloat(price1 * q * (packing / sale_unit), 10));
                                chapter_total += lines[i].line_total;
                                table[y].total += parseFloat(helper.return_value(lines[i].line_total), 10) * 1;
                                table[y].total_vat_span += vat_total_line
                            }
                        }
                    }
                    table[y].total = helper.displayNr(table[y].total);
                    table[y].total_vat_span = helper.displayNr(table[y].total_vat_span)
                }
            }
            scope.item.quote_group[x].chapter_total = helper.displayNr(chapter_total)
        }
        scope.item.total_wo_vat = helper.displayNr(total);
        scope.vat_line = [];
        var count_vats = 0;
        for (x in vats) {
            var tempObj = {};
            tempObj.subtotal = helper.displayNr(liness[x]);
            tempObj.discount_value = helper.displayNr(liness[x] * discount_global / 100);
            tempObj.net_amount = helper.displayNr(liness[x] - (liness[x] * discount_global / 100));
            tempObj.vat_value_d = (liness[x] - (liness[x] * discount_global / 100)) * x / 100;
            tempObj.vat_percent = vat_percent[x];
            tempObj.vat_value = scope.item.remove_vat == 1 ? helper.displayNr(0) : (discount_global ? helper.displayNr(tempObj.vat_value_d) : helper.displayNr(vats[x] ? parseFloat(vats[x], 10) : 0));
            scope.item.vat_lines.push(tempObj);
            count_vats++
        }
        if (count_vats == 1) {
            scope.item.hide_total = !1
        } else {
            scope.item.hide_total = !0
        }
        discount_total = total * discount_global / 100;
        if (apply_discount < 2) {
            discount_total = 0
        }
        scope.item.TOTAL_DISCOUNT = helper.displayNr(discount_total);
        scope.item.VAT_TOTAL = helper.displayNr(vat_total);
        scope.item.TOTAL_WITHOUT = helper.displayNr(total);
        if (isNaN(vat_total)) {
            vat_total = 0
        }
        scope.item.GRAND_TOTAL = helper.displayNr(parseFloat((helper.round(vat_total, 2) + helper.round(total, 2) - helper.round(discount_total, 2)), 10));
        scope.item.TOTAL_DEFAULT_CURRENCY = helper.displayNr(parseFloat((helper.round(vat_total, 2) + helper.round(total, 2) - helper.round(discount_total, 2)) * helper.return_value(scope.item.currency_rate), 10));
        //if(scope.app=='quote' && !scope.app_mod){
        if (scope.app == 'quote') {
            if (parseInt(scope.item.quote_group[scope.quote_group_active].table[0].quote_line[0].line_type) == 8 || parseInt(scope.item.quote_group[scope.quote_group_active].table[0].quote_line[scope.item.quote_group[scope.quote_group_active].table[0].quote_line.length - 1].line_type) == 8) {
                //leave it like this
            } else {
                //delete chapter subtotal line and add it to the end
                /*var chapterSubtotalIndex = 0;
                for (x in scope.item.quote_group[scope.quote_group_active].table[0].quote_line) {
                    if (parseInt(scope.item.quote_group[scope.quote_group_active].table[0].quote_line[x].line_type) == 8) {
                        chapterSubtotalIndex = x;
                        break;
                    }
                }
                if (chapterSubtotalIndex > 0) {
                    scope.item.quote_group[scope.quote_group_active].table[0].quote_line.splice(x, 1);
                }
                addQuoteArticle(scope, '', 8);*/ /*tsk nr.4366*/
            }
        }
        if (scope.typePromise) {
            $timeout.cancel(scope.typePromise)
        }
        scope.typePromise = $timeout(function() {
            //scope.save()
        }, 2000)
    }

    function calcTotalPurchase(scope, type, $index) {
        var vat_total = 0,
            total = 0,
            total_vat = 0,
            discount_total = 0,
            total_currency = 0,
            discount_global = parseFloat(helper.return_value(scope.discount), 10),
            apply_discount = scope.apply_discount,
            shipping_price = parseFloat(helper.return_value(scope.shipping_price), 10);
        if (!shipping_price || isNaN(shipping_price)) {
            shipping_price = 0;
        }
        for (x in scope.tr_id) {
            if (scope.tr_id[x].content === !1) {
                var q = parseFloat(helper.return_value(scope.tr_id[x].quantity), 10),
                    sale_unit = parseFloat(scope.tr_id[x].sale_unit, 10),
                    packing = parseFloat(scope.tr_id[x].packing, 10),
                    price = parseFloat(helper.return_value(scope.tr_id[x].price), 10),
                    price_vat = parseFloat(helper.return_value(scope.tr_id[x].price_vat), 10),
                    disc_line = parseFloat(helper.return_value(scope.tr_id[x].disc), 10),
                    price1 = 0;
                if (apply_discount == 0 || apply_discount == 2) {
                    disc_line = 0
                }
                if (apply_discount < 2) {
                    discount_global = 0
                }
                var precision = getPrecision(q);
                if (precision > 4) {
                    q = helper.round_n(q, 4);
                    scope.tr_id[x].quantity = helper.displayNr(helper.round_n(q, 4), 4)
                }
                if (isNaN(sale_unit)) {
                    sale_unit = 1
                }
                if (isNaN(packing)) {
                    packing = 1
                }
                if ($index == x) {
                    switch (type) {
                        case 'price':
                            scope.tr_id[x].price_vat = helper.displayNr(parseFloat(price, 10), helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                            break;
                        case 'quantity':
                            calcTotalPurchase(scope, 'price', $index);
                            break
                    }
                }
                price1 = price - price * disc_line / 100;
                scope.tr_id[x].vat_value = price1;
                total += price1 * q * (packing / sale_unit);
                scope.tr_id[x].line_total = helper.displayNr(parseFloat(price1 * q * (packing / sale_unit), 10))
            }
        }
        discount_total = total * discount_global / 100;
        if (apply_discount < 2) {
            discount_total = 0
        }
        scope.discount_total = helper.displayNr(discount_total);
        scope.total_vat = helper.displayNr(parseFloat((helper.round(total, 2) - helper.round(discount_total, 2)), 10));
        scope.total_default_c = helper.displayNr(parseFloat((helper.round(vat_total, 2) + helper.round(total, 2) - helper.round(discount_total, 2) + helper.round(shipping_price, 2)) * helper.return_value(scope.currency_rate), 10))
    }

    function calcTotal(scope, type, $index) {
        var vat_total = 0,
            total = 0,
            vats = [],
            discs = [],
            lines = [],
            total_vat = 0,
            discount_total = 0,
            total_currency = 0,
            discount_global = parseFloat(helper.return_value(scope.discount), 10),
            apply_discount = scope.apply_discount,
            shipping_price = parseFloat(helper.return_value(scope.shipping_price), 10);
        for (x in scope.tr_id) {
            if (scope.tr_id[x].content === !1 || scope.tr_id[x].content === null) {
                var q = parseFloat(helper.return_value(scope.tr_id[x].quantity), 10),
                    sale_unit = parseFloat(scope.tr_id[x].sale_unit, 10),
                    packing = parseFloat(scope.tr_id[x].packing, 10),
                    price = parseFloat(helper.return_value(scope.tr_id[x].price), 10),
                    price_vat = parseFloat(helper.return_value(scope.tr_id[x].price_vat), 10),
                    vat = scope.remove_vat == 1 ? 0 : (scope.tr_id[x].percent ? parseFloat(scope.tr_id[x].percent, 10) : 0),
                    disc_line = parseFloat(helper.return_value(scope.tr_id[x].disc), 10),
                    price1 = 0;
                if (isNaN(sale_unit) || !scope.allow_article_sale_unit) {
                    sale_unit = 1
                }
                if (isNaN(packing) || !scope.allow_article_packing) {
                    packing = 1
                }
                if (apply_discount == 0 || apply_discount == 2) {
                    disc_line = 0
                }
                if (apply_discount < 2) {
                    discount_global = 0
                }
                var precision = getPrecision(q);
                if (precision > 4) {
                    q = helper.round_n(q, 4);
                    scope.tr_id[x].quantity = helper.displayNr(helper.round_n(q, 4), 4)
                }
                if ($index == x) {
                    switch (type) {
                        case 'price':
                            scope.tr_id[x].price_vat = helper.displayNr(parseFloat(price + price * vat / 100, 10), helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                            break;
                        case 'price_vat':
                            price = parseFloat(price_vat / (1 + vat / 100), 10)
                            scope.tr_id[x].price = helper.displayNr(price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                            break;
                        case 'quantity':
                            if (scope.tr_id[x].article_id) {
                                if (parseFloat(scope.tr_id[$index].stock - helper.return_value(scope.tr_id[$index].quantity), 10) < scope.tr_id[x].threshold_value && scope.show_stock_warning && scope.allow_stock) {
                                    scope.tr_id[$index].show_stock_warning_line = true;
                                } else {
                                    scope.tr_id[$index].show_stock_warning_line = false;
                                }
                            }
                            if (scope.tr_id[x].is_component == 1 || scope.tr_id[x].component_for) {
                                scope.tr_id[$index].quantity = helper.displayNr(scope.tr_id[x].quantity_old, 2);
                            } else {
                                if (scope.tr_id[x].article_id) {
                                    var params = {
                                        article_id: scope.tr_id[x].article_id,
                                        price: helper.return_value(scope.tr_id[x].price),
                                        quantity: q,
                                        customer_id: scope.buyer_id,
                                        'do': 'order-order-order-get_article_quantity_price'
                                    };
                                    helper.doRequest('post', 'index.php', params, function(r) {
                                        if (r.data) {
                                            if (r.data.article_id != 0) {

                                                scope.tr_id[$index].price = helper.displayNr(r.data.new_price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);

                                                if (scope.tr_id[$index].is_combined) {
                                                    for (y in scope.tr_id) {
                                                        //console.log(scope.tr_id[$index].tr_id ,scope.tr_id[y].component_for);
                                                        if (scope.tr_id[$index].tr_id == scope.tr_id[y].component_for) {
                                                            //console.log(parseFloat(helper.return_value(scope.tr_id[y].quantity), 10),parseFloat(helper.return_value(scope.tr_id[y].quantity_component), 10) , scope.tr_id[$index].quantity );
                                                            scope.tr_id[y].quantity = helper.displayNr(parseFloat(helper.return_value(scope.tr_id[$index].quantity), 10) * parseFloat(helper.return_value(scope.tr_id[y].quantity_component), 10));
                                                            scope.tr_id[y].quantity_old = parseFloat(helper.return_value(scope.tr_id[$index].quantity), 10) * parseFloat(helper.return_value(scope.tr_id[y].quantity_component), 10);
                                                        }
                                                    }
                                                }

                                                if (scope.tr_id[$index].has_taxes) {
                                                    for (y in scope.tr_id) {
                                                        if (scope.tr_id[$index].tr_id == scope.tr_id[y].for_article) {

                                                            scope.tr_id[y].quantity = helper.displayNr(parseFloat(helper.return_value(scope.tr_id[$index].quantity), 10));
                                                            scope.tr_id[y].quantity_old = parseFloat(helper.return_value(scope.tr_id[$index].quantity), 10);
                                                        }
                                                    }
                                                }
                                                //console.log(scope.tr_id);
                                            };
                                            calcTotal(scope, 'price', $index)
                                        }
                                    });
                                }
                            }

                            break
                    }
                }
                price1 = price - price * disc_line / 100;
                scope.tr_id[x].vat_value = price1 * vat / 100;
                vat_total += (price1 - price1 * discount_global / 100) * vat / 100 * q * (packing / sale_unit);
                total += price1 * q * (packing / sale_unit);
                scope.tr_id[x].line_total = helper.displayNr(parseFloat(price1 * q * (packing / sale_unit), 10));
                if (lines[vat]) {
                    lines[vat] += q * price1
                } else {
                    lines[vat] = q * price1
                }
                if (discs[vat]) {
                    discs[vat] += q * price1 * discount_global / 100
                } else {
                    discs[vat] = q * price1 * discount_global / 100
                }
                if (vats[vat]) {
                    var nr_simple = q * (price1 - price1 * discount_global / 100) * vat / 100;
                    vats[vat] += nr_simple
                } else {
                    var nr_simple = q * (price1 - price1 * discount_global / 100) * vat / 100;
                    vats[vat] = nr_simple
                }
            }
        }
        var is_ord = scope.vat_line && scope.vat_line.length ? scope.vat_line[0].is_ord : !1;
        var view_disc = scope.vat_line && scope.vat_line.length ? scope.vat_line[0].view_discount : (scope.apply_discount > 1 ? !0 : !1);
        scope.vat_line = [];
        var count_vats = 0;
        for (x in vats) {
            var tempObj = {};
            tempObj.discount_value = helper.displayNr(discs[x]);
            tempObj.net_amount = helper.displayNr(lines[x] - discs[x]);
            tempObj.total_novat = helper.displayNr(helper.round(lines[x], 2));
            tempObj.vat_percent = helper.displayNr(x);
            tempObj.subtotal = helper.displayNr(lines[x]);
            tempObj.vat_value = helper.displayNr(helper.round(vats[x], 2));
            tempObj.is_ord = is_ord;
            tempObj.view_discount = view_disc;
            scope.vat_line.push(tempObj);
            count_vats++
        }
        discount_total = total * discount_global / 100;
        if (apply_discount < 2) {
            discount_total = 0
        }
        scope.discount_total = helper.displayNr(discount_total);
        scope.vat_total = helper.displayNr(vat_total);
        scope.total = helper.displayNr(total);
        if (isNaN(vat_total)) {
            vat_total = 0
        }
        scope.total_vat = helper.displayNr(parseFloat((helper.round(vat_total, 2) + helper.round(total, 2) - helper.round(discount_total, 2) + helper.round(shipping_price, 2)), 10));
        scope.total_default_c = helper.displayNr(parseFloat((helper.round(vat_total, 2) + helper.round(total, 2) - helper.round(discount_total, 2) + helper.round(shipping_price, 2)) * helper.return_value(scope.currency_rate), 10))
    }

    function getPrecision(number) {
        var n = number.toString().split(".");
        return n.length > 1 ? n[1].length : 0
    }

    function invoiceQuantityUpdate(item, index, scope) {
        if (item.article_id) {
            var params = {
                article_id: item.article_id,
                price: item.price,
                quantity: item.quantity,
                customer_id: scope.item.buyer_id,
                'do': 'invoice-invoice-invoice-get_article_quantity_price'
            };
            helper.doRequest('post', 'index.php', params, function(r) {
                if (r.data) {
                    if (scope.item.invoice_line[index].component_for) {
                        //the quantity for component was not disabled from the begining
                        scope.item.invoice_line[index].quantity = scope.item.invoice_line[index].quantity_old;
                    } else if (r.data.article_id != 0) {
                        //scope.item.invoice_line[index].price = helper.displayNr(helper.return_value(r.data.new_price), helper.settings.ARTICLE_PRICE_COMMA_DIGITS);

                        scope.item.invoice_line[index].price = helper.displayNr(r.data.new_price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);

                        if (scope.item.invoice_line[index].is_combined) {
                            for (y in scope.item.invoice_line) {
                                //console.log(scope.item.invoice_line[index].tr_id ,scope.item.invoice_line[y].component_for);
                                if (scope.item.invoice_line[index].tr_id == scope.item.invoice_line[y].component_for) {
                                    // console.log(scope.item.invoice_line[y].quantity,scope.item.invoice_line[y].quantity_component, scope.item.invoice_line[index].quantity );
                                    scope.item.invoice_line[y].quantity = helper.displayNr(parseFloat(helper.return_value(scope.item.invoice_line[index].quantity), 10) * parseFloat(helper.return_value(scope.item.invoice_line[y].quantity_component), 10));
                                    scope.item.invoice_line[y].quantity_old = parseFloat(helper.return_value(scope.item.invoice_line[index].quantity), 10) * parseFloat(helper.return_value(scope.item.invoice_line[y].quantity_component), 10);
                                }
                            }
                        }

                        if (scope.item.invoice_line[index].has_taxes) {
                            for (y in scope.item.invoice_line) {
                                if (scope.item.invoice_line[index].tr_id == scope.item.invoice_line[y].for_article) {
                                    scope.item.invoice_line[y].quantity = helper.displayNr(parseFloat(helper.return_value(scope.item.invoice_line[index].quantity), 10));
                                    scope.item.invoice_line[y].quantity_old = parseFloat(helper.return_value(scope.item.invoice_line[index].quantity), 10);
                                }
                            }
                        }
                        // console.log(scope.item.invoice_line);
                    };
                    calcInvoiceTotal(scope.item);
                }
            });
        }
    }

    function calcInvoiceTotal(scope) {
        var discount = parseFloat(helper.return_value(scope.discount), 10),
            vats = {},
            lines = {},
            discs = {},
            discount_value = 0,
            subtotal = 0,
            net_amount = 0,
            total = 0,
            apply_discount = scope.apply_discount,
            total_vat = 0;
        if (isNaN(discount)) {
            discount = 0
        }
        if (apply_discount < 2) {
            discount = 0
        }
        for (x in scope.invoice_line) {
            var price = parseFloat(helper.return_value(scope.invoice_line[x].price), 10),
                q = parseFloat(helper.return_value(scope.invoice_line[x].quantity), 10),
                vat = parseFloat(helper.return_value(scope.invoice_line[x].vat), 10),
                disc = parseFloat(helper.return_value(scope.invoice_line[x].discount_line), 10);
            if (apply_discount == 0 || apply_discount == 2) {
                disc = 0
            }
            var precision = getPrecision(q);
            if (precision > 4) {
                q = helper.round_n(q, 4);
                scope.invoice_line[x].quantity = helper.displayNr(helper.round_n(q, 4), 4)
            }
            scope.invoice_line[x].price_vat = helper.displayNr(helper.round_n(price + price * vat / 100, helper.settings.ARTICLE_PRICE_COMMA_DIGITS));
            scope.invoice_line[x].line_total = helper.displayNr(q * price - (q * price * disc / 100));
            price = price - price * disc / 100;
            if (lines[vat]) {
                lines[vat] += helper.round_n(q * price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS)
            } else {
                lines[vat] = helper.round_n(q * price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS)
            }
            subtotal += helper.round_n(q * price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
            if (discs[vat]) {
                discs[vat] += q * price * discount / 100
            } else {
                discs[vat] = q * price * discount / 100
            }
            var disc_l = helper.round_n(q * price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS) * discount / 100;
            discount_value += disc_l;
            net_amount += (helper.round_n(q * price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS) - disc_l);
            if (vats[vat]) {
                var nr_simple = q * (price - price * discount / 100) * vat / 100;
                vats[vat] += nr_simple
            } else {
                var nr_simple = q * (price - price * discount / 100) * vat / 100;
                vats[vat] = nr_simple
            }
        }
        var is_ord = scope.vat_line.length ? scope.vat_line[0].is_ord : !1;
        var view_disc = scope.vat_line.length ? scope.vat_line[0].view_discount : (scope.apply_discount > 1 ? !0 : !1);
        scope.vat_line = [];
        var count_vats = 0;
        for (x in vats) {
            var tempObj = {};
            tempObj.discount_value = helper.displayNr(discs[x]);
            tempObj.net_amount = helper.displayNr(lines[x] - discs[x]);
            tempObj.total_novat = helper.displayNr(helper.round_n(lines[x], 2));
            tempObj.vat_percent = helper.displayNr(x);
            tempObj.subtotal = helper.displayNr(lines[x]);
            tempObj.vat_value = helper.displayNr(helper.round_n(vats[x], 2));
            tempObj.is_ord = is_ord;
            tempObj.view_discount = view_disc;
            scope.vat_line.push(tempObj);
            total += lines[x] - discs[x];
            total_vat += vats[x];
            count_vats++
        }
        if (count_vats == 1) {
            scope.hide_total = !1
        } else {
            scope.hide_total = !0
        }
        scope.hide_content = !1;
        scope.total_wo_vat = helper.displayNr(subtotal);
        scope.net_amount = helper.displayNr(net_amount);
        scope.discount_value = helper.displayNr(discount_value);
        total = (helper.round_n(total, 2) + helper.round_n(total_vat, 2)) * 1;
        if (scope.downpayment > 0) {
            total -= scope.downpayment_value
        }
        scope.total = helper.displayNr(total);
        var req_payment_value = 0;
        if (scope.req_payment) {
            req_payment_percent = parseFloat(helper.return_value(scope.req_payment), 10);
            req_payment_value = total * req_payment_percent / 100
        }
        if (isNaN(req_payment_value)) {
            req_payment_value = 0
        }
        scope.req_payment_value = helper.displayNr(req_payment_value);
        var total_currency = total * parseFloat(helper.return_value(scope.currency_rate), 10);
        scope.total_default_currency = helper.displayNr(total_currency)
    }

    function addInvoiceArticleSpecial(scope, data) {
        if (data) {
            var item_width = 4;
            if (scope.item.show_vat) {
                item_width += 1
            }
            if (scope.item.apply_discount == 0 || scope.item.apply_discount == 2) {
                item_width += 1
            }
            var first_amount = data.price;
            var first_total = helper.return_value(first_amount) - ((helper.return_value(first_amount) * 18) / 100);
            var second_total = (helper.return_value(first_amount) * 18) / 100;
            var line = {};
            line.description = data.description;
            line.article_code = data.code;
            line.article_id = data.article_id;
            line.colspan = "";
            line.content = !1;
            line.content_class = "";
            line.discount_line = scope.item.discount;
            line.hide_currency1 = scope.item.hide_currency1;
            line.hide_currency2 = scope.item.hide_currency2;
            line.hide_stock = data.hide_stock;
            line.is_article = 1;
            line.is_profi = scope.item.is_profi;
            line.is_article_code = !0;
            line.is_tax = 0;
            line.item_width = item_width;
            line.line_total = "0,00";
            line.packing = data.packing;
            line.packing_x = data.packing;
            line.pending_articles = data.pending_articles;
            line.vat = helper.displayNr(data.vat);
            line.percent_x = helper.displayNr(data.vat);
            line.price = helper.displayNr(first_total);
            line.price_vat = scope.item.show_vat ? helper.displayNr(data.price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS) : helper.displayNr(data.price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
            line.purchase_price = data.purchase_price;
            line.quantity = data.quantity;
            line.quantity_old = helper.displayNr(data.quantity);
            line.readonly = !1;
            line.sale_unit = data.sale_unit;
            line.sale_unit_x = data.sale_unit;
            line.show_for_articles = !0;
            line.stock = data.stock;
            line.tax_for_article_id = "0";
            line.threshold_value = data.threshold_value;
            line.tr_id = "tmp" + (new Date().getTime());
            line.vat_value = data.vat_value;
            line.vat_value_x = data.vat_value;
            var exchange = 0;
            if (scope.item.currency_type != scope.item.default_currency) {
                line.price = helper.displayNr(parseFloat(helper.return_value(line.price), 10) / scope.item.currency_rate, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                exchange = 1
            }
            var line1 = {};
            line1.description = data.description;
            line1.article_code = data.code;
            line1.article_id = data.article_id;
            line1.colspan = "";
            line1.content = !1;
            line1.content_class = "";
            line1.discount_line = scope.item.discount;
            line1.hide_currency1 = scope.item.hide_currency1;
            line1.hide_currency2 = scope.item.hide_currency2;
            line1.hide_stock = data.hide_stock;
            line1.is_article = 1;
            line1.is_profi = scope.item.is_profi;
            line1.is_article_code = !0;
            line1.is_tax = 0;
            line1.item_width = item_width;
            line1.line_total = "0,00";
            line1.packing = data.packing;
            line1.packing_x = data.packing;
            line1.pending_articles = data.pending_articles;
            line1.vat = helper.displayNr(21);
            line1.percent_x = helper.displayNr(data.vat);
            line1.price = helper.displayNr(second_total);
            line1.price_vat = scope.item.show_vat ? helper.displayNr(data.price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS) : helper.displayNr(data.price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
            line1.purchase_price = data.purchase_price;
            line1.quantity = data.quantity;
            line1.quantity_old = helper.displayNr(data.quantity);
            line1.readonly = !1;
            line1.sale_unit = data.sale_unit;
            line1.sale_unit_x = data.sale_unit;
            line1.show_for_articles = !0;
            line1.stock = data.stock;
            line1.tax_for_article_id = "0";
            line1.threshold_value = data.threshold_value;
            line1.tr_id = "tmp" + (new Date().getTime());
            line1.vat_value = data.vat_value;
            line1.vat_value_x = data.vat_value;
            var exchange = 0;
            if (scope.item.currency_type != scope.item.default_currency) {
                line1.price = helper.displayNr(parseFloat(helper.return_value(line1.price), 10) / scope.item.currency_rate, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                exchange = 1
            }
            scope.hide_content = !1;
            scope.item.invoice_line.push(line);
            scope.item.invoice_line.push(line1);
            calcInvoiceTotal(scope.item)
        }
    }

    function addInvoiceArticle(scope, data, customIndex) {
        if (data) {
            var item_width = 4;
            if (scope.item.show_vat) {
                item_width += 1
            }
            if (scope.item.apply_discount == 0 || scope.item.apply_discount == 2) {
                item_width += 1
            }
            var line = {};
            line.description = data.quoteformat;
            line.article_code = data.code;
            line.article_id = data.article_id;
            line.colspan = "";
            line.content = !1;
            line.content_class = "";
            line.discount_line = scope.item.discount_line_gen ? scope.item.discount_line_gen : helper.displayNr(0);
            line.hide_currency1 = scope.item.hide_currency1;
            line.hide_currency2 = scope.item.hide_currency2;
            line.hide_stock = data.hide_stock;
            line.is_article = 1;
            line.is_profi = scope.item.is_profi;
            line.is_article_code = !0;
            line.is_tax = 0;
            line.item_width = item_width;
            line.line_total = "0,00";
            line.packing = data.packing;
            line.packing_x = data.packing;
            line.pending_articles = data.pending_articles;
            line.vat = helper.displayNr(data.vat);
            line.percent_x = helper.displayNr(data.vat);
            line.price = helper.displayNr(data.price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
            line.price_vat = scope.item.show_vat ? helper.displayNr(data.price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS) : helper.displayNr(data.price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
            line.purchase_price = helper.displayNr(data.purchase_price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
            line.quantity = helper.displayNr(helper.return_value(data.quantity), helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
            line.quantity_old = helper.displayNr(data.quantity);
            line.readonly = !1;
            line.sale_unit = data.sale_unit;
            line.sale_unit_x = data.sale_unit;
            line.show_for_articles = !0;
            line.stock = data.stock;
            line.tax_for_article_id = "0";
            line.threshold_value = data.threshold_value;
            line.tr_id = "tmp" + (new Date().getTime());
            line.vat_value = data.vat_value;
            line.vat_value_x = data.vat_value;
            line.has_variants = data.has_variants;
            line.has_variants_done = data.has_variants_done;
            line.is_variant_for = data.is_variant_for;
            line.is_variant_for_line = data.is_variant_for_line;
            line.variant_type = data.variant_type;
            line.is_combined = data.is_combined;
            line.component_for = '';
            line.visible = data.visible;
            line.has_taxes = data.has_taxes;
            var exchange = 0;
            if (scope.item.currency_type != scope.item.default_currency) {
                line.price = helper.displayNr(parseFloat(helper.return_value(line.price), 10) / parseFloat(helper.return_value(scope.item.currency_rate), 10), helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                exchange = 1
            }
            if (data.quantity > 1) {
                var params = {
                    article_id: data.article_id,
                    price: line.price,
                    quantity: data.quantity,
                    customer_id: scope.item.buyer_id,
                    'do': 'invoice-invoice-invoice-get_article_quantity_price'
                };
                helper.doRequest('post', 'index.php', params, function(r) {
                    if (r.data) {
                        line.price = helper.displayNr(r.data.new_price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS)
                    }
                    if (customIndex) {
                        scope.item.invoice_line.splice(customIndex, 0, line);
                    } else {
                        scope.item.invoice_line.push(line);
                    }
                    if (data.has_variants == true || data.has_variants == '1') {
                        for (x in scope.item.invoice_line) {
                            if (scope.item.invoice_line[x].is_variant_for > 0 && scope.item.invoice_line[x].variant_type > 0) {
                                var tmp_data = {
                                    'do': 'misc-articleVariants',
                                    'parentVariant': data.article_id,
                                    'is_variant_for_line': scope.item.invoice_line[x].tr_id,
                                    buyer_id: scope.item.buyer_id,
                                    lang_id: scope.item.email_language,
                                    cat_id: scope.item.price_category_id,
                                    remove_vat: scope.item.remove_vat,
                                    vat_regime_id: scope.item.vat_regime_id,
                                    variant_type_search: scope.item.invoice_line[x].variant_type,
                                    app: scope.app
                                };
                                helper.doRequest('get', 'index.php', tmp_data, function(o) {
                                    if (o.data.lines.length) {
                                        var actual_index = scope.item.invoice_line.length - 1;
                                        scope.item.invoice_line[actual_index].has_variants_done = '1';
                                        addInvoiceArticle(scope, o.data.lines[0]);
                                    }
                                });
                                break;
                            }
                        }
                    }
                    var params = {
                        article_id: data.article_id,
                        quantity: data.quantity,
                        exchange: exchange,
                        ex_rate: scope.item.currency_rate,
                        allow_article_sale_unit: scope.item.allow_article_sale_unit,
                        allow_article_packing: scope.item.allow_article_packing,
                        remove_vat: scope.item.remove_vat,
                        apply_discount: scope.item.apply_discount,
                        vat_regime_id: scope.item.vat_regime_id,
                        'do': 'invoice-addArticle',
                        xget: 'taxes'
                    };
                    helper.doRequest('get', 'index.php', params, function(res) {
                        if (res.data && res.data.lines) {
                            var d = res.data.lines
                            for (x in d) {
                                var show_line = !1;
                                if (scope.app == 'invoice') {
                                    if (d[x].apply_to == '0' || d[x].apply_to == '1') {
                                        show_line = !0
                                    }
                                }
                                var tax_width = 4;
                                if (scope.item.show_vat) {
                                    tax_width += 1
                                }
                                if (scope.item.apply_discount == 0 || scope.item.apply_discount == 2) {
                                    tax_width += 1
                                }
                                var taxLine = {
                                    description: d[x].tax_name,
                                    article_code: d[x].tax_code,
                                    quantity: d[x].quantity,
                                    quantity_old: d[x].quantity_old,
                                    article_id: d[x].tax_id,
                                    hide_currency1: scope.item.hide_currency1,
                                    hide_currency2: scope.item.hide_currency2,
                                    item_width: tax_width,
                                    is_tax: 1,
                                    tax_for_article_id: d[x].tax_for_article_id,
                                    vat: helper.displayNr(d[x].percent),
                                    vat_percent: d[x].percent,
                                    vat_value: d[x].vat_value,
                                    packing: 1,
                                    packing_x: 1,
                                    stock: d[x].stock,
                                    content: !1,
                                    price: d[x].price,
                                    purchase_price: 0,
                                    percent: d[x].percent,
                                    percent_x: d[x].percent_x,
                                    price_vat: helper.displayNr(d[x].price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                                    sale_unit: 1,
                                    sale_unit_x: 1,
                                    discount_line: "0,00",
                                    line_total: "0,00",
                                    for_article: line.tr_id,
                                    tr_id: "tmp" + (new Date().getTime()),
                                    show_for_articles: !0,
                                    show_line: show_line
                                }
                                if (show_line) {
                                    if (!data.has_variants || data.has_variants == '0') {
                                        if (customIndex) {
                                            scope.item.invoice_line.splice(customIndex, 0, taxLine);
                                        } else {
                                            scope.item.invoice_line.push(taxLine);
                                        }
                                    }
                                }
                            }
                        }
                        calcInvoiceTotal(scope.item)
                    })

                    //add components

                    var params = {
                        article_id: data.article_id,
                        quantity: data.quantity,
                        exchange: exchange,
                        ex_rate: scope.item.currency_rate,
                        allow_article_sale_unit: scope.item.allow_article_sale_unit,
                        allow_article_packing: scope.item.allow_article_packing,
                        remove_vat: scope.item.remove_vat,
                        apply_discount: scope.item.apply_discount,
                        vat_regime_id: scope.item.vat_regime_id,
                        'do': 'invoice-addArticle',
                        xget: 'components'
                    };
                    helper.doRequest('get', 'index.php', params, function(res) {
                        if (res.data && res.data.components) {
                            var d = res.data.components
                            for (x in d) {
                                var show_line = 1;
                                var component_width = 4;
                                if (scope.item.show_vat) {
                                    component_width += 1
                                }
                                if (scope.item.apply_discount == 0 || scope.item.apply_discount == 2) {
                                    component_width += 1
                                }

                                var componentLine = {
                                    description: d[x].component_name,
                                    article_code: d[x].component_code,
                                    quantity: d[x].quantity,
                                    quantity_old: d[x].quantity_old,
                                    quantity_component: d[x].quantity_component,
                                    article_id: d[x].component_article_id,
                                    component_id: d[x].component_id,
                                    parent_article_id: d[x].parent_article_id,
                                    is_tax: 0,
                                    is_article_code: true,
                                    is_component: 1,
                                    vat: helper.displayNr(data.vat),
                                    visible: d[x].visible,
                                    vat_percent: data.vat,
                                    vat_value: '',
                                    packing: 1,
                                    packing_x: 1,
                                    stock: d[x].stock,
                                    item_width: component_width,
                                    content: !1,
                                    price: 0,
                                    purchase_price: helper.displayNr(d[x].purchase_price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                                    percent: d[x].percent,
                                    percent_x: d[x].percent_x,
                                    price_vat: helper.displayNr(d[x].price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                                    sale_unit: 1,
                                    sale_unit_x: 1,
                                    discount_line: "0,00",
                                    line_total: "0,00",
                                    for_article: line.tr_id,
                                    tr_id: "tmp" + (new Date().getTime()),
                                    show_for_articles: !0,
                                    show_line: show_line,
                                    component_for: line.tr_id,
                                }

                                if (show_line) {
                                    scope.item.invoice_line.push(componentLine);

                                }
                            }
                        }
                        calcInvoiceTotal(scope.item)
                    })
                })
            } else {
                if (customIndex) {
                    scope.item.invoice_line.splice(customIndex, 0, line);
                } else {
                    scope.item.invoice_line.push(line);
                }
                if (data.has_variants == true || data.has_variants == '1') {
                    for (x in scope.item.invoice_line) {
                        if (scope.item.invoice_line[x].is_variant_for > 0 && scope.item.invoice_line[x].variant_type > 0) {
                            var tmp_data = {
                                'do': 'misc-articleVariants',
                                'parentVariant': data.article_id,
                                'is_variant_for_line': scope.item.invoice_line[x].tr_id,
                                buyer_id: scope.item.buyer_id,
                                lang_id: scope.item.email_language,
                                cat_id: scope.item.price_category_id,
                                remove_vat: scope.item.remove_vat,
                                vat_regime_id: scope.item.vat_regime_id,
                                variant_type_search: scope.item.invoice_line[x].variant_type,
                                app: scope.app
                            };
                            helper.doRequest('get', 'index.php', tmp_data, function(o) {
                                if (o.data.lines.length) {
                                    var actual_index = scope.item.invoice_line.length - 1;
                                    scope.item.invoice_line[actual_index].has_variants_done = '1';
                                    addInvoiceArticle(scope, o.data.lines[0]);
                                }
                            });
                            break;
                        }
                    }
                }
                var params = {
                    article_id: data.article_id,
                    quantity: helper.return_value(data.quantity),
                    exchange: exchange,
                    ex_rate: scope.item.currency_rate,
                    allow_article_sale_unit: scope.item.allow_article_sale_unit,
                    allow_article_packing: scope.item.allow_article_packing,
                    remove_vat: scope.item.remove_vat,
                    apply_discount: scope.item.apply_discount,
                    vat_regime_id: scope.item.vat_regime_id,
                    'do': 'invoice-addArticle',
                    xget: 'taxes'
                };
                helper.doRequest('get', 'index.php', params, function(res) {
                    if (res.data && res.data.lines) {
                        var d = res.data.lines
                        for (x in d) {
                            var show_line = !1;
                            if (scope.app == 'invoice') {
                                if (d[x].apply_to == '0' || d[x].apply_to == '1') {
                                    show_line = !0
                                }
                            }
                            var tax_width = 4;
                            if (scope.item.show_vat) {
                                tax_width += 1
                            }
                            if (scope.item.apply_discount == 0 || scope.item.apply_discount == 2) {
                                tax_width += 1
                            }
                            var taxLine = {
                                description: d[x].tax_name,
                                article_code: d[x].tax_code,
                                quantity: d[x].quantity,
                                quantity_old: d[x].quantity_old,
                                article_id: d[x].tax_id,
                                item_width: tax_width,
                                hide_currency1: scope.item.hide_currency1,
                                hide_currency2: scope.item.hide_currency2,
                                is_tax: 1,
                                tax_for_article_id: d[x].tax_for_article_id,
                                vat: helper.displayNr(d[x].percent),
                                vat_percent: d[x].vat,
                                vat_value: d[x].vat_value,
                                packing: 1,
                                packing_x: 1,
                                stock: d[x].stock,
                                content: !1,
                                price: d[x].price,
                                purchase_price: 0,
                                percent: d[x].vat,
                                percent_x: d[x].percent_x,
                                price_vat: helper.displayNr(d[x].price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                                sale_unit: 1,
                                sale_unit_x: 1,
                                discount_line: "0,00",
                                line_total: "0,00",
                                for_article: line.tr_id,
                                tr_id: "tmp" + (new Date().getTime()),
                                show_for_articles: !0,
                                show_line: show_line
                            }
                            if (show_line) {
                                if (!data.has_variants || data.has_variants == '0') {
                                    if (customIndex) {
                                        scope.item.invoice_line.splice(customIndex, 0, taxLine);
                                    } else {
                                        scope.item.invoice_line.push(taxLine);
                                    }
                                }
                            }
                        }
                    }
                    calcInvoiceTotal(scope.item)
                })

                //add components

                var params = {
                    article_id: data.article_id,
                    quantity: data.quantity,
                    exchange: exchange,
                    ex_rate: scope.item.currency_rate,
                    allow_article_sale_unit: scope.item.allow_article_sale_unit,
                    allow_article_packing: scope.item.allow_article_packing,
                    remove_vat: scope.item.remove_vat,
                    apply_discount: scope.item.apply_discount,
                    vat_regime_id: scope.item.vat_regime_id,
                    'do': 'invoice-addArticle',
                    xget: 'components'
                };
                helper.doRequest('get', 'index.php', params, function(res) {
                    if (res.data && res.data.components) {
                        var d = res.data.components
                        for (x in d) {
                            var show_line = 1;

                            var component_width = 4;
                            if (scope.item.show_vat) {
                                component_width += 1
                            }
                            if (scope.item.apply_discount == 0 || scope.item.apply_discount == 2) {
                                component_width += 1
                            }

                            var componentLine = {
                                description: d[x].component_name,
                                article_code: d[x].component_code,
                                quantity: d[x].quantity,
                                quantity_old: d[x].quantity_old,
                                quantity_component: d[x].quantity_component,
                                article_id: d[x].component_article_id,
                                component_id: d[x].component_id,
                                parent_article_id: d[x].parent_article_id,
                                is_tax: 0,
                                is_article_code: true,
                                is_component: 1,
                                vat: helper.displayNr(data.vat),
                                visible: d[x].visible,
                                vat_percent: data.vat,
                                vat_value: '',
                                packing: 1,
                                packing_x: 1,
                                stock: d[x].stock,
                                item_width: component_width,
                                content: !1,
                                price: 0,
                                purchase_price: helper.displayNr(d[x].purchase_price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                                percent: d[x].percent,
                                percent_x: d[x].percent_x,
                                price_vat: helper.displayNr(d[x].price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
                                sale_unit: 1,
                                sale_unit_x: 1,
                                discount_line: "0,00",
                                line_total: "0,00",
                                for_article: line.tr_id,
                                tr_id: "tmp" + (new Date().getTime()),
                                show_for_articles: !0,
                                show_line: show_line,
                                component_for: line.tr_id,
                            }

                            if (show_line) {
                                scope.item.invoice_line.push(componentLine);

                            }
                        }
                    }
                    calcInvoiceTotal(scope.item)
                })
            }
        }
    }

    function addInvoiceLine(scope, content, title) {
        var item_width = 6;
        if (scope.show_vat) {
            item_width += 1
        }
        if (scope.apply_discount == 0 || scope.apply_discount == 2) {
            item_width += 1
        }
        var line = {
            description: "",
            article_code: "",
            article_id: 0,
            colspan: "",
            content: content,
            content_class: "",
            discount_line: scope.discount,
            hide_currency1: scope.hide_currency1,
            hide_currency2: scope.hide_currency2,
            is_article: 0,
            is_profi: 0,
            is_article_code: !0,
            is_tax: 0,
            item_width: item_width,
            line_total: "0,00",
            pending_articles: 0,
            vat: scope.vat,
            price: helper.displayNr(0, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
            price_vat: helper.displayNr(0, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
            purchase_price: 0,
            quantity: helper.displayNr(1),
            quantity_old: helper.displayNr(1),
            readonly: !1,
            show_for_articles: !1,
            stock: 0,
            tax_for_article_id: "0",
            threshold_value: 0,
            tr_id: "tmp" + (new Date().getTime()),
            title: title,
            visible: 1
        }
        scope.invoice_line.push(line)
    }

    function addArticleComposedLine(scope, data) {

        var line = {
            code: data.code,
            article_id: data.article_id,
            name: data.name,
            parent_article_id: data.parent_article_id,
            /*price: helper.displayNr(data.price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
            price_vat: helper.displayNr(data.price_vat, helper.settings.ARTICLE_PRICE_COMMA_DIGITS),
            purchase_price: data.purchase_price,*/
            pim_articles_combined_id: '',
            quantity: data.quantity,
            stock: data.stock * 1,
            allow_stock: 1,
            show_stock: true,
            text_visible: helper.getLanguage("Visible on PDF. Click to change it to 'Not visible on PDF'"),
            visible: true,
            tr_id: "tmp" + (new Date().getTime()),
        }
        scope.composed.articles.push(line)
    }

    function addQuoteGroup(scope) {
        if (scope.app == 'quote') {
            if (!scope.app_mod && $rootScope.edited_data.indexOf('quote_edit') === -1) {
                $rootScope.edited_data.push('quote_edit');
            }
            helper.changedData();
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', { 'do': 'quote-nquote', 'xget': 'defaultPdfOptions' }, function(r) {
                scope.showGroup();
                var groupId = helper.unique_id();
                var newGroup = {
                    active: !0,
                    chapter_total: helper.displayNr(0),
                    group_id: groupId,
                    quote_group_id: groupId,
                    quote_group_pb: "0",
                    show_chapter_pb: "hide",
                    show_chapter_subtotal: "hide",
                    title: "",
                    table: []
                }
                for (x in r.data.default_pdf_options) {
                    newGroup[x] = r.data.default_pdf_options[x];
                }
                scope.item.quote_group.push(newGroup);
                scope.quote_group_active = scope.item.quote_group.length - 1;
                $timeout(function() {
                    $chapterHead = angular.element('#' + newGroup.group_id).find('.chapter_input_head');
                    if ($chapterHead) {
                        $chapterHead.focus();
                    }
                }, 100);
                if (scope.typePromise) {
                    $timeout.cancel(scope.typePromise)
                }
                scope.typePromise = $timeout(function() {
                    //scope.save()
                }, 2000)
            });
        } else {
            scope.showGroup();
            var groupId = helper.unique_id();
            var newGroup = {
                active: !0,
                chapter_total: helper.displayNr(0),
                group_id: groupId,
                quote_group_id: groupId,
                quote_group_pb: "0",
                show_chapter_pb: "hide",
                show_chapter_subtotal: "hide",
                show_chapter_total: "0",
                title: "",
                show_QC: !0,
                show_UP: !0,
                show_PS: !1,
                show_d: !0,
                show_AC: !0,
                table: []
            }
            scope.item.quote_group.push(newGroup);
            scope.quote_group_active = scope.item.quote_group.length - 1;
            if (scope.typePromise) {
                $timeout.cancel(scope.typePromise)
            }
            scope.typePromise = $timeout(function() {
                //scope.save()
            }, 2000)
        }
    }

    function uploadDrop(scope, e, dropbox) {
        angular.element('.newUploadFile4').on('click', function(e) {
            e.stopPropagation()
        }).on('change', function(e) {
            e.preventDefault();
            var _this = $(this),
                p = _this.parents('.upload-box'),
                x = p.find('.newUploadFile4');
            if (x[0].files.length == 0) {
                return !1
            }
            if (x[0].files[0].size > 2560000 && !dropbox) {
                alert("Error: file is to big.");
                return !1
            }
            p.find('.btn').addClass('hidden');
            p.find('.nameOfTheFile').text(x[0].files[0].name);
            if (scope.invoice) {
                scope.item = scope.invoice
            }
            if (scope.dropbox) {
                scope.item = scope
            }
            var formData = new FormData();
            formData.append("Filedata", x[0].files[0]);
            formData.append("do", 'misc--misc-dropboxUpload');
            formData.append("folder", scope.item.dropbox.folder);
            formData.append('drop_folder', scope.item.dropbox.drop_folder);
            formData.append('customer_id', scope.item.dropbox.customer_id);
            formData.append('item_id', scope.item.dropbox.item_id);
            formData.append('fu_database', scope.item.dropbox.folder);
            formData.append('isConcact', scope.item.dropbox.is_contact);
            formData.append('serial_number', scope.item.dropbox.serial_number);
            formData.append('is_batch', scope.item.dropbox.is_batch);
            p.find('.upload-progress-bar').css({
                width: '0%'
            });
            p.find('.upload-file').removeClass('hidden');
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "index.php", !0);
            oReq.upload.addEventListener("progress", function(e) {
                var pc = parseInt(e.loaded / e.total * 100);
                p.find('.upload-progress-bar').css({
                    width: pc + '%'
                })
            });
            oReq.onload = function(oEvent) {
                var res = JSON.parse(oEvent.target.response);
                p.find('.upload-file').addClass('hidden');
                p.find('.btn').removeClass('hidden');
                if (oReq.status == 200) {
                    if (res.data.success) {
                        var data = angular.copy(scope.item.drop_info);
                        data.do = 'misc-dropbox';
                        //angular.element('.loading_wrap').removeClass('hidden');
                        helper.doRequest('get', 'index.php', data, function(r) {
                            scope.item.dropbox = r.data
                        }, function(r) {
                            var obj = {
                                "error": {
                                    "error": helper.getLanguage("Problem connecting to Dropbox. Please try later.")
                                },
                                "notice": !1,
                                "success": !1
                            };
                            scope.showAlerts(obj)
                        })
                    } else {
                        var obj = {
                            "error": {
                                "error": helper.getLanguage("Not connected to Dropbox")
                            },
                            "notice": !1,
                            "success": !1
                        };
                        scope.showAlerts(obj)
                    }
                } else {
                    var obj = {
                        "error": {
                            "error": helper.getLanguage("Error " + oReq.status + " occurred when trying to upload your file.")
                        },
                        "notice": !1,
                        "success": !1
                    };
                    scope.showAlerts(obj)
                }
                angular.element('.newUploadFile4').unbind()
            };
            oReq.send(formData)
        });
        angular.element(e.target).parents('.upload-box').find('.newUploadFile4').click()
    }

    function showCustomerNotes(scope, buyer_id, customer_notes) {
        openModal(scope, 'EditCustomerNotes', { "buyer_id": buyer_id, "customer_notes": customer_notes }, 'lg');
    }

    function showPurchasePriceModal(scope, item) {
        openModal(scope, 'purchasePriceOrder', item, 'md');
    }

}]).factory('modalFc', ['helper', '$uibModal', '$timeout', function(helper, $uibModal, $timeout) {
    var modal = {
        open: open,
        setHeight: setModalMaxHeight
    };
    return modal;

    function setModalMaxHeight(element) {
        var current_modal = angular.element(element),
            modal_content = current_modal.find('.modal-content'),
            borderWidth = modal_content.outerHeight() - modal_content.innerHeight(),
            dialogMargin = angular.element(window).width() < 768 ? 20 : 60,
            contentHeight = angular.element(window).height() - (dialogMargin + borderWidth),
            headerHeight = current_modal.find('.modal-header').outerHeight() || 0,
            footerHeight = current_modal.find('.modal-footer').outerHeight() || 0,
            maxHeight = contentHeight - (headerHeight + 47);
        current_modal.find('.modal-body').css({
            'max-height': maxHeight,
            'overflow-y': 'auto'
        })
    }

    function open(modal) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: modal.template,
            controller: modal.ctrl,
            controllerAs: modal.ctrlAs ? modal.ctrlAs : 'vmMod',
            size: modal.size ? modal.size : 'lg',
            backdrop: modal.backdrop ? modal.backdrop : 'true',
            windowClass: modal.windowClass,
            resolve: {
                data: function() {
                    if (modal.item) {
                        return modal.item
                    } else if (modal.params) {
                        var d = modal.params;
                        var request_type = modal.request_type ? modal.request_type : 'get';
                        //angular.element('.loading_wrap').removeClass('hidden');
                        return helper.doRequest(request_type, 'index.php', d).then(function(d) {
                            if (modal.initial_item) {
                                var tmp_data = modal.initial_item;
                                for (var x in d.data) {
                                    tmp_data[x] = d.data[x];
                                }
                                return tmp_data;
                            } else {
                                return d.data
                            }
                        })
                    }
                    return !0
                }
            }
        });
        modalInstance.result.then(function(data) {
            if (modal.callback && typeof(modal.callback) == 'function') {
                modal.callback(data)
            }
        }, function(data) {
            if (modal.callbackExit && typeof(modal.callbackExit) == 'function') {
                modal.callbackExit(data)
            }
            modalInstance = undefined
        });
        modalInstance.rendered.then(function() {
            $timeout(function() {
                setModalMaxHeight(angular.element('.modal.in'))
            })
        })
    }
}]).factory('settingsFc', ['helper', '$uibModal', '$timeout', function(helper, $uibModal, $timeout) {
    var settings = {
        init: init
    };
    return settings;

    function init(scope) {
        scope.upload = upload;
        scope.showAlerts = showAlerts;
        scope.deleteImage = deleteImage;
        scope.setLogo = setLogo;
        scope.Changecodelabes = Changecodelabes;
        scope.Changecodedetails = Changecodedetails;
        scope.selectCustomLayout = selectCustomLayout;
        scope.selectCreditCustomLayout = selectCreditCustomLayout;
        scope.selectReminderCustomLayout = selectReminderCustomLayout;
        scope.selectDeliveryCustomLayout = selectDeliveryCustomLayout;
        scope.Resetdata = Resetdata;
        scope.ResetdataCredit = ResetdataCredit;
        scope.ResetdataReminder = ResetdataReminder;
        scope.pdfSaveData = pdfSaveData;
        scope.pdfSaveDataCredit = pdfSaveDataCredit;
        scope.pdfSaveDataReminder = pdfSaveDataReminder;
        scope.ResetdataDelivery = ResetdataDelivery;
        scope.pdfSaveDataDelivery = pdfSaveDataDelivery;
        scope.clickys = clickys;
        scope.ChangeIdentity = ChangeIdentity;
        scope.getIdentityLayout = getIdentityLayout;
        scope.selectHeaderLayout = selectHeaderLayout;
        scope.selectBodyLayout = selectBodyLayout;
        scope.selectFooterLayout = selectFooterLayout;
        scope.selectHeaderLayoutCredit = selectHeaderLayoutCredit;
        scope.selectBodyLayoutCredit = selectBodyLayoutCredit;
        scope.selectFooterLayoutCredit = selectFooterLayoutCredit;
        scope.selectHeaderLayoutReminder = selectHeaderLayoutReminder;
        scope.selectBodyLayoutReminder = selectBodyLayoutReminder;
        scope.selectFooterLayoutReminder = selectFooterLayoutReminder;
        scope.selectHeaderLayoutDelivery = selectHeaderLayoutDelivery;
        scope.selectBodyLayoutDelivery = selectBodyLayoutDelivery;
        scope.selectFooterLayoutDelivery = selectFooterLayoutDelivery;
        scope.saveDefaultEmail = saveDefaultEmail;
        scope.updateweblink = updateweblink;
        scope.Convention = Convention;
        scope.changelanguage = changelanguage;
        scope.changelang = changelang;
        scope.changelangdispatch = changelangdispatch;
        scope.changeSaved = changeSaved;
        scope.savelanguage = savelanguage;
        scope.savelanguageNew = savelanguageNew;
        scope.clickyes = clickyes;
        scope.saveSetting = saveSetting;
        scope.saveSettingdispatch = saveSettingdispatch;
        scope.ok = ok;
        scope.articlefieldup = articlefieldup;
        scope.clicky = clicky;
        scope.attachement_files = attachement_files;
        scope.deleteatach = deleteatach;
        scope.defaultcheck = defaultcheck;
        scope.opentigs = [];
        scope.openedtags = [];
        angular.element('.newUploadFile2').on('click', function(e) {
            e.stopPropagation()
        }).on('change', function(e) {
            e.preventDefault();
            var _this = $(this),
                p = _this.parents('.upload-box'),
                x = p.find('.newUploadFile2');
            if (x[0].files.length == 0) {
                return !1
            }
            if (x[0].files[0].size > 256000) {
                alert(helper.getLanguage("Error: file is to big."));
                return !1
            }
            p.find('.img-thumbnail').attr('src', '');
            var tmppath = URL.createObjectURL(x[0].files[0]);
            p.find('.img-thumbnail').attr('src', tmppath);
            p.find('.btn').addClass('hidden');
            p.find('.nameOfTheFile').text(x[0].files[0].name);
            var formData = new FormData();
            formData.append("Filedata", x[0].files[0]);
            formData.append("do", scope.app + '--' + scope.app + '-uploadify');
            p.find('.upload-progress-bar').css({
                width: '0%'
            });
            p.find('.upload-file').removeClass('hidden');
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "index.php", !0);
            oReq.upload.addEventListener("progress", function(e) {
                var pc = parseInt(e.loaded / e.total * 100);
                p.find('.upload-progress-bar').css({
                    width: pc + '%'
                })
            });
            oReq.onload = function(oEvent) {
                var res = JSON.parse(oEvent.target.response);
                p.find('.upload-file').addClass('hidden');
                p.find('.btn').removeClass('hidden');
                if (oReq.status == 200) {
                    if (res.success) {
                        helper.doRequest('get', 'index.php?do=' + scope.app + '-settings&xget=logos', function(r) {
                            scope.logos = r.data.logos
                        })
                    } else {
                        alert(res.error + ' mama')
                    }
                } else {
                    alert("Error " + oReq.status + " occurred when trying to upload your file.")
                }
            };
            oReq.send(formData)
        });
        angular.element('.newUploadFile3').on('click', function(e) {
            e.stopPropagation()
        }).on('change', function(e) {
            e.preventDefault();
            var _this = $(this),
                p = _this.parents('.upload-box1'),
                x = p.find('.newUploadFile3');
            if (x[0].files.length == 0) {
                return !1
            }
            p.find('.img-thumbnail1').attr('src', '');
            var tmppath = URL.createObjectURL(x[0].files[0]);
            p.find('.img-thumbnail1').attr('src', tmppath);
            p.find('.btn').addClass('hidden');
            p.find('.nameOfTheFile1').text(x[0].files[0].name);
            var formData = new FormData();
            formData.append("Filedata", x[0].files[0]);
            formData.append("do", scope.app + '--' + scope.app + '-atach');
            p.find('.upload-progress-bar1').css({
                width: '0%'
            });
            p.find('.upload-file1').removeClass('hidden');
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "index.php", !0);
            oReq.upload.addEventListener("progress", function(e) {
                var pc = parseInt(e.loaded / e.total * 100);
                p.find('.upload-progress-bar1').css({
                    width: pc + '%'
                })
            });
            oReq.onload = function(oEvent) {
                var res1 = JSON.parse(oEvent.target.response);
                p.find('.upload-file1').addClass('hidden');
                p.find('.btn').removeClass('hidden');
                if (oReq.status == 200) {
                    if (res1.success) {
                        helper.doRequest('get', 'index.php?do=' + scope.app + '-settings&xget=attachments', function(r) {
                            scope.atachment = r.data.atachment
                        })
                    } else {
                        var msg = {
                            'error': {
                                'error': file.name + ': ' + res1.error
                            },
                            'notice': !1,
                            'success': !1
                        };
                        scope.showAlerts(msg)
                    }
                } else {
                    var msg = {
                        'error': {
                            'error': "Error " + oReq.status + " occurred when trying to upload your file."
                        },
                        'notice': !1,
                        'success': !1
                    };
                    scope.showAlerts(msg)
                }
            };
            oReq.send(formData)
        })
    }

    function upload(scope, e) {
        angular.element(e.target).parents('.upload-box').find('.newUploadFile2').click()
    }

    function showAlerts(scope, d) {
        helper.showAlerts(scope, d)
    }

    function clickys(scope, v1, v2) {
        var myValue = v1;
        if (scope.opentigs.indexOf(v1) > -1) {
            myValue = v2
        } else {
            scope.opentigs.push(v1)
        }
        var _this = angular.element('#altid')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            scope.codelabels.variable_data = scope.codelabels.variable_data.substring(0, startPos) + myValue + scope.codelabels.variable_data.substring(endPos, scope.codelabels.variable_data.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            scope.codelabels.variable_data += myValue;
            _this.focus()
        }
        if (myValue == v2) {
            var ind = scope.opentigs.indexOf(v1);
            scope.opentigs.splice(ind, 1)
        }
    }

    function deleteImage(scope, r) {
        var d = angular.copy(r);
        d.do = scope.app + '-settings-' + scope.app + '-delete_logo';
        d.name = d.account_logo;
        d.xget = 'logos';
        helper.doRequest('post', 'index.php', d, function(r) {
            scope.showAlerts(scope, r);
            if (!r.error) {
                scope.logos = r.data.logos
            }
        })
    }

    function setLogo(scope, r) {
        var d = angular.copy(r);
        d.do = scope.app + '--' + scope.app + '-set_default_logo';
        d.name = d.account_logo;
        helper.doRequest('post', 'index.php', d, function(r) {
            scope.showAlerts(scope, r)
        })
    }

    function ChangeIdentity(scope, r) {
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-set_identity',
            'xget': 'PDFlayout',
            identity_id: r.identity
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.layout = r.data.layout;
            scope.layout_header = r.data.layout_header;
            scope.custom_layout = r.data.custom_layout;
            scope.showAlerts(scope, r)
        })
    }

    function getIdentityLayout(scope, r, get) {
        var data = {
            'do': scope.app + '-settings',
            'xget': get ? get : 'PDFlayout',
            identity_id: r
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.layout = r.data.layout;
            if (r.data.layout_header) {
                scope.layout_header = r.data.layout_header;
                scope.layout_footer = r.data.layout_footer;
                scope.custom_layout = r.data.custom_layout;
            }
            if (r.data.layout_header_delivery) {
                scope.layout_header_delivery = r.data.layout_header_delivery;
                scope.layout_footer_delivery = r.data.layout_footer_delivery;
                scope.custom_layout_delivery = r.data.custom_layout_delivery;
            }

            if (r.data.layout_credit_header) {
                scope.layout_credit_header = r.data.layout_credit_header;
                scope.layout_credit_footer = r.data.layout_credit_footer;
                scope.custom_credit_layout = r.data.custom_credit_layout;
            }

            if (r.data.layout_reminder_header) {
                scope.layout_reminder_header = r.data.layout_reminder_header;
                scope.layout_reminder_footer = r.data.layout_reminder_footer;
                scope.custom_reminder_layout = r.data.custom_reminder_layout;
            }

            scope.settings.identity = r.data.identity;
            scope.settings.identity_delivery = r.data.identity;
            scope.showAlerts(scope, r)
        })
    }

    function selectHeaderLayout(scope, type) {
        var get = 'PDFlayout';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-default_pdf_format_header',
            'xget': get,
            'header': scope.layout_header.header,
            'footer': scope.layout_header.footer,
            'type': type,
            'identity_id': scope.layout_header.identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.settings = {
                client: r.data.client,
                project: r.data.project,
                task: r.data.task,
                comment: r.data.comment,
                show_bank: r.data.show_bank,
                show_stamp: r.data.show_stamp,
                show_page: r.data.show_page,
                show_paid: r.data.show_paid,
                multiple_identity: r.data.multiple_identity,
                identity: r.data.identity,
                selected_header: r.data.selected_header,
                selected_body: r.data.selected_body,
                selected_footer: r.data.selected_footer
            };
            scope.layout_header = r.data.layout_header;
            scope.layout_body = r.data.layout_body;
            scope.layout_footer = r.data.layout_footer;
            scope.custom_layout = r.data.custom_layout;
            scope.use_custom_layout = r.data.use_custom_layout;
            scope.use_custom_layout_delivery = r.data.use_custom_layout_delivery;
            scope.showAlerts(scope, r)
        })
    }

    function selectBodyLayout(scope, type) {
        var get = 'PDFlayout';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-default_pdf_format_body',
            'xget': get,
            'header': scope.layout_header.header,
            'footer': scope.layout_footer.footer,
            'type': type,
            'identity_id': scope.layout_header.identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.settings = {
                client: r.data.client,
                project: r.data.project,
                task: r.data.task,
                comment: r.data.comment,
                show_bank: r.data.show_bank,
                show_stamp: r.data.show_stamp,
                show_page: r.data.show_page,
                show_paid: r.data.show_paid,
                multiple_identity: r.data.multiple_identity,
                identity: r.data.identity,
                selected_header: r.data.selected_header,
                selected_body: r.data.selected_body,
                selected_footer: r.data.selected_footer
            };
            scope.layout_header = r.data.layout_header;
            scope.layout_body = r.data.layout_body;
            scope.layout_footer = r.data.layout_footer;
            scope.custom_layout = r.data.custom_layout;
            scope.use_custom_layout = r.data.use_custom_layout;
            scope.use_custom_layout_delivery = r.data.use_custom_layout_delivery;
            scope.showAlerts(scope, r)
        })
    }

    function selectFooterLayout(scope, type) {
        var get = 'PDFlayout';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-default_pdf_format_footer',
            'xget': get,
            'header': scope.layout_footer.footer,
            'footer': scope.layout_footer.header,
            'type': type,
            'identity_id': scope.layout_header.identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.settings = {
                client: r.data.client,
                project: r.data.project,
                task: r.data.task,
                comment: r.data.comment,
                show_bank: r.data.show_bank,
                show_stamp: r.data.show_stamp,
                show_page: r.data.show_page,
                show_paid: r.data.show_paid,
                multiple_identity: r.data.multiple_identity,
                identity: r.data.identity,
                selected_header: r.data.selected_header,
                selected_body: r.data.selected_body,
                selected_footer: r.data.selected_footer
            };
            scope.layout_header = r.data.layout_header;
            scope.layout_body = r.data.layout_body;
            scope.layout_footer = r.data.layout_footer;
            scope.custom_layout = r.data.custom_layout;
            scope.use_custom_layout = r.data.use_custom_layout;
            scope.use_custom_layout_delivery = r.data.use_custom_layout_delivery;
            scope.showAlerts(scope, r)
        })
    }

    function selectHeaderLayoutCredit(scope, type) {
        var get = 'PDFlayoutCredit';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-default_pdf_format_header_credit',
            'xget': get,
            'header': scope.layout_credit_header.header,
            'footer': scope.layout_credit_header.footer,
            'type': type,
            'identity_id': scope.layout_credit_header.identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.credit = {
                selected_credit_header: r.data.selected_credit_header,
                selected_credit_body: r.data.selected_credit_body,
                selected_credit_footer: r.data.selected_credit_footer
            };
            scope.layout_credit_header = r.data.layout_credit_header;
            scope.layout_credit_body = r.data.layout_credit_body;
            scope.layout_credit_footer = r.data.layout_credit_footer;
            scope.custom_credit_layout = r.data.custom_credit_layout;
            scope.use_custom_credit_layout = r.data.use_custom_credit_layout;
            scope.showAlerts(scope, r)
        })
    }

    function selectBodyLayoutCredit(scope, type) {
        var get = 'PDFlayoutCredit';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-default_pdf_format_body_credit',
            'xget': get,
            'header': scope.layout_credit_header.header,
            'footer': scope.layout_credit_header.footer,
            'type': type,
            'identity_id': scope.layout_credit_header.identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.credit = {
                selected_credit_header: r.data.selected_credit_header,
                selected_credit_body: r.data.selected_credit_body,
                selected_credit_footer: r.data.selected_credit_footer
            };
            scope.layout_credit_header = r.data.layout_credit_header;
            scope.layout_credit_body = r.data.layout_credit_body;
            scope.layout_credit_footer = r.data.layout_credit_footer;
            scope.custom_credit_layout = r.data.custom_credit_layout;
            scope.use_custom_credit_layout = r.data.use_custom_credit_layout;
            scope.showAlerts(scope, r)
        })
    }

    function selectFooterLayoutCredit(scope, type) {
        var get = 'PDFlayoutCredit';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-default_pdf_format_footer_credit',
            'xget': get,
            'header': scope.layout_credit_footer.footer,
            'footer': scope.layout_credit_footer.header,
            'type': type,
            'identity_id': scope.layout_credit_header.identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.credit = {
                selected_credit_header: r.data.selected_credit_header,
                selected_credit_body: r.data.selected_credit_body,
                selected_credit_footer: r.data.selected_credit_footer
            };
            scope.layout_credit_header = r.data.layout_credit_header;
            scope.layout_credit_body = r.data.layout_credit_body;
            scope.layout_credit_footer = r.data.layout_credit_footer;
            scope.custom_credit_layout = r.data.custom_credit_layout;
            scope.use_custom_credit_layout = r.data.use_custom_credit_layout;
            scope.showAlerts(scope, r)
        })
    }

    function selectHeaderLayoutReminder(scope, type) {
        var get = 'PDFlayoutReminder';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-default_pdf_format_header_reminder',
            'xget': get,
            'header': scope.layout_reminder_header.header,
            'footer': scope.layout_reminder_header.footer,
            'type': type,
            'identity_id': scope.layout_reminder_header.identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.reminder = {
                selected_reminder_header: r.data.selected_reminder_header,
                selected_reminder_body: r.data.selected_reminder_body,
                selected_reminder_footer: r.data.selected_reminder_footer
            };
            scope.layout_reminder_header = r.data.layout_reminder_header;
            scope.layout_reminder_body = r.data.layout_reminder_body;
            scope.layout_reminder_footer = r.data.layout_reminder_footer;
            scope.custom_reminder_layout = r.data.custom_reminder_layout;
            scope.showAlerts(scope, r)
        })
    }

    function selectBodyLayoutReminder(scope, type) {
        var get = 'PDFlayoutReminder';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-default_pdf_format_body_reminder',
            'xget': get,
            'header': scope.layout_reminder_header.header,
            'footer': scope.layout_reminder_header.footer,
            'type': type,
            'identity_id': scope.layout_reminder_header.identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.reminder = {
                selected_reminder_header: r.data.selected_reminder_header,
                selected_reminder_body: r.data.selected_reminder_body,
                selected_reminder_footer: r.data.selected_reminder_footer
            };
            scope.layout_reminder_header = r.data.layout_reminder_header;
            scope.layout_reminder_body = r.data.layout_reminder_body;
            scope.layout_reminder_footer = r.data.layout_reminder_footer;
            scope.custom_reminder_layout = r.data.custom_reminder_layout;
            scope.showAlerts(scope, r)
        })
    }

    function selectFooterLayoutReminder(scope, type) {
        var get = 'PDFlayoutReminder';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-default_pdf_format_footer_reminder',
            'xget': get,
            'header': scope.layout_reminder_footer.footer,
            'footer': scope.layout_reminder_footer.header,
            'type': type,
            'identity_id': scope.layout_reminder_header.identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.reminder = {
                selected_reminder_header: r.data.selected_reminder_header,
                selected_reminder_body: r.data.selected_reminder_body,
                selected_reminder_footer: r.data.selected_reminder_footer
            };
            scope.layout_reminder_header = r.data.layout_reminder_header;
            scope.layout_reminder_body = r.data.layout_reminder_body;
            scope.layout_reminder_footer = r.data.layout_reminder_footer;
            scope.custom_reminder_layout = r.data.custom_reminder_layout;
            scope.showAlerts(scope, r)
        })
    }

    function selectHeaderLayoutDelivery(scope, type) {
        var get = 'PDFlayout';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-default_pdf_format_header_delivery',
            'xget': get,
            'header': scope.layout_header_delivery.header,
            'footer': scope.layout_header_delivery.footer,
            'type': type,
            'identity_id': scope.layout_header_delivery.identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.settings = {
                multiple_identity: r.data.multiple_identity,
                identity: r.data.identity,
                selected_header_delivery: r.data.selected_header_delivery,
                selected_body_delivery: r.data.selected_body_delivery,
                selected_footer_delivery: r.data.selected_footer_delivery
            };
            scope.layout_header_delivery = r.data.layout_header_delivery;
            scope.layout_body_delivery = r.data.layout_body_delivery;
            scope.layout_footer_delivery = r.data.layout_footer_delivery;
            scope.custom_layout_delivery = r.data.custom_layout_delivery;
            scope.use_custom_layout_delivery = r.data.use_custom_layout_delivery;
            scope.showAlerts(scope, r)
        })
    }

    function selectBodyLayoutDelivery(scope, type) {
        var get = 'PDFlayout';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-default_pdf_format_body_delivery',
            'xget': get,
            'header': scope.layout_header_delivery.header,
            'footer': scope.layout_header_delivery.footer,
            'type': type,
            'identity_id': scope.layout_header_delivery.identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.settings = {
                multiple_identity: r.data.multiple_identity,
                identity: r.data.identity,
                selected_header_delivery: r.data.selected_header_delivery,
                selected_body_delivery: r.data.selected_body_delivery,
                selected_footer_delivery: r.data.selected_footer_delivery
            };
            scope.layout_header_delivery = r.data.layout_header_delivery;
            scope.layout_body_delivery = r.data.layout_body_delivery;
            scope.layout_footer_delivery = r.data.layout_footer_delivery;
            scope.custom_layout_delivery = r.data.custom_layout_delivery;
            scope.use_custom_layout = r.data.use_custom_layout;
            scope.use_custom_layout_delivery = r.data.use_custom_layout_delivery;
            scope.showAlerts(scope, r)
        })
    }

    function selectFooterLayoutDelivery(scope, type) {
        var get = 'PDFlayout';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-default_pdf_format_footer_delivery',
            'xget': get,
            'header': scope.layout_footer_delivery.footer,
            'footer': scope.layout_footer_delivery.header,
            'type': type,
            'identity_id': scope.layout_footer_delivery.identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.settings = {
                multiple_identity: r.data.multiple_identity,
                identity: r.data.identity,
                selected_header_delivery: r.data.selected_header_delivery,
                selected_body_delivery: r.data.selected_body_delivery,
                selected_footer_delivery: r.data.selected_footer_delivery
            };
            scope.layout_header_delivery = r.data.layout_header_delivery;
            scope.layout_body_delivery = r.data.layout_body_delivery;
            scope.layout_footer_delivery = r.data.layout_footer_delivery;
            scope.custom_layout_delivery = r.data.custom_layout_delivery;
            scope.use_custom_layout_delivery = r.data.use_custom_layout_delivery;
            scope.showAlerts(scope, r)
        })
    }

    function Changecodelabes(scope, r) {
        var get = 'selectlabels';
        var data = {
            'do': scope.app + '-custom_pdf',
            'xget': get,
            make_id: r.make_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            scope.selectlabels = {
                make_id: r.data.make_id,
                make: r.data.make,
                selected_make: r.data.selected_make
            };
            scope.showAlerts(scope, r)
        })
    }

    function Changecodedetails(scope, r) {
        var get = 'selectdetails';
        var data = {
            'do': scope.app + '-custom_pdf',
            'xget': get,
            detail_id: r.detail_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            scope.selectdetails = {
                detail_id: r.data.detail_id,
                detail: r.data.detail,
                selected_detail: r.data.selected_detail
            };
            scope.showAlerts(scope, r)
        })
    }

    function Resetdata(scope, r) {
        var get = 'codelabels';
        var data = {
            'do': scope.app + '--' + scope.app + '-reset_data',
            'xget': get,
            'header': scope.codelabels.header,
            'footer': scope.codelabels.footer,
            'body': scope.codelabels.body,
            'layout': scope.codelabels.layout
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            scope.codelabels.variable_data = r.data.var_data
        })
    }

    function ResetdataCredit(scope, r) {
        var get = 'codelabels';
        var data = {
            'do': scope.app + '--' + scope.app + '-reset_datacredit',
            'xget': get,
            'header': scope.codelabels.header,
            'footer': scope.codelabels.footer,
            'body': scope.codelabels.body,
            'layout': scope.codelabels.layout
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            scope.codelabels.variable_data = r.data.var_data
        })
    }

    function ResetdataReminder(scope, r) {
        var get = 'codelabels';
        var data = {
            'do': scope.app + '--' + scope.app + '-reset_datareminder',
            'xget': get,
            'header': scope.codelabels.header,
            'footer': scope.codelabels.footer,
            'body': scope.codelabels.body,
            'layout': scope.codelabels.layout
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            scope.codelabels.variable_data = r.data.var_data
        })
    }

    function ResetdataDelivery(scope, r) {
        var get = 'codelabels';
        var data = {
            'do': scope.app + '--' + scope.app + '-reset_data_delivery',
            'xget': get,
            'header': scope.codelabels.header,
            'footer': scope.codelabels.footer,
            'layout': scope.codelabels.layout
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            scope.codelabels.variable_data = r.data.var_data
        })
    }

    function selectCustomLayout(scope, custom_type) {
        var get = 'PDFlayout';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-set_custom_pdf',
            'xget': get,
            'custom_type': custom_type
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.layout = r.data.layout;
            scope.layout_header = r.data.layout_header;
            scope.layout_body = r.data.layout_body;
            scope.layout_footer = r.data.layout_footer;
            scope.custom_layout = r.data.custom_layout;
            scope.use_custom_layout = r.data.use_custom_layout;
            scope.use_custom_layout_delivery = r.data.use_custom_layout_delivery;
            scope.settings.identity = r.data.identity;
            scope.showAlerts(scope, r)
        })
    }

    function selectCreditCustomLayout(scope, custom_type) {
        var get = 'PDFlayoutCredit';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-set_credit_custom_pdf',
            'xget': get,
            'custom_type': custom_type
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.layout_credit_header = r.data.layout_credit_header;
            scope.layout_credit_body = r.data.layout_credit_body;
            scope.layout_credit_footer = r.data.layout_credit_footer;
            scope.custom_credit_layout = r.data.custom_credit_layout;
            scope.use_custom_credit_layout = r.data.use_custom_credit_layout;
            scope.settings.identity = r.data.identity;
            scope.showAlerts(scope, r)
        })
    }

    function selectReminderCustomLayout(scope, custom_type) {
        var get = 'PDFlayoutReminder';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-set_reminder_custom_pdf',
            'xget': get,
            'custom_type': custom_type
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.layout_reminder_header = r.data.layout_reminder_header;
            scope.layout_reminder_body = r.data.layout_reminder_body;
            scope.layout_reminder_footer = r.data.layout_reminder_footer;
            scope.custom_reminder_layout = r.data.custom_reminder_layout;
            scope.use_custom_reminder_layout = r.data.use_custom_reminder_layout;
            scope.settings.identity = r.data.identity;
            scope.showAlerts(scope, r)
        })
    }

    function selectDeliveryCustomLayout(scope, custom_type) {
        var get = 'PDFlayout';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-set_delivery_custom_pdf',
            'xget': get,
            'custom_type': custom_type
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.layout_header_delivery = r.data.layout_header_delivery;
            scope.layout_body_delivery = r.data.layout_body_delivery;
            scope.layout_footer_delivery = r.data.layout_footer_delivery;
            scope.custom_layout_delivery = r.data.custom_layout_delivery;
            scope.use_custom_layout_delivery = r.data.use_custom_layout_delivery;
            scope.settings.identity = r.data.identity;
            scope.showAlerts(scope, r)
        })
    }

    function pdfSaveData(scope, r) {

        var get = 'codelabels';
        var data = {
            'do': scope.app + '-custom_pdf-' + scope.app + '-pdfSaveData',
            'xget': get,
            'header': scope.codelabels.header,
            'footer': scope.codelabels.footer,
            'body': scope.codelabels.body,
            'layout': scope.codelabels.layout,
            'variable_data': scope.codelabels.variable_data,
            'identity_id': scope.codelabels.identity_id,
        };
        if (scope.codelabels.body == 'body') {
            data.body_l = scope.codelabels.body_id
        }


        helper.doRequest('post', 'index.php', data, function(r) {
            scope.codelabels = {
                make_id: r.data.make_id,
                make: r.data.make,
                selected_make: r.data.selected_make,
                variable_data: r.data.variable_data,
                layout: r.data.layout,
                header: r.data.header,
                footer: r.data.footer
            };
            scope.showAlerts(scope, r);
            scope.closeModal()
        })
    }

    function pdfSaveDataCredit(scope, r) {
        var get = 'codelabels';
        var data = {
            'do': scope.app + '-custom_pdf_credit-' + scope.app + '-pdfSaveDataCredit',
            'xget': get,
            'header': scope.codelabels.header,
            'footer': scope.codelabels.footer,
            'body': scope.codelabels.body,
            'layout': scope.codelabels.layout,
            'variable_data': scope.codelabels.variable_data,
            'identity_id': scope.codelabels.identity_id,
        };
        if (scope.codelabels.body == 'body') {
            data.body_l = scope.codelabels.body_id
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.codelabels = {
                make_id: r.data.make_id,
                make: r.data.make,
                selected_make: r.data.selected_make,
                variable_data: r.data.variable_data,
                layout: r.data.layout,
                header: r.data.header,
                footer: r.data.footer
            };
            scope.showAlerts(scope, r);
            scope.closeModal()
        })
    }

    function pdfSaveDataReminder(scope, r) {
        var get = 'codelabels';
        var data = {
            'do': scope.app + '-custom_pdf_reminder-' + scope.app + '-pdfSaveDataReminder',
            'xget': get,
            'header': scope.codelabels.header,
            'footer': scope.codelabels.footer,
            'body': scope.codelabels.body,
            'layout': scope.codelabels.layout,
            'variable_data': scope.codelabels.variable_data,
            'identity_id': scope.codelabels.identity_id,
        };
        if (scope.codelabels.body == 'body') {
            data.body_l = scope.codelabels.body_id
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.codelabels = {
                make_id: r.data.make_id,
                make: r.data.make,
                selected_make: r.data.selected_make,
                variable_data: r.data.variable_data,
                layout: r.data.layout,
                header: r.data.header,
                footer: r.data.footer
            };
            scope.showAlerts(scope, r);
            scope.closeModal()
        })
    }
    fu

    function pdfSaveDataDelivery(scope, r) {
        var get = 'codelabels';
        var data = {
            'do': scope.app + '-custom_pdf-' + scope.app + '-pdfSaveData_Delivery',
            'xget': get,
            'header': scope.codelabels.header,
            'footer': scope.codelabels.footer,
            'layout': scope.codelabels.layout,
            'variable_data': scope.codelabels.variable_data,
            'identity_id': scope.codelabels.identity_id,
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.codelabels = {
                make_id: r.data.make_id,
                make: r.data.make,
                selected_make: r.data.selected_make,
                variable_data: r.data.variable_data,
                layout: r.data.layout,
                header: r.data.header,
                footer: r.data.footer
            };
            scope.showAlerts(scope, r);
            scope.closeModal()
        })
    }

    function Convention(scope, r) {
        var get = 'convention';
        var data = {
            'do': 'quote-settings-quote-Convention',
            'xget': get,
            'account_number': scope.convention.account_number,
            'account_digits': scope.convention.account_digits,
            'reference': scope.convention.reference,
            'del': scope.convention.del,
            'quote_weight': scope.convention.quote_weight,
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.convention = {
                account_number: r.data.account_number,
                account_digits: r.data.account_digits,
                example: r.data.example,
                reference: r.data.reference,
                del: r.data.del,
                quote_weight: r.data.quote_weight
            };
            scope.showAlerts(scope, r)
        })
    }

    function changelanguage(scope, r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            scope.general_condition = r.data.general_condition;
            scope.showAlerts(scope, r)
        })
    }

    function savelanguage(scope, languages, notes, terms) {
        var get = 'general';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-save_language',
            'xget': get,
            'languages': scope.general_condition.languages,
            'notes': scope.general_condition.notes,
            'terms': scope.general_condition.terms
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.general_condition = r.data.general_condition;
            scope.showAlerts(scope, r)
        })
    }

    function savelanguageNew(scope,formObj){
        formObj.$invalid=true;
        scope.notes_save_btn_txt=helper.getLanguage('Saved');
        var get = 'general';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-save_language',
            'xget': get,
            'languages': scope.general_condition.languages,
            'notes': scope.general_condition.notes,
            'terms': scope.general_condition.terms
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.general_condition = r.data.general_condition;
            scope.showAlerts(scope, r)
        })
    }

    function changelang(scope, r, t) {
        helper.doRequest('post', 'index.php', r, function(r) {
            scope.message = r.data.message;
            if (t) {
                scope.saved.save_button_text = getLanguage('Save');
                scope.saved.disabled_button = !1
            }
            scope.showAlerts(scope, r)
        })
    }

    function changelangdispatch(scope, r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            scope.dispatch_email = r.data.dispatch_email;
            scope.showAlerts(scope, r)
        })
    }

    function changeSaved(scope) {
        scope.saved.save_button_text = getLanguage('Save');
        scope.saved.disabled_button = !1;
        if (scope.general_condition) {
            scope.general_condition.save_button_text = getLanguage('Save');
            scope.general_condition.disabled_button = !1;
        }

    }

    function ok(scope, r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            scope.labels = r.data.labels;
            scope.showAlerts(scope, r);
            scope.cancel();
        })
    }

    function saveSetting(scope, r, t) {
        helper.doRequest('post', 'index.php', r, function(r) {
            scope.message = r.data.message;
            if (t) {
                scope.saved.save_button_text = helper.getLanguage('Saved');
                scope.saved.disabled_button = !0
            }
            scope.showAlerts(scope, r)
        })
    }

    function saveSettingdispatch(scope, r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            scope.dispatch_email = r.data.dispatch_email;
            scope.showAlerts(scope, r)
        })
    }

    function clickyes(scope, v1, v2) {
        var myValue = v1;
        if (scope.openedtags.indexOf(v1) > -1) {
            myValue = v2
        } else {
            scope.openedtags.push(v1)
        }
        var _this = angular.element('#altids')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            scope.message.text = scope.message.text.substring(0, startPos) + myValue + scope.message.text.substring(endPos, scope.message.text.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            scope.message.text += myValue;
            _this.focus()
        }
        if (myValue == v2) {
            var ind = scope.openedtags.indexOf(v1);
            scope.openedtags.splice(ind, 1)
        }
    }

    function saveDefaultEmail(scope, r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            scope.email_default = r.data.email_default;
            scope.showAlerts(scope, r)
        })
    }

    function updateweblink(scope, r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            scope.weblink = r.data.weblink;
            if (scope.app != 'quote') {
                scope.showAlerts(scope, r)
            }
        })
    }

    function articlefieldup(scope, r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            scope.articlefields = r.data.articlefields;
            scope.showAlerts(scope, r)
        })
    }

    function clicky(scope, myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            scope.articlefields.text = scope.articlefields.text.substring(0, startPos) + myValue + scope.articlefields.text.substring(endPos, scope.articlefields.text.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            scope.articlefields.text += myValue;
            _this.focus()
        }
    }

    function attachement_files(scope, e) {
        angular.element(e.target).parents('.upload-box1').find('.newUploadFile3').click()
    }

    function deleteatach(scope, id, name) {
        var get = 'attachments';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-deleteatach',
            'xget': get,
            'id': id,
            'name': name
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.atachment = r.data.atachment;
            scope.showAlerts(scope, r)
        })
    }

    function defaultcheck(scope, id, name, default_id) {
        var get = 'attachments';
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-defaultcheck',
            'xget': get,
            'id': id,
            'name': name,
            'default_id': default_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            scope.atachment = r.data.atachment;
            scope.showAlerts(scope, r)
        })
    }
}]).factory('selectizeFc', ['helper', function(helper) {
    return {
        configure: function(obj, text, remove, ls) {
            var cfg = {
                valueField: 'id',
                labelField: 'value',
                searchField: ['value'],
                delimiter: '|',
                placeholder: 'Select',
                create: !1,
                maxItems: 1,
            };
            angular.extend(cfg, obj);
            if (text) {
                cfg.render = {
                    option: function(item, escape) {
                        var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span class="text-ellipsis">' + item.symbol + '  ' + escape(item.top) + '&nbsp;' + escape(item.bottom) + '&nbsp;' + escape(item.right) + '&nbsp;</span>' + '' + '</div>' + '</div>' + '</div>';
                        if (item.id == '99999999999') {
                            html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + text + '</span></div>'
                        }
                        if (item.id == '0') {
                            html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Manage the list') + '</div>'
                        }
                        if (item.contact_id) {
                            html = str_replace('<span class="glyphicon glyphicon-map-marker"></span>', '', html)
                        }
                        return html
                    },
                    item: function(data, escape) {
                        return '<div class="item col-md-12 row">' + '<span class="text-ellipsis">' + escape(data.top) + '</span>' + '</div>'
                    }
                }
            } else {
                cfg.render = {
                    option: function(item, escape) {
                        var html = '<div  class="option " >' + item[cfg.labelField] + '</div>';
                        if (item.id == '0') {
                            if (item.name == helper.getLanguage('Main Identity') || item.name == helper.getLanguage('Never')) {} else {
                                html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Manage the list') + '</div>'
                            }
                        }
                        return html
                    },
                    item: function(data, escape) {
                        var html = '<div class="item">' + data[cfg.labelField] + '</div>';
                        return html
                    }
                }
            }
            return cfg
        }
    }
}]).filter('timelineSearch', function() {
    return function(items, search) {
        if (!items || !search) {
            return items
        }
        return items.filter(function(item) {
            if ((typeof item.serial_nr === 'string' || item.serial_nr instanceof String) && item.serial_nr.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                return true;
            } else if ((typeof item.your_ref === 'string' || item.your_ref instanceof String) && item.your_ref.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                return true;
            }
            return false;
        })
    }
}).filter('dealTimelineSearch', function() {
    return function(items, search) {
        if (!items || !search) {
            return items
        }
        return items.filter(function(item) {
            if ((typeof item.message === 'string' || item.message instanceof String) && item.message.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                return true;
            } else if ((typeof item.subject === 'string' || item.subject instanceof String) && item.subject.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                return true;
            } else if ((typeof item.your_ref === 'string' || item.your_ref instanceof String) && item.your_ref.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                return true;
            }
        })
    }
}).filter('trustThisUrl', ["$sce", function($sce) {
    return function(val) {
        return $sce.trustAsResourceUrl(val);
    };
}]).filter('generalSearch', function() {
    return function(items, search, property, equal) {
        if (!items || !search) {
            return items
        }
        return items.filter(function(item) {
            if ((typeof item[property] === 'string' || item[property] instanceof String) && !equal) {
                return item[property].toLowerCase().indexOf(search.toLowerCase()) !== -1
            }else if((typeof item[property] === 'string' || item[property] instanceof String) && equal){
                return item[property] == search
            }
        })
    }
}).filter('trustThisHtml', ["$sce", function($sce) {
    return function(html) {
        return $sce.trustAsHtml(html);
    };
}]).factory('ballFc', ['helper', function(helper) {
    return {
        configure: function(obj) {
            var cfg = {
                valueField: 'id',
                labelField: 'name',
                searchField: ['name'],
                delimiter: '|',
                placeholder: '',
                create: !1,
                maxItems: 1,
            };
            angular.extend(cfg, obj);
            cfg.render = {
                option: function(item, escape) {
                    var html = '<div  class="option"><div style="background-color:'+item[cfg.labelField]+'"></div></div>';
                    return html
                },
                item: function(data, escape) {
                    var html = '<div class="item"><div style="background-color:'+data[cfg.labelField]+'"></div></div>';
                    return html
                }
            }
            return cfg
        }
    }
}]);