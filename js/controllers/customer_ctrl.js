angular.module('customer').controller('CustomersCtrl', ['$scope', '$rootScope', '$state', 'helper', 'list', 'selectizeFc', 'modalFc', 'data', 'editItem',function($scope, $rootScope, $state, helper, list, selectizeFc, modalFc, data, editItem) {
    console.log('Customers list');
    $rootScope.opened_lists.customers = true;
    var vm = this;
    vm.ord = '';
    vm.reverse = !1;
    vm.max_rows = 0;
    vm.show = {
        advanced_search: !1
    };
    vm.app="general";
    vm.showTxt={};
    vm.acc_manager_cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Account Manager'),
        onChange(value) {
            vm.toggleSearchButtons('acc_manager_name', value)
        }
    });
    vm.type_cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Nature'),
        onChange(value) {
            vm.toggleSearchButtons('type', value)
        }
    });
    vm.type2_cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Type'),
        onChange(value) {
            vm.toggleSearchButtons('is_supplier', value)
        }
    });
    vm.c_type_cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Type of relationship'),
        onChange(value) {
            vm.toggleSearchButtons('c_type_name', value)
        }
    });
    vm.country_cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Country'),
        onChange(value) {
            vm.toggleSearchButtons('country_id', value)
        }
    });
    vm.select_cfg_lead = selectizeFc.configure({
        placeholder: helper.getLanguage('Lead Source'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            vm.checkSearch()
        }
    });
    vm.select_cfg_sector = selectizeFc.configure({
        placeholder: helper.getLanguage('Activity Sector'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            vm.checkSearch()
        }
    });
    vm.select_cfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            vm.checkSearch()
        }
    });
    vm.search = {
        search: '',
        'do': 'customers-customers',
        xget: '',
        showAdvanced: !1,
        showList: !0,
        offset: 1
    };
    vm.listObj = {
        check_add_all: !1
    };
    vm.list = [];
    vm.nav = [];
    vm.listStats = [];
    vm.export_args = '';
    vm.renderList = renderList;
    vm.toggleSearchButtons = toggleSearchButtons;
    vm.renderPage = renderPage;
    vm.searchThing = searchThing;
    vm.checkSearch = checkSearch;
    vm.filters = filters;
    vm.showArchivedList = showArchivedList;
    vm.showActiveList = showActiveList;

    function renderList(d) {
        if (d.data) {
            vm.list = d.data.list;
            vm.nav = d.data.nav;
            var ids = [];
            var count = 0;
            for(y in vm.nav){
                ids.push(vm.nav[y]['customer_id']);
                count ++;
            }
            //console.log(count);
            sessionStorage.setItem('customerView_total', count);
            sessionStorage.setItem('customerView_list', JSON.stringify(ids));
            //console.log(ids);
            vm.max_rows = d.data.max_rows;
            vm.lr = d.data.lr;
            vm.listStats = [];
            vm.export_args = d.data.export_args;
            vm.minimum_selected=d.data.minimum_selected;
            vm.all_pages_selected=d.data.all_pages_selected;
            vm.showAlerts(d)
        }
    }

    function toggleSearchButtons(item, val) {
        list.tSearch(vm, item, val, vm.renderList);
        vm.checkSearch()
    };

    function renderPage(d) {
        if (d.data) {
            for (x in d.data) {
                vm[x] = d.data[x]
            }
            /*var ids = [];
            var count = 0;
            for(y in vm.nav){
                ids.push(vm.nav[y]['customer_id']);
                count ++;
            }
            sessionStorage.setItem('customerView_total', count);
            sessionStorage.setItem('customerView_list', JSON.stringify(ids));*/
        }
        if(d.data.showGdpr){
            vm.openModal("GDPRPolicy", {})
        }
    }

    function searchThing(reset_list) {
        angular.element('.loading_wrap').removeClass('hidden');
        var params=angular.copy(vm.search);
        if(reset_list){
            params.reset_list='1';
        }
        list.search(params, vm.renderList, vm)
    }

    function checkSearch() {
        var data = angular.copy(vm.search),
            is_search = !1;
        data = helper.clearData(data, ['do', 'xget', 'showAdvanced', 'showList', 'offset']);
        for (x in data) {
            if (data[x]) {
                is_search = !0
            }
        }
        //vm.search.showList = is_search
    }

    function filters(p) {
        list.filter($scope, p, $scope.renderList)
    }

    function showArchivedList(v) {
        vm.search.archived = v;
        vm.searchThing(true);
        vm.checkSearch()
    }

    function showActiveList(v) {
        if (v.data) {
            for (x in v.data) {
                vm[x] = v.data[x]
            }
            if(v.data.showGdpr){
                vm.openModal("GDPRPolicy", {})
            }
        }
        vm.search.active = 1;
        vm.searchThing();
        vm.checkSearch()
    }
    vm.closeAdvanced = function() {
        var exclude = ['do', 'xget', 'showAdvanced', 'showList'];
        for (x in vm.search) {
            if (exclude.indexOf(x) == -1) {
                vm.search[x] = ''
            }
        }
        vm.toggleSearchButtons('showAdvanced', !1)
    }
    vm.action = function(item, action) {
        list.action(vm, item, action, vm.renderList)
    };
    vm.orderBY = function(p) {
        list.orderBy(vm, p, vm.renderList)
    };
    vm.columnSettings = function() {
        vm.openModal('column_set', {}, 'lg')
    }
    vm.openModal = function(type, item, size) {
        var params = {
            template: 'cTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'EditCustomer':
                params.params = {
                    'do': 'customers-' + type,
                    customer_id: item
                };
                params.template = 'EditCustomerModal';
                params.callbackExit = function(data) {
                    if (data && typeof(data) == 'object') {}
                }
                break;
            case 'column_set':
                params.template = 'miscTemplate/ColumnSettingsModal';
                params.ctrl = 'ColumnSettingsModalCtrl';
                params.params = {
                    'do': 'misc-column_set',
                    'list': 'accounts'
                };
                params.ctrlAs = 'vm';
                params.callback = function(d) {
                    if (d) {
                        helper.doRequest('get', 'index.php', {
                            'do': 'customers-customersDashboard',
                            'xget': 'accounts_cols'
                        }, function(o) {
                            vm.columns = o.data;
                            vm.searchThing()
                        })
                    }
                }
                break;
            case 'GDPRPolicy':
                var iTemplate = 'GDPRPolicy';
                var ctas = 'vm';
                var params = {
		            template: 'miscTemplate/' + iTemplate + 'Modal',
		            ctrl: iTemplate + 'ModalCtrl',
		            ctrlAs: ctas,
		            size: size
		        };
		        params.backdrop = 'static';
                params.params = {
                    'do': 'misc-policy_info'
                };
                break;
             case 'SmartListAdd':
                var iTemplate = 'SmartListAdd';
                var ctas = 'ls';
                var params = {
                    template: 'cTemplate/' + iTemplate + 'Modal',
                    ctrl: iTemplate + 'ModalCtrl',
                    size: size,
                    backdrop: 'static'
                };
                params.callback = function(data) {
                    $state.go(data.state, {
                        contact_list_id: data.contact_list_id
                    })
                }
                break   
        }
        modalFc.open(params)
    }

    vm.showAlerts = function(d, formObj) {
        helper.showAlerts(vm, d, formObj)
    };
    vm.showGraph = function(p) {
        var data = angular.copy(p);
        data.do = 'customers-customersDashboard-customers-showGraph';
        helper.doRequest('post', 'index.php', data, vm.renderPage)
    }

    vm.createCustomer=function(){
        var main_obj = {
            country_dd: [],
            country_id: 0,
            add_customer: !0,
            'do': 'misc--misc-tryAddC',
        };
        editItem.openModal(vm, 'AddCustomer', main_obj, 'md');
    }

    vm.createContact=function(){
        var main_obj = {
            country_dd: [],
            country_id: 0,
            add_contact: !0,
            contact_only: !0,
            'do': 'misc--misc-tryAddC',
        };
        editItem.openModal(vm, 'AddCustomer', main_obj, 'md');
    }

    vm.initPage=function(d){
        vm.renderPage(d);
        vm.searchThing();
    }

    vm.markCheckNew=function(i){
        list.markCheckNew(vm, 'customers--customers-saveAddToArchived', i);
    }
    vm.checkAllPage=function(){
        vm.listObj.check_add_all=true;
        vm.markCheckNew();
    }
    vm.checkAllPages=function(){
        list.markCheckNewAll(vm, 'customers--customers-saveAddToArchivedAll','1');
    }
    vm.uncheckAllPages=function(){
        list.markCheckNewAll(vm, 'customers--customers-saveAddToArchivedAll','0');
    }
    vm.archiveBulkCustomers = function() {
        var obj = angular.copy(vm.search);
        obj.do = 'customers-customers-customers-archiveBulkCustomers';
        helper.doRequest('post', 'index.php', obj, function(r) {
            vm.showAlerts(r);
            if (r.success) {
                vm.renderList(r)
            }
        })
    }
    vm.restoreBulkCustomers = function() {
        var obj = angular.copy(vm.search);
        obj.do = 'customers-customers-customers-restoreBulkCustomers';
        helper.doRequest('post', 'index.php', obj, function(r) {
            vm.showAlerts(r);
            if (r.success) {
                vm.renderList(r)
            }
        })
    }

    list.init(vm, 'customers-customersDashboard', vm.initPage, ($rootScope.reset_lists.customers ? true : false))
    $rootScope.reset_lists.customers = false;
    /*if (data.data.select_company == !0) {
        list.init(vm, 'customers-customersDashboard', vm.showActiveList)
    } else {
        list.init(vm, 'customers-customersDashboard', vm.renderPage)
    }*/
}]).controller('CustomerCtrl', ['$scope', '$stateParams', '$state', '$timeout', '$cacheFactory', 'helper', 'data', 'selectizeFc', 'modalFc', function($scope, $stateParams, $state, $timeout, $cacheFactory, helper, data, selectizeFc, modalFc) {
    angular.element('body').on('click', '.fa-trash', function(e) {
        e.stopPropagation();
        e.preventDefault()
    })

    var ls = this,
        triedToSave = !1,
        typePromise;
    var old_ls = {};
    ls.openModal = openModal;
    ls.customer_id = $stateParams.customer_id;
    if (ls.customer_id == 'tmp') {
        ls.addType = 'customer'
    }
    ls.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select type'),
        labelField: 'name',
        searchField: ['name'],
    });
    ls.currencyCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            if (value) {
                ls.obj.currency_code = this.options[value].code
            }
        }
    });
    ls.auto = {
        options: {
            accountManager: data.accountManager,
        }
    }
    for (x in data) {
        ls[x] = data[x]
    }
    for (x in data.extra_dd) {
        ls.extra_dd[x] = data.extra_dd[x]
    }
    helper.updateTitle($state.current.name,ls.obj.name);
    ls.customerTypeCfg = selectizeFc.configure({
        labelField: 'name',
        placeholder: helper.getLanguage('Select type'),
        searchField: ['name'],
        maxOptions: 50,
        maxItems: 50,
        create: !1
    });
    ls.salesCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 100,
        maxItems: 1,
        create: !1,
        onDropdownOpen($dropdown) {
            var _this = this,
                model = _this.$input[0].attributes.model.value;
            if (ls.obj[model] != undefined && ls.obj[model] != '0') {
                old_ls[model] = ls.obj[model]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                ls[realModel] = o.data.lines;
                ls.obj[realModel] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            if (ls.obj[model] != undefined && ls.obj[model] != '0') {
                old_ls[model] = ls.obj[model]
            }
            if (item == '0') {
                ls.openModal('selectEdit', {
                    table: table,
                    model: model
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                ls.obj[model] = undefined;
                return !1
            }
        }
    }, !1, !0, ls);
    ls.salesCfg2 = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 50,
        create: !1,
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                ls[realModel] = o.data.lines;
                ls.obj[realModel] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            if (item == '0') {
                ls.openModal('selectEdit', {
                    table: table,
                    model: model
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                ls.obj[model] = undefined;
                return !1
            }
        }
    }, !1, !0, ls);
    ls.extraCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 1000,
        maxItems: 1,
        create: !1,
        onDropdownOpen($dropdown) {
            var _this = this,
                extra_id = _this.$input[0].attributes.extra.value;
            if (ls.obj.extra[extra_id] != undefined && ls.obj.extra[extra_id] != '0') {
                old_ls['extra_' + extra_id] = ls.obj.extra[extra_id]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                ls[realModel][extra_id] = o.data.lines;
                ls.obj[realModel][extra_id] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = _this.$input[0].attributes.extra.value;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            var extra_id = _this.$input[0].attributes.extra.value;
            if (ls.obj.extra[extra_id] != undefined && ls.obj.extra[extra_id] != '0') {
                old_ls['extra_' + extra_id] = ls.obj.extra[extra_id]
            }
            if (item == '0') {
                ls.openModal('selectEdit', {
                    table: table,
                    model: model,
                    extra_id: extra_id
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                ls.obj.extra[extra_id] = undefined;
                return !1
            }
        }
    }, !1, !0, ls);
    ls.ok = ok;
    ls.showAlerts = showAlerts;
    ls.activeteTab = activeteTab;
    ls.checkVat = checkVat;
    ls.reset = reset;
    ls.save_button_text = helper.getLanguage('Save');
    ls.renderPage = renderPage;
    ls.cancel = function() {
        $state.go('customers')
    }
    ls.checkForm = function(formObj) {
        if (formObj.$invalid) {
            return !0
        } else {
            ls.save_button_text = helper.getLanguage('Save');
            return !1
        }
    }

    function ok(formObj) {
        var params = angular.copy(ls.obj);
        data = helper.clearData(data, ['country_dd']);
        ls.alertsField = {};
        params.do = data.obj.do;
        if (formObj.$name == 'financial') {
            params.do = data.obj.do_financial
        }
        if (formObj.$name == 'notes') {
            params.do = data.obj.do_notes
        }
        formObj.$invalid = !0;
        ls.save_button_text = helper.getLanguage('Saved');
        helper.doRequest('post', 'index.php', params, function(d) {
            if (d && d.data && d.data.obj.customer_id && d.success) {

                $state.go('customerView', {
                    customer_id: d.data.obj.customer_id
                });
                return
            } else if (d.success) {
                ls.showAlerts(d)
            } else if (d.error) {
                ls.showAlerts(d, formObj)
            }
        })
    };

    function showAlerts(d, formObj) {
        helper.showAlerts(ls, d, formObj)
    }

    function openModal(type, item, size, obj, extra_id) {
        if (type == 'ManageAddresses') {
            var params = {
                template: 'cTemplate/' + type + 'Modal',
                ctrl: type + 'ModalCtrl',
                size: size,
                backdrop: 'static'
            }
        } else if (type == 'selectEdit') {
            var params = {
                template: 'miscTemplate/' + type + 'Modal',
                ctrl: type + 'ModalCtrl',
                size: size,
                backdrop: 'static'
            }
        } else if (type == 'VatCompany') {
            var params = {
                template: 'cTemplate/' + type + 'Modal',
                ctrl: type + 'ModalCtrl',
                size: size,
                backdrop: 'static'
            }
        } else {
            var params = {
                template: 'miscTemplate/' + type + 'Modal',
                ctrl: type + 'ModalCtrl',
                size: size,
                backdrop: 'static'
            }
        }
        switch (type) {
            case 'vies':
                params.params = {
                    'do': 'customers-EditCustomer-customers-check_valid_vat_number',
                    'account_vat_number': item,
                    'customer_id': ls.customer_id
                };
                params.callbackExit = function(d) {
                    ls.reset();
                    if ((d.error || d.notice) && ls.obj.btw_nr) {
                        ls.isFalse = !0
                    }
                    if (d.success && ls.obj.btw_nr) {
                        ls.isOk = !0;
                        ls.obj.vies_ok = !0
                    }
                }
                break;
            case 'ManageAddresses':
                params.params = {
                    'do': 'customers-ViewCustomer',
                    'customer_id': item,
                    'xget': 'customerAddresses'
                };
                params.callback = function(data) {
                    var data = {
                        'do': 'customers-ViewCustomer',
                        'customer_id': ls.customer_id,
                        'xget': 'customerAddresses'
                    };
                    helper.doRequest('get', 'index.php', data, function(r) {
                        if (r && r.data) {
                            ls.addresses = r.data
                        }
                    })
                }
                break;
            case 'selectEdit':
                params.params = {
                    'do': 'misc-selectEdit',
                    'table': item.table,
                    'extra_id': item.extra_id
                };
                params.callback = function(data) {
                    if (data.data) {
                        if (item.extra_id) {
                            ls.extra[item.extra_id] = data.data.lines;
                            if (old_ls['extra_' + item.extra_id]) {
                                ls.obj.extra[item.extra_id] = old_ls['extra_' + item.extra_id]
                            }
                        } else {
                            ls[item.table] = data.data.lines;
                            if (old_ls[item.model]) {
                                ls.obj[item.model] = old_ls[item.model]
                            }
                        }
                    } else {
                        if (item.extra_id) {
                            ls.extra[item.extra_id] = data.lines;
                            if (old_ls['extra_' + item.extra_id]) {
                                ls.obj.extra[item.extra_id] = old_ls['extra_' + item.extra_id]
                            }
                        } else {
                            ls[item.table] = data.lines;
                            if (old_ls[item.model]) {
                                ls.obj[item.model] = old_ls[item.model]
                            }
                        }
                    }
                }
                break;
            case 'VatCompany':
                params.params = {
                    'do': 'customers-EditCustomer',
                    'exist_company': item
                };
                break;
            case 'vies_info':
                params.template = 'miscTemplate/viesModal';
                params.ctrl = 'viesModalCtrl';
                params.ctrlAs = 'vmMod';
                params.item = {
                    'address': item.comp_address,
                    'city': item.comp_city,
                    'country_id': item.comp_country,
                    'country_name': item.comp_country_name,
                    'country_dd': item.country_dd,
                    'name': item.comp_name,
                    'zip': item.comp_zip,
                    'customer_id': item.customer_id,
                    'country_code': item.full_details.countryCode,
                    'vat': item.full_details.vatNumber
                };
                break
        }
        modalFc.open(params)
    }

    function activeteTab(i) {
        ls.obj.add_customer = !1;
        ls.obj.add_contact = !1;
        ls.obj.add_notes = !1;
        ls.save_button_text = helper.getLanguage('Saved');
        ls.obj[i] = !0
    }

    function checkVat() {
        ls.reset();
        ls.checking = !0;
        if (!ls.obj.btw_nr) {
            ls.checking = !1;
            return
        }
        ls.openModal('vies', ls.obj.btw_nr, 'md')
    }

    function reset() {
        ls.checking = !1;
        ls.isOk = !1;
        ls.isFalse = !1;
        ls.isTrends = !1;
        ls.obj.vies_ok = !1
    }

    function renderPage(d) {
        ls.reset();
        if ((d.error || d.notice) && ls.obj.btw_nr) {
            ls.isFalse = !0
        }
        if (d.success && ls.obj.btw_nr) {
            if (d.data.trends_ok) {
                ls.isTrends = !0
            } else {
                ls.isOk = !0;
                ls.obj.vies_ok = !0
            }
            ls.parseData(d)
        }
    }
    ls.parseData = function(d) {
        ls.obj.name = d.data.comp_name;
        ls.obj.address = d.data.comp_address;
        ls.obj.city = d.data.comp_city;
        ls.obj.zip = d.data.comp_zip;
        ls.obj.country_id = d.data.comp_country ? d.data.comp_country : 26
    }
    ls.changeVatRegim = function() {
        switch (ls.obj.vat_regime_id) {
            case "1":
                ls.obj.no_vat = 0;
                break;
            case "2":
                ls.obj.no_vat = 0;
                ls.obj.vat_id = ls.vat_0;
                if (!ls.obj.invoice_note2) {
                    helper.doRequest('get', 'index.php?do=customers-EditCustomer&xget=message_intra_eu&lang_id=' + ls.obj.internal_language + '&table=' + ls.internal_language_table, function(r) {
                        if (r && r.data) {
                            ls.obj.invoice_note2 = r.data.message
                        }
                    })
                }
                break;
            default:
                ls.obj.no_vat = 1;
                break
        }
    }
    ls.check_for_btw = function() {
        if (ls.obj.btw_nr) {
            helper.doRequest('get', 'index.php?do=customers-EditCustomer&xget=check_for_btw&customer_id=' + ls.customer_id + '&btw_nr=' + ls.obj.btw_nr, function(r) {
                if (r) {
                    ls.showAlerts(r)
                }
            })
        }
    }
    ls.attachement_files = function(scope, e) {
        angular.element(e.target).parents('.upload-box1').find('.newUploadFile3').click()
    }
    angular.element('.newUploadFile3').on('click', function(e) {
        e.stopPropagation()
    }).on('change', function(e) {
        e.preventDefault();
        var _this = $(this),
            p = _this.parents('.upload-box1'),
            x = p.find('.newUploadFile3');
        if (x[0].files.length == 0) {
            return !1
        }
        p.find('.img-thumbnail1').attr('src', '');
        var tmppath = URL.createObjectURL(x[0].files[0]);
        p.find('.img-thumbnail1').attr('src', tmppath);
        p.find('.btn').addClass('hidden');
        p.find('.nameOfTheFile1').text(x[0].files[0].name);
        var formData = new FormData();
        formData.append("Filedata", x[0].files[0]);
        formData.append("do", 'customers--customers-atach');
        formData.append("customer_id", ls.customer_id);
        p.find('.upload-progress-bar1').css({
            width: '0%'
        });
        p.find('.upload-file1').removeClass('hidden');
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "index.php", !0);
        oReq.upload.addEventListener("progress", function(e) {
            var pc = parseInt(e.loaded / e.total * 100);
            p.find('.upload-progress-bar1').css({
                width: pc + '%'
            })
        });
        oReq.onload = function(oEvent) {
            var res1 = JSON.parse(oEvent.target.response);
            p.find('.upload-file1').addClass('hidden');
            p.find('.btn').removeClass('hidden');
            if (oReq.status == 200) {
                if (res1.success) {
                    helper.doRequest('get', 'index.php?do=customers-EditCustomer&customer_id=' + ls.customer_id + '', function(r) {
                        for (x in r.data) {
                            ls[x] = r.data[x]
                        }
                        triedToSave = !0
                    })
                } else {
                    var msg = {
                        'error': {
                            'error': file.name + ': ' + res1.error
                        },
                        'notice': !1,
                        'success': !1
                    };
                    ls.showAlerts(msg)
                }
            } else {
                var msg = {
                    'error': {
                        'error': "Error " + oReq.status + " occurred when trying to upload your file."
                    },
                    'notice': !1,
                    'success': !1
                };
                ls.showAlerts(msg)
            }
        };
        oReq.send(formData)
    });
    ls.deleteatach = function(scope, id, name, formObj) {
        var data = {
            'do': 'customers-EditCustomer-customers-deleteatach',
            'id': id,
            'name': name,
            'customer_id': ls.customer_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            formObj.$invalid = !0;
            for (x in r.data) {
                ls[x] = r.data[x]
            }
            ls.showAlerts(r, formObj)
        })
    }
    ls.defaultcheck = function(scope, id, name, default_id, formObj) {
        formObj.$invalid = !0;
        var data = {
            'do': 'customers-EditCustomer-customers-defaultcheck',
            'id': id,
            'name': name,
            'default_id': default_id,
            'customer_id': ls.customer_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            for (x in r.data) {
                ls[x] = r.data[x]
            }
            ls.showAlerts(r, formObj)
        })
    }
    ls.checkVatAdd = function() {
        ls.reset();
        ls.checking = !0;
        ls.showViesUnavailable=false;
        if (!ls.obj.btw_nr) {
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            ls.checking = !1;
            return
        }
        if (typePromise) {
            $timeout.cancel(typePromise)
        }
        typePromise = $timeout(function() {
            if (ls.obj.btw_nr) {
                var data = {
                    'do': 'misc--misc-check_vies_vat_number',
                    value: ls.obj.btw_nr,
                    customer_id: ls.customer_id
                };
                helper.doRequest('post', 'index.php', data, function(r) {
                    ls.renderPage(r);
                    if (r.success) {
                        if (ls.customer_id != 'tmp') {
                            openModal('vies_info', r.data, 'md')
                        }
                        if (r.data.exist_company) {
                            openModal('VatCompany', r.data.exist_company, {
                                'do': 'customers-EditCustomer'
                            }, 'md')
                        }
                        if (r.data.customer_legal_type_id) {
                            ls.customer_legal_type = r.data.customer_legal_type;
                            ls.obj.legal_type = r.data.customer_legal_type_id
                        }
                    }else if(r.notice){
                        ls.showViesUnavailable=true;
                    }
                })
            } else {
                ls.reset()
            }
        }, 300)
    }
    var cacheCustomer = $cacheFactory.get('ViewToCustomer');
    if (cacheCustomer != undefined) {
        var cachedDataCustomer = cacheCustomer.get('data');
        if (cachedDataCustomer != undefined) {
            ls.obj.name = cachedDataCustomer.name;
            ls.obj.address = cachedDataCustomer.address;
            ls.obj.city = cachedDataCustomer.city;
            ls.obj.zip = cachedDataCustomer.zip;
            ls.obj.country_id = cachedDataCustomer.country_id ? cachedDataCustomer.country_id : 26;
            cacheCustomer.remove('data')
        }
    }
}]).controller('CustomerViewCtrl', ['$scope', '$rootScope','$stateParams', '$state', '$timeout', 'helper', 'data', 'selectizeFc', 'modalFc', 'list', '$window', 'editItem',function($scope, $rootScope, $stateParams, $state, $timeout, helper, data, selectizeFc, modalFc, list, $window,editItem) {
    console.log('Customer view');
    angular.element('.loading_wrap').addClass('hidden');
    var ls = this,
        typePromise, currentTime = new Date(),
        current = undefined;
    ls.customer_id = $stateParams.customer_id;

    if(!data.item_exists){
        ls.item_exists = false;
        return 0;
    }; 

    ls.app = 'customers';
    ls.app_mod = 'view';
    ls.showMoreField = !1;
    ls.showMoreInstalField = !1;
    ls.showAddresses = !1;
    ls.showActivityForm = !1;
    ls.showActivityiny = !1;
    ls.activityTxt = helper.getLanguage('Type');
    ls.activity = {
        hd_events: !1
    }
    ls.activityNumber = 5;
    ls.activityLoaded = !1;
    ls.hideSwitch = !1;
    ls.tracking_o_doc = [{
        'id': '0',
        'name': helper.getLanguage('All Original Documents')
    }, {
        'id': '1',
        'name': helper.getLanguage('Invoice')
    }, {
        'id': '2',
        'name': helper.getLanguage('Quote')
    }, {
        'id': '3',
        'name': helper.getLanguage('Project')
    }, {
        'id': '4',
        'name': helper.getLanguage('Order')
    }, {
        'id': '5',
        'name': helper.getLanguage('Intervention')
    }, {
        'id': '6',
        'name': helper.getLanguage('Purchase Order')
    }, {
        'id': '7',
        'name': helper.getLanguage('Proforma')
    }, {
        'id': '8',
        'name': helper.getLanguage('Credit Invoice')
    }, {
        'id': '10',
        'name': helper.getLanguage('Purchase Invoice')
    }];

    ls.search_track = {
        search: '',
        'do': 'customers-tracking',
        view: 0,
        offset: 1,
        start_date: '',
        end_date: '',
        original_d: '0',
        target_d: '0',
        customer_id: ls.customer_id
    };
    ls.tracking = {};
    ls.search_site = {
        search: '',
        'do': 'customers-sites',
        view: 1,
        offset: 1,
        customer_id: ls.customer_id,
        address_id: undefined
    };
    ls.search_instal = {
        search: '',
        'do': 'customers-sites',
        'xget': 'Installations',
        offset: 1,
        address_id: undefined
    };
    ls.instals = {};
    ls.instal_data = {};
    ls.load_instal = !1;
    ls.search = {
        search: '',
        'do': 'customers-ViewCustomer',
        xget: 'customPrices',
        offset: 1,
        customer_id: ls.customer_id,
        supplier_reference: '',
        name: '',
        active: 1
    };
    ls.quickAdd = !1;
    angular.element('body').on('click', function(e) {
        if (angular.element(e.target).parents('.content').length == 0) {
            ls.quickAdd = !1
        } else {
            if (angular.element(e.target).parents('.uo-quick_add').length == 0) {
                ls.quickAdd = !1
            }
        }
    });
    ls.datePickOpen = {
        start_date: !1,
        end_date: !1
    };
    ls.activityParams = {
        'do': 'misc-activities',
        'customer_id': ls.customer_id,
        'xget': 'Activity',
        filter_type: 'customer'
    };
    if (ls.customer_id == 'tmp') {
        ls.addType = 'customer'
    }
    ls.format = 'dd/MM/yyyy';
    ls.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    ls.hours = [];
    ls.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    ls.currencyCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            if (value) {
                ls.obj.currency_code = this.options[value].code
            }
        }
    });
    ls.activitiesType = [{
        id: '2',
        value: helper.getLanguage('Note'),
        custom_class: 'fa fa-sticky-note'
    }, {
        id: '5',
        value: helper.getLanguage('Call'),
        custom_class: 'fa fa-phone'
    }, {
        id: '4',
        value: helper.getLanguage('Meeting'),
        custom_class: 'fa fa-calendar'
    }, {
        id: '6',
        value: helper.getLanguage('Task'),
        custom_class: 'akti-icon o_akti_checklist'
    }];
    ls.activityCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Type'),
        create: !1,
        maxItems: 1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline activities_type_dd">' + '<div class="row">' + '<div class="col-sm-2">' + '<a href="#"><span class="' + item.custom_class + '"></span></a>' + '</div>' + '<div class="col-sm-9">' + '<span>' + escape(item.value) + '&nbsp;</span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            ls.activityParams['activity_id'] = value;
            helper.doRequest('get', 'index.php', ls.activityParams, ls.renderActivities);
        }
    };
    ls.thirdAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        maxOptions: 6,
        render:{
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-8">' + '<strong class="text-ellipsis">' + item.symbol + '  ' + escape(item.top) + '</strong>' + '<small class="text-ellipsis"><span class="glyphicon glyphicon-map-marker"></span>' + escape(item.bottom) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-muted text-right">' + '<small class="text-ellipsis"> ' + escape(item.right) + '</small>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.top) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#third_party')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'customers-ViewCustomer',
                    'xget': 'third_customers',
                    term: str
                };
                helper.doRequest('get', 'index.php', data, function(d) {
                    if (d.data) {
                        ls.third_party_dd = d.data;        
                        selectize.open();
                    }
                })
            }, 300)
        },
        maxItems: 1
    };
    for (var i = 0; i <= 23; i++) {
        for (var j = 0; j <= 3; j++) {
            var min = j * 15,
                text = (i < 10 ? '0' + i : i) + ':' + (min < 10 ? '0' + min : min);
            ls.hours.push({
                'id': i + '-' + min,
                'value': text
            });
            if (i <= currentTime.getHours() && min <= currentTime.getMinutes()) {
                current = i + '-' + min
            }
        }
    }
    for (x in data) {
        ls[x] = data[x]
    }
    helper.updateTitle($state.current.name,ls.obj.name);
    ls.save_button_text = 'Save';
    ls.save_button_text_financial = 'Save';
    ls.show_disabled = !1;
    ls.filterCfgFam = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Families'),
        create: !1,
        onChange(value) {
            ls.toggleSearchButtons('article_category_id', value)
        },
        maxItems: 1
    };
    ls.renderActivities = function(r) {
        if (r.data) {
            for (x in r.data) {
                ls[x] = r.data[x]
            }
            ls.activity.location_meet = ls.activities.location_meet;
            ls.showAlerts(r);
            ls.activityLoaded = !0
        }
    }
    ls.showAlerts = function(d, formObj) {
        helper.showAlerts(ls, d, formObj)
    }
    ls.openModal = function(type, item, size) {
        var params = {
            template: 'cTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'ContactEdit':
            case 'ContactView':
                params.ctrl = 'ContactCtrl';
                params.ctrlAs = 'ls';
                params.template = 'cTemplate/' + type + 'Modal/contactHtml';
                params.params = {
                    'do': 'customers-Contact',
                    'contact_id': item,
                    'customer_id': ls.customer_id,
                    'viewMod': !0
                };
                params.callback = function(data) {
                    helper.doRequest('get', 'index.php', ls.vm.search, ls.vm.renderList)
                }
                break;
            case 'ManageAddresses':
                params.params = {
                    'do': 'customers-ViewCustomer',
                    'customer_id': item,
                    'xget': 'customerAddresses'
                };
                params.callback = function(data) {
                    var data = {
                        'do': 'customers-ViewCustomer',
                        'customer_id': ls.customer_id
                    };
                    helper.doRequest('get', 'index.php', data, function(r) {
                        if (r && r.data) {
                            ls.addresses.list = r.data.addresses.list;
                            ls.addresses.count = r.data.addresses.count;
                            ls.map = {
                                center: r.data.addresses.coords,
                                zoom: 12,
                                marker_id: 1,
                                refresh: !0,
                                marker_coord: r.data.addresses.coords
                            };
                            ls.address_primary = r.data.address_primary;
                            ls.country_primary = r.data.country_primary;
                            ls.city_primary = r.data.city_primary;
                            ls.zip_primary = r.data.zip_primary;
                            ls.full_address_map_src=r.data.full_address_map_src;              
                        }
                    })
                }
                break;
            case 'CustomerAddress':
                params.params = {
                    'do': 'customers-ViewCustomer',
                    'address_id': item,
                    'customer_id': ls.customer_id,
                    'xget': 'customerAddress'
                };
                params.callback = function(data) {
                    var data = {
                        'do': 'customers-ViewCustomer',
                        'customer_id': ls.customer_id,
                        'xget': 'customerAddresses'
                    };
                    helper.doRequest('get', 'index.php', data, function(r) {
                        if (r && r.data) {
                            ls.addresses = r.data;
                            ls.map.marker_coord = ls.addresses.coords
                        }
                    })
                }
                break;
            case 'NewActivities':
                params.template = 'miscTemplate/' + type + 'Modal/';
                var newData = {};
                newData.user_id=ls.activities.user_id;
                newData.customer_id = ls.customer_id;
                newData.reminder_dd=ls.reminder_dd;
                newData.filter_type = 'customer';
                newData.disable_customer=true;
                newData.account_date_format = ls.account_date_format;
                newData.user_auto=ls.activities.user_auto;
                params.item = newData;
                params.callback = function(r) {
                    ls.renderActivities(r)
                }
                break;
            case 'EditActivity':
                var newData = angular.copy(item);
                newData.customer_id = ls.customer_id;
                newData.reminder_dd=ls.reminder_dd;
                newData.account_date_format = ls.account_date_format;
                newData.user_auto=ls.activities.user_auto;
                newData.allow_delete=true;
                newData.filter_type = 'customer';
                params.item = newData;
                params.template = 'miscTemplate/' + type + 'Modal/';
                params.callback = function(r) {
                    //ls.renderActivities(r)
                    helper.doRequest('get', 'index.php', ls.activityParams, ls.renderActivities);
                }
                break;
            case 'addArticleCustomPrice':
                params.ctrlAs = 'vmMod';
                params.params = {
                    'do': 'misc-addArticleCustomPrice',
                    customer_id: ls.customer_id,
                    lang_id: 1,
                    remove_vat: item.remove_vat
                };
                params.template = 'miscTemplate/addArticleCustomPriceModal';
                params.callback = function(r) {
                    ls.renderList(r)
                }
                break;
            case 'ModelTracking':
                var newData = angular.copy(item);
                newData.customer_id = ls.customer_id;
                newData.filter_type = 'customer';
                params.item = newData;
                params.template = 'cTemplate/' + type + 'Modal/';
                console.log(params.template);
                break;
            case 'ModuleTracking':
                params.ctrlAs = 'vmMod';
                params.params = {
                    'do': 'customers-tracking',
                    'xget': 'More',
                    'id': item.id,
                    'type': item.type_nr,
                    'customer_id': ls.customer_id
                };
                break;
            case 'NylasEmail':
                params.ctrlAs = 'vmMod';
                params.params = {
                    'do': 'customers-nylas_email',
                    'customer_id': ls.customer_id
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                params.callback = function() {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php', ls.activityParams, ls.renderActivities)
                }
                break;
            case 'vies_info':
                params.template = 'miscTemplate/viesModal';
                params.ctrl = 'viesModalCtrl';
                params.ctrlAs = 'vmMod';
                params.item = {
                    'address': item.comp_address,
                    'city': item.comp_city,
                    'country_id': item.comp_country,
                    'country_name': item.comp_country_name,
                    'country_dd': item.country_dd,
                    'name': item.comp_name,
                    'zip': item.comp_zip,
                    'customer_id': item.customer_id,
                    'country_code': item.full_details.countryCode,
                    'vat': item.full_details.vatNumber
                };
                break;
            case 'VatCompany':
                params.template = 'cTemplate/' + type + 'Modal';
                params.ctrl = type + 'ModalCtrl';
                params.size = size;
                params.params = {
                    'do': 'customers-EditCustomer',
                    'exist_company': item
                };
                break;
            case 'stock_info':
                params.template = 'miscTemplate/modal';
                params.ctrl = 'viewRecepitCtrl';
                params.ctrlAs = 'vmMod';
                params.item = item;
                break;
            case 'allo_call':
                params.template = 'miscTemplate/AlloCallModal';
                params.ctrl = 'AlloCallModalCtrl';
                params.ctrlAs = 'vmMod';
                params.size = size;
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        ls.showAlerts(d);
                        if (d.success) {
                           // angular.element('.loading_wrap').removeClass('hidden');
                            helper.doRequest('get', 'index.php', ls.activityParams, ls.renderActivities)
                        }
                    }
                }
                break;
            case 'ZenComment':
                params.template = 'miscTemplate/ZenCommentModal';
                params.ctrlAs = 'ls';
                //angular.element('.loading_wrap').removeClass('hidden');
                params.params = {
                    'do': 'misc-zen_comments',
                    'ticket_id': item
                };
                break
        }
        modalFc.open(params)
    }
    ls.checkForm = function(formObj) {

        if (formObj.$invalid) {
            return !0
        } else {
            ls.save_button_text = 'Save';
            return !1
        }
    }
     ls.checkFormFinancial = function(formObj) {
         
        if (formObj.$invalid ) {         
            return !0
        } else {
            ls.save_button_text_financial = 'Save';   
             return !1      
        }
    }
    ls.ok = function(formObj) {
        var params = angular.copy(ls.obj);
        data = helper.clearData(data, ['country_dd']);
        ls.alertsField = {};
        params.do = data.obj.do;
        if (formObj.$name == 'financial') {
            params.do = data.obj.do_financial;
            ls.save_button_text_financial = 'Saved';
            ls.show_disabled = 1;
        }
        if (formObj.$name == 'notes') {
            params.do = data.obj.do_notes;  
            ls.save_button_text = 'Saved';       
        }
        formObj.$invalid = !0;

        helper.doRequest('post', 'index.php', params, function(d) {

            if (ls.customer_id == 'tmp' && d && d.data && d.data.obj.customer_id && d.success) {
                $state.go('customer', {
                    customer_id: d.data.obj.customer_id
                });
                return
            }
            triedToSave = !0;
            if (formObj.$name != 'financial') {// issue with selectize validation on financial tab
                for (x in d.data) {
                    ls[x] = d.data[x];
                }
            }

            ls.showAlerts(d, formObj)
        })
    };
    ls.checkVatAdd = function() {
        ls.reset();
        ls.checking = !0;
        ls.showViesUnavailable=false;
        if (!ls.obj.btw_nr) {
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            ls.checking = !1;
            return
        }
        if (typePromise) {
            $timeout.cancel(typePromise)
        }
        typePromise = $timeout(function() {
            if (ls.obj.btw_nr) {
                var data = {
                    'do': 'misc--misc-check_vies_vat_number',
                    value: ls.obj.btw_nr,
                    customer_id: ls.customer_id
                };
                helper.doRequest('post', 'index.php', data, function(r) {
                    ls.renderPage(r);
                    if (r.success) {
                        ls.openModal('vies_info', r.data, 'md');
                        if (r.data.exist_company) {
                            ls.obj.list = [];
                            if (r.data.multiple_companies) {
                                ls.obj.list.list_companies = r.data.exist_company.join(', ');
                            } else {
                                ls.obj.list.list_companies = r.data.exist_company;
                            }
                            ls.openModal('VatCompany', ls.obj.list.list_companies, {
                                'do': 'customers-EditCustomer'
                            }, 'md')
                        }
                    }else if(r.notice){
                        ls.showViesUnavailable=true;
                    }
                })
            } else {
                ls.reset()
            }
        }, 300)
    };
    ls.reset = function() {
        ls.checking = !1;
        ls.isOk = !1;
        ls.isFalse = !1;
        ls.isTrends = !1;
        ls.obj.vies_ok = !1
    };
    ls.renderPage = function(d) {
        ls.reset();
        if ((d.error || d.notice) && ls.obj.btw_nr) {
            ls.isFalse = !0
        }
        if (d.success && ls.obj.btw_nr) {
            if (d.data.trends_ok) {
                ls.isTrends = !0
            } else {
                ls.isOk = !0;
                ls.obj.vies_ok = !0
            }
        }
    };
    ls.parseData = function(d) {
        ls.obj.name = d.data.comp_name;
        ls.obj.address = d.data.comp_address;
        ls.obj.city = d.data.comp_city;
        ls.obj.zip = d.data.comp_zip;
        ls.obj.country_id = d.data.comp_country ? d.data.comp_country : 26
    }
    ls.showMore = function(item) {
        item.showMore = item.showMore ? !1 : !0
    }
    ls.toggleSearchButtons = function(item, val) {
        list.tSearch(ls, item, val, ls.renderList)
    };
    ls.renderList = function(d) {
        if (d && d.data) {
            ls.custom_prices = d.data
        }
    }
    ls.archived = function(value) {
        if (value == 0) {
            ls.search.archived = 0
        }
        if (value == 1) {
            ls.search.archived = 1
        }
        list.filter(ls, value, ls.renderList)
    };
    ls.updateCustomPrice = function(item) {
        item.loadRequest = !0;
        var custom_price = helper.return_value(item.custom_price);
        var data = {
            do: 'customers-ViewCustomer-customers-update_custom_price',
            customer_id: item.customer_id,
            article_id: item.article_id,
            custom_price: custom_price
        };
        helper.doRequest('post', 'index.php', data, function(d) {
            item.loadRequest = !1;
            if (d && d.success) {
                item.loadSuccess = !0
            } else {
                item.loadError = !0
            }
        })
    }
    ls.action = function(item, action) {
        list.action(ls, item, action, ls.vm.renderList)
    };
    ls.deleteCustomPrice=function(item, action){
        var params = {
            view: ls.activeView,
            offset: (ls.search.search ? ls.search.offset : 0)
        };
        for (x in ls.search) {
            params[x] = ls.search[x]
        }
        for (x in item[action]) {
            params[x] = item[action][x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r){
            ls.custom_prices=r.data;
        });
    }
    ls.searchThing = function() {
        list.search(ls.search, ls.renderList)
    }
    ls.vm = {};
    ls.vm.ord = '';
    ls.vm.reverse = !1;
    ls.vm.max_rows = 0;
    ls.vm.search = {
        search: '',
        'do': 'customers-customer_contacts',
        xget: '',
        showAdvanced: !1,
        showList: !1,
        offset: 1,
        'customer_id': ls.customer_id,
        archived:''
    };
    ls.vm.list = [];
    ls.vm.listStats = [];
    ls.vm.renderList = function(d) {
        if (d.data) {
            for (x in d.data) {
                ls.vm[x] = d.data[x]
            }
            ls.vm.listStats = [];
            ls.showAlerts(d);
            ls.populateContactAcitivityList(ls.vm.list)
        }
        if (!ls.activityLoaded && ls.ADV_CRM) {
            helper.doRequest('get', 'index.php', ls.activityParams, ls.renderActivities)
        }
    }
    ls.vm.toggleSearchButtons = function(item, val) {
        list.tSearch(ls.vm, item, val, ls.vm.renderList)
    };
    ls.vm.searchThing = function() {
        list.search(ls.vm.search, ls.vm.renderList)
    }
    ls.vm.orderBY = function(p) {
        list.orderBy(ls.vm, p, ls.vm.renderList)
    };
    ls.vm.saveView = function(i) {
        ls.vm.view = i;
        helper.doRequest('post', 'index.php', {
            'do': 'customers--customers-save_view_list',
            'view': i
        })
    }
    helper.doRequest('get', 'index.php', ls.vm.search, ls.vm.renderList);
    ls.removeActivity = function(item, $index) {
        var data = {
            customer_id: ls.customer_id,
            do: 'misc--customers-delete_activity',
            id: item.log_id
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.success) {
                ls.activities.item.splice($index, 1)
            }
        })
    }
    ls.changeActivityStatus = function(item) {
        var data = angular.copy(ls.activityParams);
        data.do = 'misc-activities-customers-update_status';
        data.log_code = item.log_code;
        data.status_change = item.color == 'green_status' ? 0 : 1;
        helper.doRequest('post', 'index.php', data, ls.renderActivities)
    }
    ls.showActivityF = function(show, item, type) {
        ls.showActivityForm = show;
        ls.activityTxt = helper.getLanguage(item);
        ls.activity.task_type = type;
        ls.activity.type_of_call = undefined;
        ls.activity.hd_events = !1;
        ls.hideSwitch = !1;
        if (show == !1) {
            ls.activity.comment = '';
            ls.activity.log_comment = '';
            ls.activity.contact_ids = [];
            ls.activity.duration_meet = '';
            ls.showAddActivity = !1
        }
        if (type == '1') {
            ls.activity.type_of_call = 0
        }
        if (type == '5') {
            ls.activity.type_of_call = 0
        }
        if (type == '6') {
            ls.hideSwitch = !0;
            ls.activity.hd_events = !0
        }
        if (type == '7') {
            ls.hideSwitch = !0
        }
        ls.activity.reminderHours = current;
        ls.activity.dateHours = current;
        ls.activity.dateEHours = current;
        ls.activity.date = new Date();
        ls.activity.reminder_event = new Date();
        ls.activity.end_d_meet = new Date();
        ls.changeTime(ls.activity.date, ls.activity.dateHours);
        ls.changeTime(ls.activity.reminder_event, ls.activity.reminderHours);
        ls.changeTime(ls.activity.end_d_meet, ls.activity.dateEHours);
        ls.activity.opportunity_id=undefined;
    }
    ls.changeDuration = function() {
        if (ls.activity.duration_meet) {
            var d = ls.activity.duration_meet.split('-'),
                currentH = angular.copy(ls.activity.dateHours).split('-');
            ls.activity.end_d_meet.setHours(ls.activity.date.getHours() + parseInt(d[0]), ls.activity.date.getMinutes() + parseInt(d[1]));
            var minutes = parseInt(currentH[1]) + parseInt(d[1]);
            var h = parseInt(currentH[0]) + parseInt(d[0]);
            if (minutes >= 60) {
                minutes -= 60;
                h++
            }
            ls.activity.dateEHours = (h) + '-' + (minutes)
        } else {
            ls.activity.end_d_meet = new Date();
            ls.activity.dateEHours = current
        }
    }
    ls.checkAllDay = function() {
        var new_date = angular.copy(ls.activity.date),
            day = new_date.getDate();
        ls.activity.end_d_meet = new_date;
        ls.activity.end_d_meet.setDate(day + 1)
    }
    ls.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        menubar: !1,
        autoresize_bottom_margin: 0,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        statusbar: !1,
        toolbar: ["styleselect | bold italic | bullist numlist "]
    };
    ls.contactAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select contact'),
        onDropdownOpen($dropdown) {
            var contact_drop = angular.element('#contact_ids')[0].selectize;
            if (ls.activity.task_type == '7' && ls.activity.contact_ids && ls.activity.contact_ids.length) {
                contact_drop.close()
            }
        },
        onChange(value) {
            if (ls.activity.task_type == '7' && value.length) {
                var contact_drop = angular.element('#contact_ids')[0].selectize;
                if (!this.options[value].zen_id) {
                    var msg = helper.getLanguage('Contact needs to be synced first');
                    var cont = {
                        "e_message": msg
                    };
                    ls.openModal("stock_info", cont, "md");
                    ls.activity.contact_ids = []
                } else {
                    contact_drop.close()
                }
            }
        },
        maxItems: 1000
    });
    ls.timeAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Time'),
    });
    ls.userAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Assign to')
    });
    ls.dealAutoCfg= selectizeFc.configure({
        placeholder: helper.getLanguage('Search deal')
    });
    ls.populateContactAcitivityList = function(item) {
        ls.contactsActitivty = [];
        for (x in item) {
            ls.contactsActitivty.push({
                'id': item[x].contact_id,
                'value': (item[x].con_firstname + ' ' + item[x].con_lastname),
                'zen_id': item[x].zen_id
            })
        }
    }
    ls.addActivity = function() {
        var data = angular.copy(ls.activity);
        angular.merge(data, ls.activityParams);
        data.user_id = ls.activities.user_id;
        data.do = 'misc-activities-customers-add_activity';
        data.from_customer='1';
        ls.customer_name = ls.obj.name;
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                ls.showAlerts(r)
            } else {
                ls.renderActivities(r);
                ls.showActivityF(!1, 'Type', '');
                ls.showActivityiny = !1
            }
        })
    }
    ls.changeTime = function(item, t, check) {
        if (item && t) {
            var d = t.split('-');
            item.setHours(d[0], d[1]);
            if (check) {
                ls.changeDuration();
                ls.checkAllDay()
            }
        }
    }
    ls.renderTracking = function(d) {
        if (d && d.data) {
            ls.tracking = d.data;
            ls.tracking.max_rows=ls.tracking.list.length;
        }
    }
    ls.showTracking = function() {
        ls.tracking_t_doc = [{
            'id': '0',
            'name': helper.getLanguage('All Documents')
        }];
        if(ls.ADV_CRM && ls.not_easyinv_subscription){
                ls.tracking_t_doc.push({
                    'id': '11',
                    'name': helper.getLanguage('Deal')
                });
            }
            if(ls.quickAddOptions && ls.quickAddOptions.is_5){
                ls.tracking_t_doc.push({
                    'id': '2',
                    'name': helper.getLanguage('Quote')
                });
            }
            if(ls.quickAddOptions && ls.quickAddOptions.is_11){
                ls.tracking_t_doc.push({
                    'id': '9',
                    'name': helper.getLanguage('Contracts')
                });
            }
            if(ls.quickAddOptions && ls.quickAddOptions.is_6){
                ls.tracking_t_doc.push({
                    'id': '4',
                    'name': helper.getLanguage('Order')
                });
            }
            if(ls.quickAddOptions && ls.quickAddOptions.is_14){
                ls.tracking_t_doc.push({
                    'id': '6',
                    'name': helper.getLanguage('Purchase Order')
                });
            }
            if(ls.quickAddOptions && ls.quickAddOptions.is_only_13){
                ls.tracking_t_doc.push({
                    'id': '5',
                    'name': helper.getLanguage('Intervention')
                });
            }
            if(ls.quickAddOptions && ls.quickAddOptions.is_4){
                ls.tracking_t_doc.push({
                    'id': '1',
                    'name': helper.getLanguage('Invoice')
                });
                ls.tracking_t_doc.push({
                    'id': '8',
                    'name': helper.getLanguage('Credit Invoice')
                });
                ls.tracking_t_doc.push({
                    'id': '7',
                    'name': helper.getLanguage('Proforma')
                });
                ls.tracking_t_doc.push({
                    'id': '10',
                    'name': helper.getLanguage('Purchase Invoice')
                });
            }
            if(ls.quickAddOptions && ls.quickAddOptions.is_3){
                ls.tracking_t_doc.push({
                    'id': '3',
                    'name': helper.getLanguage('Project')
                });
            }
        var obj = angular.copy(ls.search_track);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            ls.renderTracking(r)
        })
    }
    ls.removeTrackDate = function(date) {
        ls.search_track[date] = '';
        ls.search_track.offset = 1;
        ls.showTracking()
    }
    ls.showTrackMore = function(item) {
        ls.openModal('ModuleTracking', item, 'lg')
    }
    ls.countMaxRows=function(){
        ls.search_track.offset=1;
        ls.tracking.max_rows=ls.tracking.list.filter(function(item){
            return ((item.serial_nr && item.serial_nr.toLowerCase().indexOf(ls.search_track.search.toLowerCase()) !== -1) || (item.your_ref && item.your_ref.toLowerCase().indexOf(ls.search_track.search.toLowerCase()) !== -1));
        }).length;
    }
    ls.orderBY = function(p) {
        list.orderBy(ls, p, ls.renderList)
    };
    ls.addArticleCustomPrice = function(item) {
        ls.openModal('addArticleCustomPrice', item, 'lg')
    }
    ls.opendocument = function(edit, type_id, id_value,version_id) {
        if (type_id == 'invoice_id') {
            var invoice_id;
            var url = $state.href(edit, {
                invoice_id: id_value
            })
        } else if (type_id == 'quote_id') {
            var quote_id;
            var url = $state.href(edit, {
                quote_id: id_value,version_id:version_id
            })
        } else if (type_id == 'project_id') {
            var project_id;
            var url = $state.href(edit, {
                project_id: id_value
            })
        } else if (type_id == 'order_id') {
            var order_id;
            var url = $state.href(edit, {
                order_id: id_value
            })
        } else if (type_id == 'service_id') {
            var service_id;
            var url = $state.href(edit, {
                service_id: id_value
            })
        } else if (type_id == 'purchase_order_id') {
            var p_order_id;
            var url = $state.href(edit, {
                p_order_id: id_value
            })
        } else if (type_id == 'contract_id') {
            var contract_id;
            var url = $state.href(edit, {
                contract_id: id_value
            })
        } else if (type_id == 'purchase_invoice_id') {
            var incomming_invoice_id;
            var url = $state.href(edit, {
                invoice_id: id_value
            })
        } else if (type_id == 'installation_id') {
            var installation_id;
            var url = $state.href(edit, {
                installation_id: id_value
            })
        } else if (type_id == 'deal_id') {
            var deal_id;
            var url = $state.href(edit, {
                opportunity_id: id_value
            })
        }
        $window.open(url, '_blank')
    }
    ls.docOCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Original Documents'),
        closeAfterSelect: !0,
        create: !1,
        onChange(value) {
            ls.showTracking()
        },
        maxItems: 1
    };
    ls.docTCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Documents'),
        closeAfterSelect: !0,
        create: !1,
        onChange(value) {
            ls.search_track.offset = 1;
            ls.showTracking()
        },
        maxItems: 1
    };
    ls.show_mail = function() {
        ls.openModal('NylasEmail', '', 'lg')
    }
    ls.get_details = function() {
        helper.doRequest('get', 'index.php', {
            'do': 'customers-ViewCustomer',
            'customer_id': ls.customer_id
        }, function(r) {
            for (x in r.data) {
                ls[x] = r.data[x]
            }
        })
    }
    ls.viewContent = function(item) {
        var cont = {
            "e_message": item.comments
        };
        ls.openModal("stock_info", cont, "lg")
    }
    ls.alloCall = function(number, is_contact, contact_id) {
        if (ls.devices.length == 1) {
            var obj = {
                'do': 'misc--customers-alloCall',
                'device_id': ls.devices[0].id,
                'number': number,
                'customer_id': ls.customer_id
            };
            if (is_contact) {
                obj.contact_ids = [contact_id]
            }
            helper.doRequest('post', 'index.php', obj, function(r) {
                ls.showAlerts(r);
                if (r.success) {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php', ls.activityParams, ls.renderActivities)
                }
            })
        } else {
            var obj = {
                'do': 'misc--customers-alloCall',
                'number': number,
                'device_dd': ls.devices,
                'customer_id': ls.customer_id
            };
            if (is_contact) {
                obj.contact_ids = [contact_id]
            }
            ls.openModal('allo_call', obj, "md")
        }
    }
    ls.attachement_files = function(scope, e) {
        angular.element(e.target).parents('.upload-box1').find('.newUploadFile3').click();
        angular.element('.newUploadFile3').off();
        angular.element('.newUploadFile3').on('click', function(e) {
            e.stopPropagation()
        }).on('change', function(e) {
            e.preventDefault();
            var _this = $(this),
                p = _this.parents('.upload-box1'),
                x = p.find('.newUploadFile3');
            if (x[0].files.length == 0) {
                return !1
            }
            p.find('.img-thumbnail1').attr('src', '');
            var tmppath = URL.createObjectURL(x[0].files[0]);
            p.find('.img-thumbnail1').attr('src', tmppath);
            p.find('.btn').addClass('hidden');
            p.find('.nameOfTheFile1').text(x[0].files[0].name);
            var formData = new FormData();
            formData.append("Filedata", x[0].files[0]);
            formData.append("do", 'customers--customers-atach');
            formData.append("customer_id", ls.customer_id);
            p.find('.upload-progress-bar1').css({
                width: '0%'
            });
            p.find('.upload-file1').removeClass('hidden');
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "index.php", !0);
            oReq.upload.addEventListener("progress", function(e) {
                var pc = parseInt(e.loaded / e.total * 100);
                p.find('.upload-progress-bar1').css({
                    width: pc + '%'
                })
            });
            oReq.onload = function(oEvent) {
                var res1 = JSON.parse(oEvent.target.response);
                p.find('.upload-file1').addClass('hidden');
                p.find('.btn').removeClass('hidden');
                if (oReq.status == 200) {
                    if (res1.success) {
                        helper.doRequest('get', 'index.php?do=customers-ViewCustomer&customer_id=' + ls.customer_id + '', function(r) {
                            for (x in r.data) {
                                ls[x] = r.data[x]
                            }
                            triedToSave = !0
                        })
                    } else {
                        var msg = {
                            'error': {
                                'error': file.name + ': ' + res1.error
                            },
                            'notice': !1,
                            'success': !1
                        };
                        ls.showAlerts(msg)
                    }
                } else {
                    var msg = {
                        'error': {
                            'error': "Error " + oReq.status + " occurred when trying to upload your file."
                        },
                        'notice': !1,
                        'success': !1
                    };
                    ls.showAlerts(msg)
                }
            };
            oReq.send(formData)
        });
    }
    
    ls.deleteatach = function(scope, id, name, formObj) {
        var data = {
            'do': 'customers-ViewCustomer-customers-deleteatach',
            'id': id,
            'name': name,
            'customer_id': ls.customer_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            triedToSave = !0;
            for (x in r.data) {
                ls[x] = r.data[x]
            }
            ls.showAlerts(r, formObj)
        })
    }
    ls.defaultcheck = function(scope, id, name, default_id, formObj) {
        var data = {
            'do': 'customers-ViewCustomer-customers-defaultcheck',
            'id': id,
            'name': name,
            'default_id': default_id,
            'customer_id': ls.customer_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            for (x in r.data) {
                ls[x] = r.data[x]
            }
            ls.showAlerts(r, formObj)
        })
    }
    if ($stateParams.tab && $stateParams.tab == 'sites') {
        $timeout(function() {
            if ($stateParams.site_addr) {
                ls.search_site.address_id = $stateParams.site_addr
            }
            angular.element('.tabs-default-stack a[href="#' + $stateParams.tab + '"]').tab('show').triggerHandler('click')
        })
    }
    ls.siteAutoCfg = {
        valueField: 'address_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Site Addresses'),
        closeAfterSelect: !0,
        create: !1,
        onChange(value) {
            ls.search_instal.search = '';
            ls.search_instal.address_id = this.options[value].address_id;
            ls.search_site.address_id = this.options[value].address_id;
            ls.showSites()
        },
        maxItems: 1
    };
    ls.renderSites = function(d) {
        ls.sitemap = {};
        if (d && d.data) {
            ls.instals = d.data;
            if (ls.instals.addresses_list.length) {
                ls.sitemap = {
                    center: angular.copy(d.data.coords),
                    zoom: 12,
                    marker_id: 1,
                    refresh: !0,
                    marker_coord: d.data.coords
                }
            }
        }
    }
    ls.renderInstal = function(d) {
        if (d && d.data) {
            ls.instal_data = d.data
        }
    }
    ls.showInstals = function(bool) {
        if (bool) {
            ls.load_instal = !0;
            var obj1 = angular.copy(ls.search_instal);
            helper.doRequest('get', 'index.php', obj1, function(o) {
                ls.load_instal = !1;
                ls.renderInstal(o)
            })
        } else {
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                ls.load_instal = !0;
                var obj1 = angular.copy(ls.search_instal);
                helper.doRequest('get', 'index.php', obj1, function(o) {
                    ls.load_instal = !1;
                    ls.renderInstal(o)
                })
            }, 300)
        }
    }
    ls.showSites = function() {
        var obj = angular.copy(ls.search_site);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            ls.renderSites(r);
            ls.search_instal.address_id = ls.instals.address_id;
            ls.showInstals(!0)
        })
    }
    ls.toggleSearchInstal = function(item, val) {
        ls.search_instal[item] = val;
        ls.showInstals(!0)
    };
    ls.map = {
        center: angular.copy(ls.addresses.coords),
        zoom: 12,
        marker_id: 1,
        refresh: !0,
        marker_coord: ls.addresses.coords
    }

    ls.updateContact=function(item,action) {
        var params = angular.copy(ls.vm.search);
        for (x in item[action]) {
            params[x] = item[action][x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, ls.vm.renderList);
    }

    ls.editNotes = function() {
        if (typePromise) {
            $timeout.cancel(typePromise);
        }
        typePromise = $timeout(function() {
            var obj = {
                'do': 'customers--customers-customer_notes',
                'customer_id': ls.obj.customer_id,
                'customer_notes': ls.obj.customer_notes
            };
            helper.doRequest('post', 'index.php', obj);
        }, 500);       
    }

    ls.createContact=function(){
        var main_obj = {
            country_dd: [],
            country_id: 0,
            add_contact: !0,
            contact_only: !0,
            'do': 'misc--misc-tryAddC',
            buyer_id:ls.customer_id
        };
        editItem.openModal(ls, 'AddCustomer', main_obj, 'md');
    }
    
}]).controller('addArticleCustomPriceModalCtrl', ['$scope', '$timeout', 'helper', 'data', 'modalFc', '$uibModalInstance', '$window', '$state', 'list', function($scope, $timeout, helper, data, modalFc, $uibModalInstance, $window, $state, list) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.search = {
        search: '',
        'do': 'misc-addArticleCustomPrice',
        customer_id: vmMod.obj.customer_id,
        lang_id: vmMod.obj.lang_id,
        offset: 1,
        cat_id: vmMod.obj.cat_id
    };
    vmMod.ok = function(item) {
        var custom_price = helper.return_value(item.custom_price);
        var data = {
            do: 'customers-ViewCustomer-customers-update_custom_price',
            customer_id: item.customer_id,
            article_id: item.article_id,
            custom_price: custom_price,
            'xget': 'customPrices'
        };
        helper.doRequest('post', 'index.php', data, function(d) {
            item.loadRequest = !1;
            if (d && d.success) {
                vmMod.obj2 = d;
                item.loadSuccess = !0
            } else {
                item.loadError = !0
            }
        });
        item.succes_add = !0
    };
    vmMod.cancel = function() {
        $uibModalInstance.close(vmMod.obj2)
    };
    vmMod.searchThing = function() {
        list.search(vmMod.search, vmMod.renderList, vmMod)
    }
    vmMod.renderList = function(r) {
        if (r && r.data) {
            for (x in r.data) {
                vmMod.obj[x] = r.data[x]
            }
        }
        vmMod.setModalMaxHeight(angular.element('.modal.in'))
    }
    vmMod.setModalMaxHeight = function(element) {
        var current_modal = angular.element(element),
            modal_content = current_modal.find('.modal-content'),
            borderWidth = modal_content.outerHeight() - modal_content.innerHeight(),
            dialogMargin = angular.element(window).width() < 768 ? 20 : 60,
            contentHeight = angular.element(window).height() - (dialogMargin + borderWidth),
            headerHeight = current_modal.find('.modal-header').outerHeight() || 0,
            footerHeight = current_modal.find('.modal-footer').outerHeight() || 0,
            maxHeight = contentHeight - (headerHeight + footerHeight);
        current_modal.find('.modal-body').css({
            'max-height': maxHeight,
            'overflow-y': 'auto'
        })
    }
}]).controller('ManageAddressesModalCtrl', ['$scope', '$rootScope', 'helper', 'data', 'modalFc', '$uibModalInstance', 'list', 'modalFc', function($scope, $rootScope, helper, data, modalFc, $uibModalInstance, list, modalFc) {
    var vmMod = this;
    for (x in data) {
        vmMod[x] = data[x]
    }
    vmMod.renderList = function(r) {
        if (r && r.data) {
            for (x in r.data) {
                vmMod[x] = r.data[x]
            }
        }
    }
    vmMod.action = function(item, action) {
        console.log($scope);
        list.action(vmMod, item, action, vmMod.renderList)
    };
    vmMod.cancel = function() {
        $uibModalInstance.close()
    };

    vmMod.updateMainAddress=function(item){
        item.is_primary=true;
        for(var x in vmMod.list){
            if(vmMod.list[x].address_id != item.address_id){
                vmMod.list[x].is_primary=false;
                vmMod.list[x].primary_id='';
            }
        }
        helper.doRequest('post', 'index.php', {'do':'customers--customers-setMainAddress','customer_id':item.customer_id,'address_id':item.address_id});
    }

    vmMod.updateBillingAddress=function(item){
        item.is_billing=true;
        for(var x in vmMod.list){
            if(vmMod.list[x].address_id != item.address_id){
                vmMod.list[x].is_billing=false;
                vmMod.list[x].billing_id='';
            }
        }
        helper.doRequest('post', 'index.php', {'do':'customers--customers-setBillingAddress','customer_id':item.customer_id,'address_id':item.address_id});
    }

    vmMod.updateDeliveryAddress=function(item){
        helper.doRequest('post', 'index.php', {'do':'customers--customers-setDeliveryAddress','customer_id':item.customer_id,'address_id':item.address_id,'value':item.is_delivery});
    }

    vmMod.updateSiteAddress=function(item){
        helper.doRequest('post', 'index.php', {'do':'customers--customers-setSiteAddress','customer_id':item.customer_id,'address_id':item.address_id,'value':item.is_site});
    }

    vmMod.openModal = function(type, item, size) {
        var params = {
            template: 'cTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'CustomerAddress':
                params.params = {
                    'do': 'customers-ViewCustomer',
                    'address_id': item,
                    'customer_id': vmMod.customer_id,
                    'xget': 'customerAddress'
                };
                params.callback = function(data) {
                    var data = {
                        'do': 'customers-ViewCustomer',
                        'customer_id': vmMod.customer_id,
                        'xget': 'customerAddresses'
                    };
                    helper.doRequest('get', 'index.php', data, vmMod.renderList)
                }
                break
        }
        modalFc.open(params)
    }
}]).controller('CustomerAddressModalCtrl', ['$scope', '$timeout', 'helper', 'data', 'modalFc', '$uibModalInstance', function($scope, $timeout, helper, data, modalFc, $uibModalInstance) {
    var vmMod = this;
    for (x in data) {
        vmMod[x] = data[x]
    }
    vmMod.cancel = function() {
        $uibModalInstance.close()
    };
    vmMod.ok = function(formObj) {
        var data = angular.copy(vmMod);
        data = helper.clearData(data, ['cancel', 'ok', 'showAlerts']);
        data.do = data.do_next;
        formObj.$invalid = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.data) {
                for (x in r.data) {
                    vmMod[x] = r.data[x]
                }
                if(r.success){
                    $uibModalInstance.close();
                }else{
                    vmMod.showAlerts(r, formObj)
                }
            }
        })
    };
    vmMod.showAlerts = function(d, formObj) {
        helper.showAlerts(vmMod, d, formObj)
    }
}]).controller('ModuleTrackingModalCtrl', ['$scope', '$timeout', 'helper', 'data', 'modalFc', '$uibModalInstance', '$window', '$state', function($scope, $timeout, helper, data, modalFc, $uibModalInstance, $window, $state) {
    var vmMod = this;
    vmMod.obj = data;
    console.log(data);
    vmMod.opendocument = function(edit, type_id, id_value) {
        if (type_id == 'invoice_id') {
            var invoice_id;
            var url = $state.href('invoice.view', {
                invoice_id: id_value
            })
        } else if (type_id == 'quote_id') {
            var quote_id;
            var url = $state.href('quote.view', {
                quote_id: id_value
            })
        } else if (type_id == 'project_id') {
            var project_id;
            var url = $state.href(edit, {
                project_id: id_value
            })
        } else if (type_id == 'order_id') {
            var order_id;
            var url = $state.href('order.view', {
                order_id: id_value
            })
        } else if (type_id == 'service_id') {
            var service_id;
            var url = $state.href(edit, {
                service_id: id_value
            })
        } else if (type_id == 'purchase_order_id') {
            var p_order_id;
            var url = $state.href(edit, {
                p_order_id: id_value
            })
        } else if (type_id == 'contract_id') {
            var contract_id;
            var url = $state.href(edit, {
                contract_id: id_value
            })
        } else if (type_id == 'purchase_invoice') {
            var incomming_invoice_id;
            var url = $state.href(edit, {
                incomming_invoice_id: id_value
            })
        } else if (type_id == 'deal_id') {
            var deal_id;
            var url = $state.href(edit, {
                opportunity_id: id_value
            })
        }
        $window.open(url, '_blank')
    }
    vmMod.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('VatCompanyModalCtrl', ['$scope', '$uibModalInstance', '$timeout', 'helper', 'modalFc', 'data', function($scope, $uibModalInstance, $timeout, helper, modalFc, data) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.cancel = function() {
        $uibModalInstance.close()
    };
    vmMod.showAlerts = function(d, formObj) {
        helper.showAlerts(vmMod, d, formObj)
    }
}]).controller('dealsCtrl', ['$scope', '$rootScope', '$state', '$window', '$cacheFactory', '$stateParams', 'helper', '$uibModal', '$timeout', 'data', 'selectizeFc', 'modalFc', 'list', function($scope, $rootScope, $state, $window, $cacheFactory, $stateParams, helper, $uibModal, $timeout, data, selectizeFc, modalFc, list) {
    console.log('Deals');
    $rootScope.opened_lists.deals = true;
    $scope.item = {};
    $scope.nav = [];
    $scope.old_board_id = undefined;
    $scope.initial_start = !0;
    $scope.sc = {
        xScroll: !1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.ord = '';
    $scope.reverse = !1;
    $scope.search = {
        search: '',
        'do': 'customers-deals',
        'user_id': '',
        'board_id': undefined,
        'stage_id': undefined,
        'manager_id': undefined,
        'offset': undefined,
        xget: 'DealsList'
    };
    $scope.showAlerts = function(d, formObj) {
        helper.showAlerts($scope, d, formObj)
    }
    $scope.toggleSearchButtons = function(item, val) {
        $scope.search[item] = val;
        var params = {};
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', params, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.searchThing = function(val) {
        if (val) {
            $scope.search[val] = undefined;
            $scope.item[val] = undefined
        }
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.archived = function(value) {
        $scope.search.archived = value;
        $scope.listObj.check_add_all = !1;
        list.filter($scope, value, $scope.renderList, true)
    };
    $scope.saveView = function(i) {
        helper.doRequest('post', 'index.php', {
            'do': 'customers--deals-save_view_list',
            'view': i
        }, function(r) {
            var tmp_item = angular.copy($scope.search);
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', tmp_item, function(o) {
                $scope.renderList(o);
                $scope.view = i
            })
        })
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.selectItems = function(i) {
        list.selectItems($scope, 'customers-deals-deals-selectItems', i)
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.renderList = function(d) {
        $scope.resetScroll();
        $scope.item.deals_list = d.data;
        $scope.max_rows = d.data.max_rows;
        $scope.lr = d.data.lr;
        $scope.nav = d.data.nav;
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['opportunity_id']);
                count ++;
            }
            sessionStorage.clear();
            sessionStorage.setItem('dealView_total', count);
            sessionStorage.setItem('dealView_list', JSON.stringify(ids));
            //console.log(sessionStorage);
        if ($scope.search.start_date_js) {
            $scope.dateOptions.minDate = $scope.search.start_date_js
        } else {
            $scope.dateOptions.minDate = undefined
        }
        if ($scope.search.stop_date_js) {
            $scope.dateOptions.maxDate = $scope.search.stop_date_js
        } else {
            $scope.dateOptions.maxDate = undefined
        }
        $timeout(function() {
            updateStageHeight()
        })
    }
    $scope.archiveSelected = function() {
        var items = [];
        for (y in $scope.item.deals_list.list) {
            for (x in $scope.item.deals_list.list[y].data) {
                if ($scope.item.deals_list.list[y].data[x].check_add_to_product) {
                    items.push($scope.item.deals_list.list[y].data[x].id)
                }
            }
        }
        var params = {
            'do': 'customers-deals-deals-archiveSelectedDeals',
            'xget': 'DealsList',
            'items': items,
        };
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r);
            $scope.listObj.check_add_all = !1
        })
    }
    $scope.modifySelected = function() {
        var items = [];
        for (y in $scope.item.deals_list.list) {
            for (x in $scope.item.deals_list.list[y].data) {
                if ($scope.item.deals_list.list[y].data[x].check_add_to_product) {
                    items.push($scope.item.deals_list.list[y].data[x].id)
                }
            }
        }
        openModal('modify_stage', items, 'lg')
    }
    $scope.renderPage = function(d) {
        if (d) {
            $scope.item = d.data;
            $scope.view = d.data.view;
            $scope.max_rows = d.data.deals_list.max_rows;
            $scope.lr = d.data.deals_list.lr;
            $scope.list = d.data.deals_list.list;
            $scope.columns_selected = d.data.columns;
            $scope.nav = d.data.deals_list.nav;
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['opportunity_id']);
                count ++;
            }
            sessionStorage.clear();
            sessionStorage.setItem('dealView_total', count);
            sessionStorage.setItem('dealView_list', JSON.stringify(ids));
            //console.log(sessionStorage);
            if ($scope.item.board_id) {
                $scope.old_board_id = $scope.item.board_id;
                $scope.search.board_id = $scope.item.board_id
            }
            if ($scope.item.user_id) {
                $scope.search.user_id = $scope.item.user_id
            }
            if ($scope.item.stage_id) {
                $scope.search.stage_id = $scope.item.stage_id
            }
            if ($scope.item.search) {
                $scope.search.search = $scope.item.search
            }
            if ($scope.item.manager_id) {
                $scope.search.manager_id = $scope.item.manager_id
            }
            if ($scope.item.start_date_js) {
                var initDate = new Date($scope.item.start_date_js).getTime() / 1000;
                $scope.search.start_date_js = new Date((initDate + 7200) * 1000)
            }
            if ($scope.item.stop_date_js) {
                var initDate = new Date($scope.item.stop_date_js).getTime() / 1000;
                $scope.search.stop_date_js = new Date((initDate + 7200) * 1000)
            }
            $timeout(function() {
                updateStageHeight()
            });
            if ($stateParams.is_add && $scope.initial_start == !0) {
                $scope.initial_start = !1;
                var obj = {
                    'stage_id': '',
                    'board_id': '',
                    'user_id': $scope.item.user_id,
                    'deal_id': 'tmp',
                    'buyer_id': $stateParams.buyer_id,
                    'contact_id': $stateParams.contact_id
                };
                openModal('manage_deal', obj, 'lg')
            }
        }
    }
    $scope.boardAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('A Board'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.value) + ' </span>' + '</div>' + '</div>' + '</div>';
                if (item.id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Manage Boards') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            if (value == '99999999999') {
                openModal('manage_board', {}, 'md')
            } else if (value != undefined && value != '') {
                $scope.old_board_id = this.options[value].id;
                var obj = {
                    'do': 'customers-deals',
                    'xget': 'DealsList',
                    'board_id': this.options[value].id,
                    'user_id': $scope.item.user_id
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                $scope.search.search = '';
                $scope.search.board_id = this.options[value].id;
                $scope.search.user_id = $scope.item.user_id;
                helper.doRequest('post', 'index.php', obj, function(r) {
                    $scope.resetScroll();
                    $scope.item.deals_list = r.data;
                    $scope.nav = d.data.nav;
                    var ids = [];
                    var count = 0;
                    for(y in $scope.nav){
                        ids.push($scope.nav[y]['opportunity_id']);
                        count ++;
                    }
                    sessionStorage.clear();
                    sessionStorage.setItem('dealView_total', count);
                    sessionStorage.setItem('dealView_list', JSON.stringify(ids));
                    $timeout(function() {
                        updateStageHeight()
                    })
                })
            }
        },
        maxItems: 1
    };
    $scope.userAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.value) + ' </span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            if (value != undefined && value != '') {
                var obj = {
                    'do': 'customers-deals',
                    'xget': 'DealsList',
                    'board_id': $scope.item.board_id,
                    'user_id': this.options[value].id
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                $scope.search.search = '';
                $scope.search.board_id = $scope.item.board_id;
                $scope.search.user_id = this.options[value].id;
                helper.doRequest('post', 'index.php', obj, function(r) {
                    $scope.resetScroll();
                    $scope.item.deals_list = r.data;
                    $scope.nav = r.data.nav;
                    var ids = [];
                    var count = 0;
                    for(y in $scope.nav){
                        ids.push($scope.nav[y]['opportunity_id']);
                        count ++;
                    }
                    sessionStorage.clear();
                    sessionStorage.setItem('dealView_total', count);
                    sessionStorage.setItem('dealView_list', JSON.stringify(ids));
                    $timeout(function() {
                        updateStageHeight()
                    })
                })
            }
        },
        maxItems: 1
    };
    $scope.boardCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select a board'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.value) + ' </span>' + '</div>' + '</div>' + '</div>';
                if (item.id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Manage Boards') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            if (value == '99999999999') {
                openModal('manage_board', {}, 'md')
            } else if (value != undefined && value != '') {
                $scope.old_board_id = this.options[value].id;
                var obj = {
                    'do': 'customers-deals',
                    'xget': 'DealsList',
                    'board_id': this.options[value].id,
                    'stage_id': undefined,
                    'user_id': $scope.item.user_id,
                    'manager_id': $scope.item.manager_id
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                $scope.search.search = '';
                $scope.search.board_id = this.options[value].id;
                $scope.search.stage_id = undefined;
                $scope.search.user_id = $scope.item.user_id;
                $scope.search.manager_id = $scope.item.manager_id ? $scope.item.manager_id : undefined;
                helper.doRequest('post', 'index.php', obj, function(r) {
                       
                    $scope.item.deals_list = r.data;
                    $scope.item.stage_list = r.data.stage_list;
                    $scope.item.board_id = r.data.board_id;
                    $scope.item.stage_id = r.data.stage_id;
                    $scope.item.manager_id = r.data.manager_id ? r.data.manager_id : undefined;
                    $scope.max_rows = r.data.max_rows;
                    $scope.lr = r.data.lr;
                    $scope.nav = r.data.nav;
                    var ids = [];
                    var count = 0;
                    for(y in $scope.nav){
                        ids.push($scope.nav[y]['opportunity_id']);
                        count ++;
                    }
                    sessionStorage.clear();
                    sessionStorage.setItem('dealView_total', count);
                    sessionStorage.setItem('dealView_list', JSON.stringify(ids));
                    $timeout(function() {
                        generateColors()
                    })
                })
            }
        },
        maxItems: 1
    };
    $scope.stageCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select a stage'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.value) + ' </span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            if (value == '99999999999') {
                openModal('manage_board', {}, 'md')
            } else if (value != undefined && value != '') {
                $scope.old_stage_id = this.options[value].id;
                var obj = {
                    'do': 'customers-deals',
                    'xget': 'DealsList',
                    'board_id': $scope.item.board_id,
                    'stage_id': this.options[value].id,
                    'user_id': $scope.item.user_id,
                    'manager_id': $scope.item.manager_id
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                $scope.search.search = '';
                $scope.search.stage_id = this.options[value].id;
                $scope.search.board_id = $scope.item.board_id;
                $scope.search.user_id = $scope.item.user_id;
                $scope.search.manager_id = $scope.item.manager_id ? $scope.item.manager_id : undefined;
                helper.doRequest('post', 'index.php', obj, function(r) {
                    $scope.item.deals_list = r.data;
                    $scope.item.board_id = r.data.board_id;
                    $scope.item.stage_id = r.data.stage_id;
                    $scope.item.manager_id = r.data.manager_id ? r.data.manager_id : undefined;
                    $scope.max_rows = r.data.max_rows;
                    $scope.lr = r.data.lr;
                    $scope.nav = r.data.nav;
                    var ids = [];
                    var count = 0;
                    for(y in $scope.nav){
                        ids.push($scope.nav[y]['opportunity_id']);
                        count ++;
                    }
                    sessionStorage.clear();
                    sessionStorage.setItem('dealView_total', count);
                    sessionStorage.setItem('dealView_list', JSON.stringify(ids));
                    $timeout(function() {
                        generateColors()
                    })
                })
            }
        },
        maxItems: 1
    };
    $scope.managerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select an account manager'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.value) + ' </span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            if (value == '99999999999') {
                openModal('manage_board', {}, 'md')
            } else if (value != undefined && value != '') {
                $scope.old_manager_id = this.options[value].id;
                var obj = {
                    'do': 'customers-deals',
                    'xget': 'DealsList',
                    'board_id': $scope.item.board_id,
                    'stage_id': $scope.item.stage_id,
                    'user_id': $scope.item.user_id,
                    'manager_id': this.options[value].id
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                $scope.search.search = '';
                $scope.search.stage_id = $scope.item.stage_id;
                $scope.search.board_id = $scope.item.board_id;
                $scope.search.manager_id = this.options[value].id;
                $scope.search.user_id = $scope.item.user_id;
                helper.doRequest('post', 'index.php', obj, function(r) {
                    $scope.item.deals_list = r.data;
                    $scope.item.board_id = r.data.board_id;
                    $scope.item.stage_id = r.data.stage_id;
                    $scope.item.manager_id = r.data.manager_id;
                    $scope.max_rows = r.data.max_rows;
                    $scope.lr = r.data.lr;
                    $scope.nav = r.data.nav;
                    var ids = [];
                    var count = 0;
                    for(y in $scope.nav){
                        ids.push($scope.nav[y]['opportunity_id']);
                        count ++;
                    }
                    sessionStorage.clear();
                    sessionStorage.setItem('dealView_total', count);
                    sessionStorage.setItem('dealView_list', JSON.stringify(ids));
                    $timeout(function() {
                        generateColors()
                    })
                })
            }
        },
        maxItems: 1
    };
    $scope.scroll_config = {
        autoHideScrollbar: !1,
        theme: 'minimal-dark',
        axis: 'x',
        scrollInertia: 50,
        advanced: {
            autoExpandHorizontalScroll: !0
        }
    };
    $scope.scroll_config_vertical = {
        autoHideScrollbar: !1,
        theme: 'minimal-dark',
        axis: 'y',
        scrollInertia: 50,
        advanced: {
            autoExpandHorizontalScroll: !0
        }
    };

    function updateStageHeight() {
        var heightMore = 3;
        for (x in $scope.item.deals_list.list) {
            
            if ($scope.item.deals_list.list[x].data.length > 3) {
                if ($scope.item.deals_list.list[x].data.length > 3 && $scope.item.deals_list.list[x].data.length > heightMore) {
                    heightMore = $scope.item.deals_list.list[x].data.length
                }
            }
            for (y in $scope.item.deals_list.list[x].data) {
                if($scope.item.deals_list.list[x].data[y].avatar==''){
                    var color = $scope.item.deals_list.list[x].data[y].color;
                    angular.element('.dskb-item .letters_color').css('background', color);
                }
               
            }
        }
        /*if (heightMore > 3) {
            var cellsPlus = ((heightMore - 3) * 95) + 290 + 85;
            angular.element('.dskb-items-container').css('min-height', cellsPlus + 'px')
        } else {
            angular.element('.dskb-items-container').css('min-height', '290px')
        }*/
    }

    function generateColors() {
        for (x in $scope.item.deals_list.list) {
           for (y in $scope.item.deals_list.list[x].data) {
                if($scope.item.deals_list.list[x].data[y].avatar==''){
                    var color = $scope.item.deals_list.list[x].data[y].color;
                    angular.element('.dskb-item .letters_color').css('background', color);
                }
               
            }
        }       
    }

    function updateStage(stage_id) {
        var stage = {};
        for (x in $scope.item.deals_list.list) {
            if ($scope.item.deals_list.list[x].stage_id == stage_id) {
                stage = $scope.item.deals_list.list[x];
                break
            }
        }
        var obj = {
            'do': 'customers--deals-updateStage',
            'stage_id': stage.stage_id,
            'deals': stage.data
        };
        helper.doRequest('post', 'index.php', obj, function(r) {})
    }

    function updateTotals() {
        for (x in $scope.item.deals_list.list) {
            var total = 0;
            var weighted_total = 0;
            for (y in $scope.item.deals_list.list[x].data) {
                total += parseFloat($scope.item.deals_list.list[x].data[y].amount_free);
                weighted_total = $scope.item.deals_list.list[x].rate
            }
            $scope.item.deals_list.list[x].total = helper.displayNr(total);
            $scope.item.deals_list.list[x].weighted_total = helper.displayNr(total * weighted_total / 100)
        }
    }
    $scope.dragControlListeners = {
        itemMoved: function(event) {
            var stage_id = angular.element(event.dest.sortableScope.element[0]).attr("data-id");
            updateStage(stage_id);
            updateStageHeight();
            updateTotals()
        },
        orderChanged: function(event) {
            var stage_id = angular.element(event.dest.sortableScope.element[0]).attr("data-id");
            updateStage(stage_id)
        }
    }
    $scope.addDeal = function() {
        var obj = {
            'stage_id': '',
            'board_id': '',
            'user_id': $scope.item.user_id,
            'deal_id': 'tmp'
        };
        openModal('manage_deal', obj, 'lg')
    }
    $scope.editDeal = function(id) {
        var obj = {
            'stage_id': '',
            'board_id': $scope.item.board_id,
            'user_id': $scope.item.user_id,
            'deal_id': id
        };
        openModal('manage_deal', obj, 'lg')
    }
    $scope.showActivities = function(item) {
        item.closeDropdown = !1;
        item.showDocs = !1;
        openModal('show_activity', item, 'md')
    }
    $scope.toggled = function(open, item) {
        if (!open) {
            item.showDocs = !1
        }
    }
    $scope.createDocument = function(type, item) {
        switch (type) {
            case 'quote':
                var link = 'quote.edit';
                var obj = {
                    'quote_id': 'tmp'
                };
                break;
            case 'order':
                var link = 'order.edit';
                var obj = {
                    'order_id': 'tmp',
                    'buyer_id': 'tmp'
                };
                break;
            case 'project':
                var link = 'project.edit';
                var obj = {
                    'project_id': 'tmp'
                };
                break;
            case 'service':
                var link = 'service.edit';
                var obj = {
                    'service_id': 'tmp'
                };
                break;
            case 'contract':
                var link = 'contract.edit';
                var obj = {
                    'contract_id': 'tmp'
                };
                break;
            case 'invoice':
                var link = 'invoice.edit';
                var obj = {
                    'invoice_id': 'tmp'
                };
                break
        }
        var cacheDeal = $cacheFactory.get('DealToDocument');
        if (cacheDeal != undefined) {
            cacheDeal.destroy()
        }
        var cache = $cacheFactory('DealToDocument');
        cache.put('data', {
            'deal_id': item.deal_id
        });
        $state.go(link, obj)
    }
    $scope.viewDocument = function(item) {
        var url = $state.href(item.do, item.params);
        $window.open(url, '_blank')
    }
    $scope.viewPdf = function(link) {
        $window.open(link, '_blank')
    }
    $scope.resizeDoc = function(event, index) {
        var _this = event.target
    }
    $scope.swipe = function(event, operation) {
        var dsk_box = angular.element('.dskb-box')[0];
        if (dsk_box.scrollWidth != dsk_box.scrollLeft) {
            if (operation == 'left') {
                dsk_box.scrollLeft += 275
            } else {
                dsk_box.scrollLeft -= 275
            }
        }
        return !0
    }
    $scope.doScroll = function(operation) {
        if ($scope.sc.xScroll) {
            var dsk_box = angular.element('.dskb-box')[0];
            if (dsk_box.scrollWidth != dsk_box.scrollLeft) {
                if (operation == 'plus') {
                    dsk_box.scrollLeft += 50
                } else {
                    dsk_box.scrollLeft -= 50
                }
            }
        } else {
            /*var dsk_box = angular.element('.dskb')[0];
            if (dsk_box.scrollheight != dsk_box.scrollTop) {
                if (operation == 'plus') {
                    dsk_box.scrollTop += 50
                } else {
                    dsk_box.scrollTop -= 50
                }
            }*/
        }
    }
    $scope.doStageScroll=function(index,operation){
        if(!$scope.sc.xScroll){
            if($scope.item.deals_list.list.length > 4){
                var container=angular.element('.dskb-box .dskb-multi-cell:eq('+index+') .dskb-items-container');          
            }else{
                var container=angular.element('.dskb .dskb-cell:eq('+index+') .dskb-items-container');
            }
            if(operation == 'plus' && container[0].scrollHeight>container.height() && container[0].scrollHeight > container[0].scrollTop){
                container[0].scrollTop += 50;
            }else if(operation == 'minus' && container[0].scrollHeight>container.height() && container[0].scrollTop>0){
                container[0].scrollTop -= 50;
            }
        }      
    }
    $scope.resetScroll = function() {
        $scope.sc.xScroll = !1;
        var dsk_box = angular.element('.dskb-box')[0];
        var dsk = angular.element('.dskb')[0];
        if (dsk_box) {
            dsk_box.scrollLeft = 0
        }
        if (dsk) {
            dsk.scrollTop = 0
        }
    }
    $scope.columnSettings = function() {
        openModal('column_set', {}, 'lg')
    }
    $scope.scrollResize = function(param) {
        if ($window.innerWidth < 1024) {
            if (param == 'enter') {
                $scope.xScroll = !0
            } else {
                $scope.xScroll = !1
            }
        } else {
            if (param == 'enter') {
                $scope.xScroll = !1
            } else {
                $scope.xScroll = !0
            }
        }
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'manage_board':
                var cTemplate = 'ManageBoard';
                var ctas = 'vm';
                break;
            case 'manage_deal':
                var cTemplate = 'CreateDeal';
                var ctas = 'show';
                break;
            case 'modify_stage':
                var cTemplate = 'ModifyStage';
                var ctas = 'ms';
                break;
            case 'show_activity':
                var cTemplate = 'DealActivies';
                var ctas = 'ls';
                break;
            case 'column_set':
                var cTemplate = 'ColumnSettings';
                var ctas = 'vm';
                break
        }
        var params = {
            template: 'cTemplate/' + cTemplate + 'Modal',
            ctrl: cTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'manage_board':
                params.params = {
                    'do': 'customers-deals',
                    'xget': 'BoardsList',
                    'only_boards': !0
                };
                params.callback = function() {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('post', 'index.php', {
                        'do': 'customers-deals',
                        'xget': 'DealsList',
                        'board_id': $scope.old_board_id
                    }, function(r) {
                        $scope.item.board_list = r.data.board_list;
                        $scope.item.deals_list.list = r.data.list;
                        $scope.search.board_id = r.data.board_id
                    })
                }
                break;
            case 'manage_deal':
                params.params = {
                    'do': 'customers-deal_edit',
                    'opportunity_id': item.deal_id,
                    'stage_id': item.stage_id,
                    'board_id': item.board_id,
                    'user_id': item.user_id,
                    'buyer_id': item.buyer_id,
                    'contact_id': item.contact_id
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        if($scope.view == 1){
                            if ($scope.item.board_id == d.data.board_id) {
                                $scope.item.deals_list = d.data;
                                $timeout(function() {
                                    updateStageHeight()
                                })
                            }
                        }else{
                            var params = {};
                            for (x in $scope.search) {
                                params[x] = $scope.search[x]
                            }
                            //angular.element('.loading_wrap').removeClass('hidden');
                            helper.doRequest('get', 'index.php', params, function(r) {
                                $scope.renderList(r)
                            })
                        }                     
                        $scope.showAlerts(d)
                    }
                }
                break;
            case 'modify_stage':
                params.params = {
                    'do': 'customers-modify_stage',
                    'old_stage_id': $scope.old_stage_id,
                    'old_board_id': $scope.old_board_id
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.item.deals_list.list = d.data.list;
                        $scope.showAlerts(d)
                    }
                }
                break;
            case 'show_activity':
                params.params = {
                    'do': 'misc-activities',
                    'filter_type': 'customer',
                    'xget': 'Activity',
                    'customer_id': item.buyer_id,
                    'is_zen': $scope.item.is_zen,
                    'opportunity_id': item.deal_id
                };
                break;
            case 'column_set':
                params.backdrop = 'static';
                params.template = 'miscTemplate/' + cTemplate + 'Modal';
                params.params = {
                    'do': 'misc-column_set',
                    'list': 'deals'
                };
                params.callback = function(d) {
                    if (d) {
                        $state.reload()
                    }
                }
                break
        }
        modalFc.open(params)
    }
    // $scope.renderPage(data)
    list.init($scope, 'customers-deals', $scope.renderPage(data), ($rootScope.reset_lists.deals ? true : false));
    $rootScope.reset_lists.deals = false;
}]).controller('DealViewCtrl', ['$scope', '$compile', '$sce', '$state', '$window', '$timeout','helper', '$stateParams', '$uibModal', '$confirm', '$cacheFactory', 'modalFc', 'editItem', 'selectizeFc', 'data', 'Lightbox', function($scope, $compile, $sce, $state, $window, $timeout, helper, $stateParams, $uibModal, $confirm, $cacheFactory, modalFc, editItem, selectizeFc, data, Lightbox) {
    
    var edit = this,
        typePromise;
    edit.stage_saved = '';
    edit.app = 'customers';
    edit.ctrlpag = 'deal_edit';
    edit.tmpl = 'cTemplate';
    edit.item_id = $stateParams.opportunity_id;
    edit.opportunity_id = edit.item_id;
    edit.buyer_id = $stateParams.buyer_id;
    edit.search_track = {
        search: '',
        'do': 'customers-deal_edit',
        view: 0,
        offset: 1,
        start_date: '',
        end_date: '',
        original_d: '0',
        target_d: '0',
        opportunity_id: edit.opportunity_id
    };

    edit.languages = $stateParams.languages;
    edit.track_base = $stateParams.track_base;
    edit.your_ref = $stateParams.your_ref;
    edit.hide_content = !0;
    edit.item = {};
    edit.oldObj = {};
    edit.item.validity_date = new Date();
    edit.removedCustomer = !1;
    edit.item.add_customer = (edit.item_id != 'tmp' ? !0 : !1);
    edit.adjustP = [];
    edit.showTxt = {
        addBtn: !0,
        address: !0,
    }
    edit.item.dropbox = {};
    edit.show_load = !0;
    edit.contactAddObj = {};
    edit.auto = {
        options: {
            contacts: [],
            addresses: [],
            cc: []
        }
    };
    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {
        start_date: !1,
        end_date: !1,
        exp_close_date: !1,
        exp_close_date_js: !1
    }
    edit.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    edit.viewCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All items in timeline'),
        create: !1,
        maxItems: 1,
        onChange(id) {
            edit.toggleSearchButtons('view', id)
        }
    };
    edit.userAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.value) + ' </span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        maxItems: 1
    };

    edit.boardCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        onChange(value) {
            if (value == '' || value == undefined) {
                edit.item.stage_id = undefined;
                edit.item.stage_dd = []
            } else {
                var obj = {
                    'do': 'customers-deal_edit',
                    'xget': 'StagesList',
                    'board_id': value
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    edit.item.stage_dd = r.data;
                    if(edit.item.stage_dd.length){
                        edit.item.stage_id=edit.item.stage_dd[0].id;
                    }
                })
            }
        },
        onDelete(value){
            return false;
        },
        maxItems: 1
    }

    edit.renderPage = renderPage;
    edit.openDate = openDate;
    edit.showEditFnc = showEditFnc;
    edit.showCCSelectize = showCCSelectize;
    edit.cancelEdit = cancelEdit;
    edit.removeDeliveryAddr = removeDeliveryAddr;
    edit.openModal = openModal;
    edit.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: "auto",
        height: 192,
        menubar: !1,
        autoresize_bottom_margin: 0,
        relative_urls: !1,
        trusted: !0,
        debouncetime: 5000,
        onblur: !0,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        statusbar: !1,
        toolbar: ["bold italic | bullist numlist "]
    };
    editItem.init(edit);
    function renderPage(d) {
        if (d && d.data) {
            edit.item = d.data;
            if(!d.data.item_exists){
                edit.item.item_exists = false;
                return 0;
            };
            edit.stage_saved = edit.item.stage_id;           
            $scope.$parent.page_title = edit.item.page_title;
            $scope.$parent.serial_number = edit.item.serial_number;
            helper.updateTitle($state.current.name,edit.item.serial_number);
            $scope.$parent.subject = edit.item.subject;
            edit.auto.options.contacts = d.data.contacts;
            edit.auto.options.addresses = d.data.addresses;
            edit.auto.options.cc = d.data.cc;
            edit.contactAddObj = {
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                language_dd: edit.item.language_dd,
                language_id: edit.item.language_id,
                gender_dd: edit.item.gender_dd,
                gender_id: edit.item.gender_id,
                title_dd: edit.item.title_dd,
                title_id: edit.item.title_id,
                add_contact: !0,
                buyer_id: edit.item.buyer_id,
                article: edit.item.tr_id,
                'do': 'customers-deal_edit-customers-tryAddC',
                identity_id: edit.item.identity_id,
                contact_only: !0,
                item_id: edit.item_id
            }
            angular.element('.page-header-options .text-right').find('navigator').remove();
            angular.element('.page-header-options .text-right').append('<navigator class="navigator_right" pag="customers-deals" itemid="'+edit.item.opportunity_id+'" originalPage="dealView"></navigator');
            $compile(angular.element('.page-header-options .text-right').find('navigator'))($scope);
            edit.showDrop();

            edit.views = [{
                name: helper.getLanguage('All items in timeline'),
                id: 0,
                active: 'active'
            }, {
                name: helper.getLanguage('Documents'),
                id: 1,
                active: ''
            }, {
                name: helper.getLanguage('Activities'),
                id: 2,
                active: ''
            }];
            if(edit.item.is_5){
                edit.views.push({
                    'id': '2-1',
                    'name': helper.getLanguage('Quote')
                });
            }
            if(edit.item.is_6){
                edit.views.push({
                    'id': '4',
                    'name': helper.getLanguage('Order')
                });
                edit.views.push({
                    'id': '6',
                    'name': helper.getLanguage('Purchase Order')
                });
            }
            if(edit.item.is_6){
                edit.views.push({
                    'id': '4',
                    'name': helper.getLanguage('Order')
                });
            }
            if(edit.item.is_13){
                edit.views.push({
                    'id': '5',
                    'name': helper.getLanguage('Intervention')
                });
            }
            if(edit.item.is_4){
                edit.views.push({
                    'id': '1-1',
                    'name': helper.getLanguage('Invoice')
                });
                edit.views.push({
                    'id': '8',
                    'name': helper.getLanguage('Credit Invoice')
                });
                edit.views.push({
                    'id': '7',
                    'name': helper.getLanguage('Proforma')
                });
            }
            
        }
    }

    function openDate(p) {
        edit.datePickOpen[p] = !0
    }

    function removeDeliveryAddr() {
        edit.item.delivery_address = '';
        edit.item.delivery_address_id = ''
    }

    function cancelEdit() {
        if (Object.keys(edit.oldObj).length == 0) {
            return edit.removeCust(edit)
        }
        edit.showTxt.address = !0;
        for (x in edit.oldObj) {
            if (edit.item[x]) {
                if (x == 'delivery_address') {
                    edit.item[x] = edit.oldObj[x].trim()
                } else {
                    edit.item[x] = edit.oldObj[x]
                }
            }
        }
        edit.item.buyer_id = edit.oldObj.buyer_id
    }

    function showEditFnc() {
        edit.showTxt.address = !1;
        edit.item.save_final = !0;
        edit.oldObj = {
            buyer_id: angular.copy(edit.item.buyer_id),
            contact_id: angular.copy(edit.item.contact_id),
            contact_name: angular.copy(edit.item.contact_name),
            BUYER_NAME: angular.copy(edit.item.buyer_name),
            delivery_address: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address)),
            delivery_address_id: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_id)),
            delivery_address_txt: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_txt)),
            view_delivery: (edit.item.sameAddress ? !1 : angular.copy(edit.item.view_delivery)),
            sameAddress: angular.copy(edit.item.sameAddress),
        }
    }

    function showCCSelectize() {
        edit.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }
    edit.setStage = function() {
        var obj = {
            'do': 'customers-deal_edit-deals-modifyDealStage',
            'opportunity_id': edit.item.opportunity_id,
            'stage_id': edit.item.stage_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                edit.showTxt.stage = !1;
                edit.item.stage_name = r.data.stage_name;
                edit.item.board_name = r.data.board_name
            }
        })
    }
    edit.cancel = function() {
        if (isNaN(edit.opportunity_id)) {
            $state.go('deals')
        } else {
            $state.go('dealView', {
                opportunity_id: edit.opportunity_id
            })
        }
    }
    edit.setExpCloseDate = function() {
        var initDate = new Date(edit.item.exp_close_date_js).getTime() / 1000;
        edit.item.exp_close_date_js = new Date((initDate + 7200) * 1000);
        edit.item.exp_close_date2 = initDate
    }
    edit.editNotes = function() {
        if (typePromise) {
            $timeout.cancel(typePromise);
        }
        typePromise = $timeout(function() {
            var obj = {
                'do': 'customers-deal_edit-deals-updateDealNotes',
                'opportunity_id': edit.item.opportunity_id,
                'deal_notes': edit.item.deal_notes
            };
            helper.doRequest('post', 'index.php', obj, function(r) {
                if (r.success) {
                    //edit.item.deal_notes = r.data.deal_notes
                }
            })
        }, 500);       
    }
    edit.showAlerts = function(d) {
        helper.showAlerts(edit, d)
    }
    edit.renderActivities = function(r) {
        if (r.data) {
            edit.item.list = r.data.list;
            edit.showAlerts(r)
        }
    }
    edit.toggleSearchButtons = function(item, value) {
        var obj = {
            'do': 'customers-deal_edit',
            'opportunity_id': edit.item.opportunity_id,
            view: value
        };
        helper.doRequest('get', 'index.php', obj, edit.renderActivities)
    }
    edit.createDocument = function(type, item) {
        switch (type) {
            case 'quote':
                var link = 'quote.edit';
                var obj = {
                    'quote_id': 'tmp',
                    'from_customer':true
                };
                break;
            case 'order':
                var link = 'order.edit';
                var obj = {
                    'order_id': 'tmp',
                    'buyer_id': 'tmp'
                };
                break;
            case 'project':
                var link = 'project.edit';
                var obj = {
                    'project_id': 'tmp'
                };
                break;
            case 'service':
                var link = 'service.edit';
                var obj = {
                    'service_id': 'tmp'
                };
                break;
            case 'contract':
                var link = 'contract.edit';
                var obj = {
                    'contract_id': 'tmp'
                };
                break;
            case 'invoice':
                var link = 'invoice.edit';
                var obj = {
                    'invoice_id': 'tmp'
                };
                break
        }
        var cacheDeal = $cacheFactory.get('DealToDocument');
        if (cacheDeal != undefined) {
            cacheDeal.destroy()
        }
        var cache = $cacheFactory('DealToDocument');
        cache.put('data', {
            'deal_id': item.opportunity_id
        });
        $state.go(link, obj)
    }
    edit.opendocument = function(edit, type_id, id_value, version_id) {
        if (type_id == 'invoice_id') {
            var invoice_id;
            var url = $state.href(edit, {
                invoice_id: id_value
            })
        } else if (type_id == 'quote_id') {
            var quote_id;
            var url = $state.href(edit, {
                quote_id: id_value, version_id:version_id
            })
        } else if (type_id == 'project_id') {
            var project_id;
            var url = $state.href(edit, {
                project_id: id_value
            })
        } else if (type_id == 'order_id') {
            var order_id;
            var url = $state.href(edit, {
                order_id: id_value
            })
        } else if (type_id == 'service_id') {
            var service_id;
            var url = $state.href(edit, {
                service_id: id_value
            })
        } else if (type_id == 'purchase_order_id') {
            var p_order_id;
            var url = $state.href(edit, {
                p_order_id: id_value
            })
        } else if (type_id == 'contract_id') {
            var contract_id;
            var url = $state.href(edit, {
                contract_id: id_value
            })
        } else if (type_id == 'purchase_invoice_id') {
            var incomming_invoice_id;
            var url = $state.href(edit, {
                invoice_id: id_value
            })
        } else if (type_id == 'installation_id') {
            var installation_id;
            var url = $state.href(edit, {
                installation_id: id_value
            })
        } else if (type_id == 'opportunity_id') {
            var opportunity_id;
            var url = $state.href(edit, {
                opportunity_id: id_value
            })
        }
        $window.open(url, '_blank')
    }
    edit.showActivities = function(item) {
        item.closeDropdown = !1;
        item.showDocs = !1;
        openModal('show_activity', item, 'md')
    }
    edit.editActivities = function(item, $index) {
        openModal('EditActivity', item, 'md')
    }
    edit.removeActivity = function(item, $index) {
        var data = {
            customer_id: item.customer_id,
            do: 'misc--customers-delete_activity',
            id: item.log_id
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.success) {
                edit.item.list.splice($index, 1)
            }
        })
    }

    edit.showDrop = function() {
        edit.show_load = !0;
        var data_drop = angular.copy(edit.item.drop_info);
        data_drop.do = 'misc-dropbox_new';
        helper.doRequest('post', 'index.php', data_drop, function(r) {
            edit.item.dropbox = r.data;
            edit.show_load = !1
        })
    }
    edit.deleteDropFile = function(index) {
        var data = angular.copy(edit.item.dropbox.dropbox_files[index].delete_file_link);
        edit.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.show_load = !1;
            edit.showAlerts(r);
            if (r.success) {
                edit.item.dropbox.nr--;
                edit.item.dropbox.dropbox_files.splice(index, 1)
            }
        })
    }
    edit.deleteDropImage = function(index) {
        var data = angular.copy(edit.item.dropbox.dropbox_images[index].delete_file_link);
        edit.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.show_load = !1;
            edit.showAlerts(r);
            if (r.success) {
                edit.item.dropbox.nr--;
                edit.item.dropbox.dropbox_images.splice(index, 1)
            }
        })
    }
    edit.openLightboxModal = function(index) {
        Lightbox.openModal(edit.item.dropbox.dropbox_images, index)
    };
    edit.uploadDoc = function(type) {
        var obj = angular.copy(edit.item.drop_info);
        obj.type = type;
        openModal('uploadDoc', obj, 'lg')
    }

    edit.changeActivityStatus=function(item){
        var data = {};
        data.do = 'misc--customers-update_status';
        data.log_code = item.log_code;
        data.status_change = item.color == 'green_status' ? 0 : 1;
        data.return_data='1';
        helper.doRequest('post', 'index.php', data ,function(r){
            if(r.success){
                var obj = {
                    'do': 'customers-deal_edit',
                    'opportunity_id': edit.item.opportunity_id
                };
                helper.doRequest('get', 'index.php', obj, edit.renderActivities)
            }
        });
    }

    edit.editDeal = function(type) {
        openModal(type, edit, 'md')
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'show_activity':
                //var cTemplate = 'DealActivies';
                //var ctas = 'ls';              
                var params = {
                    template: 'miscTemplate/NewActivitiesModal/',
                    ctrl: 'NewActivitiesModalCtrl',
                    ctrlAs: 'vmMod',
                    size: size,
                    backdrop: 'static'
                };
                params.initial_item = {
                    'opportunity_id': item.opportunity_id,
                    'customer_id': item.buyer_id,
                    'disable_customer':true
                };
                params.params = {
                    'do': 'customers-deal_edit',
                    'filter_type': 'customer',
                    'xget': 'Activity',
                    'customer_id': item.buyer_id,
                    'opportunity_id': item.opportunity_id,
                    'is_zen': edit.item.is_zen
                };
                params.callback = function(r) {
                    data = {
                        'do': 'customers-deal_edit',
                        'filter_type': 'customer',
                        'customer_id': item.buyer_id,
                        'opportunity_id': item.opportunity_id,
                        'is_zen': edit.item.is_zen
                    };
                    helper.doRequest('get', 'index.php', data, edit.renderActivities)
                }
                break;
            case 'EditActivity':
                var newData = angular.copy(item);
                newData.reminder_dd=edit.item.list_activities.reminder_dd;
                newData.allow_delete=true;
                newData.customer_id = item.customer_id;
                newData.opportunity_id = edit.opportunity_id;
                newData.filter_type = 'user_timeline';
                newData.offset = edit.offset;
                newData.account_date_format = edit.item.list_activities.account_date_format;
                var params = {
                    template: 'miscTemplate/' + type + 'Modal',
                    ctrl: type + 'ModalCtrl',
                    size: size,
                    backdrop: 'static'
                };
                params.item = newData;
                params.callback = function(r) {
                    edit.renderActivities(r)
                }
                break;
            case 'uploadDoc':
                var params={};
                params.template = 'miscTemplate/AmazonUploadModal';
                params.ctrl ='AmazonUploadModalCtrl';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        edit.showDrop()
                    }
                }
                break;
            case 'EditDeal':
                var newData = angular.copy(item);
                newData.customer_id = item.customer_id;
                newData.opportunity_id = edit.opportunity_id;
                var params = {
                    template: 'miscTemplate/' + type + 'Modal',
                    ctrl: type + 'ModalCtrl',
                    size: size,
                    backdrop: 'static'
                };
                params.item = newData;
                break;

        }
        modalFc.open(params)
    }
    edit.renderPage(data)
}]).controller('ManageBoardModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, helper, $uibModalInstance, data, modalFc) {
    var vm = this;
    vm.obj = data;
    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d)
    }
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.addBoard = function(id) {
        openModal('create_board', id, 'md')
    }
    vm.editBoard = function(id) {
        openModal('create_board', id, 'md')
    }
    vm.deleteBoard = function(id) {
        var obj = {
            'do': 'customers-deals-deals-deleteBoard',
            'board_id': id,
            'xget': 'BoardsList',
            'only_boards': !0
        };
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            vm.showAlerts(r);
            if (r.success) {
                vm.obj = r.data
            }
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'create_board':
                var cTemplate = 'CreateBoard';
                var ctas = 'vm';
                break
        }
        var params = {
            template: 'cTemplate/' + cTemplate + 'Modal',
            ctrl: cTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'create_board':
                params.params = {
                    'do': 'customers-deals',
                    'xget': 'Board',
                    'board_id': item
                };
                params.callbackExit = function() {
                    //angular.element('.loading_alt').removeClass('hidden');
                    helper.doRequest('post', 'index.php', {
                        'do': 'customers-deals',
                        'xget': 'BoardsList',
                        'only_boards': !0
                    }, function(r) {
                        //angular.element('.loading_alt').addClass('hidden');
                        vm.obj = r.data
                    })
                }
                params.callback = function(d) {
                    if (d && d.data) {
                        vm.obj = d.data;
                        vm.showAlerts(d)
                    }
                }
                break
        }
        modalFc.open(params)
    }
}]).controller('CreateBoardModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.showAlerts = function(d, formObj) {
        helper.showAlerts(vm, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss()
    }
    vm.removeLine = function(index) {
        if (vm.obj.stages[index].id == '') {
            vm.obj.stages.splice(index, 1)
        } else {
            var obj = {
                'do': 'customers--deals-deleteStage',
                'stage_id': vm.obj.stages[index].id
            };
            //angular.element('.loading_alt').removeClass('hidden');
            helper.doRequest('post', 'index.php', obj, function(r) {
                //angular.element('.loading_alt').addClass('hidden');
                vm.showAlerts(r);
                if (r.success) {
                    vm.obj.stages.splice(index, 1)
                }
            })
        }
    }
    vm.addStage = function() {
        var tmp_obj = {
            'id': '',
            'name': ''
        };
        vm.obj.stages.push(tmp_obj)
    }
    vm.ok = function(formObj) {
        var obj = angular.copy(vm.obj);
        if (obj.board_id == 'tmp') {
            obj.do = 'customers-deals-deals-addBoard'
        } else {
            obj.do = 'customers-deals-deals-editBoard'
        }
        obj.xget = 'BoardsList';
        obj.only_boards = !0;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.error) {
                vm.showAlerts(r, formObj)
            } else {
                $uibModalInstance.close(r)
            }
        })
    }
    vm.dealTypeCfg = {
        valueField: 'id',
        labelField: 'name',
        placeholder: helper.getLanguage('Success rate'),
        searchField: ['name'],
        maxOptions: 50,
        maxItems: 1,
        create: !1
    }
}]).controller('CreateDealModalCtrl', ['$scope', '$timeout', '$state','helper', '$uibModalInstance', 'data', 'editItem', function($scope, $timeout, $state, helper, $uibModalInstance, data, editItem) {
    var show = this;
    console.log('Create Deal Modal');
    show.app = 'customers';
    show.ctrlpag = 'deal_edit';
    show.app_mod = 'deal';
    show.item = {};
    show.oldObj = {};
    show.removedCustomer = !1;
    show.contactAddObj = {};
    show.auto = {
        options: {
            contacts: [],
            addresses: [],
            cc: []
        }
    };
    show.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    show.boardCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        onChange(value) {
            if (value == '' || value == undefined) {
                show.item.stage_id = undefined;
                show.item.stages_list = []
            } else {
                var obj = {
                    'do': 'customers-deal_edit',
                    'xget': 'StagesList',
                    'board_id': value
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    show.item.stage_id = undefined;
                    show.item.stages_list = r.data
                })
            }
        },
        maxItems: 1
    }
    show.step = 1;
    show.renderPage = renderPage;
    show.renderP = function(d) {
        show.item = d;
        if (show.item.opportunity_id != 'tmp') {
            show.step = 2
        }
        show.auto.options.contacts = d.contacts;
        show.auto.options.addresses = d.addresses;
        show.auto.options.cc = d.cc;
        if (show.item.close_date) {
            show.item.close_date = new Date(show.item.close_date)
        }
        show.contactAddObj = {
            country_dd: show.item.country_dd,
            title_dd: show.item.title_dd,
            language_dd: show.item.language_dd,
            gender_dd: show.item.gender_dd,
            country_id: show.item.main_country_id,
            add_contact: !0,
            buyer_id: show.item.buyer_id,
            article: show.item.tr_id,
            'do': 'customers-deal_edit-customers-tryAddC',
            identity_id: show.item.identity_id,
            contact_only: !0,
            item_id: show.item.opportunity_id,
            board_id: show.item.board_id,
            stage_id:show.item.stage_id,
            user_id: show.item.user_id
        }
    }
    show.renderP(data);
    show.item_id = show.item.opportunity_id;

    function renderPage(d) {
        if (d && d.data) {
            show.item = d.data;
            if (show.item.opportunity_id != 'tmp') {
                show.step = 2
            }
            show.auto.options.contacts = d.data.contacts;
            show.auto.options.addresses = d.data.addresses;
            show.auto.options.cc = d.data.cc;
            if (show.item.close_date) {
                show.item.close_date = new Date(show.item.close_date)
            }
            show.contactAddObj = {
                country_dd: show.item.country_dd,
                title_dd: show.item.title_dd,
                language_dd: show.item.language_dd,
                gender_dd: show.item.gender_dd,
                country_id: show.item.main_country_id,
                add_contact: !0,
                buyer_id: show.item.buyer_id,
                article: show.item.tr_id,
                'do': 'customers-deal_edit-customers-tryAddC',
                identity_id: show.item.identity_id,
                contact_only: !0,
                item_id: show.item_id,
                board_id: show.item.board_id,
                stage_id:show.item.stage_id,
                user_id: show.item.user_id
            }
        }
    }
    show.datePickOpen = {
        close_date: !1
    };
    show.format = 'dd/MM/yyyy';
    show.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    show.showTxt = {
        addBtn: !0,
        address: !1,
        sevice_settings: !1,
    }
    editItem.init(show);
    show.showAlerts = function(d, formObj) {
        helper.showAlerts(show, d, formObj)
    }
    show.cancel = function() {
        $uibModalInstance.close()
    }
    show.ok = function(formObj) {
        var obj = angular.copy(show.item);
        if (obj.opportunity_id == 'tmp') {
            obj.do = 'customers--deals-addDeal'
        } else {
            obj.do = 'customers--deals-updateDeal'
        }
        //obj.xget = 'DealsList';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.error) {
                show.showAlerts(r, formObj)
            } else {
                if(show.item.not_redirect_deal){
                      $uibModalInstance.close({'deal_id':r.data.opportunity_id});
                }else{
                    $uibModalInstance.close();
                    $state.go('dealView', {
                        opportunity_id: r.data.opportunity_id
                    });
                }
            
            }
        })
    }
    show.deleteDeal = function() {
        var obj = {
            'do': 'customers-deals-deals-deleteDeal',
            'opportunity_id': show.item.opportunity_id,
            'board_id': show.item.board_id,
            'user_id': show.item.user_id,
            'xget': 'DealsList'
        };
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            show.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
    show.archiveDeal = function() {
        var obj = {
            'do': 'customers-deals-deals-archiveDeal',
            'opportunity_id': show.item.opportunity_id,
            'board_id': show.item.board_id,
            'user_id': show.item.user_id,
            'xget': 'DealsList'
        };
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            show.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
    show.showEditFnc = function() {
        show.showTxt.address = !1;
        show.item.save_final = !0;
        show.oldObj = {
            buyer_id: angular.copy(show.item.buyer_id),
            contact_id: angular.copy(show.item.contact_id),
            contact_name: angular.copy(show.item.contact_name),
            BUYER_NAME: angular.copy(show.item.buyer_name),
            delivery_address: (show.item.sameAddress ? !1 : angular.copy(show.item.delivery_address)),
            delivery_address_id: (show.item.sameAddress ? !1 : angular.copy(show.item.delivery_address_id)),
            delivery_address_txt: (show.item.sameAddress ? !1 : angular.copy(show.item.delivery_address_txt)),
            view_delivery: (show.item.sameAddress ? !1 : angular.copy(show.item.view_delivery)),
            sameAddress: angular.copy(show.item.sameAddress),
        }
    }
    show.showCCSelectize = function() {
        show.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }
    show.removeDeliveryAddr = function() {
        show.item.delivery_address = '';
        show.item.delivery_address_id = ''
    }
    show.userAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.value) + ' </span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        maxItems: 1
    }
}]).controller('ModifyStageModalCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'data', 'editItem', function($scope, $timeout, helper, $uibModalInstance, data, editItem) {
    var ms = this;
    console.log('Modify Stage Modal');
    ms.app = 'customers';
    ms.ctrlpag = 'modify_stage';
    ms.app_mod = 'deal';
    ms.item = {};
    ms.oldObj = {};
    ms.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    ms.boardCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        onChange(value) {
            if (value == '' || value == undefined) {
                ms.item.stage_id = undefined;
                ms.item.stages_list = []
            } else {
                var obj = {
                    'do': 'customers-modify_stage',
                    'board_id': value
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    ms.item.stage_id = undefined;
                    ms.item.stages_list = r.data.stages_list
                })
            }
        },
        maxItems: 1
    }
    ms.renderPage = renderPage;
    ms.renderP = function(d) {
        ms.item = d
    }
    ms.renderP(data);
    ms.item_id = ms.item.opportunity_id;

    function renderPage(d) {
        if (d && d.data) {
            ms.item = d.data
        }
    }
    ms.showTxt = {
        addBtn: !0,
        address: !0,
        sevice_settings: !1,
    }
    editItem.init(ms);
    ms.showAlerts = function(d, formObj) {
        helper.showAlerts(ms, d, formObj)
    }
    ms.cancel = function() {
        $uibModalInstance.close()
    }
    ms.ok = function(formObj) {
        var obj = angular.copy(ms.item);
        obj.do = 'customers-deals-deals-modifyStage';
        obj.xget = 'DealsList';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.error) {
                show.showAlerts(r, formObj)
            } else {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('VatCompanyModalCtrl', ['$scope', '$uibModalInstance', '$timeout', 'helper', 'modalFc', 'data', function($scope, $uibModalInstance, $timeout, helper, modalFc, data) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.cancel = function() {
        $uibModalInstance.close()
    };
    vmMod.showAlerts = function(d, formObj) {
        helper.showAlerts(vmMod, d, formObj)
    }
}]).controller('NylasEmailModalCtrl', ['$scope', '$uibModalInstance', 'helper', 'data', 'modalFc', function($scope, $uibModalInstance, helper, data, modalFc) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.cancel = function() {
        $uibModalInstance.close()
    };
    vmMod.showAlerts = function(d) {
        helper.showAlerts(vmMod, d)
    }
    vmMod.ok = function() {
        var new_data = {
            'list': []
        };
        for (x in vmMod.obj.list) {
            if (vmMod.obj.list[x].checked) {
                new_data.list.push(vmMod.obj.list[x])
            }
        }
        new_data.do_next = 'customers--customers-addNylasEmail';
        new_data.type = 1;
        openModal("sync_incoming", new_data, "md")
    }
    vmMod.selectAll = function(type) {
        var bool = !1;
        if (type == 1) {
            bool = !0
        }
        for (x in vmMod.obj.list) {
            vmMod.obj.list[x].checked = bool
        }
    }
    vmMod.viewMore = function(item) {
        var cont = {
            "e_message": item.body
        };
        openModal("stock_info", cont, "lg")
    }
    vmMod.viewAttach = function(item) {
        openModal("email_attach", item, "md")
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'sync_incoming':
                var iTemplate = 'SyncIncomming';
                var ctas = 'sync';
                break;
            case 'stock_info':
                var iTemplate = 'modal';
                var ctas = 'vmMod';
                break;
            case 'email_attach':
                var iTemplate = 'EmailAttach';
                var ctas = 'vmMod';
                break
        }
        var params = {
            template: 'miscTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'sync_incoming':
                params.item = item;
                params.callback = function() {
                    //angular.element('.loading_alt').removeClass('hidden');
                    helper.doRequest('get', 'index.php?do=customers-nylas_email&customer_id=' + vmMod.obj.customer_id, function(r) {
                        //angular.element('.loading_alt').addClass('hidden');
                        vmMod.obj = r.data
                    })
                }
                break;
            case 'stock_info':
                params.template = 'miscTemplate/' + iTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                break;
            case 'email_attach':
                params.item = item;
                break
        }
        modalFc.open(params)
    }
}]).controller('AlloCallModalCtrl', ['$scope', '$uibModalInstance', 'helper', 'data', 'selectizeFc', function($scope, $uibModalInstance, helper, data, selectizeFc) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.acc_device_cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Devices'),
    });
    vmMod.cancel = function() {
        $uibModalInstance.close()
    };
    vmMod.showAlerts = function(d, formObj) {
        helper.showAlerts(vmMod, d, formObj)
    }
    vmMod.ok = function(formObj) {
        var obj = angular.copy(vmMod.obj);
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.error) {
                vmMod.showAlerts(r, formObj)
            } else {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('EmailAttachModalCtrl', ['$scope', '$window', '$uibModalInstance', 'helper', 'data', function($scope, $window, $uibModalInstance, helper, data) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.cancel = function() {
        $uibModalInstance.close()
    };
    vmMod.showAlerts = function(d) {
        helper.showAlerts(vmMod, d)
    }
    vmMod.download = function(item) {
        var url = 'index.php?do=misc-nylas_download&id=' + item.id + '&filename=' + item.filename + '&content_type=' + item.content_type;
        $window.open(url, '_blank')
    }
}]).controller('DealActiviesModalCtrl', ['$scope', '$uibModalInstance', '$timeout', 'helper', 'data', 'selectizeFc', 'modalFc', function($scope, $uibModalInstance, $timeout, helper, data, selectizeFc, modalFc) {
    var ls = this,
        typePromise, currentTime = new Date(),
        current = undefined;
    ls.showMoreField = !1;
    ls.showAddresses = !1;
    ls.showActivityForm = !1;
    ls.showActivityiny = !1;
    ls.activityTxt = helper.getLanguage('Type');
    ls.activity = {
        hd_events: !1
    }
    ls.activityNumber = 5;
    ls.activityLoaded = !1;
    ls.hideSwitch = !1;
    for (x in data) {
        ls[x] = data[x]
    }
    if (ls.activities.is_data == !1 || ls.activities.item.length < 3) {
        $timeout(function() {
            angular.element('.modal-body').addClass('modal-body_alt')
        })
    }
    ls.activity.location_meet = ls.activities.location_meet;
    helper.doRequest('get', 'index.php', {
        'do': 'customers-customer_contacts',
        showList: !1,
        offset: 1,
        'customer_id': ls.activities.customer_id
    }, function(o) {
        ls.contactsActitivty = [];
        for (x in o.data.list) {
            ls.contactsActitivty.push({
                'id': o.data.list[x].contact_id,
                'value': (o.data.list[x].con_firstname + ' ' + o.data.list[x].con_lastname),
                'zen_id': o.data.list[x].zen_id
            })
        }
    });
    ls.datePickOpen = {
        start_date: !1,
        end_date: !1
    };
    ls.activityParams = {
        'do': 'misc-activities',
        'customer_id': ls.activities.customer_id,
        'opportunity_id': ls.activities.opportunity_id,
        'xget': 'Activity',
        filter_type: 'customer'
    };
    ls.format = 'dd/MM/yyyy';
    ls.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    ls.hours = [];
    ls.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    for (var i = 0; i <= 23; i++) {
        for (var j = 0; j <= 3; j++) {
            var min = j * 15,
                text = (i < 10 ? '0' + i : i) + ':' + (min < 10 ? '0' + min : min);
            ls.hours.push({
                'id': i + '-' + min,
                'value': text
            });
            if (i <= currentTime.getHours() && min <= currentTime.getMinutes()) {
                current = i + '-' + min
            }
        }
    }
    ls.cancel = function() {
        $uibModalInstance.close()
    };
    ls.showAlerts = function(d) {
        helper.showAlerts(ls, d)
    }
    ls.renderActivities = function(r) {
        if (r.data) {
            for (x in r.data) {
                ls[x] = r.data[x]
            }
            if (ls.activities.is_data == !1 || ls.activities.item.length < 3) {
                $timeout(function() {
                    angular.element('.modal-body').addClass('modal-body_alt')
                })
            } else {
                $timeout(function() {
                    angular.element('.modal-body').removeClass('modal-body_alt')
                })
            }
            ls.activity.location_meet = ls.activities.location_meet;
            ls.showAlerts(r)
        }
    }
    ls.changeTime = function(item, t, check) {
        if (item && t) {
            var d = t.split('-');
            item.setHours(d[0], d[1]);
            if (check) {
                ls.changeDuration();
                ls.checkAllDay()
            }
        }
    }
    ls.showActivityF = function(show, item, type) {
        ls.showActivityForm = show;
        ls.activityTxt = helper.getLanguage(item);
        ls.activity.task_type = type;
        ls.activity.type_of_call = undefined;
        ls.activity.hd_events = !1;
        ls.hideSwitch = !1;
        if (show == !1) {
            ls.activity.comment = '';
            ls.activity.log_comment = '';
            ls.activity.contact_ids = [];
            ls.activity.duration_meet = '';
            ls.showAddActivity = !1
        }
        if (type == '1') {
            ls.activity.type_of_call = 0
        }
        if (type == '5') {
            ls.activity.type_of_call = 0
        }
        if (type == '6') {
            ls.hideSwitch = !0;
            ls.activity.hd_events = !0
        }
        if (type == '7') {
            ls.hideSwitch = !0
        }
        ls.activity.reminderHours = current;
        ls.activity.dateHours = current;
        ls.activity.dateEHours = current;
        ls.activity.date = new Date();
        ls.activity.reminder_event = new Date();
        ls.activity.end_d_meet = new Date();
        ls.changeTime(ls.activity.date, ls.activity.dateHours);
        ls.changeTime(ls.activity.reminder_event, ls.activity.reminderHours);
        ls.changeTime(ls.activity.end_d_meet, ls.activity.dateEHours);
    }
    ls.changeDuration = function() {
        if (ls.activity.duration_meet) {
            var d = ls.activity.duration_meet.split('-'),
                currentH = angular.copy(ls.activity.dateHours).split('-');
            ls.activity.end_d_meet.setHours(ls.activity.date.getHours() + parseInt(d[0]), ls.activity.date.getMinutes() + parseInt(d[1]));
            var minutes = parseInt(currentH[1]) + parseInt(d[1]);
            var h = parseInt(currentH[0]) + parseInt(d[0]);
            if (minutes >= 60) {
                minutes -= 60;
                h++
            }
            ls.activity.dateEHours = (h) + '-' + (minutes)
        } else {
            ls.activity.end_d_meet = new Date();
            ls.activity.dateEHours = current
        }
    }
    ls.checkAllDay = function() {
        var new_date = angular.copy(ls.activity.date),
            day = new_date.getDate();
        ls.activity.end_d_meet = new_date;
        ls.activity.end_d_meet.setDate(day + 1)
    }
    ls.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        menubar: !1,
        autoresize_bottom_margin: 0,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        statusbar: !1,
        toolbar: ["styleselect | bold italic | bullist numlist "]
    };
    ls.contactAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select contact'),
        onDropdownOpen($dropdown) {
            var contact_drop = angular.element('#contact_ids')[0].selectize;
            if (ls.activity.task_type == '7' && ls.activity.contact_ids && ls.activity.contact_ids.length) {
                contact_drop.close()
            }
        },
        onChange(value) {
            if (ls.activity.task_type == '7' && value.length) {
                var contact_drop = angular.element('#contact_ids')[0].selectize;
                if (!this.options[value].zen_id) {
                    var msg = helper.getLanguage('Contact needs to be synced first');
                    var cont = {
                        "e_message": msg
                    };
                    ls.openModal("stock_info", cont, "md");
                    ls.activity.contact_ids = []
                } else {
                    contact_drop.close()
                }
            }
        },
        maxItems: 1000
    });
    ls.timeAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Time'),
    });
    ls.userAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Assign to')
    });
    ls.addActivity = function() {
        var data = angular.copy(ls.activity);
        angular.merge(data, ls.activityParams);
        data.user_id = ls.activities.user_id;
        data.do = 'misc-activities-customers-add_activity';
        data.customer_name = ls.activities.customer_name;
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                ls.showAlerts(r)
            } else {
                ls.renderActivities(r);
                ls.showActivityF(!1, 'Type', '');
                ls.showActivityiny = !1
            }
        })
    }
    ls.openModal = function(type, item, size) {
        var params = {
            template: 'cTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'EditActivity':
                var newData = angular.copy(item);
                newData.customer_id = ls.activities.customer_id;
                newData.filter_type = 'customer';
                params.item = newData;
                params.template = 'miscTemplate/' + type + 'Modal/';
                params.callback = function(r) {
                    ls.renderActivities(r)
                }
                break;
            case 'stock_info':
                params.template = 'miscTemplate/modal';
                params.ctrl = 'viewRecepitCtrl';
                params.ctrlAs = 'vmMod';
                params.item = item;
                break;
            case 'ZenComment':
                params.template = 'miscTemplate/ZenCommentModal';
                params.ctrlAs = 'ls';
                //angular.element('.loading_alt').removeClass('hidden');
                params.params = {
                    'do': 'misc-zen_comments',
                    'ticket_id': item
                };
                break
        }
        modalFc.open(params)
    }
    ls.removeActivity = function(item, $index) {
        var data = {
            customer_id: ls.activities.customer_id,
            do: 'misc--customers-delete_activity',
            id: item.log_id
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.success) {
                ls.activities.item.splice($index, 1);
                if (ls.activities.is_data == !1 || ls.activities.item.length < 3) {
                    angular.element('.modal-body').addClass('modal-body_alt')
                } else {
                    angular.element('.modal-body').removeClass('modal-body_alt')
                }
            }
        })
    }
    ls.changeActivityStatus = function(item) {
        var data = angular.copy(ls.activityParams);
        data.do = 'misc-activities-customers-update_status';
        data.log_code = item.log_code;
        data.status_change = item.color == 'green_status' ? 0 : 1;
        helper.doRequest('post', 'index.php', data, ls.renderActivities)
    }
}]).controller('ZenCommentModalCtrl', ['$scope', '$uibModalInstance', 'helper', 'data', function($scope, $uibModalInstance, helper, data) {
    var ls = this;
    ls.activityNumber = 5;
    for (x in data) {
        ls[x] = data[x]
    }
    ls.cancel = function() {
        $uibModalInstance.close()
    };
    ls.showAlerts = function(d) {
        helper.showAlerts(ls, d)
    }
    ls.renderActivities = function(r) {
        if (r.data) {
            for (x in r.data) {
                ls[x] = r.data[x]
            }
        }
    }
}]).controller('AddressFieldCtrl', ['$scope', 'helper', function($scope, helper) {}]).controller('AddressFieldEditCtrl', ['$scope', '$stateParams', 'helper', 'data', 'modalFc', function($scope, $stateParams, helper, data, modalFc) {
    console.log("Address Field Edit");
    var edit = this,
        typePromise;
    edit.address_id = $stateParams.address_id;
    edit.item = {};
    var old_item = {};
    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {};
    edit.oldObj = {};
    edit.isAddPage = !0;
    edit.fieldCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Option'),
        closeAfterSelect: !0,
        onDropdownOpen($dropdown) {
            var _this = this,
                field_index = _this.$input[0].attributes.fieldid.value;
            if (edit.item.list[field_index].value != undefined && edit.item.list[field_index].value != '0') {
                old_item[edit.item.list[field_index].field_id] = edit.item.list[field_index].value
            }
        },
        render: {
            option: function(item, escape) {
                var html = '<div  class="option " >' + item.name + '</div>';
                if (item.id == '0') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Manage the list') + '</div>'
                }
                return html
            }
        },
        onChange(item) {
            var _this = this;
            var field_index = _this.$input[0].attributes.fieldid.value;
            if (edit.item.list[field_index].value != undefined && edit.item.list[field_index].value != '0') {
                old_item[edit.item.list[field_index].field_id] = edit.item.list[field_index].value
            }
            if (item == '0') {
                var obj = {
                    'field_index': field_index
                };
                openModal('selectEdit', obj, 'md');
                edit.item.list[field_index].value = undefined;
                return !1
            }
        },
        maxItems: 1
    };
    edit.renderPage = function(d) {
        if (d && d.data) {
            edit.item = d.data;
            for (x in edit.item.list) {
                if (edit.item.list[x].field_type == '3' && edit.item.list[x].value) {
                    edit.item.list[x].value = new Date(edit.item.list[x].value)
                }
            }
        }
    }
    edit.showAlerts = function(d, formObj) {
        helper.showAlerts(edit, d, formObj)
    }
    edit.saveData = function(formObj) {
        var obj = angular.copy(edit.item);
        obj = helper.clearData(obj, ['addresses_list', 'coords']);
        obj.do = 'customers--customers-updateInstalFields';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            edit.showAlerts(r, formObj)
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'selectEdit':
                var iTemplate = 'selectEdit';
                var ctas = 'vmMod';
                break
        }
        var params = {
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'selectEdit':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = 'selectEditFieldModalCtrl';
                params.params = {
                    'do': 'misc-selectEditField',
                    'field_id': edit.item.list[item.field_index].field_id
                };
                params.callback = function(data) {
                    if (data.data) {
                        edit.item.list[item.field_index].options = data.data.lines;
                        if (old_item[edit.item.list[item.field_index].field_id]) {
                            edit.item.list[item.field_index].value = old_item[edit.item.list[item.field_index].field_id]
                        }
                    } else {
                        edit.item.list[item.field_index].options = data.lines;
                        if (old_item[edit.item.list[item.field_index].field_id]) {
                            edit.item.list[item.field_index].value = old_item[edit.item.list[item.field_index].field_id]
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }
    edit.renderPage(data)
}]).controller('selectEditFieldModalCtrl', ['$scope', '$uibModalInstance', '$timeout', '$confirm', 'helper', 'modalFc', 'data', function($scope, $uibModalInstance, $timeout, $confirm, helper, modalFc, data) {
    var vmMod = this;
    vmMod.obj = data;
    $timeout(function() {
        modalFc.setHeight(angular.element('.modal.in'))
    })
    vmMod.ok = ok;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;
    vmMod.removeLine = removeLine;
    vmMod.openModal = openModal;

    function openModal(type, item, size, table, extra_id) {
        if (type == 'AddEntry') {
            var params = {
                template: 'miscTemplate/' + type,
                ctrl: 'AddEntryFieldModalCtrl',
                size: size
            }
        }
        switch (type) {
            case 'AddEntry':
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-AddEntryField',
                    'id': item,
                    'field_id': vmMod.obj.field_id
                };
                params.callback = function(data) {
                    vmMod.obj.lines = data.data.lines
                }
                break
        }
        modalFc.open(params)
    }

    function ok() {
        var data = angular.copy(vmMod.obj);
        data.do = 'misc--misc-select_editField';
        helper.doRequest('post', 'index.php', data, function(d) {
            if (d.success) {
                $uibModalInstance.close(d)
            } else {
                vmMod.showAlerts(d)
            }
        })
    };

    function cancel() {
        $uibModalInstance.close(vmMod.obj)
    };

    function showAlerts(d) {
        helper.showAlerts(vmMod, d)
    }

    function removeLine($i) {
        if ($i > -1) {
            var data = {
                'id': vmMod.obj.lines[$i].id
            };
            data.do = 'misc--misc-delete_selectField';
            $confirm({
                ok: helper.getLanguage('Yes'),
                cancel: helper.getLanguage('No'),
                text: helper.getLanguage('Are you sure, you wanna delete this entry?')
            }).then(function() {
                helper.doRequest('post', 'index.php', data, function(d) {
                    vmMod.obj.lines.splice($i, 1);
                    modalFc.setHeight(angular.element('.modal.in'))
                })
            }, function() {
                return !1
            })
        }
    }
}]).controller('AddEntryFieldModalCtrl', ['$scope', '$uibModalInstance', '$timeout', 'helper', 'modalFc', 'data', function($scope, $uibModalInstance, $timeout, helper, modalFc, data) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;
    vmMod.ok = ok;
    if (vmMod.obj.id) {
        vmMod.obj.edit_id = !0
    } else {
        vmMod.obj.edit_id = !1
    }

    function ok() {
        var data = angular.copy(vmMod.obj);
        data.do = 'misc--misc-Add_EntryField';
        helper.doRequest('post', 'index.php', data, function(d) {
            if (d.success) {
                $uibModalInstance.close(d)
            } else {
                vmMod.showAlerts(d)
            }
        })
    };

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    };

    function showAlerts(d) {
        helper.showAlerts(vmMod, d)
    }
}]).controller('DealsSettingsCtrl', ['$scope', 'helper', 'data', 'modalFc', function($scope, helper, data, modalFc) {
    console.log('DealsSettingsCtrl list');

    var ls=this;

    for(x in data) {
        ls[x]=data[x]
    }

    ls.saveData=function(type, obj, obj2, is_dd_value) {
        var doNext='customers-customersSettings-customers';
        switch(type) {
            case'dealsRef':
                var data= {'do': doNext+'-'+obj, xget: 'dealsRef'};
                for(x in ls.dealsRef) {
                    data[x]=ls.dealsRef[x]
                }
            break;
        }
        if(data!=undefined) {
            helper.doRequest('post', 'index.php', data, function(r) {
                if(r&&r.data) {
                    for(x in r.data) {
                        ls[x]=r.data[x]
                    }
                    helper.showAlerts(ls, r)
                }
            }
            )
        }
    }
    ls.load=function(h, k) {
        ls.alerts=[];
        var d= {
            'do': 'customers-customersSettings', 'xget': h
        }
        ;
        helper.doRequest('get', 'index.php', d, function(r) {
            if(r.data) {
                for(x in r.data) {
                    ls[x]=r.data[x]
                }
            }
        }
        )
    }

    ls.loadBoards=function(){
        helper.doRequest('get', 'index.php', {'do': 'customers-deals','xget': 'BoardsList','only_boards': !0}, function(r) {
            if(r.data) {
                ls.boards=r.data;
            }
        });
    }

    ls.addBoard=function(id){
        openModal('create_board', id, 'md');
    }

    ls.editBoard = function(id) {
        openModal('create_board', id, 'md')
    }
    ls.deleteBoard = function(id) {
        var obj = {
            'do': 'customers-deals-deals-deleteBoard',
            'board_id': id,
            'xget': 'BoardsList',
            'only_boards': !0
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            helper.showAlerts(ls, r);
            if (r.success) {
                ls.boards=r.data;
            }
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'create_board':
                var cTemplate = 'CreateBoard';
                var ctas = 'vm';
                break
        }
        var params = {
            template: 'cTemplate/' + cTemplate + 'Modal',
            ctrl: cTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'create_board':
                params.params = {
                    'do': 'customers-deals',
                    'xget': 'Board',
                    'board_id': item
                };
                params.callbackExit = function() {
                    helper.doRequest('post', 'index.php', {
                        'do': 'customers-deals',
                        'xget': 'BoardsList',
                        'only_boards': !0
                    }, function(r) {
                        ls.boards=r.data;
                    })
                }
                params.callback = function(d) {
                    if (d && d.data) {
                        ls.boards=d.data;
                        helper.showAlerts(ls, d);
                    }
                }
                break
        }
        modalFc.open(params)
    }

}
]);