app.controller('cashArticlesCtrl', ['$scope', '$compile', '$timeout', 'helper', 'list', 'modalFc', 'data', function($scope, $compile, $timeout, helper, list, modalFc, data) {
    console.log("Cash Articles");
    $scope.list = [];
    $scope.product_fam = [];
    $scope.blocked_fam = [];
    $scope.product_type = [{
        name: helper.getLanguage('All Product Type'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Archived'),
        id: -1
    }];
    $scope.search = {
        search: '',
        'do': 'cashregister-articles',
        view: 0,
        xget: '',
        offset: 1,
        supplier_reference: '',
        blocked_fam: []
    };
    $scope.search_fam = {
        search: '',
        'do': 'cashregister-articles',
        view: 0,
        xget: 'categories',
        offset: 1,
        blocked_fam: []
    };
    $scope.search_tax = {
        search: '',
        'do': 'cashregister-articles',
        view: 0,
        xget: 'taxes',
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.tab = d.data.tab;
            $scope.adv_product = d.data.adv_product;
            $scope.do_next = d.data.do_next;
            if (d.data.tab == 1) {
                $scope.allow_stock = d.data.allow_stock;
                $scope.code_width = d.data.code_width;
                $scope.listStats = [];
                $scope.lid = d.data.lid;
                $scope.type = d.data.pdf_type;
                $scope.product_fam = d.data.families;
                $scope.fam_width = d.data.fam_width
            }
        }
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.filters = function(p) {
        if ($scope.activeView == p) {
            return !1
        }
        $scope.activeView = p;
        var params = angular.copy($scope.search);
        params.view = $scope.activeView;
        params.blocked_fam = angular.copy($scope.blocked_fam);
        $scope.search.view = $scope.activeView;
        $scope.search.offset = 1;
        $scope.ord = '';
        $scope.reverse = !1;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.toggleSearchButtons = function(item, val) {
        if ($scope.tab == 1) {
            $scope.search[item] = val;
            var params = {
                view: $scope.activeView,
                offset: $scope.search.offset
            };
            for (x in $scope.search) {
                params[x] = $scope.search[x]
            }
        } else if ($scope.tab == 2) {
            $scope.search_fam[item] = val;
            var params = {
                view: $scope.activeView,
                offset: $scope.search_fam.offset
            };
            for (x in $scope.search_fam) {
                params[x] = $scope.search_fam[x]
            }
        } else if ($scope.tab == 3) {
            $scope.search_tax[item] = val;
            var params = {
                view: $scope.activeView,
                offset: $scope.search_tax.offset
            };
            for (x in $scope.search_tax) {
                params[x] = $scope.search_tax[x]
            }
        }
        params.blocked_fam = angular.copy($scope.blocked_fam);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.searchThing = function() {
        if ($scope.tab == 1) {
            var data = angular.copy($scope.search)
        } else if ($scope.tab == 2) {
            var data = angular.copy($scope.search_fam)
        } else if ($scope.tab == 3) {
            var data = angular.copy($scope.search_tax)
        }
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.orderBY = function(p) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        var params = {
            view: $scope.activeView,
            offset: $scope.search.offset,
            desc: $scope.reverse,
            order_by: $scope.ord
        };
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Product Type'),
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.filterCfgFam = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Categories'),
        create: !1,
        onChange(value) {
            $scope.toggleSearchButtons('article_category_id', value)
        },
        maxItems: 1
    };
    $scope.listMore = function(index) {
        var new_line = '<tr class="versionInfo"><td colspan="7" class="no_border">' + '<h4 class="title_underline">' + $scope.list[index].name + ' (#' + $scope.list[index].code + ')</h4>' + '<div class="row">' + '<div class="col-xs-4 col-sm-4">' + '<p>' + helper.getLanguage('VAT') + ' <span class="text-muted">' + $scope.list[index].vat_value + ' %</span></p>' + '<p>' + helper.getLanguage('Default Price') + ' <span class="text-muted">' + $scope.list[index].base_price + '</span></p>' + '<p>' + helper.getLanguage('Description') + '<br>' + '<span class="text-muted">' + $scope.list[index].description + '</span></p>' + '</div>' + '<div class="col-xs-4 col-sm-4">' + '<p>' + helper.getLanguage('EAN Code') + ' <span class="text-muted">' + $scope.list[index].ean_code + '</span></p>' + '<p>' + helper.getLanguage('Category') + ' <span class="text-muted">' + $scope.list[index].category_name + '</span></p>' + '</div>' + '</div>'
        '</td></tr>';
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').addClass('hidden');
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').removeClass('hidden');
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.orderTable.products tbody tr.collapseTr').eq(index).next())($scope)
    }
    $scope.listLess = function(index) {
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').removeClass('hidden');
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').addClass('hidden')
    }
    $scope.addToBlockCategory = function(item) {
        var obj = {
            'do': 'cashregister--cashregister-bsyncFamily',
            'id': item.id,
            'value': item.block_sync
        };
        helper.doRequest('post', 'index.php', obj, function() {})
    }

    function bulkBlockArticle(params) {
        var obj = {
            'do': 'cashregister--cashregister-bulksyncArticle',
            'article_ids': params
        };
        helper.doRequest('post', 'index.php', obj, function() {})
    }

    function bulkBlockCategory(params) {
        var obj = {
            'do': 'cashregister--cashregister-bulksyncFamily',
            'ids': params
        };
        helper.doRequest('post', 'index.php', obj, function() {})
    }
    $scope.markAllBlocked = function(bool) {
        var obj = [];
        for (x in $scope.list) {
            if (!$scope.list[x].is_tactill) {
                $scope.list[x].block_sync = bool;
                if ($scope.tab == 1) {
                    obj.push({
                        'article_id': $scope.list[x].article_id,
                        'value': bool
                    })
                } else if ($scope.tab == 2) {
                    obj.push({
                        'id': $scope.list[x].id,
                        'value': bool
                    })
                }
            }
        }
        if ($scope.tab == 1) {
            bulkBlockArticle(obj)
        } else if ($scope.tab == 2) {
            bulkBlockCategory(obj)
        }
    }
    $scope.get_categories = function() {
        var data = angular.copy($scope.search_fam);
        data.do = 'cashregister-articles';
        data.xget = 'categories';
        data.blocked_fam = angular.copy($scope.blocked_fam);
        $scope.search_fam.blocked_fam = angular.copy($scope.blocked_fam);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.renderList(r)
        })
    }
    $scope.get_articles = function() {
        var data = angular.copy($scope.search);
        data.do = 'cashregister-articles';
        data.xget = 'articles';
        data.blocked_fam = angular.copy($scope.blocked_fam);
        $scope.search.blocked_fam = angular.copy($scope.blocked_fam);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.renderList(r)
        })
    }
    $scope.get_taxes = function() {
        var data = angular.copy($scope.search_tax);
        data.do = 'cashregister-articles';
        data.xget = 'taxes';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.renderList(r)
        })
    }
    $scope.doSync = function(type) {
        var new_data = {
            'list': []
        };
        if ($scope.tab < 3) {
            $xget = $scope.tab == 1 ? 'bulkArticles' : 'bulkCategories';
            helper.doRequest('get', 'index.php', {
                'do': 'cashregister-articles',
                'xget': $xget
            }, function(o) {
                for (x in o.data.list) {
                    new_data.list.push(o.data.list[x])
                }
                new_data.type = type;
                new_data.do_next = $scope.do_next;
                openModal('progress_cash', new_data, 'md')
            })
        } else {
            var data = angular.copy($scope.list);
            for (x in data) {
                if (!data[x].block_sync) {
                    new_data.list.push(data[x])
                }
            }
            new_data.type = type;
            new_data.do_next = $scope.do_next;
            openModal('progress_cash', new_data, 'md')
        }
    }
    $scope.blockSyncArticle = function(item) {
        var obj = {
            'do': 'cashregister--cashregister-bsyncArticle',
            'article_id': item.article_id,
            'value': item.block_sync
        };
        helper.doRequest('post', 'index.php', obj, function() {})
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'progress_cash':
                var iTemplate = 'ProgressCash';
                var ctas = 'prog';
                break
        }
        var params = {
            template: 'cashTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'progress_cash':
                params.item = item;
                params.callback = function() {
                    switch ($scope.tab) {
                        case 1:
                            var type = 'products';
                            break;
                        case 2:
                            var type = 'categories';
                            break;
                        case 3:
                            var type = 'taxes';
                            break
                    }
                    $timeout(function() {
                        angular.element('.nav-tabs a[href="#' + type + '"]').tab('show').triggerHandler('click');
                        angular.element('.nav-tabs a[href="#' + type + '"]').parent().triggerHandler('click')
                    })
                }
                break
        }
        modalFc.open(params)
    }
    $scope.renderList(data)
}]).controller('ProgressCashModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var prog = this;
    prog.obj = data;
    prog.value = 0;
    prog.showAlerts = function(d) {
        helper.showAlerts(prog, d)
    }
    prog.cancel = function() {
        $uibModalInstance.close()
    }

    function syncIt(array, type, i, do_next) {
        if (array.length) {
            var prm = angular.copy(array[0]);
            prm.do = do_next;
            helper.doRequest('post', 'index.php', prm, function(o) {
                i++;
                var percent = i * 100 / prog.obj.list.length;
                prog.value = percent.toFixed(2);
                if (o.error) {
                    prog.showAlerts(o)
                }
                array.shift();
                syncIt(array, type, i, do_next)
            })
        } else {
            prog.value = 100
        }
    }
    var new_data = angular.copy(prog.obj.list);
    syncIt(new_data, prog.obj.type, 0, prog.obj.do_next)
}]).controller('cashOrdersCtrl', ['$scope', '$compile', '$timeout', 'helper', 'list', 'modalFc', 'data', function($scope, $compile, $timeout, helper, list, modalFc, data) {
    console.log("Cash Orders");
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'cashregister-orders',
        view: 0,
        xget: '',
        offset: 1,
        start_date: undefined,
        end_date: undefined,
        operation: '0',
        payment: '0'
    };
    $scope.search_pay = {
        search: '',
        'do': 'cashregister-orders',
        view: 0,
        xget: 'pay_method',
        offset: 1
    };
    $scope.search_cash = {
        search: '',
        'do': 'cashregister-orders',
        view: 0,
        xget: 'cashbooks',
        offset: 1
    };
    $scope.top = {
        'checkedAll': !1
    };
    $scope.operations = [{
        'id': '0',
        'name': helper.getLanguage('All Operations')
    }, {
        'id': '1',
        'name': helper.getLanguage('Sale')
    }, {
        'id': '2',
        'name': helper.getLanguage('Invoiced')
    }, {
        'id': '3',
        'name': helper.getLanguage('Uninvoiced')
    }];
    $scope.payments = [{
        'id': '0',
        'name': helper.getLanguage('All Payments')
    }, {
        'id': '1',
        'name': helper.getLanguage('Cash')
    }, {
        'id': '2',
        'name': helper.getLanguage('Check')
    }, {
        'id': '3',
        'name': helper.getLanguage('Credit Card')
    }, {
        'id': '4',
        'name': helper.getLanguage('Credit Note')
    }];
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1,
        end_date: !1,
    }
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.top.checkedAll = !1;
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.tab = d.data.tab;
            $scope.data_incoming = d.data.data_incoming;
            $scope.do_next = d.data.do_next
        }
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.orderBY = function(p) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        var params = {
            view: $scope.activeView,
            offset: $scope.search.offset,
            desc: $scope.reverse,
            order_by: $scope.ord
        };
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.get_tickets = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php?do=cashregister-orders', function(r) {
            $scope.renderList(r);
            $scope.sync_disabled = !0;
            helper.doRequest('get', 'index.php?do=cashregister-orders&xget=incoming_tickets', function(o) {
                $scope.data_incoming = o.data.data_incoming;
                if ($scope.data_incoming.length) {
                    $scope.sync_disabled = !1
                }
            })
        })
    }
    $scope.get_pay_method = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php?do=cashregister-orders&xget=pay_method', $scope.renderList)
    }
    $scope.get_cashbooks = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php?do=cashregister-orders&xget=cashbooks', $scope.renderList)
    }
    $scope.doSync = function(type) {
        var obj = angular.copy($scope.data_incoming);
        var new_data = {
            'list': []
        };
        for (x in obj) {
            new_data.list.push(obj[x])
        }
        new_data.type = type;
        new_data.do_next = $scope.do_next;
        openModal("sync_incoming", new_data, "md")
    }
    $scope.searchThing = function() {
        if ($scope.tab == 1) {
            var data = angular.copy($scope.search)
        } else if ($scope.tab == 2) {
            var data = angular.copy($scope.search_pay)
        } else if ($scope.tab == 3) {
            var data = angular.copy($scope.search_cash)
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', data, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.toggleSearchButtons = function(item, val) {
        $scope.search[item] = val;
        var params = {
            offset: $scope.search.offset
        };
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.listMore = function(index) {
        if ($scope.list[index].articles.length) {
            var new_line = '<tr class="versionInfo"><td colspan="9" class="no_border">' + '<table class="table dispatchTable">' + '<thead><th>' + '<div class="row">' + '<div class="col-xs-2 col-sm-2">' + helper.getLanguage('Code') + '</div>' + '<div class="col-xs-4 col-sm-4">' + helper.getLanguage('Internal Name') + '</div>' + '<div class="col-xs-2 col-sm-2">' + helper.getLanguage('Quantity') + '</div>' + '<div class="col-xs-2 col-sm-2">' + helper.getLanguage('Amount') + '</div>' + '<div class="col-xs-2 col-sm-2">' + helper.getLanguage('Amount + VAT') + '</div>' + '</div>' + '</th></thead>' + '<tbody>' + '<tr ng-repeat="art in list[' + index + '].articles">' + '<td>' + '<div class="row">' + '<div class="col-xs-2 col-sm-2 text-muted">{{art.code}}</div>' + '<div class="col-xs-4 col-sm-4 text-muted"><span ng-if="!art.article_id"><i class="fa fa-exclamation text-danger" aria-hidden="true" uib-tooltip="' + helper.getLanguage('Require article sync') + '" tooltip-placement="left"></i></span><span >  {{art.name}}</span></div>' + '<div class="col-xs-2 col-sm-2 text-muted">{{art.quantity}}</div>' + '<div class="col-xs-2 col-sm-2 text-muted"><span ng-bind-html="art.amount"></span></div>' + '<div class="col-xs-2 col-sm-2 text-muted"><span ng-bind-html="art.amount_vat"></span></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>'
        } else {
            var new_line = '<tr class="versionInfo"><td colspan="9" class="no_border">' + '<div uib-alert type="warning" class="alert-warning margin-top-none margin-bottom-none">' + helper.getLanguage('No data to display') + '</div>' + '</td></tr>'
        }
        angular.element('.table_orders.tickets tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').addClass('hidden');
        angular.element('.table_orders.tickets tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').removeClass('hidden');
        angular.element('.table_orders.tickets tbody tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.table_orders.tickets tbody tr.collapseTr').eq(index).next())($scope)
    }
    $scope.listLess = function(index) {
        angular.element('.table_orders.tickets tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.table_orders.tickets tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').removeClass('hidden');
        angular.element('.table_orders.tickets tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').addClass('hidden')
    }
    $scope.checkAll = function() {
        for (x in $scope.list) {
            if ($scope.list[x].invoice_ok) {
                $scope.list[x].checked = $scope.top.checkedAll
            }
        }
    }
    $scope.createInvoices = function(type, app) {
        var obj_global = {};
        obj_global[app] = [];
        for (x in $scope.list) {
            if ($scope.list[x].invoice_ok && $scope.list[x].checked) {
                obj_global[app].push($scope.list[x].id)
            }
        }
        if (obj_global[app].length == 0) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Please select one or more items")
                },
                "notice": !1,
                "success": !1
            };
            $scope.showAlerts(obj)
        } else {
            var data = {};
            data.post_list = app;
            data[app] = obj_global[app];
            data.typeOfInvoice = type;
            openModal("progress_create_invoice", data, "md")
        }
    }
    $scope.createInvoice = function(type, app, item) {
        var obj_global = {};
        obj_global[app] = [];
        obj_global[app].push(item.id);
        var data = {};
        data.post_list = app;
        data[app] = obj_global[app];
        data.typeOfInvoice = type;
        openModal("progress_create_invoice", data, "md")
    }
    $scope.operationCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Operation'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value != undefined) {
                $scope.searchThing()
            }
        },
        maxItems: 1
    };
    $scope.paymentCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Payment Type'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value != undefined) {
                $scope.searchThing()
            }
        },
        maxItems: 1
    };

    function openModal(type, item, size) {
        switch (type) {
            case 'sync_incoming':
                var iTemplate = 'SyncIncomming';
                var ctas = 'sync';
                break;
            case 'progress_create_invoice':
                var iTemplate = 'ProgressCreateInvoice';
                var ctas = 'prog';
                break
        }
        var params = {
            template: 'cashTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'sync_incoming':
                params.item = item;
                params.callback = function() {
                    switch ($scope.tab) {
                        case 1:
                            var type = 'tickets';
                            break;
                        case 2:
                            var type = 'pay_method';
                            break;
                        case 3:
                            var type = 'cashbooks';
                            break
                    }
                    $timeout(function() {
                        angular.element('.nav-tabs a[href="#' + type + '"]').tab('show').triggerHandler('click');
                        angular.element('.nav-tabs a[href="#' + type + '"]').parent().triggerHandler('click')
                    })
                }
                break;
            case 'progress_create_invoice':
                params.template = 'iTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.callback = function(app) {
                    var obj = angular.copy($scope.search);
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php', obj, function(r) {
                        $scope.renderList(r)
                    })
                }
                break
        }
        modalFc.open(params)
    }
    $scope.renderList(data);
    $scope.sync_disabled = !0;
    angular.element('.loading_wrap').removeClass('hidden');
    helper.doRequest('get', 'index.php?do=cashregister-orders&xget=incoming_tickets', function(o) {
        $scope.data_incoming = o.data.data_incoming;
        if ($scope.data_incoming.length) {
            $scope.sync_disabled = !1
        }
    })
}]).controller('cashAccountCtrl', ['$scope', '$timeout', 'helper', 'list', 'modalFc', 'data', function($scope, $timeout, helper, list, modalFc, data) {
    console.log("Cash Account");
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'cashregister-account',
        view: 0,
        xget: '',
        offset: 1
    };
    $scope.search_offer = {
        search: '',
        'do': 'cashregister-account',
        view: 0,
        xget: 'offers',
        offset: 1
    };
    $scope.search_shop = {
        search: '',
        'do': 'cashregister-account',
        view: 0,
        xget: 'shops',
        offset: 1
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.tab = d.data.tab;
            $scope.data_incoming = d.data.data_incoming;
            $scope.do_next = d.data.do_next
        }
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.get_companies = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php?do=cashregister-account', $scope.renderList)
    }
    $scope.get_offers = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php?do=cashregister-account&xget=offers', $scope.renderList)
    }
    $scope.get_shops = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php?do=cashregister-account&xget=shops', $scope.renderList)
    }
    $scope.doSync = function(type) {
        var obj = angular.copy($scope.data_incoming);
        var new_data = {
            'list': []
        };
        for (x in obj) {
            new_data.list.push(obj[x])
        }
        new_data.type = type;
        new_data.do_next = $scope.do_next;
        openModal("sync_incoming", new_data, "md")
    }
    $scope.searchThing = function() {
        if ($scope.tab == 1) {
            var data = angular.copy($scope.search)
        } else if ($scope.tab == 2) {
            var data = angular.copy($scope.search_offer)
        } else if ($scope.tab == 3) {
            var data = angular.copy($scope.search_shop)
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', data, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.orderBY = function(p) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        var params = {
            view: $scope.activeView,
            offset: $scope.search.offset,
            desc: $scope.reverse,
            order_by: $scope.ord
        };
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r)
        })
    };

    function openModal(type, item, size) {
        switch (type) {
            case 'sync_incoming':
                var iTemplate = 'SyncIncomming';
                var ctas = 'sync';
                break
        }
        var params = {
            template: 'cashTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'sync_incoming':
                params.item = item;
                params.callback = function() {
                    switch ($scope.tab) {
                        case 1:
                            var type = 'companies';
                            break;
                        case 2:
                            var type = 'offers';
                            break;
                        case 3:
                            var type = 'shops';
                            break
                    }
                    $timeout(function() {
                        angular.element('.nav-tabs a[href="#' + type + '"]').tab('show').triggerHandler('click');
                        angular.element('.nav-tabs a[href="#' + type + '"]').parent().triggerHandler('click')
                    })
                }
                break
        }
        modalFc.open(params)
    }
    $scope.renderList(data)
}]).controller('cashCustomerCtrl', ['$scope', '$compile', 'helper', 'list', 'modalFc', 'data', function($scope, $compile, helper, list, modalFc, data) {
    console.log("Cash Customer");
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'cashregister-customer',
        view: 0,
        xget: '',
        offset: 1
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.tab = d.data.tab;
            $scope.data_incoming = d.data.data_incoming;
            $scope.do_next = d.data.do_next;
            $scope.country_dd = d.data.country_dd;
            $scope.language_dd = d.data.language_dd;
            $scope.gender_dd = d.data.gender_dd;
            $scope.title_dd = d.data.title_dd;
            $scope.customer_dd = d.data.customer_dd
        }
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.doSync = function(type) {
        var obj = angular.copy($scope.data_incoming);
        var new_data = {
            'list': []
        };
        for (x in obj) {
            new_data.list.push(obj[x])
        }
        new_data.type = type;
        new_data.do_next = $scope.do_next;
        openModal("sync_incoming", new_data, "md")
    }
    $scope.searchThing = function() {
        var data = angular.copy($scope.search);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', data, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.orderBY = function(p) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        var params = {
            view: $scope.activeView,
            offset: $scope.search.offset,
            desc: $scope.reverse,
            order_by: $scope.ord
        };
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.listMore = function(index) {
        var new_line = '<tr class="versionInfo"><td colspan="7" class="no_border">' + '<table class="table dispatchTable">' + '<thead><th>' + '<div class="row">' + '<div class="col-xs-2 col-sm-2">' + helper.getLanguage('Email') + '</div>' + '<div class="col-xs-2 col-sm-2">' + helper.getLanguage('Phone') + '</div>' + '<div class="col-xs-6 col-sm-6">' + helper.getLanguage('Address') + '</div>' + '<div class="col-xs-2 col-sm-2">' + helper.getLanguage('Zip') + '</div>' + '</div>' + '</th></thead>' + '<tbody>' + '<tr>' + '<td>' + '<div class="row">' + '<div class="col-xs-2 col-sm-2 text-muted">{{list[' + index + '].email}}</div>' + '<div class="col-xs-2 col-sm-2 text-muted">{{list[' + index + '].phone}}</div>' + '<div class="col-xs-6 col-sm-6 text-muted">{{list[' + index + '].address}}</div>' + '<div class="col-xs-2 col-sm-2 text-muted">{{list[' + index + '].zip}}</div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>';
        angular.element('.table_orders.customers tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').addClass('hidden');
        angular.element('.table_orders.customers tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').removeClass('hidden');
        angular.element('.table_orders.customers tbody tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.table_orders.customers tbody tr.collapseTr').eq(index).next())($scope)
    }
    $scope.listLess = function(index) {
        angular.element('.table_orders.customers tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.table_orders.customers tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').removeClass('hidden');
        angular.element('.table_orders.customers tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').addClass('hidden')
    }
    $scope.setExportContacts = function() {
        openModal('out_sync', '', 'lg')
    }
    $scope.createAccount = function(item) {
        var obj = {
            'do': 'cashregister-customer-cashregister-tryAddC',
            'add_customer': !0,
            'country_dd': $scope.country_dd,
            'country_id': item.country_id,
            'address': item.address,
            'zip': item.zip,
            'city': item.city,
            'email': item.email,
            'firstname': item.first_name,
            'lastname': item.last_name,
            'customer_id': item.customer_id
        };
        openModal('add_customer', obj, 'md')
    }
    $scope.createContact = function(item) {
        var obj = {
            'do': 'cashregister-customer-cashregister-tryAddC',
            'add_contact': !0,
            'is_tactill': !0,
            'contact_only': !0,
            'country_dd': $scope.country_dd,
            'country_id': item.country_id,
            'customer_id': item.customer_id,
            'language_dd': $scope.language_dd,
            'gender_dd': $scope.gender_dd,
            'title_dd': $scope.title_dd,
            'customer_dd': $scope.customer_dd,
            'firstname': item.first_name,
            'lastname': item.last_name,
            'email': item.email
        };
        openModal('add_customer', obj, 'md')
    }
    $scope.syncContact = function(item) {
        helper.doRequest('post', 'index.php', {
            'do': 'cashregister-customer',
            'xget': 'contacts',
            'customer_id': item.customer_id
        }, function(r) {
            var new_data = {
                'list': []
            };
            for (x in r.data.list) {
                new_data.list.push(r.data.list[x])
            }
            new_data.do_next = r.data.do_next;
            new_data.type = 1;
            openModal("sync_incoming", new_data, "md")
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'sync_incoming':
                var iTemplate = 'SyncIncomming';
                var ctas = 'sync';
                break;
            case 'out_sync':
                var iTemplate = 'OutSyncContacts';
                var ctas = 'sync';
                break
        }
        var params = {
            template: 'cashTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'sync_incoming':
                params.item = item;
                params.callback = function() {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php?do=cashregister-customer', $scope.renderList)
                }
                break;
            case 'out_sync':
                params.params = {
                    'do': 'cashregister-customer',
                    'xget': 'contacts'
                };
                params.callback = function() {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php?do=cashregister-customer', $scope.renderList)
                }
                break;
            case 'add_customer':
                params.template = 'miscTemplate/AddCustomerModal';
                params.ctrl = 'AddCustomerModalCtrl';
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.renderList(d);
                        $scope.showAlerts(d)
                    }
                }
                break
        }
        modalFc.open(params)
    }
    $scope.renderList(data)
}]).controller('SyncIncommingModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var sync = this;
    sync.obj = data;
    sync.value = 0;
    sync.showAlerts = function(d) {
        helper.showAlerts(sync, d)
    }
    sync.cancel = function() {
        $uibModalInstance.close()
    }

    function syncIt(array, type, i, do_next) {
        if (array.length) {
            var prm = angular.copy(array[0]);
            prm.do = do_next;
            helper.doRequest('post', 'index.php', prm, function(o) {
                i++;
                var percent = i * 100 / sync.obj.list.length;
                sync.value = parseInt(percent.toFixed(0));
                if (o.error) {
                    sync.showAlerts(o)
                }
                array.shift();
                syncIt(array, type, i, do_next)
            })
        } else {
            sync.value = 100
        }
    }
    var new_data = angular.copy(sync.obj.list);
    syncIt(new_data, sync.obj.type, 0, sync.obj.do_next)
}]).controller('OutSyncContactsModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, helper, $uibModalInstance, data, modalFc) {
    var sync = this;
    sync.obj = data;
    sync.search = {
        search: '',
        'do': 'cashregister-customer',
        'xget': 'contacts',
        view: 0,
        offset: 1
    };
    sync.renderList = function(d) {
        if (d.data) {
            sync.obj = d.data
        }
    };
    sync.toggleSearchButtons = function(item, val) {
        sync.search[item] = val;
        var params = {
            offset: sync.search.offset
        };
        for (x in sync.search) {
            params[x] = sync.search[x]
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            sync.renderList(r)
        })
    };
    sync.searchThing = function() {
        var params = {
            offset: sync.search.offset
        };
        for (x in sync.search) {
            params[x] = sync.search[x]
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            sync.renderList(r)
        })
    };
    sync.selectAll = function(type) {
        var bool = !1;
        if (type == 1) {
            bool = !0
        }
        for (x in sync.obj.list) {
            sync.obj.list[x].checked = sync.obj.list[x].no_address ? !1 : bool
        }
    }
    sync.showAlerts = function(d) {
        helper.showAlerts(sync, d)
    }
    sync.cancel = function() {
        $uibModalInstance.close()
    }
    sync.ok = function() {
        var new_data = {
            'list': []
        };
        for (x in sync.obj.list) {
            if (sync.obj.list[x].checked) {
                new_data.list.push(sync.obj.list[x])
            }
        }
        new_data.do_next = sync.obj.do_next;
        new_data.type = 1;
        openModal("sync_incoming", new_data, "md")
    }
    sync.checkAddress = function(item) {
        if (item.checked && item.no_address) {
            item.checked = !1;
            var obj = {
                'error': {
                    'error': helper.getLanguage('Billing or main address required for contact')
                },
                'success': !1,
                'notice': !1
            };
            sync.showAlerts(obj)
        }
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'sync_incoming':
                var iTemplate = 'SyncIncomming';
                var ctas = 'sync';
                break
        }
        var params = {
            template: 'cashTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size
        };
        switch (type) {
            case 'sync_incoming':
                params.backdrop = 'static';
                params.item = item;
                params.callback = function() {
                    sync.searchThing()
                }
                break
        }
        modalFc.open(params)
    }
}])