angular.module('akti').controller('SettingsCtrl', ['$scope', '$stateParams', '$timeout', '$rootScope', '$confirm', '$state', 'helper', '$uibModal', 'settingsFc', 'list', 'data', 'orderByFilter', 'selectizeFc', 'Upload', 'modalFc', function($scope, $stateParams, $timeout, $rootScope, $confirm,$state, helper, $uibModal, settingsFc, list, data, orderBy, selectizeFc, Upload, modalFc) {
    var vm = this;
    vm.app = 'settings';
    vm.ctrlpag = 'SettingsCtrl';
    vm.tmpl = 'sTemplate';
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name']
    });
    vm.timezoneCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        create: !1,
        maxItems: 1,
        render: {
            option: function(item, escape) {
                var html = '<div  class="option " >' + item.name + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        }
    };
    vm.generalCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        maxItems: 1
    };
    vm.logos = {};
    vm.account_address = {};
    vm.regional = {};
    vm.financial = {};
    vm.vat = {};
    vm.language = {};
    vm.users = {
        views: [{
            name: helper.getLanguage('All Statuses'),
            id: 0,
            active: 'active'
        }, {
            name: helper.getLanguage('Active'),
            id: 1,
            active: ''
        }, {
            name: helper.getLanguage('Inactive'),
            id: 2,
            active: ''
        }],
        activeView: 0,
        search: {
            search: '',
            'do': 'settings-settings',
            view: vm.activeView,
            xget: 'Users'
        },
        list: [],
        max_rows: 0
    };
    vm.groups = {};
    vm.integrated = [];
    vm.integrate = {};
    vm.user_access = {};
    vm.multiple = {};
    vm.appsType = undefined;
    vm.appsActive = !1;
    vm.propertyName = '';
    vm.reverse = !0;
    vm.ord = '';
    vm.save_button_text_general = helper.getLanguage('Save');
    vm.save_button_text_ar = helper.getLanguage('Save');
    vm.save_button_text_regional = helper.getLanguage('Save');
    vm.save_button_ar_disabled = false;

    vm.friends = orderBy(vm.users.list, vm.propertyName, vm.reverse);
    vm.renderList = function(d) {
        if (d.data) {
            vm.users = d.data.users;
            helper.showAlerts(vm, d)
        }
    };

    if ($stateParams.cfconn == '1') {
        $timeout(function() {
            $('#apps .cfacts_class').triggerHandler('click');
        }, 1000)
    }

    if ($stateParams.eoconn == '1') {
        $timeout(function() {
            $('#apps .exact_online_class').triggerHandler('click');
        }, 1000)
    }

    if ($stateParams.dbconn == '1') {
        $timeout(function() {
            $('#apps .dropbox_class').triggerHandler('click');
        }, 1000)
    }

    function showAlerts(d) {
        helper.showAlerts(view, d)
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.toggleSearchButtons = function(item, val) {
        list.tSearch(vm.users, item, val, vm.renderList)
    };
    vm.sortBy = function(propertyName) {
        vm.reverse = (propertyName !== null && vm.propertyName === propertyName) ? !vm.reverse : !1;
        vm.propertyName = propertyName;
        vm.users.list = orderBy(vm.users.list, vm.propertyName, vm.reverse)
    };
    vm.filterAcc = function(item) {
        if (item.app_type == 'accountancy') {
            return true;
        }
        return false;
    };
    vm.filterPay = function(item) {
        if (item.app_type == 'payment') {
            return true;
        }
        return false;
    };
    vm.filterOther = function(item) {
        if (item.app_type != 'accountancy' && item.app_type != 'payment') {
            return true;
        }
        return false;
    };
    vm.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Statuses'),
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            vm.filters(value)
        },
        maxItems: 1
    };
    vm.filters = function(p) {
        list.filter(vm.users, p, vm.renderList)
    }
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: (size ? size : 'md'),
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            if (data.data.vat) {
                vm.vat = data.data.vat
            }
            if (data.data.vat_r) {
                vm.vat_r = data.data.vat_r
            }
            if (data.data.language) {
                vm.language = data.data.language
            }
        }, function() {})
    };
    vm.modal_group = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.groups = data.data.groups
        }, function() {})
    };
    vm.modal_app = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            backdrop: 'static',
            resolve: {
                obj: function() {
                    var get_data = {
                        'do': 'settings-settings',
                        'xget': r
                    };
                    return helper.doRequest('get', 'index.php', get_data).then(function(r) {
                        return r.data
                    })
                }
            }
        });
        modalInstance.result.then(function(data) {
            // vm.showAlerts(vm, data);
            vm.integrated = data.data.integrated;
            vm.integrate = data.data.integrate
        }, function() {})
    };
    vm.load = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'settings-settings',
            'xget': h
        };

        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
            if (k == 'logos') {
                vm.default_logo = r.data.default_logo
            }

        })
    }
    vm.uploadFiles = function(files) {
        if (files && files.length) {
            Upload.upload({
                url: 'index.php',
                data: {
                    file: files,
                    'do': 'settings-settings-settings-upload1',
                    'xget': 'company'
                }
            }).then(function(r) {
                vm.account_address.logos = r.data.data.account_address.logos
            }, function() {})
        }
    }
    vm.upload = function(vm, e) {
        angular.element(e.target).parents('.upload-box1').find('.newUploadFile2').click()
    }
    angular.element('.newUploadFile2').on('click', function(e) {
        e.stopPropagation()
    }).on('change', function(e) {
        e.preventDefault();
        var _this = $(this),
            p = _this.parents('.upload-box1'),
            x = p.find('.newUploadFile2');
        if (x[0].files[0].length == 0) {
            return !1
        }
        if (x[0].files[0].size > 256000) {
            var msg = "<b>" + helper.getLanguage("The logo does not meet the requierements.") + "</b>";
            var title = helper.getLanguage("Upload logo");
            var cont = {
                "e_message": msg,
                "e_title": title
            };
            openModal("alert_info", cont, "md")
            return !1
        }
        p.find('.img-thumbnail').attr('src', '');
        var tmppath = URL.createObjectURL(x[0].files[0]);
        p.find('.img-thumbnail').attr('src', tmppath);
        p.find('.btn').addClass('hidden');
        p.find('.nameOfTheFile').text(x[0].files[0].name);
        var formData = new FormData();
        formData.append("Filedata", x[0].files[0]);
        formData.append("do", 'settings-settings-settings-upload');
        formData.append("xget", 'company');
        p.find('.upload-progress-bar').css({
            width: '0%'
        });
        p.find('.upload-file').removeClass('hidden');
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "index.php", !0);
        oReq.upload.addEventListener("progress", function(e) {
            var pc = parseInt(e.loaded / e.total * 100);
            p.find('.upload-progress-bar').css({
                width: pc + '%'
            })
        });
        oReq.onload = function(oEvent) {
            var res = JSON.parse(oEvent.target.response);
            p.find('.upload-file').addClass('hidden');
            p.find('.btn').removeClass('hidden');
            if (oReq.status == 200) {
                if (res.success) {
                    $timeout(function() {
                        vm.account_address = res.data.account_address
                    })
                } else {
                    alert(res.error)
                }
            } else {
                alert("Error " + oReq.status + " occurred when trying to upload your file.")
            }
        };
        oReq.send(formData)
    });
    vm.InvoiceAddress = function() {
        var data = angular.copy(vm.account_address);
        if (data.INVOICE_ADDRESS == !1) {
            data.ACCOUNT_BILLING_ADDRESS = angular.copy(vm.account_address.ACCOUNT_DELIVERY_ADDRESS);
            data.ACCOUNT_BILLING_ZIP = angular.copy(vm.account_address.ACCOUNT_DELIVERY_ZIP);
            data.ACCOUNT_BILLING_COUNTRY_ID = angular.copy(vm.account_address.ACCOUNT_DELIVERY_COUNTRY_ID);
            data.ACCOUNT_BILLING_CITY = angular.copy(vm.account_address.ACCOUNT_DELIVERY_CITY)
        }
        data.do = 'settings-settings-settings-address_invoice';
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.account_address = r.data.account_address;
            vm.showAlerts(vm, r)
        })
    }
    vm.CompanyInfo = function(vm, r, formObj) {
        vm.save_button_text_general = helper.getLanguage('Saved');
        $rootScope.haveGeneralSettings = !1;
        formObj.$invalid = !0;
        var data = angular.copy(vm.account_address);
        data.ACCOUNT_SEND_EMAIL=vm.mail_settings.ACCOUNT_SEND_EMAIL;
        data.MAIL_SETTINGS_PREFERRED_OPTION=vm.mail_settings.MAIL_SETTINGS_PREFERRED_OPTION;
        helper.doRequest('post', 'index.php', data, function(r) {
            //vm.account_address = r.data.account_address;
            //vm.mail_settings=r.data.mail_settings;
            //vm.showAlerts(vm, r, formObj)
        })
    }
    vm.checkFormGeneral = function(formObj) {   
        if (formObj.$invalid ) {         
            return !0
        } else {
            vm.save_button_text_general = helper.getLanguage('Save');  
             return !1      
        }
    }
    vm.RegionalSet = function(vm, r, formObj) {
        $rootScope.haveGeneralSettings = !1;
        formObj.$invalid = !0;
        vm.save_button_text_regional=helper.getLanguage('Saved');
        var data = angular.copy(vm.regional);
        helper.doRequest('post', 'index.php', r, function(r) {
            //vm.regional = r.data.regional;
            //vm.showAlerts(vm, r, formObj)
        })
    }
    vm.checkFormRegional = function(formObj) {   
        if (formObj.$invalid ) {         
            return !0
        } else {
            vm.save_button_text_regional = helper.getLanguage('Save');  
             return !1      
        }
    }
    vm.delete_custom_lang=function(item){
        var get = 'Language';
        var data = {
            'do': 'settings-settings-settings-delete_lang_custom',
            'xget': get,
            'lang_id': item.ID_CUSTOM
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                vm.language = r.data.language
            }
        })
    }
    vm.deletevat = function(item) {
        var get = 'Vat';
        var data = {
            'do': 'settings-settings-settings-deletevat',
            'xget': get,
            'ID': item.ID,
            'VALUE': item.VALUE
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                vm.vat = r.data.vat
            }
            //vm.showAlerts(vm, r)
        })
    }
    vm.deletevatnew = function(item) {
        var get = 'Vat_regime';
        var data = {
            'do': 'settings-settings-settings-deletevatnew',
            'xget': get,
            'ID': item.ID,
            'VALUE': item.VALUE
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                vm.vat_r = r.data.vat_r
            }
            //vm.showAlerts(vm, r)
        })
    }
    vm.legalMentions = function(item) {
        openModal('vat_lang', item, 'md')
    }
    vm.copydate = function(vm, r, ACCOUNT_DELIVERY_COUNTRY_ID, ACCOUNT_DELIVERY_ZIP, ACCOUNT_DELIVERY_ADDRESS, ACCOUNT_DELIVERY_CITY) {
        vm.account_address.ACCOUNT_BILLING_ADDRESS = angular.copy(ACCOUNT_DELIVERY_ADDRESS);
        vm.account_address.ACCOUNT_BILLING_ZIP = angular.copy(ACCOUNT_DELIVERY_ZIP);
        vm.account_address.ACCOUNT_BILLING_COUNTRY_ID = angular.copy(ACCOUNT_DELIVERY_COUNTRY_ID);
        vm.account_address.ACCOUNT_BILLING_CITY = angular.copy(ACCOUNT_DELIVERY_CITY)
    }
    vm.defaultvat = function(item) {
        var get = 'Vat';
        var data = {
            'do': 'settings-settings-settings-defaultvat',
            'xget': get,
            'VALUE': item.VALUE
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.vat = r.data.vat;
            //vm.showAlerts(vm, r)
        })
    }
    vm.defaultvatnew = function(item) {
        var get = 'Vat_regime';
        var data = {
            'do': 'settings-settings-settings-defaultvatnew',
            'xget': get,
            'VALUE': item.VALUE,
            'id': item.ID
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.vat_r = r.data.vat_r;
            //vm.showAlerts(vm, r)
        })
    }
    vm.changeNoVat = function(item) {
        var get = 'Vat_regime';
        var data = {
            'do': 'settings-settings-settings-novat',
            'xget': get,
            'value': item.NO_VAT,
            'id': item.ID
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.vat_r = r.data.vat_r;
            //vm.showAlerts(vm, r)
        })
    }
    vm.activate_lang = function(vm) {
        var get = 'Language';
        var data = {
            'do': 'settings-settings-settings-activate_lang',
            'xget': get,
            'VALUE': vm.language.PUBLISH_LANG_ID
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.language = r.data.language
            //vm.showAlerts(vm, r)
        })
    }
    vm.dragControlListeners = {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            var get = 'Language';
            var data = {
                'do': 'settings-settings-settings-change_language_name',
                'xget': get,
                'sort': vm.language.public_lang.public_lang,
                'sort_custom': vm.language.public_lang.extra_lang,
                'sort_custom_Extra': vm.language.public_lang.custom_lang
            };
            helper.doRequest('post', 'index.php', data, function(r) {
                vm.language = r.data.language
                vm.showAlerts(vm, r)
            })
        }
    }
    vm.deactivate_lang = function(vm, r, ID, check) {
        var get = 'Language';
        if (check == !1) {
            var data = {
                'do': 'settings-settings-settings-deactivate_lang',
                'xget': get,
                'ID': ID,
                'check': check
            }
        } else {
            var data = {
                'do': 'settings-settings-settings-activate_language',
                'xget': get,
                'ID': ID,
                'check': check
            }
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.language = r.data.language
            //vm.showAlerts(vm, r)
        })
    }
    vm.deactivate_extra_lang = function(vm, r, ID, check) {
        var get = 'Language';
        if (check == !1) {
            var data = {
                'do': 'settings-settings-settings-deactivate_custom_lang',
                'xget': get,
                'ID': ID,
                'check': check
            }
        } else {
            var data = {
                'do': 'settings-settings-settings-activate_lang_custom',
                'xget': get,
                'ID': ID,
                'check': check
            }
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.language = r.data.language
            //vm.showAlerts(vm, r)
        })
    }
    vm.change_main_user = function(vm, r, id) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-change_main_user',
            'xget': get,
            'user_id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            //vm.showAlerts(vm, r)
        })
    }
    vm.deactivate_user = function(vm, r, id) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-deactivate',
            'xget': get,
            'user_id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            //vm.showAlerts(vm, r)
        })
    }
    vm.activate = function(vm, r, id, check) {
        var get = 'Users';
        if (check == !0) {
            var data = {
                'do': 'settings-settings-settings-activate2',
                'xget': get,
                'user_id': id
            }
        } else {
            var data = {
                'do': 'settings-settings-settings-deactivate2',
                'xget': get,
                'user_id': id
            }
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            //vm.showAlerts(vm, r)
        })
    }
    vm.delete = function(vm, r, id) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-delete',
            'xget': get,
            'user_id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            //vm.showAlerts(vm, r)
        })
    }
    vm.activate2 = function(vm, r, id) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-activate',
            'xget': get,
            'user_id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            //vm.showAlerts(vm, r)
        })
    }
    vm.delete_group = function(vm, r, group_id) {
        var get = 'group';
        var data = {
            'do': 'settings-settings-settings-group_delete',
            'xget': get,
            'group_id': group_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.groups = r.data.groups
            //vm.showAlerts(vm, r)
        })
    }
    vm.user_accees_if_acc_manag = function(vm, r) {
        vm.save_button_ar_disabled=false;
        vm.save_button_text_ar=helper.getLanguage('Save');
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.user_access = r.data.user_access;
            //vm.showAlerts(vm, r)
        })
    }
    vm.multiple_id_delete = function(vm, r, identity_id) {
        var get = 'identity';
        var data = {
            'do': 'settings-settings-settings-multiple_id_delete',
            'xget': get,
            'identity_id': identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.multiple = r.data.multiple
            //vm.showAlerts(vm, r)
        })
    }
    vm.filter_apps = function(type) {
        if (vm.appsType != type) {
            vm.appsType = type
        } else {
            vm.appsType = undefined
        }
    }
    vm.filter_apps2 = function(type) {
        vm.appsActive = type
    }
    vm.financial_settings = function(vm, r, formObj) {
        $rootScope.haveGeneralSettings = !1;
        formObj.$invalid = !0;
        var data = angular.copy(vm.financial);
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.financial = r.data.financial;
            //vm.showAlerts(vm, r, formObj)
        })
    }

    vm.credential = function() {
        var date = angular.copy(vm.accountantAccess);
        var get = 'accountantAccess';
        if (date.credentials.CRM == !0) {
            var crm = 1
        } else {
            var crm = ''
        }
        if (date.credentials.ARTICLES == !0) {
            var article = 12
        } else {
            var article = ''
        }
        if (date.credentials.ORDERS == !0) {
            var order = 6
        } else {
            var order = ''
        }
        if (date.credentials.PO_ORDERS == !0) {
            var po_order = 14
        } else {
            var po_order = ''
        }
        if (date.credentials.STOCK == !0) {
            var stock = 16
        } else {
            var stock = ''
        }
        if (date.credentials.QUOTES == !0) {
            var quote = 5
        } else {
            var quote = ''
        }
        if (date.credentials.PROJECTS == !0) {
            var project = 3
        } else {
            var project = ''
        }
        if (date.credentials.MAINTENANCE == !0) {
            var intervention = 13
        } else {
            var intervention = ''
        }
        if (date.credentials.CONTRACTS == !0) {
            var contract = 11
        } else {
            var contract = ''
        }
        if (date.credentials.BILLING == !0) {
            var invoice = 4
        } else {
            var invoice = ''
        }
        if (date.credentials.PIM == !0) {
            var webshop = 9
        } else {
            var webshop = ''
        }
        if (date.credentials.TIMESHEET == !0) {
            var timesheet = 8
        } else {
            var timesheet = ''
        }
        var credential = {
            '1': crm,
            '12': article,
            '6': order,
            '14': po_order,
            '5': quote,
            '3': project,
            '13': intervention,
            '11': contract,
            '4': invoice,
            '9': webshop,
            '8': timesheet,
            '16': stock,
        };
        var data = {
            'do': 'settings-settings-settings-credentials',
            'xget': get,
            credential: credential,
            user_id: date.user_id,
            crm_admin: date.admin_cred.ADMIN_1_V,
            article_admin: date.admin_cred.ADMIN_12_V,
            order_admin: date.admin_cred.ADMIN_6_V,
            po_order_admin: date.admin_cred.ADMIN_14_V,
            stock_admin: date.admin_cred.ADMIN_16_V,
            quote_admin: date.admin_cred.ADMIN_5_V,
            project_admin: date.admin_cred.ADMIN_3_V,
            maintenance_admin: date.admin_cred.ADMIN_13_V,
            contract_admin: date.admin_cred.ADMIN_11_V,
            invoice_admin: date.admin_cred.ADMIN_4_V,
            is_accountant: date.credentials.USER_ACCOUNTANT
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.accountantAccess = r.data.accountantAccess;
            //vm.showAlerts(vm, r)
        })
    }
    vm.UpdateUser = function(vm, r, formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(vm.accountantAccess);
        data.do = 'settings-settings-settings-UpdateAccountant';
        data.xget = 'accountantAccess';
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.accountantAccess = r.data.accountantAccess;
            //vm.showAlerts(vm, r, formObj)
        })
    }
    if ($stateParams.tab) {
        //angular.element('.loading_wrap').removeClass('hidden');
        $timeout(function() {
            angular.element('#settingsNav').parent().addClass('navigation-current');
            angular.forEach(angular.element('.tabs-settings .tab-pane:not(.sub-pane)'), function(value, key) {
                if (angular.element(value)) {
                    angular.element(value).removeClass("in active");
                }
            });
            switch ($stateParams.tab) {
                case 'regional':
                    vm.load('Regional', 'regional', 'language');
                    break;
                case 'mail_settings':
                    vm.load('mail_settings', 'mail_settings');
                    break;
                case 'financial':
                    vm.load('Financial', 'financial');
                    break;
                case 'vat':
                    vm.load('Vat', 'vat', 'vat_r');
                    break;
                case 'vat_regime':
                    vm.load('Vat_regime', 'vat_r')
                    break;
                case 'languages':
                    vm.load('Language', 'language');
                    break;
                case 'apps':
                    vm.load('apps', 'integrated', 'integrate');
                    break;
                case 'access':
                    vm.load('access', 'user_access');
                    break;
                case 'identity':
                    vm.load('identity', 'multiple');
                    break;
                case 'accountantAccess':
                    vm.load('accountantAccess', 'accountantAccess');
                    break;
                case 'zenData':
                    vm.load('zenFields', 'zenData');
                    break;
                case 'legal_info':
                    vm.load('legalInfo', 'legalInfo');
                    break;
                case 'api_interface':
                    vm.load('apiInfo', 'apiInfo');
                    break;
                case 'users':
                    $state.go('users');
                    break;
                default:
                    vm.load('company', 'account_address','mail_settings');
                    break;
            }
            angular.element('#settingsNav a[href="#'+$stateParams.tab+'"]').closest('li').addClass('active');
            angular.element('.tabs-settings #' + $stateParams.tab).addClass("in active");
        });
    } else {
        $timeout(function() {
            angular.forEach(angular.element('.tabs-settings .tab-pane:not(.sub-pane)'), function(value, key) {
                if (angular.element(value)) {
                    angular.element(value).removeClass("in active");
                }
            });
            angular.element('.settings-nav').addClass('navigation-current');
            angular.element('.settings-nav .company-info').addClass('active');
            angular.element('.tabs-settings #company').addClass("in active");
        });
        $('.sidebar-header').on('touchstart click', function(e) {                
            $('#navigation-menu>li.settings-nav').removeClass('navigation-current');
        });

    }
    vm.setZenField = function(item) {
        var obj = angular.copy(item);
        obj.do = 'settings--settings-setZenField';
        helper.doRequest('post', 'index.php', obj)
    }
    vm.setZenDoc = function(item) {
        var obj = angular.copy(item);
        obj.do = 'settings--settings-setZenDocument';
        helper.doRequest('post', 'index.php', obj)
    }
    vm.setZenTimeDoc = function(item) {
        var obj = angular.copy(item);
        obj.do = 'settings--settings-setZenTimeDocument';
        helper.doRequest('post', 'index.php', obj)
    }
    vm.generateToken = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'settings--settings-generateToken'
        }, function(r) {
            if (r.data.token) {
                vm.zenData.zen_token = r.data.token
            }
        })
    }
    vm.DeleteField = function(vm, field_id, table) {
        var get = 'categorisation';
        var data = {
            'do': 'settings-settings-settings-DeleteField',
            'xget': get,
            'field_id': field_id,
            'table': table
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            //vm.showAlerts(vm, r)
        })
    }
    vm.updatefield = function(vm, table, item) {
        var get = 'categorisation';
        var data = {
            'do': 'settings-settings-settings-UpdateField',
            'xget': get,
            'id': item.field_id,
            'table': table,
            'value': item.value,
            'code': item.code
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            //vm.showAlerts(vm, r)
        })
    }
    vm.AddField = function(vm, id, table, name) {
        var get = 'categorisation';
        var data = {
            'do': 'settings-settings-settings-add_field',
            'xget': get,
            'name': name,
            'table': table
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            //vm.showAlerts(vm, r)
        })
    }
    $scope.$watch('vm.codes_file', function() {
        if (vm.codes_step != 2) {
            vm.codes_data = {};
            vm.codes_step = 1
        }
        if (vm.codes_file) {
            //angular.element('.loading_wrap').removeClass('hidden');
            var formData = new FormData();
            formData.append('do', 'settings--settings-saveCodes');
            formData.append("Filedata", vm.codes_file);
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "index.php", !0);
            oReq.onload = function(oEvent) {
                //angular.element('.loading_wrap').addClass('hidden');
                var r = JSON.parse(oEvent.target.response);
                if (r.error) {
                    vm.showAlerts(vm, r)
                }
                if (r.success) {
                    vm.codes_data = r.data;
                    vm.codes_file = undefined;
                    vm.codes_step = 2
                }
            };
            oReq.send(formData)
        }
    });
    vm.save_codes_import = function() {
        var obj = {
            'do': 'settings--settings-updateCodes',
            'headers': angular.copy(vm.codes_data.headers),
            'link': vm.codes_data.link
        };
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                vm.codes_data = {};
                vm.codes_step = 1;
                vm.codes_file = undefined
            }
        })
    }
    vm.getGeneralSettings = function() {
        helper.doRequest('get', 'index.php', {
            'do': 'settings-settings',
            'xget': 'generalSettings'
        }, function(r) {
            vm.generalSettingsData = r.data
        })
    }
    vm.updateGeneralSettings = function(vm, r, formObj) {
        var data = r;
        data.do = 'settings-settings-settings-update_general_settings';
        data.xget = 'generalSettings';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                vm.generalSettingsData = r.data;
                //vm.showAlerts(vm, r)
            }
            if (r.error) {
                vm.showAlerts(vm, r, formObj)
            }
        })
    }
    vm.policySet = function(type) {
        if (type == '1') {
            //angular.element('.loading_wrap').removeClass('hidden');
            var obj = {
                'do': 'settings-settings-settings-update_policy',
                'type': type,
                'xget': 'legalInfo'
            };
            helper.doRequest('post', 'index.php', obj, function(r) {
                if (r.success) {
                    vm.legalInfo = r.data.legalInfo
                }
            })
        } else {
            $confirm({
                ok: helper.getLanguage('Confirm'),
                cancel: helper.getLanguage('Go Back'),
                text: "<p>" + helper.getLanguage("Are you sure you don't agree to the Terms?") + "</p><p>" + helper.getLanguage("If you don't agree to the Terms, you can't use my.akti.com.") + "</p>"
            }).then(function() {
                //angular.element('.loading_wrap').removeClass('hidden');
                var obj = {
                    'do': 'settings-settings-settings-update_policy',
                    'type': type,
                    'xget': 'legalInfo'
                };
                helper.doRequest('post', 'index.php', obj, function(r) {
                    if (r.success) {
                        vm.legalInfo = r.data.legalInfo
                    }
                })
            }, function() {
                vm.load('legalInfo', 'legalInfo');
                return !1
            })
        }
    }
    vm.deleteApiApp = function(client_id) {
        //angular.element('.loading_wrap').removeClass('hidden');
        var tmp_item = {};
        tmp_item.do = "settings-settings-settings-deleteApiApp";
        tmp_item.xget = "apiInfo";
        tmp_item.client_id = client_id;
        helper.doRequest('post', 'index.php', tmp_item, function(r) {
            if (r.success) {
                vm.apiInfo = r.data.apiInfo
            }
        })
    }
    vm.deleteApiToken = function(parent_index, child_index) {
        //angular.element('.loading_wrap').removeClass('hidden');
        var tmp_item = {
            "do": "settings--settings-deleteApiToken",
            "access_token": vm.apiInfo.apps[parent_index].access_tokens[child_index].access_token
        };
        helper.doRequest("post", "index.php", tmp_item, function(r) {
            if (r.success) {
                vm.apiInfo.apps[parent_index].access_tokens.splice(child_index, 1)
            }
        })
    }
    vm.editApiApp = function(id) {
        openModal("edit_api_app", {
            "id": id
        }, "lg")
    }

    vm.testMailConnection = function(formObj) {
        vm.mail_test_success = false;
        vm.mail_test_error = "";
        var tmp_item = angular.copy(vm.mail_settings);
        tmp_item.do = 'settings--settings-testSmtpConnection';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_item, function(r) {
            if (r.success) {
                vm.mail_test_success = true;
            } else {
                vm.mail_test_error = r.error.error;
            }
        });
    }

    vm.saveMailSettings = function(formObj) {
        vm.mail_test_success = false;
        vm.mail_test_error = "";
        var tmp_item = angular.copy(vm.mail_settings);
        tmp_item.do = 'settings--settings-saveMailSettings';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_item, function(r) {
            helper.showAlerts(vm, r, formObj);
        });
    }

    vm.saveAccessRights=function(){
        vm.save_button_ar_disabled=true;
        vm.save_button_text_ar=helper.getLanguage('Saved');
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'vat_lang':
                var iTemplate = 'VatLang';
                var ctas = 'vm';
                break;
            case 'edit_api_app':
                var iTemplate = 'ApiApp';
                var ctas = 'vm';
                break;
            case 'sendgrid_history':
                var iTemplate = 'SendGridHistory';
                var ctas = 'vm';

                break;
            case 'alert_info':
                var iTemplate = 'modal';
                var ctas = 'vmMod';
                break
        }
        var params = {
            template: 'sTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'vat_lang':
                params.params = {
                    "do": "settings-settings",
                    "xget": "VatLang",
                    "id": item.ID
                };
                break;
            case 'edit_api_app':
                params.params = {
                    "do": "settings-editApiApp",
                    "client_id": item.id
                };
                params.callback = function(data) {
                    if (data) {
                        vm.apiInfo = data.data.apiInfo
                    }
                }
                break;
            case 'sendgrid_history':
                params.params = { "do": "settings-settings", "xget": "sendgridHistory" };
                break;
            case 'alert_info':
                params.template = 'miscTemplate/' + iTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                break
        }
        modalFc.open(params)
    }
    if (!$stateParams.tab) {
        angular.element('.tabs-settings #company').addClass("in active");
        vm.account_address = data.data.account_address;
        vm.mail_settings=data.data.mail_settings;
    }
    /*vm.account_address = data.data.account_address;
    vm.is_accountant = data.data.is_accountant;
    vm.easyinvoice = data.data.easyinvoice;
    vm.is_zen = data.data.is_zen;
    vm.is_cozie = data.data.is_cozie*/
}]).controller('labelSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this,
        do_next = obj.ID ? 'settings-settings-settings-vat_change' : 'settings-settings-settings-addlabel',
        do_xget = obj.ID ? obj.xget : 'Vat';
    vm.obj = obj;
    if (obj.ID) {
        vm.oldValue = angular.copy(vm.obj.VALUE)
    } else {
        vm.obj.VALUE = '0,00';
        vm.obj.page_title = helper.getLanguage('New Vat rate')
    }
    vm.label = {};
    vm.save_me = function(vm) {
        var data = angular.copy(vm);
        data.do = do_next;
        data.xget = do_xget;
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.cancel = function() {
        vm.obj.VALUE = vm.oldValue;
        $uibModalInstance.dismiss('dismiss')
    }
}]).controller('labelnewSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this,
        do_next = obj.ID ? 'settings-settings-settings-vat_changenew' : 'settings-settings-settings-addlabelnew',
        do_xget = 'Vat_regime';
    vm.obj = obj;
    vm.obj.NO_VAT_PLUS = vm.obj.NO_VAT;
    if (obj.ID) {
        vm.oldValue = angular.copy(vm.obj.VALUE)
    } else {
        vm.obj.VALUE = '0,00';
        vm.obj.page_title = helper.getLanguage('New Vat Regime rate')
    }
    vm.vatCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Value'),
        create: !1,
        maxItems: 1
    };
    vm.setVatId = function() {
        vm.obj.vat_id = undefined
    }
    vm.label = {};
    vm.save_me = function(formObj) {
        var data = angular.copy(vm.obj);
        data.do = do_next;
        data.xget = do_xget;
        helper.doRequest('post', 'index.php', data, function(r) {            
            if (r.success) {
                $uibModalInstance.close(r)
            }else{
                helper.showAlerts(vm, r, formObj);
            }
        })
    }
    vm.cancel = function() {
        vm.obj.VALUE = vm.oldValue;
        $uibModalInstance.dismiss('dismiss')
    }
}]).controller('VatLangModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.cancel = function() {
        $uibModalInstance.close()
    };
    vm.save_me = function() {
        var obj = angular.copy(vm.obj);
        obj.do = 'settings--settings-setLangTranslation';
        helper.doRequest('post', 'index.php', obj, function(r) {
            helper.showAlerts(vm, r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('labelAddCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.obj = obj;
    vm.addlabels = {};
    vm.addlabel = function(vm) {
        var data = angular.copy(vm);
        data.do = 'settings-settings-settings-addlabel';
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('LangCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this,
        do_next = obj.ID_CUSTOM ? 'settings-settings-settings-edit_lang_custom' : 'settings-settings-settings-add_lang_custom',
        do_xget = obj.ID_CUSTOM ? 'Language' : 'Language';
    vm.obj = obj;
    if (obj.ID_CUSTOM) {
        vm.oldValue = angular.copy(vm.obj.NAME_CUSTOM)
    }
    vm.addcustomlang = function(vm) {
        var data = angular.copy(vm);
        data.do = do_next;
        data.xget = do_xget;
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('UserCtrl', ['$scope', '$stateParams', '$state', 'helper', 'data', 'selectizeFc', 'modalFc', '$uibModal', '$timeout', function($scope, $stateParams, $state, helper, data, selectizeFc, modalFc, $uibModal, $timeout) {
    console.log('UserEdit');
    var vm = this,
        typePromise;
    vm.app = 'settings';
    vm.ctrlpag = 'SettingsEditCtrl';
    vm.tmpl = 'settingsEdit';
    vm.user = {};
    vm.show_disabled = !1;
    vm.user_id = $stateParams.user_id;
    vm.inputType = 'password';
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    vm.groupCfg = {
        valueField: 'group_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Group'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.name) + ' </span>' + '</div>' + '</div>' + '</div>';
                if (item.group_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Add Group') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined || value == '') {} else if (value == '99999999999') {
                vm.modal_group(vm.user.group_add, 'sTemplate/group', 'GroupSetCtrl')
            } else {}
        },
        onType(str) {
            var selectize = angular.element('#group_list')[0].selectize;
            selectize.close()
        },
        maxItems: 1
    };
    vm.modal_group = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.groups = data.data.groups;
            var data = {
                'do': 'settings-settings',
                xget: 'groups'
            };
            helper.doRequest('get', 'index.php', data, function(r) {
                vm.user.group = r.data;
                $timeout(function() {
                    var selectize = angular.element('#group_list')[0].selectize;
                    selectize.open()
                })
            })
        }, function() {})
    };
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.changeuser = function(email) {
        vm.user.username = email;
        vm.checkInput(email)
    }
    vm.hideShowPassword = function(tip) {
        if (tip == 'password')
            vm.inputType = 'text';
        else vm.inputType = 'password'
    };
    vm.genPass = function(nr) {
        var pass = helper.generatePassword(nr);
        vm.user.password = pass;
        vm.user.password1 = pass
    }
    vm.save_button_text = helper.getLanguage('Save');
    vm.AddUser = function(vm, r, formObj) {
        formObj.$invalid = !0;
        //vm.save_button_text = 'Saved';
        var data = angular.copy(vm.user);
        helper.doRequest('post', 'index.php', data, function(r) {
            //vm.user = r.data.user;
            //vm.showAlerts(vm, r, formObj)
            $state.go('users');
        });
        vm.show_disabled = !0
    }
    vm.checkInput = function(input) {
        vm.show_disabled = !1;
        vm.save_button_text = helper.getLanguage('Save');
    }
    vm.checkForm = function(formObj) {
        if (formObj.$invalid) {
            return !0
        } else {
            if (vm.show_disabled && formObj.$name == 'Player') {
                vm.save_button_text = helper.getLanguage('Saved');
            } else {
                vm.save_button_text = helper.getLanguage('Save');
            }
            return !1
        }
    }
    vm.activateTab = function(i) {
        if (i == 'add_devices') {
            vm.get_devices()
        }
        vm.user.add_details = !1;
        vm.user.add_credentials = !1;
        vm.user.add_devices = !1;
        vm.save_button_text = helper.getLanguage('Saved');
        vm.user[i] = !0
    }
    vm.UpdateUser = function(vm, r, formObj) {
        vm.show_disabled = !1;
        formObj.$invalid = !0;
        vm.save_button_text = helper.getLanguage('Saved');
        if (vm.user.add_credentials == !1 || vm.user.add_credentials == undefined) {
            var data = angular.copy(vm.user);
            helper.doRequest('post', 'index.php', data, function(r) {
                vm.showAlerts(vm, r, formObj);
                vm.user.credentials=r.data.user.credentials;
            })
        }
    }
    vm.credential = function() {
        var get = 'NewUser';
        var date = angular.copy(vm.user);
        if (date.credentials.CRM == !0) {
            var crm = 1
        } else {
            var crm = ''
        }
        if (date.credentials.ARTICLES == !0) {
            var article = 12
        } else {
            var article = ''
        }
        if (date.credentials.ORDERS == !0) {
            var order = 6
        } else {
            var order = ''
        }
        if (date.credentials.PO_ORDERS == !0) {
            var po_order = 14
        } else {
            var po_order = ''
        }
        if (date.credentials.STOCK == !0) {
            var stock = 16
        } else {
            var stock = ''
        }
        if (date.credentials.QUOTES == !0) {
            var quote = 5
        } else {
            var quote = ''
        }
        if (date.credentials.PROJECTS == !0) {
            var project = 3
        } else {
            var project = ''
        }
        if (date.credentials.TIMETRACKER == !0) {
            var timetracker = 19
        } else {
            var timetracker = ''
        }
        if (date.credentials.MAINTENANCE == !0) {
            var intervention = 13
        } else {
            var intervention = ''
        }
        if (date.credentials.INSTALLATIONS == !0) {
            var installation = 17
        } else {
            var installation = ''
        }
        if (date.credentials.CONTRACTS == !0) {
            var contract = 11
        } else {
            var contract = ''
        }
        if (date.credentials.BILLING == !0) {
            var invoice = 4
        } else {
            var invoice = ''
        }
        if (date.credentials.PIM == !0) {
            var webshop = 9
        } else {
            var webshop = ''
        }
        if (date.credentials.TIMESHEET == !0) {
            var timesheet = 8
        } else {
            var timesheet = ''
        }
        if (date.credentials.REPORTS == !0) {
            var reports = 18
        } else {
            var reports = ''
        }
        var credential = {
            '1': crm,
            '12': article,
            '6': order,
            '14': po_order,
            '5': quote,
            '3': project,
            '19': timetracker,
            '13': intervention,
            '17': installation,
            '11': contract,
            '4': invoice,
            '9': webshop,
            '8': timesheet,
            '18': reports,
            '16': stock
        };
        var data = {
            'do': 'settings-settings-settings-credentials',
            'xget': get,
            credential: credential,
            user_id: date.user_id,
            crm_admin: date.admin_cred.ADMIN_1_V,
            article_admin: date.admin_cred.ADMIN_12_V,
            order_admin: date.admin_cred.ADMIN_6_V,
            po_order_admin: date.admin_cred.ADMIN_14_V,
            stock_admin: date.admin_cred.ADMIN_16_V,
            quote_admin: date.admin_cred.ADMIN_5_V,
            project_admin: date.admin_cred.ADMIN_3_V,
            timetracker_admin: date.admin_cred.ADMIN_19_V,
            maintenance_admin: date.admin_cred.ADMIN_13_V,
            installation_admin: date.admin_cred.ADMIN_17_V,
            contract_admin: date.admin_cred.ADMIN_11_V,
            invoice_admin: date.admin_cred.ADMIN_4_V,
            project_manage: date.credentials.PROJECTS_A,
            intervention_manage: date.credentials.MAINTENANCE_A,
            timetracker_manage: date.credentials.TIMETRACKER_A,
            is_accountant: date.credentials.USER_ACCOUNTANT,
            timesheet_manage: vm.user.credentials.TIMESHEET_A
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.user = r.data.user;
            vm.user.add_details = date.add_details;
            vm.user.add_credentials = date.add_credentials;
            vm.user.add_devices = date.add_devices;
            vm.showAlerts(vm, r)
        })
    }
    if (vm.user_id) {
        var get_data = {
            'do': 'settings-settings',
            'xget': 'NewUser',
            'user_id': vm.user_id
        }
    } else {
        var get_data = {
            'do': 'settings-settings',
            'xget': 'NewUser'
        }
    }
    vm.renderList = function(r) {
        if (r && r.data && r.data.redirect) {
            $state.go('users');
            return
        }
        vm.user = r.data.user
    };
    vm.get_devices = function() {
        helper.doRequest('get', 'index.php?do=settings-allodevices&user_id=' + vm.user.user_id, function(r) {
            vm.allo = r.data
        })
    }
    vm.importDevices = function() {
        openModal('import_devices', '', 'lg')
    }
    vm.deleteDevice = function(item, index, formObj) {
        var obj = {
            "do": "settings--settings-deleteDevice",
            "id": item.id
        };
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                vm.allo.list.splice(index, 1)
            }
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'import_devices':
                var iTemplate = 'ImportDevices';
                var ctas = 'sync';
                break
        }
        var params = {
            template: 'sTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'import_devices':
                params.params = {
                    "do": "settings-allodevices",
                    "xget": "Devices",
                    "user_id": vm.user.user_id
                };
                params.callback = function() {
                    vm.get_devices()
                }
                break
        }
        modalFc.open(params)
    }
    vm.renderList(data)
}]).controller('GroupSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.obj = obj;
    vm.disabled = !1;
    if (vm.obj.group_id == 1 || vm.obj.group_id == 2) {
        vm.disabled = !0
    }
    vm.add_group = function(vm, r, crm, article, order, po_order, quote, project, intervention, contract, invoice, webshop, timesheet, reports, timetracker, group_id, name) {
        var get = 'group';
        if (crm == 1) {
            var crm = 1
        }
        if (article == 1) {
            var article = 12
        }
        if (order == 1) {
            var order = 6
        }
        if (po_order == 1) {
            var po_order = 14
        }
        if (quote == 1) {
            var quote = 5
        }
        if (project == 1) {
            var project = 3
        }
        if (intervention == 1) {
            var intervention = 13
        }
        if (contract == 1) {
            var contract = 11
        }
        if (invoice == 1) {
            var invoice = 4
        }
        if (webshop == 1) {
            var webshop = 9
        }
        if (timesheet == 1) {
            var timesheet = 8
        }
        if (reports == 1) {
            var reports = 18
        }
        if (timetracker == 1) {
            var timetracker = 19
        }
        var credential = {
            '1': crm,
            '12': article,
            '6': order,
            '14': po_order,
            '5': quote,
            '3': project,
            '13': intervention,
            '11': contract,
            '4': invoice,
            '9': webshop,
            '8': timesheet,
            '18': reports,
            '19': timetracker
        };
        var data = {
            'do': 'settings-settings-settings-add_group',
            'xget': get,
            'credential': credential,
            'group_id': group_id,
            'name': name
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.update_group = function(vm, r, crm, article, order, po_order, quote, project, intervention, contract, invoice, webshop, timesheet, reports, timetracker, group_id, name, obj) {
        var get = 'group';
        if (crm == 1) {
            var crm = 1
        }
        if (article == 1) {
            var article = 12
        }
        if (order == 1) {
            var order = 6
        }
        if (po_order == 1) {
            var po_order = 14
        }
        if (quote == 1) {
            var quote = 5
        }
        if (project == 1) {
            var project = 3
        }
        if (intervention == 1) {
            var intervention = 13
        }
        if (contract == 1) {
            var contract = 11
        }
        if (invoice == 1) {
            var invoice = 4
        }
        if (webshop == 1) {
            var webshop = 9
        }
        if (timesheet == 1) {
            var timesheet = 8
        }
        if (obj.STOCK) {
            var stock = 16
        }
        if (obj.INSTALLATION) {
            var installation = 17
        }
        if (obj.CASHREGISTER) {
            var cashregister = 15
        }
        if (reports == 1) {
            var reports = 18
        }
        if (timetracker == 1) {
            var timetracker = 19
        }
        var credential = {
            '1': crm,
            '12': article,
            '6': order,
            '14': po_order,
            '5': quote,
            '3': project,
            '13': intervention,
            '11': contract,
            '4': invoice,
            '9': webshop,
            '8': timesheet,
            '16': stock,
            '17': installation,
            '15': cashregister,
            '18': reports,
            '19': timetracker
        };
        var data = {
            'do': 'settings-settings-settings-update_group',
            'xget': get,
            'credential': credential,
            'group_id': group_id,
            'name': name
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('campSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.campain = obj;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    vm.Campaign = function(vm, r, campaign_user, campaign_password, campaign_site) {
        var get = 'camp';
        var data = {
            'do': 'settings-settings-settings-campaign_monitor',
            'xget': get,
            'campaign_user': campaign_user,
            'campaign_password': campaign_password,
            'campaign_site': campaign_site
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.deactivate_camp = function(vm, r) {
        var get = 'camp';
        var data = {
            'do': 'settings-settings-settings-deactivate_camp',
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
}]).controller('chimpSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.chimp_mail = obj;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    vm.Chimp = function(vm, r, api_key) {
        var get = 'chimp';
        var data = {
            'do': 'settings-settings-settings-activate_chimp',
            'xget': get,
            'api_key': api_key
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.Chimp_deactivate = function(vm, r, api_key) {
        var get = 'chimp';
        var data = {
            'do': 'settings-settings-settings-deactivate_chimp',
            'xget': get,
            'api_key': api_key
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
}]).controller('dropbox_setupSetCtrl', ['$scope', 'helper', '$stateParams', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $stateParams, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.dropbox = obj;
    vm.dbconn = $stateParams.dbconn;
    vm.showAlerts = function(scope, d, formObj) {
            helper.showAlerts(scope, d, formObj)
        }
        /*vm.cancel = function() {
            $uibModalInstance.dismiss('cancel')
        };*/

    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r);
            if (vm.cfconn) {
                $state.go('settings', {
                    'tab': 'apps',
                    'dbconn': ''
                }, {
                    reload: !0
                })
            }

        })
    };

    vm.step2 = function(vm, r, authCode) {
        var get = 'dropbox_setup';
        var data = {
            'do': 'settings-settings-dropbox-step2',
            'xget': 'apps',
            'authCode': authCode
        };
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            vm.showAlerts(vm, r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }

    vm.activateDrop = function(formObj) {
        window.location.href = 'https://www.dropbox.com/oauth2/authorize?locale=&client_id=' + vm.dropbox.key + '&response_type=code&token_access_type=offline&redirect_uri=' + vm.dropbox.redirect_url;
    }

    vm.deactivateDrop = function(vm, r, authCode) {
        var get = 'dropbox_setup';
        var data = {
            'do': 'settings-settings-dropbox-deactivateDrop',
            'xget': 'apps',
            'authCode': authCode,
            'dbconn': ''
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('dropboxCallbackCtrl', ['$scope', '$state', 'helper', '$uibModal', 'data', function($scope, $state, helper, $uibModal, data) {
    console.log("dropbox callback");

    if (data.error && data.error.error) {
        $scope.message = data.error.error
    } else if (data.success.success) {
        $scope.message = data.success.success;
        window.location.href = data.data.redirect_link;

    }

}]).controller('codaboxSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.coda = obj;
    vm.coda.save_button_text = helper.getLanguage('Save');
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    vm.coda_connect = function() {
        var get = 'codabox';
        var data = angular.copy(vm.coda);
        data.do = 'settings-settings-settings-coda_connect';
        data.xget = 'apps';
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
    vm.coda_tok = function(vm, r, vat_nr) {
        var get = 'codabox';
        var data = {
            'do': 'settings-settings-settings-coda_tok',
            'xget': get,
            'vat_nr': vat_nr
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                vm.coda = r.data
            }
        })
    }
    vm.coda_cred = function(vm, r, token_id, sales_check, payments_check) {
        var get = 'codabox';
        var data = {
            'do': 'settings-settings-settings-coda_cred',
            'xget': get,
            'token_id': token_id,
            'sales_check': sales_check,
            'payments_check': payments_check
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                vm.coda = r.data
            }
        })
    }
    vm.coda_back = function(vm, r) {
        var get = 'codabox';
        var data = {
            'do': 'settings-settings-settings-coda_back',
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.coda = r.data;
            vm.showAlerts(vm, r)
        })
    }
    vm.coda_disconnect = function() {
        var get = 'codabox';
        var data = {
            'do': 'settings-settings-settings-coda_disconnect',
            //'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.coda = r.data;
            //vm.showAlerts(vm, r)
            $uibModalInstance.close(r)
        })
    }
    vm.setOption = function(type) {
        if (!vm.coda.active) {
            return !1
        }
        vm.coda.save_button_text = helper.getLanguage('Save');
        helper.doRequest('post', 'index.php', {
            'do': 'settings--settings-setCodaOptions',
            'type': type,
            'value': vm.coda[type]
        })
    }

    vm.saveSettings = function(formObj) {
        helper.doRequest('post', 'index.php', { 'do': 'settings--ExportAuto-updateExportAutoInvoice', 'invoice': vm.coda.exportAutoInvoice, 'pinvoice': vm.coda.exportAutoPInvoice, 'module': 'codabox' });
        vm.coda.save_button_text = helper.getLanguage('Saved');
    }

    vm.exportAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        readOnly: !0,
        onChange(value) {
            //helper.doRequest('post','index.php',{'do':'settings--ExportAuto-updateExportAutoInvoice','invoice':vm.coda.exportAutoInvoice,'pinvoice':vm.coda.exportAutoPInvoice,'module':'codabox'});
            vm.coda.save_button_text = helper.getLanguage('Save');
        },
        maxItems: 1
    };

}]).controller('calendarSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.calendars = obj;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    vm.toggleActivate = function(vm, r, value) {
        var get = 'codabox';
        var data = {
            'do': 'settings-settings-settings-toggleActivate',
            'xget': get,
            'value': value
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
}]).controller('ftp_importSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.import = obj;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    vm.ftp_import = function(vm, r, activate, app_id, ftp_host, ftp_user, ftp_password) {
        var get = 'ftp_import';
        var data = {
            'do': 'settings-settings-ftp_set-ftp_import',
            'xget': get,
            'activate': activate,
            'app_id': app_id,
            'ftp_host': ftp_host,
            'ftp_user': ftp_user,
            'ftp_password': ftp_password
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
}]).controller('btbSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', 'modalFc', function($scope, helper, $uibModalInstance, settingsFc, obj, modalFc) {
    var vm = this;
    vm.winbook = obj;
    vm.winbook.save_button_text = helper.getLanguage('Save');
    vm.providers = [{
        'id': '0',
        'name': helper.getLanguage('Ftp')
    }, {
        'id': '1',
        'name': helper.getLanguage('Dropbox')
    }];
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    vm.winbooks = function() {
        var obj = angular.copy(vm.winbook);
        obj.do = 'settings-settings-ftp_set-winbooks';
        obj.get = 'btb';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (!r.error) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, d)
            }
        })
    }
    vm.saveSettings = function(formObj) {
        helper.doRequest('post', 'index.php', { 'do': 'settings--ExportAuto-updateExportAutoInvoice', 'invoice': vm.winbook.exportAutoInvoice, 'pinvoice': vm.winbook.exportAutoPInvoice, 'module': 'efff' });
        vm.winbook.save_button_text = helper.getLanguage('Saved');
    }
    vm.showFolders = function() {
        if (vm.winbook.hide_deactive && vm.winbook.eff_drop > 0) {
            openModal('SelectTree', '', 'md')
        }
    }
    vm.providerCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select provider'),
        readOnly: !0,
        onChange(value) {
            if (!vm.winbook.drop_active && value == '1') {
                vm.winbook.eff_drop = '0';
                var error_msg = {
                    'error': {
                        'error': helper.getLanguage('Please activate Dropbox first')
                    },
                    'notice': !1,
                    'success': !1
                };
                vm.showAlerts(vm, error_msg)
            }
        },
        maxItems: 1
    };

    vm.exportAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        readOnly: !0,
        onChange(value) {
            //helper.doRequest('post','index.php',{'do':'settings--ExportAuto-updateExportAutoInvoice','invoice':vm.winbook.exportAutoInvoice,'pinvoice':vm.winbook.exportAutoPInvoice,'module':'efff'});
            vm.winbook.save_button_text = helper.getLanguage('Save');
        },
        maxItems: 1
    };

    function openModal(type, item, size) {
        var params = {
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'SelectTree':
                params.template = 'miscTemplate/' + type + 'Modal';
                //angular.element('.loading_wrap').removeClass('hidden');
                params.params = {
                    'do': 'settings--dropbox-getFolder',
                    'path': item
                };
                params.callback = function(d) {
                    if (d && d.path) {
                        vm.winbook.eff_drop_link = d.path
                    }
                }
                break
        }
        modalFc.open(params)
    }
}]).controller('yukiSetCtrl', ['$scope', 'helper', '$uibModalInstance', '$timeout', 'obj', 'modalFc', function($scope, helper, $uibModalInstance, $timeout, obj, modalFc) {
    var vm = this;
    vm.yuk = obj;
    vm.yuk.save_button_text = helper.getLanguage('Save');
    $timeout(function() {
        angular.element('.modal-dialog').removeClass('modal-md').css("width", "700px")
    });
    vm.providers = [{
        'id': '0',
        'name': helper.getLanguage('Api')
    }, {
        'id': '1',
        'name': helper.getLanguage('Dropbox')
    }];
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    vm.yuky = function(formObj) {
        var obj = angular.copy(vm.yuk);
        obj.do = 'settings-settings-settings-yuki';
        obj.get = 'yuki';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (!r.error) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r, formObj)
            }
        })
    }
    vm.saveSettings = function(formObj) {
        helper.doRequest('post', 'index.php', { 'do': 'settings--ExportAuto-updateExportAutoInvoice', 'invoice': vm.yuk.exportAutoInvoice, 'pinvoice': vm.yuk.exportAutoPInvoice, 'yuki_projects_enabled': vm.yuk.yuki_projects_enabled, 'module': 'yuki' });
        vm.yuk.save_button_text = helper.getLanguage('Saved');
    }
    vm.providerCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select provider'),
        readOnly: !0,
        onChange(value) {
            if (!vm.yuk.drop_active && value == '1') {
                vm.yuk.eff_drop = '0';
                var error_msg = {
                    'error': {
                        'error': helper.getLanguage('Please activate Dropbox first')
                    },
                    'notice': !1,
                    'success': !1
                };
                vm.showAlerts(vm, error_msg)
            }
        },
        maxItems: 1
    };
    vm.showFolders = function(item) {
        if (vm.yuk.hide_deactive) {
            vm.openModal('SelectTree', item, 'md')
        }
    }

    vm.exportAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        readOnly: !0,
        onChange(value) {
            // helper.doRequest('post','index.php',{'do':'settings--ExportAuto-updateExportAutoInvoice','invoice':vm.yuk.exportAutoInvoice,'pinvoice':vm.yuk.exportAutoPInvoice,'module':'yuki'});
            vm.yuk.save_button_text = helper.getLanguage('Save');
        },
        maxItems: 1
    };

    vm.openModal = function(type, item, size) {
        var params = {
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'SelectTree':
                params.template = 'miscTemplate/' + type + 'Modal';
                //angular.element('.loading_wrap').removeClass('hidden');
                params.params = {
                    'do': 'settings--dropbox-getFolder',
                    'path': ''
                };
                params.callback = function(d) {
                    if (d && d.path) {
                        vm.yuk[item] = d.path
                    }
                }
                break;
            case 'yuki_settings':
                var iTemplate = 'ExportSettings';
                var ctas = 'set';
                var params = {
                    template: 'iTemplate/' + iTemplate + 'Modal',
                    ctrl: iTemplate + 'ModalCtrl',
                    ctrlAs: ctas,
                    size: size,
                    backdrop: 'static'
                };
                params.params = {
                    'do': 'invoice-yuki_settings',
                    app: 'yuki'
                };
                break;
        }
        modalFc.open(params)
    }
}]).controller('pay_stripeSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.stripe = obj;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    vm.stripe_disconnect = function(vm, r) {
        var get = 'pay_stripe';
        var data = {
            'do': 'settings-settings-settings-stripe_disconnect',
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.stripe_connect = function(vm, r, api_key, publish_key) {
        var get = 'apps';
        var data = {
            'do': 'settings-settings-settings-stripe_connect',
            'xget': get,
            'api_key': api_key,
            'publish_key': publish_key
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('pay_icepaySetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.icepay = obj;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    vm.icepay_disconnect = function(vm, r) {
        var get = 'pay_icepay';
        var data = {
            'do': 'settings-settings-settings-icepay_disconnect',
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.icepay_connect = function(vm, r, api_key, merchant_id) {
        var get = 'apps';
        var data = {
            'do': 'settings-settings-settings-icepay_connect',
            'xget': get,
            'api_key': api_key,
            'merchant_id': merchant_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('lengowSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.lengo = obj;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    vm.lengow_disconnect = function(vm, r) {
        var get = 'lengow';
        var data = {
            'do': 'settings-settings-settings-lengow_disconnect',
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.lengow_connect = function(vm, r, access_token, secret) {
        var get = 'lengow';
        var data = {
            'do': 'settings-settings-settings-lengow_connect',
            'xget': get,
            'access_token': access_token,
            'secret': secret
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
}]).controller('IdentityCtrl', ['$scope', '$stateParams', '$state', 'settingsFc', 'helper', 'data', 'selectizeFc', 'Upload', function($scope, $stateParams, $state, settingsFc, helper, data, selectizeFc, Upload) {
    var vm = this;
    vm.app = 'settings';
    vm.ctrlpag = 'IdentityCtrl';
    vm.tmpl = 'sTemplate';
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    vm.save_btn_text=helper.getLanguage('Save');
    vm.disable_btn_submit=false;
    vm.identitys = {};
    vm.identity_id = $stateParams.identity_id;
    vm.add_identity = function(vm, r) {
        var params=angular.copy(r);
        params.return_data=1;
        vm.disable_btn_submit=true;
        helper.doRequest('post', 'index.php', params, function(result) {
            if(vm.tmp_files && vm.tmp_files.length){
                Upload.upload({
                    url: 'index.php',
                    data: {
                        file: vm.tmp_files,
                        'do': 'settings-settings-settings-uploadify1',
                        'xget': 'NewIdentity',
                        'identity_id': result.data.identity_id
                    }
                }).then(function(resp) {
                    $state.go('settings',{'tab':'identity'});
                }, function() {
                    $state.go('settings',{'tab':'identity'});
                })
            }else{
                $state.go('settings',{'tab':'identity'});
            }
            //vm.identitys = r.data.identitys;
            //vm.showAlerts(vm, r);        
        })
    }
    vm.update_identity = function(vm, r) {
        vm.disable_btn_submit=true;
        vm.save_btn_text=helper.getLanguage('Saved');
        helper.doRequest('post', 'index.php', r);
    }

    vm.resetSaveBtn=function(){
        vm.disable_btn_submit=false;
        vm.save_btn_text=helper.getLanguage('Save');
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.uploadFiles = function(files) {
        if (files && files.length) {
            if(vm.identity_id != 'tmp'){
                var p = angular.element('.upload-box1');
                p.find('.img-thumbnail').attr('src', '');
                var tmppath = URL.createObjectURL(files[0]);
                p.find('.img-thumbnail').attr('src', tmppath);
                p.find('.btn').addClass('hidden');
                p.find('.nameOfTheFile').text(files[0].name);
                Upload.upload({
                    url: 'index.php',
                    data: {
                        file: files,
                        'do': 'settings-settings-settings-uploadify1',
                        'xget': 'NewIdentity',
                        'identity_id': vm.identitys.identity_id
                    }
                }).then(function(r) {
                    vm.identitys = r.data.identitys
                }, function() {})
            }else{
                var p = angular.element('.upload-box1');
                p.find('.img-thumbnail').attr('src', '');
                var tmppath = URL.createObjectURL(files[0]);
                p.find('.img-thumbnail').attr('src', tmppath);
                p.find('.btn').addClass('hidden');
                p.find('.nameOfTheFile').text(files[0].name);
                vm.tmp_files=files;
            }       
        }
    }
    vm.uploadidentity = function(vm, e) {
        angular.element(e.target).parents('.upload-box1').find('.newUploadFile3').click()
    }
    angular.element('.newUploadFile3').on('click', function(e) {
        e.stopPropagation()
    }).on('change', function(e) {
        e.preventDefault();
        var _this = $(this),
            p = _this.parents('.upload-box1'),
            x = p.find('.newUploadFile3');
        if (x[0].files.length == 0) {
            return !1
        }
        if (x[0].files[0].size > 256000) {
            alert("Error: file is to big.");
            return !1
        }
        p.find('.img-thumbnail').attr('src', '');
        var tmppath = URL.createObjectURL(x[0].files[0]);
        p.find('.img-thumbnail').attr('src', tmppath);
        p.find('.btn').addClass('hidden');
        p.find('.nameOfTheFile').text(x[0].files[0].name);
        if(vm.identity_id != 'tmp'){
            var formData = new FormData();
            formData.append("Filedata", x[0].files[0]);
            formData.append("do", 'settings--settings-uploadify');
            formData.append("identity_id", vm.identity_id);
            p.find('.upload-progress-bar').css({
                width: '0%'
            });
            p.find('.upload-file').removeClass('hidden');
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "index.php", !0);
            oReq.upload.addEventListener("progress", function(e) {
                var pc = parseInt(e.loaded / e.total * 100);
                p.find('.upload-progress-bar').css({
                    width: pc + '%'
                })
            });
            oReq.onload = function(oEvent) {
                var res = JSON.parse(oEvent.target.response);
                p.find('.upload-file').addClass('hidden');
                p.find('.btn').removeClass('hidden');
                if (oReq.status == 200) {
                    if (res.success) {
                        helper.doRequest('get', 'index.php?do=settings-settings&xget=NewIdentity&identity_id=' + vm.identity_id + '&file=' + res.file_name, function(r) {
                            vm.identitys.image = r.data.identitys.image
                        })
                    } else {
                        alert(res.error)
                    }
                } else {
                    alert("Error " + oReq.status + " occurred when trying to upload your file.")
                }
            };
            oReq.send(formData)
        }else{
            vm.tmp_files=[x[0].files[0]];
        }     
    });
    if (vm.identity_id) {
        var get_data = {
            'do': 'settings-settings',
            'xget': 'NewIdentity',
            'identity_id': vm.identity_id
        }
    } else {
        var get_data = {
            'do': 'settings-settings',
            'xget': 'NewIdentity'
        }
    }
    vm.renderList = function(r) {
        vm.identitys = r.data.identitys
    };
    vm.renderList(data)
}]).controller('edebexSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.edebex = obj;
    vm.showAlerts = function(scope, d) {
        helper.showAlerts(scope, d)
    }
    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.edebex_connect = function() {
        var data = {
            'do': 'settings-settings-settings-edebex_connect',
            'xget': 'apps'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.edebex_disconnect = function() {
        var data = {
            'do': 'settings-settings-settings-edebex_disconnect',
            'xget': 'edebex'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.edebex = r.data;
            vm.showAlerts(vm, r)
        })
    }
}]).controller('instapaidSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.insta = obj;
    vm.showAlerts = function(scope, d) {
        helper.showAlerts(scope, d)
    }
    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.instapaid_connect = function() {
        var data = {
            'do': 'settings-settings-settings-instapaid_connect',
            'xget': 'apps'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.instapaid_disconnect = function() {
        var data = {
            'do': 'settings-settings-settings-instapaid_disconnect',
            'xget': 'instapaid'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.insta = r.data;
            vm.showAlerts(vm, r)
        })
    }
}]).controller('pay_mollieSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.mollie = obj;
    vm.showAlerts = function(scope, d) {
        helper.showAlerts(scope, d)
    }
    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.mollie_connect = function() {
        var data = {
            'do': 'settings-settings-settings-mollie_connect',
            'xget': 'apps',
            'api_key': vm.mollie.api_key
        };
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r)
            }
        })
    }
    vm.mollie_disconnect = function() {
        var data = {
            'do': 'settings-settings-settings-mollie_disconnect',
            'xget': 'pay_mollie'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.mollie = r.data;
            vm.showAlerts(vm, r)
        })
    }
}]).controller('smartsheetSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.smart = obj;
    vm.showAlerts = function(scope, d) {
        helper.showAlerts(scope, d)
    }
    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.smart_connect = function() {
        var data = {
            'do': 'settings-settings-settings-smart_connect',
            'xget': 'apps',
            'token_id': vm.smart.token_id
        };
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r)
            }
        })
    }
    vm.smart_disconnect = function() {
        var data = {
            'do': 'settings-settings-settings-smart_disconnect',
            'xget': 'smartsheet'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.smart = r.data;
            vm.showAlerts(vm, r)
        })
    }
}]).controller('subscriptionCtrl', ['$scope', '$state', '$compile', '$rootScope', 'helper', 'modalFc', 'data', function($scope, $state, $compile, $rootScope, helper, modalFc, data) {
    console.log('Subscription');
    $scope.item = {};
    var subscription = {
        'month': {
            'services': [29, 59, 99, 159, 199],
            'products': [49, 89, 129, 189, 249],
            'products_web': [89, 129, 169, 229, 289],
            'products_web_full': [149, 189, 229, 289, 349],
            'products_services': [59, 119, 169, 239, 309],
            'products_services_web': [99, 159, 209, 279, 349],
            'products_services_web_full': [159, 219, 269, 339, 409]
        },
        'year': {
            'services': [313, 637, 1069, 1717, 2149],
            'products': [529, 961, 1393, 2041, 2689],
            'products_web': [961, 1393, 1825, 2473, 3121],
            'products_web_full': [1609, 2041, 2473, 3121, 3769],
            'products_services': [637, 1285, 1825, 2581, 3337],
            'products_services_web': [1069, 1717, 2257, 3013, 3769],
            'products_services_web_full': [1717, 2365, 2905, 3661, 4417]
        }
    };
    var stripe_val = 0;
    var stripe_vat = 0;
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.renderPage = function(d) {
        if (d.data) {
            $scope.item = d.data;
            $scope.changeValues()
        }
    }

    function setStartAt() {
        var period = $scope.item.period,
            d = new Date();
        if (period == 1) {
            d.setMonth(d.getMonth() + 1)
        } else {
            d.setFullYear(d.getFullYear() + 1)
        }
        $scope.item.start_at = Math.round(d.getTime() / 1000)
    }

    function check_users() {
        var max_u = $scope.item.MAX_U,
            max_t = $scope.item.time_users,
            users = $scope.item.how_many,
            show_error = !1,
            show_t_error = !1,
            plan = $scope.item.plan;
        if (plan > 0) {
            switch (users) {
                case '0':
                    if (max_u > 1) {
                        show_error = !0
                    }
                    if (max_t > 1) {
                        show_t_error = !0
                    }
                    break;
                case '1':
                    if (max_u > 4) {
                        show_error = !0
                    }
                    if (max_t > 8) {
                        show_t_error = !0
                    }
                    break;
                case '2':
                    if (max_u > 8) {
                        show_error = !0
                    }
                    break;
                case '3':
                    if (max_u > 16) {
                        show_error = !0
                    }
                    break
            }
            if (show_error) {
                var msg = helper.getLanguage("You have too many active users (current number is ") + max_u + helper.getLanguage("). Remove some users or increase the number of users. ");
                var cont = {
                    "e_message": msg
                };
                openModal("stock_info", cont, "md");
                return !1
            }
            if (show_t_error) {
                var msg = helper.getLanguage("You have too many active timesheet users (current number is ") + max_t + helper.getLanguage("). Remove some timesheet users or increase the number of timesheet users");
                var cont = {
                    "e_message": msg
                };
                openModal("stock_info", cont, "md");
                return !1
            }
        }
        return !0
    }
    $scope.changeValues = function() {
        var sell = parseInt($scope.item.plan);
        how_many = parseInt($scope.item.how_many), period = parseInt($scope.item.period), result = 0, webshop = parseInt($scope.item.webshop), eu_countries = ['AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'], vat_added = !1;
        if (period == 1) {
            if (sell == 5) {
                result = subscription.month.services[how_many]
            }
            if (sell == 4) {
                if (webshop == 0) {
                    result = subscription.month.products[how_many]
                }
                if (webshop == 1) {
                    result = subscription.month.products_web[how_many]
                }
                if (webshop == 2) {
                    result = subscription.month.products_web_full[how_many]
                }
            }
            if (sell == 3) {
                if (webshop == 0) {
                    result = subscription.month.products_services[how_many]
                }
                if (webshop == 1) {
                    result = subscription.month.products_services_web[how_many]
                }
                if (webshop == 2) {
                    result = subscription.month.products_services_web_full[how_many]
                }
            }
        } else {
            if (sell == 5) {
                result = subscription.year.services[how_many]
            }
            if (sell == 4) {
                if (webshop == 0) {
                    result = subscription.year.products[how_many]
                }
                if (webshop == 1) {
                    result = subscription.year.products_web[how_many]
                }
                if (webshop == 2) {
                    result = subscription.year.products_web_full[how_many]
                }
            }
            if (sell == 3) {
                if (webshop == 0) {
                    result = subscription.year.products_services[how_many]
                }
                if (webshop == 1) {
                    result = subscription.year.products_services_web[how_many]
                }
                if (webshop == 2) {
                    result = subscription.year.products_services_web_full[how_many]
                }
            }
        }
        if ($scope.item.user_discount && $scope.item.user_disc != '0') {
            if ($scope.item.user_disc == '0') {
                var stripe_disc = 0
            } else {
                var stripe_disc = 0;
                for (x in $scope.item.user_disc_dd) {
                    if ($scope.item.user_disc_dd[x].id == $scope.item.user_disc) {
                        stripe_disc = parseInt($scope.item.user_disc_dd[x].name)
                    }
                }
            }
            if ($scope.item.stripe_discount_type == 'percent') {
                result = result - (result * stripe_disc / 100)
            } else if ($scope.item.stripe_discount_type == 'amount') {
                result = result - stripe_disc
            }
        }
        var result_without_vat = result,
            result_vat = 0;
        $scope.item.default = helper.displayNr(result_without_vat);
        if (eu_countries.indexOf($scope.item.COUNTRY_CODE) != -1 && ($scope.item.VALID_VAT == '' || $scope.item.VALID_VAT == "0")) {
            result_vat = result * (21 / 100);
            result = result + (result * (21 / 100));
            vat_added = !0
        }
        if (eu_countries.indexOf($scope.item.COUNTRY_CODE) == 1 && !vat_added) {
            result_vat = result * (21 / 100);
            result = result + (result * (21 / 100))
        }
        setStartAt();
        if (result_vat != 0) {
            $scope.item.vat_value = helper.displayNr(result_vat)
        }
        stripe_val = result_without_vat + result_vat;
        stripe_vat = result_vat
    }
    $scope.changePlan = function() {
        $scope.item.webshop = '0';
        $scope.changeValues()
    }
    $scope.changePeriod = function() {
        if ($scope.item.period == 1) {
            $scope.item.month_year = helper.getLanguage('per month')
        } else {
            $scope.item.month_year = helper.getLanguage('per year')
        }
        $scope.changeValues()
    }
    $scope.changeTax = function(type) {
        openModal("bill_change", type, "md")
    }
    $scope.cancelSubscription = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'settings-subscription-settings-cancel'
        }, function(r) {
            $state.reload();
            $scope.showAlerts(r)
        })
    }
    $scope.changeCCard = function() {
        $scope.item.change_card = 1;
        $scope.paySubscription()
    }
    $scope.paySubscription = function() {
        if (!check_users()) {
            return !1
        }
        var data = {};
        data.plan = $scope.item.plan;
        data.how_many = $scope.item.how_many;
        data.period = $scope.item.period;
        data.new_plan_webshop = $scope.item.webshop;
        data.sub_new = '1';
        data.amount = parseFloat($scope.item.vat_value) ? (parseFloat($scope.item.default) + parseFloat($scope.item.vat_value)) * 100 : parseFloat($scope.item.default) * 100;
        data.valid_vat = $scope.item.VALID_VAT;
        data.order_id = $scope.item.ORDER_ID;
        data.payed = $scope.item.PAYED;
        data.change_card = $scope.item.change_card ? $scope.item.change_card : 0;
        data.user_disc = $scope.item.user_disc;
        data.stripe_vat = stripe_vat;
        if ($scope.item.plan == '4') {
            data.user_time_amount = 0
        }
        if ($scope.item.stripe_user == '0' || $scope.item.stripe_user == '1') {
            $scope.stripeCheckout(data);
            return !1
        }
        if ($scope.item.stripe_user == '2') {
            data.old_stripe = 2;
            if (data.change_card == 1) {
                $scope.stripeCheckout(data)
            } else {
                data.do = 'settings-subscription';
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('post', 'index.php', data, function(r) {
                    if (r.success) {
                        helper.removeCachedTemplates();
                        angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                        angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                        $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                        $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                        $state.reload();
                        var msg = '<div class="alert alert-success text-center" role="alert"><b>' + r.success.success + '</b></div>';
                        var cont = {
                            "e_message": msg
                        };
                        openModal("stock_info", cont, "md")
                    } else {
                        var msg = '<div class="alert alert-info text-center" role="alert"><b>' + r.notice.notice + '</b></div>';
                        var cont = {
                            "e_message": msg
                        };
                        openModal("stock_info", cont, "md")
                    }
                })
            }
            return !1
        }
    }
    $scope.stripeCheckout = function(data) {
        if (data.old_stripe && data.change_card == 1) {
            var label = helper.getLanguage("Submit");
            var amount = '0'
        } else {
            var label = helper.getLanguage("Pay");
            var amount = stripe_val * 100
        }
        var handler = StripeCheckout.configure({
            key: $scope.item.stripe_pub_key,
            locale: $scope.item.stripe_locale,
            token: function(token) {
                data.stripeToken = token.id;
                data.stripeEmail = token.email;
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('post', 'index.php?do=settings-subscription', data, function(r) {
                    if (r.success) {
                        helper.removeCachedTemplates();
                        angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                        angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                        $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                        $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                        var msg = '<div class="alert alert-success text-center" role="alert"><b>' + r.success.success + '</b></div>';
                        var cont = {
                            "e_message": msg
                        };
                        openModal("stock_info", cont, "md")
                    } else {
                        var msg = '<div class="alert alert-info text-center" role="alert"><b>' + r.notice.notice + '</b></div>';
                        var cont = {
                            "e_message": msg
                        };
                        openModal("stock_info", cont, "md")
                    }
                })
            }
        });
        handler.open({
            name: 'Akti',
            amount: amount,
            image: 'images/stripe_akti.png',
            currency: 'eur',
            panelLabel: label
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'bill_change':
                var iTemplate = 'BillChange';
                var ctas = 'bill';
                break;
            case 'stock_info':
                var iTemplate = 'modal';
                var ctas = 'vmMod';
                break
        }
        var params = {
            template: 'settingsEdit/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'bill_change':
                params.params = {
                    'do': 'settings-change_billing',
                    type: item
                };
                params.callback = function() {
                    $state.reload()
                }
                break;
            case 'stock_info':
                params.template = 'miscTemplate/' + iTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                break
        }
        modalFc.open(params)
    }
    $scope.renderPage(data)
}]).controller('subscriptionNewCtrl', ['$scope', '$state', '$compile', '$rootScope', 'helper', 'modalFc', 'data', function($scope, $state, $compile, $rootScope, helper, modalFc, data) {
    console.log('Subscription New');
    $scope.item = {};
    var stripe_val = 0;
    var stripe_vat = 0;
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.showTxt = {
        pay: !0,
        update_pay: !1
    };

    $scope.renderPage = function(d) {
        if (d.data) {
            $scope.item = d.data
            $scope.item.pricing_plan_id = $scope.item.selected_plan;
            if ($scope.item.selected_plan == '6') {
                $scope.showTxt.pay = !1;
                $scope.showTxt.update_pay = !1;
            }
            if ($scope.item.is_trial) {

                $scope.showTxt.update_pay = 1;
            }
        }
    }



    function check_users() {
        var max_u = parseInt($scope.item.users),
            users = parseInt($scope.item.actual_users),
            show_error = !1,
            show_t_error = !1;
        if (max_u < users) {
            show_error = !0
        }
        if (show_error) {
            var msg = helper.getLanguage("You have too many active users (current number is ") + users + helper.getLanguage("). Remove some users or increase the number of users. ");
            var cont = {
                "e_message": msg
            };
            openModal("stock_info", cont, "md");
            return !1
        }
        return !0
    }
    $scope.changeTax = function(type) {
        openModal("bill_change", type, "md")
    }
    $scope.cancelSubscription = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        /*  helper.doRequest('post', 'index.php', {
              'do': 'settings-subscription-settings-cancel'
          }, function(r) {
              $state.reload();
              $scope.showAlerts(r)
          })*/
    }
    $scope.changeCCard = function() {
        $scope.item.change_card = 1;
        $scope.paySubscription()
    }
    $scope.recalculate = function() {
        var eu_countries = ['AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'];
        var vat_added = !1;
        var total_price = $scope.item.start_price;
        var nr_users = 1;

        //console.log($scope.item.pricing_plan_id, $scope.item.users);

        switch ($scope.item.pricing_plan_id) {
            case '1':
                total_price = 30;
                nr_users = 1;
                break;
            case '2':
                total_price = 60;
                nr_users = 2;
                break;
            case '3':
                total_price = 110;
                nr_users = 2;
                break;
            case '4':
                total_price = 110;
                nr_users = 5;
                break;
            case '5':
                total_price = 160;
                nr_users = 5;
                break;
            default:
                for (x in $scope.item.aditional) {
                    if ($scope.item.aditional[x][$scope.item.aditional[x].model]) {
                        total_price += $scope.item.aditional[x].price;
                    }
                }


                break;
        }

        if (!$scope.item.period) {
            total_price += (($scope.item.users - nr_users) * 12);
        } else {
            total_price -= total_price * 10 / 100;
            total_price += (($scope.item.users - nr_users) * 10);
        }
        /*total_price+=($scope.item.users*15);
        total_price+=($scope.item.users_t*5);*/

        if ($scope.item.user_discount && $scope.item.user_disc != '0') {
            if ($scope.item.user_disc == '0') {
                var stripe_disc = 0
            } else {
                var stripe_disc = 0;
                for (x in $scope.item.user_disc_dd) {
                    if ($scope.item.user_disc_dd[x].id == $scope.item.user_disc) {
                        stripe_disc = parseInt($scope.item.user_disc_dd[x].name)
                    }
                }
            }
            if ($scope.item.stripe_discount_type == 'percent') {
                total_price = total_price - (total_price * stripe_disc / 100)
            } else if ($scope.item.stripe_discount_type == 'amount') {
                total_price = total_price - stripe_disc
            }
        }
        $scope.item.total_price = helper.displayNr(total_price);
        var result_vat = 0;
        if (eu_countries.indexOf($scope.item.COUNTRY_CODE) != -1 && ($scope.item.VALID_VAT == '' || $scope.item.VALID_VAT == "0")) {
            result_vat = total_price * (21 / 100);
            vat_added = !0
        }
        if (eu_countries.indexOf($scope.item.COUNTRY_CODE) == 1 && !vat_added) {
            result_vat = total_price * (21 / 100)
        }
        if (result_vat != 0) {
            $scope.item.vat_value = helper.displayNr(result_vat)
        }
        $scope.item.stripe_val = !$scope.item.period ? total_price + result_vat : (total_price + result_vat) * 12;
        $scope.item.stripe_vat = !$scope.item.period ? result_vat : result_vat * 12
    }
    $scope.paySubscription = function() {

        if (!check_users()) {
            return !1
        }
        var data = angular.copy($scope.item);
        data.amount = parseFloat($scope.item.vat_value) > 0 ? (parseFloat($scope.item.total_price) + parseFloat($scope.item.vat_value)) * 100 : parseFloat($scope.item.total_price) * 100;
        data.valid_vat = $scope.item.VALID_VAT;
        data.payed = $scope.item.PAYED;
        data.change_card = $scope.item.change_card ? $scope.item.change_card : 0;
        data.period = $scope.item.period ? '2' : '1';
        data.stripe_cust_id = $scope.item.stripe_cust_id;
        data.xget = 'setupIntent';

        helper.doRequest('post', 'index.php?do=settings-stripeSetup', data, function(r) {
            if (r.data) {
                data.client_secret = r.data.client_secret;
                data.stripe_cust_id = r.data.stripe_cust_id;
                data.change_card = r.data.change_card;
                if (data.client_secret) {
                    $scope.stripeCheckout(data);
                }
            }
        })

        /*     if ($scope.item.stripe_user == '0' || $scope.item.stripe_user == '1') {
                 //console.log(data);
                 $scope.stripeCheckout(data);
                 return !1
             }
             if ($scope.item.stripe_user == '2') {
                 data.old_stripe = 2;
                 if (data.change_card == 1) {
                     $scope.stripeCheckout(data)
                 } else {
                     data.do = 'settings-subscription_new';
                     angular.element('.loading_wrap').removeClass('hidden');
                     helper.doRequest('post', 'index.php', data, function(r) {
                         if (r.success) {
                             $rootScope.haveGeneralSettings = !1;
                             helper.removeCachedTemplates();
                             angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                             angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                             $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                             $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                             $state.reload();
                             var msg = '<div class="alert alert-success text-center" role="alert"><b>' + r.success.success + '</b></div>';
                             var cont = {
                                 "e_message": msg
                             };
                             openModal("stock_info", cont, "md")
                         } else {
                             var msg = '<div class="alert alert-info text-center" role="alert"><b>' + r.notice.notice + '</b></div>';
                             var cont = {
                                 "e_message": msg
                             };
                             openModal("stock_info", cont, "md")
                         }
                         $scope.showAlerts(r)
                     })
                 }
                 return !1
             }*/
    }
    $scope.stripeCheckout = function(data) {
        //console.log(data);
        if (data.change_card == "1" || (data.change_card == "0" && data.is_trial)) {
            openModal("stripe", data, "sm");
        } else if (data.change_card == "0" && !data.is_trial) {
            data.xget = "updateSubscription";
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php?do=settings-stripeSetup', data, function(r) {
                if (r.success) {
                    $rootScope.haveGeneralSettings = !1;
                    helper.removeCachedTemplates();
                    angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                    angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                    $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                    $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                    $state.reload();
                    var msg = '<div class="alert alert-success text-center" role="alert"><b>' + r.success.success + '</b></div>';
                    var cont = {
                        "e_message": msg
                    };
                    openModal("stock_info", cont, "md")
                } else {
                    var msg = '<div class="alert alert-info text-center" role="alert"><b>' + r.notice.notice + '</b></div>';
                    var cont = {
                        "e_message": msg
                    };
                    openModal("stock_info", cont, "md")
                }
            })
        }

        return false;

        /*if (data.old_stripe && data.change_card == 1) {
            var label = helper.getLanguage("Submit");
            var amount = '0'
        } else {
            var label = helper.getLanguage("Pay");
            var amount = $scope.item.stripe_val * 100
        }
        var handler = StripeCheckout.configure({
            key: $scope.item.stripe_pub_key,
            locale: $scope.item.stripe_locale,
            token: function(token) {              
                  data.stripeToken = token.id;
                data.stripeEmail = token.email;
                data.stripeSecret = clientSecret;
                angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('post', 'index.php?do=settings-subscription_new', data, function(r) {
                    if (r.success) {
                        $rootScope.haveGeneralSettings = !1;
                        helper.removeCachedTemplates();
                        angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                        angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                        $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                        $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                        $state.reload();
                        var msg = '<div class="alert alert-success text-center" role="alert"><b>' + r.success.success + '</b></div>';
                        var cont = {
                            "e_message": msg
                        };
                        openModal("stock_info", cont, "md")
                    } else {
                        var msg = '<div class="alert alert-info text-center" role="alert"><b>' + r.notice.notice + '</b></div>';
                        var cont = {
                            "e_message": msg
                        };
                        openModal("stock_info", cont, "md")
                    }
                })
            }
        });
        handler.open({
            name: 'Akti',
            amount: amount,
            image: 'images/stripe_akti.png',
            currency: 'eur',
            panelLabel: label
        })*/
    }

    $scope.selectPricingPlan = function(id) {
        var data = angular.copy($scope.item),
            pricing_plans = data.pricing_plans;

        $scope.item.pricing_plan_id = id;
        for (x in pricing_plans) {

            if (pricing_plans[x]['id'] == id) {
                $scope.item.users = $scope.item.pricing_plans[x]['nr_users'];
                $scope.item.pricing_plans[x]['selected'] = true;
                $scope.item.pricing_plans[x]['can_be_selected'] = false;
                $scope.item.pricing_plans[x]['select_text'] = helper.getLanguage('Selected');
            } else {
                if (pricing_plans[x]['id'] < id) {
                    $scope.item.pricing_plans[x]['can_be_selected'] = false;
                } else if (id == '3') {
                    $scope.item.pricing_plans[3]['can_be_selected'] = false;
                } else {
                    $scope.item.pricing_plans[x]['can_be_selected'] = true;
                }

                $scope.item.pricing_plans[x]['selected'] = false;
                $scope.item.pricing_plans[x]['select_text'] = helper.getLanguage('Select');
            }
        }
        $scope.item.pricing_plans[6]['can_be_selected'] = false;
        $scope.recalculate();
        $scope.showTxt.update_pay = !0;

        for (let x = $scope.item.USER_DD.length - 1; x >= 0; x--) {
            if ($scope.item.USER_DD[x]['id'] < $scope.item.users) {
                var idx = $scope.item.USER_DD.indexOf($scope.item.USER_DD[x])
                $scope.item.USER_DD.splice(idx, 1);
            }
        }
    }

    $scope.userCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Users'),
        create: !1,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<i class="fa fa-user" aria-hidden="true"></i> ' + escape(item.name) + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item"><i class="fa fa-user" aria-hidden="true"></i> ' + escape(data.name) + '</div>'
            }
        },
        onChange(value) {
            if (value != undefined || value != '') {
                if (parseInt(value) == 25) {
                    $scope.showTxt.pay = !1;
                    $scope.showTxt.update_pay = !1;
                } else {

                    if ($scope.item.pricing_plan_id == '6') {
                        $scope.showTxt.update_pay = !1;
                        $scope.showTxt.pay = !1;
                    } else {
                        $scope.showTxt.update_pay = !0;
                        $scope.showTxt.pay = !0;
                    }

                    $scope.recalculate()
                }
            }
        },
        maxItems: 1
    };

    function openModal(type, item, size) {
        switch (type) {
            case 'bill_change':
                var iTemplate = 'BillChange';
                var ctas = 'bill';
                break;
            case 'stock_info':
                var iTemplate = 'modal';
                var ctas = 'vmMod';
                break;
            case 'stripe':
                var iTemplate = 'StripeCheckout';
                var ctas = 'vmMod';
                break;
        }
        var params = {
            template: 'settingsEdit/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'bill_change':
                params.params = {
                    'do': 'settings-change_billing',
                    type: item
                };
                params.callback = function() {
                    $state.reload()
                }
                break;
            case 'stock_info':
                params.template = 'miscTemplate/' + iTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                params.callback = function() {
                    $state.reload()
                }
                break;
            case 'stripe':
                params.item = item;
                params.callback = function() {
                    $state.reload()
                }
                break
        }
        modalFc.open(params)
    }
    $scope.renderPage(data)
}]).controller('BillChangeModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var bill = this;
    bill.obj = data;
    bill.showAlerts = function(d, formObj) {
        helper.showAlerts(bill, d, formObj)
    }
    bill.cancel = function() {
        $uibModalInstance.close()
    }
    bill.ok = function(formObj) {
        if (bill.obj.is_address) {
            var data = angular.copy(bill.obj);
            delete data.account_vat_number
        } else {
            var data = {};
            data.account_vat_number = bill.obj.account_vat_number;
            data.account_billing_country_id = bill.obj.account_billing_country_id;
            data.customer_id = bill.obj.customer_id
        }
        data.do = bill.obj.do_next;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            bill.showAlerts(r, formObj);
            if (r.success) {
                $uibModalInstance.close()
            }
        })
    }
}]).controller('StripeCheckoutModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'modalFc', '$timeout', 'data', function($scope, helper, $uibModalInstance, modalFc, $timeout, data) {
    var vmMod = this;
    vmMod.obj = data;
    if (vmMod.obj.change_card == '1') {
        vmMod.obj.button_text = helper.getLanguage("Update card info");
    } else {
        vmMod.obj.button_text = helper.getLanguage("Pay") + ' &euro;' + helper.displayNr(vmMod.obj.stripe_val);
    }

    var stripe = Stripe(vmMod.obj.stripe_pub_key);
    var elements = stripe.elements({
        fonts: [{
            cssSrc: 'https://fonts.googleapis.com/css?family=Quicksand',
        }, ],
        // Stripe's examples are localized to specific languages, but if
        // you wish to have Elements automatically detect your user's locale,
        // use `locale: 'auto'` instead.
        locale: window.__exampleLocale,
    });

    var elementStyles = {
        base: {
            color: '#55595c',
            fontWeight: 600,
            fontFamily: 'Quicksand, Open Sans, Segoe UI, sans-serif',
            fontSize: '16px',
            fontSmoothing: 'antialiased',

            ':focus': {
                color: '#55595c',
            },

            '::placeholder': {
                color: '#989fa3',
            },

            ':focus::placeholder': {
                color: '#989fa3',
            },
        },
        invalid: {
            color: '#eb1c26',
            ':focus': {
                color: '#eb1c26',
            },
            '::placeholder': {
                color: '#eb1c26',
            },
        },
    };

    var elementClasses = {
        focus: 'focus',
        empty: 'empty',
        invalid: 'invalid',
    };


    var cardNumber = elements.create('cardNumber', {
        style: elementStyles,
        classes: elementClasses,
    });


    var cardExpiry = elements.create('cardExpiry', {
        style: elementStyles,
        classes: elementClasses,
    });


    var cardCvc = elements.create('cardCvc', {
        style: elementStyles,
        classes: elementClasses,
    });

    // registerElements([cardNumber, cardExpiry, cardCvc]);

    $timeout(function() {
        cardNumber.mount('#card-number');
        cardExpiry.mount('#card-expiry');
        cardCvc.mount('#card-cvc');
        /* cardElement.mount('#card-element');*/
    }, 500);

    vmMod.showAlerts = function(d, formObj) {
        helper.showAlerts(vmMod, d, formObj)
    }
    vmMod.cancel = function() {
        $uibModalInstance.close()
    }
    vmMod.ok = function(formObj) {
        vmMod.message = '';
        var cardholderName = angular.element('#cardholder-name').val();
        var cardholderEmail = angular.element('#cardholder-email').val();
        var clientSecret = vmMod.obj.client_secret;
        var stripeCustId = vmMod.obj.stripe_cust_id;
        var changeCard = vmMod.obj.change_card;

        stripe.handleCardSetup(
            clientSecret, cardNumber, {
                payment_method_data: {
                    billing_details: { email: cardholderEmail }
                }
            }
        ).then(function(result) {
            if (result.error) {
                vmMod.message = result.error.message;
            } else {
                d = angular.copy(vmMod.obj);;
                d.payment_method = result.setupIntent.payment_method;
                d.stripe_name = cardholderName;
                d.stripe_email = cardholderEmail;
                d.stripe_cust_id = stripeCustId;
                d.change_card = changeCard;
                d.xget = 'stripeCustomer';

                if (d.payment_method) {
                    helper.doRequest('post', 'index.php?do=settings-stripeSetup', d, function(r) {
                        if (r.success) {
                            if (d.change_card) {
                                if (r.success) {
                                    var msg = '<div class="alert alert-success text-center" role="alert"><b>' + r.success.success + '</b></div>';

                                    var cont = {
                                        "e_message": msg
                                    };
                                    openModal("stock_info", cont, "md")
                                }
                                $uibModalInstance.close();
                            } else {
                                $uibModalInstance.close()
                            }

                        } else if (r.error) {
                            vmMod.message = r.error.message;
                        }
                    })
                } else {
                    vmMod.message = result.error.message;
                }
            }
        });
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'stock_info':
                var iTemplate = 'modal';
                var ctas = 'vmMod';
                break;
        }
        var params = {
            template: 'settingsEdit/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'stock_info':
                params.template = 'miscTemplate/' + iTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                break;
        }
        modalFc.open(params)
    }
}]).controller('tactillSetCtrl', ['$scope', '$rootScope', '$compile', '$state', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, $rootScope, $compile, $state, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.tact = obj;
    vm.showAlerts = function(scope, d) {
        helper.showAlerts(scope, d)
    }
    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.tact_connect = function() {
        var data = {
            'do': 'settings-settings-settings-tact_connect',
            'xget': 'apps',
            'api_key': vm.tact.api_key
        };
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                $uibModalInstance.close(r);
                helper.removeCachedTemplates();
                angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                $state.go('settings', {
                    'tab': 'apps'
                }, {
                    reload: !0
                })
            } else {
                vm.showAlerts(vm, r)
            }
        })
    }
    vm.tact_disconnect = function() {
        var data = {
            'do': 'settings-settings-settings-tact_disconnect',
            'xget': 'tactill'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            helper.removeCachedTemplates();
            angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
            angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
            $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
            $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
            $state.go('settings', {
                'tab': 'apps'
            }, {
                reload: !0
            });
            vm.showAlerts(vm, r)
        })
    }
}]).controller('nylasSetCtrl', ['$scope', '$state', '$window', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, $state, $window, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.nylas = obj;
    vm.showAlerts = function(scope, d) {
        helper.showAlerts(scope, d)
    }
    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.nylas_connect = function() {
        var data = {
            'do': 'settings-settings-settings-nylas_connect',
            'xget': 'apps'
        };
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            $uibModalInstance.close(r)
        })
    }
    vm.nylas_disconnect = function() {
        var data = {
            'do': 'settings-settings-settings-nylas_disconnect',
            'xget': 'nylas'
        };
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                vm.nylas = r.data
            }
            vm.showAlerts(vm, r)
        })
    }
}]).controller('nylasCallbackCtrl', ['$scope', '$state', 'data', function($scope, $state, data) {
    console.log("nylas callback");
    if (data.error && data.error.error) {
        $scope.message = data.error.error
    } else if (data.success.success) {
        $scope.message = data.success.success
    }
}]).controller('BillingCtrl', ['$scope', 'helper', 'list', 'data', function($scope, helper, list, data) {
    console.log('Billing Invoices');
    $scope.views = [{
        name: helper.getLanguage('All Statuses'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Draft'),
        id: 1,
        active: ''
    }, {
        name: helper.getLanguage('Final'),
        id: 6,
        active: ''
    }, {
        name: helper.getLanguage('Proforma'),
        id: 6,
        active: ''
    }, {
        name: helper.getLanguage('Credit Invoice'),
        id: 3,
        active: ''
    }, {
        name: helper.getLanguage('Late'),
        id: 4,
        active: ''
    }, {
        name: helper.getLanguage('Will not be paid'),
        id: 7,
        active: ''
    }, {
        name: helper.getLanguage('Open Invoices'),
        id: 5,
        active: ''
    }, {
        name: helper.getLanguage('Paid'),
        id: 8
    }, {
        name: helper.getLanguage('Archived'),
        id: -1
    }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'settings-billing',
        offset: 1
    };
    $scope.ord = '';
    $scope.reverse = !1;
    $scope.renderList = function(d) {
        if (d && d.data) {
            for (x in d.data) {
                $scope[x] = d.data[x]
            }
        }
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.renderList(data)
}]).controller('MyUsersCtrl', ['$scope', '$stateParams', '$timeout', 'helper', '$uibModal', 'settingsFc', 'list', 'data', 'orderByFilter', '$state', function($scope, $stateParams, $timeout, helper, $uibModal, settingsFc, list, data, orderBy, $state) {
    var vm = this;
    vm.app = 'settings';
    vm.ctrlpag = 'SettingsCtrl';
    vm.tmpl = 'miscTemplate';
    vm.logos = {};
    vm.account_address = {};
    vm.regional = {};
    vm.financial = {};
    vm.vat = {};
    vm.language = {};
    vm.users = {
        views: [{
            name: helper.getLanguage('All Statuses'),
            id: 0,
            active: 'active'
        }, {
            name: helper.getLanguage('Active'),
            id: 1,
            active: ''
        }, {
            name: helper.getLanguage('Inactive'),
            id: 2,
            active: ''
        }],
        activeView: 1,
        search: {
            search: '',
            'do': 'settings-settings',
            view: 1,
            xget: 'Users'
        },
        list: [],
        max_rows: 0
    };
    vm.groups = {};
    vm.integrated = {};
    vm.integrate = {};
    vm.user_access = {};
    vm.multiple = {};
    vm.appsType = undefined;
    vm.appsActive = !1;
    vm.propertyName = '';
    vm.reverse = !0;
    vm.ord = '';
    vm.friends = orderBy(vm.users.list, vm.propertyName, vm.reverse);
    vm.renderList = function(d) {
        if (d.data) {
            vm.users = d.data.users;
            var ids = [];
            var count = 0;
            for(y in vm.users.nav){
                ids.push(vm.users.nav[y]['user_id']);
                count ++;
            }
            sessionStorage.setItem('user.edit_total', count);
            sessionStorage.setItem('user.edit_list', JSON.stringify(ids));
            helper.showAlerts(vm, d)
        }
    };

    function showAlerts(d) {
        helper.showAlerts(view, d)
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.toggleSearchButtons = function(item, val) {
        list.tSearch(vm.users, item, val, vm.renderList)
    };
    vm.sortBy = function(propertyName) {
        vm.reverse = (propertyName !== null && vm.propertyName === propertyName) ? !vm.reverse : !1;
        vm.propertyName = propertyName;
        vm.users.list = orderBy(vm.users.list, vm.propertyName, vm.reverse)
    };
    vm.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Statuses'),
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            vm.filters(value)
        },
        maxItems: 1
    };
    vm.filters = function(p) {
        list.filter(vm.users, p, vm.renderList)
    }
    vm.modal_users = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.vat = data.data.vat;
            vm.language = data.data.language;
            helper.doRequest('get', 'index.php?do=settings-settings&xget=Users', function(r) {
                vm.users = r.data.users
            })
        }, function() {})
    };
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.vat = data.data.vat;
            vm.language = data.data.language
        }, function() {})
    };
    vm.modal_group = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.groups = data.data.groups
        }, function() {})
    };
    vm.modal_app = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            backdrop: 'static',
            resolve: {
                obj: function() {
                    var get_data = {
                        'do': 'settings-settings',
                        'xget': r
                    };
                    return helper.doRequest('get', 'index.php', get_data).then(function(r) {
                        return r.data
                    })
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.integrated = data.data.integrated;
            vm.integrate = data.data.integrate
        }, function() {})
    };
    vm.load = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'settings-settings',
            'xget': h
        };
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
            if (k == 'logos') {
                vm.default_logo = r.data.default_logo
            }
        })
    }
    vm.upload = function(vm, e) {
        angular.element(e.target).parents('.upload-box').find('.newUploadFile2').click()
    }
    angular.element('.newUploadFile2').on('click', function(e) {
        e.stopPropagation()
    }).on('change', function(e) {
        e.preventDefault();
        var _this = $(this),
            p = _this.parents('.upload-box'),
            x = p.find('.newUploadFile2');
        if (x[0].files.length == 0) {
            return !1
        }
        if (x[0].files[0].size > 256000) {
            alert("Error: file is to big.");
            return !1
        }
        p.find('.img-thumbnail').attr('src', '');
        var tmppath = URL.createObjectURL(x[0].files[0]);
        p.find('.img-thumbnail').attr('src', tmppath);
        p.find('.btn').addClass('hidden');
        p.find('.nameOfTheFile').text(x[0].files[0].name);
        var formData = new FormData();
        formData.append("Filedata", x[0].files[0]);
        formData.append("do", 'settings-settings-settings-upload');
        formData.append("xget", 'company');
        p.find('.upload-progress-bar').css({
            width: '0%'
        });
        p.find('.upload-file').removeClass('hidden');
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "index.php", !0);
        oReq.upload.addEventListener("progress", function(e) {
            var pc = parseInt(e.loaded / e.total * 100);
            p.find('.upload-progress-bar').css({
                width: pc + '%'
            })
        });
        oReq.onload = function(oEvent) {
            var res = JSON.parse(oEvent.target.response);
            p.find('.upload-file').addClass('hidden');
            p.find('.btn').removeClass('hidden');
            if (oReq.status == 200) {
                if (res.success) {
                    $timeout(function() {
                        vm.account_address = res.data.account_address
                    })
                } else {
                    alert(res.error)
                }
            } else {
                alert("Error " + oReq.status + " occurred when trying to upload your file.")
            }
        };
        oReq.send(formData)
    });
    vm.dragControlListeners = {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            var get = 'Language';
            var data = {
                'do': 'settings-settings-settings-change_language_name',
                'xget': get,
                'sort': vm.language.public_lang.public_lang,
                'sort_custom': vm.language.public_lang.extra_lang,
                'sort_custom_Extra': vm.language.public_lang.custom_lang
            };
            helper.doRequest('post', 'index.php', data, function(r) {
                vm.language = r.data.language
                vm.showAlerts(vm, r)
            })
        }
    }
    vm.deactivate_lang = function(vm, r, ID, check) {
        var get = 'Language';
        if (check == !1) {
            var data = {
                'do': 'settings-settings-settings-deactivate_lang',
                'xget': get,
                'ID': ID,
                'check': check
            }
        } else {
            var data = {
                'do': 'settings-settings-settings-activate_language',
                'xget': get,
                'ID': ID,
                'check': check
            }
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.language = r.data.language
            vm.showAlerts(vm, r)
        })
    }
    vm.deactivate_extra_lang = function(vm, r, ID, check) {
        var get = 'Language';
        if (check == !1) {
            var data = {
                'do': 'settings-settings-settings-deactivate_custom_lang',
                'xget': get,
                'ID': ID,
                'check': check
            }
        } else {
            var data = {
                'do': 'settings-settings-settings-activate_lang_custom',
                'xget': get,
                'ID': ID,
                'check': check
            }
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.language = r.data.language
            vm.showAlerts(vm, r)
        })
    }
    vm.change_main_user = function(vm, r, id) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-change_main_user',
            'xget': get,
            'user_id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            vm.showAlerts(vm, r)
        })
    }
    vm.deactivate_user = function(vm, r, id) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-deactivate',
            'xget': get,
            'user_id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            vm.showAlerts(vm, r)
        })
    }
    vm.activate = function(vm, r, id, check) {
        var get = 'Users';
        if (check == !0) {
            var data = {
                'do': 'settings-settings-settings-activate2',
                'xget': get,
                'user_id': id
            }
        } else {
            var data = {
                'do': 'settings-settings-settings-deactivate2',
                'xget': get,
                'user_id': id
            }
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            vm.showAlerts(vm, r)
        })
    }
    vm.ressend = function(vm, r, id, name) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-ressend',
            'xget': get,
            'crypt': id,
            'email': name
        };
        //angular.element('.loading_wrap').removeClass('hide');
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                vm.users.list = r.data.users.list
            }
        })
    }
    vm.cancelInvitation = function(item) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-cancelInvitation',
            'xget': get,
            'crypt': item
        };
        //angular.element('.loading_wrap').removeClass('hide');
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list;
            vm.showAlerts(vm, r)
        })
    }
    vm.delete = function(vm, r, id, crypt) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-delete',
            'xget': get,
            'user_id': id,
            'crypt': crypt
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            vm.showAlerts(vm, r)
        })
    }
    vm.activate2 = function(vm, r, id) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-activate',
            'xget': get,
            'user_id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            vm.showAlerts(vm, r)
        })
    }
    vm.delete_group = function(vm, r, group_id) {
        var get = 'group';
        var data = {
            'do': 'settings-settings-settings-group_delete',
            'xget': get,
            'group_id': group_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.groups = r.data.groups
            vm.showAlerts(vm, r)
        })
    }
    vm.user_accees_if_acc_manag = function(vm, r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.user_access = r.data.user_access;
            vm.showAlerts(vm, r)
        })
    }
    vm.multiple_id_delete = function(vm, r, identity_id) {
        var get = 'identity';
        var data = {
            'do': 'settings-settings-settings-multiple_id_delete',
            'xget': get,
            'identity_id': identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.multiple = r.data.multiple
            vm.showAlerts(vm, r)
        })
    }
    vm.filter_apps = function(type) {
        if (vm.appsType != type) {
            vm.appsType = type
        } else {
            vm.appsType = undefined
        }
    }
    vm.filter_apps2 = function(type) {
        vm.appsActive = type
    }
    vm.financial_settings = function(vm, r, formObj) {
        $rootScope.haveGeneralSettings = !1;
        formObj.$invalid = !0;
        var data = angular.copy(vm.financial);
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.financial = r.data.financial;
            vm.showAlerts(vm, r, formObj)
        })
    }
    vm.credential = function(vm, r, crm, article, order, po_order, quote, project, intervention, contract, invoice, webshop, timesheet, user_id, crm_admin, article_admin, order_admin, po_order_admin, quote_admin, project_admin, maintenance_admin, contract_admin, invoice_admin, project_manage, intervention_manage, is_accountant) {
        var get = 'accountantAccess';
        if (crm == !0) {
            var crm = 1
        } else {
            var crm = ''
        }
        if (article == !0) {
            var article = 12
        } else {
            var article = ''
        }
        if (order == !0) {
            var order = 6
        } else {
            var article = ''
        }
        if (po_order == !0) {
            var po_order = 14
        } else {
            var po_order = ''
        }
        if (stock == !0) {
            var stock = 16
        } else {
            var stock = ''
        }
        if (quote == !0) {
            var quote = 5
        } else {
            var quote = ''
        }
        if (project == !0) {
            var project = 3
        } else {
            var project = ''
        }
        if (intervention == !0) {
            var intervention = 13
        } else {
            var intervention = ''
        }
        if (contract == !0) {
            var contract = 11
        } else {
            var contract = ''
        }
        if (invoice == !0) {
            var invoice = 4
        } else {
            var invoice = ''
        }
        if (webshop == !0) {
            var webshop = 9
        } else {
            var webshop = ''
        }
        if (timesheet == !0) {
            var timesheet = 8
        } else {
            var timesheet = ''
        }
        var credential = {
            '1': crm,
            '12': article,
            '6': order,
            '14': po_order,
            '5': quote,
            '3': project,
            '13': intervention,
            '11': contract,
            '4': invoice,
            '9': webshop,
            '8': timesheet,
            '16': stock,
        };
        var data = {
            'do': 'settings-settings-settings-credentials',
            'xget': get,
            credential: credential,
            user_id: user_id,
            crm_admin: crm_admin,
            article_admin: article_admin,
            order_admin: order_admin,
            po_order_admin: po_order_admin,
            stock_admin: stock_admin,
            quote_admin: quote_admin,
            project_admin: project_admin,
            maintenance_admin: maintenance_admin,
            contract_admin: contract_admin,
            invoice_admin: invoice_admin,
            project_manage: project_manage,
            intervention_manage: intervention_manage,
            is_accountant: is_accountant
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.accountantAccess = r.data.accountantAccess;
            vm.showAlerts(vm, r)
        })
    }
    vm.UpdateUser = function(vm, r, formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(vm.accountantAccess);
        data.do = 'settings-settings-settings-UpdateAccountant';
        data.xget = 'accountantAccess';
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.accountantAccess = r.data.accountantAccess;
            vm.showAlerts(vm, r, formObj)
        })
    }

    var list_data = angular.copy(helper.getItem('settings-users')),
    params = {
        'do': 'settings-settings',
        'xget':'Users'
    };

    if (list_data && list_data.search) {
        for (x in list_data.search) {
            params[x] = list_data.search[x]
        }
    }
    angular.element('.loading_wrap').removeClass('hidden');

    helper.doRequest('get', 'index.php', params, function(r) {
        vm.users = r.data.users;
        var ids = [];
        var count = 0;
        for(y in vm.users.nav){
            ids.push(vm.users.nav[y]['user_id']);
            count ++;
        }
        sessionStorage.setItem('user.edit_total', count);
        sessionStorage.setItem('user.edit_list', JSON.stringify(ids));
        vm.group_list = r.data.group_list
    })
}]).controller('MultiuserSetCtrl', ['$scope', 'helper', '$uibModalInstance', '$uibModal', 'settingsFc', 'obj', 'selectizeFc', '$state', function($scope, helper, $uibModalInstance, $uibModal, settingsFc, obj, selectizeFc, $state) {
    var vm = this;
    vm.user_add = !1;
    vm.group_add = !1;
    vm.group_list = !1;
    vm.group_id = '';
    vm.emails = '';
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name']
    });
    vm.renderList = function() {
        var data = {
            'do': 'settings-multiuser'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            vm.groups = r.data.group_list.groups;
            vm.language = r.data.group_list.language;
            vm.group = r.data.group
        })
    };
    vm.checkgroup = function(group_id) {
        vm.group_id = group_id
    }
    vm.change_group = function(list) {
        if (list == !0) {
            vm.group_list = !0
        } else {
            vm.group_list = !1
        }
    }
    vm.newUser = function(add) {
        if (add == !0) {
            $state.go('user', {
                'user_id': "tmp"
            });
            $uibModalInstance.dismiss('cancel')
        } else {}
    }
    vm.modal_group = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.groups = data.data.groups.group_list
        }, function() {})
    };
    vm.ok = function(emails, lang) {
        var data = {
            'do': 'settings-settings-settings-add_user_email',
            'xget': 'Users',
            'emails': emails,
            'lang_id': lang,
            'group_id': vm.group_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r)
            }
        })
    };
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    vm.renderList()
}]).controller('BillingCtrl', ['$scope', 'helper', 'list', 'data', function($scope, helper, list, data) {
    console.log('Billing Invoices');
    $scope.views = [{
        name: helper.getLanguage('All Statuses'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Draft'),
        id: 1,
        active: ''
    }, {
        name: helper.getLanguage('Final'),
        id: 6,
        active: ''
    }, {
        name: helper.getLanguage('Proforma'),
        id: 6,
        active: ''
    }, {
        name: helper.getLanguage('Credit Invoice'),
        id: 3,
        active: ''
    }, {
        name: helper.getLanguage('Late'),
        id: 4,
        active: ''
    }, {
        name: helper.getLanguage('Will not be paid'),
        id: 7,
        active: ''
    }, {
        name: helper.getLanguage('Open Invoices'),
        id: 5,
        active: ''
    }, {
        name: helper.getLanguage('Paid'),
        id: 8
    }, {
        name: helper.getLanguage('Archived'),
        id: -1
    }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'settings-billing',
        offset: 1
    };
    $scope.ord = '';
    $scope.reverse = !1;
    $scope.renderList = function(d) {
        if (d && d.data) {
            for (x in d.data) {
                $scope[x] = d.data[x]
            }
        }
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.renderList(data)
}]).controller('zendeskSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'obj', function($scope, helper, $uibModalInstance, obj) {
    var vm = this;
    vm.zendesk = obj;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.zendesk_connect = function(formObj) {
        var data = {
            'do': 'settings-settings-settings-zendesk_connect',
            'xget': 'apps',
            'url': vm.zendesk.url,
            'email': vm.zendesk.email,
            'password': vm.zendesk.password
        };
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r, formObj)
            }
        })
    }
    vm.zendesk_disconnect = function() {
        var data = {
            'do': 'settings-settings-settings-zendesk_disconnect',
            'xget': 'zendesk'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                vm.zendesk = r.data
            }
        })
    }
}]).controller('clearfactsSetCtrl', ['$scope', '$state', '$stateParams', 'helper', '$uibModalInstance', 'obj', function($scope, $state, $stateParams, helper, $uibModalInstance, obj) {
    var vm = this;
    vm.clearfacts = obj;
    vm.cfconn = $stateParams.cfconn;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r);
            if (vm.cfconn) {
                $state.go('settings', {
                    'tab': 'apps',
                    'cfconn': ''
                }, {
                    reload: !0
                })
            }

        })
    };


    vm.save = function() {
        var data = {
            'do': 'settings-settings-settings-update_setting_clearfacts',
            'xget': 'apps',
            'sync_statuses': vm.clearfacts.sync_statuses ? 1 : 0
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r);
            if (vm.cfconn) {
                $state.go('settings', {
                    'tab': 'apps',
                    'cfconn': ''
                }, {
                    reload: !0
                })
            }
        })
    };
    vm.clearfacts_connect = function(formObj) {
        var data = {
            'do': 'settings-settings-settings',
            'xget': 'clearfacts_url',
        };

        helper.doRequest('get', 'index.php', data, function(r) {
            if (r.success) {
                window.location.href = r.data;
            } else {
                vm.showAlerts(vm, r, formObj)
            }
        })
    }

    vm.clearfacts_disconnect = function() {
        var data = {
            'do': 'settings-settings-settings-clearfacts_disconnect',
            'xget': 'clearfacts'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                vm.clearfacts = r.data
            }
        })
    }

    vm.exportAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        readOnly: !0,
        onChange(value) {
            helper.doRequest('post', 'index.php', { 'do': 'settings--ExportAuto-updateExportAutoInvoice', 'invoice': vm.clearfacts.exportAutoInvoice, 'pinvoice': vm.clearfacts.exportAutoPInvoice, 'module': 'clearfacts' });
        },
        maxItems: 1
    };


}]).controller('clearfactsCallbackCtrl', ['$scope', '$state', 'helper', '$uibModal', 'data', function($scope, $state, helper, $uibModal, data) {
    console.log("clearfacts callback");

    if (data.error && data.error.error) {
        $scope.message = data.error.error
    } else if (data.success.success) {
        $scope.message = data.success.success;
        window.location.href = data.data.redirect_link;

    }

}]).controller('allocloudSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'obj', function($scope, helper, $uibModalInstance, obj) {
    var vm = this;
    vm.allo = obj;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.allo_connect = function(formObj) {
        var data = {
            'do': 'settings-settings-settings-allo_connect',
            'xget': 'apps',
            'api_key': vm.allo.api_key,
            'username': vm.allo.username,
            'url': vm.allo.url
        };
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r, formObj)
            }
        })
    }
    vm.allo_disconnect = function() {
        var data = {
            'do': 'settings-settings-settings-allo_disconnect',
            'xget': 'allocloud'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                vm.allo = r.data
            }
        })
    }
}]).controller('ImportDevicesModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var sync = this;
    sync.obj = data;
    sync.showAlerts = function(d) {
        helper.showAlerts(sync, d)
    }
    sync.cancel = function() {
        $uibModalInstance.close()
    }
    sync.selectAll = function(bool) {
        for (x in sync.obj.list) {
            sync.obj.list[x].checked = bool
        }
    }
    sync.ok = function() {
        var obj = {
            'list': [],
            'user_id': sync.obj.user_id
        };
        for (x in sync.obj.list) {
            if (sync.obj.list[x].checked) {
                obj.list.push(sync.obj.list[x])
            }
        }
        obj.do = 'settings-allodevices-settings-importDevice';
        obj.xget = 'Devices';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            sync.showAlerts(r);
            if (r.success) {
                sync.obj = r.data
            }
        })
    }
}]).controller('SelectTreeModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.replyObj = {};
    $scope.cancel = function() {
        $uibModalInstance.close()
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.basicTree = [{
        name: "/",
        children: []
    }];
    if (data) {
        var tmpChildren = [];
        for (x in data) {
            var tmpObj = {};
            tmpObj.name = data[x].name;
            tmpObj.path = data[x].path;
            tmpObj.children = [];
            tmpChildren.push(tmpObj)
        }
        if (tmpChildren.length) {
            $scope.basicTree[0].children = tmpChildren
        }
    }
    $scope.$on('expanded-state-tochange', function(e, node) {
        if (!node.expanded && !node.children.length) {
            $scope.getDrop(node)
        }
    });
    $scope.$on('selection-changed', function(e, node) {
        $scope.replyObj.path = node.path
    });
    $scope.getDrop = function(node) {
        //angular.element('.loading_alt').removeClass('hidden');
        var obj = {
            'do': 'settings--dropbox-getFolder',
            'path': node.path
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (!r.error) {
                if (r.data.length) {
                    var tmpChildren = [];
                    for (x in r.data) {
                        var tmpObj = {};
                        tmpObj.name = r.data[x].name;
                        tmpObj.path = r.data[x].path;
                        tmpObj.children = [];
                        tmpChildren.push(tmpObj)
                    }
                    if (tmpChildren.length) {
                        node.children = tmpChildren;
                        node.expanded = !0
                    }
                } else {
                    node.expanded = !1;
                    var error_msg = {
                        'error': {
                            'error': helper.getLanguage('No subfolders')
                        },
                        'notice': !1,
                        'success': !1
                    };
                    $scope.showAlerts(error_msg)
                }
            }
        })
    }
    $scope.ok = function() {
        $uibModalInstance.close($scope.replyObj)
    }
}]).controller('ApiAppModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.showAlerts = function(d, formObj) {
        helper.showAlerts(vm, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.ok = function(formObj) {
        var tmp_item = angular.copy(vm.obj);
        tmp_item.do = tmp_item.client_id ? "settings-settings-settings-updateApiApplication" : "settings-settings-settings-addApiApplication";
        tmp_item.xget = "apiInfo";
        helper.doRequest("post", "index.php", tmp_item, function(r) {
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(r, formObj)
            }
        })
    }
}]).controller('billtoboxSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.billtobox = obj;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        // $uibModalInstance.dismiss('cancel')
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.billtobox_disconnect = function(vm, r) {
        var get = 'billtobox';
        var data = {
            'do': 'settings-settings-settings-billtobox_disconnect',
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.billtobox_connect = function(vm, formObj) {
        var get = 'apps';

        var data = {
            'do': 'settings-settings-settings-billtobox_connect',
            'xget': get,
            'username': vm.billtobox.username,
            'api_key': vm.billtobox.api_key,
            'sendSalesInvoice':vm.billtobox.sendSalesInvoice,
            'active_sales_api_key':vm.billtobox.active_sales_api_key,
            'access_key': vm.billtobox.access_key,
            'access_key_pinv': vm.billtobox.access_key_pinv,
            'exportAutoInvoice': vm.billtobox.exportAutoInvoice,
            //'exportAutoPInvoice':vm.billtobox.exportAutoPInvoice
            'export_billtobox_to_dropbox': vm.billtobox.export_billtobox_to_dropbox,
            'export_pinv_to_billtobox': vm.billtobox.export_pinv_to_billtobox,
            'exportPurchaseInvoices': vm.billtobox.exportPurchaseInvoices
        };
        helper.doRequest('post', 'index.php', data, function(r) {

            if (r.success) {
                vm.billtobox.is_active = true;
                vm.billtobox.title = helper.getLanguage('Deactivate');
                vm.showAlerts(vm, r);

                $uibModalInstance.close(r)
            } else {
                //console.log(vm, r);
                vm.showAlerts(vm, r, formObj);

            }
        })
    }

    vm.save = function(vm, formObj) {
        var get = 'billtobox';
        var data = {
            'do': 'settings-settings-settings-billtobox_activate_export',
            'export_billtobox_to_dropbox': vm.billtobox.export_billtobox_to_dropbox,
            'export_pinv_to_billtobox': vm.billtobox.export_pinv_to_billtobox,
            'access_key': vm.billtobox.access_key,
            'access_key_pinv': vm.billtobox.access_key_pinv,
            'api_key': vm.billtobox.api_key,
            'active_sales_api_key':vm.billtobox.active_sales_api_key,
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            //console.log(r);
            if (r.success) {
                //vm.billtobox.save_button_text = helper.getLanguage('Saved'); 
                $uibModalInstance.close(r);
            } else {
                //vm.billtobox.export_billtobox_to_dropbox = '';
                //vm.billtobox.access_key = '';
            }

            vm.showAlerts(vm, r, formObj);
        })
    }

    vm.change = function(vm) {
        vm.billtobox.save_button_text = helper.getLanguage('Save');
    }

    vm.exportAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        readOnly: !0,
        onChange(value) {
            helper.doRequest('post', 'index.php', { 'do': 'settings--ExportAuto-updateExportAutoInvoice', 'invoice': vm.billtobox.exportAutoInvoice, 'pinvoice': vm.billtobox.exportAutoPInvoice, 'module': 'billtobox' });
        },
        maxItems: 1
    };

    vm.exportAutoPurchaseCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        readOnly: !0,
        onChange(value) {

            switch (vm.billtobox.exportPurchaseInvoices) {
                case '0':
                    vm.billtobox.export_billtobox_to_dropbox = '0';
                    vm.billtobox.export_pinv_to_billtobox = '0';
                    vm.billtobox.access_key = '0';
                    vm.billtobox.access_key_pinv = '0';
                    break;
                case '1':
                    vm.billtobox.export_billtobox_to_dropbox = '1';
                    vm.billtobox.export_pinv_to_billtobox = '0';
                    break;
                case '2':
                    vm.billtobox.export_billtobox_to_dropbox = '0';
                    vm.billtobox.export_pinv_to_billtobox = '1';
                    break;
                default:
                    vm.billtobox.export_billtobox_to_dropbox = '0';
                    vm.billtobox.export_pinv_to_billtobox = '0';
                    break;
            }
        },
        maxItems: 1
    };

    vm.sendInvoiceCfg={
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        onDelete(val){
            return false;
        },
        maxItems: 1
    };

}]).controller('facqSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.facq = obj;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.facq_disconnect = function(vm, r) {
        var get = 'facq';
        var data = {
            'do': 'settings-settings-settings-facq_disconnect',
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.facq_connect = function(form) {
        var get = 'apps';

        var data = {
            'do': 'settings-settings-settings-facq_connect',
            'xget': get,
            'UserID': vm.facq.UserID,
            'Login': vm.facq.Login,
            'Password': vm.facq.Password,
            'active': 1,
        };
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                /* vm.facq.is_active=true;
                 vm.facq.title = helper.getLanguage('Deactivate');
                 vm.showAlerts(vm, r);*/
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r, form);
            }
        })
    }

    vm.save = function(vm) {
        var get = 'facq';
        var data = {
            'do': 'settings-settings-settings-facq_connect',
            'UserID': vm.facq.UserID,
            'Login': vm.facq.Login,
            'Password': vm.facq.Password,
            'active': 0,
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {

            /*if(r.success){
               vm.facq.save_button_text = helper.getLanguage('Saved'); 
            }
            
            vm.showAlerts(vm, r);*/
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r);
            }
        })
    }

    vm.change = function(vm) {
        vm.facq.save_button_text = helper.getLanguage('Save');
    }


}]).controller('exact_onlineSetCtrl', ['$scope', 'helper', '$uibModalInstance', '$stateParams', 'settingsFc', 'modalFc', 'obj', function($scope, helper, $uibModalInstance, $stateParams, settingsFc, modalFc, obj) {
    var vm = this;
    vm.exact_online = obj;
    vm.eoconn = $stateParams.eoconn;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r);
            if (vm.cfconn) {
                $state.go('settings', {
                    'tab': 'apps',
                    'eoconn': ''
                }, {
                    reload: !0
                })
            }

        })
    };


    vm.save = function() {
        var data = {
            'do': 'settings-settings-settings-update_setting_exact_online',
            'xget': 'apps',
            'sync_statuses': vm.exact_online.sync_statuses ? 1 : 0
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r);
            if (vm.eoconn) {
                $state.go('settings', {
                    'tab': 'apps',
                    'eoconn': ''
                }, {
                    reload: !0
                })
            }
        })
    };


    vm.exact_online_connect = function(formObj) {
        var data = {
            'do': 'settings-settings-settings',
            'xget': 'exact_online_url'
        };

        helper.doRequest('get', 'index.php', data, function(r) {
            if (r.success) {
                window.location.href = r.data;
            } else {
                vm.showAlerts(vm, r, formObj);
            }
        })
    }

    vm.exact_online_disconnect = function() {
        var data = {
            'do': 'settings-settings-settings-exact_online_disconnect',
            'xget': 'exact_online'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                vm.exact_online = r.data
            }
        })
    }

    vm.change = function(vm) {
        vm.exact_online.save_button_text = helper.getLanguage('Save');
    }

    vm.exportAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        readOnly: !0,
        onChange(value) {
            helper.doRequest('post', 'index.php', { 'do': 'settings--ExportAuto-updateExportAutoInvoice', 'invoice': vm.exact_online.exportAutoInvoice, 'pinvoice': vm.exact_online.exportAutoPInvoice, 'module': 'exact_online' });
        },
        maxItems: 1
    };

    vm.openModal = function(type, item, size) {
        var params = {
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {

            case 'exact_settings':
                var iTemplate = 'ExportSettings';
                var ctas = 'set';
                var params = {
                    template: 'iTemplate/' + iTemplate + 'Modal',
                    ctrl: iTemplate + 'ModalCtrl',
                    ctrlAs: ctas,
                    size: size,
                    backdrop: 'static'
                };
                params.params = {
                    'do': 'invoice-exact_settings',
                    app: 'exact_online'
                };
                break;
        }
        modalFc.open(params)
    }

}]).controller('exact_onlineCallbackCtrl', ['$scope', '$state', 'helper', '$uibModal', 'data', function($scope, $state, helper, $uibModal, data) {
    console.log("exact_online callback");

    if (data.error && data.error.error) {
        $scope.message = data.error.error
    } else if (data.success.success) {
        $scope.message = data.success.success;
        window.location.href = data.data.redirect_link;

    }

}]).controller('SendGridHistoryModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, helper, $uibModalInstance, data, modalFc) {
    var vm = this;
    vm.obj = data;

    vm.search = { 'do': 'settings-settings', 'offset': 1, 'xget': 'sendgridHistory' };

    vm.renderList = function(d) {
        if (d) {
            vm.obj = d.data;
        }
    }

    vm.cancel = function() {
        $uibModalInstance.close();
    };

    vm.get_more = function() {
        var obj = angular.copy(vm.search);
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            vm.obj = r.data;
        });
    }

    vm.showFullData = function(item) {
        var msg = "<h2>" + helper.getLanguage("Received") + ":</h2></br><p class=\"word_break_all\">" + item.webservice_incomming + "</p>" +
            "<h2>" + helper.getLanguage("Sent") + ":</h2></br><p class=\"word_break_all\">" + item.webservice_outgoing + "</p>";
        var cont = { "e_message": msg };
        vm.openModal('stock_info', cont, 'lg');
    }

    vm.openModal = function(type, item, size) {
        switch (type) {
            case 'stock_info':
                var iTemplate = 'modal';
                var ctas = 'vmMod';
                break;
        }
        var params = { template: 'miscTemplate/' + iTemplate + 'Modal', ctrl: iTemplate + 'ModalCtrl', ctrlAs: ctas, size: size };
        switch (type) {
            case 'stock_info':
                params.template = 'miscTemplate/' + iTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                break;
        }
        modalFc.open(params);
    }

}]).controller('sendgridSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'obj', function($scope, helper, $uibModalInstance, obj) {
    var vm = this;
    vm.sendgrid = obj;

    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj);
    }

    vm.cancel = function() {
        var data = { 'do': 'settings-settings', 'xget': 'apps' };
        helper.doRequest('get', 'index.php', data, function(r) {
            $uibModalInstance.close(r);
        });
    };
    vm.sendgrid_connect = function(formObj) {
        var data = angular.copy(vm.sendgrid);
        data = helper.clearData(data, ['CLASS2', 'active', 'title']);
        data.do = 'settings-settings-settings-sendgrid_connect';
        data.xget = 'apps';

        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                r.sec_app = 'sendgrid';
                r.sec_app_active = 'true';
                $uibModalInstance.close(r);
            } else {
                vm.showAlerts(vm, r, formObj);
            }
        });
    }
    vm.sendgrid_disconnect = function() {
        var data = { 'do': 'settings-settings-settings-sendgrid_disconnect', 'xget': 'apps' };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                vm.sendgrid = r.data;
                r.sec_app = 'sendgrid';
                r.sec_app_active = 'false';
                $uibModalInstance.close(r);
            }
        });
    }
}]).controller('netsuiteSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.netsuite = obj;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.netsuite_disconnect = function(vm, r) {
        var get = 'netsuite';
        var data = {
            'do': 'settings-settings-settings-netsuite_disconnect',
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.netsuite_connect = function(form) {
        var get = 'apps';

        var data = {
            'do': 'settings-settings-settings-netsuite_connect',
            'xget': get,
            'active': 1,
        };
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r, form);
            }
        })
    }

    vm.save = function(vm) {
        var get = 'netsuite';
        var data = {
            'do': 'settings-settings-settings-netsuite_connect',
            'active': 0,
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r);
            }
        })
    }

    vm.change = function(vm) {
        vm.netsuite.save_button_text = helper.getLanguage('Save');
    }


}]).controller('jefactureSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.jefacture = obj;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        var data = {
            'do': 'settings-settings',
            'xget': 'apps'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.jefacture_disconnect = function(vm, r) {
        var get = 'jefacture';
        var data = {
            'do': 'settings-settings-settings-jefacture_disconnect',
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.jefacture_connect = function(vm, formObj) {
        var get = 'apps';

        var data = {
            'do': 'settings-settings-settings-jefacture_connect',
            'xget': get,
            'api_key': vm.jefacture.api_key
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                vm.jefacture.is_active = true;
                vm.jefacture.title = helper.getLanguage('Deactivate');
                vm.showAlerts(vm, r);
            } else {
                vm.showAlerts(vm, r, formObj);

            }
        })
    }

}]);