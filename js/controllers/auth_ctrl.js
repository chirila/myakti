app.controller('loginCtrl', ['$scope', '$window', 'helper', '$stateParams', '$state', '$rootScope', '$compile', 'UserService', 'data', 'modalFc', function($scope, $window, helper, $stateParams, $state, $rootScope, $compile, UserService, data, modalFc) {
    console.log('loginCtrl');
    $scope.login = {
        remember: !1,
        username: '',
        lang: 'en',
        email: ''
    };
    $scope.disableBtn = !1;
    $scope.forgotDisable = !1;
    $scope.loginBtn = function() {
        $scope.disableBtn = !0;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.Loading++;
        helper.doRequest('post', 'index.php?do=auth-isLoged-auth-login', $scope.login, function(r) {
            $scope.disableBtn = !1;
            if (!$scope.alerts) {
                $scope.alerts = []
            }
            $scope.alerts = [];
            if (!r.error) {
                UserService.name = r.data.name ? r.data.name : '';
                helper.removeCachedTemplates();
                angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                UserService.must_pay = r.data.must_pay;
                UserService.default_pag = r.data.default_pag;
                helper.settings = [];
                helper.doRequest('get', 'index.php?do=misc-lang', function(d) {
                    LANG = d.data;
                    if (UserService.must_pay || r.data.pag) {
                        $state.go(r.data.pag)
                    } else {
                        $state.go(UserService.default_pag)
                    }
                }, function(r) {
                    if (UserService.must_pay || r.data.pag) {
                        $state.go(r.data.pag)
                    } else {
                        $state.go(UserService.default_pag)
                    }
                });
                if (r.data.showGdpr) {
                    openModal("GDPRPolicy", {})
                }
            } else {
                angular.element('.loading_wrap').addClass('hidden')
            }
            helper.showAlerts($scope, r)
        })
    }
    $scope.recoverBtn = function(user, email, lang) {
        if (!email) {
            return !1
        }
        $scope.forgotDisable = !0;
        var username = user;
        var email = email;
        $scope.recover = {
            email: email,
            lang: lang
        };
        $scope.email = {
            email: email
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.Loading++;
        helper.doRequest('post', 'index.php?do=auth-login-auth-reset_pass', $scope.recover, function(r) {
            $scope.forgotDisable = !1;
            if (!r.error) {
                $scope.recover;
                $scope.login.forgot = 1;
                $state.go('forgot', {
                    email: email,
                    lang: r.data.lang
                })
            } else {
                helper.showAlerts($scope, r);
                helper.Loading = 0;
                angular.element('.loading_wrap').addClass('hidden')
            }
        })
    }
    $scope.forgot = function() {
        $scope.login.forgot = 1
    }
    $scope.back = function() {
        $scope.login.forgot = 0
    }
    $scope.renderPage = function(r) {
        if (r.data) {
            $scope.login.lang = r.data.lang
        }
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'GDPRPolicy':
                var iTemplate = 'GDPRPolicy';
                var ctas = 'vm';
                break
        }
        var params = {
            template: 'miscTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size
        };
        switch (type) {
            case 'GDPRPolicy':
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-policy_info'
                };
                break
        }
        modalFc.open(params)
    }
    angular.element('.loading_wrap').removeClass('hidden');
    $scope.renderPage(data)
}]).controller('RecoverPassCtrl', ['$scope', '$stateParams', '$state', 'helper', 'data', function($scope, $stateParams, $state, helper, data) {
    console.log(data);
    $scope.login = {};
    $scope.lang = data.data.lang;
    $scope.back = function() {
        $state.go('login', {
            lang: data.data.lang
        })
    }
    $scope.change = function(username, password, password2, crypt) {
        var username = username;
        var password = password;
        var password2 = password2;
        var crypt = crypt;
        $scope.change_pass = {
            username: username,
            password: password,
            password2: password2,
            crypt: crypt
        };
        helper.doRequest('post', 'index.php?do=auth-login-auth-reset', $scope.change_pass, function(r) {
            if (!r.error) {
                $scope.login.forgot = 0;
                $state.go('login', {
                    lang: data.data.lang
                })
            }
            helper.showAlerts($scope, r)
        })
    }
    $scope.renderPage = function(r) {
        if (r.data) {
            $scope.login.username = r.data.usernmae;
            $scope.login.password = r.data.password;
            $scope.login.password2 = r.data.password2;
            $scope.login.crypt = r.data.crypt
        }
    }
    angular.element('.loading_wrap').addClass('hidden');
    $scope.renderPage(data)
}]).controller('CreateAccountCtrl', ['$scope', '$compile', '$rootScope', '$stateParams', '$state', 'helper', 'selectizeFc', 'data', 'UserService', 'modalFc', function($scope, $compile, $rootScope, $stateParams, $state, helper, selectizeFc, data, UserService, modalFc) {
    $scope.login = {};
    $scope.back = function() {
        $state.go('login', {
            lang: data.data.lang
        })
    }
    $scope.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    $scope.create = function(formObj) {
        var obj = angular.copy($scope.login);
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php?do=auth-isLoged-auth-create', obj, function(r) {
            if (r.error) {
                helper.showAlerts($scope, r, formObj)
            }
            if (r.success) {
                UserService.name = r.data.name ? r.data.name : '';
                helper.removeCachedTemplates();
                angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                UserService.must_pay = r.data.must_pay;
                UserService.default_pag = r.data.default_pag;
                helper.settings = [];
                helper.doRequest('get', 'index.php?do=misc-lang', function(d) {
                    LANG = d.data;
                    if (UserService.must_pay || r.data.pag) {
                        $state.go(r.data.pag)
                    } else {
                        $state.go(UserService.default_pag)
                    }
                }, function(r) {
                    if (UserService.must_pay || r.data.pag) {
                        $state.go(r.data.pag)
                    } else {
                        $state.go(UserService.default_pag)
                    }
                });
                if (r.data.showGdpr) {
                    openModal("GDPRPolicy", {})
                }
            }
        })
    }
    $scope.renderPage = function(r) {
        if (r.data) {
            $scope.login = r.data
        }
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'GDPRPolicy':
                var iTemplate = 'GDPRPolicy';
                var ctas = 'vm';
                break
        }
        var params = {
            template: 'miscTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size
        };
        switch (type) {
            case 'GDPRPolicy':
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-policy_info'
                };
                break
        }
        modalFc.open(params)
    }
    $scope.renderPage(data)
}]).controller('ForgotCtrl', ['$scope', 'helper', '$stateParams', '$state', 'data', function($scope, helper, $stateParams, $state, data) {
    var ls = this;
    ls.email = $stateParams.email;
    ls.lang = $stateParams.lang;
    ls.back = function(lang) {
        $state.go('login', {
            lang: ls.lang
        })
    }
}]).controller('FirstCtrl', ['$rootScope', '$stateParams', '$state', '$compile', 'helper', 'data', 'UserService', function($rootScope, $stateParams, $state, $compile, helper, data, UserService) {
    var ls = this;
    console.log($stateParams);
    ls.username = $stateParams.username;
    ls.lang = $stateParams.lang;
    ls.loginBtn = function() {
        ls.disableBtn = !0;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.Loading++;
        helper.doRequest('post', 'index.php?do=auth-isLoged-auth-setPass', {
            'username': ls.username,
            'password': ls.password,
            'password2': ls.password2
        }, function(r) {
            ls.disableBtn = !1;
            if (!r.error) {
                UserService.name = r.data.name ? r.data.name : '';
                helper.removeCachedTemplates();
                angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                UserService.must_pay = r.data.must_pay;
                UserService.default_pag = r.data.default_pag;
                helper.doRequest('get', 'index.php?do=misc-lang', function(d) {
                    LANG = d.data;
                    if (UserService.must_pay || r.data.pag) {
                        $state.go(r.data.pag)
                    } else {
                        $state.go(UserService.default_pag)
                    }
                }, function(r) {
                    if (UserService.must_pay || r.data.pag) {
                        $state.go(r.data.pag)
                    } else {
                        $state.go(UserService.default_pag)
                    }
                })
            } else {
                angular.element('.loading_wrap').addClass('hidden')
            }
            helper.showAlerts(ls, r)
        })
    }
}]).controller('loginTwoCtrl', ['$scope', '$rootScope', '$timeout', 'helper', '$state', '$compile', 'data', 'UserService', 'modalFc', function($scope, $rootScope, $timeout, helper, $state, $compile, data, UserService, modalFc) {
    var ls = this;
    console.log('loginTwoCtrl');
    ls.login = {};
    ls.back = function() {
        helper.doRequest('post', 'index.php', {
            'do': 'auth-isLoged-auth-logout'
        }, function(r) {
            $state.go('login')
        })
    }
    ls.sendCode = function() {
        helper.doRequest('post', 'index.php', {
            'do': 'auth-isLoged-auth-checkCode',
            'code': ls.login.code,
            'trust': ls.login.trust
        }, function(r) {
            if (!r.error) {
                helper.removeCachedTemplates();
                angular.element('.page').removeClass('page-login');
                angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                UserService.two_done = !0;
                $state.go(r.data.default_pag);
                if (r.data.showGdpr) {
                    openModal("GDPRPolicy", {})
                }
            } else {
                angular.element('.loading_wrap').addClass('hidden');
                helper.showAlerts(ls, r)
            }
        })
    }
    $timeout(function() {
        angular.element('#login_two_input').focus()
    });

    function openModal(type, item, size) {
        switch (type) {
            case 'GDPRPolicy':
                var iTemplate = 'GDPRPolicy';
                var ctas = 'vm';
                break
        }
        var params = {
            template: 'miscTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size
        };
        switch (type) {
            case 'GDPRPolicy':
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-policy_info'
                };
                break
        }
        modalFc.open(params)
    }
}]).controller('NewAccountCtrl', ['$scope', '$rootScope', '$stateParams', '$state', '$compile', 'helper', 'selectizeFc', 'data', function($scope, $rootScope, $stateParams, $state, $compile, helper, selectizeFc, data) {
    console.log('NewAccountCtrl');
    $scope.login = {};
    $scope.showAlerts = function(d, formObj) {
        helper.showAlerts($scope, d, formObj)
    }
    $scope.create = function(formObj) {
        var obj = angular.copy($scope.login);
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php?do=auth--auth-newAccount', obj, function(r) {
            if (r.success) {
                helper.removeCachedTemplates();
                angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                helper.doRequest('get', 'index.php?do=misc-lang', function(d) {
                    LANG = d.data;
                    $state.go('board')
                }, function(r) {})
            }
            $scope.showAlerts(r, formObj)
        })
    }
    $scope.renderPage = function(r) {
        if (r.data) {
            for (x in r.data) {
                $scope.login[x] = r.data[x]
            }
        }
    }
    $scope.renderPage(data)
}])