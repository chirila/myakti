app.controller('invoicesCtrl', ['$scope', '$rootScope', '$stateParams', 'helper', 'list', '$uibModal', '$timeout', '$confirm', 'modalFc', 'data', function($scope, $rootScope, $stateParams, helper, list, $uibModal, $timeout, $confirm, modalFc, data) {
    console.log('Invoices');
    $rootScope.opened_lists.invoices = true;
    $scope.views = [{
            name: helper.getLanguage('All Statuses'),
            id: 0,
            active: 'active'
        }, {
            name: helper.getLanguage('Draft'),
            id: 1,
            active: ''
        }, {
            name: ($rootScope.show_settings.not_easy_invoice_diy ? helper.getLanguage('Final') : helper.getLanguage('Exported')),
            id: 6,
            active: ''
        },
        /*{
               name: helper.getLanguage('Credit Invoice'),
               id: 3,
               active: ''
           }, {
               name: helper.getLanguage('Proforma'),
               id: 9,
               active: ''
           },*/
        {
            name: helper.getLanguage('Late'),
            id: 4,
            active: ''
        }, {
            name: helper.getLanguage('Will not be paid'),
            id: 7,
            active: ''
        }, {
            name: helper.getLanguage('Open Invoices'),
            id: 5,
            active: ''
        }, {
            name: helper.getLanguage('Paid'),
            id: 8
        }
    ];

    $scope.activeView = 0;
    $scope.list = [];
    $scope.nav = [];
    $scope.search = {
        search: '',
        'do': 'invoice-invoices',
        view: $scope.activeView,
        xget: '',
        offset: 1,
        archived: 0,
        showAdvanced: !1
    };
    $scope.search_deliveries = {
        search: '',
        'do': 'invoice-to_invoice_list',
        'to_invoice': 'Orders',
    };
    $scope.search_interventions = {
        search: '',
        'do': 'invoice-to_invoice_list',
        'to_invoice': 'Interventions',
    };
    $scope.listObj = {
        check_add_all: !1,
        markAll: !1
    };
    $scope.ord = '';
    $scope.reverse = !1;
    $scope.inv_labels = [helper.getLanguage('Invoices'), helper.getLanguage('Late Invoices')];
    $scope.inv_labels_month = [helper.getLanguage('Percent')];
    $scope.chart_options = {
        legend: {
            display: !1
        }
    };
    $scope.chart_colors = ['#DCDCDC', '#FCF8E3'];
    $scope.chart_data = [];
    $scope.show_load = !0;
    $scope.show_post_load = !0;
    $scope.nr_show = !0;
    $scope.amountsRefreshBlocked = false;
    $scope.minimum_selected = false;
    $scope.all_pages_selected = false;
    $scope.renderList = function(d, bool) {

        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.types = d.data.types;
            $scope.max_rows = d.data.max_rows;
            $scope.post_active = d.data.post_active;
            $scope.bPaid_reg = d.data.bPaid_reg;
            $scope.provider_ref = d.data.provider_ref;
            $scope.lr = d.data.lr;
            $scope.seller_data = d.data.seller_data;
            $scope.lg = d.data.lg;
            $scope.bpaid_active = d.data.bpaid_active;
            $scope.edebex_active = d.data.edebex_active;
            $scope.yuki_active = d.data.yuki_active;
            $scope.show_deliveries = d.data.show_deliveries;
            $scope.show_projects = d.data.show_projects;
            $scope.show_interventions = d.data.show_interventions;
            $scope.adv_search = d.data.adv_search;
            $scope.columns = d.data.columns;
            $scope.minimum_selected = d.data.minimum_selected;
            $scope.all_pages_selected = d.data.all_pages_selected;
            
            //$scope.initCheckAll();
            $scope.search_customer=d.in.search;
            $scope.customer_vat=d.in.customer_vat;
            if (bool || $scope.search_customer || $scope.customer_vat || $scope.search_customer=='' || $scope.customer_vat=='' ) {
                $scope.show_load = !0;
                $scope.show_post_load = !0;
                helper.doRequest('get', 'index.php', {
                    'do': 'invoice-invoices',
                    'search': $scope.search_customer,
                    'customer_vat': $scope.customer_vat,
                    'xget': 'KPI'
                }, function(r) {
                    if (r.data.chart_data) {
                        $scope.chart_data = r.data.chart_data
                    }
                    if (r.data.chart_data_month) {
                        $scope.chart_data_month = r.data.chart_data_month
                    }
                    $scope.data_month_i = r.data.data_month_i;
                    $scope.data_month_i_30 = r.data.data_month_i_30;
                    $scope.data_month_i_with_btw = r.data.data_month_i_with_btw;
                    if (r.data.inv_data) {
                        $scope.inv_data = r.data.inv_data
                    }
                    $scope.show_load = !1;
                });
                $scope.getAmounts();
                helper.doRequest('get', 'index.php', {
                    'do': 'invoice-invoices',
                    'xget': 'PostLibrary'
                }, function(r) {
                    $scope.show_post_load = !1;
                    $scope.post_library = r.data.post_library
                });
            }
            $scope.nav = d.data.nav;
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['invoice_id']);
                count ++;
            }
            sessionStorage.setItem('invoice.view_total', count);
            sessionStorage.setItem('invoice.view_list', JSON.stringify(ids));
        }
    };
    $scope.archived = function(value) {
        $scope.search.archived = value;
        list.filter($scope, value, $scope.renderList, true)
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList, true)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList, true)
    };
    $scope.searchThing = function(reset_list) {
        angular.element('.loading_wrap').removeClass('hidden');
        var params = angular.copy($scope.search);
        if (reset_list) {
            params.reset_list = '1';
        }
        list.search(params, $scope.renderList, $scope)
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.saveForPDF = function(p) {
        if (p) {
            if (p.check_add_to_product) {
                var elem_counter = 0;
                for (x in $scope.list) {
                    if ($scope.list[x].check_add_to_product) {
                        elem_counter++;
                    }
                }
                if (elem_counter == $scope.list.length && $scope.list.length > 0) {
                    $scope.listObj.check_add_all = true;
                }
            } else {
                $scope.listObj.check_add_all = false;
            }
        }
        list.saveForPDF($scope, 'invoice--invoice-saveAddToPdf', p)
    }
    $scope.deleteBulkInvoices = function() {
        var obj = angular.copy($scope.search);
        obj.do = 'invoice-invoices-invoice-deleteBulkInvoices';
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.renderList(r)
            }
        })
    }
    $scope.markSentBulkInvoices = function() {
        var obj = angular.copy($scope.search);
        obj.sent_date = new Date();
        obj.do = 'invoice-invoices-invoice-markSentBulkInvoices';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.renderList(r)
            }
        })
    }
    $scope.checkYuki = function() {
        var params = {
            'do': 'invoice-invoices-yuki-checkPay'
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r);
            if (r && r.error) {
                helper.showAlerts($scope, r)
            }
            if (r && r.in && r.in.inv_up && !r.error) {
                openModal("yuki_status", r.in.inv_up, 'md')
            }
        })
    };
    $scope.createExports = function($event, type) {
        list.exportFnc($scope, $event)
        if (type == 'export') {
            var params = angular.copy($scope.search);
            params.do = 'invoice-invoices'
            params.xget = 'List';
            params.exported = !0;
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', params, function(r) {
                $scope.renderList(r);
            })
        }
    }
    $scope.markCheckNew = function(i) {
        list.markCheckNew($scope, 'invoice--invoice-saveAddToPdf', i);
    }
    $scope.checkAllPage = function() {
        $scope.listObj.check_add_all = true;
        $scope.markCheckNew();
    }
    $scope.checkAllPages = function() {
        list.markCheckNewAll($scope, 'invoice--invoice-saveAddToPdfAll', '1');
    }
    $scope.uncheckAllPages = function() {
        list.markCheckNewAll($scope, 'invoice--invoice-saveAddToPdfAll', '0');
    }
    $scope.pay = function(item) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'iTemplate/payInvoice',
            controller: 'payInvoiceCtrl',
            resolve: {
                data: function() {
                    var d = {};
                    for (x in $scope.search) {
                        d[x] = $scope.search[x]
                    }
                    d.do = 'invoice-invoice_payment';
                    d.invoice_id = item.id;
                    return helper.doRequest('get', 'index.php', d).then(function(d) {
                        return d.data
                    })
                }
            }
        });
        modalInstance.result.then(function(d) {
            $scope.renderList(d)
        }, function() {
            console.log('modal closed')
        })
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.greenpost = function(item) {
        if ($scope.post_active) {
            var data = {};
            for (x in $scope.search) {
                data[x] = $scope.search[x]
            }
            data.do = 'invoice-invoices-invoice-postIt';
            data.id = item.id;
            data.type = item.invoice_type;
            data.c_invoice_id = item.c_invoice_id;
            data.related = 'list';
            data.no_status = !0;
            data.post_library = $scope.post_library;
            openModal("resend_post", data, "md")
        } else {
            var data = {};
            data.clicked_item = item;
            for (x in $scope.search) {
                data[x] = $scope.search[x]
            }
            openModal("post_register", data, "md")
        }
    }
    $scope.resendPost = function(item) {
        var data = {};
        for (x in $scope.search) {
            data[x] = $scope.search[x]
        }
        data.do = 'invoice-invoices-invoice-postIt';
        data.id = item.id;
        data.type = item.invoice_type;
        data.c_invoice_id = item.c_invoice_id;
        data.related = 'list';
        data.post_library = $scope.post_library;
        openModal("resend_post", data, "md")
    }
    $scope.bPaidFlow = function(index) {
        if ($scope.bPaid_reg) {
            var data = {};
            data.invoice_id = $scope.list[index].id;
            data.provider_ref = $scope.provider_ref;
            data.buyer_id = $scope.list[index].buyer_id;
            data.contact_id = $scope.list[index].contact_id;
            data.related = 'list';
            data.lg = $scope.lg;
            for (x in $scope.search) {
                data[x] = $scope.search[x]
            }
            openModal('bPaidOptions', data, 'md')
        } else {
            var data = {
                'index': index,
                'lg': $scope.lg
            };
            openModal('bPaidInfo', data, 'md')
        }
    }
    $scope.bePaidStatus = function(inv_id) {
        openModal('bpaid_status', inv_id, 'lg')
    }
    $scope.edebexFlow = function(inv_id) {
        openModal("edebex_sell", inv_id, 'md')
    }
    $scope.edebexStatus = function(inv_id) {
        openModal("edebex_status", inv_id, 'md')
    }
    $scope.showOutstanding = function() {
        $scope.filters(5)
    }
    $scope.get_invoices = function() {
        $scope.listObj.markAll = !1;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', $scope.search, function(r) {
            $scope.renderList(r, !0)
        })
    }
    $scope.get_deliveries = function() {
        $scope.listObj.markAll = !1;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'invoice-to_invoice_list',
            'to_invoice': 'deliveries'
        }, function(r) {
            $scope.renderList(r);
            if (r.data.amount_o) {
                $scope.amount_o = r.data.amount_o
            }
            if (r.data.number_o) {
                $scope.number_o = r.data.number_o
            }
            if (r.data.date_o) {
                $scope.date_o = r.data.date_o
            }
            if (r.data.downpayment_o) {
                $scope.downpayment_o = r.data.downpayment_o
            }
        })
    }
    $scope.renderDeliveries = function(r) {
        $scope.renderList(r);
        if (r.data.amount_o) {
            $scope.amount_o = r.data.amount_o
        }
        if (r.data.number_o) {
            $scope.number_o = r.data.number_o
        }
        if (r.data.date_o) {
            $scope.date_o = r.data.date_o
        }
        if (r.data.downpayment_o) {
            $scope.downpayment_o = r.data.downpayment_o
        }
    }
    $scope.removeSearchDeliveries = function(i, v) {
        var params = {};
        $scope.search_deliveries[i] = v;
        for (x in $scope.search_deliveries) {
            params[x] = $scope.search_deliveries[x]
        }
        list.search(params, $scope.renderDeliveries, $scope);
    }
    $scope.get_projects = function() {
        $scope.listObj.markAll = !1;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'invoice-to_invoice_list',
            'to_invoice': 'projects'
        }, function(r) {
            $scope.renderList(r);
            if (r.data.amount_days) {
                $scope.amount_days = r.data.amount_days
            }
            if (r.data.amount_p) {
                $scope.amount_p = r.data.amount_p
            }
            if (r.data.number_p) {
                $scope.number_p = r.data.number_p
            }
            if (r.data.date_p) {
                $scope.date_p = r.data.date_p
            }
        })
    }
    $scope.get_interventions = function() {
        $scope.listObj.markAll = !1;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'invoice-to_invoice_list',
            'to_invoice': 'interventions'
        }, function(r) {
            $scope.renderList(r);
            if (r.data.number_s) {
                $scope.number_s = r.data.number_s
            }
            if (r.data.date_s) {
                $scope.date_s = r.data.date_s
            }
            if (r.data.amount_s) {
                $scope.amount_s = r.data.amount_s
            }
            $scope.is_cozie = r.data.is_cozie
        })
    }
    $scope.renderInterventions = function(r) {
        $scope.renderList(r);
        if (r.data.number_s) {
            $scope.number_s = r.data.number_s
        }
        if (r.data.date_s) {
            $scope.date_s = r.data.date_s
        }
        if (r.data.amount_s) {
            $scope.amount_s = r.data.amount_s
        }
    }
    $scope.removeSearchInterventions = function(i, v) {
        var params = {};
        $scope.search_interventions[i] = v;
        for (x in $scope.search_interventions) {
            params[x] = $scope.search_interventions[x]
        }
        list.search(params, $scope.renderInterventions, $scope);
    }
    $scope.checkAll = function(type) {
        if (type == 1 || type == 3) {
            for (x in $scope.list) {
                for (y in $scope.list[x].quote) {
                    $scope.list[x].quote[y].checked = $scope.listObj.markAll
                }
            }
        } else {
            for (x in $scope.list) {
                for (y in $scope.list[x].quote) {
                    for (z in $scope.list[x].quote[y].project) {
                        $scope.list[x].quote[y].project[z].checked = $scope.listObj.markAll
                    }
                }
            }
        }
    }
    $scope.createInvoice = function(type, app) {
        var obj_global = {};
        obj_global[app] = [];
        if (app == 'orders' || app == 'interventions') {
            for (x in $scope.list) {
                for (y in $scope.list[x].quote) {
                    if ($scope.list[x].quote[y].checked == !0) {
                        obj_global[app].push($scope.list[x].quote[y].id_line)
                    }
                }
            }
        } else {
            for (x in $scope.list) {
                for (y in $scope.list[x].quote) {
                    for (z in $scope.list[x].quote[y].project) {
                        if ($scope.list[x].quote[y].project[z].checked == !0) {
                            obj_global[app].push($scope.list[x].quote[y].project[z].val)
                        }
                    }
                }
            }
        }
        if (obj_global[app].length == 0) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Please select one or more items")
                },
                "notice": !1,
                "success": !1
            };
            $scope.showAlerts(obj)
        } else {
            var data = {};
            data.post_list = app;
            data[app] = obj_global[app];
            data.typeOfInvoice = type;
            if (type == '2' && app == 'interventions') {
                data.is_cozie = $scope.is_cozie
            }
            openModal("progress_create_invoice", data, "md")
        }
    }
    $scope.setCustomHours = function(item) {
        openModal("custom_hours", item, "md")
    }
    $scope.setDefaultHours = function(item) {
        item.val = item.default_val
    }
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Statuses'),
        create: !1,
        onItemAdd(value) {
            if (value == undefined) {
                value = 0
            }
            $scope.filters(value)
        },
        maxItems: 1
    };

    $scope.filterCfgType = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All types'),
        create: !1,
        onChange(value) {
            $scope.toggleSearchButtons('doc_type', value)
        },
        maxItems: 1
    };

    $scope.checkOgm = function(val, item) {
        if (item != undefined) {
            var is_ok = !0;
            for (var i = 0; i < item.length; i++) {
                if (isNaN(parseInt(item.charAt(i)))) {
                    is_ok = !1;
                    break
                }
            }
            if (!is_ok) {
                $scope.search[val] = undefined
            }
        }
    }
    $scope.resetAdvanced = function() {
        $scope.search.ogm1 = undefined;
        $scope.search.ogm2 = undefined;
        $scope.search.ogm3 = undefined;
        $scope.search.ogm = undefined;
        $scope.search.ogm = undefined;
        $scope.search.article_name_search = undefined;
        $scope.search.your_reference_search = undefined;
        $scope.search.showAdvanced = !1;
        $scope.searchThing(true)
    }
    $scope.searchAdvanced = function() {
        $scope.search.ogm = undefined;
        if ($scope.search.ogm1 && $scope.search.ogm2 && $scope.search.ogm3) {
            $scope.search.ogm = $scope.search.ogm1 + $scope.search.ogm2 + $scope.search.ogm3
        }
        $scope.searchThing(true)
    }
    $scope.useVatKPI = function(value) {
        var obj = angular.copy($scope.search);
        obj.use_vat = value;
        obj.do = 'invoice-invoices-invoice-setVatKPI';
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.renderList(r, !0)
            }
        })
    }

    $scope.initCheckAll = function() {
        var total_checked = 0;
        for (x in $scope.list) {
            if ($scope.list[x].check_add_to_product) {
                total_checked++;
            }
        }
        if ($scope.list.length > 0 && $scope.list.length == total_checked) {
            $scope.listObj.check_add_all = true;
        } else {
            $scope.listObj.check_add_all = false;
        }
    }

    $scope.getAmounts = function(doRefresh) {
        $scope.nr_show = !0;
        helper.doRequest('get', 'index.php', {
            'do': 'invoice-invoices',
            'xget': 'Amounts',
            'refresh_data': (doRefresh ? 1 : 0)
        }, function(r) {
            if (r.data.amount_o) {
                $scope.amount_o = r.data.amount_o;
                $rootScope.amount_o = $scope.amount_o;
            }
            if (r.data.total_amount_p) {
                $scope.total_amount_p = r.data.total_amount_p;
                $rootScope.total_amount_p = $scope.total_amount_p;
            }
            if (r.data.amount_s) {
                $scope.amount_s = r.data.amount_s;
                $rootScope.amount_s = $scope.amount_s;
            }
            if (r.data.number_o) {
                $scope.number_o = r.data.number_o
            }
            if (r.data.date_o) {
                $scope.date_o = r.data.date_o
            }
            if (r.data.number_p) {
                $scope.number_p = r.data.number_p
            }
            if (r.data.amount_p) {
                $scope.amount_p = r.data.amount_p
            }
            if (r.data.amount_days) {
                $scope.amount_days = r.data.amount_days
            }
            if (r.data.date_p) {
                $scope.date_p = r.data.date_p
            }
            if (r.data.number_s) {
                $scope.number_s = r.data.number_s
            }
            if (r.data.date_s) {
                $scope.date_s = r.data.date_s
            }
            $scope.nr_show = !1;
            $rootScope.nr_show = $scope.nr_show;
            if (doRefresh) {
                angular.element('#refresh_amounts').removeClass("fa-spin fa-1x fa-fw");
            }
        });
    }

    $scope.refreshAmounts = function() {
        if ($scope.amountsRefreshBlocked) {
            return false;
        }
        angular.element('#refresh_amounts').addClass("fa-spin fa-1x fa-fw");
        $scope.getAmounts(true);
    }

    $scope.columnSettings = function() {
        openModal('column_set', {}, 'lg')
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'bpaid_status':
                var iTemplate = 'BePaidStatus';
                var ctas = 'bPay';
                break;
            case 'bPaidRegister':
                var iTemplate = 'BePaid';
                var ctas = 'bPay';
                break;
            case 'bPaidInfo':
                var iTemplate = 'BePaidInfo';
                var ctas = 'bPay';
                break;
            case 'bPaidOptions':
                var iTemplate = 'BePaidOptions';
                var ctas = 'bPay';
                break;
            case 'post_register':
                var iTemplate = 'PostRegister';
                var ctas = 'post';
                break;
            case 'edebex_sell':
                var iTemplate = 'EdebexSell';
                var ctas = 'edb';
                break;
            case 'edebex_status':
                var iTemplate = 'EdebexStatus';
                var ctas = 'edb';
                break;
            case 'progress_create_invoice':
                var iTemplate = 'ProgressCreateInvoice';
                var ctas = 'prog';
                break;
            case 'custom_hours':
                var iTemplate = 'CustomProjectHours';
                var ctas = 'prog';
                break;
            case 'resend_post':
                var iTemplate = 'ResendPostGreen';
                var ctas = 'post';
                break;
            case 'yuki_status':
                var iTemplate = 'YukiStatus';
                var ctas = 'vm';
                break
        }
        var params = {
            template: 'iTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'bpaid_status':
                params.params = {
                    'do': 'invoice--invoice-bePaidData',
                    invoice_id: item
                };
                break;
            case 'bPaidRegister':
                params.item = angular.copy($scope.seller_data);
                for (x in $scope.search) {
                    params.item[x] = $scope.search[x]
                }
                params.item.do = 'invoice-invoices-invoice-bpay_register';
                params.item.provider_ref = $scope.provider_ref;
                params.item.index = item.index;
                params.callback = function(data) {
                    if (data) {
                        if (data.data) {
                            $scope.renderList(data);
                            var new_data = {};
                            new_data.invoice_id = $scope.list[data.index].id;
                            new_data.provider_ref = $scope.provider_ref;
                            new_data.buyer_id = $scope.list[data.index].buyer_id;
                            new_data.contact_id = $scope.list[data.index].contact_id;
                            new_data.related = 'list';
                            new_data.lg = $scope.lg;
                            for (x in $scope.search) {
                                new_data[x] = $scope.search[x]
                            }
                            openModal('bPaidOptions', new_data, 'md')
                        }
                        $scope.showAlerts(data)
                    }
                }
                break;
            case 'bPaidInfo':
                params.item = item;
                params.callback = function(data) {
                    if (data) {
                        openModal('bPaidRegister', data, 'md')
                    }
                }
                break;
            case 'bPaidOptions':
                params.item = angular.copy(item);
                for (x in $scope.search) {
                    params.item[x] = $scope.search[x]
                }
                params.item.do = 'invoice-invoices-invoice-bPaid_notification';
                params.callback = function(data) {
                    if (data && data.data) {
                        $scope.renderList(data)
                    }
                }
                break;
            case 'post_register':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.item.do = 'invoice-invoices-invoice-postRegister';
                params.callback = function(data) {
                    if (data && data.data) {
                        $scope.renderList(data);
                        $scope.greenpost(data.clicked_item)
                    }
                    $scope.showAlerts(data)
                }
                break;
            case 'edebex_sell':
                params.params = {
                    'do': 'invoice--edebex-offer',
                    invoice_id: item
                };
                params.callback = function() {
                    helper.doRequest('get', 'index.php?do=invoice-invoices', $scope.search, $scope.renderList)
                }
                break;
            case 'edebex_status':
                params.params = {
                    'do': 'invoice--edebex-status',
                    invoice_id: item
                };
                break;
            case 'progress_create_invoice':
                params.item = item;
                params.callback = function(d) {
                    /*switch (app) {
                        case 'orders':
                            $scope.get_deliveries();
                            break;
                        case 'projects':
                            $scope.get_projects();
                            break;
                        case 'interventions':
                            $scope.get_interventions();
                            break
                    }*/
                    if (item.typeOfInvoice == '2' && d.done === false) {
                        angular.element('.tabs-default.nav_plus a[href="#invoices"]').tab('show');
                        $timeout(function() {
                            angular.element('.tabs-default.nav_plus a[href="#invoices"]').find('span').triggerHandler('click');
                        }, 1000);
                    }
                }
                break;
            case 'custom_hours':
                params.backdrop = 'static';
                params.item = item;
                break;
            case 'resend_post':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.request_type = 'post';
                params.params = {
                    'do': 'invoice--invoice-postInvoiceData',
                    'invoice_id': item.id,
                    'related': 'status',
                    'old_obj': item
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.renderList(d);
                        $scope.showAlerts(d)
                    }
                }
                break;
            case 'yuki_status':
                params.item = item;
                params.backdrop = 'static';
                break;
            case 'column_set':
                params.template = 'miscTemplate/ColumnSettingsModal';
                params.ctrl = 'ColumnSettingsModalCtrl';
                params.params = {
                    'do': 'misc-column_set',
                    'list': 'invoices'
                };
                params.ctrlAs = 'vm';
                params.callback = function(d) {
                    if (d) {
                        helper.doRequest('get', 'index.php', {
                            'do': 'invoice-invoices',
                            'xget': 'invoices_cols'
                        }, function(o) {
                            $scope.columns = o.data;
                            $scope.searchThing()
                        })
                    }
                }
                break;
        }
        modalFc.open(params)
    }
    $timeout(function() {
        angular.forEach(angular.element('.tabs-navigation .tab-pane'), function(value, key) {
            if (angular.element(value)) {
                angular.element(value).removeClass("in active");
            }
        });
        if ($stateParams.tab) {
            var paneActive = "";
            switch ($stateParams.tab) {
                case 'deliveries':
                    $scope.getAmounts();
                    paneActive = $stateParams.tab;
                    $scope.get_deliveries();
                    break;
                case 'projects':
                    $scope.getAmounts();
                    paneActive = $stateParams.tab;
                    $scope.get_projects();
                    break;
                case 'interventions':
                    $scope.getAmounts();
                    paneActive = $stateParams.tab;
                    $scope.get_interventions();
                    break;
                default:
                    paneActive = 'invoices';
                    list.init($scope, 'invoice-invoices', $scope.renderList, ($rootScope.reset_lists.invoices ? true : false))
            }
            //angular.element('.tabs-navigation ul.nav_plus a[href="#' + paneActive + '"]').parent().addClass('active');
            angular.element('.tabs-navigation #' + paneActive).addClass("in active");
        } else {
            //angular.element('.tabs-navigation ul.nav_plus a[href="#invoices"]').parent().addClass('active');
            angular.element('.tabs-navigation #invoices').addClass("in active");
            list.init($scope, 'invoice-invoices', $scope.renderList, ($rootScope.reset_lists.invoices ? true : false))
        }
        $rootScope.reset_lists.invoices = false;
    });


}]).controller('payInvoiceCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.pay = data;
    if (!$scope.pay.payment_date) {
        $scope.pay.payment_date = new Date()
    }
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        payment_date: !1,
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
    $scope.checkbox = function() {
        $scope.pay.do = 'invoice-invoices-invoice-pay';
        if ($scope.pay.not_paid == !1) {
            return !1
        }
        helper.doRequest('post', 'index.php', $scope.pay, $scope.closeModal)
    }
    $scope.ok = function() {
        angular.element('.loading_alt').removeClass('hidden');
        $scope.pay.do = $scope.pay.do_next;
        helper.doRequest('post', 'index.php', $scope.pay, $scope.closeModal)
    }
    $scope.closeModal = function(d) {
        angular.element('.loading_alt').addClass('hidden');
        $uibModalInstance.close(d)
    }
    $scope.ciCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        onChange(value) {
            if (value && this.options[value].date) {
                $scope.pay.payment_date = new Date(this.options[value].date);
                $scope.pay.amount = this.options[value].amount;
            }
        },
        maxItems: 1
    };

}]).controller('PostRegisterModalCtrl', ['$scope', 'helper', '$timeout', '$uibModalInstance', 'modalFc', 'data', function($scope, helper, $timeout, $uibModalInstance, modalFc, data) {
    var post = this;
    post.obj = data;
    post.showAlerts = function(d) {
        helper.showAlerts(post, d)
    }
    post.closeAlert = function(index) {
        post.alerts.splice(index, 1)
    }
    post.cancel = function() {
        $uibModalInstance.close()
    }
    post.ok = function() {
        var data = angular.copy(post.obj);
        if (data.clicked_item) {
            delete data.clicked_item
        }
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            post.showAlerts(r);
            if (r.success) {
                $timeout(function() {
                    if (post.obj.clicked_item) {
                        r.clicked_item = post.obj.clicked_item
                    }
                    $uibModalInstance.close(r)
                }, '1500')
            }
        })
    }
}]).controller('InvoiceCtrl', ['$scope', 'helper', function($scope, helper) {
    $scope.serial_number = '';
    $scope.isAddPage = !0;
    $scope.page_title = helper.getLanguage('Invoice');
}]).controller('InvoiceViewCtrl', ['$scope','$rootScope','helper', '$stateParams', '$uibModal', '$confirm', '$state', 'modalFc', 'editItem', 'selectizeFc', 'data', 'Lightbox', function($scope, $rootScope, helper, $stateParams, $uibModal, $confirm, $state, modalFc, editItem, selectizeFc, data, Lightbox) {
    var view = this;
    view.invoice_id = $stateParams.invoice_id;
    $scope.$parent.isAddPage = !0;
    view.invoice = {};
    view.invoice.item_exists = true;
    var pdfDoc = null,
        pageNum = 1,
        pageRendering = !1,
        pageNumPending = null,
        scale = 1.5,
        canvas = document.getElementById('the-canvas'),
        ctx = canvas? canvas.getContext('2d') :'';
    PDFJS.workerSrc = 'js/libraries/pdf.worker.js';
    var baseshowTxt = {
        sendEmail: !1,
        markAsReady: !1,
        viewNotes: !1,
        marsAsSent: !1,
        showPayment: !1,
        creditInvOpt: !1,
        sendPost: !1,
        source: !1,
        type: !1,
    };
    var date = new Date();
    view.format = 'dd/MM/yyyy';
    view.datePickOpen = {
        sent_date: !1,
        invoice_date: !1,
        payment_date: !1
    };
    view.dateOptions = {
        dateFormat: '',
        startingDay: 1
    };
    view.email = {};
    view.auto = {
        options: {
            recipients: []
        }
    };
    view.stationaryCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select E-Stationary'),
        create: !1,
        maxOptions: 100,
        closeAfterSelect: !0,
        maxItems: 1
    };
    view.selectfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    view.contactForEmailAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select recipient'),
        create: !0,
        createOnBlur: true,
        maxItems: 100
    });
    view.tinymceOptions = {
        selector: ".mail_send",
        resize: "height",
        height: 184,
        menubar: !1,
        relative_urls: 1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
    };
    view.renderPage = function(d, noAlert) {
        view.showTxt = angular.copy(baseshowTxt);
        view.showTxt.drop = !1;
        if (d.data) {
            if(!d.data.item_exists){
                view.invoice.item_exists = false;
                return 0;
            }; 
            if (d.data.redirect) {
                $state.go(d.data.redirect);
                return !1
            }

            for (x in d.data) {
                view.invoice[x] = d.data[x]
            }
            $scope.$parent.page_title = view.invoice.page_title;
            $scope.$parent.serial_number = view.invoice.factur_nr;
            $scope.$parent.type_title = view.invoice.type_title;
            $scope.$parent.nr_status = view.invoice.nr_status;
            helper.updateTitle($state.current.name, ' - ' + view.invoice.page_title + (view.invoice.factur_nr ? ' ' + view.invoice.factur_nr : ''));
            if (view.invoice.sent_date) {
                view.invoice.sent_date = new Date(view.invoice.sent_date)
            }
            if (view.invoice.invoice_date) {
                view.invoice.invoice_date = new Date(view.invoice.invoice_date)
            }
            if (view.invoice.payment_date) {
                view.invoice.payment_date = new Date(view.invoice.payment_date)
            }
            if (view.invoice.invoice_d_date) {
                view.invoice.invoice_d_date = new Date(view.invoice.invoice_d_date)
            }
            view.dateOptions.minDate = view.invoice.invoice_d_date;
            view.auto.options.recipients = view.invoice.recipients;
            view.email.e_subject = view.invoice.subject;
            view.email.e_message = view.invoice.e_message;
            view.email.email_language = view.invoice.languages;
            view.email.include_pdf = view.invoice.include_pdf;
            view.email.include_xml = view.invoice.include_xml;
            view.email.invoice_id = view.invoice_id;
            view.email.files = view.invoice.files_row;
            view.email.user_loged_email = view.invoice.user_loged_email;
            view.email.user_acc_email = view.invoice.user_acc_email;
            view.email.recipients = d.data.recipients_ids;
            view.email.sendgrid_selected = view.invoice.sendgrid_selected;
            view.save_disabled = d.data.save_disabled;
            if (!d.error) {
                view.showDrop()
            }
            if (!noAlert) {
                /*view.showAlerts(d)*/
            }
            angular.element('#invoicePreview center').show();
            angular.element('#bxWrapper').addClass('hide');
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            PDFJS.externalLinkTarget = PDFJS.LinkTarget.BLANK;
            PDFJS.getDocument(view.invoice.pdf_link).then(function(pdfDoc_) {
                pdfDoc = pdfDoc_;
                document.getElementById('page_count').textContent = '/ ' + pdfDoc.numPages;
                renderPdf(pageNum)
            });
        }
    }
    editItem.init(view);
    view.draftNext = function() {
        view.showTxt.markAsReady == !0 ? view.showTxt.markAsReady = !1 : view.showTxt.markAsReady = !0;
        view.showTxt.sendEmail = !1;
        view.showTxt.markAsSent = !1
    }
    view.sendEmail = function() {
        /*view.showTxt.sendEmail = !0;*/
        view.showTxt.markAsReady = !1;
        view.email.alerts = [];
        var params = {
            'do': 'invoice-email_preview',
            invoice_id: view.invoice_id,
            message: view.email.e_message,
            use_html: view.invoice.use_html
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            var viewEmail = angular.copy(view.email);
            viewEmail.e_message = r.data.e_message;
            var obj = {
                "app": "invoice",
                "DRAFT_INVOICE_NO_NUMBER": view.invoice.DRAFT_INVOICE_NO_NUMBER,
                "invoice_date": view.invoice.invoice_date,
                "recipients": view.auto.options.recipients,
                "check_acc_manager": view.invoice.check_acc_manager,
                "use_html": view.invoice.use_html,
                "xml_available": view.invoice.xml_available,
                "is_file": view.invoice.is_file,
                "dropbox": view.invoice.dropbox,
                "email": viewEmail
            };
            view.openModal("send_email", angular.copy(obj), "lg");
        })
    }
    view.cancelSendMail = function() {
        view.showTxt.sendEmail = !1;
        view.showTxt.markAsReady = !0
    }
    view.showAlerts = function(d, formObj) {
        helper.showAlerts(view, d, formObj)
    }
    view.saveData = function(formObj, func) {
        formObj.$invalid = !0;
        var obj = {};
        for (x in view.invoice) {
            obj[x] = view.invoice[x]
        }
        obj.do = 'invoice-invoice-invoice-' + func;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(d) {
            if (d.success) {
                if (func == 'add_nr') {
                    angular.element('.loading_wrap').removeClass('hidden');
                    $state.go('invoice.view', {
                        invoice_id: d.data.invoice_id
                    })
                } else {
                    view.showTxt = angular.copy(baseshowTxt);
                    view.invoice_id = d.data.invoice_id;
                    view.renderPage(d)
                }

            } else {
                if (func == 'mark_sent') {
                    view.showAlerts(d, formObj)
                }
            }
        })
    }
    view.myFunction = function() {
        if (view.email.recipients.length) {
            view.save_disabled = !1
        } else {
            view.save_disabled = !0
        }
    }
    view.sendEmails = function(r) {
        var d = angular.copy(r);

        if (d.recipients.length) {
            /*if (view.invoice.dropbox) {
                d.dropbox_files = view.invoice.dropbox.dropbox_files
            }*/
            view.showTxt.sendEmail = !1;
            d.new_email = d.recipients[0];
            if (d.new_email) {
                view.email.alerts.push({
                    type: 'email',
                    msg: d.new_email + ' - '
                });
                d.do = 'invoice-invoice-invoice-sendNewEmail';
                d.use_html = view.email.use_html;
                d.lid = d.email_language;
                d.invoice_id = view.invoice_id;
                d.serial_number = view.invoice.serial_number;
                d.c_invoice_id = view.invoice.c_invoice_id;
                d.typee = view.invoice.type;
                d.mark_as_sent = view.invoice.draft;
                d.i_date = view.invoice.i_date;
                helper.doRequest('post', 'index.php', d, function(r) {

                    if (r.success && r.success.success) {
                        view.email.alerts[view.email.alerts.length - 1].type = 'success';
                        view.email.alerts[view.email.alerts.length - 1].msg += ' ' + r.success.success
                    }
                    if (r.notice && r.notice.notice) {
                        view.email.alerts[view.email.alerts.length - 1].type = 'info';
                        view.email.alerts[view.email.alerts.length - 1].msg += ' ' + r.notice.notice
                    }
                    if (r.error && r.error.error) {
                        view.email.alerts[view.email.alerts.length - 1].type = 'danger';
                        view.email.alerts[view.email.alerts.length - 1].msg += ' ' + r.error.error
                    }
                    d.recipients.shift();
                    if (d.new_email == d.user_loged_email) {
                        d.copy = !1;
                    }
                    if (d.new_email == d.user_acc_email) {
                        d.copy_acc = !1;
                    }

                    d.many_email_addresses = 1;
                    view.sendEmails(d);
                    if (r.success) {
                        view.renderPage(r, !0)
                    }
                })
            } else {
                d.recipients.shift();
                view.sendEmails(d)
            }
        } else {
            if (d.user_loged_email && d.copy) {
                d.new_email = d.user_loged_email;
                d.copy = !1;
                d.mark_as_sent = 0;
                /*if (view.invoice.dropbox) {
                    d.dropbox_files = view.invoice.dropbox.dropbox_files
                }*/
                if (d.new_email) {
                    view.email.alerts.push({
                        type: 'email',
                        msg: d.new_email + ' - '
                    });
                    d.do = 'invoice-invoice-invoice-sendNewEmail';
                    d.use_html = view.email.use_html;
                    d.lid = d.email_language;
                    d.invoice_id = view.invoice_id;
                    d.serial_number = view.invoice.serial_number;
                    d.c_invoice_id = view.invoice.c_invoice_id;
                    d.typee = view.invoice.type;
                    d.mark_as_sent = 0;
                    d.i_date = view.invoice.i_date;
                    helper.doRequest('post', 'index.php', d, function(r) {
                        if (r.success && r.success.success) {
                            view.email.alerts[view.email.alerts.length - 1].type = 'success';
                            view.email.alerts[view.email.alerts.length - 1].msg += ' ' + r.success.success
                        }
                        if (r.notice && r.notice.notice) {
                            view.email.alerts[view.email.alerts.length - 1].type = 'info';
                            view.email.alerts[view.email.alerts.length - 1].msg += ' ' + r.notice.notice
                        }
                        if (r.error && r.error.error) {
                            view.email.alerts[view.email.alerts.length - 1].type = 'danger';
                            view.email.alerts[view.email.alerts.length - 1].msg += ' ' + r.error.error
                        }
                        //d.user_loged_email = null;
                        d.user_mail_sent = 1;
                        view.sendEmails(d);
                        if (r.success) {
                            view.renderPage(r, !0)
                        }
                    })
                }
            } else if (d.user_acc_email && d.copy_acc) {
                //console.log(d.user_mail_sent, d.user_acc_email, d.user_loged_email);
                if (d.user_mail_sent && d.user_acc_email == d.user_loged_email) {

                } else {
                    d.new_email = d.user_acc_email;
                    d.copy_acc = !1;
                    d.mark_as_sent = 0;
                    /*if (view.invoice.dropbox) {
                        d.dropbox_files = view.invoice.dropbox.dropbox_files
                    }*/
                    if (d.new_email) {
                        view.email.alerts.push({
                            type: 'email',
                            msg: d.new_email + ' - '
                        });
                        d.do = 'invoice-invoice-invoice-sendNewEmail';
                        d.use_html = view.email.use_html;
                        d.lid = d.email_language;
                        d.invoice_id = view.invoice_id;
                        d.serial_number = view.invoice.serial_number;
                        d.c_invoice_id = view.invoice.c_invoice_id;
                        d.typee = view.invoice.type;
                        d.mark_as_sent = 0;
                        d.i_date = view.invoice.i_date;
                        helper.doRequest('post', 'index.php', d, function(r) {
                            if (r.success && r.success.success) {
                                view.email.alerts[view.email.alerts.length - 1].type = 'success';
                                view.email.alerts[view.email.alerts.length - 1].msg += ' ' + r.success.success
                            }
                            if (r.notice && r.notice.notice) {
                                view.email.alerts[view.email.alerts.length - 1].type = 'info';
                                view.email.alerts[view.email.alerts.length - 1].msg += ' ' + r.notice.notice
                            }
                            if (r.error && r.error.error) {
                                view.email.alerts[view.email.alerts.length - 1].type = 'danger';
                                view.email.alerts[view.email.alerts.length - 1].msg += ' ' + r.error.error
                            }
                        })
                    }
                }

            } else {
                helper.refreshLogging();
            }

        }

    }
    view.showPreview = function(exp) {
        var params = {
            template: 'miscTemplate/modal',
            ctrl: 'viewRecepitCtrl',
            size: 'md',
            ctrlAs: 'vmMod',
            params: {
                'do': 'invoice-email_preview',
                invoice_id: view.invoice_id,
                message: exp.e_message,
                use_html: exp.use_html
            },
            request_type: 'post'
        }
        modalFc.open(params)
    };
    view.changeState = function(func, sent) {
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'invoice-invoice-invoice-' + func,
            invoice_id: view.invoice_id,
            type: view.invoice.type,
            credit_invoice_id: view.invoice.c_invoice_id,
            sent: sent
        }, function(o) {
            view.renderPage(o)
        })
    }
    view.bPaidFlow = function() {
        if (view.invoice.bPaid_reg) {
            view.openModal('bPaidOptions', '', 'md')
        } else {
            var data = {
                'lg': view.invoice.lg
            };
            view.openModal('bPaidInfo', data, 'md')
        }
    }
    view.showDrop = function() {
        view.show_load = !0;
        var data_drop = angular.copy(view.invoice.drop_info);
        data_drop.do = 'misc-dropbox_new';
        helper.doRequest('post', 'index.php', data_drop, function(r) {
            view.invoice.dropbox = r.data;
            view.show_load = !1
        })
    }
    view.deleteDropFile = function(index) {
        var data = angular.copy(view.invoice.dropbox.dropbox_files[index].delete_file_link);
        angular.element('.loading_wrap').removeClass('hidden');
        view.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            view.show_load = !1;
            /*view.showAlerts(r);*/
            if (r.success) {
                view.invoice.dropbox.nr--;
                view.invoice.dropbox.dropbox_files.splice(index, 1)
            }
        })
    }
    view.deleteDropImage = function(index) {
        var data = angular.copy(view.invoice.dropbox.dropbox_images[index].delete_file_link);
        view.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            view.show_load = !1;
            /*view.showAlerts(r);*/
            if (r.success) {
                view.invoice.dropbox.nr--;
                view.invoice.dropbox.dropbox_images.splice(index, 1)
            }
        })
    }
    view.openLightboxModal = function(index) {
        Lightbox.openModal(view.invoice.dropbox.dropbox_images, index)
    };
    view.uploadDoc = function(type) {
        var obj = angular.copy(view.invoice.drop_info);
        obj.type = type;
        view.openModal('uploadDoc', obj, 'lg')
    }
    view.bePaidStatus = function() {
        view.openModal('bpaid_status', view.invoice_id, 'lg')
    }
    view.edebexFlow = function() {
        view.openModal("edebex_sell", view.invoice_id, 'md')
    }
    view.edebexStatus = function() {
        view.openModal("edebex_status", view.invoice_id, 'md')
    }
    view.greenpost = function() {
        if (view.invoice.post_active) {
            view.showTxt.sendPost = !view.showTxt.sendPost
        } else {
            var data = {};
            data.invoice_id = view.invoice_id;
            view.openModal("post_register", data, "md")
        }
    }
    view.sendPost = function() {
        var data = {};
        data.do = 'invoice-invoice-invoice-postIt';
        data.id = view.invoice_id;
        data.type = view.invoice.type;
        data.c_invoice_id = view.invoice.c_invoice_id;
        data.related = 'view';
        if (view.invoice.stationary_id) {
            data.stationary_id = view.invoice.stationary_id
        }
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                view.renderPage(r)
            }
            if (r.error) {
                /*view.showAlerts(r)*/
            }
        })
    }
    view.resendGreenPost = function() {
        var data = {};
        data.do = 'invoice-invoice-invoice-postIt';
        data.id = view.invoice_id;
        data.type = view.invoice.type;
        data.c_invoice_id = view.invoice.c_invoice_id;
        data.post_library = angular.copy(view.invoice.post_library);
        data.related = 'view';
        view.openModal("resend_post", data, "md")
    }
    view.setSegment = function() {
        var obj = {
            'do': 'invoice--invoice-updateSegment',
            'id': view.invoice.invoice_id,
            'segment_id': view.invoice.segment_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                view.showTxt.segment = !1;
                view.invoice.segment = r.data.segment
            }
        })
    }
    view.setSource = function() {
        var obj = {
            'do': 'invoice--invoice-updateSource',
            'id': view.invoice.invoice_id,
            'source_id': view.invoice.source_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                view.showTxt.source = !1;
                view.invoice.source = r.data.source
            }
        })
    }
    view.setType = function() {
        var obj = {
            'do': 'invoice--invoice-updateType',
            'id': view.invoice.invoice_id,
            'type_id': view.invoice.type_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                view.showTxt.type = !1;
                view.invoice.xtype = r.data.xtype
            }
        })
    }
    view.deletePayment = function(link) {
        helper.doRequest('get', link, view.renderPage)
    }
    view.openPaymentModal = function(payment_id) {
        var action = 'invoice-editPaymentModal',
            params = {
                template: 'iTemplate/paymentModal',
                size: "md",
                ctrl: 'paymentModalCtrl',
                ctrlAs: 'editPay',
                params: {
                    'do': action,
                    invoice_id: view.invoice.invoice_id,
                    payment_id: payment_id
                },
                callback: function(data) {
                    if (data) {
                        view.renderPage(data);
                        /*if (data.data) {
                            view.invoice = data.data
                        }*/
                        /*view.showAlerts(data)*/
                    }
                }
            };
        modalFc.open(params);        
    }
    view.settingsPDF = function(type, item) {
        if (view.invoice.has_multiple_identities) {
            view.openModal(type, item)
        } else {
            var data = {
                'do': 'invoice-invoice-invoice-pdf_settings',
                logo: view.invoice.logo,
                pdf_layout: 1,
                invoice_id: item,
                identity_id: 0,
                type: view.invoice.type
            };
            helper.doRequest('post', 'index.php', data, function(d) {
                view.renderPage(d);
                view.showAlerts(d)
            })
        }
    }
    view.pushToStripe = function(id) {
        var obj = {
            'do': 'invoice--invoice-push_invoice_to_stripe',
            'invoice_id': id,
        };
        helper.doRequest('post', 'index.php', obj, function(d) {
            // console.log(d);
            if (d.success) {
                /*view.showAlerts(d);*/
            }
        })
    }

    function renderPdf(num) {
        pageRendering = !0;
        pdfDoc.getPage(num).then(function(page) {
            var viewport = page.getViewport(scale);
            canvas.height = viewport.height;
            canvas.width = viewport.width;
            var renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };
            var renderTask = page.render(renderContext);
            renderTask.promise.then(function() {
                pageRendering = !1;
                if (pageNumPending !== null) {
                    renderPdf(pageNumPending);
                    pageNumPending = null
                }
            });
            setupAnnotations(page, viewport, canvas, angular.element('#annotation-layer'));
        });
        document.getElementById('page_num').value = pageNum;
        $('#invoicePreview center').hide();
        $('#bxWrapper').removeClass('hide')
    }

    function setupAnnotations(page, viewport, canvas, annotationLayerDiv) {
        annotationLayerDiv.empty();
        var promise = page.getAnnotations().then(function(annotationsData) {
            // Canvas offset
            var canvas_offset = $(canvas).position();

            // Canvas height
            var canvas_height = $(canvas).get(0).height;

            // Canvas width
            var canvas_width = $(canvas).get(0).width;

            // CSS for annotation layer
            annotationLayerDiv.css({ left: canvas_offset.left + 'px', top: canvas_offset.top + 'px', height: canvas_height + 'px', width: canvas_width + 'px' });

            PDFJS.AnnotationLayer.render({
                viewport: viewport.clone({ dontFlip: true }),
                div: annotationLayerDiv[0],
                annotations: annotationsData,
                page: page
            });
        });
        return promise;
    }

    function queueRenderPdf(num) {
        if (pageRendering) {
            pageNumPending = num
        } else {
            renderPdf(num)
        }
    }

    function onPrevPage(e) {
        e.preventDefault();
        if (pageNum <= 1) {
            return
        }
        pageNum--;
        queueRenderPdf(pageNum)
    }
    var prev = document.getElementById('prev');
    if(prev){
        prev.addEventListener('click', onPrevPage);
    }

    function onNextPage(e) {
        e.preventDefault();
        if (pageNum >= pdfDoc.numPages) {
            return
        }
        pageNum++;
        queueRenderPdf(pageNum)
    }

    var next = document.getElementById('next');
    if(next){
        next.addEventListener('click', onNextPage);
    }

    view.statusInvoice = function(status) {
        var tmp_obj = { 'id': view.invoice.invoice_id, 'invoice_id': view.invoice.invoice_id };
        tmp_obj.do = status == '1' ? 'invoice-invoice-invoice-archive' : 'invoice-invoice-invoice-activate';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_obj, function(r) {
            if (r.success) {
                view.renderPage(r);
                helper.refreshLogging();
            } else {
                /*view.showAlerts(r);*/
            }
        });
    }

    view.deleteInvoice = function() {
        var tmp_obj = { 'id': view.invoice.invoice_id };
        tmp_obj.do = 'invoice-invoices-invoice-delete';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_obj, function(r) {
            if (r.success) {
                helper.setItem('invoice-invoices', {});
                $state.go("invoices");
            } else {
                view.showAlerts(r);
            }
        });
    }

    view.editInvoice = function() {
        var tmp_obj = { 'invoice_id': view.invoice.invoice_id };
        tmp_obj.do = 'invoice-invoices-invoice-delete_all_payments';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_obj, function(r) {
            if (r.success) {
                $state.go('invoice.edit', {
                    invoice_id: view.invoice.invoice_id
                })
            } else {
                view.showAlerts(r);
            }
        });
    }

    view.sendToJefacture=function(){
        var tmp_obj = { 'invoice_id': view.invoice.invoice_id, 'mark_as_sent':'1' };
        tmp_obj.do = 'invoice-invoice-invoice-export_jefacture';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_obj, function(r) {
            if (r.success) {
                view.renderPage(r);
                helper.refreshLogging();
            }
        });
    }

    view.sendToBilltoBox=function(){
        var tmp_obj = { 'invoice_id': view.invoice.invoice_id, 'mark_as_sent':'1' };
        tmp_obj.do = 'invoice-invoice-invoice-export_billtobox';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_obj, function(r) {
            if (r.success) {
                view.renderPage(r);
                helper.refreshLogging();
            }
        });
    }

    view.openModal = function(type, item, size) {
        switch (type) {
            case 'web_log':
                var iTemplate = 'WebLog';
                var ctas = 'weblog';
                break;
            case 'pdf_settings':
                var iTemplate = 'PdfSettings';
                var ctas = 'pdfset';
                var size = 'md';
                break;
            case 'recordPayment':
                var iTemplate = 'RecPay';
                var ctas = 'recpay';
                var size = 'md';
                break;
            case 'bPaidRegister':
                var iTemplate = 'BePaid';
                var ctas = 'bPay';
                break;
            case 'bpaid_status':
                var iTemplate = 'BePaidStatus';
                var ctas = 'bPay';
                break;
            case 'bPaidInfo':
                var iTemplate = 'BePaidInfo';
                var ctas = 'bPay';
                break;
            case 'bPaidOptions':
                var iTemplate = 'BePaidOptions';
                var ctas = 'bPay';
                break;
            case 'post_register':
                var iTemplate = 'PostRegister';
                var ctas = 'post';
                break;
            case 'edebex_sell':
                var iTemplate = 'EdebexSell';
                var ctas = 'edb';
                break;
            case 'edebex_status':
                var iTemplate = 'EdebexStatus';
                var ctas = 'edb';
                break;
            case 'resend_post':
                var iTemplate = 'ResendPostGreen';
                var ctas = 'post';
                break;
            case 'uploadDoc':
                var iTemplate = 'AmazonUpload';
                break;
            case 'send_email':
                var iTemplate = 'SendEmail';
                var ctas = 'vm';
                break;
        }
        var params = {
            template: 'iTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'web_log':
                params.params = {
                    'do': 'invoice-' + type,
                    invoice_id: item
                };
                break;
            case 'recordPayment':
            case 'pdf_settings':
                params.params = {
                    'do': 'invoice-' + type,
                    invoice_id: item
                };
                params.callback = function(data) {
                    if (data) {
                        view.renderPage(data);
                        /*if (data.data) {
                            view.invoice = data.data
                        }*/
                        view.showAlerts(data)
                    }
                }
                break;
            case 'bPaidRegister':
                params.item = angular.copy(view.invoice);
                params.item.do = 'invoice-invoice-invoice-bpay_register';
                params.callback = function(data) {
                    if (data) {
                        if (data.data) {
                            view.invoice = data.data;
                            view.openModal('bPaidOptions', '', 'md')
                        }
                        /*view.showAlerts(data)*/
                    }
                }
                break;
            case 'bpaid_status':
                params.params = {
                    'do': 'invoice--invoice-bePaidData',
                    invoice_id: item
                };
                break;
            case 'bPaidInfo':
                params.item = item;
                params.callback = function(data) {
                    if (data) {
                        view.openModal('bPaidRegister', '', 'md')
                    }
                }
                break;
            case 'bPaidOptions':
                params.item = angular.copy(view.invoice);
                params.item.do = 'invoice-invoice-invoice-bPaid_notification';
                params.callback = function(data) {
                    if (data && data.data) {
                        view.showTxt = angular.copy(baseshowTxt);
                        view.invoice = data.data
                    }
                }
                break;
            case 'post_register':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.item.do = 'invoice-invoice-invoice-postRegister';
                params.callback = function(data) {
                    if (data && data.data) {
                        view.renderPage(data);
                        view.greenpost()
                    }
                    /*view.showAlerts(data)*/
                }
                break;
            case 'edebex_sell':
                params.params = {
                    'do': 'invoice--edebex-offer',
                    invoice_id: item
                };
                params.callback = function() {
                    helper.doRequest('get', 'index.php?do=invoice-invoice', {
                        invoice_id: view.invoice_id
                    }, view.renderPage)
                }
                break;
            case 'edebex_status':
                params.params = {
                    'do': 'invoice--edebex-status',
                    invoice_id: item
                };
                break;
            case 'resend_post':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.request_type = 'post';
                params.params = {
                    'do': 'invoice--invoice-postInvoiceData',
                    'invoice_id': view.invoice_id,
                    'related': 'status',
                    'old_obj': item
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        view.renderPage(d)
                    }
                }
                break;
            case 'uploadDoc':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        view.showDrop()
                    }
                }
                break;
            case 'send_email':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        view.sendEmails(d);
                    }
                }
                break;
        }
        modalFc.open(params)
    }
    helper.doRequest('get', 'index.php?do=invoice-invoice', { 'invoice_id': $stateParams.invoice_id }, view.renderPage);
    //view.renderPage(data)
}]).controller('paymentModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', '$compile', '$uibModal', 'modalFc', function($scope, helper, $uibModalInstance, data, $compile, $uibModal, modalFc) {
    var editPay = this;
    var date = new Date();
    editPay.format = 'dd/MM/yyyy';
    editPay.datePickOpen = {
        payment_date: !1
    };
    editPay.dateOptions = {
        dateFormat: '',
    };
    if (data.payment_date) {
        data.payment_date = data.payment_date = new Date(data.payment_date)
    }
    editPay.obj = data;
    editPay.checkbox = function(formObj) {
        var data = angular.copy(editPay.obj);
        if (data.not_paid == !1) {
            return !1
        }
        data.do = 'invoice-invoice-invoice-edit_payment_invoice';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                editPay.showAlerts(r, formObj)
            } else {
                $uibModalInstance.close(r)
            }
        })
    };
    editPay.ok = function(formObj) {
        var data = angular.copy(editPay.obj);
        data.do = 'invoice-invoice-invoice-edit_payment_invoice';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                editPay.showAlerts(r, formObj)
            } else {
                $uibModalInstance.close(r)
            }
        })
    };
    editPay.cancel = function() {
        $uibModalInstance.close()
    };
    editPay.showAlerts = function(d, formObj) {
        helper.showAlerts(editPay, d, formObj)
    }
}]).controller('WebLogModalCtrl', ['$scope', 'helper', '$uibModalInstance', '$timeout', 'data', 'modalFc', function($scope, helper, $uibModalInstance, $timeout, data, modalFc) {
    var weblog = this;
    weblog.list = [];
    weblog.max_rows = data.max_rows;
    weblog.list = data.weblog_row;
    weblog.lr = data.lr;
    weblog.is_data = weblog.max_rows > 0 ? !0 : !1;
    weblog.alerts = [{
        type: 'danger'
    }];
    weblog.search = {
        offset: 1
    };
    $timeout(function() {
        modalFc.setHeight(angular.element('.modal.in'))
    });
    $scope.renderList = function(d) {
        if (d.data.query) {
            weblog.list = d.data.query;
            weblog.lr = d.data.lr;
            weblog.max_rows = d.data.max_rows;
            modalFc.setHeight(angular.element('.modal.in'))
        }
    };
    weblog.cancel = function() {
        $uibModalInstance.close()
    }
    weblog.closeAlert = function(index) {
        weblog.alerts.splice(index, 1)
    }
    weblog.searchThing = function() {}
}]).controller('PdfSettingsModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var pdfset = this;
    pdfset.obj = data;
    pdfset.ok = function() {
        var data = {
            'do': pdfset.obj.do_next,
            logo: pdfset.obj.default_logo,
            pdf_layout: pdfset.obj.pdf_type_id,
            invoice_id: pdfset.obj.invoice_id,
            identity_id: pdfset.obj.multiple_identity_id,
            type: pdfset.obj.type
        };
        helper.doRequest('post', 'index.php', data, function(d) {
            pdfset.closeModal(d)
        })
    };
    pdfset.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    pdfset.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('RecPayModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var recpay = this;
    var date = new Date();
    recpay.format = 'dd/MM/yyyy';
    recpay.datePickOpen = {
        payment_date: !1
    };
    recpay.dateOptions = {
        dateFormat: '',
    };
    if (data.payment_date) {
        data.payment_date = data.payment_date = new Date(data.payment_date)
    }
    recpay.obj = data;
    recpay.checkbox = function(formObj) {
        var data = angular.copy(recpay.obj);
        if (data.not_paid == !1) {
            return !1
        }
        data.do = 'invoice-invoice-invoice-pay';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                recpay.showAlerts(r, formObj)
            } else {
                $uibModalInstance.close(r)
            }
        })
    };
    recpay.ok = function(formObj) {
        angular.element('.loading_alt').removeClass('hidden');
        var data = angular.copy(recpay.obj);
        data.do = 'invoice-invoice-invoice-pay';
        helper.doRequest('post', 'index.php', data, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            if (r.error) {
                recpay.showAlerts(r, formObj)
            } else {
                $uibModalInstance.close(r)
            }
        })
    };
    recpay.cancel = function() {
        $uibModalInstance.close()
    };
    recpay.showAlerts = function(d, formObj) {
        helper.showAlerts(recpay, d, formObj)
    }

    recpay.ciCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        onChange(value) {
            if (value && this.options[value].date) {
                recpay.obj.payment_date = new Date(this.options[value].date);
                recpay.obj.total_amount_due1 = this.options[value].amount;
            }
        },
        maxItems: 1
    };

}]).controller('BePaidModalCtrl', ['$scope', 'helper', '$timeout', '$uibModalInstance', 'data', 'modalFc', function($scope, helper, $timeout, $uibModalInstance, data, modalFc) {
    var bPay = this;
    bPay.obj = data;
    bPay.closeAlert = function(index) {
        bPay.alerts.splice(index, 1)
    }
    bPay.showAlerts = function(d, form) {
        helper.showAlerts(bPay, d, form)
    }
    bPay.cancel = function() {
        $uibModalInstance.close()
    }
    bPay.ok = function(form) {
        var data = angular.copy(bPay.obj);
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            if (r.error) {
                bPay.showAlerts(r, form)
            } else {
                bPay.showAlerts(r, form);
                $timeout(function() {
                    if (bPay.obj.index || bPay.obj.index == 0) {
                        r.index = bPay.obj.index
                    }
                    if (bPay.obj.sub_index || bPay.obj.sub_index == 0) {
                        r.sub_index = bPay.obj.sub_index
                    }
                    $uibModalInstance.close(r)
                }, '1500')
            }
        })
    }
}]).controller('BePaidStatusModalCtrl', ['$scope', '$sce', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, $sce, helper, $uibModalInstance, data, modalFc) {
    var bPay = this;
    bPay.obj = data;
    bPay.obj.link = $sce.trustAsResourceUrl(bPay.obj.bp_link);
    bPay.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('BePaidInfoModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, helper, $uibModalInstance, data, modalFc) {
    var bPay = this;
    bPay.obj = {};
    bPay.obj.accept = !1;
    bPay.obj.lg = data.lg;
    bPay.cancel = function() {
        $uibModalInstance.dismiss()
    }
    bPay.ok = function() {
        var o = (data.index || data.index == 0) ? data : 1;
        $uibModalInstance.close(o)
    }
}]).controller('BePaidOptionsModalCtrl', ['$scope', '$compile', '$sce', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, $compile, $sce, helper, $uibModalInstance, data, modalFc) {
    var bPay = this;
    bPay.obj = data;
    var response = {};
    bPay.closeAlert = function(index) {
        bPay.alerts.splice(index, 1)
    }
    bPay.showAlerts = function(d) {
        helper.showAlerts(bPay, d)
    }
    bPay.cancel = function() {
        $uibModalInstance.close(response)
    }
    bPay.ok = function() {
        var data = angular.copy(bPay.obj);
        data.bPaid_type = bPay.obj.letter ? 1 : 2;
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            bPay.showAlerts(r);
            if (r.success) {
                response = r;
                if (bPay.obj.index || bPay.obj.index == 0) {
                    response.index = bPay.obj.index
                }
                if (r.data.bPaid_modUrl) {
                    angular.element('.bepaid_options').empty();
                    angular.element('.bepaid_btn').empty();
                    var s_link = $sce.trustAsResourceUrl(r.data.bPaid_modUrl);
                    var new_line = '<iframe class="fluid-iframe" src="' + s_link + '"></iframe>';
                    angular.element('.bepaid_options').append(new_line);
                    var close_btn = '<button class="btn btn-plain" type="button" ng-click="bPay.cancel()">' + helper.getLanguage('Close') + '</button>';
                    angular.element('.modal-dialog').removeClass('modal-md').addClass('modal-lg');
                    angular.element('.bepaid_btn').append($compile(close_btn)($scope))
                }
            }
        })
    }
}]).controller('EdebexSellModalCtrl', ['$scope', '$sce', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, $sce, helper, $uibModalInstance, data, modalFc) {
    var edb = this;
    edb.obj = data;
    edb.obj.accept = !1;
    edb.showAlerts = function(d) {
        helper.showAlerts(edb, d)
    }
    if (edb.obj.error) {
        edb.showAlerts(edb.obj)
    }
    edb.cancel = function() {
        $uibModalInstance.close()
    }
    edb.ok = function() {
        var data = {
            'do': edb.obj.do_next,
            'invoice_id': edb.obj.invoice_id
        };
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(o) {
            angular.element('.loading_alt').addClass('hidden');
            if (o.error) {
                edb.showAlerts(o)
            } else {
                edb.obj.redirect = o.data.redirect
            }
        })
    }
}]).controller('EdebexStatusModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, helper, $uibModalInstance, data, modalFc) {
    var edb = this;
    edb.obj = data;
    edb.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('InvoiceEditCtrl', ['$scope', '$rootScope','$stateParams', '$cacheFactory', '$state', '$timeout', 'editItem', '$confirm', 'modalFc', 'helper', 'selectizeFc', function($scope, $rootScope, $stateParams, $cacheFactory, $state, $timeout, editItem, $confirm, modalFc, helper, selectizeFc) {
    console.log('invoiceEdit');
    var edit = this,
        typePromise;
    edit.app = 'invoice';
    edit.ctrlpag = 'ninvoice';
    edit.tmpl = 'iTemplate';
    $scope.vm = {};
    $scope.vm.isAddPage = !1;
    edit.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    edit.currencyCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            edit.changeCurrency()
        }
    });
    edit.paymentTypeTermCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        persist: false,
        onChange(value) {
            if(value){
                edit.setDueDate();
            }
        },
        onDelete(values){
            return false;
        }
    });

    var cache = $cacheFactory.get('ProjectToInvoice');
    if (cache != undefined) {
        var cachedData = cache.get('data')
    }
    var cacheService = $cacheFactory.get('ServiceToInvoice');
    if (cacheService != undefined) {
        var cachedDataService = cacheService.get('data')
    }
    var cacheOrder = $cacheFactory.get('OrderToInvoice');
    if (cacheOrder != undefined) {
        var cachedDataOrder = cacheOrder.get('data')
    }
    var cacheContract = $cacheFactory.get('ContractToInvoice');
    if (cacheContract != undefined) {
        var cachedDataContract = cacheContract.get('data')
    }
    var cacheDeal = $cacheFactory.get('DealToDocument');
    if (cacheDeal != undefined) {
        var cachedDataDeal = cacheDeal.get('data')
    }
    edit.item_id = $stateParams.invoice_id;
    edit.invoice_id = edit.item_id;
    edit.duplicate_invoice_id = $stateParams.duplicate_invoice_id;
    edit.buyer_id = $stateParams.buyer_id;
    edit.c_invoice_id = $stateParams.c_invoice_id;
    edit.type = $stateParams.type;
    edit.a_invoice_id = $stateParams.a_invoice_id;
    edit.base_type = $stateParams.base_type;
    edit.languages = $stateParams.languages;
    edit.c_quote_id = $stateParams.c_quote_id;
    edit.a_quote_id = $stateParams.a_quote_id;
    edit.track_base = $stateParams.track_base;
    edit.your_ref = $stateParams.your_ref;
    edit.copy_free_text = $stateParams.copy_free_text;
    edit.hide_content = !0;
    edit.item = {};
    edit.oldObj = {};
    edit.item.validity_date = new Date();
    edit.removedCustomer = !1;
    //edit.item.add_customer = (edit.item_id != 'tmp' ? !0 : !1);
    edit.item.add_customer = !1;
    edit.adjustP = [];
    edit.showTxt = {
        invoice_settings: !1,
        addBtn: !0,
        address: !1,
        invoice_notes: !1,
        addArticleBtn: !1,
    }
    edit.contactAddObj = {};
    edit.auto = {
        options: {
            contacts: [],
            addresses: [],
            cc: []
        }
    };
    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {
        validity_date: !1,
        invoice_date: !1,
        invoice_due_date_vat: !1,
        invoice_due: !1,
    }
    edit.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    edit.refreshList = function() {
        var data = {
            'do': 'invoice-ninvoice',
            xget: 'articles_list',
            search: '',
            customer_id: edit.item.buyer_id,
            lang_id: edit.item.email_language,
            cat_id: edit.item.price_category_id,
            remove_vat: edit.item.remove_vat,
            vat_regime_id: edit.item.vat_regime_id
        };
        edit.requestAuto(edit, data, edit.renderArticleListAuto)
    }

    edit.selectCategoryCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        maxItems: 1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value) {
                $confirm({
                    text: helper.getLanguage('Recalculate prices')
                }).then(function() {
                    var data = {};
                    for (x in edit.item) {
                        data[x] = edit.item[x]
                    }
                    data['do'] = data.invoice_id != 'tmp' ? 'invoice-ninvoice-invoice-updatePriceCategory' : ''
                    data.changeCatPrices = '1';
                    data.item_id = data.invoice_id;

                    helper.doRequest("post", 'index.php', data, function(r) {
                        if (r.success) {
                            edit.renderPage(r);
                        }
                    })

                }, function() {
                    // console.log('test');

                })
            }
        }
    };

    edit.vatregcfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onDropdownOpen($dropdown) {
            var _this = this;
            edit.item.old_vat_regime_id = _this.$input[0].attributes.value.value;
        },
        onChange(value) {
            if (value) {
                var selectize = angular.element('#vat_regime_select')[0].selectize;
                selectize.blur();
                var _this = this;
                $confirm({
                    ok: helper.getLanguage('Yes'),
                    cancel: helper.getLanguage('No'),
                    text: helper.getLanguage('Apply VAT Regime on all lines')
                }).then(function() {
                    if (_this.options[value].no_vat) {
                        edit.item.vat = helper.displayNr(0);
                        edit.item.block_vat = !1;
                        edit.item.vat_regular = !1;
                        if (edit.item.no_vat == !0) {
                            edit.changeVat()
                        }
                    } else {
                        if (_this.options[value].regime_type == '2') {
                            edit.item.vat = helper.displayNr(0);
                            edit.item.block_vat = !0;
                            for (x in edit.item.vat_line) {
                                edit.item.vat_line[x].vat_percent = helper.displayNr(0)
                            }
                            for (x in edit.item.invoice_line) {
                                edit.item.invoice_line[x].vat = helper.displayNr(0)
                            }
                            edit.calcInvoice();
                            if (edit.item.no_vat == !1) {
                                edit.changeVat()
                            }
                        } else if (_this.options[value].regime_type == '3') {
                            edit.item.vat = helper.displayNr(_this.options[value].value);
                            edit.item.block_vat = _this.options[value].regime_id == '4' ? !0 : !1;

                            /*if(_this.options[value].name == helper.getLanguage('Contracting Partner')){
                                edit.item.block_vat = !0;
                            } else {
                                edit.item.block_vat = !1;
                            } */

                            for (x in edit.item.vat_line) {
                                edit.item.vat_line[x].vat_percent = helper.displayNr(_this.options[value].value)
                            }
                            for (x in edit.item.invoice_line) {
                                edit.item.invoice_line[x].vat = helper.displayNr(_this.options[value].value)
                            }
                            edit.calcInvoice();
                            if (edit.item.no_vat == !1) {
                                edit.changeVat()
                            }
                        } else {
                            edit.item.vat = helper.displayNr(_this.options[value].value);
                            edit.item.block_vat = !1;
                            if (edit.item.no_vat == !1) {
                                edit.changeVat()
                            }
                        }
                        if (_this.options[value].regime_type == '1') {
                            edit.item.vat_regular = !0
                        } else {
                            edit.item.vat_regular = !1
                        }
                    }
                    edit.changeLang();
                    edit.vatApplyAll();
                    edit.refreshList();
                }, function(e) {
                    if (e == 'cancel') {
                        if (_this.options[value].no_vat) {
                            edit.item.vat = helper.displayNr(0);
                            edit.item.block_vat = !1;
                            edit.item.vat_regular = !1;
                            if (edit.item.no_vat == !0) {
                                edit.changeVat()
                            }
                        } else {
                            if (_this.options[value].regime_type == '2') {
                                edit.item.vat = helper.displayNr(0);
                                edit.item.block_vat = !0;
                                for (x in edit.item.vat_line) {
                                    edit.item.vat_line[x].vat_percent = helper.displayNr(0)
                                }
                                for (x in edit.item.invoice_line) {
                                    edit.item.invoice_line[x].vat = helper.displayNr(0)
                                }
                                edit.calcInvoice();
                                if (edit.item.no_vat == !1) {
                                    edit.changeVat()
                                }
                            } else if (_this.options[value].regime_type == '3') {
                                edit.item.vat = helper.displayNr(_this.options[value].value);
                                edit.item.block_vat = _this.options[value].regime_id == '4' ? !0 : !1;

                                /*if(_this.options[value].name == helper.getLanguage('Contracting Partner')){
                                    edit.item.block_vat = !0;
                                } else {
                                    edit.item.block_vat = !1;
                                } */

                                for (x in edit.item.vat_line) {
                                    edit.item.vat_line[x].vat_percent = helper.displayNr(_this.options[value].value)
                                }
                                for (x in edit.item.invoice_line) {
                                    edit.item.invoice_line[x].vat = helper.displayNr(_this.options[value].value)
                                }
                                edit.calcInvoice();
                                if (edit.item.no_vat == !1) {
                                    edit.changeVat()
                                }
                            } else {
                                edit.item.vat = helper.displayNr(_this.options[value].value);
                                edit.item.block_vat = !1;
                                if (edit.item.no_vat == !1) {
                                    edit.changeVat()
                                }
                            }
                            if (_this.options[value].regime_type == '1') {
                                edit.item.vat_regular = !0
                            } else {
                                edit.item.vat_regular = !1
                            }
                        }
                        edit.changeLang();
                        edit.refreshList();
                    } else if (e == 'dismiss') {
                        selectize.addItem(edit.item.old_vat_regime_id, true);
                    }
                });
            } else {
                edit.item.vat = helper.displayNr(0);
                edit.item.block_vat = !1;
                edit.item.vat_regular = !0;
                if (edit.item.no_vat == !1) {
                    edit.changeVat()
                }
                edit.refreshList()
            }
        }
    });
    edit.linevatcfg = {
        valueField: 'vat',
        labelField: 'name',
        searchField: ['vat'],
        delimiter: '|',
        placeholder: helper.getLanguage('VAT'),
        create: !1,
        maxOptions: 100,
        onDelete(values) {
            return !1
        },
        closeAfterSelect: !0,
        maxItems: 1
    };
    edit.articlesListAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select article'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.family) {
                    item.family = "/ " + item.family
                }
                if (item.allow_stock && item.is_service == 0) {
                    var stock_display = '';
                    if (item.hide_stock == 0) {
                        stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">' + helper.getLanguage('Stock') + ' <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                    }
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + stock_display + '</div>' + '</div>';
                } else {
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-right">' + (item.base_price) + '</div>' + '</div>' + '</div>'
                }
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'invoice-ninvoice',
                    xget: 'articles_list',
                    search: str,
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    //cat_id: edit.item.price_category_id,
                    cat_id: edit.item.cat_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id
                };
                edit.requestAuto(edit, data, edit.renderArticleListAuto)
            }, 300)
        },
        onChange(value) {
            if (value == undefined) {} else if (value == '99999999999') {
                edit.openModal(edit, 'AddAnArticle', {
                    'do': 'invoice-addAnArticle',
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    //cat_id: edit.item.price_category_id,
                    cat_id: edit.item.cat_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id
                }, 'md');
                return !1
            } else {
                for (x in edit.auto.options.articles_list) {
                    if (edit.auto.options.articles_list[x].article_id == value) {
                        editItem.addInvoiceArticle(edit, edit.auto.options.articles_list[x]);
                        edit.selected_article_id = '';
                        edit.showTxt.addArticleBtn = !1;
                        var data = {
                            'do': 'invoice-ninvoice',
                            xget: 'articles_list',
                            customer_id: edit.item.buyer_id,
                            lang_id: edit.item.email_language,
                            //cat_id: edit.item.price_category_id,
                            cat_id: edit.item.cat_id,
                            remove_vat: edit.item.remove_vat,
                            vat_regime_id: edit.item.vat_regime_id
                        };
                        edit.requestAuto(edit, data, edit.renderArticleListAuto);
                        return !1
                    }
                }
            }
        },
        onItemAdd(value, $item) {
            if (value == '99999999999') {
                edit.selected_article_id = ''
            }
        },
        onBlur() {
            $timeout(function() {
                edit.showTxt.addArticleBtn = !1
            })
        },
        maxItems: 1
    };

    edit.parentVariantCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select article'),
        maxOptions: 5,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.family) {
                    item.family = "/ " + item.family
                }
                var stock_display = '';
                if (item.hide_stock == 0) {
                    stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">' + helper.getLanguage('Stock') + ' <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + stock_display + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onFocus() {
            var selectize = this.$input[0].selectize;
            var index = this.$input[0].attributes['selectindex'].value;
            var parentVariant = this.$input[0].attributes['selectarticle'].value;
            var tmp_data = {
                'do': 'misc-articleVariants',
                'parentVariant': parentVariant,
                buyer_id: edit.item.buyer_id,
                lang_id: edit.item.email_language,
                cat_id: edit.item.price_category_id,
                remove_vat: edit.item.remove_vat,
                vat_regime_id: edit.item.vat_regime_id,
                app: edit.app
            };
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', tmp_data, function(r) {
                edit.item.invoice_line[index].variantsOpts = r.data.lines;
                if (edit.item.invoice_line[index].variantsOpts.length) {
                    selectize.open();
                }
            });
        },
        onType(str) {
            var selectize = this.$input[0].selectize;
            var index = this.$input[0].attributes['selectindex'].value;
            var parentVariant = this.$input[0].attributes['selectarticle'].value;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do': 'misc-articleVariants',
                    'parentVariant': parentVariant,
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id,
                    app: edit.app
                };
                helper.doRequest('get', 'index.php', tmp_data, function(r) {
                    edit.item.invoice_line[index].variantsOpts = r.data.lines;
                    if (edit.item.invoice_line[index].variantsOpts.length) {
                        selectize.open();
                    }
                });
            }, 300)
        },
        onChange(value) {
            var index = this.$input[0].attributes['selectindex'].value;
            if (value != undefined) {
                var collection = edit.item.invoice_line[index].variantsOpts;
                for (x in collection) {
                    if (collection[x].article_id == value) {
                        edit.item.invoice_line[index].has_variants_done = '1';
                        collection[x].is_variant_for_line = edit.item.invoice_line[index].tr_id;
                        editItem.addInvoiceArticle(edit, collection[x], parseInt(index) + 1);
                        break;
                    }
                }
            }
        },
        onBlur() {
            var index = this.$input[0].attributes['selectindex'].value;
            var parentVariant = this.$input[0].attributes['selectarticle'].value;
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do': 'misc-articleVariants',
                    'parentVariant': parentVariant,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id,
                    app: edit.app
                };
                helper.doRequest('get', 'index.php', tmp_data, function(r) {
                    edit.item.invoice_line[index].variantsOpts = r.data.lines;
                });
            }, 300)
        },
        maxItems: 1
    };

    edit.childVariantCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select article'),
        maxOptions: 5,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.family) {
                    item.family = "/ " + item.family
                }
                var stock_display = '';
                if (item.hide_stock == 0) {
                    stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">' + helper.getLanguage('Stock') + ' <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + stock_display + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onType(str) {
            var selectize = this.$input[0].selectize;
            var index = this.$input[0].attributes['selectindex'].value;
            var parentVariant = this.$input[0].attributes['selectarticle'].value;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do': 'misc-articleVariants',
                    'parentVariant': parentVariant,
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id,
                    app: edit.app
                };
                helper.doRequest('get', 'index.php', tmp_data, function(r) {
                    edit.item.invoice_line[index].variantsOpts = r.data.lines;
                    if (edit.item.invoice_line[index].variantsOpts.length) {
                        selectize.open();
                    }
                });
            }, 300)
        },
        onChange(value) {
            var index = this.$input[0].attributes['selectindex'].value;
            if (value != undefined) {
                var tmp_data = angular.copy(edit.item.invoice_line[index].variantsOpts);
                for (x in tmp_data) {
                    if (tmp_data[x].article_id == value) {
                        var tmp_selected_item = angular.copy(tmp_data[x]);
                        tmp_selected_item.quantity = parseFloat(helper.return_value(edit.item.invoice_line[index].quantity, 10));
                        edit.removeLine(index);
                        editItem.addInvoiceArticle(edit, tmp_selected_item, index);
                        break;
                    }
                }
            }
        },
        onBlur() {
            var index = this.$input[0].attributes['selectindex'].value;
            edit.item.invoice_line[index].showVariants = false;
        },
        maxItems: 1
    };

    edit.dealAutoCfg = {
        valueField: 'deal_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Deal'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.name) + ' </span>' + '</div>' + '</div>' + '</div>';
                if (item.deal_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Add Deal') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined || value == '') {
                edit.item.old_deal_id = value
            } else if (value == '99999999999') {
                openModal('create_deal', {}, 'lg')
            } else {
                edit.item.old_deal_id = value
            }
        },
        maxItems: 1
    };
    edit.renderPage = renderPage;
    edit.openDate = openDate;
    edit.showEditFnc = showEditFnc;
    edit.showCCSelectize = showCCSelectize;
    edit.cancelEdit = cancelEdit;
    edit.removeDeliveryAddr = removeDeliveryAddr;
    edit.removeLine = removeLine;
    edit.removeLineSpecial = removeLineSpecial;
    edit.addArticle = addArticle;
    edit.changeCurrency = changeCurrency;
    edit.initial = initial;
    edit.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 850,
        height: 300,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    edit.tinymceOptionsLines = {
        selector: "",
        resize: "both",
        auto_resize: !0,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code autoresize"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        autoresize_bottom_margin: 0,
        setup: function(editor) {
            editor.on('click', function(e) {
                e.stopPropagation();
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                angular.element(editor.editorContainer).parent().removeClass('wysiwyg-hide')
            }).on('blur', function(e) {
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                edit.delaySave()
            })
        },
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
        content_css: [helper.base_url + 'css/tinymce_styles.css']
    };
    edit.delaySave = function(showLoading, reload) {
        angular.element('.keep-open-on-click').on('click', function(e) {
            console.log('ccc');
            e.stopPropagation()
        });
        if (edit.typePromise) {
            console.log('ccc1');
            $timeout.cancel(edit.typePromise)
        }
        edit.typePromise = $timeout(function() {
            console.log('ccc2');
            //edit.save(showLoading, reload)
        }, 2000)
    }

    editItem.init(edit);

    function renderPage(d) {
        if (d && d.data) {
            edit.item = d.data;
            //console.log(edit.item.page_title,edit.item.type_title, edit.item.nr_status);
            $scope.$parent.page_title = edit.item.page_title;
            if (edit.item.type_title) {
                $scope.$parent.type_title = edit.item.type_title;
            }
            if (edit.item.type_title) {
                $scope.$parent.nr_status = edit.item.nr_status;
            }

            $scope.$parent.serial_number = edit.item.serial_number;
            helper.updateTitle($state.current.name, ' - ' + edit.item.page_title + (edit.item.serial_number ? ' ' + edit.item.serial_number : ''));
            if (edit.item.validity_date) {
                edit.item.validity_date = new Date(edit.item.validity_date)
            }
            if (edit.item.invoice_date) {
                edit.item.invoice_date = new Date(edit.item.invoice_date)
            }
            if (edit.item.invoice_due_date_vat) {
                edit.item.invoice_due_date_vat = new Date(edit.item.invoice_due_date_vat)
            }
            if (edit.item.invoice_due) {
                edit.item.invoice_due = new Date(edit.item.invoice_due)
            }
            if (edit.item.invoice_due_date) {
                edit.item.invoice_due_date = new Date(edit.item.invoice_due_date)
            }
            if (edit.item.quote_ts) {
                edit.item.quote_ts = new Date(edit.item.quote_ts)
            }
            if (edit.item.due_date_vat) {
                edit.item.due_date_vat = new Date(edit.item.due_date_vat)
            }
            edit.auto.options.contacts = d.data.contacts;
            edit.auto.options.addresses = d.data.addresses;
            edit.auto.options.cc = d.data.cc;
            edit.auto.options.articles_list = d.data.articles_list && d.data.articles_list.lines ? d.data.articles_list.lines : [];
            edit.auto.options.invoices = d.data.invoices_list;
            edit.contactAddObj = {
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                language_dd: edit.item.language_dd,
                language_id: edit.item.language_id,
                gender_dd: edit.item.gender_dd,
                gender_id: edit.item.gender_id,
                title_dd: edit.item.title_dd,
                title_id: edit.item.title_id,
                add_contact: !0,
                buyer_id: edit.item.buyer_id,
                article: edit.item.tr_id,
                'do': 'invoice-ninvoice-invoice-tryAddC',
                identity_id: edit.item.identity_id,
                contact_only: !0,
                item_id: edit.item_id
            }
            if (edit.item.type == 3 && edit.item.invoice_line.length) {
                edit.hide_content = !1
            }
            if (((edit.item_id == 'tmp' && edit.old_buyer_id != d.data.buyer_id) || (edit.item_id != 'tmp' && edit.old_buyer_id != undefined && edit.old_buyer_id != d.data.buyer_id)) && d.data.customer_credit_limit) {
                var msg = "<b>" + helper.getLanguage("") + "</b> <span>" + helper.getLanguage("This client is over his credit limit of") + " " + d.data.customer_credit_limit + "</span>";
                var cont = {
                    "e_message": msg,
                    'e_title': helper.getLanguage('Credit warning')
                };
                openModal("stock_info", cont, "md")
            }
        }
    }

    function openDate(p) {
        edit.datePickOpen[p] = !0
    }

    function removeDeliveryAddr() {
        edit.item.delivery_address = '';
        edit.item.delivery_address_id = ''
    }

    function cancelEdit() {
        if (Object.keys(edit.oldObj).length == 0) {
            return edit.removeCust(edit)
        }
        edit.showTxt.address = !0;
        for (x in edit.oldObj) {
            if (edit.item[x]) {
                if (x == 'delivery_address') {
                    edit.item[x] = edit.oldObj[x].trim()
                } else {
                    edit.item[x] = edit.oldObj[x]
                }
            }
        }
        edit.item.buyer_id = edit.oldObj.buyer_id
    }

    function showEditFnc() {
        edit.showTxt.address = !1;
        edit.item.save_final = !0;
        edit.oldObj = {
            buyer_id: angular.copy(edit.item.buyer_id),
            contact_id: angular.copy(edit.item.contact_id),
            contact_name: angular.copy(edit.item.contact_name),
            BUYER_NAME: angular.copy(edit.item.buyer_name),
            delivery_address: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address)),
            delivery_address_id: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_id)),
            delivery_address_txt: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_txt)),
            view_delivery: (edit.item.sameAddress ? !1 : angular.copy(edit.item.view_delivery)),
            sameAddress: angular.copy(edit.item.sameAddress),
        }
    }

    function showCCSelectize() {
        edit.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }

    function removeLine($i) {
        var tr_id = edit.item.invoice_line[$i].tr_id;
        removeTaxes(tr_id);
        removeComponents(tr_id);
        if (edit.item.invoice_line[$i].has_variants == true || edit.item.invoice_line[$i].has_variants == '1') {
            //removeVariant(edit.item.invoice_line[$i].article_id);
            removeVariant(tr_id);
        }
        if ($i > -1) {
            edit.item.invoice_line.splice($i, 1);
            edit.calcInvoice()
        }
    }

    function removeLineSpecial($i) {
        edit.item.invoice_line = [];
        edit.hide_content = !0;
        edit.calcInvoice()
    }

    function removeVariant(id) {
        for (x in edit.item.invoice_line) {
            //if (edit.item.invoice_line[x].is_variant_for == id) {
            if (edit.item.invoice_line[x].is_variant_for_line == id) {
                edit.item.invoice_line.splice(x, 1);
                break;
            }
        }
    }

    function removeTaxes(id) {
        for (x in edit.item.invoice_line) {
            if (edit.item.invoice_line[x].for_article == id) {
                edit.item.invoice_line.splice(x, 1);
                removeTaxes(id)
            }
        }
    }

    function removeComponents(id) {
        for (x in edit.item.invoice_line) {
            if (edit.item.invoice_line[x].component_for == id) {
                edit.item.invoice_line.splice(x, 1);
                removeComponents(id)
            }
        }
    }

    function addArticle() {
        edit.showTxt.addArticleBtn = !0;
        $timeout(function() {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.open()
        })
    }
    edit.calcInvoice = function() {
        editItem.calcInvoiceTotal(edit.item)
    }

    edit.showChildVariants = function(item, index) {
        var tmp_data = {
            'do': 'misc-articleVariants',
            'parentVariant': item.is_variant_for,
            'is_variant_for_line': item.is_variant_for_line,
            buyer_id: edit.item.buyer_id,
            lang_id: edit.item.email_language,
            cat_id: edit.item.price_category_id,
            remove_vat: edit.item.remove_vat,
            vat_regime_id: edit.item.vat_regime_id,
            app: edit.app
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', tmp_data, function(r) {
            item.showVariants = true;
            edit.item.invoice_line[index].variantsOpts = r.data.lines;
        });
    }

    function changeCurrency() {
        var data = {
            'do': 'invoice-ninvoice',
            invoice_id: edit.item_id,
            'currency_type': edit.item.currency_type,
            'change_currency': '1'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            if (r && r.data) {
                edit.item.currency_type = r.data.currency_type;
                edit.item.currency_val = r.data.currency_val;
                edit.item.currency_rate = r.data.currency_rate;
                edit.item.total_currency_hide = r.data.total_currency_hide;
                edit.item.total_default_currency = helper.displayNr(helper.return_value(edit.item.total) * helper.return_value(r.data.currency_rate))
            }
        });
        edit.calcInvoice()
    }
    edit.changeVat = function() {
        if (edit.item.no_vat == !0) {
            edit.item.no_vat = !1;
            edit.item.show_vat = !0;
            edit.item.vat = helper.displayNr(0);
            edit.item.hide_vattd = !1;
            edit.item.remove_vat = 1;
            for (x in edit.item.vat_line) {
                edit.item.vat_line[x].vat_percent = helper.displayNr(0)
            }
            for (x in edit.item.invoice_line) {
                edit.item.invoice_line[x].vat = helper.displayNr(0);
                edit.item.invoice_line[x].item_width += 1
            }
            edit.item.item_width += 1
        } else {
            for (x in edit.item.invoice_line) {
                edit.item.invoice_line[x].item_width -= 1
            }
            edit.item.item_width -= 1;
            edit.item.no_vat = !0;
            edit.item.show_vat = !1;
            edit.item.hide_vattd = !0;
            edit.item.remove_vat = 0
        }
        edit.calcInvoice()
    }
    edit.vatApplyAll = function() {
        for (x in edit.item.vat_line) {
            edit.item.vat_line[x].vat_percent = helper.displayNr(helper.return_value(edit.item.vat))
        }
        for (x in edit.item.invoice_line) {
            edit.item.invoice_line[x].vat = helper.displayNr(helper.return_value(edit.item.vat))
        }
        edit.calcInvoice()
    }
    edit.changeDiscount = function() {
        var val = edit.item.apply_discount;
        if (edit.item.apply_discount_line && !edit.item.apply_discount_global) {
            edit.item.apply_discount = 1
        } else if (edit.item.apply_discount_global && !edit.item.apply_discount_line) {
            edit.item.apply_discount = 2
        } else if (edit.item.apply_discount_line && edit.item.apply_discount_global) {
            edit.item.apply_discount = 3
        } else if (!edit.item.apply_discount_line && !edit.item.apply_discount_global) {
            edit.item.apply_discount = 0
        }
        edit.item.hide_global_discount = edit.item.apply_discount < 2 ? !1 : !0;
        edit.item.hide_line_global_discount = edit.item.apply_discount == 1 ? !0 : !1;
        edit.item.hide_discount_line = edit.item.apply_discount == 0 || edit.item.apply_discount == 2 ? !1 : !0;
        var line_width = 0;
        if ((edit.item.apply_discount == 0 || edit.item.apply_discount == 2) && (val == 1 || val == 3)) {
            edit.item.item_width += 1;
            line_width += 1
        } else if ((edit.item.apply_discount == 1 || edit.item.apply_discount == 3) && (val == 0 || val == 2)) {
            edit.item.item_width -= 1;
            line_width -= 1
        }
        for (x in edit.item.invoice_line) {
            edit.item.invoice_line[x].discount_line = edit.item.apply_discount == 0 ? helper.displayNr(helper.return_value(0)) : (edit.item.apply_discount == 1 || edit.item.apply_discount == 3 ? helper.displayNr(helper.return_value(edit.item.discount_line_gen)) : helper.displayNr(helper.return_value(edit.item.discount)));
            edit.item.invoice_line[x].hide_discount_line = edit.item.apply_discount == 1 || edit.item.apply_discount == 3 ? !0 : !1;
            edit.item.invoice_line[x].item_width += line_width
        }
        for (x in edit.item.vat_line) {
            edit.item.vat_line[x].view_discount = edit.item.apply_discount > 1 ? !0 : !1
        }
        edit.calcInvoice()
    }
    edit.changeDisc = function() {
        if (edit.item.apply_discount == 1) {
            for (x in edit.item.invoice_line) {
                edit.item.invoice_line[x].discount_line = helper.displayNr(helper.return_value(edit.item.discount_line_gen))
            }
        } else if (edit.item.apply_discount == 3) {
            for (x in edit.item.invoice_line) {
                edit.item.invoice_line[x].discount_line = helper.displayNr(helper.return_value(edit.item.discount_line_gen))
            }
        }
        edit.calcInvoice()
    }
    edit.cancel = function() {
        if (isNaN(edit.c_invoice_id)) {
            $state.go('invoices')
        } else if(!isNaN(edit.duplicate_invoice_id)) {
            $state.go('invoice.view', {
                invoice_id: edit.duplicate_invoice_id
            })
        } else {
            $state.go('invoice.view', {
                invoice_id: edit.c_invoice_id
            })
        }
    }
    edit.changePriceVat = function(val, index) {
        var value = helper.return_value(val) * 100 / (100 + parseFloat(helper.return_value(edit.item.invoice_line[index].vat), 10));
        edit.item.invoice_line[index].price = helper.displayNr(value, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
        edit.calcInvoice()
    }
    edit.changeLang = function() {
        var data = {
            'do': 'invoice-ninvoice',
            invoice_id: edit.item_id,
            'email_language': edit.item.email_language,
            'languages': edit.item.email_language,
            'xget': 'notes',
            'vat_regime_id': edit.item.vat_regime_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            if (r && r.data) {
                edit.item.translate_loop = r.data.translate_loop;
                edit.item.NOTES = r.data.NOTES;
                edit.item.notes2 = r.data.notes2;
                edit.item.translate_loop_custom = r.data.translate_loop_custom;
                edit.item.translate_lang_active = r.data.translate_lang_active;
                edit.item.languages = edit.item.email_language
            }
        })
    }
    edit.addLine = function(content, title) {
        editItem.addInvoiceLine(edit.item, content, title)
    }
    edit.changePurchasePrice = function(index) {
        var temp = {};
        temp.purchase_price = edit.item.invoice_line[index].purchase_price;
        temp.index = index;
        openModal("edit_purchase_price", temp, "md")
    }
    edit.setDueDate = function() {
        var tempDate = new Date(edit.item.invoice_date);
        var initDate = new Date(edit.item.invoice_date).getTime() / 1000;
        var nextMonth = new Date(tempDate.setMonth(tempDate.getMonth() + 1, 0)).getTime() / 1000;
        if (edit.item.payment_type_choose == 1) {
            edit.item.invoice_due_date = initDate + (edit.item.due_days * 60 * 60 * 24)
        } else {
            //edit.item.invoice_due_date = nextMonth + (edit.item.due_days * 60 * 60 * 24)
            let tmp_date=initDate + (edit.item.due_days * 60 * 60 * 24)
            let due_date_month=new Date((tmp_date-7200)*1000)
            edit.item.invoice_due_date = new Date(due_date_month.setMonth(due_date_month.getMonth() + 1, 0)).getTime() / 1000;
        }
        edit.item.invoice_due_date = new Date((edit.item.invoice_due_date + 7200) * 1000);
        edit.item.quote_ts = new Date((initDate + 7200) * 1000)
    }
    edit.setDueDateVat = function() {
        var initDate = new Date(edit.item.invoice_due_date_vat).getTime() / 1000;
        edit.item.due_date_vat = new Date((initDate + 7200) * 1000)
    }
    edit.showAlerts = function(d) {
        helper.showAlerts(edit, d)
    }
    edit.save = function() {
        if(edit.item.is_first_invoice && !edit.showTxt.invoiceConvention){
            openModal("invoice_nrModal",{},"md");
            return false;
        }
        var data = angular.copy(edit.item);
        data = helper.clearData(data, ['APPLY_DISCOUNT_LIST', 'accmanager', 'addresses', 'articles_list', 'cc', 'contacts', 'country_dd', 'currency_type_list', 'language_dd', 'main_comp_info', 'multiple_identity_dd', 'seller_b_country_dd', 'seller_d_country_dd', 'vat_regime_dd']);
        if (edit.item.invoice_id == 'tmp') {
            data.do = 'invoice-ninvoice-invoice-add'
        } else {
            data.do = data.do_next
        }
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            angular.element('.loading_wrap').addClass('hidden');
            if (r && r.success) {
                $state.go('invoice.view', {
                    invoice_id: r.data.invoice_id
                })
            } else {
                helper.showAlertsManual(edit, r);
            }
        })
    }

    edit.applyThirdParty = function() {
        $confirm({
            ok: helper.getLanguage('Confirm'),
            cancel: helper.getLanguage('Cancel'),
            text: helper.getLanguage('Are you sure you want to apply third party invoicing?') + ' ' + helper.getLanguage('This will change the invoicing details to the third party and will change the VAT regime.')
        }).then(function() {
            edit.item.third_party_applied = '1';
            edit.item.vat_regime_id = edit.item.third_party_regime_id;
            edit.item.buyer_id = edit.item.third_party_id;
            edit.auto.options.contacts = [];
            edit.item.contact_id = undefined;

            var vat_regime_data = null;
            for (var x in edit.item.vat_regime_dd) {
                if (edit.item.vat_regime_dd[x].id == edit.item.vat_regime_id) {
                    vat_regime_data = edit.item.vat_regime_dd[x];
                }
            }
            if (vat_regime_data !== null) {
                if (vat_regime_data.no_vat) {
                    edit.item.vat = helper.displayNr(0);
                    edit.item.block_vat = !1;
                    edit.item.vat_regular = !1;
                    if (edit.item.no_vat == !0) {
                        edit.changeVat()
                    }
                } else {
                    if (vat_regime_data.regime_type == '2') {
                        edit.item.vat = helper.displayNr(0);
                        edit.item.block_vat = !0;
                        for (x in edit.item.vat_line) {
                            edit.item.vat_line[x].vat_percent = helper.displayNr(0)
                        }
                        for (x in edit.item.invoice_line) {
                            edit.item.invoice_line[x].vat = helper.displayNr(0)
                        }
                        edit.calcInvoice();
                        if (edit.item.no_vat == !1) {
                            edit.changeVat()
                        }
                    } else if (vat_regime_data.regime_type == '3') {
                        edit.item.vat = helper.displayNr(vat_regime_data.value);
                        edit.item.block_vat = vat_regime_data.regime_id == '4' ? !0 : !1;

                        for (x in edit.item.vat_line) {
                            edit.item.vat_line[x].vat_percent = helper.displayNr(vat_regime_data.value)
                        }
                        for (x in edit.item.invoice_line) {
                            edit.item.invoice_line[x].vat = helper.displayNr(vat_regime_data.value)
                        }
                        edit.calcInvoice();
                        if (edit.item.no_vat == !1) {
                            edit.changeVat()
                        }
                    } else {
                        edit.item.vat = helper.displayNr(vat_regime_data.value);
                        edit.item.block_vat = !1;
                        if (edit.item.no_vat == !1) {
                            edit.changeVat()
                        }
                    }
                    if (vat_regime_data.regime_type == '1') {
                        edit.item.vat_regular = !0
                    } else {
                        edit.item.vat_regular = !1
                    }
                }
                edit.changeLang();
                edit.vatApplyAll();
                edit.refreshList();
                edit.saveClientData(edit, '', '', true);
            }
        }, function() {
            //
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'edit_purchase_price':
                var iTemplate = 'PurchasePrice';
                var ctas = 'pprice';
                break
            case 'create_deal':
                var iTemplate = 'CreateDeal';
                var ctas = 'show';
                break;
            case 'stock_info':
                var iTemplate = 'modal';
                var ctas = 'vmMod';
                break;
             case 'invoice_nrModal':
                var iTemplate = 'invoiceNr';
                var ctas = 'vmMod';
                break;
        }
        var params = {
            template: 'iTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'edit_purchase_price':
                params.params = {
                    'do': 'invoice-' + type,
                    purchase_price: item.purchase_price,
                    index: item.index
                };
                params.callback = function(data) {
                    if (data) {
                        edit.item.invoice_line[data.index].purchase_price = data.purchase_price
                    }
                }
                break
            case 'create_deal':
                params.template = 'cTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'customers-deal_edit',
                    'opportunity_id': 'tmp',
                    'stage_id': edit.item.stage_id,
                    'board_id': edit.item.board_id,
                    'user_id': edit.item.user_id,
                    'buyer_id': edit.item.buyer_id,
                    'contact_id': edit.item.contact_id,
                    'not_redirect_deal': 0,
                    'amount': edit.item.total,
                    'subject': edit.item.subject
                };
                params.callback = function(d) {
                    if (d) {
                        var data = {
                            'do': 'invoice-ninvoice',
                            xget: 'deals',
                            buyer_id: edit.item.buyer_id
                        };
                        helper.doRequest('get', 'index.php', data, function(r) {
                            edit.item.deals = r.data;
                            edit.item.deal_id = d.deal_id;
                            edit.item.old_deal_id = d.deal_id;
                            editItem.init(edit)
                        })
                    } else {
                        edit.item.deal_id = edit.item.old_deal_id
                    }
                }
                break;
            case 'stock_info':
                params.template = 'miscTemplate/' + iTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                break;
            case 'invoice_nrModal':
                params.initial_item = edit.item.type;
                params.params = {
                    'do': 'invoice-settings',
                    'xget': 'Convention'
                };
                params.callback=function(d){
                    if(d){
                        edit.showTxt.invoiceConvention = !0;
                        edit.save();
                    }
                }
                break;
                
        }
        modalFc.open(params)
    }
    edit.adjustPrice = function() {
        if (edit.item.adjust_prices) {
            edit.adjustP = [];
            var artadjust = '';
            for (x in edit.item.invoice_line) {
                if (edit.item.invoice_line[x].article_id != 0) {
                    artadjust += edit.item.invoice_line[x].article_id + ','
                }
            }
            var obj = {
                'do': 'invoice-ninvoice',
                'xget': 'articles_list',
                buyer_id: edit.item.buyer_id,
                lang_id: edit.item.email_language,
                cat_id: edit.item.price_category_id,
                remove_vat: edit.item.remove_vat,
                artadjust: artadjust
            };
            helper.doRequest('post', 'index.php', obj, function(r) {
                for (x in edit.item.invoice_line) {
                    for (y in r.data.lines) {
                        if (edit.item.invoice_line[x].article_id == r.data.lines[y].article_id) {
                            edit.adjustP.push(angular.copy(edit.item.invoice_line[x]));
                            edit.item.invoice_line[x].price = helper.displayNr(r.data.lines[y].price);
                            break
                        }
                    }
                }
                edit.calcInvoice()
            })
        } else {
            for (x in edit.item.invoice_line) {
                for (y in edit.adjustP) {
                    if (edit.item.invoice_line[x].article_id == edit.adjustP[y].article_id) {
                        edit.item.invoice_line[x].price = edit.adjustP[y].price;
                        break
                    }
                }
            }
            edit.calcInvoice()
        }
    }

    edit.invoiceQuantityUpdate = function(item, index) {
        editItem.invoiceQuantityUpdate(item, index, edit);
    }

    edit.dragControlListeners = {
        dragStart: function(event) {
            $timeout(function() {
                var id = angular.element('.table_orders').find('li').eq(event.source.index).find('textarea').attr('id');
                if (id) {
                    $scope.$broadcast('$tinymce:refresh', id)
                }
            })
        },
        dragEnd: function(event) {
            $timeout(function() {
                var id = angular.element('.table_orders').find('li').eq(event.dest.index).find('textarea').attr('id');
                if (id) {
                    $scope.$broadcast('$tinymce:refresh', id)
                }
            })
        }
    }

    edit.contactUpdated=function(language){
        //if(edit.invoice_id && !isNaN(edit.invoice_id)){
            if(language && language>0 && language != edit.item.email_language){
                var selectize = angular.element('#email_language')[0].selectize;
                selectize.addItem(language);
            }
        //}
    }

    function initial() {
        var data = {
            'invoice_id': edit.item_id,
            'duplicate_invoice_id': edit.duplicate_invoice_id,
            'buyer_id': edit.buyer_id,
            'c_invoice_id': edit.c_invoice_id,
            'type': edit.type,
            'a_invoice_id': edit.a_invoice_id,
            'base_type': edit.base_type,
            'languages': edit.languages,
            'c_quote_id': edit.c_quote_id,
            'a_quote_id': edit.a_quote_id,
            'track_base': edit.track_base,
            'your_ref': edit.your_ref,
            'copy_free_text': edit.copy_free_text
        };
        if (cachedData != undefined) {
            for (x in cachedData) {
                data[x] = cachedData[x]
            }
            cache.remove('data')
        } else if (cachedDataService != undefined) {
            for (x in cachedDataService) {
                data[x] = cachedDataService[x]
            }
            cacheService.remove('data')
        } else if (cachedDataOrder != undefined) {
            for (x in cachedDataOrder) {
                data[x] = cachedDataOrder[x]
            }
            cacheOrder.remove('data')
        } else if (cachedDataContract != undefined) {
            for (x in cachedDataContract) {
                data[x] = cachedDataContract[x]
            }
            cacheContract.remove('data')
        } else if (cachedDataDeal != undefined) {
            for (x in cachedDataDeal) {
                data[x] = cachedDataDeal[x]
            }
            cacheDeal.remove('data')
        }
        if (edit.type == 3 && edit.c_quote_id) {
            edit.openModal(edit, 'addArticleSpecial', '', 'lg')
        }
        helper.doRequest('post', 'index.php?do=invoice-ninvoice', data, edit.renderPage)
    }
    angular.element('.loading_wrap').removeClass('hidden');
    edit.initial()
}]).controller('PurchasePriceModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var pprice = this;
    pprice.obj = data;
    pprice.ok = function() {
        $uibModalInstance.close(pprice.obj)
    };
    pprice.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('InvoiceSettingsCtrl', ['$scope', '$rootScope', '$compile', '$timeout', '$sce', 'helper', '$uibModal', 'settingsFc', 'selectizeFc', 'data', function($scope, $rootScope, $compile, $timeout, $sce, helper, $uibModal, settingsFc, selectizeFc, data) {
    var vm = this;
    vm.app = 'invoice';
    vm.ctrlpag = 'settings';
    vm.tmpl = 'iTemplate';
    settingsFc.init(vm);
    vm.logos = {};
    vm.settings = {
        show_bank: !1,
        show_stamp: !1,
        show_page: !1,
        show_paid: !1
    };
    vm.credit = {};
    vm.customLabel = {};
    vm.custom = {
        pdf_active: !1,
        pdf_note: !1,
        pdf_condition: !1,
        pdf_stamp: !1
    };
    vm.custom.layouts = {
        clayout: [],
        selectedOption: '0'
    };
    vm.message = {};
    vm.email_default = {};
    vm.weblink = {};
    vm.creditnote = {};
    vm.timesheets = {};
    vm.chargevats = {};
    vm.exportsettings = {};
    vm.vats = {};
    vm.sepa_active = {};
    vm.convention = {};
    vm.changelanguageinvoice = changelanguageinvoice;
    vm.savelanguageinvoice = savelanguageinvoice;
    vm.showAlerts = showAlerts;
    vm.saveEmail = saveEmail;
    vm.saved = {};
    vm.saved.save_button_text = helper.getLanguage('Save');
    vm.saved.disabled_button = !1;
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 850,
        height: 300,
        relative_urls: 1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.rem = {};
    vm.rem.tinymceOptions = {
        selector: ".mail_send",
        resize: "height",
        height: 184,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
    };

    function showAlerts(d, formObj) {
        helper.showAlerts(vm, d, formObj)
    }

    function saveEmail(vm, r, formObj) {
        vm.alertsField = {};
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.email_default = r.data.email_default;
            if (r.success) {
                vm.showAlerts(r, formObj);
                angular.element('.btn-saved').addClass('disabled')
            } else if (r.error) {
                vm.showAlerts(r, formObj);                
            }
        })
    }
    vm.updateLayout = function() {
        var get = 'PDFlayout';
        var data = {
            'do': 'invoice--invoice-settings',
            'xget': get,
            show_bank: vm.settings.show_bank,
            'show_stamp': vm.settings.show_stamp,
            'show_page': vm.settings.show_page,
            'show_paid': vm.settings.show_paid
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.Changecodelabes = function(vm, r, xget) {
        var get = xget ? xget : 'emailMessage';
        var data = {
            'do': 'invoice-settings',
            'xget': get,
            detail_id: r.detail_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            if (xget && xget == 'reminders') {
                vm.reminders.message_labels = {
                    detail_id: r.data.reminders.message_labels.detail_id,
                    detail: r.data.reminders.message_labels.detail,
                    selected_detail: r.data.reminders.message_labels.selected_detail
                };
            } else {
                vm.message_labels = {
                    detail_id: r.data.message_labels.detail_id,
                    detail: r.data.message_labels.detail,
                    selected_detail: r.data.message_labels.selected_detail
                };
            }
            vm.showAlerts(r)
        })
    }
    vm.load = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'invoice-settings',
            'xget': h
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
            if (k == 'logos') {
                vm.default_logo = r.data.default_logo
            }
            if (k == 'reminders') {
                $timeout(function() {
                    angular.element('#reminders .nav-tabs li:first-child').addClass('active');
                    angular.element('#reminders #tab_rem_0').addClass('active')
                })
            }
        })
    }

    vm.loadLayout = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'invoice-settings',
            'xget': h
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            vm = r.data;
        })
    }
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            backdrop: (ctrl == 'InvoicelabelCtrl' ? 'static' : true),
            size: 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            vm.selected = selectedItem
        }, function() {
            if (r.load) {
                vm.load(r.load[0], r.load[1])
            }
        })
    };
    vm.modal_tax = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(data);
            vm.vat = data.data.vat;
            vm.language = data.data.language
        }, function() {})
    };
    vm.restoreData = function(vm, r) {
        var get = 'restore';
        var data = {
            'do': 'invoice-settings-invoice-backup_data',
            'xget': 'restore',
            'file_id': vm.restore.file_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.restore = r.data.restore;
            vm.showAlerts(r)
        })
    }
    vm.eraseData = function(vm, r) {
        var get = 'restore';
        var data = {
            'do': 'invoice-settings-invoice-erase_data',
            'xget': 'restore'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.restore = r.data.restore;
            vm.showAlerts(r)
        })
    }
    vm.importData = function(vm, r) {
        var data_restore = {
            'do': 'invoice-settings-invoice-restore_data',
            'xget': 'restore',
            'backup_id': vm.restore.file2_id
        };
        vm.modal_tax(data_restore, 'iTemplate/restoredata', 'RestoreCtrl', 'sm')
    }
    vm.saveterm = function() {
        var data = angular.copy(vm.invoiceTerm);
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.invoiceTerm = r.data.invoiceTerm;
            vm.showAlerts(r)
        })
    }
    vm.PageActive = function(h) {
        var data = {
            'do': 'invoice--invoice-PageActive',
            'xget': h.xget,
            'type': h.type,
            'page': h.page
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.genNotesActive = function(h) {
        var data = {
            'do': 'invoice--invoice-genNotesActive',
            'xget': h.xget,
            'type': h.type,
            'gen_notes': h.gen_notes
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.Convention = function() {
        var get = 'convention';
        var data = angular.copy(vm.convention);
        data.do = 'invoice-settings-invoice-Convention';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.convention = r.data;
            vm.showAlerts(r)
        })
    }
    vm.creditupdate = function(r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.creditnote = r.data.creditnote;
            vm.showAlerts(r)
        })
    }
    vm.deleteatach = function(vm, id, name) {
        var get = 'attachments';
        var data = {
            'do': 'invoice-settings-invoice-deleteatach',
            'xget': get,
            'id': id,
            'name': name
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.atachment = r.data.atachment;
            vm.showAlerts(r)
        })
    }
    vm.defaultcheck = function(vm, id, name, default_id) {
        var get = 'attachments';
        var data = {
            'do': 'invoice-settings-invoice-defaultcheck',
            'xget': get,
            'id': id,
            'name': name,
            'default_id': default_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.atachment = r.data.atachment;
            vm.showAlerts(r)
        })
    }
    vm.timesheetup = function(r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.creditnote = r.data.creditnote;
            vm.showAlerts(r)
        })
    }
    vm.chargevatup = function(r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.chargevats = r.data.chargevats;
            vm.showAlerts(r)
        })
    }
    vm.exportsettingup = function(field_name, field_id, field_value) {
        var get = 'exportsetting';
        var data = {
            'do': 'invoice-settings-invoice-update_export_settings',
            'xget': get,
            'field_name': field_name,
            'field_id': field_id,
            'field_value': field_value
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.exportsettings = r.data.exportsettings;
            vm.showAlerts(r)
        })
    }
    vm.vatclasification = function(field_name, field_id, field_value, field_id_2) {
        var get = 'exportsetting';
        var data = {
            'do': 'invoice-settings-invoice-update_export_settings',
            'xget': get,
            'field_name': field_name,
            'field_id': field_id,
            'field_value': field_value,
            'field_id_2': field_id_2
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.exportsettings = r.data.exportsettings;
            vm.showAlerts(r)
        })
    }
    vm.sepaupdate = function(r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.sepa_active = r.data.sepa_active;
            vm.showAlerts(r)
        })
    }
    vm.clickyArticle = function(scope, myValue) {
        if (!scope.articlefields.ADV_PRODUCT) {
            return !1
        }
        vm.clicky(myValue)
    }
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.articlefields.text = vm.articlefields.text.substring(0, startPos) + myValue + vm.articlefields.text.substring(endPos, vm.articlefields.text.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.articlefields.text += myValue;
            _this.focus()
        }
    }
    vm.clickys = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.message.html_content = vm.message.html_content.substring(0, startPos) + myValue + vm.message.html_content.substring(endPos, vm.message.html_content.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.message.html_content += myValue;
            _this.focus()
        }
    }
    vm.clickAddReminderEditor = function(myValue) {
        var _this = angular.element('.email_body_reminders')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.reminders.text = vm.reminders.text.substring(0, startPos) + myValue + vm.reminders.text.substring(endPos, vm.reminders.text.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.reminders.text += myValue;
            _this.focus()
        }
    }
    vm.updateReminders = function(obj) {
        var data = angular.copy(obj);
        data.do = 'invoice-settings-invoice-updateRemSettings';
        data.xget = 'reminders';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.reminders = r.data.reminders;
            vm.showAlerts(r);
            /*$timeout(function() {
                angular.element('#reminders .nav-tabs li:first-child').addClass('active');
                angular.element('#reminders #tab_rem_0').addClass('active')
            })*/
        })
    }
    vm.changeRemLang = function() {
        var data = {};
        data.languages = vm.reminders.languages;
        helper.doRequest('get', 'index.php?do=invoice-settings&languages=' + vm.reminders.languages + '&xget=reminders', function(r) {
            vm.reminders = r.data.reminders;
            vm.showAlerts(r);
            $timeout(function() {
                angular.element('#reminders .nav-tabs li:first-child').addClass('active');
                angular.element('#reminders #tab_rem_0').addClass('active')
            })
        })
    }
    vm.changeSepaLang = function() {
        var data = {};
        data.languages = vm.sepa.languages;
        helper.doRequest('get', 'index.php?do=invoice-settings&xget=sepa&languages=' + vm.sepa.languages, function(r) {
            vm.sepa = r.data.sepa;
            vm.showAlerts(r)
        })
    }
    vm.sepa_activate = function() {
        helper.doRequest('post', 'index.php', {
            'do': 'invoice--invoice-active_sepa',
            'sepa_active': vm.sepa.sepa_active
        }, function(r) {
            /*vm.showAlerts(r);*/
            /*helper.removeCachedTemplates();
            angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
            angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
            $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
            $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope)*/
        })
    }
    vm.updateSepa = function(obj) {
        var data = angular.copy(obj);
        data.do = 'invoice-settings-invoice-sepa_details';
        data.xget = 'sepa';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.sepa = r.data.sepa;
            vm.showAlerts(r)
        })
    }
    vm.pinvoiceupdate = function(obj) {
        var data = angular.copy(vm.p_invoices);
        data.do = 'invoice-settings-invoice-purchase_invoice_details';
        data.xget = 'p_invoices';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.p_invoices = r.data.p_invoices;
            vm.showAlerts(r)
        })
    }

    var base_url = angular.element('base').attr('href');
    var pInvoiceDisabledMsg = helper.getLanguage('Activate dropbox integration to acces this setting');
    pInvoiceDisabledMsg = pInvoiceDisabledMsg.replace("dropbox", "<a href=\"" + base_url + "settings/?tab=apps\">dropbox</a>");
    vm.pInvoiceDisabledMsg = $sce.trustAsHtml(pInvoiceDisabledMsg);
    vm.renderPage = function(r) {
        helper.doRequest('get', 'index.php?do=invoice-settings&xget=PDFlayout', function(r) {
            vm.settings = {
                show_bank: r.data.show_bank,
                show_stamp: r.data.show_stamp,
                show_page: r.data.show_page,
                show_paid: r.data.show_paid,
                multiple_identity: r.data.multiple_identity,
                identity: r.data.identity,
                selected_header: r.data.selected_header,
                selected_body: r.data.selected_body,
                selected_footer: r.data.selected_footer
            };
            vm.layout_header = r.data.layout_header;
            vm.layout_body = r.data.layout_body;
            vm.layout_footer = r.data.layout_footer;
            vm.custom_layout = r.data.custom_layout;
            vm.adv_sepa = r.data.adv_sepa;
            vm.has_custom_layout = r.data.has_custom_layout;
            vm.use_custom_layout = r.data.use_custom_layout;
            vm.identity = r.data.identity;
        });
        helper.doRequest('get', 'index.php?do=invoice-settings&xget=PDFlayoutCredit', function(r) {
            vm.credit = {
                selected_credit_header: r.data.selected_credit_header,
                selected_credit_body: r.data.selected_credit_body,
                selected_credit_footer: r.data.selected_credit_footer
            };
            vm.layout_credit_header = r.data.layout_credit_header;
            vm.layout_credit_body = r.data.layout_credit_body;
            vm.layout_credit_footer = r.data.layout_credit_footer;
            vm.custom_credit_layout = r.data.custom_credit_layout;
            vm.has_custom_credit_layout = r.data.has_custom_credit_layout;
            vm.use_custom_credit_layout = r.data.use_custom_credit_layout;
            vm.identity = r.data.identity;
        });
        helper.doRequest('get', 'index.php?do=invoice-settings&xget=PDFlayoutReminder', function(r) {
            vm.reminder = {
                selected_reminder_header: r.data.selected_reminder_header,
                selected_reminder_body: r.data.selected_reminder_body,
                selected_reminder_footer: r.data.selected_reminder_footer
            };
            vm.layout_reminder_header = r.data.layout_reminder_header;
            vm.layout_reminder_body = r.data.layout_reminder_body;
            vm.layout_reminder_footer = r.data.layout_reminder_footer;
            vm.custom_reminder_layout = r.data.custom_reminder_layout;
            vm.has_custom_reminder_layout = r.data.has_custom_reminder_layout;
            vm.identity = r.data.identity;
        });
        helper.doRequest('get', 'index.php?do=invoice-settings&xget=Convention', function(r) {
            vm.convention = {
                account_number: r.data.account_number,
                account_digits: r.data.account_digits,
                example: r.data.example,
                account_credit: r.data.account_credit,
                account_credit_digits: r.data.account_credit_digits,
                example2: r.data.example2,
                account_purchase: r.data.account_purchase,
                account_purchase_digits: r.data.account_purchase_digits,
                example3: r.data.example3,
                use_credit_number: r.data.use_credit_number,
                account_purchase_credit: r.data.account_purchase_credit,
                account_purchase_credit_digits: r.data.account_purchase_credit_digits,
                example4: r.data.example4,
                payment_instruction: r.data.payment_instruction,
                random_digits: r.data.random_digits,
                draft_invoices: r.data.draft_invoices,
                reference: r.data.reference,
                del: r.data.del,
                last_serial_number: r.data.last_serial_number,
                example5: r.data.example5,
                block_draft: r.data.block_draft,
                account_proforma: r.data.account_proforma,
                account_proforma_digits: r.data.account_proforma_digits,
                example_proforma: r.data.example_proforma,
                invoice_starting_number: r.data.invoice_starting_number
            }
        })
    };

    function changelanguageinvoice(scope, r) {
        var dataS = {
            'do': scope.app + '-settings-' + scope.app + '-save_language',
            'xget': r.xget,
            'type': r.type,
            'languages': r.old_languages_value,
            'notes': r.notes,
            'terms': r.terms
        };
        helper.doRequest('post', 'index.php', dataS, function(r) {});
        helper.doRequest('post', 'index.php', r, function(r) {
            if (r.data.invoice_general_conditions) {
                vm.invoice_general_conditions = r.data.invoice_general_conditions;
            }
            if (r.data.credit_general_conditions) {
                vm.credit_general_conditions = r.data.credit_general_conditions;
            }
            vm.showAlerts(r)
        })
    }

    function savelanguageinvoice(scope, type, languages, notes, terms) {
        var get = type.xget;
        var data = {
            'do': scope.app + '-settings-' + scope.app + '-save_language',
            'xget': get,
            'type': type.type,
            'languages': type.languages,
            'notes': type.notes,
            'terms': type.terms
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                if (r.data.invoice_general_conditions) {
                    vm.invoice_general_conditions = r.data.invoice_general_conditions;
                    vm.invoice_general_conditions.save_button_text = helper.getLanguage('Saved');
                    vm.invoice_general_conditions.disabled_button = true;
                }
                if (r.data.credit_general_conditions) {
                    vm.credit_general_conditions = r.data.credit_general_conditions;
                    vm.credit_general_conditions.save_button_text = helper.getLanguage('Saved');
                    vm.credit_general_conditions.disabled_button = true;
                }
                // vm.showAlerts(r)
            }

        })
    }

    vm.removeActiveTabs = function(array) {
        for (var x in array) {
            angular.element('#' + array[x]).removeClass('active in');
        }
    }

    vm.renderPage(data)
}]).controller('InvoiceCustomPdfCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'invoice';
    vm.ctrlpag = 'custom_pdf';
    vm.tmpl = 'iTemplate';
    settingsFc.init(vm);
    vm.codelabels = {};
    vm.selectlabels = {};
    vm.showtiny = !0;
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 1110,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload_image_pdf').trigger('click');
                $('#upload_image_pdf').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.codelabels.variable_data = vm.codelabels.variable_data.substring(0, startPos) + myValue + vm.codelabels.variable_data.substring(endPos, vm.codelabels.variable_data.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.codelabels.variable_data += myValue;
            _this.focus()
        }
    }
    vm.closeModal = closeModal;
    vm.cancel = cancel;

    function closeModal() {
        $uibModalInstance.close()
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    }
    helper.doRequest('get', 'index.php?do=invoice-custom_pdf&layout=' + obj.id + '&header=' + obj.header + '&footer=' + obj.footer + '&body=' + obj.body + '&identity_id=' + obj.identity_id, function(r) {
        vm.codelabels = r.data.codelabels;
        vm.selectlabels = {
            make_id: r.data.selectlabels.make_id,
            make: r.data.selectlabels.make,
            selected_make: r.data.selectlabels.selected_make
        };
        vm.selectdetails = {
            detail_id: r.data.selectdetails.detail_id,
            detail: r.data.selectdetails.detail
        }
    })
}]).controller('InvoiceCreditPdfCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'invoice';
    vm.ctrlpag = 'custom_pdf_credit';
    vm.tmpl = 'iTemplate';
    settingsFc.init(vm);
    vm.codelabels = {};
    vm.selectlabels = {};
    vm.showtiny = !0;
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 1110,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload_image_pdf').trigger('click');
                $('#upload_image_pdf').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.codelabels.variable_data = vm.codelabels.variable_data.substring(0, startPos) + myValue + vm.codelabels.variable_data.substring(endPos, vm.codelabels.variable_data.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.codelabels.variable_data += myValue;
            _this.focus()
        }
    }
    vm.closeModal = closeModal;
    vm.cancel = cancel;

    function closeModal() {
        $uibModalInstance.close()
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    }
    helper.doRequest('get', 'index.php?do=invoice-custom_pdf_credit&layout=' + obj.id + '&header=' + obj.header + '&footer=' + obj.footer + '&body=' + obj.body + '&identity_id=' + obj.identity_id, function(r) {
        vm.codelabels = r.data.codelabels;
        vm.selectlabels = {
            make_id: r.data.selectlabels.make_id,
            make: r.data.selectlabels.make,
            selected_make: r.data.selectlabels.selected_make
        };
        vm.selectdetails = {
            detail_id: r.data.selectdetails.detail_id,
            detail: r.data.selectdetails.detail
        }
    })
}]).controller('InvoiceReminderPdfCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'invoice';
    vm.ctrlpag = 'custom_pdf_reminder';
    vm.tmpl = 'iTemplate';
    settingsFc.init(vm);
    vm.codelabels = {};
    vm.selectlabels = {};
    vm.showtiny = !0;
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 1110,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload_image_pdf').trigger('click');
                $('#upload_image_pdf').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.codelabels.variable_data = vm.codelabels.variable_data.substring(0, startPos) + myValue + vm.codelabels.variable_data.substring(endPos, vm.codelabels.variable_data.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.codelabels.variable_data += myValue;
            _this.focus()
        }
    }
    vm.closeModal = closeModal;
    vm.cancel = cancel;

    function closeModal() {
        $uibModalInstance.close()
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    }
    helper.doRequest('get', 'index.php?do=invoice-custom_pdf_reminder&layout=' + obj.id + '&header=' + obj.header + '&footer=' + obj.footer + '&body=' + obj.body + '&identity_id=' + obj.identity_id, function(r) {
        vm.codelabels = r.data.codelabels;
        vm.selectlabels = {
            make_id: r.data.selectlabels.make_id,
            make: r.data.selectlabels.make,
            selected_make: r.data.selectlabels.selected_make
        };
        vm.selectdetails = {
            detail_id: r.data.selectdetails.detail_id,
            detail: r.data.selectdetails.detail
        }
    })
}]).controller('InvoicelabelCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'invoice';
    vm.ctrlpag = 'labelCtrl';
    vm.tmpl = 'iTemplate';
    settingsFc.init(vm);
    vm.labels = {};
    helper.doRequest('get', 'index.php', obj, function(r) {
        vm.labels = r.data.labels
    });
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('recinvoicesCtrl', ['$scope', '$rootScope','helper', 'list', '$uibModal', 'data', 'modalFc', function($scope, $rootScope,helper, list, $uibModal, data, modalFc) {
    console.log('Recurring Invoices');
    $rootScope.opened_lists.recinvoices=true;
    $scope.views = [{
        name: 'Active',
        id: 0,
        active: 'active'
    }, {
        name: 'Archived',
        id: -1
    }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.nav = [];
    $scope.search = {
        search: '',
        'do': 'invoice-recurring_invoices',
        view: $scope.activeView,
        xget: '',
        offset: 1
    };
    $scope.periods = [{
        'id': '1',
        'value': helper.getLanguage('Weekly')
    }, {
        'id': '2',
        'value': helper.getLanguage('Monthly')
    }, {
        'id': '3',
        'value': helper.getLanguage('Quarterly')
    }, {
        'id': '4',
        'value': helper.getLanguage('Yearly')
    }, {
        'id': '5',
        'value': helper.getLanguage('Custom')
    }];
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.ord = '';
    $scope.reverse = !1;
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'Active',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.periodCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Period'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.start_date = new Date();
            $scope.search.from = undefined;
            $scope.search.top = undefined;
            if (value && this.options[value].id != 5) {
                $scope.searchThing()
            }
        },
        maxItems: 1
    };
    $scope.minimum_selected=false;
    $scope.all_pages_selected=false;
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.post_active = d.data.post_active;
            $scope.lr = d.data.lr;
            $scope.amount_this_month = d.data.amount_this_month;
            $scope.to_be_invoiced = d.data.to_be_invoiced;
            $scope.mrr = d.data.mrr;
            $scope.columns = d.data.columns;
            $scope.minimum_selected=d.data.minimum_selected;
            $scope.all_pages_selected=d.data.all_pages_selected;
            $scope.nav = d.data.nav;
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['recurring_invoice_id']);
                count ++;
            }
            sessionStorage.setItem('recinvoice.view_total', count);
            sessionStorage.setItem('recinvoice.view_list', JSON.stringify(ids));
            //$scope.show_deliveries = d.data.show_deliveries;
            //$scope.show_projects = d.data.show_projects;
            //$scope.show_interventions = d.data.show_interventions;
            //$scope.initCheckAll();
        }
        //$scope.getAmounts();
    };

    $scope.getAmounts = function(doRefresh) {
        $scope.nr_show = !0;
        helper.doRequest('get', 'index.php', {
            'do': 'invoice-invoices',
            'xget': 'Amounts',
            'refresh_data': (doRefresh ? 1 : 0)
        }, function(r) {
            if (r.data.amount_o) {
                $scope.amount_o = r.data.amount_o
            }
            if (r.data.total_amount_p) {
                $scope.total_amount_p = r.data.total_amount_p
            }
            if (r.data.amount_s) {
                $scope.amount_s = r.data.amount_s
            }
            if (r.data.number_o) {
                $scope.number_o = r.data.number_o
            }
            if (r.data.date_o) {
                $scope.date_o = r.data.date_o
            }
            if (r.data.number_p) {
                $scope.number_p = r.data.number_p
            }
            if (r.data.amount_p) {
                $scope.amount_p = r.data.amount_p
            }
            if (r.data.amount_days) {
                $scope.amount_days = r.data.amount_days
            }
            if (r.data.date_p) {
                $scope.date_p = r.data.date_p
            }
            if (r.data.number_s) {
                $scope.number_s = r.data.number_s
            }
            if (r.data.date_s) {
                $scope.date_s = r.data.date_s
            }
            $scope.nr_show = !1;
            if (doRefresh) {
                angular.element('#refresh_amounts').removeClass("fa-spin fa-1x fa-fw");
            }
        });
    }

    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList,true)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList, true)
    };
    $scope.searchThing = function(reset_list) {
        angular.element('.loading_wrap').removeClass('hidden');
        var params=angular.copy($scope.search);
        if(reset_list){
            params.reset_list='1';
        }
        list.search(params, $scope.renderList, $scope)
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.createExports = function($event) {
        list.exportFnc($scope, $event)
        var params = angular.copy($scope.search);
        params.do = 'invoice-recurring_invoices'
        params.exported = !0;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r);
        })
    }
    $scope.saveForXls = function(p) {
        if (p) {
            if (p.check_add_to_product) {
                var elem_counter = 0;
                for (x in $scope.list) {
                    if ($scope.list[x].check_add_to_product) {
                        elem_counter++;
                    }
                }
                if (elem_counter == $scope.list.length && $scope.list.length > 0) {
                    $scope.listObj.check_add_all = true;
                }
            } else {
                $scope.listObj.check_add_all = false;
            }
        }
        list.saveForPDF($scope, 'invoice--invoice-saveAddToXls', p)
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.archived = function(value) {
        $scope.search.archived = value;
        list.filter($scope, value, $scope.renderList, true)
    };
    $scope.initCheckAll = function() {
        var total_checked = 0;
        for (x in $scope.list) {
            if ($scope.list[x].check_add_to_product) {
                total_checked++;
            }
        }
        if ($scope.list.length > 0 && $scope.list.length == total_checked) {
            $scope.listObj.check_add_all = true;
        } else {
            $scope.listObj.check_add_all = false;
        }
    }

    $scope.markCheckNew=function(i){
        list.markCheckNew($scope, 'invoice--invoice-saveAddToXls', i);
    }
    $scope.checkAllPage=function(){
        $scope.listObj.check_add_all=true;
        $scope.markCheckNew();
    }
    $scope.checkAllPages=function(){
        list.markCheckNewAll($scope, 'invoice--invoice-saveAddToXlsAll','1');
    }
    $scope.uncheckAllPages=function(){
        list.markCheckNewAll($scope, 'invoice--invoice-saveAddToXlsAll','0');
    }

    $scope.columnSettings = function() {
        openModal('column_set', {}, 'lg')
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'column_set':
                var iTemplate = 'ColumnSettings';
                var ctas = 'vm';
                break;
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'column_set':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.params = {
                    'do': 'misc-column_set',
                    'list': 'recinvoices'
                };

                params.callback = function(d) {
                    if (d) {
                        helper.doRequest('get', 'index.php', {
                            'do': 'invoice-recurring_invoices',
                            'xget': 'invoices_cols'
                        }, function(o) {
                            $scope.columns = o.data;
                            $scope.searchThing()
                        })
                    }
                }
                break;
        }
        modalFc.open(params)
    }

    list.init($scope, 'invoice-recurring_invoices', $scope.renderList, ($rootScope.reset_lists.recinvoices ? true : false));
    $rootScope.reset_lists.recinvoices=false;

}]).controller('recInvoiceCtrl', ['$scope', 'helper', function($scope, helper) {
    $scope.isAddPage = !0
}]).controller('recInvoiceEditCtrl', ['$scope', '$stateParams', '$state', '$timeout', 'editItem', '$confirm', 'modalFc', 'helper', 'data', 'selectizeFc', function($scope, $stateParams, $state, $timeout, editItem, $confirm, modalFc, helper, data, selectizeFc) {
    console.log('recinvoiceEdit');
    var edit = this,
        typePromise;
    edit.app = 'invoice';
    edit.app_mod = 'recinvoice';
    edit.ctrlpag = 'recurring_ninvoice';
    edit.tmpl = 'iTemplate';
    $scope.vm = {};
    $scope.vm.isAddPage = !1;
    $scope.$parent.type_title = "";
    $scope.$parent.nr_status = "";
    edit.item_id = $stateParams.recurring_invoice_id;
    edit.invoice_id = $stateParams.invoice_id;
    edit.recurring_invoice_id = edit.item_id;
    edit.contract_id = $stateParams.contract_id;
    edit.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    edit.currencyCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            edit.changeCurrency()
        }
    });
    edit.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    edit.deliveryCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        onChange(value) {
            if (value == '1') {
                //edit.item.view_send_box = true;
                edit.changeLang();
            } else {
                edit.item.view_send_box = false;
            }
        },
        maxItems: 1
    };
    edit.frequencyCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            if (!isNaN(parseInt(edit.recurring_invoice_id))) {
                edit.item.start_date = "";
            }
        },
    });
    edit.refreshList = function() {
        var data = {
            'do': 'invoice-recurring_ninvoice',
            xget: 'articles_list',
            search: '',
            customer_id: edit.item.buyer_id,
            lang_id: edit.item.email_language,
            cat_id: edit.item.price_category_id,
            remove_vat: edit.item.remove_vat,
            vat_regime_id: edit.item.vat_regime_id
        };
        edit.requestAuto(edit, data, edit.renderArticleListAuto)
    }
    edit.vatregcfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onDropdownOpen($dropdown) {
            var _this = this;
            edit.item.old_vat_regime_id = _this.$input[0].attributes.value.value;
        },
        onChange(value) {
            if (value) {
                var selectize = angular.element('#vat_regime_select')[0].selectize;
                selectize.blur();
                var _this = this;
                $confirm({
                    ok: helper.getLanguage('Yes'),
                    cancel: helper.getLanguage('No'),
                    text: helper.getLanguage('Apply VAT Regime on all lines')
                }).then(function() {
                    if (_this.options[value].no_vat) {
                        edit.item.vat = helper.displayNr(0);
                        edit.item.block_vat = !1;
                        edit.item.vat_regular = !1;
                        if (edit.item.no_vat == !0) {
                            edit.changeVat()
                        }
                    } else {
                        if (_this.options[value].regime_type == '2') {
                            edit.item.vat = helper.displayNr(0);
                            edit.item.block_vat = !0;
                            for (x in edit.item.vat_line) {
                                edit.item.vat_line[x].vat_percent = helper.displayNr(0)
                            }
                            for (x in edit.item.invoice_line) {
                                edit.item.invoice_line[x].vat = helper.displayNr(0)
                            }
                            edit.calcInvoice();
                            if (edit.item.no_vat == !1) {
                                edit.changeVat()
                            }
                        } else if (_this.options[value].regime_type == '3') {
                            edit.item.vat = helper.displayNr(_this.options[value].value);
                            edit.item.block_vat = _this.options[value].regime_id == '4' ? !0 : !1;

                            /*if(_this.options[value].name == helper.getLanguage('Contracting Partner')){
                                edit.item.block_vat = !0;
                            } else {
                                edit.item.block_vat = !1;
                            } */

                            for (x in edit.item.vat_line) {
                                edit.item.vat_line[x].vat_percent = helper.displayNr(_this.options[value].value)
                            }
                            for (x in edit.item.invoice_line) {
                                edit.item.invoice_line[x].vat = helper.displayNr(_this.options[value].value)
                            }
                            edit.calcInvoice();
                            if (edit.item.no_vat == !1) {
                                edit.changeVat()
                            }
                        } else {
                            edit.item.vat = helper.displayNr(_this.options[value].value);
                            edit.item.block_vat = !1;
                            if (edit.item.no_vat == !1) {
                                edit.changeVat()
                            }
                        }
                        if (_this.options[value].regime_type == '1') {
                            edit.item.vat_regular = !0
                        } else {
                            edit.item.vat_regular = !1
                        }
                    }
                    edit.changeLang();
                    edit.vatApplyAll();
                    edit.refreshList();
                }, function(e) {
                    if (e == 'cancel') {
                        if (_this.options[value].no_vat) {
                            edit.item.vat = helper.displayNr(0);
                            edit.item.block_vat = !1;
                            edit.item.vat_regular = !1;
                            if (edit.item.no_vat == !0) {
                                edit.changeVat()
                            }
                        } else {
                            if (_this.options[value].regime_type == '2') {
                                edit.item.vat = helper.displayNr(0);
                                edit.item.block_vat = !0;
                                for (x in edit.item.vat_line) {
                                    edit.item.vat_line[x].vat_percent = helper.displayNr(0)
                                }
                                for (x in edit.item.invoice_line) {
                                    edit.item.invoice_line[x].vat = helper.displayNr(0)
                                }
                                edit.calcInvoice();
                                if (edit.item.no_vat == !1) {
                                    edit.changeVat()
                                }
                            } else if (_this.options[value].regime_type == '3') {
                                edit.item.vat = helper.displayNr(_this.options[value].value);
                                edit.item.block_vat = _this.options[value].regime_id == '4' ? !0 : !1;

                                /*if(_this.options[value].name == helper.getLanguage('Contracting Partner')){
                                    edit.item.block_vat = !0;
                                } else {
                                    edit.item.block_vat = !1;
                                } */

                                for (x in edit.item.vat_line) {
                                    edit.item.vat_line[x].vat_percent = helper.displayNr(_this.options[value].value)
                                }
                                for (x in edit.item.invoice_line) {
                                    edit.item.invoice_line[x].vat = helper.displayNr(_this.options[value].value)
                                }
                                edit.calcInvoice();
                                if (edit.item.no_vat == !1) {
                                    edit.changeVat()
                                }
                            } else {
                                edit.item.vat = helper.displayNr(_this.options[value].value);
                                edit.item.block_vat = !1;
                                if (edit.item.no_vat == !1) {
                                    edit.changeVat()
                                }
                            }
                            if (_this.options[value].regime_type == '1') {
                                edit.item.vat_regular = !0
                            } else {
                                edit.item.vat_regular = !1
                            }
                        }
                        edit.changeLang();
                        edit.refreshList();
                    } else if (e == 'dismiss') {
                        selectize.addItem(edit.item.old_vat_regime_id, true);
                    }
                });
            } else {
                edit.item.block_vat = !1;
                edit.item.vat_regular = !0;
                edit.item.vat = helper.displayNr(0);
                if (edit.item.no_vat == !1) {
                    edit.changeVat()
                }
                edit.refreshList()
            }
        }
    });
    edit.linevatcfg = {
        valueField: 'vat',
        labelField: 'name',
        searchField: ['vat'],
        delimiter: '|',
        placeholder: helper.getLanguage('VAT'),
        create: !1,
        maxOptions: 100,
        onDelete(values) {
            return !1
        },
        closeAfterSelect: !0,
        maxItems: 1
    };
    edit.item = {};
    edit.oldObj = {};
    edit.item.validity_date = new Date();
    edit.removedCustomer = !1;
    //edit.item.add_customer = (edit.item_id != 'tmp' ? !0 : !1);
    edit.item.add_customer = !1;
    edit.adjustP = [];
    edit.showTxt = {
        invoice_settings: !1,
        addBtn: !0,
        address: !1,
        invoice_notes: !1,
        addArticleBtn: !1,
        invoiceConvention: !1,
    }
    edit.contactAddObj = {};
    edit.auto = {
        options: {
            contacts: [],
            addresses: [],
            cc: [],
            recipients: []
        }
    };
    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {
        start_date: !1,
        end_date: !1,
    }
    edit.contactForEmailAutoCfg = {
        valueField: 'recipient_email',
        labelField: 'recipient_email',
        searchField: ['recipient_email'],
        delimiter: '|',
        placeholder: helper.getLanguage('Specify a valid email address'),
        create: !0,
        createOnBlur: true,
        maxItems: 100
    };
    edit.emailAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Specify a valid email address'),
        create: !0,
        createOnBlur: true,
        maxItems: 100
    };
    edit.tinymceOptions = {
        selector: ".mail_send",
        resize: "height",
        height: 184,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
    };
    edit.articlesListAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select article'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                if (item.allow_stock && item.is_service == 0) {
                    var stock_display = '';
                    if (item.hide_stock == 0) {
                        stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">' + helper.getLanguage('Stock') + ' <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                    }
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-8">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;</small>' + '</div>' + stock_display + '</div>' + '</div>';
                } else {
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-8">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-right">' + (item.base_price) + '</div>' + '</div>' + '</div>';
                }
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'invoice-recurring_ninvoice',
                    xget: 'articles_list',
                    search: str,
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id
                };
                edit.requestAuto(edit, data, edit.renderArticleListAuto)
            }, 300)
        },
        onChange(value) {
            if (value == undefined) {} else if (value == '99999999999') {
                edit.openModal(edit, 'AddAnArticle', {
                    'do': 'invoice-addAnArticle',
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id
                }, 'md');
                return !1
            } else {
                for (x in edit.auto.options.articles_list) {
                    if (edit.auto.options.articles_list[x].article_id == value) {
                        editItem.addInvoiceArticle(edit, edit.auto.options.articles_list[x]);
                        edit.selected_article_id = '';
                        edit.showTxt.addArticleBtn = !1;
                        var data = {
                            'do': 'invoice-recurring_ninvoice',
                            xget: 'articles_list',
                            customer_id: edit.item.buyer_id,
                            lang_id: edit.item.email_language,
                            cat_id: edit.item.price_category_id,
                            remove_vat: edit.item.remove_vat,
                            vat_regime_id: edit.item.vat_regime_id
                        };
                        edit.requestAuto(edit, data, edit.renderArticleListAuto);
                        return !1
                    }
                }
            }
        },
        onItemAdd(value, $item) {
            if (value == '99999999999') {
                edit.selected_article_id = ''
            }
        },
        onBlur() {
            $timeout(function() {
                edit.showTxt.addArticleBtn = !1
            })
        },
        maxItems: 1
    };
    edit.tinymceOptionsLines = {
        selector: "",
        resize: "both",
        auto_resize: !0,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code autoresize"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        autoresize_bottom_margin: 0,
        setup: function(editor) {
            editor.on('click', function(e) {
                e.stopPropagation();
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                angular.element(editor.editorContainer).parent().removeClass('wysiwyg-hide')
            }).on('blur', function(e) {
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                edit.delaySave()
            })
        },
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
        content_css: [helper.base_url + 'css/tinymce_styles.css']
    };
    edit.showEditFnc = showEditFnc;
    edit.openDate = openDate;
    edit.showEditFnc = showEditFnc;
    edit.showCCSelectize = showCCSelectize;
    edit.cancelEdit = cancelEdit;
    edit.removeDeliveryAddr = removeDeliveryAddr;
    edit.removeLine = removeLine;
    edit.renderPage = renderPage;
    edit.addArticle = addArticle;
    edit.changeCurrency = changeCurrency;
    edit.initial = initial;
    editItem.init(edit);

    function renderPage(d) {
        if (d && d.data) {
            edit.item = d.data;
            if (edit.item.start_date) {
                edit.item.start_date = new Date(edit.item.start_date)
            }
            if (edit.item.end_date) {
                edit.item.end_date = new Date(edit.item.end_date)
            }
            if (edit.item.quote_ts) {
                edit.item.quote_ts = new Date(edit.item.quote_ts)
            }
            edit.auto.options.contacts = d.data.contacts;
            edit.auto.options.addresses = d.data.addresses;
            edit.auto.options.cc = d.data.cc;
            edit.auto.options.recipients = d.data.recipients;
            edit.auto.options.articles_list = d.data.articles_list && d.data.articles_list.lines ? d.data.articles_list.lines : [];
            edit.contactAddObj = {
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                language_dd: edit.item.language_dd,
                language_id: edit.item.language_id,
                gender_dd: edit.item.gender_dd,
                gender_id: edit.item.gender_id,
                title_dd: edit.item.title_dd,
                title_id: edit.item.title_id,
                add_contact: !0,
                buyer_id: edit.item.buyer_id,
                article: edit.item.tr_id,
                'do': 'invoice-recurring_ninvoice-invoice-tryAddC',
                identity_id: edit.item.identity_id,
                contact_only: !0,
                item_id: edit.item_id,
                app_mod: edit.app_mod
            }
            edit.email = {
                subject: edit.item.subject,
                text: edit.item.text,
                include_pdf: edit.item.include_pdf,
                include_xml: edit.item.include_xml,
                copy: edit.item.copy_checked,
                use_html: edit.item.use_html,
                recipients: []
            };
            for (x in d.data.contact_email_data) {
                edit.email.recipients.push(d.data.contact_email_data[x])
            }
        }
    }

    function openDate(p) {
        edit.datePickOpen[p] = !0
    }

    function removeDeliveryAddr() {
        edit.item.delivery_address = '';
        edit.item.delivery_address_id = ''
    }

    function cancelEdit() {
        if (Object.keys(edit.oldObj).length == 0) {
            return edit.removeCust(edit)
        }
        edit.showTxt.address = !0;
        for (x in edit.oldObj) {
            if (edit.item[x]) {
                if (x == 'delivery_address') {
                    edit.item[x] = edit.oldObj[x].trim()
                } else {
                    edit.item[x] = edit.oldObj[x]
                }
            }
        }
        edit.item.buyer_id = edit.oldObj.buyer_id
    }

    function showEditFnc() {
        edit.showTxt.address = !1;
        edit.oldObj = {
            buyer_id: angular.copy(edit.item.buyer_id),
            contact_id: angular.copy(edit.item.contact_id),
            contact_name: angular.copy(edit.item.contact_name),
            BUYER_NAME: angular.copy(edit.item.buyer_name),
            delivery_address: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address)),
            delivery_address_id: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_id)),
            delivery_address_txt: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_txt)),
            view_delivery: (edit.item.sameAddress ? !1 : angular.copy(edit.item.view_delivery)),
            sameAddress: angular.copy(edit.item.sameAddress),
        }
    }

    function showCCSelectize() {
        edit.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }

    function removeLine($i) {
        var tr_id = edit.item.invoice_line[$i].tr_id;
        removeTaxes(tr_id);
        if ($i > -1) {
            edit.item.invoice_line.splice($i, 1);
            edit.calcInvoice()
        }
    }

    function addArticle() {
        edit.showTxt.addArticleBtn = !0;
        $timeout(function() {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.open()
        })
    }

    function removeTaxes(id) {
        for (x in edit.item.invoice_line) {
            if (edit.item.invoice_line[x].for_article == id) {
                edit.item.invoice_line.splice(x, 1);
                removeTaxes(id)
            }
        }
    }
    edit.calcInvoice = function() {
        editItem.calcInvoiceTotal(edit.item)
    }

    function changeCurrency() {
        var data = {
            'do': 'invoice-recurring_ninvoice',
            recurring_invoice_id: edit.item.recurring_invoice_id,
            invoice_id: edit.item.invoice_id,
            buyer_id: edit.item.buyer_id,
            'currency_type': edit.item.currency_type,
            'change_currency': '1'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            if (r && r.data) {
                edit.item.currency_type = r.data.currency_type;
                edit.item.currency_val = r.data.currency_val;
                edit.item.currency_rate = r.data.currency_rate;
                edit.item.total_currency_hide = r.data.total_currency_hide;
                edit.item.total_default_currency = helper.displayNr(helper.return_value(edit.item.total) * helper.return_value(r.data.currency_rate))
            }
        });
        edit.calcInvoice()
    }
    edit.changeVat = function() {
        if (edit.item.no_vat == !0) {
            edit.item.no_vat = !1;
            edit.item.show_vat = !0;
            edit.item.vat = helper.displayNr(0);
            edit.item.hide_vattd = !1;
            edit.item.remove_vat = 1;
            for (x in edit.item.vat_line) {
                edit.item.vat_line[x].vat_percent = helper.displayNr(0)
            }
            for (x in edit.item.invoice_line) {
                edit.item.invoice_line[x].vat = helper.displayNr(0);
                edit.item.invoice_line[x].item_width += 1
            }
            edit.item.item_width += 1
        } else {
            for (x in edit.item.invoice_line) {
                edit.item.invoice_line[x].item_width -= 1
            }
            edit.item.item_width -= 1;
            edit.item.no_vat = !0;
            edit.item.show_vat = !1;
            edit.item.hide_vattd = !0;
            edit.item.remove_vat = 0
        }
        edit.calcInvoice()
    }
    edit.vatApplyAll = function() {
        for (x in edit.item.vat_line) {
            edit.item.vat_line[x].vat_percent = helper.displayNr(helper.return_value(edit.item.vat))
        }
        for (x in edit.item.invoice_line) {
            edit.item.invoice_line[x].vat = helper.displayNr(helper.return_value(edit.item.vat))
        }
        edit.calcInvoice()
    }
    edit.changeDiscount = function(val) {
        var val = edit.item.apply_discount;
        if (edit.item.apply_discount_line && !edit.item.apply_discount_global) {
            edit.item.apply_discount = 1
        } else if (edit.item.apply_discount_global && !edit.item.apply_discount_line) {
            edit.item.apply_discount = 2
        } else if (edit.item.apply_discount_line && edit.item.apply_discount_global) {
            edit.item.apply_discount = 3
        } else if (!edit.item.apply_discount_line && !edit.item.apply_discount_global) {
            edit.item.apply_discount = 0
        }
        edit.item.hide_global_discount = edit.item.apply_discount < 2 ? !1 : !0;
        edit.item.hide_line_global_discount = edit.item.apply_discount == 1 ? !0 : !1;
        edit.item.hide_discount_line = edit.item.apply_discount == 0 || edit.item.apply_discount == 2 ? !1 : !0;
        var line_width = 0;
        if ((edit.item.apply_discount == 0 || edit.item.apply_discount == 2) && (val == 1 || val == 3)) {
            edit.item.item_width += 1;
            line_width += 1
        } else if ((edit.item.apply_discount == 1 || edit.item.apply_discount == 3) && (val == 0 || val == 2)) {
            edit.item.item_width -= 1;
            line_width -= 1
        }
        for (x in edit.item.invoice_line) {
            edit.item.invoice_line[x].discount_line = edit.item.apply_discount == 0 ? helper.displayNr(helper.return_value(0)) : (edit.item.apply_discount == 1 || edit.item.apply_discount == 3 ? helper.displayNr(helper.return_value(edit.item.discount_line_gen)) : helper.displayNr(helper.return_value(edit.item.discount)));
            edit.item.invoice_line[x].hide_discount_line = edit.item.apply_discount == 1 || edit.item.apply_discount == 3 ? !0 : !1;
            edit.item.invoice_line[x].item_width += line_width
        }
        for (x in edit.item.vat_line) {
            edit.item.vat_line[x].view_discount = edit.item.apply_discount > 1 ? !0 : !1
        }
        edit.calcInvoice()
    }
    edit.changeDisc = function() {
        if (edit.item.apply_discount == 1) {
            for (x in edit.item.invoice_line) {
                edit.item.invoice_line[x].discount_line = helper.displayNr(helper.return_value(edit.item.discount_line_gen))
            }
        } else if (edit.item.apply_discount == 3) {
            for (x in edit.item.invoice_line) {
                edit.item.invoice_line[x].discount_line = helper.displayNr(helper.return_value(edit.item.discount_line_gen))
            }
        }
        edit.calcInvoice()
    }
    edit.cancel = function() {
        if (isNaN(edit.recurring_invoice_id)) {
            $state.go('recinvoices')
        } else {
            $state.go('recinvoice.view', {
                recurring_invoice_id: edit.recurring_invoice_id
            })
        }
    }
    edit.changePriceVat = function(val, index) {
        var value = helper.return_value(val) * 100 / (100 + parseFloat(helper.return_value(edit.item.invoice_line[index].vat), 10));
        edit.item.invoice_line[index].price = helper.displayNr(value, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
        edit.calcInvoice()
    }
    edit.changeLang = function() {
        var data = {
            'do': 'invoice-recurring_ninvoice',
            recurring_invoice_id: edit.item_id,
            'email_language': edit.item.email_language,
            'languages': edit.item.email_language,
            'xget': 'notes',
            'vat_regime_id': edit.item.vat_regime_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            if (r && r.data) {
                edit.item.translate_loop = r.data.translate_loop;
                edit.item.NOTES = r.data.NOTES;
                edit.item.notes2 = r.data.notes2;
                edit.item.translate_loop_custom = r.data.translate_loop_custom;
                edit.item.translate_lang_active = r.data.translate_lang_active;
                edit.item.languages = edit.item.email_language;
                edit.email.subject = r.data.subject;
                edit.email.text = r.data.text;
                if (edit.item.invoice_delivery == '1') {
                    edit.item.view_send_box = true;
                }
            }
        });
        var art_data = {
            'do': 'invoice-recurring_ninvoice',
            xget: 'articles_list',
            search: '',
            customer_id: edit.item.buyer_id,
            lang_id: edit.item.email_language,
            cat_id: edit.item.price_category_id,
            remove_vat: edit.item.remove_vat,
            vat_regime_id: edit.item.vat_regime_id
        };
        edit.requestAuto(edit, art_data, edit.renderArticleListAuto)
    }
    edit.setStartDate = function() {
        var initDate = new Date(edit.item.start_date).getTime() / 1000;
        edit.item.quote_ts = new Date((initDate + 7200) * 1000)
    }
    edit.addLine = function(content, title) {
        editItem.addInvoiceLine(edit.item, content, title)
    }
    edit.showAlerts = function(d) {
        helper.showAlerts(edit, d)
    }
    edit.save = function() {
        for (x in edit.email) {
            edit.item[x] = edit.email[x]
        }
        var data = angular.copy(edit.item);
        data = helper.clearData(data, ['APPLY_DISCOUNT_LIST', 'accmanager', 'addresses', 'articles_list', 'cc', 'contacts', 'country_dd', 'currency_type_list', 'language_dd', 'main_comp_info', 'multiple_identity_dd', 'seller_b_country_dd', 'seller_d_country_dd']);
        if (edit.item.recurring_invoice_id == 'tmp' || edit.item.duplicate_recurring_invoice_id) {
            data.do = 'invoice-recurring_ninvoice-invoice-add_recurring'
        } else {
            data.do = 'invoice-recurring_ninvoice-invoice-update_recurring'
        }
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            angular.element('.loading_wrap').addClass('hidden');
            //edit.showAlerts(r);
            if (r.success) {
                // edit.renderPage(r)
                //$state.go('recinvoices')
                $state.go('recinvoice.view', {
                    recurring_invoice_id: r.data.recurring_invoice_id
                });
            }
            if (r.error) {
                helper.showAlertsManual(edit, r);
            }
        })
    }
    edit.adjustPrice = function() {
        if (edit.item.adjust_prices) {
            edit.adjustP = [];
            var artadjust = '';
            for (x in edit.item.invoice_line) {
                if (edit.item.invoice_line[x].article_id != 0) {
                    artadjust += edit.item.invoice_line[x].article_id + ','
                }
            }
            var obj = {
                'do': 'invoice-recurring_ninvoice',
                'xget': 'articles_list',
                buyer_id: edit.item.buyer_id,
                lang_id: edit.item.email_language,
                cat_id: edit.item.price_category_id,
                remove_vat: edit.item.remove_vat,
                artadjust: artadjust
            };
            helper.doRequest('post', 'index.php', obj, function(r) {
                for (x in edit.item.invoice_line) {
                    for (y in r.data.lines) {
                        if (edit.item.invoice_line[x].article_id == r.data.lines[y].article_id) {
                            edit.adjustP.push(angular.copy(edit.item.invoice_line[x]));
                            edit.item.invoice_line[x].price = helper.displayNr(r.data.lines[y].price);
                            break
                        }
                    }
                }
                edit.calcInvoice()
            })
        } else {
            for (x in edit.item.invoice_line) {
                for (y in edit.adjustP) {
                    if (edit.item.invoice_line[x].article_id == edit.adjustP[y].article_id) {
                        edit.item.invoice_line[x].price = edit.adjustP[y].price;
                        break
                    }
                }
            }
            edit.calcInvoice()
        }
    }

    edit.showEmail = function() {
        var viewEmail = angular.copy(edit.email);
        viewEmail.e_subject = viewEmail.subject;
        viewEmail.e_message = viewEmail.text;
        var obj = {
            "app": "invoice",
            "app_mod": "recinvoice",
            "recipients": edit.auto.options.recipients,
            "use_html": edit.email.use_html,
            "xml_available": edit.item.xml_available,
            "is_file": false,
            "dropbox": { dropbox_is_file: false },
            "email": viewEmail
        };
        openModal("send_email", angular.copy(obj), "lg");
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'send_email':
                var iTemplate = 'SendEmail';
                var ctas = 'vm';
                break;
        }
        var params = {
            template: 'iTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'send_email':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        edit.email = d.email;
                        edit.email.subject = d.email.e_subject;
                        edit.email.text = d.email.e_message;
                        edit.auto.options.recipients = d.recipients;
                    }
                }
                break;
        }
        modalFc.open(params)
    }

    function initial() {
        edit.renderPage(data)
    }
    edit.initial()
}]).controller('remindersCtrl', ['$scope', 'helper', '$compile', 'list', '$uibModal', '$confirm', 'modalFc', '$timeout', 'data', function($scope, helper, $compile, list, $uibModal, $confirm, modalFc, $timeout, data) {
    console.log('Reminders');
    $scope.activeView = 0;
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'invoice-late_invoices',
        view: $scope.activeView,
        xget: '',
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.ord = '';
    $scope.reverse = !1;
    $scope.show_load = !0;
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'Active',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.renderList = function(d, index, bool) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.post_active = d.data.post_active;
            $scope.provider_ref = d.data.provider_ref;
            $scope.bPaid_reg = d.data.bPaid_reg;
            $scope.lr = d.data.lr;
            $scope.seller_data = d.data.seller_data;
            $scope.lg = d.data.lg;
            $scope.bpaid_active = d.data.bpaid_active;
            $scope.columns = d.data.columns;
            $scope.use_vat_invoices_kpi=d.data.use_vat_invoices_kpi;
            //$scope.show_deliveries = d.data.show_deliveries;
            //$scope.show_projects = d.data.show_projects;
            //$scope.show_interventions = d.data.show_interventions;

            if (index) {
                $timeout(function() {
                    $scope.listMore(index)
                })
            }
            if (bool) {
                $scope.show_load = !0;
                helper.doRequest('get', 'index.php', {
                    'do': 'misc-dashboard',
                    'xget': 'KPI'
                }, function(r) {
                    $scope.amount_to_be_inv_late=r.data.amount_to_be_inv_late;
                    $scope.show_load = !1;
                });
                helper.doRequest('get', 'index.php', {
                    'do': 'misc-post_library'
                }, function(r) {
                    $scope.post_library = r.data.post_library
                })
            }
        }
        //$scope.getAmounts();
    };

    $scope.getAmounts = function(doRefresh) {
        $scope.nr_show = !0;
        helper.doRequest('get', 'index.php', {
            'do': 'invoice-invoices',
            'xget': 'Amounts',
            'refresh_data': (doRefresh ? 1 : 0)
        }, function(r) {
            if (r.data.amount_o) {
                $scope.amount_o = r.data.amount_o
            }
            if (r.data.total_amount_p) {
                $scope.total_amount_p = r.data.total_amount_p
            }
            if (r.data.amount_s) {
                $scope.amount_s = r.data.amount_s
            }
            if (r.data.number_o) {
                $scope.number_o = r.data.number_o
            }
            if (r.data.date_o) {
                $scope.date_o = r.data.date_o
            }
            if (r.data.number_p) {
                $scope.number_p = r.data.number_p
            }
            if (r.data.amount_p) {
                $scope.amount_p = r.data.amount_p
            }
            if (r.data.amount_days) {
                $scope.amount_days = r.data.amount_days
            }
            if (r.data.date_p) {
                $scope.date_p = r.data.date_p
            }
            if (r.data.number_s) {
                $scope.number_s = r.data.number_s
            }
            if (r.data.date_s) {
                $scope.date_s = r.data.date_s
            }
            $scope.nr_show = !1;
            if (doRefresh) {
                angular.element('#refresh_amounts').removeClass("fa-spin fa-1x fa-fw");
            }
        });
    }

    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.createExports = function($event) {
        return list.exportFnc($scope, $event)
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.listMore = function(index) {
        angular.element('.orderTable tbody tr.collapseTr').eq(index).next().removeClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').addClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').removeClass('hidden');
        var pst_nr = 0;
        getPostStatus(index, pst_nr)
    }
    $scope.listLess = function(index) {
        angular.element('.orderTable tbody tr.collapseTr').eq(index).next().addClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').removeClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').addClass('hidden')
    }
    $scope.update_grade = function(item, dir) {
        var data = {};
        data.do = 'invoice--invoice-update_grade';
        data.dir = dir;
        data.invoice_id = item.invoice_id;
        helper.doRequest('post', 'index.php', data, function(r) {
            item.grade = r.data.new_nr
        })
    }
    $scope.bPaidFlow = function(parent_index, child_index) {
        if ($scope.bPaid_reg) {
            var data = {};
            data.invoice_id = $scope.list[parent_index].subdata[child_index].invoice_id;
            data.provider_ref = $scope.provider_ref;
            data.buyer_id = $scope.list[parent_index].subdata[child_index].buyer_id;
            data.contact_id = $scope.list[parent_index].subdata[child_index].contact_id;
            data.related = 'list';
            data.index = parent_index;
            data.lg = $scope.lg;
            for (x in $scope.search) {
                data[x] = $scope.search[x]
            }
            openModal('bPaidOptions', data, 'md')
        } else {
            var data = {
                'index': parent_index,
                'sub_index': child_index,
                'lg': $scope.lg
            };
            openModal('bPaidInfo', data, 'md')
        }
    }
    $scope.bPaid_status = function(inv_id) {
        openModal('bpaid_status', inv_id, 'lg')
    }
    $scope.remind_info = function(buyer_id) {
        openModal("reminder_status", buyer_id, "lg")
    }
    $scope.remind_sent_date = function(item, index) {
        item.index = index;
        openModal("edit_sent_date", item, "md")
    }
    $scope.remind_email = function(c_id) {
        openModal("send_email", c_id, "lg")
    }
    $scope.remind_contact = function(index) {
        openModal("reminder_contact", $scope.list[index], "md")
    }
    $scope.greenpost = function(c_id, index, with_names, initial) {
        if ($scope.post_active) {
            if (with_names && initial == 0) {
                var data = {};
                data.clicked_item = {
                    'c_id': c_id,
                    'index': index,
                    'with_names': with_names
                };
                data.contacts = $scope.list[index].contacts;
                openModal("reminder_contact", data, "md")
            } else {
                var data = {};
                data.do = 'invoice-late_invoices-invoice-postMultiple';
                data.customer_id = c_id;
                data.invoice_ids = [];
                for (i = 0; i < $scope.list[index].subdata.length; i++) {
                    data.invoice_ids.push($scope.list[index].subdata[i].invoice_id)
                }
                data.related = 'list';
                data.no_status = !0;
                data.post_library = $scope.post_library;
                openModal("resend_post", data, "md")
            }
        } else {
            var data = {};
            data.clicked_item = {
                'c_id': c_id,
                'index': index,
                'with_names': with_names
            };
            for (x in $scope.search) {
                data[x] = $scope.search[x]
            }
            openModal("post_register", data, "md")
        }
    }

    function getPostStatus(index, nr) {
        var temp = angular.copy($scope.list[index].subdata);
        if (nr < temp.length) {
            if (temp[nr].postgreen_id != "") {
                var data = {};
                data.do = 'invoice--invoice-postInvoiceData';
                data.invoice_id = temp[nr].invoice_id;
                data.related = "minilist";
                helper.doRequest('post', 'index.php', data, function(r) {
                    angular.element('.orderTable tbody tr.collapseTr').eq(index).next().find('tbody').children('tr').eq(nr).find('.pg_status').empty().html(r.data.status);
                    nr++;
                    getPostStatus(index, nr)
                })
            } else {
                nr++;
                getPostStatus(index, nr)
            }
        }
    }

    $scope.columnSettings = function() {
        openModal('column_set', {}, 'lg')
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'reminder_status':
                var iTemplate = 'ReminderStatus';
                var ctas = 'remstatus';
                break;
            case 'send_email':
                var iTemplate = 'ReminderEmail';
                var ctas = 'remail';
                break;
            case 'bpaid_status':
                var iTemplate = 'BePaidStatus';
                var ctas = 'bPay';
                break;
            case 'bPaidRegister':
                var iTemplate = 'BePaid';
                var ctas = 'bPay';
                break;
            case 'bPaidInfo':
                var iTemplate = 'BePaidInfo';
                var ctas = 'bPay';
                break;
            case 'bPaidOptions':
                var iTemplate = 'BePaidOptions';
                var ctas = 'bPay';
                break;
            case 'post_register':
                var iTemplate = 'PostRegister';
                var ctas = 'post';
                break;
            case 'reminder_contact':
                var iTemplate = 'ReminderContact';
                var ctas = 'cont';
                break;
            case 'resend_post':
                var iTemplate = 'ResendPostGreen';
                var ctas = 'post';
                break;
            case 'edit_sent_date':
                var iTemplate = 'ReminderSentDate';
                var ctas = 'remsdate';
                break
        }
        var params = {
            template: 'iTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'send_email':
                params.params = {};
                for (x in $scope.search) {
                    params.params[x] = $scope.search[x]
                }
                params.params['do'] = 'invoice-' + type;
                params.params.c_id = item;
                params.callback = function(data) {
                    if (data) {
                        $scope.searchThing();
                        /*if (data.data) {
                            $scope.list = data.data.query
                        }
                        $scope.showAlerts(data)*/
                    }
                }
                break;
            case 'reminder_status':
                params.params = {
                    'do': 'invoice-' + type,
                    customer_id: item
                };
                break;
            case 'bpaid_status':
                params.params = {
                    'do': 'invoice--invoice-bePaidData',
                    invoice_id: item
                };
                break;
            case 'bPaidRegister':
                params.item = angular.copy($scope.seller_data);
                for (x in $scope.search) {
                    params.item[x] = $scope.search[x]
                }
                params.item.do = 'invoice-late_invoices-invoice-bpay_register';
                params.item.index = item.index;
                params.item.sub_index = item.sub_index;
                params.callback = function(data) {
                    if (data) {
                        if (data.data) {
                            $scope.renderList(data, data.index);
                            var new_data = {};
                            new_data.invoice_id = $scope.list[data.index].subdata[data.sub_index].invoice_id;
                            new_data.provider_ref = $scope.provider_ref;
                            new_data.buyer_id = $scope.list[data.index].subdata[data.sub_index].buyer_id;
                            new_dataa.contact_id = $scope.list[data.index].subdata[data.sub_index].contact_id;
                            new_data.related = 'list';
                            new_data.index = data.index;
                            new_data.lg = $scope.lg;
                            for (x in $scope.search) {
                                new_data[x] = $scope.search[x]
                            }
                            openModal('bPaidOptions', new_data, 'md')
                        }
                        $scope.showAlerts(data)
                    }
                }
                break;
            case 'bPaidInfo':
                params.item = item;
                params.callback = function(data) {
                    if (data) {
                        openModal('bPaidRegister', data, 'md')
                    }
                }
                break;
            case 'bPaidOptions':
                params.item = angular.copy(item);
                params.item.do = 'invoice-late_invoices-invoice-bPaid_notification';
                params.callback = function(data) {
                    if (data && data.data) {
                        $scope.renderList(data, data.index)
                    }
                }
                break;
            case 'post_register':
                params.item = item;
                params.item.do = 'invoice-late_invoices-invoice-postRegister';
                params.callback = function(data) {
                    if (data && data.data) {
                        $scope.renderList(data);
                        $scope.greenpost(data.clicked_item.c_id, data.clicked_item.index, data.clicked_item.with_names, 0)
                    }
                    $scope.showAlerts(data)
                }
                break;
            case 'reminder_contact':
                params.item = item;
                params.callback = function(data) {
                    if (data) {
                        $scope.greenpost(data.clicked_item.c_id, data.clicked_item.index, data.clicked_item.with_names, 1)
                    }
                }
                break;
            case 'resend_post':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.request_type = 'post';
                params.params = {
                    'do': 'invoice--invoice-postInvoiceData',
                    'related': 'status',
                    'old_obj': item
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.showAlerts(d)
                    }
                }
                break;
            case 'edit_sent_date':
                params.params = {
                    'do': 'invoice-' + type,
                    customer_id: item.b_id,
                    index: item.index
                };
                params.callback = function(d) {
                    if (d.data) {
                        if (d.data.sent_date) {
                            $scope.list[d.data.index].date_sent = d.data.formated_sent_date;
                            $scope.list[d.data.index].is_date = !0
                        }
                    }
                }
                break;
            case 'column_set':
                params.template = 'miscTemplate/ColumnSettingsModal';
                params.ctrl = 'ColumnSettingsModalCtrl';
                params.params = {
                    'do': 'misc-column_set',
                    'list': 'reminders'
                };
                params.ctrlAs = 'vm';
                params.callback = function(d) {
                    if (d) {
                        helper.doRequest('get', 'index.php', {
                            'do': 'invoice-late_invoices',
                            'xget': 'reminders_cols'
                        }, function(o) {
                            $scope.columns = o.data;
                            $scope.searchThing()
                        })
                    }
                }
                break;
        }
        modalFc.open(params)
    }
    list.init($scope, 'invoice-late_invoices', $scope.renderList);

    //$scope.renderList(data, null, !0)
}]).controller('ReminderStatusModalCtrl', ['$scope', '$state', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, $state, helper, $uibModalInstance, data, modalFc) {
    var remstatus = this;
    remstatus.obj = data;
    remstatus.list = data.reminders;
    remstatus.title = data.title;
    remstatus.cancel = function() {
        $uibModalInstance.close()
    }
    remstatus.go_link = function(inv_id) {
        $uibModalInstance.close();
        $state.go('invoice.view', {
            invoice_id: inv_id
        })
    }
}]).controller('ReminderEmailModalCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, $timeout, helper, $uibModalInstance, data, modalFc) {
    var remail = this;
    remail.obj = data;
    remail.obj.auto = {
        options: {
            recipients: []
        }
    };
    remail.obj.auto.options.recipients = data.recipients;
    remail.obj.email = {
        recipients: []
    };
    if (Array.isArray(data.contact_email_data)) {
        remail.obj.email.recipients = data.contact_email_data;
    } else {
        remail.obj.email.recipients.push(data.contact_email_data);
    }
    delete remail.obj.recipients;
    remail.contactForEmailAutoCfg = {
        valueField: 'recipient_email',
        labelField: 'recipient_email',
        searchField: ['recipient_email'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select recipient'),
        create: !0,
        createOnBlur: true,
        maxItems: 100
    };
    if (remail.obj.has_contacts) {
        remail.ccCfg = {
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            delimiter: '|',
            placeholder: helper.getLanguage('Select contact'),
            create: !0,
            maxItems: 1,
            onChange(value) {
                if (value != undefined) {
                    var fname = this.options[value].firstname;
                    var lname = this.options[value].lastname;
                    var title = this.options[value].title;
                    remail.obj.subject = remail.obj.subject.replace('[!FIRST_NAME!]', fname);
                    remail.obj.subject = remail.obj.subject.replace('[!LAST_NAME!]', lname);
                    remail.obj.subject = remail.obj.subject.replace('[!SALUTATION!]', title);
                    var contentToShow = remail.obj.e_message;
                    contentToShow = contentToShow.replace('[!FIRST_NAME!]', fname);
                    contentToShow = contentToShow.replace('[!LAST_NAME!]', lname);
                    contentToShow = contentToShow.replace('[!SALUTATION!]', title);
                    tinyMCE.activeEditor.setContent(contentToShow)
                }
            }
        };
        remail.obj.auto.options.contacts = data.contacts;
        delete remail.obj.contacts
    }
    remail.tinymceOptions = {
        selector: ".mail_send",
        resize: "height",
        height: 184,
        menubar: !1,
        relative_urls: !1,
        toolbar: !1
    };
    remail.showAlerts = function(d) {
        helper.showAlerts(remail, d)
    }
    remail.cancel = function() {
        $uibModalInstance.close()
    }
    remail.ok = function(r) {
        remail.save_disabled=true;
        var data = angular.copy(r);
        var content_e_message = tinyMCE.activeEditor.getContent();
        data.e_message = content_e_message;
        if (!data.has_contacts || (data.has_contacts && data.contact_selected && data.contact_selected.length)) {
            if (data.email.recipients.length) {
                if(!remail.alerts){
                    remail.alerts=[];
                }
                data.do = 'invoice-late_invoices-invoice-noticeNew';
                data.new_email = data.email.recipients[0];
                remail.alerts.push({
                    type: 'email',
                    msg: data.new_email + ' - '
                });
                data.many_email_addresses = data.email.recipients.length > 1 ? 1 : 0;
                helper.doRequest('post', 'index.php', data, function(o) {
                    //remail.showAlerts(o);
                    if (o.success && o.success.success) {
                        remail.alerts[remail.alerts.length - 1].type = 'success';
                        remail.alerts[remail.alerts.length - 1].msg += ' ' + o.success.success
                    }
                    if (o.notice && o.notice.notice) {
                        remail.alerts[remail.alerts.length - 1].type = 'info';
                        remail.alerts[remail.alerts.length - 1].msg += ' ' + o.notice.notice
                    }
                    if (o.error && o.error.error) {
                        remail.alerts[remail.alerts.length - 1].type = 'danger';
                        remail.alerts[remail.alerts.length - 1].msg += ' ' + o.error.error
                    }
                    data.email.recipients.shift();

                    //data.many_email_addresses = 1;
                    if (data.new_email == data.user_loged_email) {
                        data.copy = !1;
                    }
                    remail.ok(data)

                })
            } else if (data.user_loged_email && data.copy) {                
                data.new_email = data.user_loged_email;
                data.copy = !1;
                if (data.new_email) {
                    if(!remail.alerts){
                        remail.alerts=[];
                    }
                    remail.alerts.push({
                        type: 'email',
                        msg: data.new_email + ' - '
                    });
                    data.do = 'invoice-late_invoices-invoice-noticeNew';
                    data.many_email_addresses = data.email.recipients.length > 1 ? 1 : 0;
                    helper.doRequest('post', 'index.php', data, function(o) {
                        //remail.showAlerts(o);
                        if (o.success && o.success.success) {
                            remail.alerts[remail.alerts.length - 1].type = 'success';
                            remail.alerts[remail.alerts.length - 1].msg += ' ' + o.success.success
                        }
                        if (o.notice && o.notice.notice) {
                            remail.alerts[remail.alerts.length - 1].type = 'info';
                            remail.alerts[remail.alerts.length - 1].msg += ' ' + o.notice.notice
                        }
                        if (o.error && o.error.error) {
                            remail.alerts[remail.alerts.length - 1].type = 'danger';
                            remail.alerts[remail.alerts.length - 1].msg += ' ' + o.error.error
                        }
                        data.email.recipients.shift();
                        if (data.email.recipients.length) {
                            //data.many_email_addresses = 1;
                            if (data.new_email == data.user_loged_email) {
                                data.copy = !1;
                            }
                            remail.ok(data)
                        } else {
                            remail.save_disabled=false;
                            $timeout(function() {
                                $uibModalInstance.close(true)
                            }, '1000')
                        }
                    })
                }
            } else {
                remail.save_disabled=false;
                $timeout(function() {
                    $uibModalInstance.close(true)
                }, '1000')
            }
        } else {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Select contact")
                },
                "notice": !1,
                "success": !1
            };
            remail.showAlerts(obj)
            remail.save_disabled=false;
        }
    }
}]).controller('ReminderContactModalCtrl', ['$scope', '$state', 'helper', '$uibModalInstance', '$window', 'data', 'modalFc', function($scope, $state, helper, $uibModalInstance, $window, data, modalFc) {
    var cont = this;
    cont.obj = data;
    cont.ccCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select contact'),
        create: !0,
        maxItems: 1,
    };
    cont.showAlerts = function(d) {
        helper.showAlerts(cont, d)
    }
    cont.cancel = function() {
        $uibModalInstance.close()
    }
    cont.ok = function() {
        var data = angular.copy(cont.obj);
        if (data.clicked_item) {
            var r = {};
            r.clicked_item = data.clicked_item;
            $uibModalInstance.close(r)
        } else {
            var link = data.pdf_link + '&contact_id_sent=' + data.contact_selected;
            $window.open(link, '_blank');
            $uibModalInstance.close();
            cont.obj.contact_selected = ""
        }
    }
}]).controller('reminderSettingsCtrl', ['$scope', '$state', 'helper', '$uibModal', 'modalFc', 'data', function($scope, $state, helper, $uibModal, modalFc, data) {
    console.log('setari rem');
    var set = this;
    set.rem = {};
    set.initial = initial;
    set.renderPage = renderPage;

    function renderPage(d) {
        if (d.data) {
            set.rem = d.data
        }
    }
    set.showAlerts = function(d) {
        helper.showAlerts(set, d)
    }
    set.tinymceOptions = {
        selector: ".mail_send",
        resize: "height",
        height: 184,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
    };
    set.changeLang = function() {
        var data = {};
        data.languages = set.rem.languages;
        helper.doRequest('get', 'index.php?do=invoice-late_settings', data, set.renderPage)
    }
    set.save = function() {
        var data = angular.copy(set.rem);
        data.do = 'invoice-late_settings-invoice-updateRemSettings';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            set.showAlerts(r);
            if (r.success) {
                set.renderPage(r)
            }
        })
    }

    function initial() {
        set.renderPage(data)
    }
    set.initial()
}]).controller('purchase_invoicesCtrl', ['$scope', '$rootScope','helper', 'list', '$uibModal', 'data', 'modalFc', function($scope, $rootScope, helper, list, $uibModal, data, modalFc) {
    console.log('Purchase Invoices');
    $rootScope.reset_lists.purchase_invoices=true;
    $scope.views = [{
        name: helper.getLanguage('All Statuses'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Not Paid'),
        id: 1,
        active: ''
    }, {
        name: helper.getLanguage('Credit Invoices'),
        id: 4,
        active: ''
    }, {
        name: helper.getLanguage('Late'),
        id: 2,
        active: ''
    }, {
        name: helper.getLanguage('Uploaded'),
        id: 5,
        active: ''
    }, {
        name: helper.getLanguage('Approved'),
        id: 6,
        active: ''
    }, {
        name: helper.getLanguage('Paid'),
        id: 3,
        active: ''
    }, {
        name: helper.getLanguage('Uploaded and outstanding'),
        id: 7,
        active: ''
    }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.nav = [];
    $scope.search = {
        search: '',
        'do': 'invoice-purchase_invoices',
        view: $scope.activeView,
        xget: '',
        offset: 1,
        archived: 0
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.ord = '';
    $scope.reverse = !1;
    $scope.show_load = !0;
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'Active',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };

    $scope.filterCfgType = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All types'),
        create: !1,
        onChange(value) {
            $scope.toggleSearchButtons('doc_type', value)
        },
        maxItems: 1
    };

    $scope.filterCfgExp = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Expense Categories'),
        create: !1,
        onChange(value) {
            $scope.toggleSearchButtons('expense_category_id', value)
        },
        maxItems: 1
    };
    $scope.minimum_selected=false;
    $scope.all_pages_selected=false;
    $scope.renderList = function(d, bool = !1) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.types = d.data.types;
            $scope.expense_categories = d.data.expense_categories;
            $scope.max_rows = d.data.max_rows;
            $scope.post_active = d.data.post_active;
            $scope.lr = d.data.lr;
            $scope.show_deliveries = d.data.show_deliveries;
            $scope.show_projects = d.data.show_projects;
            $scope.show_interventions = d.data.show_interventions;
            $scope.columns = d.data.columns;
            $scope.minimum_selected=d.data.minimum_selected;
            $scope.all_pages_selected=d.data.all_pages_selected;
            $scope.nav = d.data.nav;
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['invoice_id']);
                count ++;
            }
            sessionStorage.setItem('purchase_invoice.view_total', count);
            sessionStorage.setItem('purchase_invoice.view_list', JSON.stringify(ids));
            //$scope.initCheckAll();
            if (bool) {
                $scope.show_load = !0;
                helper.doRequest('post', 'index.php', {
                    'do': 'invoice-purchase_invoices',
                    'xget': 'Amounts'
                }, function(r) {
                    if (r.data.inv_data) {
                        $scope.inv_data = r.data.inv_data
                    }
                    $scope.show_load = !1
                })
            }
            $scope.getAmounts();
        }
    };

    $scope.getAmounts = function(doRefresh) {
        $scope.nr_show = !0;
        helper.doRequest('get', 'index.php', {
            'do': 'invoice-invoices',
            'xget': 'Amounts',
            'refresh_data': (doRefresh ? 1 : 0)
        }, function(r) {
            if (r.data.amount_o) {
                $scope.amount_o = r.data.amount_o
            }
            if (r.data.total_amount_p) {
                $scope.total_amount_p = r.data.total_amount_p
            }
            if (r.data.amount_s) {
                $scope.amount_s = r.data.amount_s
            }
            if (r.data.number_o) {
                $scope.number_o = r.data.number_o
            }
            if (r.data.date_o) {
                $scope.date_o = r.data.date_o
            }
            if (r.data.number_p) {
                $scope.number_p = r.data.number_p
            }
            if (r.data.amount_p) {
                $scope.amount_p = r.data.amount_p
            }
            if (r.data.amount_days) {
                $scope.amount_days = r.data.amount_days
            }
            if (r.data.date_p) {
                $scope.date_p = r.data.date_p
            }
            if (r.data.number_s) {
                $scope.number_s = r.data.number_s
            }
            if (r.data.date_s) {
                $scope.date_s = r.data.date_s
            }
            $scope.nr_show = !1;
            if (doRefresh) {
                angular.element('#refresh_amounts').removeClass("fa-spin fa-1x fa-fw");
            }
        });
    }

    $scope.pay = function(item) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'iTemplate/RecPayIncommingModal',
            controller: 'RecPayIncommingModalCtrl as recpayincomming',
            resolve: {
                data: function() {
                    var d = {};
                    for (x in $scope.search) {
                        d[x] = $scope.search[x]
                    }
                    d.do = 'invoice-recordPaymentIncomming';
                    d.invoice_id = item.id;
                    d.from_list = "1";
                    return helper.doRequest('get', 'index.php', d).then(function(d) {
                        return d.data
                    })
                }
            }
        });
        modalInstance.result.then(function(d) {
            if (d) {
                $scope.renderList(d)
            }
        }, function() {})
    };
    $scope.payMultiple = function() {
        helper.doRequest('get', 'index.php', { 'do': 'invoice-recordPaymentIncommingMultiple', 'xget': 'verify_bulk_payment' }, function(r) {
            if (r.error) {
                helper.showAlerts($scope, r);
            } else {
                openModal('pay_multiple', {}, 'md');
            }
        });
    };
    $scope.archived = function(value) {
        $scope.search.archived = value;
        list.filter($scope, value, $scope.renderList, true)
       // list.search($scope, $scope.renderList, $scope, true);
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList,true)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList,true)
    };
    $scope.searchThing = function(reset_list) {
        angular.element('.loading_wrap').removeClass('hidden');
        var params=angular.copy($scope.search);
        if(reset_list){
            params.reset_list='1';
        }
        list.search(params, $scope.renderList, $scope)
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.createExports = function($event) {
        list.exportFnc($scope, $event)
        var params = angular.copy($scope.search);
        params.do = 'invoice-purchase_invoices'
        params.exported = !0;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r);
        })
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.saveForPDF = function(p) {
        list.saveForPDF($scope, 'invoice--invoice-saveAddToPurchase', p);
        if (p && !p.check_add_to_product) {
            $scope.listObj.check_add_all = false;
        }
    }

    $scope.markCheckNew=function(i){
        list.markCheckNew($scope, 'invoice--invoice-saveAddToPurchase', i);
    }
    $scope.checkAllPage=function(){
        $scope.listObj.check_add_all=true;
        $scope.markCheckNew();
    }
    $scope.checkAllPages=function(){
        list.markCheckNewAll($scope, 'invoice--invoice-saveAddToPurchaseAll','1');
    }
    $scope.uncheckAllPages=function(){
        list.markCheckNewAll($scope, 'invoice--invoice-saveAddToPurchaseAll','0');
    }

    $scope.initCheckAll = function() {
        var total_checked = 0;
        for (x in $scope.list) {
            if ($scope.list[x].check_add_to_product) {
                total_checked++;
            }
        }
        if ($scope.list.length > 0 && $scope.list.length == total_checked) {
            $scope.listObj.check_add_all = true;
        } else {
            $scope.listObj.check_add_all = false;
        }
    }
    $scope.useVatKPI = function(value) {
        var obj = angular.copy($scope.search);
        obj.use_vat = value;
        obj.do = 'invoice-purchase_invoices-invoice-setVatKPIPurchase';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.renderList(r, !0)
            }
        })
    }
    $scope.columnSettings = function() {
        openModal('column_set', {}, 'lg')
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'column_set':
                var iTemplate = 'ColumnSettings';
                var ctas = 'vm';
                break;
            case 'pay_multiple':
                var iTemplate = 'RecPayIncomming';
                var ctas = 'recpayincomming';
                break;
        }
        var params = {
            template: 'iTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'column_set':
                params.template = 'miscTemplate/ColumnSettingsModal';
                params.ctrl = 'ColumnSettingsModalCtrl';
                params.params = {
                    'do': 'misc-column_set',
                    'list': 'purchase_invoices'
                };
                params.callback = function(d) {
                    if (d) {
                        helper.doRequest('get', 'index.php', {
                            'do': 'invoice-purchase_invoices'
                        }, function(o) {
                            $scope.columns = o.data;
                            $scope.searchThing()
                        })
                    }
                }
                break;
            case 'pay_multiple':
                params.params = { 'do': "invoice-recordPaymentIncommingMultiple" };
                params.callback = function(d) {
                    if (d) {
                        $scope.searchThing();
                    }
                }
                break;
        }
        modalFc.open(params)
    }

    list.init($scope, 'invoice-purchase_invoices', $scope.renderList,($rootScope.reset_lists.purchase_invoices ? true : false));
    $rootScope.reset_lists.purchase_invoices=false;
}]).controller('Purchase_invoiceCtrl', ['$scope', 'helper', function($scope, helper) {
    $scope.serial_number = '';
    $scope.page_title = helper.getLanguage('Invoice');
    $scope.isAddPage = !0
}]).controller('Purchase_invoiceViewCtrl', ['$scope', '$state', 'helper', '$timeout', '$stateParams', '$uibModal', 'modalFc', 'data', function($scope, $state, helper, $timeout, $stateParams, $uibModal, modalFc, data) {
    var view = this,
        typePromise, pdfDoc = null,
        pageNum = 1,
        pageRendering = !1,
        pageNumPending = null,
        scale = 1.5,
        canvas = document.getElementById('the-canvas'),
        ctx = canvas.getContext('2d');
    view.invoice_id = $stateParams.invoice_id;
    $scope.$parent.isAddPage = !0;
    view.invoice = [];
    view.invoice.item_exists = true;
    var baseshowTxt = {
        sendEmail: !1,
        markAsReady: !1,
        viewNotes: !1,
        marsAsSent: !1,
        showPayment: !1,
        creditInvOpt: !1,
        addPorder: !1,
        markFinal: !1,
    };
    var date = new Date();
    view.format = 'dd/MM/yyyy';
    view.datePickOpen = {
        sent_date: !1,
        invoice_date: !1,
        payment_date: !1
    };
    view.dateOptions = {
        dateFormat: '',
    };
    view.email = {};
    view.item = {};
    view.auto = {
        options: {
            recipients: []
        }
    };
    view.openModal = openModal;
    view.renderPage = function(d) {
        view.showTxt = angular.copy(baseshowTxt);
        if (d.data) {
            if(!d.data.item_exists){
                view.invoice.item_exists = false;
                return 0;
            }; 
            if (d.data.redirect) {
                $state.go(d.data.redirect);
                return !1
            }
            view.invoice = d.data;
            $scope.$parent.serial_number = view.invoice.booking_number;
            helper.updateTitle($state.current.name, view.invoice.booking_number);
            $scope.$parent.type_title = view.invoice.type_title;
            $scope.$parent.page_title = view.invoice.page_title;
            $scope.$parent.nr_status = view.invoice.nr_status;
            if (view.invoice.invoice_date) {
                view.invoice.invoice_date = new Date(view.invoice.invoice_date)
            }
            if (view.invoice.payment_date) {
                view.invoice.payment_date = new Date(view.invoice.payment_date)
            }
            view.auto.options.recipients = d.data.recipients;
            view.item.orders = d.data.orders;
            view.email = {
                e_subject: view.invoice.subject,
                e_message: view.invoice.e_message,
                email_language: view.invoice.languages,
                include_pdf: view.invoice.include_pdf,
                invoice_id: view.invoice_id,
                files: view.invoice.files_row,
                use_html: view.invoice.use_html,
                copy_acc: view.invoice.copy_acc,
                recipients: []
            };
            view.email.recipients.push(d.data.contact_email_data);
            view.showAlerts(d);
            if (view.invoice.link_url && !view.invoice.is_image) {
                angular.element('#quotePreview center').show();
                angular.element('#bxWrapper').addClass('hide');
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                PDFJS.workerSrc = 'js/libraries/pdf.worker.js';
                PDFJS.getDocument(view.invoice.link_url).then(function(pdfDoc_) {
                    pdfDoc = pdfDoc_;
                    document.getElementById('page_count').textContent = '/ ' + pdfDoc.numPages;
                    renderPage(pageNum)
                })
            } else if (view.invoice.link_url && view.invoice.is_image) {
                angular.element('#quotePreview center').show();
                angular.element('#bxWrapper').addClass('hide');
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                var img = new Image();
                img.src = view.invoice.file_link;
                img.onload = function() {
                    renderImage(canvas, ctx, img)
                }
            }
        }
    }

    function renderPage(num) {
        pageRendering = !0;
        pdfDoc.getPage(num).then(function(page) {
            var viewport = page.getViewport(scale);
            canvas.height = viewport.height;
            canvas.width = viewport.width;
            var renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };
            var renderTask = page.render(renderContext);
            renderTask.promise.then(function() {
                pageRendering = !1;
                if (pageNumPending !== null) {
                    renderPage(pageNumPending);
                    pageNumPending = null
                }
            })
        });
        document.getElementById('page_num').value = pageNum;
        $('#quotePreview center').hide();
        $('#bxWrapper').removeClass('hide')
    }

    function renderImage(canvas, ctx, img) {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0);
        $('#quotePreview center').hide();
        $('#bxWrapper').removeClass('hide')
    }

    function queueRenderPage(num) {
        if (pageRendering) {
            pageNumPending = num
        } else {
            renderPage(num)
        }
    }

    function onPrevPage(e) {
        e.preventDefault();
        if (pageNum <= 1) {
            return
        }
        pageNum--;
        queueRenderPage(pageNum)
    }
    document.getElementById('prev').addEventListener('click', onPrevPage);

    function onNextPage(e) {
        e.preventDefault();
        if (pageNum >= pdfDoc.numPages) {
            return
        }
        pageNum++;
        queueRenderPage(pageNum)
    }
    document.getElementById('next').addEventListener('click', onNextPage);
    view.porderAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: 'Select Purchase Order',
        create: !1,
        maxOptions: 6,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-8">' + '<strong class="text-ellipsis">' + escape(item.value) + ' </strong>' + '</div>' + '<div class="col-sm-4 text-muted text-right">' + '<small class="text-ellipsis"><span class="glyphicon glyphicon-map-marker"></span> ' + escape(item.country) + '</small>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#inputPorder')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'invoice-order_auto_complete',
                    current_id: view.invoice.c_id,
                    term: str
                };
                view.requestAuto(data, view.renderPorderAuto)
            }, 300)
        },
        onChange(value) {
            if (value == 0) {
                return !1
            }
            if (view.item.order_id == undefined) {
                var data = {
                    'do': 'invoice-order_auto_complete',
                    current_id: view.invoice.c_id
                };
                helper.doRequest('get', 'index.php', data, function(o) {
                    view.item.orders = o.data
                })
            } else {
                var data = {
                    'do': 'invoice-purchase_invoice-invoice-updatePurchaseInvoiceOrder',
                    'invoice_id': view.invoice_id,
                    'order_id': value
                };
                helper.doRequest('get', 'index.php', data, function(o) {
                    view.renderPage(o);
                    view.item.order_id = ''
                })
            }
        },
        maxItems: 1
    };
    view.requestAuto = requestAuto;
    view.renderPorderAuto = renderPorderAuto;

    function requestAuto(data, renderer) {
        helper.doRequest('get', 'index.php', data, renderer)
    }

    function renderPorderAuto(d) {
        if (d.data) {
            view.item.order = d.data;
            var selectize = angular.element('#inputPorder')[0].selectize;
            selectize.open()
        }
    }
    view.next = function() {
        view.showTxt.markFinal = !view.showTxt.markFinal
    }
    view.markFinal = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        var data = {
            'do': 'invoice-purchase_invoice-invoice-setFinal',
            'invoice_id': view.invoice_id
        };
        helper.doRequest('get', 'index.php', data, function(o) {
            view.renderPage(o)
        })
    }
    view.markDraft = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        var data = {
            'do': 'invoice-purchase_invoice-invoice-setDraft',
            'invoice_id': view.invoice_id
        };
        helper.doRequest('get', 'index.php', data, function(o) {
            view.renderPage(o)
        })
    }
    view.markApprove = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        var data = {
            'do': 'invoice-purchase_invoice-invoice-setApprove',
            'invoice_id': view.invoice_id
        };
        helper.doRequest('get', 'index.php', data, function(o) {
            view.renderPage(o)
        })
    }
    view.deletepayment = function(link) {
        helper.doRequest('get', link, view.renderPage)
    }

    view.openPaymentModal = function(payment_id) {
        var obj = { 'payment_id': payment_id };
        view.openModal("editPayment", obj, 'md');
    }

    view.deletepurchaseorder = function(link) {
        var obj = { 'orders': view.item.orders, 'invoice_id': view.invoice_id };
        helper.doRequest('get', link, view.renderPage)
    }
    view.addPorder = function() {
        view.showTxt.addPorder = !0
    }
    view.showAlerts = function(d, formObj) {
        helper.showAlerts(view, d, formObj)
    }

    view.linkToPo = function() {
        var obj = { 'orders': view.item.orders, 'invoice_id': view.invoice_id };
        view.openModal("purchaseInvoiceLinkPo", obj, 'md');
    }

    view.statusDocument = function(status) {
        var tmp_obj = { 'id': view.invoice_id, 'invoice_id': view.invoice_id };
        tmp_obj.do = status == '1' ? 'invoice-purchase_invoice-invoice-archive_purchase_invoice' : 'invoice-purchase_invoice-invoice-activate_purchase_invoice';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_obj, function(r) {
            if (r.success) {
                view.renderPage(r);
                helper.refreshLogging();
            } else {
                view.showAlerts(r);
            }
        });
    }

    view.deleteDocument = function() {
        var tmp_obj = { 'id': view.invoice_id, 'invoice_id': view.invoice_id };
        tmp_obj.do = 'invoice-purchase_invoices-invoice-incomming_invoices_delete';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_obj, function(r) {
            if (r.success) {
                helper.setItem('invoice-purchase_invoices', {});
                $state.go("purchase_invoices");
            } else {
                view.showAlerts(r);
            }
        });
    }

    view.editInvoice = function() {
        var tmp_obj = { 'id': view.invoice_id, 'invoice_id': view.invoice_id };
        tmp_obj.do = 'invoice--invoice-delete_purchase_invoice_payments';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_obj, function(r) {
            if (r.success) {
                $state.go('purchase_invoice.edit', {
                    incomming_invoice_id: view.invoice_id
                })
            } else {
                view.showAlerts(r);
            }
        });
    }
    view.saveData = function(formObj, func) {
        formObj.$invalid = !0;
        var obj = {'id': view.invoice_id};
        for (x in view.invoice) {
            obj[x] = view.invoice[x]
        }
        obj.do = 'invoice-invoice-invoice-' + func;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(d) {
            if (d.success) {
                if (func == 'add_nr_purchase') {
                    angular.element('.loading_wrap').removeClass('hidden');
                    $state.go('purchase_invoice.edit', {
                        incomming_invoice_id: d.data.invoice_id
                    })
                } else {
                    view.showTxt = angular.copy(baseshowTxt);
                    view.invoice_id = d.data.invoice_id;
                    view.renderPage(d)
                }

            } else {

                    view.showAlerts(d,formObj);
            }
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'recordPayment':
                var iTemplate = 'RecPayIncomming';
                var ctas = 'recpayincomming';
                var size = 'md';
                break;
            case 'purchaseInvoiceLinkPo':
                var iTemplate = 'PurchaseInvoiceLinkPo';
                var ctas = 'vm';
                var size = size;
                break;
            case 'editPayment':
                var iTemplate = 'PaymentIncomming';
                var ctas = 'editPay';
                var size = 'md';
                break;
        }
        var params = {
            template: 'iTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'recordPayment':
                params.params = {
                    'do': 'invoice-recordPaymentIncomming',
                    invoice_id: item
                };
                params.callback = function(data) {
                    if (data) {
                        if (data.data) {
                            // view.invoice = data.data
                            view.renderPage(data);
                        }
                        view.showAlerts(data)
                    }
                }
                break;
            case 'purchaseInvoiceLinkPo':
                params.item = item;
                params.callback = function(data) {
                    if (data) {
                        if (data.data) {
                            view.renderPage(data);
                        }
                        view.showAlerts(data)
                    }
                }
                break;
            case 'editPayment':
                params.params = { 'do': 'invoice-editPaymentIncommingModal', invoice_id: view.invoice_id, payment_id: item.payment_id };
                params.callback = function(data) {
                    if (data) {
                        if (data.data) {
                            view.renderPage(data);
                        }
                    }
                }
                break;
        }
        modalFc.open(params)
    }
    view.renderPage(data)
}]).controller('RecPayIncommingModalCtrl', ['$scope', 'helper', '$uibModalInstance', '$timeout', 'data', function($scope, helper, $uibModalInstance, $timeout, data) {
    var recpayincomming = this;
    var date = new Date();
    recpayincomming.format = 'dd/MM/yyyy';
    recpayincomming.datePickOpen = {
        payment_date: !1
    };
    recpayincomming.dateOptions = {
        dateFormat: '',
    };
    if (data.payment_date) {
        data.payment_date = data.payment_date = new Date(data.payment_date)
    }
    recpayincomming.obj = data;

    recpayincomming.ok = function(formObj) {
        var data = angular.copy(recpayincomming.obj);
        data.do = (recpayincomming.obj.bulk_pay ? 'invoice--invoice-pay_incomming_invoice_bulk' : 'invoice-recordPayment-invoice-pay_incomming_invoice');
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                recpayincomming.showAlerts(r, formObj)
            } else {
                $uibModalInstance.close(r)
            }
        })
    };
    recpayincomming.cancel = function() {
        $uibModalInstance.close()
    };
    recpayincomming.showAlerts = function(d, formObj) {
        helper.showAlerts(recpayincomming, d, formObj)
    };

    recpayincomming.activateButtonQRCode = function(formObj) {
        var data = angular.copy(recpayincomming.obj);
        var amount = parseFloat(helper.return_value(data.total_amount_due1));
        var cost = 0;
        if (data.cost) {
            cost = parseFloat(helper.return_value(data.cost));
            amount = amount + cost;
        }

        recpayincomming.obj.show_QRCode = false;
        recpayincomming.qrcode = '';

        $timeout(function() {
            //wait for display-nr directive to format the amount value
        }, 1000);

        if (!isNaN(amount) && amount != '0.00' && amount > 0) {
            recpayincomming.obj.disabled_QRCode_button = false;
            recpayincomming.obj.amount_cost = amount;
        } else {
            recpayincomming.obj.disabled_QRCode_button = true;
        }
    };

    recpayincomming.generateQRCode = function(formObj) {
        var data = angular.copy(recpayincomming.obj);
        data.do = 'invoice--invoice-getQRCode';
        $timeout(function() {
            //wait for display-nr directive to format the amount value
        }, 1000);
        var amount = 0;
        if (data.amount_cost && data.amount_cost != 'undefined') {
            amount = data.amount_cost;
        } else {
            amount = parseFloat(helper.return_value(data.total_amount_due1));
        }

        if (!isNaN(amount)) {
            recpayincomming.obj.show_QRCode = true;
            data.amount = amount;
            helper.doRequest('post', 'index.php', data, function(o) {
                if (o.success) {
                    if (o.data) {
                        recpayincomming.obj.show_QRCode = true;
                        recpayincomming.qrcode = '<img src="' + o.data.src + '" height="170" width="170"/>';
                        recpayincomming.obj.disabled_QRCode_button = true;
                    }
                } else {
                    recpayincomming.showAlerts(o);
                }
            })
        }

    };
}]).controller('exportAppsCtrl', ['$scope', '$state', '$uibModal', 'helper', 'list', 'data', function($scope, $state, $uibModal, helper, list, data) {
    console.log('Export general');
    $scope.list = [];
    $scope.renderPage = renderPage;
    $scope.initial = initial;

    function renderPage(d) {
        if (d.data) {
            $scope.list = d.data
        }
        $scope.show_deliveries = d.data.show_deliveries;
        $scope.show_projects = d.data.show_projects;
        $scope.show_interventions = d.data.show_interventions;

        $scope.getAmounts();
    }

    $scope.getAmounts = function(doRefresh) {
        $scope.nr_show = !0;
        helper.doRequest('get', 'index.php', {
            'do': 'invoice-invoices',
            'xget': 'Amounts',
            'refresh_data': (doRefresh ? 1 : 0)
        }, function(r) {
            if (r.data.amount_o) {
                $scope.amount_o = r.data.amount_o
            }
            if (r.data.total_amount_p) {
                $scope.total_amount_p = r.data.total_amount_p
            }
            if (r.data.amount_s) {
                $scope.amount_s = r.data.amount_s
            }
            if (r.data.number_o) {
                $scope.number_o = r.data.number_o
            }
            if (r.data.date_o) {
                $scope.date_o = r.data.date_o
            }
            if (r.data.number_p) {
                $scope.number_p = r.data.number_p
            }
            if (r.data.amount_p) {
                $scope.amount_p = r.data.amount_p
            }
            if (r.data.amount_days) {
                $scope.amount_days = r.data.amount_days
            }
            if (r.data.date_p) {
                $scope.date_p = r.data.date_p
            }
            if (r.data.number_s) {
                $scope.number_s = r.data.number_s
            }
            if (r.data.date_s) {
                $scope.date_s = r.data.date_s
            }
            $scope.nr_show = !1;
            if (doRefresh) {
                angular.element('#refresh_amounts').removeClass("fa-spin fa-1x fa-fw");
            }
        });
    }

    function initial() {
        $scope.renderPage(data)
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.showApp = function(item) {
        if (!item.activated) {
            if ($scope.list.can_activate) {
                var app = item.app == 'winbooks' ? 'btb' : item.app;
                $scope.modal_app(app, 'sTemplate/' + app, app + 'SetCtrl')
            } else {
                var obj = {
                    'error': {
                        'error': helper.getLanguage('Application has to be activated first by admin')
                    },
                    'notice': !1,
                    'success': !1
                };
                $scope.showAlerts(obj)
            }
            return !1
        }
        $state.go('export_btb', {
            'app': item.app
        })
    }
    $scope.modal_app = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    var get_data = {
                        'do': 'settings-settings',
                        'xget': r
                    };
                    return helper.doRequest('get', 'index.php', get_data).then(function(o) {
                        return o.data
                    })
                }
            }
        });
        modalInstance.result.then(function(data) {
            var is_activated = !1;
            for (x in data.data.integrated) {
                if (data.data.integrated[x].id == r && data.data.integrated[x].activ == "app_active") {
                    is_activated = !0;
                    break
                }
            }
            if (is_activated) {
                $state.go('export_btb', {
                    'app': r == 'btb' ? 'winbooks' : r
                })
            }
        }, function() {})
    };
    $scope.initial()
}]).controller('exportCsvsCtrl', ['$scope', 'helper', 'list', 'data', 'modalFc', function($scope, helper, list, data, modalFc) {
    console.log('Export Csv');
    $scope.activeView = 0;
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'invoice-export_csvs',
        view: $scope.activeView,
        xget: '',
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.ord = '';
    $scope.reverse = !1;
    $scope.renderList = function(d, index) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr
        }
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.createExports = function($event) {
        return list.exportFnc($scope, $event)
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.openModal = function(type, item, size) {
        switch (type) {
            case 'csv_settings':
                var iTemplate = 'CsvSettings';
                var ctas = 'set';
                break
        }
        var params = {
            template: 'iTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'csv_settings':
                params.params = {
                    'do': 'invoice-csv_settings'
                };
                break
        }
        modalFc.open(params)
    }
    $scope.renderList(data)
}]).controller('CsvSettingsModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'selectizeFc', function($scope, helper, $uibModalInstance, data, selectizeFc) {
    var set = this;
    set.obj = data;
    set.showAlerts = function(d) {
        helper.showAlerts(set, d)
    }
    set.cancel = function() {
        $uibModalInstance.close()
    };
    set.exportsettingup = function(item) {
        var obj = {
            'do': 'invoice--invoice-csvRegimes',
            'vat_id': item.id,
            'val': item.selected
        };
        helper.doRequest('post', 'index.php', obj)
    }
    set.exportsettingType = function(item) {
        var obj = {
            'do': 'invoice--invoice-csvTypes',
            'field_id': item.type,
            'val': item.selected
        };
        helper.doRequest('post', 'index.php', obj)
    }
}]).controller('exportCsvCtrl', ['$scope', 'helper', function($scope, helper) {}]).controller('exportCsvEditCtrl', ['$scope', '$stateParams', '$state', '$confirm', 'modalFc', 'helper', 'data', function($scope, $stateParams, $state, $confirm, modalFc, helper, data) {
    console.log('exportEdit');
    var edit = this,
        typePromise;
    edit.app = 'invoice';
    edit.ctrlpag = 'export';
    edit.tmpl = 'iTemplate';
    $scope.vm = {};
    $scope.vm.isAddPage = !1;
    edit.item_id = $stateParams.list_id;
    edit.list_id = edit.item_id;
    edit.renderPage = renderPage;
    edit.initial = initial;
    edit.item = {};
    edit.oldObj = {};

    function renderPage(d) {
        if (d && d.data) {
            edit.item = d.data
        }
    }
    edit.showAlerts = function(d, formObj) {
        helper.showAlerts(edit, d, formObj)
    }
    edit.addLine = function() {
        edit.item.custom_fields.push({
            "header": "",
            "value": ""
        })
    }
    edit.removeLine = function(index) {
        edit.item.custom_fields.splice(index, 1)
    }
    edit.save = function(formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(edit.item);
        data.list_id = edit.list_id;
        data.do = data.do_next;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(d) {
            if (d.success) {
                edit.list_id = d.data.list_id;
                edit.renderPage(d);
                edit.showAlerts(d)
            } else {
                edit.showAlerts(d, formObj)
            }
        })
    }

    function initial() {
        edit.renderPage(data)
    }
    edit.initial()
}]).controller('exportViewCtrl', ['$scope', '$stateParams', '$state', '$confirm', '$window', 'modalFc', 'helper', 'list', 'data', function($scope, $stateParams, $state, $confirm, $window, modalFc, helper, list, data) {
    console.log('Export View');
    $scope.list_id = $stateParams.list_id;
    $scope.views = [{
        name: helper.getLanguage('List'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Exported'),
        id: -1
    }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'invoice-export_view',
        view: $scope.activeView,
        xget: '',
        offset: 1,
        type: 0,
        list_id: $scope.list_id
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.ord = '';
    $scope.reverse = !1;
    $scope.checkboxAll = {
        checkIt: !1
    };
    $scope.initial = invoices;
    $scope.second = pinvoices;
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.title = d.data.title;
            $scope.export_link = d.data.export_link;
            /*var i = 0;
            for (x in $scope.list) {
                if ($scope.list[x].checked) {
                    i++
                }
            }
            if (i == $scope.list.length) {
                $scope.checkboxAll.checkIt = !0
            }*/
            $scope.initCheckAll();
        }
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        var obj = angular.copy($scope.search);
        angular.element('.loading_wrap').removeClass('hidden');
        $scope.checkboxAll.checkIt = !1;
        helper.doRequest('do', 'index.php', obj, $scope.renderList)
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.createExports = function($event) {
        return list.exportFnc($scope, $event)
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.checkAll = function() {
        var array = [];
        for (x in $scope.list) {
            $scope.list[x].checked = $scope.checkboxAll.checkIt;
            array.push($scope.list[x])
        }
        $scope.setForImport(array)
    }
    $scope.checkSingle = function(index) {
        $scope.checkboxAll.checkIt = !1;
        var i = 0;
        for (x in $scope.list) {
            if ($scope.list[x].checked) {
                i++
            }
        }
        if (i == $scope.list.length) {
            $scope.checkboxAll.checkIt = !0
        }
        var array = [];
        array.push($scope.list[index]);
        $scope.setForImport(array)
    }
    $scope.setForImport = function(array) {
        if (array.length) {
            /*angular.element('.loading_wrap').removeClass('hidden');
            var data = {};
            if (array[0].checked) {
                data.value = 1
            } else {
                data.value = 0
            }
            data.invoice_id = array[0].id;
            data.do = 'invoice--invoice-setForImport';
            helper.doRequest('do', 'index.php', data, function(o) {
                array.shift();
                $scope.setForImport(array)
            })*/
            var data = { 'list': [] };
            for (x in array) {
                data.list.push({ 'invoice_id': array[x].id, 'value': (array[x].checked ? 1 : 0) });
            }
            data.do = 'invoice--invoice-setForImport';
            helper.doRequest('post', 'index.php', data);
        }
        /*else {
                   angular.element('.loading_wrap').addClass('hidden')
               }*/
    }
    $scope.exportMultiple = function(type) {
        if (type == 1) {
            $window.open($scope.export_link, '_blank');
            $confirm({
                text: helper.getLanguage('After the download is complete check the export.') + helper.getLanguage('Click ok to remove the invoices from the list or cancel to leave them.')
            }).then(function() {
                var array = [];
                for (x in $scope.list) {
                    if ($scope.list[x].checked) {
                        array.push($scope.list[x])
                    }
                }
                $scope.exportMultipleUnique(array)
            })
        } else {
            var array = [];
            for (x in $scope.list) {
                if ($scope.list[x].checked) {
                    array.push($scope.list[x])
                }
            }
            $scope.exportMultipleUnique(array)
        }
    }
    $scope.exportMultipleUnique = function(array) {
        if (array.length) {
            var data = {};
            data.do = 'invoice-export_view-invoice-markExport';
            data.invoice_id = array[0].id;
            data.type = $scope.search.type;
            data.list_id = $scope.list_id;
            helper.doRequest('do', 'index.php', data, function(o) {
                array.shift();
                $scope.exportMultipleUnique(array)
            })
        } else {
            helper.doRequest('get', 'index.php', $scope.search, $scope.renderList)
        }
    }
    $scope.go_link = function(inv_id) {
        if ($scope.search.type == 0) {
            $state.go('invoice.view', {
                invoice_id: inv_id
            })
        } else {
            $state.go('purchase_invoice.view', {
                invoice_id: inv_id
            })
        }
    }
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Statuses'),
        create: !1,
        onItemAdd(value) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };

    $scope.changeView = function(value) {
        $scope.search.archived = 0;
        if (value == -1) {
            $scope.search.archived = 1
        }
        $scope.filters(value)
    }

    $scope.initCheckAll = function() {
        $scope.checkboxAll.checkIt = false;
        var total_checked = 0;
        for (x in $scope.list) {
            if ($scope.list[x].checked) {
                total_checked++;
            }
        }
        if ($scope.list.length > 0 && $scope.list.length == total_checked) {
            $scope.checkboxAll.checkIt = true;
        }
    }

    function invoices() {
        angular.element('.loading_wrap').removeClass('hidden');
        $scope.search.type = 0;
        helper.doRequest('get', 'index.php', $scope.search, $scope.renderList)
    }

    function pinvoices() {
        angular.element('.loading_wrap').removeClass('hidden');
        $scope.search.type = 1;
        helper.doRequest('get', 'index.php', $scope.search, $scope.renderList)
    }
    $scope.initial()
}]).controller('exportBtbAppCtrl', ['$rootScope','$scope', '$stateParams', '$state', '$confirm', '$window', 'modalFc', 'helper', 'list', 'data', function($rootScope,$scope, $stateParams, $state, $confirm, $window, modalFc, helper, list, data) {
    console.log('Export Btb');
    $rootScope.opened_lists.export_btb=true;
    $scope.app = $stateParams.app;
    $scope.views = [{
        name: helper.getLanguage('List'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Exported'),
        id: -1
    }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'invoice-export_btb',
        view: $scope.activeView,
        xget: '',
        offset: 1,
        type: 0,
        app: $scope.app,
        search: ''
    };
    $scope.listObj = {
        checkIt: !1,
        checkItUnmark: !1,
    };
    $scope.ord = '';
    $scope.reverse = !1;
    //$scope.checkIt = !1;
    //$scope.checkItR = !1;
    $scope.initial = invoices;
    $scope.second = pinvoices;
    $scope.openModal = openModal;
    $scope.minimum_selected=false;
    $scope.all_pages_selected=false;

    $scope.filterCfgType = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All types'),
        create: !1,
        onChange(value) {
            $scope.toggleSearchButtons('doc_type', value)
        },
        maxItems: 1
    };

    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.title = d.data.title;
            $scope.col_plus = d.data.col_plus;
            $scope.export_errors = d.data.export_errors;
            $scope.minimum_selected=d.data.minimum_selected;
            $scope.all_pages_selected=d.data.all_pages_selected;
            $scope.types=d.data.types;
            $scope.col_search = d.data.col_search;
            $scope.doc_type = d.data.doc_type;
            //$scope.initCheckAll();
        }
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList, true)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList, true)
    };
    $scope.searchThing = function(reset_list) {
        angular.element('.loading_wrap').removeClass('hidden');
        var params=angular.copy($scope.search);
        if(reset_list){
            params.reset_list='1';
        }     
        list.search(params, $scope.renderList, $scope)
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.createExports = function($event) {
        return list.exportFnc($scope, $event)
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.checkAll = function() {
        //$scope.checkIt = !$scope.checkIt;
        var array = [];
        for (x in $scope.list) {
            $scope.list[x].checked = $scope.listObj.checkIt;
            array.push($scope.list[x])
        }
        $scope.setForImport(array)
    }
    $scope.checkSingle = function(index) {
        if ($scope.list[index].checked) {
            var elem_counter = 0;
            for (x in $scope.list) {
                if ($scope.list[x].checked) {
                    elem_counter++;
                }
            }
            if (elem_counter == $scope.list.length && $scope.list.length > 0) {
                $scope.listObj.checkIt = true;
            }
        } else {
            $scope.listObj.checkIt = false;
        }
        var array = [];
        array.push($scope.list[index]);
        $scope.setForImport(array)
    }
    $scope.checkAllPage=function(){
        $scope.listObj.checkIt=true;
        $scope.checkAll();
    }
    $scope.checkAllPages=function(){
        for (x in $scope.list) {
            $scope.list[x].checked =  true;
        }
        var params = {
            'do': 'invoice--invoice-setForImportAll',
            'value': '1'
        };
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.all_pages_selected = r.data.all_pages_selected;
            $scope.minimum_selected = r.data.minimum_selected;
        });
    }
    $scope.uncheckAllPages=function(){
        for (x in $scope.list) {
            $scope.list[x].checked =  false;
        }
        var params = {
            'do': 'invoice--invoice-setForImportAll',
            'value': '0'
        };
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.all_pages_selected = r.data.all_pages_selected;
            $scope.minimum_selected = r.data.minimum_selected;
        });
    }
    $scope.markSelectedExported=function(){
        var params=angular.copy($scope.search);
        angular.element('.loading_wrap').removeClass('hidden');
        params.do='invoice-export_btb-invoice-markSelectedExport';
        helper.doRequest('post', 'index.php', params, $scope.renderList)
    }
    $scope.restoreSelectedExported=function(){
        var params=angular.copy($scope.search);
        angular.element('.loading_wrap').removeClass('hidden');
        params.do='invoice-export_btb-invoice-restoreSelectedExport';
        helper.doRequest('post', 'index.php', params, $scope.renderList)
    }
    $scope.exportNetsuite=function(){
        var params=angular.copy($scope.search);
  /*      var invoices = '';
        for (x in $scope.list) {
            if ($scope.list[x].checked) {
                invoices = invoices + $scope.list[x].id + ',';
            }
        }*/
        params.do='invoice-export_btb-netsuite-exportNetsuite';
        var doc_type=$scope.doc_type;

        $window.open("index.php?do=invoice-download_netsuite_csv&doc_type="+doc_type, '_blank');
        helper.doRequest('post', 'index.php', params, $scope.renderList)
    }
    $scope.go_link = function(inv_id) {
        if ($scope.search.type == 0) {
            $state.go('invoice.view', {
                invoice_id: inv_id
            })
        } else {
            $state.go('purchase_invoice.view', {
                invoice_id: inv_id
            })
        }
    }
    $scope.setForImport = function(array) {
        if (array.length) {
            /*var data = {};
            if (array[0].checked) {
                data.value = 1
            } else {
                data.value = 0
            }
            data.invoice_id = array[0].id;
            data.do = 'invoice--invoice-setForImport';
            helper.doRequest('post', 'index.php', data, function(o) {
                array.shift();
                $scope.setForImport(array)
            })*/
            var data = { 'list': [] };
            for (x in array) {
                data.list.push({ 'invoice_id': array[x].id, 'value': (array[x].checked ? 1 : 0) });
            }
            data.do = 'invoice--invoice-setForImport';
            helper.doRequest('post', 'index.php', data, function(r){
                $scope.all_pages_selected = r.data.all_pages_selected;
                $scope.minimum_selected = r.data.minimum_selected;
            });
        }
    }
    $scope.checkAllUnmark = function() {
        //$scope.checkItUnmark = !$scope.checkItUnmark;
        var array = [];
        for (x in $scope.list) {
            $scope.list[x].checked = $scope.listObj.checkItUnmark;
            array.push($scope.list[x])
        }
        $scope.setForImportUnmark(array)
    }
    $scope.checkSingleUnmark = function(index) {
        if ($scope.list[index].checked) {
            var elem_counter = 0;
            for (x in $scope.list) {
                if ($scope.list[x].checked) {
                    elem_counter++;
                }
            }
            if (elem_counter == $scope.list.length && $scope.list.length > 0) {
                $scope.listObj.checkItUnmark = true;
            }
        } else {
            $scope.listObj.checkItUnmark = false;
        }
        var array = [];
        array.push($scope.list[index]);
        $scope.setForImportUnmark(array)
    }
    $scope.setForImportUnmark = function(array) {
        if (array.length) {
            /*var data = {};
            if (array[0].checked) {
                data.value = 1
            } else {
                data.value = 0
            }
            data.invoice_id = array[0].id;
            data.do = 'invoice--invoice-setForImportUnmark';
            helper.doRequest('post', 'index.php', data, function(o) {
                array.shift();
                $scope.setForImportUnmark(array)
            })*/
            var data = { 'list': [] };
            for (x in array) {
                data.list.push({ 'invoice_id': array[x].id, 'value': (array[x].checked ? 1 : 0) });
            }
            data.do = 'invoice--invoice-setForImportUnmark';
            helper.doRequest('post', 'index.php', data, function(r){
                $scope.all_pages_selected = r.data.all_pages_selected;
                $scope.minimum_selected = r.data.minimum_selected;
            });
        }
    }
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };

    $scope.changeView = function(value) {
        $scope.search.archived = 0;
        if (value == -1) {
            $scope.search.archived = 1
        }
        $scope.filters(value)
    }

    function invoices() {
        $scope.search.type = 0;
        var params=angular.copy($scope.search);
        params.reset_list='1';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', params, $scope.renderList)
    }

    function pinvoices() {
        $scope.search.type = 1;
        var params=angular.copy($scope.search);
        params.reset_list='1';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', params, $scope.renderList)
    }
    $scope.showFailed = function() {
        openModal('failed_export', '', 'lg')
    }

    $scope.initCheckAll = function() {
        $scope.listObj.checkItUnmark = false;
        $scope.listObj.checkIt = false;
        var total_checked = 0;
        for (x in $scope.list) {
            if ($scope.list[x].checked) {
                total_checked++;
            }
        }
        if ($scope.list.length > 0 && $scope.list.length == total_checked) {
            if ($scope.search.archived) {
                $scope.listObj.checkItUnmark = true;
            } else {
                $scope.listObj.checkIt = true;
            }
        }
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'progress_invoice':
                var iTemplate = 'ProgressInvoice';
                var ctas = 'prog';
                break;
            case 'progress_invoice_unmark':
                var iTemplate = 'ProgressInvoice';
                var ctas = 'prog';
                break;
            case 'coda_settings':
                var iTemplate = 'ExportSettings';
                var ctas = 'set';
                break;
            case 'yuki_settings':
                var iTemplate = 'ExportSettings';
                var ctas = 'set';
                break;
            case 'btb_settings':
                var iTemplate = 'ExportSettings';
                var ctas = 'set';
                break;
            case 'exact_settings':
                var iTemplate = 'ExportSettings';
                var ctas = 'set';
                break;
            case 'failed_export':
                var iTemplate = 'FailedExport';
                var ctas = 'vm';
                break
        }
        var params = {
            template: 'iTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'progress_invoice':
                params.item = {
                    'title': helper.getLanguage('Export')
                };
                switch ($scope.app) {
                    case 'yuki':
                        if ($scope.search.type == 0) {
                            var action = 'invoice--yuki-exportYuki'
                        } else {
                            var action = 'invoice--yuki-exportIncYuki'
                        }
                        break;
                    case 'codabox':
                        var action = 'invoice--btb-exportCoda';
                        break;
                    case 'clearfacts':
                        if ($scope.search.type == 0) {
                            var action = 'invoice--clearfacts-exportFacts'
                        } else {
                            var action = 'invoice--clearfacts-exportIncFacts'
                        }
                        break;
                    case 'billtobox':
                        if ($scope.search.type == 0) {
                            var action = 'invoice--billtobox-exportBillToBox'
                        } else {
                            var action = 'invoice--billtobox-exportIncBillToBox'
                        }
                        break;
                    case 'exact_online':
                        if ($scope.search.type == 0) {
                            var action = 'invoice--exact_online-exportExactOnline'
                        } else {
                            var action = 'invoice--exact_online-exportIncExactOnline'
                        }
                        break;
                    case 'netsuite':
                        var action = 'invoice--netsuite-exportNetsuite';
                        break;
                    case 'jefacture':
                        var action = 'invoice--jefacture-exportJefacture';
                        break;
                    default:
                        if ($scope.search.type == 0) {
                            var action = 'invoice--btb-exportBtb'
                        } else {
                            var action = 'invoice--btb-exportIncBtb'
                        }
                        break
                }
                params.item.action = action;
                params.item.app = $scope.app;
                var invoices = [];
                for (x in $scope.list) {
                    if ($scope.list[x].checked) {
                        invoices.push($scope.list[x])
                    }
                }
                params.item.invoices = invoices;
                params.item.type = $scope.search.type;
                params.callback = function() {
                    helper.doRequest('get', 'index.php', $scope.search, $scope.renderList)
                }
                break;
            case 'progress_invoice_unmark':
                params.item = {
                    'title': helper.getLanguage('Restore to be exported')
                };
                var action = 'invoice--invoice-unmarkExport'
                params.item.action = action;
                params.item.app = $scope.app;
                var invoices = [];
                for (x in $scope.list) {
                    if ($scope.list[x].checked) {
                        invoices.push($scope.list[x])
                    }
                }
                params.item.invoices = invoices;
                params.item.type = $scope.search.type;
                params.callback = function() {
                    helper.doRequest('get', 'index.php', $scope.search, $scope.renderList)
                }
                break;
            case 'coda_settings':
                params.params = {
                    'do': 'invoice-coda_settings',
                    app: $scope.app
                };
                break;
            case 'yuki_settings':
                params.params = {
                    'do': 'invoice-yuki_settings',
                    app: $scope.app
                };
                break;
            case 'btb_settings':
                params.params = {
                    'do': 'invoice-btb_settings',
                    app: $scope.app
                };
                break;
            case 'exact_settings':
                params.params = {
                    'do': 'invoice-exact_settings',
                    app: $scope.app
                };
                break;
            case 'failed_export':
                params.params = {
                    'do': 'invoice-export_fail',
                    'app': $scope.app
                };
                break
        }
        modalFc.open(params)
    }
    $scope.renderList(data)
}]).controller('ProgressInvoiceModalCtrl', ['$scope', 'helper', '$window', '$timeout','$uibModalInstance', 'data', function($scope, helper,$window, $timeout, $uibModalInstance, data) {
    var prog = this;
    prog.obj = data;
    prog.start = !0;
    prog.value = 0;
    var i = 0;
    prog.showAlerts = function(d) {
        helper.showAlerts(prog, d)
    }
    prog.ok = function() {
        angular.element('.loading_alt').removeClass('hidden');
        prog.start = !1;
        postIt(prog.obj.invoices, i, prog.obj.app, prog.obj.action, prog.obj.type)
    };
    prog.cancel = function() {
        $uibModalInstance.close()
    };
    if (!prog.obj.invoices.length) {
        var obj = {
            "error": {
                "error": helper.getLanguage("Select invoice")
            },
            "notice": !1,
            "success": !1
        };
        prog.showAlerts(obj)
    }

    function postIt(invoices, i, app, action, type) {
        if (invoices.length) {
            var data = {};
            data.invoice_id = invoices[0].id;
            data.type = type;
            data.app = app;
            data.do = action;
            helper.doRequest("post", "index.php", data, function(o) {
                i++;
                prog.value = i * 100 / invoices.length;
                if (!o.error) {
                   /* if(app=='netsuite'){
                        if(data.do =='invoice--netsuite-exportNetsuite'){
                            $window.open("index.php?do=invoice-download_csv&invoice_id="+data.invoice_id, '_blank');
                        }

                         var obj = {
                            "error": !1,
                            "notice": !1,
                            "success": {
                                "success": helper.getLanguage("Invoice no") + ": " + invoices[0].s_number + ' - ' + o.data.actiune
                            }
                        };
                        prog.showAlerts(obj);
                        $timeout(function() {
                            invoices.shift();
                            postIt(invoices, i, app, action, type)
                         },200);

                    }else{*/
                        var obj = {
                            "error": !1,
                            "notice": !1,
                            "success": {
                                "success": helper.getLanguage("Invoice no") + ": " + invoices[0].s_number + ' - ' + o.data.actiune
                            }
                        };
                        prog.showAlerts(obj);
                        invoices.shift();
                        postIt(invoices, i, app, action, type)
                   /* }*/

                }
                if (o.error.error) {
                    if (angular.element('.hid').hasClass('hidden')){
                        angular.element('.hid').removeClass('hidden');
                    }
                    var obj = {
                        "error": {
                            "error": helper.getLanguage("Invoice no") + ": " + invoices[0].s_number + ' - ' + o.data.actiune + ' ' + o.error.error
                        },
                        "notice": !1,
                        "success": !1
                    };
                    prog.showAlerts(obj);
                    invoices.shift();
                    postIt(invoices, i, app, action, type)
                }
            })
        } else {
            angular.element('.loading_alt').addClass('hidden')
        }
    }
}]).controller('ExportSettingsModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'selectizeFc', function($scope, helper, $uibModalInstance, data, selectizeFc) {
    var set = this;
    set.obj = data;
    set.showAlerts = function(d) {
        helper.showAlerts(set, d)
    }
    set.changeVats = function(index, value) {
        var data = {};
        data.do = set.obj.do_next;
        data.vat_id = set.obj.vats[index].id;
        data.val = value;
        helper.doRequest('post', 'index.php', data, function(o) {
            if (o.success) {
                set.showAlerts(o)
            }
        })
    }
    set.cancel = function() {
        $uibModalInstance.close()
    };
    set.ledgeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: set.obj.is_admin,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-8">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.description) + '&nbsp;</small>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                set.obj[realModel] = o.data.lines;
                set.obj[realModel + '_id'] = o.data.inserted_id
            })
        },
        onChange(value) {
            var val = value == undefined ? '' : this.options[value].id;
            helper.doRequest('post', 'index.php', {
                'do': 'invoice--invoice-updateDefaultLedger',
                'name': set.obj.ledger_name,
                'value': val
            }, function(r) {
                set.showAlerts(r)
            })
        }
    }
}]).controller('purchaseInvoiceEditCtrl', ['$scope', '$sce', '$stateParams', '$state', '$timeout', 'editItem', '$confirm', 'modalFc', 'helper', 'data', 'selectizeFc', function($scope, $sce, $stateParams, $state, $timeout, editItem, $confirm, modalFc, helper, data, selectizeFc) {
    console.log('incomminginvoiceEdit');
    var edit = this,
        typePromise;
    edit.app = 'invoice';
    edit.app_mod = 'incomminginvoice';
    edit.ctrlpag = 'purchase_ninvoice';
    edit.tmpl = 'iTemplate';
    $scope.vm = {};
    $scope.vm.isAddPage = !1;
    edit.item_id = $stateParams.incomming_invoice_id;
    edit.invoice_id = $stateParams.invoice_id;
    edit.incomming_invoice_id = edit.item_id;
    edit.cfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        maxItems: 1
    };
    edit.invoiceTypeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        onChange(value){
            if(value == '2'){
                $scope.$parent.page_title=helper.getLanguage('Credit Note');
            }else{
                $scope.$parent.page_title=helper.getLanguage('Invoice');
            }
            if(value =='1'){
                edit.hide=true;
            }else{
                edit.hide=false;
            }
        },
        onDelete(value){
            return false;
        },
        maxItems: 1
    };
    edit.item = {};
    edit.oldObj = {};
    edit.item.validity_date = new Date();
    edit.removedCustomer = !1;
    //edit.item.add_customer = (edit.item_id != 'tmp' ? !0 : !1);
    edit.item.add_customer = !1;
    edit.show_drop_load = !1;
    edit.showTxt = {
        invoice_settings: !1,
        addBtn: !0,
        address: !1,
        invoice_notes: !1,
        addArticleBtn: !1,
    }

    old_add = {};
    edit.contactAddObj = {};
    edit.auto = {
        options: {
            contacts: [],
            addresses: [],
            cc: [],
            recipients: []
        }
    };
    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {
        invoice_date: !1,
    }
    edit.showEditFnc = showEditFnc;
    edit.openDate = openDate;
    edit.showEditFnc = showEditFnc;
    edit.showCCSelectize = showCCSelectize;
    edit.cancelEdit = cancelEdit;
    edit.removeDeliveryAddr = removeDeliveryAddr;
    edit.renderPage = renderPage;
    edit.removeLine = removeLine;
    edit.initial = initial;
    editItem.init(edit);
    edit.linevatcfg = {
        valueField: 'vat',
        labelField: 'name',
        searchField: ['vat'],
        delimiter: '|',
        placeholder: helper.getLanguage('VAT'),
        create: !1,
        maxOptions: 100,
        onDelete(values) {
            return !1
        },
        closeAfterSelect: !0,
        maxItems: 1
    };

    edit.extraCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 200,
        maxItems: 1,
        create: !1,
        onDropdownOpen($dropdown) {
            var _this = this,
                model_id = _this.$input[0].attributes.model.value;
            if (edit.item[model_id] != undefined && edit.item[model_id] != '0') {
                old_add[model_id] = edit.item[model_id]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                //console.log(edit.item, realModel, o.data.lines,o.data.inserted_id);
                edit.item[realModel] = o.data.lines;
                edit.item[realModel + '_id'] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            var opts = _this.$input[0].attributes.opts.value;
            var extra_id = undefined;
            if (item == '0') {
                //console.log(item, table, model, opts, extra_id, edit.item);
                openModal('selectEdit', {
                    table: table,
                    model: model,
                    extra_id: extra_id,
                    opts: opts,
                    upper: 1
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                edit.item[model] = undefined;
                return !1
            }
        }
    }, !1, !0, edit.item);

    edit.link_dropbox = function() {
        var data = angular.copy(edit.item);
        data.do = 'invoice-purchase_ninvoice-invoice-link';
        data.incomming_invoice_id = edit.item_id;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(o) {
            if (!o.error) {
                edit.renderPage(o)
            } else {
                edit.showAlerts(o)
            }
        })
    };
    edit.upload = function(edit, e) {
        angular.element('.newUploadFile2').on('click', function(e) {
            e.stopPropagation()
        }).on('click', function(e) {
            angular.element(e.target).prop('value', '');
        }).on('change', function(e) {
            e.preventDefault();
            var _this = $(this),
                p = _this.parents('.upload-box'),
                x = p.find('.newUploadFile2');
            if (x[0].files.length == 0) {
                return !1
            }
            p.find('.btn').addClass('hidden');
            p.find('.nameOfTheFile').text(x[0].files[0].name);
            var formData = new FormData();
            formData.append("Filedata", x[0].files[0]);
            formData.append("do", 'invoice-purchase_ninvoice-invoice-uploadInc');
            formData.append("incomming_invoice_id", edit.item_id);
            p.find('.upload-progress-bar').css({
                width: '0%'
            });
            p.find('.upload-file').removeClass('hidden');
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "index.php", !0);
            oReq.upload.addEventListener("progress", function(e) {
                var pc = parseInt(e.loaded / e.total * 100);
                p.find('.upload-progress-bar').css({
                    width: pc + '%'
                })
            });
            oReq.onload = function(oEvent) {
                var res = JSON.parse(oEvent.target.response);
                p.find('.upload-file').addClass('hidden');
                p.find('.btn').removeClass('hidden');
                if (oReq.status == 200) {
                    if (res.success) {
                        $('#image_frame').empty();

                        $timeout(function() {
                            edit.item.exist_image = !0;
                            edit.item.view_upload = !1;
                            if (edit.incomming_invoice_id == 'tmp') {
                                edit.item.link_url = res.name;
                                var buyer_exist = edit.item.buyer_id ? !0 : !1;
                                if (res.xml_data) {
                                    for (x in res.xml_data) {
                                        if (x == 'invoice_date' || x == 'due_date') {
                                            edit.item[x] = new Date(res.xml_data[x]);
                                        } else {
                                            edit.item[x] = res.xml_data[x]
                                        }
                                    }
                                    if (!buyer_exist && edit.item.buyer_id) {
                                        edit.saveClientData(edit)
                                    }
                                    if (!res.xml_data.buyer_id && res.xml_data.buyer_data) {
                                        createSupplier(res.xml_data.buyer_data)
                                    }
                                }
                            }
                        });

                        $('#image_frame').html('<iframe width="614" height="616" src="' + res.name + '" ></iframe>').removeClass('hide');
                        if (edit.incomming_invoice_id != 'tmp') {

                            if (res.xml_data) {
                                $confirm({
                                    ok: helper.getLanguage('Keep'),
                                    cancel: helper.getLanguage('Discard'),
                                    text: helper.getLanguage('Do you want to keep existing financial data?')
                                }).then(function() {
                                    edit.item.invoice_currency = res.xml_data.invoice_currency;
                                    edit.item.invoice_total_wo_vat = res.xml_data.invoice_total_wo_vat;
                                    edit.item.total_amount_due = res.xml_data.total_amount_due;
                                    edit.item.invoice_row = res.xml_data.invoice_row;
                                    edit.item.invoice_vat_line = res.xml_data.invoice_vat_line
                                }, function() {
                                    if (res.xml_data) {
                                        for (x in res.xml_data) {
                                            edit.item[x] = res.xml_data[x]
                                        }
                                    }
                                })
                            }
                        }
                        $('#remove_button').removeClass('hide')
                    } else {
                        var msg_err = {
                            'error': {
                                "error": res.error
                            },
                            'success': !1,
                            'notice': !1
                        };
                        edit.showAlerts(msg_err)
                    }
                } else {
                    var msg_err = {
                        'error': {
                            "error": "Error " + oReq.status + " occurred when trying to upload your file."
                        },
                        'success': !1,
                        'notice': !1
                    };
                    edit.showAlerts(msg_err)
                }
                angular.element('.newUploadFile2').unbind()
            };
            oReq.send(formData)
        });
        angular.element(e.target).parents('.upload-box').find('.newUploadFile2').click()
    }
    edit.allowDisconto = function() {
        if (!edit.item.allow_disconto) {
            edit.item.disconto = helper.displayNr(0)
        }
        edit.calcPrice()
    }
    edit.addLine = function(d) {
        var line = {
            tr_id: "tmp" + (new Date().getTime()),
            vat_line: helper.displayNr(0),
            total_no_vat: helper.displayNr(0),
            total: helper.displayNr(0)
        }
        edit.item.invoice_line.push(line)
    }
    edit.calcPrice = function(d) {
        var total = 0;
        var discount = 0;
        if (edit.item.allow_disconto) {
            discount = parseFloat((edit.item.disconto), 10)
        }
        for (x in edit.item.invoice_line) {
            var vat_line = parseFloat(helper.return_value(edit.item.invoice_line[x].vat_line), 10),
                total_no_vat = parseFloat(helper.return_value(edit.item.invoice_line[x].total_no_vat), 10);
            edit.item.invoice_line[x].total = helper.displayNr(total_no_vat + total_no_vat * vat_line / 100 - (total_no_vat + total_no_vat * vat_line / 100) * discount / 100);
            total += total_no_vat + total_no_vat * vat_line / 100 - (total_no_vat + total_no_vat * vat_line / 100) * discount / 100
        }
        edit.item.total = helper.displayNr(total)
    }
    edit.remove_file = function(type, link) {
        edit.item.invoice_row = undefined;
        edit.item.invoice_currency = undefined;
        edit.item.invoice_total_wo_vat = undefined;
        edit.item.total_amount_due = undefined;
        edit.item.invoice_vat_line = undefined;
        edit.item.ogm = undefined;
        if (edit.item.incomming_invoice_id == 'tmp') {
            $confirm({
                ok: helper.getLanguage('Keep'),
                cancel: helper.getLanguage('Discard'),
                text: helper.getLanguage('Do you want to keep existing vats?')
            }).then(function() {
                var data = angular.copy(edit.item);
                data.do = 'invoice-purchase_ninvoice-invoice-remove_image';
                data.incomming_invoice_id = edit.item_id;
                data.type_r = type;
                data.link = link;
                angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('post', 'index.php', data, function(o) {
                    edit.renderPage(o)
                })
            }, function() {
                edit.item.invoice_line = undefined;
                edit.item.total = undefined;
                var data = angular.copy(edit.item);
                data.do = 'invoice-purchase_ninvoice-invoice-remove_image';
                data.incomming_invoice_id = edit.item_id;
                data.type_r = type;
                data.link = link;
                angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('post', 'index.php', data, function(o) {
                    edit.renderPage(o)
                })
            })
        } else {
            edit.item.invoice_line = undefined;
            edit.item.total = undefined;
            var data = angular.copy(edit.item);
            data.do = 'invoice-purchase_ninvoice-invoice-remove_image';
            data.incomming_invoice_id = edit.item_id;
            data.type_r = type;
            data.link = link;
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(o) {
                edit.renderPage(o)
            })
        }
    };
    edit.AutomaticTransfer = function() {
        if (!edit.item.a_transfer) {
            edit.item.a_transfer = 0
        } else {
            edit.item.a_transfer = 1
        }
        var data = {
            'do': 'invoice-purchase_ninvoice-invoice-paid_by_automatic_transfer',
            'invoice_id': edit.item.incomming_invoice_id,
            'a_transfer': edit.item.a_transfer
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(edit, r)
        })
    }

    function removeLine($i) {
        var tr_id = edit.item.invoice_line[$i].tr_id;
        if ($i > -1) {
            edit.item.invoice_line.splice($i, 1)
        }
        edit.calcPrice()
    }

    function renderPage(d) {
        if (d && d.data) {
            edit.item = d.data;
            helper.updateTitle($state.current.name, edit.item.booking_number);
            $scope.$parent.page_title = edit.item.type == '2' ? helper.getLanguage('Credit Note') : helper.getLanguage('Invoice');
            $scope.$parent.serial_number=edit.item.booking_number;
            $scope.$parent.nr_status=edit.item.nr_status;
            if(edit.item.invoice_number=='' && edit.item.incomming_invoice_id !='tmp'){
                $scope.$parent.proforma_to_invoice=1;
            }
            if (d.data.incomming_invoice_id) {
                edit.item_id = d.data.incomming_invoice_id
            }
            if (edit.item.invoice_date) {
                edit.item.invoice_date = new Date(edit.item.invoice_date)
            }
            if (edit.item.due_date) {
                edit.item.due_date = new Date(edit.item.due_date)
            }
            if (edit.item.link_url && edit.item_id != 'tmp') {
                edit.item.link_url = $sce.trustAsResourceUrl(edit.item.link_url)
            }
            if (edit.item.buyer_data && !edit.item.buyer_id) {
                createSupplier(edit.item.buyer_data)
            }
            edit.auto.options.contacts = d.data.contacts;
            edit.auto.options.addresses = d.data.addresses;
            edit.auto.options.cc = d.data.cc;
            edit.contactAddObj = {
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                add_contact: !0,
                buyer_id: edit.item.buyer_id,
                article: edit.item.tr_id,
                'do': 'invoice-purchase_ninvoice-invoice-tryAddC',
                contact_only: !0,
                item_id: edit.item_id
            }
            if (edit.item.is_dropbox) {
                getDropBoxFiles()
            }
        }
    }

    function openDate(p) {
        edit.datePickOpen[p] = !0
    }

    function removeDeliveryAddr() {
        edit.item.delivery_address = '';
        edit.item.delivery_address_id = ''
    }

    function cancelEdit() {
        if (Object.keys(edit.oldObj).length == 0) {
            return edit.removeCust(edit)
        }
        edit.showTxt.address = !0;
        for (x in edit.oldObj) {
            if (edit.item[x]) {
                if (x == 'delivery_address') {
                    edit.item[x] = edit.oldObj[x].trim()
                } else {
                    edit.item[x] = edit.oldObj[x]
                }
            }
        }
        edit.item.buyer_id = edit.oldObj.buyer_id
    }

    function showEditFnc() {
        edit.showTxt.address = !1;
        edit.oldObj = {
            buyer_id: angular.copy(edit.item.buyer_id),
            contact_id: angular.copy(edit.item.contact_id),
            contact_name: angular.copy(edit.item.contact_name),
            BUYER_NAME: angular.copy(edit.item.buyer_name),
            delivery_address: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address)),
            delivery_address_id: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_id)),
            delivery_address_txt: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_txt)),
            view_delivery: (edit.item.sameAddress ? !1 : angular.copy(edit.item.view_delivery)),
            sameAddress: angular.copy(edit.item.sameAddress),
        }
    }

    function showCCSelectize() {
        edit.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }
    edit.cancel = function() {
        $state.go('purchase_invoices')
    }
    edit.setStartDate = function() {
        var initDate = new Date(edit.item.start_date).getTime() / 1000;
        edit.item.quote_ts = new Date((initDate + 7200) * 1000)
    }
    edit.showAlerts = function(d, formObj) {
        helper.showAlerts(edit, d, formObj)
    }
    edit.save = function(formObj, new_item = !1) {
        var data = angular.copy(edit.item);
        data = helper.clearData(data, ['APPLY_DISCOUNT_LIST', 'accmanager', 'addresses', 'articles_list', 'cc', 'contacts', 'country_dd', 'currency_type_list', 'language_dd', 'main_comp_info', 'multiple_identity_dd', 'seller_b_country_dd', 'seller_d_country_dd']);
        if (edit.item_id == 'tmp') {
            data.do = 'invoice-purchase_ninvoice-invoice-add_incomming'
        } else {
            data.do = 'invoice-purchase_ninvoice-invoice-update_incomming'
        }
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            angular.element('.loading_wrap').addClass('hidden');
            if (r && r.success) {
                /*if (new_item) {
                    $state.reload()
                }*/
                if (edit.create_new) {
                    $state.reload();
                } else {
                    $scope.$parent.proforma_to_invoice=0;
                    $state.go('purchase_invoice.view', {
                        invoice_id: r.data.incomming_invoice_id
                    })
                }
            } else {
                edit.showAlerts(r, formObj)
            }
        })
    }

    function createSupplier(buyer_data) {
        $confirm({
            ok: helper.getLanguage('Yes'),
            cancel: helper.getLanguage('No'),
            text: helper.getLanguage('Supplier not found by vat number. Do you want to create it?')
        }).then(function() {
            var obj = {
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                add_customer: !0,
                article: edit.item.tr_id,
                'do': edit.app + '-' + edit.ctrlpag + '-' + edit.app + '-tryAddC',
                identity_id: edit.item.identity_id,
                item_id: edit.item_id,
                app_mod: edit.app_mod,
            }
            obj.file_path = edit.item.file_path;
            obj.linkpath = edit.item.linkpath;
            obj.link_url = edit.item.link_url;
            obj.drop_file_path = edit.item.drop_file_path;
            obj.drop_xml = edit.item.drop_xml;
            obj.buyer_data = buyer_data;
            obj.invoice_row = edit.item.invoice_row;
            obj.invoice_currency = edit.item.invoice_currency;
            obj.invoice_total_wo_vat = edit.item.invoice_total_wo_vat;
            obj.total_amount_due = edit.item.total_amount_due;
            obj.invoice_vat_line = edit.item.invoice_vat_line;
            obj.ogm = edit.item.ogm;
            obj.invoice_line = edit.item.invoice_line;
            obj.total = edit.item.total;
            obj.invoice_number = edit.item.invoice_number;
            edit.openModal(edit, 'AddCustomer', obj, 'md');
            return !1
        }, function() {})
    }

    function getDropBoxFiles() {
        edit.show_drop_load = !0;
        helper.doRequest('get', 'index.php', {
            'do': 'invoice-purchase_ninvoice',
            'xget': 'dropboxFiles'
        }, function(r) {
            edit.show_drop_load = !1;
            edit.item.file_selected_dd = r.data.file_selected_dd
        })
    }

    edit.showInvoiceDate = function() {
        if (!edit.item.invoice_date && edit.item_id == 'tmp') {
            edit.item.invoice_date = new Date();
            edit.updateDueDate();
        }
        edit.datePickOpen.invoice_date = true;
    }

    edit.updateDueDate = function() {
        if (edit.item_id == 'tmp' && edit.item.invoice_date && !isNaN(new Date(edit.item.invoice_date))) {
            if (edit.item.payment_term_type == '1') {
                edit.item.due_date = new Date((Math.round(edit.item.invoice_date.getTime() / 1000) + (parseInt(edit.item.payment_term) * (60 * 60 * 24) - 1)) * 1000);
            } else {
                var invoice_date = new Date(edit.item.invoice_date);
                if (invoice_date.getMonth() == 11) {
                    var firstDayNextMonth = new Date(invoice_date.getFullYear() + 1, 0, 1);
                } else {
                    var firstDayNextMonth = new Date(invoice_date.getFullYear(), invoice_date.getMonth() + 1, 1);
                }
                edit.item.due_date = new Date((Math.round(firstDayNextMonth.getTime() / 1000) + (parseInt(edit.item.payment_term) * (60 * 60 * 24) - 1)) * 1000);
            }
        }
    }

    function openModal(type, item, size, txt) {
        var params = {
            template: 'atemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            txt: txt,
            backdrop: 'static'
        };

        switch (type) {
            case 'selectEdit':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.params = {
                    'do': 'misc-' + type,
                    'table': item.table,
                    'upper': item.upper
                };
                params.callback = function(data) {
                    if (data.data) {
                        edit.item[item.opts] = data.data.lines;
                        if (old_add[item.model]) {
                            edit.item[item.model] = old_add[item.model]
                        }
                    } else {
                        edit.item[item.opts] = data.lines;
                        if (old_add[item.model]) {
                            edit.item[item.model] = old_add[item.model]
                        }
                    }
                }
                break;
        }
        modalFc.open(params)
    }

    function initial() {
        edit.renderPage(data)
    }
    edit.initial()
}]).controller('paymentsCtrl', ['$scope', '$compile', '$timeout', '$window', 'helper', 'list', '$uibModal', 'modalFc', 'data', function($scope, $compile, $timeout, $window, helper, list, $uibModal, modalFc, data) {
    console.log('Payments');
    $scope.views = [{
        name: helper.getLanguage('List'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Archived'),
        id: -1
    }];
    $scope.views_codabox = [{
            name: helper.getLanguage('Not Fully Linked'),
            id: 0,
            active: 'active'
        },
        {
            name: helper.getLanguage('Linked'),
            id: 1,
        },
        {
            name: helper.getLanguage('Partially Linked'),
            id: 2,
        },
        {
            name: helper.getLanguage('Not Linked'),
            id: 3
        }
        /*, {
                name: helper.getLanguage('Archived'),
                id: -1
            }*/
    ];
    $scope.activeView = 0;
    $scope.payments = {};
    $scope.list = [];
    $scope.feeds = [];
    $scope.feed_clients = [];
    $scope.fetchDisabled = false;

    $scope.search = {
        search: '',
        'do': 'invoice-payments_list',
        view: $scope.activeView,
        xget: '',
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.payments.listObj = {
        check_add_all: !1
    };

    $scope.ord = '';
    $scope.reverse = !1;
    $scope.renderList = function(d, index) {
        if (d.data) {
            if (d.data.payments) {
                $scope.payments = d.data.payments;
                $scope.payments.list = d.data.payments.query;
                $scope.codabox_active = d.data.codabox_active;
                $scope.initCheckAll();
            }
            if (d.data.coda_incoming) {
                $scope.list = d.data.query;
                $scope.feeds = d.data.coda_incoming;
                $scope.feed_clients = d.data.coda_clients;
                $scope.lr = d.data.lr;
                $scope.max_rows = d.data.max_rows;
                if (index) {
                    $timeout(function() {
                        $scope.listMore(index)
                    })
                } else {
                    $scope.feeds = {};
                    $scope.feed_clients = {};
                }
            }


        }
        $scope.showAlerts(d)
    };

    $scope.getCodaboxPayments = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', { 'do': 'invoice-payments_list', 'xget': 'codabox_payments', 'view': $scope.search.view }, function(r) {

            $scope.list = r.data.query;
            $scope.feeds = r.data.coda_incoming;
            $scope.feed_clients = r.data.coda_clients;
            $scope.lr = r.data.lr;
            $scope.max_rows = r.data.max_rows;
            $scope.search.xget = 'codabox_payments';
            $scope.activeView = $scope.search.view;

        })
    }
    $scope.hideFetch = function() {
        $scope.feeds = {};
        $scope.feed_clients = {};
    }
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function(xget) {
        angular.element('.loading_wrap').removeClass('hidden');
        if (xget) {
            $scope.search.xget = xget;
        } else {
            $scope.search.xget = '';
        }
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.createExports = function($event) {
        return list.exportFnc($scope, $event)
    }
    $scope.saveForXls = function(p) {
        if (!$scope.payments.listObj) {
            $scope.payments.listObj = {};
        }
        if (p) {
            if (p.check_add_to_product) {
                var elem_counter = 0;
                for (x in $scope.payments.list) {
                    if ($scope.payments.list[x].check_add_to_product) {
                        elem_counter++;
                    }
                }
                if (elem_counter == $scope.payments.list.length && $scope.payments.list.length > 0) {
                    $scope.payments.listObj.check_add_all = true;
                }
            } else {
                $scope.payments.listObj.check_add_all = false;
            }
        }
        list.saveForPDF($scope.payments, 'invoice--invoice-saveAddToXls', p)
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };

    $scope.filterPaymentCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Payment Method'),
        onChange(value) {
            var selectize = angular.element('#payment_methods_list')[0].selectize;
            if (Array.isArray($scope.search.payment_method_id) && $scope.search.payment_method_id.length > 1) {
                if ($scope.search.payment_method_id.indexOf("0") > 0) {
                    for (x in $scope.search.payment_method_id) {
                        if ($scope.search.payment_method_id[x] != '0') {
                            selectize.removeItem($scope.search.payment_method_id[x]);
                        }
                    }
                } else {
                    for (x in $scope.search.payment_method_id) {
                        if ($scope.search.payment_method_id[x] == '0') {
                            selectize.removeItem('0');
                            break;
                        }
                    }
                }
            }
            $scope.searchThing();
        },
        maxItems: 100
    };

    $scope.listMore = function(index) {
        angular.element('.orderTable tbody tr.collapseTr').eq(index).next().removeClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').addClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').removeClass('hidden')
    }
    $scope.listLess = function(index) {
        angular.element('.orderTable tbody tr.collapseTr').eq(index).next().addClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').removeClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').addClass('hidden')
    }
    $scope.getData = function() {
        $scope.fetchDisabled = true;
        var data = [];
        data.push($scope.feeds);
        data.push($scope.feed_clients);
        openModal("pay_load", data, "md")
    }
    $scope.fetchInvoices = function(child_index) {
        var data = {};
        /* var parent_index = 0;
         for (x in $scope.list) {
             if ($scope.list[x].id == parent_obj.id) {
                 parent_index = x;
                 break
             }
         }
         data.parent_index = parent_index;
         data.id = $scope.list[parent_index].payments[child_index].id;*/
        data.id = $scope.list[child_index].id;

        for (x in $scope.search) {
            data[x] = $scope.search[x]
        }
        openModal("fetch_invoices", data, "lg")
    }
    $scope.archiveSubline = function(child_index) {
        var obj = {};
        /* var parent_index = 0;
         for (x in $scope.list) {
             if ($scope.list[x].id == parent_obj.id) {
                 parent_index = x;
                 break
             }
         }
         obj.id = $scope.list[parent_index].payments[child_index].id;*/
        obj.id = $scope.list[child_index].id;
        obj.do = 'invoice--btb-archiveSubline';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                /*$scope.list[parent_index].payments.splice(child_index, 1);
                var rows = 0;
                for (x in $scope.list[parent_index].payments) {
                    rows++
                }
                if (!rows) {
                    $scope.list[child_index].nr_status = '0';
                    $scope.list[child_index].status = helper.getLanguage('Archived')
                }*/
                $scope.getCodaboxPayments();
            }
        })
    }

    $scope.markAsLinked = function(child_index) {
        var obj = {};
        obj.id = $scope.list[child_index].id;
        obj.do = 'invoice--btb-mark_as_linked';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.getCodaboxPayments();
            }
        })
    }

    $scope.unlink = function(child_index) {
        var obj = {};
        obj.id = $scope.list[child_index].id;
        obj.do = 'invoice--btb-unlink';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.getCodaboxPayments();
            }
        })
    }

    $scope.initCheckAll = function() {
        var total_checked = 0;
        for (x in $scope.payments.list) {
            if ($scope.payments.list[x].check_add_to_product) {
                total_checked++;
            }
        }
        if (!$scope.payments.listObj) {
            $scope.payments.listObj = {};
        }
        if ($scope.payments.list.length > 0 && $scope.payments.list.length == total_checked) {
            $scope.payments.listObj.check_add_all = true;
        } else {
            $scope.payments.listObj.check_add_all = false;
        }
    }

    $scope.exportPayments = function() {
        var data = angular.copy($scope.search);
        delete data.do;
        delete data.xget;
        $window.open('index.php?do=invoice-export_selected_payments&export_data=' + encodeURI(btoa(JSON.stringify(data))), '_blank');
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'pay_load':
                var iTemplate = 'PaymentsLoad';
                var ctas = 'pay';
                break;
            case 'fetch_invoices':
                var iTemplate = 'FetchInvoices';
                var ctas = 'fetch';
                break
        }
        var params = {
            template: 'iTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'pay_load':
                params.item = item;
                params.callback = function() {
                    //$scope.search.xget = 'codabox_payments';              
                    //list.init($scope, 'invoice-payments_list', $scope.renderList)
                    $scope.getCodaboxPayments();
                    $scope.fetchDisabled = false;
                }
                break;
            case 'fetch_invoices':
                params.params = {};
                for (x in item) {
                    params.params[x] = item[x]
                }
                params.params.search = '';
                params.params.do = 'invoice-matchInvoices';
                params.callback = function(data) {
                    $scope.fetchDisabled = false;
                    if (data) {
                        $scope.search.search = '';
                        $scope.renderList(data, data.parent_index)
                    }
                }
                break
        }
        modalFc.open(params)
    }

    $scope.renderList(data)
}]).controller('PaymentsLoadModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, helper, $uibModalInstance, data, modalFc) {
    var pay = this;
    pay.feeds = data;
    pay.value = 0;
    pay.showAlerts = function(d) {
        helper.showAlerts(pay, d)
    }
    var feed_copy = angular.copy(pay.feeds);
    getFeed(feed_copy, 0);

    function getFeed(feeds, i) {
        if (feeds[0].length) {
            var data = {};
            data.feed_index = feeds[0][0];
            data.feed_client = feeds[1][0];
            data.do = 'invoice--btb-coda_payment';
            helper.doRequest("post", "index.php", data, function(o) {
                i++;
                var percent = i * 100 / pay.feeds[0].length;
                pay.value = parseInt(percent.toFixed(0));
                if (o.error) {
                    pay.showAlerts(o)
                } else {
                    feeds[0].shift();
                    feeds[1].shift();
                    getFeed(feeds, i)
                }
            })
        } else {
            $uibModalInstance.close()
        }
    }

}]).controller('FetchInvoicesModalCtrl', ['$scope', 'helper', 'list', '$timeout', '$uibModalInstance', 'data', 'modalFc', function($scope, helper, list, $timeout, $uibModalInstance, data, modalFc) {
    var fetch = this,
        typePromise;
    fetch.show_amount_warning=false;
    fetch.obj = {};
    fetch.search = {
        'do': 'invoice-matchInvoices',
        'fetch_iban_nr': '',
        'fetch_amount_nr': '',
        'filter_iban': undefined,
        'filter_amount': undefined,
        'search': ''
    };
    for (x in data) {
        if (x == 'filters') {
            continue
        } else {
            fetch.obj[x] = data[x]
        }
    }
    for (x in data.filters) {
        fetch.search[x] = data.filters[x]
    }
    fetch.renderPage = function(d) {

        if (d && d.data) {
            for (x in d.data) {
                if (x == 'filters') {
                    continue
                } else {
                    fetch.obj[x] = d.data[x]
                }
            }
        }
    }
    fetch.renderList = function(d) {

        if (d && d.data) {
            if (d.data.invoices.length) {
                fetch.obj.invoices = d.data.invoices;
                /*for (x in d.data.invoices) {
                    fetch.obj.invoices[x] = d.data.invoices[x];
                }*/
            } else {

                for (x in d.data) {
                    if (x == 'filters') {
                        continue
                    } else {
                        fetch.obj[x] = d.data[x]
                    }
                }

            }
        }
    }

    fetch.showAlerts = function(d) {
        helper.showAlerts(fetch, d)
    }
    fetch.cancel = function() {
        data.do = 'invoice--btb-unsetAddToFetch';
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close();
        })

    }
    fetch.searchThing = function(k, val, search) {
        if (search) {
            fetch.search[k] = val
        } else {
            fetch.search[k] = ''
        }
        var obj = angular.copy(fetch.search);
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            data.do = 'invoice--btb-unsetAddToFetch';
            helper.doRequest('post', 'index.php', data, function(o) {
                fetch.renderPage(r)
            })

        })
    };
    fetch.searchThingList = function(k, val, search) {
        if (search) {
            fetch.search[k] = val
        } else {
            fetch.search[k] = ''
        }
        var obj = angular.copy(fetch.search);
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            fetch.renderList(r)
        })
    };
    fetch.toggleSearchButtons = function(item, val) {
        fetch.search[item] = val;
        var obj = angular.copy(fetch.search);
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            fetch.renderPage(r)
        })
    };
    fetch.searchIt = function() {
        if (typePromise) {
            $timeout.cancel(typePromise)
        }
        typePromise = $timeout(function() {
            var obj = angular.copy(fetch.search);
            angular.element('.loading_alt').removeClass('hidden');
            helper.doRequest('get', 'index.php', obj, function(r) {
                angular.element('.loading_alt').addClass('hidden');
                fetch.renderPage(r)
            })
        }, 500)
    }
    fetch.saveForFetch = function(item, checked) {
        var data = {};
        data.item = item;
        data.checked = checked ? 1 : 0;
        data.do = 'invoice--btb-saveAddToFetch';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.data) {

                for (x in fetch.obj.all_invoices) {
                    for (y in r.data) {
                        if (fetch.obj.all_invoices[x].id == y) {

                            fetch.obj.all_invoices[x].add_to_fetch = 1;
                        }
                    }
                }
            }
        })
    }

    fetch.fetch = function(index, checked) {
        var total_amount = parseFloat(helper.return_value(fetch.obj.amount), 10);
        var amount_invoice = parseFloat(helper.return_value(fetch.obj.invoices[index].amount_vat), 10);

        if (checked == !0) {
            if (total_amount == 0) {
                fetch.obj.invoices[index].add_to_fetch = !1;
                fetch.obj.all_invoices[index].add_to_fetch = !1;
                /*var obj = {
                    "error": {
                        "error": helper.getLanguage("No amount available")
                    },
                    "notice": !1,
                    "success": !1
                };
                fetch.showAlerts(obj)*/
            } else {
                if (fetch.obj.currency == fetch.obj.invoices[index].currency) {
                    getFetched(total_amount, amount_invoice, index, 1)
                } else {
                    var data = {};
                    data.from = fetch.obj.invoices[index].currency_type;
                    data.to = fetch.obj.currency_type;
                    data.do = 'invoice--btb-getConverted';
                    helper.doRequest('post', 'index.php', data, function(r) {
                        getFetched(total_amount, amount_invoice, index, r.data.rate)
                    })
                }
                fetch.saveForFetch(fetch.obj.invoices[index].id, checked);
            }
        } else {
            var partial_amount = parseFloat(helper.return_value(fetch.obj.invoices[index].partial_amount), 10);
            if (fetch.obj.currency == fetch.obj.invoices[index].currency) {
                if (isNaN(partial_amount)) {
                    fetch.obj.amount = helper.displayNr(total_amount + amount_invoice)
                } else {
                    fetch.obj.amount = helper.displayNr(total_amount + partial_amount);
                    fetch.obj.invoices[index].partial_amount = 0;
                    fetch.obj.invoices[index].previous_partial_amount = 0
                }
            } else {
                var data = {};
                data.from = fetch.obj.invoices[index].currency_type;
                data.to = fetch.obj.currency_type;
                data.do = 'invoice--btb-getConverted';
                helper.doRequest('post', 'index.php', data, function(r) {
                    if (isNaN(partial_amount)) {
                        fetch.obj.amount = helper.displayNr(total_amount + amount_invoice * r.data.rate)
                    } else {
                        fetch.obj.amount = helper.displayNr(total_amount + partial_amount * r.data.rate);
                        fetch.obj.invoices[index].partial_amount = 0;
                        fetch.obj.invoices[index].previous_partial_amount = 0
                    }
                })
            }
            fetch.saveForFetch(fetch.obj.invoices[index].id, checked);
            fetch.show_amount_warning = false;
        }

    }

    function getFetched(total_amount, amount_invoice, index, rate) {
        if (total_amount - amount_invoice * rate < 0) {
            fetch.obj.invoices[index].partial_amount = helper.displayNr(total_amount / rate);
            fetch.obj.invoices[index].previous_partial_amount = helper.displayNr(total_amount / rate);
            fetch.obj.amount = helper.displayNr(0)
        } else {
            fetch.obj.amount = helper.displayNr(total_amount - amount_invoice * rate)
        }
        fetch.show_amount_warning=parseFloat(helper.return_value(fetch.obj.amount), 10) == 0 ? true : false;
    }
    fetch.changePartial = function(index) {
        var total_amount = parseFloat(helper.return_value(fetch.obj.amount), 10);
        var current_partial = parseFloat(helper.return_value(fetch.obj.invoices[index].partial_amount), 10);
        var previous_partial = parseFloat(helper.return_value(fetch.obj.invoices[index].previous_partial_amount), 10);
        if (fetch.obj.currency == fetch.obj.invoices[index].currency) {
            reFetch(total_amount, previous_partial, current_partial, index, 1)
        } else {
            var data = {};
            data.from = fetch.obj.invoices[index].currency_type;
            data.to = fetch.obj.currency_type;
            data.do = 'invoice--btb-getConverted';
            helper.doRequest('post', 'index.php', data, function(r) {
                reFetch(total_amount, previous_partial, current_partial, index, r.data.rate)
            })
        }
    }

    function reFetch(total_amount, previous_partial, current_partial, index, rate) {
        var amount = total_amount + previous_partial * rate;
        if (isNaN(current_partial)) {
            fetch.obj.amount = helper.displayNr(amount);
            fetch.obj.invoices[index].add_to_fetch = !1;
            fetch.obj.all_invoices[index].add_to_fetch = !1;
            fetch.obj.invoices[index].partial_amount = 0;
            fetch.obj.invoices[index].previous_partial_amount = 0
        } else {
            if (amount - current_partial < 0) {
                fetch.obj.amount = helper.displayNr(0);
                fetch.obj.invoices[index].partial_amount = helper.displayNr(amount);
                fetch.obj.invoices[index].previous_partial_amount = helper.displayNr(amount);
                var message = helper.getLanguage("Maxmimum of") + " " + helper.displayNr(amount) + " " + fetch.obj.currency_type + " " + helper.getLanguage("allowed");
                var obj = {
                    "error": {
                        "error": message
                    },
                    "notice": !1,
                    "success": !1
                };
                fetch.showAlerts(obj)
            } else {
                fetch.obj.amount = helper.displayNr(amount - current_partial);
                fetch.obj.invoices[index].previous_partial_amount = helper.displayNr(current_partial);
                if (current_partial == 0) {
                    fetch.obj.invoices[index].add_to_fetch = !1;
                    fetch.obj.all_invoices[index].add_to_fetch = !1;
                    fetch.obj.invoices[index].partial_amount = 0;
                    fetch.obj.invoices[index].previous_partial_amount = 0
                }
            }
        }
    }
    fetch.ok = function() {
        var data = angular.copy(fetch.obj);
        data.do = 'invoice-payments_list-btb-saveFetch';
        data.xget = 'codabox_payments';
        data.invoices_fetched = [];
        data.search = '';

        for (x in data.all_invoices) {
            if (data.all_invoices[x].add_to_fetch == !0) {
                if (parseFloat(helper.return_value(data.all_invoices[x].partial_amount), 10) > 0) {
                    data.all_invoices[x].amount_vat = data.all_invoices[x].partial_amount
                }
                data.invoices_fetched.push(data.all_invoices[x])
            }
        }
        delete data.all_invoices;
        if (data.invoices_fetched.length) {
            angular.element('.loading_alt').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                angular.element('.loading_alt').addClass('hidden');
                r.parent_index = fetch.obj.parent_index;
                r.child_index = fetch.obj.child_index;
                $uibModalInstance.close(r)
            })
        } else {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Select at least one invoice")
                },
                "notice": !1,
                "success": !1
            };
            fetch.showAlerts(obj)
        }
    }
}]).controller('sepaCtrl', ['$scope', 'helper', '$uibModal', 'modalFc', 'data', 'list', function($scope, helper, $uibModal, modalFc, data, list) {
    console.log('Sepa');
    $scope.views = [{
        name: helper.getLanguage('To be sent'),
        id: 1,
        active: ''
    }, {
        name: helper.getLanguage('Sent'),
        id: 2,
        active: ''
    }, {
        name: helper.getLanguage('Archived'),
        id: -1
    }];
    $scope.activeView = 1;
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'invoice-sepa',
        view: $scope.activeView,
        xget: '',
        offset: 1
    };
    $scope.search_mand = {
        search: '',
        'do': 'invoice-sepa',
        view: $scope.activeView,
        xget: 'mandates',
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.ord = '';
    $scope.reverse = !1;
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.tab = d.data.tab;
            //$scope.show_deliveries = d.data.show_deliveries;
            //$scope.show_projects = d.data.show_projects;
            //$scope.show_interventions = d.data.show_interventions;

            if ($scope.tab == 2) {
                $scope.pdf_lg = d.data.pdf_lg
            }
            $scope.initCheckAll();
        }
        //$scope.getAmounts();
    };

    $scope.getAmounts = function(doRefresh) {
        $scope.nr_show = !0;
        helper.doRequest('get', 'index.php', {
            'do': 'invoice-invoices',
            'xget': 'Amounts',
            'refresh_data': (doRefresh ? 1 : 0)
        }, function(r) {
            if (r.data.amount_o) {
                $scope.amount_o = r.data.amount_o
            }
            if (r.data.total_amount_p) {
                $scope.total_amount_p = r.data.total_amount_p
            }
            if (r.data.amount_s) {
                $scope.amount_s = r.data.amount_s
            }
            if (r.data.number_o) {
                $scope.number_o = r.data.number_o
            }
            if (r.data.date_o) {
                $scope.date_o = r.data.date_o
            }
            if (r.data.number_p) {
                $scope.number_p = r.data.number_p
            }
            if (r.data.amount_p) {
                $scope.amount_p = r.data.amount_p
            }
            if (r.data.amount_days) {
                $scope.amount_days = r.data.amount_days
            }
            if (r.data.date_p) {
                $scope.date_p = r.data.date_p
            }
            if (r.data.number_s) {
                $scope.number_s = r.data.number_s
            }
            if (r.data.date_s) {
                $scope.date_s = r.data.date_s
            }
            $scope.nr_show = !1;
            if (doRefresh) {
                angular.element('#refresh_amounts').removeClass("fa-spin fa-1x fa-fw");
            }
        });
    }

    $scope.initCheckAll = function() {
        var total_checked = 0;
        for (x in $scope.list) {
            if ($scope.list[x].add_to_pdf) {
                total_checked++;
            }
        }
        if ($scope.list.length > 0 && $scope.list.length == total_checked) {
            $scope.listObj.check_add_all = true;
        } else {
            $scope.listObj.check_add_all = false;
        }
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        if ($scope.tab == 1) {
            $scope.search[item] = val;
            var params = {
                view: $scope.activeView,
                offset: $scope.search.offset
            };
            for (x in $scope.search) {
                params[x] = $scope.search[x]
            }
        } else {
            $scope.search_mand[item] = val;
            var params = {
                view: $scope.activeView,
                offset: $scope.search_mand.offset
            };
            for (x in $scope.search_mand) {
                params[x] = $scope.search_mand[x]
            }
        }
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        if ($scope.tab == 1) {
            var data = angular.copy($scope.search)
        } else {
            var data = angular.copy($scope.search_mand)
        }
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.renderList(r)
        })
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.orderBY = function(p) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        if ($scope.tab == 1) {
            var params = {
                view: $scope.activeView,
                offset: $scope.search.offset,
                desc: $scope.reverse,
                order_by: $scope.ord
            };
            for (x in $scope.search) {
                params[x] = $scope.search[x]
            }
        } else {
            var params = {
                view: $scope.activeView,
                offset: $scope.search_mand.offset,
                desc: $scope.reverse,
                order_by: $scope.ord
            };
            for (x in $scope.search_mand) {
                params[x] = $scope.search_mand[x]
            }
        }
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.get_invoices = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', $scope.search, function(r) {
            $scope.renderList(r)
        })
    }
    $scope.get_mandates = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', $scope.search_mand, function(r) {
            $scope.renderList(r)
        })
    }
    $scope.checkAll = function(item) {
        /*for (x in $scope.list) {
            $scope.list[x].add_to_pdf = $scope.listObj.check_add_all
        }*/
        var items = [];
        if (item == undefined) {
            for (x in $scope.list) {
                items.push($scope.list[x].id);
                $scope.list[x].add_to_pdf = $scope.listObj.check_add_all
            }
        } else {
            var elem_counter = 0;
            for (x in $scope.list) {
                if ($scope.list[x].add_to_pdf) {
                    elem_counter++;
                }
            }
            if (elem_counter == $scope.list.length && $scope.list.length > 0) {
                $scope.listObj.check_add_all = true;
            } else {
                $scope.listObj.check_add_all = false;
            }
        }
        var params = {
            'do': 'invoice--invoice-saveSepaCheck',
            'value': item ? item.add_to_pdf : $scope.listObj.check_add_all,
            'item': item ? item.id : items,
            'all': item ? undefined : !0
        };
        helper.doRequest('post', 'index.php', params);
    }
    $scope.export_xml = function() {
        var ready_export = !1;
        var data_export = [];
        for (x in $scope.list) {
            if ($scope.list[x].add_to_pdf) {
                ready_export = !0;
                data_export.push($scope.list[x].invoice_id)
            }
        }
        if (!ready_export) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Select at least one invoice")
                },
                "notice": !1,
                "success": !1
            };
            $scope.showAlerts(obj)
        } else {
            var data = {
                'invoice_export': data_export,
                'base_url': $scope.base_url
            };
            openModal("sepa_collection", data, "md")
        }
    }
    $scope.showPdf = function(item) {
        var data = angular.copy(item);
        data.pdf_lg = $scope.pdf_lg;
        openModal("sepa_pdf", data, "md")
    }
    $scope.showMandate = function(type) {
        var mod_size = type == 'tmp' ? 'lg' : 'md';
        openModal("show_mandate", type, mod_size)
    }
    $scope.deleteMandate = function(index) {
        var data = {};
        data.do = 'invoice-sepa-invoice-delete_mandate';
        data.xget = 'mandates';
        data.mandate_id = $scope.list[index].id_mandate;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.showAlerts(r);
            $scope.renderList(r)
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'sepa_collection':
                var iTemplate = 'SelectCollection';
                var ctas = 'col';
                break;
            case 'sepa_pdf':
                var iTemplate = 'SepaPdf';
                var ctas = 'sp';
                break;
            case 'show_mandate':
                var iTemplate = 'ShowMandate';
                var ctas = 'show';
                break
        }
        var params = {
            template: 'iTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'sepa_collection':
                params.item = item;
                params.callback = function() {
                    helper.doRequest('get', 'index.php', $scope.search, function(r) {
                        $scope.renderList(r)
                    })
                }
                break;
            case 'sepa_pdf':
                params.item = item;
                break;
            case 'show_mandate':
                params.params = {
                    'do': 'invoice-mandate_edit',
                    'mandate_id': item
                };
                params.callback = function() {
                    helper.doRequest('get', 'index.php', $scope.search_mand, function(r) {
                        $scope.renderList(r)
                    })
                }
                break
        }
        modalFc.open(params)
    }
    $scope.renderList(data)
}]).controller('SelectCollectionModalCtrl', ['$scope', '$window', 'helper', '$uibModalInstance', 'data', function($scope, $window, helper, $uibModalInstance, data) {
    var col = this;
    col.obj = data;
    col.datePickOpen = {
        col_date: !1
    };
    col.format = 'dd/MM/yyyy';
    col.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    col.showAlerts = function(d) {
        helper.showAlerts(col, d)
    }
    col.cancel = function() {
        $uibModalInstance.close()
    }
    col.ok = function() {
        var obj = angular.copy(col.obj);
        obj.do = 'invoice--invoice-markAsXML';
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            $window.open('index.php?do=invoice-export_xml', '_blank');
            $uibModalInstance.close()
        })
    }
}]).controller('SepaPdfModalCtrl', ['$scope', '$window', 'helper', '$uibModalInstance', 'data', function($scope, $window, helper, $uibModalInstance, data) {
    var sp = this;
    sp.obj = data;
    sp.obj.lang_id = '1';
    sp.showAlerts = function(d) {
        helper.showAlerts(sp, d)
    }
    sp.cancel = function() {
        $uibModalInstance.close()
    }
    sp.ok = function() {
        $window.open('index.php?do=invoice-invoice_sepa&mandate_id=' + sp.obj.id_mandate + '&type=' + sp.obj.type + '&pdf_lang=' + sp.obj.lang_id, '_blank')
    }
}]).controller('ShowMandateModalCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'data', 'editItem', function($scope, $timeout, helper, $uibModalInstance, data, editItem) {
    var show = this;
    show.item = data;
    show.app = 'invoice';
    show.ctrlpag = 'mandate_edit';
    show.app_mod = 'sepa';
    show.item_id = show.item.mandate_id;
    show.oldObj = {};
    show.item.validity_date = new Date();
    show.removedCustomer = !1;
    show.item.add_customer = !1;
    show.contactAddObj = {};
    show.auto = {
        options: {
            contacts: [],
            addresses: [],
            cc: []
        }
    };
    if (show.item_id == 'tmp') {
        show.auto.options.contacts = show.item.contacts;
        show.auto.options.addresses = show.item.addresses;
        show.auto.options.cc = show.item.cc
    }
    show.renderPage = renderPage;

    function renderPage(d) {
        if (d && d.data) {
            show.item = d.data;
            show.auto.options.contacts = d.data.contacts;
            show.auto.options.addresses = d.data.addresses;
            show.auto.options.cc = d.data.cc;
            show.contactAddObj = {
                country_dd: show.item.country_dd,
                country_id: show.item.main_country_id,
                language_dd: show.item.language_dd,
                language_id: show.item.language_id,
                gender_dd: show.item.gender_dd,
                gender_id: show.item.gender_id,
                title_dd: show.item.title_dd,
                title_id: show.item.title_id,
                add_contact: !0,
                buyer_id: show.item.buyer_id,
                article: show.item.tr_id,
                'do': 'invoice-mandate_edit-invoice-tryAddC',
                identity_id: show.item.identity_id,
                contact_only: !0,
                mandate_id: show.item.mandate_id
            }
        }
    }
    if (show.item.mandate_id != 'tmp') {
        if (show.item.signature_date) {
            show.item.signature_date = new Date(show.item.signature_date)
        } else {
            show.item.signature_date = new Date()
        }
    }
    show.datePickOpen = {
        signature_date: !1
    };
    show.format = 'dd/MM/yyyy';
    show.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    show.showTxt = {
        addBtn: !0,
        address: !1,
    }
    editItem.init(show);
    show.showAlerts = function(d, formObj) {
        helper.showAlerts(show, d, formObj)
    }
    show.cancel = function() {
        $uibModalInstance.close()
    }
    show.ok = function(formObj) {
        var data = angular.copy(show.item);
        data.do = data.mandate_id == 'tmp' ? 'invoice--invoice-add_mandate' : data.do_next;
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //show.showAlerts(r, formObj);
            if (r.success && show.item.mandate_id == 'tmp') {
                $uibModalInstance.close()
            } else if (r.error) {
                show.showAlerts(r, formObj);
            } else {
                $uibModalInstance.close()
            }
        })
    }
    show.uploadDrop = function(obj, event) {
        helper.uploadDropSepa(obj, event)
    }
    show.removeFile = function() {
        var data = {};
        data.do = 'invoice--invoice-originalPDF_delete';
        data.mandate_id = show.item.mandate_id;
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            show.showAlerts(r);
            if (r.success) {
                show.item.PDF_hide = !0;
                show.item.name_PDF = !1;
                show.item.original_name = ''
            }
        })
    }
    show.showEditFnc = function() {
        show.showTxt.address = !1;
        show.item.save_final = !0;
        show.oldObj = {
            buyer_id: angular.copy(show.item.buyer_id),
            contact_id: angular.copy(show.item.contact_id),
            contact_name: angular.copy(show.item.contact_name),
            BUYER_NAME: angular.copy(show.item.buyer_name),
            delivery_address: (show.item.sameAddress ? !1 : angular.copy(show.item.delivery_address)),
            delivery_address_id: (show.item.sameAddress ? !1 : angular.copy(show.item.delivery_address_id)),
            delivery_address_txt: (show.item.sameAddress ? !1 : angular.copy(show.item.delivery_address_txt)),
            view_delivery: (show.item.sameAddress ? !1 : angular.copy(show.item.view_delivery)),
            sameAddress: angular.copy(show.item.sameAddress),
        }
    }
    show.showCCSelectize = function() {
        show.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }
    show.removeDeliveryAddr = function() {
        show.item.delivery_address = '';
        show.item.delivery_address_id = ''
    }
}]).controller('ProgressCreateInvoiceModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var prog = this;
    prog.obj = data;
    prog.start = !0;
    prog.value = 0;
    var i = 0;
    var increment = 0;
    prog.showAlerts = function(d) {
        helper.showAlerts(prog, d)
    }
    prog.ok = function() {
        helper.doRequest('post', 'index.php', {
            'do': 'invoice--invoice-start_to_invoice'
        }, function(r) {
            if (prog.obj.typeOfInvoice == '2' && prog.obj.post_list == 'interventions' && prog.obj.is_cozie) {
                angular.element('.loading_alt').removeClass('hidden');
                helper.doRequest('post', 'index.php', {
                    'do': 'invoice--invoice-checkMultipleInt',
                    'services': angular.copy(prog.obj.interventions)
                }, function(resp) {
                    angular.element('.loading_alt').addClass('hidden');
                    if (resp.error) {
                        prog.showAlerts(resp)
                    }
                    if (resp.success) {
                        prog.start = !1;
                        var params = angular.copy(prog.obj);
                        postIt(params, i, r.data.same_invoice_time)
                    }
                })
            } else {
                prog.start = !1;
                var params = angular.copy(prog.obj);
                postIt(params, i, r.data.same_invoice_time)
            }
        })
    };
    prog.cancel = function() {
        $uibModalInstance.close({ 'app': prog.obj.post_list, 'done': prog.start });
    };

    function postIt(params, i, time) {
        if (params[prog.obj.post_list].length) {
            var data = {};
            data.do = 'invoice--invoice-to_invoice';
            data.type = prog.obj.post_list;
            data.item = params[prog.obj.post_list][0];
            data.same_time = time;
            data.typeOfInvoice = prog.obj.typeOfInvoice;
            data.debug = 1;
            angular.element('.loading_alt').removeClass('hidden');
            helper.doRequest("post", "index.php", data, function(o) {
                i++;
                var percent = i * 100 / prog.obj[prog.obj.post_list].length;
                prog.value = parseInt(percent.toFixed(0));
                if (o.data.serial_number == 'no_number') {
                    if (o.data.actiune_id == '1') {
                        increment++
                    }
                    o.data.serial_number = increment.toString()
                }
                if (o.error.error) {
                    var obj = {
                        "error": {
                            "error": helper.getLanguage("Invoice no") + ": " + o.data.serial_number + ' - ' + o.data.actiune + ' ' + o.error.error
                        },
                        "notice": !1,
                        "success": !1
                    };
                    prog.showAlerts(obj)
                } else if (!o.error) {
                    var obj = {
                        "error": !1,
                        "notice": !1,
                        "success": {
                            "success": helper.getLanguage("Invoice no") + ": " + o.data.serial_number + ' - ' + o.data.actiune + ' <a href="invoice/view/' + (o.data.same_invoice_id ? o.data.same_invoice_id : o.data.invoice_id) + '" target="_blank" class="text-link">' + helper.getLanguage('View document') + '</a>'
                        }
                    };
                    prog.showAlerts(obj)
                }
                params[prog.obj.post_list].shift();
                postIt(params, i, time)
            })
        } else {
            angular.element('.loading_alt').addClass('hidden')
        }
    }
}]).controller('CustomProjectHoursModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var prog = this;
    prog.old = data;
    prog.obj = {};
    prog.format = 'dd/MM/yyyy';
    prog.datePickOpen = {
        from: !1,
        to: !1
    };
    prog.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    prog.showAlerts = function(d) {
        helper.showAlerts(prog, d)
    }
    prog.cancel = function() {
        if (prog.obj.custom_hours == undefined) {
            prog.old.custom_hour = '0'
        }
        $uibModalInstance.close()
    };
    prog.ok = function() {
        var err = 0;
        if (prog.obj.from == undefined || prog.obj.to == undefined || prog.obj.custom_hours == undefined) {
            err = 1
        } else if ((prog.obj.from.getTime() / 1000 < parseInt(prog.obj.begin_time)) || (prog.obj.from.getTime() / 1000 > parseInt(prog.obj.end_time))) {
            err = 1
        } else if ((prog.obj.to.getTime() / 1000 < parseInt(prog.obj.begin_time)) || (prog.obj.to.getTime() / 1000 > parseInt(prog.obj.end_time))) {
            err = 1
        }
        if (err == 1) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Invalid Interval")
                },
                "notice": !1,
                "success": !1
            };
            prog.showAlerts(obj);
            return !1
        }
        var add_on = '&custom_hours=' + prog.obj.custom_hours;
        if (prog.obj.custom_hours == 3) {
            add_on += '&from=' + prog.obj.from.getTime() / 1000 + '&to=' + (prog.obj.to.getTime() / 1000)
        }
        prog.old.val += add_on;
        delete prog.obj;
        $uibModalInstance.close()
    };
    angular.element('.loading_alt').removeClass('hidden');
    helper.doRequest('get', 'index.php', {
        'do': 'invoice-custome_hours',
        'val': prog.old.val
    }, function(r) {
        angular.element('.loading_alt').addClass('hidden');
        prog.obj = r.data;
        if (prog.obj.from) {
            prog.obj.from = new Date(prog.obj.from)
        }
        if (prog.obj.to) {
            prog.obj.to = new Date(prog.obj.to)
        }
    })
}]).controller('ResendPostGreenModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var post = this;
    post.obj = data;
    post.cancel = function() {
        $uibModalInstance.close()
    };
    post.showAlerts = function(d) {
        helper.showAlerts(post, d)
    }
    post.stationaryCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select E-Stationary'),
        create: !1,
        maxOptions: 100,
        closeAfterSelect: !0,
        maxItems: 1
    };
    post.resend = function() {
        var obj = angular.copy(post.obj.old_obj);
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                post.showAlerts(r)
            }
        })
    }
}]).controller('FailedExportModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.search = {
        'do': 'invoice-export_fail',
        'offset': 1,
        'app': vm.obj.app
    };
    vm.renderList = function(d) {
        if (d) {
            vm.obj = d.data
        }
    }
    vm.cancel = function() {
        $uibModalInstance.close()
    };
    vm.clear = function() {
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'invoice--invoice-clearFailedExport',
            'app': vm.obj.app,
            'module': 'invoice'
        }, function(o) {
            angular.element('.loading_alt').removeClass('hidden');
            if (o.success) {
                angular.element('.loading_alt').removeClass('hidden');
                helper.doRequest('get', 'index.php', {
                    'do': 'invoice-export_fail',
                    'offset': 1,
                    'app': vm.obj.app
                }, function(r) {
                    angular.element('.loading_alt').addClass('hidden');
                    if (!angular.element('.hid').hasClass('hidden')) {
                        angular.element('.hid').addClass('hidden')
                    }
                    vm.obj = r.data
                })
            }
        })
    }
    vm.get_more = function() {
        var obj = angular.copy(vm.search);
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            angular.element('.loading_alt').addClass('hidden');
            vm.obj = r.data
        })
    }
}]).controller('YukiStatusModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('invoiceNrModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vmMod = this;
    vmMod.obj = data;

    vmMod.cancel = function() {
        $uibModalInstance.close()
    }

    vmMod.ok = function() {
        if(isNaN(parseInt(vmMod.obj.account_digits)) || isNaN(parseInt(vmMod.obj.invoice_starting_number))){
            return false;
        }
        var get = 'convention';
        var data = angular.copy(vmMod.obj);
        data.do = 'invoice-settings-invoice-Convention';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
           $uibModalInstance.close(true)
        })
    }

    vmMod.refreshInvoiceExample=function(){
        vmMod.obj.example=vmMod.obj.account_number.toString()+vmMod.obj.invoice_starting_number.toString().padStart(vmMod.obj.account_digits,"0");
    }

}]).controller('ReminderSentDateModalCtrl', ['$scope', '$state', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, $state, helper, $uibModalInstance, data, modalFc) {
    var remsdate = this;
    remsdate.obj = data;
    if (remsdate.obj.sent_date) {
        remsdate.obj.sent_date = new Date(remsdate.obj.sent_date * 1000)
    } else {
        remsdate.obj.sent_date = new Date()
    }
    remsdate.title = data.title;
    remsdate.openDate = openDate;
    remsdate.format = remsdate.obj.account_date_format;
    remsdate.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    remsdate.datePickOpen = {
        date: !1,
    }
    remsdate.cancel = function() {
        $uibModalInstance.close('close')
    }
    remsdate.ok = function() {
        var data = angular.copy(remsdate.obj);
        data.do = 'invoice--invoice-update_sent_date';
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }

    function openDate(p) {
        remsdate.datePickOpen[p] = !0
    }
}]).controller('PurchaseInvoiceLinkPoModalCtrl', ['$scope', '$state', 'helper', '$uibModalInstance', 'data', function($scope, $state, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    if (vm.obj.orders && vm.obj.orders.length == 1) {
        vm.obj.order_id = vm.obj.orders[0].id;
    }

    vm.porderAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Purchase Order'),
        maxItems: 1
    };

    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d)
    }

    vm.cancel = function() {
        $uibModalInstance.close();
    }
    vm.ok = function(formObj) {
        var data = angular.copy(vm.obj);
        data.do = 'invoice-purchase_invoice-invoice-updatePurchaseInvoiceOrder';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(r);
            }
        })
    }

}]).controller('PaymentIncommingModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', '$compile', '$uibModal', 'modalFc', function($scope, helper, $uibModalInstance, data, $compile, $uibModal, modalFc) {
    var editPay = this;
    var date = new Date();
    editPay.format = 'dd/MM/yyyy';
    editPay.datePickOpen = {
        payment_date: !1
    };
    editPay.dateOptions = {
        dateFormat: '',
    };
    if (data.payment_date) {
        data.payment_date = data.payment_date = new Date(data.payment_date)
    }
    editPay.obj = data;

    editPay.ok = function(formObj) {
        var data = angular.copy(editPay.obj);
        data.do = 'invoice-purchase_invoice-invoice-edit_payment_incomming_invoice';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                editPay.showAlerts(r, formObj)
            } else {
                $uibModalInstance.close(r)
            }
        })
    };
    editPay.cancel = function() {
        $uibModalInstance.close()
    };
    editPay.showAlerts = function(d, formObj) {
        helper.showAlerts(editPay, d, formObj)
    }
}]).controller('RecurringInvoiceViewCtrl', ['$scope', 'helper', '$stateParams', '$state', '$uibModal', '$confirm', 'modalFc', 'selectizeFc', 'editItem', 'data', function($scope, helper, $stateParams, $state, $uibModal, $confirm, modalFc, selectizeFc, editItem, data) {
    var view = this;
    view.recurring_invoice_id = $stateParams.recurring_invoice_id;
    $scope.$parent.isAddPage = !0;
    view.invoice = {};
    view.invoice.item_exists = true;
    var baseshowTxt = {
        viewNotes: !1
    };

    view.renderPage = function(d, noAlert) {
        view.showTxt = angular.copy(baseshowTxt);
        if (d.data) {
            if(!d.data.item_exists){
                view.invoice.item_exists = false;
                return 0;
            }; 
            if (d.data.redirect) {
                $state.go(d.data.redirect);
                return !1
            }
            for (x in d.data) {
                view.invoice[x] = d.data[x]
            }
            $scope.$parent.type_title = view.invoice.type_title;
            $scope.$parent.nr_status = view.invoice.nr_status;

            if (!noAlert) {
                /*view.showAlerts(d)*/
            }
        }
    }

    view.showAlerts = function(d, formObj) {
        helper.showAlerts(view, d, formObj)
    }

    view.statusInvoice = function(status) {
        var tmp_obj = { 'recurring_invoice_id': view.recurring_invoice_id };
        tmp_obj.do = status == '1' ? 'invoice-recurring_invoice-invoice-archive_recurring' : 'invoice-recurring_invoice-invoice-activater';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_obj, function(r) {
            if (r.success) {
                view.renderPage(r);
                helper.refreshLogging();
            } else {
                /*view.showAlerts(r);*/
            }
        });
    }

    view.deleteInvoice = function() {
        var tmp_obj = { 'recurring_invoice_id': view.recurring_invoice_id };
        tmp_obj.do = 'invoice-recurring_invoices-invoice-delete_recurring';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_obj, function(r) {
            if (r.success) {
                helper.setItem('invoice-recurring_invoices', {});
                $state.go("recinvoices");
            } else {
                view.showAlerts(r);
            }
        });
    }
    view.showCustomerNotes = function(scope, buyer_id, customer_notes) {
        editItem.openModal(scope, 'EditCustomerNotes', { "buyer_id": buyer_id, "customer_notes": customer_notes }, 'lg');
    }

    helper.doRequest('get', 'index.php?do=invoice-recurring_invoice', { 'recurring_invoice_id': $stateParams.recurring_invoice_id }, view.renderPage);

}]);