app.controller('OrdersCtrl', ['$scope', '$rootScope','$confirm', 'helper', 'list', 'data', 'modalFc', function($scope, $rootScope, $confirm, helper, list, data, modalFc) {
    console.log('Orders list');
    $rootScope.opened_lists.orders=true;
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    $scope.views = [];
    $scope.activeView = 4;
    $scope.invoice_status_id=0;
    $scope.list = [];
    $scope.nav = [];
    $scope.listStats = [];
    $scope.invoice_statuses=[];
    $scope.search = {
        search: '',
        'do': 'order-orders',
        view: 4,
        xget: '',
        showAdvanced: !1,
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.show_load = !0;
    $scope.minimum_selected=false;
    $scope.all_pages_selected=false;
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.listStats = [];
            $scope.lid = d.data.lid;
            $scope.type = d.data.pdf_type;
            $scope.lr = d.data.lr;
            $scope.export_args = d.data.export_args;
            $scope.post_active = d.data.post_active;
            $scope.columns=d.data.columns;
            $scope.views=d.data.views;
            $scope.minimum_selected=d.data.minimum_selected;
            $scope.all_pages_selected=d.data.all_pages_selected;
            $scope.nav = d.data.nav;
            $scope.invoice_statuses=d.data.invoice_statuses;
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['order_id']);
                count ++;
            }
            sessionStorage.setItem('order.view_total', count);
            sessionStorage.setItem('order.view_list', JSON.stringify(ids));

            //$scope.initCheckAll();
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1,
        stop_date: !1,
        d_start_date: !1,
        d_stop_date: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.archived = function(value) {
        $scope.search.archived = 0;
        if (value == -1) {
            $scope.search.archived = 1
        }
        $scope.filters(value)
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList, true)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList, true)
    };
    $scope.searchThing = function(reset_list) {
        angular.element('.loading_wrap').removeClass('hidden');
        var params=angular.copy($scope.search);
        if(reset_list){
            params.reset_list='1';
        }     
        list.search(params, $scope.renderList, $scope)
    };
    $scope.removeInvoiceStatus = function(){
        $scope.search.invoice_status_id=0;
        $scope.invoice_status_id=0;
        $scope.searchThing(true);
    };
    $scope.resetAdvanced = function() {
        $scope.search.article_name_search = undefined;
        $scope.search.your_reference_search = undefined;
        $scope.search.commercial_name_search = undefined;
        $scope.search.showAdvanced = !1;
        $scope.searchThing(true)
    };
    $scope.saveForPDF = function(i) {
        if(i){
            if(i.check_add_to_product){
              var elem_counter=0;
              for(x in $scope.list){
                if($scope.list[x].check_add_to_product){
                  elem_counter++;
                }
              }
              if(elem_counter==$scope.list.length && $scope.list.length>0){
                $scope.listObj.check_add_all=true;
              }
            }else{
              $scope.listObj.check_add_all=false;
            }
        }  
        list.saveForPDF($scope, 'order--order-saveAddToPdf', i)
    };
    $scope.createExports = function($event) {
        list.exportFnc($scope, $event)
        var params = angular.copy($scope.search);
        params.do = 'order-orders'
        params.exported = !0;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r);
        })
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.markCheckNew=function(i){
        list.markCheckNew($scope, 'order--order-saveAddToPdf', i);
    }
    $scope.checkAllPage=function(){
        $scope.listObj.check_add_all=true;
        $scope.markCheckNew();
    }
    $scope.checkAllPages=function(){
        list.markCheckNewAll($scope, 'order--order-saveAddToPdfAll','1');
    }
    $scope.uncheckAllPages=function(){
        list.markCheckNewAll($scope, 'order--order-saveAddToPdfAll','0');
    }
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.invoiceCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Invoice Statuses'),
        create: !1,
        onChange(value){
            if (value == undefined) {
                value = 0
            }
            $scope.search.invoice_status_id=value;
            $scope.searchThing(true);
        },
        maxItems: 1
    };
    $scope.greenpost = function(item) {
        if ($scope.post_active) {
            var data = {};
            for (x in $scope.search) {
                data[x] = $scope.search[x]
            }
            data.do = 'order-orders-order-postIt';
            data.id = item.id;
            data.related = 'list';
            data.no_status = !0;
            data.post_library = $scope.post_library;
            openModal("resend_post", data, "md")
        } else {
            var data = {};
            data.clicked_item = item;
            for (x in $scope.search) {
                data[x] = $scope.search[x]
            }
            openModal("post_register", data, "md")
        }
    }
    $scope.resendPost = function(item) {
        var data = {};
        for (x in $scope.search) {
            data[x] = $scope.search[x]
        }
        data.do = 'order-orders-order-postIt';
        data.id = item.id;
        data.related = 'list';
        data.post_library = $scope.post_library;
        openModal("resend_post", data, "md")
    }

    $scope.columnSettings = function() {
        openModal('column_set', {}, 'lg')
    }

    $scope.initCheckAll=function(){
        var total_checked=0;
        for(x in $scope.list){
            if($scope.list[x].check_add_to_product){
                total_checked++;
            }
        }
        if($scope.list.length > 0 && $scope.list.length == total_checked){
            $scope.listObj.check_add_all=true;
        }else{
            $scope.listObj.check_add_all=false;
        }
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'post_register':
                var iTemplate = 'PostRegister';
                var ctas = 'post';
                break;
            case 'resend_post':
                var iTemplate = 'ResendPostGreen';
                var ctas = 'post';
                break;
            case 'column_set':
                var iTemplate = 'ColumnSettings';
                var ctas = 'vm';
                break;
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'post_register':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.item.do = 'order-orders-order-postRegister';
                params.callback = function(data) {
                    if (data && data.data) {
                        $scope.renderList(data);
                        $scope.greenpost(data.clicked_item)
                    }
                    $scope.showAlerts(data)
                }
                break;
            case 'resend_post':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.request_type = 'post';
                params.params = {
                    'do': 'order--order-postOrderData',
                    'order_id': item.id,
                    'related': 'status',
                    'old_obj': item
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.renderList(d);
                        $scope.showAlerts(d)
                    }
                }
                break;
            case 'column_set':
                params.template = 'miscTemplate/ColumnSettingsModal';
                params.params = {
                    'do': 'misc-column_set',
                    'list': 'orders'
                };
                params.callback = function(d) {
                    if (d) {
                        $scope.searchThing();
                    }
                }
                break;
        }
        modalFc.open(params)
    }
    list.init($scope, 'order-orders', $scope.renderList, ($rootScope.reset_lists.orders ? true : false));
    $rootScope.reset_lists.orders=false;
    $scope.show_load = !0;
    helper.doRequest('get', 'index.php', {
        'do': 'misc-post_library'
    }, function(r) {
        $scope.show_load = !1;
        $scope.post_library = r.data.post_library
    })
}]).controller('OrderCtrl', ['$scope', 'helper', function($scope, helper) {
    $scope.serial_number = '';
    $scope.is_view = !1;
    $scope.order_id = ''
}]).controller('OrderViewCtrl', ['$scope', '$state', '$cacheFactory', 'helper', '$timeout','$stateParams', '$confirm', 'editItem', 'modalFc', 'data', 'selectizeFc', 'Lightbox',function($scope, $state, $cacheFactory, helper,$timeout, $stateParams, $confirm, editItem, modalFc, data, selectizeFc, Lightbox) {
    $scope.order_id = $stateParams.order_id;
    $scope.$parent.isAddPage = !0;
    $scope.$parent.is_view = !0;
    $scope.$parent.order_id = $stateParams.order_id;
    $scope.order = [];
    $scope.calcTotal = calcTotal;
    $scope.updateVat = updateVat;
    $scope.tmpl = 'otemplate';
    $scope.app = 'order';
    $scope.showTxt = {
        sendEmail: !1,
        drop: !1,
        markAsReady: !1,
        viewNotes: !1,
        sendPost: !1,
        source: !1,
        type: !1,
        segment: !1,
        edit_delivery_date:!1,
        show_edit_index:null
    };

    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        delivery_date: !1
    }

    $scope.email = {};
    $scope.auto = {
        options: {
            recipients: []
        }
    };
    $scope.stationaryCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select E-Stationary'),
        create: !1,
        maxOptions: 100,
        closeAfterSelect: !0,
        maxItems: 1
    };
    $scope.selectfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    $scope.tinymceOptions = {
        selector: ".mail_send",
        resize: "height",
        height: 184,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
    };
    $scope.renderPage = function(d) {
        if (d.data) {
            $scope.order = d.data;
            if(d.data.del_date){
                $scope.order.del_date=new Date(d.data.del_date);
            }
            $scope.$parent.serial_number = $scope.order.factur_nr;
            helper.updateTitle($state.current.name,$scope.order.factur_nr);
            $scope.$parent.type_title = $scope.order.type_title;
            $scope.$parent.nr_status = $scope.order.nr_status;
            $scope.auto.options.recipients = d.data.recipients;
            $scope.email = {
                e_subject: $scope.order.subject,
                e_message: $scope.order.e_message,
                email_language: $scope.order.languages,
                include_pdf: $scope.order.include_pdf,
                order_id: $scope.order_id,
                files: $scope.order.files,
                use_html: $scope.order.use_html,
                copy_acc: $scope.order.copy_acc,
                user_loged_email: $scope.order.user_loged_email,
                user_acc_email: $scope.order.user_acc_email,
                recipients: [],
                sendgrid_selected : d.data.sendgrid_selected
            };
            $scope.email.recipients = d.data.recipients_ids;
            $scope.save_disabled = d.data.save_disabled;
            $scope.showAlerts(d);
            if (!d.error) {
                $scope.showDrop();
            }
        }
    }
    editItem.init($scope);
    $scope.showPreview = function(exp) {
        var params = {
            template: 'miscTemplate/modal',
            ctrl: 'viewRecepitCtrl',
            size: 'md',
            ctrlAs: 'vmMod',
            params: {
                'do': 'order-email_preview',
                order_id: $scope.order_id,
                message: exp.e_message,
                use_html: exp.use_html
            }
        }
        modalFc.open(params)
    };
    $scope.dispatchModal = function() {
        var params = {
            template: 'otemplate/dispatchModal',
            ctrl: 'DispatchModalCtrl',
            size: 'md',
            item: {
                'del_note': $scope.order.del_note,
                'do': 'order-order-order-addDispatchNote',
                'order_id': $scope.order_id
            },
            callback: function(data) {
                if (data) {
                    $scope.order.del_note_txt = data.data.del_note_txt;
                    $scope.order.del_note = data.data.del_note;
                    $scope.showAlerts(data)
                }
            }
        }
        modalFc.open(params)
    };
    $scope.sendEmails = function(r) {
        var d = angular.copy(r);
        $scope.showTxt.sendEmail = !1;

        if (d.recipients.length) {
            /*if ($scope.order.dropbox) {
                d.dropbox_files = $scope.order.dropbox.dropbox_files
            }*/
            if (Array.isArray(d.recipients)) {
                d.new_email = d.recipients[0]
            } else {
                d.new_email = d.recipients
                d.recipients = d.recipients.split();

            }
            if (d.new_email) {
                $scope.email.alerts.push({
                    type: 'email',
                    msg: d.new_email + ' - '
                });
                if(!d.delivery_id){
                    d.do = 'order--order-sendNew';
                }            
                d.use_html = $scope.email.use_html;
                d.lid = d.email_language;
                d.serial_number = $scope.order.serial_number;
                helper.doRequest('post', 'index.php', d, function(r) {
                    if (r.success && r.success.success) {
                        $scope.email.alerts[$scope.email.alerts.length - 1].type = 'success';
                        $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.success.success
                    }
                    if (r.notice && r.notice.notice) {
                        $scope.email.alerts[$scope.email.alerts.length - 1].type = 'info';
                        $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.notice.notice
                    }
                    if (r.error && r.error.error) {
                        $scope.email.alerts[$scope.email.alerts.length - 1].type = 'danger';
                        $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.error.error
                    }
                    if (Array.isArray(d.recipients)) {
                        d.recipients.shift();
                        if( d.new_email == d.user_loged_email){
                            d.copy = !1;
                        }
                        if(d.new_email == d.user_acc_email){
                            d.copy_acc = !1;
                        }
                        $scope.sendEmails(d)
                    }
                })
            } else {
                if (Array.isArray(d.recipients)) {
                    d.recipients.shift();
                    $scope.sendEmails(d)
                }
            }
        } else {
            if (d.user_loged_email && d.copy) {
                d.new_email = d.user_loged_email;
                d.copy = !1;
                d.mark_as_sent = 0;
                /*if ($scope.order.dropbox) {
                    d.dropbox_files = $scope.order.dropbox.dropbox_files
                }*/
                if (d.new_email) {
                    $scope.email.alerts.push({
                        type: 'email',
                        msg: d.new_email + ' - '
                    });
                    if(!d.delivery_id){
                        d.do = 'order--order-sendNew';
                    }                 
                    d.use_html = $scope.email.use_html;
                    d.lid = d.email_language;
                    d.serial_number = $scope.order.serial_number;
                    helper.doRequest('post', 'index.php', d, function(r) {
                        if (r.success && r.success.success) {
                            $scope.email.alerts[$scope.email.alerts.length - 1].type = 'success';
                            $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.success.success
                        }
                        if (r.notice && r.notice.notice) {
                            $scope.email.alerts[$scope.email.alerts.length - 1].type = 'info';
                            $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.notice.notice
                        }
                        if (r.error && r.error.error) {
                            $scope.email.alerts[$scope.email.alerts.length - 1].type = 'danger';
                            $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.error.error
                        }
                        d.user_mail_sent =1;
                        $scope.sendEmails(d)
                    })
                }
            } else if (d.user_acc_email && d.copy_acc) {

                if(d.user_mail_sent && d.user_acc_email == d.user_loged_email){
                  
                }else{
                    d.new_email = d.user_acc_email;
                    d.copy_acc = !1;
                    d.mark_as_sent = 0;
                    /*if ($scope.order.dropbox) {
                        d.dropbox_files = $scope.order.dropbox.dropbox_files
                    }*/
                    if (d.new_email) {
                        $scope.email.alerts.push({
                            type: 'email',
                            msg: d.new_email + ' - '
                        });
                        if(!d.delivery_id){
                            d.do = 'order--order-sendNew';
                        }                  
                        d.use_html = $scope.email.use_html;
                        d.lid = d.email_language;
                        d.serial_number = $scope.order.serial_number;
                        helper.doRequest('post', 'index.php', d, function(r) {
                            if (r.success && r.success.success) {
                                $scope.email.alerts[$scope.email.alerts.length - 1].type = 'success';
                                $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.success.success
                            }
                            if (r.notice && r.notice.notice) {
                                $scope.email.alerts[$scope.email.alerts.length - 1].type = 'info';
                                $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.notice.notice
                            }
                            if (r.error && r.error.error) {
                                $scope.email.alerts[$scope.email.alerts.length - 1].type = 'danger';
                                $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.error.error
                            }
                           /* d.recipients.shift();
                            $scope.sendEmails(d)*/
                        })
                    }
                }
            }else{
                 helper.refreshLogging();
            }
        }
         
    }
    $scope.myFunction = function() {
        if ($scope.email.recipients.length) {
            $scope.save_disabled = !1
        } else {
            $scope.save_disabled = !0
        }
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.contactForEmailAutoCfg = selectizeFc.configure({
        placeholder: 'Select recipient',
        create: !0,
        createOnBlur:true,
        maxItems: 100
    });
    $scope.markOrder = function(func) {
        helper.doRequest('post', 'index.php', {
            'do': 'order-order-order-' + func,
            order_id: $scope.order_id
        }, function(r){
            $scope.renderPage(r);
            helper.refreshLogging();
        });
    }
    $scope.openDeliveyModal = function(delivery_id, isTwoSteps, initialItem) {
        var action = isTwoSteps === !0 ? 'order-approve_order_line' : 'order-order_line',
            params = {
                template: 'otemplate/deliveryModal',
                ctrl: 'deliveryModalCtrl',
                ctrlAs: '',
                backdrop: 'static',
                params: {
                    'do': action,
                    order_id: $scope.order_id,
                    delivery_id: delivery_id
                },
                callback: function(data) {
                    if (data) {
                        if (data.data) {
                            $scope.order = data.data;
                            $scope.$parent.type_title = $scope.order.type_title;
                            $scope.$parent.nr_status = $scope.order.nr_status;
                            if(data.data.reopen_delivery){
                                let tmp_open_deliveries=[];
                                for(let x in $scope.order.deliveries){
                                    if($scope.order.deliveries[x].not_approved){
                                        tmp_open_deliveries.push($scope.order.deliveries[x]);
                                    }
                                }
                                if(tmp_open_deliveries.length){              
                                    tmp_open_deliveries.sort((a, b) => (a.delivery_id > b.delivery_id) ? 1 : -1);
                                    let multiple_deliveries=tmp_open_deliveries.map(function(obj) {return {'id':obj.delivery_id,'name':obj.delivey}});
                                    $scope.openDeliveyModal(multiple_deliveries[0].id,true,{'multiple_deliveries':multiple_deliveries});
                                }
                            }
                        }
                        $scope.showAlerts(data)
                    }
                },
                callbackExit:function(data){
                    if(typeof data === 'object'){
                        $scope.openDeliveyModal(data.delivery_id,isTwoSteps,initialItem);
                    }
                }
            };
            if(initialItem){
                params.initial_item = initialItem;
            }
        modalFc.open(params)
    }
  
    $scope.openReturnModal = function(return_id) {
        var params = {
            template: 'otemplate/returnModal',
            ctrl: 'returnModalCtrl',
            ctrlAs: '',
            params: {
                'do': 'order-return_article',
                order_id: $scope.order_id,
                return_id: return_id
            },
            callback: function(data) {
                if (data) {
                    if (data.data) {
                        $scope.order = data.data
                    }
                    $scope.showAlerts(data)
                }
            }
        };
        modalFc.open(params)
    }
       $scope.openCertificateModal=function(){
         var params = {
            template: 'otemplate/CertificateModal',
            ctrl: 'CertificateModalCtrl',
            ctrlAs: '',
            params: {
                'do': 'order-certificate_line',
                order_id: $scope.order_id,
            },
            callback: function(data) {
                if (data) {
                   
                    /*if (data.data) {
                        $scope.order = data.data
                    }*/
                    $scope.show_load = !0;
                    var data_drop = angular.copy($scope.order.drop_info);
                    data_drop.do = 'misc-dropbox_new';
                    helper.doRequest('post', 'index.php', data_drop, function(r) {
                        $scope.order.dropbox = r.data;
                        $scope.show_load = !1
                    })
                    $scope.showAlerts(data)
                }
            }
        };
        modalFc.open(params)
    }
    $scope.openSendModal = function(delivery_id) {
        $scope.email.alerts=[];
        var params = {
            template: 'otemplate/sendModal',
            ctrl: 'sendModalOrdersCtrl',
            ctrlAs: '',
            backdrop:'static',
            params: {
                'do': 'order-send_email',
                order_id: $scope.order_id,
                delivery_id: delivery_id
            },
            callback: function(data) {
                if (data) {
                    $scope.sendEmails(data);
                }
            }
        };
        modalFc.open(params)
    }
    $scope.deleteDelivery = function(link) {
        helper.doRequest('get', link, $scope.renderPage)
    }
    $scope.showPDFsettings = function(i) {
        var params = {
            template: 'otemplate/pdf_settings',
            ctrl: 'pdfSettingsOrderCtrl',
            ctrlAs: '',
            size: 'md',
            params: {
                'do': 'order-pdf_settings',
                order_id: $scope.order_id,
                xls: i
            },
            callback: function(data) {
                if (data) {
                    if (data.data) {
                        $scope.order = data.data
                    }
                    $scope.showAlerts(data)
                }
            }
        };
        modalFc.open(params)
    };
    $scope.showDrop = function() {
        $scope.show_load = !0;
        var data_drop = angular.copy($scope.order.drop_info);
        data_drop.do = 'misc-dropbox_new';
        helper.doRequest('post', 'index.php', data_drop, function(r) {
            $scope.order.dropbox = r.data;
            $scope.show_load = !1
        })
    }
    $scope.deleteDropFile = function(index) {
        var data = angular.copy($scope.order.dropbox.dropbox_files[index].delete_file_link);
        //angular.element('.loading_wrap').removeClass('hidden');
        $scope.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.show_load = !1;
            $scope.showAlerts(r);
            if (r.success) {
                $scope.order.dropbox.nr--;
                $scope.order.dropbox.dropbox_files.splice(index, 1)
            }
        })
    }
    $scope.deleteDropImage = function(index) {
        var data = angular.copy($scope.order.dropbox.dropbox_images[index].delete_file_link);
        $scope.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.show_load = !1;
            $scope.showAlerts(r);
            if (r.success) {
                $scope.order.dropbox.nr--;
                $scope.order.dropbox.dropbox_images.splice(index, 1)
            }
        })
    }
    $scope.openLightboxModal = function(index) {
        Lightbox.openModal($scope.order.dropbox.dropbox_images, index)
    };
    $scope.uploadDoc = function(type) {
        var obj = angular.copy($scope.order.drop_info);
        obj.type = type;
        openModal('uploadDoc', obj, 'lg')
    }

    $scope.removeDispatchNote = function() {
        helper.doRequest('post', 'index.php', {
            'del_note': '',
            'do': 'order-order-order-addDispatchNote',
            'order_id': $scope.order_id
        }, function(data) {
            if (data) {
                $scope.order.del_note_txt = data.data.del_note_txt;
                $scope.order.del_note = data.data.del_note;
                $scope.showAlerts(data)
            }
        })
    }
    $scope.goToInvoice = function(link) {
        var cacheOrder = $cacheFactory.get('OrderToInvoice');
        if (cacheOrder != undefined) {
            cacheOrder.destroy()
        }
        var data = {};
        var pieces = link.split('&');
        for (var i = 0, len = pieces.length; i < len; i++) {
            var piece = pieces[i].split('=');
            data[piece[0]] = piece[1]
        }
        var cache = $cacheFactory('OrderToInvoice');
        cache.put('data', data);
        $state.go('invoice.edit', {
            invoice_id: "tmp"
        })
    }
    $scope.goToPurchase_orders = function(link) {
        /*var data = {};
        var pieces = link.split('&');
        for (var i = 0, len = pieces.length; i < len; i++) {
            var piece = pieces[i].split('=');
            data[piece[0]] = piece[1]
        }
        var cacheQuote = $cacheFactory.get('OrderToPurchase');
        if (cacheQuote != undefined) {
            cacheQuote.destroy()
        }
        var cache = $cacheFactory('OrderToPurchase');
        cache.put('data', data);
        $state.go('purchase_order.bulk', {
            order_id: data.order_id
        })*/
        openModal("create_po", {}, "lg");
    }
    $scope.greenpost = function() {
        if ($scope.order.post_active) {
            $scope.showTxt.sendPost = !$scope.showTxt.sendPost
        } else {
            var data = {};
            data.order_id = $scope.order_id;
            openModal("post_register", data, "md")
        }
    }
    $scope.sendPost = function() {
        var data = {};
        data.do = 'order-order-order-postIt';
        data.id = $scope.order_id;
        data.related = 'view';
        if ($scope.order.stationary_id) {
            data.stationary_id = $scope.order.stationary_id
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.showTxt.sendPost = !1;
            if (r.success) {
                $scope.renderPage(r)
            }
        })
    }
    $scope.resendGreenPost = function() {
        var data = {};
        data.do = 'order-order-order-postIt';
        data.id = $scope.order_id;
        data.related = 'view';
        data.post_library = angular.copy($scope.order.post_library);
        openModal("resend_post", data, "md")
    }
    $scope.setDownType = function(type) {
        $scope.showTxt.down_notice = !1;
        var total_order = $scope.order.totalul.total_order_no_vat;
        if (type == '1') {
            $scope.order.downpayment_percent = helper.displayNr(parseFloat(helper.return_value($scope.order.downpayment_fixed), 10) * 100 / total_order)
        } else {
            $scope.order.downpayment_fixed = helper.displayNr(parseFloat(helper.return_value($scope.order.downpayment_percent), 10) / 100 * total_order)
        }
        $scope.order.downpayment_type = type;
        if (parseFloat(helper.return_value($scope.order.downpayment_fixed), 10) > total_order) {
            $scope.showTxt.down_notice = !0
        }
    }
    $scope.createDownPayment = function() {
        var obj = {};
        obj.downpayment = $scope.order.downpayment;
        obj.downpayment_type = $scope.order.downpayment_type;
        obj.downpayment_fixed = $scope.order.downpayment_fixed;
        obj.downpayment_percent = $scope.order.downpayment_percent;
        obj.service_type = $scope.order.service_type;
        obj.order_id = $scope.order_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        obj.do = 'order-order-order-downpaymentInvoice';
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.order = r.data
            }
        })
    }
    $scope.setSegment = function() {
        var obj = {
            'do': 'order--order-updateSegment',
            'order_id': $scope.order.order_id,
            'segment_id': $scope.order.segment_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.showTxt.segment = !1;
                $scope.order.segment = r.data.segment
            }
        })
    }
    $scope.setSource = function() {
        var obj = {
            'do': 'order--order-updateSource',
            'order_id': $scope.order.order_id,
            'source_id': $scope.order.source_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.showTxt.source = !1;
                $scope.order.source = r.data.source
            }
        })
    }
    $scope.setType = function() {
        var obj = {
            'do': 'order--order-updateType',
            'order_id': $scope.order.order_id,
            'type_id': $scope.order.type_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.showTxt.type = !1;
                $scope.order.xtype = r.data.xtype
            }
        })
    }

    $scope.setVisibility= function(item, i) {
        

        var data = {
            do: 'order-order-order-set_article_visibility',
            article_id: item.article_id,
            order_id:  $scope.order.order_id,
            order_articles_id: item.order_articles_id,
            visible: !item.visible,
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
               $scope.order.lines[i].visible = r.data.lines[i].visible;
            }
        })
    }

    $scope.showDownpayment=function(){
        openModal('downpayment_modal',$scope.order,'md');
    }

    $scope.statusOrder=function(status){
        var tmp_obj={'order_id':$scope.order.order_id};
        tmp_obj.do=status=='1' ? 'order-order-order-archive_order': 'order-order-order-activate_order'; 
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post','index.php',tmp_obj,function(r){
            if(r.success){
                $scope.renderPage(r);
                helper.refreshLogging();
            }else{
                $scope.showAlerts(r);
            }
        });
    }

    $scope.deleteOrder=function(){
        var tmp_obj={'order_id':$scope.order.order_id};
        tmp_obj.do='order-orders-order-delete_order'; 
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post','index.php',tmp_obj,function(r){
            if(r.success){
                helper.setItem('order-orders',{});
                $state.go("orders");
            }else{
                $scope.showAlerts(r);
            }
        });
    }

    $scope.sendEmail = function() {
        $scope.showDrop();
        var params={
            'do': 'order-email_preview',
            order_id: $scope.order_id,
            message: $scope.email.e_message,
            use_html: $scope.order.use_html,
            send_email:true
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post','index.php',params,function(r){
            var viewEmail=angular.copy($scope.email);
            viewEmail.e_message=r.data.e_message;
            var obj={"app":"order",
                "recipients":$scope.auto.options.recipients,
                "use_html":$scope.order.use_html,
                "is_file":$scope.order.is_file,
                "dropbox":$scope.order.dropbox,
                "email":viewEmail
            };
            openModal("send_email", angular.copy(obj), "lg");
        })      
    }

    $scope.setDeliveryDate = function() {
        if(!$scope.order.del_date){
            return false;
        }
        var obj = {
            'do': 'order--order-updateDeliveryDate',
            'order_id': $scope.order.order_id,
            'del_date': Math.floor($scope.order.del_date.getTime() / 1000)
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.showTxt.edit_delivery_date = !1;
                $scope.order.factur_del_date = r.data.factur_del_date
            }
        })
    }

   


    $scope.setOurReference = function() {
        var obj = {
            'do': 'order--order-updateOurReference',
            'order_id': $scope.order.order_id,
            'our_ref': $scope.order.our_ref
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.showTxt.edit_our_ref = !1;
            }
        })
    }

    $scope.setYourReference = function() {
        var obj = {
            'do': 'order--order-updateYourReference',
            'order_id': $scope.order.order_id,
            'your_ref': $scope.order.your_ref
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.showTxt.edit_your_ref = !1;
            }
        })
    }
    $scope.showYourReference = function() {
        $scope.showTxt.edit_your_ref=true;
        $timeout(function(){
            angular.element('#inp_yr').focus();
        });
    }
    $scope.showOurReference = function() {
        $scope.showTxt.edit_our_ref=true;
        $timeout(function(){
            angular.element('#inp_or').focus();
        });
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'post_register':
                var iTemplate = 'PostRegister';
                var ctas = 'post';
                break;
            case 'resend_post':
                var iTemplate = 'ResendPostGreen';
                var ctas = 'post';
                break;
            case 'uploadDoc':
                var iTemplate = 'AmazonUpload';
                break;
            case 'downpayment_modal':
                var iTemplate = 'OrderDownpayment';
                var ctas = 'vm';
                break;
            case 'send_email':
                var iTemplate = 'SendEmail';
                var ctas = 'vm';
                break;
            case 'create_po':
                var iTemplate = 'CreatePoBulk';
                var ctas = 'vm';
                break;
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'post_register':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.item.do = 'order-order-order-postRegister';
                params.callback = function(data) {
                    if (data && data.data) {
                        $scope.renderPage(data);
                        $scope.greenpost()
                    }
                }
                break;
            case 'resend_post':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.request_type = 'post';
                params.params = {
                    'do': 'order--order-postOrderData',
                    'order_id': $scope.order_id,
                    'related': 'status',
                    'old_obj': item
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.renderPage(d)
                    }
                }
                break;
            case 'uploadDoc':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        $scope.showDrop();
                    }
                }
                break;
            case 'downpayment_modal':
                params.template = 'otemplate/' + iTemplate + 'Modal';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(data) {
                    if (data && data.data) {
                        $scope.renderPage(data);
                    }
                }
                break;
            case 'send_email':
                params.template ='miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        $scope.sendEmails(d);
                    }
                }
                break;
            case 'create_po':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.params={'do':'po_order-add_bulk','order_id':$scope.order_id};
                params.callback = function(d) {
                    if (d) {
                        $state.go('purchase_orders');
                        /*helper.doRequest('get', 'index.php', {'do': 'order-order', 'order_id': $scope.order_id}).then(function(data) {
                            $scope.renderPage(data);
                        });*/
                    }
                }
                break;
        }
        modalFc.open(params)
    }

    function calcTotal(type, $index) {
        editItem.calcTotal(edit.item, type, $index)
    }

    function updateVat($index) {
        edit.item.tr_id[$index].percent = helper.return_value(edit.item.tr_id[$index].percent_x);
        editItem.calcTotal(edit.item, 'price', $index)
    }

    $scope.showAccountNotes=function(){
        $scope.showCustomerNotes($scope,$scope.order.buyer_id,$scope.order.customer_notes);
    }

    $scope.showPurchasePrice=function(item,index){
        item.save_data=true;
        item.c_index=index;
        $scope.edit_purchase_price=true;
        $scope.showPurchasePriceModal($scope,item);
    }

    $scope.linevatcfg = {
        valueField: 'vat',
        labelField: 'name',
        searchField: ['vat'],
        delimiter: '|',
        placeholder: helper.getLanguage('VAT'),
        create: !1,
        maxOptions: 100,
        onDelete(values) {
            return !1
        },
        closeAfterSelect: !0,
        maxItems: 1
    };

    $scope.showEditLine=function(index){
        $scope.order.lines[index].show_edit=true;
        $scope.showTxt.show_edit_index=index;
        $scope.tmp_edit=angular.copy($scope.order.lines[index]);
    }

    $scope.saveLineUpdates=function(item){
        $scope.showTxt.show_edit_index=null; 
        $scope.tmp_edit=undefined;
        var obj=angular.copy(item);
        obj.do='order-order-order-update_order_line';
        obj.order_id=$scope.order_id;
        obj.discount=$scope.order.discount;
        obj.apply_discount=$scope.order.apply_discount;
        helper.doRequest('post', 'index.php', obj,$scope.renderPage);
    }

    $scope.recalculateTotal=function(type, $index) {
        var vat_total = 0,
            total = 0,
            vats = [],
            discs = [],
            lines = [],
            total_vat = 0,
            discount_total = 0,
            total_currency = 0,
            discount_global = $scope.order.discount,
            apply_discount = $scope.order.apply_discount,
            shipping_price = $scope.order.shipping_price_number;
        for (x in $scope.order.lines) {
            if ($scope.order.lines[x].content) {
                var q = parseFloat(helper.return_value($scope.order.lines[x].quantity), 10),
                    sale_unit = parseFloat($scope.order.lines[x].sale_unit, 10),
                    packing = parseFloat($scope.order.lines[x].packing, 10),
                    price = parseFloat(helper.return_value($scope.order.lines[x].price), 10),
                    price_vat = parseFloat(helper.return_value($scope.order.lines[x].price_vat), 10),
                    vat = $scope.order.remove_vat == 1 ? 0 : ($scope.order.lines[x].percent ? parseFloat(helper.return_value($scope.order.lines[x].percent), 10) : 0),
                    disc_line = parseFloat(helper.return_value($scope.order.lines[x].disc), 10),
                    price1 = 0;
                if (isNaN(sale_unit)) {
                    sale_unit = 1
                }
                if (isNaN(packing)) {
                    packing = 1
                }
                if (apply_discount == 0 || apply_discount == 2) {
                    disc_line = 0
                }
                if (apply_discount < 2) {
                    discount_global = 0
                }
                var precision = getPrecision(q);
                if (precision > 4) {
                    q = helper.round_n(q, 4);
                    $scope.order.lines[x].quantity = helper.displayNr(helper.round_n(q, 4), 4)
                }
                if ($index && $index == x) {
                    switch (type) {
                        case 'price':
                            $scope.order.lines[x].price_vat = helper.displayNr(parseFloat(price + price * vat / 100, 10), helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                            break;
                        case 'price_vat':
                            price = parseFloat(price_vat / (1 + vat / 100), 10)
                            $scope.order.lines[x].price = helper.displayNr(price, helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                            break;                 
                    }
                }
                price1 = price - price * disc_line / 100;
                $scope.order.lines[x].vat_value = price1 * vat / 100;
                vat_total += (price1 - price1 * discount_global / 100) * vat / 100 * q * (packing / sale_unit);
                total += price1 * q * (packing / sale_unit);
                $scope.order.lines[x].line_total = helper.displayNr(parseFloat(price1 * q * (packing / sale_unit), 10));
                if (lines[vat]) {
                    lines[vat] += q * price1
                } else {
                    lines[vat] = q * price1
                }
                if (discs[vat]) {
                    discs[vat] += q * price1 * discount_global / 100
                } else {
                    discs[vat] = q * price1 * discount_global / 100
                }
                if (vats[vat]) {
                    var nr_simple = q * (price1 - price1 * discount_global / 100) * vat / 100;
                    vats[vat] += nr_simple
                } else {
                    var nr_simple = q * (price1 - price1 * discount_global / 100) * vat / 100;
                    vats[vat] = nr_simple
                }
            }
        }
        var is_ord = $scope.order.vat_line && $scope.order.vat_line.length ? $scope.order.vat_line[0].is_ord : !1;
        var view_disc = $scope.order.vat_line && $scope.order.vat_line.length ? $scope.order.vat_line[0].view_discount : (apply_discount > 1 ? !0 : !1);
        $scope.order.vat_line = [];
        var count_vats = 0;
        for (y in vats) {
            var tempObj = {};
            tempObj.discount_value = helper.displayNr(discs[y]);
            tempObj.net_amount = helper.displayNr(lines[y] - discs[y]);
            tempObj.total_novat = helper.displayNr(helper.round(lines[y], 2));
            tempObj.vat_percent = helper.displayNr(y);
            tempObj.subtotal = helper.displayNr(lines[y]);
            tempObj.vat_value = helper.displayNr(helper.round(vats[y], 2));
            tempObj.is_ord = is_ord;
            tempObj.view_discount = view_disc;
            $scope.order.vat_line.push(tempObj);
            count_vats++
        }
        discount_total = total * discount_global / 100;
        if (apply_discount < 2) {
            discount_total = 0
        }
        $scope.order.totalul.discount_total = helper.displayNr(discount_total);
        $scope.order.totalul.vat_total = helper.displayNr(vat_total);
        $scope.order.totalul.total = helper.displayNr(total);
        if (isNaN(vat_total)) {
            vat_total = 0
        }
        $scope.order.totalul.total_vat = helper.displayNr(parseFloat((helper.round(vat_total, 2) + helper.round(total, 2) - helper.round(discount_total, 2) + helper.round(shipping_price, 2)), 10));
    }

    function getPrecision(number) {
        var n = number.toString().split(".");
        return n.length > 1 ? n[1].length : 0
    }

    $scope.confirmDelivery=function(){
        //check how many open deliveries:if more then one show modal else press confirm button
        let tmp_open_deliveries=[];
        for(let x in $scope.order.deliveries){
            if($scope.order.deliveries[x].not_approved){
                tmp_open_deliveries.push($scope.order.deliveries[x]);
            }
        }
        if(tmp_open_deliveries.length && tmp_open_deliveries.length == 1){
            $scope.openDeliveyModal(tmp_open_deliveries[0].delivery_id,true);
        }else if(tmp_open_deliveries.length && tmp_open_deliveries.length > 1){
            tmp_open_deliveries.sort((a, b) => (a.delivery_id > b.delivery_id) ? 1 : -1);
            let multiple_deliveries=tmp_open_deliveries.map(function(obj) {return {'id':obj.delivery_id,'name':obj.delivey}});
            $scope.openDeliveyModal(multiple_deliveries[0].id,true,{'multiple_deliveries':multiple_deliveries});
        }
    }

    angular.element(document).mousedown(function(e) {
        if(!$scope.edit_purchase_price && $scope.showTxt.show_edit_index !== null){
            var closestTableLine=angular.element(e.target).closest('tr.table_body_line');
            if(closestTableLine.length>0){
                if((closestTableLine[0].rowIndex-1) !== $scope.showTxt.show_edit_index){
                    $scope.order.lines[$scope.showTxt.show_edit_index]=$scope.tmp_edit; 
                    $scope.order.lines[$scope.showTxt.show_edit_index].show_edit=false;          
                    $scope.showTxt.show_edit_index=null; 
                    $scope.tmp_edit=undefined;
                    $scope.recalculateTotal();
                }   
            }else{           
                $scope.order.lines[$scope.showTxt.show_edit_index]=$scope.tmp_edit; 
                $scope.order.lines[$scope.showTxt.show_edit_index].show_edit=false;
                $scope.showTxt.show_edit_index=null;
                $scope.tmp_edit=undefined;
                $scope.recalculateTotal();
            }
        }
    });

    $scope.renderPage(data)
}]).controller('deliveryModalCtrl', ['$scope', 'helper', '$uibModalInstance', '$timeout','data', '$compile', '$uibModal', 'modalFc', '$confirm',function($scope, helper, $uibModalInstance, $timeout, data, $compile, $uibModal, modalFc, $confirm) {
    var typePromise;
    $scope.delivery = data;
    $scope.delivery.do = $scope.delivery.delivery_id ? 'order-order-order-edit_delivery' : 'order-order-order-make_delivery';
    $scope.delivery.hour = $scope.delivery.delivery_id ? data.hour : "12";
    $scope.delivery.minute = $scope.delivery.delivery_id ? data.minute : "00";
    $scope.delivery.pm = $scope.delivery.delivery_id ? data.pm : 'pm';
    $scope.delivery.date_del_line = $scope.delivery.delivery_id ? new Date(data.delivery_date_js) : new Date();
    $scope.delivery.contact_id = $scope.delivery.delivery_id ? data.contact_id : (data.contact_id ? data.contact_id : undefined);
    $scope.delivery.contact_name = $scope.delivery.delivery_id ? data.contact_name : '';
    $scope.disableAddress = !0;
    $scope.listStats = [];
    $scope.listStats2 = [];
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        date_del_line: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.hours = [12, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11];
    $scope.minutes = ["00", 15, 30, 45];
    $scope.isDisabled = false;
    
    $scope.buyerAutoCfg={
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select customer'),
        maxItems: 1,
        onItemRemove(value) {
            $scope.delivery.buyer_name = '';
            $scope.delivery.recipients=[];
            $scope.delivery.delivery_addresses=[];
            $scope.delivery.contact_id=undefined;
            $scope.delivery.contact_name = '';
            $scope.delivery.delivery_address_id=undefined;
            $scope.delivery.delivery_address = '';
        },
        onType(str) {
            var selectize = angular.element('#buyer_id')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'order-order_line',
                    'xget': 'cc',
                    term: str
                };
                helper.doRequest('get', 'index.php', data, function(o) {
                    if(o.data){
                        selectize.open();
                        $scope.delivery.cc = o.data;                   
                        $scope.delivery.recipients=[];
                        $scope.delivery.delivery_addresses=[];
                        $scope.delivery.contact_id=undefined;
                        $scope.delivery.contact_name = '';
                        $scope.delivery.delivery_address_id=undefined;
                        $scope.delivery.delivery_address = '';
                    }
                })
            }, 300)
        },
        onChange(value){
            if (value != undefined) {
                $scope.delivery.buyer_name=this.options[value].name;
                var selectize = angular.element('#buyer_id')[0].selectize;
                selectize.close();
                if (typePromise) {
                    $timeout.cancel(typePromise)
                }
                typePromise = $timeout(function() {
                    helper.doRequest('get', 'index.php', {
                        'do': 'order-order_line',
                        xget: 'addresses',
                        'buyer_id': $scope.delivery.buyer_id
                    }, function(o) {
                        $scope.delivery.delivery_addresses = o.data;
                        if($scope.delivery.delivery_addresses.length){
                            $scope.delivery.delivery_address_id=$scope.delivery.delivery_addresses[0].delivery_address_id;
                        }
                    });
                    helper.doRequest('get', 'index.php', {
                        'do': 'order-order_line',
                        xget: 'contacts',
                        'buyer_id': $scope.delivery.buyer_id
                    }, function(o) {
                        $scope.delivery.recipients = o.data;
                        if($scope.delivery.recipients.length){
                            $scope.delivery.contact_id=$scope.delivery.recipients[0].recipient_id;
                        }
                    })
                }, 300)
            }
        }
    };

    $scope.contactAutoCfg = {
        valueField: 'recipient_id',
        labelField: 'recipient_name',
        searchField: ['recipient_name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select contact'),
        maxItems: 1,
        onItemAdd(value, $item) {
            $scope.delivery.contact_name = $item.html()
        },
        onItemRemove(value) {
            $scope.delivery.contact_name = ''
        }
    };

    $scope.addressAutoCfg = {
        valueField: 'delivery_address_id',
        labelField: 'address_more',
        searchField: ['address_more'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Delivery Address'),
        maxItems: 1,
        onItemAdd(value, $item) {
            $scope.delivery.delivery_address = $item.html()
        },
        onItemRemove(value) {
            $scope.delivery.delivery_address = ''
        }
    };

    $scope.stockLocationCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        onChange(value){
            if(value){
                $scope.changedAddress();
            }        
        },
        maxItems: 1
    };

    $scope.multiDeliveryCfg={
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        maxItems: 1,
        onDelete(value){
            return false;
        },
        onChange(value){
            $uibModalInstance.dismiss({'delivery_id':value})
        }
    };

    $scope.hideDetails = !0;
    $scope.disabledt = function() {
        $scope.disableAddress = !$scope.disableAddress;
        return !1
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
    $scope.showStats = function(item, $index) {
        var add = !1;
        if (!$scope.listStats[$index]) {
            $scope.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (add == !0) {
            $scope.listStats[$index] = item.disp_addres_info;
            angular.element('.deliveryTable tbody tr.collapseTr').eq($index).after('<tr class="versionInfo"><td colspan="7" class="no_border">' + '<table class="table_minimal">' + '<tbody>' + '<tr ng-repeat="item2 in listStats[' + $index + ']">' + '<td>' + '<div class="row" >' + '<div class="col-xs-3"><b>{{::item2.customer_name}}</b></div>' + '<div class="col-xs-3">' + '<div>{{::item2.order_buyer_address}}</div>' + '<div>{{::item2.order_buyer_zip}} {{::item2.order_buyer_city}}</div>' + '<div>{{::item2.order_buyer_country}}</div></div>' + '<div class="col-xs-3" ng-class="{ \'is-error\':item2.is_error }"><input type="text" class="form-control" display-nr="" ng-disabled="' + item.use_batch_no_art + '" ng-model="item2.quantity_delivered" ng-change="changeQuantity2(item2, ' + $index + ')"><i class="fa fa-exclamation-circle" ng-if="item2.is_error"></i></div>' + '<div class="col-xs-3" ng-if="item2.is_article && item2.is_not_combined">In Stock: <b>{{::item2.quantity}}</b></div>' + '</div>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>');
            $compile(angular.element('.deliveryTable tbody tr.collapseTr').eq($index).next())($scope);
            modalFc.setHeight(angular.element('.modal.in'))
        } else {
            angular.element('.deliveryTable tbody tr.collapseTr').eq($index).next().toggleClass('hidden');
            modalFc.setHeight(angular.element('.modal.in'))
        }
        if (item.isopen == !0) {
            var idx = 0;
            for (x in $scope.delivery.location_dd) {
                if ($scope.delivery.location_dd[x].id == $scope.delivery.select_main_location) {
                    idx = x
                }
            }
            var q = parseFloat(item.quantity_value) > parseFloat($scope.listStats[$index][idx].order_qty) ? $scope.listStats[$index][idx].quantity : item.quantity;
            $scope.listStats[$index][idx].quantity_delivered = q
        }
    }
    $scope.changedAddress = function() {
        for (x in $scope.delivery.location_dd) {
            if ($scope.delivery.location_dd[x].id == $scope.delivery.select_main_location) {
                idx = x
            }
        }
        for (y in $scope.delivery.lines) {
            var i = $scope.delivery.lines[y];
            i.is_error = !1;
            for (x in i.disp_addres_info) {
                i.disp_addres_info[x].quantity_delivered = helper.displayNr(0);
                i.disp_addres_info[x].is_error = !1;
                if (x == idx) {
                    i.disp_addres_info[x].quantity_delivered = helper.return_value(i.quantity) * 1;
                    if (helper.return_value(i.quantity) * 1 > i.disp_addres_info[x].qty * 1 && i.show_stock && i.is_article) {
                        i.is_error = !0;
                        i.disp_addres_info[x].is_error = !0;
                        i.disp_addres_info[x].quantity_delivered = i.quantity
                    } else {
                        i.disp_addres_info[x].quantity_delivered = i.quantity
                    }
                }
            }
        }
    }
    $scope.changeQuantity = function(i) {
        var idx = 0;
        i.is_error = !1;
        for (x in $scope.delivery.location_dd) {
            if ($scope.delivery.location_dd[x].id == $scope.delivery.select_main_location) {
                idx = x
            }
        }
        for (x in i.disp_addres_info) {
            i.disp_addres_info[x].quantity_delivered = helper.displayNr(0);
            i.disp_addres_info[x].is_error = !1;
            if (x == idx) {
                i.disp_addres_info[x].quantity_delivered = helper.displayNr(helper.return_value(i.quantity));   
                //console.log(i.is_article);          
                if (helper.return_value(i.quantity)*1*helper.return_value(i.packing)/helper.return_value(i.sale_unit) > i.disp_addres_info[x].qty * 1  && i.is_article && i.is_not_combined) {

                    i.is_error = !0;
                    i.disp_addres_info[x].is_error = !0
                }
            }
        }
    }
    $scope.changeQuantity2 = function(i, index_line) {
        var sum = 0;
        var packing= $scope.delivery.lines[index_line].packing;
        var sale_unit = helper.return_value($scope.delivery.lines[index_line].sale_unit);
        for (x in $scope.delivery.lines[index_line].disp_addres_info) {
            sum = sum * 1 + helper.return_value($scope.delivery.lines[index_line].disp_addres_info[x].quantity_delivered) * 1*packing/sale_unit
        }

        if(!i.is_article || !i.is_not_combined){
            $scope.delivery.lines[index_line].is_error = 0;
        }else{

            $scope.delivery.lines[index_line].is_error = !0;
        }

        if (sum == helper.return_value($scope.delivery.lines[index_line].quantity) * 1 *packing/sale_unit) {
            $scope.delivery.lines[index_line].is_error = !1;
        }else{
           if(sum < helper.return_value($scope.delivery.lines[index_line].quantity) * 1*packing/sale_unit ){
                $scope.delivery.lines[index_line].is_error = !1;
             }
            $scope.delivery.lines[index_line].quantity = helper.displayNr(sum * sale_unit/packing);
        }

        i.is_error = !1;
        if (helper.return_value(i.quantity_delivered) * 1 *packing/sale_unit > i.qty * 1 && i.is_article && i.is_not_combined) {
            i.is_error = !0
        }
    }
    $scope.ok = function() {
        $scope.isDisabled = true;
        //console.log($scope.delivery);return false;
        helper.doRequest('post', 'index.php', $scope.delivery, $scope.closeModal)
    }
    $scope.closeModal = function(d) {
        if (d.error) {
            $scope.alerts = [{
                type: 'danger',
                msg: d.error.error
            }];
            return !1
        }else if(d.notice){
            $confirm({
                ok: helper.getLanguage('Yes'),
                cancel: helper.getLanguage('No'),
                text: helper.getLanguage('Are you sure you want to register this reception? If you do, the total delivered amount will exceed the total ordered amount.')
            }).then(function() {
                var tmp_obj=angular.copy($scope.delivery);
                tmp_obj.confirm_over_total_delivery='1';
                helper.doRequest('post', 'index.php', tmp_obj, $scope.closeModal)
            }, function() {
                return !1
            });
            return false;
        }
        if (d) {
            if(d.data && $scope.delivery.multiple_deliveries){
                let data_copy=angular.copy(d);
                data_copy.data.reopen_delivery=true;
                $uibModalInstance.close(data_copy)
            }else{
                $uibModalInstance.close(d)
            }           
        } else {
            $uibModalInstance.close()
        }
    }
    $scope.showStats2 = function(item, $index) {
        if (item.locations.length > 0) {
            $scope.listStats2[$index] = item;
            angular.element('.deliveryTable tbody tr.collapseTr').eq($index).after('<tr class="versionInfo" ng-if="listStats2[' + $index + '].view_location"><td colspan="5" class="no_border">' + '<table class="table_minimal">' + '<tbody>' + '<tr ng-repeat="item2 in listStats2[' + $index + '].locations">' + '<td>' + '<div class="row" >' + '<div class="col-xs-9"><div><b>{{::item2.depo}}</b> </div>' + '<div>{{::item2.address}}</div>' + '<div>{{::item2.zip}} {{::item2.city}}</div>' + '<div>{{::item2.country}}</div>' + '</div>' + '<div class="col-xs-3">' + '<div>{{::item2.quantity_loc}}</div>' + '</div>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>');
            $compile(angular.element('.deliveryTable tbody tr.collapseTr').eq($index).next())($scope);
            modalFc.setHeight(angular.element('.modal.in'))
        }
    }
    $scope.openModal = function(type, item, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            backdrop: 'static',
            templateUrl: 'otemplate/' + type + 'Modal',
            controller: type + 'ModalCtrl',
            size: size ? size : 'lg',
            resolve: {
                data: function() {
                    switch (type) {
                        case 'serialNr':
                            var d = {
                                'do': 'order-' + type,
                                order_id: $scope.delivery.order_id,
                                order_articles_id: item.order_articles_id,
                                count_serial_nr: parseInt(helper.return_value(item.quantity)),
                                delivery_id: $scope.delivery.delivery_id,
                                selected_s_n: item.selected_s_n
                            };
                            break;
                        case 'batchNr':
                            //var q = parseInt(helper.return_value(item.quantity));
                            var q = helper.return_value(item.quantity);
                            if ($scope.delivery.delivery_id > 0 && $scope.delivery.delivery_approved == undefined) {
                                q = undefined
                            }
                            var d = {
                                'do': 'order-' + type,
                                order_id: $scope.delivery.order_id,
                                order_articles_id: item.order_articles_id,
                                count_quantity_nr: q,
                                delivery_id: $scope.delivery.delivery_id,
                                delivery_approved: $scope.delivery.delivery_approved,
                                selected_items: item.edit_batches
                            };
                            break;
                        case 'customAddress':
                            var d = {
                                'do': 'order-norder',
                                'xget': 'countries'
                            };
                            break;
                    }
                    return helper.doRequest('post', 'index.php', d).then(function(d) {
                        return d.data
                    })            
                }
            }
        });
        modalInstance.result.then(function(data) {
            switch (type) {
                case 'serialNr':
                    if (data) {
                        item.count_selected_s_n = data.length;
                        item.selected_s_n = data;
                        item.serialOk = !0
                    }
                    break;
                case 'batchNr':
                    if (data) {
                        item.count_selected_b_n = 0;
                        for (x in data) {
                            if (data[x].batch_no_checked) {
                                item.count_selected_b_n += helper.return_value(data[x].b_n_q_selected) * 1
                            }
                        }

                        item.selected_b_n = data;
                        
                        item.batchOk = !0;
                        item.show_stock =0;
                        item.is_error =0;
                        var selected_addresses = [];  
                        //console.log(item.selected_b_n);                  
                        for (y in item.selected_b_n) {
                                if(item.selected_b_n[y].batch_no_checked==true){
                                    var address_id = item.selected_b_n[y].address_id;
                                        qty = helper.return_value(item.selected_b_n[y].b_n_q_selected) * 1 ;
                                        
                                            //item.disp_addres_info[x].quantity_delivered =  helper.return_value(item.disp_addres_info[x].quantity_delivered) * 1 +qty;
                                            var temp=[];
                                            temp['address_id'] =address_id;
                                            temp['qty'] =qty;
                                           // console.log(address_id, qty );

                                            var found = false;
                                            for(var i = 0; i < selected_addresses.length; i++) {
                                                if (selected_addresses[i].address_id == address_id) {
                                                    selected_addresses[i].qty +=qty;
                                                    found = true;
                                                    break;
                                                }
                                            }

                                           if(!found){
                                                selected_addresses.push(temp);
                                                
                                            }                                       
                                        

                                }


                        }
                             //console.log(selected_addresses);

                        for (x in item.disp_addres_info) {

                             var found2 = false;
                                for(var i = 0; i < selected_addresses.length; i++) {
                                    if (selected_addresses[i].address_id == item.disp_addres_info[x].address_id) {                                    
                                        item.disp_addres_info[x].quantity_delivered = selected_addresses[i].qty;
                                        found2 = true;
                                        break;
                                    }
                                }

                               if(!found2){
                                    item.disp_addres_info[x].quantity_delivered = 0;                                  
                                }     

                                 item.disp_addres_info[x].quantity_delivered = helper.displayNr(item.disp_addres_info[x].quantity_delivered);    

                        }
  

                    }
                    break;
                case 'customAddress':
                    if(data){
                        $scope.delivery.is_custom_address=true;
                        $scope.delivery.delivery_address=data.delivery_address;
                        $scope.delivery.delivery_address_id=0;
                    }
                    break;
            }
        }, function() {
            console.log('modal closed')
        })
    }
    
}]).controller('sendModalOrdersCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'modalFc', 'selectizeFc', function($scope, helper, $uibModalInstance, data, modalFc, selectizeFc) {
    $scope.email = data;

    $scope.contactForEmailAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select recipient'),
        create: !0,
        createOnBlur:true,
        maxItems: 100
    });
    /*$scope.auto = {
        options: {
            recipients: data.recipients
        }
    };*/
    $scope.tinymceOptions = {
        selector: ".mail_send",
        resize: "height",
        height: 284,
        menubar: !1,
        relative_urls: 1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["fontsizeselect | bold italic strikethrough underline | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
    };
    $scope.showPreview = function(exp) {
        var params = {
            template: 'miscTemplate/modal',
            ctrl: 'viewRecepitCtrl',
            size: 'md',
            ctrlAs: 'vmMod',
            params: {
                'do': 'order-email_preview',
                order_id: data.order_id,
                message: exp.e_message,
                use_html: exp.use_html
            }
        }
        modalFc.open(params)
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
    $scope.ok = function() {
        $scope.email.do = $scope.email.do_next;
        $scope.closeModal($scope.email);
        //helper.doRequest('post', 'index.php', $scope.email, $scope.closeModal)
    }
    $scope.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
}]).controller('returnModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', '$uibModal', function($scope, helper, $uibModalInstance, data, $uibModal) {
    $scope.returns = data;
    $scope.returns.date = new Date(data.date);
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        date: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
    $scope.ok = function() {
        $scope.returns.do = $scope.returns.do_next;
        helper.doRequest('post', 'index.php', $scope.returns, $scope.closeModal)
    }
    $scope.openModal = function(type, item, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            backdrop: 'static',
            templateUrl: 'otemplate/' + type + 'Modal',
            controller: type + 'ModalCtrl',
            size: size ? size : 'lg',
            resolve: {
                data: function() {
                    if (type == 'batchNr') {
                       /* var q = parseInt(helper.return_value(item.quantity));*/
                        var q = helper.return_value(item.quantity);
                        var d = {
                            'do': ($scope.returns.p_order_id ? 'po_order-batchNrReturn' : 'order-' + type),
                            order_articles_id: item.order_articles_id,
                            count_quantity_nr: q,
                            selected_items: item.edit_batches,
                            ret_quantity: item.ret_quantity,
                            return: 1,
                            return_id: $scope.returns.return_id,
                            has_changed: item.has_changed
                        }
                        if($scope.returns.p_order_id){
                            d.p_order_id=$scope.returns.p_order_id;
                        }else{
                            d.order_id=$scope.returns.order_id;
                        }
                    } else if (type == 'serialNr') {
                        var d = {
                            'do': 'order-' + type,
                            order_id: $scope.returns.order_id,
                            order_articles_id: item.order_articles_id,
                            count_serial_nr: parseInt(helper.return_value(item.ret_quantity)),
                            return: 1,
                            return_id: $scope.returns.return_id,
                            selected_s_n: item.selected_s_n
                        }
                    }
                    return helper.doRequest('post', 'index.php', d).then(function(d) {
                        return d.data
                    })
                }
            }
        });
        modalInstance.result.then(function(data) {
            if (type == 'batchNr') {
                if (data) {
                    item.count_selected_b_n = 0;
                    for (x in data) {
                        if (data[x].batch_no_checked) {
                            item.count_selected_b_n += helper.return_value(data[x].b_n_q_selected) * 1
                        }
                    }
                    item.selected_b_n = data;
                    item.batchOk = !0
                }
            } else if (type == 'serialNr') {
                if (data) {
                    item.count_selected_s_n = data.length;
                    item.selected_s_n = data;
                    item.serialOk = !0
                }
            }
        }, function() {
            console.log('Modal closed')
        })
    }
    $scope.checkQuantity = function(item) {
        item.has_changed = !0;
        if (helper.return_value(item.ret_quantity) * 1 > helper.return_value(item.quantity_rem) * 1) {
            item.is_error = !0
        } else {
            item.is_error = !1
        }
        if (item.ret_quantity == '0') {
            item.no_quantity = !0
        } else {
            item.no_quantity = !1
        }
    }
    $scope.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
}]).controller('pdfSettingsOrderCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.obj = data;
    $scope.ok = function() {
        var data = {
            'do': $scope.obj.do_next,
            logo: $scope.obj.default_logo,
            email_language: $scope.obj.language_id,
            pdf_layout: $scope.obj.pdf_type_id,
            order_id: $scope.obj.order_id,
            identity_id: $scope.obj.multiple_identity_id,
        };
        if ($scope.obj.xls) {
            var extra = '&lid=' + $scope.obj.language_id,
                h = $('#gen_XLS').attr('href') + extra;
            window.open(h, '_blank');
            $scope.closeModal()
        } else {
            helper.doRequest('post', 'index.php', data, $scope.closeModal)
        }
    };
    $scope.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('OrderEditCtrl', ['$scope', 'helper', '$stateParams', '$state', '$uibModal', '$timeout', 'editItem', '$confirm', 'selectizeFc', 'modalFc', 'data', function($scope, helper, $stateParams, $state, $uibModal, $timeout, editItem, $confirm, selectizeFc, modalFc, data) {
    var edit = this,
        typePromise;
    edit.app = 'order';
    edit.ctrlpag = 'norder';
    edit.tmpl = 'otemplate';
    $scope.$parent.isAddPage = !1;
    /*$scope.$parent.type_title = "";
    $scope.$parent.nr_status = "";*/
    edit.disabled = {
        address: !0,
        daddress: !0
    };
    edit.item = {};
    edit.item.date_h = new Date();
    edit.item.del_date = new Date();
    edit.showWarning = !0;
    edit.removedCustomer = !1;
    edit.oldObj = {};
    edit.contactAddObj = {};
    edit.showEditFnc = showEditFnc;
    edit.showCCSelectize = showCCSelectize;
    edit.item_id = $stateParams.order_id;
    edit.order_id = edit.item_id;
    edit.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            edit.changeDiscount()
        }
    });
    edit.currencyCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            edit.changeCurrency()
        }
    });
    edit.vatregcfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onDropdownOpen($dropdown) {
            var _this = this;
            edit.item.old_vat_regime_id=_this.$input[0].attributes.value.value;
        },
        onChange(value) {
            if (value) {
                var selectize = angular.element('#vat_regime_select')[0].selectize;
                selectize.blur();
                var _this = this;
                $confirm({
                        ok: helper.getLanguage('Yes'),
                        cancel: helper.getLanguage('No'),
                        text: helper.getLanguage('Apply VAT Regime on all lines')
                    }).then(function() {
                       if (_this.options[value].no_vat) {
                            edit.item.is_vat = !1;
                            edit.item.block_vat = !1;
                            edit.item.vat_regular = !1;                  
                        } else {
                            if (_this.options[value].regime_type == '2') {
                                edit.item.block_vat = !0;
                            } else if (_this.options[value].regime_type == '3') {
                                edit.item.block_vat = _this.options[value].regime_id == '4' ? !0 : !1;
                            } else {
                                edit.item.block_vat = !1
                            }
                            if (_this.options[value].regime_type == '1') {
                                edit.item.vat_regular = !0
                            } else {
                                edit.item.vat_regular = !1
                            }
                            edit.item.is_vat = !0
                        }
                        edit.changeLang(edit.item.email_language);
                        edit.vatApplyAll();
                        edit.refreshList();
                    }, function(e) {
                        if(e == 'cancel'){
                            if (_this.options[value].no_vat) {
                                edit.item.is_vat = !1;
                                edit.item.block_vat = !1;
                                edit.item.vat_regular = !1;
                                edit.vatApplyAll()
                            } else {
                                if (_this.options[value].regime_type == '2') {
                                    edit.item.block_vat = !0;
                                    edit.vatApplyAll()
                                } else if (_this.options[value].regime_type == '3') {
                                    edit.item.block_vat = _this.options[value].regime_id == '4' ? !0 : !1;
                                    edit.vatApplyAll()
                                } else {
                                    edit.item.block_vat = !1
                                }
                                if (_this.options[value].regime_type == '1') {
                                    edit.item.vat_regular = !0
                                } else {
                                    edit.item.vat_regular = !1
                                }
                                edit.item.is_vat = !0
                            }
                            edit.changeLang(edit.item.email_language);
                            edit.refreshList();
                        }else if(e == 'dismiss'){
                            selectize.addItem(edit.item.old_vat_regime_id, true);
                        }                 
                    });             
            } else {
                edit.item.block_vat = !1;
                edit.item.vat_regular = !0;
                edit.refreshList()
            }         
        }
    });

     edit.selectCategoryCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        maxItems: 1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value) {
            $confirm({
                    text: helper.getLanguage('Recalculate prices')
                }).then(function() {
                    var data = {};
                    for (x in edit.item) {
                        data[x] = edit.item[x]
                    }
                   
                    data['do'] = data.order_id != 'tmp' ? 'order-norder-order-updatePriceCategory' : ''
                    data.changeCatPrices = '1';
                    data.item_id = data.order_id;
                    helper.doRequest("post", 'index.php', data, function(r) {
                        if (r.success) {
                           edit.renderPage(r);
                        } 
                    })

                }, function() {
                   // console.log('test');
                   
                })
            }
        }
    };


    edit.linevatcfg = {
        valueField: 'vat',
        labelField: 'name',
        searchField: ['vat'],
        delimiter: '|',
        placeholder: helper.getLanguage('VAT'),
        create: !1,
        maxOptions: 100,
        onDelete(values) {
            return !1
        },
        closeAfterSelect: !0,
        maxItems: 1
    };
    edit.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    edit.showTxt = {
        info: !0,
        address: !1,
        editRef: !1,
        editYRef: !1,
        addBtn: !0,
        addArticleBtn: !1
    };
    edit.auto = {
        options: {
            author: [],
            accmanager: [],
            contacts: [],
            customers: [],
            addresses: [],
            site_addresses: [],
            cc: [],
            articles_list: []
        }
    };
    edit.dealAutoCfg = {
        valueField: 'deal_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Deal'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.name) + ' </span>' + '</div>' + '</div>' + '</div>';
                if (item.deal_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Add Deal') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined || value == '') {
                edit.item.old_deal_id = value
            } else if (value == '99999999999') {
                openModal('create_deal', {}, 'lg')
            } else {
                edit.item.old_deal_id = value
            }
        },
        onType(str) {
            var selectize = angular.element('#deal_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'order-norder',
                    xget: 'deals',
                    search: str,
                    order_id: edit.item.order_id,
                    version_id: edit.item.version_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.deals = r.data;
                    if (edit.item.deals.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        maxItems: 1
    };

    edit.tinymceOptionsLines = {
        selector: "",
        resize: "both",
        auto_resize: !0,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code autoresize"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        autoresize_bottom_margin: 0,
        setup: function(editor) {
            editor.on('click', function(e) {
                e.stopPropagation();
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                angular.element(editor.editorContainer).parent().removeClass('wysiwyg-hide')
            }).on('blur', function(e) {
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                edit.delaySave()
            })
        },
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
        content_css: [helper.base_url + 'css/tinymce_styles.css']
    };
edit.delaySave=function(showLoading, reload) {
        angular.element('.keep-open-on-click').on('click', function(e) {
            console.log('ccc');
            e.stopPropagation()
        });
        if (edit.typePromise) {
            console.log('ccc1');
            $timeout.cancel(edit.typePromise)
        }
        edit.typePromise = $timeout(function() {
            console.log('ccc2');
            //edit.save(showLoading, reload)
        }, 2000)
    }

    edit.dragControlListeners = {
        dragStart: function(event) {
            $timeout(function() {
                var id = angular.element('.table_orders').find('li').eq(event.source.index).find('textarea').attr('id');
                if(id){
                    $scope.$broadcast('$tinymce:refresh', id)
                }            
            })
        },
        dragEnd: function(event) {
            $timeout(function() {
                var id = angular.element('.table_orders').find('li').eq(event.dest.index).find('textarea').attr('id');
                if(id){
                    $scope.$broadcast('$tinymce:refresh', id)
                }            
            })
        }
    }

    edit.refreshList = function() {
        var data = {
            'do': 'order-addArticle',
            search: '',
            customer_id: edit.item.buyer_id,
            lang_id: edit.item.email_language,
            cat_id: edit.item.price_category_id,
            remove_vat: edit.item.remove_vat,
            vat_regime_id: edit.item.vat_regime_id
        };
        edit.requestAuto(edit, data, edit.renderArticleListAuto)
    }
    edit.vatApplyAll = function() {
        var vat = '0.00';
        var vat_intra = !1;
        for (x in edit.item.vat_regim_dd) {
            if (edit.item.vat_regime_id == edit.item.vat_regim_dd[x].id) {
                vat = edit.item.vat_regim_dd[x].value;
                if (edit.item.vat_regim_dd[x].regime_type == '2') {
                    vat_intra = !0
                }
                break
            }
        }
        if (vat_intra) {
            vat = 0
        }
        for (x in edit.item.tr_id) {
            edit.item.tr_id[x].percent_x = helper.displayNr(vat);
            edit.updateVat(x)
        }
    }
    edit.articlesListAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code', 'supplier_reference', 'name2'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                if (item.name2) {
                    item.name2 = "/ " + item.name2
                }
                if (item.allow_stock && item.is_service == 0) {
                    var stock_display = '';
                    if (item.hide_stock == 0) {
                        stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                    }
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '</small>' + '</div>' + stock_display + '</div>' + '</div>'
                } else {
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '</small>' + '</div>' + '<div class="col-sm-4 text-right">' + (item.base_price) + '</div>' + '</div>' + '</div>'
                }
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span>' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'order-addArticle',
                    search: str,
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id
                };
                edit.requestAuto(edit, data, edit.renderArticleListAuto)
            }, 300)
        },
        onChange(value) {
            if (value == undefined) {} else if (value == '99999999999') {
                edit.openModal(edit, 'AddAnArticle', {
                    'do': 'order-addAnArticle',
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id
                }, 'md');
                return !1
            } else {
                for (x in edit.auto.options.articles_list) {
                    if (edit.auto.options.articles_list[x].article_id == value) {
                        editItem.addArticle(edit, edit.auto.options.articles_list[x]);
                        edit.selected_article_id = '';
                        edit.showTxt.addArticleBtn = !1;
                        var data = {
                            'do': 'order-addArticle',
                            customer_id: edit.item.buyer_id,
                            lang_id: edit.item.email_language,
                            cat_id: edit.item.price_category_id,
                            remove_vat: edit.item.remove_vat,
                            vat_regime_id: edit.item.vat_regime_id
                        };
                        edit.requestAuto(edit, data, edit.renderArticleListAuto);
                        return !1
                    }
                }
            }
        },
        onItemAdd(value, $item) {
            if (value == '99999999999') {
                edit.selected_article_id = ''
            }
        },
        onBlur() {
            $timeout(function() {
                edit.showTxt.addArticleBtn = !1
            })
        },
        maxItems: 1
    };

    edit.parentVariantCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code', 'supplier_reference', 'name2'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        maxOptions: 5,
        render: {
            option: function(item, escape) {
                if (item.name2) {
                    item.name2 = "/ " + item.name2
                }
                if (item.allow_stock && item.is_service == 0) {
                    var stock_display = '';
                    if (item.hide_stock == 0) {
                        stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                    }
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '</small>' + '</div>' + stock_display + '</div>' + '</div>'
                } else {
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '</small>' + '</div>' + '<div class="col-sm-4 text-right">' + (item.base_price) + '</div>' + '</div>' + '</div>'
                }
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span>' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onFocus(){
            var selectize=this.$input[0].selectize;
            var index=this.$input[0].attributes['selectindex'].value;
            var parentVariant=this.$input[0].attributes['selectarticle'].value;
            var tmp_data={
                'do':'misc-articleVariants',
                'parentVariant':parentVariant,
                buyer_id: edit.item.buyer_id,
                lang_id: edit.item.email_language,
                cat_id: edit.item.price_category_id,
                remove_vat: edit.item.remove_vat,
                vat_regime_id: edit.item.vat_regime_id,
                app:edit.app
            };
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get','index.php',tmp_data,function(r){
                edit.item.tr_id[index].variantsOpts=r.data.lines;
                if(edit.item.tr_id[index].variantsOpts.length){
                    selectize.open();
                }
            });
        },
        onType(str) {
            var selectize=this.$input[0].selectize;
            var index=this.$input[0].attributes['selectindex'].value;
            var parentVariant=this.$input[0].attributes['selectarticle'].value;         
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do':'misc-articleVariants',
                    'parentVariant':parentVariant,
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id,
                    app:edit.app
                };
                helper.doRequest('get','index.php',tmp_data,function(r){
                    edit.item.tr_id[index].variantsOpts=r.data.lines;
                    if(edit.item.tr_id[index].variantsOpts.length){
                        selectize.open();
                    }
                });
            }, 300)
        },
        onChange(value) {
            var index=this.$input[0].attributes['selectindex'].value;
            if(value != undefined){
                var collection = edit.item.tr_id[index].variantsOpts;
                for(x in collection){
                    if(collection[x].article_id == value){
                        edit.item.tr_id[index].has_variants_done='1';
                        collection[x].is_variant_for_line = edit.item.tr_id[index].tr_id;
                        editItem.addArticle(edit, collection[x], parseInt(index)+1);
                        break; 
                    }
                }
            }
        },
        onBlur() {
            var index=this.$input[0].attributes['selectindex'].value;
            var parentVariant=this.$input[0].attributes['selectarticle'].value;
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {                
                var tmp_data={
                    'do':'misc-articleVariants',
                    'parentVariant':parentVariant,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id,
                    app:edit.app
                };
                helper.doRequest('get','index.php',tmp_data,function(r){
                    edit.item.tr_id[index].variantsOpts=r.data.lines;
                });
            }, 300)
        },
        maxItems: 1
    };

    edit.childVariantCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code', 'supplier_reference', 'name2'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        maxOptions: 5,
        render: {
            option: function(item, escape) {
                if (item.name2) {
                    item.name2 = "/ " + item.name2
                }
                if (item.allow_stock && item.is_service == 0) {
                    var stock_display = '';
                    if (item.hide_stock == 0) {
                        stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                    }
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '</small>' + '</div>' + stock_display + '</div>' + '</div>'
                } else {
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '</small>' + '</div>' + '<div class="col-sm-4 text-right">' + (item.base_price) + '</div>' + '</div>' + '</div>'
                }
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span>' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onType(str) {
            var selectize=this.$input[0].selectize;
            var index=this.$input[0].attributes['selectindex'].value;
            var parentVariant=this.$input[0].attributes['selectarticle'].value;         
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do':'misc-articleVariants',
                    'parentVariant':parentVariant,
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id,
                    app:edit.app
                };
                helper.doRequest('get','index.php',tmp_data,function(r){
                    edit.item.tr_id[index].variantsOpts=r.data.lines;
                    if(edit.item.tr_id[index].variantsOpts.length){
                        selectize.open();
                    }
                });
            }, 300)
        },
        onChange(value) {
            var index=this.$input[0].attributes['selectindex'].value;
            if(value != undefined){
                var tmp_data = angular.copy(edit.item.tr_id[index].variantsOpts);
                for(x in tmp_data){
                    if(tmp_data[x].article_id == value){
                        var tmp_selected_item=angular.copy(tmp_data[x]);
                        tmp_selected_item.quantity=parseFloat(helper.return_value(edit.item.tr_id[index].quantity,10));
                        edit.removeLine(index);
                        editItem.addArticle(edit, tmp_selected_item, index);
                        break; 
                    }
                }
            }
        },
        onBlur() {
            var index=this.$input[0].attributes['selectindex'].value;
            edit.item.tr_id[index].showVariants=false;
        },
        maxItems: 1
    };


    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {
        date: !1,
        delivery_date: !1
    }
    edit.renderPage = renderPage;
    edit.warningFnc = warningFnc;
    edit.openDate = openDate;
    edit.removeLine = removeLine;
    edit.calcTotal = calcTotal;
    edit.removeTaxes = removeTaxes;
    edit.removeComponent = removeComponent;
    edit.addContentLine = addContentLine;
    edit.changeDiscount = changeDiscount;
    edit.applyLineDiscount = applyLineDiscount;
    edit.save = save;
    edit.cancel = cancel;
    edit.changeLang = changeLang;
    edit.showAlerts = showAlerts;
    edit.item.showAlerts = showAlerts;
    edit.addArticle = addArticle;
    edit.addLine = addLine;
    edit.updateVat = updateVat;
    edit.removeDeliveryAddr = removeDeliveryAddr;
    edit.openModal=openModal;
    edit.changeCurrency = changeCurrency;
    //edit.setVisibility = setVisibility;
    editItem.init(edit);
    edit.renderPage(data);
    edit.salesCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: edit.item.is_admin,
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
           //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                edit.item[realModel] = o.data.lines;
                edit.item.payment_type = o.data.inserted_id
            })
        }
    }, !1, !0, edit.item);

    function renderPage(r) {
        if (r && r.data) {
            if (r.data.redirect) {
                $state.go(r.data.redirect);
                return !1
            }
            $scope.$parent.serial_number = r.data.ref_serial_number;
            helper.updateTitle($state.current.name,r.data.ref_serial_number);
            if(r.data.type_title){
                $scope.$parent.type_title =r.data.type_title;
            }
            if(r.data.nr_status){
                $scope.$parent.nr_status =r.data.nr_status;
            }
            if (((edit.item_id == 'tmp' && edit.old_buyer_id != r.data.buyer_id) || (edit.item_id != 'tmp' && edit.old_buyer_id !=undefined && edit.old_buyer_id != r.data.buyer_id)) && r.data.customer_credit_limit) {
                var msg = "<b>" + helper.getLanguage("") + "</b> <span>" + helper.getLanguage("This client is over his credit limit of") + " " + r.data.customer_credit_limit + "</span>";
                var cont = {
                    "e_message": msg,
                    'e_title': helper.getLanguage('Credit warning')
                };
                openModal("stock_info", cont, "md")
            }
            edit.item_id = r.data.order_id;
            for (x in r.data) {
                edit.item[x] = r.data[x]
            }
            edit.auto.options.author = r.data.authors;
            edit.auto.options.accmanager = r.data.accmanager;
            edit.auto.options.contacts = r.data.contacts;
            edit.auto.options.customers = r.data.customers;
            edit.auto.options.addresses = r.data.addresses;
            edit.auto.options.site_addresses = r.data.site_addresses;
            edit.auto.options.cc = r.data.cc;
            edit.auto.options.articles_list = r.data.articles_list.lines;
            edit.contactAddObj = {
                language_dd: edit.item.language_dd,
                language_id: edit.item.language_id,
                gender_dd: edit.item.gender_dd,
                gender_id: edit.item.gender_id,
                title_dd: edit.item.title_dd,
                title_id: edit.item.title_id,
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                add_contact: !0,
                buyer_id: edit.item.buyer_id,
                article: edit.item.tr_id,
                'do': 'order-norder-order-tryAddC',
                identity_id: edit.item.identity_id,
                contact_only: !0,
                order_id: edit.item_id
            }
            if (edit.item.date_h) {
                edit.item.date_h = new Date(edit.item.date_h)
            }
            if (edit.item.del_date) {
                edit.item.del_date = new Date(edit.item.del_date)
            }
            if (!edit.item.tr_id) {
                edit.item.tr_id = []
            }
            if (edit.showWarning && r.data.allow_stock && r.data.is_service == 0) {
                edit.warningFnc()
            }


        }
    }

    function showEditFnc() {
        edit.showTxt.address = !1;
        edit.oldObj = {
            buyer_id: angular.copy(edit.item.buyer_id),
            contact_id: angular.copy(edit.item.contact_id),
            contact_name: angular.copy(edit.item.contact_name),
            CUSTOMER_NAME: angular.copy(edit.item.customer_name),
            delivery_address: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address)),
            delivery_address_id: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_id)),
            delivery_address_txt: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_txt)),
            view_delivery: (edit.item.sameAddress ? !1 : angular.copy(edit.item.view_delivery)),
            sameAddress: angular.copy(edit.item.sameAddress),
        }
    }

    function showCCSelectize() {
        edit.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }

    function warningFnc() {
        var lines = [];
        if (edit.item.tr_id) {
            for (x in edit.item.tr_id) {
                var e = edit.item.tr_id[x];
                if (e.content === !1 && e.is_tax == 0) {
                    if (parseFloat(e.stock) - parseFloat(helper.return_value(e.quantity)) < parseFloat(e.threshold_value) && e.hide_stock != 1) {
                        lines.push(e)
                    }
                }
            }
        }
        if (lines.length > 0) {
            edit.openModal(edit, 'stockWarning', lines, 'md')
        }
        edit.showWarning = !1
    }

    edit.contactUpdated=function(language){
        //if(edit.order_id && !isNaN(edit.order_id)){
            if(language && language>0 && language != edit.item.email_language){
                var selectize = angular.element('#email_language')[0].selectize;
                selectize.addItem(language);
            }
        //}
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'create_deal':
                var iTemplate = 'CreateDeal';
                var ctas = 'show';
                break;
            case 'stock_info':
                var iTemplate = 'modal';
                var ctas = 'vmMod';
                break
        }
        var params = {
            template: 'oTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'create_deal':
                params.template = 'cTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'customers-deal_edit',
                    'opportunity_id': 'tmp',
                    'stage_id': edit.item.stage_id,
                    'board_id': edit.item.board_id,
                    'user_id': edit.item.user_id,
                    'buyer_id': edit.item.buyer_id,
                    'contact_id': edit.item.contact_id,
                    'not_redirect_deal': 0,
                    'amount':edit.item.total_vat,
                    'subject':edit.item.subject
                };
                params.callback = function(d) {
                    if (d) {
                        var data = {
                            'do': 'order-norder',
                            xget: 'deals',
                            buyer_id: edit.item.buyer_id
                        };
                        helper.doRequest('get', 'index.php', data, function(r) {
                            edit.item.deals = r.data.deals;
                            edit.item.deal_id=d.deal_id;
                            edit.item.old_deal_id=d.deal_id;
                            editItem.init(edit)
                        })
                    }else{
                        edit.item.deal_id = edit.item.old_deal_id
                    }           
                }
                break;
            case 'stock_info':
                params.template = 'miscTemplate/' + iTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                break
        }
        modalFc.open(params)
    }

    function openDate(p) {
        edit.datePickOpen[p] = !0
    }

   /* function setVisibility(item, i) {
        edit.item.tr_id[i].visible = !item.visible;
    }*/

    function removeDeliveryAddr() {
        if (edit.item.sameAddress == !0) {}
    }

    function removeLine($i) {
        //console.log(edit.item.tr_id); return false;
        var tr_id = edit.item.tr_id[$i].tr_id;
        edit.removeTaxes(tr_id);
        edit.removeComponent(tr_id);
        if(edit.item.tr_id[$i].has_variants == true || edit.item.tr_id[$i].has_variants == '1'){
            //removeVariant(edit.item.tr_id[$i].article_id);
            removeVariant(tr_id);
        }
    
        if ($i > -1) {
            edit.item.tr_id.splice($i, 1);
            edit.calcTotal()
        }
    }

    function calcTotal(type, $index) {
        editItem.calcTotal(edit.item, type, $index)
    }

    function updateVat($index) {
        edit.item.tr_id[$index].percent = helper.return_value(edit.item.tr_id[$index].percent_x);
        editItem.calcTotal(edit.item, 'price', $index)
    }

    function removeTaxes(id) {
        for (x in edit.item.tr_id) {
            if (edit.item.tr_id[x].for_article == id) {
                edit.item.tr_id.splice(x, 1);
                edit.removeTaxes(id)
            }
        }
    }

    function removeComponent(id) {
        for (x in edit.item.tr_id) {
            if (edit.item.tr_id[x].component_for == id) {
                edit.item.tr_id.splice(x, 1);
                edit.removeComponent(id)
            }
        }
    }

    function removeVariant(id) {
        for (x in edit.item.tr_id) {
            //if (edit.item.tr_id[x].is_variant_for == id) {
            if (edit.item.tr_id[x].is_variant_for_line == id) {
                edit.item.tr_id.splice(x, 1);
                break;
            }
        }
    }

    edit.showChildVariants=function(item,index){
        var tmp_data = {
            'do':'misc-articleVariants',
            'parentVariant':item.is_variant_for,
            'is_variant_for_line':item.is_variant_for_line,
            buyer_id: edit.item.buyer_id,
            lang_id: edit.item.email_language,
            cat_id: edit.item.price_category_id,
            remove_vat: edit.item.remove_vat,
            vat_regime_id: edit.item.vat_regime_id,
            app:edit.app
        };
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get','index.php',tmp_data,function(r){
            item.showVariants=true;
            edit.item.tr_id[index].variantsOpts=r.data.lines;
        });
    }

    function addContentLine() {
        /*var continut = 'ceva';
        for (x in edit.item.tr_id) {
            if (edit.item.tr_id[x].content === !0) {
                continut = edit.item.tr_id[x].article
            }
        }
        if (!continut) {
            edit.openModal(edit, 'Alert', 'Please fill the \'Article\' field.', 'sm');
            return !1
        }*/
        var content = {
            content: !0,
            article: '',
            tr_id: "tmp" + (new Date().getTime()),
            visible:'1'
        }
        edit.item.tr_id.push(content)
    }

    function changeDiscount() {
        if (edit.item.apply_discount_line == !0 && edit.item.apply_discount_global == !1) {
            edit.item.apply_discount = 1
        } else if (edit.item.apply_discount_global == !0 && edit.item.apply_discount_line == !1) {
            edit.item.apply_discount = 2
        } else if (edit.item.apply_discount_line == !0 && edit.item.apply_discount_global == !0) {
            edit.item.apply_discount = 3
        } else if (edit.item.apply_discount_line == !1 && edit.item.apply_discount_global == !1) {
            edit.item.apply_discount = 0
        }
        if (edit.item.apply_discount == 0 || edit.item.apply_discount == 2) {
            if (edit.item.hide_disc === !0) {
                for (x in edit.item.tr_id) {
                    if (edit.item.tr_id[x].content === !1) {
                        edit.item.tr_id[x].colum++
                    }
                }
                edit.item.colum++;
                edit.item.hide_disc = !1
            }
        } else {
            if (edit.item.hide_disc === !1) {
                for (x in edit.item.tr_id) {
                    if (edit.item.tr_id[x].content === !1) {
                        edit.item.tr_id[x].colum--
                    }
                }
                edit.item.colum--;
                edit.item.hide_disc = !0
            }
        }
        if (edit.item.apply_discount > 1) {
            edit.item.discount_percent = '( ' + edit.item.discount + '% )';
            edit.item.hide_global_discount = !0
        } else {
            edit.item.discount_percent = '( ' + helper.displayNr(0) + '% )';
            edit.item.hide_global_discount = !1
        }
        edit.calcTotal()
    }

    function applyLineDiscount() {
        for (x in edit.item.tr_id) {
            if (edit.item.tr_id[x].content === !1) {
                edit.item.tr_id[x].disc = edit.item.discount_line_gen
            }
        }
        edit.calcTotal()
    }

    function save(formObj) {
        angular.element('.loading_wrap').removeClass('hidden');
        var data = angular.copy(edit.item);
        data.do = data.do_next;
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.success) {
                $state.go('order.view', {
                    order_id: r.data.order_id
                })
            }
            if(r && r.error){
                helper.showAlerts(edit,r,formObj);
            }
        })
    }

    function cancel() {
        if (edit.item_id == 'tmp') {
            $state.go('orders')
        } else {
            $state.go('order.view', {
                order_id: edit.item_id
            })
        }
    }

    function changeLang(language) {
        var data = {
            'do': 'order-norder',
            'email_language': language,
            'xget': 'notes',
            'vat_regime_id': edit.item.vat_regime_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            if (r && r.data) {
                edit.item.notes = r.data.notes;
                edit.item.email_language = r.data.email_language;
            }
        })
    }

    function showAlerts(d) {
        helper.showAlerts(edit, d)
    }

    function addArticle() {
        edit.showTxt.addArticleBtn = !0;
        $timeout(function() {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.open()
        })
    }

    function addLine() {
        edit.item.packing = 1;
        edit.item.sale_unit = 1;
        editItem.addArticle(edit, edit.item)
    }

    function changeCurrency() {
        var data = {
            'do': 'order-norder',
            order_id: edit.item_id,
            buyer_id: edit.item.buyer_id,
            'currency_type': edit.item.currency_type,
            'change_currency': '1'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            if (r && r.data) {
                edit.item.currency_val = r.data.currency_val;
                edit.item.currency_txt = r.data.currency_txt;
                edit.item.currency_rate = r.data.currency_rate;
                edit.item.total_currency_hide = r.data.total_currency_hide;
                edit.item.total_default_currency = helper.displayNr(helper.return_value(edit.item.total) * helper.return_value(r.data.currency_rate));
            }
        });
    }


}]).controller('deliveryAddressModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.obj = data;
    $scope.ok = function() {
        $scope.closeModal($scope.address)
    };
    $scope.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    $scope.setItem = function(item) {
        $scope.address = [item.address + "<br>" + item.zip + " " + item.city + "<br>" + item.country, item.address + "\n" + item.zip + " " + item.city + "\n" + item.country]
    }
}]).controller('addArticleModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'editItem', 'data', 'list',function($scope, helper, $uibModalInstance, editItem, data, list) {
    console.log('Add Article Modal Ctrl');
    var originalScope = editItem.getScope();

    $scope.obj = data;
    $scope.max_rows = data.max_rows;
    $scope.lr = data.lr;
    $scope.search = {
        search: '',
        'do': 'misc-addArticleList',
        app: originalScope.app,
        customer_id: $scope.obj.customer_id,
        lang_id: $scope.obj.lang_id,
        offset: 1,
        //cat_id: $scope.obj.cat_id,
        cat_id:originalScope.item.cat_id,
        remove_vat: $scope.obj.remove_vat,
        vat_regime_id: $scope.obj.vat_regime_id,
        hide_article_ids :$scope.obj.hide_article_ids
    };
    $scope.extraParams = {
        customer_id: $scope.obj.customer_id,
        lang_id: $scope.obj.lang_id,
        //cat_id: $scope.obj.cat_id
        cat_id:originalScope.item.cat_id,
    };
    $scope.filterCfgFam = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Families'),
        create: !1,
        onChange(value) {
            $scope.toggleSearchButtons('article_category_id', value)
        },
        maxItems: 1
    };
    $scope.ok = function(item) {
        if (originalScope.app == 'invoice') {
            editItem.addInvoiceArticle(originalScope, item)
        }
        if (originalScope.app == 'order' ) {
            editItem.addArticle(originalScope, item)
        }
        if (originalScope.app == 'article' ) {
            editItem.addArticleComposedLine(originalScope, item);
            var obj = {};
            obj.value = item.article_id;
            obj.parent_article_id = originalScope.item.article_id;
            obj.quantity = item.quantity;
            obj.use_combined = originalScope.composed.use_combined;
            obj.do = 'article-add-article-addCombinedArticle';
            obj.xget = 'articleCombine';

            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', obj, function(r) {
                if (r && r.data) {
                    if (r.success) {
                        originalScope.composed.potential_stock = r.data.composed.potential_stock;
                        for (x in originalScope.composed.articles) {
                            originalScope.composed.articles[x].pim_articles_combined_id = r.data.composed.articles[x].pim_articles_combined_id;
                        }
                    }
                }
            })


        }
        if (originalScope.app == 'quote' || originalScope.app == 'contract') {
            editItem.addQuoteArticle(originalScope, item)
        }
        if (originalScope.app == 'maintenance' || originalScope.app == 'project') {
            if (item.is_service == '1') {
                if (originalScope.app == 'maintenance') {
                    var obj = {};
                    obj.service_id = originalScope.item.service_id;
                    obj.do = 'maintenance-service-maintenance-serviceAddService';
                    obj.xget = 'serviceSubServices';
                    obj.task_service_id = item.article_id;
                    obj.task_name = item.name;
                    obj.task_budget = item.price;
                    obj.quantity = item.quantity;
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('post', 'index.php', obj, function(r) {
                        if (r && r.data) {
                            if (r.success) {
                                originalScope.item.services = r.data;
                                originalScope.item.task_service_id = undefined
                            }
                            originalScope.item.task_name = '';
                            originalScope.item.add_subtask1 = !1;
                            originalScope.item.add_subtask2 = !1
                        }
                    })
                } else {
                    originalScope.item.task_name = item.name;
                    originalScope.AddTask(1)
                }
            } else {
                originalScope.addArticle(item)
            }
        }
        item.succes_add = !0
    };
    $scope.createItem = function() {
        editItem.openModal($scope, 'AddAnArticle', {
            'do': 'misc-addAnArticle',
            customer_id: $scope.obj.customer_id,
            lang_id: $scope.obj.lang_id,
            cat_id: $scope.obj.cat_id,
            remove_vat: $scope.obj.remove_vat,
            vat_regime_id: $scope.obj.vat_regime_id
        }, 'md')
    }
    $scope.openModal = function(type, item, size) {
        var params = {
            template: 'iTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'edit_purchase_price':
                params.params = {
                    'do': 'invoice-' + type,
                    purchase_price: item.purchase_price,
                    index: item.index
                };
                params.callback = function(data) {
                    if (data) {
                        edit.item.invoice_line[data.index].purchase_price = data.purchase_price
                    }
                }
                break
        }
        modalFc.open(params)
    }
    $scope.renderList = function(d) {
        if (d.data) {
            $scope.obj = d.data;
            $scope.lr = d.data.lr;
            $scope.offset = d.data.offset;
            $scope.max_rows = d.data.max_rows
        }
    };
    $scope.toggleSearchButtons = function(item, val) {
        $scope.search[item] = val;
        helper.doRequest('get', 'index.php?do=misc-addArticleList', {
            search: $scope.search.search,
            offset: $scope.obj.offset,
            customer_id: $scope.obj.customer_id,
            lang_id: $scope.obj.lang_id,
            remove_vat: $scope.obj.remove_vat,
            vat_regime_id: $scope.obj.vat_regime_id,
            article_category_id: $scope.search.article_category_id,
            app: $scope.obj.app
        }, $scope.renderList)
    };
    $scope.searchThing = function() {
        helper.searchIt($scope.search, $scope.renderList)
    }
    $scope.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }

    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };

}]).controller('addArticleSpecialModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'editItem', 'data', function($scope, helper, $uibModalInstance, editItem, data) {
    var originalScope = editItem.getScope();
    $scope.obj = data;
    $scope.ok = function(item) {
        editItem.addInvoiceArticleSpecial(originalScope, item);
        if (!$scope.alerts) {
            $scope.alerts = []
        }
        $scope.alerts.push({
            type: 'success',
            msg: helper.getLanguage('Item added.')
        });
        $uibModalInstance.dismiss('cancel')
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('purchasePriceModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.obj = data;
    $scope.ok = function() {
        $scope.closeModal($scope.obj)
    };
    $scope.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('stockWarningModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.obj = data;
    $scope.isMultiple = !1;

    if ($scope.obj.length) {
        // $scope.isMultiple = !0;
        // for (x in $scope.obj) {
        //     $scope.obj[x].new_stock = parseFloat($scope.obj[x].stock) - parseFloat(helper.return_value($scope.obj[x].quantity));
        //     if ($scope.obj[x].new_stock < 0) {
        //         $scope.obj[x].color = '#ff2100'
        //     } else {
        //         $scope.obj[x].color = '#ea7a69'
        //     }
        // }
    } else {
        //$scope.obj.new_stock = helper.return_value(data.stock2) - data.quantity;
        //console.log(parseFloat(data.stock),parseFloat(helper.return_value(data.quantity)));
        $scope.obj.new_stock = helper.displayNr( parseFloat(data.stock) - parseFloat(helper.return_value(data.quantity)) );
        if ($scope.obj.new_stock < 0) {
            $scope.obj.color = '#ff2100'
        } else {
            $scope.obj.color = '#ea7a69'
        }
    }
    $scope.ok = function() {
        $uibModalInstance.close()
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('serialNrModalCtrl', ['$scope', '$filter', '$timeout','helper', '$uibModalInstance', 'data', function($scope, $filter,$timeout, helper, $uibModalInstance, data) {;
    var typePromise;
    $scope.obj = data;
    $scope.obj.serial_nr_row_search=angular.copy($scope.obj.serial_nr_row);
    $scope.slectItem = function(item, from, to) {
        $scope.obj[to].push(item);
        for(x in $scope.obj[from]){
            if($scope.obj[from][x].serial_nr_id == item.serial_nr_id){
                $scope.obj[from].splice(x, 1);
                break;
            }
        }
        $scope.obj.sel_s_n = $scope.obj.sel_serial_nr_row.length;
        $scope.searchSerial();
    }
    $scope.search="";
    $scope.searchSerial=function(){
        if (typePromise) {
            $timeout.cancel(typePromise);
        }
        typePromise = $timeout(function() {
            $scope.obj.serial_nr_row_search=$filter('filter')($scope.obj.serial_nr_row, { 'serial_number':$scope.search });
        },300);       
    }
    $scope.ok = function(d) {
        if ($scope.obj.sel_s_n != $scope.obj.total_s_n) {
            if (!$scope.alerts) {
                $scope.alerts = []
            }
            $scope.alerts.push({
                type: 'danger',
                msg: 'Serial Numbers quantity is incorrect'
            });
            return !1
        }
        $uibModalInstance.close(d)
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('batchNrModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.obj = data;
    $scope.ok = function(d) {
        if ($scope.obj.selected_b_n != $scope.obj.total_b_n) {
            if (!$scope.alerts) {
                $scope.alerts = []
            }
            $scope.alerts.push({
                type: 'danger',
                msg: 'Article quantity is incorrect'
            });
            return !1
        }
        for (x in $scope.obj.b_n_row) {
            if ($scope.obj.b_n_row[x].batch_no_checked && $scope.obj.b_n_row[x].is_error) {
                   if (!$scope.alerts) {
                        $scope.alerts = []
                    }
                    $scope.alerts.push({
                        type: 'danger',
                        msg: 'Article quantity is incorrect'
                    });
                    return !1         
            }
        }
        $uibModalInstance.close(d)
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    $scope.batchNrChange = function($index) {
        $scope.alerts = [];
        $scope.obj.total_b_n = 0;
        for (x in $scope.obj.b_n_row) {
            if ($scope.obj.b_n_row[x].batch_no_checked) {
                $scope.obj.total_b_n += parseFloat(helper.return_value($scope.obj.b_n_row[x].b_n_q_selected)) * 1
            }
        }
        //console.log($scope.obj.b_n_row[$index].b_n_q_selected, $scope.obj.b_n_row[$index].in_stock,$scope.obj.packing, $scope.obj.sale_unit, parseFloat(helper.return_value($scope.obj.b_n_row[$index].b_n_q_selected)) * 1, parseFloat(helper.return_value($scope.obj.b_n_row[$index].b_n_q_selected)) );
        if($scope.obj.b_n_row[$index].b_n_q_selected> $scope.obj.b_n_row[$index].in_stock/$scope.obj.packing* $scope.obj.sale_unit){
            $scope.obj.b_n_row[$index].is_error = true;
        }else{
            $scope.obj.b_n_row[$index].is_error = false;
        }
         $scope.obj.total_b_n = helper.displayNr($scope.obj.total_b_n);
    }
    $scope.batchSelChange = function() {
        $scope.obj.selected_batches = 0;
        $scope.obj.total_b_n = 0;
        for (x in $scope.obj.b_n_row) {
            if ($scope.obj.b_n_row[x].batch_no_checked) {
                $scope.obj.selected_batches++;
                $scope.obj.total_b_n +=  parseFloat(helper.return_value($scope.obj.b_n_row[x].b_n_q_selected)) * 1
            }else{
                $scope.obj.b_n_row[x].b_n_q_selected = helper.displayNr(0);
            }
        }
        $scope.obj.total_b_n = helper.displayNr($scope.obj.total_b_n);
    }
}]).controller('AlertModalCtrl', ['$scope', '$uibModalInstance', 'data', function($scope, $uibModalInstance, data) {
    var vmMod = this
    vmMod.obj = data;
    vmMod.ok = function() {
        $uibModalInstance.close()
    };
    vmMod.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('DispatchModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.ok = ok;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;

    function ok() {
        helper.doRequest('post', 'index.php', vmMod.obj, function(d) {
            if (d.success) {
                $uibModalInstance.close(d)
            } else {
                vmMod.showAlerts(d)
            }
        })
    };

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    };

    function showAlerts(d) {
        helper.showAlerts(vmMod, d)
    }
}]).controller('OrderSettingsCtrl', ['$scope', 'helper', '$uibModal', 'settingsFc', 'selectizeFc', 'data', function($scope, helper, $uibModal, settingsFc, selectizeFc, data) {
    var vm = this;
    vm.app = 'order';
    vm.ctrlpag = 'settings';
    vm.tmpl = 'otemplate';
    settingsFc.init(vm);
    vm.logos = {};
    vm.settings = {
        show_bank: !1,
        show_stamp: !1,
        show_page: !1,
        show_paid: !1
    };
    vm.customLabel = {};
    vm.custom = {
        pdf_active: !1,
        pdf_note: !1,
        pdf_condition: !1,
        pdf_stamp: !1
    };
    vm.custom.layouts = {
        clayout: [],
        selectedOption: '0'
    };
    vm.message = {};
    vm.email_default = {};
    vm.weblink = {};
    vm.creditnote = {};
    vm.timesheets = {};
    vm.chargevats = {};
    vm.exportsettings = {};
    vm.vats = {};
    vm.sepa_active = {};
    vm.convention = {};
    vm.message_po = {};
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select type'),
        labelField: 'name',
        searchField: ['name'],
    });
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 850,
        height: 300,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.updateLayout = function() {
        var get = 'PDFlayout';
        var data = {
            'do': 'order-settings-order-settings',
            'xget': get,
            'show_backorders': vm.settings.show_backorders
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.Changecodelabes = function(vm, r) {
        var get = 'emailMessage';
        var data = {
            'do': 'order-settings',
            'xget': get,
            detail_id: r.detail_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            vm.message_labels = {
                detail_id: r.data.message_labels.detail_id,
                detail: r.data.message_labels.detail,
                selected_detail: r.data.message_labels.selected_detail
            };
            vm.showAlerts(vm, r)
        })
    }
    vm.salesCfg = selectizeFc.configure({
        maxOptions: 100,
        placeholder: helper.getLanguage('Select User'),
        onChange(value) {
            var data = {
                'do': 'order-settings-order-setMobile',
                'xget': 'mobile',
                user_id: vm.mobile.user_exist,
                'active': vm.mobile.active
            };
            helper.doRequest('post', 'index.php', data, function(r) {
                vm.mobile = r.data.mobile;
                vm.showAlerts(vm, r)
            })
        }
    });
    vm.mobileSettings = function(vm, r) {
        var get = 'mobile';
        var data = {
            'do': 'order-settings-order-setMobile',
            'xget': get,
            'active': vm.mobile.active,
            user_id: vm.mobile.user_exist
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.mobile = r.data.mobile;
            vm.showAlerts(r)
        })
    }
    vm.listSet = function(vm, r) {
        var get = 'listSettings';
        var data = {
            'do': 'order-settings-order-order_note_list',
            'xget': get,
            'list': vm.listSettings.list
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.listSettings = r.data.listSettings;
            vm.showAlerts(r)
        })
    }
    vm.load = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'order-settings',
            'xget': h
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
            if (k == 'logos') {
                vm.default_logo = r.data.default_logo
            }
        })
    }
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            vm.selected = selectedItem
        }, function() {
            if (r.load) {
                vm.load(r.load[0], r.load[1])
            }
        })
    };
    vm.PageActive = function() {
        var get = 'general';
        var data = {
            'do': 'invoice--invoice-PageActive',
            'xget': get,
            'page': vm.general_condition.page
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.changedelivery = function() {
        var get = 'delivery';
        var data = {
            'do': 'order-settings-order-changedelivery',
            'xget': get,
            'delivery_type': vm.orderTerm.delivery.delivery_type
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.orderTerm.delivery = r.data.delivery;
            //vm.showAlerts(vm, r)
        })
    }
    vm.Convention = function() {
        var get = 'Convention';
        var data = angular.copy(vm.convention);
        data.do = 'order-settings-order-Convention';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.convention = r.data.convention;
            vm.showAlerts(vm, r)
        })
    }
    vm.saveSettingOrder = function() {
        var get = 'emailMessage';
        var data = angular.copy(vm.message);
        data.do = 'order-settings-order-default_message_order';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.message = r.data.message;
            vm.showAlerts(vm, r)
        })
    }
    vm.saveSettingDelivery = function() {
        var get = 'emailMessageDelivery';
        var data = angular.copy(vm.message);
        data.do = 'order-settings-order-default_message';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.message = r.data.message;
            vm.showAlerts(vm, r)
        })
    }
    vm.changelang_po = function() {
        var get = 'emailMessage';
        var data = angular.copy(vm.message.message_po);
        data.do = 'order-settings-order-default_message_po';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.message = r.data.message;
            vm.message.message_po = r.data.message.message_po;
            vm.showAlerts(vm, r)
        })
    }
    vm.saveSettingOrder_po = function() {
        var get = 'emailMessage';
        var data = angular.copy(vm.message);
        data.do = 'order-settings-order-default_message_order_po';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.message = r.data.message;
            vm.showAlerts(vm, r)
        })
    }
    vm.check_allow = function() {
        var get = 'attachments';
        var data = angular.copy(vm.checkbox_allow);
        data.do = 'order-settings-order-setSetting';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.checkbox_allow = r.data.checkbox_allow;
            vm.atachment = r.data.atachment;
            vm.showAlerts(vm, r)
        })
    }
    vm.creditupdate = function(r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.creditnote = r.data.creditnote;
            vm.showAlerts(vm, r)
        })
    }
    vm.deleteatach = function(vm, id, name) {
        var get = 'attachments';
        var data = {
            'do': 'order-settings-order-deleteatach',
            'xget': get,
            'id': id,
            'name': name
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.atachment = r.data.atachment;
            vm.showAlerts(vm, r)
        })
    }
    vm.defaultcheck = function(vm, id, name, default_id) {
        var get = 'attachments';
        var data = {
            'do': 'order-settings-order-defaultcheck',
            'xget': get,
            'id': id,
            'name': name,
            'default_id': default_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.atachment = r.data.atachment;
            vm.showAlerts(vm, r)
        })
    }
    vm.timesheetup = function(r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.creditnote = r.data.creditnote;
            vm.showAlerts(vm, r)
        })
    }
    vm.chargevatup = function(r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.chargevats = r.data.chargevats;
            vm.showAlerts(vm, r)
        })
    }
    vm.exportsettingup = function(field_name, field_id, field_value) {
        var get = 'exportsetting';
        var data = {
            'do': 'invoice-settings-invoice-update_export_settings',
            'xget': get,
            'field_name': field_name,
            'field_id': field_id,
            'field_value': field_value
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.exportsettings = r.data.exportsettings;
            vm.showAlerts(vm, r)
        })
    }
    vm.vatclasification = function(field_name, field_id, field_value, field_id_2) {
        var get = 'exportsetting';
        var data = {
            'do': 'invoice-settings-invoice-update_export_settings',
            'xget': get,
            'field_name': field_name,
            'field_id': field_id,
            'field_value': field_value,
            'field_id_2': field_id_2
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.exportsettings = r.data.exportsettings;
            vm.showAlerts(vm, r)
        })
    }
    vm.saveterm = function() {
        var data = angular.copy(vm.orderTerm);
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.orderTerm = r.data.orderTerm;
            vm.showAlerts(vm, r)
        })
    }
    vm.sepaupdate = function(r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.sepa_active = r.data.sepa_active;
            vm.showAlerts(vm, r)
        })
    }
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.articlefields.text = vm.articlefields.text.substring(0, startPos) + myValue + vm.articlefields.text.substring(endPos, vm.articlefields.text.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.articlefields.text += myValue;
            _this.focus()
        }
    }
    vm.clickys = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.message.html_content = vm.message.html_content.substring(0, startPos) + myValue + vm.message.html_content.substring(endPos, vm.message.html_content.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.message.html_content += myValue;
            _this.focus()
        }
    }
    vm.renderPage = function(r) {
        helper.doRequest('get', 'index.php?do=order-settings&xget=PDFlayout', function(r) {
            vm.settings = {
                show_bank: r.data.show_bank,
                show_stamp: r.data.show_stamp,
                show_page: r.data.show_page,
                show_paid: r.data.show_paid,
                multiple_identity: r.data.multiple_identity,
                identity: r.data.identity,
                identity_delivery: r.data.identity,
                selected_header: r.data.selected_header,
                selected_body: r.data.selected_body,
                selected_footer: r.data.selected_footer,
                show_backorders: r.data.show_backorders
            };
            vm.layout_header = r.data.layout_header;
            vm.layout_body = r.data.layout_body;
            vm.layout_footer = r.data.layout_footer;
            vm.layout_header_delivery = r.data.layout_header_delivery;
            vm.layout_body_delivery = r.data.layout_body_delivery;
            vm.layout_footer_delivery = r.data.layout_footer_delivery;
            vm.custom_layout = r.data.custom_layout;
            vm.custom_layout_delivery = r.data.custom_layout_delivery;
            vm.has_custom_layout = r.data.has_custom_layout;
            vm.use_custom_layout = r.data.use_custom_layout;
            vm.use_custom_layout_delivery = r.data.use_custom_layout_delivery;
        })
    };

    vm.removeActiveTabs=function(array){
        for(var x in array){
            angular.element('#'+array[x]).removeClass('active in');
        }
    }

    vm.renderPage(data);
    vm.load('Convention','convention');

}]).controller('OrderCustomPdfCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'order';
    vm.ctrlpag = 'custom_pdf';
    vm.tmpl = 'otemplate';
    settingsFc.init(vm);
    vm.codelabels = {};
    vm.selectlabels = {};
    vm.showtiny = !0;
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 1110,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload_image_pdf').trigger('click');
                $('#upload_image_pdf').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.codelabels.variable_data = vm.codelabels.variable_data.substring(0, startPos) + myValue + vm.codelabels.variable_data.substring(endPos, vm.codelabels.variable_data.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.codelabels.variable_data += myValue;
            _this.focus()
        }
    }
    vm.closeModal = closeModal;
    vm.cancel = cancel;

    function closeModal() {
        $uibModalInstance.close()
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    }

    helper.doRequest('get', 'index.php?do=order-custom_pdf&layout=' + obj.id + '&header=' + obj.header + '&footer=' + obj.footer+ '&identity_id=' + obj.identity_id, function(r) {
        vm.codelabels = {
            variable_data: r.data.codelabels.variable_data,
            layout: r.data.codelabels.layout,
            header: r.data.codelabels.header,
            footer: r.data.codelabels.footer,
            identity_id: r.data.codelabels.identity_id,
        };
        vm.selectlabels = {
            make_id: r.data.selectlabels.make_id,
            make: r.data.selectlabels.make,
            selected_make: r.data.selectlabels.selected_make
        };
        vm.selectdetails = {
            detail_id: r.data.selectdetails.detail_id,
            detail: r.data.selectdetails.detail
        }
    })
}]).controller('OrderlabelCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'order';
    vm.ctrlpag = 'labelCtrl';
    vm.tmpl = 'oTemplate';
    settingsFc.init(vm);
    vm.labels = {};
    helper.doRequest('get', 'index.php', obj, function(r) {
        vm.labels = r.data.labels
    });
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('OrderDeliveryCustomPdfCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'order';
    vm.ctrlpag = 'custom_pdf_delivery';
    vm.tmpl = 'otemplate';
    settingsFc.init(vm);
    vm.codelabels = {};
    vm.selectlabels = {};
    vm.showtiny = !0;
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 1110,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload_image_pdf').trigger('click');
                $('#upload_image_pdf').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.codelabels.variable_data = vm.codelabels.variable_data.substring(0, startPos) + myValue + vm.codelabels.variable_data.substring(endPos, vm.codelabels.variable_data.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.codelabels.variable_data += myValue;
            _this.focus()
        }
    }
    vm.closeModal = closeModal;
    vm.cancel = cancel;

    function closeModal() {
        $uibModalInstance.close()
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    }

    helper.doRequest('get', 'index.php?do=order-custom_pdf_delivery&layout=' + obj.id + '&header=' + obj.header + '&footer=' + obj.footer+ '&identity_id=' + obj.identity_id, function(r) {
        vm.codelabels = {
            variable_data: r.data.codelabels.variable_data,
            layout: r.data.codelabels.layout,
            header: r.data.codelabels.header,
            footer: r.data.codelabels.footer,
            identity_id: r.data.codelabels.identity_id,
        };
        vm.selectlabels = {
            make_id: r.data.selectlabels.make_id,
            make: r.data.selectlabels.make,
            selected_make: r.data.selectlabels.selected_make
        };
        vm.selectdetails = {
            detail_id: r.data.selectdetails.detail_id,
            detail: r.data.selectdetails.detail
        }
    })
}]).controller('OrderDownpaymentModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.data=data;

    vm.showTxt={
        'btn_create':false
    };

    vm.downpayment_opts=[{'id':'1','name':helper.getLanguage('Excl. VAT')},{'id':'2','name':helper.getLanguage('Incl. VAT')}];

    vm.selectfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        onChange(value){
            vm.setDownType('1');
        },
        maxItems: 1
    };

    vm.downpayment_opt_id='1';

    vm.vat = 0;

    var vat_intra = !1;
    for (x in vm.data.vat_regim_dd) {
        if (vm.data.vat_regime_id == vm.data.vat_regim_dd[x].id) {
            vm.vat = parseInt(vm.data.vat_regim_dd[x].value);
            if (vm.data.vat_regim_dd[x].regime_type == '2') {
                vat_intra = !0;
            }
            break;
        }
    }
    if (vat_intra) {
        vm.vat = 0;
    }  

    vm.cancel=function() {
        $uibModalInstance.close();
    }

    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d);
    }

    vm.setDownType = function(type) {
        /*var total_order = vm.data.totalul.total_order_no_vat;
        if(vm.vat && vm.vat>0 && vm.downpayment_opt_id == '2'){
            total_order+=(vm.data.totalul.total_order_no_vat*vm.vat/100);
        }*/
        var total_order=vm.downpayment_opt_id == '2' ? vm.data.totalul.total_order_vat : vm.data.totalul.total_order_no_vat;
        if (type == '1') {
            vm.data.downpayment_percent = helper.displayNr(parseFloat(helper.return_value(vm.data.downpayment_fixed), 10) * 100 / total_order)
        } else {
            vm.data.downpayment_fixed = helper.displayNr(parseFloat(helper.return_value(vm.data.downpayment_percent), 10) / 100 * total_order)
        }
        vm.data.downpayment_type = type;
        //if (parseFloat(helper.return_value(vm.data.downpayment_fixed), 10) > vm.data.totalul.total_order_no_vat) {
        if (parseFloat(helper.return_value(vm.data.downpayment_fixed), 10) > total_order) {
            vm.showTxt.btn_create=false;
            var obj = {
                "error": {
                    "error": helper.getLanguage("Downpayment amount greater than order value")
                },
                "notice": !1,
                "success": !1
            };
            vm.showAlerts(obj);         
        }else if(parseFloat(helper.return_value(vm.data.downpayment_fixed), 10)<=0){
            /*vm.showTxt.btn_create=false;
            var obj = {
                "error": {
                    "error": helper.getLanguage("Please select downpayment value")
                },
                "notice": !1,
                "success": !1
            };
            vm.showAlerts(obj);  */
        }else{
            vm.showTxt.btn_create=true;
        }
    }

    vm.createDownPayment = function() {
        var obj = {};
        obj.downpayment = '1';
        obj.downpayment_type = vm.data.downpayment_type;
        obj.downpayment_fixed = vm.vat && vm.vat>0 && vm.downpayment_opt_id == '2' ? helper.displayNr((parseFloat(helper.return_value(vm.data.downpayment_fixed),10)*100)/(100+vm.vat)) : vm.data.downpayment_fixed;
        obj.downpayment_percent = vm.data.downpayment_percent;
        obj.service_type = vm.data.service_type;
        obj.order_id = vm.data.order_id;
        obj.do = 'order-order-order-downpaymentInvoice';
        helper.doRequest('post', 'index.php', obj, function(r) {           
            if (r.success) {
                $uibModalInstance.close(r);
            }else{
                vm.showAlerts(r);
            }
        })
    }

    
}]).controller('OrdersToBeDeliveredCtrl', ['$scope', 'helper', 'list', 'selectizeFc', function($scope, helper, list, selectizeFc) {
    console.log('Orders to be delivered list');
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.ord="item_code";
    $scope.max_rows = 0;
    // $scope.views = [
    // {
    //     name: helper.getLanguage('All Articles'),
    //     id: 0,
    //     active: 'active'
    // },
    // {
    //     name: helper.getLanguage('In stock articles'),
    //     id: 1,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Not in stock articles'),
    //     id: 2,
    //     active: ''
    // }];
    $scope.list = [];
    $scope.listStats = {};
    $scope.search = {
        search: '',
        'do': 'order-articles_to_deliver',
        view: 0,
        xget: '',
        showAdvanced: !1,
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1,
        expand_all:!1
    };
    $scope.show_load = !0;
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = parseInt(d.data.max_rows);
            $scope.listStats = {};
            $scope.lr = parseInt(d.data.lr);
            $scope.minimum_selected=d.data.minimum_selected;
            $scope.all_pages_selected=d.data.all_pages_selected;
            $scope.nav = d.data.nav;
            $scope.view_dd = d.data.view_dd
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['article_id']);
                count ++;
            }
            sessionStorage.setItem('order_to_be_delivered.view_total', count);
            sessionStorage.setItem('order_to_be_delivered.view_list', JSON.stringify(ids));

            //$scope.initCheckAll();
        }
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        $scope.listObj.expand_all=false;
        angular.element('.mainTable thead tr').find('.fa-chevron-right').removeClass('fa-chevron-down');
        list.tSearch($scope, item, val, $scope.renderList)
        $scope.checkSearch()
    };
    $scope.checkSearch = function() {
        var data = angular.copy($scope.search),
            is_search = !1;
        data = helper.clearData(data, ['do', 'xget', 'showAdvanced', 'showList', 'offset']);
        for (x in data) {
            if (data[x]) {
                is_search = !0
            }
        }
        // vm.search.showList = is_search
    }
    $scope.searchThing = function() {
        $scope.listObj.expand_all=false;
        angular.element('.mainTable thead tr').find('.fa-chevron-right').removeClass('fa-chevron-down');
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.orderBY = function(p) {
        $scope.listObj.expand_all=false;
        angular.element('.mainTable thead tr').find('.fa-chevron-right').removeClass('fa-chevron-down');
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.markCheckNew=function(i){
        list.markCheckNew($scope, 'order--order-saveAddToPdfToBeDelivered', i);
    }
    $scope.checkAllPage=function(){
        $scope.listObj.check_add_all=true;
        $scope.markCheckNew();
    }
    $scope.checkAllPages=function(){
        list.markCheckNewAll($scope, 'order--order-saveAddToPdfAllToBeDelivered','1');
    }
    $scope.uncheckAllPages=function(){
        list.markCheckNewAll($scope, 'order--order-saveAddToPdfAllToBeDelivered','0');
    }
    $scope.createExports = function($event) {
        list.exportFnc($scope, $event)
        var params = angular.copy($scope.search);
        params.do = 'order-articles_to_deliver'
        params.exported = !0;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r);
        })
    };
    $scope.filterCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('All Articles'),
        onChange(value) {
            $scope.toggleSearchButtons('view', value)
        }
    });
    // $scope.filterCfg = {
    //     valueField: 'id',
    //     labelField: 'name',
    //     searchField: ['name'],
    //     placeholder:helper.getLanguage('All Articles'),
    //     delimiter: '|',
    //     create: !1,
    //     onChange(value){
    //         $scope.searchThing();
    //     },
    //     maxItems: 1
    // };

    $scope.showStatsAll=function(){
        $scope.listObj.expand_all=!$scope.listObj.expand_all;
        if($scope.listObj.expand_all){
            angular.element('.mainTable thead tr').find('.fa-chevron-right').addClass('fa-chevron-down');
            angular.element('.mainTable tbody tr.collapseTr').find('.fa-chevron-right').addClass('fa-chevron-down');
            let articlesAr=[];
            for(let x in $scope.list){
                $scope.list[x].isopen=!0;
                if(!$scope.listStats[x]){
                    $scope.listStats[x] = {};
                    articlesAr.push($scope.list[x].article_id);
                }
            }
            if(articlesAr.length>0){
                helper.doRequest('post','index.php',{'do':'order-articles_to_deliver','xget':'orders_list','article_ids':articlesAr}, function(r) {
                    if (r.data) {
                        for(let x in r.data){
                            for(let y in $scope.list){
                                if(parseInt(x) == parseInt($scope.list[y].article_id)){
                                    $scope.listStats[y]=r.data[x];
                                }
                            }
                        }
                    }
                })
            }
        }else{
            for(let x in $scope.list){
                $scope.list[x].isopen=!1;
            }
            angular.element('.mainTable thead tr').find('.fa-chevron-right').removeClass('fa-chevron-down');
            angular.element('.mainTable tbody tr.collapseTr').find('.fa-chevron-right').removeClass('fa-chevron-down');
        }

    }

    $scope.showStats = function(item, $index) {
        var add = !1;
        if (!$scope.listStats[$index]) {
            $scope.listStats[$index] = {};
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        let articlesAr=[];
        articlesAr.push(item.article_id);
        if (add == !0) {
            helper.doRequest('post','index.php',{'do':'order-articles_to_deliver','xget':'orders_list','article_ids':articlesAr}, function(r) {
                if (r.data) {
                    $scope.listStats[$index] = r.data[item.article_id];
                }
            })
        }
        angular.element('.mainTable tbody tr.collapseTr').eq($index).find('.fa-chevron-right').toggleClass('fa-chevron-down');
    }

    list.init($scope, 'order-articles_to_deliver', $scope.renderList);

}]).controller('OrdersDeliveriesCtrl', ['$scope', '$window','helper', 'list', function($scope, $window,helper, list) {
    console.log('Orders deliveries list');

    $scope.archiveList = 1;
    $scope.reverse = !0;
    $scope.ord="delivery_date";
    $scope.max_rows = 0;
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'order-deliveries',
        view: 0,
        xget: '',
        showAdvanced: !1,
        offset: 1,
        'item_code':'',
        'start_date':undefined,
        'stop_date':undefined
    };
    $scope.listObj = {
        check_add_all: !1
    };

    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1,
        stop_date: !1,
        d_start_date: !1,
        d_stop_date: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };

    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = parseInt(d.data.max_rows);
            $scope.lr = parseInt(d.data.lr);
        }
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }

    $scope.exportDeliveries=function(){
        var data =angular.copy($scope.search);
        delete data.do;
        delete data.xget;
        $window.open('index.php?do=order-export_selected_deliveries&export_data='+encodeURI(btoa(JSON.stringify(data))),'_blank');
    }

    list.init($scope, 'order-deliveries', $scope.renderList);

}]).controller('purchasePriceOrderModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'selectizeFc',function($scope, helper, $uibModalInstance, data,selectizeFc) {
    $scope.obj = data;
    $scope.ok = function() {
        var newItem={};
        newItem.purchase_price_other_currency_id=$scope.obj.currency_id;
        newItem.tr_id=$scope.obj.tr_id;
        if($scope.obj.currency_id != undefined && $scope.obj.currency_id != '' && $scope.obj.currency_id != $scope.obj.default_currency_id){
            newItem.purchase_price=$scope.obj.purchase_price_new;
            newItem.purchase_price_other_currency=$scope.obj.purchase_price;
        }else{
            newItem.purchase_price=$scope.obj.purchase_price;           
            newItem.purchase_price_other_currency=helper.displayNr(0);
        }
        $scope.closeModal(newItem);
    };
    $scope.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }

    $scope.currencyCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
           
            if(value != undefined && value != '' && value == $scope.obj.default_currency_id){
                //$scope.obj.purchase_price_new=helper.displayNr(0);
                $scope.obj.purchase_price_new=$scope.obj.purchase_price;
                
            }else if(value != undefined && value != '' && value != $scope.obj.default_currency_id){
                var obj={
                    'do': 'misc--misc-getExchangeRate',
                    purchase_price: helper.return_value($scope.obj.purchase_price),
                    currency_id_to_change: value,
                    currency_id_base: $scope.obj.default_currency_id
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    if(r.data){
                        $scope.obj.purchase_price_new=helper.displayNr(r.data.purchase_price_new);
                    }
                    
                })
            }
        },
        maxItems: 1
    });

    $scope.setPurchasePrice=function(){
        if($scope.obj.currency_id != undefined && $scope.obj.currency_id != '' && $scope.obj.currency_id != $scope.obj.default_currency_id){
            var obj={
                'do': 'misc--misc-getExchangeRate',
                purchase_price: helper.return_value($scope.obj.purchase_price),
                currency_id_to_change: $scope.obj.currency_id,
                currency_id_base: $scope.obj.default_currency_id
            };
            helper.doRequest('get', 'index.php', obj, function(r) {
                if(r.data){
                    $scope.obj.purchase_price_new=helper.displayNr(r.data.purchase_price_new);
                }
                
            })
        }
    }

}]).controller('CreatePoBulkModalCtrl', ['$scope','$confirm', '$timeout','helper', '$uibModalInstance', 'data', function($scope, $confirm, $timeout, helper, $uibModalInstance, data) {
    var vm = this;
    var typePromise;
    vm.data=data;
    vm.index = 0;

    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }

    vm.filterCust = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Supplier'),
        create: !1,
        onDropdownOpen($dropdown) {
            var element_data = angular.element($dropdown.closest('tr'));
            vm.index = element_data[0].rowIndex
        },
        onDropdownClose($dropdown) {
            if (vm.data.lines[vm.index - 1].supplier_id == '99999999999') {}
        },
        render: {
            option: function(item, escape) {
                var html = '<div class="row">' + '<div class="col-sm-12">' + '<p class="text-ellipsis">' + escape(item.name) + ' </p>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            var element_data = angular.element('#po_articles_list tbody tr:nth-child(' + vm.index + ') selectize');
            var selectize = element_data[0].selectize;
            if (value == undefined) {
                vm.data.lines[vm.index - 1].add_p_order = undefined;
                //angular.element('.loading_wrap').removeClass('hidden');
                var obj={
                    'do': 'po_order-add_bulk',
                    'xget': 'cc'
                };
                if(vm.data.order_id){
                    obj.order_id=vm.data.order_id;
                }
                helper.doRequest('get', 'index.php', obj, function(r) {
                    vm.data.lines[vm.index - 1].supplier_dd = r.data;
                    selectize.open()
                })
            } else {
                vm.data.lines[vm.index - 1].add_p_order = !0
            }
        },
        onType(value) {
            var element_data = angular.element('#po_articles_list tbody tr:nth-child(' + vm.index + ') selectize');
            var selectize = element_data[0].selectize;
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                if (value != undefined) {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    var obj={
                        'do': 'po_order-add_bulk',
                        'xget': 'cc',
                        'term': value
                    };
                    if(vm.data.order_id){
                        obj.order_id=vm.data.order_id;
                    }
                    helper.doRequest('get', 'index.php', obj, function(r) {
                        vm.data.lines[vm.index - 1].supplier_dd = r.data;
                        if (vm.data.lines[vm.index - 1].supplier_dd.length) {
                            selectize.open()
                        }
                    })
                }
            }, 300)
        },
        maxItems: 1
    };

    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d)
    }

    vm.ok=function() {
        var data={'list':[]};
        for(var x in vm.data.lines){
            if(vm.data.lines[x].supplier_id && vm.data.lines[x].supplier_id!='0'){
                data.list.push(vm.data.lines[x]);
            }
        }
        if(!data.list.length){
            $uibModalInstance.close();
            return false;
        }
        data.do = vm.data.do_next;
        data.order_id = vm.data.order_id;
        data.service_id = vm.data.service_id;
        data.project_id = vm.data.project_id;
        data.quote_id = vm.data.quote_id;
        data.add_to_purchase = vm.data.add_to_purchase;
        data.cost_centre = vm.data.cost_centre;
        data.type_id = vm.data.type_id;
        data.source_id = vm.data.source_id;
        data.segment_id = vm.data.segment_id;
        data.deliver_address=vm.data.deliver_address;
        if (vm.data.service_id || vm.data.project_id) {
            data.add_to_purchase = '1'
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.error) {
                vm.showAlerts(r)
            }
            if (r && r.success) {
                $uibModalInstance.close(true);
            }
        })
    }

}]).controller('customAddressModalCtrl', ['$scope','helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    
    $scope.obj=data;
    $scope.cfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 1
    };

    $scope.ok = ok;
    $scope.cancel = cancel;
    $scope.showAlerts = showAlerts;

    function ok() {
        if(!$scope.obj.country_id){
            return false;
        }
        var custom_addres="";
        if($scope.obj.address){
            custom_addres+=$scope.obj.address+"\n";
        }
        if($scope.obj.zip && $scope.obj.city){
            custom_addres+=$scope.obj.zip+" "+$scope.obj.city+"\n";
        }else if($scope.obj.zip && !$scope.obj.city){
            custom_addres+=$scope.obj.zip+"\n";
        }else if(!$scope.obj.zip && $scope.obj.city){
            custom_addres+=$scope.obj.city+"\n";
        }
        var country_name="";
        for(var x in $scope.obj.countries){
            if($scope.obj.countries[x].id == $scope.obj.country_id){
                country_name=$scope.obj.countries[x].name;
                break;
            }
        }
        custom_addres+=country_name;
        $uibModalInstance.close({'delivery_address':custom_addres.trim()});
    };

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    };

    function showAlerts(d) {
        helper.showAlerts(vmMod, d)
    }

}]).controller('CertificateModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'modalFc', 'selectizeFc', function($scope, helper, $uibModalInstance, data, modalFc, selectizeFc) {
   
   $scope.obj=data;
    $scope.cfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 1
    };

    $scope.delCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 1,
        onChange(value) {

            var obj={
                'do': 'order-certificate_line',
                'xget': 'lines',
                'order_id': $scope.obj.order_id,
                'delivery_id': $scope.obj.delivery_id,
            };

            helper.doRequest('get', 'index.php', obj, function(r) {
                $scope.obj.lines = r.data.lines;

            })
            
        }
    };
    

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
    $scope.ok = function() {

        if($scope.obj.lines.length>0){
            angular.element('.loading_alt').removeClass('hidden');
            var articles = [];
            for (x in $scope.obj.lines) {
                articles[x] = $scope.obj.lines[x].order_articles_id;
            }

           var data={
                'do': 'order-order-order-make_certificate',
                'order_id': $scope.obj.order_id,
                'delivery_id': $scope.obj.delivery_id,
                'order_articles_id': articles,
                'lid': $scope.obj.lang_id,
                'date_cert_line_h': $scope.obj.date_cert_line_h,
                'date_cert_line': $scope.obj.date_cert_line,
            };

            helper.doRequest('post', 'index.php', data, function(r) {
                
                if(r.success){                
                    $uibModalInstance.close(data);
                }

            }) 
        }else{
            console.log('no lines');
        }

        
    }
    $scope.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    $scope.removeLine = function($i) {
    
        if ($i > -1) {
            $scope.obj.lines.splice($i, 1);
        }
    }


}]);