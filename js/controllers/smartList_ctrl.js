angular.module('customer').controller('SmartListCtrl', ['$state', '$stateParams' ,'helper', 'list', 'data', 'modalFc', 'editItem',function($state, $stateParams, helper, list, data, modalFc, editItem) {
    console.log('Smart list');
    var ls = this;
    ls.ord = '';
    ls.reverse = !1;
    ls.max_rows = 0;
    ls.search = {
        search: '',
        'do': 'customers-smartList',
        xget: '',
        showAdvanced: !1,
        showList: !1,
        offset: 1,
        list_for: ($stateParams.isContact ? '0' : '1')
    };
    ls.app="general";
    ls.list = [];
    ls.toggleSearchButtons = function(item, val) {
        list.tSearch(ls, item, val, ls.renderPage)
    };
    ls.renderPage = function(d) {
        if (d && d.data) {
            for (x in d.data) {
                ls[x] = d.data[x]
            }
        }
    }
    ls.openModal = function(type, item, size) {
        var params = {
            template: 'cTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'SmartListAdd':
                params.callback = function(data) {
                    $state.go(data.state, {
                        contact_list_id: data.contact_list_id
                    })
                }
                break
        }
        modalFc.open(params)
    }
    ls.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search(ls.search, ls.renderPage, ls)
    }
    ls.filters = function(p) {
        list.filter(ls, p, ls.renderPage)
    }
    ls.action = function(item, action) {
        list.action(ls, item, action, ls.renderPage)
    };
    ls.orderBY = function(p) {
        list.orderBy(ls, p, ls.renderPage)
    };
    ls.showAlerts = function(d, formObj) {
        helper.showAlerts(ls, d, formObj)
    };

    ls.createCustomer=function(){
        var main_obj = {
            country_dd: [],
            country_id: 0,
            add_customer: !0,
            'do': 'misc--misc-tryAddC',
        };
        editItem.openModal(ls, 'AddCustomer', main_obj, 'md');
    };

    ls.createContact=function(){
        var main_obj = {
            country_dd: [],
            country_id: 0,
            add_contact: !0,
            contact_only: !0,
            'do': 'misc--misc-tryAddC',
        };
        editItem.openModal(ls, 'AddCustomer', main_obj, 'md');
    }

    helper.setItem('customers-smartList', {'search':ls.search}) 

    list.init(ls, 'customers-smartList', ls.renderPage)
}]).controller('SmartListAddModalCtrl', ['helper', '$uibModalInstance', function(helper, $uibModalInstance) {
    var vmMod = this;
    vmMod.obj = {
        list_type: 0,
        list_for: 0,
        do: 'customers--customers-list_add'
    }
    vmMod.cancel = function() {
        $uibModalInstance.dismiss()
    };
    vmMod.ok = function(formObj) {
        var data = angular.copy(vmMod.obj);
        formObj.$invalid = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.data) {
                if (r.error) {
                    vmMod.showAlerts(r, formObj)
                } else {
                    $uibModalInstance.close(r.data)
                }
            }
        })
    }
    vmMod.showAlerts = function(d, formObj) {
        helper.showAlerts(vmMod, d, formObj)
    }
}]).controller('SmartListEditCtrl', ['$state', '$stateParams', 'helper', 'list', 'data', function($state, $stateParams, helper, list, data) {
    console.log('Smart list');
    var ls = this;
    ls.contact_list_id = $stateParams.contact_list_id;
    ls.appliedFilters = {};
    ls.search = {
        search: '',
        'do': 'customers-smartListEdit',
        xget: 'smartListContacts',
        offset: 1,
        contact_list_id: ls.contact_list_id,
        filter: {}
    };
    ls.max_rows = 0;
    ls.renderPage = function(d, formObj) {
        if (d && d.data) {
            for (x in d.data) {
                ls[x] = d.data[x]
            }
            if (ls.search.filter == !1) {
                ls.search.filter = {}
            }
            if (ls.appliedFilters == !1) {
                ls.appliedFilters = {}
            }
            ls.showAlerts(d, formObj)
        }
    }
    ls.showAlerts = function(d, formObj) {
        helper.showAlerts(ls, d, formObj)
    };
    ls.searchThing = function() {
        list.search(ls.search, ls.renderPage, ls)
    }
    ls.applyFilter = function(e, item, ls) {
        e.stopPropagation();
        if (!ls.search.filter[item.key]) {
            ls.search.filter[item.key] = []
        }
        if (!ls.appliedFilters[item.key]) {
            ls.appliedFilters[item.key] = []
        }
        var ind = ls.search.filter[item.key].indexOf(item.id),
            indApply = ls.appliedFilters[item.key].indexOf(item.name);
        if (ind > -1) {
            ls.search.filter[item.key].splice(ind, 1)
        } else {
            ls.search.filter[item.key].push(item.id)
        }
        if (indApply > -1) {
            ls.appliedFilters[item.key].splice(indApply, 1)
        } else {
            ls.appliedFilters[item.key].push(item.name)
        }
        ls.searchThing()
    }
    ls.action = function(item) {
        var data = angular.copy(ls.search);
        data.do = 'customers-smartListEdit-customers-list_deactivate_c';
        data.id = item.REL;
        data.active = item.CHECKED; /*angular.element('.loading_wrap').removeClass('hidden');*/
        helper.doRequest('post', 'index.php', data, function(r) {
            ls.renderPage(r)
        })
    };
    ls.save = function(formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(ls);
        data = helper.clearData(data, ['save', 'applyFilter', 'searchThing', 'showAlerts', 'renderPage', 'appliedFilters', 'list', 'filters', 'search']);
        data.do = data.do_next;
        data.filter = ls.search.filter;
        helper.doRequest('post', 'index.php', data, function(r) {
            ls.renderPage(r, formObj)
        })
    }
    ls.removeAllFilters=function(){
        ls.search.filter={};
        ls.appliedFilters={};
        for(let x in ls.filters){
            ls.filters[x].show=false;
            for(let y in ls.filters[x].items){
                ls.filters[x].items[y].CHECKED=false;
            }
        }
        ls.searchThing();
    }

    ls.getItemParams=function(param,val){
        if(param == 'customer_id'){
            return {'customer_id':val};
        }else{  
            return {'contact_id':val};
        }
    }

    ls.cancel=function(){
        $state.go('smartListView',{"contact_list_id": $stateParams.contact_list_id});
    }

    ls.goBack=function(){
        $state.go((ls.list_for == '1' ? 'smartList' : 'smartListContact'));
    }

    ls.renderPage(data)
}]).controller('SmartListViewCtrl', ['$state', '$stateParams', 'helper', 'list', 'data', function($state, $stateParams, helper, list, data) {
    console.log('Smart list view');
    var ls = this;
    ls.contact_list_id = $stateParams.contact_list_id;
    ls.search = {
        search: '',
        'do': 'customers-smartListView',
        offset: 1,
        contact_list_id: ls.contact_list_id,
        withoutFilters: 1
    };
    ls.max_rows = 0;
    ls.listStats = [];
    ls.renderPage = function(d, formObj) {
        if (d && d.data) {
            for (x in d.data) {
                ls[x] = d.data[x]
            }
            ls.showAlerts(d, formObj)
        }
    }
    ls.showAlerts = function(d, formObj) {
        helper.showAlerts(ls, d, formObj)
    };
    ls.searchThing = function() {
        list.search(ls.search, ls.renderPage, ls)
    };
    ls.toggleSearchButtons = function(item, val) {
        list.tSearch(ls, item, val, ls.renderPage)
    };
    ls.action = function(item, action) {
        var item = {
            refresh_link: {
                'do': 'customers-smartListView-customers-refresh',
                'contact_list_id': ls.contact_list_id
            }
        };
        list.action(ls, item, action, ls.renderPage)
    };
    ls.showStats = function(item, $index) {
        var add = !1;
        if (!ls.listStats[$index]) {
            ls.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (add == !0) {
            var data = {
                xget: 'customerActivity',
                'do': 'customers-ViewCustomer'
            };
            if (ls.list_for == 0) {
                data.contact_id = item.REL
            }
            if (ls.list_for == 1) {
                data.customer_id = item.REL;
                data.filter_type = 'customer'
            }
            helper.doRequest('post', 'index.php', data, function(r) {
                if (r.data) {
                    ls.listStats[$index] = r.data
                }
            })
        }
        item.isopen = !1
    }
    ls.renderPage(data)
}]).controller('SmartListAddCtrl', ['$state', '$stateParams', 'helper', 'list', 'data', function($state, $stateParams, helper, list, data) {
    console.log('Smart list Add');
    var ls = this;
    ls.appliedFilters = {};
    ls.search = {
        search: '',
        'do': 'customers-smartListAdd',
        xget: 'smartListContacts',
        offset: 1,
        filter: {},
        list_type: 0,
        list_for: $stateParams.list_for
    };
    ls.max_rows = 0;
    ls.renderPage = function(d, formObj) {
        if (d && d.data) {
            for (x in d.data) {
                ls[x] = d.data[x]
            }
            if (ls.search.filter == !1) {
                ls.search.filter = {}
            }
            if (ls.appliedFilters == !1) {
                ls.appliedFilters = {}
            }
            ls.showAlerts(d, formObj)
        }
    }
    ls.showAlerts = function(d, formObj) {
        helper.showAlerts(ls, d, formObj)
    };
    ls.searchThing = function() {
        list.search(ls.search, ls.renderPage, ls)
    }
    ls.applyFilter = function(e, item, ls) {
        e.stopPropagation();
        if (!ls.search.filter[item.key]) {
            ls.search.filter[item.key] = []
        }
        if (!ls.appliedFilters[item.key]) {
            ls.appliedFilters[item.key] = []
        }
        var ind = ls.search.filter[item.key].indexOf(item.id),
            indApply = ls.appliedFilters[item.key].indexOf(item.name);
        if (ind > -1) {
            ls.search.filter[item.key].splice(ind, 1)
        } else {
            ls.search.filter[item.key].push(item.id)
        }
        if (indApply > -1) {
            ls.appliedFilters[item.key].splice(indApply, 1)
        } else {
            ls.appliedFilters[item.key].push(item.name)
        }
        ls.searchThing()
    }
    ls.action = function(item) {
        var data = angular.copy(ls.search);
        data.do = 'customers-smartListAdd-customers-list_deactivate_c';
        data.id = item.REL;
        data.active = item.CHECKED; /*angular.element('.loading_wrap').removeClass('hidden');*/
        helper.doRequest('post', 'index.php', data, function(r) {
            ls.renderPage(r)
        })
    };
    ls.save = function(formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(ls);
        data.list_for=ls.search.list_for;
        data.list_type=ls.search.list_type;
        data = helper.clearData(data, ['save', 'applyFilter', 'searchThing', 'showAlerts', 'renderPage', 'appliedFilters', 'list', 'filters', 'search']);
        data.do = data.do_next;
        data.filter = ls.search.filter;
        helper.doRequest('post', 'index.php', data, function(r) {
            if(r.success){
                $state.go('smartListEdit',{'contact_list_id':r.data.contact_list_id});
            }else{
                ls.renderPage(r, formObj)
            }
            
        })
    }
    ls.removeAllFilters=function(){
        ls.search.filter={};
        ls.appliedFilters={};
        for(let x in ls.filters){
            ls.filters[x].show=false;
            for(let y in ls.filters[x].items){
                ls.filters[x].items[y].CHECKED=false;
            }
        }
        ls.searchThing();
    }

    ls.getItemParams=function(param,val){
        if(param == 'customer_id'){
            return {'customer_id':val};
        }else{  
            return {'contact_id':val};
        }
    }

    helper.doRequest('get', 'index.php', {
        'do': 'customers-smartListEdit',
        'xget': ($stateParams.list_for == '1' ? 'customersFilters' :'contactsFilters'), 
        'list_for':$stateParams.list_for,
        'list_type':'0'
    }, function(r) {
        for(let x in r.data){
            ls[x]=r.data[x];
        }
    })

    ls.cancel=function(){
        $state.go(($stateParams.list_for == '1' ? 'smartList' : 'smartListContact'));
    }

    ls.goBack=function(){
        $state.go(($stateParams.list_for == '1' ? 'smartList' : 'smartListContact'));
    }

    ls.renderPage(data)
}])