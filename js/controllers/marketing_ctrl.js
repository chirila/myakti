angular.module('customer').controller('MarketingCtrl', ['helper', 'data', function(helper, data) {
    var ls = this;
    for (x in data.data) {
        ls[x] = data.data[x]
    }
    ls.showAlerts = function(d, formObj) {
        helper.showAlerts(ls, d, formObj)
    }
}]).controller('CampaignReportsCtrl', ['helper', 'modalFc', 'data', function(helper, modalFc, data) {
    var ls = this;
    ls.listStats = [];
    for (x in data.data) {
        ls[x] = data.data[x]
    }
    ls.showStats = function(item, $index) {
        var add = !1;
        if (!ls.listStats[$index]) {
            ls.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (add == !0) {
            helper.doRequest('get', 'index.php?do=customers-campaignReports&xget=xcampaign2&campaign_id=' + item.CAMPAIGN_ID, function(r) {
                if (r.data) {
                    ls.listStats[$index] = r.data
                }
            })
        }
    }
    ls.openModal = function(type, item, size) {
        var params = {
            template: 'cTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'RecipientActivity':
                params.params = {
                    'do': 'customers-campaignReports',
                    'xget': 'recipient_activity2',
                    campaign_id: item.campaign_id,
                    opened: item.opened
                };
                break
        }
        modalFc.open(params)
    }
}]).controller('RecipientActivityModalCtrl', ['helper', 'modalFc', 'data', '$uibModalInstance', function(helper, modalFc, data, $uibModalInstance) {
    var vmMod = this;
    for (x in data) {
        vmMod[x] = data[x]
    }
    vmMod.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('CampaignsCtrl', ['helper', 'modalFc', '$stateParams', 'list', 'data', function(helper, modalFc, $stateParams, list, data) {
    var ls = this;
    ls.app = $stateParams.app;
    ls.loadedCampaigns = !1;
    ls.listStats = [];
    ls.renderPage = function(data) {
        if (data && data.data) {
            for (x in data.data) {
                ls[x] = data.data[x]
            }
            ls.showAlerts(data)
        }
    };
    ls.renderPage(data);
    ls.action = function(item, action) {
        list.action(ls, item, action, ls.renderPage)
    };
    ls.showAlerts = function(d, formObj) {
        helper.showAlerts(ls, d, formObj)
    };
    ls.openModal = function(type, item, size) {
        //angular.element('.loading_wrap').removeClass('hidden');
        var params = {
            template: 'cTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'CampaignEditList':
                params.params = {
                    'do': 'customers-campaign',
                    'xget': type,
                    list_id: item.LIST_ID,
                    app: ls.app
                };
                params.callback = function(data) {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('post', 'index.php', {
                        do: 'customers-campaign-' + ls.model + '-refresh',
                        xget: ls.app,
                        list_id: item.LIST_ID
                    }, ls.renderPage)
                }
                break;
            case 'RecipientActivity':
                params.params = {
                    'do': 'customers-campaign',
                    'xget': 'recipient_activity' + ls.app,
                    campaign_id: item.campaign_id,
                    opened: item.opened
                };
                break
        }
        modalFc.open(params)
    }
    ls.loadCampaigns = function() {
        if (!ls.loadedCampaigns) {
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php?do=customers-campaign&xget=' + ls.app + 'Campaigns', ls.renderPage)
        }
    }
    ls.showStats = function(item, $index) {
        var add = !1;
        if (!ls.listStats[$index]) {
            ls.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (add == !0) {
            helper.doRequest('get', 'index.php?do=customers-campaignReports&xget=xcampaign' + ls.app + '&campaign_id=' + item.campaign_id, function(r) {
                if (r.data) {
                    ls.listStats[$index] = r.data
                }
            })
        }
    }
    //angular.element('.loading_wrap').removeClass('hidden');
    helper.doRequest('get', 'index.php?do=customers-campaign&xget=' + ls.app, ls.renderPage)
}]).controller('CampaignEditListModalCtrl', ['helper', 'modalFc', 'data', '$uibModalInstance', function(helper, modalFc, data, $uibModalInstance) {
    var vmMod = this,
        contacts = [],
        increment = 0;
    for (x in data) {
        vmMod[x] = data[x]
    }
    vmMod.total = 0;
    vmMod.width = 0;
    vmMod.widthpr = 0;
    vmMod.increment = 0;
    vmMod.importContacts = !1;
    vmMod.importedContacts = 0;
    vmMod.importAlerts = [];
    vmMod.cancel = function() {
        $uibModalInstance.close()
    };
    vmMod.ok = function() {
        contacts = [];
        vmMod.total = 0;
        vmMod.width = 0;
        vmMod.importedContacts = 0;
        increment = 0;
        vmMod.importContacts = !0;
        helper.doRequest('post', 'index.php', {
            'do': 'customers-campaign-' + vmMod.app + '-add_subscriber',
            list_id: vmMod.list_id,
            lists_id: vmMod.lists_id,
            client_id: vmMod.client_id
        }, function(r) {
            contacts = r.data.contacts_ids;
            vmMod.total = contacts.length;
            vmMod.widthpr = 100 / vmMod.total;
            vmMod.subscribe(contacts)
        })
    }
    vmMod.subscribe = function(data) {
        if (data[0]) {
            increment++;
            helper.doRequest('post', 'index.php', {
                'do': 'customers--' + vmMod.app + '-insert_subscriber',
                list_id: vmMod.list_id,
                lists_id: vmMod.lists_id,
                client_id: vmMod.client_id,
                contact_id: data[0]
            }, function(r) {
                vmMod.width = vmMod.widthpr * increment;
                data.shift();
                if (r) {
                    if (r.success) {
                        vmMod.importedContacts++
                    }
                    if (r.error) {
                        vmMod.importAlerts.push({
                            type: 'danger',
                            msg: r.error.error
                        })
                    }
                }
                vmMod.subscribe(data)
            })
        } else {
            vmMod.alerts = vmMod.importAlerts;
            vmMod.alerts.unshift({
                type: 'info',
                msg: helper.getLanguage('Import complete. You can close this window')
            })
        }
    }
}])