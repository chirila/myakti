app.controller('contractsCtrl', ['$scope', '$compile', '$confirm', 'helper', 'list', 'selectizeFc', 'data', 'modalFc', function($scope, $compile, $confirm, helper, list, selectizeFc, data, modalFc) {
    console.log("Contracts");
    $scope.list = [];
    $scope.nav = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'contract-contracts',
        view: 0,
        xget: '',
        offset: 1
    };
    $scope.ord = '';
    $scope.reverse = !1;
    // $scope.views = [{
    //     name: helper.getLanguage('All Statuses'),
    //     id: 0,
    //     active: 'active'
    // }, {
    //     name: helper.getLanguage('Draft'),
    //     id: 1,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Sent'),
    //     id: 2,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Rejected'),
    //     id: 3,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Active'),
    //     id: 4,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Closed'),
    //     id: 5,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Archived'),
    //     id: -1
    // }];
    $scope.activeView = 0;
    $scope.show_load = !0;
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.post_active = d.data.post_active;
            $scope.listStats = [];
            $scope.view_dd = d.data.view_dd;
            $scope.nav = d.data.nav;
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['contract_id']);
                count ++;
            }
            sessionStorage.setItem('contract.view_total', count);
            sessionStorage.setItem('contract.view_list', JSON.stringify(ids));
            $scope.showAlerts(d)
        }
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.filterCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('All Statuses'),
        onChange(value) {
            $scope.toggleSearchButtons('view', value)
        }
    });
    // $scope.filterCfg = {
    //     valueField: 'id',
    //     labelField: 'name',
    //     searchField: ['name'],
    //     delimiter: '|',
    //     placeholder: helper.getLanguage('All Statuses'),
    //     create: !1,
    //     onItemAdd(value, $item) {
    //         if (value == undefined) {
    //             value = 0
    //         }
    //         $scope.search.archived = 0;
    //         if (value == -1) {
    //             $scope.search.archived = 1
    //         }
    //         $scope.filters(value)
    //     },
    //     maxItems: 1
    // };
    $scope.listMore = function(index) {
        var add = !1;
        if (!$scope.listStats[index]) {
            $scope.listStats[index] = [];
            add = !0
        }
        if (add == !0) {
            var data = {};
            data.contract_id = $scope.list[index].contract_id;
            data.do = 'contract-contracts';
            data.xget = 'subList';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', data, function(r) {
                $scope.listStats[index] = r.data.list;
                showStats(index)
            })
        } else {
            showStats(index)
        }
    }

    function showStats(index) {
var new_line = '<tr class="versionInfo"><td colspan="8" class="no_border">' + '<table class="table">' + '<thead>' + '<tr>' + '<th class="col-sm-2 expand_row">' + helper.getLanguage('Version') + '</th>' + '<th class="col-sm-2">' + helper.getLanguage('Start Date') + '</th>' + '<th class="col-sm-2">' + helper.getLanguage('End Date') + '</th>' + '<th class="col-sm-2"></th>' + '<th class="col-sm-3"></th>' + '<th class="col-sm-1">' + helper.getLanguage('Amount') + '</th>' + '<th class="col-sm-1 text-center col-status">' + helper.getLanguage('Status') + '</th>' + '<th class="col-sm-1"></th>' + '</tr>' + '</thead>' + '<tbody>' + '<tr ng-repeat="item2 in listStats[' + index + ']">' + '<td class="expand_row" ui-sref="contract.view({\'contract_id\': item2.contract_id,\'version_id\': item2.version_id})" list="{ \'type\':\'contract-contracts\', \'obj\': { \'search\':search } }"><span>{{item2.version}}</span></td>' + '<td ui-sref="contract.view({\'contract_id\': item2.contract_id,\'version_id\': item2.version_id})" list="{ \'type\':\'contract-contracts\', \'obj\': { \'search\':search } }"><span>{{item2.CONTRACT_DATE}}</span></td>' + '<td ui-sref="contract.view({\'contract_id\': item2.contract_id,\'version_id\': item2.version_id})" list="{ \'type\':\'contract-contracts\', \'obj\': { \'search\':search } }"><span>{{item2.CONTRACT_END_DATE}}</span></td>' + '<td ui-sref="contract.view({\'contract_id\': item2.contract_id,\'version_id\': item2.version_id})" list="{ \'type\':\'contract-contracts\', \'obj\': { \'search\':search } }"></td>' + '<td ui-sref="contract.view({\'contract_id\': item2.contract_id,\'version_id\': item2.version_id})" list="{ \'type\':\'contract-contracts\', \'obj\': { \'search\':search } }"></td>' + '<td ui-sref="contract.view({\'contract_id\': item2.contract_id,\'version_id\': item2.version_id})" list="{ \'type\':\'contract-contracts\', \'obj\': { \'search\':search } }"><span ng-bind-html="item2.amount"></span></td>' + '<td ui-sref="contract.view({\'contract_id\': item2.contract_id,\'version_id\': item2.version_id})" list="{ \'type\':\'contract-contracts\', \'obj\': { \'search\':search } }"><span class="badge item-status-gray" ng-class="{ \'item-status-orange\':item2.sent==\'1\' && item2.status_customer==\'0\', \'item-status-green\':item2.sent==\'1\' && item2.status_customer==\'2\', \'item-status-yellow\':item2.status_customer==\'3\', \'item-status-red\':item2.sent==\'1\' && item2.status_customer==\'1\' }">{{item2.status}}</span></td>' + '<td class="table_actions"><div class="btn-group">' + '<button type="button" class="btn btn-plain dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>' + '<ul class="dropdown-menu dropdown-menu-right" role="menu">' + '<li class="dropdown-header">' + helper.getLanguage('Action') + '</li>' + '<li><a href="" ng-href="{{item2.pdf_link}}" target="_blank" ><span class="glyphicon glyphicon-print text-primary"></span> ' + helper.getLanguage('PDF') + '</a></li>' + '<li ng-if="list[' + index + '].sent==\'0\' && list[' + index + '].active==\'1\'"><a href="#" ui-sref="contract.edit({\'contract_id\': item2.contract_id,\'version_id\': item2.version_id})" list="{ \'type\':\'contract-contracts\', \'obj\': { \'search\':search } }"><span class="glyphicon glyphicon-edit text-primary"></span> ' + helper.getLanguage('Edit') + '</a></li>' + '<li ng-if="list[' + index + '].sent==\'0\' && list[' + index + '].active==\'0\'"><a href="#" ng-click="deleteSublistItem(' + index + ',$index)" confirm="' + helper.getLanguage('Are you sure, you wanna delete this entry?') + '"><span class="glyphicon glyphicon-remove text-primary"></span> ' + helper.getLanguage('Delete') + '</a></li>' + '<li ng-if="!item2.post_order"><a href="#" ng-click="greenpost(item2)"><img width="16" height="16" alt="" src="images/postgreen_icon.png"> ' + helper.getLanguage('Send by Post') + '</a></li>' + '<li ng-if="item2.post_order"><a href="#" ng-click="resendPost(item2)"><img width="16" height="16" alt="" src="images/postgreen_icon.png"> ' + helper.getLanguage('Post status') + '</a></li>' + '</ul></div></td>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>';
        angular.element('.orderTable tbody tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.orderTable tbody tr.collapseTr').eq(index).next())($scope);
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').addClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').removeClass('hidden')
    }
    $scope.listLess = function(index) {
        angular.element('.orderTable tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').removeClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').addClass('hidden')
    }
    $scope.deleteSublistItem = function(list_index, index) {
        var obj = angular.copy($scope.listStats[list_index][index].DELETE_LINK);
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.listStats[list_index].splice(index, 1)
            }
        })
    }
    $scope.greenpost = function(item) {
        if ($scope.post_active) {
            var data = {};
            for (x in $scope.search) {
                data[x] = $scope.search[x]
            }
            data.do = 'contract-contracts-contract-postIt';
            data.id = item.contract_id;
            data.version_id = item.version_id;
            data.related = 'list';
            data.no_status = !0;
            data.post_library = $scope.post_library;
            openModal("resend_post", data, "md")
        } else {
            var data = {};
            data.clicked_item = item;
            for (x in $scope.search) {
                data[x] = $scope.search[x]
            }
            openModal("post_register", data, "md")
        }
    }
    $scope.resendPost = function(item) {
        var data = {};
        for (x in $scope.search) {
            data[x] = $scope.search[x]
        }
        data.do = 'contract-contracts-contract-postIt';
        data.id = item.contract_id;
        data.version_id = item.version_id;
        data.related = 'list';
        data.post_library = $scope.post_library;
        openModal("resend_post", data, "md")
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'post_register':
                var iTemplate = 'PostRegister';
                var ctas = 'post';
                break;
            case 'resend_post':
                var iTemplate = 'ResendPostGreen';
                var ctas = 'post';
                break
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'post_register':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.item.do = 'contract-contracts-contract-postRegister';
                params.callback = function(data) {
                    if (data && data.data) {
                        $scope.renderList(data);
                        $scope.greenpost(data.clicked_item)
                    }
                    $scope.showAlerts(data)
                }
                break;
            case 'resend_post':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.request_type = 'post';
                params.params = {
                    'do': 'contract--contract-postContractData',
                    'contract_id': item.contract_id,
                    'version_id': item.version_id,
                    'related': 'status',
                    'old_obj': item
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.renderList(d);
                        $scope.showAlerts(d)
                    }
                }
                break
        }
        modalFc.open(params)
    }
    list.init($scope, 'contract-contracts', $scope.renderList);
    $scope.show_load = !0;
    helper.doRequest('get', 'index.php', {
        'do': 'misc-post_library'
    }, function(r) {
        $scope.show_load = !1;
        $scope.post_library = r.data.post_library
    })
}]).controller('contractCtrl', ['$scope', 'helper', function($scope, helper) {
    var vm = this;
    vm.serial_number = '';
    vm.isAddPage = !0
}]).controller('contractEditCtrl', ['$scope', '$state', '$stateParams', '$timeout', 'helper', 'data', 'modalFc', 'selectizeFc', 'editItem', '$confirm',function($scope, $state, $stateParams, $timeout, helper, data, modalFc, selectizeFc, editItem, $confirm) {
    console.log("Contract Edit");
    var edit = this,
        typePromise;
    edit.app = 'contract';
    edit.openContent = !1;
    edit.ctrlpag = 'contract';
    edit.tmpl = 'conTemplate';
    edit.item_id = $stateParams.contract_id;
    edit.contract_id = $stateParams.contract_id;
    edit.item = {};
    edit.oldObj = {};
    edit.item.validity_date = new Date();
    edit.removedCustomer = !1;
    //edit.item.add_customer = (edit.item_id != 'tmp' ? true : !1);
    edit.item.add_customer = !1;
    edit.quote_group_active = 0;
    edit.originalArticleList = undefined;
    edit.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            edit.changeDiscount()
        }
    });
    edit.vatregcfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onDropdownOpen($dropdown) {
            var _this = this;
            edit.item.old_vat_regime_id=_this.$input[0].attributes.value.value;
        },
        onChange(value) {
            if (value) {
                var selectize = angular.element('#vat_regime_select')[0].selectize;
                selectize.blur();
                var _this = this;
                $confirm({
                        ok: helper.getLanguage('Yes'),
                        cancel: helper.getLanguage('No'),
                        text: helper.getLanguage('Apply VAT Regime on all lines')
                    }).then(function() {
                        if (_this.options[value].no_vat) {
                            edit.item.remove_vat = !0;
                            edit.item.show_vat = !1;
                            edit.item.block_vat = !1;
                            edit.item.vat_regular = !1;
                            edit.item.vat = helper.displayNr(0);                        
                        } else {
                            if (_this.options[value].regime_type == '2') {
                                edit.item.vat = helper.displayNr(0);
                                edit.item.block_vat = !0;
                            } else if (_this.options[value].regime_type == '3') {
                                edit.item.vat = helper.displayNr(_this.options[value].value);
                                edit.item.block_vat = _this.options[value].regime_id == '4' ? !0 : !1;
                            } else {
                                edit.item.vat = helper.displayNr(_this.options[value].value);
                                edit.item.block_vat = !1
                            }
                            if (_this.options[value].regime_type == '1') {
                                edit.item.vat_regular = !0
                            } else {
                                edit.item.vat_regular = !1
                            }
                            edit.item.show_vat = !0;
                            edit.item.remove_vat = !1
                        }
                        edit.changeLang();
                        edit.vatApplyAll();
                        var data = {
                            'do': 'contract-contract',
                            xget: 'articles_list',
                            buyer_id: edit.item.buyer_id,
                            lang_id: edit.item.email_language,
                            cat_id: edit.item.cat_id,
                            remove_vat: (edit.item.show_vat ? '0' : '1'),
                            vat_regime_id: edit.item.vat_regime_id
                        };
                        edit.requestAuto(edit, data, edit.renderArticleListAuto)
                    }, function(e) {
                        if(e == 'cancel'){
                            if (_this.options[value].no_vat) {
                                edit.item.remove_vat = !0;
                                edit.item.show_vat = !1;
                                edit.item.block_vat = !1;
                                edit.item.vat_regular = !1;
                                edit.item.vat = helper.displayNr(0);
                                edit.vatApplyAll()
                            } else {
                                if (_this.options[value].regime_type == '2') {
                                    edit.item.vat = helper.displayNr(0);
                                    edit.item.block_vat = !0;
                                    edit.vatApplyAll()
                                } else if (_this.options[value].regime_type == '3') {
                                    edit.item.vat = helper.displayNr(_this.options[value].value);
                                    edit.item.block_vat = _this.options[value].regime_id == '4' ? !0 : !1;
                                    edit.vatApplyAll()
                                } else {
                                    edit.item.vat = helper.displayNr(_this.options[value].value);
                                    edit.item.block_vat = !1
                                }
                                if (_this.options[value].regime_type == '1') {
                                    edit.item.vat_regular = !0
                                } else {
                                    edit.item.vat_regular = !1
                                }
                                edit.item.show_vat = !0;
                                edit.item.remove_vat = !1
                            }
                            edit.changeLang();
                            var data = {
                                'do': 'contract-contract',
                                xget: 'articles_list',
                                buyer_id: edit.item.buyer_id,
                                lang_id: edit.item.email_language,
                                cat_id: edit.item.cat_id,
                                remove_vat: (edit.item.show_vat ? '0' : '1'),
                                vat_regime_id: edit.item.vat_regime_id
                            };
                            edit.requestAuto(edit, data, edit.renderArticleListAuto)
                        }else if(e == 'dismiss'){
                            selectize.addItem(edit.item.old_vat_regime_id, true);
                        }                 
                    });          
            } else {
                edit.item.block_vat = !1;
                edit.item.vat_regular = !0;
                var data = {
                    'do': 'contract-contract',
                    xget: 'articles_list',
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id
                };
                edit.requestAuto(edit, data, edit.renderArticleListAuto)
            }         
        }
    });
    edit.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    edit.linevatcfg = {
        valueField: 'vat',
        labelField: 'name',
        searchField: ['vat'],
        delimiter: '|',
        placeholder: helper.getLanguage('VAT'),
        create: !1,
        maxOptions: 100,
        onDelete(values) {
            return !1
        },
        closeAfterSelect: !0,
        maxItems: 1
    };
    edit.dealAutoCfg = {
        valueField: 'deal_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Deal'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.name) + ' </span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        maxItems: 1
    };
    edit.showTxt = {
        contract_settings: !1,
        addBtn: !0,
        address: !1,
        addArticleBtn: !1,
        notes: !1,
    }
    edit.contactAddObj = {};
    edit.auto = {
        options: {
            contacts: [],
            addresses: [],
            cc: [],
            articlesList: []
        }
    };
    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {
        validity_date: !1,
        start_date: !1,
        end_date: !1,
        reminder_date: !1,
    }
    edit.cancel = function() {
        $state.go('contracts')
    }
    edit.articlesListAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code', 'supplier_reference', 'family'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.family) {
                    item.family = "/ " + item.family
                }
                if(item.allow_stock && item.is_service == 0){
                    var stock_display = '';
                    if (item.hide_stock == 0) {
                        stock_display = (item.base_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                    }
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-right">' + stock_display + '</div>' + '</div>';
                }else{
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-right">' + (item.base_price) + '</div>' + '</div>' + '</div>'            
                }               
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> Create a new item</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'contract-contract',
                    xget: 'articles_list',
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id
                };
                edit.requestAuto(edit, data, edit.renderArticleListAuto)
            }, 300)
        },
        onChange(value) {
            if (value == undefined) {} else if (value == '99999999999') {
                edit.openModal(edit, 'AddAnArticle', {
                    'do': 'misc-addAnArticle',
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id
                }, 'md');
                return !1
            } else {
                var collection = edit.auto.options.articles_list;
                for (x in collection) {
                    if (collection[x].article_id == value) {
                        editItem.addQuoteArticle(edit, collection[x]);
                        edit.selected_article_id = '';
                        edit.showTxt.addArticleBtn = !1;
                        var data = {
                            'do': 'contract-contract',
                            xget: 'articles_list',
                            buyer_id: edit.item.buyer_id,
                            lang_id: edit.item.email_language,
                            cat_id: edit.item.cat_id,
                            remove_vat: (edit.item.show_vat ? '0' : '1'),
                            vat_regime_id: edit.item.vat_regime_id
                        };
                        edit.requestAuto(edit, data, edit.renderArticleListAuto);
                        break
                    }
                }
            }
        },
        onItemAdd(value, $item) {
            if (value == '99999999999') {
                edit.selected_article_id = ''
            }
        },
        onBlur() {
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'contract-contract',
                    xget: 'articles_list',
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id
                };
                edit.showTxt.addArticleBtn = !1;
                if (edit.originalArticleList === undefined) {
                    edit.requestAuto(edit, data, edit.renderArticleListAuto)
                } else {
                    edit.auto.options.articles_list = edit.originalArticleList
                }
            }, 300)
        },
        maxItems: 1
    };
    edit.renderPage = renderPage;
    edit.openDate = openDate;
    edit.removeDeliveryAddr = removeDeliveryAddr;
    edit.save = save;
    edit.removeLine = removeLine;
    edit.addArticle = addArticle;
    edit.calc = calc;
    edit.changeDiscount = changeDiscount;
    edit.vatApplyAll = vatApplyAll;
    edit.applyLineDiscount = applyLineDiscount;
    edit.showhide = showhide;
    edit.changeLang = changeLang;
    edit.delaySave = delaySave;
    editItem.init(edit);

    function renderPage(d) {
        if (d && d.data) {
            if (d.data.redirect) {
                $state.go(d.data.redirect);
                return !1
            }
            edit.item = d.data;
            if (edit.item_id == 'tmp' && d.data.contract_id && d.data.contract_id != 'tmp') {
                edit.item_id = d.data.contract_id;
                edit.contract_id = edit.item_id;
                $state.go('contract.edit', {
                    contract_id: edit.item_id,
                    version_id:d.data.version_id
                }, {inherit: false})
            }
            if (edit.item_id != 'tmp') {
                $scope.vm.serial_number = edit.item.serial_number;
                $scope.vm.version_code = edit.item.CURRENT_VERSION;
                helper.updateTitle($state.current.name,edit.item.serial_number+' '+edit.item.CURRENT_VERSION);
            }
            if (edit.item.validity_date) {
                edit.item.validity_date = new Date(edit.item.validity_date)
            }
            if (edit.item.start_date) {
                edit.item.start_date = new Date(edit.item.start_date)
            }
            if (edit.item.end_date) {
                edit.item.end_date = new Date(edit.item.end_date)
            }
            if (edit.item.reminder_date) {
                edit.item.reminder_date = new Date(edit.item.reminder_date)
            }
            if (edit.item.contract_version_date) {
                edit.item.contract_version_date = new Date(edit.item.contract_version_date)
            }
            edit.auto.options.contacts = d.data.contacts;
            edit.auto.options.addresses = d.data.addresses;
            edit.auto.options.cc = d.data.cc;
            edit.auto.options.articles_list = d.data.articles_list && d.data.articles_list.lines ? d.data.articles_list.lines : [];
            if (edit.originalArticleList === undefined) {
                edit.originalArticleList = edit.auto.options.articles_list
            }
            edit.contactAddObj = {
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                add_contact: !0,
                buyer_id: edit.item.buyer_id,
                article: edit.item.tr_id,
                'do': 'contract-contract-contract-tryAddC',
                identity_id: edit.item.identity_id,
                contact_only: !0,
                item_id: edit.item_id
            }
            //helper.showAlerts(edit, d)
        }
    }

    function openDate(p) {
        edit.datePickOpen[p] = !0
    }

    function removeDeliveryAddr() {
        edit.item.delivery_address = '';
        edit.item.delivery_address_id = ''
    }

    function save(showLoading, reload) {
        var data = angular.copy(edit.item);
        data = helper.clearData(data, ['APPLY_DISCOUNT_LIST', 'CURRENCY_TYPE_LIST', 'SOURCE_DD', 'STAGE_DD', 'TYPE_DD', 'accmanager', 'addresses', 'authors', 'articles_list', 'authors', 'cc', 'contacts', 'country_dd', 'language_dd', 'main_comp_info', 'sort_order']);
        data.do = 'contract-contract-contract-update';
        if (edit.item.contract_id == 'tmp') {
            data.do = 'contract-contract-contract-add'
        } else {
            data.do = 'contract-contract-contract-update'
        }
        if (showLoading) {
            //helper.Loading++;
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                if (r && r.success) {
                    if (edit.item.contract_id == 'tmp') {
                        if($stateParams.quote_id){
                            $state.go('contract.view', {
                                contract_id: r.data.contract_id
                            })
                        }else{
                            $state.go('contract.view', {
                                contract_id: r.data.contract_id,
                                version_id:r.data.version_id
                            }, {inherit: false})
                        }                  
                    } else {
                        $state.go('contract.view', {
                            contract_id: r.data.contract_id
                        })
                    }
                }
                helper.showAlerts(edit, r)
            })
        } else {
            helper.doRequest('post', 'index.php', data, function(d) {
                if (reload) {
                    edit.renderPage(d)
                }
                //helper.showAlerts(edit, d)
            })
        }
    }

    function removeLine($index, obj, isChapter) {
        if ($index > -1) {
            obj.splice($index, 1);           
            if (isChapter) {
                edit.quote_group_active = 0;
                edit.item.quote_group[0].active = !0
            }
            edit.calcQuoteTotal(edit);
        }
    }

    function delaySave() {
        angular.element('.keep-open-on-click').on('click', function(e) {
            e.stopPropagation()
        });
        if (edit.typePromise) {
            $timeout.cancel(edit.typePromise)
        }
        edit.typePromise = $timeout(function() {
           // edit.save()
        }, 2000)
    }

    function addArticle() {
        edit.showTxt.addArticleBtn = !0;
        $timeout(function() {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.open()
        })
    }

    function calc(text, $index, id) {
        edit.calcQuoteTotal(edit, text, $index, id)
    }

    function vatApplyAll() {
        var collections = edit.item.quote_group;
        for (x in collections) {
            var table = collections[x].table;
            if (table.length) {
                for (y in table) {
                    var lines = table[y].quote_line;
                    console.log(lines);
                    if (lines.length) {
                        for (i in lines) {
                            lines[i].line_vat = edit.item.vat;
                            lines[i].price_vat = parseFloat(helper.return_value(lines[i].price), 10) + (parseFloat(helper.return_value(lines[i].price), 10) * parseFloat(helper.return_value(edit.item.vat), 10)) / 100;
                            lines[i].price_vat = helper.displayNr(lines[i].price_vat)
                        }
                    }
                }
            }
        }
        edit.item.vat_lines.vat_value = parseFloat(helper.return_value(lines[i].net_amount), 10) + (parseFloat(helper.return_value(lines[i].net_amount), 10) * parseFloat(helper.return_value(edit.item.vat), 10)) / 100;
        edit.calcQuoteTotal(edit)
    }

    function changeDiscount() {
        if (edit.item.apply_discount_line == !0 && edit.item.apply_discount_global == !1) {
            edit.item.apply_discount = 1
        } else if (edit.item.apply_discount_global == !0 && edit.item.apply_discount_line == !1) {
            edit.item.apply_discount = 2
        } else if (edit.item.apply_discount_line == !0 && edit.item.apply_discount_global == !0) {
            edit.item.apply_discount = 3
        } else if (edit.item.apply_discount_line == !1 && edit.item.apply_discount_global == !1) {
            edit.item.apply_discount = 0
        }
        var collection = edit.item.quote_group;
        if (edit.item.apply_discount == 0 || edit.item.apply_discount == 2) {
            if (edit.item.VIEW_DISCOUNT === !0) {
                edit.item.colum++;
                edit.item.hcolum++;
                for (x in collection) {
                    for (y in collection[x].table) {
                        for (z in collection[x].table[y].quote_line) {
                            collection[x].table[y].quote_line[z].colum++
                        }
                    }
                }
                edit.item.VIEW_DISCOUNT = !1
            }
        } else {
            if (edit.item.VIEW_DISCOUNT === !1) {
                edit.item.colum--;
                edit.item.hcolum--;
                for (x in collection) {
                    for (y in collection[x].table) {
                        for (z in collection[x].table[y].quote_line) {
                            collection[x].table[y].quote_line[z].colum--
                        }
                    }
                }
                edit.item.VIEW_DISCOUNT = !0
            }
        }
        if (edit.item.apply_discount > 1) {
            edit.item.hide_global_discount = !0
        } else {
            edit.item.hide_global_discount = !1
        }
        if (edit.item.apply_discount == 3) {
            edit.applyLineDiscount(!0)
        } else {
            edit.calcQuoteTotal(edit)
        }
    }

    function applyLineDiscount(both) {
        var collection = edit.item.quote_group;
        for (x in collection) {
            var table = collection[x].table;
            if (table.length) {
                for (y in table) {
                    var lines = table[y].quote_line;
                    if (lines.length) {
                        for (i in lines) {
                            if (both == !0) {
                                lines[i].line_discount = edit.item.discount_line_gen
                            } else {
                                lines[i].line_discount = edit.item.discount
                            }
                            if (edit.item.apply_discount == 0) {
                                lines[i].line_discount = 0
                            }
                        }
                    }
                }
            }
        }
        edit.calcQuoteTotal(edit)
    }

    function showhide(t, m) {
        var collection = edit.item.quote_group;
        for (x in collection) {
            var table = collection[x].table;
            if (table.length) {
                for (y in table) {
                    var lines = table[y].quote_line;
                    if (lines.length) {
                        for (i in lines) {
                            lines[i][t] = m
                        }
                    }
                }
            }
        }
    }

    function changeLang() {
        var data = {
            'do': 'contract-contract',
            'lang_id': edit.item.email_language,
            'xget': 'notes',
            'vat_regime_id': edit.item.vat_regime_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            if (r && r.data) {
                for (x in r.data) {
                    edit.item[x] = r.data[x]
                }
            }
        })
    }
    edit.addLine = function(content, title) {
        editItem.addInvoiceLine(edit.item, content, title)
    }
    edit.tinymceOptions = {
        selector: "",
        resize: "both",
        width: 850,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        selector: 'textarea',
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    edit.tinymceOptionsLines = {
        selector: "",
        resize: "both",
        auto_resize: !0,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code autoresize"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        autoresize_bottom_margin: 0,
        setup: function(editor) {
            editor.on('click', function(e) {
                e.stopPropagation();
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                angular.element(editor.editorContainer).parent().removeClass('wysiwyg-hide')
            }).on('blur', function(e) {
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                edit.delaySave()
            })
        },
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
        content_css: [helper.base_url + 'css/tinymce_styles.css']
    };
    /*angular.element('.content').on('click', function() {
        angular.element('.wysiwyg-holder').addClass('wysiwyg-hide')
    });*/
    edit.showGroup = function($event, $index) {
        var collection = edit.item.quote_group;
        for (x in collection) {
            collection[x].active = !1;
            if (x == $index) {
                collection[x].active = !0
            }
        }
        if ($index !== undefined) {
            edit.quote_group_active = $index
        }
    }
    edit.installAutoCfg = {
        valueField: 'installation_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Installation'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.name) + ' </span>' + '</div>' + '</div>' + '</div>';
                if (item.installation_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Add Installation') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined || value == '') {
                edit.item.old_installation_id = value
            } else if (value == '99999999999') {
                openModal('create_install', {}, 'lg')
            } else {
                edit.item.old_installation_id = value
            }
        },
        onType(str) {
            var selectize = angular.element('#installation_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'contract-contract',
                    xget: 'installations',
                    search: str,
                    contract_id: edit.item.contract_id,
                    version_id: edit.item.version_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.installations = r.data;
                    if (edit.item.installations.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        maxItems: 1
    };
    edit.dragControlListeners = {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            var collection = edit.item.quote_group;
            edit.delaySave();
            $timeout(function() {
                angular.element('.table_orders').find('textarea[id^="ui-tinymce"]').each(function() {
                    var id = angular.element(this).attr('id');
                    if(id){
                        $scope.$broadcast('$tinymce:refresh', id)
                    }               
                })
            })
        },
        dragEnd: function(event) {
            if (event && event.dest.index == event.source.index) {
                var collection = edit.item.quote_group;
                if (collection[edit.quote_group_active].table[0].quote_line[event.dest.index].line_type == 3) {
                    $timeout(function() {
                        var id = angular.element('.table_orders').find('li').eq(event.dest.index).find('textarea').attr('id');
                        if(id){
                            $scope.$broadcast('$tinymce:refresh', id)
                        }                    
                    })
                }
            }
        }
    }
    edit.dragChaptersControlListeners = {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            edit.delaySave()
        }
    };
    edit.orderChapter = function($index, next) {
        if ($index == 0 && next === undefined) {
            return !1
        }
        if (next && $index == edit.item.quote_group.length - 1) {
            return !1
        }
        var ind = next ? $index + 1 : $index - 1,
            oldKey = angular.copy(edit.item.quote_group[ind]),
            newKey = angular.copy(edit.item.quote_group[$index]);
        edit.item.quote_group[ind] = newKey;
        edit.item.quote_group[$index] = oldKey;
        edit.delaySave()
    }
    edit.viewInstall = function(id) {
        var url = $state.href('installation.view', {
            'installation_id': id
        });
        $window.open(url, '_blank')
    }
    edit.viewDataInstall = function(id) {
        openModal("install_data", id, "lg")
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'edit_purchase_price':
                var iTemplate = 'PurchasePrice';
                var ctas = 'pprice';
                break;
            case 'create_install':
                var iTemplate = 'CreateInstall';
                var ctas = 'vm';
                break;
            case 'install_data':
                var iTemplate = 'InstallData';
                var ctas = 'vm';
                break
        }
        var params = {
            template: 'qTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'edit_purchase_price':
                params.params = {
                    'do': 'quote-' + type,
                    purchase_price: item.purchase_price,
                    index: item.index
                };
                params.callback = function(data) {
                    if (data) {
                        edit.item.quote_group[edit.quote_group_active].table[0].quote_line[data.index].purchase_price = data.purchase_price
                    }
                }
                break;
            case 'create_install':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'installation-installation',
                    'installation_id': 'tmp',
                    'buyer_id': edit.item.buyer_id,
                    'main_address_id': edit.item.main_address_id
                };
                params.callback = function(d) {
                    if (d) {
                        var data = {
                            'do': 'contract-contract',
                            xget: 'installations',
                            buyer_id: edit.item.buyer_id
                        };
                        helper.doRequest('get', 'index.php', data, function(r) {
                            edit.item.installations = r.data
                        })
                    }
                    edit.item.installation_id = edit.item.old_installation_id
                }
                break;
            case 'install_data':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                var sr_n = '';
                for (x in edit.item.installations) {
                    if (edit.item.installations[x].installation_id == item) {
                        sr_n = edit.item.installations[x].serial_number;
                        break
                    }
                }
                params.params = {
                    'do': 'misc-s3_link',
                    's3_folder': 'installation',
                    'id': edit.item.installation_id
                };
                break
        }
        modalFc.open(params)
    }
    edit.renderPage(data)
}]).controller('contractTemplatesModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vmMod = this;
    for (x in data) {
        vmMod[x] = data[x]
    }
    vmMod.templates = [];
    vmMod.ok = closeModal;
    vmMod.cancel = cancel;

    function closeModal() {
        $uibModalInstance.close(vmMod.templates)
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('contractViewCtrl', ['$scope', '$stateParams', '$state', '$timeout', '$cacheFactory', '$confirm', 'helper', 'editItem', 'modalFc', 'selectizeFc', 'data', 'Lightbox', function($scope, $stateParams, $state, $timeout, $cacheFactory, $confirm, helper, editItem, modalFc, selectizeFc, data, Lightbox) {
    var view = this,
        pdfDoc = null,
        pageNum = 1,
        pageRendering = !1,
        pageNumPending = null,
        scale = 1.5,
        canvas = document.getElementById('the-canvas'),
        ctx = canvas.getContext('2d');
    var view = this;
    view.contract_id = $stateParams.contract_id;
    view.item = {};
    view.showTxt = {
        sendEmail: !1,
        markAsReady: !1,
        viewNotes: !1,
        viewMarkAsSent: !1,
        drop: !1,
        viewMarkAsWon: !1,
        sendPost: !1,
    };
    view.show_load = !0;
    view.email = {};
    view.auto = {
        options: {
            recipients: []
        }
    };
    view.contactForEmailAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select recipient'),
        create: !0,
        createOnBlur: !0,
        maxItems: 100
    });
    view.stationaryCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select E-Stationary'),
        create: !1,
        maxOptions: 100,
        closeAfterSelect: !0,
        maxItems: 1
    };
    view.selectfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    view.selectfgType = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1,
        onBlur() {
            view.setType();
        },
    };
    view.format = 'dd/MM/yyyy';
    view.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    view.datePickOpen = {
        start_date: !1,
        end_date: !1,
        reminder_date: !1,
        sent_date: !1
    }
    view.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        height: 300,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    editItem.init(view);
    view.renderPage = renderPag;
    view.showAlerts = showAlerts;
    view.sendEmails = sendEmails;
    view.markContract = markContract;
    view.markContractAsSent = markContractAsSent;
    view.showPDFsettings = showPDFsettings;
    view.showPreview = showPreview;
    view.showWeblinkLog = showWeblinkLog;
    view.lost = lost;
    view.is_admin = data.data.is_admin;
    view.id_contract = data.data.id_contract;
    view.salesCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: view.is_admin,
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'contract_id': view.id_contract,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                view[realModel] = o.data.lines;
                view.item[realModel] = o.data.inserted_id;
                view.lost()
            })
        },
        onChange(value) {
            if (value != undefined || value != '') {
                view.lost()
            }
        }
    }, !1, !0, view);

    function renderPag(d) {
        if (d.data) {
            if (!d.error) {
                view.item = d.data;
                view.showDrop();
                view.auto.options.recipients = d.data.recipients;
                view.item.email.recipients = d.data.email.recipients;
                view.item.email.sendgrid_selected = d.data.sendgrid_selected;
                view.item.save_disabled = d.data.email.save_disabled;
                if (view.item.start_date) {
                    view.item.start_date = new Date(view.item.start_date)
                }
                if (view.item.end_date && !view.item.unlimited_period) {
                    view.item.end_date = new Date(view.item.end_date)
                }
                if (view.item.sent_date) {
                    view.item.sent_date = new Date(view.item.sent_date)
                }
                view.dateOptions.minDate = view.item.contract_version_date;
                $scope.vm.serial_number = view.item.serial_number;
                $scope.vm.version_code = view.item.version_code;
                helper.updateTitle($state.current.name,view.item.serial_number+' '+view.item.version_code);
                angular.element('#quotePreview center').show();
                angular.element('#bxWrapper').addClass('hide');
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                PDFJS.workerSrc = 'js/libraries/pdf.worker.js';
                PDFJS.externalLinkTarget = PDFJS.LinkTarget.BLANK;
                PDFJS.getDocument(view.item.PDF_LINK2).then(function(pdfDoc_) {
                    pdfDoc = pdfDoc_;
                    document.getElementById('page_count').textContent = '/ ' + pdfDoc.numPages;
                    renderPage(pageNum)
                })
            }
            view.showAlerts(d)
        }
    }
    view.myFunction = function() {
        if (view.item.email.recipients.length) {
            view.item.save_disabled = !1
        } else {
            view.item.save_disabled = !0
        }
    }

    function showPreview(exp) {
        var params = {
            template: 'miscTemplate/modal',
            ctrl: 'viewRecepitCtrl',
            size: 'md',
            ctrlAs: 'vmMod',
            params: {
                'do': 'contract-contract',
                contract_id: view.contract_id,
                version_id: view.item.version_id,
                message: exp.e_message,
                use_html: exp.use_html,
                xget: 'email_preview'
            }
        }
        modalFc.open(params)
    };

    function sendEmails(r) {
        var d = angular.copy(r);
        view.showTxt.sendEmail = !1;
        
        if (d.recipients.length) {
            if (view.item.dropbox) {
                d.dropbox_files = view.item.dropbox.dropbox_files
            }
            if (Array.isArray(d.recipients)) {
                d.email = d.recipients[0]
            } else {
                d.email = d.recipients
            }
            if (d.email) {
                view.item.email.alerts.push({
                    type: 'email',
                    msg: d.email + ' - '
                });
                d.do = 'contract--contract-sendNewEmail';
                d.xview = 1;
                helper.doRequest('post', 'index.php', d, function(r) {
                    if (r.data.mark_as_sent == '1') {
                        view.item.selectedSent = 'selected';
                        view.item.selectedDraft = !1;
                        view.item.external_url = r.data.external_url;
                        view.item.web_url = r.data.web_url;
                        view.item.sent_date_txt = r.data.sent_date_txt
                    }
                    if (r.success && r.success.success) {
                        view.item.email.alerts[view.item.email.alerts.length - 1].type = 'success';
                        view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.success.success
                    }
                    if (r.notice && r.notice.notice) {
                        view.item.email.alerts[view.item.email.alerts.length - 1].type = 'info';
                        view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.notice.notice
                    }
                    if (r.error && r.error.error) {
                        view.item.email.alerts[view.item.email.alerts.length - 1].type = 'danger';
                        view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.error.error
                    }
                    if (Array.isArray(d.recipients)) {
                        d.recipients.shift();
                        d.logging_id = r.data.logging_id;
                        d.copy = !1;
                        d.copy_acc = !1;
                        view.sendEmails(d)
                    }
                })
            } else {
                if (Array.isArray(d.recipients)) {
                    d.recipients.shift();
                    view.sendEmails(d)
                }
            }
        } else {
            if (d.user_loged_email && d.copy) {
                d.email = d.user_loged_email;
                d.copy = !1;
                d.mark_as_sent = 0;
                if (view.item.dropbox) {
                    d.dropbox_files = view.item.dropbox.dropbox_files
                }
                if (d.email) {
                    view.item.email.alerts.push({
                        type: 'email',
                        msg: d.email + ' - '
                    });
                    d.do = 'contract--contract-sendNewEmail';
                    d.xview = 1;
                    helper.doRequest('post', 'index.php', d, function(r) {
                        if (r.data.mark_as_sent == '1') {
                            view.item.selectedSent = 'selected';
                            view.item.selectedDraft = !1;
                            view.item.external_url = r.data.external_url;
                            view.item.web_url = r.data.web_url;
                            view.item.sent_date_txt = r.data.sent_date_txt
                        }
                        if (r.success && r.success.success) {
                            view.item.email.alerts[view.item.email.alerts.length - 1].type = 'success';
                            view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.success.success
                        }
                        if (r.notice && r.notice.notice) {
                            view.item.email.alerts[view.item.email.alerts.length - 1].type = 'info';
                            view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.notice.notice
                        }
                        if (r.error && r.error.error) {
                            view.item.email.alerts[view.item.email.alerts.length - 1].type = 'danger';
                            view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.error.error
                        }
                        d.logging_id = r.data.logging_id;
                        d.user_loged_email = null;
                        view.sendEmails(d)
                    })
                }
            } else if (d.user_acc_email && d.copy_acc) {
                d.email = d.user_acc_email;
                d.copy_acc = !1;
                d.mark_as_sent = 0;
                if (view.item.dropbox) {
                    d.dropbox_files = view.item.dropbox.dropbox_files
                }
                if (d.email) {
                    view.item.email.alerts.push({
                        type: 'email',
                        msg: d.email + ' - '
                    });
                    d.do = 'contract--contract-sendNewEmail';
                    d.xview = 1;
                    helper.doRequest('post', 'index.php', d, function(r) {
                        if (r.data.mark_as_sent == '1') {
                            view.item.selectedSent = 'selected';
                            view.item.selectedDraft = !1;
                            view.item.external_url = r.data.external_url;
                            view.item.web_url = r.data.web_url;
                            view.item.sent_date_txt = r.data.sent_date_txt
                        }
                        if (r.success && r.success.success) {
                            view.item.email.alerts[view.item.email.alerts.length - 1].type = 'success';
                            view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.success.success
                        }
                        if (r.notice && r.notice.notice) {
                            view.item.email.alerts[view.item.email.alerts.length - 1].type = 'info';
                            view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.notice.notice
                        }
                        if (r.error && r.error.error) {
                            view.item.email.alerts[view.item.email.alerts.length - 1].type = 'danger';
                            view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.error.error
                        }
                        d.logging_id = r.data.logging_id
                    })
                }
            }
        }
          helper.refreshLogging();
    }

    function showAlerts(d, formObj) {
        helper.showAlerts(view, d, formObj)
    }

    function markContract(func, obj, formObj) {
        var data = {
            'do': 'contract-contract-contract-' + func,
            contract_id: view.contract_id,
            xview: 1,
            version_id: view.item.version_id
        };
        if (obj) {
            angular.extend(data, obj)
        }
        if (data.status_customer == 2) {
            data.start_date = view.item.start_date;
            data.end_date = view.item.end_date;
            data.unlimited_period = view.item.unlimited_period
        }
        if (data.start_date > data.end_date & !data.unlimited_period & view.item.status_customer < 2) {
            view.showTxt.date_notice = !0
        } else {
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                if (r.error) {
                    if (formObj) {
                        view.showAlerts(r, formObj)
                    } else {
                        view.showAlerts(r)
                    }
                } else {
                    view.showTxt.date_notice = !1;
                    view.showTxt.viewMarkAsWon = !1;
                    view.renderPage(r)
                }
            })
        }
    }

    function markContractAsSent(formObj) {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'contract-contract-contract-mark_sent',
            contract_id: view.contract_id,
            xview: 1,
            sent_date: view.item.sent_date,
            version_id: view.item.version_id
        }, function(r) {
            view.showAlerts(r, formObj);
            if (r.success) {
                view.showTxt.markAsReady = !1;
                view.showTxt.viewMarkAsSent = !1;
                view.renderPage(r)
            }
        })
    }

    function markContractAsWon(formObj) {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'contract-contract-contract-mark_won',
            contract_id: view.contract_id,
            xview: 1,
            start_date: view.item.start_date,
            end_date: view.item.end_date,
            unlimited_period: view.item.unlimited_period,
            version_id: view.item.version_id
        }, function(r) {
            view.showAlerts(r, formObj);
            if (r.success) {
                view.showTxt.viewMarkAsWon = !1;
                view.renderPage(r)
            }
        })
    }

    function lost() {
        view.showTxt.viewLost = !1;
        view.markContract('status', {
            status_customer: 1,
            lost_id: view.item.lost_cc
        })
    }

    function showWeblinkLog() {
        var params = {
            template: 'conTemplate/weblink_log',
            ctrl: 'weblinkLogCCtrl',
            ctrlAs: 'vm',
            size: 'md',
            params: {
                'do': 'contract-contract',
                contract_id: view.contract_id,
                xget: 'weblinkLog'
            }
        };
        modalFc.open(params)
    };

    function showPDFsettings(i) {
        var params = {
            template: 'conTemplate/pdf_settings',
            ctrl: 'pdfSettingsCCtrl',
            ctrlAs: 'vm',
            size: 'md',
            params: {
                'do': 'contract-contract',
                contract_id: view.contract_id,
                xget: 'pdfSettings',
                version_id: view.item.version_id
            },
            callback: function(data) {
                view.renderPage(data)
            }
        };
        modalFc.open(params)
    };
    view.addVersion = function() {
        helper.Loading++;
        //angular.element('.loading_wrap').removeClass('hidden');
        var params = {
            do: 'contract-contract-contract_version-next',
            contract_id: view.contract_id,
            xview: 1,
            current_version_id: view.item.version_id
        }
        helper.doRequest('post', 'index.php', params, function(d) {
            $state.go('contract.edit', {
                'contract_id': view.contract_id
            })
        })
    }
    view.InvoiceGo = function() {
        var cacheQuote = $cacheFactory.get('ContractToInvoice');
        if (cacheQuote != undefined) {
            cacheQuote.destroy()
        }
        var cache = $cacheFactory('ContractToInvoice');
        cache.put('data', view.item.invoice_link);
        $state.go('invoice.edit')
    }
    view.RecurringGo = function() {
        $state.go('recinvoice.edit', {
            recurring_invoice_id: "tmp",
            contract_id: view.contract_id
        })
    }
    view.showDrop = function() {
        view.show_load = !0;
        var data_drop = angular.copy(view.item.drop_info);
        data_drop.do = 'misc-dropbox_new';
        helper.doRequest('post', 'index.php', data_drop, function(r) {
            view.item.dropbox = r.data;
            view.show_load = !1
        })
    }
    view.deleteDropFile = function(index) {
        var data = angular.copy(view.item.dropbox.dropbox_files[index].delete_file_link);
        view.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            view.show_load = !1;
            view.showAlerts(r);
            if (r.success) {
                view.item.dropbox.nr--;
                view.item.dropbox.dropbox_files.splice(index, 1)
            }
        })
    }
    view.deleteDropImage = function(index) {
        var data = angular.copy(view.item.dropbox.dropbox_images[index].delete_file_link);
        view.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            view.show_load = !1;
            view.showAlerts(r);
            if (r.success) {
                view.item.dropbox.nr--;
                view.item.dropbox.dropbox_images.splice(index, 1)
            }
        })
    }
    view.openLightboxModal = function(index) {
        Lightbox.openModal(view.item.dropbox.dropbox_images, index)
    };
    view.uploadDoc = function(type) {
        var obj = angular.copy(view.item.drop_info);
        obj.type = type;
        openModal('uploadDoc', obj, 'lg')
    }
    view.serviceGo = function() {
        var cacheQuote = $cacheFactory.get('ContractToService');
        if (cacheQuote != undefined) {
            cacheQuote.destroy()
        }
        var cache = $cacheFactory('ContractToService');
        cache.put('data', view.item.service_link);
        $state.go('service.edit', {
            service_id: "tmp"
        })
    }
    view.recserviceGo = function() {
        var cacheQuote = $cacheFactory.get('ContractRecurringToService');
        if (cacheQuote != undefined) {
            cacheQuote.destroy()
        }
        var cache = $cacheFactory('ContractRecurringToService');
        cache.put('data', view.item.recurring_service_link);
        $state.go('rec_service.edit', {
            service_id: "tmp",
            is_recurring: true
        })
    }

    function renderPage(num) {
        pageRendering = !0;
        pdfDoc.getPage(num).then(function(page) {
            var viewport = page.getViewport(scale);
            canvas.height = viewport.height;
            canvas.width = viewport.width;
            var renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };
            var renderTask = page.render(renderContext);
            renderTask.promise.then(function() {
                pageRendering = !1;
                if (pageNumPending !== null) {
                    renderPage(pageNumPending);
                    pageNumPending = null
                }
            });
            setupAnnotations(page, viewport, canvas, angular.element('#annotation-layer'));
        });
        document.getElementById('page_num').value = pageNum;
        $('#quotePreview center').hide();
        $('#bxWrapper').removeClass('hide')
    }

    function setupAnnotations(page, viewport, canvas, annotationLayerDiv){
        annotationLayerDiv.empty();
        var promise = page.getAnnotations().then(function (annotationsData) {
            // Canvas offset
            var canvas_offset = $(canvas).position();

            // Canvas height
            var canvas_height = $(canvas).get(0).height;

            // Canvas width
            var canvas_width = $(canvas).get(0).width;

            // CSS for annotation layer
            annotationLayerDiv.css({ left: canvas_offset.left + 'px', top: canvas_offset.top + 'px', height: canvas_height + 'px', width: canvas_width + 'px' });

            PDFJS.AnnotationLayer.render({
                viewport: viewport.clone({ dontFlip: true }),
                div: annotationLayerDiv[0],
                annotations: annotationsData,
                page: page
            });
        });
        return promise;
    }

    function queueRenderPage(num) {
        if (pageRendering) {
            pageNumPending = num
        } else {
            renderPage(num)
        }
    }

    function onPrevPage(e) {
        e.preventDefault();
        if (pageNum <= 1) {
            return
        }
        pageNum--;
        queueRenderPage(pageNum)
    }
    document.getElementById('prev').addEventListener('click', onPrevPage);

    function onNextPage(e) {
        e.preventDefault();
        if (pageNum >= pdfDoc.numPages) {
            return
        }
        pageNum++;
        queueRenderPage(pageNum)
    }
    document.getElementById('next').addEventListener('click', onNextPage);
    view.greenpost = function() {
        if (view.item.post_active) {
            view.showTxt.sendPost = !view.showTxt.sendPost
        } else {
            var data = {};
            data.contract_id = view.item.contract_id;
            data.version_id = view.item.version_id;
            openModal("post_register", data, "md")
        }
    }
    view.sendPost = function() {
        var data = {};
        data.do = 'contract-contract-contract-postIt';
        data.id = view.item.contract_id;
        data.version_id = view.item.version_id;
        data.related = 'view';
        data.xview = 1;
        if (view.item.stationary_id) {
            data.stationary_id = view.item.stationary_id
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            view.showTxt.sendPost = !1;
            if (r.success) {
                view.renderPage(r)
            }
            if (r.error) {
                view.showAlerts(r)
            }
        })
    }
    view.resendGreenPost = function() {
        var data = {};
        data.do = 'contract-contract-contract-postIt';
        data.id = view.item.contract_id;
        data.version_id = view.item.version_id;
        data.related = 'view';
        data.xview = 1;
        data.post_library = angular.copy(view.item.post_library);
        openModal("resend_post", data, "md")
    }
    view.setSegment = function() {
        var obj = {
            'do': 'contract--contract-updateSegment',
            'contract_id': view.item.contract_id,
            'segment_id': view.item.segment_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                view.showTxt.segment = !1;
                view.item.segment = r.data.segment
            }
        })
    }
    view.setSource = function() {
        var obj = {
            'do': 'contract--contract-updateSource',
            'contract_id': view.item.contract_id,
            'source_id': view.item.source_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                view.showTxt.source = !1;
                view.item.source = r.data.source
            }
        })
    }
    view.setType = function() {
        var obj = {
            'do': 'contract--contract-updateType',
            'contract_id': view.item.contract_id,
            'type_id': view.item.type_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                view.showTxt.type = !1;
                view.item.xtype = r.data.xtype
            }
        })
    }

    view.sendEmail = function() {
        var params={
            'do': 'contract-contract',
            contract_id: view.contract_id,
            version_id: view.item.version_id,
            message: view.item.email.e_message,
            use_html: view.item.use_html,
            xget: 'email_preview'
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post','index.php',params,function(r){
            var viewEmail=angular.copy(view.item.email);
            viewEmail.e_message=r.data.e_message;
            var obj={"app":"contract",
                "recipients":view.auto.options.recipients,
                "use_html":view.item.use_html,
                "is_file":view.item.is_file,
                "dropbox":view.item.dropbox,
                "email":viewEmail
            };
            openModal("send_email", angular.copy(obj), "lg");
        })      
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'post_register':
                var iTemplate = 'PostRegister';
                var ctas = 'post';
                break;
            case 'resend_post':
                var iTemplate = 'ResendPostGreen';
                var ctas = 'post';
                break;
            case 'uploadDoc':
                var iTemplate = 'AmazonUpload';
                break;
            case 'send_email':
                var iTemplate = 'SendEmail';
                var ctas = 'vm';
                break;
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'post_register':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.item.do = 'contract-contract-contract-postRegister';
                params.callback = function(data) {
                    if (data && data.data) {
                        view.renderPage(data);
                        view.greenpost()
                    }
                    view.showAlerts(data)
                }
                break;
            case 'resend_post':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.request_type = 'post';
                params.params = {
                    'do': 'contract--contract-postContractData',
                    'contract_id': view.item.contract_id,
                    'version_id': view.item.version_id,
                    'related': 'status',
                    'old_obj': item
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        view.renderPage(d)
                    }
                }
                break;
            case 'uploadDoc':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        view.showDrop()
                    }
                }
                break;
            case 'send_email':
                params.template ='miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        view.sendEmails(d);
                    }
                }
                break;
        }
        modalFc.open(params)
    }
    view.renderPage(data)
}]).controller('pdfSettingsCCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.ok = function() {
        var data = {
            'do': vm.obj.do_next,
            logo: vm.obj.default_logo,
            email_language: vm.obj.language_id,
            pdf_layout: vm.obj.pdf_type_id,
            contract_id: vm.obj.contract_id,
            version_id: vm.obj.version_id,
            identity_id: vm.obj.multiple_identity_id,
            xview: 1
        };
        helper.doRequest('post', 'index.php', data, vm.closeModal)
    };
    vm.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('contractTemplatesCtrl', ['$scope', 'helper', 'list', 'data', function($scope, helper, list, data) {
    console.log('Contract Templates');
    var vm = this
    vm.list = [];
    vm.search = {
        search: '',
        'do': 'contract-contracts_templates',
        offset: 1
    };
    vm.ord = '';
    vm.reverse = !1;
    vm.renderList = function(d) {
        if (d.data) {
            for (x in d.data) {
                vm[x] = d.data[x]
            }
            helper.showAlerts(vm, d)
        }
    };
    vm.toggleSearchButtons = function(item, val) {
        list.tSearch(vm, item, val, vm.renderList)
    };
    vm.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search(vm.search, vm.renderList, vm)
    }
    vm.orderBY = function(p) {
        list.orderBy(vm, p, vm.renderList)
    };
    vm.action = function(item, action) {
        list.action(vm, item, action, vm.renderList)
    }
    vm.renderList(data)
}]).controller('ContractEditTemplateCtrl', ['$scope', '$stateParams', '$state', '$timeout', 'helper', 'editItem', 'data', function($scope, $stateParams, $state, $timeout, helper, editItem, data) {
    console.log('template edit');
    var edit = this,
        typePromise;
    edit.app = 'contract';
    edit.ctrlpag = 'contract';
    edit.tmpl = 'conTemplate';
    $scope.vm.isAddPage = !1;
    edit.item_id = $stateParams.template_id;
    edit.contract_id = edit.item_id;
    edit.item = {};
    edit.oldObj = {};
    edit.item.validity_date = new Date();
    edit.removedCustomer = !1;
    edit.item.add_customer = (edit.item_id != 'tmp' ? true : !1);
    edit.quote_group_active = 0;
    edit.originalArticleList = undefined;
    edit.showTxt = {
        addBtn: !0,
        addArticleBtn: !1,
    }
    edit.auto = {
        options: {
            articlesList: []
        }
    };
    editItem.init(edit);
    edit.renderPage = function(d) {
        if (d && d.data) {
            if (d.data.redirect) {
                $state.go(d.data.redirect);
                return !1
            }
            edit.item = d.data;
            edit.auto.options.articles_list = d.data.articles_list && d.data.articles_list.lines ? d.data.articles_list.lines : [];
            if (edit.originalArticleList === undefined) {
                edit.originalArticleList = edit.auto.options.articles_list
            }
            if (edit.item.start_date) {
                edit.item.start_date = new Date(edit.item.start_date)
            }
            if (edit.item.end_date) {
                edit.item.end_date = new Date(edit.item.end_date)
            }
            if (edit.item.reminder_date) {
                edit.item.reminder_date = new Date(edit.item.reminder_date)
            }
            $scope.vm.serial_number = edit.item.template_name;
            edit.contactAddObj = {
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                add_contact: !0,
                buyer_id: edit.item.buyer_id,
                article: edit.item.tr_id,
                'do': 'contract-contract-contract-tryAddC',
                identity_id: edit.item.identity_id,
                contact_only: !0,
                item_id: edit.item_id
            }
            helper.showAlerts(edit, d)
        }
    }
    edit.save = function(showLoading) {
        if (!edit.item.template_name) {
            return !1
        }
        var data = angular.copy(edit.item),
            add = !1;
        data = helper.clearData(data, ['APPLY_DISCOUNT_LIST', 'CURRENCY_TYPE_LIST', 'SOURCE_DD', 'STAGE_DD', 'TYPE_DD', 'accmanager', 'addresses', 'authors', 'articles_list', 'authors', 'cc', 'contacts', 'country_dd', 'language_dd', 'main_comp_info', 'sort_order']);
        data.do = 'contract-contract-contract-update';
        if (edit.item_id == 'tmp') {
            data.do = 'contract-contract-contract-add';
            add = !0
        }
        data.template = 'template';
        if (showLoading) {
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                if (r && r.success && add == !0) {
                    $state.go('contract.editTemplate', {
                        template_id: r.data.contract_id
                    })
                }
                helper.showAlerts(edit, r)
            })
        } else {
            helper.doRequest('post', 'index.php', data, function(d) {
                if (d && d.success && add == !0) {
                    $state.go('contract.editTemplate', {
                        template_id: d.data.contract_id
                    })
                }
                helper.showAlerts(edit, d)
            })
        }
    }
    edit.removeLine = function($index, obj, isChapter) {
        if ($index > -1) {
            obj.splice($index, 1);         
            if (isChapter) {
                edit.quote_group_active = 0;
                edit.item.quote_group[0].active = !0
            }
            edit.delaySave();
        }
    }
    edit.delaySave = function() {
        angular.element('.keep-open-on-click').on('click', function(e) {
            e.stopPropagation()
        });
        if (edit.typePromise) {
            $timeout.cancel(edit.typePromise)
        }
        edit.typePromise = $timeout(function() {
            //edit.save()
        }, 2000)
    }
    edit.linevatcfg = {
        valueField: 'vat',
        labelField: 'name',
        searchField: ['vat'],
        delimiter: '|',
        placeholder: helper.getLanguage('VAT'),
        create: !1,
        maxOptions: 100,
        onDelete(values) {
            return !1
        },
        closeAfterSelect: !0,
        maxItems: 1
    };
    edit.tinymceOptions = {
        selector: "",
        resize: "both",
        width: 850,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        selector: 'textarea',
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    edit.tinymceOptionsLines = {
        selector: "",
        resize: "both",
        auto_resize: !0,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code autoresize"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        autoresize_bottom_margin: 0,
        setup: function(editor) {
            editor.on('click', function(e) {
                e.stopPropagation();
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                angular.element(editor.editorContainer).parent().removeClass('wysiwyg-hide')
            }).on('blur', function(e) {
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                edit.delaySave()
            })
        },
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
        content_css: [helper.base_url + 'css/tinymce_styles.css']
    };
    edit.delaySave=function(showLoading, reload) {
        angular.element('.keep-open-on-click').on('click', function(e) {
            console.log('ccc');
            e.stopPropagation()
        });
        if (edit.typePromise) {
            console.log('ccc1');
            $timeout.cancel(edit.typePromise)
        }
        edit.typePromise = $timeout(function() {
            console.log('ccc2');
            //edit.save(showLoading, reload)
        }, 2000)
    }
    /*angular.element('.content').on('click', function() {
        angular.element('.wysiwyg-holder').addClass('wysiwyg-hide')
    });*/
    edit.addArticle = function() {
        edit.showTxt.addArticleBtn = !0;
        $timeout(function() {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.open()
        })
    }
    edit.showGroup = function($event, $index) {
        var collection = edit.item.quote_group;
        for (x in collection) {
            collection[x].active = !1;
            if (x == $index) {
                collection[x].active = !0
            }
        }
        if ($index !== undefined) {
            edit.quote_group_active = $index
        }
    }
    edit.calc = function(text, $index, id) {
        edit.calcQuoteTotal(edit, text, $index, id)
    }
    edit.dragControlListeners = {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            var collection = edit.item.quote_group;
            edit.delaySave();
            $timeout(function() {
                angular.element('.table_orders').find('textarea[id^="ui-tinymce"]').each(function() {
                    var id = angular.element(this).attr('id');
                    if(id){
                        $scope.$broadcast('$tinymce:refresh', id)
                    }                   
                })
            })
        },
        dragEnd: function(event) {
            if (event && event.dest.index == event.source.index) {
                var collection = edit.item.quote_group;
                if (collection[edit.quote_group_active].table[0].quote_line[event.dest.index].line_type == 3) {
                    $timeout(function() {
                        var id = angular.element('.table_orders').find('li').eq(event.dest.index).find('textarea').attr('id');
                        if(id){
                            $scope.$broadcast('$tinymce:refresh', id)
                        }                 
                    })
                }
            }
        }
    }
    edit.dragChaptersControlListeners = {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            edit.delaySave()
        }
    };
    edit.orderChapter = function($index, next) {
        if ($index == 0 && next === undefined) {
            return !1
        }
        if (next && $index == edit.item.quote_group.length - 1) {
            return !1
        }
        var ind = next ? $index + 1 : $index - 1,
            oldKey = angular.copy(edit.item.quote_group[ind]),
            newKey = angular.copy(edit.item.quote_group[$index]);
        edit.item.quote_group[ind] = newKey;
        edit.item.quote_group[$index] = oldKey;
        edit.delaySave()
    }
    edit.articlesListAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.family) {
                    item.family = "/ " + item.family
                }
                var stock_display = '';
                if (item.hide_stock == 0) {
                    stock_display = (item.base_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-right">' + stock_display + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> Create a new item</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'contract-contract',
                    xget: 'articles_list',
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id
                };
                edit.requestAuto(edit, data, edit.renderArticleListAuto)
            }, 300)
        },
        onChange(value) {
            if (value == undefined) {} else if (value == '99999999999') {
                edit.openModal(edit, 'AddAnArticle', {
                    'do': 'misc-addAnArticle',
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: edit.item.remove_vat
                }, 'md');
                return !1
            } else {
                var collection = edit.auto.options.articles_list;
                for (x in collection) {
                    if (collection[x].article_id == value) {
                        editItem.addQuoteArticle(edit, collection[x]);
                        edit.selected_article_id = '';
                        edit.showTxt.addArticleBtn = !1;
                        var data = {
                            'do': 'contract-contract',
                            xget: 'articles_list',
                            buyer_id: edit.item.buyer_id,
                            lang_id: edit.item.email_language,
                            cat_id: edit.item.cat_id
                        };
                        edit.requestAuto(edit, data, edit.renderArticleListAuto);
                        break
                    }
                }
            }
        },
        onItemAdd(value, $item) {
            if (value == '99999999999') {
                edit.selected_article_id = ''
            }
        },
        onBlur() {
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'contract-contract',
                    xget: 'articles_list',
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id
                };
                edit.showTxt.addArticleBtn = !1;
                if (edit.originalArticleList === undefined) {
                    edit.requestAuto(edit, data, edit.renderArticleListAuto)
                } else {
                    edit.auto.options.articles_list = edit.originalArticleList
                }
            }, 300)
        },
        maxItems: 1
    };
    edit.renderPage(data)
}]).controller('contractSettingsCtrl', ['$scope', 'helper', '$uibModal', 'settingsFc', 'data', 'selectizeFc', function($scope, helper, $uibModal, settingsFc, data, selectizeFc) {
    var vm = this;
    vm.app = 'contract';
    vm.ctrlpag = 'settings';
    vm.tmpl = 'conTemplate';
    settingsFc.init(vm);
    vm.logos = {};
    vm.layout = {};
    vm.layout_header = {};
    vm.layout_body = {};
    vm.layout_footer = {};
    vm.settings = {
        use_page_numbering: !1,
        show_stamp: !1
    };
    vm.custom = {
        pdf_active: !1,
        pdf_note: !1,
        pdf_condition: !1,
        pdf_stamp: !1
    };
    vm.custom.layouts = {
        clayout: [],
        selectedOption: '0'
    };
    vm.convention = {
        reference: !1
    };
    vm.general_condition = {};
    vm.customLabel = {};
    vm.message = {};
    vm.email_default = {};
    vm.combo = !0;
    vm.defaultEmail = {};
    vm.weblink = {};
    vm.bigloop = {};
    vm.articlefields = {};
    vm.packing = {};
    vm.atachment = {};
    vm.var = '#5d97b9';
    vm.listset = {};
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 850,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        selector: 'textarea',
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.updateLayout = function() {
        var get = 'PDFlayout';
        var data = {
            'do': 'quote--quote-settings',
            'xget': get,
            show_bank: vm.settings.show_bank,
            'show_stamp': vm.settings.show_stamp,
            'show_page': vm.settings.show_page,
            'show_paid': vm.settings.show_paid
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.load = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'contract-settings',
            'xget': h
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
            if (k == 'logos') {
                vm.default_logo = r.data.default_logo
            }
        })
    }
    vm.modal = function(r, template, ctrl, size) {
        console.log(r, template, ctrl, size);
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'lg',
            resolve: {
                obj: function() {
                    console.log(r);
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            vm.selected = selectedItem
        }, function() {
            if (r.load) {
                vm.load(r.load[0], r.load[1])
            }
        })
    };
    vm.clickys = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.message.text = vm.message.text.substring(0, startPos) + myValue + vm.message.text.substring(endPos, vm.message.text.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.message.text += myValue;
            _this.focus()
        }
    }
    vm.clickyArticle = function(scope, myValue) {
        if (!scope.articlefields.ADV_PRODUCT) {
            return !1
        }
        vm.clicky(scope, myValue)
    }
    vm.PDFactive = function() {
        var get = 'CustomizePDF';
        var data = {
            'do': 'quote--quote-PdfAcitve',
            'xget': get,
            'pdf_active': vm.custom.pdf_active,
            'pdf_note': vm.custom.pdf_note,
            'pdf_condition': vm.custom.pdf_condition,
            'pdf_stamp': vm.custom.pdf_stamp
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.PageActive = function() {
        var get = 'general';
        var data = {
            'do': 'quote--quote-PageActive',
            'xget': get,
            'page': vm.general_condition.page
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.DeleteField = function(vm, field_id, table) {
        var get = 'categorisation';
        var data = {
            'do': 'quote-settings-quote-DeleteField',
            'xget': get,
            'field_id': field_id,
            'table': table
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            vm.showAlerts(vm, r)
        })
    }
    vm.updatefield = function(vm, id, table, value) {
        var get = 'categorisation';
        var data = {
            'do': 'quote-settings-quote-UpdateField',
            'xget': get,
            'id': id,
            'table': table,
            'value': value
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            vm.showAlerts(vm, r)
        })
    }
    vm.AddField = function(vm, id, table, name) {
        var get = 'categorisation';
        var data = {
            'do': 'quote-settings-quote-add_field',
            'xget': get,
            'name': name,
            'table': table
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            vm.showAlerts(vm, r)
        })
    }
    vm.savepacking = function(vm, r) {
        var get = 'packingsettings';
        var data = {
            'do': 'quote-settings-quote-savepacking',
            'xget': get,
            'pack': vm.packing.allowpack,
            'sale': vm.packing.allowsale
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.packing = r.data.packing
            vm.showAlerts(vm, r)
        })
    }
    vm.Changecodelabes = function(vm, r) {
        var get = 'emailMessage';
        var data = {
            'do': 'quote-settings',
            'xget': get,
            detail_id: r.detail_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            vm.message_labels = {
                detail_id: r.data.message_labels.detail_id,
                detail: r.data.message_labels.detail,
                selected_detail: r.data.message_labels.selected_detail
            };
            vm.showAlerts(vm, r)
        })
    }
    vm.setact = function(vm, id) {
        var get = 'listsettings';
        var data = {
            'do': 'quote-settings-quote-setact',
            'xget': get,
            'id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.listset = r.data.listset;
            vm.showAlerts(vm, r)
        })
    }
    vm.saveterm = function() {
        var data = angular.copy(vm.quoteTerm);
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.quoteTerm = r.data.quoteTerm;
            vm.showAlerts(vm, r)
        })
    }
    vm.renderList = function(r) {
        helper.doRequest('get', 'index.php?do=contract-settings&xget=PDFlayout', function(r) {
            vm.settings = {
                use_page_numbering: r.data.use_page_numbering,
                show_stamp: r.data.show_stamp,
                font_pdf: r.data.font_pdf,
                layout_header: r.data.layout_header,
                layout_footer: r.data.layout_footer,
                selected_header: r.data.selected_header,
                selected_body: r.data.selected_body,
                selected_footer: r.data.selected_footer,
                lang_id: r.data.lang_id,
                multiple_identity: r.data.multiple_identity,
                identity: r.data.identity
            };
            vm.layout_header = r.data.layout_header;
            vm.layout_body = r.data.layout_body;
            vm.layout_footer = r.data.layout_footer;
            vm.custom_layout = r.data.custom_layout;
            vm.databasename = r.data.databasename
        });
        helper.doRequest('get', 'index.php?do=contract-settings&xget=CustomizePDF', function(r) {
            vm.custom = {
                pdf_active: r.data.pdf_active,
                pdf_note: r.data.pdf_note,
                pdf_condition: r.data.pdf_condition,
                pdf_stamp: r.data.pdf_stamp,
                layouts: {
                    clayout: [],
                    selectedOption: ""
                },
                header: r.data.header,
                footer: r.data.footer,
                lang_id: r.data.lang_id,
                layout: r.data.layout,
                layout_footer: r.data.layout_footer
            };
            vm.custom.layouts.clayout = r.data.layouts;
            vm.custom.layouts.selectedOption = '0'
        });
        helper.doRequest('get', 'index.php?do=contract-settings&xget=convention', function(r) {
            vm.convention = {
                account_number: r.data.account_number,
                account_digits: r.data.account_digits,
                example: r.data.example,
                reference: r.data.reference,
                del: r.data.del
            }
        })
    };
    vm.renderList(data)
}]).controller('custom_pdfCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'contract';
    vm.ctrlpag = 'custom_pdf';
    vm.tmpl = 'conTemplate';
    settingsFc.init(vm);
    vm.codelabels = {};
    vm.selectlabels = {};
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 1100,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload_image_pdf').trigger('click');
                $('#upload_image_pdf').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.closeModal = closeModal;
    vm.cancel = cancel;
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.codelabels.variable_data = vm.codelabels.variable_data.substring(0, startPos) + myValue + vm.codelabels.variable_data.substring(endPos, vm.codelabels.variable_data.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.codelabels.variable_data += myValue;
            _this.focus()
        }
    }

    function closeModal() {
        $uibModalInstance.close()
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    }
    helper.doRequest('get', 'index.php?do=contract-custom_pdf&layout=' + obj.id + '&header=' + obj.header + '&footer=' + obj.footer, function(r) {
        vm.codelabels = {
            variable_data: r.data.codelabels.variable_data,
            layout: r.data.codelabels.layout,
            header: r.data.codelabels.header,
            footer: r.data.codelabels.footer
        };
        vm.selectlabels = {
            make_id: r.data.selectlabels.make_id,
            make: r.data.selectlabels.make,
            selected_make: r.data.selectlabels.selected_make
        };
        vm.selectdetails = {
            detail_id: r.data.selectdetails.detail_id,
            detail: r.data.selectdetails.detail
        }
    })
}]).controller('labelCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'contract';
    vm.ctrlpag = 'label';
    vm.tmpl = 'conTemplate';
    settingsFc.init(vm);
    vm.labels = {};
    helper.doRequest('get', 'index.php', obj, function(r) {
        vm.labels = r.data.labels
    });
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}])