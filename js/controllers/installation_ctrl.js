app.controller('installationsCtrl', ['$scope', '$rootScope', 'helper', 'list', 'data', function($scope, $rootScope, helper, list, data) {
    console.log("Installations");
    $scope.list = [];
    $scope.nav = [];
    $scope.search = {
        search: '',
        'do': 'installation-installations',
        view: 0,
        xget: '',
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $rootScope.reset_lists.installations=true;
    $scope.minimum_selected=false;
    $scope.all_pages_selected=false;
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.export_args = d.data.export_args;
            $scope.minimum_selected=d.data.minimum_selected;
            $scope.all_pages_selected=d.data.all_pages_selected;
            $scope.nav = d.data.nav;
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['installation_id']);
                count ++;
            }
            sessionStorage.setItem('installation.view_total', count);
            sessionStorage.setItem('installation.view_list', JSON.stringify(ids));
        }
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList,true)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList,true)
    };
    $scope.searchThing = function(reset_list) {
        angular.element('.loading_wrap').removeClass('hidden');
        var params=angular.copy($scope.search);
        if(reset_list){
            params.reset_list='1';
        }
        list.search(params, $scope.renderList, $scope)
    }
    $scope.markCheckNew=function(i){
        list.markCheckNew($scope, 'installation--installation-saveAddToPdf', i);
    }
    $scope.checkAllPage=function(){
        $scope.listObj.check_add_all=true;
        $scope.markCheckNew();
    }
    $scope.checkAllPages=function(){
        list.markCheckNewAll($scope, 'installation--installation-saveAddToPdfAll','1');
    }
    $scope.uncheckAllPages=function(){
        list.markCheckNewAll($scope, 'installation--installation-saveAddToPdfAll','0');
    }

    list.init($scope, 'installation-installations', $scope.renderList,($rootScope.reset_lists.installations ? true : false));
    $rootScope.reset_lists.installations=false;

    //$scope.renderList(data)
}]).controller('installationFieldsCtrl', ['$scope', 'helper', 'data', 'list', function($scope, helper, data, list) {
    console.log("Installation Fields");
    $scope.item = {};
    $scope.search = {
        offset: 1,
        'do': 'installation-install_fields'
    }
    $scope.renderPage = function(d) {
        if (d && d.data) {
            $scope.item = d.data.query
            $scope.max_rows = d.data.max_rows
            $scope.lr = d.data.lr
        }
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.searchThing = function(reset_list) {
        angular.element('.loading_wrap').removeClass('hidden');
        var params=angular.copy($scope.search);
        if(reset_list){
            params.reset_list='1';
        }
        list.search(params, $scope.renderPage, $scope)
    };
    $scope.addField = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'installation-install_fields-ifields-addField'
        }, function(r) {
            $scope.showAlerts(r);
            $scope.renderPage(r)
        })
    }
    $scope.deleteField = function(item) {
        var obj = {};
        obj.do = 'installation-install_fields-ifields-deleteField';
        obj.field_id = item.field_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.showAlerts(r);
            $scope.renderPage(r)
        })
    }
    $scope.changeFieldName = function(item) {
        var obj = {};
        obj.do = 'installation--ifields-editNameField';
        obj.field_id = item.field_id;
        obj.name = item.name;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.showAlerts(r)
        })
    }
    $scope.editData = function(item) {
        var obj = {};
        obj.do = 'installation-install_fields-ifields-editDataField';
        obj.value = item.type;
        obj.field_id = item.field_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.showAlerts(r);
            $scope.renderPage(r)
        })
    }
    $scope.renderPage(data)
}]).controller('installationTemplatesCtrl', ['$scope', 'helper', 'list', 'data', function($scope, helper, list, data) {
    console.log("Installations");
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'installation-install_templates',
        view: 0,
        xget: '',
        offset: 1
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr
        }
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.renderList(data)
}]).controller('installationTemplateCtrl', ['$scope', 'helper', function($scope, helper) {}]).controller('installationTemplateEditCtrl', ['$scope', '$stateParams', 'helper', 'data', function($scope, $stateParams, helper, data) {
    console.log("Installation Template Edit");
    var edit = this,
        typePromise;
    edit.template_id = $stateParams.template_id;
    edit.item = {};
    edit.search = '';
    edit.renderPage = function(d) {
        if (d && d.data) {
            edit.item = d.data
        }
    }
    edit.showAlerts = function(d, formObj) {
        helper.showAlerts(edit, d, formObj)
    }
    edit.saveData = function(formObj) {
        var formData = new FormData();
        for (x in edit.item) {
            if (Array.isArray(edit.item[x])) {
                formData.append(x, JSON.stringify(edit.item[x]))
            } else {
                formData.append(x, edit.item[x])
            }
        }
        formData.append("do", edit.item.do_next);
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "index.php", !0);
        oReq.onload = function(oEvent) {
            var r = JSON.parse(oEvent.target.response);
            edit.showAlerts(r, formObj);
            if (r.success) {
                edit.item = r.data
            }
        };
        oReq.send(formData)
    }
    edit.clearSearch = function() {
        edit.search = ''
    }
    $scope.$watch('edit.item.files', function() {
        if (edit.item.files) {
            if (edit.item.files.size > 256000) {
                var obj = {
                    "error": {
                        "error": helper.getLanguage("Error: file is to big.")
                    },
                    "notice": !1,
                    "success": !1
                };
                edit.showAlerts(obj);
                edit.item.files = undefined;
                return !1
            }
            edit.item.icon = URL.createObjectURL(edit.item.files)
        }
    });
    edit.removeIcon = function() {
        edit.item.files = undefined;
        edit.item.icon = undefined
    }
    edit.renderPage(data)
}]).controller('installationCtrl', ['$scope', 'helper', function($scope, helper) {
    $scope.serial_number = ''
}]).controller('installationEditCtrl', ['$scope', '$state', '$stateParams', '$timeout', 'helper', 'data', 'modalFc', 'editItem', function($scope, $state, $stateParams, $timeout, helper, data, modalFc, editItem) {
    console.log("Installation Edit");
    var edit = this,
        typePromise;
    edit.app = 'installation';
    edit.ctrlpag = 'installation';
    edit.tmpl = 'insTemplate';
    edit.item_id = $stateParams.installation_id;
    edit.installation_id = $stateParams.installation_id;
    edit.isAddPage = !0;
    edit.item = {};
    var old_item = {};
    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {};
    edit.oldObj = {};
    edit.showTxt = {
        settings: !1,
        address: !1,
        addBtn: !0,
    };
    edit.auto = {
        options: {
            contacts: [],
            addresses: [],
            cc: [],
            articles: [],
            services: [],
        }
    };
    edit.item.add_customer = !1;
    //edit.item.add_customer = (edit.item_id != 'tmp' ? true : !1);
    edit.showEditFnc = showEditFnc;
    edit.showCCSelectize = showCCSelectize;
    edit.cancelEdit = cancelEdit;
    edit.removeDeliveryAddr = removeDeliveryAddr;
    editItem.init(edit);
    edit.renderPage = function(d) {
        if (d && d.data) {
            edit.item = d.data;
            helper.updateTitle($state.current.name,edit.item.serial_number);
            edit.item_id = edit.item.installation_id;
            if (edit.item.installation_id != 'tmp') {
                for (x in edit.item.template.left_fields) {
                    if (edit.item.template.left_fields[x].type == '3') {
                        if (edit.item.template.left_fields[x].name_final) {
                            edit.item.template.left_fields[x].name_final = new Date(edit.item.template.left_fields[x].name_final)
                        } else {
                            edit.item.template.left_fields[x].name_final = ''
                        }
                    }
                }
                for (x in edit.item.template.right_fields) {
                    if (edit.item.template.right_fields[x].type == '3') {
                        if (edit.item.template.right_fields[x].name_final) {
                            edit.item.template.right_fields[x].name_final = new Date(edit.item.template.right_fields[x].name_final)
                        } else {
                            edit.item.template.right_fields[x].name_final = ''
                        }
                    }
                }
            }
            edit.auto.options.addresses = d.data.addresses;
            edit.auto.options.cc = d.data.cc;
            if (d.data.articles_list) {
                edit.auto.options.articles = d.data.articles_list.lines
            }
            if (d.data.services_list) {
                edit.auto.options.services = d.data.services_list.lines
            }
            edit.contactAddObj = {
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                add_contact: !0,
                buyer_id: edit.item.buyer_id,
                'do': 'installation-installation-installation-tryAddC',
                contact_only: !0,
                item_id: edit.item_id
            }
        }
    }

    function showEditFnc() {
        edit.showTxt.address = !1;
        edit.oldObj = {
            buyer_id: angular.copy(edit.item.buyer_id),
            contact_id: angular.copy(edit.item.contact_id),
            contact_name: angular.copy(edit.item.contact_name),
            BUYER_NAME: angular.copy(edit.item.buyer_name),
            delivery_address: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address)),
            delivery_address_id: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_id)),
            delivery_address_txt: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_txt)),
            view_delivery: (edit.item.sameAddress ? !1 : angular.copy(edit.item.view_delivery)),
            sameAddress: angular.copy(edit.item.sameAddress),
        }
    }

    function showCCSelectize() {
        edit.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }

    function removeDeliveryAddr() {
        edit.item.delivery_address = '';
        edit.item.delivery_address_id = ''
    }

    function cancelEdit() {
        if (Object.keys(edit.oldObj).length == 0) {
            return edit.removeCust(edit)
        }
        edit.showTxt.address = !0;
        for (x in edit.oldObj) {
            if (edit.item[x]) {
                if (x == 'delivery_address') {
                    edit.item[x] = edit.oldObj[x].trim()
                } else {
                    edit.item[x] = edit.oldObj[x]
                }
            }
        }
        edit.item.buyer_id = edit.oldObj.buyer_id
    }
    edit.showAlerts = function(d, formObj) {
        helper.showAlerts(edit, d, formObj)
    }
    edit.fieldCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Option'),
        closeAfterSelect: !0,
        onDropdownOpen($dropdown) {
            var _this = this,
                field_index = _this.$input[0].attributes.fieldid.value,
                pos = _this.$input[0].attributes.position.value;
            if (edit.item.template[pos][field_index].name_final != undefined && edit.item.template[pos][field_index].name_final != '0') {
                old_item[edit.item.template[pos][field_index].field_id] = edit.item.template[pos][field_index].name_final
            }
        },
        render: {
            option: function(item, escape) {
                var html = '<div  class="option " >' + item.name + '</div>';
                if (item.id == '0') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Manage the list') + '</div>'
                }
                return html
            }
        },
        onChange(item) {
            var _this = this;
            var field_index = _this.$input[0].attributes.fieldid.value;
            var pos = _this.$input[0].attributes.position.value;
            if (edit.item.template[pos][field_index].name_final != undefined && edit.item.template[pos][field_index].name_final != '0') {
                old_item[edit.item.template[pos][field_index].field_id] = edit.item.template[pos][field_index].name_final
            }
            if (item == '0') {
                var obj = {
                    'pos': pos,
                    'field_index': field_index
                };
                openModal('selectEdit', obj, 'md');
                edit.item.template[pos][field_index].name_final = undefined;
                return !1
            }
        },
        maxItems: 1
    };
    edit.articleAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Article'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.name2) {
                    item.name2 = "/ " + item.name2
                }
                if (item.ALLOW_STOCK == !0) {
                    var step = '<br><small class="text-muted">' + helper.getLanguage('Stock') + ' <strong>' + escape(item.stock2) + '</strong></small>'
                } else {
                    var step = ''
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '&nbsp; </small>' + '</div>' + '<div class="col-sm-3 pull-right text-right">' + (helper.displayNr(item.price)) + step + '</div>' + '</div>' + '</div>';
                return html
            }
        },
        onType(str) {
            var selectize = angular.element('#articles_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'installation-installation',
                    xget: 'articles_list',
                    search: str,
                    cat_id: edit.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.auto.options.articles = r.data.lines;
                    if (edit.auto.options.articles.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value) {
            if (value == undefined || value == '') {
                var data = {
                    'do': 'installation-installation',
                    xget: 'articles_list',
                    cat_id: edit.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.auto.options.articles = r.data.lines
                })
            }
        },
        onBlur() {
            if (!edit.auto.options.articles.length) {
                var data = {
                    'do': 'installation-installation',
                    xget: 'articles_list',
                    cat_id: edit.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.auto.options.articles = r.data.lines
                })
            }
        },
        maxItems: 1
    };
    edit.serviceAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Service'),
        create: !1,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp; </small>' + '</div>' + '<div class="col-sm-3 pull-right text-right">' + (helper.displayNr(item.price)) + '</div>' + '</div>' + '</div>';
                return html
            }
        },
        onType(str) {
            var selectize = angular.element('#services_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'installation-installation',
                    xget: 'articles_list',
                    search: str,
                    cat_id: edit.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.auto.options.services = r.data.lines;
                    if (edit.auto.options.services.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value) {
            if (value == undefined || value == '') {
                var data = {
                    'do': 'installation-installation',
                    xget: 'articles_list',
                    cat_id: edit.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.auto.options.services = r.data.lines
                })
            }
        },
        onBlur() {
            if (!edit.auto.options.services.length) {
                var data = {
                    'do': 'installation-installation',
                    xget: 'articles_list',
                    cat_id: edit.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.auto.options.services = r.data.lines
                })
            }
        },
        maxItems: 1
    };
    edit.saveData = function(formObj) {
        var obj = angular.copy(edit.item);
        if (edit.item.installation_id == 'tmp') {
            obj.do = 'installation-installation-ifields-addInstallation'
        } else {
            obj.do = 'installation-installation-ifields-updateInstallation'
        }
        obj = helper.clearData(obj, ['cc', 'country_dd', 'templates', 'articles_list', 'services_list']);
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            edit.showAlerts(r, formObj);
            if (r.success) {
                edit.renderPage(r)
            }
        })
    }
    edit.selectTemplate = function() {
        openModal("select_template", edit, "lg")
    }

    edit.cancel = function() {
        if (isNaN(edit.installation_id)) {
            $state.go('installations')
        } else {
            $state.go('installation.view', {
                installation_id: edit.installation_id
            })
        }
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'add_steps':
                var iTemplate = 'InstallationAddSteps';
                var ctas = 'add';
                break;
            case 'select_template':
                var iTemplate = 'InstallationAddTemplate';
                var ctas = 'add';
                break;
            case 'selectEdit':
                var iTemplate = 'selectEdit';
                var ctas = 'vmMod';
                break
        }
        var params = {
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'add_steps':
                params.template = 'insTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.item = item;
                params.callbackExit = function() {
                    $state.go('installations')
                }
                break;
            case 'select_template':
                params.template = 'insTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.item = item;
                params.callback = function() {
                    if (edit.item.template_id && edit.item.name) {
                        angular.element('#template_id').removeClass('hidden');
                        angular.element('#template_name').removeClass('hidden');
                        angular.element('#blank_template').remove()
                    }
                }
                break;
            case 'selectEdit':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = 'selectInstallFieldModalCtrl';
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-selectInstallField',
                    'field_id': edit.item.template[item.pos][item.field_index].field_id
                };
                params.callback = function(data) {
                    if (data.data) {
                        edit.item.template[item.pos][item.field_index].no_fields = data.data.lines;
                        if (old_item[edit.item.template[item.pos][item.field_index].field_id]) {
                            edit.item.template[item.pos][item.field_index].name_final = old_item[edit.item.template[item.pos][item.field_index].field_id]
                        }
                    } else {
                        edit.item.template[item.pos][item.field_index].no_fields = data.lines;
                        if (old_item[edit.item.template[item.pos][item.field_index].field_id]) {
                            edit.item.template[item.pos][item.field_index].name_final = old_item[edit.item.template[item.pos][item.field_index].field_id]
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }
    edit.renderPage(data)
}]).controller('installationViewCtrl', ['$scope', '$state', '$stateParams', 'helper', 'data', 'editItem', 'Lightbox', 'modalFc', function($scope, $state, $stateParams, helper, data, editItem, Lightbox, modalFc) {
    console.log("Installation View");
    var edit = this,
        typePromise;
    edit.installation_id = $stateParams.installation_id;
    edit.item = {};
    edit.show_load = !0;
    editItem.init(edit);
    edit.showAlerts = function(d) {
        helper.showAlerts(edit, d)
    }
    edit.renderPage = function(d) {
        if (d && d.data) {
            edit.item = d.data;
            if(!d.data.item_exists){
                edit.item.item_exists = false;
                return 0;
            }; 
            helper.updateTitle($state.current.name,edit.item.name);
            edit.showS3()
        }
    }
    edit.showTxt = {
        showMoreField: !1,
        drop: !1,
    };
    edit.showDrop = function() {
        if (edit.showTxt.drop == !1) {
            var data = angular.copy(edit.item.drop_info);
            data.do = 'misc-dropbox';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                edit.item.dropbox = r.data;
                edit.showTxt.drop = !0
            }, function(r) {
                var obj = {
                    "error": {
                        "error": helper.getLanguage("Problem connecting to Dropbox. Please try later.")
                    },
                    "notice": !1,
                    "success": !1
                };
                edit.showAlerts(obj)
            })
        } else {
            edit.showTxt.drop = !1
        }
    }
    edit.showS3 = function() {
        edit.show_load = !0;
        var data = angular.copy(edit.item.s3_info);
        data.do = 'misc-s3_link';
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.item.dropbox = r.data;
            edit.show_load = !1
        })
    }
    edit.deleteDropFile = function(index) {
        var data = angular.copy(edit.item.dropbox.dropbox_files[index].delete_file_link);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.dropbox.nr--;
                edit.item.dropbox.dropbox_files.splice(index, 1)
            }
        })
    }
    edit.deleteDropImage = function(index) {
        var data = angular.copy(edit.item.dropbox.dropbox_images[index].delete_file_link);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.dropbox.nr--;
                edit.item.dropbox.dropbox_images.splice(index, 1)
            }
        })
    }
    edit.openLightboxModal = function(index) {
        Lightbox.openModal(edit.item.dropbox.dropbox_images, index)
    };
    edit.uploadDoc = function(type) {
        var obj = angular.copy(edit.item.s3_info);
        obj.type = type;
        openModal('uploadDoc', obj, 'lg')
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'uploadDoc':
                var iTemplate = 'AmazonUpload';
                break
        }
        var params = {
            template: 'miscTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'uploadDoc':
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        edit.showS3()
                    }
                }
                break
        }
        modalFc.open(params)
    }
    edit.renderPage(data);
    edit.map = {
        center: angular.copy(edit.item.coords),
        zoom: 12,
        marker_id: 1,
        refresh: !0,
        marker_coord: edit.item.coords
    }
}]).controller('InstallationAddTemplateModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var add = this,
        typePromise;
    add.obj = data;
    add.close_modal = function() {
        $uibModalInstance.close()
    }
    add.showAlerts = function(d, formObj) {
        helper.showAlerts(add, d, formObj)
    }
    add.chooseTemplate = function(index) {
        if (add.obj.item.templates[index].checked) {
            for (x in add.obj.item.templates) {
                if (x != index) {
                    add.obj.item.templates[x].checked = ''
                }
            }
        }
    }
    add.ok = function(formObj) {
        var template_nr = 0;
        var ind = 0;
        for (x in add.obj.item.templates) {
            if (add.obj.item.templates[x].checked) {
                template_nr = add.obj.item.templates[x].template_id;
                ind = x
            }
        }
        if (!template_nr) {
            var obj = {
                'error': {
                    'error': helper.getLanguage('No template selected')
                },
                'notice': !1,
                'success': !1
            };
            add.showAlerts(obj);
            return !1
        } else if (!add.obj.item.templates[ind].alias) {
            var obj = {
                'data': {},
                'error': {}
            };
            obj.error['alias_' + template_nr] = helper.getLanguage('This field is required');
            add.showAlerts(obj, formObj);
            return !1
        }
        add.obj.item.template_id = add.obj.item.templates[ind].template_id;
        add.obj.item.name = add.obj.item.templates[ind].alias;
        add.obj.item.template = add.obj.item.templates[ind];
        $uibModalInstance.close()
    }
}]).controller('installationSettingsCtrl', ['$scope', 'helper', '$uibModal', 'data', function($scope, helper, $uibModal, data) {
    console.log('installation settings');
    var ls = this;
    ls.ConventionNum = convention;
    for (x in data) {
        ls[x] = data[x]
    }
    ls.dragFieldControlListeners = {
        orderChanged: function(event) {
            var d = {
                'do': 'installation--ifields-updateSortOrder',
                'fields': ls.siteFields.data
            };
            helper.doRequest('post', 'index.php', d, function(r) {
                helper.showAlerts(ls, r)
            })
        }
    }
    ls.load = function(h, k) {
        ls.alerts = [];
        var d = {
            'do': 'installation-settings',
            'xget': h
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            if (r.data) {
                for (x in r.data) {
                    ls[x] = r.data[x]
                }
            }
        })
    }
    ls.addField = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'installation-settings-ifields-addCustomField',
            'xget': 'siteFields'
        }, function(r) {
            for (x in r.data) {
                ls.siteFields[x] = r.data.data
            }
            helper.showAlerts(ls, r)
        })
    }
    ls.changeFieldName = function(item) {
        var obj = {};
        obj.do = 'installation--ifields-editNameCustomField';
        obj.field_id = item.field_id;
        obj.name = item.name;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            helper.showAlerts(ls, r)
        })
    }
    ls.editData = function(item) {
        var obj = {};
        obj.do = 'installation--ifields-editDataCustomField';
        obj.value = item.type;
        obj.field_id = item.field_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            helper.showAlerts(ls, r)
        })
    }
    ls.deleteField = function(item) {
        var obj = {};
        obj.do = 'installation-settings-ifields-deleteCustomField';
        obj.field_id = item.field_id;
        obj.xget = 'siteFields';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            for (x in r.data) {
                ls.siteFields[x] = r.data.data
            }
            helper.showAlerts(ls, r)
        })
    }

    function convention(ls, r) {
        var get = 'Convention';
        var data = {
            'do': 'installation-settings-installation-Convention',
            'xget': get,
            'account_number': ls.convention.account_number,
            'account_digits': ls.convention.account_digits
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            ls.convention = {
                account_number: r.data.convention.account_number,
                account_digits: r.data.convention.account_digits,
                example: r.data.convention.example
            };
            helper.showAlerts(ls, r)
        })
    }
}]).controller('selectInstallFieldModalCtrl', ['$scope', '$uibModalInstance', '$timeout', '$confirm', 'helper', 'modalFc', 'data', function($scope, $uibModalInstance, $timeout, $confirm, helper, modalFc, data) {
    var vmMod = this;
    vmMod.obj = data;
    $timeout(function() {
        modalFc.setHeight(angular.element('.modal.in'))
    })
    vmMod.ok = ok;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;
    vmMod.removeLine = removeLine;
    vmMod.openModal = openModal;

    function openModal(type, item, size, table, extra_id) {
        if (type == 'AddEntry') {
            var params = {
                template: 'miscTemplate/' + type,
                ctrl: 'AddEntryInstallFieldModalCtrl',
                size: size,
                backdrop: 'static'
            }
        }
        switch (type) {
            case 'AddEntry':
                params.params = {
                    'do': 'misc-AddEntryInstallField',
                    'id': item,
                    'field_id': vmMod.obj.field_id
                };
                params.callback = function(data) {
                    vmMod.obj.lines = data.data.lines
                }
                break
        }
        modalFc.open(params)
    }

    function ok() {
        var data = angular.copy(vmMod.obj);
        data.do = 'misc--misc-select_InstallField';
        helper.doRequest('post', 'index.php', data, function(d) {
            if (d.success) {
                $uibModalInstance.close(d)
            } else {
                vmMod.showAlerts(d)
            }
        })
    };

    function cancel() {
        $uibModalInstance.close(vmMod.obj)
    };

    function showAlerts(d) {
        helper.showAlerts(vmMod, d)
    }

    function removeLine($i) {
        if ($i > -1) {
            var data = {
                'id': vmMod.obj.lines[$i].id
            };
            data.do = 'misc--misc-delete_InstallField';
            $confirm({
                ok: helper.getLanguage('Yes'),
                cancel: helper.getLanguage('No'),
                text: helper.getLanguage('Are you sure, you wanna delete this entry?')
            }).then(function() {
                helper.doRequest('post', 'index.php', data, function(d) {
                    vmMod.obj.lines.splice($i, 1);
                    modalFc.setHeight(angular.element('.modal.in'))
                })
            }, function() {
                return !1
            })
        }
    }
}]).controller('AddEntryInstallFieldModalCtrl', ['$scope', '$uibModalInstance', '$timeout', 'helper', 'modalFc', 'data', function($scope, $uibModalInstance, $timeout, helper, modalFc, data) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;
    vmMod.ok = ok;
    if (vmMod.obj.id) {
        vmMod.obj.edit_id = !0
    } else {
        vmMod.obj.edit_id = !1
    }

    function ok() {
        var data = angular.copy(vmMod.obj);
        data.do = 'misc--misc-Add_InstallField';
        helper.doRequest('post', 'index.php', data, function(d) {
            if (d.success) {
                $uibModalInstance.close(d)
            } else {
                vmMod.showAlerts(d)
            }
        })
    };

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    };

    function showAlerts(d) {
        helper.showAlerts(vmMod, d)
    }
}])