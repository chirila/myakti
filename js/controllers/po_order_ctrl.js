app.controller('StockdispatchCtrl', ['$scope', 'helper', function($scope, helper) {
    $scope.serial_number = ''
}]).controller('StockdispatchViewCtrl', ['$scope', '$state', 'helper', '$stateParams', 'modalFc', 'data', function($scope, $state, helper, $stateParams, modalFc, data) {
    console.log('StockdispatchViewCtrl');
    $scope.stock_disp_id = $stateParams.stock_disp_id;
    $scope.$parent.isAddPage = !0;
    $scope.order = [];
    $scope.showTxt = {
        sendEmail: !1,
        markAsReady: !1,
        viewNotes: !1
    };
    $scope.email = {};
    $scope.auto = {
        options: {
            recipients: []
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1,
    };
    $scope.datePickOpen = {
        sent_date: !1
    }
    $scope.renderPage = function(d) {
        if (d.data) {
            $scope.stock_disp = d.data;
            if ($scope.stock_disp.sent_date) {
                $scope.stock_disp.sent_date = new Date($scope.stock_disp.sent_date)
            }
            $scope.$parent.serial_number = $scope.stock_disp.factur_nr;
            helper.updateTitle($state.current.name, $scope.stock_disp.factur_nr);
            $scope.$parent.type_title = $scope.stock_disp.type_title;
            $scope.$parent.nr_status = $scope.stock_disp.nr_status;
            $scope.auto.options.recipients = d.data.recipients;
            $scope.email = {
                e_subject: $scope.stock_disp.subject,
                e_message: $scope.stock_disp.e_message,
                email_language: $scope.stock_disp.languages,
                include_pdf: $scope.stock_disp.include_pdf,
                stock_disp_id: $scope.stock_disp_id,
                files: $scope.stock_disp.files,
                use_html: $scope.stock_disp.use_html,
                recipients: []

            };
            $scope.email.recipients = d.data.recipients_ids;
            /*for (x in d.data.recipients) {
                $scope.email.recipients.push(d.data.recipients[x].recipient_email)
            }*/
            $scope.stock_disp.dropbox={'dropbox_files':[],'dropbox_images':[]};
            $scope.showAlerts(d)
        }
    }
    $scope.showPreview = function(exp) {
        var params = {
            template: 'miscTemplate/modal',
            ctrl: 'viewRecepitCtrl',
            size: 'md',
            ctrlAs: 'vmMod',
            params: {
                'do': 'order-email_preview',
                order_id: $scope.order_id,
                message: exp.e_message,
                use_html: exp.use_html
            }
        }
        modalFc.open(params)
    };
    $scope.dispatchModal = function() {
        var params = {
            template: 'stocktemplate/dispatchModal',
            ctrl: 'DispatchModalCtrl',
            size: 'md',
            item: {
                'del_note': $scope.order.del_note,
                'do': 'order-order-order-addDispatchNote',
                'order_id': $scope.order_id
            },
            callback: function(data) {
                if (data) {
                    $scope.order.del_note_txt = data.data.del_note_txt;
                    $scope.order.del_note = data.data.del_note;
                    $scope.showAlerts(data)
                }
            }
        }
        modalFc.open(params)
    };
    $scope.sendEmails = function(r) {
        var d = angular.copy(r);
        $scope.showTxt.sendEmail = !1;
        if (d.recipients.length) {
            if (Array.isArray(d.recipients)) {
                d.new_email = d.recipients[0]
            } else {
                d.new_email = d.recipients
            }
            if (d.new_email) {
                $scope.email.alerts.push({
                    type: 'email',
                    msg: d.new_email + ' - '
                });
                d.do = 'stock--stock_dispatch-sendNew';
                d.use_html = $scope.email.use_html;
                d.lid = d.email_language;
                d.serial_number = $scope.order.serial_number;
                helper.doRequest('post', 'index.php', d, function(r) {
                    if (r.success && r.success.success) {
                        $scope.email.alerts[$scope.email.alerts.length - 1].type = 'success';
                        $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.success.success
                    }
                    if (r.notice && r.notice.notice) {
                        $scope.email.alerts[$scope.email.alerts.length - 1].type = 'info';
                        $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.notice.notice
                    }
                    if (r.error && r.error.error) {
                        $scope.email.alerts[$scope.email.alerts.length - 1].type = 'danger';
                        $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.error.error
                    }
                    if (Array.isArray(d.recipients)) {
                        d.recipients.shift();
                        $scope.sendEmails(d)
                    }
                })
            } else {
                if (Array.isArray(d.recipients)) {
                    d.recipients.shift();
                    $scope.sendEmails(d)
                }
            }
        } else {
            helper.refreshLogging();
        }

    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.contactForEmailAutoCfg = {
        valueField: 'recipient_email',
        labelField: 'contact_name_data',
        searchField: ['contact_name_data'],
        delimiter: '|',
        placeholder: 'Select recipient',
        create: !0,
        createOnBlur: true,
        maxItems: 100
    };
    $scope.markOrder = function(func) {
        helper.doRequest('post', 'index.php', {
            'do': 'stock-stock_dispatch-stock_dispatch-' + func,
            stock_disp_id: $scope.stock_disp_id
        }, $scope.renderPage)
    }
    $scope.openDeliveyModal = function(delivery_id) {
        var action = 'stock-stock_dispatch_line',
            params = {
                template: 'stocktemplate/deliveryStockDispatchModal',
                ctrl: 'deliveryStockDispatchModalCtrl',
                ctrlAs: '',
                backdrop: 'static',
                params: {
                    'do': action,
                    stock_disp_id: $scope.stock_disp_id
                },
                callback: function(data) {
                    if (data) {
                        if (data.data) {
                            $scope.stock_disp = data.data
                        }
                        $scope.showAlerts(data)
                    }
                }
            };
        modalFc.open(params)
    }
    $scope.openSendModal = function(delivery_id) {
        var params = {
            template: 'otemplate/sendModal',
            ctrl: 'sendModalCtrl',
            ctrlAs: '',
            params: {
                'do': 'order-send_email',
                order_id: $scope.order_id,
                delivery_id: delivery_id
            },
            callback: function(data) {
                $scope.showAlerts(data)
            }
        };
        modalFc.open(params)
    }
    $scope.deleteDelivery = function(link) {
        helper.doRequest('get', link, $scope.renderPage)
    }
    $scope.showPDFsettings = function(i) {
        var params = {
            template: 'otemplate/pdf_settings',
            ctrl: 'pdfSettingsOCtrl',
            ctrlAs: '',
            size: 'md',
            params: {
                'do': 'order-pdf_settings',
                porder: !0,
                p_order_id: $scope.p_order_id,
                xls: i
            },
            callback: function(data) {
                if (data) {
                    if (data.data) {
                        $scope.order = data.data
                    }
                    $scope.showAlerts(data)
                }
            }
        };
        modalFc.open(params)
    };
    $scope.removeDispatchNote = function() {
        helper.doRequest('post', 'index.php', {
            'del_note': '',
            'do': 'order-order-order-addDispatchNote',
            'order_id': $scope.order_id
        }, function(data) {
            if (data) {
                $scope.order.del_note_txt = data.data.del_note_txt;
                $scope.order.del_note = data.data.del_note;
                $scope.showAlerts(data)
            }
        })
    }

    $scope.statusDocument=function(status){
        var tmp_obj={'stock_disp_id':$scope.stock_disp_id};
        tmp_obj.do=status=='1' ? 'stock-stock_dispatch-stock_dispatch-archive': 'stock-stock_dispatch-stock_dispatch-activate'; 
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post','index.php',tmp_obj,function(r){
            if(r.success){
                $scope.renderPage(r);
                helper.refreshLogging();
            }else{
                $scope.showAlerts(r);
            }
        });
    }

    $scope.deleteDocument=function(){
        var tmp_obj={'stock_disp_id':$scope.stock_disp_id};
        tmp_obj.do='stock-stock_dispatching-stock_dispatch-delete'; 
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post','index.php',tmp_obj,function(r){
            if(r.success){
                helper.setItem('stock-stock_dispatching',{});
                $state.go("stock_dispatching");
            }else{
                $scope.showAlerts(r);
            }
        });
    }

    $scope.sendEmail = function() {
        var params={
            'do': 'stock-email_preview',
            stock_disp_id: $scope.stock_disp_id,
            message: $scope.email.e_message,
            use_html:$scope.stock_disp.use_html,
            send_email:true
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post','index.php',params,function(r){
            var viewEmail=angular.copy($scope.email);
            viewEmail.e_message=r.data.e_message;
            var obj={"app":"stock",
                "recipients":$scope.auto.options.recipients,
                "use_html":$scope.stock_disp.use_html,
                "is_file":$scope.stock_disp.is_file,
                "dropbox":$scope.stock_disp.dropbox,
                "email":viewEmail
            };
            openModal("send_email", angular.copy(obj), "lg");
        })      
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'send_email':
                var iTemplate = 'SendEmail';
                var ctas = 'vm';
                break;
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'send_email':
                params.template ='miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        $scope.sendEmails(d);
                    }
                }
                break;
        }
        modalFc.open(params)
    }

    $scope.renderPage(data)
}]).controller('StockdispatchingCtrl', ['$scope', 'helper', 'list', 'selectizeFc', 'data', function($scope, helper, list, selectizeFc, data) {
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    // $scope.views = [{
    //     name: helper.getLanguage('All Statuses'),
    //     id: 0,
    //     active: 'active'
    // }, {
    //     name: helper.getLanguage('Draft'),
    //     id: 1,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Own Company'),
    //     id: 2,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Other Company'),
    //     id: 3,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Archived'),
    //     id: -1
    // }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'stock-stock_dispatching',
        view: 0,
        xget: '',
        showAdvanced: !1,
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.listStats = [];
            $scope.lid = d.data.lid;
            $scope.type = d.data.pdf_type;
            $scope.lr = d.data.lr
            $scope.view_dd = d.data.view_dd
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1,
        stop_date: !1,
        d_start_date: !1,
        d_stop_date: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
        $scope.checkSearch()
    };
    $scope.checkSearch = function() {
        var data = angular.copy($scope.search),
            is_search = !1;
        data = helper.clearData(data, ['do', 'xget', 'showAdvanced', 'showList', 'offset']);
        for (x in data) {
            if (data[x]) {
                is_search = !0
            }
        }
        // vm.search.showList = is_search
    }
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filterCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('All Statuses'),
        onChange(value) {
            $scope.toggleSearchButtons('view', value)
        }
    });
    // $scope.filterCfg = {
    //     valueField: 'id',
    //     labelField: 'name',
    //     searchField: ['name'],
    //     delimiter: '|',
    //     placeholder: 'All Statuses',
    //     create: !1,
    //     onItemAdd(value, $item) {
    //         if (value == undefined) {
    //             value = 0
    //         }
    //         $scope.search.archived = 0;
    //         if (value == -1) {
    //             $scope.search.archived = 1
    //         }
    //         $scope.filters(value)
    //     },
    //     maxItems: 1
    // };
    list.init($scope, 'stock-stock_dispatching', $scope.renderList)
}]).controller('StockdispatchEditCtrl', ['$scope', 'helper', '$stateParams', '$state', '$uibModal', '$timeout', 'editItem', '$confirm', 'selectizeFc', 'modalFc', 'data', function($scope, helper, $stateParams, $state, $uibModal, $timeout, editItem, $confirm, selectizeFc, modalFc, data) {
    var edit = this,
        typePromise;
    edit.app = 'stock';
    edit.ctrlpag = 'nstock_dispatch';
    edit.tmpl = 'stocktemplate';
    $scope.$parent.isAddPage = !1;
    edit.disabled = {
        address: !0,
        daddress: !0
    };
    edit.item = {};
    edit.item.date_h = new Date();
    edit.item.del_date = new Date();
    edit.showWarning = !0;
    edit.removedCustomer = !1;
    edit.oldObj = {};
    edit.showEditFnc = showEditFnc;
    edit.showCCSelectize = showCCSelectize;
    edit.showtoCCSelectize = showtoCCSelectize;
    edit.calcTotalPurchase = calcTotalPurchase;
    edit.addArticle = addArticle;
    edit.calcTotal = calcTotal;
    edit.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    edit.item_id = $stateParams.stock_disp_id;
    edit.stock_disp_id = edit.item_id;
    edit.showTxt = {
        info: !0,
        address: !1,
        editRef: !1,
        editYRef: !1,
        addBtn: !0,
        addArticleBtn: !1
    };
    edit.auto = {
        options: {
            author: [],
            accmanager: [],
            contacts: [],
            customers: [],
            addresses: [],
            to_addresses: [],
            cc: [],
            to_cc:[],
            articles_list: []
        }
    };
    edit.articlesListAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code', 'supplier_reference', 'name2'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                if (item.allow_stock && item.is_service == 0) {
                    var stock_display = '';
                    if (item.hide_stock == 0) {
                        stock_display = '<div class="col-sm-4 text-right">' + (item.purchase_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                    }
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;-&nbsp;' + escape(item.name2) + '</small>' + '</div>' + stock_display + '</div>' + '</div>'
                } else {
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;-&nbsp;' + escape(item.name2) + '</small>' + '</div>' + '<div class="col-sm-4 text-right">' + (item.purchase_price) + '</div>' + '</div>' + '</div>'
                }
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span>' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'po_order-addArticle',
                    search: str,
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat
                };
                edit.requestAuto(edit, data, edit.renderArticleListAuto)
            }, 300)
        },
        onChange(value) {
            if (value == undefined) {} else if (value == '99999999999') {
                edit.openModal(edit, 'AddAnArticle', {
                    'do': 'po_order-addAnArticle',
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat
                }, 'lg');
                return !1
            } else {
                for (x in edit.auto.options.articles_list) {
                    if (edit.auto.options.articles_list[x].article_id == value) {
                        editItem.addArticle(edit, edit.auto.options.articles_list[x]);
                        edit.selected_article_id = '';
                        edit.showTxt.addArticleBtn = !1;
                        var data = {
                            'do': 'po_order-addArticle',
                            customer_id: edit.item.buyer_id,
                            lang_id: edit.item.email_language,
                            cat_id: edit.item.price_category_id,
                            remove_vat: edit.item.remove_vat
                        };
                        edit.requestAuto(edit, data, edit.renderArticleListAuto);
                        return !1
                    }
                }
            }
        },
        onItemAdd(value, $item) {
            if (value == '99999999999') {
                edit.selected_article_id = ''
            }
        },
        onBlur() {
            $timeout(function() {
                edit.showTxt.addArticleBtn = !1
            })
        },
        maxItems: 1
    };
    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {
        date: !1,
        delivery_date: !1
    }
    edit.renderPage = renderPage;
    edit.openDate = openDate;
    edit.removeLine = removeLine;
    edit.stockaddContentLine = stockaddContentLine;
    edit.save = save;
    edit.cancel = cancel;
    edit.showAlerts = showAlerts;
    edit.removeDeliveryAddr = removeDeliveryAddr;
    editItem.init(edit);
    if (edit.item_id == 'tmp') {
        edit.tmp = {
            'do': 'stock-nstock_dispatch',
            'buyer_id': 'tmp'
        };
        edit.init(edit, edit.tmp)
    } else {
        renderPage(data)
    }

    function renderPage(r) {
        if (r && r.data) {
            if (r.data.redirect) {
                $state.go(r.data.redirect);
                return !1
            }
            $scope.$parent.serial_number = r.data.ref_serial_number;
            helper.updateTitle($state.current.name, r.data.serial_number);
            for (x in r.data) {
                edit.item[x] = r.data[x]
            }
            edit.auto.options.contacts = r.data.contacts;
            edit.auto.options.customers = r.data.customers;
            edit.auto.options.addresses = r.data.addresses;
            if (r.data.to_addresses) {
                edit.auto.options.to_addresses = r.data.to_addresses
            }
            edit.auto.options.cc = r.data.cc;
            edit.auto.options.to_cc = r.data.to_cc;
            edit.auto.options.articles_list = r.data.articles_list.lines;
            if (edit.item.date_h) {
                edit.item.date_h = new Date(edit.item.date_h)
            }
            if (!edit.item.tr_id) {
                edit.item.tr_id = []
            }
        }
    }

    function showEditFnc() {
        edit.showTxt.address = !1;
        edit.oldObj = {
            buyer_id: angular.copy(edit.item.buyer_id),
            contact_id: angular.copy(edit.item.contact_id),
            contact_name: angular.copy(edit.item.contact_name),
            CUSTOMER_NAME: angular.copy(edit.item.customer_name),
            delivery_address: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address)),
            delivery_address_id: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_id)),
            delivery_address_txt: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_txt)),
            view_delivery: (edit.item.sameAddress ? !1 : angular.copy(edit.item.view_delivery)),
            sameAddress: angular.copy(edit.item.sameAddress),
        }
    }

    function showCCSelectize() {
        edit.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }

    function showtoCCSelectize() {
        edit.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#tocustandcont')[0].selectize;
            selectize.open()
        })
    }

    function calcTotalPurchase(type, $index) {
        editItem.calcTotalPurchase(edit.item, type, $index)
    }

    function openDate(p) {
        edit.datePickOpen[p] = !0
    }

    function removeDeliveryAddr() {
        edit.item.delivery_address = '';
        edit.item.delivery_address_id = ''
    }

    function removeLine($i) {
        var tr_id = edit.item.tr_id[$i].tr_id;
        if ($i > -1) {
            edit.item.tr_id.splice($i, 1);
            edit.calcTotalPurchase()
        }
    }

    function calcTotal(type, $index) {}

    function stockaddContentLine() {
        /*var continut = 'ceva';
        for (x in edit.item.tr_id) {
            if (edit.item.tr_id[x].content === !0) {
                continut = edit.item.tr_id[x].article
            }
        }
        if (!continut) {
            edit.openModal(edit, 'Alert', 'Please fill the \'Article\' field.', 'sm');
            return !1
        }*/
        var content = {
            content: !0,
            article: '',
            tr_id: "tmp" + (new Date().getTime()),
        }
        edit.item.tr_id.push(content)
    }

    function save() {
        angular.element('.loading_wrap').removeClass('hidden');
        var data = angular.copy(edit.item);
        data.do = data.do_next;
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.success) {
                $state.go('stock_dispatch.view', {
                    stock_disp_id: r.data.stock_disp_id
                })
            }
        })
    }

    function cancel() {
        if (edit.item_id == 'tmp') {
            $state.go('stock_dispatching')
        } else {
            $state.go('stock_dispatch.view', {
                stock_disp_id: edit.item_id
            })
        }
    }

    function showAlerts(d) {
        helper.showAlerts(edit, d)
    }

    function addArticle() {
        edit.showTxt.addArticleBtn = !0;
        $timeout(function() {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.open()
        })
    }

    edit.tinymceOptionsLines = {
        selector: "",
        resize: "both",
        auto_resize: !0,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code autoresize"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        autoresize_bottom_margin: 0,
        setup: function(editor) {
            editor.on('click', function(e) {
                e.stopPropagation();
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                angular.element(editor.editorContainer).parent().removeClass('wysiwyg-hide')
            }).on('blur', function(e) {
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                //edit.delaySave()
            })
        },
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
        content_css: [helper.base_url + 'css/tinymce_styles.css']
    };
    
}]).controller('StockdateCtrl', ['$scope', 'helper', 'list', 'data', function($scope, helper, list, data) {
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    $scope.views = [{
        name: helper.getLanguage('Movement type'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Article added'),
        id: 1,
        active: ''
    }, {
        name: helper.getLanguage('Article updated'),
        id: 2
    }, {
        name: helper.getLanguage('Delivery added'),
        id: 3,
        active: ''
    }, {
        name: helper.getLanguage('Delivery deleted'),
        id: 4
    }, {
        name: helper.getLanguage('Order deleted'),
        id: 5,
        active: ''
    }, {
        name: helper.getLanguage('Entry added'),
        id: 6
    }, {
        name: helper.getLanguage('Entry deleted'),
        id: 7,
        active: ''
    }, {
        name: helper.getLanguage('Purchase order deleted'),
        id: 8
    }, {
        name: helper.getLanguage('Delivery edited'),
        id: 9,
        active: ''
    }, {
        name: helper.getLanguage('Entry edited'),
        id: 10
    }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'stock-stock_date',
        view: 0,
        xget: '',
        showAdvanced: !1,
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.export_args = '';
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.current_item_stock = d.data.current_item_stock;
            $scope.items_stock_at_date = d.data.items_stock_at_date;
            $scope.stock_value_at_date = d.data.stock_value_at_date;
            $scope.current_value_of_stock = d.data.current_value_of_stock;
            $scope.listStats = [];
            $scope.lid = d.data.lid;
            $scope.type = d.data.pdf_type;
            $scope.lr = d.data.lr;
            $scope.EXPORT_HREF = d.data.EXPORT_HREF;
            $scope.export_args = d.data.export_args
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1,
        stop_date: !1,
        d_start_date: !1,
        d_stop_date: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.renderList(data)
}]).controller('StocklowCtrl', ['$scope', '$state', 'helper', 'selectizeFc', 'list', 'data', function($scope, $state, helper, selectizeFc, list, data) {
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    $scope.views = [{
        name: helper.getLanguage('Movement type'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Article added'),
        id: 1,
        active: ''
    }, {
        name: helper.getLanguage('Article updated'),
        id: 2
    }, {
        name: helper.getLanguage('Delivery added'),
        id: 3,
        active: ''
    }, {
        name: helper.getLanguage('Delivery deleted'),
        id: 4
    }, {
        name: helper.getLanguage('Order deleted'),
        id: 5,
        active: ''
    }, {
        name: helper.getLanguage('Entry added'),
        id: 6
    }, {
        name: helper.getLanguage('Entry deleted'),
        id: 7,
        active: ''
    }, {
        name: helper.getLanguage('Purchase order deleted'),
        id: 8
    }, {
        name: helper.getLanguage('Delivery edited'),
        id: 9,
        active: ''
    }, {
        name: helper.getLanguage('Entry edited'),
        id: 10
    }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'stock-stock_low',
        view: 0,
        xget: '',
        showAdvanced: !1,
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.supplier_exist = d.data.supplier_exist;
            $scope.family_exist = d.data.family_exist;
            $scope.max_rows = d.data.max_rows;
            $scope.listStats = [];
            $scope.lid = d.data.lid;
            $scope.type = d.data.pdf_type;
            $scope.lr = d.data.lr;
            $scope.initCheckAll();
        }
    };
    $scope.type_cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Supplier'),
        onChange(value) {
            $scope.search.offset = 1;
            $scope.toggleSearchButtons('supplier_id', value)
        }
    });
    $scope.type2_cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Family'),
        onChange(value) {
            $scope.search.offset = 1;
            $scope.toggleSearchButtons('article_category_id', value)
        }
    });
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1,
        stop_date: !1,
        d_start_date: !1,
        d_stop_date: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.showStats = function(item, $index) {
        var add = !1;
        if (!$scope.listStats[$index]) {
            $scope.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (add == !0) {
            helper.doRequest('get', 'index.php?do=stock-stock_article&xget=depo_list&article_id=' + item.article_id, function(r) {
                if (r.data) {
                    $scope.listStats[$index] = r.data
                }
            })
        }
        angular.element('.orderTable tbody tr.collapseTr').eq($index).find('.fa-chevron-right').toggleClass('fa-chevron-down');
        angular.element('.orderTable tbody tr.collapseTr').eq($index).next().toggleClass('hidden')
    }
    $scope.checkAll = function(type) {
        for (x in $scope.list) {
            $scope.list[x].checked = $scope.listObj.check_add_all
        }
    }
    $scope.createPorders = function(type) {
        var obj_global = {};
        obj_global.keys = [];
        for (x in $scope.list) {
            if ($scope.list[x].check_add_to_product == !0) {
                obj_global.keys.push($scope.list[x].article_id)
            }
        }
        if (obj_global.keys.length == 0) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Please select one or more items")
                },
                "notice": !1,
                "success": !1
            };
            $scope.showAlerts(obj)
        } else {
            var data = {};
            data = obj_global;
            $state.go('purchase_order.bulk', data)
        }
    }
    $scope.saveForPDF = function(p) {
        if (p) {
            if (p.check_add_to_product) {
                var elem_counter = 0;
                for (x in $scope.list) {
                    if ($scope.list[x].check_add_to_product) {
                        elem_counter++;
                    }
                }
                if (elem_counter == $scope.list.length && $scope.list.length > 0) {
                    $scope.listObj.check_add_all = true;
                }
            } else {
                $scope.listObj.check_add_all = false;
            }
        }
        list.saveForPDF($scope, 'po_order--po_order-saveAddToPurchase', p)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.initCheckAll = function() {
        var total_checked = 0;
        for (x in $scope.list) {
            if ($scope.list[x].check_add_to_product) {
                total_checked++;
            }
        }
        if ($scope.list.length > 0 && $scope.list.length == total_checked) {
            $scope.listObj.check_add_all = true;
        } else {
            $scope.listObj.check_add_all = false;
        }
    }

    $scope.renderList(data)
}]).controller('ToOrderCtrl', ['$scope', '$compile', 'helper', '$stateParams', '$state', '$uibModal', '$timeout', 'editItem', '$confirm', 'modalFc', 'list', 'data', 'selectizeFc', function($scope, $compile, helper, $stateParams, $state, $uibModal, $timeout, editItem, $confirm, modalFc, list, data, selectizeFc) {
    $scope.ord = '';
    $scope.max_rows = 0;
    $scope.list = [];
    $scope.list_order = [];
    $scope.listStats = {};
    $scope.listStats.orders = [];
    $scope.listStats.interventions = [];
    $scope.listStats.projects = [];
    $scope.search = {
        search: '',
        'do': 'stock-to_order',
        view: 0,
        xget: '',
        showAdvanced: !1,
        offset: 1
    };
    $scope.renderList = function(d) {
        if (d.data.query_order) {
            $scope.list_order = d.data.query_order;
            $scope.list_project = d.data.query_project;
            $scope.list_intervention = d.data.query_intervention;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.show_settings = d.data.show_settings;
            $scope.number_o = d.data.number_o;
            $scope.number_p = d.data.number_p;
            $scope.number_i = d.data.number_i;
            $scope.module_settings = d.data.module_settings;
        }
    };
    $scope.checkAll = function(type) {
        for (x in $scope.list_order) {
            $scope.list_order[x].checked = $scope.listObj.check_add_all
        }
        for (x in $scope.list_project) {
            $scope.list_project[x].checked = $scope.listObj.check_add_all
        }
        for (x in $scope.list_intervention) {
            $scope.list_intervention[x].checked = $scope.listObj.check_add_all
        }
    }
    $scope.listMoreOrder = function(index) {
        var add = !1;
        if (!$scope.listStats.orders[index]) {
            $scope.listStats.orders[index] = [];
            add = !0
        }
        if (add == !0) {
            var data = {};
            data.order_id = $scope.list_order[index].id;
            data.do = 'stock-items_articles';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', data, function(r) {
                $scope.listStats.orders[index] = r.data;
                showStats(index, 'orders')
            })
        } else {
            showStats(index, 'orders')
        }
    }
    $scope.listMoreProject = function(index) {
        var add = !1;
        if (!$scope.listStats.projects[index]) {
            $scope.listStats.projects[index] = [];
            add = !0
        }
        if (add == !0) {
            var data = {};
            data.project_id = $scope.list_project[index].id;
            data.do = 'stock-items_articles';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', data, function(r) {
                $scope.listStats.projects[index] = r.data;
                showStats(index, 'projects')
            })
        } else {
            showStats(index, 'projects')
        }
    }
    $scope.listMoreIntervention = function(index) {
        var add = !1;
        if (!$scope.listStats.interventions[index]) {
            $scope.listStats.interventions[index] = [];
            add = !0
        }
        if (add == !0) {
            var data = {};
            data.intervention_id = $scope.list_intervention[index].id;
            data.do = 'stock-items_articles';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', data, function(r) {
                $scope.listStats.interventions[index] = r.data;
                showStats(index, 'interventions')
            })
        } else {
            showStats(index, 'interventions')
        }
    }

    function showStats(index, type) {
        if ($scope.listStats[type][index].is_data) {
            var new_line = '<tr class="versionInfo"><td colspan="6" class="no_border">' + '<table class="table">' + '<thead>' + '<tr>' + '<th>' + '<div class="row">' + '<div class="col-sm-3">' + helper.getLanguage('Code') + '</div>' + '<div class="col-sm-5">' + helper.getLanguage('Article') + '</div>' + '<div class="col-sm-4 ">' + helper.getLanguage('Quantity') + '</div>' + '</div>' + '</th>' + '</tr>' + '</thead>' + '<tbody>' + '<tr ng-repeat="item in listStats.' + type + '[' + index + '].articles">' + '<td>' + '<div class="row">' + '<div class="col-sm-3 text-muted"><span ng-bind-html="item.article_code"></span></div>' + '<div class="col-sm-5 text-muted"><span ng-bind-html="item.article"></span></div>' + '<div class="col-sm-4 text-muted"><span ng-bind-html="item.quantity"></span></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>'
        } else {
            var new_line = '<tr class="versionInfo"><td colspan="3" class="no_border">' + '<table class="table">' + '<tbody>' + '<tr>' + '<td>' + '<div uib-alert type="notice" class="alert-warning">' + helper.getLanguage('No data to display') + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>'
        }
        angular.element('.orderTable tbody.' + type + ' tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.orderTable tbody.' + type + ' tr.collapseTr').eq(index).next())($scope);
        angular.element('.orderTable tbody.' + type + ' tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').addClass('hidden');
        angular.element('.orderTable tbody.' + type + ' tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').removeClass('hidden')
    }
    $scope.listLess = function(index, type) {
        angular.element('.orderTable tbody.' + type + ' tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable tbody.' + type + ' tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').removeClass('hidden');
        angular.element('.orderTable tbody.' + type + ' tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').addClass('hidden')
    }
    $scope.createPorders = function(type) {
        var obj_global = {};
        var app = 'orders';
        obj_global[app] = [];
        obj_global.keys = [];
        for (x in $scope.list_order) {
            if ($scope.list_order[x].checked == !0) {
                obj_global[app].push($scope.list_order[x].id);
                obj_global.keys.push($scope.list_order[x].type)
            }
        }
        for (x in $scope.list_project) {
            if ($scope.list_project[x].checked == !0) {
                obj_global[app].push($scope.list_project[x].id);
                obj_global.keys.push($scope.list_project[x].type)
            }
        }
        for (x in $scope.list_intervention) {
            if ($scope.list_intervention[x].checked == !0) {
                obj_global[app].push($scope.list_intervention[x].id);
                obj_global.keys.push($scope.list_intervention[x].type)
            }
        }
        if (obj_global[app].length == 0) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Please select one or more items")
                },
                "notice": !1,
                "success": !1
            };
            $scope.showAlerts(obj)
        } else {
            obj_global.selected_bulk = {};
            var data = {};
            for (x in obj_global.keys) {
                switch (obj_global.keys[x]) {
                    case 'orders':
                        if (!obj_global.selected_bulk['order_ids']) {
                            obj_global.selected_bulk['order_ids'] = [];
                        }
                        obj_global.selected_bulk['order_ids'].push(obj_global[app][x]);
                        break;
                    case 'projects':
                        if (!obj_global.selected_bulk['project_ids']) {
                            obj_global.selected_bulk['project_ids'] = [];
                        }
                        obj_global.selected_bulk['project_ids'].push(obj_global[app][x]);
                        break;
                    case 'interventions':
                        if (!obj_global.selected_bulk['intervention_ids']) {
                            obj_global.selected_bulk['intervention_ids'] = [];
                        }
                        obj_global.selected_bulk['intervention_ids'].push(obj_global[app][x]);
                        break;
                }
            }
            data = obj_global;
            openModal("progress_create_porder", data, "lg")
        }
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'progress_create_porder':
                var iTemplate = 'ProgressCreatePorder';
                var ctas = 'prog';
                break
        }
        var params = {
            template: 'stocktemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'progress_create_porder':
                params.request_type = 'post';
                params.initial_item = item;
                var tmp_obj = { 'do': 'stock-to_order', 'xget': 'create_po_articles' };
                for (x in item.selected_bulk) {
                    tmp_obj[x] = item.selected_bulk[x];
                }
                params.params = tmp_obj;
                params.callback = function(app) {
                    helper.doRequest('post', 'index.php', {
                        'do': 'stock-to_order'
                    }, $scope.renderList)
                }
                break
        }
        modalFc.open(params)
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.renderList(data)
}]).controller('ProgressCreatePorderModalCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'data', function($scope, $timeout, helper, $uibModalInstance, data) {
    var prog = this,
        typePromise;
    prog.obj = data;
    prog.start = !0;
    prog.value = 0;
    var i = 0;
    var increment = 0;
    prog.showAlerts = function(d) {
        helper.showAlerts(prog, d)
    }
    prog.ok = function() {
        var lines_counter = 0;
        var selected_lines = {};
        for (x in prog.obj.lines) {
            if (prog.obj.lines[x].supplier_id && prog.obj.lines[x].supplier_id != '0') {
                selected_lines[prog.obj.lines[x].article_id] = prog.obj.lines[x].supplier_id;
                lines_counter++;
            }
        }
        if (!lines_counter) {
            return false;
        }
        helper.doRequest('post', 'index.php', {
            'do': 'po_order--po_order-start_to_order'
        }, function(r) {
            prog.start = !1;
            var params = angular.copy(prog.obj);
            postIt(params, i, r.data.same_order_time, selected_lines)
        })
    };
    prog.cancel = function() {
        $uibModalInstance.close(prog.obj.post_list)
    };

    prog.filterCust = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Supplier'),
        create: !1,
        onDropdownOpen($dropdown) {
            var element_data = angular.element($dropdown.closest('tr'));
            prog.index = element_data[0].rowIndex
        },
        onDropdownClose($dropdown) {
            if (prog.obj.lines[prog.index - 1].supplier_id == '99999999999') {}
        },
        render: {
            option: function(item, escape) {
                var html = '<div class="row">' + '<div class="col-sm-12">' + '<p class="text-ellipsis">' + escape(item.name) + ' </p>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            var element_data = angular.element('#po_articles_list tbody tr:nth-child(' + prog.index + ') selectize');
            var selectize = element_data[0].selectize;
            if (value == undefined) {
                prog.obj.lines[prog.index - 1].add_p_order = undefined;
                var obj = {
                    'do': 'stock-to_order',
                    'xget': 'cc'
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    prog.obj.lines[prog.index - 1].supplier_dd = r.data;
                    selectize.open()
                })
            } else {
                prog.obj.lines[prog.index - 1].add_p_order = !0
            }
        },
        onType(value) {
            var element_data = angular.element('#po_articles_list tbody tr:nth-child(' + prog.index + ') selectize');
            var selectize = element_data[0].selectize;
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                if (value != undefined) {
                    var obj = {
                        'do': 'stock-to_order',
                        'xget': 'cc',
                        'term': value
                    };
                    helper.doRequest('get', 'index.php', obj, function(r) {
                        prog.obj.lines[prog.index - 1].supplier_dd = r.data;
                        if (prog.obj.lines[prog.index - 1].supplier_dd.length) {
                            selectize.open()
                        }
                    })
                }
            }, 300)
        },
        maxItems: 1
    };

    function postIt(params, i, time, selected_lines) {
        if (params.orders.length) {
            var data = {};
            data.do = 'po_order--po_order-to_order';
            data.type = params.keys[0];
            data.item = params.orders[0];
            data.selected_articles = selected_lines;
            data.same_time = time;
            data.debug = 1;
            //angular.element('.loading_alt').removeClass('hidden');
            helper.doRequest("post", "index.php", data, function(o) {
                i++;
                var percent = i * 100 / prog.obj.orders.length;
                prog.value = parseInt(percent.toFixed(0));
                if (o.data.serial_number == 'no_number') {
                    if (o.data.actiune_id == '1') {
                        increment++
                    }
                    o.data.serial_number = increment.toString()
                }
                if (o.error.error) {
                    var obj = {
                        "error": {
                            "error": helper.getLanguage("Purchase Order no") + ": " + o.data.serial_number + ' - ' + o.data.actiune + ' ' + o.error.error
                        },
                        "notice": !1,
                        "success": !1
                    };
                    prog.showAlerts(obj)
                } else if (!o.error && !o.notice) {
                    for (x in o.data.actiune) {
                        var obj = {
                            "error": !1,
                            "notice": !1,
                            "success": {
                                "success": helper.getLanguage("Purchase Order no") + ": " + o.data.actiune[x][1] + ' - ' + o.data.actiune[x][0]
                            }
                        };
                        prog.showAlerts(obj)
                    }
                }
                params.orders.shift();
                params.keys.shift();
                postIt(params, i, time, selected_lines)
            })
        } else {
            //angular.element('.loading_alt').addClass('hidden')
        }
    }
}]).controller('StockcustomerCtrl', ['$scope', 'helper', 'list', 'data', function($scope, helper, list, data) {
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    $scope.views = [{
        name: helper.getLanguage('Movement type'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Article added'),
        id: 1,
        active: ''
    }, {
        name: helper.getLanguage('Article updated'),
        id: 2
    }, {
        name: helper.getLanguage('Delivery added'),
        id: 3,
        active: ''
    }, {
        name: helper.getLanguage('Delivery deleted'),
        id: 4
    }, {
        name: helper.getLanguage('Order deleted'),
        id: 5,
        active: ''
    }, {
        name: helper.getLanguage('Entry added'),
        id: 6
    }, {
        name: helper.getLanguage('Entry deleted'),
        id: 7,
        active: ''
    }, {
        name: helper.getLanguage('Purchase order deleted'),
        id: 8
    }, {
        name: helper.getLanguage('Delivery edited'),
        id: 9,
        active: ''
    }, {
        name: helper.getLanguage('Entry edited'),
        id: 10
    }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'stock-stock_customer',
        view: 0,
        xget: '',
        showAdvanced: !1,
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.export_args = '';
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.listStats = [];
            $scope.lid = d.data.lid;
            $scope.type = d.data.pdf_type;
            $scope.lr = d.data.lr;
            $scope.export_args = d.data.export_args
            $scope.minimum_selected=d.data.minimum_selected;
            $scope.all_pages_selected=d.data.all_pages_selected;
            $scope.id=d.data.id;
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1,
        stop_date: !1,
        d_start_date: !1,
        d_stop_date: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.showStats = function(item, $index) {
        var add = !1;
        if (!$scope.listStats[$index]) {
            $scope.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (add == !0) {
            helper.doRequest('get', 'index.php?do=stock-stock_customer&xget=stock_list&address_id=' + item.address_id + '&customer_id=' + item.customer_id, function(r) {
                if (r.data) {
                    $scope.listStats[$index] = r.data
                }
            })
        }
        angular.element('.orderTable tbody tr.collapseTr').eq($index).find('.fa-chevron-right').toggleClass('fa-chevron-down');
        angular.element('.orderTable tbody tr.collapseTr').eq($index).next().toggleClass('hidden')
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.markCheckNew=function(i){
        list.markCheckNew($scope, 'stock-stock_customer-stock_dispatch-saveAddToExport', i);
    }
    $scope.checkAllPage = function(){
        $scope.listObj.check_add_all=true;
        $scope.markCheckNew();
    }
    $scope.checkAllPages=function(){
        list.markCheckNewAll($scope, 'stock-stock_customer-stock_dispatch-saveAddToExportAll','1');
    }
    $scope.uncheckAllPages=function(){
        list.markCheckNewAll($scope, 'stock-stock_customer-stock_dispatch-saveAddToExportAll','0');
    }
    $scope.createExportsOnLocation = function($event) {
        list.exportFnc($scope, $event)
        var params = angular.copy($scope.search);
        params.do = 'stock-stock_customer'
        params.exported = !0;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r);
        })
    };
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.renderList(data)
}]).controller('StockarticleCtrl', ['$scope', 'helper', 'list', 'data', function($scope, helper, list, data) {
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    $scope.views = [{
        name: helper.getLanguage('Movement type'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Article added'),
        id: 1,
        active: ''
    }, {
        name: helper.getLanguage('Article updated'),
        id: 2
    }, {
        name: helper.getLanguage('Delivery added'),
        id: 3,
        active: ''
    }, {
        name: helper.getLanguage('Delivery deleted'),
        id: 4
    }, {
        name: helper.getLanguage('Order deleted'),
        id: 5,
        active: ''
    }, {
        name: helper.getLanguage('Entry added'),
        id: 6
    }, {
        name: helper.getLanguage('Entry deleted'),
        id: 7,
        active: ''
    }, {
        name: helper.getLanguage('Purchase order deleted'),
        id: 8
    }, {
        name: helper.getLanguage('Delivery edited'),
        id: 9,
        active: ''
    }, {
        name: helper.getLanguage('Entry edited'),
        id: 10
    }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'stock-stock_article',
        view: 0,
        xget: '',
        showAdvanced: !1,
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.isOpen = !1;
    $scope.export_args = '';
    $scope.export_args_location = '';
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.listStats = [];
            $scope.lid = d.data.lid;
            $scope.type = d.data.pdf_type;
            $scope.lr = d.data.lr;
            $scope.export_args = d.data.export_args;
            $scope.export_args_location = d.data.export_args_location;
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1,
        stop_date: !1,
        d_start_date: !1,
        d_stop_date: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.showStats = function(item, $index) {
        var add = !1;
        if (!$scope.listStats[$index]) {
            $scope.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (add == !0) {
            helper.doRequest('get', 'index.php?do=stock-stock_article&xget=depo_list&article_id=' + item.article_id, function(r) {
                if (r.data) {
                    $scope.listStats[$index] = r.data
                }
            })
        }
        angular.element('.orderTable tbody tr.collapseTr').eq($index).find('.glyphicon-plus').toggleClass('glyphicon-minus');
        angular.element('.orderTable tbody tr.collapseTr').eq($index).next().toggleClass('hidden')
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.renderList(data)
}]).controller('StockinfoCtrl', ['$scope', 'helper', 'list', 'selectizeFc', 'data', 'modalFc', function($scope, helper, list, selectizeFc, data, modalFc) {
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    // $scope.views = [{
    //     name: helper.getLanguage('Movement type'),
    //     id: 0,
    //     active: 'active'
    // }, {
    //     name: helper.getLanguage('Article added'),
    //     id: 1,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Article updated'),
    //     id: 2
    // }, {
    //     name: helper.getLanguage('Delivery added'),
    //     id: 3,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Delivery deleted'),
    //     id: 4
    // }, {
    //     name: helper.getLanguage('Order deleted'),
    //     id: 5,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Entry added'),
    //     id: 6
    // }, {
    //     name: helper.getLanguage('Entry deleted'),
    //     id: 7,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Purchase order deleted'),
    //     id: 8
    // }, {
    //     name: helper.getLanguage('Delivery edited'),
    //     id: 9,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Entry edited'),
    //     id: 10
    // }, {
    //     name: helper.getLanguage('Return added'),
    //     id: 13
    // }];
    $scope.activeView = 0;
    $scope.export_args = '';
    $scope.list = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'stock-stock_info',
        view: 0,
        xget: '',
        showAdvanced: !1,
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.listStats = [];
            $scope.lid = d.data.lid;
            $scope.type = d.data.pdf_type;
            $scope.lr = d.data.lr;
            $scope.export_args = d.data.export_args;
            $scope.columns=d.data.columns;
            $scope.view_dd=d.data.view_dd;
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1,
        stop_date: !1,
        d_start_date: !1,
        d_stop_date: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
        $scope.checkSearch()
    };
    $scope.checkSearch = function() {
        var data = angular.copy($scope.search),
            is_search = !1;
        data = helper.clearData(data, ['do', 'xget', 'showAdvanced', 'showList', 'offset']);
        for (x in data) {
            if (data[x]) {
                is_search = !0
            }
        }
        // vm.search.showList = is_search
    }
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };

    $scope.filterCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Movement type'),
        onChange(value) {
            $scope.toggleSearchButtons('view', value)
        }
    });

    // $scope.filterCfg = {
    //     valueField: 'id',
    //     labelField: 'name',
    //     searchField: ['name'],
    //     delimiter: '|',
    //     placeholder: 'All Statuses',
    //     create: !1,
    //     onItemAdd(value, $item) {
    //         if (value == undefined) {
    //             value = 0
    //         }
    //         $scope.search.archived = 0;
    //         if (value == -1) {
    //             $scope.search.archived = 1
    //         }
    //         $scope.filters(value)
    //     },
    //     maxItems: 1
    // };

    $scope.columnSettings = function() {
        $scope.openModal('column_set', {}, 'lg')
    }

    $scope.openModal = function(type, item, size) {
        var params = {
            template: 'stocktemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'viewStock':
                params.template = 'stocktemplate/' + type + 'Modal';
                params.ctrlAs = type;
                params.item = item;
                break
            case 'column_set':
                params.template = 'miscTemplate/ColumnSettingsModal';
                params.ctrl='ColumnSettingsModalCtrl';
                params.ctrlAs = 'vm';
                params.params = {
                    'do': 'misc-column_set',
                    'list': 'stock_transactions'
                };
                params.callback = function(d) {
                    if (d) {
                        $scope.searchThing();
                    }
                }
                break;
        }
        modalFc.open(params)
    }
    list.init($scope, 'stock-stock_info', $scope.renderList)
}]).controller('viewStockModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', 'selectizeFc', 'modalFc', '$compile', function($scope, helper, $uibModalInstance, data, list, selectizeFc, modalFc, $compile) {
    var viewStock = this;
    viewStock.item = data;
    viewStock.showAlerts = showAlerts;
    viewStock.cancel = function() {
        $uibModalInstance.close()
    };

    function showAlerts(d, formObj) {
        helper.showAlerts(viewStock, d, formObj)
    }
}]).controller('PurchaseordersCtrl', ['$scope', '$rootScope','$confirm', 'helper', 'list', 'data', 'modalFc', function($scope, $rootScope, $confirm, helper, list, data, modalFc) {
    console.log('PO Orders list');
    $rootScope.reset_lists.purchase_orders=true;
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    $scope.views = [];
    $scope.activeView = 0;
    $scope.show_load = !0;
    $scope.list = [];
    $scope.nav = [];
    $scope.listStats = [];
    $scope.invoice_statuses=[];
    $scope.search = {
        search: '',
        'do': 'po_order-po_orders',
        created_from: '',
        view: 0,
        xget: '',
        showAdvanced: !1,
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.minimum_selected=false;
    $scope.all_pages_selected=false;
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.listStats = [];
            $scope.lid = d.data.lid;
            $scope.type = d.data.pdf_type;
            $scope.lr = d.data.lr;
            $scope.post_active = d.data.post_active;
            $scope.columns=d.data.columns;
            $scope.views=d.data.views;
            $scope.minimum_selected=d.data.minimum_selected;
            $scope.all_pages_selected=d.data.all_pages_selected;
            $scope.invoice_statuses=d.data.invoice_statuses;
            $scope.nav = d.data.nav;
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['p_order_id']);
                count ++;
            }
            sessionStorage.setItem('purchase_order.view_total', count);
            sessionStorage.setItem('purchase_order.view_list', JSON.stringify(ids));
            //console.log(ids);
            //$scope.initCheckAll();
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1,
        stop_date: !1,
        d_start_date: !1,
        d_stop_date: !1
    }
    $scope.archived = function(value) {
        $scope.search.archived = value;
        list.filter($scope, value, $scope.renderList, true)
    };
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList, true)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList, true)
    };
    $scope.searchThing = function(reset_list) {
        angular.element('.loading_wrap').removeClass('hidden');
        var params=angular.copy($scope.search);
        if(reset_list){
            params.reset_list='1';
        }
        list.search(params, $scope.renderList, $scope)
    };
    $scope.resetAdvanced = function() {
        $scope.search.article_name_search = undefined;
        $scope.search.your_reference_search = undefined;
        $scope.search.associated_customer_search = undefined;
        $scope.search.showAdvanced = !1;
        $scope.searchThing(true)
    };
    $scope.saveForPDF = function(i) {
        if (i) {
            if (i.check_add_to_product) {
                var elem_counter = 0;
                for (x in $scope.list) {
                    if ($scope.list[x].check_add_to_product) {
                        elem_counter++;
                    }
                }
                if (elem_counter == $scope.list.length && $scope.list.length > 0) {
                    $scope.listObj.check_add_all = true;
                }
            } else {
                $scope.listObj.check_add_all = false;
            }
        }
        list.saveForPDF($scope, 'po_order--po_order-saveAddToPdf', i)
    };
    $scope.markCheckNew=function(i){
        list.markCheckNew($scope, 'po_order--po_order-saveAddToPdf', i);
    }
    $scope.checkAllPage=function(){
        $scope.listObj.check_add_all=true;
        $scope.markCheckNew();
    }
    $scope.checkAllPages=function(){
        list.markCheckNewAll($scope, 'po_order--po_order-saveAddToPdfAll','1');
    }
    $scope.uncheckAllPages=function(){
        list.markCheckNewAll($scope, 'po_order--po_order-saveAddToPdfAll','0');
    }
    $scope.createExports = function($event) {
        list.exportFnc($scope, $event)
        var params = angular.copy($scope.search);
        params.do = 'po_order-po_orders'
        params.exported = !0;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r);
        })
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.invoiceCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Invoice Statuses'),
        create: !1,
        onChange(value){
            if (value == undefined) {
                value = 0
            }
            $scope.search.invoice_status_id=value;
            $scope.searchThing(true);
        },
        maxItems: 1
    };
    $scope.searchThing = function(reset_list) {
        angular.element('.loading_wrap').removeClass('hidden');
        var params=angular.copy($scope.search);
        if(reset_list){
            params.reset_list='1';
        }
        list.search(params, $scope.renderList, $scope)
    };
    $scope.removeInvoiceStatus = function(){
        $scope.search.invoice_status_id=0;
        $scope.invoice_status_id=0;
        $scope.searchThing(true);
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.greenpost = function(item) {
        if ($scope.post_active) {
            var data = {};
            for (x in $scope.search) {
                data[x] = $scope.search[x]
            }
            data.do = 'po_order-po_orders-po_order-postIt';
            data.id = item.id;
            data.related = 'list';
            data.no_status = !0;
            data.post_library = $scope.post_library;
            openModal("resend_post", data, "md")
        } else {
            var data = {};
            data.clicked_item = item;
            for (x in $scope.search) {
                data[x] = $scope.search[x]
            }
            openModal("post_register", data, "md")
        }
    }
    $scope.resendPost = function(item) {
        var data = {};
        for (x in $scope.search) {
            data[x] = $scope.search[x]
        }
        data.do = 'po_order-po_orders-po_order-postIt';
        data.id = item.id;
        data.related = 'list';
        data.post_library = $scope.post_library;
        openModal("resend_post", data, "md")
    }

    $scope.columnSettings = function() {
        openModal('column_set', {}, 'lg')
    }

    $scope.initCheckAll = function() {
        var total_checked = 0;
        for (x in $scope.list) {
            if ($scope.list[x].check_add_to_product) {
                total_checked++;
            }
        }
        if ($scope.list.length > 0 && $scope.list.length == total_checked) {
            $scope.listObj.check_add_all = true;
        } else {
            $scope.listObj.check_add_all = false;
        }
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'post_register':
                var iTemplate = 'PostRegister';
                var ctas = 'post';
                break;
            case 'resend_post':
                var iTemplate = 'ResendPostGreen';
                var ctas = 'post';
                break;
            case 'column_set':
                var iTemplate = 'ColumnSettings';
                var ctas = 'vm';
                break;
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'post_register':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.item.do = 'po_order-po_orders-po_order-postRegister';
                params.callback = function(data) {
                    if (data && data.data) {
                        $scope.renderList(data);
                        $scope.greenpost(data.clicked_item)
                    }
                    $scope.showAlerts(data)
                }
                break;
            case 'resend_post':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.request_type = 'post';
                params.params = {
                    'do': 'po_order--po_order-postOrderData',
                    'p_order_id': item.id,
                    'related': 'status',
                    'old_obj': item
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.renderList(d);
                        $scope.showAlerts(d)
                    }
                }
                break;
            case 'column_set':
                params.template = 'miscTemplate/ColumnSettingsModal';
                params.params = {
                    'do': 'misc-column_set',
                    'list': 'purchase_orders'
                };
                params.callback = function(d) {
                    if (d) {
                        $scope.searchThing();
                    }
                }
                break;
        }
        modalFc.open(params)
    }
    list.init($scope, 'po_order-po_orders', $scope.renderList,($rootScope.reset_lists.purchase_orders ? true : false));
    $rootScope.reset_lists.purchase_orders=false;
    $scope.show_load = !0;
    helper.doRequest('get', 'index.php', {
        'do': 'misc-post_library'
    }, function(r) {
        $scope.show_load = !1;
        $scope.post_library = r.data.post_library
    })
}]).controller('PurchaseOrdersDeliveriesCtrl', ['$scope', '$window', 'helper', 'list', function($scope, $window, helper, list) {
    console.log('POrders deliveries list');

    $scope.archiveList = 1;
    $scope.reverse = !0;
    $scope.ord = "delivery_date";
    $scope.max_rows = 0;
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'po_order-deliveries',
        view: 0,
        xget: '',
        showAdvanced: !1,
        offset: 1,
        'item_code': '',
        'start_date': undefined,
        'stop_date': undefined
    };
    $scope.listObj = {
        check_add_all: !1
    };

    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1,
        stop_date: !1,
        d_start_date: !1,
        d_stop_date: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };

    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = parseInt(d.data.max_rows);
            $scope.lr = parseInt(d.data.lr);
        }
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }

    $scope.exportDeliveries = function() {
        var data = angular.copy($scope.search);
        delete data.do;
        delete data.xget;
        $window.open('index.php?do=po_order-export_selected_deliveries&export_data=' + encodeURI(btoa(JSON.stringify(data))), '_blank');
    }

    list.init($scope, 'po_order-deliveries', $scope.renderList);

}]).controller('PurchaseorderBulkCtrl', ['$scope', 'helper', '$stateParams', '$state', '$timeout', '$confirm', 'modalFc', 'data', function($scope, helper, $stateParams, $state, $timeout, $confirm, modalFc, data) {
    $scope.order_id = $stateParams.order_id;
    $scope.service_id = $stateParams.service_id;
    var vm = this,
        typePromise;
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.filterCust = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Supplier'),
        create: !1,
        onDropdownOpen($dropdown) {
            var element_data = angular.element($dropdown.closest('tr'));
            $scope.order.index = element_data[0].rowIndex
        },
        onDropdownClose($dropdown) {
            if ($scope.order.list[$scope.order.index - 1].supplier_id == '99999999999') {}
        },
        render: {
            option: function(item, escape) {
                var html = '<div class="row">' + '<div class="col-sm-12">' + '<p class="text-ellipsis">' + escape(item.name) + ' </p>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            var element_data = angular.element('#contacts_list tbody tr:nth-child(' + $scope.order.index + ') selectize');
            var selectize = element_data[0].selectize;
            if (value == undefined) {
                $scope.order.list[$scope.order.index - 1].add_p_order = undefined;
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', {
                    'do': 'customers-nylas_contacts',
                    'xget': 'cc',
                    'supplier': '1'
                }, function(r) {
                    $scope.order.list[$scope.order.index - 1].supplier_dd = r.data;
                    selectize.open()
                })
            } else {
                $scope.order.list[$scope.order.index - 1].add_p_order = !0
            }
        },
        onType(value) {
            var element_data = angular.element('#contacts_list tbody tr:nth-child(' + $scope.order.index + ') selectize');
            var selectize = element_data[0].selectize;
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                if (value != undefined) {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php', {
                        'do': 'customers-nylas_contacts',
                        'xget': 'cc',
                        'term': value,
                        'supplier': '1'
                    }, function(r) {
                        $scope.order.list[$scope.order.index - 1].supplier_dd = r.data;
                        if ($scope.order.list[$scope.order.index - 1].supplier_dd.length) {
                            selectize.open()
                        }
                    })
                }
            }, 300)
        },
        maxItems: 1
    };

    function save() {
        //angular.element('.loading_wrap').removeClass('hidden');
        var data = {
            list: $scope.order.list
        };
        data.do = $scope.order.do_next;
        data.order_id = $scope.order.order_id;
        data.service_id = $scope.service_id;
        data.add_to_purchase = $scope.order.add_to_purchase;
        data.cost_centre = $scope.order.cost_centre;
        data.type_id = $scope.order.type_id;
        data.source_id = $scope.order.source_id;
        data.segment_id = $scope.order.segment_id;
        if ($scope.service_id) {
            data.add_to_purchase = '1'
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.error) {
                $scope.showAlerts(r)
            }
            if (r && r.success) {
                $state.go('purchase_orders')
            }
        })
    }

    function cancel() {
        if ($scope.order_id) {
            $state.go('order.view', {
                order_id: $scope.order_id
            })
        } else if ($scope.service_id) {
            $state.go('service.view', {
                service_id: $scope.service_id
            })
        } else {
            $state.go('orders')
        }
    }
    $scope.renderPage = function(d) {
        $scope.order = d.data;
        if ($scope.order.serial_number) {
            $scope.$parent.bulk_info = helper.getLanguage('based on order#') + $scope.order.serial_number
        }
        if (d.data) {
            $scope.order = d.data;
            $scope.order.list = d.data.lines;
            $scope.order.index = 0
        }
        $scope.initCheckAll();
    }
    $scope.order_act = function(item) {
        if (item.supplier_id == 0) {
            $confirm({
                text: helper.getLanguage('Select First the Supplier')
            });
            item.add_p_order = !1
        }
        if (item.add_p_order && item.supplier_id > 0) {
            var elem_counter = 0;
            for (x in $scope.order.list) {
                if ($scope.order.list[x].add_p_order) {
                    elem_counter++;
                }
            }
            if (elem_counter == $scope.order.list.length && $scope.order.list.length > 0) {
                $scope.listObj.check_add_all = true;
            }
        } else {
            $scope.listObj.check_add_all = false;
        }
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }

    $scope.checkAll = function() {
        for (x in $scope.order.list) {
            $scope.order.list[x].add_p_order = $scope.listObj.check_add_all;
        }
    }
    $scope.initCheckAll = function() {
        var total_checked = 0;
        for (x in $scope.order.list) {
            if ($scope.order.list[x].add_p_order) {
                total_checked++;
            }
        }
        if ($scope.order.list.length > 0 && $scope.order.list.length == total_checked) {
            $scope.listObj.check_add_all = true;
        } else {
            $scope.listObj.check_add_all = false;
        }
    }

    $scope.renderPage(data)
}]).controller('PurchaseorderBulkFacqCtrl', ['$scope', 'helper', '$stateParams', '$state', '$timeout', '$confirm', 'modalFc', 'data', function($scope, helper, $stateParams, $state, $timeout, $confirm, modalFc, data) {
    $scope.order_id = $stateParams.order_id;
    $scope.service_id = $stateParams.service_id;
    $scope.quote_id = $stateParams.quote_id;

    var vm = this,
        typePromise;
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.filterCust = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Supplier'),
        create: !1,
        onDropdownOpen($dropdown) {
            var element_data = angular.element($dropdown.closest('tr'));
            $scope.order.index = element_data[0].rowIndex
        },
        onDropdownClose($dropdown) {
            if ($scope.order.list[$scope.order.index - 1].supplier_id == '99999999999') {}
        },
        render: {
            option: function(item, escape) {
                var html = '<div class="row">' + '<div class="col-sm-12">' + '<p class="text-ellipsis">' + escape(item.name) + ' </p>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            var element_data = angular.element('#contacts_list tbody tr:nth-child(' + $scope.order.index + ') selectize');
            var selectize = element_data[0].selectize;
            if (value == undefined) {
                $scope.order.list[$scope.order.index - 1].add_p_order = undefined;
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', {
                    'do': 'customers-nylas_contacts',
                    'xget': 'cc',
                    'supplier': '1'
                }, function(r) {
                    $scope.order.list[$scope.order.index - 1].supplier_dd = r.data;
                    selectize.open()
                })
            } else {
                $scope.order.list[$scope.order.index - 1].add_p_order = !0
            }
        },
        onType(value) {
            var element_data = angular.element('#contacts_list tbody tr:nth-child(' + $scope.order.index + ') selectize');
            var selectize = element_data[0].selectize;
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                if (value != undefined) {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php', {
                        'do': 'customers-nylas_contacts',
                        'xget': 'cc',
                        'term': value,
                        'supplier': '1'
                    }, function(r) {
                        $scope.order.list[$scope.order.index - 1].supplier_dd = r.data;
                        if ($scope.order.list[$scope.order.index - 1].supplier_dd.length) {
                            selectize.open()
                        }
                    })
                }
            }, 300)
        },
        maxItems: 1
    };

    function save() {
        //angular.element('.loading_wrap').removeClass('hidden');
        var data = {
            list: $scope.order.list
        };
        data.do = $scope.order.do_next;
        data.order_id = $scope.order.order_id;
        data.service_id = $scope.service_id;
        data.quote_id = $scope.quote_id;
        data.add_to_purchase = $scope.order.add_to_purchase;
        data.cost_centre = $scope.order.cost_centre;
        data.type_id = $scope.order.type_id;
        data.source_id = $scope.order.source_id;
        data.segment_id = $scope.order.segment_id;
        if ($scope.service_id) {
            data.add_to_purchase = '1'
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.error) {
                $scope.showAlerts(r)
            }
            if (r && r.success) {
                $state.go('purchase_orders')
            }
        })
    }

    function cancel() {
        if ($scope.order_id) {
            $state.go('order.view', {
                order_id: $scope.order_id
            })
        } else if ($scope.service_id) {
            $state.go('service.view', {
                service_id: $scope.service_id
            })
        } else {
            $state.go('orders')
        }
    }
    $scope.renderPage = function(d) {

        $scope.order = d.data;
        if ($scope.order.serial_number) {
            $scope.$parent.bulk_info = helper.getLanguage('based on order#') + $scope.order.serial_number
        }
        if (d.data) {
            $scope.order = d.data;
            $scope.order.list = d.data.lines;
            $scope.order.index = 0
        }
        $scope.initCheckAll();
    }
    $scope.order_act = function(item) {
        if (item.supplier_id == 0) {
            $confirm({
                text: helper.getLanguage('Select First the Supplier')
            });
            item.add_p_order = !1
        }
        if (item.add_p_order && item.supplier_id > 0) {
            var elem_counter = 0;
            for (x in $scope.order.list) {
                if ($scope.order.list[x].add_p_order) {
                    elem_counter++;
                }
            }
            if (elem_counter == $scope.order.list.length && $scope.order.list.length > 0) {
                $scope.listObj.check_add_all = true;
            }
        } else {
            $scope.listObj.check_add_all = false;
        }
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }

    $scope.checkAll = function() {
        for (x in $scope.order.list) {
            $scope.order.list[x].add_p_order = $scope.listObj.check_add_all;
        }
    }
    $scope.initCheckAll = function() {
        var total_checked = 0;
        for (x in $scope.order.list) {
            if ($scope.order.list[x].add_p_order) {
                total_checked++;
            }
        }
        if ($scope.order.list.length > 0 && $scope.order.list.length == total_checked) {
            $scope.listObj.check_add_all = true;
        } else {
            $scope.listObj.check_add_all = false;
        }
    }

    $scope.renderPage(data)
}]).controller('PurchaseorderCtrl', ['$scope', 'helper', function($scope, helper) {
    $scope.serial_number = '';
    $scope.is_view = !1;
    $scope.p_order_id = ''
}]).controller('PurchaseorderViewCtrl', ['$scope', '$state', 'helper', '$timeout', '$stateParams', '$confirm', 'editItem', 'modalFc', 'data', 'selectizeFc', 'Lightbox', function($scope, $state, helper, $timeout, $stateParams, $confirm, editItem, modalFc, data, selectizeFc, Lightbox) {
    $scope.p_order_id = $stateParams.p_order_id;
    var date = new Date();
    $scope.format = 'dd/MM/yyyy';
    $scope.datePickOpen = {
        sent_date: !1,
        delivery_date: !1
    };
    $scope.dateOptions = {
        dateFormat: '',
    };
    $scope.$parent.isAddPage = !0;
    $scope.$parent.is_view = !0;
    $scope.$parent.p_order_id = $stateParams.p_order_id;
    $scope.order = [];
    $scope.showTxt = {
        sendEmail: !1,
        drop: !1,
        markAsReady: !1,
        viewNotes: !1,
        sendPost: !1,
        source: !1,
        type: !1,
        segment: !1,
        edit_delivery_date: !1
    };

    $scope.email = {};
    $scope.auto = {
        options: {
            recipients: []
        }
    };
    $scope.show_load = !0;
    $scope.stationaryCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select E-Stationary'),
        create: !1,
        maxOptions: 100,
        closeAfterSelect: !0,
        maxItems: 1
    };
    $scope.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    $scope.tinymceOptions = {
        selector: ".mail_send",
        resize: "height",
        height: 184,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile bold italic underline | alignleft aligncenter alignright | bullist numlist | link image", "forecolor backcolor | fontsizeselect |  table | code"],
    };
    $scope.renderPage = function(d, showAlerts = true) {
        $scope.showTxt.viewMarkAsSent = !1;
        if (d.data) {
            $scope.p_order = d.data;
            if ($scope.p_order.sent_date) {
                $scope.p_order.sent_date = new Date($scope.p_order.sent_date)
            }
            if ($scope.p_order.del_date) {
                $scope.p_order.del_date = new Date($scope.p_order.del_date)
            }
            if ($scope.p_order.po_d_factur_date) {
                $scope.p_order.po_d_factur_date = new Date($scope.p_order.po_d_factur_date)
            }
            if ($scope.email.alerts) {
                var alerts = $scope.email.alerts
            } else {
                var alerts = []
            }
            $scope.dateOptions.minDate = $scope.p_order.po_d_factur_date;
            $scope.$parent.serial_number = $scope.p_order.factur_nr;
            helper.updateTitle($state.current.name, $scope.p_order.factur_nr);
            $scope.$parent.type_title = $scope.p_order.type_title;
            $scope.$parent.nr_status = $scope.p_order.nr_status;
            $scope.auto.options.recipients = d.data.recipients;
            $scope.email = {
                e_subject: $scope.p_order.subject,
                e_message: $scope.p_order.e_message,
                email_language: $scope.p_order.languages,
                include_pdf: $scope.p_order.include_pdf,
                p_order_id: $scope.p_order_id,
                files: $scope.p_order.files,
                use_html: $scope.p_order.use_html,
                user_loged_email: $scope.p_order.user_loged_email,
                recipients: [],
                alerts: alerts,
                sendgrid_selected: d.data.sendgrid_selected
            };
            if (d.data.recipient_default) {
                $scope.email.recipients = d.data.recipient_default.id
            }
            $scope.email.recipients = d.data.recipients_ids;
            $scope.save_disabled = d.data.save_disabled;
            if (!d.error) {
                $scope.showDrop();
            }
            if (showAlerts) {
                $scope.showAlerts(d);
            }
        }
    }
    editItem.init($scope);
    $scope.showPreview = function(exp) {
        var params = {
            template: 'miscTemplate/modal',
            ctrl: 'viewRecepitCtrl',
            size: 'md',
            ctrlAs: 'vmMod',
            params: {
                'do': 'po_order-email_preview',
                p_order_id: $scope.p_order_id,
                message: exp.e_message,
                use_html: exp.use_html
            }
        }
        modalFc.open(params)
    };
    $scope.dispatchModal = function() {
        var params = {
            template: 'potemplate/dispatchModal',
            ctrl: 'DispatchModalCtrl',
            size: 'md',
            item: {
                'del_note': $scope.order.del_note,
                'do': 'order-order-order-addDispatchNote',
                'order_id': $scope.order_id
            },
            callback: function(data) {
                if (data) {
                    $scope.order.del_note_txt = data.data.del_note_txt;
                    $scope.order.del_note = data.data.del_note;
                    $scope.showAlerts(data)
                }
            }
        }
        modalFc.open(params)
    };
    $scope.sendEmail = function() {
        //$scope.showTxt.sendEmail = !0;
        //$scope.showTxt.markAsReady = !1;
        //$scope.email.alerts = []
        $scope.showDrop();
        var params = {
            'do': 'po_order-email_preview',
            p_order_id: $scope.p_order_id,
            message: $scope.email.e_message,
            use_html: $scope.p_order.use_html,
            send_email: true
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            var viewEmail = angular.copy($scope.email);
            viewEmail.e_message = r.data.e_message;
            var obj = {
                "app": "p_order",
                "recipients": $scope.auto.options.recipients,
                "use_html": $scope.p_order.use_html,
                "is_file": $scope.p_order.is_file,
                "dropbox": $scope.p_order.dropbox,
                "email": viewEmail
            };
            openModal("send_email", angular.copy(obj), "lg");
        })
    }
    $scope.cancelSendMail = function() {
        $scope.showTxt.sendEmail = !1;
        $scope.showTxt.markAsReady = !0
    }
    $scope.sendEmails = function(r) {
        var d = angular.copy(r);
        $scope.showTxt.sendEmail = !1;

        if (d.recipients.length) {
            $scope.showTxt.sendEmail = !1;
            /*if ($scope.p_order.dropbox) {
                d.dropbox_files = $scope.p_order.dropbox.dropbox_files
            }*/
            if (angular.isArray(d.recipients)) {
                d.new_email = d.recipients[0]
            } else {
                d.new_email = d.recipients
            }
            if (d.new_email) {
                $scope.email.alerts.push({
                    type: 'email',
                    msg: d.new_email + ' - '
                });
                d.do = 'po_order-po_order-po_order-sendNew';
                d.use_html = $scope.email.use_html;
                d.lid = d.email_language;
                d.serial_number = $scope.p_order.serial_number;
                helper.doRequest('post', 'index.php', d, function(r) {
                    if (r.success && r.success.success) {
                        $scope.email.alerts[$scope.email.alerts.length - 1].type = 'success';
                        $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.success.success
                    }
                    if (r.notice && r.notice.notice) {
                        $scope.email.alerts[$scope.email.alerts.length - 1].type = 'info';
                        $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.notice.notice
                    }
                    if (r.error && r.error.error) {
                        $scope.email.alerts[$scope.email.alerts.length - 1].type = 'danger';
                        $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.error.error
                    }
                    if (Array.isArray(d.recipients)) {
                        d.recipients.shift();
                        if (d.new_email == d.user_loged_email) {
                            d.copy = !1;
                        }
                        $scope.sendEmails(d)
                    }
                    if (r.success) {
                        $scope.renderPage(r, false)
                    }
                })
            } else {
                if (Array.isArray(d.recipients)) {
                    d.recipients.shift();
                    $scope.sendEmails(d)
                }
            }
        } else {
            if (d.user_loged_email && d.copy) {
                d.new_email = d.user_loged_email;
                d.copy = !1;
                d.mark_as_sent = 0;
                if (d.new_email) {
                    $scope.email.alerts.push({
                        type: 'email',
                        msg: d.new_email + ' - '
                    });
                    d.do = 'po_order-po_order-po_order-sendNew';
                    d.use_html = $scope.email.use_html;
                    d.lid = d.email_language;
                    d.serial_number = $scope.p_order.serial_number;
                    helper.doRequest('post', 'index.php', d, function(r) {
                        if (r.success && r.success.success) {
                            $scope.email.alerts[$scope.email.alerts.length - 1].type = 'success';
                            $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.success.success
                        }
                        if (r.notice && r.notice.notice) {
                            $scope.email.alerts[$scope.email.alerts.length - 1].type = 'info';
                            $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.notice.notice
                        }
                        if (r.error && r.error.error) {
                            $scope.email.alerts[$scope.email.alerts.length - 1].type = 'danger';
                            $scope.email.alerts[$scope.email.alerts.length - 1].msg += ' ' + r.error.error
                        }
                        $scope.sendEmails(d)
                        if (r.success) {
                            $scope.renderPage(r, false)
                        }
                    })
                }
            } else {
                helper.refreshLogging();
            }
        }
    }
    $scope.myFunction = function() {
        if ($scope.email.recipients.length) {
            $scope.save_disabled = !1
        } else {
            $scope.save_disabled = !0
        }
    }
    $scope.showDrop = function() {
        $scope.show_load = !0;
        var data_drop = angular.copy($scope.p_order.drop_info);
        data_drop.do = 'misc-dropbox_new';
        helper.doRequest('post', 'index.php', data_drop, function(r) {
            $scope.p_order.dropbox = r.data;
            $scope.show_load = !1
        })
    }
    $scope.deleteDropFile = function(index) {
        var data = angular.copy($scope.p_order.dropbox.dropbox_files[index].delete_file_link);
        $scope.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.show_load = !1;
            $scope.showAlerts(r);
            if (r.success) {
                $scope.p_order.dropbox.nr--;
                $scope.p_order.dropbox.dropbox_files.splice(index, 1)
            }
        })
    }
    $scope.deleteDropImage = function(index) {
        var data = angular.copy($scope.p_order.dropbox.dropbox_images[index].delete_file_link);
        $scope.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.show_load = !1;
            $scope.showAlerts(r);
            if (r.success) {
                $scope.p_order.dropbox.nr--;
                $scope.p_order.dropbox.dropbox_images.splice(index, 1)
            }
        })
    }
    $scope.openLightboxModal = function(index) {
        Lightbox.openModal($scope.p_order.dropbox.dropbox_images, index)
    };
    $scope.uploadDoc = function(type) {
        var obj = angular.copy($scope.p_order.drop_info);
        obj.type = type;
        openModal('uploadDoc', obj, 'lg')
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.contactForEmailAutoCfg = selectizeFc.configure({
        placeholder: 'Select recipient',
        create: !0,
        createOnBlur: true,
        maxItems: 100
    });
    $scope.markOrder = function(func) {
        helper.doRequest('post', 'index.php', {
            'do': 'po_order-po_order-po_order-' + func,
            p_order_id: $scope.p_order_id,
            sent_date: $scope.p_order.sent_date
        }, $scope.renderPage)
    }
    $scope.openDeliveyModal = function(delivery_id, isTwoSteps, initialItem) {
        var action = isTwoSteps === !0 ? 'po_order-approve_po_order_line' : 'po_order-p_order_line',
            params = {
                template: 'potemplate/deliveryModal',
                ctrl: 'p_deliveryModalCtrl',
                ctrlAs: '',
                backdrop: 'static',
                params: {
                    'do': action,
                    p_order_id: $scope.p_order_id,
                    delivery_id: delivery_id
                },
                callback: function(data) {
                    if (data) {
                        if (data.data) {
                            $scope.p_order = data.data;
                            $scope.$parent.type_title = $scope.p_order.type_title;
                            $scope.$parent.nr_status = $scope.p_order.nr_status;
                            if(data.data.reopen_delivery){
                                let tmp_open_deliveries=[];
                                for(let x in $scope.p_order.deliveries){
                                    if($scope.p_order.deliveries[x].not_approved){
                                        tmp_open_deliveries.push($scope.p_order.deliveries[x]);
                                    }
                                }
                                if(tmp_open_deliveries.length){              
                                    tmp_open_deliveries.sort((a, b) => (a.delivery_id > b.delivery_id) ? 1 : -1);
                                    let multiple_deliveries=tmp_open_deliveries.map(function(obj) {return {'id':obj.delivery_id,'name':obj.delivey}});
                                    $scope.openDeliveyModal(multiple_deliveries[0].id,true,{'multiple_deliveries':multiple_deliveries});
                                }
                            }
                        }
                        $scope.showAlerts(data)
                    }
                },
                callbackExit:function(data){
                    if(typeof data === 'object'){
                        $scope.openDeliveyModal(data.delivery_id,isTwoSteps,initialItem);
                    }
                }
            };
            if(initialItem){
                params.initial_item = initialItem;
            }
        modalFc.open(params)
    }
    $scope.openReturnModal = function(return_id) {
        var params = {
            template: 'otemplate/returnModal',
            ctrl: 'returnModalCtrl',
            ctrlAs: '',
            params: {
                'do': 'po_order-return_article',
                p_order_id: $scope.p_order_id,
                return_id: return_id
            },
            callback: function(data) {
                if (data) {
                    if (data.data) {
                        $scope.p_order = data.data
                    }
                    $scope.showAlerts(data)
                }
            }
        };
        modalFc.open(params)
    }
    $scope.openSendModal = function(delivery_id) {
        var params = {
            template: 'otemplate/sendModal',
            ctrl: 'sendModalCtrl',
            ctrlAs: '',
            params: {
                'do': 'order-send_email',
                order_id: $scope.order_id,
                delivery_id: delivery_id
            },
            callback: function(data) {
                $scope.showAlerts(data)
            }
        };
        modalFc.open(params)
    }
    $scope.deleteDelivery = function(link) {
        helper.doRequest('get', link, function(r) {
            if (r.success) {
                $scope.renderPage(r, false);
            }
        });
    }
    $scope.showPDFsettings = function(i) {
        var params = {
            template: 'otemplate/pdf_settings',
            ctrl: 'pdfSettingsOCtrl',
            ctrlAs: '',
            size: 'md',
            params: {
                'do': 'order-pdf_settings',
                porder: !0,
                p_order_id: $scope.p_order_id,
                xls: i
            },
            callback: function(data) {
                if (data) {
                    if (data.data) {
                        $scope.order = data.data
                    }
                    $scope.showAlerts(data)
                }
            }
        };
        modalFc.open(params)
    };
    $scope.removeDispatchNote = function() {
        helper.doRequest('post', 'index.php', {
            'del_note': '',
            'do': 'order-order-order-addDispatchNote',
            'order_id': $scope.order_id
        }, function(data) {
            if (data) {
                $scope.order.del_note_txt = data.data.del_note_txt;
                $scope.order.del_note = data.data.del_note;
                $scope.showAlerts(data)
            }
        })
    }
    $scope.greenpost = function() {
        if ($scope.p_order.post_active) {
            $scope.showTxt.sendPost = !$scope.showTxt.sendPost
        } else {
            var data = {};
            data.p_order_id = $scope.p_order_id;
            openModal("post_register", data, "md")
        }
    }
    $scope.sendPost = function() {
        var data = {};
        data.do = 'po_order-po_order-po_order-postIt';
        data.id = $scope.p_order_id;
        data.related = 'view';
        if ($scope.p_order.stationary_id) {
            data.stationary_id = $scope.p_order.stationary_id
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.showTxt.sendPost = !1;
            if (r.success) {
                $scope.renderPage(r)
            }
            if (r.error) {
                $scope.showAlerts(r)
            }
        })
    }
    $scope.resendGreenPost = function() {
        var data = {};
        data.do = 'po_order-po_order-po_order-postIt';
        data.id = $scope.p_order_id;
        data.related = 'view';
        data.post_library = angular.copy($scope.p_order.post_library);
        openModal("resend_post", data, "md")
    }
    $scope.setSegment = function() {
        var obj = {
            'do': 'po_order--po_order-updateSegment',
            'p_order_id': $scope.p_order.p_order_id,
            'segment_id': $scope.p_order.segment_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.showTxt.segment = !1;
                $scope.p_order.segment = r.data.segment
            }
        })
    }
    $scope.setSource = function() {
        var obj = {
            'do': 'po_order--po_order-updateSource',
            'p_order_id': $scope.p_order.p_order_id,
            'source_id': $scope.p_order.source_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.showTxt.source = !1;
                $scope.p_order.source = r.data.source
            }
        })
    }

    $scope.statusDocument = function(status) {
        var tmp_obj = { 'p_order_id': $scope.p_order_id };
        tmp_obj.do = status == '0' ? 'po_order-po_order-po_order-archive_po_order' : 'po_order-po_order-po_order-activate_po_order';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_obj, function(r) {
            if (r.success) {
                $scope.renderPage(r);
                helper.refreshLogging();
            } else {
                $scope.showAlerts(r);
            }
        });
    }

    $scope.orderFacq = function($amount) {
        var order_obj = {
            'p_order_id': $scope.p_order_id,
            'do': 'po_order-po_order-po_order-orderFacq',
            buyer_id: $scope.buyer_id,
            contact_id: $scope.contact_id,
        };
        $scope.PVCheck(true);

    }

    $scope.PVCheck = function($send_order) {
        var obj = {
            'do': 'po_order-po_order-po_order-PVCheck',
            'p_order_id': $scope.p_order_id,

        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                angular.element('.loading_wrap').addClass('hidden');

                $scope.renderPage(r);

                if ($send_order) {
                    var check_obj = {
                        'p_order_id': $scope.p_order_id,
                        'do': 'po_order-po_order-po_order-validate_facq_articles',
                    };
                    helper.doRequest('post', 'index.php', check_obj, function(r) {
                        if (r.error) {
                            $scope.showAlerts(r);

                        } else {
                            var $amount = r.data.totalul.total_vat;
                            $confirm({
                                title: helper.getLanguage('Order at Facq'),
                                text: helper.getLanguage('Confirm the order can be placed at Facq for a total value of ') + $amount + helper.getLanguage('. <br> If you want to see the individual article prices go back to the previous screen. '),
                                ok: helper.getLanguage('Confirm'),
                                cancel: helper.getLanguage('Cancel'),
                            }).then(function() {

                                var order_obj = {
                                    'p_order_id': $scope.p_order_id,
                                    'do': 'po_order-po_order-po_order-orderFacq',
                                    buyer_id: $scope.buyer_id,
                                    contact_id: $scope.contact_id,
                                };
                                helper.doRequest('post', 'index.php', order_obj, function(r) {
                                    if (r.success) {
                                        $scope.renderPage(r);

                                    } else {
                                        $scope.showAlerts(r);
                                    }
                                });
                                helper.refreshLogging();


                            }, function() {
                                //console.log('no');
                                $state.go('purchase_order.view', order_obj);
                            })

                        }
                    });


                }


            }
        })
    }

    $scope.deletePorder = function() {
        var tmp_obj = { 'p_order_id': $scope.p_order_id };
        tmp_obj.do = 'po_order-po_orders-po_order-delete_po_order';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_obj, function(r) {
            if (r.success) {
                helper.setItem('po_order-po_orders', {});
                $state.go("purchase_orders");
            } else {
                $scope.showAlerts(r);
            }
        });
    }

    $scope.setType = function() {
        var obj = {
            'do': 'po_order--po_order-updateType',
            'p_order_id': $scope.p_order.p_order_id,
            'type_id': $scope.p_order.type_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.showTxt.type = !1;
                $scope.p_order.xtype = r.data.xtype
            }
        })
    }

    $scope.setDeliveryDate = function() {
        if (!$scope.p_order.del_date) {
            return false;
        }
        var obj = {
            'do': 'po_order--po_order-updateDeliveryDate',
            'p_order_id': $scope.p_order.p_order_id,
            'del_date': Math.floor($scope.p_order.del_date.getTime() / 1000)
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.showTxt.edit_delivery_date = !1;
                $scope.p_order.factur_del_date = r.data.factur_del_date
            }
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'post_register':
                var iTemplate = 'PostRegister';
                var ctas = 'post';
                break;
            case 'resend_post':
                var iTemplate = 'ResendPostGreen';
                var ctas = 'post';
                break;
            case 'uploadDoc':
                var iTemplate = 'AmazonUpload';
                break;
            case 'send_email':
                var iTemplate = 'SendEmail';
                var ctas = 'vm';
                break;
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'post_register':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.item.do = 'po_order-po_order-po_order-postRegister';
                params.callback = function(data) {
                    if (data && data.data) {
                        $scope.renderPage(data);
                        $scope.greenpost()
                    }
                    $scope.showAlerts(data)
                }
                break;
            case 'resend_post':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.request_type = 'post';
                params.params = {
                    'do': 'po_order--po_order-postOrderData',
                    'p_order_id': $scope.p_order_id,
                    'related': 'status',
                    'old_obj': item
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.renderPage(d)
                    }
                }
                break;
            case 'uploadDoc':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        $scope.showDrop()
                    }
                }
                break;
            case 'send_email':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        $scope.sendEmails(d);
                    }
                }
                break;
        }
        modalFc.open(params)
    }

    $scope.showAccountNotes = function() {
        $scope.showCustomerNotes($scope, $scope.p_order.buyer_id, $scope.p_order.customer_notes);
    }

    $scope.setOurReference = function() {
        var obj = {
            'do': 'po_order--po_order-updateOurReference',
            'p_order_id': $scope.p_order_id,
            'our_ref': $scope.p_order.our_ref
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.showTxt.edit_our_ref = !1;
            }
        })
    }

    $scope.setYourReference = function() {
        var obj = {
            'do': 'po_order--po_order-updateYourReference',
            'p_order_id': $scope.p_order_id,
            'your_ref': $scope.p_order.your_ref
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $scope.showTxt.edit_your_ref = !1;
            }
        })
    }

    $scope.showYourReference = function() {
        $scope.showTxt.edit_your_ref = true;
        $timeout(function() {
            angular.element('#inp_yr').focus();
        });
    }
    $scope.showOurReference = function() {
        $scope.showTxt.edit_our_ref = true;
        $timeout(function() {
            angular.element('#inp_or').focus();
        });
    }

    $scope.confirmReception=function(){
        //check how many open deliveries:if more then one show modal else press confirm button
        let tmp_open_deliveries=[];
        for(let x in $scope.p_order.deliveries){
            if($scope.p_order.deliveries[x].not_approved){
                tmp_open_deliveries.push($scope.p_order.deliveries[x]);
            }
        }
        if(tmp_open_deliveries.length && tmp_open_deliveries.length == 1){
            $scope.openDeliveyModal(tmp_open_deliveries[0].delivery_id,true);
        }else if(tmp_open_deliveries.length && tmp_open_deliveries.length > 1){
            tmp_open_deliveries.sort((a, b) => (a.delivery_id > b.delivery_id) ? 1 : -1);
            let multiple_deliveries=tmp_open_deliveries.map(function(obj) {return {'id':obj.delivery_id,'name':obj.delivey}});
            $scope.openDeliveyModal(multiple_deliveries[0].id,true,{'multiple_deliveries':multiple_deliveries});
        }
    }

    $scope.showEditLine=function(index){
        $scope.p_order.lines[index].show_edit=true;
        $scope.showTxt.show_edit_index=index;
        $scope.tmp_edit=angular.copy($scope.p_order.lines[index]);
    }

    $scope.saveLineUpdates=function(item){
        $scope.showTxt.show_edit_index=null;
        $scope.tmp_edit=undefined;
        var obj=angular.copy(item);
        obj.do='po_order-po_order-po_order-update_po_order_line';
        obj.p_order_id=$scope.p_order_id;
        obj.discount=$scope.p_order.discount;
        obj.apply_discount=$scope.p_order.apply_discount;
        helper.doRequest('post', 'index.php', obj,$scope.renderPage);
    }

    $scope.renderPage(data)
}]).controller('deliveryStockDispatchModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', '$compile', '$uibModal', 'modalFc', function($scope, helper, $uibModalInstance, data, $compile, $uibModal, modalFc) {
    $scope.delivery = data;
    $scope.delivery.do = 'stock-stock_dispatch-stock_dispatch-make_delivery';
    $scope.disableAddress = !0;
    $scope.listStats = [];
    $scope.listStats2 = [];
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
    $scope.ok = function() {
        helper.doRequest('post', 'index.php', $scope.delivery, $scope.closeModal)
    }
    $scope.closeModal = function(d) {
        if (d.error) {
            $scope.alerts = [{
                type: 'danger',
                msg: d.error.error
            }];
            return !1
        }
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    $scope.openModal = function(type, item, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            backdrop: 'static',
            templateUrl: 'stocktemplate/' + type + 'Modal',
            controller: type + 'ModalCtrl',
            size: size ? size : 'lg',
            resolve: {
                data: function() {
                    switch (type) {
                        case 'serialNr':
                            var d = {
                                'do': 'stock-' + type,
                                stock_disp_id: $scope.delivery.stock_disp_id,
                                stock_disp_articles_id: item.stock_disp_articles_id,
                                count_serial_nr: parseInt(helper.return_value(item.quantity)),
                                delivery_id: $scope.delivery.delivery_id,
                                selected_s_n: item.selected_s_n
                            };
                            break;
                        case 'batchNr':
                            var q = helper.return_value(item.quantity);
                            var d = {
                                'do': 'stock-' + type,
                                stock_disp_id: $scope.delivery.stock_disp_id,
                                stock_disp_articles_id: item.stock_disp_articles_id,
                                count_quantity_nr: q,
                                selected_items: item.edit_batches,
                                ret_quantity: item.ret_quantity,
                                has_changed: item.has_changed
                            }
                            break;

                    }
                    return helper.doRequest('post', 'index.php', d).then(function(d) {
                        return d.data
                    })
                }
            }
        });
        modalInstance.result.then(function(data) {
            switch (type) {
                case 'serialNr':
                    if (data) {
                        item.count_selected_s_n = data.length;
                        item.selected_s_n = data;
                        item.serialOk = !0
                    }
                    break;
                case 'batchNr':
                    if (data) {
                        item.count_selected_b_n = 0;
                        for (x in data) {
                            if (data[x].batch_no_checked) {
                                item.count_selected_b_n += helper.return_value(data[x].b_n_q_selected) * 1
                            }
                        }
                        item.selected_b_n = data;
                        item.batchOk = !0
                    }
                    break;

            }
        }, function() {
            console.log('modal closed')
        })
    }
}]).controller('p_deliveryModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', '$compile', '$uibModal', 'modalFc', '$confirm', function($scope, helper, $uibModalInstance, data, $compile, $uibModal, modalFc, $confirm) {
    $scope.delivery = data;
    $scope.delivery.do = $scope.delivery.delivery_id ? 'po_order-po_order-po_order-edit_delivery' : 'po_order-po_order-po_order-make_entry';
    $scope.delivery.hour = $scope.delivery.delivery_id ? data.hour : "12";
    $scope.delivery.minute = $scope.delivery.delivery_id ? data.minute : "00";
    $scope.delivery.pm = $scope.delivery.delivery_id ? data.pm : 'pm';
    $scope.delivery.date_del_line = $scope.delivery.delivery_id ? new Date(data.delivery_date_js) : new Date();
    $scope.delivery.contact_id = $scope.delivery.delivery_id ? data.contact_id : undefined;
    $scope.delivery.contact_name = $scope.delivery.delivery_id ? data.contact_name : '';
    $scope.disableAddress = !0;
    $scope.listStats = [];
    $scope.listStats2 = [];
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        date_del_line: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.hours = [12, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11];
    $scope.minutes = ["00", 15, 30, 45];
    $scope.contactAutoCfg = {
        valueField: 'recipient_id',
        labelField: 'recipient_name',
        searchField: ['recipient_name'],
        delimiter: '|',
        placeholder: 'Select contact',
        create: !0,
        maxItems: 1,
        onItemAdd(value, $item) {
            $scope.delivery.contact_name = $item.html()
        },
        onItemRemove(value) {
            $scope.delivery.contact_name = ''
        }
    };

    $scope.multiDeliveryCfg={
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        maxItems: 1,
        onDelete(value){
            return false;
        },
        onChange(value){
            $uibModalInstance.dismiss({'delivery_id':value})
        }
    };

    $scope.hideDetails = !1;
    $scope.disabledt = function() {
        $scope.disableAddress = !$scope.disableAddress;
        return !1
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
    $scope.showStats = function(item, $index) {
        var add = !1;
        if (!$scope.listStats[$index]) {
            $scope.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (item.isopen == !0) {
            var idx = 0;
            for (x in $scope.delivery.location_dd) {
                if ($scope.delivery.location_dd[x].id == $scope.delivery.select_main_location) {
                    idx = x
                }
            }
            var q = parseFloat(item.quantity_value) > parseFloat($scope.listStats[$index][idx].order_qty) ? $scope.listStats[$index][idx].quantity : item.quantity;
            $scope.listStats[$index][idx].quantity_delivered = q
        }
    }
    $scope.ok = function() {

        var batchOkAll = true;
        var listBatches = '';
        for (x in $scope.delivery.lines) {
            if ($scope.delivery.lines[x].use_batch_no_art && parseInt(helper.return_value($scope.delivery.lines[x].quantity))) {
                if ($scope.delivery.lines[x].batchOk == false) {
                    batchOkAll = false;
                    listBatches = $scope.delivery.lines[x].article + "\n" + listBatches;
                }
            }
        }
        if (batchOkAll) {
            helper.doRequest('post', 'index.php', $scope.delivery, $scope.closeModal)
        } else {
            $scope.alerts = [{
                type: 'danger',
                msg: helper.getLanguage('Batch Number') + ' ' + helper.getLanguage("quantities don't match for") + ':\n' + listBatches
            }];
            return !1
        }

    }
    $scope.closeModal = function(d) {
        if (d.error) {
            $scope.alerts = [{
                type: 'danger',
                msg: d.error.error
            }];
            return !1
        }else if(d.notice){
            $confirm({
                ok: helper.getLanguage('Yes'),
                cancel: helper.getLanguage('No'),
                text: helper.getLanguage('Are you sure you want to register this reception? If you do, the total entered amount will exceed the total ordered amount.')
            }).then(function() {
                var tmp_obj=angular.copy($scope.delivery);
                tmp_obj.confirm_over_total_delivery='1';
                helper.doRequest('post', 'index.php', tmp_obj, $scope.closeModal)
            }, function() {
                return !1
            });
            return false;
        }
        if (d) {
            if(d.data && $scope.delivery.multiple_deliveries){
                let data_copy=angular.copy(d);
                data_copy.data.reopen_delivery=true;
                $uibModalInstance.close(data_copy)
            }else{
                $uibModalInstance.close(d)
            }
        } else {
            $uibModalInstance.close()
        }
    }
    $scope.showStats2 = function(item, $index) {
        if (item.locations) {
            if (item.locations.length > 0) {
                $scope.listStats2[$index] = item;
                angular.element('.deliveryTable tbody tr.collapseTr').eq($index).after('<tr class="versionInfo" ng-if="listStats2[' + $index + '].view_location"><td colspan="5" class="no_border">' + '<table class="table_minimal">' + '<tbody>' + '<tr ng-repeat="item2 in listStats2[' + $index + '].locations">' + '<td>' + '<div class="row" >' + '<div class="col-xs-9"><div><b>{{::item2.depo}}</b> </div>' + '<div>{{::item2.address}}</div>' + '<div>{{::item2.zip}} {{::item2.city}}</div>' + '<div>{{::item2.country}}</div>' + '</div>' + '<div class="col-xs-3">' + '<div>{{::item2.quantity_loc}}</div>' + '</div>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>');
                $compile(angular.element('.deliveryTable tbody tr.collapseTr').eq($index).next())($scope);
                modalFc.setHeight(angular.element('.modal.in'))
            }
        }
    }
    $scope.stockLocationCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        onChange(value) {
            if (value) {
                $scope.changedAddress();
            }
        },
        maxItems: 1
    };
    $scope.changedAddress = function() {
        for (x in $scope.delivery.location_dd) {
            if ($scope.delivery.location_dd[x].id == $scope.delivery.select_main_location) {
                idx = x
            }
        }
        for (y in $scope.delivery.lines) {
            var i = $scope.delivery.lines[y];
            i.is_error = !1;
            for (x in i.disp_addres_info) {
                i.disp_addres_info[x].quantity_delivered = helper.displayNr(0);
                i.disp_addres_info[x].is_error = !1;
                if (x == idx) {
                    i.disp_addres_info[x].quantity_delivered = helper.return_value(i.quantity) * 1;
                    if (helper.return_value(i.quantity) * 1 > i.disp_addres_info[x].qty * 1 && i.show_stock && i.is_article) {
                        i.is_error = !0;
                        i.disp_addres_info[x].is_error = !0;
                        i.disp_addres_info[x].quantity_delivered = i.quantity
                    } else {
                        i.disp_addres_info[x].quantity_delivered = i.quantity
                    }
                }
            }
        }
    }
    $scope.openModal = function(type, item, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            backdrop: 'static',
            templateUrl: 'potemplate/' + type + 'Modal',
            controller: type + 'ModalCtrl',
            size: size ? size : 'md',
            resolve: {
                data: function() {
                    switch (type) {
                        case 'serialNrP':
                            var d = {
                                'do': 'po_order-' + type,
                                article_name: item.article,
                                p_order_id: $scope.delivery.p_order_id,
                                order_articles_id: item.order_articles_id,
                                article_no: parseInt(helper.return_value(item.quantity)),
                                selected_s_n: item.selected_s_n
                            };
                            break;
                        case 'batchNrP':
                            $scope.alerts = [];
                            //var q = parseInt(helper.return_value(item.quantity))
                            var q = item.quantity;
                            if ($scope.delivery.delivery_id > 0 && $scope.delivery.delivery_approved == undefined) {
                                q = undefined
                            }
                            var d = {
                                'do': 'po_order-' + type,
                                order_articles_id: item.order_articles_id,
                                article_no: q,
                                article_id: item.article_id,
                                p_delivery_id: $scope.delivery.delivery_id,
                                article_name: item.article
                            };
                            break
                    }
                    return helper.doRequest('post', 'index.php', d).then(function(d) {
                        return d.data
                    })
                }
            }
        });
        modalInstance.result.then(function(data) {
            switch (type) {
                case 'serialNrP':
                    if (data) {
                        item.selected_s_n = data;
                        item.serialOk = !0
                    }
                    break;
                case 'batchNrP':
                    if (data) {
                        item.selected_b_n = data;
                        item.batchOk = !0
                    }
                    break
            }
        }, function() {
            console.log('modal closed')
        })
    }
}]).controller('sendModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.email = data;
    $scope.contactForEmailAutoCfg = {
        valueField: 'recipient_email',
        labelField: 'recipient_name',
        searchField: ['recipient_name'],
        delimiter: '|',
        placeholder: 'Select recipient',
        create: !0,
        createOnBlur: true,
        maxItems: 100
    };
    $scope.auto = {
        options: {
            recipients: data.recipients
        }
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
    $scope.ok = function() {
        $scope.email.do = $scope.email.do_next;
        helper.doRequest('post', 'index.php', $scope.email, $scope.closeModal)
    }
    $scope.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
}]).controller('pdfSettingsOCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.obj = data;
    $scope.ok = function() {
        var data = {
            'do': $scope.obj.do_next,
            logo: $scope.obj.default_logo,
            email_language: $scope.obj.language_id,
            pdf_layout: $scope.obj.pdf_type_id,
            p_order_id: $scope.obj.p_order_id,
            p_order_id: $scope.obj.p_order_id,
            identity_id: $scope.obj.multiple_identity_id
        };
        helper.doRequest('post', 'index.php', data, $scope.closeModal)
    };
    $scope.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('PurchaseorderEditCtrl', ['$scope', 'helper', '$stateParams', '$state', '$cacheFactory', '$uibModal', '$timeout', 'editItem', '$confirm', 'modalFc', 'data', 'selectizeFc', function($scope, helper, $stateParams, $state, $cacheFactory, $uibModal, $timeout, editItem, $confirm, modalFc, data, selectizeFc) {
    var edit = this,
        typePromise;
    edit.app = 'po_order';
    edit.ctrlpag = 'npo_order';
    edit.tmpl = 'potemplate';
    $scope.$parent.isAddPage = !1;
    edit.disabled = {
        address: !0,
        daddress: !0
    };
    edit.item = {};
    edit.item.date_h = new Date();
    edit.item.del_date = new Date();
    edit.showWarning = !0;
    edit.removedCustomer = !1;
    edit.oldObj = {};
    edit.contactAddObj = {};
    edit.showEditFnc = showEditFnc;
    edit.showCCSelectize = showCCSelectize;
    edit.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    edit.item_id = $stateParams.p_order_id;
    edit.p_order_id = edit.item_id;
    edit.showTxt = {
        info: !0,
        address: !1,
        editRef: !1,
        editYRef: !1,
        addBtn: !0,
        addArticleBtn: !1
    };
    edit.auto = {
        options: {
            author: [],
            accmanager: [],
            contacts: [],
            customers: [],
            addresses: [],
            cc: [],
            articles_list: []
        }
    };
    edit.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    edit.articlesListAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code', 'supplier_reference', 'name2'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                if (item.name2) {
                    item.name2 = "/ " + item.name2
                }
                if (item.allow_stock && item.is_service == 0) {
                    var stock_display = '';
                    if (item.hide_stock == 0) {
                        stock_display = '<div class="col-sm-4 text-right">' + (item.purchase_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                    }
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '</small>' + '</div>' + stock_display + '</div>' + '</div>'
                } else {
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '</small>' + '</div>' + '<div class="col-sm-4 text-right">' + (item.purchase_price) + '</div>' + '</div>' + '</div>'
                }
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span>' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'po_order-addArticle',
                    search: str,
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat
                };
                edit.requestAuto(edit, data, edit.renderArticleListAuto)
            }, 300)
        },
        onChange(value) {
            if (value == undefined) {} else if (value == '99999999999') {
                edit.openModal(edit, 'AddAnArticle', {
                    'do': 'po_order-addAnArticle',
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat
                }, 'md');
                return !1
            } else {
                for (x in edit.auto.options.articles_list) {
                    if (edit.auto.options.articles_list[x].article_id == value) {
                        editItem.addPurchaseOrderArticle(edit, edit.auto.options.articles_list[x]);
                        edit.selected_article_id = '';
                        edit.showTxt.addArticleBtn = !1;
                        var data = {
                            'do': 'po_order-addArticle',
                            customer_id: edit.item.buyer_id,
                            lang_id: edit.item.email_language,
                            cat_id: edit.item.price_category_id,
                            remove_vat: edit.item.remove_vat
                        };
                        edit.requestAuto(edit, data, edit.renderArticleListAuto);
                        return !1
                    }
                }
            }
        },
        onItemAdd(value, $item) {
            if (value == '99999999999') {
                edit.selected_article_id = ''
            }
        },
        onBlur() {
            $timeout(function() {
                edit.showTxt.addArticleBtn = !1
            })
        },
        maxItems: 1
    };

    edit.parentVariantCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code', 'supplier_reference', 'name2'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        maxOptions: 5,
        render: {
            option: function(item, escape) {
                if (item.name2) {
                    item.name2 = "/ " + item.name2
                }
                if (item.allow_stock) {
                    var stock_display = '';
                    if (item.hide_stock == 0) {
                        stock_display = '<div class="col-sm-4 text-right">' + (item.purchase_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                    }
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '</small>' + '</div>' + stock_display + '</div>' + '</div>'
                } else {
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '</small>' + '</div>' + '<div class="col-sm-4 text-right">' + (item.purchase_price) + '</div>' + '</div>' + '</div>'
                }
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span>' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onFocus() {
            var selectize = this.$input[0].selectize;
            var index = this.$input[0].attributes['selectindex'].value;
            var parentVariant = this.$input[0].attributes['selectarticle'].value;
            var tmp_data = {
                'do': 'misc-articleVariants',
                'parentVariant': parentVariant,
                buyer_id: edit.item.buyer_id,
                lang_id: edit.item.email_language,
                cat_id: edit.item.price_category_id,
                remove_vat: edit.item.remove_vat,
                app: edit.app
            };
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', tmp_data, function(r) {
                edit.item.tr_id[index].variantsOpts = r.data.lines;
                if (edit.item.tr_id[index].variantsOpts.length) {
                    selectize.open();
                }
            });
        },
        onType(str) {
            var selectize = this.$input[0].selectize;
            var index = this.$input[0].attributes['selectindex'].value;
            var parentVariant = this.$input[0].attributes['selectarticle'].value;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do': 'misc-articleVariants',
                    'parentVariant': parentVariant,
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    app: edit.app
                };
                helper.doRequest('get', 'index.php', tmp_data, function(r) {
                    edit.item.tr_id[index].variantsOpts = r.data.lines;
                    if (edit.item.tr_id[index].variantsOpts.length) {
                        selectize.open();
                    }
                });
            }, 300)
        },
        onChange(value) {
            var index = this.$input[0].attributes['selectindex'].value;
            if (value != undefined) {
                var collection = edit.item.tr_id[index].variantsOpts;
                for (x in collection) {
                    if (collection[x].article_id == value) {
                        edit.item.tr_id[index].has_variants_done = '1';
                        editItem.addPurchaseOrderArticle(edit, collection[x], parseInt(index) + 1);
                        break;
                    }
                }
            }
        },
        onBlur() {
            var index = this.$input[0].attributes['selectindex'].value;
            var parentVariant = this.$input[0].attributes['selectarticle'].value;
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do': 'misc-articleVariants',
                    'parentVariant': parentVariant,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    app: edit.app
                };
                helper.doRequest('get', 'index.php', tmp_data, function(r) {
                    edit.item.tr_id[index].variantsOpts = r.data.lines;
                });
            }, 300)
        },
        maxItems: 1
    };

    edit.childVariantCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code', 'supplier_reference', 'name2'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        maxOptions: 5,
        render: {
            option: function(item, escape) {
                if (item.name2) {
                    item.name2 = "/ " + item.name2
                }
                if (item.allow_stock) {
                    var stock_display = '';
                    if (item.hide_stock == 0) {
                        stock_display = '<div class="col-sm-4 text-right">' + (item.purchase_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                    }
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '</small>' + '</div>' + stock_display + '</div>' + '</div>'
                } else {
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '</small>' + '</div>' + '<div class="col-sm-4 text-right">' + (item.purchase_price) + '</div>' + '</div>' + '</div>'
                }
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span>' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onType(str) {
            var selectize = this.$input[0].selectize;
            var index = this.$input[0].attributes['selectindex'].value;
            var parentVariant = this.$input[0].attributes['selectarticle'].value;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do': 'misc-articleVariants',
                    'parentVariant': parentVariant,
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    app: edit.app
                };
                helper.doRequest('get', 'index.php', tmp_data, function(r) {
                    edit.item.tr_id[index].variantsOpts = r.data.lines;
                    if (edit.item.tr_id[index].variantsOpts.length) {
                        selectize.open();
                    }
                });
            }, 300)
        },
        onChange(value) {
            var index = this.$input[0].attributes['selectindex'].value;
            if (value != undefined) {
                var tmp_data = angular.copy(edit.item.tr_id[index].variantsOpts);
                for (x in tmp_data) {
                    if (tmp_data[x].article_id == value) {
                        var tmp_selected_item = angular.copy(tmp_data[x]);
                        tmp_selected_item.quantity = parseFloat(helper.return_value(edit.item.tr_id[index].quantity, 10));
                        edit.removeLine(index);
                        editItem.addPurchaseOrderArticle(edit, tmp_selected_item, index);
                        break;
                    }
                }
            }
        },
        onBlur() {
            var index = this.$input[0].attributes['selectindex'].value;
            edit.item.tr_id[index].showVariants = false;
        },
        maxItems: 1
    };

    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {
        date: !1,
        delivery_date: !1
    }
    edit.renderPage = renderPage;
    edit.openDate = openDate;
    edit.removeLine = removeLine;
    edit.removeTaxes = removeTaxes;
    edit.calcTotalPurchase = calcTotalPurchase;
    edit.addContentLine = addContentLine;
    edit.addLine = addLine;
    edit.save = save;
    edit.cancel = cancel;
    edit.changeLang = changeLang;
    edit.changeDelivery = changeDelivery;
    edit.showAlerts = showAlerts;
    edit.addPurchaseOrderArticle = addPurchaseOrderArticle;
    edit.removeDeliveryAddr = removeDeliveryAddr;
    edit.changeDiscount = changeDiscount;
    edit.applyLineDiscount = applyLineDiscount;
    editItem.init(edit);
    if (edit.item_id == 'tmp') {
        edit.tmp = {
            'do': 'po_order-npo_order',
            'buyer_id': ($stateParams.buyer_id ? $stateParams.buyer_id : 'tmp')
        };
        edit.init(edit, edit.tmp)
    } else {
        renderPage(data)
    }

    function renderPage(r) {
        if (r && r.data) {
            if (r.data.redirect) {
                $state.go(r.data.redirect);
                return !1
            }
            if (r.data.order_ref) {
                $scope.$parent.serial_number = r.data.ref_serial_number;
            } else {
                $scope.$parent.serial_number = '';
            }
            helper.updateTitle($state.current.name, r.data.ref_serial_number);
            if (r.data.type_title) {
                $scope.$parent.type_title = r.data.type_title;
            }
            if (r.data.nr_status) {
                $scope.$parent.nr_status = r.data.nr_status;
            }
            edit.item = r.data;
            edit.auto.options.contacts = r.data.contacts;
            edit.auto.options.customers = r.data.customers;
            edit.auto.options.addresses = r.data.addresses;
            edit.auto.options.second_addresses = r.data.second_addresses;
            edit.auto.options.cc = r.data.cc;
            edit.auto.options.sc = r.data.sc;
            edit.auto.options.articles_list = r.data.articles_list.lines;
            edit.contactAddObj = {
                language_dd: edit.item.language_dd,
                language_id: edit.item.language_id,
                gender_dd: edit.item.gender_dd,
                gender_id: edit.item.gender_id,
                title_dd: edit.item.title_dd,
                title_id: edit.item.title_id,
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                add_contact: !0,
                buyer_id: edit.item.buyer_id,
                article: edit.item.tr_id,
                'do': 'po_order-npo_order-po_order-tryAddC',
                identity_id: edit.item.identity_id,
                contact_only: !0,
                p_order_id: edit.item_id
            }
            if (edit.item.date_h) {
                edit.item.date_h = new Date(edit.item.date_h)
            }
            if (edit.item.del_date) {
                edit.item.del_date = new Date(edit.item.del_date)
            }
            if (!edit.item.tr_id) {
                edit.item.tr_id = []
            }
        }
    }

    function showEditFnc() {
        edit.showTxt.address = !1;
        edit.oldObj = {
            buyer_id: angular.copy(edit.item.buyer_id),
            contact_id: angular.copy(edit.item.contact_id),
            contact_name: angular.copy(edit.item.contact_name),
            CUSTOMER_NAME: angular.copy(edit.item.customer_name),
            delivery_address: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address)),
            delivery_address_id: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_id)),
            delivery_address_txt: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_txt)),
            view_delivery: (edit.item.sameAddress ? !1 : angular.copy(edit.item.view_delivery)),
            sameAddress: angular.copy(edit.item.sameAddress),
        }
    }

    function showCCSelectize() {
        edit.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }

    function openDate(p) {
        edit.datePickOpen[p] = !0
    }

    function removeDeliveryAddr() {
        edit.item.delivery_address = '';
        edit.item.delivery_address_id = ''
    }

    function removeLine($i) {
        var tr_id = edit.item.tr_id[$i].tr_id;
        edit.removeTaxes(tr_id);
        if (edit.item.tr_id[$i].has_variants == true || edit.item.tr_id[$i].has_variants == '1') {
            removeVariant(edit.item.tr_id[$i].article_id);
        }
        if ($i > -1) {
            edit.item.tr_id.splice($i, 1);
            edit.calcTotalPurchase()
        }
    }

    function calcTotalPurchase(type, $index) {
        editItem.calcTotalPurchase(edit.item, type, $index)
    }

    function removeTaxes(id) {
        for (x in edit.item.tr_id) {
            if (edit.item.tr_id[x].for_article == id) {
                edit.item.tr_id.splice(x, 1);
                edit.removeTaxes(id)
            }
        }
    }

    function removeVariant(id) {
        for (x in edit.item.tr_id) {
            if (edit.item.tr_id[x].is_variant_for == id) {
                edit.item.tr_id.splice(x, 1);
                break;
            }
        }
    }

    edit.showChildVariants = function(item, index) {
        var tmp_data = {
            'do': 'misc-articleVariants',
            'parentVariant': item.is_variant_for,
            buyer_id: edit.item.buyer_id,
            lang_id: edit.item.email_language,
            cat_id: edit.item.price_category_id,
            remove_vat: edit.item.remove_vat,
            app: edit.app
        };
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', tmp_data, function(r) {
            item.showVariants = true;
            edit.item.tr_id[index].variantsOpts = r.data.lines;
        });
    }
    edit.tinymceOptionsLines = {
        selector: "",
        resize: "both",
        auto_resize: !0,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code autoresize"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        autoresize_bottom_margin: 0,
        setup: function(editor) {
            editor.on('click', function(e) {
                e.stopPropagation();
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                angular.element(editor.editorContainer).parent().removeClass('wysiwyg-hide')
            }).on('blur', function(e) {
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                //edit.delaySave()
            })
        },
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
        content_css: [helper.base_url + 'css/tinymce_styles.css']
    };
    edit.tinymceOptionsPO = {
        selector: "",
        resize: "both",
        auto_resize: !0,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code autoresize"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        autoresize_bottom_margin: 0,
        setup: function(editor) {
            editor.on('click', function(e) {
                e.stopPropagation();
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                angular.element(editor.editorContainer).parent().removeClass('wysiwyg-hide')
            }).on('blur', function(e) {
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                //edit.delaySave()
            })
        },
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
        content_css: [helper.base_url + 'css/tinymce_styles.css']
    };

    edit.dragControlListeners = {
        dragStart: function(event) {
            $timeout(function() {
                var id = angular.element('.table_orders').find('li').eq(event.source.index).find('textarea').attr('id');
                if(id){
                    $scope.$broadcast('$tinymce:refresh', id)
                }            
            })
        },
        dragEnd: function(event) {
            $timeout(function() {
                var id = angular.element('.table_orders').find('li').eq(event.dest.index).find('textarea').attr('id');
                if(id){
                    $scope.$broadcast('$tinymce:refresh', id)
                }            
            })
        }
    }

    function addContentLine() {
        /*var continut = 'ceva';
        for (x in edit.item.tr_id) {
            if (edit.item.tr_id[x].content === !0) {
                continut = edit.item.tr_id[x].article
            }
        }
        if (!continut) {
            edit.openModal(edit, 'Alert', 'Please fill the \'Article\' field.', 'sm');
            return !1
        }*/
        var content = {
            content: !0,
            article: '',
            tr_id: "tmp" + (new Date().getTime()),
        }
        edit.item.tr_id.push(content)
    }

    function save() {
        angular.element('.loading_wrap').removeClass('hidden');
        var data = angular.copy(edit.item);
        //console.log(data);
        data.do = data.do_next;
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.success) {
                $state.go('purchase_order.view', {
                    p_order_id: r.data.p_order_id
                })
            }
        })
    }

    function cancel() {
        if (edit.item_id == 'tmp') {
            $state.go('purchase_orders')
        } else {
            $state.go('purchase_order.view', {
                p_order_id: edit.item_id
            })
        }
    }

    function changeLang() {
        var data = {
            'do': 'po_order-npo_order',
            p_order_id: edit.item_id,
            'email_language': edit.item.email_language,
            'languages': edit.item.email_language,
            'xget': 'notes'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            if (r && r.data) {
                edit.item.translate_loop = r.data.translate_loop;
                edit.item.translate_loop_custom = r.data.translate_loop_custom;
                edit.item.translate_lang_active = r.data.translate_lang_active;
                edit.item.languages = edit.item.email_language
            }
        })
    }

    function changeDelivery() {
        if (edit.item.client_delivery == !1) {
            edit.item.sc_id = 0;
            edit.item.second_address_id = 0
        }
    }

    function showAlerts(d) {
        helper.showAlerts(edit, d)
    }

    function addPurchaseOrderArticle() {
        edit.showTxt.addArticleBtn = !0;
        $timeout(function() {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.open()
        })
    }

    function addLine() {
        edit.item.packing = 1;
        edit.item.sale_unit = 1;
        editItem.addPurchaseOrderArticle(edit, edit.item)
    }

    function changeDiscount() {
        if (edit.item.apply_discount_line == !0 && edit.item.apply_discount_global == !1) {
            edit.item.apply_discount = 1
        } else if (edit.item.apply_discount_global == !0 && edit.item.apply_discount_line == !1) {
            edit.item.apply_discount = 2
        } else if (edit.item.apply_discount_line == !0 && edit.item.apply_discount_global == !0) {
            edit.item.apply_discount = 3
        } else if (edit.item.apply_discount_line == !1 && edit.item.apply_discount_global == !1) {
            edit.item.apply_discount = 0
        }
        if (edit.item.apply_discount == 0 || edit.item.apply_discount == 2) {
            if (edit.item.hide_disc === !0) {
                for (x in edit.item.tr_id) {
                    if (edit.item.tr_id[x].content === !1) {
                        edit.item.tr_id[x].colum++
                    }
                }
                edit.item.colum++;
                edit.item.hide_disc = !1
            }
        } else {
            if (edit.item.hide_disc === !1) {
                for (x in edit.item.tr_id) {
                    if (edit.item.tr_id[x].content === !1) {
                        edit.item.tr_id[x].colum--
                    }
                }
                edit.item.colum--;
                edit.item.hide_disc = !0
            }
        }
        if (edit.item.apply_discount > 1) {
            edit.item.discount_percent = '( ' + edit.item.discount + '% )';
            edit.item.hide_global_discount = !0
        } else {
            edit.item.discount_percent = '( ' + helper.displayNr(0) + '% )';
            edit.item.hide_global_discount = !1
        }
        edit.calcTotalPurchase()
    }

    function applyLineDiscount() {
        for (x in edit.item.tr_id) {
            if (edit.item.tr_id[x].content === !1) {
                edit.item.tr_id[x].disc = edit.item.discount_line_gen
            }
        }
        edit.calcTotalPurchase()
    }

    edit.contactUpdated=function(language){
        //if(edit.p_order_id && !isNaN(edit.p_order_id)){
            if(language && language>0 && language != edit.item.email_language){
                var selectize = angular.element('#email_language')[0].selectize;
                selectize.addItem(language);
            }
        //}
    }

}]).controller('deliveryAddressModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.obj = data;
    $scope.ok = function() {
        $scope.closeModal($scope.address)
    };
    $scope.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    $scope.setItem = function(item) {
        $scope.address = [item.address + "<br>" + item.zip + " " + item.city + "<br>" + item.country, item.address + "\n" + item.zip + " " + item.city + "\n" + item.country]
    }
}]).controller('addArticlePOModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'editItem', 'data', 'list', function($scope, helper, $uibModalInstance, editItem, data, list) {
    var originalScope = editItem.getScope();
    $scope.obj = data;
    $scope.max_rows = data.max_rows;
    $scope.lr = data.lr;
    $scope.search = {
        search: '',
        'do': 'po_order-addArticleList',
        customer_id: $scope.obj.customer_id,
        lang_id: $scope.obj.lang_id,
        offset: 1,
        cat_id: $scope.obj.cat_id,
        remove_vat: $scope.obj.remove_vat,
        vat_regime_id: $scope.obj.vat_regime_id
    };
    $scope.extraParams = {
        customer_id: $scope.obj.customer_id,
        lang_id: $scope.obj.lang_id,
        cat_id: $scope.obj.cat_id
    };
    $scope.filterCfgFam = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Families'),
        create: !1,
        onChange(value) {
            $scope.toggleSearchButtons('article_category_id', value)
        },
        maxItems: 1
    };
    $scope.ok = function(item) {
        if (originalScope.app == 'po_order' || originalScope.app == 'stock') {
            editItem.addPurchaseOrderArticle(originalScope, item)
        }
        item.succes_add = !0
    };
    $scope.createItem = function() {
        editItem.openModal($scope, 'AddAnArticle', {
            'do': 'misc-addAnArticle',
            customer_id: $scope.obj.customer_id,
            lang_id: $scope.obj.lang_id,
            cat_id: $scope.obj.cat_id,
            remove_vat: $scope.obj.remove_vat,
            vat_regime_id: $scope.obj.vat_regime_id
        }, 'md')
    }
    $scope.renderList = function(d) {
        if (d.data) {
            $scope.obj = d.data;
            $scope.lr = d.data.lr;
            $scope.offset = d.data.offset;
            $scope.max_rows = d.data.max_rows
        }
    };
    $scope.toggleSearchButtons = function(item, val) {
        $scope.search[item] = val;
        helper.doRequest('get', 'index.php?do=misc-addArticleList', {
            search: $scope.search.search,
            offset: $scope.obj.offset,
            customer_id: $scope.obj.customer_id,
            lang_id: $scope.obj.lang_id,
            remove_vat: $scope.obj.remove_vat,
            vat_regime_id: $scope.obj.vat_regime_id,
            article_category_id: $scope.search.article_category_id,
            app: $scope.obj.app
        }, $scope.renderList)
    };
    $scope.searchThing = function() {
        helper.searchIt($scope.search, $scope.renderList)
    }
    $scope.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }

    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };

}]).controller('purchasePriceModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.obj = data;
    $scope.ok = function() {
        $scope.closeModal($scope.obj)
    };
    $scope.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('serialNrSModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {;
    $scope.obj = data;
    $scope.slectItem = function(item, from, to, $index) {
        $scope.obj[to].push(item);
        $scope.obj[from].splice($index, 1);
        $scope.obj.sel_s_n = $scope.obj.sel_serial_nr_row.length
    }
    $scope.ok = function(d) {
        if ($scope.obj.sel_s_n != $scope.obj.total_s_n) {
            if (!$scope.alerts) {
                $scope.alerts = []
            }
            $scope.alerts.push({
                type: 'danger',
                msg: 'Serial Numbers quantity is incorrect'
            });
            return !1
        }
        $uibModalInstance.close(d)
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('serialNrPModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.obj = data;
    $scope.serialNrPChange = function() {
        $scope.obj.completed = 0;
        for (x in $scope.obj.s_n_row) {
            if ($scope.obj.s_n_row[x].serial_number) {
                $scope.obj.completed++
            }
        }
    }
    $scope.ok = function(d) {
        if ($scope.obj.completed != $scope.obj.total) {
            if (!$scope.alerts) {
                $scope.alerts = []
            }
            $scope.alerts.push({
                type: 'danger',
                msg: 'Complete all the fields'
            });
            return !1
        }
        $uibModalInstance.close(d)
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('batchNrPModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.obj = data;
    $scope.obj.removeLine = removeLine;
    $scope.obj.article_no = data.article_no;
    $scope.obj.q_added = data.q_added;
    $scope.obj.b_n_row = data.b_n_row;
    $scope.format = helper.settings.PICK_DATE_FORMAT;
    $scope.datePickOpen = {
        date: !1
    };

    function removeLine($i) {
        if ($i > -1) {
            $scope.obj.b_n_row.splice($i, 1)
        }
    }
    $scope.obj.addLine = function(d) {
        var line = {
            tr_id: "tmp" + (new Date().getTime())
        }
        $scope.obj.b_n_row.push(line)
    }
    $scope.ok = function(d) {
        var bno = 0;
        for (x in $scope.obj.b_n_row) {
            if ($scope.obj.b_n_row[x].batch_number_v) {
                bno++
            }
        }
        if ($scope.obj.b_n_row.length != bno) {
            if (!$scope.alerts) {
                $scope.alerts = []
            }
            $scope.alerts.push({
                type: 'danger',
                msg: 'Complete all the fields'
            });
            return !1
        }
        if ($scope.obj.article_no != $scope.obj.q_added) {
            if (!$scope.alerts) {
                $scope.alerts = []
            }
            $scope.alerts.push({
                type: 'danger',
                msg: 'Quantities must be the same'
            });
            return !1
        }
        $uibModalInstance.close(d)
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    $scope.batchNrPChange = function() {
        $scope.obj.q_added = 0;
        for (x in $scope.obj.b_n_row) {
            if ($scope.obj.b_n_row[x].batch_number_q) {
                $scope.obj.q_added += parseFloat(helper.return_value($scope.obj.b_n_row[x].batch_number_q)) * 1
            }
        }
        $scope.obj.q_added = helper.displayNr($scope.obj.q_added);

    }
    $scope.batchExpDateChange = function(batch_number, order_articles_id, i) {
        var data = {
            'do': 'po_order--po_order-get_article_batch_expdate',
            'batch_number': batch_number,
            'order_articles_id': order_articles_id,
            'i': i
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.data.date_exp_js) {
                $scope.obj.b_n_row[i].date_h = new Date(r.data.date_exp_js)
            }
        })
    }
}]).controller('batchNrModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.obj = data;
    $scope.ok = function(d) {
        //console.log($scope.obj);return false;
        if ($scope.obj.selected_b_n != $scope.obj.total_b_n) {
            if (!$scope.alerts) {
                $scope.alerts = []
            }
            $scope.alerts.push({
                type: 'danger',
                msg: 'Article quantity is incorrect'
            });
            return !1
        }
        for (x in $scope.obj.b_n_row) {
            if ($scope.obj.b_n_row[x].batch_no_checked && $scope.obj.b_n_row[x].is_error) {
                if (!$scope.alerts) {
                    $scope.alerts = []
                }
                $scope.alerts.push({
                    type: 'danger',
                    msg: 'Article quantity is incorrect'
                });
                return !1
            }
        }
        $uibModalInstance.close(d)
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    $scope.batchNrChange = function($index) {
        $scope.alerts = [];
        $scope.obj.total_b_n = 0;
        for (x in $scope.obj.b_n_row) {
            if ($scope.obj.b_n_row[x].batch_no_checked) {
                $scope.obj.total_b_n += parseFloat(helper.return_value($scope.obj.b_n_row[x].b_n_q_selected)) * 1
            }
        }
        //console.log($scope.obj.b_n_row[$index].b_n_q_selected, $scope.obj.b_n_row[$index].in_stock,$scope.obj.packing, $scope.obj.sale_unit, parseFloat(helper.return_value($scope.obj.b_n_row[$index].b_n_q_selected)) * 1, parseFloat(helper.return_value($scope.obj.b_n_row[$index].b_n_q_selected)) );
        if ($scope.obj.b_n_row[$index].b_n_q_selected > $scope.obj.b_n_row[$index].in_stock / $scope.obj.packing * $scope.obj.sale_unit) {
            $scope.obj.b_n_row[$index].is_error = true;
        } else {
            $scope.obj.b_n_row[$index].is_error = false;
        }
        $scope.obj.total_b_n = helper.displayNr($scope.obj.total_b_n);
    }
    $scope.batchSelChange = function() {
        $scope.obj.selected_batches = 0;
        $scope.obj.total_b_n = 0;
        for (x in $scope.obj.b_n_row) {
            if ($scope.obj.b_n_row[x].batch_no_checked) {
                $scope.obj.selected_batches++;
                $scope.obj.total_b_n += parseFloat(helper.return_value($scope.obj.b_n_row[x].b_n_q_selected)) * 1
            } else {
                $scope.obj.b_n_row[x].b_n_q_selected = helper.displayNr(0);
            }
        }
        $scope.obj.total_b_n = helper.displayNr($scope.obj.total_b_n);
    }

    $scope.batchLocationsCfg={
        valueField:'address_id',
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select'),
        maxItems: 1
    };

}]).controller('AlertModalCtrl', ['$scope', '$uibModalInstance', 'data', function($scope, $uibModalInstance, data) {
    var vmMod = this
    vmMod.obj = data;
    vmMod.ok = function() {
        $uibModalInstance.close()
    };
    vmMod.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('DispatchModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.ok = ok;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;

    function ok() {
        helper.doRequest('post', 'index.php', vmMod.obj, function(d) {
            if (d.success) {
                $uibModalInstance.close(d)
            } else {
                vmMod.showAlerts(d)
            }
        })
    };

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    };

    function showAlerts(d) {
        helper.showAlerts(vmMod, d)
    }
}]).controller('PurchaseOrderSettingsCtrl', ['$scope', 'helper', '$uibModal', 'settingsFc', 'selectizeFc', 'data', function($scope, helper, $uibModal, settingsFc, selectizeFc, data) {
    var vm = this;
    vm.app = 'po_order';
    vm.ctrlpag = 'settings';
    vm.tmpl = 'potemplate';
    settingsFc.init(vm);
    vm.logos = {};
    vm.settings = {
        show_bank: !1,
        show_stamp: !1,
        show_page: !1,
        show_paid: !1
    };
    vm.customLabel = {};
    vm.custom = {
        pdf_active: !1,
        pdf_note: !1,
        pdf_condition: !1,
        pdf_stamp: !1
    };
    vm.custom.layouts = {
        clayout: [],
        selectedOption: '0'
    };
    vm.message = {};
    vm.email_default = {};
    vm.weblink = {};
    vm.creditnote = {};
    vm.timesheets = {};
    vm.chargevats = {};
    vm.exportsettings = {};
    vm.vats = {};
    vm.sepa_active = {};
    vm.convention = {};
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select type'),
        labelField: 'name',
        searchField: ['name'],
    });
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 850,
        height: 300,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.load = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'po_order-settings',
            'xget': h
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
            if (k == 'logos') {
                vm.default_logo = r.data.default_logo
            }
        })
    }
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            vm.selected = selectedItem
        }, function() {
            if (r.load) {
                vm.load(r.load[0], r.load[1])
            }
        })
    };
    vm.modal_tax = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.vat = data.data.vat;
            vm.language = data.data.language
        }, function() {})
    };
    vm.modal_small = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.vat = data.data.vat;
            vm.language = data.data.language;
            vm.dispatch_address = data.data.dispatch_address
        }, function() {})
    };
    vm.changedelivery = function() {
        var get = 'delivery';
        var data = {
            'do': 'po_order-settings-po_order-changedelivery',
            'xget': get,
            'delivery_type': vm.deliverySettings.delivery.delivery_type
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.deliverySettings.delivery = r.data.delivery;
            //vm.showAlerts(vm, r)
        })
    }
    vm.Convention = function() {
        var get = 'convention';
        var data = angular.copy(vm.convention);
        data.do = 'po_order-settings-po_order-Convention';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.convention = r.data.convention;
            vm.showAlerts(vm, r)
        })
    }
    vm.DispatchUpdate = function() {
        var get = 'dispatch';
        var data = angular.copy(vm.dispatch);
        data.do = 'po_order-settings-po_order-naming_set';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.dispatch = r.data.dispatch;
            vm.showAlerts(vm, r)
        })
    }
    vm.updatestock = function() {
        var data = angular.copy(vm.stock);
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.stock = r.data.stock;
            vm.showAlerts(vm, r)
        })
    }
    vm.saveterm = function() {
        var data = {
            'do': 'po_order-settings-po_order-saveterm',
            'xget': 'deliverySettings',
            'edit_confirmed_po_orders': vm.deliverySettings.edit_confirmed_po_orders
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.deliverySettings.edit_confirmed_po_orders = r.data.deliverySettings.edit_confirmed_po_orders;
            vm.showAlerts(vm, r)
        })
    }
    vm.changelanguagedispatch = function() {
        var data = angular.copy(vm.dispatch_note);
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.dispatch_note = r.data.dispatch_note;
            vm.showAlerts(vm, r)
        })
    }
    vm.savelanguagedispatch = function(vm, languages, notes) {
        var get = 'dispatch_note';
        var data = {
            'do': 'po_order-settings-po_order-note_set',
            'xget': get,
            'languages': languages,
            'notes': notes
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.dispatch_note = r.data.dispatch_note;
            vm.showAlerts(vm, r)
        })
    }
    vm.deleteaddress = function(vm, address) {
        var get = 'dispatch_address';
        var data = {
            'do': 'po_order-settings-po_order-delete_address',
            'xget': get,
            'address_id': address
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.dispatch_address = r.data.dispatch_address;
            vm.showAlerts(vm, r)
        })
    }
    vm.setAddress = function(vm, address) {
        var get = 'dispatch_address';
        var data = {
            'do': 'po_order-settings-po_order-set_main',
            'xget': get,
            'address_id': address
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.dispatch_address = r.data.dispatch_address;
            vm.showAlerts(vm, r)
        })
    }
    vm.clickyArticle = function(scope, myValue) {
        if (!scope.articlefields.ADV_PRODUCT) {
            return !1
        }
        vm.clicky(myValue)
    }
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.articlefields.text = vm.articlefields.text.substring(0, startPos) + myValue + vm.articlefields.text.substring(endPos, vm.articlefields.text.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.articlefields.text += myValue;
            _this.focus()
        }
    }
    vm.clickys = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.message.html_content = vm.message.html_content.substring(0, startPos) + myValue + vm.message.html_content.substring(endPos, vm.message.html_content.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.message.html_content += myValue;
            _this.focus()
        }
    }
    vm.renderPage = function(r) {
        helper.doRequest('get', 'index.php?do=po_order-settings&xget=PDFlayout', function(r) {
            vm.layout_header = r.data.layout_header;
            vm.layout_body = r.data.layout_body;
            vm.layout_footer = r.data.layout_footer;
            vm.custom_layout = r.data.custom_layout;
            vm.has_custom_layout = r.data.has_custom_layout;
            vm.use_custom_layout = r.data.use_custom_layout;
            vm.settings.identity=r.data.identity;
            vm.settings.multiple_identity=r.data.multiple_identity;
        })
    };
    vm.renderPage(data)
}]).controller('PurchaseCustomPdfCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'po_order';
    vm.ctrlpag = 'custom_pdf';
    vm.tmpl = 'potemplate';
    settingsFc.init(vm);
    vm.codelabels = {};
    vm.selectlabels = {};
    vm.showtiny = !0;
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 1110,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile bold italic underline | alignleft aligncenter alignright | bullist numlist | link image", "forecolor backcolor | fontsizeselect |  table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload_image_pdf').trigger('click');
                $('#upload_image_pdf').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.codelabels.variable_data = vm.codelabels.variable_data.substring(0, startPos) + myValue + vm.codelabels.variable_data.substring(endPos, vm.codelabels.variable_data.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.codelabels.variable_data += myValue;
            _this.focus()
        }
    }
    vm.closeModal = closeModal;
    vm.cancel = cancel;

    function closeModal() {
        $uibModalInstance.close()
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    }
    helper.doRequest('get', 'index.php?do=po_order-custom_pdf&layout=' + obj.id + '&header=' + obj.header + '&footer=' + obj.footer+ '&identity_id=' + obj.identity_id, function(r) {
        vm.codelabels = {
            variable_data: r.data.codelabels.variable_data,
            layout: r.data.codelabels.layout,
            header: r.data.codelabels.header,
            footer: r.data.codelabels.footer,
            identity_id: r.data.codelabels.identity_id,
        };
        vm.selectlabels = {
            make_id: r.data.selectlabels.make_id,
            make: r.data.selectlabels.make,
            selected_make: r.data.selectlabels.selected_make
        };
        vm.selectdetails = {
            detail_id: r.data.selectdetails.detail_id,
            detail: r.data.selectdetails.detail
        }
    })
}]).controller('PurchaselabelCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'po_order';
    vm.ctrlpag = 'labelCtrl';
    vm.tmpl = 'potemplate';
    settingsFc.init(vm);
    vm.labels = {};
    helper.doRequest('get', 'index.php', obj, function(r) {
        vm.labels = r.data.labels
    });
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('DispatchAddressCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this,
        do_next = obj.address_id ? 'stock-settings-stock_dispatch-update_address' : 'stock-settings-stock_dispatch-add_address',
        do_xget = obj.address_id ? 'dispatch_address' : 'dispatch_address';
    vm.obj = obj;
    if (obj.address_id) {
        vm.obj.page_title = 'Edit Address'
    } else {
        vm.obj.naming = '';
        vm.obj.address = '';
        vm.obj.zip = '';
        vm.obj.city = '';
        vm.obj.country_id = '';
        vm.obj.page_title = 'Add Address';
        helper.doRequest('get', 'index.php?do=stock-settings&xget=dispatch_address', function(r) {
            vm.obj.countries = r.data.dispatch_address_add.countries
        })
    }
    vm.save_me = function(vm) {
        var data = angular.copy(vm);
        data.do = do_next;
        data.xget = do_xget;
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('dismiss')
    }
}]).controller('InvoicelabelCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'invoice';
    vm.ctrlpag = 'labelCtrl';
    vm.tmpl = 'iTemplate';
    settingsFc.init(vm);
    vm.labels = {};
    helper.doRequest('get', 'index.php', obj, function(r) {
        vm.labels = r.data.labels
    });
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('StockPickingCtrl', ['$scope', 'helper', 'list', 'modalFc', 'data', function($scope, helper, list, modalFc, data) {
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    $scope.views = [{
        name: helper.getLanguage('All Statuses'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('To Be Picked'),
        id: 1,
        active: ''
    }, {
        name: helper.getLanguage('Partially Picked'),
        id: 2,
        active: ''
    }, {
        name: helper.getLanguage('Picked'),
        id: 3,
        active: ''
    }];
    $scope.activeView = 0;
    $scope.activeLocation = 0;
    $scope.list = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'stock-stock_picking',
        view: 0,
        location: undefined,
        showAdvanced: !1,
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.listStats = [];
            $scope.lid = d.data.lid;
            $scope.type = d.data.pdf_type;
            $scope.lr = d.data.lr;
            $scope.locations = d.data.locations
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1,
        stop_date: !1,
        d_start_date: !1,
        d_stop_date: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        var params = angular.copy($scope.search);
        //angular.element(".loading_wrap").removeClass("hidden");
        helper.doRequest("get", "index.php", params, $scope.renderList)
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onChange(value) {
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.locationFilterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Locations',
        create: !1,
        onChange(value) {
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.orderPicking = function(serv, location) {
        var obj = {
            service_id: serv,
            location_id: location
        }
        openModal("stock_picking", obj, "lg")
    };

    function openModal(type, item, size) {
        switch (type) {
            case 'stock_picking':
                var iTemplate = 'stockPicking';
                var ctas = 'art';
                break
        }
        var params = {
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'stock_picking':
                params.template = 'stocktemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'stock-stock_picking_details',
                    'service_id': item.service_id,
                    'location_id': item.location_id
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        list.init($scope, 'stock-stock_picking', $scope.renderList)
                    }
                }
                break
        }
        modalFc.open(params)
    }
    list.init($scope, 'stock-stock_picking', $scope.renderList)
}]).controller('stockPickingModalCtrl', ['$scope', '$compile', '$timeout', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, $compile, $timeout, helper, $uibModalInstance, data, modalFc) {
    var art = this;
    art.obj = data;
    art.obj.lines = data.query;
    art.obj.page_title = data.page_title;
    art.hideDetails = !0;
    art.showAlerts = function(d) {
        helper.showAlerts(art, d)
    }
    art.cancel = function() {
        $uibModalInstance.close()
    }
    art.changeQuantity = function(i) {
        var idx = 0;
        i.is_error = !1;
        if (art.obj.allow_stock) {
            for (x in art.obj.location_dd) {
                if (art.obj.location_dd[x].id == art.obj.select_main_location) {
                    idx = x
                }
            }
            for (x in i.disp_addres_info) {
                i.disp_addres_info[x].quantity_reserved = helper.displayNr(0);
                i.disp_addres_info[x].is_error = !1;
                if (x == idx) {
                    i.disp_addres_info[x].quantity_reserved = helper.displayNr(helper.return_value(i.quantity));
                    if (i.quantity * 1 > i.disp_addres_info[x].qty * 1) {
                        i.is_error = !0;
                        i.disp_addres_info[x].is_error = !0
                    }
                }
            }
        }
    }
    art.ok = function() {
        var obj = angular.copy(art.obj);
        obj.do = 'stock--stock_dispatch-pick_articles';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            art.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('StockSettingsCtrl', ['$scope', 'helper', '$uibModal', 'settingsFc', 'selectizeFc', 'data', function($scope, helper, $uibModal, settingsFc, selectizeFc, data) {
    var vm = this;
    vm.app = 'stock';
    vm.ctrlpag = 'settings';
    vm.tmpl = 'stocktemplate';
    settingsFc.init(vm);
    vm.logos = {};
    vm.settings = {
        show_bank: !1,
        show_stamp: !1,
        show_page: !1,
        show_paid: !1
    };
    vm.customLabel = {};
    vm.custom = {
        pdf_active: !1,
        pdf_note: !1,
        pdf_condition: !1,
        pdf_stamp: !1
    };
    vm.custom.layouts = {
        clayout: [],
        selectedOption: '0'
    };
    vm.message = {};
    vm.email_default = {};
    vm.weblink = {};
    vm.creditnote = {};
    vm.timesheets = {};
    vm.chargevats = {};
    vm.exportsettings = {};
    vm.vats = {};
    vm.sepa_active = {};
    vm.convention = {};
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select type'),
        labelField: 'name',
        searchField: ['name'],
    });
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 850,
        height: 300,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile bold italic underline | alignleft aligncenter alignright | bullist numlist | link image", "forecolor backcolor | fontsizeselect |  table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.load = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'stock-settings',
            'xget': h
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
            if (k == 'logos') {
                vm.default_logo = r.data.default_logo
            }
        })
    }
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            vm.selected = selectedItem
        }, function() {
            if (r.load) {
                vm.load(r.load[0], r.load[1])
            }
        })
    };
    vm.modal_tax = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.vat = data.data.vat;
            vm.language = data.data.language
        }, function() {})
    };
    vm.modal_small = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.vat = data.data.vat;
            vm.language = data.data.language;
            vm.dispatch_address = data.data.dispatch_address
        }, function() {})
    };
    vm.DispatchUpdate = function() {
        var get = 'dispatch';
        var data = angular.copy(vm.dispatch);
        data.do = 'stock-settings-stock_dispatch-naming_set_stock';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.dispatch = r.data.dispatch;
            vm.showAlerts(vm, r)
        })
    }
    vm.updatestock = function() {
        var data = angular.copy(vm.stock);
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.stock = r.data.stock;
            vm.showAlerts(vm, r)
        })
    }
    vm.changelanguagedispatch = function() {
        var data = angular.copy(vm.dispatch_note);
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.dispatch_note = r.data.dispatch_note;
            vm.showAlerts(vm, r)
        })
    }
    vm.savelanguagedispatch = function(vm, languages, notes) {
        var get = 'dispatch_note';
        var data = {
            'do': 'stock-settings-stock_dispatch-note_set',
            'xget': get,
            'languages': languages,
            'notes': notes
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.dispatch_note = r.data.dispatch_note;
            vm.showAlerts(vm, r)
        })
    }
    vm.deleteaddress = function(vm, address) {
        var get = 'dispatch_address';
        var data = {
            'do': 'stock-settings-stock_dispatch-delete_address',
            'xget': get,
            'address_id': address
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.dispatch_address = r.data.dispatch_address;
            vm.showAlerts(vm, r)
        })
    }
    vm.setAddress = function(vm, address) {
        var get = 'dispatch_address';
        var data = {
            'do': 'stock-settings-stock_dispatch-set_main',
            'xget': get,
            'address_id': address
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.dispatch_address = r.data.dispatch_address;
            vm.showAlerts(vm, r)
        })
    }
    vm.Changecodelabes = function(vm, r) {
        var get = 'emailMessage';
        var data = {
            'do': 'stock-settings',
            'xget': get,
            detail_id: r.detail_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            vm.message_labels = {
                detail_id: r.data.message_labels.detail_id,
                detail: r.data.message_labels.detail,
                selected_detail: r.data.message_labels.selected_detail
            };
            vm.showAlerts(vm, r)
        })
    }
    vm.clickyArticle = function(scope, myValue) {
        if (!scope.articlefields.ADV_PRODUCT) {
            return !1
        }
        vm.clicky(myValue)
    }
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.articlefields.text = vm.articlefields.text.substring(0, startPos) + myValue + vm.articlefields.text.substring(endPos, vm.articlefields.text.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.articlefields.text += myValue;
            _this.focus()
        }
    }
    vm.clickys = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.message.html_content = vm.message.html_content.substring(0, startPos) + myValue + vm.message.html_content.substring(endPos, vm.message.html_content.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.message.html_content += myValue;
            _this.focus()
        }
    }
    vm.renderPage = function(r) {
        helper.doRequest('get', 'index.php?do=stock-settings&xget=stock', function(r) {
            vm.stock = r.data.stock;
            vm.showAlerts(vm, r)
        })
    };
    vm.renderPage(data)
}]).controller('StockbatchesCtrl', ['$scope','$rootScope','helper', 'list', 'data', 'modalFc',function($scope, $rootScope,helper, list, data,modalFc) {
    $rootScope.opened_lists.stock_batches=true;
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;

    $scope.activeView = 0;
    $scope.list = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'stock-stock_batches',
        view: 0,
        xget: '',
        showAdvanced: !1,
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.isOpen = !1;
    $scope.export_args = '';
    $scope.minimum_selected=false;
    $scope.all_pages_selected=false;
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.minimum_selected=d.data.minimum_selected;
            $scope.all_pages_selected=d.data.all_pages_selected;
            $scope.export_args = d.data.export_args;
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1,
        stop_date: !1,
        d_start_date: !1,
        d_stop_date: !1
    }
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList, true)
    };
    $scope.searchThing = function(reset_list) {
        angular.element('.loading_wrap').removeClass('hidden');
        var params=angular.copy($scope.search);
        if(reset_list){
            params.reset_list='1';
        }     
        list.search(params, $scope.renderList, $scope)
    };

    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };

    $scope.markCheckNew=function(i){
        list.markCheckNew($scope, 'stock--stock_dispatch-saveAddToPdfBatch', i);
    }
    $scope.checkAllPage=function(){
        $scope.listObj.check_add_all=true;
        $scope.markCheckNew();
    }
    $scope.checkAllPages=function(){
        list.markCheckNewAll($scope, 'stock--stock_dispatch-saveAddToPdfBatchAll','1');
    }
    $scope.uncheckAllPages=function(){
        list.markCheckNewAll($scope, 'stock--stock_dispatch-saveAddToPdfBatchAll','0');
    }

    $scope.openModal=function(type, item, size, txt) {
        var params = {
            template: 'atemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            txt: txt,
            backdrop: 'static'
        };
        switch (type) {
            
            case 'editBatchNo':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'id': item
                };
                params.ctrlAs = 'editBatchNo';
                params.callback = function(data) {
                    if (data && data.data) {
                        $scope.searchThing();
                    }
                }
                break;         
        }
        modalFc.open(params)
    }

    list.init($scope, 'stock-stock_batches', $scope.renderList, ($rootScope.reset_lists.stock_batches ? true : false));
    $rootScope.reset_lists.stock_batches=false;

}]).controller('StockSerialCtrl', ['$scope', 'helper', 'list', 'selectizeFc', 'data', 'modalFc', function($scope, helper, list, selectizeFc, data, modalFc) {
    $scope.ord = '';
    $scope.max_rows = 0;
    $scope.export_args = '';
    $scope.search = {
        search: '',
        'do': 'stock-stock_serial',
        view: 0,
        xget: '',
        showAdvanced: !1,
        offset: 1
    };

    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.listStats = [];
            $scope.lid = d.data.lid;
            $scope.type = d.data.pdf_type;
            $scope.lr = d.data.lr;
            $scope.export_args = d.data.export_args;
            $scope.columns=d.data.columns;
            $scope.categories_dd=d.data.categories_dd;
        }
    };

    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };

    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
        $scope.checkSearch()
    };
    $scope.checkSearch = function() {
        var data = angular.copy($scope.search),
            is_search = !1;
        data = helper.clearData(data, ['do', 'xget', 'showAdvanced', 'showList', 'offset']);
        for (x in data) {
            if (data[x]) {
                is_search = !0
            }
        }
        // vm.search.showList = is_search
    }
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };

    $scope.filterCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Product categorie'),
        onChange(value) {
            $scope.toggleSearchButtons('product_id', value)
        }
    });

    list.init($scope, 'stock-stock_serial', $scope.renderList)

}])