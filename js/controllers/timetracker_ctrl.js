app.controller('timetracktasksCtrl', ['$scope', 'helper', 'list', 'selectizeFc', 'data', 'modalFc', function($scope, helper, list, selectizeFc, data, modalFc) {
    console.log("Tasks");
    $scope.views = [{
        value: helper.getLanguage('Active'),
        id: 1,
    },{
        value: helper.getLanguage('Archived'),
        id: -1,
    }];
    // $scope.activeView = 1;
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'timetracker-tasks',
        view: 0,
        xget: '',
        offset: 1,
        'archived': '1',
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.can_do = d.data.can_do
        }
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
        $scope.checkSearch()
    };
    $scope.checkSearch = function() {
        var data = angular.copy($scope.search),
            is_search = !1;
        // data = helper.clearData(data, ['do', 'xget', 'showAdvanced', 'showList', 'offset']);
        for (x in data) {
            if (data[x]) {
                is_search = !0
            }
        }
        // vm.search.showList = is_search
    }
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    }
    // $scope.filterCfg = {
    //     valueField: 'id',
    //     labelField: 'name',
    //     searchField: ['name'],
    //     delimiter: '|',
    //     placeholder: helper.getLanguage('All Statuses'),
    //     create: !1,
    //     onItemAdd(value) {
    //         if (value == undefined) {
    //             value = 0
    //         }
    //         $scope.search.archived = 0;
    //         if (value == -1) {
    //             $scope.search.archived = 1
    //         }
    //         $scope.filters(value)
    //     },
    //     maxItems: 1
    // };
    $scope.filterCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('All Statuses'),
        onChange(value) {
            $scope.toggleSearchButtons('archived', value)
        }
    });
    $scope.addTask = function(id) {
        if (!$scope.can_do) {
            return !1
        }
        openModal('add_task', id, 'md')
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'add_task':
                var iTemplate = 'TimeTaskEdit';
                var ctas = 'vm';
                break
        }
        var params = {
            template: 'tTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'add_task':
                params.params = {
                    'do': 'timetracker-tasks',
                    'xget': 'task',
                    'task_id': item
                };
                params.callback = function(r) {
                    $scope.searchThing();
                    $scope.showAlerts(r)
                }
                break
        }
        modalFc.open(params)
    }
    $scope.renderList(data)
}]).controller('TimeTaskEditModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.cancel = function() {
        $uibModalInstance.dismiss()
    }
    vm.showAlerts = function(d, formobj) {
        helper.showAlerts(vm, d, formobj)
    }
    vm.ok = function(formobj) {
        var obj = angular.copy(vm.obj);
        obj.do = obj.do_next;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.error) {
                vm.showAlerts(r, formobj)
            }
            if (r.success || r.notice) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('TimetrackSettingsCtrl', ['$scope', 'helper', '$uibModal', 'settingsFc', 'data', 'selectizeFc', function($scope, helper, $uibModal, settingsFc, data, selectizeFc) {
    var vm = this;
    vm.app = 'timetracker';
    vm.ctrlpag = 'settings';
    vm.tmpl = 'tTemplate';
    settingsFc.init(vm);
    vm.logos = {};
    vm.settings = {
        client: !1,
        project: !1,
        task: !1,
        comment: !1
    };
    vm.customLabel = {};
    vm.custom = {
        pdf_active: !1,
        pdf_note: !1,
        pdf_condition: !1,
        pdf_stamp: !1
    };
    vm.custom.layouts = {
        clayout: [],
        selectedOption: '0'
    };
    vm.message = {};
    vm.email_default = {};
    vm.weblink = {};
    vm.creditnote = {};
    vm.timesheets = {};
    vm.chargevats = {};
    vm.exportsettings = {};
    vm.vats = {};
    vm.sepa_active = {};
    vm.convention = {};
    vm.expenses = {};
    vm.expenses_add = {};
    vm.timesheet = {};
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 850,
        height: 300,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        //toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.salesCfg = selectizeFc.configure({
        maxOptions: 100,
        placeholder: helper.getLanguage('Select Internal Company'),
        onChange(value) {
            var data = {
                'do': 'timetracker-settings-project-add_internal',
                'xget': 'internal',
                customer_id: value
            };
            helper.doRequest('post', 'index.php', data, function(r) {
                vm.internal = r.data.internal;
                vm.showAlerts(vm, r)
            })
        }
    });
    vm.deleteexpense = function(vm, Q_ID) {
        var get = 'expenses';
        var data = {
            'do': 'timetracker-settings-project-delete_expense',
            'xget': get,
            'Q_ID': Q_ID
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.expenses = r.data.expenses;
            vm.showAlerts(vm, r)
        })
    }
    vm.save_conv = function() {
        var get = 'name_convention';
        var data = angular.copy(vm.name_convention);
        data.do = 'timetracker-settings-project-project_naming';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.name_convention = r.data.name_convention;
            vm.showAlerts(vm, r)
        })
    }
    vm.updateHtml = function() {
        var get = 'emailMessage';
        var data = {
            'do': 'timetracker-settings-project-use_html',
            'xget': get,
            'use_html': vm.message.USE_HTML,
            'lang_code': vm.message.LANG_ID
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.message = r.data.message;
            vm.showAlerts(vm, r)
        })
    }
    vm.saveMessage = function() {
        var get = 'emailMessage';
        var data = angular.copy(vm.message);
        data.do = 'timetracker-settings-project-default_message';
        data.xget = 'emailMessage';
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.message = r.data.message;
            vm.showAlerts(vm, r)
        })
    }
    vm.ChangeLang = function() {
        var get = 'emailMessage';
        var data = {
            'do': 'timetracker-settings',
            'xget': get,
            'lang_code': vm.message.LANG_ID
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            vm.message = r.data.message;
            vm.showAlerts(vm, r)
        })
    }
    vm.deleteaddress = function() {
        var get = 'emailMessage';
        var data = {
            'do': 'timetracker-settings',
            'xget': get,
            'lang_code': vm.message.LANG_ID
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            vm.message = r.data.message;
            vm.showAlerts(vm, r)
        })
    }
    vm.updatetimesheet = function() {
        var get = 'timesheet';
        var data = angular.copy(vm.timesheet);
        data.do = 'timetracker-settings-project-setcnst';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.timesheet = r.data.timesheet;
            vm.showAlerts(vm, r)
        })
    }
    vm.updateLayout = function() {
        var get = 'PDFlayout';
        var data = {
            'do': 'timetracker--project-settings',
            'xget': get,
            client: vm.settings.client,
            'project': vm.settings.project,
            'task': vm.settings.task,
            'comment': vm.settings.comment
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.Changecodelabes = function(vm, r) {
        var get = 'emailMessage';
        var data = {
            'do': 'invoice-settings',
            'xget': get,
            detail_id: r.detail_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            vm.message_labels = {
                detail_id: r.data.message_labels.detail_id,
                detail: r.data.message_labels.detail,
                selected_detail: r.data.message_labels.selected_detail
            };
            vm.showAlerts(vm, r)
        })
    }
    vm.AddField = function(vm, id, table, name) {
        var get = 'business';
        var data = {
            'do': 'timetracker-settings-project-add_field',
            'xget': get,
            'name': name,
            'table': table
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            vm.showAlerts(vm, r)
        })
    }
    vm.DeleteField = function(vm, field_id, table) {
        var get = 'business';
        var data = {
            'do': 'timetracker-settings-project-DeleteField',
            'xget': get,
            'field_id': field_id,
            'table': table
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            vm.showAlerts(vm, r)
        })
    }
    vm.updatefield = function(vm, id, table, value) {
        var get = 'business';
        var data = {
            'do': 'timetracker-settings-project-UpdateField',
            'xget': get,
            'id': id,
            'table': table,
            'value': value
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            vm.showAlerts(vm, r)
        })
    }
    vm.AddFieldInternal = function(item) {
        var internal = {
            field_id: '',
            label: helper.getLanguage('Internal Companies') + ' ' + (item.small.length + 1),
            value: ''
        };
        item.small.push(internal)
    }
    vm.DeleteFieldInternal = function(vm, field_id, table) {
        var get = 'internal';
        var data = {
            'do': 'timetracker-settings-project-delete_internal',
            'xget': get,
            'customer_id': field_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.internal = r.data.internal;
            vm.showAlerts(vm, r)
        })
    }
    vm.load = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'timetracker-settings',
            'xget': h
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
            if (k == 'logos') {
                vm.default_logo = r.data.default_logo
            }
        })
    }
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            vm.selected = selectedItem
        }, function() {
            if (r.load) {
                vm.load(r.load[0], r.load[1])
            }
        })
    };
    vm.modal_small = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    var i = angular.copy(r);
                    return i
                }
            }
        });
        modalInstance.result.then(function(r) {
            console.log('e', r);
            if (r && r.data) {
                for (x in r.data) {
                    vm[x] = r.data[x]
                }
            }
        }, function() {
            if (r.load) {
                vm.load(r.load[0], r.load[1])
            }
        })
    };
    vm.PageActive = function() {
        var get = 'general';
        var data = {
            'do': 'invoice--invoice-PageActive',
            'xget': get,
            'page': vm.general_condition.page
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.articlefields.text = vm.articlefields.text.substring(0, startPos) + myValue + vm.articlefields.text.substring(endPos, vm.articlefields.text.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.articlefields.text += myValue;
            _this.focus()
        }
    }
    vm.clickys = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.message.html_content = vm.message.html_content.substring(0, startPos) + myValue + vm.message.html_content.substring(endPos, vm.message.html_content.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.message.html_content += myValue;
            _this.focus()
        }
    }
    vm.renderPage = function(r) {
        vm.expenses = r.data.expenses;
        vm.expenses_add = r.data.expenses_add;
        helper.doRequest('get', 'index.php?do=timetracker-settings&xget=PDFlayout', function(r) {
            vm.settings = {
                client: r.data.client,
                project: r.data.project,
                task: r.data.task,
                comment: r.data.comment,
                selected_header: r.data.selected_header,
                selected_body: r.data.selected_body,
                selected_footer: r.data.selected_footer
            };
            vm.layout_header = r.data.layout_header;
            vm.layout_body = r.data.layout_body;
            vm.layout_footer = r.data.layout_footer;
            vm.custom_layout = r.data.custom_layout
        })
    };
    vm.renderPage(data)
}]).controller('TimesheetViewCtrl', ['$scope', 'helper', 'data', 'modalFc', function($scope, helper, data, modalFc) {
    console.log('TimesheetViewCtrl');
    var info = this;
    info.obj = data.data;

    for(let x in info.obj.task_row){
        if(info.obj.task_row[x].project_type=='0'){
            for(let y in info.obj.task_row[x].task_day_row){
                info.obj.task_row[x].task_day_row[y].HOURSB=convert_to_time(info.obj.task_row[x].task_day_row[y].HOURSB);
            }
        }
    }

    info.activeType = 2;
    info.types = [{
        name: 'Weekly',
        id: 2,
        active: 'active'
    }, {
        name: 'Monthly',
        id: 3,
        active: ''
    }];
    info.subtotal = [{
        'total_h_wo_0': 0,
        'total_h_0': '',
        'total_d_0': ''
    }, {
        'total_h_wo_1': 0,
        'total_h_1': '',
        'total_d_1': ''
    }, {
        'total_h_wo_2': 0,
        'total_h_2': '',
        'total_d_2': ''
    }, {
        'total_h_wo_3': 0,
        'total_h_3': '',
        'total_d_3': ''
    }, {
        'total_h_wo_4': 0,
        'total_h_4': '',
        'total_d_4': ''
    }, {
        'total_h_wo_5': 0,
        'total_h_5': '',
        'total_d_5': ''
    }, {
        'total_h_wo_6': 0,
        'total_h_6': '',
        'total_d_6': ''
    }];
    info.timesheet = {
        start: 0
    };
    info.auto = {
        project: [],
        task: [],
        customer: [],
    };
    info.auto.project = data.projects_list;
    info.auto.customer = data.customers_list;
    info.auto.task = data.tasks_list;
    init_calculus();

    function init_calculus() {
        for (var i = 0; i < 7; i++) {
            var subtotal_h = 0;
            var subtotal_d = 0;
            for (x in data.task_row) {
                if (data.task_row[x].project_type == '0') {
                    subtotal_h += parseFloat(data.task_row[x].task_day_row[i].HOURS)
                } else {
                    subtotal_d += parseFloat(data.task_row[x].task_day_row[i].HOURS)
                }
            }
            if (subtotal_h > 0) {
                info.subtotal[i]['total_h_wo_' + i] = subtotal_h;
                info.subtotal[i]['total_h_' + i] = corect_val(subtotal_h)
            } else {
                info.subtotal[i]['total_h_wo_' + i] = 0;
                info.subtotal[i]['total_h_' + i] = ''
            }
            if (subtotal_d > 0) {
                info.subtotal[i]['total_d_' + i] = subtotal_d
            } else {
                info.subtotal[i]['total_d_' + i] = ''
            }
        }
        var total_h = 0;
        var total_d = 0;
        for (x in info.obj.task_row) {
            var line_h = 0;
            var line_d = 0;
            if (info.obj.task_row[x].project_type == '0') {
                for (y in info.obj.task_row[x].task_day_row) {
                    if (info.obj.task_row[x].task_day_row[y].HOURS != '') {
                        line_h += parseFloat(info.obj.task_row[x].task_day_row[y].HOURS)
                    }
                }
            } else {
                for (y in info.obj.task_row[x].task_day_row) {
                    if (info.obj.task_row[x].task_day_row[y].HOURS != '') {
                        line_d += parseFloat(info.obj.task_row[x].task_day_row[y].HOURS)
                    }
                }
            }
            total_h += line_h;
            total_d += line_d;
            if (line_h > 0) {
                info.obj.task_row[x].line_h_wo = line_h;
                info.obj.task_row[x].line_h = corect_val(line_h)
            } else {
                info.obj.task_row[x].line_h_wo = 0;
                info.obj.task_row[x].line_h = ''
            }
            if (line_d > 0) {
                info.obj.task_row[x].line_d = line_d
            } else {
                info.obj.task_row[x].line_d = ''
            }
        }
        if (total_h > 0) {
            info.obj.total_h_wo = total_h;
            info.obj.total_h = corect_val(total_h)
        } else {
            info.obj.total_h_wo = 0;
            info.obj.total_h = ''
        }
        if (total_d > 0) {
            info.obj.total_d = total_d
        } else {
            info.obj.total_d = ''
        }
    }
    info.init_calculus = init_calculus;
    info.showAlerts = function(d) {
        helper.showAlerts(info, d)
    }
    info.cancel = function() {
        $uibModalInstance.close()
    }
    info.renderPage = function(d) {
        if (d.data) {
            info.obj = d.data;
            for(let x in info.obj.task_row){
                if(info.obj.task_row[x].project_type=='0'){
                    for(let y in info.obj.task_row[x].task_day_row){
                        info.obj.task_row[x].task_day_row[y].HOURSB=convert_to_time(info.obj.task_row[x].task_day_row[y].HOURSB);
                    }
                }
            }
            if (info.obj.tab == 1 && info.activeType < 3) {
                info.auto.project = d.data.projects_list;
                info.auto.customer = d.data.customers_list;
                info.auto.task = d.data.tasks_list;
                if (info.activeType == 2) {
                    init_calculus()
                }
            }
        }
    }
    info.getTimesheet = function() {
        info.activeType = 2;
        helper.doRequest('get', 'index.php?do=timetracker-timesheet&start=' + info.obj.start + '&user_id=' + info.obj.user_id, info.renderPage)
    }
    info.getExpense = function() {
        helper.doRequest('get', 'index.php?do=timetracker-timesheet_expense&start=' + info.obj.start + '&user_id=' + info.obj.user_id, info.renderPage)
    }
    info.customerCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Users'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 1
    };
    info.typeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Overview'),
        create: !1,
        onChange(value) {
            var type_r = '';
            switch (value) {
                case '1':
                    type_r = 'Day';
                    break;
                case '2':
                    type_r = 'Week';
                    break;
                case '3':
                    type_r = 'Month';
                    break
            }
            info.timesheet.start = 0;
            //angular.element('.loading_alt').removeClass('hidden');
            helper.doRequest('get', 'index.php', {
                'do': 'timetracker-timesheet',
                'xget': type_r,
                'start': info.obj.start,
                'user_id': info.obj.user_id
            }, function(r) {
                //angular.element('.loading_alt').addClass('hidden');
                info.renderPage(r)
            })
        },
        maxItems: 1
    };
    info.projectAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select project'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value == undefined || value == '') {
                var data = {
                    'do': 'timetracker-timesheet',
                    xget: 'tasks_list'
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    info.auto.task = r.data
                })
            } else if (value != undefined) {
                var data = {
                    'do': 'timetracker-timesheet',
                    xget: 'tasks_list',
                    project_id: this.options[value].id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    info.auto.task = r.data
                })
            }
        },
        maxItems: 1
    };
    info.taskAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select task'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 100
    };
    info.changeDayData = function(cell, increment) {
        if ((cell.HOURSB == '1' && increment > 0) || (cell.HOURSB == '0' && increment < 0)) {
            return !1
        } else {
            if (isNaN(cell.HOURSB) || cell.HOURSB == '') {
                cell.HOURSB = '0';
                cell.HOURS = '0'
            }
            cell.HOURSB = parseFloat(cell.HOURSB) + increment;
            cell.HOURS = parseFloat(cell.HOURS) + increment
        }
        init_calculus();
        info.saveTimesheet(cell)
    }
    info.changeHourData = function(cell) {
        if (cell.IS_BILLED || cell.IS_APPROVED || cell.IS_SUBMITED || cell.IS_CLOSED) {
            return !1
        }
        cell.HOURS = convert_value(cell.HOURSB.getHours()+':'+cell.HOURSB.getMinutes());
        //cell.HOURSB = corect_val(cell.HOURSB);
        init_calculus();
        info.saveTimesheet(cell)
    }
    info.changeHourDataKey = function(event, cell) {
        if (event.keyCode == 13) {
            if (cell.IS_BILLED || cell.IS_APPROVED || cell.IS_SUBMITED || cell.IS_CLOSED) {
                return !1
            } else {
                info.changeHourData(cell)
            }
        }
    }
    info.showNote = function(cell) {
        openModal("show_note", cell, "md")
    }
    info.saveTimesheet = function(cell) {
        var data = angular.copy(cell);
        data.do = 'timetracker--time-add_hour_simple';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            info.showAlerts(r)
        })
    }
    info.approve = function(time, action) {
        var params = angular.copy(time);
        if (action == 'approve') {
            params.do = "timetracker-timesheet-time-approve";
            helper.doRequest('post', 'index.php', params, function(r) {
                if (r.success) {
                    info.obj.approve_text = helper.getLanguage('Approved timesheet');
                    info.obj.disabled1 = !0;
                    info.obj.btn_type2 = 'plain';
                    info.obj.reject_text = helper.getLanguage('Re-open');
                    info.obj.action2 = 'reopen'
                }
            })
        } else if (action == 'resubmit') {
            openModal("submit_time", params, "md")
        }
    }
    info.reject = function(time, action) {
        var data = angular.copy(time);
        if (action == 'reject') {
            openModal("reject_time", data, "md")
        } else if (action == 'reopen') {
            data.do = "timetracker-timesheet-time-remove_accept";
            helper.doRequest('post', 'index.php', data, function(r) {
                if (r.success) {
                    info.obj.approve_text = helper.getLanguage('Approve timesheet');
                    info.obj.disabled1 = !1;
                    info.obj.btn_type1 = 'primary';
                    info.obj.reject_text = helper.getLanguage('Reject timesheet');
                    info.obj.btn_type2 = 'secondary';
                    info.obj.disabled2 = !1;
                    info.obj.action2 = 'reject'
                }
            })
        }
    }
    info.deleteEntry = function(index) {
        var data = angular.copy(info.obj.task_row[index]);
        data.do = 'timetracker-timesheet-time-delete_time';
        data.start_date = info.obj.start;
        data.user_id = info.obj.user_id;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            info.showAlerts(r);
            if (r.success) {
                info.renderPage(r)
            }
        })
    }
    info.submitTime = function() {
        var data = angular.copy(info.obj);
        openModal("submit_time", data, "md")
    }
    info.addTask = function(type) {
        var data = {
            'activeType': info.activeType,
            'auto_project': angular.copy(info.auto.project),
            'auto_customer': angular.copy(info.auto.customer),
            'auto_task': angular.copy(info.auto.task),
            'item': angular.copy(info.obj),
            't_type': type
        };
        openModal('select_task', data, 'md')
    }
    info.unlock_time = function(task) {
        if (!task.approved && !task.billed && info.obj.can_unlock) {
            var obj = {
                "error": !1,
                "notice": {
                    "notice": helper.getLanguage("Entries unlocked")
                },
                "success": !1
            };
            info.showAlerts(obj);
            for (x in task.task_day_row) {
                task.task_day_row[x].IS_APPROVED = !1;
                task.task_day_row[x].IS_SUBMITED = !1;
                task.task_day_row[x].unlocked_manually = !0
            }
        }
    }
    info.showReciept = function(exp) {
        openModal("show_reciept", exp.IMG_HREF, "md")
    }
    info.deleteReciept = function(exp) {
        info.timesheet.start = 0;
        var data = {};
        data.id = exp.E_ID;
        data.do = 'timetracker-timesheet_expense-time-delete_image';
        data.start_date = info.obj.start;
        data.user_id = info.obj.user_id;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            info.renderPage(r);
            info.showAlerts(r)
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'show_note':
                var iTemplate = 'TimesheetNoteProject';
                var ctas = 'note';
                break;
            case 'submit_time':
                var iTemplate = 'SubmitTimeProject';
                var ctas = 'time';
                break;
            case 'select_task':
                var iTemplate = 'SelectTaskProject';
                var ctas = 'tsk';
                break;
            case 'show_reciept':
                var iTemplate = 'ShowRecieptProject';
                var ctas = 'rec';
                break;
            case 'approve_partial':
                var iTemplate = 'ApprovePartial';
                var ctas = 'time';
                break;
            case 'reject_time':
                var iTemplate = 'RejectTime';
                var ctas = 'note';
                break
        }
        var params = {
            template: 'tTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'show_note':
                params.params = {
                    'do': 'timetracker-xcomment',
                    'task_time_id': item.TASK_DAY_ID,
                    'unlocked_manually': item.unlocked_manually
                };
                params.callback = function() {
                    info.timesheet.start = 0;
                    //angular.element('.loading_alt').removeClass('hidden');
                    helper.doRequest('get', 'index.php', {
                        'do': 'timetracker-timesheet',
                        'xget': 'Week',
                        'start': info.obj.start,
                        'user_id': info.obj.user_id
                    }, function(r) {
                        //angular.element('.loading_alt').addClass('hidden');
                        info.renderPage(r)
                    })
                }
                break;
            case 'submit_time':
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        if (d.success) {
                            info.obj.approve_text = helper.getLanguage('Approve timesheet');
                            info.obj.disabled1 = !1;
                            info.obj.btn_type1 = 'primary';
                            info.obj.reject_text = helper.getLanguage('Reject timesheet');
                            info.obj.btn_type2 = 'secondary';
                            info.obj.disabled2 = !1;
                            info.obj.action2 = 'reject'
                        }
                    }
                }
                break;
            case 'select_task':
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        info.timesheet.start = 0;
                        info.renderPage(d);
                        info.showAlerts(d)
                    }
                }
                break;
            case 'show_reciept':
                params.item = item;
                break;
            case 'approve_partial':
                params.params = {
                    'do': 'timetracker-approve_timesheet',
                    'user_id': item.user_id,
                    'start': item.start,
                    'partial_sub': item.partial_sub
                };
                params.callback = function(d) {}
                break;
            case 'reject_time':
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        if (d.success) {
                            info.obj.reject_text = helper.getLanguage('Rejected timesheet');
                            info.obj.disabled2 = !0;
                            info.obj.btn_type1 = 'plain';
                            info.obj.approve_text = helper.getLanguage('Re-submit');
                            info.obj.action1 = 'resubmit'
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }
}]).controller('ApprovePartialModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var time = this;
    time.obj = data;
    time.showAlerts = function(d) {
        helper.showAlerts(time, d)
    }
    time.cancel = function() {
        $uibModalInstance.close()
    }
    time.ok = function() {
        var data = angular.copy(time.obj);
        data.do = 'timetracker--time-approve';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            time.showAlerts(r)
        })
    }
}]).controller('RejectTimeModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var note = this;
    note.obj = data;
    note.showAlerts = function(d) {
        helper.showAlerts(note, d)
    }
    note.cancel = function() {
        $uibModalInstance.close()
    }
    note.ok = function() {
        var obj = angular.copy(note.obj);
        for (x in note.obj.ARCHIVE_LINK) {
            obj[x] = note.obj.ARCHIVE_LINK[x]
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            $uibModalInstance.close(r)
        })
    }
}]).controller('SubmitTimeProjectModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var time = this;
    time.obj = data;
    time.obj.partial_full_radio = '1';
    time.showAlerts = function(d) {
        helper.showAlerts(time, d)
    }
    time.cancel = function() {
        $uibModalInstance.close()
    }
    time.showPartialTime = function() {
        time.showPartial = !0;
        time.obj.partial_end = time.obj.partial_submit[parseInt(time.obj.TODAY_nr) - 1].id.toString()
    }
    time.ok = function() {
        var data = angular.copy(time.obj);
        data.do = 'timetracker-timesheet-time-submit_hour';
        if (time.obj.partial_full_radio == '2') {
            data.week_e = time.obj.partial_end
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            time.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}])