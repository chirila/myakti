app.controller('servicesCtrl', ['$scope', '$rootScope','helper', '$compile', 'list', 'data', 'modalFc','ballFc',function($scope, $rootScope, helper, $compile, list, data, modalFc,ballFc) {
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    $rootScope.reset_lists.services=true;
    $scope.views = [{
        name: helper.getLanguage('Draft'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Planned'),
        id: 1,
        active: ''
    }, {
        name: helper.getLanguage('Finished'),
        id: 3,
        active: ''
    }, {
        name: helper.getLanguage('Accepted'),
        id: 4,
        active: ''
    }, {
        name: helper.getLanguage('All Statuses'),
        id: 5,
        active: ''
    }, {
        name: helper.getLanguage('Draft Or Planned'),
        id: 6,
        active: ''
    }];
    $scope.activeView = 6;
    $scope.list = [];
    $scope.nav = [];
    $scope.listStats = [];
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.search = {
        search: '',
        'do': 'maintenance-services',
        view: $scope.activeView,
        xget: '',
        offset: 1,
        showAdvanced: !1,
        'manager_id': undefined,
        archived: '',
        showDistance:false,
        color_ball:undefined
    };
    $scope.minimum_selected=false;
    $scope.all_pages_selected=false;
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.listStats = [];
            $scope.columns=d.data.columns;
            $scope.color_ball_dd=d.data.color_ball_dd;
            if(d.data.country_dd){
                $scope.country_dd=d.data.country_dd;
            }
            if(d.data.ord){
                $scope.ord=d.data.ord;
                $scope.search.order_by=d.data.ord;
            }
            if(d.data.matrix_first){
                $scope.search.matrix_first=d.data.matrix_first;
            }
            //$scope.initCheckAll();
            $scope.minimum_selected=d.data.minimum_selected;
            $scope.all_pages_selected=d.data.all_pages_selected;
            $scope.nav = d.data.nav;
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['service_id']);
                count ++;
            }
            //console.log(ids);
            sessionStorage.setItem('service.view_total', count);
            sessionStorage.setItem('service.view_list', JSON.stringify(ids));
        }
        $scope.managers = d.data.managers
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.archived = function(value) {
        $scope.search.archived = value;
        list.filter($scope, value, $scope.renderList, true)
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList, true)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList, true)
    };
    $scope.saveForPDF = function(i) {
        if(i){
            if(i.check_add_to_product){
              var elem_counter=0;
              for(x in $scope.list){
                if($scope.list[x].check_add_to_product){
                  elem_counter++;
                }
              }
              if(elem_counter==$scope.list.length && $scope.list.length>0){
                $scope.listObj.check_add_all=true;
              }
            }else{
              $scope.listObj.check_add_all=false;
            }
        }  
        list.saveForPDF($scope, 'maintenance--maintenance-saveAddToPdf', i)
    };
    $scope.searchThing = function(reset_list) {
        angular.element('.loading_wrap').removeClass('hidden');
        var params=angular.copy($scope.search);
        if(reset_list){
            params.reset_list='1';
        }
        list.search(params, $scope.renderList, $scope)
    }
    $scope.resetAdvanced = function() {
        $scope.search.manager_id = undefined;
        $scope.search.color_ball = undefined;
        $scope.search.showAdvanced = !1;
        $scope.searchThing(true)
    }
    $scope.searchAdvanced = function() {
        $scope.searchThing(true)
    }
    $scope.markCheckNew=function(i){
        list.markCheckNew($scope, 'maintenance--maintenance-saveAddToPdf', i);
    }
    $scope.checkAllPage=function(){
        $scope.listObj.check_add_all=true;
        $scope.markCheckNew();
    }
    $scope.checkAllPages=function(){
        list.markCheckNewAll($scope, 'maintenance--maintenance-saveAddToPdfAll','1');
    }
    $scope.uncheckAllPages=function(){
        list.markCheckNewAll($scope, 'maintenance--maintenance-saveAddToPdfAll','0');
    }
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Statuses'),
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.offset = 1;
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.countryCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        maxItems: 1
    };
    $scope.managerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Managers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.ballCfg = ballFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Color')
    });
    $scope.showStats = function(item, $index) {
        var add = !1;
        if (!$scope.listStats[$index]) {
            $scope.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (add == !0) {
            helper.doRequest('get', 'index.php?do=maintenance-service&xget=serviceUserStat&service_id=' + item.service_id, function(r) {
                if (r.data) {
                    $scope.listStats[$index] = r.data;
                    var expandColWidth=angular.element('.interventionTable thead tr th:eq(0)').outerWidth();
                    var firstColWidth=angular.element('.interventionTable thead tr th:eq(1)').outerWidth();
                    var totalRowWidth=angular.element('.interventionTable thead tr:eq(0)').outerWidth();
                    angular.element('.interventionTable tbody tr.collapseTr').eq($index).after('<tr class="versionInfo"><td colspan="'+($scope.columns.length+3)+'" class="no_border"><table class="table"><thead><tr><th class="expand_row" style="width:'+expandColWidth+'px"></th><th style="width:'+firstColWidth+'px">Name</th><th style="width:'+(totalRowWidth-expandColWidth-firstColWidth)+'px">Hours</th></tr></thead><tbody><tr ng-repeat="item2 in listStats[' + $index + '].list"><td class="expand_row" style="width:'+expandColWidth+'px"></td><td style="width:'+firstColWidth+'px">{{item2.name}}</td><td class="text-strong" style="width:'+(totalRowWidth-expandColWidth-firstColWidth)+'px">{{item2.hours}}</td></tr></tbody></table></td></tr>');
                    $compile(angular.element('.interventionTable tbody tr.collapseTr').eq($index).next())($scope)
                }
            })
        } else {
            angular.element('.interventionTable tbody tr.collapseTr').eq($index).next().toggleClass('hidden')
        }
    }

    $scope.createExports = function($event) {
        list.exportFnc($scope, $event)
        var params = angular.copy($scope.search);
        params.do = 'maintenance-services'
        params.exported = !0;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r);
        })
    };

    $scope.initCheckAll=function(){
        var total_checked=0;
        for(x in $scope.list){
            if($scope.list[x].check_add_to_product){
                total_checked++;
            }
        }
        if($scope.list.length > 0 && $scope.list.length == total_checked){
            $scope.listObj.check_add_all=true;
        }else{
            $scope.listObj.check_add_all=false;
        }
    }

    $scope.columnSettings = function() {
        openModal('column_set', {}, 'lg')
    }

    function openModal(type, item, size) {      
        var params = {
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'column_set':
                params.template = 'miscTemplate/ColumnSettingsModal';
                params.ctrl = 'ColumnSettingsModalCtrl';
                params.params = {
                    'do': 'misc-column_set',
                    'list': 'interventions'
                };
                params.ctrlAs = 'vm';
                params.callback = function(d) {
                    if (d) {
                        $scope.searchThing();
                    }
                }
                break;
        }       
        modalFc.open(params);
    }

    list.init($scope, 'maintenance-services', $scope.renderList,($rootScope.reset_lists.services ? true : false));
    $rootScope.reset_lists.services=false;
}]).controller('serviceCtrl', ['$scope', function($scope) {
    $scope.isAddPage = !1
}]).controller('serviceEditCtrl', ['$scope', '$state', '$compile', 'helper', '$stateParams', '$cacheFactory', '$timeout', '$uibModal', '$confirm', '$window', 'editItem', 'modalFc', 'selectizeFc', 'Lightbox', 'data', 'ballFc',function($scope, $state, $compile, helper, $stateParams, $cacheFactory, $timeout, $uibModal, $confirm, $window, editItem, modalFc, selectizeFc, Lightbox, data,ballFc) {
    console.log('Edit Service');
    var edit = this,
        typePromise;
    edit.item = {};
    edit.app = 'maintenance';
    edit.ctrlpag = 'service';
    edit.tmpl = 'template';
    edit.item_id = $stateParams.template_id ? $stateParams.template_id : $stateParams.service_id;
    edit.service_id = $stateParams.template_id ? $stateParams.template_id : $stateParams.service_id;
    edit.duplicate_service_id = $stateParams.duplicate_service_id ? $stateParams.duplicate_service_id : '';
    edit.isAddPage = !0;
    edit.initial_start = !0;
    edit.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    edit.ballCfg = ballFc.configure({
        labelField: 'name',
        searchField: ['name'],
    });
    edit.frequencycfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            if (value) {
                helper.doRequest('post', 'index.php', {
                    'do': 'maintenance--maintenance-setFrequency',
                    'service_id': edit.service_id,
                    'rec_frequency': edit.item.rec_frequency
                })
            }
        }
    });
    edit.showTxt = {
        info: !0,
        address: !0,
        report: !0,
        plannedate: !0,
        fillSheet: !1,
        sendEmail: !1,
        sevice_settings: !1,
        addBtn: !0,
        address: !1,
        notes: !1,
        down_notice: !1,
        site: !1,
        drop: !1,
    };
    edit.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    edit.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Languages'),
        maxItems: 1
    };
    edit.dealAutoCfg = {
        valueField: 'deal_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Deal'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.name) + ' </span>' + '</div>' + '</div>' + '</div>';
                if (item.deal_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Add Deal') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined || value == '') {
                edit.item.old_deal_id = value
            } else if (value == '99999999999') {
                openModal('create_deal', {}, 'lg')
            } else {
                edit.item.old_deal_id = value
            }
        },
        onType(str) {
            var selectize = angular.element('#deal_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'deals',
                    search: str,
                    service_id: edit.item.service_id,
                    version_id: edit.item.version_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.deals = r.data;
                    if (edit.item.deals.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        maxItems: 1
    };
    edit.add_customer = !0;
    edit.oldObj = {};
    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {
        finishDate: !1,
        planeddate: !1,
        planeddate2: !1,
        startDate: !1,
        planning_date: !1,
        rec_start_date: !1,
        rec_end_date: !1
    }
    edit.contactAddObj = {};
    edit.auto = {
        options: {
            customer: [],
            contact: [],
            user: [],
            userSrv: [],
            expenseCat: [],
            recipients: [],
            addresses: [],
            contacts: [],
            cc: [],
            articles: [],
        },
        disable: {
            customer: !0,
            contact: !0
        }
    };
    edit.contactForEmailAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select recipient'),
        create: !0,
        createOnBlur:true,
        maxItems: 100
    });
    edit.add = {
        staff: !0,
        subtask: !0,
        supplie: !0,
        article: !0,
    }
    edit.service_sheet = {
        servicing_sheet_id: edit.service_id,
        service_date: new Date(edit.item.planeddate_js)
    };
    edit.service_expense = {
        service_id: edit.service_id,
        service_date: new Date()
    }
    edit.email = {};
    edit.yourModel = {
        menu: [
            ['bold', 'italic'],
            ['left-justify', 'center-justify', 'right-justify'],
            ['ordered-list', 'unordered-list', 'outdent', 'indent'],
            ['link', 'image', 'image-upload']
        ]
    };
    edit.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    edit.show_load = !0;
    edit.openModal = openModal;
    edit.cancelEdit = cancelEdit;
    editItem.init(edit);
    edit.renderPage = function(d) {
        if (!d.data === undefined) {
            d.data.articles = edit.item.articles;
            d.data.tasks = edit.item.tasks;
            d.data.users = edit.item.users;
            d.data.services = edit.item.services;
            d.data.supplies = edit.item.supplies;
        }
        if (d.data) {
            if (edit.item.articles) {
                d.data.articles = edit.item.articles;
            }
            if (edit.item.users) {
                d.data.users = edit.item.users;
            }
            if (edit.item.tasks) {
                d.data.tasks = edit.item.tasks;
            }
            if (edit.item.services) {
                d.data.services = edit.item.services;
            }
            if (edit.item.supplies) {
                d.data.supplies = edit.item.supplies;
            }
            edit.item = d.data;
            helper.updateTitle($state.current.name,edit.item.serial_number);
            if(edit.item_id != 'tmp'){
                edit.hoursFix();
            }          
            if (edit.item.finishDate_ts_js) {
                edit.item.finishDate_js = new Date(edit.item.finishDate_ts_js)
            }
            if (edit.item.planeddate_js) {
                edit.item.planeddate_js = new Date(edit.item.planeddate_js);
                edit.service_sheet.service_date = new Date(edit.item.planeddate_js)
            }
            if (edit.item.planning && edit.item.planning.planning_date) {
                edit.item.planning.planning_date = new Date(edit.item.planning.planning_date)
            }
            if (edit.item.rec_start_date) {
                edit.item.rec_start_date = new Date(edit.item.rec_start_date)
            }
            if (edit.item.rec_end_date) {
                edit.item.rec_end_date = new Date(edit.item.rec_end_date)
            }
            edit.auto.options.user = d.data.user_auto;
                          
            edit.auto.options.addresses = d.data.addresses;
            edit.auto.options.site_addresses = d.data.site_addresses;
            edit.auto.options.contacts = d.data.contacts;
            edit.auto.options.cc = d.data.cc;
            edit.auto.options.articles = d.data.articles_list.lines;

            edit.format = edit.item.date_pick_format;
            edit.auto.options.customer = [{
                id: edit.item.customer_id,
                value: edit.item.customer_name
            }];
            edit.contactAddObj = {
                language_dd: edit.item.language_dd2,
                language_id: edit.item.language_id,
                gender_dd: edit.item.gender_dd,
                gender_id: edit.item.gender_id,
                title_id: edit.item.title_id,
                title_dd: edit.item.title_dd,
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                add_contact: !0,
                buyer_id: edit.item.buyer_id,
                'do': 'maintenance-service-maintenance-tryAddC',
                contact_only: !0,
                item_id: edit.item_id,
                service_type: edit.item.service_type,
                billable: edit.item.billable,
            }
            if(edit.item_id != 'tmp'){
                edit.item.save_disabled = d.data.email.save_disabled;
                edit.auto.options.userSrv = d.data.autoSrvUsers;
                edit.auto.options.expenseCat = d.data.expenseCat;
                edit.auto.options.recipients = d.data.recipients;
                edit.item.email.recipients = d.data.email.recipients;
                edit.item.email.sendgrid_selected = d.data.sendgrid_selected;

                edit.service_sheet.sheet_user_id = d.data.loged_user ? d.data.loged_user : '';
                edit.service_expense.user_id = d.data.loged_user ? d.data.loged_user : '';

                edit.email = {
                    e_subject: edit.item.e_subject,
                    e_message: edit.item.e_message,
                    email_language: edit.item.email_language,
                    include_pdf: edit.item.include_pdf,
                    service_id: edit.service_id,
                    use_html: edit.item.use_html,
                    recipients: edit.item.email.recipients,
                    sendgrid_selected: edit.item.email.sendgrid_selected,
                    user_loged_email: edit.item.email.user_loged_email,
                    user_acc_email: edit.item.email.user_acc_email,
                    copy: edit.item.email.copy,
                    copy_acc: edit.item.email.copy_acc,
                };
            }          
            if (edit.item_id == 'tmp' && edit.item.is_recurring && !edit.item.contract_id) {
                edit.contactAddObj.is_recurring = edit.item.is_recurring;
                edit.contactAddObj.rec_start_date = edit.item.rec_start_date;
                edit.contactAddObj.rec_frequency = edit.item.rec_frequency;
                edit.contactAddObj.rec_end_date = edit.item.rec_end_date;
                edit.contactAddObj.rec_days = edit.item.rec_days;
                edit.contactAddObj.rec_number = edit.item.rec_number;
                edit.contactAddObj.recurring_no_end = edit.item.recurring_no_end
            }
            if (edit.item.status == 1 || edit.item.status == 2) {
                edit.showS3()
            }
            if (edit.item_id == 'tmp' && edit.initial_start == !0 && !$stateParams.template_id) {
                openModal("add_steps", edit, "lg")
            }
        }
        edit.showAlerts(d)
    }
    edit.myFunction = function() {
          if (edit.email.recipients.length) {
            edit.item.save_disabled = !1;
            edit.save_disabled = !1
        } else {
            edit.item.save_disabled = !0
             edit.save_disabled = !0
        }
    }
    edit.hoursFix = function() {
        for (x in edit.item.hours.hours) {
            edit.item.hours.hours[x].date_js = new Date(edit.item.hours.hours[x].tmsmp_js)
        }
    }
    edit.showEdit = function(o, p) {
        if (edit.showTxt[o] === !1) {
            var data = angular.copy(edit.item);
            data.startdate = convert_value(data.startdate);
            data.duration = convert_value(data.duration);
            data.do = 'maintenance-service-maintenance-' + p;
            helper.doRequest('post', 'index.php', data, edit.renderPage)
        }
        edit.showTxt[o] = !edit.showTxt[o]
    }
    edit.showEditFnc = function() {
        edit.showTxt.address = !1;
        edit.oldObj = {
            buyer_id: angular.copy(edit.item.buyer_id),
            contact_id: angular.copy(edit.item.contact_id),
            contact_name: angular.copy(edit.item.contact_name),
            BUYER_NAME: angular.copy(edit.item.buyer_name),
            delivery_address: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address)),
            delivery_address_id: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_id)),
            delivery_address_txt: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_txt)),
            view_delivery: (edit.item.sameAddress ? !1 : angular.copy(edit.item.view_delivery)),
            sameAddress: angular.copy(edit.item.sameAddress),
        }
    }
    edit.showCCSelectize = function() {
        edit.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }
    edit.removeDeliveryAddr = function() {
        edit.item.delivery_address = '';
        edit.item.delivery_address_id = ''
    }

    function cancelEdit() {
        if (Object.keys(edit.oldObj).length == 0) {
            return edit.removeCust(edit)
        }
        edit.showTxt.address = !0;
        for (x in edit.oldObj) {
            if (edit.item[x]) {
                if (x == 'delivery_address') {
                    edit.item[x] = edit.oldObj[x].trim()
                } else {
                    edit.item[x] = edit.oldObj[x]
                }
            }
        }
        edit.item.buyer_id = edit.oldObj.buyer_id
    }
    edit.showS3 = function() {
        edit.show_load = !0;
        var data = angular.copy(edit.item.s3_info);
        data.do = 'misc-s3_link';
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.item.dropbox = r.data;
            edit.show_load = !1
        })
    }
    edit.changeStatus = function() {
        if (arguments.length >= 3) {
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', {
                'do': 'maintenance-service-maintenance-changeServiceStatus',
                status: arguments[0],
                planeddate_js: arguments[1],
                startdate: convert_value(arguments[2]),
                duration: convert_value(arguments[3]),
                service_id: edit.service_id,
                planeddate_ts: edit.item.planeddate_ts,
            }, edit.renderChange)
        } else {
            if (arguments[0] == 0) {
                var obj = {
                    "service_id": edit.service_id,
                    'planeddate_ts': edit.item.planeddate_ts
                };
                $confirm({
                    ok: helper.getLanguage('Yes'),
                    cancel: helper.getLanguage('No'),
                    text: helper.getLanguage('Do you want to reset planned date, time and duration?')
                }).then(function() {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('post', 'index.php', {
                        'do': 'maintenance-service-maintenance-changeServiceStatus',
                        status: arguments[0],
                        service_id: edit.service_id,
                        planeddate_ts: edit.item.planeddate_ts,
                        remove_dates: !0
                    }, edit.renderChange)
                }, function() {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('post', 'index.php', {
                        'do': 'maintenance-service-maintenance-changeServiceStatus',
                        status: arguments[0],
                        service_id: edit.service_id,
                        planeddate_ts: edit.item.planeddate_ts
                    }, edit.renderChange)
                });
                return !1
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', {
                'do': 'maintenance-service-maintenance-changeServiceStatus',
                status: arguments[0],
                service_id: edit.service_id,
                planeddate_ts: edit.item.planeddate_ts
            }, edit.renderChange)
        }
    }
    edit.renderChange = function(d) {
        if (d.success.success) {
            edit.showTxt.plannedate = !0;
            edit.item = d.data
        }
        if (d.data.planeddate) {
            edit.item.planeddate = d.data.planeddate
        }
        if (d.data.planeddate_js) {
            d.data.planeddate_js = new Date(d.data.planeddate_js)
        }
        if (edit.item.planning && edit.item.planning.planning_date) {
            edit.item.planning.planning_date = new Date(edit.item.planning.planning_date)
        }
        if (edit.item.status == 1 || edit.item.status == 2) {
            edit.showS3()
        }
        edit.showAlerts(d)
    }
    edit.openDate = function(p) {
        edit.datePickOpen[p] = !0
    };
    edit.changeAuto = function(d) {
        edit.auto.disable[d] = !edit.auto.disable[d]
    }
    edit.showAdd = function(d) {
        if (edit.add[d] === !1) {
            switch (d) {
                case "staff":
                    if (edit.item.staff_id) {
                        var is_error_msg=false;
                        for(x in edit.item.users.users){
                            if(edit.item.staff_id==edit.item.users.users[x].user_id){
                                edit.item.staff_id = null;
                                edit.add.staff=true;
                                is_error_msg=true;
                                var error_msg={'error':{'error':helper.getLanguage("This value is already in use.")},'success':false,'notice':false};
                                edit.showAlerts(error_msg);
                                break;
                            }
                        }
                        if(is_error_msg){
                            return false;
                        }
                        var tmp_item={};
                        tmp_item.name           = edit.item.staff_name;
                        tmp_item.id           = 'tmp_'+Math.floor(Date.now() / 1000);
                        tmp_item.user_id          = edit.item.staff_id;
                        tmp_item.pr_m           = false ;
                        tmp_item.hours          = '00:00';
                        tmp_item.expences       = 0;
                        tmp_item.hours_data = [];
                        tmp_item.expences_data      = [];   
                        edit.item.staff_id = null;
                        edit.item.users.users.push(tmp_item);
                        edit.item.users.is_data+=1;
                        /*if (edit.item.status == '0') {
                            var s_date = convert_value(edit.item.startdate);
                            var duration = convert_value(edit.item.duration);
                            var data = {
                                service_id: edit.service_id,
                                'do': 'maintenance-service-maintenance-addServiceUser',
                                user_id: edit.item.staff_id,
                                name: edit.item.staff_name,
                                free: !0,
                                planeddate_js: edit.item.planeddate_js,
                                planning_date: edit.item.planning.planning_date.toUTCString(),
                                startdate: s_date,
                                duration: duration
                            }
                        } else {
                            var data = {
                                service_id: edit.service_id,
                                'do': 'maintenance-service-maintenance-addServiceUser',
                                user_id: edit.item.staff_id,
                                name: edit.item.staff_name
                            }
                        }*/
                    }
                    //edit.item.staff_name = '';
                    //var func = edit.renderUserPage;
                    break;
                case "subtask":
                    if (edit.item.task_name) {
                        var tmp_item={};
                        tmp_item.name               = edit.item.task_name;
                        tmp_item.id                 = 'tmp_'+Math.floor(Date.now() / 1000);
                        tmp_item.checked            = false;
                        tmp_item['no-plus']         = '';
                        tmp_item.comment            = '';
                        tmp_item.readonly       = '';
                        tmp_item.task_budget    = helper.displayNr(0);
                        tmp_item.task_quantity  = helper.displayNr(0);
                        tmp_item.article_id = 0;
                        edit.item.tasks.tasks.push(tmp_item);
                        edit.item.tasks.is_data+=1;
                        edit.item.task_name='';
                        edit.item.add_subtask1=false;
                        edit.item.add_subtask2=false;
                        edit.add.subtask=false;
                        /*var data = {
                            service_id: edit.service_id,
                            'do': 'maintenance-service-maintenance-serviceAddTask',
                            task_name: edit.item.task_name,
                            xget: 'serviceSubTasks'
                        }*/
                    }
                    var func = edit.renderSubTaskPage;
                    break;
                case "supplie":
                    if (edit.item.supplie) {
                        var tmp_item={};                            
                        tmp_item.article_id     = '0';
                        tmp_item.name           = edit.item.supplie;
                        tmp_item.id         = 'tmp_'+Math.floor(Date.now() / 1000);
                        tmp_item.quantity       = helper.displayNr(1);
                        tmp_item.to_deliver = helper.displayNr(0);
                        tmp_item.delivered      = helper.displayNr(0);
                        tmp_item.price          = helper.displayNr(0);
                        tmp_item.margin     = helper.displayNr(0);
                        tmp_item.sell_price = helper.displayNr(0);
                        tmp_item.readonly       = '';
                        tmp_item.billed     = false;
                        tmp_item.billable       = true;
                        edit.item.supplies.supplies.push(tmp_item);
                        edit.item.supplies.is_data+=1;
                        edit.item.supplie='';
                        /*var data = {
                            service_id: edit.service_id,
                            'do': 'maintenance-service-maintenance-serviceAddSupply',
                            name: edit.item.supplie,
                            xget: 'serviceSupplies'
                        }*/
                    }
                    var func = edit.renderSuppliesPage;
                    break;
                default:
                    break
            }
            /*if (data) {
                helper.doRequest('post', 'index.php', data, func)
            }*/
        }
        if(d != 'subtask'){
            edit.add[d] = !edit.add[d]
        }
    }
    edit.renderUserPage = function(d) {
        if (d.data) {
            edit.renderPage(d)
        }
        edit.item.staff_id = null;
        edit.add.staff = !0;
        edit.showAlerts(d)
    }
    edit.renderSubTaskPage = function(d) {
        if (d.data) {
            edit.item.tasks = d.data;
            edit.item.task_name = '';
            edit.item.add_subtask1 = !1;
            edit.item.add_subtask2 = !1;
            edit.add.subtask = !0
        }
        edit.showAlerts(d)
    }
    edit.renderSubServicePage = function(d) {
        if (d.data) {
            edit.item.services = d.data;
            edit.item.add_subtask1 = !1;
            edit.item.add_subtask2 = !1
        }
        edit.showAlerts(d)
    }
    edit.renderSuppliesPage = function(d) {
        if (d.data) {
            edit.showAlerts(d);
            if (d.success) {
                edit.item.supplies = d.data;
                helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-service',
                    'xget': 'downpayment',
                    'service_id': edit.item.service_id
                }, function(r) {
                    edit.item.show_down = r.data.show_down
                })
            }
        }
    }
    edit.renderArticlesPage = function(d) {
        if (d.data) {
            edit.showAlerts(d);
            if (d.success) {
                edit.item.articles = d.data;
                helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-service',
                    'xget': 'downpayment',
                    'service_id': edit.item.service_id
                }, function(r) {
                    edit.item.show_down = r.data.show_down
                })
            }
        }
    }
    edit.editUser = function(d, type, $event) {
        switch (type) {
            case 'user':
                if(!isNaN(d.id)){
                    var data = {
                        service_id: edit.service_id,
                        'do': 'maintenance--maintenance-serviceSetPrM',
                        user_id: d.id,
                        value: d.pr_m
                    };
                    if (edit.item.status == 2) {
                        $event.preventDefault();
                        return !1
                    }
                }            
                break;
            case 'task':
                if(!isNaN(d.id)){
                    var data = {
                        service_id: edit.service_id,
                        'do': 'maintenance--maintenance-closeTask',
                        id: d.id,
                        value: d.checked
                    };
                    if (edit.item.status == 2) {
                        $event.preventDefault();
                        return !1
                    }
                }             
                break;
            case 'shropdf':
                var data = {
                    service_id: edit.service_id,
                    'do': 'maintenance--maintenance-shropdf',
                    value: d
                };
                break;
            case 'quantity_art':
                if(!isNaN(d.id)){
                    var data = {
                        service_id: edit.service_id,
                        'do': 'maintenance-service-maintenance-update_article_qty',
                        qty: d.quantity,
                        a_id: d.id,
                        xget: 'serviceArticles'
                    };
                    if (edit.item.status == 2) {
                        $event.preventDefault();
                        return !1
                    }
                }else{
                    for(var x in edit.item.articles.articles){
                        if(edit.item.articles.articles[x].is_combined){
                            let component_id=d.id;
                            for (y in edit.item.articles.articles) {
                                if (edit.item.articles.articles[y].component_for == component_id) {
                                    edit.item.articles.articles[y].quantity = helper.displayNr(parseFloat(helper.return_value(edit.item.articles.articles[x].quantity), 10) * parseFloat(helper.return_value(edit.item.articles.articles[y].quantity_component), 10));
                                }
                            }
                        }
                    }
                }              
                break;
            case 'name_art':
                if(!isNaN(d.id)){
                    var data = {
                        service_id: edit.service_id,
                        'do': 'maintenance-service-maintenance-update_article_name',
                        name: d.name,
                        a_id: d.id,
                        xget: 'serviceArticles'
                    };
                    if (edit.item.status == 2) {
                        $event.preventDefault();
                        return !1
                    }
                }           
                break;
            case 'task_art':
                if(!isNaN(d.id)){
                    var data = {
                        service_id: edit.service_id,
                        'do': 'maintenance-service-maintenance-update_task_name',
                        name: d.name,
                        task_id: d.id,
                        xget: 'serviceArticles'
                    };
                    if (edit.item.status == 2) {
                        $event.preventDefault();
                        return !1
                    }
                }            
                break;
            case 'quantity_serv':
                if(!isNaN(d.id)){
                    var data = {
                        service_id: edit.service_id,
                        'do': 'maintenance-service-maintenance-update_service_qty',
                        qty: d.quantity,
                        task_id: d.id,
                        xget: 'serviceSubServices'
                    };
                    if (edit.item.status == 2) {
                        $event.preventDefault();
                        return !1
                    }
                }             
                break;
            case 'quantity':
            case 'delivered':
                if(!isNaN(d.id)){
                    var data = {
                        service_id: edit.service_id,
                        'do': 'maintenance--maintenance-serviceArticleUpdate',
                        value: d[type],
                        field: type,
                        article_id: d.id
                    };
                    if (edit.item.status == 2) {
                        $event.preventDefault();
                        return !1
                    }
                }           
                break;
            case 'sell_price':
                if(!isNaN(d.id)){
                    var data = {
                        service_id: edit.service_id,
                        'do': 'maintenance--maintenance-serviceArtSellUpdate',
                        value: d[type],
                        field: type,
                        article_id: d.id
                    };
                    if (edit.item.status == 2) {
                        $event.preventDefault();
                        return !1
                    }
                }              
                break;
            default:
                break
        }
        if (data) {
            helper.doRequest('post', 'index.php', data, function(r) {
                edit.showAlerts(r);
                if (r.in.minus_stock) {
                    var msg = "<b>" + helper.getLanguage("Selected quantity exceeds article stock of") + " <span class='text-danger'>" + r.in.stock + "</span></b>";
                    var cont = {
                        "e_message": msg
                    };
                    openModal("stock_info", cont, "md")
                }
                if (r.data.one_manager) {
                    for (x in edit.item.users.users) {
                        if (edit.item.users.users[x].id == r.data.user_id) {
                            edit.item.users.users[x].pr_m = !0
                        }
                    }
                }
                if (r.in.qty_done) {
                    edit.item.articles = r.data
                }
                if (r.in.qty) {
                    d[type] = r.in.qty
                }
            })
        }
    }
    edit.showAlerts = function(d) {
        helper.showAlerts(edit, d)
    }
    edit.delete = function(type, d) {
        switch (type) {
            case 'user':
                if(!isNaN(d.id)){
                    if (edit.item.status == '0') {
                        var s_date = convert_value(edit.item.startdate);
                        var duration = convert_value(edit.item.duration);
                        var data = {
                            service_id: edit.service_id,
                            'do': 'maintenance-service-maintenance-deleteServiceUser',
                            user_id: d.id,
                            u_id: d.user_id,
                            free: !0,
                            planeddate_js: edit.item.planeddate_js,
                            planning_date: edit.item.planning.planning_date.toUTCString(),
                            startdate: s_date,
                            duration: duration
                        }
                    } else {
                        var data = {
                            service_id: edit.service_id,
                            'do': 'maintenance-service-maintenance-deleteServiceUser',
                            user_id: d.id,
                            u_id: d.user_id
                        }
                    }
                    var func = edit.renderUserPage;
                }else{
                    for(var x in edit.item.users.users){
                        if(edit.item.users.users[x].id == d.id){
                            edit.item.users.users.splice(x,1);
                            break;
                        }
                    }
                }            
                break;
            case 'task':
                if(!isNaN(d.id)){
                    var data = {
                        service_id: edit.service_id,
                        'do': 'maintenance-service-maintenance-deleteServiceTask',
                        task_id: d.id,
                        xget: 'serviceSubTasks'
                    };
                    var func = edit.renderSubTaskPage;
                }else{
                    for(var x in edit.item.tasks.tasks){
                        if(edit.item.tasks.tasks[x].id == d.id){
                            edit.item.tasks.tasks.splice(x,1);
                            break;
                        }
                    }
                }             
                break;
            case 'service':
                if(!isNaN(d.id)){
                    var data = {
                        service_id: edit.service_id,
                        'do': 'maintenance-service-maintenance-deleteServiceTask',
                        task_id: d.id,
                        xget: 'serviceSubServices'
                    };
                    var func = edit.renderSubServicePage;
                }else{
                    for(var x in edit.item.services.services){
                        if(edit.item.services.services[x].id == d.id){
                            edit.item.services.services.splice(x,1);
                            break;
                        }
                    }
                }                
                break;
            case 'supplie':
                if(!isNaN(d.id)){
                    var data = {
                        service_id: edit.service_id,
                        'do': 'maintenance-service-maintenance-deleteServiceSupply',
                        a_id: d.id,
                        xget: 'serviceSupplies'
                    };
                    var func = edit.renderSuppliesPage;
                }else{
                    for(var x in edit.item.supplies.supplies){
                        if(edit.item.supplies.supplies[x].id == d.id){
                            edit.item.supplies.supplies.splice(x,1);
                            break;
                        }
                    }
                }          
                break;
            case 'article':
                if(!isNaN(d.id)){
                    var data = {
                        service_id: edit.service_id,
                        'do': 'maintenance-service-maintenance-deleteServiceArticle',
                        a_id: d.id,
                        article_id: d.article_id,
                        xget: 'serviceArticles'
                    };
                    var func = edit.renderArticlesPage;
                }else{
                    for(var x in edit.item.articles.articles){
                        if(edit.item.articles.articles[x].id == d.id){
                            let component_id=d.id;
                            edit.item.articles.articles.splice(x,1);
                            for (y in edit.item.articles.articles) {
                                if (edit.item.articles.articles[y].component_for == component_id) {
                                   edit.item.articles.articles.splice(y,1);
                                }
                            }
                            break;
                        }
                    }
                }            
            default:
                break
        }
        if (data) {
            helper.doRequest('post', 'index.php', data, func)
        }
    }
    edit.changeDraftStatus = function() {
        edit.showTxt.plannedate = !edit.showTxt.plannedate
    }
    edit.toggleStuff = function(d, e) {
        edit.showTxt[d] = !edit.showTxt[d];
        edit.showTxt[e] = null
    }
    edit.correctVal = function() {
        if (arguments[0].start_time) {
            arguments[0].start_time = corect_val(arguments[0].start_time)
        }
        if (arguments[0].end_time) {
            arguments[0].end_time = corect_val(arguments[0].end_time)
        }
        if (arguments[0].break) {
            arguments[0].break = corect_val(arguments[0].break)
        }
    }
    edit.addSheet = function(d) {
        if (edit.validateAddSheet(d)) {
            edit.showTxt.fillSheet = !1
            d.do = 'maintenance-service-maintenance-add_sheet';
            d.xget = 'serviceUser';
            d.start_time = convert_value(d.start_time);
            d.end_time = convert_value(d.end_time);
            d.break = convert_value(d.break);
            helper.doRequest('post', 'index.php', d, function(r) {
                edit.item.users = r.data;
                edit.service_sheet = {
                    servicing_sheet_id: edit.service_id,
                    service_date: new Date(edit.item.planeddate_js),
                    sheet_user_id: edit.item.loged_user ? edit.item.loged_user : ''
                };
                edit.showAlerts(r)
            })
        }
    }
    edit.validateAddSheet = function(p) {
        var err_valid = !1;
        var msg = '';
        if (!p.servicing_sheet_id) {
            msg = helper.getLanguage("Please select Sheet");
            err_valid = !0
        } else if (!p.start_time) {
            msg = helper.getLanguage("Please select Start time");
            err_valid = !0
        } else if (!p.end_time) {
            msg = helper.getLanguage("Please select a End time");
            err_valid = !0
        } else if (parseFloat(convert_value(p.start_time)) >= parseFloat(convert_value(p.end_time))) {
            msg = helper.getLanguage("End time should be bigger then Start time");
            err_valid = !0
        } else if (parseFloat(convert_value(p['break'])) >= (parseFloat(convert_value(p.end_time)) - parseFloat(convert_value(p.start_time)))) {
            msg = helper.getLanguage("Break is bigger then the hours");
            err_valid = !0
        } else if (!p.service_date) {
            msg = helper.getLanguage("Please select a day");
            err_valid = !0
        }
        if (err_valid) {
            var obj = {
                "error": {
                    "error": helper.getLanguage(msg)
                },
                "notice": !1,
                "success": !1
            };
            edit.showAlerts(obj);
            return !1
        }
        return !0
    }
    edit.deleteSheet = function(r, index) {
        var d = {
            'do': 'maintenance-service-maintenance-deleteServiceSheet',
            'servicing_sheet_id': r.service_id,
            'serv_sheet_id': r.id,
            'service_id': edit.service_id,
            'xget': 'serviceUser'
        }
        helper.doRequest('post', 'index.php', d, function(r) {
            edit.item.users = r.data;
            edit.showAlerts(r);
            $timeout(function() {
                edit.listMore(index)
            })
        })
    }
    edit.showEditHour = function(r, index) {
        var obj = angular.copy(r);
        obj.index = index;
        obj.service_type = edit.item.service_type;
        openModal("service_hours", obj, "md")
    }
    edit.editSheet = function(r) {
        var d = angular.copy(r);
        d.do = 'maintenance-service-maintenance-editServiceSheet';
        d.start_time = convert_value(d.start_time);
        d.end_time = convert_value(d.end_time);
        d.break = convert_value(d.break);
        d.xget = 'serviceHours';
        edit.showTxt.editSheett = !1;
        helper.doRequest('post', 'index.php', d, function(r) {
            edit.item.hours = r.data;
            edit.hoursFix();
            edit.showAlerts(r)
        })
    }
    edit.addExpense = function(d) {
        edit.showTxt.fillTime = !1
        d.do = 'maintenance-service-maintenance-add_exp';
        d.xget = 'serviceUser';
        helper.doRequest('post', 'index.php', d, function(r) {
            edit.item.users = r.data;
            edit.showAlerts(r)
        })
    }
    edit.deleteExpense = function(r, index) {
        var d = {
            'do': 'maintenance-service-time-delete_expense',
            'id': r.E_ID,
            'service_id': edit.service_id,
            'xget': 'serviceUser'
        }
        helper.doRequest('post', 'index.php', d, function(r) {
            edit.item.users = r.data;
            edit.showAlerts(r);
            $timeout(function() {
                edit.listMore(index)
            })
        })
    }
    edit.showEditExp = function(r, index) {
        var obj = angular.copy(r);
        obj.index = index;
        openModal("service_expences", obj, "md")
    }
    edit.editExpense = function(r) {
        var d = angular.copy(r);
        var formdata = new FormData(),
            x = $('#attachement');
        d.do = 'maintenance-service-maintenance-update_expense';
        d.xget = 'serviceExpense';
        d.service_id = edit.service_id;
        d.amount = d.amount_txt;
        d.expense_id = d.EXP_ID;
        d.id = d.E_ID;
        d.date = d.DATEE;
        for (y in d) {
            formdata.append(y, d[y])
        }
        if (x[0].files.length > 0) {
            formdata.append("Filedata", x[0].files[0])
        }
        edit.showTxt.editExpensee = !1;
        helper.doRequest('post', 'index.php', formdata, function(r) {
            edit.item.expense = r.data;
            edit.showAlerts(r)
        })
    }
    edit.viewRecepeit = function(exp) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'miscTemplate/modal',
            controller: 'viewRecepitCtrl as vmMod',
            resolve: {
                data: function() {
                    var obj = {
                        e_message: '<center><img src="' + exp.IMG_HREF + '" class="img-responsive" ></center>'
                    }
                    return obj
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            edit.selected = selectedItem
        }, function() {
            console.log('modal closed')
        })
    };
    edit.deleteRecepeit = function(r, index) {
        var d = angular.copy(r);
        d.do = 'maintenance-service-time-delete_image';
        d.id = d.E_ID;
        d.service_id = edit.service_id;
        d.xget = 'serviceUser';
        helper.doRequest('post', 'index.php', d, function(r) {
            edit.item.users = r.data;
            edit.showAlerts(r);
            $timeout(function() {
                edit.listMore(index)
            })
        })
    }
    edit.editSupply = function(r) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'template/recepeitModal',
            controller: 'supplyCtrl',
            resolve: {
                obj: function() {
                    r.service_id = edit.service_id;
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            edit.selected = selectedItem
        }, function() {
            console.log('modal closed')
        })
    };
    edit.taskModal = function(r) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'template/taskModal',
            controller: 'taskModalCtrl',
            resolve: {
                obj: function() {
                    r.service_id = edit.service_id;
                    r.service_status = edit.item.status;
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            edit.selected = selectedItem
        }, function() {
            console.log('modal closed')
        })
    };
    edit.sendEmails = function(r) {
        var d = angular.copy(r);
        edit.showTxt.sendEmail = !1;
        if (d.recipients.length) {
            if (edit.item.dropbox) {
                d.dropbox_files = edit.item.dropbox.dropbox_files
            }
            if (Array.isArray(d.recipients)) {
                d.email = d.recipients[0]
            } else {
                d.email = d.recipients
            }
            edit.item.email.alerts.push({
                type: 'email',
                msg: d.email + ' - '
            });
            d.do = 'maintenance--maintenance-sendNewEmail';
            d.use_html = edit.item.use_html;
            d.mark_as_finished = 2;
            if (edit.item.status) {
                d.mark_as_finished = ''
            }
            helper.doRequest('post', 'index.php', d, function(r) {
                //if (Array.isArray(d.recipients)) {
                    if (r.success && r.success.success) {
                        edit.item.email.alerts[edit.item.email.alerts.length - 1].type = 'success';
                        edit.item.email.alerts[edit.item.email.alerts.length - 1].msg += ' ' + r.success.success
                    }
                    if (r.notice && r.notice.notice) {
                        edit.item.email.alerts[edit.item.email.alerts.length - 1].type = 'info';
                        edit.item.email.alerts[edit.item.email.alerts.length - 1].msg += ' ' + r.notice.notice
                    }
                    if (r.error && r.error.error) {
                        edit.item.email.alerts[edit.item.email.alerts.length - 1].type = 'danger';
                        edit.item.email.alerts[edit.item.email.alerts.length - 1].msg += ' ' + r.error.error
                    }
                    if( d.email == d.user_loged_email){
                            d.copy = !1;
                        }
                        if(d.email == d.user_acc_email){
                            d.copy_acc = !1;
                        }
                    if(Array.isArray(d.recipients)){
                        d.recipients.shift();
                        
                        edit.sendEmails(d)
                    }else{
                        d.recipients =[];
                        edit.sendEmails(d)
                    }               
                //}
                 
                    
            })
        }else {
            if (d.user_loged_email && d.copy) {
                d.email = d.user_loged_email;
                d.copy = !1;
                d.mark_as_sent = 0;
                /*if (view.invoice.dropbox) {
                    d.dropbox_files = view.invoice.dropbox.dropbox_files
                }*/
                if (d.email) {
                    edit.item.email.alerts.push({
                        type: 'email',
                        msg: d.email + ' - '
                    });
                    d.do = 'maintenance-service-maintenance-sendNewEmail';
                    d.use_html = edit.item.use_html;
                    helper.doRequest('post', 'index.php', d, function(r) {
                        if (r.success && r.success.success) {
                            edit.item.email.alerts[edit.item.email.alerts.length - 1].type = 'success';
                            edit.item.email.alerts[edit.item.email.alerts.length - 1].msg += ' ' + r.success.success
                        }
                        if (r.notice && r.notice.notice) {
                            edit.item.email.alerts[edit.item.email.alerts.length - 1].type = 'info';
                            edit.item.email.alerts[edit.item.email.alerts.length - 1].msg += ' ' + r.notice.notice
                        }
                        if (r.error && r.error.error) {
                            edit.item.email.alerts[edit.item.email.alerts.length - 1].type = 'danger';
                            edit.item.email.alerts[edit.item.email.alerts.length - 1].msg += ' ' + r.error.error
                        }
                        //d.user_loged_email = null;
                        d.user_mail_sent =1;
                        edit.sendEmails(d);
                        /*if (r.success) {
                            view.renderPage(r, !0)
                        }*/
                    })
                }
            } else if (d.user_acc_email && d.copy_acc ) {
                //console.log(d.user_mail_sent, d.user_acc_email, d.user_loged_email);
                if(d.user_mail_sent && d.user_acc_email == d.user_loged_email){
                  
                }else{
                    d.email = d.user_acc_email;
                    d.copy_acc = !1;
                    d.mark_as_sent = 0;
                    /*if (view.invoice.dropbox) {
                        d.dropbox_files = view.invoice.dropbox.dropbox_files
                    }*/
                    if (d.email) {
                        edit.item.email.alerts.push({
                            type: 'email',
                            msg: d.email + ' - '
                        });
                        d.do = 'maintenance-service-maintenance-sendNewEmail';
                        d.use_html = edit.item.use_html;
                       
                        helper.doRequest('post', 'index.php', d, function(r) {
                            if (r.success && r.success.success) {
                                edit.item.email.alerts[edit.item.email.alerts.length - 1].type = 'success';
                                edit.item.email.alerts[edit.item.email.alerts.length - 1].msg += ' ' + r.success.success
                            }
                            if (r.notice && r.notice.notice) {
                                edit.item.email.alerts[edit.item.email.alerts.length - 1].type = 'info';
                                edit.item.email.alerts[edit.item.email.alerts.length - 1].msg += ' ' + r.notice.notice
                            }
                            if (r.error && r.error.error) {
                                edit.item.email.alerts[edit.item.email.alerts.length - 1].type = 'danger';
                                edit.item.email.alerts[edit.item.email.alerts.length - 1].msg += ' ' + r.error.error
                            }
                        })
                    }
                }

            }else{
                helper.refreshLogging();
            }
          
        }
         
    }
    edit.showPreview = function(exp) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'miscTemplate/modal',
            controller: 'viewRecepitCtrl as vmMod',
            resolve: {
                data: function() {
                    var d = {
                        'do': 'maintenance-email_preview',
                        service_id: edit.service_id,
                        message: exp.e_message,
                        use_html: exp.use_html
                    };
                    return helper.doRequest('post', 'index.php', d).then(function(d) {
                        return d.data
                    })
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            edit.selected = selectedItem
        }, function() {
            console.log('modal closed')
        })
    };
    edit.showPDFsettings = function() {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'template/pdf_settings',
            controller: 'pdfSettingsCtrl',
            resolve: {
                data: function() {
                    var d = {
                        'do': 'maintenance-pdf_settings',
                        service_id: edit.service_id
                    };
                    return helper.doRequest('get', 'index.php', d).then(function(d) {
                        return d.data
                    })
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            edit.selected = selectedItem
        }, function() {
            console.log('modal closed')
        })
    };
    edit.renderCustomerAuto = function(d) {
        edit.auto.options.customer = [];
        if (d.data) {
            edit.auto.options.cc = d.data;
            if (edit.item.customer_id) {
                edit.auto.options.cc.push({
                    id: edit.item.customer_id,
                    value: edit.item.customer_name
                })
            }
        }
    }
    edit.renderContactForEmailAuto = function(d) {
        edit.auto.options.recipients = [];
        if (d.data) {
            edit.auto.options.recipients = d.data
        }
    }
    edit.renderUserAuto = function(d) {
        edit.auto.options.user = [];
        if (d.data) {
            edit.auto.options.user = d.data
        }
    }
    edit.addUser = function() {
        edit.add.staff = !1;
        $timeout(function() {
            var selectize = angular.element('#users_list')[0].selectize;
            selectize.open()
        })
    }
    edit.userAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select collaborator'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.value) + ' </span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            if (value != undefined) {
                edit.item.staff_name = this.options[value].value;
                edit.showAdd('staff')
            }
        },
        onBlur() {
            $timeout(function() {
                edit.add.staff = !0
            })
        },
        maxItems: 1
    };
    edit.expenseAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: 'Select category',
        create: !1,
        maxItems: 1
    };
    edit.addArticleBtn = function() {
        edit.add.article = !1;
        $timeout(function() {
            var selectize = angular.element('#articles_list')[0].selectize;
            selectize.open()
        })
    }
    edit.cancelAddArticle = function() {
        edit.add.article = !0;
        edit.item.article_id = 0;
        var data = {
            'do': 'maintenance-service',
            xget: 'articles_list',
            cat_id: edit.item.cat_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            edit.auto.options.articles = r.data.lines
        })
    }
    edit.addArticle = function(obj,customIndex) {
        var name = '';
        var p_price = 0;
        var s_price = 0;
        var a_line = {};
        if (obj == undefined) {
            for (x in edit.auto.options.articles) {
                if (edit.auto.options.articles[x].article_id == edit.item.article_id) {
                    a_line = edit.auto.options.articles[x];
                    break
                }
            }
        } else {
            a_line = obj
        }
        var tmp_item={};
        tmp_item.article_id     = a_line.article_id;
        tmp_item.name           = a_line.name;
        tmp_item.id             = 'tmp_'+Math.floor(Date.now() / 1000);
        tmp_item.quantity           = helper.displayNr(1);
        tmp_item.quantity_nr        = 1;
        tmp_item.ALLOW_STOCK        = helper.settings.ALLOW_STOCK==1 ? true : false;
        tmp_item.to_deliver     = helper.displayNr(1);
        tmp_item.delivered          = false;
        tmp_item.to_be_reserved     =  1;
        tmp_item.to_be_delivered        =  1;
        tmp_item.reserved_stock     = 0;
        tmp_item.price          = a_line.price;
        tmp_item.margin         = a_line.margin;
        tmp_item.purchase_price     = a_line.purchase_price;
        tmp_item.sell_price     = helper.displayNr(a_line.price);
        tmp_item.hstock         = a_line.hstock ? '1' : '0'; 
        tmp_item.code           = a_line.code;
        tmp_item.stock          = a_line.stock;
        tmp_item.billed         = false;
        tmp_item.billable           = true;
        tmp_item.stock_status       = a_line.stock_status;
        tmp_item.has_variants = a_line.has_variants;
        tmp_item.has_variants_done = a_line.has_variants_done;
        tmp_item.is_variant_for = a_line.is_variant_for;
        tmp_item.is_variant_for_line = a_line.is_variant_for_line;
        tmp_item.variant_type = a_line.variant_type; 
        tmp_item.is_combined = a_line.is_combined;
        tmp_item.component_for = a_line.component_for;
        if(customIndex){
            edit.item.articles.articles.splice(customIndex, 0, tmp_item);
        }else{
            edit.item.articles.articles.push(tmp_item);
        }      
        edit.item.articles.is_data+=1;         
        edit.add.article=true;
        edit.cancelAddArticle();  

        if(tmp_item.is_combined){
            var params = {
                article_id: a_line.article_id,
                quantity: 1,
                'do': 'maintenance-service',
                xget: 'components'
            };
            helper.doRequest('get', 'index.php', params, function(res) {
                if (res.data && res.data.components) {
                    var d = res.data.components
                    for (x in d) {

                        var componentLine = {
                            article_id: d[x].component_article_id,
                            name: d[x].component_name,
                            id:'tmp_'+Math.floor(Date.now() / 1000),
                            quantity: d[x].quantity,
                            quantity_nr: d[x].quantity_nr,
                            ALLOW_STOCK:helper.settings.ALLOW_STOCK==1 ? true : false,
                            to_deliver:d[x].quantity,
                            delivered:false,
                            to_be_reserved:d[x].quantity_nr,
                            to_be_delivered:d[x].quantity_nr,
                            reserved_stock:d[x].quantity_nr,
                            price: 0,
                            margin:d[x].margin,
                            purchase_price: d[x].purchase_price,
                            sell_price:0,
                            hstock:d[x].hstock ? '1' : '0',
                            code:d[x].component_code,
                            stock:d[x].stock,
                            billed:false,
                            billable:true,
                            stock_status:d[x].stock_status,
                            component_for:tmp_item.id,
                            quantity_component: d[x].quantity_component,
                        }
                        edit.item.articles.articles.push(componentLine);
                        edit.item.articles.is_data+=1;   
                    }
                }
            })
        }

        /*var data = {};
        data.service_id = edit.service_id;
        data.article_id = obj == undefined ? edit.item.article_id : a_line.article_id;
        data.purchase_price = a_line.purchase_price;
        data.sell_price = a_line.price;
        data.name = a_line.name;
        data.quantity = a_line.quantity ? a_line.quantity : '1';
        data.do = 'maintenance-service-maintenance-serviceAddArticle';
        data.xget = 'serviceArticles';
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.articles = r.data;
                edit.cancelAddArticle();
                var new_stock = parseFloat(a_line.stock - parseFloat(a_line.quantity));
                var hide_stock = a_line.hide_stock;
                if (edit.item.article_stock_warn) {
                    if ((new_stock < parseFloat(a_line.threshold_value)) && hide_stock != '1') {}
                }
                helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-service',
                    'xget': 'downpayment',
                    'service_id': edit.item.service_id
                }, function(o) {
                    edit.item.show_down = o.data.show_down
                })
            }
        })*/
    }
    edit.articleAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Article'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.name2) {
                    item.name2 = "/ " + item.name2
                }
                if (item.ALLOW_STOCK == !0 && item.hide_stock == 0) {
                    var step = (helper.displayNr(item.price)) + '<br><small class="text-muted">' + helper.getLanguage('Stock') + ' <strong>' + escape(item.stock2) + '</strong></small>'
                } else {
                    var step = ''
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '&nbsp; </small>' + '</div>' + '<div class="col-sm-4 text-right">' + step + '</div>' + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#articles_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'articles_list',
                    search: str,
                    cat_id: edit.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.auto.options.articles = r.data.lines;
                    if (edit.auto.options.articles) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value) {
            if (value == undefined) {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'articles_list',
                    cat_id: edit.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.auto.options.articles = r.data.lines
                })
            } else if (value == '99999999999') {
                edit.openModal(edit, 'AddAnArticle', {
                    'do': 'misc-addAnArticle',
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat
                }, 'md');
                return !1
            } else if (value != undefined) {
                edit.addArticle()
            }
        },
        onItemAdd(value, $item) {
            if (value == '99999999999') {
                edit.selected_article_id = ''
            }
        },
        onBlur() {
            edit.add.article = !0;
            if (!edit.auto.options.articles.length) {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'articles_list',
                    cat_id: edit.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.auto.options.articles = r.data.lines
                })
            }
        },
        maxItems: 1
    };
    edit.checkArtDelivered = function(item) {
        if (item.delivered) {
            if (item.hstock == '0' && edit.item.article_loc) {
                openModal("article_delivery", item, "lg")
            } else {
                var data = {};
                data.delivered = 1;
                data.a_id = item.id;
                data.xget = 'serviceArticles';
                data.do = 'maintenance-service-maintenance-update_article_delivered';
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('post', 'index.php', data, function(r) {
                    edit.showAlerts(r);
                    edit.item.articles = r.data
                })
            }
        } else {
            var data = {};
            data.delivered = 0;
            data.a_id = item.id;
            data.service_id = edit.service_id;
            data.xget = 'serviceArticles';
            data.do = 'maintenance-service-maintenance-undo_article_delivered';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                edit.showAlerts(r);
                if (r.error) {
                    item.delivered = !0
                } else {
                    edit.item.articles = r.data
                }
            })
        }
    }
    edit.createInvoice = function() {
        var cacheService = $cacheFactory.get('ServiceToInvoice');
        if (cacheService != undefined) {
            cacheService.destroy()
        }
        var data = angular.copy(edit.item.invoice_link);
        var cache = $cacheFactory('ServiceToInvoice');
        cache.put('data', data);
        $state.go('invoice.edit', {
            invoice_id: "tmp"
        })
    }
    edit.duplicate = function() {
        var data = {
            'do': 'maintenance--maintenance-duplicate',
            'd_service_id': edit.service_id
        };
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                edit.showAlerts(r)
            } else {
                $state.go('service.edit', {
                    service_id: r.data.service_id
                })
            }
        })
    }
    edit.saveData = function(formObj,saveOnly) {
        var data = angular.copy(edit.item);
        if ($stateParams.template_id) {
            data.template_id = $stateParams.template_id
        }
        if (data.startdate) {
            data.startdate = convert_value(data.startdate)
        }
        if (data.duration) {
            data.duration = convert_value(data.duration)
        }
        data = helper.clearData(data, ['addresses', 'articles_list', 'autoSrvUsers', 'cc', 'contacts', 'expense', 'expenseCat', 'hours', 'language_dd2', 'recipients', 'user_auto']);
        if(edit.item_id == 'tmp' || edit.item.duplicate_service_id){
            data.do = 'maintenance-service-maintenance-addService';
        }else{
            data.do = 'maintenance-service-maintenance-editService';
        }       
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                if (edit.item.duplicate_service_id) {
                    $state.go('service.view', {
                        'service_id': r.data.service_id
                    })
                }
                if(!edit.item.duplicate_service_id && !edit.item.is_recurring && !edit.item.template_id && !saveOnly){
                    $state.go('service.view', {
                        service_id: (edit.item_id == 'tmp' ? r.data.service_id : edit.item_id)
                    })
                }else{
                    if (edit.item_id == 'tmp' && r.data.is_recurring) {
                        $state.go('rec_service.edit', {
                            'service_id': r.data.service_id
                        })
                    }else if (edit.item_id == 'tmp' && $stateParams.template_id) {
                        $state.go('service.editModel', {
                            'template_id': r.data.service_id
                        })
                    }else{
                        edit.renderPage(r)
                    }                    
                }                
            } else {
                edit.showAlerts(r, formObj)
            }
        })
    }
    edit.saveNote = function(formObj) {
        var data = angular.copy(edit.item);
        if (data.startdate) {
            data.startdate = convert_value(data.startdate)
        }
        if (data.duration) {
            data.duration = convert_value(data.duration)
        }
        data = helper.clearData(data, ['addresses', 'articles', 'articles_list', 'autoSrvUsers', 'cc', 'contacts', 'expense', 'expenseCat', 'hours', 'language_dd2', 'recipients', 'supplies', 'tasks', 'user_auto', 'users']);
        data.do = 'maintenance-service-maintenance-saveNote';
        helper.doRequest('post', 'index.php', data, function(r) {})
    }
    edit.SubjectRate = function() {
        var obj = {
            'do': 'maintenance--maintenance-setSubjectRate',
            'service_id': edit.service_id,
            'subject': edit.item.subject,
            'rate': edit.item.rate
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            edit.showAlerts(r)
        })
    }
    edit.changeBillable = function() {
        if (!edit.item.edit || edit.item.is_team || edit.item.contract_id != '0') {
            return !1
        }
        if (edit.item.billable) {
            openModal("show_types", {
                'service_id': edit.service_id
            }, "md")
        } else {
            var obj = {
                'do': 'maintenance--maintenance-setServiceBillable',
                'service_id': edit.service_id
            };
            helper.doRequest('post', 'index.php', obj, function(r) {
                edit.showAlerts(r)
            })
        }
    }
    edit.changeArticleBillable = function(item) {
        if(!isNaN(item.id)){
            var data = {};
            data.a_id = item.id;
            data.billable = item.billable;
            data.do = 'maintenance--maintenance-updateArticleBillable';
            helper.doRequest('post', 'index.php', data, function(r) {
                edit.showAlerts(r);
                helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-service',
                    'xget': 'downpayment',
                    'service_id': edit.item.service_id
                }, function(o) {
                    edit.item.show_down = o.data.show_down
                })
            })
        }     
    }
    edit.listMore = function(index) {
        if (edit.item.users.users[index].hours_data.is_data || edit.item.users.users[index].expences_data.is_data) {
            var width_hour = (edit.item.edit && !edit.item.is_team) ? 3 : 5;
            var width_exp = (edit.item.edit && !edit.item.is_team) ? 2 : 4;
            var new_line = '<tr class="versionInfo"><td colspan="8" class="no_border">' + '<table class="table dispatchTable" ng-if="edit.item.users.users[' + index + '].hours_data.is_data">' + '<thead><tr><th>' + '<div class="row">' + '<div class="col-xs-2"><b>' + helper.getLanguage('Date') + '</b></div>' + '<div class="col-xs-' + width_hour + '"><b>' + helper.getLanguage('Report') + '</b></div>' + '<div class="col-xs-1"><b>' + helper.getLanguage('Internal') + '</b></div>' + '<div class="col-xs-1"><b>' + helper.getLanguage('Extra hours') + '</b></div>' + '<div class="col-xs-1"><b>' + helper.getLanguage('Start time') + '</b></div>' + '<div class="col-xs-1"><b>' + helper.getLanguage('End time') + '</b></div>' + '<div class="col-xs-1"><b>' + helper.getLanguage('Break') + '</b></div>' + '<div class="col-xs-1" ng-if="edit.item.edit && !edit.item.is_team"></div>' + '<div class="col-xs-1" ng-if="edit.item.edit && !edit.item.is_team"></div>' + '</div>' + '</th></tr></thead>' + '<tbody>' + '<tr ng-repeat="hour in edit.item.users.users[' + index + '].hours_data.hours">' + '<td>' + '<div class="row">' + '<div class="col-xs-2 text-muted">{{hour.DATE}}</div>' + '<div class="col-xs-' + width_hour + ' text-muted"><span ng-bind-html="hour.time_report_txt"></span></div>' + '<div class="col-xs-1 text-muted">{{hour.internal_report_txt}}</div>' + '<div class="col-xs-1 text-muted">{{hour.extra_hours_txt}}</div>' + '<div class="col-xs-1 text-muted">{{hour.start_time}}</div>' + '<div class="col-xs-1 text-muted">{{hour.end_time}}</div>' + '<div class="col-xs-1 text-muted">{{hour.break}}</div>' + '<div class="col-xs-1 text-center" ng-if="edit.item.edit && !edit.item.is_team"><a class="table_minimal-btn" ng-click="edit.showEditHour(hour,' + index + ')"><span class="glyphicon glyphicon-pencil"></span></a></div>' + '<div class="col-xs-1 text-center" ng-if="edit.item.edit && !edit.item.is_team"><a class="table_minimal-btn" ng-click="edit.deleteSheet(hour,' + index + ')" confirm="' + helper.getLanguage('Are you sure, you wanna delete this entry?') + '"><span class="glyphicon glyphicon-trash"></span></a></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '<table class="table dispatchTable" ng-if="edit.item.users.users[' + index + '].expences_data.is_data">' + '<thead><tr><th>' + '<div class="row">' + '<div class="col-xs-' + width_exp + '"><b>' + helper.getLanguage('Date') + '</b></div>' + '<div class="col-xs-2"><b>' + helper.getLanguage('Category') + '</b></div>' + '<div class="col-xs-2"><b>' + helper.getLanguage('Amount') + '</b></div>' + '<div class="col-xs-2"><b>' + helper.getLanguage('Notes') + '</b></div>' + '<div class="col-xs-2"><b>' + helper.getLanguage('Receipt') + '</b></div>' + '<div class="col-xs-1" ng-if="edit.item.edit && !edit.item.is_team"></div>' + '<div class="col-xs-1" ng-if="edit.item.edit && !edit.item.is_team"></div>' + '</div>' + '</th></tr></thead>' + '<tbody>' + '<tr ng-repeat="exp in edit.item.users.users[' + index + '].expences_data.expense">' + '<td>' + '<div class="row">' + '<div class="col-xs-' + width_exp + ' text-muted">{{exp.DATE}}</div>' + '<div class="col-xs-2 text-muted">{{exp.category}}</div>' + '<div class="col-xs-2 text-muted"><span ng-bind-html="exp.amount"></span></div>' + '<div class="col-xs-2 text-muted"><span ng-bind-html="exp.note"></span></div>' + '<div class="col-xs-2 text-muted">' + '<a href="#" ng-click="edit.viewRecepeit(exp)" ng-if="exp.VIEW_IMG"><small>' + helper.getLanguage('View receipt') + '</small></a><br/>' + '<a href="#"><small ng-click="edit.deleteRecepeit(exp,' + index + ')" ng-if="exp.VIEW_IMG && !edit.item.is_team">' + helper.getLanguage('Delete receipt') + '</small></a>' + '</div>' + '<div class="col-xs-1 text-center" ng-if="edit.item.edit && !edit.item.is_team"><a class="table_minimal-btn" ng-click="edit.showEditExp(exp,' + index + ')"><span class="glyphicon glyphicon-pencil"></span></a></div>' + '<div class="col-xs-1 text-center" ng-if="edit.item.edit && !edit.item.is_team"><a class="table_minimal-btn" ng-click="edit.deleteExpense(exp,' + index + ')" confirm="' + helper.getLanguage('Are you sure, you wanna delete this entry?') + '"><span class="glyphicon glyphicon-trash"></span></a></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>';
            angular.element('#users_table tbody tr.collapseTr').eq(index).after(new_line);
            $compile(angular.element('.orderTable tbody tr.collapseTr').eq(index).next())($scope);
            angular.element('#users_table tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').addClass('hidden');
            angular.element('#users_table tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').removeClass('hidden')
        } else {
            var obj = {
                "error": !1,
                "notice": {
                    "notice": helper.getLanguage("No data to display")
                },
                "success": !1
            };
            edit.showAlerts(obj)
        }
    }
    edit.listLess = function(index) {
        angular.element('#users_table tbody tr.collapseTr').eq(index).next().remove();
        angular.element('#users_table tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').removeClass('hidden');
        angular.element('#users_table tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').addClass('hidden')
    }
    edit.showTime = function(event, parent, model, location, align) {
        var _this = event.target;
        angular.element('body').find('timepicknew').remove();
        if (align) {
            var compiledHTML = $compile('<timepicknew nparent="' + parent + '" nmodel="' + model + '" nalign="' + align + '""></timepicknew>')($scope)
        } else {
            var compiledHTML = $compile('<timepicknew nparent="' + parent + '" nmodel="' + model + '""></timepicknew>')($scope)
        }
        if (location == 1) {
            angular.element(_this).parent().after(compiledHTML)
        } else {
            angular.element(_this).after(compiledHTML)
        }
    }
    edit.task_double_show = function() {
        edit.item.add_subtask1 = !1;
        edit.item.add_subtask2 = !1;
        edit.add.subtask = !1
    }
    edit.addServiceBtn = function() {
        edit.item.add_subtask1 = !0;
        $timeout(function() {
            var selectize = angular.element('#tasks_list')[0].selectize;
            selectize.open()
        })
    }
    edit.taskAutoCfg = {
        valueField: 'name',
        labelField: 'name',
        searchField: ['name','code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Service'),
        create: !1,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7 text-left">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + ' </small>' + '</div>' + '<div class="col-sm-4 text-right">' + (helper.displayNr(item.price)) + '</div>' + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            }
        },
        onType(str) {
            var selectize = angular.element('#tasks_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'articles_list',
                    search: str,
                    cat_id: edit.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.services_list = r.data;
                    if (edit.item.services_list.lines.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value) {
            if (value == undefined || value == '') {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'articles_list',
                    cat_id: edit.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.services_list = r.data
                })
            } else if (this.options[value].article_id && this.options[value].article_id == '99999999999') {
                edit.openModal(edit, 'AddAnArticle', {
                    'do': 'misc-addAnArticle',
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    service_checked: !0
                }, 'md');
                edit.item.task_service_id = undefined;
                return !1
            } else if (value != undefined) {
                var is_error_msg=false;
                for(x in edit.item.services.services){
                    if(this.options[value].article_id==edit.item.services.services[x].article_id){
                        edit.item.task_service_id = null;
                        edit.item.task_name='';
                        edit.item.add_subtask1=false;
                        edit.item.add_subtask2=false;
                        is_error_msg=true;
                        var error_msg={'error':{'error':helper.getLanguage("This value is already in use.")},'success':false,'notice':false};
                        edit.showAlerts(error_msg);
                        break;
                    }
                }
                if(is_error_msg){
                    return false;
                }
                var tsmp=Math.floor(Date.now() / 1000);
                var tmp_item={};
                tmp_item.name               = value;
                tmp_item.id                 = 'tmp_'+tsmp;
                tmp_item.checked            = false;
                tmp_item['no-plus']         = '';
                tmp_item.comment            = '';
                tmp_item.ALLOW_STOCK        = helper.settings.ALLOW_STOCK==1 ? true : false;
                tmp_item.readonly       = '';
                tmp_item.task_budget    = helper.displayNr(this.options[value].price);
                tmp_item.code               = this.options[value].code;
                tmp_item.quantity           = helper.displayNr(1);
                tmp_item.article_id     = this.options[value].article_id;
                edit.item.services.services.push(tmp_item);
                edit.item.services.is_data+=1;
                edit.item.task_service_id = null;
                edit.item.task_name='';
                edit.item.add_subtask1=false;
                edit.item.add_subtask2=false;
                /*var obj = {};
                obj.service_id = edit.item.service_id;
                obj.do = 'maintenance-service-maintenance-serviceAddService';
                obj.xget = 'serviceSubServices';
                obj.task_service_id = this.options[value].article_id;
                obj.task_name = value;
                obj.task_budget = this.options[value].price;
                helper.doRequest('post', 'index.php', obj, function(r) {
                    if (r && r.data) {
                        edit.showAlerts(r);
                        if (r.success) {
                            edit.item.services = r.data;
                            edit.item.task_service_id = undefined
                        }
                        edit.item.task_name = '';
                        edit.item.add_subtask1 = !1;
                        edit.item.add_subtask2 = !1
                    }
                })*/
            }
        },
        onBlur() {
            edit.item.add_subtask1 = !1;
            if (!edit.item.services_list.length) {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'articles_list',
                    cat_id: edit.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.services_list = r.data
                })
            }
        },
        maxItems: 1
    };
    edit.setDownType = function(type) {
        edit.showTxt.down_notice = !1;
        var task_total = 0;
        for (x in edit.item.tasks.tasks) {
            task_total += parseFloat(helper.return_value(edit.item.tasks.tasks[x].task_budget), 10)
        }
        for (x in edit.item.services.services) {
            task_total += (parseFloat(helper.return_value(edit.item.services.services[x].quantity), 10) * parseFloat(helper.return_value(edit.item.services.services[x].task_budget), 10))
        }
        for (x in edit.item.supplies.supplies) {
            if (edit.item.supplies.supplies[x].billable) {
                task_total += (parseFloat(helper.return_value(edit.item.supplies.supplies[x].quantity), 10) * parseFloat(helper.return_value(edit.item.supplies.supplies[x].sell_price), 10))
            }
        }
        for (x in edit.item.articles.articles) {
            if (edit.item.articles.articles[x].billable) {
                task_total += (parseFloat(helper.return_value(edit.item.articles.articles[x].quantity), 10) * parseFloat(helper.return_value(edit.item.articles.articles[x].sell_price), 10))
            }
        }
        if (type == '1') {
            if (edit.item.service_type == '1') {
                edit.item.downpayment_percent = helper.displayNr(parseFloat(helper.return_value(edit.item.downpayment_fixed), 10) * 100 / task_total)
            } else {
                edit.item.downpayment_percent = helper.displayNr(0)
            }
        } else {
            edit.item.downpayment_fixed = helper.displayNr(parseFloat(helper.return_value(edit.item.downpayment_percent), 10) / 100 * task_total)
        }
        edit.item.downpayment_type = type;
        if (parseFloat(helper.return_value(edit.item.downpayment_fixed), 10) > task_total) {
            edit.showTxt.down_notice = !0
        }
    }
    edit.createDownPayment = function() {
        var obj = {};
        obj.downpayment = edit.item.downpayment;
        obj.downpayment_type = edit.item.downpayment_type;
        obj.downpayment_fixed = edit.item.downpayment_fixed;
        obj.downpayment_percent = edit.item.downpayment_percent;
        obj.service_type = edit.item.service_type;
        obj.service_id = edit.service_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        obj.do = 'maintenance-service-maintenance-downpaymentInvoice';
        helper.doRequest('post', 'index.php', obj, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item = r.data
            }
        })
    }
    edit.checkFree = function() {
        var obj = {};
        obj.do = 'maintenance-service';
        obj.xget = 'planUsers';
        obj.planeddate_js = edit.item.planeddate_js.getTime();
        obj.free = '1';
        obj.service_id = edit.service_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            edit.item.planning = r.data;
            if (edit.item.planning.planning_date) {
                edit.item.planning.planning_date = new Date(parseInt(edit.item.planning.planning_date))
            }
        })
    }
    edit.teamAutoCfg = {
        valueField: 'team_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('A Team'),
        create: !1,
        onItemAdd(value, $item) {
            $scope.staff_name = $item.html()
        },
        onChange(value) {
            var obj = {};
            obj.do = 'maintenance-service';
            obj.xget = 'planUsers';
            if (edit.item.planning.planning_date) {
                obj.planning_date = edit.item.planning.planning_date.toUTCString()
            }
            if (value) {
                obj.team_id = this.options[value].team_id
            }
            obj.free = edit.item.planning.free;
            obj.service_id = edit.service_id;
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', obj, function(r) {
                edit.item.planning = r.data;
                if (edit.item.planning.planning_date) {
                    edit.item.planning.planning_date = new Date(edit.item.planning.planning_date)
                }
            })
        },
        maxItems: 1
    };
    edit.navigate = function(i) {
        edit.item.planning.planning_date.setDate(edit.item.planning.planning_date.getDate() + (i * 7));
        var obj = {};
        obj.do = 'maintenance-service';
        obj.xget = 'planUsers';
        if (edit.item.planning.planning_date) {
            obj.planning_date = edit.item.planning.planning_date.toUTCString()
        }
        if (edit.item.planning.team_id) {
            obj.team_id = edit.item.planning.team_id
        }
        obj.free = edit.item.planning.free;
        obj.service_id = edit.service_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            edit.item.planning = r.data;
            if (edit.item.planning.planning_date) {
                edit.item.planning.planning_date = new Date(edit.item.planning.planning_date)
            }
        })
    }
    edit.changePlanningDate = function() {
        var obj = {};
        obj.do = 'maintenance-service';
        obj.xget = 'planUsers';
        obj.planning_date = edit.item.planning.planning_date.toUTCString();
        if (edit.item.planning.team_id) {
            obj.team_id = edit.item.planning.team_id
        }
        obj.free = edit.item.planning.free;
        obj.service_id = edit.service_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            edit.item.planning = r.data;
            if (edit.item.planning.planning_date) {
                edit.item.planning.planning_date = new Date(edit.item.planning.planning_date)
            }
        })
    }
    edit.showMore = function(event, item, user_id) {
        if (item.service_nr == 0) {
            return !1
        }
        var _this = event.target;
        edit.listStats = [];
        if (angular.element(_this).parents('.planning_details').hasClass('active')) {
            angular.element(_this).parents('.planning_details').removeClass('active');
            angular.element(_this).parents('.planning_details').removeClass('is_top_class');
            angular.element(_this).parents('.planning_details').find('.cell_c').empty()
        } else {
            angular.element(_this).parents('.planning_month').find('.planning_details').removeClass('active');
            angular.element(_this).parents('.planning_month').find('.planning_details').removeClass('is_top_class');
            angular.element(_this).parents('.planning_month').find('.planning_details .cell_c').empty();
            var data = {};
            data.do = 'maintenance-planning_over';
            data.xget = 'Day';
            data.start_date = item.services_data[0].tmsmp;
            data.selected_user_id = user_id;
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                edit.listStats = r.data;
                if (edit.listStats.is_top) {
                    angular.element(_this).parents('.planning_details').addClass('is_top_class')
                }
                var new_content = '<div ng-repeat="nitem in edit.listStats.user_data[0].all_day" class="entry {{nitem.phour}} {{nitem.pwidth}} {{nitem.pduration}} {{nitem.colors}}" ng-click="edit.modal(nitem,\'template/viewIntervention\',\'viewInterventionCtrl\')">' + '<p class="text-ellipsis" ng-class="{ \'text-success\' : nitem.status==1, \'text-warning\' : nitem.status==0, \'text-muted\':nitem.status==2 }" title="{{nitem.c_name}}"><del ng-if="nitem.status==2">{{nitem.c_name}}</del><span ng-if="nitem.status!=2">{{nitem.c_name}}</span></p></div>' + '<div class="planning_details_body day-preview" ng-scrollbars ng-scrollbars-config="edit.scroll_config_in">' + '<div class="planning_cell_alt" ng-repeat="nitem2 in edit.listStats.days_nr">{{nitem2.day}}</div>' + '<div ng-repeat="nitem3 in edit.listStats.user_data[0].data" class="entry {{nitem3.phour}} {{nitem3.pduration}} {{nitem3.pwidth}} {{nitem3.colors}}" ng-click="edit.modal(nitem3,\'template/viewIntervention\',\'viewInterventionCtrl\')">' + '<p class="text-ellipsis" ng-class="{ \'text-success\' : nitem3.status==1, \'text-warning\' : nitem3.status==0, \'text-muted\':nitem3.status==2 }"><del ng-if="nitem3.status==2">{{nitem3.c_name}}</del><span ng-if="nitem3.status!=2">{{nitem3.c_name}}</span></p>' + '</div>' + '</div>';
                angular.element(_this).parents('.planning_details').find('.cell_c').append(new_content);
                $compile(angular.element(_this).parents('.planning_details').find('a').nextAll())($scope);
                angular.element(_this).parents('.planning_details').addClass('active');
                if (!edit.scroll_config_in) {
                    $timeout(function() {
                        edit.scroll_config_in = {
                            autoHideScrollbar: !1,
                            theme: 'minimal-dark',
                            axis: 'y',
                            scrollInertia: 50,
                            setTop: '-234px',
                            setHeight: '273px'
                        }
                    })
                }
            })
        }
    }
    edit.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl,
            size: size ? size : 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem
        }, function() {})
    };
    edit.changeTaskBudget = function(item) {
        if(!isNaN(item.id)){
            var data = {};
            data.task_budget = item.task_budget;
            data.task_id = item.id;
            data.service_id = edit.item.service_id;
            data.xget = 'serviceSubTasks';
            data.do = 'maintenance-service-maintenance-update_task_budget';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                edit.showAlerts(r);
                edit.item.tasks = r.data
            })
        }      
    }
    edit.changeTaskQuantity = function(item) {
        if(!isNaN(item.id)){
            var data = {};
            data.task_quantity = item.task_quantity;
            data.task_id = item.id;
            data.service_id = edit.item.service_id;
            data.xget = 'serviceSubTasks';
            data.do = 'maintenance-service-maintenance-update_task_quantity';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                edit.showAlerts(r);
                edit.item.tasks = r.data
            })
        }
    }
    edit.makeDelivery = function() {
        var obj = {
            'service_id': edit.item.service_id,
            'delivery_id': ''
        };
        openModal("article_delivery", obj, "lg")
    }
    edit.makeReservation = function() {
        var obj = {
            'service_id': edit.item.service_id,
            'reservation_id': ''
        };
        openModal("article_reservation", obj, "lg")
    }
    edit.showDeliveries = function() {
        openModal("article_deliveries", edit.item.service_id, "lg")
    }
    edit.installAutoCfg = {
        valueField: 'installation_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Installation'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.name) + ' </span>' + '</div>' + '</div>' + '</div>';
                if (item.installation_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Add Installation') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined || value == '') {
                var selectize = angular.element('#installation_list')[0].selectize;
                var data = {
                    'do': 'maintenance-service',
                    xget: 'installations',
                    service_id: edit.item.service_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.installations = r.data;
                    if (edit.item.installations.length) {
                        selectize.open()
                    }
                });
                if(edit.item.service_id!='tmp'){
                    var obj = {};
                    obj.do = 'maintenance--maintenance-setInstallation';
                    obj.service_id = edit.item.service_id;
                    obj.installation_id = value;
                    edit.item.old_installation_id = value;
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('post', 'index.php', obj, function(r) {
                        edit.showAlerts(r);
                        edit.installationInfo(!0)
                    })
                }else{
                    edit.installationInfo(!0)
                }               
            } else if (value == '99999999999') {
                openModal('create_install', {}, 'lg')
            } else {
                if(edit.item.service_id!='tmp'){
                    var obj = {};
                    obj.do = 'maintenance--maintenance-setInstallation';
                    obj.service_id = edit.item.service_id;
                    obj.installation_id = value;
                    edit.item.old_installation_id = value;
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('post', 'index.php', obj, function(r) {
                        edit.showAlerts(r);
                        edit.installationInfo(!0)
                    })
                }else{
                    edit.installationInfo(!0)
                }               
            }
        },
        onType(str) {
            var selectize = angular.element('#installation_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'installations',
                    search: str,
                    service_id: edit.item.service_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.installations = r.data;
                    if (edit.item.installations.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        maxItems: 1
    };
    edit.addUserPlan = function(item) {
        edit.add.staff = !1;
        edit.item.staff_id = item.user_id;
        edit.item.staff_name = item.name;
        edit.showAdd('staff')
    }
    edit.closeOpened = function() {
        edit.add.subtask = !0;
        edit.add.supplie = !0
    }
    edit.savePlann = function() {
        var obj = angular.copy(edit.item);
        openModal("last_step", obj, "md")
    }
    edit.viewInstall = function(id) {
        var url = $state.href('installation.view', {
            'installation_id': id
        });
        $window.open(url, '_blank')
    }
    edit.showDrop = function() {
        if (edit.showTxt.drop == !1) {
            var data = angular.copy(edit.item.drop_info);
            data.do = 'misc-dropbox';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                edit.item.dropbox = r.data;
                edit.showTxt.drop = !0
            }, function(r) {
                var obj = {
                    "error": {
                        "error": helper.getLanguage("Problem connecting to Dropbox. Please try later.")
                    },
                    "notice": !1,
                    "success": !1
                };
                edit.showAlerts(obj)
            })
        } else {
            edit.showTxt.drop = !1
        }
    }
    edit.deleteDropFile = function(index) {
        var data = angular.copy(edit.item.dropbox.dropbox_files[index].delete_file_link);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.dropbox.nr--;
                edit.item.dropbox.dropbox_files.splice(index, 1)
            }
        })
    }
    edit.viewDataInstall = function(id) {
        openModal("install_data", id, "lg")
    }
    edit.deleteDropImage = function(index) {
        var data = angular.copy(edit.item.dropbox.dropbox_images[index].delete_file_link);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.dropbox.nr--;
                edit.item.dropbox.dropbox_images.splice(index, 1)
            }
        })
    }
    edit.deleteSignature = function(index) {
        var data = angular.copy(edit.item.dropbox.signature[index].delete_file_link);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.dropbox.nr--;
                edit.item.dropbox.signature.splice(index, 1)
            }
        })
    }
    edit.openLightboxModal = function(index, object) {
        Lightbox.openModal(edit[object].dropbox.dropbox_images, index)
    };
    edit.openSignature = function(index, object) {
        Lightbox.openModal(edit[object].dropbox.signature, index)
    };
    edit.uploadDoc = function(type) {
        var obj = angular.copy(edit.item.s3_info);
        obj.type = type;
        openModal('uploadDoc', obj, 'lg')
    }
    edit.changeFrequencyDays = function() {
        helper.doRequest('post', 'index.php', {
            'do': 'maintenance--maintenance-setFrequencyDay',
            'service_id': edit.item.service_id,
            'rec_days': edit.item.rec_days
        }, function(r) {
            if (r.success) {
                edit.showAlerts(r)
            }
        })
    }
    edit.showPlann = function() {
        openModal('saved_planning', {}, 'lg')
    }
    edit.setEnd = function(type) {
        if (type == '1') {
            if (isNaN(edit.item.rec_number)) {
                edit.item.rec_number = undefined
            }else{
                if(edit.item_id != 'tmp'){
                    edit.item.rec_end_date = undefined;
                    helper.doRequest('post', 'index.php', {
                        'do': 'maintenance--maintenance-setRecOpts',
                        'service_id': edit.item.service_id,
                        'rec_number': edit.item.rec_number,
                        'rec_end_date': edit.item.rec_end_date
                    }, function(r) {
                        if (r.success) {
                            edit.showAlerts(r)
                        }
                    })
                }else{
                    edit.item.rec_end_date = undefined
                }                
            }
        } else {
            edit.item.rec_number = undefined;
            if(edit.item_id != 'tmp'){
                helper.doRequest('post', 'index.php', {
                    'do': 'maintenance--maintenance-setRecOpts',
                    'service_id': edit.item.service_id,
                    'rec_number': edit.item.rec_number,
                    'rec_end_date': edit.item.rec_end_date
                }, function(r) {
                    if (r.success) {
                        edit.showAlerts(r)
                    }
                })
            }        
        }
    }
    edit.goEdit = function(service_id, credit_limit = '') {
        if (credit_limit && credit_limit != '') {
            var msg = "<b>" + helper.getLanguage("") + "</b> <span>" + helper.getLanguage("This client is over his credit limit of") + " " + credit_limit + "</span>";
            var cont = {
                "e_message": msg,
                'e_title': helper.getLanguage('Credit warning')
            };
            openModal("stock_info", cont, "md")
        }
        $state.go('service.edit', {
            'service_id': service_id
        })
    }
    edit.goRecEdit = function(service_id) {
        $state.go('rec_service.edit', {
            'service_id': service_id
        })
    }
    edit.setAdvanceDay = function() {
        helper.doRequest('post', 'index.php', {
            'do': 'maintenance--maintenance-setAdvanceDay',
            'service_id': edit.item.service_id,
            'advance_day': edit.item.advance_day
        }, function(r) {
            if (r.success) {
                edit.showAlerts(r)
            }
        })
    }
    edit.installationInfo = function(bool) {
        edit.instal_data = {};
        edit.site_data = {};
        edit.sitemap = {};
        edit.showInstData = !1;
        if (!bool) {
            edit.showTxt.site = !edit.showTxt.site
        }
        if (typePromise) {
            $timeout.cancel(typePromise)
        }
        typePromise = $timeout(function() {
            if (edit.item.installation_id != '0' && edit.item.installation_id != undefined && edit.showTxt.site) {
                edit.showInstLoad = !0;
                helper.doRequest('get', 'index.php', {
                    'do': 'installation-ninstallation',
                    'installation_id': edit.item.installation_id
                }, function(o) {
                    edit.instal_data = o.data;
                    var obj = angular.copy(o.data.s3_info);
                    obj.do = 'misc-s3_link';
                    helper.doRequest('post', 'index.php', obj, function(s) {
                        edit.instal_data.dropbox = s.data;
                        edit.sitemap = {
                            center: angular.copy(o.data.coords),
                            zoom: 12,
                            marker_id: 1,
                            refresh: !0,
                            marker_coord: o.data.coords
                        };
                        helper.doRequest('get', 'index.php', {
                            'do': 'customers-sites',
                            'customer_id': edit.item.customer_id,
                            'address_id': o.data.address_id,
                            view: 1
                        }, function(d) {
                            edit.site_data = d.data;
                            edit.showInstLoad = !1;
                            edit.showInstData = !0
                        })
                    })
                })
            }
        }, 700)
    }
    edit.submitIntervention = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'maintenance--maintenance-submitIntervention',
            'service_id': edit.item.service_id,
            'serial_number': edit.item.serial_number
        }, function(r) {
            edit.showAlerts(r)
        })
    }
    edit.linkToInstall = function() {
        var obj = {};
        obj.target_id = edit.item.installation_id;
        obj.target_folder = 'installation';
        obj.images = angular.copy(edit.item.dropbox.dropbox_images);
        openModal('to_install', obj, 'lg')
    }
    edit.addModel = function() {
        openModal('add_model', {}, 'lg')
    }
    edit.saveModel = function() {
        return !1
    }

    edit.sendEmail = function() {
        var params={
            'do': 'maintenance-email_preview',
            service_id: edit.service_id,
            message: edit.email.e_message,
            use_html: edit.item.use_html
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post','index.php',params,function(r){
            var viewEmail=angular.copy(edit.email);
            viewEmail.e_message=r.data.e_message;
            var obj={"app":"maintenance",
                "recipients":edit.auto.options.recipients,
                "use_html":edit.item.use_html,
                "is_file":edit.item.is_file,
                "dropbox":edit.item.dropbox,
                "email":viewEmail
            };
            openModal("send_email", angular.copy(obj), "lg");
        })      
    }

    edit.cancel=function() {
        if (edit.service_id == 'tmp') {
            if(edit.item.is_recurring){
                $state.go('rec_services');
            }else{
                $state.go('services'); 
            }
        } else {
            if(edit.item.is_recurring){
                $state.go('rec_services');             
            }else{
                $state.go('service.view', {
                    service_id: edit.item_id
                })
            }        
        }
    }

    edit.dragUsers= {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            /*if(edit.service_id != 'tmp'){
                var tmp_users=[];
                for(var i in edit.item.users.users){
                    tmp_users.push(edit.item.users.users[i].id);            
                }
                helper.doRequest('post', 'index.php', {
                    'do': 'maintenance--maintenance-changeOrderUsers',
                    'order_users':tmp_users,
                    'service_id': edit.service_id
                })
            }*/           
        }
    };

    edit.dragTasks= {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            /*if(edit.service_id != 'tmp'){
                var tmp_tasks=[];
                for(var i in edit.item.tasks.tasks){
                    tmp_tasks.push(edit.item.tasks.tasks[i].id);            
                }
                helper.doRequest('post', 'index.php', {
                    'do': 'maintenance--maintenance-changeOrderTasks',
                    'order_tasks':tmp_tasks,
                    'service_id': edit.service_id
                })
            }*/           
        }
    };

    edit.dragServices= {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            /*if(edit.service_id != 'tmp'){
                var tmp_services=[];
                for(var i in edit.item.services.services){
                    tmp_services.push(edit.item.services.services[i].id);            
                }
                helper.doRequest('post', 'index.php', {
                    'do': 'maintenance--maintenance-changeOrderServices',
                    'order_services':tmp_services,
                    'service_id': edit.service_id
                })
            } */          
        }
    };

    edit.dragArticles= {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            /*if(edit.service_id != 'tmp'){
                var tmp_articles=[];
                for(var i in edit.item.articles.articles){
                    tmp_articles.push(edit.item.articles.articles[i].id);            
                }
                helper.doRequest('post', 'index.php', {
                    'do': 'maintenance--maintenance-changeOrderArticles',
                    'order_articles':tmp_articles,
                    'service_id': edit.service_id
                })
            }*/           
        }
    };

    edit.dragSupplies= {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            /*if(edit.service_id != 'tmp'){
                var tmp_supplies=[];
                for(var i in edit.item.supplies.supplies){
                    tmp_supplies.push(edit.item.supplies.supplies[i].id);            
                }
                helper.doRequest('post', 'index.php', {
                    'do': 'maintenance--maintenance-changeOrderSupplies',
                    'order_supplies':tmp_supplies,
                    'service_id': edit.service_id
                })
            }*/           
        }
    };

    edit.parentVariantCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select article'),
        maxOptions: 5,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.family) {
                    item.family = "/ " + item.family
                }
                var stock_display = '';
                if (item.hide_stock == 0) {
                    stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">' + helper.getLanguage('Stock') + ' <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + stock_display + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onFocus() {
            var selectize = this.$input[0].selectize;
            var index = this.$input[0].attributes['selectindex'].value;
            var parentVariant = this.$input[0].attributes['selectarticle'].value;
            var tmp_data = {
                'do': 'misc-articleVariants',
                'parentVariant': parentVariant,
                buyer_id: edit.item.buyer_id,
                lang_id: edit.item.email_language,
                cat_id: edit.item.price_category_id,
                remove_vat: edit.item.remove_vat,
                vat_regime_id: edit.item.vat_regime_id,
                app: edit.app
            };
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', tmp_data, function(r) {
                edit.item.articles.articles[index].variantsOpts = r.data.lines;
                if (edit.item.articles.articles[index].variantsOpts.length) {
                    selectize.open();
                }
            });
        },
        onType(str) {
            var selectize = this.$input[0].selectize;
            var index = this.$input[0].attributes['selectindex'].value;
            var parentVariant = this.$input[0].attributes['selectarticle'].value;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do': 'misc-articleVariants',
                    'parentVariant': parentVariant,
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id,
                    app: edit.app
                };
                helper.doRequest('get', 'index.php', tmp_data, function(r) {
                    edit.item.articles.articles[index].variantsOpts = r.data.lines;
                    if (edit.item.articles.articles[index].variantsOpts.length) {
                        selectize.open();
                    }
                });
            }, 300)
        },
        onChange(value) {
            var index = this.$input[0].attributes['selectindex'].value;
            if (value != undefined) {
                var collection = edit.item.articles.articles[index].variantsOpts;
                for (x in collection) {
                    if (collection[x].article_id == value) {
                        edit.item.articles.articles[index].has_variants_done = '1';
                        collection[x].is_variant_for_line = edit.item.articles.articles[index].tr_id;
                        edit.addArticle(collection[x]);
                        break;
                    }
                }
            }
        },
        onBlur() {
            var index = this.$input[0].attributes['selectindex'].value;
            var parentVariant = this.$input[0].attributes['selectarticle'].value;
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do': 'misc-articleVariants',
                    'parentVariant': parentVariant,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id,
                    app: edit.app
                };
                helper.doRequest('get', 'index.php', tmp_data, function(r) {
                    edit.item.articles.articles[index].variantsOpts = r.data.lines;
                });
            }, 300)
        },
        maxItems: 1
    };

    edit.childVariantCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select article'),
        maxOptions: 5,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.family) {
                    item.family = "/ " + item.family
                }
                var stock_display = '';
                if (item.hide_stock == 0) {
                    stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">' + helper.getLanguage('Stock') + ' <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + stock_display + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onType(str) {
            var selectize = this.$input[0].selectize;
            var index = this.$input[0].attributes['selectindex'].value;
            var parentVariant = this.$input[0].attributes['selectarticle'].value;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do': 'misc-articleVariants',
                    'parentVariant': parentVariant,
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    vat_regime_id: edit.item.vat_regime_id,
                    app: edit.app
                };
                helper.doRequest('get', 'index.php', tmp_data, function(r) {
                    edit.item.articles.articles[index].variantsOpts = r.data.lines;
                    if (edit.item.articles.articles[index].variantsOpts.length) {
                        selectize.open();
                    }
                });
            }, 300)
        },
        onChange(value) {
            var index = this.$input[0].attributes['selectindex'].value;
            if (value != undefined) {
                var tmp_data = angular.copy(edit.item.articles.articles[index].variantsOpts);
                for (x in tmp_data) {
                    if (tmp_data[x].article_id == value) {
                        var tmp_selected_item = angular.copy(tmp_data[x]);
                        tmp_selected_item.quantity = parseFloat(helper.return_value(edit.item.articles.articles[index].quantity, 10));
                        if(!isNaN(edit.item.articles.articles[index].id)){
                            var data = {
                                service_id: edit.service_id,
                                'do': 'maintenance-service-maintenance-deleteServiceArticle',
                                a_id: edit.item.articles.articles[index].id,
                                article_id: edit.item.articles.articles[index].article_id,
                                xget: 'serviceArticles'
                            };
                            helper.doRequest('post', 'index.php', data);                      
                            edit.item.articles.articles.splice(index, 1);
                        }else{
                            edit.delete('article',edit.item.articles.articles[index]);
                        }              
                        edit.addArticle(tmp_selected_item, index);            
                        break;
                    }
                }
            }
        },
        onBlur() {
            var index = this.$input[0].attributes['selectindex'].value;
            edit.item.articles.articles[index].showVariants = false;
        },
        maxItems: 1
    };

    edit.showChildVariants = function(item, index) {
        var tmp_data = {
            'do': 'misc-articleVariants',
            'parentVariant': item.is_variant_for,
            'is_variant_for_line': item.is_variant_for_line,
            buyer_id: edit.item.buyer_id,
            lang_id: edit.item.email_language,
            cat_id: edit.item.price_category_id,
            remove_vat: edit.item.remove_vat,
            vat_regime_id: edit.item.vat_regime_id,
            app: edit.app
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', tmp_data, function(r) {
            item.showVariants = true;
            edit.item.articles.articles[index].variantsOpts = r.data.lines;
        });
    }

    $timeout(function() {
        angular.element('.color-ball-select').find('.selectize-input input').attr('disabled','disabled');
    });

    function openModal(type, item, size) {
        switch (type) {
            case 'article_delivery':
                var iTemplate = 'ArtDelivery';
                var ctas = 'art';
                break;
            case 'stock_info':
                var iTemplate = 'modal';
                var ctas = 'vmMod';
                break;
            case 'show_types':
                var iTemplate = 'ShowServiceType';
                var ctas = 'add';
                break;
            case 'service_hours':
                var iTemplate = 'ShowServiceHours';
                var ctas = 'edit';
                break;
            case 'service_expences':
                var iTemplate = 'ShowServiceExp';
                var ctas = 'add';
                break;
            case 'article_deliveries':
                var iTemplate = 'ArtDeliveries';
                var ctas = 'art';
                break;
            case 'last_step':
                var iTemplate = 'RecLastStep';
                var ctas = 'vm';
                break;
            case 'create_install':
                var iTemplate = 'CreateInstall';
                var ctas = 'vm';
                break;
            case 'install_data':
                var iTemplate = 'InstallData';
                var ctas = 'vm';
                break;
            case 'uploadDoc':
                var iTemplate = 'AmazonUpload';
                break;
            case 'saved_planning':
                var iTemplate = 'SavedPlann';
                var ctas = 'vm';
                break;
            case 'article_reservation':
                var iTemplate = 'ArtReservation';
                var ctas = 'art';
                break;
            case 'to_install':
                var iTemplate = 'ToInstallData';
                var ctas = 'vm';
                break;
            case 'create_deal':
                var iTemplate = 'CreateDeal';
                var ctas = 'show';
                break;
            case 'add_model':
                var iTemplate = 'serviceModels';
                var ctas = 'vmMod';
                break;
            case 'send_email':
                var iTemplate = 'SendEmail';
                var ctas = 'vm';
                break;
            case 'add_steps':
                var iTemplate = 'ServiceAddSteps';
                var ctas = 'add';
                break;
        }
        var params = {
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'article_delivery':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'maintenance-article_delivery',
                    'service_id': item.service_id,
                    'delivery_id': item.delivery_id
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        edit.renderPage(d)
                    }
                }
                break;
            case 'article_reservation':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'maintenance-article_reservation',
                    'service_id': item.service_id,
                    'reservation_id': item.reservation_id
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        edit.renderPage(d)
                    }
                }
                break;
            case 'stock_info':
                params.template = 'miscTemplate/' + iTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                break;
            case 'show_types':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        edit.renderPage(d)
                    }
                }
                params.callbackExit = function() {
                    edit.item.billable = !1
                }
                break;
            case 'service_hours':
            case 'service_expences':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        edit.item.users = d.data;
                        edit.showAlerts(d);
                        $timeout(function() {
                            edit.listMore(d.in.index)
                        })
                    }
                }
                break;
            case 'article_deliveries':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'maintenance-article_deliveries',
                    'service_id': item
                };
                params.callback = function() {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('post', 'index.php', {
                        'do': 'maintenance-service',
                        'service_id': edit.item.service_id
                    }, function(r) {
                        edit.renderPage(r)
                    })
                }
                break;
            case 'last_step':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        edit.renderPage(d)
                    }
                }
                break;
            case 'create_install':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'installation-installation',
                    'installation_id': 'tmp',
                    'buyer_id': edit.item.buyer_id,
                    'main_address_id': edit.item.main_address_id,
                    'delivery_address_id': edit.item.delivery_address_id
                };
                params.callback = function(d) {
                    if (d) {
                        var data = {
                            'do': 'maintenance-service',
                            xget: 'installations',
                            service_id: edit.item.service_id
                        };
                        helper.doRequest('get', 'index.php', data, function(r) {
                            edit.item.installations = r.data;
                            editItem.init(edit)
                        })
                    }
                    edit.item.installation_id = edit.item.old_installation_id
                }
                break;
            case 'install_data':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                var sr_n = '';
                for (x in edit.item.installations) {
                    if (edit.item.installations[x].installation_id == item) {
                        sr_n = edit.item.installations[x].serial_number;
                        break
                    }
                }
                params.params = {
                    'do': 'misc-s3_link',
                    's3_folder': 'installation',
                    'id': edit.item.installation_id
                };
                break;
            case 'uploadDoc':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        edit.showS3()
                    }
                }
                break;
            case 'saved_planning':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'maintenance-service',
                    'xget': 'planRecurringSaved',
                    'start_date': edit.item.rec_first_day,
                    'rec_first_day': edit.item.rec_first_day,
                    'service_id': edit.item.service_id,
                    'activeType': '1'
                };
                params.callback = function(d) {
                    if (d) {
                        edit.renderPage(d)
                    }
                }
                break;
            case 'to_install':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.item = item;
                break;
            case 'create_deal':
                params.template = 'cTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'customers-deal_edit',
                    'opportunity_id': 'tmp',
                    'stage_id': edit.item.stage_id,
                    'board_id': edit.item.board_id,
                    'user_id': edit.item.user_id,
                    'buyer_id': edit.item.buyer_id,
                    'contact_id': edit.item.contact_id,
                    'not_redirect_deal': 0
                };
                params.callback = function(d) {
                    if (d) {
                        var data = {
                            'do': 'maintenance-service',
                            xget: 'deals',
                            buyer_id: edit.item.buyer_id
                        };
                        helper.doRequest('get', 'index.php', data, function(r) {
                            edit.item.deals = r.data;
                            edit.item.deal_id=d.deal_id;
                            edit.item.old_deal_id=d.deal_id;
                            editItem.init(edit)
                        })
                    }else{
                        edit.item.deal_id = edit.item.old_deal_id
                    }               
                }
                break;
            case 'add_model':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'maintenance-service',
                    xget: 'serviceModels',
                    'lang': edit.item.email_language,
                    'service_id': edit.item.service_id
                };
                params.callback = function(d) {
                    if (d) {
                        if(d.data.service_id != 'tmp') {
                            edit.saveData(project_all,true);
                        } else {
                            if (d.data.articles) {
                                i = 1;
                                for (y in d.data.articles) {
                                    for(x in d.data.articles[y]) {
                                        var tsmp=Math.floor(Date.now() / 1000) + i;
                                        d.data.articles[y][x].id = 'tmp_' + tsmp;
                                        i++;
                                    }
                                }
                                if (!edit.item.articles) {
                                    edit.item.articles = d.data.articles;
                                } else {
                                    edit.item.articles.articles = edit.item.articles.articles.concat(d.data.articles.articles)
                                    edit.item.articles.is_data = edit.item.articles.is_data + d.data.articles.is_data;
                                }
                            }
                            if (d.data.users) {
                                i = 1;
                                for (y in d.data.users) {
                                    for(x in d.data.users[y]) {
                                        var tsmp=Math.floor(Date.now() / 1000) + i;
                                        d.data.users[y][x].id = 'tmp_' + tsmp;
                                        i++;
                                    }
                                }
                                if (!edit.item.users) {
                                edit.item.users = d.data.users;
                                } else {
                                    edit.item.users.users = edit.item.users.users.concat(d.data.users.users)
                                    edit.item.users.is_data = edit.item.users.is_data + d.data.users.is_data;
                                }
                            }
                            if (d.data.tasks) {
                                i = 1;
                                for (y in d.data.tasks) {
                                    for(x in d.data.tasks[y]) {
                                        var tsmp=Math.floor(Date.now() / 1000) + i;
                                        d.data.tasks[y][x].id = 'tmp_' + tsmp;
                                        i++;
                                    }
                                }
                                if (!edit.item.tasks) {
                                edit.item.tasks = d.data.tasks;
                                } else {
                                    edit.item.tasks.tasks = edit.item.tasks.tasks.concat(d.data.tasks.tasks)
                                    edit.item.tasks.is_data = edit.item.tasks.is_data + d.data.tasks.is_data;
                                }
                            }
                            if (d.data.services) {
                                i = 1;
                                for (y in d.data.services) {
                                    for(x in d.data.services[y]) {
                                        var tsmp=Math.floor(Date.now() / 1000) + i;
                                        d.data.services[y][x].id = 'tmp_' + tsmp;
                                        i++;
                                    }
                                }
                                if (!edit.item.services) {
                                edit.item.services = d.data.services;
                                } else {
                                    edit.item.services.services = edit.item.services.services.concat(d.data.services.services)
                                    edit.item.services.is_data = edit.item.services.is_data + d.data.services.is_data;
                                }
                            }
                            if (d.data.supplies) {
                                i = 1;
                                for (y in d.data.supplies) {
                                    for(x in d.data.supplies[y]) {
                                        var tsmp=Math.floor(Date.now() / 1000) + i;
                                        d.data.supplies[y][x].id = 'tmp_' + tsmp;
                                        i++;
                                    }
                                }
                                if (!edit.item.supplies) {
                                edit.item.supplies = d.data.supplies;
                                } else {
                                    edit.item.supplies.supplies = edit.item.supplies.supplies.concat(d.data.supplies.supplies)
                                    edit.item.supplies.is_data = edit.item.supplies.is_data + d.data.supplies.is_data;
                                }
                            }
                            edit.renderPage(edit.item);
                        }
                    }
                }
                break;
            case 'send_email':
                params.template ='miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        edit.sendEmails(d);
                    }
                }
                break;
            case 'add_steps':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.item = item;
                params.callbackExit = function() {
                    $state.go('services')
                }
                params.callback = function() {
                    if (edit.item.quote_id || edit.item.contract_id || edit.item.deal_id) {
                        //edit.saveClientData(edit, edit.item.field, edit.project_all)
                        if(edit.item.quote_task){
                            for(x in edit.item.quote_group_data){
                                if(edit.item.quote_group_data[x].checked){
                                    for(y in edit.item.quote_group_data[x].data_line){
                                        var tmp_item={};
                                        tmp_item.name               = edit.item.quote_group_data[x].data_line[y].item;
                                        tmp_item.id                 = 'tmp_'+Math.floor(Date.now() / 1000);
                                        tmp_item.checked            = false;
                                        tmp_item['no-plus']         = '';
                                        tmp_item.comment            = '';
                                        tmp_item.readonly       = '';
                                        tmp_item.task_budget    = helper.displayNr(edit.item.quote_group_data[x].data_line[y].amount_nr);
                                        tmp_item.article_id = 0;
                                        edit.item.tasks.tasks.push(tmp_item);
                                        edit.item.tasks.is_data+=1;
                                    }
                                }
                            }
                        }
                        if(edit.item.quote_service){
                            for(x in edit.item.quote_group_services){
                                if(edit.item.quote_group_services[x].checked){
                                    for(y in edit.item.quote_group_services[x].services){
                                        var tsmp=Math.floor(Date.now() / 1000);
                                        var tmp_item={};
                                        tmp_item.name               = edit.item.quote_group_services[x].services[y].item;
                                        tmp_item.id                 = 'tmp_'+tsmp;
                                        tmp_item.checked            = false;
                                        tmp_item['no-plus']         = '';
                                        tmp_item.comment            = '';
                                        tmp_item.ALLOW_STOCK        = helper.settings.ALLOW_STOCK==1 ? true : false;
                                        tmp_item.readonly       = '';
                                        tmp_item.task_budget    = helper.displayNr(edit.item.quote_group_services[x].services[y].unit_price_nr);
                                        tmp_item.code               = edit.item.quote_group_services[x].services[y].code;
                                        tmp_item.quantity           = edit.item.quote_group_services[x].services[y].quantity;
                                        tmp_item.article_id     = edit.item.quote_group_services[x].services[y].article_id;
                                        edit.item.services.services.push(tmp_item);
                                        edit.item.services.is_data+=1;
                                    }
                                }
                            }
                        }
                        if(edit.item.quote_art){
                            for(x in edit.item.quote_group_articles){
                                if(edit.item.quote_group_articles[x].checked){
                                    for(y in edit.item.quote_group_articles[x].articles){
                                        var tmp_item={};
                                        tmp_item.article_id     = edit.item.quote_group_articles[x].articles[y].article_id;
                                        tmp_item.name           = edit.item.quote_group_articles[x].articles[y].item;
                                        tmp_item.id             = 'tmp_'+Math.floor(Date.now() / 1000);
                                        tmp_item.quantity           = edit.item.quote_group_articles[x].articles[y].quantity;
                                        tmp_item.quantity_nr        = helper.return_value(edit.item.quote_group_articles[x].articles[y].quantity);
                                        tmp_item.ALLOW_STOCK        = helper.settings.ALLOW_STOCK==1 ? true : false;
                                        tmp_item.to_deliver     = edit.item.quote_group_articles[x].articles[y].quantity;
                                        tmp_item.delivered          = false;
                                        tmp_item.to_be_reserved     =  helper.return_value(edit.item.quote_group_articles[x].articles[y].quantity);
                                        tmp_item.to_be_delivered        =  helper.return_value(edit.item.quote_group_articles[x].articles[y].quantity);
                                        tmp_item.reserved_stock     = 0;
                                        tmp_item.price          = edit.item.quote_group_articles[x].articles[y].price;
                                        tmp_item.margin         = edit.item.quote_group_articles[x].articles[y].margin;
                                        tmp_item.purchase_price     = edit.item.quote_group_articles[x].articles[y].purchase_price;
                                        tmp_item.sell_price     = helper.displayNr(edit.item.quote_group_articles[x].articles[y].price);
                                        tmp_item.hstock         = edit.item.quote_group_articles[x].articles[y].hstock ? '1' : '0'; 
                                        tmp_item.code           = edit.item.quote_group_articles[x].articles[y].code;
                                        tmp_item.stock          = edit.item.quote_group_articles[x].articles[y].stock;
                                        tmp_item.billed         = false;
                                        tmp_item.billable           = true;
                                        tmp_item.stock_status       = edit.item.quote_group_articles[x].articles[y].stock_status;
                                        tmp_item.has_variants = edit.item.quote_group_articles[x].articles[y].has_variants;
                                        tmp_item.has_variants_done = edit.item.quote_group_articles[x].articles[y].has_variants_done;
                                        tmp_item.is_variant_for = edit.item.quote_group_articles[x].articles[y].is_variant_for;
                                        tmp_item.is_variant_for_line = edit.item.quote_group_articles[x].articles[y].is_variant_for_line;
                                        tmp_item.variant_type = edit.item.quote_group_articles[x].articles[y].variant_type; 
                                        edit.item.articles.articles.push(tmp_item);
                                        edit.item.articles.is_data+=1;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
        }
        modalFc.open(params)
    }
    edit.renderPage(data)
}]).controller('supplyCtrl', ['$scope', 'helper', '$uibModalInstance', 'obj', 'helper', function($scope, helper, $uibModalInstance, obj, helper) {
    $scope.obj = obj;
    $scope.title = $scope.obj.article_id == 0 ? helper.getLanguage("Supplies") : helper.getLanguage("Articles");
    var style = helper.settings.ACCOUNT_NUMBER_FORMAT;
    $scope.showAlerts = function(d, formObj) {
        helper.showAlerts($scope, d, formObj)
    }
    $scope.showAdd = function(d, type) {
        switch (type) {
            case 'sell_price':
            case 'price':
            case 'margin':
                if (type == 'sell_price') {
                    d.margin = number_format(((helper.return_value(d.sell_price) - helper.return_value(d.price)) / helper.return_value(d.price)) * 100, 2, style[1], style[0]);
                    var data2 = {
                        service_id: d.service_id,
                        'do': 'maintenance--maintenance-serviceArticleUpdate',
                        value: d.margin,
                        field: 'margin',
                        article_id: d.id
                    }
                } else {
                    d.sell_price = number_format(((helper.return_value(d.price) * helper.return_value(d.margin) / 100) + (helper.return_value(d.price) * 1)), 2, style[1], style[0]);
                    var data2 = {
                        service_id: d.service_id,
                        'do': 'maintenance--maintenance-serviceArticleUpdate',
                        value: d.sell_price,
                        field: 'sell_price',
                        article_id: d.id
                    }
                }
                var data = {
                    service_id: d.service_id,
                    'do': 'maintenance--maintenance-serviceArticleUpdate',
                    value: d[type],
                    field: type,
                    article_id: d.id
                };
                break;
            case 'name':
            case 'info':
                var data = {
                    service_id: d.service_id,
                    'do': 'maintenance--maintenance-updateArt',
                    value: d[type],
                    type: type,
                    purchase_id: d.id
                };
            default:
                break
        }
        if (data) {
            helper.doRequest('post', 'index.php', data, function() {
                if (data2) {
                    helper.doRequest('post', 'index.php', data2)
                }
            })
        }
    }
    $scope.updateAdd = function(d, type) {
        switch (type) {
            case 'sell_price':
                var data = {
                    service_id: d.service_id,
                    'do': 'maintenance--maintenance-serviceArtSellUpdate',
                    value: d.sell_price,
                    article_id: d.id
                };
                //angular.element('.loading_alt').removeClass('hidden');
                helper.doRequest('post', 'index.php', data, function(r) {
                    //angular.element('.loading_alt').addClass('hidden');
                    if (r.success) {
                        d.margin = number_format(((helper.return_value(d.sell_price) - helper.return_value(d.price)) / helper.return_value(d.sell_price)) * 100, 2, style[1], style[0])
                    } else {
                        d.sell_price = r.data.price
                    }
                });
                break;
            case 'purchase_price':
                var data = {
                    service_id: d.service_id,
                    'do': 'maintenance--maintenance-serviceArtPriceUpdate',
                    value: d.purchase_price,
                    article_id: d.id
                };
                //angular.element('.loading_alt').removeClass('hidden');
                helper.doRequest('post', 'index.php', data, function(r) {
                   // angular.element('.loading_alt').addClass('hidden');
                    if (r.success) {
                        if (helper.return_value(d.margin) == 100) {
                            var sell_price1 = (-helper.return_value(d.purchase_price) * 100) / (1 - 100);
                            var sell_price2 = (-helper.return_value(d.purchase_price) * 100) / (99 - 100);
                            d.sell_price = number_format((sell_price1 + sell_price2), 2, style[1], style[0])
                        } else {
                            d.sell_price = number_format(((-helper.return_value(d.purchase_price) * 100) / (helper.return_value(d.margin) - 100)), 2, style[1], style[0])
                        }
                    } else {
                        d.purchase_price = r.data.purchase_price
                    }
                });
                break;
            case 'margin':
                var data = {
                    service_id: d.service_id,
                    'do': 'maintenance--maintenance-serviceArtMargUpdate',
                    value: d.margin,
                    article_id: d.id
                };
                //angular.element('.loading_alt').removeClass('hidden');
                helper.doRequest('post', 'index.php', data, function(r) {
                    //angular.element('.loading_alt').addClass('hidden');
                    if (r.success) {
                        if (helper.return_value(d.margin) == 100) {
                            var sell_price1 = (-helper.return_value(d.purchase_price) * 100) / (1 - 100);
                            var sell_price2 = (-helper.return_value(d.purchase_price) * 100) / (99 - 100);
                            d.sell_price = number_format((sell_price1 + sell_price2), 2, style[1], style[0])
                        } else {
                            d.sell_price = number_format(((-helper.return_value(d.purchase_price) * 100) / (helper.return_value(d.margin) - 100)), 2, style[1], style[0])
                        }
                    } else {
                        d.margin = r.data.margin
                    }
                });
                break;
        }
    }
    $scope.ok = function() {
        $uibModalInstance.close()
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('taskModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'obj', function($scope, helper, $uibModalInstance, obj) {
    $scope.obj = obj;
    var style = helper.settings.ACCOUNT_NUMBER_FORMAT;
    $scope.ok = function() {
        if(!isNaN(obj.id)){
            var data = {
                'do': 'maintenance--maintenance-addTaskComment',
                t_comment: obj.comment,
                task_id: obj.id,
                service_id: obj.service_id
            };
            helper.doRequest('post', 'index.php', data);
        }       
        $uibModalInstance.close()
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('pdfSettingsCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.obj = data;
    $scope.ok = function() {
        var data = {
            'do': 'maintenance--maintenance-pdf_settings',
            logo: $scope.obj.default_logo,
            email_language: $scope.obj.language_id,
            service_id: $scope.obj.service_id,
            identity_id: $scope.obj.identity_id
        };
        helper.doRequest('post', 'index.php', data);
        $uibModalInstance.close()
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('serviceReportCtrl', ['$scope', 'helper', '$stateParams', function($scope, helper, $stateParams) {
    console.log('hello from serviceReportCtrl');
    $scope.table = [];
    $scope.service_id = $stateParams.service_id;
    $scope.hours = '';
    helper.doRequest('get', 'index.php?do=maintenance-service&xget=report&service_id=' + $scope.service_id, function(r) {
        $scope.table = r.data.table;
        $scope.hours = r.data.hours;
        $scope.expenses = r.data.expenses;
        $scope.chart = {
            labels: r.data.chart.name,
            data: r.data.chart.hours,
            color: r.data.chart.color,
            opt: {
                legend: !1,
                elements: {
                    arc: {
                        borderWidth: 0
                    },
                    point: !1
                }
            }
        }
    })
}]).controller('serviceAddCtrl', ['$scope', '$cacheFactory', '$stateParams', 'helper', '$timeout', '$state', 'editItem', 'modalFc', 'selectizeFc', 'ballFc',function($scope, $cacheFactory, $stateParams, helper, $timeout, $state, editItem, modalFc, selectizeFc,ballFc) {
    var edit = this,
        typePromise;
    edit.item = {};
    edit.app = 'maintenance';
    edit.ctrlpag = 'service';
    edit.tmpl = 'template';
    edit.item_id = "tmp";
    edit.service_id = "tmp";
    edit.isAddPage = !0;
    edit.initial_start = !0;
    var cacheQuote = $cacheFactory.get('QuoteToService');
    if (cacheQuote != undefined) {
        var cachedDataQuote = cacheQuote.get('data')
    }
    var cacheMContract = $cacheFactory.get('ContractToService');
    if (cacheMContract != undefined) {
        var cachedDataMContract = cacheMContract.get('data')
    }
    var cacheMRContract = $cacheFactory.get('ContractRecurringToService');
    if (cacheMRContract != undefined) {
        var cachedDataMRContract = cacheMRContract.get('data')
    }
    var cacheDeal = $cacheFactory.get('DealToDocument');
    if (cacheDeal != undefined) {
        var cachedDataDeal = cacheDeal.get('data')
    }
    edit.showTxt = {
        sevice_settings: !1,
        addBtn: !0,
        address: !1,
    };
    edit.datePickOpen = {
        rec_start_date: !1,
        rec_end_date: !1,
    }
    edit.add_customer = !0;
    edit.oldObj = {};
    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.ballCfg = ballFc.configure({
        labelField: 'name',
        searchField: ['name'],
    });
    edit.auto = {
        options: {
            contacts: [],
            cc: [],
            user: [],
            addresses: []
        },
        disable: {
            customer: !0,
            contact: !0
        }
    };
    edit.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    edit.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Languages'),
        maxItems: 1
    };
    edit.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    edit.showEditFnc = showEditFnc;
    edit.showCCSelectize = showCCSelectize;
    edit.cancelEdit = cancelEdit;
    edit.removeDeliveryAddr = removeDeliveryAddr;
    editItem.init(edit);
    edit.item.multiple_id = "0";
    edit.contactAddObj = {};
    edit.renderPage = function(r) {
        edit.item = r.data;
        edit.auto.options.addresses = r.data.addresses;
        edit.auto.options.site_addresses = r.data.site_addresses;
        edit.auto.options.contacts = r.data.contacts;
        edit.auto.options.cc = r.data.cc;
        edit.auto.options.user = r.data.user_auto;
        if (edit.item.rec_start_date) {
            edit.item.rec_start_date = new Date(edit.item.rec_start_date)
        }
        if (edit.item.rec_end_date) {
            edit.item.rec_end_date = new Date(edit.item.rec_end_date)
        }
        if (edit.initial_start == !0 && !$stateParams.template_id) {
            openModal("add_steps", edit, "lg")
        }
        edit.contactAddObj = {
            language_dd: edit.item.language_dd2,
            language_id: edit.item.language_id,
            gender_dd: edit.item.gender_dd,
            gender_id: edit.item.gender_id,
            title_dd: edit.item.title_dd,
            title_id: edit.item.title_id,
            country_dd: edit.item.country_dd,
            country_id: edit.item.main_country_id,
            add_contact: !0,
            buyer_id: edit.item.buyer_id,
            'do': 'maintenance-service-maintenance-tryAddC',
            contact_only: !0,
            item_id: edit.item_id,
            service_type: edit.item.service_type,
            billable: edit.item.billable,
        }
        if (edit.item.is_recurring && !edit.item.contract_id) {
            edit.contactAddObj.is_recurring = edit.item.is_recurring;
            edit.contactAddObj.rec_start_date = edit.item.rec_start_date;
            edit.contactAddObj.rec_frequency = edit.item.rec_frequency;
            edit.contactAddObj.rec_end_date = edit.item.rec_end_date;
            edit.contactAddObj.rec_days = edit.item.rec_days;
            edit.contactAddObj.rec_number = edit.item.rec_number;
            edit.contactAddObj.recurring_no_end = edit.item.recurring_no_end
        }
    }
    edit.userAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select collaborator'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.value) + ' </span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        maxItems: 1
    };
    edit.dealAutoCfg = {
        valueField: 'deal_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Deal'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.name) + ' </span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        maxItems: 1
    };

    function removeDeliveryAddr() {
        edit.item.delivery_address = '';
        edit.item.delivery_address_id = ''
    }

    function cancelEdit() {
        if (Object.keys(edit.oldObj).length == 0) {
            return edit.removeCust(edit)
        }
        edit.showTxt.address = !0;
        for (x in edit.oldObj) {
            if (edit.item[x]) {
                if (x == 'delivery_address') {
                    edit.item[x] = edit.oldObj[x].trim()
                } else {
                    edit.item[x] = edit.oldObj[x]
                }
            }
        }
        edit.item.buyer_id = edit.oldObj.buyer_id
    }

    function showEditFnc() {
        edit.showTxt.address = !1;
        edit.oldObj = {
            buyer_id: angular.copy(edit.item.buyer_id),
            contact_id: angular.copy(edit.item.contact_id),
            contact_name: angular.copy(edit.item.contact_name),
            BUYER_NAME: angular.copy(edit.item.buyer_name),
            delivery_address: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address)),
            delivery_address_id: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_id)),
            delivery_address_txt: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_txt)),
            view_delivery: (edit.item.sameAddress ? !1 : angular.copy(edit.item.view_delivery)),
            sameAddress: angular.copy(edit.item.sameAddress),
        }
    }

    function showCCSelectize() {
        edit.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }
    edit.setEnd = function(type) {
        if (type == '1') {
            if (isNaN(edit.item.rec_number)) {
                edit.item.rec_number = undefined
            } else {
                edit.item.rec_end_date = undefined
            }
        } else {
            edit.item.rec_number = undefined
        }
    }
    edit.goEdit = function(service_id, credit_limit = '') {
        if (credit_limit && credit_limit != '') {
            var msg = "<b>" + helper.getLanguage("") + "</b> <span>" + helper.getLanguage("This client is over his credit limit of") + " " + credit_limit + "</span>";
            var cont = {
                "e_message": msg,
                'e_title': helper.getLanguage('Credit warning')
            };
            openModal("stock_info", cont, "md")
        }
        $state.go('service.edit', {
            'service_id': service_id
        })
    }
    edit.goRecEdit = function(service_id) {
        $state.go('rec_service.edit', {
            'service_id': service_id
        })
    }
    edit.showAlerts = function(d, formObj) {
        helper.showAlerts(edit, d, formObj)
    }
    edit.saveModel = function() {
        if (!edit.item.template_name) {
            return !1
        }
        edit.saveData(edit.serviceModelForm)
    }
    edit.saveData = function(formObj) {
        var obj = angular.copy(edit.item);
        if ($stateParams.template_id) {
            obj.template_id = $stateParams.template_id
        }
        obj.do = 'maintenance-service-maintenance-addService';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                if (r.data.is_recurring) {
                    $state.go('rec_service.edit', {
                        'service_id': r.data.service_id
                    })
                } else {
                    if (r.data.customer_credit_limit) {
                        var msg = "<b>" + helper.getLanguage("") + "</b> <span>" + helper.getLanguage("This client is over his credit limit of") + " " + r.data.customer_credit_limit + "</span>";
                        var cont = {
                            "e_message": msg,
                            'e_title': helper.getLanguage('Credit warning')
                        };
                        openModal("stock_info", cont, "md")
                    }
                    if ($stateParams.template_id) {
                        $state.go('service.editModel', {
                            'template_id': r.data.service_id
                        })
                    } else {
                        $state.go('service.edit', {
                            'service_id': r.data.service_id
                        })
                    }
                }
            } else {
                edit.showAlerts(r, formObj)
            }
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'add_steps':
                var iTemplate = 'ServiceAddSteps';
                var ctas = 'add';
                break;
            case 'stock_info':
                var iTemplate = 'modal';
                var ctas = 'vmMod';
                break
        }
        var params = {
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'add_steps':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.item = item;
                params.callbackExit = function() {
                    $state.go('services')
                }
                params.callback = function() {
                    if (edit.item.quote_id || edit.item.contract_id || edit.item.deal_id) {
                        edit.saveClientData(edit, edit.item.field, edit.project_all)
                    }
                }
                break;
            case 'stock_info':
                params.template = 'miscTemplate/' + iTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                break
        }
        modalFc.open(params)
    }
    var initData = {};
    if ($stateParams.template_id) {
        initData.service_id = $stateParams.template_id
    } else {
        initData.service_id = edit.service_id
    }
    if ($stateParams.for_user) {
        initData.for_user = $stateParams.for_user
    }
    if ($stateParams.buyer_id) {
        initData.buyer_id = $stateParams.buyer_id
    }
    if ($stateParams.is_recurring) {
        initData.is_recurring = $stateParams.is_recurring
    }
    if (cachedDataQuote != undefined) {
        for (x in cachedDataQuote) {
            initData[x] = cachedDataQuote[x]
        }
        cacheQuote.remove('data')
    }
    if (cachedDataMContract != undefined) {
        for (x in cachedDataMContract) {
            initData[x] = cachedDataMContract[x]
        }
        cacheMContract.remove('data')
    }
    if (cachedDataMRContract != undefined) {
        for (x in cachedDataMRContract) {
            initData[x] = cachedDataMRContract[x]
        }
        cacheMRContract.remove('data')
    }
    if (cachedDataDeal != undefined) {
        for (x in cachedDataDeal) {
            initData[x] = cachedDataDeal[x]
        }
        cacheDeal.remove('data')
    }

    $timeout(function() {
        angular.element('.color-ball-select').find('.selectize-input input').attr('disabled','disabled');
    });
    
    helper.doRequest('get', 'index.php?do=maintenance-service', initData, edit.renderPage)
}]).controller('serviceSettingsCtrl', ['$scope', 'helper', '$uibModal', 'settingsFc', 'data', function($scope, helper, $uibModal, settingsFc, data) {
    settingsFc.init($scope);
    $scope.logos = {};
    $scope.layout = {};
    $scope.namingConv = {};
    $scope.default_logo = '';
    $scope.customLabel = {};
    $scope.message = {};
    $scope.settings = {};
    $scope.expenses = {};
    $scope.weblink = {};
    $scope.email_default = {};
    $scope.yourModel = {
        menu: [
            ['bold', 'italic'],
            ['left-justify', 'center-justify', 'right-justify'],
            ['ordered-list', 'unordered-list', 'outdent', 'indent'],
            ['link', 'image', 'image-upload']
        ]
    };
    $scope.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 580,
        height: 300,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile bold italic underline | alignleft aligncenter alignright | bullist numlist | link image", "forecolor backcolor | fontsizeselect |  table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    $scope.upload = function(e) {
        angular.element(e.target).parents('.upload-box').find('.newUploadFile2').click()
    }
    angular.element('.newUploadFile2').on('click', function(e) {
        e.stopPropagation()
    }).on('change', function(e) {
        e.preventDefault();
        var _this = $(this),
            p = _this.parents('.upload-box'),
            x = p.find('.newUploadFile2');
        if (x[0].files.length == 0) {
            return !1
        }
        if (x[0].files[0].size > 61440) {
            alert("Error: file is to big.");
            return !1
        }
        p.find('.img-thumbnail').attr('src', '');
        var tmppath = URL.createObjectURL(x[0].files[0]);
        p.find('.img-thumbnail').attr('src', tmppath);
        p.find('.btn').addClass('hidden');
        p.find('.nameOfTheFile').text(x[0].files[0].name);
        var formData = new FormData();
        formData.append("Filedata", x[0].files[0]);
        formData.append("do", 'maintenance--maintenance-uploadify');
        p.find('.upload-progress-bar').css({
            width: '0%'
        });
        p.find('.upload-file').removeClass('hidden');
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "index.php", !0);
        oReq.upload.addEventListener("progress", function(e) {
            var pc = parseInt(e.loaded / e.total * 100);
            p.find('.upload-progress-bar').css({
                width: pc + '%'
            })
        });
        oReq.onload = function(oEvent) {
            var res = JSON.parse(oEvent.target.response);
            p.find('.upload-file').addClass('hidden');
            p.find('.btn').removeClass('hidden');
            if (oReq.status == 200) {
                if (res.success) {
                    helper.doRequest('get', 'index.php?do=maintenance-settings&xget=logos', function(r) {
                        $scope.logos = r.data.logos
                    })
                } else {
                    alert(res.error)
                }
            } else {
                alert("Error " + oReq.status + " occurred when trying to upload your file.")
            }
        };
        oReq.send(formData)
    });
    $scope.deleteImage = function(r) {
        var d = angular.copy(r);
        d.do = 'maintenance-settings-maintenance-delete_logo';
        d.name = d.account_logo;
        d.xget = 'logos';
        helper.doRequest('post', 'index.php', d, function(r) {
            $scope.showAlerts(r);
            if (!r.error) {
                $scope.logos = r.data.logos
            }
        })
    }
    $scope.setLogo = function(r) {
        var d = angular.copy(r);
        d.do = 'maintenance--maintenance-set_default_logo';
        d.name = d.account_logo;
        helper.doRequest('post', 'index.php', d, function(r) {
            $scope.showAlerts(r)
        })
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.load = function(h, k) {
        $scope.alerts = [];
        var d = {
            'do': 'maintenance-settings',
            'xget': h
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            $scope.showAlerts(r);
            $scope[k] = r.data[k];
            if (k == 'logos') {
                $scope.default_logo = r.data.default_logo
            }
        })
    }
    $scope.saveSetting = function(item, k) {
        helper.doRequest('post', 'index.php', item, function(r) {
            $scope.showAlerts(r);
            $scope[k] = r.data[k]
        })
    }
    $scope.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl,
            size: size ? size : 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem
        }, function() {
            if (r.load) {
                $scope.load(r.load[0], r.load[1])
            }
        })
    };
    $scope.changeLang = function(r) {
        //console.log(r);
        helper.doRequest('post', 'index.php', r, function(r) {
            $scope.message = r.data.message;
            $scope.showAlerts(r)
        })
    }
    $scope.deleteExpense = function(r, $index) {
        var d = angular.copy(r);
        d.do = 'maintenance--maintenance-delete_expense';
        helper.doRequest('post', 'index.php', d, function(r) {
            $scope.showAlerts(r);
            if (!r.error) {
                $scope.expenses.splice($index, 1)
            }
        })
    }
    $scope.renderPage = function(r) {
        helper.doRequest('get', 'index.php?do=maintenance-settings&xget=namingConv', function(r) {
            $scope.namingConv = r.data.namingConv
        })
    };
    $scope.saveDefaultEmailInt = function(r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            $scope.email_default = r.data.email_default;
            $scope.showAlerts( r)
        })
    }
    $scope.renderPage(data)
}]).controller('intlabelCtrl', ['$scope', 'helper', '$uibModalInstance', 'obj', function($scope, helper, $uibModalInstance, obj) {
    $scope.label = {};
    helper.doRequest('get', 'index.php', obj, function(r) {
        $scope.label = r.data
    });
    $scope.ok = function() {
        var d = angular.copy($scope.label);
        d.do = 'maintenance--maintenance-label_update';
        helper.doRequest('post', 'index.php', d, function(r) {
            $scope.showAlerts(r)
        })
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('expenseModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'obj', function($scope, helper, $uibModalInstance, obj) {
    $scope.expense = obj;
    $scope.ok = function() {
        if ($scope.expense.expense_id) {
            $scope.expense.do = 'maintenance--maintenance-update_expense2'
        } else {
            $scope.expense.do = 'maintenance--maintenance-add_expense'
        }
        $scope.expense.load = ['expenses', 'expenses'];
        var d = angular.copy($scope.expense);
        helper.doRequest('post', 'index.php', d, function(r) {
            $scope.showAlerts(r)
        })
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('serviceTsCtrl', ['$scope', '$compile', '$stateParams', '$uibModal', 'helper', 'list', function($scope, $compile, $stateParams, $uibModal, helper, list) {
    $scope.listStats = [];
    $scope.intNotSeen = {};
    $scope.isReadonly = !0;
    $scope.listStats = [];
    $scope.activeView = 1;
    $scope.activeType = 2;
    $scope.planning = {
        user_id: $stateParams.user_id,
        planning: [],
        is_top_class: '',
        is_top: !1,
        start: 0,
        week_txt: '',
        week_days: []
    };
    $scope.views = [{
        name: 'Planned',
        id: 1,
        active: 'active'
    }, {
        name: 'Finished',
        id: 3,
        active: ''
    }, {
        name: 'Accepted',
        id: 4,
        active: ''
    }];
    $scope.types = [{
        name: 'Weekly',
        id: 2,
        active: 'active'
    }, {
        name: 'Monthly',
        id: 3,
        active: ''
    }];
    $scope.search = {
        search: '',
        'do': 'maintenance-planning',
        view: $scope.activeView,
        xget: 'intervention',
        offset: 1
    };

    $scope.markAsSeenList = function(p) {
        list.saveForPDF($scope, 'maintenance--maintenance-markAsSeenList', p)
    }
    $scope.markAsSeen = function(item) {
        var items = [];
        if (item == undefined) {
            for (x in $scope.list) {
                 if($scope.list[x].check_add_to_product){
                     items.push($scope.list[x].cid);
                 }   
                }
        }else{
            items.push(item.cid);
        }
        var data = {
            'do': 'maintenance-planning-maintenance-MarkAsSeen',
            items: items,
            'xget': 'intNotSeen'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if(r.data){
                $scope.intNotSeen = r.data.intNotSeen
            }

        })
    }
 /*   $scope.markSeen = function(item) {
        var data = {
            'do': 'maintenance-planning-maintenance-MarkAsSeen',
            'id': item.cid,
            'xget': 'intNotSeen'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.intNotSeen = r.data.intNotSeen
        })
    }*/
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1
    }
    $scope.max_rows = 0;
    $scope.list = [];
    $scope.renderList = function(d) {
        if (d.data.intNotSeen) {
            //$scope.list = d.data.query;
             $scope.list = d.data.intNotSeen;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.listStats = []
        }
    };
    $scope.searchThing = function() {
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.filters = function(p, i) {
        list.filter($scope, p, $scope.renderInterventionsList)
    }
    $scope.load = function() {
        $scope.list=[];
        helper.doRequest('get', 'index.php?do=maintenance-planning&xget=intervention', $scope.renderInterventionsList);
    }
    $scope.renderInterventionsList=function(d){
        $scope.list = d.data.query;
        $scope.lr = d.data.lr;
        $scope.listStats = []
    }
    $scope.loadPlanning=function(){
        helper.doRequest('get', 'index.php?do=maintenance-planning&xget=intNotSeen', {
            'user_id': $stateParams.user_id
        }, $scope.renderPlanning);
    }
    $scope.showStats = function(item, $index) {
        var add = !1;
        if (!$scope.listStats[$index]) {
            $scope.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (add == !0) {
            helper.doRequest('get', 'index.php?do=maintenance-service&xget=serviceUserStat&service_id=' + item.service_id, function(r) {
                if (r.data) {
                    $scope.listStats[$index] = r.data;
                    angular.element('.interventionTable tbody tr.collapseTr').eq($index).after('<tr class="versionInfo"><td colspan="6" class="no_border">' + '<table class="table_minimal">' + '<thead>' + '<tr>' + '<th>' + '<div class="row">' + '<div class="col-xs-8">Name</div>' + '<div class="col-xs-4">Hours</div>' + '</div>' + '</th>' + '</tr>' + '</thead>' + '<tbody>' + '<tr ng-repeat="item2 in listStats[' + $index + '].list">' + '<td>' + '<div class="row" >' + '<div class="col-xs-8">{{item2.name}}</div>' + '<div class="col-xs-4">{{item2.hours}}</div>' + '</div>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>');
                    $compile(angular.element('.interventionTable tbody tr.collapseTr').eq($index).next())($scope)
                }
            })
        } else {
            angular.element('.interventionTable tbody tr.collapseTr').eq($index).next().toggleClass('hidden')
        }
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.renderPlanning = function(r) {
        if (r.data) {
            $scope.intNotSeen = r.data.intNotSeen;
            $scope.list = r.data.intNotSeen;
            $scope.auto.options.user = r.data.select;
            $scope.planning.user_id = r.data.user_id;
            $scope.planning.planning = r.data.planning;
            $scope.planning.planningAllDay = r.data.planningAllDay;
            $scope.planning.is_top_class = r.data.is_top_class;
            $scope.planning.is_top = r.data.is_top;
            $scope.planning.week_txt = r.data.week_txt;
            $scope.planning.week_days = r.data.week_days;
            $scope.scrollPlanning(r.data.first_hour);
            if (r.data.start_date) {
                $scope.start_date = new Date(r.data.start_date)
            } else {
                $scope.start_date = new Date()
            }
        }
    }
    $scope.renderMonth = function(r) {
        if (r.data) {
            $scope.auto.options.user = r.data.select;
            $scope.planning.user_id = r.data.user_id;
            $scope.planning.month_txt = r.data.month_txt;
            $scope.planning.week = r.data.week;
            if (r.data.start_date) {
                $scope.start_date = new Date(r.data.start_date)
            } else {
                $scope.start_date = new Date()
            }
        }
    }
    $scope.scrollPlanning = function(e) {
        if (e < 1) {
            e = 0
        }
        angular.element('.planning_body').scrollTop(e * 39)
    }
    $scope.userAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select collaborator'),
        create: !1,
        onItemAdd(value, $item) {
            $scope.staff_name = $item.html()
        },
        onChange(value) {
            if (value) {
                if ($scope.activeType < 3) {
                    helper.doRequest('get', 'index.php?do=maintenance-planning&xget=intNotSeen&start_date=' + $scope.start_date.toUTCString() + '&user_id=' + value, $scope.renderPlanning)
                } else {
                    helper.doRequest('get', 'index.php?do=maintenance-planning&xget=Month&start_date=' + $scope.start_date.toUTCString() + '&user_id=' + value, $scope.renderMonth)
                }
            }
        },
        maxItems: 1
    };
    $scope.auto = {
        options: {
            user: []
        }
    };
    $scope.navigate = function(i) {
        $scope.start_date.setDate($scope.start_date.getDate() + (i * 7));
        $scope.planning.start = $scope.planning.start + i;
        helper.doRequest('get', 'index.php?do=maintenance-planning&xget=intNotSeen&start=' + $scope.planning.start + '&start_date=' + $scope.start_date.toUTCString() + '&user_id=' + $scope.planning.user_id, $scope.renderPlanning)
    }
    $scope.navigate_month = function(i) {
        $scope.start_date = $scope.start_date.addMonths(i);
        $scope.planning.start = $scope.planning.start + i;
        helper.doRequest('get', 'index.php?do=maintenance-planning&xget=Month&start=' + $scope.planning.start + '&start_date=' + $scope.start_date.toUTCString() + '&user_id=' + $scope.planning.user_id, $scope.renderMonth)
    }
    $scope.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl,
            size: size ? size : 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem
        }, function() {})
    };
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.typeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'Overview',
        create: !1,
        onChange(value) {
            if (value < 3) {
                $scope.planning.start = 0;
                helper.doRequest('get', 'index.php?do=maintenance-planning&xget=intNotSeen&start_date=' + $scope.start_date.toUTCString() + '&user_id=' + $scope.planning.user_id, $scope.renderPlanning)
            } else {
                $scope.planning.start = 0;
                helper.doRequest('get', 'index.php?do=maintenance-planning&xget=Month&start_date=' + $scope.start_date.toUTCString() + '&user_id=' + $scope.planning.user_id, $scope.renderMonth)
            }
        },
        maxItems: 1
    };
    $scope.scroll_config = {
        autoHideScrollbar: !1,
        theme: 'minimal-dark',
        scrollInertia: 50,
        setTop: '-234px'
    };
    $scope.showMore = function(event, item) {
        if (item.data_nr == 0) {
            return !1
        }
        var _this = event.target;
        $scope.listStats = [];
        delete $scope.scroll_config;
        if (angular.element(_this).parents('.planning_details').hasClass('active')) {
            angular.element(_this).parents('.planning_details').removeClass('active');
            angular.element(_this).parents('.planning_details').removeClass('is_top_class');
            angular.element(_this).parents('.planning_details').find('.cell_c').empty()
        } else {
            if (!$scope.scroll_config_in) {
                $scope.scroll_config_in = {
                    autoHideScrollbar: !1,
                    theme: 'minimal-dark',
                    axis: 'y',
                    scrollInertia: 50,
                    setTop: '-234px',
                    setHeight: '273px'
                }
            }
            angular.element(_this).parents('.planning_month').find('.planning_details').removeClass('active');
            angular.element(_this).parents('.planning_month').find('.planning_details').removeClass('is_top_class');
            angular.element(_this).parents('.planning_month').find('.planning_details .cell_c').empty();
            var data = {};
            data.do = 'maintenance-planning';
            data.xget = 'Day';
            data.start_date = item.data[0].tmsmp;
            data.selected_user_id = $scope.planning.user_id;
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                $scope.listStats = r.data;
                if ($scope.listStats.is_top) {
                    angular.element(_this).parents('.planning_details').addClass('is_top_class')
                }
                var new_content = '<div ng-repeat="nitem in listStats.user_data[0].all_day" class="entry {{nitem.phour}} {{nitem.pwidth}} {{nitem.pduration}}" style="border-color:{{nitem.color_ball}}" ng-click="modal(nitem,\'template/viewIntervention\',\'viewInterventionCtrl\')">' + '<p class="text-ellipsis" ng-class="{ \'text-muted\':nitem.status==2 }" ng-style="{\'color\': nitem.status!=2 ? nitem.color_ball : \'\'}" title="{{nitem.c_name}}"><del ng-if="nitem.status==2">{{nitem.c_name}}</del><span ng-if="nitem.status!=2">{{nitem.c_name}}</span></p></div>' + '<div class="planning_details_body day-preview" ng-scrollbars ng-scrollbars-config="scroll_config_in">' + '<div class="planning_cell_alt" ng-repeat="nitem2 in listStats.days_nr">{{nitem2.day}}</div>' + '<div ng-repeat="nitem3 in listStats.user_data[0].data" class="entry {{nitem3.phour}} {{nitem3.pduration}} {{nitem3.pwidth}}" style="border-color:{{nitem3.color_ball}}" ng-click="modal(nitem3,\'template/viewIntervention\',\'viewInterventionCtrl\')">' + '<small ng-class="{ \'text-muted\':nitem3.status==2 }" ng-style="{\'color\': nitem3.status!=2 ? nitem3.color_ball : \'\'}"><span>{{nitem3.start_time}}</span> <span ng-if="nitem3.pduration">- {{nitem3.end_time}}</span></small>' + '<p class="text-ellipsis" ng-class="{ \'text-muted\':nitem3.status==2 }" ng-style="{\'color\': nitem3.status!=2 ? nitem3.color_ball : \'\'}"><del ng-if="nitem3.status==2">{{nitem3.c_name}}</del><span ng-if="nitem3.status!=2">{{nitem3.c_name}}</span></p>' + '</div>' + '</div>';
                angular.element(_this).parents('.planning_details').find('.cell_c').append(new_content);
                $compile(angular.element(_this).parents('.planning_details').find('a').nextAll())($scope);
                angular.element(_this).parents('.planning_details').addClass('active')
            })
        }
    }
    $scope.changeDate = function() {
        $scope.planning.start = 0;
        helper.doRequest('get', 'index.php?do=maintenance-planning&xget=intNotSeen&start_date=' + $scope.start_date.toUTCString() + '&user_id=' + $scope.planning.user_id, $scope.renderPlanning)
    }
    $scope.loadPlanning();
}]).controller('planningCtrl', ['$scope', '$timeout', '$compile', 'helper', '$uibModal', 'list', 'data', function($scope, $timeout, $compile, helper, $uibModal, list, data) {
    $scope.listStats = [];
    $scope.isReadonly = !0;
    $scope.activeType = parseInt(data.data.activeView);
    $scope.planning = {
        user_id: 0,
        is_top_class: '',
        is_top: !1,
        start: 0
    };
    $scope.types = [{
        name: helper.getLanguage('Daily'),
        id: 1,
        active: ''
    }, {
        name: helper.getLanguage('Weekly'),
        id: 2,
        active: 'active'
    }, {
        name: helper.getLanguage('Monthly'),
        id: 3,
        active: ''
    }];
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1
    }
    $scope.renderPlanning = function(r) {
        if (r.data) {
            $scope.auto.options.team = r.data.select;
            $scope.planning.team_id = r.data.team_id;
            $scope.planning.user_data = r.data.user_data;
            $scope.planning.days_nr = r.data.days_nr;
            if ($scope.activeType == 1) {
                $scope.planning.day_txt = r.data.day_txt;
                $scope.planning.is_top = r.data.is_top;
                if (!$scope.scroll_config) {
                    $timeout(function() {
                        $scope.scroll_config = {
                            autoHideScrollbar: !1,
                            theme: 'minimal-dark',
                            axis: 'x',
                            scrollInertia: 50,
                            setLeft: '-679px',
                            advanced: {
                                autoExpandHorizontalScroll: !0
                            }
                        }
                    })
                }
            } else if ($scope.activeType == 2) {
                $scope.planning.week_txt = r.data.week_txt
            } else {
                $scope.planning.month_txt = r.data.month_txt
            }
            if (r.data.start_date) {
                $scope.start_date = new Date(r.data.start_date)
            } else {
                $scope.start_date = new Date()
            }
        }
    }
    $scope.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl,
            size: size ? size : 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem
        }, function() {})
    };
    $scope.teamAutoCfg = {
        valueField: 'team_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('A Team'),
        create: !1,
        onItemAdd(value, $item) {
            $scope.staff_name = $item.html()
        },
        onChange(value) {
            if (value) {
                var type_r = '';
                switch (parseInt($scope.activeType)) {
                    case 1:
                        type_r = 'Day';
                        break;
                    case 2:
                        type_r = 'Week';
                        break;
                    case 3:
                        type_r = 'Month';
                        break
                }
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-planning_over',
                    'xget': type_r,
                    'start_date': $scope.start_date.toUTCString(),
                    'team_id': value
                }, function(r) {
                    $scope.renderPlanning(r)
                })
            }
        },
        maxItems: 1
    };
    $scope.auto = {
        options: {
            team: []
        }
    };
    $scope.navigate = function(i) {
        $scope.start_date.setDate($scope.start_date.getDate() + (i * 7));
        $scope.planning.start = $scope.planning.start + i;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'maintenance-planning_over',
            'xget': 'Week',
            'start': $scope.planning.start,
            'start_date': $scope.start_date.toUTCString(),
            'team_id': $scope.planning.team_id
        }, function(r) {
            $scope.renderPlanning(r)
        })
    }
    $scope.navigate_month = function(i) {
        $scope.start_date = $scope.start_date.addMonths(i);
        $scope.planning.start = $scope.planning.start + i;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'maintenance-planning_over',
            'xget': 'Month',
            'start': $scope.planning.start,
            'start_date': $scope.start_date.toUTCString(),
            'team_id': $scope.planning.team_id
        }, function(r) {
            $scope.renderPlanning(r)
        })
    }
    $scope.navigate_day = function(i) {
        $scope.start_date.setDate($scope.start_date.getDate() + i);
        $scope.planning.start = $scope.planning.start + i;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'maintenance-planning_over',
            'xget': 'Day',
            'start': $scope.planning.start,
            'start_date': $scope.start_date.toUTCString(),
            'team_id': $scope.planning.team_id
        }, function(r) {
            $scope.renderPlanning(r)
        })
    }
    $scope.typeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'Overview',
        create: !1,
        onChange(value) {
            var type_r = '';
            switch (value) {
                case '1':
                    type_r = 'Day';
                    break;
                case '2':
                    type_r = 'Week';
                    break;
                case '3':
                    type_r = 'Month';
                    break
            }
            $scope.planning.start = 0;
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', {
                'do': 'maintenance-planning_over',
                'xget': type_r,
                'start_date': $scope.start_date.toUTCString(),
                'team_id': $scope.planning.team_id,
                'track': value
            }, function(r) {
                $scope.renderPlanning(r)
            })
        },
        maxItems: 1
    };
    $scope.showMore = function(event, item, user_id, parent_index, child_index) {
        var _this = event.target;
        if (item.service_nr == 0) {
            angular.element('.planning_row_content').find('planning').remove();
            var compiledHTML = $compile('<planning pindex="' + parent_index + '" cindex="' + child_index + '" nuser="' + user_id + '""></planning>')($scope);
            angular.element(_this).parent().after(compiledHTML);
            return !1
        }
        $scope.listStats = [];
        if (angular.element(_this).parents('.planning_details').hasClass('active')) {
            angular.element(_this).parents('.planning_details').removeClass('active');
            angular.element(_this).parents('.planning_details').removeClass('is_top_class');
            angular.element(_this).parents('.planning_details').find('.cell_c').empty()
        } else {
            angular.element(_this).parents('.planning_groups').find('.planning_details').removeClass('active');
            angular.element(_this).parents('.planning_groups').find('.planning_details').removeClass('is_top_class');
            angular.element(_this).parents('.planning_groups').find('.planning_details .cell_c').empty();
            var data = {};
            data.do = 'maintenance-planning_over';
            data.xget = 'Day';
            data.start_date = item.services_data[0].tmsmp;
            data.selected_user_id = user_id;
            data.team_id = $scope.planning.team_id;
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                $scope.listStats = r.data;
                if ($scope.listStats.is_top) {
                    angular.element(_this).parents('.planning_details').addClass('is_top_class')
                }
                var new_content = '<div ng-repeat="nitem in listStats.user_data[0].all_day" class="entry {{nitem.phour}} {{nitem.pwidth}} {{nitem.pduration}} {{nitem.colors}}" ng-click="modal(nitem,\'template/viewIntervention\',\'viewInterventionCtrl\')">' + '<p class="text-ellipsis" ng-class="{ \'text-success\' : nitem.status==1, \'text-warning\' : nitem.status==0, \'text-muted\':nitem.status==2 }" title="{{nitem.c_name}}"><del ng-if="nitem.status==2">{{nitem.c_name}}</del><span ng-if="nitem.status!=2">{{nitem.c_name}}</span></p></div>' + '<div class="planning_details_body day-preview" ng-scrollbars ng-scrollbars-config="scroll_config_in">' + '<div class="planning_cell_alt" ng-repeat="nitem2 in listStats.days_nr">{{nitem2.day}}</div>' + '<div ng-repeat="nitem3 in listStats.user_data[0].data" class="entry {{nitem3.phour}} {{nitem3.pduration}} {{nitem3.pwidth}} {{nitem3.colors}}" ng-click="modal(nitem3,\'template/viewIntervention\',\'viewInterventionCtrl\')">' + '<p class="text-ellipsis" ng-class="{ \'text-success\' : nitem3.status==1, \'text-warning\' : nitem3.status==0, \'text-muted\':nitem3.status==2 }"><del ng-if="nitem3.status==2">{{nitem3.c_name}}</del><span ng-if="nitem3.status!=2">{{nitem3.c_name}}</span></p>' + '</div>' + '</div>';
                angular.element(_this).parents('.planning_details').find('.cell_c').append(new_content);
                $compile(angular.element(_this).nextAll())($scope);
                angular.element(_this).parents('.planning_details').addClass('active');
                if (!$scope.scroll_config_in) {
                    $timeout(function() {
                        $scope.scroll_config_in = {
                            autoHideScrollbar: !1,
                            theme: 'minimal-dark',
                            axis: 'y',
                            scrollInertia: 50,
                            setTop: '-234px',
                            setHeight: '273px'
                        }
                    })
                }
            })
        }
    }
    $scope.changeDate = function() {
        var type_r = '';
        switch ($scope.activeType) {
            case '1':
                type_r = 'Day';
                break;
            case '2':
                type_r = 'Week';
                break
        }
        $scope.planning.start = 0;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'maintenance-planning_over',
            'xget': type_r,
            'start_date': $scope.start_date.toUTCString(),
            'team_id': $scope.planning.team_id
        }, function(r) {
            $scope.renderPlanning(r)
        })
    }
    $scope.renderPlanning(data)
}]).controller('viewInterventionCtrl', ['$scope', '$compile', '$timeout', 'helper', 'obj', '$uibModalInstance', '$state', 'Lightbox', function($scope, $compile, $timeout, helper, obj, $uibModalInstance, $state, Lightbox) {
    var typePromise;
    $scope.showTxt = {
        fillSheet: !1,
        fillTime: !1,
        notes: !1,
        site: !1,
    };
    $scope.service = obj;
    $scope.luser = 0;
    $scope.service_sheet = {
        servicing_sheet_id: $scope.service.service_id,
        service_date: new Date()
    };
    $scope.edit = {
        service_sheet: {}
    };
    $scope.edit.service_sheet = $scope.service_sheet;
    $scope.service_expense = {
        service_id: $scope.service.service_id,
        service_date: new Date()
    }
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        finishDate: !1,
        planeddate: !1,
        planeddate2: !1,
        startDate: !1
    }
    $scope.auto = {
        options: {
            expenseCat: []
        }
    };
    $scope.showTime = function(event, parent, model, location, align) {
        var _this = event.target;
        angular.element('body').find('timepicknew').remove();
        if (align) {
            var compiledHTML = $compile('<timepicknew nparent="' + parent + '" nmodel="' + model + '" nalign="' + align + '""></timepicknew>')($scope)
        } else {
            var compiledHTML = $compile('<timepicknew nparent="' + parent + '" nmodel="' + model + '""></timepicknew>')($scope)
        }
        if (location == 1) {
            angular.element(_this).parent().after(compiledHTML)
        } else {
            angular.element(_this).after(compiledHTML)
        }
    }
    $scope.expenseAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: 'Select category',
        create: !1,
        maxItems: 1
    };
    $scope.openDate = function(p) {
        $scope.datePickOpen[p] = !0
    };
    $scope.correctVal = function() {
        if (arguments[0].start_time) {
            arguments[0].start_time = corect_val(arguments[0].start_time)
        }
        if (arguments[0].end_time) {
            arguments[0].end_time = corect_val(arguments[0].end_time)
        }
        if (arguments[0].break) {
            arguments[0].break = corect_val(arguments[0].break)
        }
    }
    helper.doRequest('get', 'index.php', {
        'do': 'maintenance-service',
        service_id: $scope.service.service_id,
        'xget': 'viewIntervention'
    }, function(r) {
        $scope.service.customer_name = r.data.customer_name;
        $scope.service.contact_name = r.data.contact_name;
        $scope.service.free_field = r.data.free_field;
        $scope.service.customer_email = r.data.customer_email;
        $scope.service.customer_phone = r.data.customer_phone;
        $scope.service_sheet.sheet_user_id = r.data.loged_user;
        $scope.service_expense.user_id = r.data.loged_user;
        $scope.luser = r.data.loged_user;
        $scope.auto.options.expenseCat = r.data.expenseCat;
        $scope.service.report = r.data.report;
        $scope.service.installation_id = r.data.installation_id;
        $scope.service.customer_id = r.data.customer_id;
        $scope.service.service_type = r.data.service_type
    });
    $scope.toggleStuff = function(d, e) {
        $scope.showTxt[d] = !$scope.showTxt[d];
        $scope.showTxt[e] = null
    }
    $scope.addSheet = function(d) {
        if ($scope.validateAddSheet(d)) {
            $scope.showTxt.fillSheet = !1;
            d.do = 'maintenance--maintenance-add_sheet';
            d.start_time = convert_value(d.start_time.getHours()+":"+d.start_time.getMinutes());
            d.end_time = convert_value(d.end_time.getHours()+":"+d.end_time.getMinutes());
            d.break = convert_value(d.break);
            helper.doRequest('post', 'index.php', d, function(r) {
                $scope.showAlerts(r);
                if (r.success) {
                    $scope.service_sheet = {
                        servicing_sheet_id: $scope.service.service_id,
                        service_date: new Date(),
                        sheet_user_id: $scope.luser
                    };
                    $scope.edit.service_sheet = $scope.service_sheet
                }
            })
        }
    }
    $scope.validateAddSheet = function(p) {
        var err_valid = !1;
        var msg = '';
        
        if (!p.servicing_sheet_id) {
            msg = helper.getLanguage("Please select a Sheet");
            err_valid = !0
        } else if (!p.start_time) {
            msg = helper.getLanguage("Please select Start time");
            err_valid = !0
        } else if (!p.end_time) {
            msg = helper.getLanguage("Please select End time");
            err_valid = !0
        } else if (parseFloat(convert_value(p.start_time.getHours()+":"+p.start_time.getMinutes())) >= parseFloat(convert_value(p.end_time.getHours()+":"+p.end_time.getMinutes()))) {
            msg = helper.getLanguage("End time should be bigger then Start time");
            err_valid = !0
        } else if (parseFloat(convert_value(p['break'])) >= (parseFloat(convert_value(p.end_time.getHours()+":"+p.end_time.getMinutes())) - parseFloat(convert_value(p.start_time.getHours()+":"+p.start_time.getMinutes())))) {
            msg = helper.getLanguage("Break is bigger then the hours");
            err_valid = !0
        } else if (!p.service_date) {
            msg = helper.getLanguage("Please select a day");
            err_valid = !0
        }
        if (err_valid) {
            var obj = {
                "error": {
                    "error": helper.getLanguage(msg)
                },
                "notice": !1,
                "success": !1
            };
            $scope.showAlerts(obj);
            return !1
        }
        return !0
    }
    $scope.addExpense = function(d) {
        $scope.showTxt.fillTime = !1
        d.do = 'maintenance--maintenance-add_exp';
        helper.doRequest('post', 'index.php', d, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.exps = r.data;
                $scope.service_expense = {
                    service_id: $scope.service.service_id,
                    service_date: new Date(),
                    user_id: $scope.luser
                }
            }
        })
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.link = function() {
        $state.go('service.view', {
            'service_id': $scope.service.service_id
        });
        $uibModalInstance.dismiss('cancel')
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
    $scope.installationInfo = function() {
        $scope.instal_data = {};
        $scope.site_data = {};
        $scope.sitemap = {};
        $scope.showInstData = !1;
        $scope.showTxt.site = !$scope.showTxt.site;
        if (typePromise) {
            $timeout.cancel(typePromise)
        }
        typePromise = $timeout(function() {
            if ($scope.service.installation_id != '0' && $scope.service.installation_id != undefined && $scope.showTxt.site) {
                $scope.showInstLoad = !0;
                helper.doRequest('get', 'index.php', {
                    'do': 'installation-ninstallation',
                    'installation_id': $scope.service.installation_id
                }, function(o) {
                    $scope.instal_data = o.data;
                    var obj = angular.copy(o.data.s3_info);
                    obj.do = 'misc-s3_link';
                    helper.doRequest('post', 'index.php', obj, function(s) {
                        $scope.instal_data.dropbox = s.data;
                        $scope.sitemap = {
                            center: angular.copy(o.data.coords),
                            zoom: 12,
                            marker_id: 1,
                            refresh: !0,
                            marker_coord: o.data.coords
                        };
                        helper.doRequest('get', 'index.php', {
                            'do': 'customers-sites',
                            'customer_id': $scope.service.customer_id,
                            'address_id': o.data.address_id,
                            view: 1
                        }, function(d) {
                            $scope.site_data = d.data;
                            $scope.showInstLoad = !1;
                            $scope.showInstData = !0
                        })
                    })
                })
            }
        }, 700)
    }
    $scope.openLightboxModal = function(index, object) {
        Lightbox.openModal($scope[object].dropbox.dropbox_images, index)
    }
}]).controller('ArtDeliveryModalCtrl', ['$scope', '$compile', '$timeout', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, $compile, $timeout, helper, $uibModalInstance, data, modalFc) {
    var art = this;
    art.obj = data;
    art.hideDetails = !0;
    art.obj.do = art.obj.delivery_id ? (art.obj.xview ? 'maintenance-service-maintenance-edit_delivery' : 'maintenance--maintenance-edit_delivery') : 'maintenance-service-maintenance-update_article_delivered';
    art.obj.hour = art.obj.delivery_id ? data.hour : "12";
    art.obj.minute = art.obj.delivery_id ? data.minute : "00";
    art.obj.pm = art.obj.delivery_id ? data.pm : 'pm';
    art.obj.date_del_line = art.obj.delivery_id ? new Date(data.delivery_date_js) : new Date();
    art.obj.contact_id = art.obj.delivery_id ? data.contact_id : undefined;
    art.obj.contact_name = art.obj.delivery_id ? data.contact_name : '';
    art.disableAddress = !0;
    art.listStats = [];
    art.listStats2 = [];
    art.format = 'dd/MM/yyyy';
    art.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    art.datePickOpen = {
        date_del_line: !1
    }
    art.openDate = function(p) {
        art.datePickOpen[p] = !0
    };
    art.hours = [12, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11];
    art.minutes = ["00", 15, 30, 45];
    art.contactAutoCfg = {
        valueField: 'recipient_id',
        labelField: 'recipient_name',
        searchField: ['recipient_name'],
        delimiter: '|',
        placeholder: 'Select contact',
        create: !0,
        maxItems: 1,
        onItemAdd(value, $item) {
            art.obj.contact_name = $item.html()
        },
        onItemRemove(value) {
            art.obj.contact_name = ''
        }
    };
    art.disabledt = function() {
        art.disableAddress = !art.disableAddress;
        return !1
    }
    art.showAlerts = function(d) {
        helper.showAlerts(art, d)
    }
    art.cancel = function() {
        $uibModalInstance.close()
    }
    if (!art.obj.delivery_id) {
        for (x in art.obj.lines) {
            art.obj.lines[x].disp_addres_info[0].quantity_delivered = art.obj.lines[x].quantity
        }
    }
    art.showStats = function(item, $index) {
        var add = !1;
        if (!art.listStats[$index]) {
            art.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (add == !0) {
            art.listStats[$index] = item.disp_addres_info;
            angular.element('.deliveryTable tbody tr.collapseTr').eq($index).after('<tr class="versionInfo"><td colspan="7" class="no_border">' + '<table class="table_minimal">' + '<tbody>' + '<tr ng-repeat="item2 in art.listStats[' + $index + ']">' + '<td>' + '<div class="row" >' + '<div class="col-xs-3"><b>{{::item2.customer_name}}</b></div>' + '<div class="col-xs-3">' + '<div>{{::item2.order_buyer_address}}</div>' + '<div>{{::item2.order_buyer_zip}} {{::item2.order_buyer_city}}</div>' + '<div>{{::item2.order_buyer_country}}</div></div>' + '<div class="col-xs-3" ng-class="{ \'is-error\':item2.is_error }"><input type="text" class="form-control" display-nr="" ng-model="item2.quantity_delivered" ng-change="art.changeSubQ(' + $index + ',item2)"><i class="fa fa-exclamation-circle" ng-if="item2.is_error"></i></div>' + '<div class="col-xs-3">In Stock: <b>{{::item2.quantity}}</b></div>' + '</div>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>');
            $compile(angular.element('.deliveryTable tbody tr.collapseTr').eq($index).next())($scope);
            modalFc.setHeight(angular.element('.modal.in'))
        } else {
            angular.element('.deliveryTable tbody tr.collapseTr').eq($index).next().toggleClass('hidden');
            modalFc.setHeight(angular.element('.modal.in'))
        }
        if (item.isopen == !0) {
            var idx = 0;
            for (x in art.obj.location_dd) {
                if (art.obj.location_dd[x].id == art.obj.select_main_location) {
                    idx = x
                }
            }
            var q = parseFloat(item.quantity_value) > parseFloat(art.listStats[$index][idx].qty) ? art.listStats[$index][idx].quantity : item.quantity;
            art.listStats[$index][idx].quantity_delivered = q
        }
    }
    art.showStats2 = function(item, $index) {
        if (item.locations.length > 0) {
            art.listStats2[$index] = item;
            angular.element('.deliveryTable tbody tr.collapseTr').eq($index).after('<tr class="versionInfo" ng-if="art.listStats2[' + $index + '].view_location"><td colspan="5" class="no_border">' + '<table class="table_minimal">' + '<tbody>' + '<tr ng-repeat="item2 in art.listStats2[' + $index + '].locations">' + '<td>' + '<div class="row" >' + '<div class="col-xs-9"><div><b>{{::item2.depo}}</b> </div>' + '<div>{{::item2.address}}</div>' + '<div>{{::item2.zip}} {{::item2.city}}</div>' + '<div>{{::item2.country}}</div>' + '</div>' + '<div class="col-xs-3">' + '<div>{{::item2.quantity_loc}}</div>' + '</div>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>');
            $compile(angular.element('.deliveryTable tbody tr.collapseTr').eq($index).next())($scope);
            modalFc.setHeight(angular.element('.modal.in'))
        }
    }
    art.changeQuantity = function(i) {
        var idx = 0;
        i.is_error = !1;
        if (art.obj.allow_stock) {
            i.quantity_value = helper.return_value(i.quantity) * 1;
            if (i.quantity_value > i.quantity_value2) {
                i.is_error = !0
            }
            if (i.is_reserved) {
                if (i.quantity_value < i.quantity_value2) {
                    var RepQ = i.quantity_value;
                    for (x in i.disp_addres_info) {
                        i.disp_addres_info[x].quantity_delivered = helper.displayNr(0);
                        i.disp_addres_info[x].is_error = !1;
                        if (helper.return_value(i.disp_addres_info[x].quantity_reserved) * 1 > 0 && RepQ > 0) {
                            if (helper.return_value(i.disp_addres_info[x].quantity_reserved) * 1 < RepQ) {
                                i.disp_addres_info[x].quantity_delivered = i.disp_addres_info[x].quantity_reserved;
                                RepQ = RepQ - helper.return_value(i.disp_addres_info[x].quantity_reserved) * 1
                            } else {
                                i.disp_addres_info[x].quantity_delivered = RepQ;
                                RepQ = 0
                            }
                        }
                    }
                } else {
                    for (x in i.disp_addres_info) {
                        i.disp_addres_info[x].quantity_delivered = i.disp_addres_info[x].quantity_reserved
                    }
                }
            } else {
                for (x in art.obj.location_dd) {
                    if (art.obj.location_dd[x].id == art.obj.select_main_location) {
                        idx = x
                    }
                }
                for (x in i.disp_addres_info) {
                    i.disp_addres_info[x].quantity_delivered = helper.displayNr(0);
                    i.disp_addres_info[x].is_error = !1;
                    if (x == idx) {
                        i.disp_addres_info[x].quantity_delivered = helper.displayNr(helper.return_value(i.quantity));
                        if (i.quantity * 1 > i.disp_addres_info[x].qty * 1) {
                            i.is_error = !0;
                            i.disp_addres_info[x].is_error = !0
                        }
                    }
                }
            }
        }
    }

    function recalculateQuantity(index) {
        var initQ = 0;
        var err = 0;
        for (x in art.obj.lines[index].disp_addres_info) {
            initQ += parseFloat(helper.return_value(art.obj.lines[index].disp_addres_info[x].quantity_delivered), 10);
            if (art.obj.lines[index].disp_addres_info[x].is_error) {
                err++
            }
        }
        art.obj.lines[index].quantity = helper.displayNr(initQ);
        if (err > 0) {
            art.obj.lines[index].is_error = !0
        }
    }
    art.changeSubQ = function(index, item) {
        if (helper.return_value(item.quantity_delivered) >= 0 && helper.return_value(item.quantity_delivered) > helper.return_value(item.quantity)) {
            item.is_error = !0;
            art.obj.lines[index].is_error = !0
        } else {
            item.is_error = !1;
            art.obj.lines[index].is_error = !1
        }
        recalculateQuantity(index)
    }
    art.changeLocation = function() {
        var line = 0;
        for (x in art.obj.location_dd) {
            if (art.obj.location_dd[x].id == art.obj.select_main_location) {
                idx = x
            }
        }
        for (y in art.obj.lines) {
            var i = art.obj.lines[y];
            i.is_error = !1;
            for (x in i.disp_addres_info) {
                i.disp_addres_info[x].quantity_delivered = helper.displayNr(0);
                i.disp_addres_info[x].is_error = !1;
                if (x == idx) {
                    i.disp_addres_info[x].quantity_delivered = i.quantity;
                    if (helper.return_value(i.quantity) * 1 > i.disp_addres_info[x].qty * 1) {
                        i.is_error = !0;
                        i.disp_addres_info[x].is_error = !0;
                        i.quantity = helper.displayNr(i.disp_addres_info[x].qty);
                        i.disp_addres_info[x].quantity_delivered = helper.displayNr(i.disp_addres_info[x].qty)
                    } else {
                        i.disp_addres_info[x].quantity_delivered = i.quantity2;
                        i.quantity = i.quantity2;
                        if (helper.return_value(i.quantity2) * 1 > i.disp_addres_info[x].qty * 1) {
                            i.is_error = !0;
                            i.disp_addres_info[x].is_error = !0;
                            i.quantity = helper.displayNr(i.disp_addres_info[x].qty);
                            i.disp_addres_info[x].quantity_delivered = helper.displayNr(i.disp_addres_info[x].qty)
                        }
                    }
                }
            }
        }
    }
    art.ok = function() {
        var obj = angular.copy(art.obj);
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            art.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
    art.showSerials = function(item, index) {
        item.index = index;
        openModal("serial_no", item, "lg")
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'serial_no':
                var iTemplate = 'SerialN';
                var ctas = 'srl';
                break
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            size: size,
            ctrlAs: ctas,
            template: 'template/' + iTemplate + 'Modal',
            backdrop: 'static'
        };
        switch (type) {
            case 'serial_no':
                params.params = {
                    'do': 'maintenance-select_serial_no',
                    'a_id': item.a_id,
                    'count_selected_s_n': item.count_selected_s_n,
                    'selected_s_n': item.selected_s_n
                };
                params.callback = function(data) {
                    art.obj.lines[item.index].selected_s_n = data
                }
                break
        }
        modalFc.open(params)
    }
}]).controller('ArtReservationModalCtrl', ['$scope', '$compile', '$timeout', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, $compile, $timeout, helper, $uibModalInstance, data, modalFc) {
    var art = this;
    art.obj = data;
    art.hideDetails = !0;
    art.obj.do = art.obj.reservation_id ? 'maintenance--maintenance-edit_stock_reservation' : 'maintenance-service-maintenance-update_article_reserved';
    art.obj.date_del_line = art.obj.reservation_id ? new Date(data.delivery_date_js) : new Date();
    art.obj.contact_id = art.obj.reservation_id ? data.contact_id : undefined;
    art.obj.contact_name = art.obj.reservation_id ? data.contact_name : '';
    art.disableAddress = !0;
    art.listStats = [];
    art.listStats2 = [];
    art.format = 'dd/MM/yyyy';
    art.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    art.datePickOpen = {
        date_del_line: !1
    }
    art.openDate = function(p) {
        art.datePickOpen[p] = !0
    };
    art.hours = [12, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11];
    art.minutes = ["00", 15, 30, 45];
    art.contactAutoCfg = {
        valueField: 'recipient_id',
        labelField: 'recipient_name',
        searchField: ['recipient_name'],
        delimiter: '|',
        placeholder: 'Select contact',
        create: !0,
        maxItems: 1,
        onItemAdd(value, $item) {
            art.obj.contact_name = $item.html()
        },
        onItemRemove(value) {
            art.obj.contact_name = ''
        }
    };
    art.disabledt = function() {
        art.disableAddress = !art.disableAddress;
        return !1
    }
    art.showAlerts = function(d) {
        helper.showAlerts(art, d)
    }
    art.cancel = function() {
        $uibModalInstance.close()
    }
    if (!art.obj.reservation_id) {
        for (x in art.obj.lines) {
            art.obj.lines[x].disp_addres_info[0].quantity_reserved = art.obj.lines[x].quantity
        }
    }
    art.showStats = function(item, $index) {
        var add = !1;
        if (!art.listStats[$index]) {
            art.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (add == !0) {
            art.listStats[$index] = item.disp_addres_info;
            angular.element('.deliveryTable tbody tr.collapseTr').eq($index).after('<tr class="versionInfo"><td colspan="7" class="no_border">' + '<table class="table_minimal">' + '<tbody>' + '<tr ng-repeat="item2 in art.listStats[' + $index + ']">' + '<td>' + '<div class="row" >' + '<div class="col-xs-3"><b>{{::item2.customer_name}}</b></div>' + '<div class="col-xs-3">' + '<div>{{::item2.order_buyer_address}}</div>' + '<div>{{::item2.order_buyer_zip}} {{::item2.order_buyer_city}}</div>' + '<div>{{::item2.order_buyer_country}}</div></div>' + '<div class="col-xs-3" ng-class="{ \'is-error\':item2.is_error }"><input type="text" class="form-control" display-nr="" ng-model="item2.quantity_reserved" ng-change="art.changeSubQ(' + $index + ',item2)"><i class="fa fa-exclamation-circle" ng-if="item2.is_error"></i></div>' + '<div class="col-xs-3">In Stock: <b>{{::item2.quantity}}</b></div>' + '</div>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>');
            $compile(angular.element('.deliveryTable tbody tr.collapseTr').eq($index).next())($scope);
            modalFc.setHeight(angular.element('.modal.in'))
        } else {
            angular.element('.deliveryTable tbody tr.collapseTr').eq($index).next().toggleClass('hidden');
            modalFc.setHeight(angular.element('.modal.in'))
        }
        if (item.isopen == !0) {
            var idx = 0;
            for (x in art.obj.location_dd) {
                if (art.obj.location_dd[x].id == art.obj.select_main_location) {
                    idx = x
                }
            }
            var q = parseFloat(item.quantity_value) > parseFloat(art.listStats[$index][idx].qty) ? art.listStats[$index][idx].quantity : item.quantity;
            art.listStats[$index][idx].quantity_reserved = q
        }
    }
    art.showStats2 = function(item, $index) {
        if (item.locations.length > 0) {
            art.listStats2[$index] = item;
            angular.element('.deliveryTable tbody tr.collapseTr').eq($index).after('<tr class="versionInfo" ng-if="art.listStats2[' + $index + '].view_location"><td colspan="5" class="no_border">' + '<table class="table_minimal">' + '<tbody>' + '<tr ng-repeat="item2 in art.listStats2[' + $index + '].locations">' + '<td>' + '<div class="row" >' + '<div class="col-xs-9"><div><b>{{::item2.depo}}</b> </div>' + '<div>{{::item2.address}}</div>' + '<div>{{::item2.zip}} {{::item2.city}}</div>' + '<div>{{::item2.country}}</div>' + '</div>' + '<div class="col-xs-3">' + '<div>{{::item2.quantity_loc}}</div>' + '</div>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>');
            $compile(angular.element('.deliveryTable tbody tr.collapseTr').eq($index).next())($scope);
            modalFc.setHeight(angular.element('.modal.in'))
        }
    }
    art.changeQuantity = function(i) {
        var idx = 0;
        i.is_error = !1;
        if (art.obj.allow_stock) {
            i.quantity_value = helper.return_value(i.quantity) * 1;
            if (i.quantity_value > i.quantity_value2) {
                i.is_error = !0
            }
            for (x in art.obj.location_dd) {
                if (art.obj.location_dd[x].id == art.obj.select_main_location) {
                    idx = x
                }
            }
            for (x in i.disp_addres_info) {
                i.disp_addres_info[x].quantity_reserved = helper.displayNr(0);
                i.disp_addres_info[x].is_error = !1;
                if (x == idx) {
                    i.disp_addres_info[x].quantity_reserved = helper.displayNr(helper.return_value(i.quantity));
                    if (helper.return_value(i.quantity) * 1 > i.disp_addres_info[x].qty * 1) {
                        i.is_error = !0;
                        i.disp_addres_info[x].is_error = !0
                    }
                }
            }
        }
    }

    function recalculateQuantity(index, i) {
        var initQ = 0;
        var err = 0;
        for (x in art.obj.lines[index].disp_addres_info) {
            initQ += parseFloat(helper.return_value(art.obj.lines[index].disp_addres_info[x].quantity_reserved), 10);
            if (art.obj.lines[index].disp_addres_info[x].is_error) {
                err++
            }
        }
        art.obj.lines[index].is_error = !0;
        if (initQ == helper.return_value(art.obj.lines[index].quantity) * 1 && initQ <= helper.return_value(art.obj.lines[index].quantity2) * 1) {
            art.obj.lines[index].is_error = !1
        }
        i.is_error = !1;
        if (helper.return_value(i.quantity_delivered) * 1 > i.qty * 1) {
            i.is_error = !0
        }
    }
    art.changeSubQ = function(index, item) {
        if (helper.return_value(item.quantity_reserved) >= 0 && parseFloat(helper.return_value(item.quantity_reserved), 100) > parseFloat(helper.return_value(item.quantity), 100)) {
            item.is_error = !0;
            art.obj.lines[index].is_error = !0
        } else {
            item.is_error = !1;
            art.obj.lines[index].is_error = !1
        }
        recalculateQuantity(index, item)
    }
    art.changeLocation = function() {
        var line = 0;
        for (x in art.obj.location_dd) {
            if (art.obj.location_dd[x].id == art.obj.select_main_location) {
                idx = x
            }
        }
        for (y in art.obj.lines) {
            var i = art.obj.lines[y];
            i.is_error = !1;
            for (x in i.disp_addres_info) {
                i.disp_addres_info[x].quantity_reserved = helper.displayNr(0);
                i.disp_addres_info[x].is_error = !1;
                if (x == idx) {
                    i.disp_addres_info[x].quantity_reserved = i.quantity;
                    if (helper.return_value(i.quantity) * 1 > i.disp_addres_info[x].qty * 1) {
                        i.is_error = !0;
                        i.disp_addres_info[x].is_error = !0;
                        i.quantity = helper.displayNr(i.disp_addres_info[x].qty);
                        i.disp_addres_info[x].quantity_reserved = helper.displayNr(i.disp_addres_info[x].qty)
                    } else {
                        i.disp_addres_info[x].quantity_reserved = i.quantity2;
                        i.quantity = i.quantity2;
                        if (helper.return_value(i.quantity2) * 1 > i.disp_addres_info[x].qty * 1) {
                            i.is_error = !0;
                            i.disp_addres_info[x].is_error = !0;
                            i.quantity = helper.displayNr(i.disp_addres_info[x].qty);
                            i.disp_addres_info[x].quantity_reserved = helper.displayNr(i.disp_addres_info[x].qty)
                        }
                    }
                }
            }
        }
    }
    art.ok = function() {
        var obj = angular.copy(art.obj);
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            art.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
    art.showSerials = function(item, index) {
        item.index = index;
        openModal("serial_no", item, "lg")
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'serial_no':
                var iTemplate = 'SerialN';
                var ctas = 'srl';
                break
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            size: size,
            ctrlAs: ctas,
            template: 'template/' + iTemplate + 'Modal',
            backdrop: 'static'
        };
        switch (type) {
            case 'serial_no':
                params.params = {
                    'do': 'maintenance-select_serial_no',
                    'a_id': item.a_id,
                    'count_selected_s_n': item.count_selected_s_n,
                    'selected_s_n': item.selected_s_n
                };
                params.callback = function(data) {
                    art.obj.lines[item.index].selected_s_n = data
                }
                break
        }
        modalFc.open(params)
    }
}]).controller('SerialNModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var srl = this;
    srl.obj = data;
    srl.showAlerts = function(d) {
        helper.showAlerts(srl, d)
    }
    srl.cancel = function() {
        $uibModalInstance.dismiss()
    }
    srl.goRight = function(item, index) {
        srl.obj.sel_serial_row.push(item);
        srl.obj.serial_row.splice(index, 1);
        srl.obj.sel_s_n++
    }
    srl.goLeft = function(item, index) {
        srl.obj.serial_row.push(item);
        srl.obj.sel_serial_row.splice(index, 1);
        srl.obj.sel_s_n--
    }
    srl.ok = function() {
        var serials = '';
        for (x in srl.obj.sel_serial_row) {
            serials = serials + srl.obj.sel_serial_row[x].serial_nr_id + ','
        }
        $uibModalInstance.close(serials)
    }
}]).controller('ServiceAddStepsModalCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'data', 'selectizeFc', function($scope, $timeout, helper, $uibModalInstance, data, selectizeFc) {
    var add = this;
    add.obj = data;
    add.obj.initial_start = !1;
    add.search = {
        'do': 'maintenance-service',
        'xget': 'planRecurring',
        'rec_start_date': undefined,
        'rec_end_date': undefined,
        'rec_number': undefined,
        'rec_frequency': add.obj.item.rec_frequency,
        'rec_days': add.obj.item.rec_days,
        'start_date': undefined,
        'activeType': '1',
        'team_id': undefined,
        'planning_dates': undefined,
        'planning_users': undefined
    };
    add.types = [{
        name: 'Weekly',
        id: '1',
        active: 'active'
    }, {
        name: 'Monthly',
        id: '2',
        active: ''
    }];
    add.planning = {};
    add.step = 1;
    add.close_btn = helper.getLanguage('Cancel');
    add.showAlerts = function(d) {
        helper.showAlerts(add, d)
    }
    add.close_modal = function() {
        $uibModalInstance.dismiss()
    }
    add.format = 'dd/MM/yyyy';
    add.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    add.datePickOpen = {
        rec_start_date: !1,
        rec_end_date: !1,
    }
    add.renderPlanning = function(d) {
        if (d) {
            add.planning = d.data;
            add.search.start_date = new Date(d.data.start_date);
            add.search.team_id = d.data.team_id;
            add.search.planning_dates = d.data.planning_dates;
            add.search.planning_users = d.data.planning_users
        }
    }
    add.searchThing = function() {
        var d = angular.copy(add.search);
        d.rec_start_date = d.rec_start_date.toUTCString();
        if (d.rec_end_date) {
            d.rec_end_date = d.rec_end_date.toUTCString()
        }
        if (d.start_date) {
            d.start_date = d.start_date.toUTCString()
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', d, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            add.renderPlanning(r)
        })
    }
    add.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            add.search.rec_frequency = add.obj.item.rec_frequency;
            add.search.rec_days = add.obj.item.rec_frequency == '5' ? add.obj.item.rec_days : undefined
        }
    });
    add.teamAutoCfg = {
        valueField: 'team_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('A Team'),
        create: !1,
        onChange(value) {
            if (value) {
                add.searchThing()
            }
        },
        maxItems: 1
    };
    add.typeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'Overview',
        create: !1,
        onChange(value) {
            if (value) {
                add.searchThing()
            }
        },
        maxItems: 1
    };
    add.cancel = function() {
        if (add.step == 1) {
            $uibModalInstance.dismiss()
        } else {
            if (add.obj.item.contract_id && add.obj.item.service_ctype == 2) {
                add.step = 1;
                add.close_btn = helper.getLanguage('Cancel')
            } else {
                add.step--;
                if (add.step == 1) {
                    add.close_btn = helper.getLanguage('Cancel')
                } else {
                    if (add.step == 2) {
                        add.ok_btn = helper.getLanguage("Next Step")
                    } else if (add.step == 4) {
                        angular.element('.modal-body').addClass('modal-body_alt');
                        add.search.start_date = undefined;
                        add.search.activeType = '1';
                        add.search.team_id = undefined;
                        add.search.planning_dates = undefined;
                        add.search.planning_users = undefined
                    } else {
                        angular.element('.modal-body').removeClass('modal-body_alt')
                    }
                }
            }
        }
    }
    add.set_type = function(type) {
        add.obj.item.service_type = type;
        if (!add.obj.item.contract_id) {
            if (type == '2') {
                add.obj.item.billable = !1
            } else {
                add.obj.item.billable = !0
            }
        }
        add.close_btn = helper.getLanguage("Previous Step");
        add.obj.item.billable_items = !1;
        if (add.obj.item.quote_id || (!add.obj.item.contract_id && add.obj.item.is_recurring)) {
            add.step = 2;
            if (!add.obj.item.contract_id && add.obj.item.is_recurring) {
                angular.element('.modal-body').addClass('modal-body_alt')
            }
        } else if (add.obj.item.contract_id) {
            add.step = 3
        } else {
            $uibModalInstance.close()
        }
    }
    add.set_ctype = function(type) {
        add.obj.item.service_ctype = type;
        add.step = 2;
        if (type != '0') {
            add.obj.item.billable = !1;
            if (type == '2') {
                add.obj.item.service_type = '2';
                add.step = 3
            }
        } else {
            add.obj.item.billable = !0
        }
        add.close_btn = helper.getLanguage("Previous Step")
    }
    add.setEnd = function(type) {
        add.search.rec_start_date = add.obj.item.rec_start_date;
        delete add.dateOptions.maxDate;
        if (type == '1') {
            add.obj.item.rec_end_date = undefined;
            add.search.rec_end_date = undefined;
            add.search.rec_number = add.obj.item.rec_number;
            add.obj.item.recurring_no_end = !1
        } else if (type == '2') {
            delete add.dateOptions.maxDate;
            if (add.obj.item.contract_has_end && add.obj.item.end_contract) {
                add.dateOptions.maxDate = new Date(add.obj.item.contract_end_date);
                add.obj.item.rec_end_date = new Date(add.obj.item.contract_end_date);
                add.search.rec_end_date = new Date(add.obj.item.contract_end_date)
            }
            add.obj.item.rec_number = undefined;
            add.search.rec_number = undefined
        } else if (type == '3') {
            add.obj.item.rec_end_date = undefined;
            add.search.rec_end_date = undefined;
            add.obj.item.rec_number = undefined;
            add.search.rec_number = undefined
        } else {
            add.obj.item.rec_number = undefined;
            add.search.rec_number = undefined;
            add.obj.item.recurring_no_end = !1;
            if (add.obj.item.contract_has_end && add.obj.item.end_contract && add.obj.item.rec_end_date.getTime() > add.obj.item.contract_end_date) {
                add.obj.item.rec_end_date = new Date(add.obj.item.contract_end_date);
                var error_msg = {
                    'error': {
                        'error': helper.getLanguage('End date greater than contract end date')
                    },
                    'success': !1,
                    'notice': !1
                };
                add.showAlerts(error_msg)
            } else {
                add.search.rec_end_date = add.obj.item.rec_end_date
            }
        }
    }
    add.navigate = function(i) {
        switch (add.search.activeType) {
            case '1':
                add.search.start_date.setDate(add.search.start_date.getDate() + (i * 7));
                break;
            case '2':
                add.search.start_date = add.search.start_date.addMonths(i);
                break
        }
        add.searchThing()
    }
    add.dropped = function(line, origin_tmsp, dest_tmsp) {
        for (x in add.search.planning_dates) {
            if (add.search.planning_dates[x] == origin_tmsp) {
                add.search.planning_dates[x] = dest_tmsp;
                break
            }
        }
        for (x in add.planning.user_data) {
            if (x == line) {
                continue
            }
            var source_el = angular.element('#master' + add.planning.user_data[x].user_id + '_' + origin_tmsp);
            var target_el = angular.element('#master' + add.planning.user_data[x].user_id + '_' + dest_tmsp);
            if (target_el.children().length == 1) {
                target_el.find('div:first-child').addClass('hidden')
            }
            if (source_el.children().length == 1) {
                source_el.find('div:first-child').appendTo(target_el)
            } else {
                source_el.find('div:nth-child(2)').appendTo(target_el);
                source_el.find('div:first-child').removeClass('hidden')
            }
        }
    }
    add.userPlan = function(item) {
        add.search.planning_users.push(item.user_id);
        item.added = !0
    }
    add.rmuserPlan = function(item) {
        for (x in add.search.planning_users) {
            if (add.search.planning_users[x] == item.user_id) {
                add.search.planning_users.splice(x, 1);
                item.added = !1;
                break
            }
        }
    }
    add.ok = function() {
        var t_nr = 0;
        var t_nr_line = 0;
        var art_nr = 0;
        var serv_nr = 0;
        var art_nr_line = 0;
        var proposed_price = 0;
        for (x in add.obj.item.quote_group_data) {
            t_nr_line++;
            proposed_price += add.obj.item.quote_group_data[x].total_free;
            if (add.obj.item.quote_group_data[x].checked) {
                t_nr++
            }
        }
        for (x in add.obj.item.quote_group_services) {
            if (add.obj.item.quote_group_services[x].checked) {
                serv_nr++
            }
        }
        for (x in add.obj.item.quote_group_articles) {
            art_nr_line++;
            if (add.obj.item.quote_group_articles[x].checked) {
                art_nr++
            }
        }
        if (t_nr == 0 && art_nr == 0 && serv_nr == 0 && add.obj.item.contract_id) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Select at least one task list,service list or article list")
                },
                "notice": !1,
                "success": !1
            };
            add.showAlerts(obj)
        } else {
            if (add.obj.item.is_recurring) {
                if ((add.step == 4 && add.obj.item.contract_id) || (add.step == 2 && !add.obj.item.contract_id)) {
                    var err = !1;
                    if (!add.obj.item.rec_start_date) {
                        var msg_err = helper.getLanguage("Start Date required");
                        err = !0
                    } else if (!add.obj.item.rec_frequency) {
                        var msg_err = helper.getLanguage("Frequency required");
                        err = !0
                    } else if (add.obj.item.rec_frequency == '5' && (add.obj.item.rec_days == '0' || isNaN(add.obj.item.rec_days))) {
                        var msg_err = helper.getLanguage("Days interval required");
                        err = !0
                    } else if (add.obj.item.rec_number && isNaN(add.obj.item.rec_number)) {
                        var msg_err = helper.getLanguage("End Date number required");
                        err = !0
                    } else if (!add.obj.item.rec_end_date && !add.obj.item.rec_number && !add.obj.item.end_contract && add.obj.item.contract_id) {
                        var msg_err = helper.getLanguage("End Date or Number of interventions required");
                        err = !0
                    } else if (!add.obj.item.rec_end_date && !add.obj.item.rec_number && !add.obj.item.recurring_no_end && !add.obj.item.contract_id && add.obj.item.is_recurring) {
                        var msg_err = helper.getLanguage("End Date or Number of interventions required");
                        err = !0
                    }
                    if (err) {
                        var obj = {
                            "error": {
                                "error": msg_err
                            },
                            "notice": !1,
                            "success": !1
                        };
                        add.showAlerts(obj)
                    } else {
                        $uibModalInstance.close()
                    }
                } else {
                    add.step++;
                    if (add.step == 4) {
                        angular.element('.modal-body').addClass('modal-body_alt')
                    }
                }
            } else {
                $uibModalInstance.close()
            }
        }
    }
}]).controller('ShowServiceTypeModalCtrl', ['$scope', '$uibModalInstance', 'helper', 'data', function($scope, $uibModalInstance, helper, data) {
    var add = this;
    add.obj = data;
    add.cancel = function() {
        $uibModalInstance.dismiss()
    }
    add.set_type = function(type) {
        var obj = {
            'do': 'maintenance-service-maintenance-updateServiceType',
            'service_id': add.obj.service_id,
            'service_type': type
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            $uibModalInstance.close(r)
        })
    }
}]).controller('ShowServiceHoursModalCtrl', ['$scope', '$uibModalInstance', '$compile', 'helper', 'data', function($scope, $uibModalInstance, $compile, helper, data) {
    var edit = this;
    edit.obj = data;
    edit.obj.date_js = new Date(edit.obj.tmsmp_js);
    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {
        service_date: !1
    }
    edit.showAlerts = function(d) {
        helper.showAlerts(edit, d)
    }
    edit.cancel = function() {
        $uibModalInstance.close()
    }
    edit.showTime = function(event, parent, model, location) {
        var _this = event.target;
        angular.element('body').find('timepicknew').remove();
        var compiledHTML = $compile('<timepicknew nparent="' + parent + '" nmodel="' + model + '""></timepicknew>')($scope);
        if (location == 1) {
            angular.element(_this).parent().after(compiledHTML)
        } else {
            angular.element(_this).after(compiledHTML)
        }
    }
    edit.ok = function() {
        var obj = angular.copy(edit.obj);
        obj.do = 'maintenance-service-maintenance-editServiceSheet';
        obj.start_time = convert_value(obj.start_time);
        obj.end_time = convert_value(obj.end_time);
        obj.break = convert_value(obj.break);
        obj.xget = 'serviceUser';
        obj.service_date == obj.tmsmp;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                edit.showAlerts(r)
            }
        })
    }
}]).controller('ShowServiceExpModalCtrl', ['$scope', '$uibModalInstance', 'helper', 'data', function($scope, $uibModalInstance, helper, data) {
    var add = this;
    add.obj = data;
    add.obj.service_date = new Date(add.obj.service_date);
    add.format = 'dd/MM/yyyy';
    add.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    add.datePickOpen = {
        date: !1
    }
    add.showAlerts = function(d) {
        helper.showAlerts(add, d)
    }
    add.cancel = function() {
        $uibModalInstance.close()
    }
    add.ok = function() {
        var obj = angular.copy(add.obj);
        var formdata = new FormData(),
            x = $('#attachement');
        obj.do = 'maintenance-service-maintenance-update_expense';
        obj.xget = 'serviceUser';
        obj.amount = obj.amount_txt;
        obj.expense_id = obj.EXP_ID;
        obj.id = obj.E_ID;
        obj.service_date = add.obj.service_date.getTime() / 1000;
        for (y in obj) {
            formdata.append(y, obj[y])
        }
        if (x[0].files.length > 0) {
            formdata.append("Filedata", x[0].files[0])
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', formdata, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                add.showAlerts(r)
            }
        })
    }
}]).controller('teamsCtrl', ['$scope', 'helper', 'list', 'data', function($scope, helper, list, data) {
    console.log('Services Teams');
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    $scope.search = {
        search: '',
        'do': 'maintenance-teams',
        view: $scope.activeView,
        xget: '',
        offset: 1
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr
        }
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.renderList(data)
}]).controller('teamCtrl', ['$scope', function($scope) {
    $scope.isAddPage = !1
}]).controller('teamEditCtrl', ['$scope', '$state','helper', '$stateParams', 'data', function($scope, $state, helper, $stateParams, data) {
    console.log('Team Edit');
    var edit = this,
        typePromise;
    edit.item = {};
    edit.team_id = $stateParams.team_id;
    edit.isAddPage = !0;
    edit.search = '';
    edit.renderPage = function(d) {
        if (d && d.data) {
            edit.item = d.data
        }
    }
    edit.showAlerts = function(d, formObj) {
        helper.showAlerts(edit, d, formObj)
    }
    edit.saveData = function(formObj) {
        var obj = angular.copy(edit.item);
        obj.do = obj.do_next;
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            edit.showAlerts(r, formObj);
            if (r.success) {
                //edit.item = r.data
                $state.go('teams');
            }
        })
    }
    edit.clearSearch = function() {
        edit.search = ''
    }
    edit.renderPage(data)
}]).controller('ArtDeliveriesModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, helper, $uibModalInstance, data, modalFc) {
    var art = this;
    art.obj = data;
    art.showAlerts = function(d) {
        helper.showAlerts(art, d)
    }
    art.cancel = function() {
        $uibModalInstance.close()
    }
    art.deleteDelivery = function(item) {
        var obj = {};
        obj.do = 'maintenance-article_deliveries-maintenance-delete_delivery';
        obj.delivery_id = item.delivery_id;
        obj.service_id = art.obj.service_id;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            art.showAlerts(r);
            if (r.success) {
                art.obj = r.data
            }
        })
    }
    art.showDelivery = function(item) {
        var obj = {
            'service_id': art.obj.service_id,
            'delivery_id': item.delivery_id
        };
        openModal("article_delivery", obj, "lg")
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'article_delivery':
                var iTemplate = 'ArtDelivery';
                var ctas = 'art';
                break
        }
        var params = {
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'article_delivery':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'maintenance-article_delivery',
                    'service_id': item.service_id,
                    'delivery_id': item.delivery_id
                };
                break
        }
        modalFc.open(params)
    }
}]).controller('rec_servicesCtrl', ['$scope', 'helper', 'list', 'data', function($scope, helper, list, data) {
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    $scope.views = [{
        name: helper.getLanguage('Active'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Archived'),
        id: -1
    }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'maintenance-rec_services',
        view: $scope.activeView,
        xget: '',
        offset: 1,
        archived: '',
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr
        }
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.archived = function(value) {
        $scope.search.archived = value;
        list.filter($scope, value, $scope.renderList)
    };
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    list.init($scope, 'maintenance-rec_services', $scope.renderList)
}]).controller('RecLastStepModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.search = {
        'do': 'maintenance-service',
        'xget': 'planRecurring',
        'rec_start_date': vm.obj.rec_start_date,
        'rec_end_date': undefined,
        'rec_number': vm.obj.rec_number,
        'rec_frequency': vm.obj.rec_frequency,
        'rec_days': vm.obj.rec_days,
        'start_date': vm.obj.rec_start_date,
        'activeType': '1',
        'team_id': undefined,
        'planning_dates': undefined,
        'planning_users': undefined
    };
    vm.types = [{
        name: 'Weekly',
        id: '1',
        active: 'active'
    }, {
        name: 'Monthly',
        id: '2',
        active: ''
    }];
    vm.planning = {};
    if (vm.obj.rec_end_date) {
        vm.search.rec_end_date = vm.obj.rec_end_date
    }
    vm.step = 1;
    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d)
    }
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.lastOpt = function(choice) {
        vm.obj['choice_' + choice] = !1
    }
    vm.renderPlanning = function(d) {
        if (d) {
            vm.planning = d.data;
            vm.search.start_date = new Date(d.data.start_date);
            vm.search.team_id = d.data.team_id;
            vm.search.planning_dates = d.data.planning_dates;
            vm.search.planning_users = d.data.planning_users
        }
    }
    vm.searchThing = function() {
        var d = angular.copy(vm.search);
        d.rec_start_date = d.rec_start_date.toUTCString();
        if (d.rec_end_date) {
            d.rec_end_date = d.rec_end_date.toUTCString()
        }
        if (d.start_date) {
            d.start_date = d.start_date.toUTCString()
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', d, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            vm.renderPlanning(r)
        })
    }
    vm.teamAutoCfg = {
        valueField: 'team_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('A Team'),
        create: !1,
        onChange(value) {
            if (value) {
                vm.searchThing()
            }
        },
        maxItems: 1
    };
    vm.typeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Overview'),
        create: !1,
        onChange(value) {
            if (value) {
                vm.searchThing()
            }
        },
        maxItems: 1
    };
    vm.navigate = function(i) {
        switch (vm.search.activeType) {
            case '1':
                vm.search.start_date.setDate(vm.search.start_date.getDate() + (i * 7));
                break;
            case '2':
                vm.search.start_date = vm.search.start_date.addMonths(i);
                break
        }
        vm.searchThing()
    }
    vm.dropped = function(line, origin_tmsp, dest_tmsp) {
        if (vm.search.rec_end_date && vm.obj.end_contract && vm.obj.contract_has_end) {
            if (dest_tmsp > vm.search.rec_end_date.getTime() / 1000) {
                var source_el = angular.element('#master' + vm.planning.user_data[line].user_id + '_' + origin_tmsp);
                var target_el = angular.element('#master' + vm.planning.user_data[line].user_id + '_' + dest_tmsp);
                var elem = angular.element('#master' + vm.planning.user_data[line].user_id + '_' + dest_tmsp).find('div:last-child');
                if (source_el.children().length) {
                    source_el.find('div:first-child').addClass('hidden')
                }
                elem.appendTo(source_el);
                if (target_el.children().length) {
                    target_el.find('div:first-child').removeClass('hidden')
                }
                var msg = {
                    'error': {
                        'error': helper.getLanguage('End date greater than contract end date')
                    },
                    'success': !1,
                    'notice': !1
                };
                vm.showAlerts(msg);
                return !1
            }
        }
        for (x in vm.search.planning_dates) {
            if (vm.search.planning_dates[x] == origin_tmsp) {
                vm.search.planning_dates[x] = dest_tmsp;
                break
            }
        }
        for (x in vm.planning.user_data) {
            if (x == line) {
                continue
            }
            var source_el = angular.element('#master' + vm.planning.user_data[x].user_id + '_' + origin_tmsp);
            var target_el = angular.element('#master' + vm.planning.user_data[x].user_id + '_' + dest_tmsp);
            if (target_el.children().length == 1) {
                target_el.find('div:first-child').addClass('hidden')
            }
            if (source_el.children().length == 1) {
                source_el.find('div:first-child').appendTo(target_el)
            } else {
                source_el.find('div:nth-child(2)').appendTo(target_el);
                source_el.find('div:first-child').removeClass('hidden')
            }
        }
    }
    vm.userPlan = function(item) {
        vm.search.planning_users.push(item.user_id);
        item.added = !0
    }
    vm.rmuserPlan = function(item) {
        for (x in vm.search.planning_users) {
            if (vm.search.planning_users[x] == item.user_id) {
                vm.search.planning_users.splice(x, 1);
                item.added = !1;
                break
            }
        }
    }
    vm.goBack = function() {
        vm.step = 1;
        angular.element('.modal-dialog').removeClass('modal-lg')
    }
    vm.getPlann = function() {
        vm.step = 2;
        angular.element('.modal-dialog').addClass('modal-lg');
        vm.searchThing()
    }
    vm.ok = function() {
        var data = angular.copy(vm.obj);
        data = helper.clearData(data, ['addresses', 'articles_list', 'autoSrvUsers', 'cc', 'contacts', 'expense', 'expenseCat', 'hours', 'language_dd2', 'recipients', 'user_auto']);
        data.do = 'maintenance-service-maintenance-editService';
        if (vm.step == 2) {
            data.planning_dates = vm.search.planning_dates;
            data.planning_users = vm.search.planning_users
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            vm.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('SavedPlannModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.planning = {};
    vm.planning = data;
    vm.search = {
        'do': 'maintenance-service',
        'xget': 'planRecurringSaved',
        'start_date': undefined,
        'rec_end_date': undefined,
        'activeType': '1',
        'team_id': undefined,
        'planning_dates': undefined,
        'planning_users': undefined
    };
    vm.types = [{
        name: 'Weekly',
        id: '1',
        active: 'active'
    }, {
        name: 'Monthly',
        id: '2',
        active: ''
    }];
    vm.search.service_id = data.service_id;
    vm.search.start_date = new Date(data.start_date);
    vm.search.rec_start_date = new Date(data.start_date);
    vm.search.team_id = data.team_id;
    vm.search.planning_dates = data.planning_dates;
    vm.search.planning_users = data.planning_users;
    vm.search.rec_first_day = data.rec_first_day;
    if (vm.planning.rec_end_date) {
        vm.search.rec_end_date = new Date(vm.planning.rec_end_date)
    }
    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d)
    }
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.renderPlanning = function(d) {
        if (d) {
            vm.planning = d.data;
            vm.search.start_date = new Date(d.data.start_date);
            vm.search.team_id = d.data.team_id;
            vm.search.planning_dates = d.data.planning_dates;
            vm.search.planning_users = d.data.planning_users
        }
    }
    vm.searchThing = function() {
        var d = angular.copy(vm.search);
        d.start_date = d.start_date.toUTCString();
        if (d.rec_end_date) {
            d.rec_end_date = d.rec_end_date.toUTCString()
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', d, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            vm.renderPlanning(r)
        })
    }
    vm.teamAutoCfg = {
        valueField: 'team_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('A Team'),
        create: !1,
        onChange(value) {
            if (value) {
                vm.searchThing()
            }
        },
        maxItems: 1
    };
    vm.typeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Overview'),
        create: !1,
        onChange(value) {
            if (value) {
                vm.searchThing()
            }
        },
        maxItems: 1
    };
    vm.navigate = function(i) {
        switch (vm.search.activeType) {
            case '1':
                vm.search.start_date.setDate(vm.search.start_date.getDate() + (i * 7));
                break;
            case '2':
                vm.search.start_date = vm.search.start_date.addMonths(i);
                break
        }
        vm.searchThing()
    }
    vm.dropped = function(line, origin_tmsp, dest_tmsp) {
        if (vm.search.rec_end_date && vm.planning.end_contract && vm.planning.contract_has_end) {
            if (dest_tmsp > vm.search.rec_end_date.getTime() / 1000) {
                var source_el = angular.element('#master' + vm.planning.user_data[line].user_id + '_' + origin_tmsp);
                var target_el = angular.element('#master' + vm.planning.user_data[line].user_id + '_' + dest_tmsp);
                var elem = angular.element('#master' + vm.planning.user_data[line].user_id + '_' + dest_tmsp).find('div:last-child');
                if (source_el.children().length) {
                    source_el.find('div:first-child').addClass('hidden')
                }
                elem.appendTo(source_el);
                if (target_el.children().length) {
                    target_el.find('div:first-child').removeClass('hidden')
                }
                var msg = {
                    'error': {
                        'error': helper.getLanguage('End date greater than contract end date')
                    },
                    'success': !1,
                    'notice': !1
                };
                vm.showAlerts(msg);
                return !1
            }
        }
        for (x in vm.search.planning_dates) {
            if (vm.search.planning_dates[x] == origin_tmsp) {
                vm.search.planning_dates[x] = dest_tmsp;
                break
            }
        }
        for (x in vm.planning.user_data) {
            if (x == line) {
                continue
            }
            var source_el = angular.element('#master' + vm.planning.user_data[x].user_id + '_' + origin_tmsp);
            var target_el = angular.element('#master' + vm.planning.user_data[x].user_id + '_' + dest_tmsp);
            if (target_el.children().length == 1) {
                target_el.find('div:first-child').addClass('hidden')
            }
            if (source_el.children().length == 1) {
                source_el.find('div:first-child').appendTo(target_el)
            } else {
                source_el.find('div:nth-child(2)').appendTo(target_el);
                source_el.find('div:first-child').removeClass('hidden')
            }
        }
    }
    vm.userPlan = function(item) {
        vm.search.planning_users.push(item.user_id);
        item.added = !0
    }
    vm.rmuserPlan = function(item) {
        for (x in vm.search.planning_users) {
            if (vm.search.planning_users[x] == item.user_id) {
                vm.search.planning_users.splice(x, 1);
                item.added = !1;
                break
            }
        }
    }
    vm.ok = function() {
        var obj = angular.copy(vm.planning);
        obj.do = 'maintenance-service-maintenance-changeRecurringPlan';
        obj.planning_dates = vm.search.planning_dates;
        obj.planning_users = vm.search.planning_users;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('serviceModelsCtrl', ['$scope', 'helper', 'list', 'selectizeFc', 'data', function($scope, helper, list, selectizeFc, data) {
    console.log('serviceModels');
    $scope.list = [];
    $scope.search = {
        search: '',
        'do': 'maintenance-serviceModels',
        offset: 1,
        'lang': ''
    };
    $scope.ord = '';
    $scope.reverse = !1;
    $scope.renderList = function(d) {
        if (d.data) {
            for (x in d.data) {
                $scope[x] = d.data[x]
            }
            helper.showAlerts($scope, d)
        }
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
        $scope.checkSearch()
    };
    $scope.checkSearch = function() {
        var data = angular.copy($scope.search),
            is_search = !1;
        data = helper.clearData(data, ['do', 'xget', 'showAdvanced', 'showList', 'offset']);
        for (x in data) {
            if (data[x]) {
                is_search = !0
            }
        }
        // vm.search.showList = is_search
    }
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.filterCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('All Languages'),
        onChange(value) {
            $scope.toggleSearchButtons('lang', value)
        }
    });
    // $scope.filterCfg = {
    //     valueField: 'id',
    //     labelField: 'name',
    //     searchField: ['name'],
    //     delimiter: '|',
    //     placeholder: helper.getLanguage('All Languages'),
    //     onChange(value) {
    //         if (value) {
    //             $scope.toggleSearchButtons($scope.lang, this.options[value].id)
    //         } else {
    //             $scope.toggleSearchButtons($scope.lang, '')
    //         }
    //     },
    //     maxItems: 1
    // };
    $scope.renderList(data)
}]).controller('serviceModelsModalCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'data', 'list', function($scope, $timeout, helper, $uibModalInstance, data, list) {
    var vmMod = this;
    vmMod.search = {
        search: '',
        'do': 'maintenance-service',
        xget: 'serviceModels',
        offset: 1,
        'lang': '',
        'service_id': data.service_id
    };
    for (x in data) {
        vmMod[x] = data[x];
        if (vmMod.lang) {
            vmMod.search.lang = vmMod.lang
        }
        if (vmMod.list.length < 10) {
            $timeout(function() {
                angular.element('.modal-body').addClass('modal-body_alt')
            })
        } else {
            $timeout(function() {
                angular.element('.modal-body').removeClass('modal-body_alt')
            })
        }
    }
    vmMod.templates = [];
    vmMod.cancel = cancel;
    vmMod.ok = ok;

    function cancel() {
        $uibModalInstance.close()
    }
    vmMod.renderList = function(d) {
        if (d.data) {
            for (x in d.data) {
                vmMod[x] = d.data[x];
                if (vmMod.lang) {
                    vmMod.search.lang = vmMod.lang
                }
                if (vmMod.list.length < 10) {
                    $timeout(function() {
                        angular.element('.modal-body').addClass('modal-body_alt')
                    })
                } else {
                    $timeout(function() {
                        angular.element('.modal-body').removeClass('modal-body_alt')
                    })
                }
            }
        }
    };
    vmMod.searchThing = function() {
        list.search(vmMod.search, vmMod.renderList, vmMod)
    }
    vmMod.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Languages'),
        onChange(value) {
            if (value) {
                vmMod.search.lang = this.options[value].id
            } else {
                vmMod.search.lang = ''
            }
            vmMod.searchThing()
        },
        maxItems: 1
    };

    function ok() {
        var obj = {};
        obj.list = angular.copy(vmMod.list);
        obj.service_id = vmMod.search.service_id;
        obj.do = 'maintenance-service-maintenance-serviceUpdateModel';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                helper.showAlerts(vmMod, r)
            }
        })
    }
    vmMod.checkModel = function(index) {
        if (vmMod.list[index].checked) {
            for (x in vmMod.list) {
                if (x != index) {
                    vmMod.list[x].checked = !1
                }
            }
        }
    }
}]).controller('serviceViewCtrl', ['$scope', '$state', '$compile', 'helper', '$stateParams', '$cacheFactory', '$timeout', '$uibModal', '$confirm', '$window', 'editItem', 'modalFc', 'selectizeFc', 'Lightbox', 'data', function($scope, $state, $compile, helper, $stateParams, $cacheFactory, $timeout, $uibModal, $confirm, $window, editItem, modalFc, selectizeFc, Lightbox, data) {
    console.log('View Service');
    var view = this,
        typePromise;
    view.item = {};
    view.app = 'maintenance';
    view.ctrlpag = 'service';
    view.tmpl = 'template';
    view.service_id = $stateParams.service_id;

    view.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    view.showTxt = {
        info: !0,
        address: !0,
        report: !0,
        plannedate: !0,
        fillSheet: !1,
        sendEmail: !1,
        sevice_settings: !1,
        addBtn: !0,
        address: !0,
        notes: !1,
        down_notice: !1,
        site: !1,
        drop: !1,
    };
    view.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    
    view.oldObj = {};
    view.format = 'dd/MM/yyyy';
    view.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    view.datePickOpen = {
        finishDate: !1,
        planeddate: !1,
        planeddate2: !1,
        startDate: !1,
        planning_date: !1,
        rec_start_date: !1
    }
    view.auto = {
        options: {
            customer: [],
            contact: [],
            user: [],
            userSrv: [],
            expenseCat: [],
            recipients: [],
            contacts: [],
            cc: [],
            articles: [],
        },
        disable: {
            customer: !0,
            contact: !0
        }
    };
    view.contactForEmailAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select recipient'),
        create: !0,
        createOnBlur:true,
        maxItems: 100
    });
    view.add = {
        staff: !0,
        subtask: !0,
        supplie: !0,
        article: !0,
    }
    view.service_sheet = {
        servicing_sheet_id: view.service_id,
        service_date: new Date(view.item.planeddate_js)
    };
    view.service_expense = {
        service_id: view.service_id,
        service_date: new Date()
    }
    view.email = {};
    view.yourModel = {
        menu: [
            ['bold', 'italic'],
            ['left-justify', 'center-justify', 'right-justify'],
            ['ordered-list', 'unordered-list', 'outdent', 'indent'],
            ['link', 'image', 'image-upload']
        ]
    };
    view.show_load = !0;
    view.openModal = openModal;
    editItem.init(view);
    view.renderPage = function(d) {
        if (d.data) {
            view.item = d.data;
            if(!d.data.item_exists){
                view.item.item_exists = false;
                return 0;
            }; 
            helper.updateTitle($state.current.name,view.item.serial_number);
            view.hoursFix();
            if (view.item.finishDate_ts_js) {
                view.item.finishDate_js = new Date(view.item.finishDate_ts_js)
            }
            if (view.item.planeddate_js) {
                view.item.planeddate_js = new Date(view.item.planeddate_js);
                view.service_sheet.service_date = new Date(view.item.planeddate_js);
            }
            if (view.item.planning && view.item.planning.planning_date) {
                view.item.planning.planning_date = new Date(view.item.planning.planning_date)
            }
            view.auto.options.user = d.data.user_auto;
            view.auto.options.userSrv = d.data.autoSrvUsers;
            view.auto.options.expenseCat = d.data.expenseCat;

            view.auto.options.recipients = d.data.recipients;
            view.item.email.recipients = d.data.email.recipients;
            view.item.email.sendgrid_selected = d.data.sendgrid_selected;
            view.auto.options.articles = d.data.articles_list.lines;
            view.service_sheet.sheet_user_id = d.data.loged_user ? d.data.loged_user : '';
            view.service_expense.user_id = d.data.loged_user ? d.data.loged_user : '';
            view.format = view.item.date_pick_format;
            view.auto.options.customer = [{
                id: view.item.buyer_id,
                value: view.item.buyer_name
            }];
            view.email = {
                e_subject: view.item.e_subject,
                e_message: view.item.e_message,
                email_language: view.item.email_language,
                include_pdf: view.item.include_pdf,
                service_id: view.service_id,
                use_html: view.item.use_html,
                recipients: view.item.email.recipients,
                sendgrid_selected: view.item.email.sendgrid_selected,
                user_loged_email: view.item.email.user_loged_email,
                user_acc_email: view.item.email.user_acc_email,
                copy: view.item.email.copy,
                copy_acc: view.item.email.copy_acc,
            };
            if (view.item.status == 0 || view.item.status == 1 || view.item.status == 2) {
                view.showS3()
            }
        }
        view.showAlerts(d)
    }
    view.hoursFix = function() {
        for (x in view.item.hours.hours) {
            view.item.hours.hours[x].date_js = new Date(view.item.hours.hours[x].tmsmp_js)
        }
    }
    view.showEdit = function(o, p) {
        if (view.showTxt[o] === !1) {
            var data = angular.copy(view.item);
            data.startdate = convert_value(data.startdate);
            data.duration = convert_value(data.duration);
            data.do = 'maintenance-service-maintenance-' + p;
            data.xview='1';
            helper.doRequest('post', 'index.php', data, view.renderPage)
        }
        view.showTxt[o] = !view.showTxt[o]
    }

    view.showS3 = function() {
        view.show_load = !0;
        var data = angular.copy(view.item.s3_info);
        data.do = 'misc-s3_link';
        helper.doRequest('post', 'index.php', data, function(r) {
            view.item.dropbox = r.data;
            view.show_load = !1
        })
    }
    view.changeStatus = function() {
        if (arguments.length >= 3) {
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', {
                'do': 'maintenance-service-maintenance-changeServiceStatus',
                'xview':'1',
                status: arguments[0],
                planeddate_js: arguments[1],
                startdate: convert_value(arguments[2]),
                duration: convert_value(arguments[3]),
                service_id: view.service_id,
                planeddate_ts: view.item.planeddate_ts,
            }, view.renderChange)
        } else {
            if (arguments[0] == 0) {
                var obj = {
                    "service_id": view.service_id,
                    'planeddate_ts': view.item.planeddate_ts
                };
                $confirm({
                    ok: helper.getLanguage('Yes'),
                    cancel: helper.getLanguage('No'),
                    text: helper.getLanguage('Do you want to reset planned date, time and duration?')
                }).then(function() {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('post', 'index.php', {
                        'do': 'maintenance-service-maintenance-changeServiceStatus',
                         'xview':'1',
                        status: arguments[0],
                        service_id: view.service_id,
                        planeddate_ts: view.item.planeddate_ts,
                        remove_dates: !0
                    }, view.renderChange)
                }, function() {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('post', 'index.php', {
                        'do': 'maintenance-service-maintenance-changeServiceStatus',
                         'xview':'1',
                        status: arguments[0],
                        service_id: view.service_id,
                        planeddate_ts: view.item.planeddate_ts
                    }, view.renderChange)
                });
                return !1
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', {
                'do': 'maintenance-service-maintenance-changeServiceStatus',
                 'xview':'1',
                status: arguments[0],
                service_id: view.service_id,
                planeddate_ts: view.item.planeddate_ts
            }, view.renderChange)
        }
    }
    view.renderChange = function(d) {
        if (d.success.success) {
            view.showTxt.plannedate = !0;
            view.item = d.data
        }
        if (d.data.planeddate) {
            view.item.planeddate = d.data.planeddate
        }
        if (d.data.planeddate_js) {
            d.data.planeddate_js = new Date(d.data.planeddate_js)
        }
        if (view.item.planning && view.item.planning.planning_date) {
            view.item.planning.planning_date = new Date(view.item.planning.planning_date)
        }
        if (view.item.status == 1 || view.item.status == 2) {
            view.showS3()
        }
        view.showAlerts(d)
    }
    view.openDate = function(p) {
        view.datePickOpen[p] = !0
    };
    view.changeAuto = function(d) {
        view.auto.disable[d] = !view.auto.disable[d]
    }
    view.showAdd = function(d) {
        if (view.add[d] === !1) {
            switch (d) {
                case "staff":
                    if (view.item.staff_id) {
                        if (view.item.status == '0') {
                            var s_date = convert_value(view.item.startdate);
                            var duration = convert_value(view.item.duration);
                            var data = {
                                service_id: view.service_id,
                                'do': 'maintenance-service-maintenance-addServiceUser',
                                'xview':'1',
                                user_id: view.item.staff_id,
                                name: view.item.staff_name,
                                free: !0,
                                planeddate_js: view.item.planeddate_js,
                                planning_date: view.item.planning.planning_date.toUTCString(),
                                startdate: s_date,
                                duration: duration
                            }
                        } else {
                            var data = {
                                service_id: view.service_id,
                                'do': 'maintenance-service-maintenance-addServiceUser',
                                'xview':'1',
                                user_id: view.item.staff_id,
                                name: view.item.staff_name
                            }
                        }
                    }
                    view.item.staff_name = '';
                    var func = view.renderUserPage;
                    break;
                case "subtask":
                    if (view.item.task_name) {
                        var data = {
                            service_id: view.service_id,
                            'do': 'maintenance-service-maintenance-serviceAddTask',
                            task_name: view.item.task_name,
                            xget: 'serviceSubTasks'
                        }
                    }
                    var func = view.renderSubTaskPage;
                    break;
                case "supplie":
                    if (view.item.supplie) {
                        var data = {
                            service_id: view.service_id,
                            'do': 'maintenance-service-maintenance-serviceAddSupply',
                            name: view.item.supplie,
                            xget: 'serviceSupplies'
                        }
                    }
                    var func = view.renderSuppliesPage;
                    break;
                default:
                    break
            }
            if (data) {
                helper.doRequest('post', 'index.php', data, func)
            }
        }
        view.add[d] = !view.add[d]
    }
    view.renderUserPage = function(d) {
        if (d.data) {
            view.renderPage(d)
        }
        view.item.staff_id = null;
        view.add.staff = !0;
        view.showAlerts(d)
    }
    view.renderSubTaskPage = function(d) {
        if (d.data) {
            view.item.tasks = d.data;
            view.item.task_name = '';
            view.item.add_subtask1 = !1;
            view.item.add_subtask2 = !1;
            view.add.subtask = !0
        }
        view.showAlerts(d)
    }
    view.renderSubServicePage = function(d) {
        if (d.data) {
            view.item.services = d.data;
            view.item.add_subtask1 = !1;
            view.item.add_subtask2 = !1
        }
        view.showAlerts(d)
    }
    view.renderSuppliesPage = function(d) {
        if (d.data) {
            view.showAlerts(d);
            if (d.success) {
                view.item.supplies = d.data;
                helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-service',
                    'xget': 'downpayment',
                    'service_id': view.item.service_id
                }, function(r) {
                    view.item.show_down = r.data.show_down
                })
            }
        }
    }
    view.renderArticlesPage = function(d) {
        if (d.data) {
            view.showAlerts(d);
            if (d.success) {
                view.item.articles = d.data;
                helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-service',
                    'xget': 'downpayment',
                    'service_id': view.item.service_id
                }, function(r) {
                    view.item.show_down = r.data.show_down
                })
            }
        }
    }
    view.editUser = function(d, type, $event) {
        switch (type) {
            case 'user':
                var data = {
                    service_id: view.service_id,
                    'do': 'maintenance--maintenance-serviceSetPrM',
                    'xview':'1',
                    user_id: d.id,
                    value: d.pr_m
                };
                if (view.item.status == 2) {
                    $event.preventDefault();
                    return !1
                }
                break;
            case 'task':
                var data = {
                    service_id: view.service_id,
                    'do': 'maintenance--maintenance-closeTask',
                    'xview':'1',
                    id: d.id,
                    value: d.checked
                };
                if (view.item.status == 2) {
                    $event.preventDefault();
                    return !1
                }
                break;
            case 'shropdf':
                var data = {
                    service_id: view.service_id,
                    'do': 'maintenance--maintenance-shropdf',
                    'xview':'1',
                    value: d
                };
                break;
            case 'quantity_art':
                var data = {
                    service_id: view.service_id,
                    'do': 'maintenance-service-maintenance-update_article_qty',
                    qty: d.quantity,
                    a_id: d.id,
                    xget: 'serviceArticles'
                };
                if (view.item.status == 2) {
                    $event.preventDefault();
                    return !1
                }
                break;
            case 'name_art':
                var data = {
                    service_id: view.service_id,
                    'do': 'maintenance-service-maintenance-update_article_name',
                    name: d.name,
                    a_id: d.id,
                    xget: 'serviceArticles'
                };
                if (view.item.status == 2) {
                    $event.preventDefault();
                    return !1
                }
                break;
            case 'task_art':
                var data = {
                    service_id: view.service_id,
                    'do': 'maintenance-service-maintenance-update_task_name',
                    name: d.name,
                    task_id: d.id,
                    xget: 'serviceArticles'
                };
                if (view.item.status == 2) {
                    $event.preventDefault();
                    return !1
                }
                break;
            case 'quantity_serv':
                var data = {
                    service_id: view.service_id,
                    'do': 'maintenance-service-maintenance-update_service_qty',
                    qty: d.quantity,
                    task_id: d.id,
                    xget: 'serviceSubServices'
                };
                if (view.item.status == 2) {
                    $event.preventDefault();
                    return !1
                }
                break;
            case 'quantity':
            case 'delivered':
                var data = {
                    service_id: view.service_id,
                    'do': 'maintenance--maintenance-serviceArticleUpdate',
                    'xview':'1',
                    value: d[type],
                    field: type,
                    article_id: d.id
                };
                if (view.item.status == 2) {
                    $event.preventDefault();
                    return !1
                }
                break;
            case 'sell_price':
                var data = {
                    service_id: view.service_id,
                    'do': 'maintenance--maintenance-serviceArtSellUpdate',
                    'xview':'1',
                    value: d[type],
                    field: type,
                    article_id: d.id
                };
                if (view.item.status == 2) {
                    $event.preventDefault();
                    return !1
                }
                break;
            default:
                break
        }
        if (data) {
            helper.doRequest('post', 'index.php', data, function(r) {
                view.showAlerts(r);
                if (r.in.minus_stock) {
                    var msg = "<b>" + helper.getLanguage("Selected quantity exceeds article stock of") + " <span class='text-danger'>" + r.in.stock + "</span></b>";
                    var cont = {
                        "e_message": msg
                    };
                    openModal("stock_info", cont, "md")
                }
                if (r.data.one_manager) {
                    for (x in view.item.users.users) {
                        if (view.item.users.users[x].id == r.data.user_id) {
                            view.item.users.users[x].pr_m = !0
                        }
                    }
                }
                if (r.in.qty_done) {
                    view.item.articles = r.data
                }
                if (r.in.qty) {
                    d[type] = r.in.qty
                }
            })
        }
    }
    view.showAlerts = function(d) {
        helper.showAlerts(view, d)
    }
    view.delete = function(type, d) {
        switch (type) {
            case 'user':
                if (view.item.status == '0') {
                    var s_date = convert_value(view.item.startdate);
                    var duration = convert_value(view.item.duration);
                    var data = {
                        service_id: view.service_id,
                        'do': 'maintenance-service-maintenance-deleteServiceUser',
                        'xview':'1',
                        user_id: d.id,
                        u_id: d.user_id,
                        free: !0,
                        planeddate_js: view.item.planeddate_js,
                        planning_date: view.item.planning.planning_date.toUTCString(),
                        startdate: s_date,
                        duration: duration
                    }
                } else {
                    var data = {
                        service_id: view.service_id,
                        'do': 'maintenance-service-maintenance-deleteServiceUser',
                        'xview':'1',
                        user_id: d.id,
                        u_id: d.user_id
                    }
                }
                var func = view.renderUserPage;
                break;
            case 'task':
                var data = {
                    service_id: view.service_id,
                    'do': 'maintenance-service-maintenance-deleteServiceTask',
                    task_id: d.id,
                    xget: 'serviceSubTasks'
                };
                var func = view.renderSubTaskPage;
                break;
            case 'service':
                var data = {
                    service_id: view.service_id,
                    'do': 'maintenance-service-maintenance-deleteServiceTask',
                    task_id: d.id,
                    xget: 'serviceSubServices'
                };
                var func = view.renderSubServicePage;
                break;
            case 'supplie':
                var data = {
                    service_id: view.service_id,
                    'do': 'maintenance-service-maintenance-deleteServiceSupply',
                    a_id: d.id,
                    xget: 'serviceSupplies'
                };
                var func = view.renderSuppliesPage;
                break;
            case 'article':
                var data = {
                    service_id: view.service_id,
                    'do': 'maintenance-service-maintenance-deleteServiceArticle',
                    a_id: d.id,
                    article_id: d.article_id,
                    xget: 'serviceArticles'
                };
                var func = view.renderArticlesPage;
            default:
                break
        }
        if (data) {
            helper.doRequest('post', 'index.php', data, func)
        }
    }
    view.changeDraftStatus = function() {
        openModal("service_plan", angular.copy(view.item), "lg");
    }
    view.toggleStuff = function(d, e) {
        view.showTxt[d] = !view.showTxt[d];
        view.showTxt[e] = null
    }
    view.correctVal = function() {
        if (arguments[0].start_time) {
            arguments[0].start_time = corect_val(arguments[0].start_time)
        }
        if (arguments[0].end_time) {
            arguments[0].end_time = corect_val(arguments[0].end_time)
        }
        if (arguments[0].break) {
            arguments[0].break = corect_val(arguments[0].break)
        }
    }
    view.addSheet = function(d) {
        if (view.validateAddSheet(d)) {
            view.showTxt.fillSheet = !1
            d.do = 'maintenance-service-maintenance-add_sheet';
            d.xget = 'serviceUser';
            d.start_time = convert_value(d.start_time);
            d.end_time = convert_value(d.end_time);
            d.break = convert_value(d.break);
            helper.doRequest('post', 'index.php', d, function(r) {
                view.item.users = r.data;
                view.service_sheet = {
                    servicing_sheet_id: view.service_id,
                    service_date: new Date(view.item.planeddate_js),
                    sheet_user_id: view.item.loged_user ? view.item.loged_user : ''
                };
                view.showAlerts(r)
            })
        }
    }
    view.validateAddSheet = function(p) {
        var err_valid = !1;
        var msg = '';
        if (!p.servicing_sheet_id) {
            msg = helper.getLanguage("Please select Sheet");
            err_valid = !0
        } else if (!p.start_time) {
            msg = helper.getLanguage("Please select Start time");
            err_valid = !0
        } else if (!p.end_time) {
            msg = helper.getLanguage("Please select a End time");
            err_valid = !0
        } else if (parseFloat(convert_value(p.start_time)) >= parseFloat(convert_value(p.end_time))) {
            msg = helper.getLanguage("End time should be bigger then Start time");
            err_valid = !0
        } else if (parseFloat(convert_value(p['break'])) >= (parseFloat(convert_value(p.end_time)) - parseFloat(convert_value(p.start_time)))) {
            msg = helper.getLanguage("Break is bigger then the hours");
            err_valid = !0
        } else if (!p.service_date) {
            msg = helper.getLanguage("Please select a day");
            err_valid = !0
        }
        if (err_valid) {
            var obj = {
                "error": {
                    "error": helper.getLanguage(msg)
                },
                "notice": !1,
                "success": !1
            };
            view.showAlerts(obj);
            return !1
        }
        return !0
    }
    view.deleteSheet = function(r, index) {
        var d = {
            'do': 'maintenance-service-maintenance-deleteServiceSheet',
            'servicing_sheet_id': r.service_id,
            'serv_sheet_id': r.id,
            'service_id': view.service_id,
            'xget': 'serviceUser'
        }
        helper.doRequest('post', 'index.php', d, function(r) {
            view.item.users = r.data;
            view.showAlerts(r);
            $timeout(function() {
                view.listMore(index)
            })
        })
    }
    view.showEditHour = function(r, index) {
        var obj = angular.copy(r);
        obj.index = index;
        obj.service_type = view.item.service_type;
        openModal("service_hours", obj, "lg")
    }
    view.editSheet = function(r) {
        var d = angular.copy(r);
        d.do = 'maintenance-service-maintenance-editServiceSheet';
        d.start_time = convert_value(d.start_time);
        d.end_time = convert_value(d.end_time);
        d.break = convert_value(d.break);
        d.xget = 'serviceHours';
        view.showTxt.editSheett = !1;
        helper.doRequest('post', 'index.php', d, function(r) {
            view.item.hours = r.data;
            view.hoursFix();
            view.showAlerts(r)
        })
    }
    view.addExpense = function(d) {
        view.showTxt.fillTime = !1
        d.do = 'maintenance-service-maintenance-add_exp';
        d.xget = 'serviceUser';
        helper.doRequest('post', 'index.php', d, function(r) {
            view.item.users = r.data;
            view.showAlerts(r)
        })
    }
    view.deleteExpense = function(r, index) {
        var d = {
            'do': 'maintenance-service-time-delete_expense',
            'id': r.E_ID,
            'service_id': view.service_id,
            'xget': 'serviceUser',
        }
        helper.doRequest('post', 'index.php', d, function(r) {
            view.item.users = r.data;
            view.showAlerts(r);
            $timeout(function() {
                view.listMore(index)
            })
        })
    }
    view.showEditExp = function(r, index) {
        var obj = angular.copy(r);
        obj.index = index;
        openModal("service_expences", obj, "md")
    }
    view.editExpense = function(r) {
        var d = angular.copy(r);
        var formdata = new FormData(),
            x = $('#attachement');
        d.do = 'maintenance-service-maintenance-update_expense';
        d.xget = 'serviceExpense';
        d.service_id = view.service_id;
        d.amount = d.amount_txt;
        d.expense_id = d.EXP_ID;
        d.id = d.E_ID;
        d.date = d.DATEE;
        for (y in d) {
            formdata.append(y, d[y])
        }
        if (x[0].files.length > 0) {
            formdata.append("Filedata", x[0].files[0])
        }
        view.showTxt.editExpensee = !1;
        helper.doRequest('post', 'index.php', formdata, function(r) {
            view.item.expense = r.data;
            view.showAlerts(r)
        })
    }
    view.viewRecepeit = function(exp) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'miscTemplate/modal',
            controller: 'viewRecepitCtrl as vmMod',
            resolve: {
                data: function() {
                    var obj = {
                        e_message: '<center><img src="' + exp.IMG_HREF + '" class="img-responsive" ></center>'
                    }
                    return obj
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            view.selected = selectedItem
        }, function() {
            console.log('modal closed')
        })
    };
    view.deleteRecepeit = function(r, index) {
        var d = angular.copy(r);
        d.do = 'maintenance-service-time-delete_image';
        d.id = d.E_ID;
        d.service_id = view.service_id;
        d.xget = 'serviceUser';
        helper.doRequest('post', 'index.php', d, function(r) {
            view.item.users = r.data;
            view.showAlerts(r);
            $timeout(function() {
                view.listMore(index)
            })
        })
    }
    view.editSupply = function(r) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'template/recepeitModal',
            controller: 'supplyCtrl',
            resolve: {
                obj: function() {
                    r.service_id = view.service_id;
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            view.selected = selectedItem
        }, function() {
            console.log('modal closed')
        })
    };
    view.taskModal = function(r) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'template/taskModal',
            controller: 'taskModalCtrl',
            resolve: {
                obj: function() {
                    r.service_id = view.service_id;
                    r.service_status = view.item.status;
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            view.selected = selectedItem
        }, function() {
            console.log('modal closed')
        })
    };
    view.sendEmails = function(r) {
        var d = angular.copy(r);
        view.showTxt.sendEmail = !1;
        if (d.recipients.length) {
            /*if (view.item.dropbox) {
                d.dropbox_files = view.item.dropbox.dropbox_files
            }*/
            if (Array.isArray(d.recipients)) {
                d.email = d.recipients[0]
            } else {
                d.email = d.recipients
            }
            view.item.email.alerts.push({
                type: 'email',
                msg: d.email + ' - '
            });
            d.do = 'maintenance--maintenance-sendNewEmail';
            d.use_html = view.item.use_html;
            d.mark_as_finished = 2;
            if (view.item.status) {
                d.mark_as_finished = ''
            }
            d.xview='1';
            helper.doRequest('post', 'index.php', d, function(r) {
                //if (Array.isArray(d.recipients)) {
                    if (r.success && r.success.success) {
                        view.item.email.alerts[view.item.email.alerts.length - 1].type = 'success';
                        view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.success.success
                    }
                    if (r.notice && r.notice.notice) {
                        view.item.email.alerts[view.item.email.alerts.length - 1].type = 'info';
                        view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.notice.notice
                    }
                    if (r.error && r.error.error) {
                        view.item.email.alerts[view.item.email.alerts.length - 1].type = 'danger';
                        view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.error.error
                    }
                    if( d.email == d.user_loged_email){
                            d.copy = !1;
                        }
                        if(d.email == d.user_acc_email){
                            d.copy_acc = !1;
                        }
                    if(Array.isArray(d.recipients)){
                        d.recipients.shift();
                        
                        view.sendEmails(d)
                    }else{
                        d.recipients =[];
                        view.sendEmails(d)
                    }               
                //}
                 
                    
            })
        }else {
            if (d.user_loged_email && d.copy) {
                d.email = d.user_loged_email;
                d.copy = !1;
                d.mark_as_sent = 0;
                /*if (view.invoice.dropbox) {
                    d.dropbox_files = view.invoice.dropbox.dropbox_files
                }*/
                if (d.email) {
                    view.item.email.alerts.push({
                        type: 'email',
                        msg: d.email + ' - '
                    });
                    d.do = 'maintenance-service-maintenance-sendNewEmail';
                    d.use_html = view.item.use_html;
                    d.xview='1';
                    helper.doRequest('post', 'index.php', d, function(r) {
                        if (r.success && r.success.success) {
                            view.item.email.alerts[view.item.email.alerts.length - 1].type = 'success';
                            view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.success.success
                        }
                        if (r.notice && r.notice.notice) {
                            view.item.email.alerts[view.item.email.alerts.length - 1].type = 'info';
                            view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.notice.notice
                        }
                        if (r.error && r.error.error) {
                            view.item.email.alerts[view.item.email.alerts.length - 1].type = 'danger';
                            view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.error.error
                        }
                        //d.user_loged_email = null;
                        d.user_mail_sent =1;
                        view.sendEmails(d);
                        /*if (r.success) {
                            view.renderPage(r, !0)
                        }*/
                    })
                }
            } else if (d.user_acc_email && d.copy_acc ) {
                //console.log(d.user_mail_sent, d.user_acc_email, d.user_loged_email);
                if(d.user_mail_sent && d.user_acc_email == d.user_loged_email){
                  
                }else{
                    d.email = d.user_acc_email;
                    d.copy_acc = !1;
                    d.mark_as_sent = 0;
                    /*if (view.invoice.dropbox) {
                        d.dropbox_files = view.invoice.dropbox.dropbox_files
                    }*/
                    if (d.email) {
                        view.item.email.alerts.push({
                            type: 'email',
                            msg: d.email + ' - '
                        });
                        d.do = 'maintenance-service-maintenance-sendNewEmail';
                        d.use_html = view.item.use_html;
                        d.xview='1';
                       
                        helper.doRequest('post', 'index.php', d, function(r) {
                            if (r.success && r.success.success) {
                                view.item.email.alerts[view.item.email.alerts.length - 1].type = 'success';
                                view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.success.success
                            }
                            if (r.notice && r.notice.notice) {
                                view.item.email.alerts[view.item.email.alerts.length - 1].type = 'info';
                                view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.notice.notice
                            }
                            if (r.error && r.error.error) {
                                view.item.email.alerts[view.item.email.alerts.length - 1].type = 'danger';
                                view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.error.error
                            }
                        })
                    }
                }

            }else{
                helper.refreshLogging();
            }
          
        }
         
    }
    view.showPreview = function(exp) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'miscTemplate/modal',
            controller: 'viewRecepitCtrl as vmMod',
            resolve: {
                data: function() {
                    var d = {
                        'do': 'maintenance-email_preview',
                        service_id: view.service_id,
                        message: exp.e_message,
                        use_html: exp.use_html
                    };
                    return helper.doRequest('post', 'index.php', d).then(function(d) {
                        return d.data
                    })
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            view.selected = selectedItem
        }, function() {
            console.log('modal closed')
        })
    };
    view.showPDFsettings = function() {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'template/pdf_settings',
            controller: 'pdfSettingsCtrl',
            resolve: {
                data: function() {
                    var d = {
                        'do': 'maintenance-pdf_settings',
                        service_id: view.service_id
                    };
                    return helper.doRequest('get', 'index.php', d).then(function(d) {
                        return d.data
                    })
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            view.selected = selectedItem
        }, function() {
            console.log('modal closed')
        })
    };
    view.renderContactForEmailAuto = function(d) {
        view.auto.options.recipients = [];
        if (d.data) {
            view.auto.options.recipients = d.data
        }
    }
    view.renderUserAuto = function(d) {
        view.auto.options.user = [];
        if (d.data) {
            view.auto.options.user = d.data
        }
    }
    view.addUser = function() {
        view.add.staff = !1;
        $timeout(function() {
            var selectize = angular.element('#users_list')[0].selectize;
            selectize.open()
        })
    }
    view.userAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select collaborator'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.value) + ' </span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            if (value != undefined) {
                view.item.staff_name = this.options[value].value;
                view.showAdd('staff')
            }
        },
        onBlur() {
            $timeout(function() {
                view.add.staff = !0
            })
        },
        maxItems: 1
    };
    view.expenseAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: 'Select category',
        create: !1,
        maxItems: 1
    };
    view.addArticleBtn = function() {
        view.add.article = !1;
        $timeout(function() {
            var selectize = angular.element('#articles_list')[0].selectize;
            selectize.open()
        })
    }
    view.cancelAddArticle = function() {
        view.item.article = !0;
        view.item.article_id = 0;
        var data = {
            'do': 'maintenance-service',
            xget: 'articles_list',
            cat_id: view.item.cat_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            view.auto.options.articles = r.data.lines
        })
    }
    view.addArticle = function(obj) {
        var name = '';
        var p_price = 0;
        var s_price = 0;
        var a_line = {};
        if (obj == undefined) {
            for (x in view.auto.options.articles) {
                if (view.auto.options.articles[x].article_id == view.item.article_id) {
                    a_line = view.auto.options.articles[x];
                    break
                }
            }
        } else {
            a_line = obj
        }
        var data = {};
        data.service_id = view.service_id;
        data.article_id = obj == undefined ? view.item.article_id : a_line.article_id;
        data.purchase_price = a_line.purchase_price;
        data.sell_price = a_line.price;
        data.name = a_line.name;
        data.quantity = a_line.quantity ? a_line.quantity : '1';
        data.do = 'maintenance-service-maintenance-serviceAddArticle';
        data.xget = 'serviceArticles';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            view.showAlerts(r);
            if (r.success) {
                view.item.articles = r.data;
                view.cancelAddArticle();
                var new_stock = parseFloat(a_line.stock - parseFloat(a_line.quantity));
                var hide_stock = a_line.hide_stock;
                if (view.item.article_stock_warn) {
                    if ((new_stock < parseFloat(a_line.threshold_value)) && hide_stock != '1') {}
                }
                helper.doRequest('get', 'index.php', {
                    'do': 'maintenance-service',
                    'xget': 'downpayment',
                    'service_id': view.item.service_id
                }, function(o) {
                    view.item.show_down = o.data.show_down
                })
            }
        })
    }
    view.articleAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Article'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.name2) {
                    item.name2 = "/ " + item.name2
                }
                if (item.ALLOW_STOCK == !0 && item.hide_stock == 0) {
                    var step = (helper.displayNr(item.price)) + '<br><small class="text-muted">' + helper.getLanguage('Stock') + ' <strong>' + escape(item.stock2) + '</strong></small>'
                } else {
                    var step = ''
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '&nbsp; </small>' + '</div>' + '<div class="col-sm-4 text-right">' + step + '</div>' + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#articles_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'articles_list',
                    search: str,
                    cat_id: view.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    view.auto.options.articles = r.data.lines;
                    if (view.auto.options.articles) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value) {
            if (value == undefined) {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'articles_list',
                    cat_id: view.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    view.auto.options.articles = r.data.lines
                })
            } else if (value == '99999999999') {
                view.openModal(view, 'AddAnArticle', {
                    'do': 'misc-addAnArticle',
                    customer_id: view.item.buyer_id,
                    lang_id: view.item.email_language,
                    cat_id: view.item.price_category_id,
                    remove_vat: view.item.remove_vat
                }, 'md');
                return !1
            } else if (value != undefined) {
                view.addArticle()
            }
        },
        onItemAdd(value, $item) {
            if (value == '99999999999') {
                view.selected_article_id = ''
            }
        },
        onBlur() {
            view.add.article = !0;
            if (!view.auto.options.articles.length) {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'articles_list',
                    cat_id: view.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    view.auto.options.articles = r.data.lines
                })
            }
        },
        maxItems: 1
    };
    view.checkArtDelivered = function(item) {
        if (item.delivered) {
            if (item.hstock == '0' && view.item.article_loc) {
                openModal("article_delivery", item, "lg")
            } else {
                var data = {};
                data.delivered = 1;
                data.a_id = item.id;
                data.xget = 'serviceArticles';
                data.do = 'maintenance-service-maintenance-update_article_delivered';
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('post', 'index.php', data, function(r) {
                    view.showAlerts(r);
                    view.item.articles = r.data
                })
            }
        } else {
            var data = {};
            data.delivered = 0;
            data.a_id = item.id;
            data.service_id = view.service_id;
            data.xget = 'serviceArticles';
            data.do = 'maintenance-service-maintenance-undo_article_delivered';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                view.showAlerts(r);
                if (r.error) {
                    item.delivered = !0
                } else {
                    view.item.articles = r.data
                }
            })
        }
    }
    view.createInvoice = function() {
        var cacheService = $cacheFactory.get('ServiceToInvoice');
        if (cacheService != undefined) {
            cacheService.destroy()
        }
        var data = angular.copy(view.item.invoice_link);
        var cache = $cacheFactory('ServiceToInvoice');
        cache.put('data', data);
        $state.go('invoice.edit', {
            invoice_id: "tmp"
        })
    }
    view.duplicate = function() {
        var data = {
            'do': 'maintenance--maintenance-duplicate',
            'd_service_id': view.service_id
        };
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                view.showAlerts(r)
            } else {
                $state.go('service.edit', {
                    service_id: r.data.service_id
                })
            }
        })
    }
    view.saveNote = function(formObj) {
        var data = angular.copy(view.item);
        if (data.startdate) {
            data.startdate = convert_value(data.startdate)
        }
        if (data.duration) {
            data.duration = convert_value(data.duration)
        }
        data = helper.clearData(data, ['addresses', 'articles', 'articles_list', 'autoSrvUsers', 'cc', 'contacts', 'expense', 'expenseCat', 'hours', 'language_dd2', 'recipients', 'supplies', 'tasks', 'user_auto', 'users']);
        data.do = 'maintenance-service-maintenance-saveNote';
        helper.doRequest('post', 'index.php', data, function(r) {})
    }
    view.changeArticleBillable = function(item) {
        var data = {};
        data.a_id = item.id;
        data.billable = item.billable;
        data.do = 'maintenance--maintenance-updateArticleBillable';
        helper.doRequest('post', 'index.php', data, function(r) {
            view.showAlerts(r);
            helper.doRequest('get', 'index.php', {
                'do': 'maintenance-service',
                'xget': 'downpayment',
                'service_id': view.item.service_id
            }, function(o) {
                view.item.show_down = o.data.show_down
            })
        })
    }
    view.listMore = function(index) {
        if (view.item.users.users[index].hours_data.is_data || view.item.users.users[index].expences_data.is_data) {
            var width_hour = (view.item.edit && !view.item.is_team) ? 3 : 5;
            var width_exp = (view.item.edit && !view.item.is_team) ? 2 : 4;
            var new_line = '<tr class="versionInfo"><td colspan="8" class="no_border">' + '<table class="table dispatchTable" ng-if="view.item.users.users[' + index + '].hours_data.is_data">' + '<thead><tr><th>' + '<div class="row">' + '<div class="col-xs-2"><b>' + helper.getLanguage('Date') + '</b></div>' + '<div class="col-xs-' + width_hour + '"><b>' + helper.getLanguage('Report') + '</b></div>' + '<div class="col-xs-1"><b>' + helper.getLanguage('Internal') + '</b></div>' + '<div class="col-xs-1"><b>' + helper.getLanguage('Extra hours') + '</b></div>' + '<div class="col-xs-1"><b>' + helper.getLanguage('Start time') + '</b></div>' + '<div class="col-xs-1"><b>' + helper.getLanguage('End time') + '</b></div>' + '<div class="col-xs-1"><b>' + helper.getLanguage('Break') + '</b></div>' + '<div class="col-xs-1" ng-if="view.item.edit && !view.item.is_team"></div>' + '<div class="col-xs-1" ng-if="view.item.edit && !view.item.is_team"></div>' + '</div>' + '</th></tr></thead>' + '<tbody>' + '<tr ng-repeat="hour in view.item.users.users[' + index + '].hours_data.hours">' + '<td>' + '<div class="row">' + '<div class="col-xs-2 text-muted">{{hour.DATE}}</div>' + '<div class="col-xs-' + width_hour + ' text-muted"><span ng-bind-html="hour.time_report_txt"></span></div>' + '<div class="col-xs-1 text-muted">{{hour.internal_report_txt}}</div>' + '<div class="col-xs-1 text-muted">{{hour.extra_hours_txt}}</div>' + '<div class="col-xs-1 text-muted">{{hour.start_time}}</div>' + '<div class="col-xs-1 text-muted">{{hour.end_time}}</div>' + '<div class="col-xs-1 text-muted">{{hour.break}}</div>' + '<div class="col-xs-1 text-center" ng-if="view.item.edit && !view.item.is_team"><a class="table_minimal-btn" ng-click="view.showEditHour(hour,' + index + ')"><span class="glyphicon glyphicon-pencil"></span></a></div>' + '<div class="col-xs-1 text-center" ng-if="view.item.edit && !view.item.is_team"><a class="table_minimal-btn" ng-click="view.deleteSheet(hour,' + index + ')" confirm="' + helper.getLanguage('Are you sure, you wanna delete this entry?') + '"><span class="glyphicon glyphicon-trash"></span></a></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '<table class="table dispatchTable" ng-if="view.item.users.users[' + index + '].expences_data.is_data">' + '<thead><tr><th>' + '<div class="row">' + '<div class="col-xs-' + width_exp + '"><b>' + helper.getLanguage('Date') + '</b></div>' + '<div class="col-xs-2"><b>' + helper.getLanguage('Category') + '</b></div>' + '<div class="col-xs-2"><b>' + helper.getLanguage('Amount') + '</b></div>' + '<div class="col-xs-2"><b>' + helper.getLanguage('Notes') + '</b></div>' + '<div class="col-xs-2"><b>' + helper.getLanguage('Receipt') + '</b></div>' + '<div class="col-xs-1" ng-if="view.item.edit && !view.item.is_team"></div>' + '<div class="col-xs-1" ng-if="view.item.edit && !view.item.is_team"></div>' + '</div>' + '</th></tr></thead>' + '<tbody>' + '<tr ng-repeat="exp in view.item.users.users[' + index + '].expences_data.expense">' + '<td>' + '<div class="row">' + '<div class="col-xs-' + width_exp + ' text-muted">{{exp.DATE}}</div>' + '<div class="col-xs-2 text-muted">{{exp.category}}</div>' + '<div class="col-xs-2 text-muted"><span ng-bind-html="exp.amount"></span></div>' + '<div class="col-xs-2 text-muted"><span ng-bind-html="exp.note"></span></div>' + '<div class="col-xs-2 text-muted">' + '<a href="#" ng-click="view.viewRecepeit(exp)" ng-if="exp.VIEW_IMG"><small>' + helper.getLanguage('View receipt') + '</small></a><br/>' + '<a href="#"><small ng-click="view.deleteRecepeit(exp,' + index + ')" ng-if="exp.VIEW_IMG && !view.item.is_team">' + helper.getLanguage('Delete receipt') + '</small></a>' + '</div>' + '<div class="col-xs-1 text-center" ng-if="view.item.edit && !view.item.is_team"><a class="table_minimal-btn" ng-click="view.showEditExp(exp,' + index + ')"><span class="glyphicon glyphicon-pencil"></span></a></div>' + '<div class="col-xs-1 text-center" ng-if="view.item.edit && !view.item.is_team"><a class="table_minimal-btn" ng-click="view.deleteExpense(exp,' + index + ')" confirm="' + helper.getLanguage('Are you sure, you wanna delete this entry?') + '"><span class="glyphicon glyphicon-trash"></span></a></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>';
            angular.element('#users_table tbody tr.collapseTr').eq(index).after(new_line);
            $compile(angular.element('.orderTable tbody tr.collapseTr').eq(index).next())($scope);
            angular.element('#users_table tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').addClass('hidden');
            angular.element('#users_table tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').removeClass('hidden')
        } else {
            var obj = {
                "error": !1,
                "notice": {
                    "notice": helper.getLanguage("No data to display")
                },
                "success": !1
            };
            view.showAlerts(obj)
        }
    }
    view.listLess = function(index) {
        angular.element('#users_table tbody tr.collapseTr').eq(index).next().remove();
        angular.element('#users_table tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').removeClass('hidden');
        angular.element('#users_table tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').addClass('hidden')
    }
    view.showTime = function(event, parent, model, location, align) {
        var _this = event.target;
        angular.element('body').find('timepicknew').remove();
        if (align) {
            var compiledHTML = $compile('<timepicknew nparent="' + parent + '" nmodel="' + model + '" nalign="' + align + '""></timepicknew>')($scope)
        } else {
            var compiledHTML = $compile('<timepicknew nparent="' + parent + '" nmodel="' + model + '""></timepicknew>')($scope)
        }
        if (location == 1) {
            angular.element(_this).parent().after(compiledHTML)
        } else {
            angular.element(_this).after(compiledHTML)
        }
    }
    view.task_double_show = function() {
        view.item.add_subtask1 = !1;
        view.item.add_subtask2 = !1;
        view.add.subtask = !1
    }
    view.addServiceBtn = function() {
        view.item.add_subtask1 = !0;
        $timeout(function() {
            var selectize = angular.element('#tasks_list')[0].selectize;
            selectize.open()
        })
    }
    view.taskAutoCfg = {
        valueField: 'name',
        labelField: 'name',
        searchField: ['name','code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Service'),
        create: !1,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7 text-left">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + ' </small>' + '</div>' + '<div class="col-sm-4 text-right">' + (helper.displayNr(item.price)) + '</div>' + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            }
        },
        onType(str) {
            var selectize = angular.element('#tasks_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'articles_list',
                    search: str,
                    cat_id: view.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    view.item.services_list = r.data;
                    if (view.item.services_list.lines.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value) {
            if (value == undefined || value == '') {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'articles_list',
                    cat_id: view.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    view.item.services_list = r.data
                })
            } else if (this.options[value].article_id && this.options[value].article_id == '99999999999') {
                view.openModal(view, 'AddAnArticle', {
                    'do': 'misc-addAnArticle',
                    customer_id: view.item.buyer_id,
                    lang_id: view.item.email_language,
                    cat_id: view.item.price_category_id,
                    remove_vat: view.item.remove_vat,
                    service_checked: !0
                }, 'md');
                view.item.task_service_id = undefined;
                return !1
            } else if (value != undefined) {
                var obj = {};
                obj.service_id = view.item.service_id;
                obj.do = 'maintenance-service-maintenance-serviceAddService';
                obj.xget = 'serviceSubServices';
                obj.task_service_id = this.options[value].article_id;
                obj.task_name = value;
                obj.task_budget = this.options[value].price;
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('post', 'index.php', obj, function(r) {
                    if (r && r.data) {
                        view.showAlerts(r);
                        if (r.success) {
                            view.item.services = r.data;
                            view.item.task_service_id = undefined
                        }
                        view.item.task_name = '';
                        view.item.add_subtask1 = !1;
                        view.item.add_subtask2 = !1
                    }
                })
            }
        },
        onBlur() {
            view.item.add_subtask1 = !1;
            if (!view.item.services_list.length) {
                var data = {
                    'do': 'maintenance-service',
                    xget: 'articles_list',
                    cat_id: view.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    view.item.services_list = r.data
                })
            }
        },
        maxItems: 1
    };
    
    view.checkFree = function() {
        var obj = {};
        obj.do = 'maintenance-service';
        obj.xget = 'planUsers';
        obj.planeddate_js = view.item.planeddate_js.getTime();
        obj.free = '1';
        obj.service_id = view.service_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            view.item.planning = r.data;
            if (view.item.planning.planning_date) {
                view.item.planning.planning_date = new Date(parseInt(view.item.planning.planning_date))
            }
        })
    }
    view.teamAutoCfg = {
        valueField: 'team_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('A Team'),
        create: !1,
        onItemAdd(value, $item) {
            $scope.staff_name = $item.html()
        },
        onChange(value) {
            var obj = {};
            obj.do = 'maintenance-service';
            obj.xget = 'planUsers';
            if (view.item.planning.planning_date) {
                obj.planning_date = view.item.planning.planning_date.toUTCString()
            }
            if (value) {
                obj.team_id = this.options[value].team_id
            }
            obj.free = view.item.planning.free;
            obj.service_id = view.service_id;
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', obj, function(r) {
                view.item.planning = r.data;
                if (view.item.planning.planning_date) {
                    view.item.planning.planning_date = new Date(view.item.planning.planning_date)
                }
            })
        },
        maxItems: 1
    };
    view.navigate = function(i) {
        view.item.planning.planning_date.setDate(view.item.planning.planning_date.getDate() + (i * 7));
        var obj = {};
        obj.do = 'maintenance-service';
        obj.xget = 'planUsers';
        if (view.item.planning.planning_date) {
            obj.planning_date = view.item.planning.planning_date.toUTCString()
        }
        if (view.item.planning.team_id) {
            obj.team_id = view.item.planning.team_id
        }
        obj.free = view.item.planning.free;
        obj.service_id = view.service_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            view.item.planning = r.data;
            if (view.item.planning.planning_date) {
                view.item.planning.planning_date = new Date(view.item.planning.planning_date)
            }
        })
    }
    view.changePlanningDate = function() {
        var obj = {};
        obj.do = 'maintenance-service';
        obj.xget = 'planUsers';
        obj.planning_date = view.item.planning.planning_date.toUTCString();
        if (view.item.planning.team_id) {
            obj.team_id = view.item.planning.team_id
        }
        obj.free = view.item.planning.free;
        obj.service_id = view.service_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            view.item.planning = r.data;
            if (view.item.planning.planning_date) {
                view.item.planning.planning_date = new Date(view.item.planning.planning_date)
            }
        })
    }
    view.showMore = function(event, item, user_id) {
        if (item.service_nr == 0) {
            return !1
        }
        var _this = event.target;
        view.listStats = [];
        if (angular.element(_this).parents('.planning_details').hasClass('active')) {
            angular.element(_this).parents('.planning_details').removeClass('active');
            angular.element(_this).parents('.planning_details').removeClass('is_top_class');
            angular.element(_this).parents('.planning_details').find('.cell_c').empty()
        } else {
            angular.element(_this).parents('.planning_month').find('.planning_details').removeClass('active');
            angular.element(_this).parents('.planning_month').find('.planning_details').removeClass('is_top_class');
            angular.element(_this).parents('.planning_month').find('.planning_details .cell_c').empty();
            var data = {};
            data.do = 'maintenance-planning_over';
            data.xget = 'Day';
            data.start_date = item.services_data[0].tmsmp;
            data.selected_user_id = user_id;
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                view.listStats = r.data;
                if (view.listStats.is_top) {
                    angular.element(_this).parents('.planning_details').addClass('is_top_class')
                }
                var new_content = '<div ng-repeat="nitem in view.listStats.user_data[0].all_day" class="entry {{nitem.phour}} {{nitem.pwidth}} {{nitem.pduration}} {{nitem.colors}}" ng-click="view.modal(nitem,\'template/viewIntervention\',\'viewInterventionCtrl\')">' + '<p class="text-ellipsis" ng-class="{ \'text-success\' : nitem.status==1, \'text-warning\' : nitem.status==0, \'text-muted\':nitem.status==2 }" title="{{nitem.c_name}}"><del ng-if="nitem.status==2">{{nitem.c_name}}</del><span ng-if="nitem.status!=2">{{nitem.c_name}}</span></p></div>' + '<div class="planning_details_body day-preview" ng-scrollbars ng-scrollbars-config="view.scroll_config_in">' + '<div class="planning_cell_alt" ng-repeat="nitem2 in view.listStats.days_nr">{{nitem2.day}}</div>' + '<div ng-repeat="nitem3 in view.listStats.user_data[0].data" class="entry {{nitem3.phour}} {{nitem3.pduration}} {{nitem3.pwidth}} {{nitem3.colors}}" ng-click="view.modal(nitem3,\'template/viewIntervention\',\'viewInterventionCtrl\')">' + '<p class="text-ellipsis" ng-class="{ \'text-success\' : nitem3.status==1, \'text-warning\' : nitem3.status==0, \'text-muted\':nitem3.status==2 }"><del ng-if="nitem3.status==2">{{nitem3.c_name}}</del><span ng-if="nitem3.status!=2">{{nitem3.c_name}}</span></p>' + '</div>' + '</div>';
                angular.element(_this).parents('.planning_details').find('.cell_c').append(new_content);
                $compile(angular.element(_this).parents('.planning_details').find('a').nextAll())($scope);
                angular.element(_this).parents('.planning_details').addClass('active');
                if (!view.scroll_config_in) {
                    $timeout(function() {
                        view.scroll_config_in = {
                            autoHideScrollbar: !1,
                            theme: 'minimal-dark',
                            axis: 'y',
                            scrollInertia: 50,
                            setTop: '-234px',
                            setHeight: '273px'
                        }
                    })
                }
            })
        }
    }
    view.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl,
            size: size ? size : 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem
        }, function() {})
    };
    view.changeTaskBudget = function(item) {
        var data = {};
        data.task_budget = item.task_budget;
        data.task_id = item.id;
        data.service_id = view.item.service_id;
        data.xget = 'serviceSubTasks';
        data.do = 'maintenance-service-maintenance-update_task_budget';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            view.showAlerts(r);
            view.item.tasks = r.data
        })
    }
    view.makeDelivery = function() {
        var obj = {
            'service_id': view.item.service_id,
            'delivery_id': ''
        };
        openModal("article_delivery", obj, "lg")
    }
    view.makeReservation = function() {
        var obj = {
            'service_id': view.item.service_id,
            'reservation_id': ''
        };
        openModal("article_reservation", obj, "lg")
    }
    view.showDelivery = function(item) {
        var obj = {
            'service_id': view.item.service_id,
            'delivery_id': item.delivery_id
        };
        openModal("article_delivery", obj, "lg")
    }
    view.deleteDelivery = function(item) {
        var obj = {};
        obj.do = 'maintenance-service-maintenance-delete_delivery';
        obj.delivery_id = item.delivery_id;
        obj.service_id = view.service_id;
        obj.xview='1';
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                view.renderPage(r);
            }
        })
    }
    view.showDeliveries = function() {
        openModal("article_deliveries", view.item.service_id, "lg")
    }
    view.addUserPlan = function(item) {
        view.add.staff = !1;
        view.item.staff_id = item.user_id;
        view.item.staff_name = item.name;
        view.showAdd('staff')
    }
    view.closeOpened = function() {
        view.add.subtask = !0;
        view.add.supplie = !0
    }
    view.showDrop = function() {
        if (view.showTxt.drop == !1) {
            var data = angular.copy(view.item.drop_info);
            data.do = 'misc-dropbox';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                view.item.dropbox = r.data;
                view.showTxt.drop = !0
            }, function(r) {
                var obj = {
                    "error": {
                        "error": helper.getLanguage("Problem connecting to Dropbox. Please try later.")
                    },
                    "notice": !1,
                    "success": !1
                };
                view.showAlerts(obj)
            })
        } else {
            view.showTxt.drop = !1
        }
    }
    view.deleteDropFile = function(index) {
        var data = angular.copy(view.item.dropbox.dropbox_files[index].delete_file_link);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            view.showAlerts(r);
            if (r.success) {
                view.item.dropbox.nr--;
                view.item.dropbox.dropbox_files.splice(index, 1)
            }
        })
    }
    view.deleteDropImage = function(index) {
        var data = angular.copy(view.item.dropbox.dropbox_images[index].delete_file_link);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            view.showAlerts(r);
            if (r.success) {
                view.item.dropbox.nr--;
                view.item.dropbox.dropbox_images.splice(index, 1)
            }
        })
    }
    view.deleteSignature = function(index) {
        var data = angular.copy(view.item.dropbox.signature[index].delete_file_link);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            view.showAlerts(r);
            if (r.success) {
                view.item.dropbox.nr--;
                view.item.dropbox.signature.splice(index, 1)
            }
        })
    }
    view.openLightboxModal = function(index, object) {
        Lightbox.openModal(view[object].dropbox.dropbox_images, index)
    };
    view.openSignature = function(index, object) {
        Lightbox.openModal(view[object].dropbox.signature, index)
    };
    view.uploadDoc = function(type) {
        var obj = angular.copy(view.item.s3_info);
        obj.type = type;
        if(obj.type==3){
            openModal('signatureDraw', obj, 'lg')
        }else {
            openModal('uploadDoc', obj, 'lg')
        }

    }
    view.changeFrequencyDays = function() {
        helper.doRequest('post', 'index.php', {
            'do': 'maintenance--maintenance-setFrequencyDay',
            'service_id': view.item.service_id,
            'rec_days': view.item.rec_days
        }, function(r) {
            if (r.success) {
                view.showAlerts(r)
            }
        })
    }
    view.showPlann = function() {
        openModal('saved_planning', {}, 'lg')
    }
    view.submitIntervention = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'maintenance--maintenance-submitIntervention',
            'service_id': view.item.service_id,
            'serial_number': view.item.serial_number
        }, function(r) {
            view.showAlerts(r)
        })
    }
    view.linkToInstall = function() {
        var obj = {};
        obj.target_id = view.item.installation_id;
        obj.target_folder = 'installation';
        obj.images = angular.copy(view.item.dropbox.dropbox_images);
        openModal('to_install', obj, 'lg')
    }
    view.addModel = function() {
        openModal('add_model', {}, 'lg')
    }
    view.saveModel = function() {
        return !1
    }

    view.sendEmail = function() {
        var params={
            'do': 'maintenance-email_preview',
            service_id: view.service_id,
            message: view.email.e_message,
            use_html: view.item.use_html
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post','index.php',params,function(r){
            var viewEmail=angular.copy(view.email);
            viewEmail.e_message=r.data.e_message;
            var obj={"app":"maintenance",
                "recipients":view.auto.options.recipients,
                "use_html":view.item.use_html,
                "is_file":view.item.is_file,
                "dropbox":view.item.dropbox,
                "email":viewEmail, 
                'attach_calendar': r.data.attach_calendar,
                'deny_view_article_price': view.item.deny_view_article_price
            };
            openModal("send_email", angular.copy(obj), "lg");
        })      
    }

    view.statusDocument=function(status){
        var tmp_obj={'service_id':view.service_id};
        tmp_obj.do=status=='1' ? 'maintenance-service-maintenance-activateService': 'maintenance-service-maintenance-archiveService'; 
        tmp_obj.xview='1';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post','index.php',tmp_obj,function(r){
            if(r.success){
                view.renderPage(r);
                helper.refreshLogging();
            }else{
                view.showAlerts(r);
            }
        });
    }

    view.showDownpayment=function(){
        openModal('downpayment_modal',view.item,'md');
    }

    view.registerTime=function(){
        var tmp_obj={'service_sheet':angular.copy(view.service_sheet),
        'planeddate_js':view.item.planeddate_js,
        'service_type':view.item.service_type,
        'userSrv':view.auto.options.userSrv
        };
        if(view.auto.options.userSrv.length == 1){
            tmp_obj.service_sheet.sheet_user_id=view.auto.options.userSrv[0].id;
        }
        openModal('register_time',tmp_obj,'lg');
    }

    view.registerExpense=function(){
        var tmp_obj={'service_expense':angular.copy(view.service_expense),
        'expenseCat':view.auto.options.expenseCat,
        'userSrv':view.auto.options.userSrv
        };
        openModal('register_expense',tmp_obj,'md');
    }

    view.viewInstall = function(id) {
        var url = $state.href('installation.view', {
            'installation_id': id
        });
        $window.open(url, '_blank')
    }

    view.dragUsers= {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            var tmp_users=[];
            for(var i in view.item.users.users){
                tmp_users.push(view.item.users.users[i].id);            
            }
            helper.doRequest('post', 'index.php', {
                'do': 'maintenance--maintenance-changeOrderUsers',
                'order_users':tmp_users,
                'service_id': view.service_id
            })         
        }
    };

    view.dragTasks= {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            var tmp_tasks=[];
            for(var i in view.item.tasks.tasks){
                tmp_tasks.push(view.item.tasks.tasks[i].id);            
            }
            helper.doRequest('post', 'index.php', {
                'do': 'maintenance--maintenance-changeOrderTasks',
                'order_tasks':tmp_tasks,
                'service_id': view.service_id
            })          
        }
    };

    view.dragServices= {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            var tmp_services=[];
            for(var i in view.item.services.services){
                tmp_services.push(view.item.services.services[i].id);            
            }
            helper.doRequest('post', 'index.php', {
                'do': 'maintenance--maintenance-changeOrderServices',
                'order_services':tmp_services,
                'service_id': view.service_id
            })          
        }
    };

    view.dragArticles= {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            var tmp_articles=[];
            for(var i in view.item.articles.articles){
                tmp_articles.push(view.item.articles.articles[i].id);            
            }
            helper.doRequest('post', 'index.php', {
                'do': 'maintenance--maintenance-changeOrderArticles',
                'order_articles':tmp_articles,
                'service_id': view.service_id
            })          
        }
    };

    view.dragSupplies= {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            var tmp_supplies=[];
            for(var i in view.item.supplies.supplies){
                tmp_supplies.push(view.item.supplies.supplies[i].id);            
            }
            helper.doRequest('post', 'index.php', {
                'do': 'maintenance--maintenance-changeOrderSupplies',
                'order_supplies':tmp_supplies,
                'service_id': view.service_id
            })           
        }
    };

    view.goToPurchase_orders=function(){
        openModal("create_po", {}, "lg");
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'article_delivery':
                var iTemplate = 'ArtDelivery';
                var ctas = 'art';
                break;
            case 'stock_info':
                var iTemplate = 'modal';
                var ctas = 'vmMod';
                break;
            case 'show_types':
                var iTemplate = 'ShowServiceType';
                var ctas = 'add';
                break;
            case 'service_hours':
                var iTemplate = 'ShowServiceHours';
                var ctas = 'edit';
                break;
            case 'service_expences':
                var iTemplate = 'ShowServiceExp';
                var ctas = 'add';
                break;
            case 'article_deliveries':
                var iTemplate = 'ArtDeliveries';
                var ctas = 'art';
                break;
            case 'uploadDoc':
                var iTemplate = 'AmazonUpload';
                break;
            case 'saved_planning':
                var iTemplate = 'SavedPlann';
                var ctas = 'vm';
                break;
            case 'article_reservation':
                var iTemplate = 'ArtReservation';
                var ctas = 'art';
                break;
            case 'to_install':
                var iTemplate = 'ToInstallData';
                var ctas = 'vm';
                break;
            case 'add_model':
                var iTemplate = 'serviceModels';
                var ctas = 'vmMod';
                break;
            case 'send_email':
                var iTemplate = 'SendEmail';
                var ctas = 'vm';
                break;
            case 'downpayment_modal':
                var iTemplate = 'ServiceDownpayment';
                var ctas = 'vm';
                break;
            case 'service_plan':
                var iTemplate = 'InterventionPlan';
                var ctas = 'edit';
                break;
            case 'register_time':
                var iTemplate = 'InterventionRegisterTime';
                var ctas = 'edit';
                break;
            case 'register_expense':
                var iTemplate = 'InterventionRegisterExpense';
                var ctas = 'edit';
                break;
            case 'create_po':
                var iTemplate = 'CreatePoBulk';
                var ctas = 'vm';
                break;
            case 'signatureDraw':
                var iTemplate = 'AmazonUpload';
                var ctas = 'vm';
                break;
        }
        var params = {
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'article_delivery':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'maintenance-article_delivery',
                    'service_id': item.service_id,
                    'delivery_id': item.delivery_id,
                    'xview':'1'
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        view.renderPage(d)
                    }
                }
                break;
            case 'article_reservation':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'maintenance-article_reservation',
                    'service_id': item.service_id,
                    'reservation_id': item.reservation_id,
                    'xview':'1'
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        view.renderPage(d)
                    }
                }
                break;
            case 'stock_info':
                params.template = 'miscTemplate/' + iTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                break;
            case 'show_types':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        view.renderPage(d)
                    }
                }
                params.callbackExit = function() {
                    view.item.billable = !1
                }
                break;
            case 'service_hours':
            case 'service_expences':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        view.item.users = d.data;
                        view.showAlerts(d);
                        $timeout(function() {
                            view.listMore(d.in.index)
                        })
                    }
                }
                break;
            case 'article_deliveries':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'maintenance-article_deliveries',
                    'service_id': item
                };
                params.callback = function() {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('post', 'index.php', {
                        'do': 'maintenance-service',
                        'service_id': view.item.service_id
                    }, function(r) {
                        view.renderPage(r)
                    })
                }
                break;
            case 'uploadDoc':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        view.showS3()
                    }
                }
                break;
            case 'saved_planning':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'maintenance-service',
                    'xget': 'planRecurringSaved',
                    'start_date': view.item.rec_first_day,
                    'rec_first_day': view.item.rec_first_day,
                    'service_id': view.item.service_id,
                    'activeType': '1'
                };
                params.callback = function(d) {
                    if (d) {
                        view.renderPage(d)
                    }
                }
                break;
            case 'to_install':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.item = item;
                break;
            case 'add_model':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'maintenance-service',
                    xget: 'serviceModels',
                    'lang': view.item.email_language,
                    'service_id': view.item.service_id
                };
                params.callback = function(d) {
                    if (d) {
                        view.renderPage(d)
                    }
                }
                break;
            case 'send_email':
                params.template ='miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        view.sendEmails(d);
                    }
                }
                break;
            case 'downpayment_modal':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(data) {
                    if (data && data.data) {
                        view.renderPage(data);
                    }
                }
                break;
            case 'service_plan':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.item=item;
                params.callback = function(data) {
                    if (data && data.data) {
                        view.renderPage(data);
                    }
                }
                break;
            case 'register_time':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.item=item;
                params.callback = function(data) {
                    if (data && data.data) {
                        view.item.users = data.data;
                        view.service_sheet = {
                            servicing_sheet_id: view.item.service_id,
                            service_date: new Date(view.item.planeddate_js),
                            sheet_user_id: view.item.loged_user ? view.item.loged_user : ''
                        };
                        view.showAlerts(data)
                    }
                }
                break;
            case 'register_expense':
                params.template = 'template/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.item=item;
                params.callback = function(data) {
                    if (data && data.data) {
                        view.item.users = data.data;
                        view.showAlerts(data)
                    }
                }
                break;
            case 'create_po':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params={'do':'po_order-add_bulk','service_id':view.item.service_id,'source_id':view.item.source_id,'type_id':view.item.type_id,'segment_id':view.item.segment_id};
                params.callback = function(d) {
                    if (d) {
                        $state.go('purchase_orders');
                    }
                }
                break;
            case 'signatureDraw':
                params.template = 'template/SignatureDrawModal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        view.showS3()
                    }
                }
                break;
        }
        modalFc.open(params)
    }
    view.renderPage(data)
}]).controller('ServiceDownpaymentModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.data=data;
    vm.isDisabled = false;
    vm.showTxt={
        'btn_create':false
    };

    vm.downpayment_opts=[{'id':'1','name':helper.getLanguage('Excl. VAT')},{'id':'2','name':helper.getLanguage('Incl. VAT')}];

    vm.selectfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        onChange(value){
            vm.setDownType('1');
        },
        maxItems: 1
    };

    vm.downpayment_opt_id='1';

    vm.vat = 0;

    var vat_intra = !1;
    for (x in vm.data.vat_regim_dd) {
        if (vm.data.vat_regime_id == vm.data.vat_regim_dd[x].id) {
            vm.vat = parseInt(vm.data.vat_regim_dd[x].value);
            if (vm.data.vat_regim_dd[x].regime_type == '2') {
                vat_intra = !0;
            }
            break;
        }
    }
    if (vat_intra) {
        vm.vat = 0;
    }  

    vm.cancel=function() {
        $uibModalInstance.close();
    }

    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d);
    }

    vm.setDownType = function(type) {
        var task_total = 0;
        for (x in vm.data.tasks.tasks) {
            task_total += parseFloat(helper.return_value(vm.data.tasks.tasks[x].task_budget), 10)
        }
        for (x in vm.data.services.services) {
            task_total += (parseFloat(helper.return_value(vm.data.services.services[x].quantity), 10) * parseFloat(helper.return_value(vm.data.services.services[x].task_budget), 10))
        }
        for (x in vm.data.supplies.supplies) {
            if (vm.data.supplies.supplies[x].billable) {
                task_total += (parseFloat(helper.return_value(vm.data.supplies.supplies[x].quantity), 10) * parseFloat(helper.return_value(vm.data.supplies.supplies[x].sell_price), 10))
            }
        }
        for (x in vm.data.articles.articles) {
            if (vm.data.articles.articles[x].billable) {
                task_total += (parseFloat(helper.return_value(vm.data.articles.articles[x].quantity), 10) * parseFloat(helper.return_value(vm.data.articles.articles[x].sell_price), 10))
            }
        }

        if(vm.vat && vm.vat>0 && vm.downpayment_opt_id == '2'){
            task_total+=(task_total*vm.vat/100);
        }

        if (type == '1') {
            if (vm.data.service_type == '1') {
                vm.data.downpayment_percent = helper.displayNr(parseFloat(helper.return_value(vm.data.downpayment_fixed), 10) * 100 / task_total)
            } else {
                vm.data.downpayment_percent = helper.displayNr(0)
            }
        } else {
            vm.data.downpayment_fixed = helper.displayNr(parseFloat(helper.return_value(vm.data.downpayment_percent), 10) / 100 * task_total)
        }
        vm.data.downpayment_type = type;
        if (vm.data.service_type=='1' && parseFloat(helper.return_value(vm.data.downpayment_fixed), 10) > task_total) {
            vm.showTxt.btn_create=false;
            var obj = {
                "error": {
                    "error": helper.getLanguage("Downpayment amount greater than intervention fixed price")
                },
                "notice": !1,
                "success": !1
            };
            vm.showAlerts(obj);         
        }else if(parseFloat(helper.return_value(vm.data.downpayment_fixed), 10)<=0){
            //
        }else{
            vm.showTxt.btn_create=true;
        }
    }

    vm.createDownPayment = function() {
        vm.isDisabled=true;
        var obj = {};
        obj.downpayment = '1';
        obj.downpayment_type = vm.data.downpayment_type;
        obj.downpayment_fixed = vm.vat && vm.vat>0 && vm.downpayment_opt_id == '2' ? helper.displayNr((parseFloat(helper.return_value(vm.data.downpayment_fixed),10)*100)/(100+vm.vat)) : vm.data.downpayment_fixed;
        obj.downpayment_percent = vm.data.downpayment_percent;
        obj.service_type = vm.data.service_type;
        obj.service_id = vm.data.service_id;
        obj.do = 'maintenance-service-maintenance-downpaymentInvoice';
        obj.xview='1';
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $uibModalInstance.close(r);
            }else{
                vm.showAlerts(r);
            }
        })
    }

    
}]).controller('InterventionPlanModalCtrl', ['$scope', '$compile','$timeout','helper', '$uibModalInstance', 'data', '$uibModal', function($scope, $compile, $timeout, helper, $uibModalInstance, data, $uibModal) {
    console.log('Plan intervention Modal Ctrl');

    var edit=this;
    edit.item = data;
    if(edit.item.startdate){
        edit.item.startdate=convert_to_time(edit.item.startdate);
    }
    if(edit.item.duration){
        edit.item.duration=convert_to_time(edit.item.duration);
    }

    edit.cancel = function() {
        $uibModalInstance.close();
    }

    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {};

    edit.openDate = function(p) {
        edit.datePickOpen[p] = !0
    };

    edit.add = {
        staff: !0
    }

    edit.showAlerts = function(d) {
        helper.showAlerts(edit, d)
    }

    edit.showTime = function(event, parent, model, location) {
        var _this = event.target;
        angular.element('body').find('timepicknew').remove();
        var compiledHTML = $compile('<timepicknew nparent="' + parent + '" nmodel="' + model + '""></timepicknew>')($scope);
        if (location == 1) {
            angular.element(_this).parent().after(compiledHTML)
        } else {
            angular.element(_this).after(compiledHTML)
        }
    }

    edit.checkFree = function() {
        var obj = {};
        obj.do = 'maintenance-service';
        obj.xget = 'planUsers';
        obj.planeddate_js = edit.item.planeddate_js.getTime();
        obj.free = '1';
        obj.service_id = edit.item.service_id;
        helper.doRequest('post', 'index.php', obj, function(r) {
            edit.item.planning = r.data;
            if (edit.item.planning.planning_date) {
                edit.item.planning.planning_date = new Date(parseInt(edit.item.planning.planning_date))
            }
        })
    }

    edit.teamAutoCfg = {
        valueField: 'team_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('A Team'),
        create: !1,
        onItemAdd(value, $item) {
            $scope.staff_name = $item.html()
        },
        onChange(value) {
            var obj = {};
            obj.do = 'maintenance-service';
            obj.xget = 'planUsers';
            if (edit.item.planning.planning_date) {
                obj.planning_date = edit.item.planning.planning_date.toUTCString()
            }
            if (value) {
                obj.team_id = this.options[value].team_id
            }
            obj.free = edit.item.planning.free;
            obj.service_id = edit.item.service_id;
            helper.doRequest('post', 'index.php', obj, function(r) {
                edit.item.planning = r.data;
                if (edit.item.planning.planning_date) {
                    edit.item.planning.planning_date = new Date(edit.item.planning.planning_date)
                }
            })
        },
        maxItems: 1
    };

    edit.navigate = function(i) {
        edit.item.planning.planning_date.setDate(edit.item.planning.planning_date.getDate() + (i * 7));
        var obj = {};
        obj.do = 'maintenance-service';
        obj.xget = 'planUsers';
        if (edit.item.planning.planning_date) {
            obj.planning_date = edit.item.planning.planning_date.toUTCString()
        }
        if (edit.item.planning.team_id) {
            obj.team_id = edit.item.planning.team_id
        }
        obj.free = edit.item.planning.free;
        obj.service_id = edit.item.service_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            edit.item.planning = r.data;
            if (edit.item.planning.planning_date) {
                edit.item.planning.planning_date = new Date(edit.item.planning.planning_date)
            }
        })
    }

    edit.changePlanningDate = function() {
        var obj = {};
        obj.do = 'maintenance-service';
        obj.xget = 'planUsers';
        obj.planning_date = edit.item.planning.planning_date.toUTCString();
        if (edit.item.planning.team_id) {
            obj.team_id = edit.item.planning.team_id
        }
        obj.free = edit.item.planning.free;
        obj.service_id = edit.item.service_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            edit.item.planning = r.data;
            if (edit.item.planning.planning_date) {
                edit.item.planning.planning_date = new Date(edit.item.planning.planning_date)
            }
        })
    }

    edit.addUserPlan = function(item) {
        edit.add.staff = !1;
        edit.item.staff_id = item.user_id;
        edit.item.staff_name = item.name;
        if (edit.item.staff_id) {
            if (edit.item.status == '0') {
                var s_date = convert_value(edit.item.startdate.getHours()+':'+edit.item.startdate.getMinutes());
                var duration = convert_value(edit.item.duration.getHours()+':'+edit.item.duration.getMinutes());
                var data = {
                    service_id: edit.item.service_id,
                    'do': 'maintenance-service-maintenance-addServiceUser',
                    'xview':'1',
                    user_id: edit.item.staff_id,
                    name: edit.item.staff_name,
                    free: !0,
                    planeddate_js: edit.item.planeddate_js,
                    planning_date: edit.item.planning.planning_date.toUTCString(),
                    startdate: s_date,
                    duration: duration
                }
            } else {
                var data = {
                    service_id: edit.item.service_id,
                    'do': 'maintenance-service-maintenance-addServiceUser',
                    'xview':'1',
                    user_id: edit.item.staff_id,
                    name: edit.item.staff_name
                }
            }
        }
        edit.item.staff_name = '';
        if(data){
            helper.doRequest('post', 'index.php', data, function(d){
                if(d.success){
                    edit.item=d.data;
                    if(edit.item.startdate){
                        edit.item.startdate=convert_to_time(edit.item.startdate);
                    }
                    if(edit.item.duration){
                        edit.item.duration=convert_to_time(edit.item.duration);
                    }
                    if (edit.item.planeddate_js) {
                        edit.item.planeddate_js = new Date(edit.item.planeddate_js);
                    }
                    if (edit.item.planning.planning_date) {
                        edit.item.planning.planning_date = new Date(edit.item.planning.planning_date)
                    }
                    edit.item.staff_id = null;
                    edit.add.staff = !0;
                }else{
                    edit.showAlerts(d);
                }             
            });
        }       
    }

    edit.removeUserPlan=function(d){
        var s_date = convert_value(edit.item.startdate.getHours()+':'+edit.item.startdate.getMinutes());
        var duration = convert_value(edit.item.duration.getHours()+':'+edit.item.duration.getMinutes());
        var data = {
            service_id: edit.item.service_id,
            'do': 'maintenance-service-maintenance-deleteServiceUser',
            'xview':'1',
            user_id:d.user_id,
            u_id: d.user_id,
            free: !0,
            planeddate_js: edit.item.planeddate_js,
            planning_date: edit.item.planning.planning_date.toUTCString(),
            startdate: s_date,
            duration: duration
        }
        helper.doRequest('post', 'index.php', data, function(d){
            if(d.success){
                edit.item=d.data;
                if(edit.item.startdate){
                    edit.item.startdate=convert_to_time(edit.item.startdate);
                }
                if(edit.item.duration){
                    edit.item.duration=convert_to_time(edit.item.duration);
                }
                if (edit.item.planeddate_js) {
                    edit.item.planeddate_js = new Date(edit.item.planeddate_js);
                }
                if (edit.item.planning.planning_date) {
                    edit.item.planning.planning_date = new Date(edit.item.planning.planning_date)
                }
                edit.item.staff_id = null;
                edit.add.staff = !0;
            }else{
                edit.showAlerts(d);
            }             
        });
    }

    edit.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl,
            size: size ? size : 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem
        }, function() {})
    };

    edit.showMore = function(event, item, user_id) {
        if (item.service_nr == 0) {
            return !1
        }
        var _this = event.target;
        edit.listStats = [];
        if (angular.element(_this).parents('.planning_details').hasClass('active')) {
            angular.element(_this).parents('.planning_details').removeClass('active');
            angular.element(_this).parents('.planning_details').removeClass('is_top_class');
            angular.element(_this).parents('.planning_details').find('.cell_c').empty()
        } else {
            angular.element(_this).parents('.planning_month').find('.planning_details').removeClass('active');
            angular.element(_this).parents('.planning_month').find('.planning_details').removeClass('is_top_class');
            angular.element(_this).parents('.planning_month').find('.planning_details .cell_c').empty();
            var data = {};
            data.do = 'maintenance-planning_over';
            data.xget = 'Day';
            data.start_date = item.services_data[0].tmsmp;
            data.selected_user_id = user_id;
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                edit.listStats = r.data;
                if (edit.listStats.is_top) {
                    angular.element(_this).parents('.planning_details').addClass('is_top_class')
                }
                var new_content = '<div ng-repeat="nitem in edit.listStats.user_data[0].all_day" class="entry {{nitem.phour}} {{nitem.pwidth}} {{nitem.pduration}}" style="border-color:{{nitem.color_ball}}" ng-click="edit.modal(nitem,\'template/viewIntervention\',\'viewInterventionCtrl\')">' + '<p class="text-ellipsis" ng-class="{ \'text-muted\':nitem.status==2 }" ng-style="{\'color\': nitem.status!=2 ? nitem.color_ball : \'\'}"title="{{nitem.c_name}}"><del ng-if="nitem.status==2">{{nitem.c_name}}</del><span ng-if="nitem.status!=2">{{nitem.c_name}}</span></p></div>' + '<div class="planning_details_body day-preview" ng-scrollbars ng-scrollbars-config="edit.scroll_config_in">' + '<div class="planning_cell_alt" ng-repeat="nitem2 in edit.listStats.days_nr">{{nitem2.day}}</div>' + '<div ng-repeat="nitem3 in edit.listStats.user_data[0].data" class="entry {{nitem3.phour}} {{nitem3.pduration}} {{nitem3.pwidth}}" style="border-color:{{nitem3.color_ball}}" ng-click="edit.modal(nitem3,\'template/viewIntervention\',\'viewInterventionCtrl\')">' + '<p class="text-ellipsis" ng-class="{ \'text-muted\':nitem3.status==2 }" ng-style="{\'color\': nitem3.status!=2 ? nitem3.color_ball : \'\'}"><del ng-if="nitem3.status==2">{{nitem3.c_name}}</del><span ng-if="nitem3.status!=2">{{nitem3.c_name}}</span></p>' + '</div>' + '</div>';
                angular.element(_this).parents('.planning_details').find('.cell_c').append(new_content);
                $compile(angular.element(_this).parents('.planning_details').find('a').nextAll())($scope);
                angular.element(_this).parents('.planning_details').addClass('active');
                if (!edit.scroll_config_in) {
                    $timeout(function() {
                        edit.scroll_config_in = {
                            autoHideScrollbar: !1,
                            theme: 'minimal-dark',
                            axis: 'y',
                            scrollInertia: 50,
                            setTop: '-234px',
                            setHeight: '273px'
                        }
                    })
                }
            })
        }
    }

    edit.ok=function(){
        var tmp_obj={
                'do': 'maintenance-service-maintenance-changeServiceStatus',
                'xview':'1',
                status: 1,
                planeddate_js: edit.item.planeddate_js,
                startdate: convert_value(edit.item.startdate.getHours()+':'+edit.item.startdate.getMinutes()),
                duration: convert_value(edit.item.duration.getHours()+':'+edit.item.duration.getMinutes()),
                service_id: edit.item.service_id,
                planeddate_ts: edit.item.planeddate_ts,
            };
        helper.doRequest('post', 'index.php', tmp_obj, function(d){
            if (d.success) {
                $uibModalInstance.close(d);
            }else{
                edit.showAlerts(d);
            }       
        });
    }

}]).controller('InterventionRegisterTimeModalCtrl', ['$scope', '$compile','$timeout','helper', '$uibModalInstance', 'data', '$uibModal', function($scope, $compile, $timeout, helper, $uibModalInstance, data, $uibModal) {
    console.log('Intervention register time Modal Ctrl');

    var edit=this;
    edit.item = {
        'planeddate_js':data.planeddate_js,
        'service_type':data.service_type,
        'userSrv':data.userSrv
    };
    edit.service_sheet = data.service_sheet;

    edit.add = {
        staff: !0
    }

    edit.cancel = function() {
        $uibModalInstance.close();
    }

    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {};

    edit.openDate = function(p) {
        edit.datePickOpen[p] = !0
    };

    edit.showAlerts = function(d,formObj) {
        helper.showAlerts(edit, d, formObj)
    }
    edit.correctVal = function() {
        // if (arguments[0].start_time) {
        //     arguments[0].start_time = corect_val(arguments[0].start_time)
        // }
        // if (arguments[0].end_time) {
        //     arguments[0].end_time = corect_val(arguments[0].end_time)
        // }
        if (arguments[0].break) {
            arguments[0].break = corect_val(arguments[0].break)
        }
    }

    edit.userAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select collaborator'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.value) + ' </span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            if (value != undefined) {
                edit.item.staff_name = this.options[value].value;
            }
        },
        onBlur() {
            $timeout(function() {
                edit.add.staff = !0
            })
        },
        maxItems: 1
    };

    edit.showTime = function(event, parent, model, location) {
        var _this = event.target;
        angular.element('body').find('timepicknew').remove();
        var compiledHTML = $compile('<timepicknew nparent="' + parent + '" nmodel="' + model + '""></timepicknew>')($scope);
        if (location == 1) {
            angular.element(_this).parent().after(compiledHTML)
        } else {
            angular.element(_this).after(compiledHTML)
        }
    }

    edit.validateAddSheet = function(p) {
        //new conversion
        if(p.start_time){
           p.start_time=p.start_time.getHours()+":"+p.start_time.getMinutes();
        }
        if(p.end_time){
           p.end_time=p.end_time.getHours()+":"+p.end_time.getMinutes();
        }
        // if(p.break){
        //    p.break=p.break.getHours()+":"+p.break.getMinutes();
        // }

        var err_valid = !1;
        var msg = '';
        if (!p.servicing_sheet_id) {
            msg = helper.getLanguage("Please select Sheet");
            err_valid = !0
        } else if (!p.start_time) {
            msg = helper.getLanguage("Please select Start time");
            err_valid = !0
        } else if (!p.end_time) {
            msg = helper.getLanguage("Please select a End time");
            err_valid = !0
        } else if (parseFloat(convert_value(p.start_time)) >= parseFloat(convert_value(p.end_time))) {
            msg = helper.getLanguage("End time should be bigger then Start time");
            err_valid = !0
        } else if (parseFloat(convert_value(p.break)) >= (parseFloat(convert_value(p.end_time)) - parseFloat(convert_value(p.start_time)))) {
            msg = helper.getLanguage("Break is bigger then the hours");
            err_valid = !0
        } else if (!p.service_date) {
            msg = helper.getLanguage("Please select a day");
            err_valid = !0
        }
        if (err_valid) {
            var obj = {
                "error": {
                    "error": helper.getLanguage(msg)
                },
                "notice": !1,
                "success": !1
            };
            edit.showAlerts(obj);
            return !1
        }
        return !0
    }

    edit.ok=function(){
        if (edit.validateAddSheet(angular.copy(edit.service_sheet))) {
            var tmp_obj=angular.copy(edit.service_sheet);
            tmp_obj.do = 'maintenance-service-maintenance-add_sheet';
            tmp_obj.xget = 'serviceUser';
            tmp_obj.start_time = convert_value(tmp_obj.start_time.getHours()+":"+tmp_obj.start_time.getMinutes());
            tmp_obj.end_time = convert_value(tmp_obj.end_time.getHours()+":"+tmp_obj.end_time.getMinutes());
            tmp_obj.break = convert_value(tmp_obj.break);
            helper.doRequest('post', 'index.php', tmp_obj, function(d){
                if (d.success) {
                    $uibModalInstance.close(d);
                }else{
                    edit.showAlerts(d);
                }       
            });
        }
    }

}]).controller('InterventionRegisterExpenseModalCtrl', ['$scope', '$compile','$timeout','helper', '$uibModalInstance', 'data', '$uibModal', function($scope, $compile, $timeout, helper, $uibModalInstance, data, $uibModal) {
    console.log('Intervention register expense Modal Ctrl');

    var edit=this;
    edit.item = {
        'userSrv':data.userSrv,
        'expenseCat':data.expenseCat
    };
    edit.service_expense = data.service_expense;

    edit.add = {
        staff: !0
    }

    edit.cancel = function() {
        $uibModalInstance.close();
    }

    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {};

    edit.openDate = function(p) {
        edit.datePickOpen[p] = !0
    };

    edit.showAlerts = function(d,formObj) {
        helper.showAlerts(edit, d, formObj)
    }

    edit.userAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select collaborator'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.value) + ' </span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            if (value != undefined) {
                edit.item.staff_name = this.options[value].value;
            }
        },
        onBlur() {
            $timeout(function() {
                edit.add.staff = !0
            })
        },
        maxItems: 1
    };

    edit.expenseAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: 'Select category',
        create: !1,
        maxItems: 1
    };

    edit.showTime = function(event, parent, model, location) {
        var _this = event.target;
        angular.element('body').find('timepicknew').remove();
        var compiledHTML = $compile('<timepicknew nparent="' + parent + '" nmodel="' + model + '""></timepicknew>')($scope);
        if (location == 1) {
            angular.element(_this).parent().after(compiledHTML)
        } else {
            angular.element(_this).after(compiledHTML)
        }
    }

    edit.ok=function(){
        var tmp_obj=angular.copy(edit.service_expense);
        tmp_obj.do = 'maintenance-service-maintenance-add_exp';
        tmp_obj.xget = 'serviceUser';
        helper.doRequest('post', 'index.php', tmp_obj, function(d){
            if (d.success) {
                $uibModalInstance.close(d);
            }else{
                edit.showAlerts(d);
            }       
        });    
    }

}])