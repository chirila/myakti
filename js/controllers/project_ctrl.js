app.controller('projectsCtrl', ['$scope', '$compile', 'helper', 'list', 'selectizeFc', 'data', function($scope, $compile, helper, list, selectizeFc, data) {
    console.log('Projects');
    $scope.views = [{
        value: helper.getLanguage('Open'),
        id: 1,
    }, {
        value: helper.getLanguage('Closed'),
        id: 2,
    }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.nav = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'project-projects',
        view: undefined,
        xget: '',
        offset: 1,
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.ord = '';
    $scope.reverse = !1;
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.archive_access_top = d.data.archive_access_top;
            $scope.nav = d.data.nav;
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['project_id']);
                count ++;
            }
            sessionStorage.setItem('project.edit_total', count);
            sessionStorage.setItem('project.edit_list', JSON.stringify(ids));
        }
    };
    $scope.archived = function(value) {
        $scope.search.archived = value;
        var obj = angular.copy($scope.search);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, $scope.renderList)
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
        $scope.checkSearch()

    };
    $scope.checkSearch = function() {
        var data = angular.copy($scope.search),
            is_search = !1;
        // data = helper.clearData(data, ['do', 'xget', 'showAdvanced', 'showList', 'offset']);
        for (x in data) {
            if (data[x]) {
                is_search = !0
            }
        }
        // vm.search.showList = is_search
    }
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.filterCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Open'),
        onChange(value) {
            $scope.toggleSearchButtons('view', value)
        }
    });
    // $scope.filterCfg = {
    //     valueField: 'id',
    //     labelField: 'name',
    //     searchField: ['name'],
    //     delimiter: '|',
    //     placeholder: helper.getLanguage('All Statuses'),
    //     create: !1,
    //     onItemAdd(value) {
    //         if (value == undefined) {
    //             value = -1
    //         }
    //         $scope.filters(value)
    //     },
    //     maxItems: 1
    // };
    $scope.listMore = function(index) {
        var add = !1;
        if (!$scope.listStats[index]) {
            $scope.listStats[index] = [];
            add = !0
        }
        if (add == !0) {
            var data = {};
            data.project_id = $scope.list[index].project_id;
            data.budget = $scope.list[index].BUDGET;
            data.spent = $scope.list[index].SPENT;
            data.budget_left = $scope.list[index].BUDGET_LEFT;
            data.procent = $scope.list[index].PROCENT;
            data.do = 'project-project_details';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', data, function(r) {
                $scope.listStats[index] = r.data;
                showStats(index)
            })
        } else {
            showStats(index)
        }
    }

    function showStats(index) {
        if ($scope.listStats[index].is_data) {
            var new_line = '<tr class="versionInfo"><td colspan="7" class="no_border">' + '<table class="table">' + '<thead>' + '<tr>' + '<th>' + '<div class="row">' + '<div class="col-xs-4 col-sm-4">' + helper.getLanguage('Budget') + '</div>' + '<div class="col-xs-4 col-sm-4">' + helper.getLanguage('Spent') + '</div>' + '<div class="col-xs-4 col-sm-4 text-right">' + helper.getLanguage('Budget Left') + '</div>' + '</div>' + '</th>' + '</tr>' + '</thead>' + '<tbody>' + '<tr>' + '<td>' + '<div class="row">' + '<div class="col-xs-4 col-sm-4 text-muted"><span ng-bind-html="listStats[' + index + '].BUDGET"></span></div>' + '<div class="col-xs-4 col-sm-4 text-muted"><span ng-bind-html="listStats[' + index + '].SPENT"></span></div>' + '<div class="col-xs-4col-sm-4 text-muted"><span ng-bind-html="listStats[' + index + '].BUDGET_LEFT"></span><span class="pull-right">({{listStats[' + index + '].PROCENT}}%)</span></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '<table class="table" ng-show="listStats[' + index + '].HIDE_TASK_REP">' + '<thead>' + '<tr>' + '<th>' + '<div class="row">' + '<div class="col-sm-2">' + helper.getLanguage('Task name') + '</div>' + '<div class="text-right" ng-class="{ \'col-sm-3\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-1\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="listStats[' + index + '].hide_these">' + helper.getLanguage('Budget(amount)') + '</div>' + '<div ng-class="{ \'col-sm-2\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-2\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }"  ng-show="listStats[' + index + '].hide_these"></div>' + '<div class="text-right" ng-class="{ \'col-sm-3\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-2\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="listStats[' + index + '].hide_these">' + helper.getLanguage('Balance spent / left(amount)') + '</div>' + '<div class="text-right" ng-class="{ \'col-sm-3\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-2\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }"  ng-show="listStats[' + index + '].HIDE_THIS">' + helper.getLanguage('Budget (hours)') + '</div>' + '<div ng-class="{ \'col-sm-2\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-1\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="listStats[' + index + '].HIDE_THIS"></div>' + '<div class="text-right" ng-class="{ \'col-sm-3\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-2\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="listStats[' + index + '].HIDE_THIS">' + helper.getLanguage('Balance spent / left(hours)') + '</div>' + '</div>' + '</th>' + '</tr>' + '</thead>' + '<tbody>' + '<tr ng-repeat="item in listStats[' + index + '].tasks">' + '<td>' + '<div class="row">' + '<div class="col-sm-2 text-muted"><span ng-bind-html="item.TASK_NAME"></span></div>' + '<div class="text-muted" ng-class="{ \'col-sm-3\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-1\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="item.hide_these"><span class="pull-right" ng-bind-html="item.TASK_BUDGET"></span></div>' + '<div ng-class="{ \'col-sm-2\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-2\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="item.hide_these">' + '<div ng-show="item.p" class="progress margin-bottom-none">' + '<div class="progress-bar" style="width: {{item.width_t}}%;"></div>' + '<div ng-if="item.over_spent_t" class="progress-bar progress-bar-danger" style="width: {{item.width_red_t}}%;"></div>' + '</div></div>' + '<div class="text-muted" ng-class="{ \'col-sm-3\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-2\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="item.hide_these"><span class="pull-right" ng-bind-html="item.RATE"></span></div>' + '<div class="text-muted" ng-class="{ \'col-sm-3\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-2\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="item.HIDE_THIS"><span class="pull-right" ng-bind-html="item.TASK_HOURS"></span></div>' + '<div ng-class="{ \'col-sm-2\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-1\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="item.HIDE_THIS">' + '<div ng-show="item.p_h" class="progress margin-bottom-none">' + '<div class="progress-bar" style="width: {{item.width_h}}%;"></div>' + '<div ng-if="item.over_spent_h" class="progress-bar progress-bar-danger" style="width: {{item.width_red_h}}%;"></div>' + '</div></div>' + '<div class="text-muted" ng-class="{ \'col-sm-3\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-2\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="item.HIDE_THIS"><span class="pull-right" ng-bind-html="item.RATE_HOURS"></span></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '<tfoot>' + '<tr>' + '<td>' + '<div class="row">' + '<div class="col-sm-3 text-muted">' + helper.getLanguage('Total') + '</div>' + '<div class="text-muted" ng-class="{ \'col-sm-3\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-2\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="listStats[' + index + '].hide_these"><span class="pull-right" ng-bind-html="listStats[' + index + '].TASK_BUDGET"></span></div>' + '<div class="text-muted" ng-class="{ \'col-sm-5\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-2\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="listStats[' + index + '].hide_these"><span class="pull-right" ng-bind-html="listStats[' + index + '].TOTAL_AMOUNT_SPENT"></span></div>' + '<div class="text-muted" ng-class="{ \'col-sm-3\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-2\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="listStats[' + index + '].HIDE_THIS"><span class="pull-right" ng-bind-html="listStats[' + index + '].TOTAL_HOURS"></span></div>' + '<div class="text-muted" ng-class="{ \'col-sm-5\' : listStats[' + index + '].hide_these && ! listStats[' + index + '].HIDE_THIS, \'col-sm-3\' : listStats[' + index + '].hide_these && listStats[' + index + '].HIDE_THIS }" ng-show="listStats[' + index + '].HIDE_THIS"><span class="pull-right" ng-bind-html="listStats[' + index + '].TOTAL_HOURS_SPENT"></span></div>' + '</div>' + '</td>' + '</tr>' + '</tfoot>' + '</table>' + '</td></tr>'
        } else {
            var new_line = '<tr class="versionInfo"><td colspan="3" class="no_border">' + '<table class="table">' + '<tbody>' + '<tr>' + '<td>' + '<div uib-alert type="notice" class="alert-warning">' + helper.getLanguage('No data to display') + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>'
        }
        angular.element('.orderTable tbody tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.orderTable tbody tr.collapseTr').eq(index).next())($scope);
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').addClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').removeClass('hidden')
    }
    $scope.listLess = function(index) {
        angular.element('.orderTable tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').removeClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').addClass('hidden')
    }
    list.init($scope, 'project-projects', $scope.renderList)
}]).controller('projectCtrl', ['$scope', 'helper', function($scope, helper) {}]).controller('ProjectEditCtrl', ['$scope', '$stateParams', '$state', '$cacheFactory', '$timeout', 'editItem', '$confirm', 'modalFc', 'helper', 'selectizeFc', 'Lightbox', function($scope, $stateParams, $state, $cacheFactory, $timeout, editItem, $confirm, modalFc, helper, selectizeFc, Lightbox) {
    console.log('Project edit');
    var edit = this,
        typePromise;
    edit.app = 'project';
    edit.ctrlpag = 'project';
    edit.tmpl = 'pTemplate';
    edit.item_id = $stateParams.project_id;
    edit.project_id = $stateParams.project_id;
    edit.buyer_id = $stateParams.buyer_id;
    edit.isAddPage = !0;
    edit.initial_start = !0;
    edit.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    var cacheQuote = $cacheFactory.get('QuoteToProject');
    if (cacheQuote != undefined) {
        var cachedDataQuote = cacheQuote.get('data')
    }
    var cacheDeal = $cacheFactory.get('DealToDocument');
    if (cacheDeal != undefined) {
        var cachedDataDeal = cacheDeal.get('data')
    }
    edit.item = {};
    edit.item.item_exists = true;
    edit.item.dropbox = {};
    edit.oldObj = {};
    edit.item.validity_date = new Date();
    edit.removedCustomer = !1;
    edit.item.add_customer = !1;
    edit.show_load = !0;
    edit.showTxt = {
        project_settings: !1,
        addBtn: !0,
        address: !1,
        notes: !1,
        drop: !1,
        type_time: !1,
        type_fixed: !1,
        hourly: !1,
        daily: !1,
        fixed_fee: !1,
        showApproveOpts: !1,
        closeDropdown: !1
    };
    edit.item.add_staff = !0;
    edit.item.add_subtask = !0;
    edit.item.add_article = !0;
    edit.item.add_purchase = !0;
    edit.contactAddObj = {};
    edit.auto = {
        options: {
            contacts: [],
            addresses: [],
            cc: [],
            user: []
        }
    };
    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {
        validity_date: !1,
        start_date: !1,
        end_date: !1,
        planned_date: !1,
        planned_end: !1,
        start: [],
        end: []
    }
    edit.renderPage = renderPage;
    edit.openDate = openDate;
    edit.showEditFnc = showEditFnc;
    edit.showCCSelectize = showCCSelectize;
    edit.cancelEdit = cancelEdit;
    edit.removeDeliveryAddr = removeDeliveryAddr;
    editItem.init(edit);
    edit.bunitAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Business Unit'),
        create: !1,
        closeAfterSelect: !0,
        onType(str) {
            var selectize = angular.element('#business_list')[0].selectize;
            selectize.close()
        },
        maxItems: 1
    };
    edit.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    edit.taskAutoCfg = {
        valueField: 'name',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Service'),
        create: !1,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7 text-left">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + ' </small>' + '</div>' + '<div class="col-sm-4 text-right">' + (helper.displayNr(item.price)) + '</div>' + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            }
        },
        onType(str) {
            var selectize = angular.element('#tasks_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'project-project',
                    xget: 'articles_list',
                    search: str,
                    cat_id: edit.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.tasks_list = r.data;
                    if (edit.item.tasks_list.lines.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value) {
            if (value == undefined || value == '') {
                var data = {
                    'do': 'project-project',
                    xget: 'articles_list',
                    cat_id: edit.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.tasks_list = r.data
                })
            } else if (this.options[value].article_id && this.options[value].article_id == '99999999999') {
                edit.openModal(edit, 'AddAnArticle', {
                    'do': 'misc-addAnArticle',
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat,
                    service_checked: !0
                }, 'md');
                edit.item.task_name = undefined;
                return !1
            } else if (value != undefined) {
                edit.AddTask(1)
            }
        },
        onBlur() {
            edit.item.add_subtask1 = !1;
            if (!edit.item.tasks_list) {
                var data = {
                    'do': 'project-project',
                    xget: 'articles_list',
                    cat_id: edit.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.tasks_list = r.data
                })
            }
        },
        maxItems: 1
    };
    edit.userAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Manager'),
        create: !1,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '</div>' + '</div>' + '</div>';
                return html
            }
        },
        onChange(value) {
            if (value != undefined && edit.item.project_id != 'tmp') {
                edit.addUser()
            }
        },
        onBlur() {
            edit.item.add_staff = !0
        },
        maxItems: 1
    };
    edit.articleAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Article'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.name2) {
                    item.name2 = "/ " + item.name2
                }
                if (item.ALLOW_STOCK == !0 && item.hide_stock == 0) {
                    var step = (helper.displayNr(item.price)) + '<br><small class="text-muted">' + helper.getLanguage('Stock') + ' <strong>' + escape(item.stock2) + '</strong></small>'
                } else {
                    var step = ''
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-right">' + step + '</div>' + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#articles_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'project-project',
                    xget: 'articles_list',
                    search: str,
                    cat_id: edit.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.articles_list = r.data.lines;
                    if (edit.item.articles_list.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value) {
            if (value == undefined) {
                var data = {
                    'do': 'project-project',
                    xget: 'articles_list',
                    cat_id: edit.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.articles_list = r.data.lines
                })
            } else if (value == '99999999999') {
                edit.openModal(edit, 'AddAnArticle', {
                    'do': 'misc-addAnArticle',
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.price_category_id,
                    remove_vat: edit.item.remove_vat
                }, 'md');
                return !1
            } else if (value != undefined) {
                edit.addArticle()
            }
        },
        onItemAdd(value, $item) {
            if (value == '99999999999') {
                edit.selected_article_id = ''
            }
        },
        onBlur() {
            edit.item.add_article = !0;
            if (!edit.item.articles_list.length) {
                var data = {
                    'do': 'project-project',
                    xget: 'articles_list',
                    cat_id: edit.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.articles_list = r.data.lines
                })
            }
        },
        maxItems: 1
    };
    edit.dealAutoCfg = {
        valueField: 'deal_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Deal'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.name) + ' </span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        maxItems: 1
    };

    function renderPage(d) {
        if (d.data) {
            edit.item = d.data;
            if(!d.data.item_exists){
                edit.item.item_exists = false;
                return 0;
            }; 
            helper.updateTitle($state.current.name,edit.item.serial_number);
            if (edit.project_id == 'tmp' && edit.project_id != d.data.project_id && d.data.customer_credit_limit) {
                var msg = "<b>" + helper.getLanguage("Warning") + ":</br> <span>" + helper.getLanguage("This client is over his credit limit of") + " " + d.data.customer_credit_limit + "</span></b>";
                var cont = {
                    "e_message": msg,
                    'e_title': helper.getLanguage('Credit warning')
                };
                openModal("stock_info", cont, "md")
            }
            edit.project_id = edit.item.project_id;
            if (edit.item.start_date) {
                edit.item.start_date = new Date(edit.item.start_date);
                edit.item.planned_date = new Date(edit.item.start_date)
            }
            if (edit.item.end_date) {
                edit.item.end_date = new Date(edit.item.end_date);
                edit.item.planned_end = new Date(edit.item.end_date)
            }
            edit.item.bunits = d.data.bunits;
            edit.item.users_list = d.data.users_list;
            if (edit.project_id != 'tmp') {
                edit.item.tasks_list = d.data.tasks_list;
                edit.item.articles_list = d.data.articles_list.lines;
                edit.item.add_staff = !0;
                edit.item.add_subtask = !0;
                edit.item.add_article = !0;
                edit.item.add_purchase = !0;
                for (x in edit.item.tasks.tasks) {
                    edit.datePickOpen.start[x] = !1;
                    edit.datePickOpen.end[x] = !1;
                    if (edit.item.tasks.tasks[x].start_date) {
                        edit.item.tasks.tasks[x].start_date = new Date(edit.item.tasks.tasks[x].start_date)
                    }
                    if (edit.item.tasks.tasks[x].end_date) {
                        edit.item.tasks.tasks[x].end_date = new Date(edit.item.tasks.tasks[x].end_date)
                    }
                }
            }
            edit.auto.options.contacts = d.data.contacts;
            edit.auto.options.addresses = d.data.addresses;
            edit.auto.options.cc = d.data.cc;
            edit.showAlerts(d);
            edit.contactAddObj = {
                language_dd: edit.item.language_dd,
                language_id: edit.item.language_id,
                gender_dd: edit.item.gender_dd,
                gender_id: edit.item.gender_id,
                title_dd: edit.item.title_dd,
                title_id: edit.item.title_id,
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                add_contact: !0,
                buyer_id: edit.item.buyer_id,
                'do': 'project-project-project-tryAddC',
                contact_only: !0,
                item_id: edit.item_id,
                name: edit.item.name,
                project_type: edit.item.project_type,
                budget_type: edit.item.budget_type,
                project_rate: edit.item.project_rate,
                billable_type: edit.item.billable_type,
                start_date: edit.item.start_date ? new Date(edit.item.start_date) : '',
                end_date: edit.item.end_date ? new Date(edit.item.end_date) : '',
                t_pr_hour: edit.item.t_pr_hour,
                send_email_alerts: edit.item.send_email_alerts,
                alert_percent: edit.item.alert_percent,
            }
            if (edit.project_id == 'tmp' && edit.initial_start == !0) {
                openModal("add_steps", edit, "lg")
            }
            if (edit.item.project_id != 'tmp') {
                edit.showDrop()
            }
        }
    }

    function openDate(p) {
        edit.datePickOpen[p] = !0
    }

    function removeDeliveryAddr() {
        edit.item.delivery_address = '';
        edit.item.delivery_address_id = ''
    }

    function cancelEdit() {
        if (Object.keys(edit.oldObj).length == 0) {
            return edit.removeCust(edit)
        }
        edit.showTxt.address = !0;
        for (x in edit.oldObj) {
            if (edit.item[x]) {
                if (x == 'delivery_address') {
                    edit.item[x] = edit.oldObj[x].trim()
                } else {
                    edit.item[x] = edit.oldObj[x]
                }
            }
        }
        edit.item.buyer_id = edit.oldObj.buyer_id
    }

    function showEditFnc() {
        edit.showTxt.address = !1;
        edit.oldObj = {
            buyer_id: angular.copy(edit.item.buyer_id),
            contact_id: angular.copy(edit.item.contact_id),
            contact_name: angular.copy(edit.item.contact_name),
            BUYER_NAME: angular.copy(edit.item.buyer_name),
            delivery_address: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address)),
            delivery_address_id: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_id)),
            delivery_address_txt: (edit.item.sameAddress ? !1 : angular.copy(edit.item.delivery_address_txt)),
            view_delivery: (edit.item.sameAddress ? !1 : angular.copy(edit.item.view_delivery)),
            sameAddress: angular.copy(edit.item.sameAddress),
        }
    }

    function showCCSelectize() {
        edit.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }
    edit.showAlerts = function(d, formObj) {
        helper.showAlerts(edit, d, formObj)
    }
    edit.saveData = function(formObj) {
        var data = angular.copy(edit.item);
        data.do = data.do_next;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                edit.renderPage(r)
            } else {
                edit.showAlerts(r, formObj)
            }
        })
    }
    edit.addArticleBtn = function() {
        edit.item.add_article = !1;
        $timeout(function() {
            var selectize = angular.element('#articles_list')[0].selectize;
            selectize.open()
        })
    }
    edit.AddTask = function(type) {
        var data = {};
        data.project_id = edit.item.project_id;
        data.task_name = edit.item.task_name;
        data.do = 'project-project-project-add_task';
        data.xget = 'tasks';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.tasks = r.data;
                edit.cancelAddTask();
                for (x in edit.item.tasks.tasks) {
                    edit.datePickOpen.start[x] = !1;
                    edit.datePickOpen.end[x] = !1;
                    if (edit.item.tasks.tasks[x].start_date) {
                        edit.item.tasks.tasks[x].start_date = new Date(edit.item.tasks.tasks[x].start_date)
                    }
                    if (edit.item.tasks.tasks[x].end_date) {
                        edit.item.tasks.tasks[x].end_date = new Date(edit.item.tasks.tasks[x].end_date)
                    }
                }
            }
        })
    }
    edit.cancelAddTask = function() {
        edit.item.task_name = '';
        edit.item.add_subtask1 = !1;
        edit.item.add_subtask2 = !1
    }
    edit.cancelAddUser = function() {
        edit.item.add_staff = !0;
        edit.item.staff_id = 0
    }
    edit.cancelAddArticle = function() {
        edit.item.add_article = !0;
        edit.item.article_id = 0;
        var data = {
            'do': 'project-project',
            xget: 'articles_list',
            cat_id: edit.item.cat_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            edit.item.articles_list = r.data.lines
        })
    }
    edit.cancelAddPurchase = function() {
        edit.item.add_purchase = !0;
        edit.item.purchase_name = ''
    }
    edit.changeBillable = function(type) {
        edit.item.invoice_method = type;
        edit.item.HIDE_NOT = !edit.item.HIDE_NOT;
        edit.item.HIDE_YES = !edit.item.HIDE_YES;
        for (x in edit.item.bill_type) {
            edit.item.bill_type[x].disabled = edit.item.bill_type[x].id >= 5 ? !0 : (type == 0 ? !0 : !1)
        }
    }
    edit.setInternal = function(type) {
        var old_internal = angular.copy(edit.item.internal_value);
        edit.item.internal_value = type;
        var data = {
            'do': 'project-project',
            'xget': 'cc',
            'internal': type
        };
        helper.doRequest('get', 'index.php', data, function(o) {
            edit.auto.options.cc = o.data;
            edit.item.invoice_method = !type;
            edit.showCCSelectize(edit);
            if (edit.project_id != 'tmp') {
                edit.listLayout(old_internal, edit.item.billable_type, edit.item.budget_type)
            }
        })
    }
    edit.draftNext = function(formObj) {
        if (edit.item.start_date == '' || (edit.item.billable_type == '6' && edit.item.end_date == '')) {
            edit.item.show_planned = !edit.item.show_planned
        } else {
            edit.changeStatus('1', formObj)
        }
    }
    edit.changeStatus = function(stage, formObj) {
        var data = angular.copy(edit.item);
        data.do = 'project-project-project-update_stage';
        data.next_status = stage;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(o) {
            if (o.error) {
                edit.showAlerts(o, formObj)
            } else {
                edit.renderPage(o)
            }
        })
    }
    edit.changeBudgetType = function(previous) {
        edit.listLayout(edit.item.internal_value, edit.item.billable_type, previous)
    }
    edit.changeBillableType = function(previous) {
        var data = {
            'do': 'project--project-updateBillableType',
            'project_id': edit.item.project_id,
            'billable_type': edit.item.billable_type
        };
        helper.doRequest('post', 'index.php', data, function() {
            edit.listLayout(edit.item.internal_value, previous, edit.item.budget_type)
        })
    }
    edit.listLayout = function(internal, billable, budget) {
        if (billable != '1' && billable != '7') {
            if (billable == '4') {
                edit.item.tasks.t_width = edit.item.tasks.t_width - 2
            } else if (edit.item.billable_type == '1' || edit.item.billable_type == '7') {
                edit.item.tasks.t_width = edit.item.tasks.t_width - 2
            }
        } else if (edit.item.billable_type != '1' && edit.item.billable_type != '7') {
            edit.item.tasks.t_width = edit.item.tasks.t_width + 2
        }
        if (billable != '5' && billable != '2') {
            if (edit.item.billable_type == '5' || edit.item.billable_type == '2') {
                edit.item.users.t_width = edit.item.users.t_width - 2
            }
        } else if (edit.item.billable_type != '5' && edit.item.billable_type != '2') {
            edit.item.users.t_width = edit.item.users.t_width + 2
        }
        if (internal == !0 && edit.item.internal_value == !1) {
            edit.item.tasks.t_width--
        } else if (internal == !0 && edit.item.internal_value == !0) {} else if (edit.item.internal_value == !0) {
            edit.item.tasks.t_width++
        }
        if (budget != '4') {
            if (edit.item.budget_type == '4') {
                edit.item.tasks.t_width--
            }
        } else if (edit.item.budget_type != '4') {
            edit.item.tasks.t_width++
        }
        if (budget != '5') {
            if (edit.item.budget_type == '5') {
                edit.item.users.t_width = edit.item.users.t_width - 2
            }
        } else if (edit.item.budget_type != '5') {
            edit.item.users.t_width = edit.item.users.t_width + 2
        }
    }
    edit.changeTaskName = function(item) {
        var data = {};
        data.task_name = item.task_name;
        data.task_id = item.REL;
        data.do = 'project--project-update_task_name';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r)
        })
    }
    edit.changeTasksBillable = function() {
        for (x in edit.item.tasks.tasks) {
            edit.item.tasks.tasks[x].BILLABLE_CHECKED = edit.item.tasks.BILLABLE_CHECKEDs
        }
        var data = {};
        data.task = edit.item.tasks.BILLABLE_CHECKEDs;
        data.project_id = edit.item.project_id;
        data.do = 'project--project-update_project_tasks_billable';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r)
        })
    }
    edit.changeTaskBillable = function(item) {
        var global_check_bil = 0;
        for (x in edit.item.tasks.tasks) {
            if (edit.item.tasks.tasks[x].BILLABLE_CHECKED == !0) {
                global_check_bil++
            }
        }
        if (global_check_bil == edit.item.tasks.tasks.length && global_check_bil != 0) {
            edit.item.tasks.BILLABLE_CHECKEDs = !0
        } else {
            edit.item.tasks.BILLABLE_CHECKEDs = !1
        }
        var data = {};
        data.task = item.BILLABLE_CHECKED;
        data.task_id = item.REL;
        data.do = 'project--project-update_project_task_billable';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r)
        })
    }
    edit.changeTaskPrice = function(type, item) {
        var data = {};
        data.h_rate = type == 1 ? item.daily_h_rate : item.t_h_rate;
        data.task_id = item.REL;
        data.project_id = edit.item.project_id;
        data.xget = 'tasks';
        data.do = type == 1 ? 'project-project-project-update_daily_t_h_rate' : 'project-project-project-update_task_t_h_rate';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            edit.item.tasks = r.data
        })
    }
    edit.changeTaskHours = function(item) {
        var data = {};
        data.t_hours = item.t_hours;
        data.task_id = item.REL;
        data.project_id = edit.item.project_id;
        data.xget = 'tasks';
        data.do = 'project-project-project-update_task_hours';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            edit.item.tasks = r.data
        })
    }
    edit.changeTaskBudget = function(item) {
        var data = {};
        data.t_budget = item.task_budget;
        data.task_id = item.REL;
        data.project_id = edit.item.project_id;
        data.xget = 'tasks';
        data.do = 'project-project-project-update_task_budget';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            edit.item.tasks = r.data
        })
    }
    edit.changeTaskClosed = function(item) {
        var global_check_closed = 0;
        for (x in edit.item.tasks.tasks) {
            if (edit.item.tasks.tasks[x].task_budget_c == !0) {
                global_check_closed++
            }
        }
        if (global_check_closed == edit.item.tasks.tasks.length && global_check_closed != 0) {
            edit.item.tasks.TASKS_CLOSED_CHECKED = !0
        } else {
            edit.item.tasks.TASKS_CLOSED_CHECKED = !1
        }
        var data = {};
        data.t_closed = item.task_budget_c;
        data.task_id = item.REL;
        data.do = 'project--project-update_project_task_closed';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r)
        })
    }
    edit.changeTasksClosed = function() {
       
        for (x in edit.item.tasks.tasks) {
            if(!edit.item.tasks.tasks[x].DISABLED){
                edit.item.tasks.tasks[x].task_budget_c = edit.item.tasks.TASKS_CLOSED_CHECKED 
            }

        }
        var data = {};
        data.t_closed = edit.item.tasks.TASKS_CLOSED_CHECKED;
        data.project_id = edit.item.project_id;
        data.do = 'project--project-update_project_tasks_closed';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r)
        })
    }
    edit.removeTask = function(item) {
        var data = {};
        data.task_id = item.REL;
        data.project_id = edit.item.project_id;
        data.do = 'project-project-project-delete_task';
        data.xget = 'tasks';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.tasks = r.data;
                for (x in edit.item.tasks.tasks) {
                    edit.datePickOpen.start[x] = !1;
                    edit.datePickOpen.end[x] = !1;
                    if (edit.item.tasks.tasks[x].start_date) {
                        edit.item.tasks.tasks[x].start_date = new Date(edit.item.tasks.tasks[x].start_date)
                    }
                    if (edit.item.tasks.tasks[x].end_date) {
                        edit.item.tasks.tasks[x].end_date = new Date(edit.item.tasks.tasks[x].end_date)
                    }
                }
            }
        })
    }
    edit.changeStart = function(item) {
        if (item.start_date != undefined) {
            var data = {};
            data.start = item.start_date;
            data.task_id = item.REL;
            data.do = 'project--project-update_project_task_start';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                edit.showAlerts(r)
            })
        }
    }
    edit.changeEnd = function(item) {
        if (item.end_date != undefined) {
            var data = {};
            data.end = item.end_date;
            data.task_id = item.REL;
            data.do = 'project--project-update_project_task_end';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                edit.showAlerts(r)
            })
        }
    }
    edit.task_double_show = function() {
        edit.item.add_subtask1 = !1;
        edit.item.add_subtask2 = !1
    }
    edit.addUser = function() {
        var user_name = '';
        for (x in edit.item.users_list) {
            if (edit.item.users_list[x].id == edit.item.staff_id) {
                user_name = edit.item.users_list[x].name
            }
        }
        var data = {};
        data.project_id = edit.item.project_id;
        data.user_id = edit.item.staff_id;
        data.user_name = user_name;
        data.do = 'project-project-project-add_user';
        data.xget = 'users';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.users = r.data;
                edit.cancelAddUser()
            }
        })
    }
    edit.AddBulkUser = function() {
        openModal("bulk_users", "", "md")
    }
    edit.checkManager = function(item) {
        var data = {};
        data.manager = item.MANAGER_CHECKED;
        data.project_user_id = item.REL;
        data.do = 'project--project-update_user_manager';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r)
        })
    }
    edit.changeUserBudget = function(item) {
        var data = {};
        data.p_hours = item.p_hours;
        data.project_user_id = item.REL;
        data.project_id = edit.item.project_id;
        data.xget = 'users';
        data.do = 'project-project-project-update_user_phours';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            edit.item.users = r.data
        })
    }
    edit.changeUserPrice = function(type, item) {
        var data = {};
        data.h_rate = type == 1 ? item.p_daily_rate : item.p_h_rate;
        data.project_user_id = item.REL;
        data.project_id = edit.item.project_id;
        data.xget = 'users';
        data.do = type == 1 ? 'project-project-project-update_user_pdailyrate' : 'project-project-project-update_user_phrate';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            edit.item.users = r.data
        })
    }
    edit.viewCost = function(item) {
        var data = angular.copy(item);
        openModal("user_cost", data, "sm")
    }
    edit.removeUser = function(item) {
        var data = {};
        data.project_user_id = item.REL;
        data.project_id = edit.item.project_id;
        data.do = 'project-project-project-delete_user';
        data.xget = 'users';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.users = r.data
            }
        })
    }
    edit.addArticle = function(obj) {
        var name = '';
        var p_price = 0;
        var s_price = 0;
        var a_line = {};
        if (obj == undefined) {
            for (x in edit.item.articles_list) {
                if (edit.item.articles_list[x].article_id == edit.item.article_id) {
                    a_line = edit.item.articles_list[x]
                }
            }
        } else {
            a_line = obj
        }
        var data = {};
        data.project_id = edit.item.project_id;
        data.article_id = obj == undefined ? edit.item.article_id : a_line.article_id;
        data.purchase_price = a_line.purchase_price;
        data.sell_price = a_line.price;
        data.name = a_line.name;
        data.quantity = a_line.quantity ? a_line.quantity : '1';
        data.do = 'project-project-project-projectAddArticle';
        data.xget = 'articles';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.articles = r.data;
                edit.cancelAddArticle();
                var new_stock = parseFloat(a_line.stock - parseFloat(a_line.quantity));
                var hide_stock = a_line.hide_stock;
                if (edit.item.article_stock_warn && edit.item.articles.allow_stock) {
                    if ((new_stock < parseFloat(a_line.threshold_value)) && hide_stock != '1') {
                        if (new_stock < 0) {
                            var msg = "<b>" + helper.getLanguage("When delivered the stock") + " <span class='text-danger'>" + new_stock + "</span> " + helper.getLanguage("will be under the threshold of") + " " + a_line.threshold_value + "</b>"
                        } else {
                            var msg = "<b>" + helper.getLanguage("When delivered the stock") + " <span class='text-warning'>" + new_stock + "</span> " + helper.getLanguage("will be under the threshold of") + " " + a_line.threshold_value + "</b>"
                        }
                        var cont = {
                            "e_message": msg
                        };
                        openModal("stock_info", cont, "md")
                    }
                }
            }
        })
    }
    edit.changeArticleName = function(item) {
        var data = {};
        data.name = item.name;
        data.a_id = item.id;
        data.do = 'project--project-update_article_name';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r)
        })
    }
    edit.changeArticleQty = function(item) {
        var data = {};
        data.qty = item.quantity;
        data.a_id = item.id;
        data.project_id = edit.item.project_id;
        data.do = 'project-project-project-update_article_qty';
        data.xget = 'articles';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.articles = r.data
            }
            if (r.error) {
                item.quantity = r.data.qty
            }
            /*if (r.data.minus_stock) {
                var msg = "<b>" + helper.getLanguage("Selected quantity exceeds article stock of") + " <span class='text-danger'>" + r.data.stock + "</span></b>";
                var cont = {
                    "e_message": msg
                };
                openModal("stock_info", cont, "md")
            }*/
        })
    }
    edit.changeArticlePrice = function(item) {
        var data = {};
        data.price = item.price;
        data.a_id = item.id;
        data.do = 'project--project-update_article_price';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r)
        })
    }
    edit.checkArtDelivered = function(item) {
        if (item.delivered_checked) {
            if (item.hstock == '0' && edit.item.article_loc) {
                openModal("article_delivery", item, "lg")
            } else {
                var data = {};
                data.delivered = 1;
                data.a_id = item.id;
                data.do = 'project-project-project-update_article_delivered';
                data.xget = 'articles';
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('post', 'index.php', data, function(r) {
                    edit.showAlerts(r);
                    edit.item.articles = r.data
                })
            }
        } else {
            var data = {};
            data.delivered = 0;
            data.a_id = item.id;
            data.project_id = edit.item.project_id;
            data.xget = 'articles';
            data.do = 'project-project-project-undo_article_delivered';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                edit.showAlerts(r);
                if (r.error) {
                    item.delivered_checked = !0
                } else {
                    edit.item.articles = r.data
                }
            })
        }
    }
    edit.viewArtMargin = function(item) {
        var data = angular.copy(item);
        openModal("article_margin", data, "md")
    }
    edit.removeArticle = function(item) {
        var data = {};
        data.a_id = item.id;
        data.do = 'project-project-project-delete_article';
        data.project_id = edit.item.project_id;
        data.article_id = item.article_id;
        data.xget = 'articles';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.articles = r.data
            }
        })
    }
    edit.addPurchase = function() {
        var data = {
            'project_id': edit.item.project_id,
            'project_purchase_id': '',
            'purchase_name': edit.item.purchase_name
        };
        openModal("purchase_modal", data, 'md')
    }
    edit.showPurchase = function(item) {
        if (edit.item.edit_access) {
            var data = {
                'project_id': edit.item.project_id,
                'project_purchase_id': item.PURCHASE_ID,
                'purchase_name': item.PURCHASE
            };
            openModal("purchase_modal", data, 'md')
        }
    }
    edit.purchaseNote = function(item) {
        if (edit.item.edit_access) {
            var data = {
                'project_id': edit.item.project_id,
                'project_purchase_id': item.PURCHASE_ID,
                'purchase_name': item.PURCHASE
            };
            openModal("purchase_note", data, 'md')
        }
    }
    edit.changePurchasesPrice = function(item) {
        var unit_price = parseFloat(helper.return_value(item.unit_price), 10);
        var margin = parseFloat(helper.return_value(item.margin), 10);
        var sell_price = (margin * unit_price) / 100 + unit_price;
        item.sell_price = helper.displayNr(sell_price);
        var data = {
            'do': 'project--project-update_purchase_field',
            'price': unit_price,
            'margin': margin,
            'project_purchase_id': item.PURCHASE_ID
        };
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r)
        })
    }
    edit.changePurchasesSPrice = function(item) {
        var unit_price = parseFloat(helper.return_value(item.unit_price), 10);
        var sell_price = parseFloat(helper.return_value(item.sell_price), 10);
        var margin = ((sell_price - unit_price) / unit_price) * 100;
        item.margin = helper.displayNr(margin);
        var data = {
            'do': 'project--project-update_purchase_field',
            'price': unit_price,
            'margin': margin,
            'project_purchase_id': item.PURCHASE_ID
        };
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r)
        })
    }
    edit.removePurchase = function(item) {
        var data = {};
        data.project_purchase_id = item.PURCHASE_ID;
        data.project_id = edit.item.project_id;
        data.do = 'project-project-project-delete_purchase';
        data.xget = 'purchases';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            if (r.success) {
                edit.item.purchases = r.data
            }
        })
    }
    edit.checkPurchaseDelivered = function(item) {
        var data = {};
        data.project_purchase_id = item.PURCHASE_ID;
        data.delivered = item.delivered;
        data.project_id = edit.item.project_id;
        data.do = 'project-project-project-update_particle_delivered';
        data.xget = 'purchases';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r);
            edit.item.purchases = r.data
        })
    }
    edit.showInvoices = function() {
        openModal("show_invoices", edit.item.project_id, "md")
    }
    edit.prepareInvoice = function() {
        var data = {};
        var href = edit.item.INVOICE_LINK;
        var pieces = href.split('&');
        for (var i = 0, len = pieces.length; i < len; i++) {
            var piece = pieces[i].split('=');
            data[piece[0]] = piece[1]
        }
        data.do = 'project-prepare_invoice';
        openModal("prepare_invoice", data, "lg")
    }
    edit.approveHours = function() {
        var data = {
            'project_id': edit.item.project_id,
            'title': edit.item.approve_hours
        };
        openModal("approve_hours", data, "md")
    }
    edit.approveTasks = function() {
        var data = {
            'project_id': edit.item.project_id,
            'title': edit.item.approve_hours,
            'tasks': edit.item.tasks
        };
        openModal("approve_tasks", data, "md")
    }
    edit.undoApproved = function() {
        var data = {
            'project_id': edit.item.project_id,
            'title': edit.item.approve_hours,
            'undo_all_approved': edit.item.undo_all_approved,
            'weeks': edit.item.weeks
        };
        openModal("unapprove_app", data, "md")
    }
    edit.showDrop = function() {
        edit.show_load = !0;
        var data_drop = angular.copy(edit.item.drop_info);
        data_drop.do = 'misc-dropbox_new';
        helper.doRequest('post', 'index.php', data_drop, function(r) {
            edit.item.dropbox = r.data;
            edit.show_load = !1
        })
    }
    edit.deleteDropFile = function(index) {
        var data = angular.copy(edit.item.dropbox.dropbox_files[index].delete_file_link);
        edit.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.show_load = !1;
            edit.showAlerts(r);
            if (r.success) {
                edit.item.dropbox.nr--;
                edit.item.dropbox.dropbox_files.splice(index, 1)
            }
        })
    }
    edit.deleteDropImage = function(index) {
        var data = angular.copy(edit.item.dropbox.dropbox_images[index].delete_file_link);
        edit.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.show_load = !1;
            edit.showAlerts(r);
            if (r.success) {
                edit.item.dropbox.nr--;
                edit.item.dropbox.dropbox_images.splice(index, 1)
            }
        })
    }
    edit.openLightboxModal = function(index) {
        Lightbox.openModal(edit.item.dropbox.dropbox_images, index)
    };
    edit.uploadDoc = function(type) {
        var obj = angular.copy(edit.item.drop_info);
        obj.type = type;
        openModal('uploadDoc', obj, 'lg')
    }
    edit.addServiceBtn = function() {
        edit.item.add_subtask1 = !0;
        $timeout(function() {
            var selectize = angular.element('#tasks_list')[0].selectize;
            selectize.open()
        })
    }
    edit.makeDelivery = function() {
        var obj = {
            'project_id': edit.item.project_id,
            'delivery_id': ''
        };
        openModal("article_delivery", obj, "lg")
    }
    edit.showDeliveries = function() {
        openModal("article_deliveries", edit.item.project_id, "lg")
    }
    edit.closeOpened = function() {
        edit.item.add_subtask2 = !1;
        edit.item.add_purchase = !0
    }
    edit.addUserBtn = function() {
        edit.item.add_staff = !1;
        $timeout(function() {
            var selectize = angular.element('#users_list')[0].selectize;
            selectize.open()
        })
    }
    edit.toggleActions = function(open) {
        if (!open) {
            edit.showTxt.showApproveOpts = !1
        }
    }
    edit.duplicateProject = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'project--project-duplicate',
            'c_project_id': edit.item.project_id
        }, function(r) {
            if (r.success) {
                $state.go('project.edit', {
                    'project_id': r.data.project_id
                })
            }
        })
    }

    edit.goToPurchase_orders=function(){
        openModal("create_po", {}, "lg");
    }

    edit.showDownpayment=function(){
        openModal('downpayment_modal',edit.item,'md');
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'bulk_users':
                var iTemplate = 'BulkUsers';
                var ctas = 'blk';
                break;
            case 'user_cost':
                var iTemplate = 'UserCost';
                var ctas = 'cost';
                break;
            case 'stock_info':
                var iTemplate = 'modal';
                var ctas = 'vmMod';
                break;
            case 'article_margin':
                var iTemplate = 'ArticleMargin';
                var ctas = 'mrg';
                break;
            case 'article_delivery':
                var iTemplate = 'ArticleDelivery';
                var ctas = 'art';
                break;
            case 'purchase_modal':
                var iTemplate = 'Purchase';
                var ctas = 'prc';
                break;
            case 'purchase_note':
                var iTemplate = 'PurchaseNote';
                var ctas = 'prc';
                break;
            case 'show_invoices':
                var iTemplate = 'ProjectInvoices';
                var ctas = 'inv';
                break;
            case 'prepare_invoice':
                var iTemplate = 'PrepareInvoice';
                var ctas = 'inv';
                break;
            case 'approve_hours':
                var iTemplate = 'ProjectApproveHours';
                var ctas = 'app';
                break;
            case 'approve_tasks':
                var iTemplate = 'ProjectApproveTasks';
                var ctas = 'app';
                break;
            case 'unapprove_app':
                var iTemplate = 'ProjectUnapprove';
                var ctas = 'app';
                break;
            case 'add_steps':
                var iTemplate = 'ProjectAddSteps';
                var ctas = 'add';
                break;
            case 'article_deliveries':
                var iTemplate = 'ArticleDeliveries';
                var ctas = 'art';
                break;
            case 'uploadDoc':
                var iTemplate = 'AmazonUpload';
                break;
            case 'create_po':
                var iTemplate = 'CreatePoBulk';
                var ctas = 'vm';
                break;
            case 'downpayment_modal':
                var iTemplate = 'ProjectDownpayment';
                var ctas = 'vm';
                break;
        }
        var params = {
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'bulk_users':
                params.template = 'pTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'project-bulk_users',
                    'project_id': edit.item.project_id
                };
                params.callback = function(data) {
                    if (data.users) {
                        edit.item.users = data
                    }
                }
                break;
            case 'user_cost':
                params.template = 'pTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.params = {
                    'do': 'project-project_user_cost',
                    'project_user_id': item.REL,
                    'status_rate': edit.item.project_rate_status
                };
                break;
            case 'stock_info':
                params.template = 'miscTemplate/' + iTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                break;
            case 'article_margin':
                params.template = 'pTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.params = {
                    'do': 'project-project_article_margin',
                    'article_id': item.id
                };
                params.callback = function() {
                    var data = {
                        'do': 'project-project',
                        'xget': 'articles',
                        'project_id': edit.item.project_id
                    };
                    helper.doRequest('get', 'index.php', data, function(r) {
                        edit.item.articles = r.data
                    })
                }
                break;
            case 'article_delivery':
                params.template = 'pTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'project-article_delivery',
                    'project_id': item.project_id,
                    'delivery_id': item.delivery_id
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        edit.renderPage(d)
                    }
                }
                break;
            case 'purchase_modal':
                params.template = 'pTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.params = {};
                for (x in item) {
                    params.params[x] = item[x]
                }
                params.params.do = 'project-project_purchase';
                params.callback = function(data) {
                    edit.cancelAddPurchase();
                    if (data) {
                        edit.item.purchases = data.data
                    }
                }
                break;
            case 'purchase_note':
                params.template = 'pTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {};
                for (x in item) {
                    params.params[x] = item[x]
                }
                params.params.do = 'project-project_purchase_note';
                break;
            case 'show_invoices':
                params.template = 'pTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'project-project_invoices',
                    project_id: item
                };
                break;
            case 'prepare_invoice':
                params.template = 'pTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = item;
                break;
            case 'approve_hours':
            case 'approve_tasks':
            case 'unapprove_app':
                params.template = 'pTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.item = item;
                params.callback = function(data) {
                    if (data) {
                        edit.renderPage(data)
                    }
                }
                break;
            case 'add_steps':
                params.template = 'pTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.item = item;
                params.callbackExit = function() {
                    $state.go('projects')
                }
                params.callback = function() {
                    if (edit.item.quote_id || edit.item.deal_id) {
                        edit.saveData(edit.project_all)
                    }
                }
                break;
            case 'article_deliveries':
                params.template = 'pTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'project-article_deliveries',
                    'project_id': item
                };
                params.callback = function() {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('post', 'index.php', {
                        'do': 'project-project',
                        'project_id': edit.item.project_id
                    }, function(r) {
                        edit.renderPage(r)
                    })
                }
                break;
            case 'uploadDoc':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        edit.showDrop()
                    }
                }
                break;
            case 'create_po':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params={'do':'po_order-add_bulk','project_id':edit.item.project_id};
                params.callback = function(d) {
                    if (d) {
                        $state.go('purchase_orders');
                    }
                }
                break;
            case 'downpayment_modal':
                params.template = 'pTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(data) {
                    if (data && data.data) {
                        edit.renderPage(data);
                    }
                }
                break;
        }
        modalFc.open(params)
    }
    var initData = {};
    initData.project_id = edit.project_id;
    initData.buyer_id = edit.buyer_id;
    if (cachedDataQuote != undefined) {
        for (x in cachedDataQuote) {
            initData[x] = cachedDataQuote[x]
        }
        cacheQuote.remove('data')
    }
    if (cachedDataDeal != undefined) {
        for (x in cachedDataDeal) {
            initData[x] = cachedDataDeal[x]
        }
        cacheDeal.remove('data')
    }
    edit.dragTasks= {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            var tmp_tasks=[];
            for(var i in edit.item.tasks.tasks){
                tmp_tasks.push(edit.item.tasks.tasks[i].REL);
            }
            helper.doRequest('post', 'index.php', {
                'do': 'project--project-changeOrderTasks',
                'order_tasks':tmp_tasks,
                'project_id': edit.item.project_id
            })
        }
    };
    angular.element('.loading_wrap').removeClass('hidden');
    helper.doRequest('post', 'index.php?do=project-project', initData, edit.renderPage)
}]).controller('ProjectServicesCtrl', ['$scope', 'helper', 'list', 'data', function($scope, helper, list, data) {
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    $scope.product_type = [{
        name: helper.getLanguage('Active'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Archived'),
        id: -1
    }];
    $scope.activeView = 0;
    $scope.product_fam = [];
    $scope.list = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'project-pservices',
        view: 0,
        xget: '',
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.listStats = [];
            $scope.lid = d.data.lid;
            $scope.type = d.data.pdf_type;
            $scope.product_fam = d.data.families;
            $scope.lr = d.data.lr
        }
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Services Type'),
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.filterCfgFam = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Families'),
        create: !1,
        onChange(value) {
            $scope.toggleSearchButtons('article_category_id', value)
        },
        maxItems: 1
    };
    $scope.renderList(data)
}]).controller('ProjectServiceCtrl', ['$scope', 'helper', function($scope, helper) {}]).controller('ProjectServiceEditCtrl', ['$scope', '$stateParams', '$timeout', 'helper', 'modalFc', 'selectizeFc', function($scope, $stateParams, $timeout, helper, modalFc, selectizeFc) {
    var edit = this,
        typePromise;
    edit.app = 'project';
    edit.ctrlpag = 'pservice';
    edit.tmpl = 'pTemplate';
    edit.item = {};
    edit.showAlerts = showAlerts;
    edit.openModal = openModal;
    edit.article_id = $stateParams.article_id;
    edit.renderPage = renderPage;
    edit.add_mode = edit.article_id == 'tmp' ? !0 : !1;
    edit.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });

    function renderPage(d) {
        if (d.data) {
            edit.item = d.data
        }
    }
    edit.save = function(formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(edit.item);
        data.do = data.do_next;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            edit.showAlerts(r, formObj);
            if (r.data.article_id != 'tmp') {
                edit.add_mode = !1
            }
        })
    }

    function showAlerts(d, formObj) {
        helper.showAlerts(edit, d, formObj)
    }

    function openModal(type, item, size) {
        var params = {
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'selectEdit':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.params = {
                    'do': 'misc-' + type,
                    'table': item
                };
                params.callback = function(data) {
                    if (data) {
                        edit.item.article_category = data.data.lines
                    }
                }
                break
        }
        modalFc.open(params)
    }
    helper.doRequest('get', 'index.php?do=project-pservice', {
        article_id: edit.article_id
    }, edit.renderPage)
}]).controller('BulkUsersModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', function($scope, helper, $uibModalInstance, data, list) {
    var blk = this;
    blk.obj = data;
    var response = {};
    blk.ord = '';
    blk.reverse = !1;
    blk.search = {
        search: '',
        'do': 'project-bulk_users',
        view: 0,
        xget: '',
        offset: 1
    };
    blk.renderList = function(d) {
        if (d.data) {
            blk.obj = d.data
        }
    };
    blk.filters = function(p) {
        list.filter(blk, p, blk.renderList)
    };
    blk.toggleSearchButtons = function(item, val) {
        list.tSearch(blk, item, val, blk.renderList)
    };
    blk.showAlerts = function(d) {
        helper.showAlerts(blk, d)
    }
    blk.cancel = function() {
        $uibModalInstance.close(response)
    }
    blk.addUser = function(item) {
        var data = {};
        data.project_id = blk.obj.project_id;
        data.user_id = item.user_id;
        data.user_name = item.NAME2;
        data.do = 'project-project-project-add_user';
        data.xget = 'users';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            blk.showAlerts(r);
            if (r.success) {
                response = r.data
            }
        })
    }
}]).controller('UserCostModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var cost = this;
    cost.obj = data;
    cost.showAlerts = function(d) {
        helper.showAlerts(cost, d)
    }
    cost.cancel = function() {
        $uibModalInstance.close()
    }
    cost.changeCost = function() {
        var data = angular.copy(cost.obj);
        data.do = 'project--project-update_user_cost';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            cost.showAlerts(r)
        })
    }
}]).controller('ArticleMarginModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var mrg = this;
    mrg.obj = data;
    mrg.showAlerts = function(d) {
        helper.showAlerts(mrg, d)
    }
    mrg.cancel = function() {
        $uibModalInstance.close()
    }
    mrg.changePPrice = function() {
        var data = {};
        data.purchase_price = mrg.obj.article_purchase_price;
        data.a_id = mrg.obj.REL;
        data.do = 'project--project-update_article_purchase_price';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            mrg.showAlerts(r);
            if (r.error) {
                mrg.obj.article_purchase_price = r.data.purchase_price
            } else {
                mrg.obj.article_sell_price = r.data.sell_price
            }
        })
    }
    mrg.changeMargin = function() {
        var data = {};
        data.margin = mrg.obj.article_margin;
        data.a_id = mrg.obj.REL;
        data.do = 'project--project-update_article_margin';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            mrg.showAlerts(r);
            if (r.error) {
                mrg.obj.article_margin = r.data.margin
            } else {
                mrg.obj.article_sell_price = r.data.sell_price
            }
        })
    }
    mrg.changeSellPrice = function() {
        var data = {};
        data.price = mrg.obj.article_sell_price;
        data.a_id = mrg.obj.REL;
        data.do = 'project--project-update_article_sell_price';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            mrg.showAlerts(r);
            if (r.error) {
                mrg.obj.article_sell_price = r.data.price
            } else {
                mrg.obj.article_margin = r.data.margin
            }
        })
    }
}]).controller('ArticleDeliveryModalCtrl', ['$scope', '$compile', '$timeout', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, $compile, $timeout, helper, $uibModalInstance, data, modalFc) {
    var art = this;
    art.obj = data;
    art.hideDetails = !0;
    art.obj.do = art.obj.delivery_id ? 'project--project-edit_delivery' : 'project-project-project-update_article_delivered';
    art.obj.hour = art.obj.delivery_id ? data.hour : "12";
    art.obj.minute = art.obj.delivery_id ? data.minute : "00";
    art.obj.pm = art.obj.delivery_id ? data.pm : 'pm';
    art.obj.date_del_line = art.obj.delivery_id ? new Date(data.delivery_date_js) : new Date();
    art.obj.contact_id = art.obj.delivery_id ? data.contact_id : undefined;
    art.obj.contact_name = art.obj.delivery_id ? data.contact_name : '';
    art.disableAddress = !0;
    art.listStats = [];
    art.listStats2 = [];
    art.format = 'dd/MM/yyyy';
    art.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    art.datePickOpen = {
        date_del_line: !1
    }
    art.openDate = function(p) {
        art.datePickOpen[p] = !0
    };
    art.hours = [12, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11];
    art.minutes = ["00", 15, 30, 45];
    art.contactAutoCfg = {
        valueField: 'recipient_id',
        labelField: 'recipient_name',
        searchField: ['recipient_name'],
        delimiter: '|',
        placeholder: 'Select contact',
        create: !0,
        maxItems: 1,
        onItemAdd(value, $item) {
            art.obj.contact_name = $item.html()
        },
        onItemRemove(value) {
            art.obj.contact_name = ''
        }
    };
    art.disabledt = function() {
        art.disableAddress = !art.disableAddress;
        return !1
    }
    art.showAlerts = function(d) {
        helper.showAlerts(art, d)
    }
    art.cancel = function() {
        $uibModalInstance.close()
    }
    if (!art.obj.delivery_id) {
        for (x in art.obj.lines) {
            art.obj.lines[x].disp_addres_info[0].quantity_delivered = art.obj.lines[x].quantity
        }
    }
    art.showStats = function(item, $index) {
        var add = !1;
        if (!art.listStats[$index]) {
            art.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (add == !0) {
            art.listStats[$index] = item.disp_addres_info;
            angular.element('.deliveryTable tbody tr.collapseTr').eq($index).after('<tr class="versionInfo"><td colspan="7" class="no_border">' + '<table class="table_minimal">' + '<tbody>' + '<tr ng-repeat="item2 in art.listStats[' + $index + ']">' + '<td>' + '<div class="row" >' + '<div class="col-xs-3"><b>{{::item2.customer_name}}</b></div>' + '<div class="col-xs-3">' + '<div>{{::item2.order_buyer_address}}</div>' + '<div>{{::item2.order_buyer_zip}} {{::item2.order_buyer_city}}</div>' + '<div>{{::item2.order_buyer_country}}</div></div>' + '<div class="col-xs-3" ng-class="{ \'is-error\':item2.is_error }"><input type="text" class="form-control" display-nr="" ng-model="item2.quantity_delivered" ng-change="art.changeSubQ(' + $index + ',item2)"><i class="fa fa-exclamation-circle" ng-if="item2.is_error"></i></div>' + '<div class="col-xs-3">In Stock: <b>{{::item2.quantity}}</b></div>' + '</div>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>');
            $compile(angular.element('.deliveryTable tbody tr.collapseTr').eq($index).next())($scope);
            modalFc.setHeight(angular.element('.modal.in'))
        } else {
            angular.element('.deliveryTable tbody tr.collapseTr').eq($index).next().toggleClass('hidden');
            modalFc.setHeight(angular.element('.modal.in'))
        }
        if (item.isopen == !0) {
            var idx = 0;
            for (x in art.obj.location_dd) {
                if (art.obj.location_dd[x].id == art.obj.select_main_location) {
                    idx = x
                }
            }
            var q = parseFloat(item.quantity_value) > parseFloat(art.listStats[$index][idx].qty) ? art.listStats[$index][idx].quantity : item.quantity;
            art.listStats[$index][idx].quantity_delivered = q
        }
    }
    art.showStats2 = function(item, $index) {
        if (item.locations.length > 0) {
            art.listStats2[$index] = item;
            angular.element('.deliveryTable tbody tr.collapseTr').eq($index).after('<tr class="versionInfo" ng-if="art.listStats2[' + $index + '].view_location"><td colspan="5" class="no_border">' + '<table class="table_minimal">' + '<tbody>' + '<tr ng-repeat="item2 in art.listStats2[' + $index + '].locations">' + '<td>' + '<div class="row" >' + '<div class="col-xs-9"><div><b>{{::item2.depo}}</b> </div>' + '<div>{{::item2.address}}</div>' + '<div>{{::item2.zip}} {{::item2.city}}</div>' + '<div>{{::item2.country}}</div>' + '</div>' + '<div class="col-xs-3">' + '<div>{{::item2.quantity_loc}}</div>' + '</div>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>');
            $compile(angular.element('.deliveryTable tbody tr.collapseTr').eq($index).next())($scope);
            modalFc.setHeight(angular.element('.modal.in'))
        }
    }
    art.changeQuantity = function(i) {
        var idx = 0;
        i.is_error = !1;
        if (art.obj.allow_stock) {
            for (x in art.obj.location_dd) {
                if (art.obj.location_dd[x].id == art.obj.select_main_location) {
                    idx = x
                }
            }
            for (x in i.disp_addres_info) {
                i.disp_addres_info[x].quantity_delivered = helper.displayNr(0);
                i.disp_addres_info[x].is_error = !1;
                if (x == idx) {
                    i.disp_addres_info[x].quantity_delivered = helper.displayNr(helper.return_value(i.quantity));
                    if (i.quantity * 1 > i.disp_addres_info[x].qty * 1) {
                        i.is_error = !0;
                        i.disp_addres_info[x].is_error = !0
                    }
                }
            }
        }
    }

    function recalculateQuantity(index) {
        var initQ = 0;
        var err = 0;
        for (x in art.obj.lines[index].disp_addres_info) {
            initQ += parseFloat(helper.return_value(art.obj.lines[index].disp_addres_info[x].quantity_delivered), 10);
            if (art.obj.lines[index].disp_addres_info[x].is_error) {
                err++
            }
        }
        art.obj.lines[index].quantity = helper.displayNr(initQ);
        if (err > 0) {
            art.obj.lines[index].is_error = !0
        }
    }
    art.changeSubQ = function(index, item) {
        if (helper.return_value(item.quantity_delivered) >= 0 && helper.return_value(item.quantity_delivered) > helper.return_value(item.quantity)) {
            item.is_error = !0;
            art.obj.lines[index].is_error = !0
        } else {
            item.is_error = !1;
            art.obj.lines[index].is_error = !1
        }
        recalculateQuantity(index)
    }
    art.changeLocation = function() {
        var line = 0;
        for (x in art.obj.location_dd) {
            if (art.obj.location_dd[x].id == art.obj.select_main_location) {
                idx = x
            }
        }
        for (y in art.obj.lines) {
            var i = art.obj.lines[y];
            i.is_error = !1;
            for (x in i.disp_addres_info) {
                i.disp_addres_info[x].quantity_delivered = helper.displayNr(0);
                i.disp_addres_info[x].is_error = !1;
                if (x == idx) {
                    i.disp_addres_info[x].quantity_delivered = i.quantity;
                    if (helper.return_value(i.quantity) * 1 > i.disp_addres_info[x].qty * 1) {
                        i.is_error = !0;
                        i.disp_addres_info[x].is_error = !0;
                        i.quantity = helper.displayNr(i.disp_addres_info[x].qty);
                        i.disp_addres_info[x].quantity_delivered = helper.displayNr(i.disp_addres_info[x].qty)
                    } else {
                        i.disp_addres_info[x].quantity_delivered = i.quantity2;
                        i.quantity = i.quantity2;
                        if (helper.return_value(i.quantity2) * 1 > i.disp_addres_info[x].qty * 1) {
                            i.is_error = !0;
                            i.disp_addres_info[x].is_error = !0;
                            i.quantity = helper.displayNr(i.disp_addres_info[x].qty);
                            i.disp_addres_info[x].quantity_delivered = helper.displayNr(i.disp_addres_info[x].qty)
                        }
                    }
                }
            }
        }
    }
    art.ok = function() {
        var obj = angular.copy(art.obj);
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            art.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
    art.showSerials = function(item, index) {
        item.index = index;
        openModal("serial_no", item, "lg")
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'serial_no':
                var iTemplate = 'SerialNo';
                var ctas = 'srl';
                break
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            size: size,
            ctrlAs: ctas,
            template: 'pTemplate/' + iTemplate + 'Modal',
            backdrop: 'static'
        };
        switch (type) {
            case 'serial_no':
                params.params = {
                    'do': 'project-select_serial_no',
                    'a_id': item.a_id,
                    'count_selected_s_n': item.count_selected_s_n,
                    'selected_s_n': item.selected_s_n
                };
                params.callback = function(data) {
                    art.obj.lines[item.index].selected_s_n = data
                }
                break
        }
        modalFc.open(params)
    }
}]).controller('SerialNoModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var srl = this;
    srl.obj = data;
    srl.showAlerts = function(d) {
        helper.showAlerts(srl, d)
    }
    srl.cancel = function() {
        $uibModalInstance.dismiss()
    }
    srl.goRight = function(item, index) {
        srl.obj.sel_serial_row.push(item);
        srl.obj.serial_row.splice(index, 1);
        srl.obj.sel_s_n++
    }
    srl.goLeft = function(item, index) {
        srl.obj.serial_row.push(item);
        srl.obj.sel_serial_row.splice(index, 1);
        srl.obj.sel_s_n--
    }
    srl.ok = function() {
        var serials = '';
        for (x in srl.obj.sel_serial_row) {
            serials = serials + srl.obj.sel_serial_row[x].serial_nr_id + ','
        }
        $uibModalInstance.close(serials)
    }
}]).controller('PurchaseModalCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'data', function($scope, $timeout, helper, $uibModalInstance, data) {
    var prc = this,
        typePromise;
    prc.obj = data;
    prc.supplierAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Supplier'),
        create: !1,
        closeAfterSelect: !0,
        onType(str) {
            var selectize = angular.element('#suppliers_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'project-project_purchase',
                    xget: 'suppliers',
                    search: str
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    prc.obj.suppliers = r.data;
                    if (prc.obj.suppliers.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value) {
            if (value == undefined || value == '') {
                var data = {
                    'do': 'project-project_purchase',
                    xget: 'suppliers'
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    prc.obj.suppliers = r.data
                })
            }
        },
        onBlur() {
            if (!prc.obj.suppliers.length) {
                var data = {
                    'do': 'project-project_purchase',
                    xget: 'suppliers'
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    prc.obj.suppliers = r.data
                })
            }
        },
        maxItems: 1
    };
    prc.showAlerts = function(d) {
        helper.showAlerts(prc, d)
    }
    prc.cancel = function() {
        $uibModalInstance.close()
    }
    prc.changePrices = function() {
        var unit_price = parseFloat(helper.return_value(prc.obj.unit_price), 10);
        var margin = parseFloat(helper.return_value(prc.obj.margin), 10);
        var sell_price = (margin * unit_price) / 100 + unit_price;
        prc.obj.sell_price = helper.displayNr(sell_price)
    }
    prc.changeSellPrice = function() {
        var unit_price = parseFloat(helper.return_value(prc.obj.unit_price), 10);
        var sell_price = parseFloat(helper.return_value(prc.obj.sell_price), 10);
        var margin = ((sell_price - unit_price) / unit_price) * 100;
        prc.obj.margin = helper.displayNr(margin)
    }
    prc.ok = function() {
        var data = angular.copy(prc.obj);
        data.do = data.ACT;
        data.xget = 'purchases';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            prc.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('PurchaseNoteModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var prc = this;
    prc.obj = data;
    prc.showAlerts = function(d) {
        helper.showAlerts(prc, d)
    }
    prc.cancel = function() {
        $uibModalInstance.close()
    }
    prc.ok = function() {
        var data = angular.copy(prc.obj);
        data.do = 'project--project-edit_purchase_note';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            prc.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('ProjectInvoicesModalCtrl', ['$scope', '$state', 'helper', '$uibModalInstance', 'data', 'list', function($scope, $state, helper, $uibModalInstance, data, list) {
    var inv = this;
    inv.obj = data;
    inv.activeView = 0;
    inv.search = {
        search: '',
        'do': 'project-project_invoices',
        view: inv.activeView,
        xget: '',
        offset: 1
    };
    inv.renderList = function(d) {
        if (d.data) {
            inv.obj = d.data
        }
    };
    inv.searchThing = function() {
        list.search(inv.search, inv.renderList, inv)
    }
    inv.showAlerts = function(d) {
        helper.showAlerts(inv, d)
    }
    inv.cancel = function() {
        $uibModalInstance.close()
    }
    inv.viewInvoice = function(inv) {
        $uibModalInstance.close();
        $state.go('invoice.view', {
            invoice_id: inv
        })
    }
}]).controller('PrepareInvoiceModalCtrl', ['$scope', '$state', '$cacheFactory', 'helper', '$uibModalInstance', 'data', function($scope, $state, $cacheFactory, helper, $uibModalInstance, data) {
    var inv = this;
    inv.obj = data;
    inv.format = 'dd/MM/yyyy';
    inv.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    inv.datePickOpen = {
        invoice_start_date: !1,
        invoice_end_date: !1
    }
    inv.showAlerts = function(d) {
        helper.showAlerts(inv, d)
    }
    inv.cancel = function() {
        $uibModalInstance.close()
    }
    inv.ok = function() {
        var cacheProject = $cacheFactory.get('ProjectToInvoice');
        if (cacheProject != undefined) {
            cacheProject.destroy()
        }
        var data = angular.copy(inv.obj);
        var unused = [];
        for (x in data) {
            if (x[0] == x[0].toUpperCase()) {
                unused.push(x)
            }
        }
        data = helper.clearData(data, unused);
        var cache = $cacheFactory('ProjectToInvoice');
        cache.put('data', data);
        $uibModalInstance.close();
        $state.go('invoice.edit', {
            invoice_id: "tmp"
        })
    }
}]).controller('ProjectApproveHoursModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var app = this;
    app.obj = data;
    app.format = 'dd/MM/yyyy';
    app.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    app.datePickOpen = {
        end_date: !1
    }
    app.obj.end_date = new Date();
    app.showAlerts = function(d) {
        helper.showAlerts(app, d)
    }
    app.cancel = function() {
        $uibModalInstance.close()
    }
    app.ok = function() {
        var data = angular.copy(app.obj);
        data.do = 'project-project-project-approveAllHours';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            app.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('ProjectApproveTasksModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var app = this;
    app.obj = data;
    app.showAlerts = function(d) {
        helper.showAlerts(app, d)
    }
    app.cancel = function() {
        $uibModalInstance.close()
    }
    app.ok = function() {
        var data = angular.copy(app.obj);
        data.do = 'project-project-project-approveAllTask';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            app.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('ProjectUnapproveModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var app = this;
    app.obj = data;
    app.showAlerts = function(d) {
        helper.showAlerts(app, d)
    }
    app.cancel = function() {
        $uibModalInstance.close()
    }
    app.ok = function() {
        var data = angular.copy(app.obj);
        data.do = 'project-project-project-unApproveHours';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            app.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('ProjectReportCtrl', ['$scope', '$stateParams', '$timeout', 'helper', function($scope, $stateParams, $timeout, helper) {
    console.log('Project report');
    var rep = this,
        typePromise;
    rep.app = 'project';
    rep.ctrlpag = 'report';
    rep.tmpl = 'pTemplate';
    rep.item_id = $stateParams.project_id;
    rep.project_id = $stateParams.project_id;
    rep.isAddPage = !0;
    rep.chart_options = {
        legend: {
            display: !0,
            position: 'bottom'
        }
    };
    rep.chart_opt = {
        legend: {
            display: !0,
            position: 'top'
        }
    };
    rep.item = {};
    rep.budget_labels = [helper.getLanguage('Budget spent'), helper.getLanguage('Budget left')];
    rep.budget_data = [];
    rep.bar_series = [helper.getLanguage('Forecast'), helper.getLanguage('Actual'), helper.getLanguage('Cost')];
    rep.bar_data = [];
    rep.bar_labels = [];
    rep.renderPage = renderPage;

    function renderPage(r) {
        if (r.data) {
            rep.item = r.data;
            rep.budget_data = r.data.budget_data;
            rep.bar_labels = r.data.bar_labels;
            rep.bar_data = r.data.bar_data
        }
    }
    helper.doRequest('get', 'index.php?do=project-project_report', {
        'project_id': rep.project_id
    }, rep.renderPage)
}]).controller('ProjectReportDetailedCtrl', ['$scope', '$stateParams', '$state', '$timeout', 'helper', function($scope, $stateParams, $state, $timeout, helper) {
    console.log('Project report detailed');
    var rep = this,
        typePromise;
    rep.app = 'project';
    rep.ctrlpag = 'report';
    rep.tmpl = 'pTemplate';
    rep.item_id = $stateParams.project_id;
    rep.project_id = $stateParams.project_id;
    rep.isAddPage = !0;
    rep.item = {};
    rep.renderPage = renderPage;

    function renderPage(r) {
        if (r.data) {
            rep.item = r.data
        }
    }
    helper.doRequest('get', 'index.php?do=project-project_report_detail', {
        'project_id': rep.project_id
    }, rep.renderPage)
}]).controller('ProjectInvoiceCtrl', ['$scope', 'helper', 'list', 'selectizeFc', 'data', function($scope, helper, list, selectizeFc, data) {
    console.log('Project to be invoiced');
    $scope.periods = [];
    $scope.customers = [];
    $scope.item = {};
    $scope.renderPage = function(d) {
        if (d.data) {
            $scope.item = d.data
        }
    };
    $scope.search = {
        period_id:undefined,
        customer_id:undefined
    };
    // $scope.filterCfg = {
    //     valueField: 'id',
    //     labelField: 'name',
    //     searchField: ['name'],
    //     delimiter: '|',
    //     placeholder: helper.getLanguage('All Periods'),
    //     create: !1,
    //     onChange(value) {
    //         $scope.filterData()
    //     },
    //     maxItems: 1
    // };
    // $scope.custCfg = {
    //     valueField: 'id',
    //     labelField: 'name',
    //     searchField: ['name'],
    //     delimiter: '|',
    //     placeholder: helper.getLanguage('All Customers'),
    //     create: !1,
    //     onChange(value) {
    //         $scope.filterData()
    //     },
    //     maxItems: 1
    // };
    $scope.filterCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('All Periods'),
        onChange(value) {
            $scope.toggleSearchButtons('period_id', value);
        }
    });
    $scope.custCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('All Customers'),
        onChange(value) {
            $scope.toggleSearchButtons('customer_id', value);
        }
    });
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
        $scope.filterData();
    };
    $scope.filterData = function() {
        var data = {};
        data.period_id = $scope.search.period_id;
        data.customer_id = $scope.search.customer_id;
        data.do = 'project-to_be_invoiced';
        data.show_approved = $scope.item.show_approved;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.renderPage(r)
        })
    }
    $scope.renderPage(data)
}]).controller('ProjectDeliverCtrl', ['$scope', '$compile', 'helper', 'list', function($scope, $compile, helper, list) {
    console.log('Project to deliver');
    $scope.views = [{
        name: helper.getLanguage('Open'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Closed'),
        id: 1,
        active: ''
    }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'project-to_deliver',
        view: $scope.activeView,
        xget: '',
        offset: 1
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.ord = '';
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.lr = d.data.lr;
            $scope.export_link = d.data.export_link
        }
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.listMore = function(index) {
        var add = !1;
        if (!$scope.listStats[index]) {
            $scope.listStats[index] = [];
            add = !0
        }
        if (add == !0) {
            var data = {};
            data.project_id = $scope.list[index].project_id;
            data.customer_name = $scope.list[index].CUSTOMER;
            data.serial_number = $scope.list[index].PROJECT_NUMBER;
            data.delivery_status = $scope.list[index].status_title;
            data.do = 'project-undelivered_list';
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', data, function(r) {
                $scope.listStats[index] = r.data;
                showStats(index)
            })
        } else {
            showStats(index)
        }
    }

    function showStats(index) {
        if ($scope.listStats[index].is_data) {
            var new_line = '<tr class="versionInfo"><td colspan="7" class="no_border">' + '<table class="table">' + '<thead>' + '<tr>' + '<th>' + '<div class="row">' + '<div class="col-xs-2 col-sm-2">' + helper.getLanguage('Project') + '</div>' + '<div class="col-xs-6 col-sm-6">' + helper.getLanguage('Article Name') + '</div>' + '<div class="col-xs-2 col-sm-2">' + helper.getLanguage('Quantity') + '</div>' + '<div class="col-xs-2 col-sm-2">' + helper.getLanguage('Price') + '</div>' + '</div>' + '</th>' + '</tr>' + '</thead>' + '<tbody>' + '<tr ng-repeat="item in listStats[' + index + '].list">' + '<td>' + '<div class="row">' + '<div class="col-xs-2 col-sm-2 text-muted"><span>{{item.serial_number}}</span></div>' + '<div class="col-xs-6 col-sm-6 text-muted"><span>{{item.article_name}}</span></div>' + '<div class="col-xs-2 col-sm-2 text-muted"><span>{{item.quantity}}</span></div>' + '<div class="col-xs-2 col-sm-2 text-muted"><span ng-bind-html="item.price"></span></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>'
        } else {
            var new_line = '<tr class="versionInfo"><td colspan="3" class="no_border">' + '<table class="table">' + '<tbody>' + '<tr>' + '<td>' + '<div uib-alert type="notice" class="alert-warning">' + helper.getLanguage('No data to display') + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>'
        }
        angular.element('.orderTable tbody tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.orderTable tbody tr.collapseTr').eq(index).next())($scope);
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').addClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').removeClass('hidden')
    }
    $scope.listLess = function(index) {
        angular.element('.orderTable tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-right').removeClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('i.fa.fa-chevron-down').addClass('hidden')
    }
    list.init($scope, 'project-to_deliver', $scope.renderList)
}]).controller('ProjectTimesheetCtrl', ['$scope', '$timeout', 'helper', 'data', 'modalFc', 'list', function($scope, $timeout, helper, data, modalFc, list) {
    console.log('Project Timesheets');
    $scope.item = {};
    $scope.search = {
        search: '',
        'do': 'timetracker-timesheets',
        'view': '0',
        xget: '',
        'offset': 1,
        'users_id': undefined,
        'from_w': undefined,
        'to_w': undefined,
        'status': '5'
    };
    $scope.expanded_users = [];
    $scope.views = [{
        'id': '0',
        'name': helper.getLanguage('Last 5 weeks')
    }, {
        'id': '1',
        'name': helper.getLanguage('This month')
    }, {
        'id': '2',
        'name': helper.getLanguage('Last month')
    }, {
        'id': '3',
        'name': helper.getLanguage('Custom')
    }];
    $scope.statuses = [{
        'id': '0',
        'name': helper.getLanguage('All Statuses')
    }, {
        'id': '1',
        'name': helper.getLanguage('Unsubmited')
    }, {
        'id': '2',
        'name': helper.getLanguage('Submitted')
    }, {
        'id': '3',
        'name': helper.getLanguage('Rejected')
    }, {
        'id': '4',
        'name': helper.getLanguage('Approved')
    }, {
        'id': '5',
        'name': helper.getLanguage('Not Approved')
    }];
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        from_w: !1,
        to_w: !1,
    }
    $scope.listObj = {
        check_add_all: !1
    };

    function shrink_row() {
        for (x in $scope.expanded_users) {
            angular.element('.table_orders.unapproved').find("span.glyphicon-minus[data-mainid='" + $scope.expanded_users[x] + "']").click()
        }
    }
    $scope.renderList = function(d) {
        $scope.showAlerts(d);
        if (d && d.data) {
            $scope.item = d.data;
            $scope.search.view = d.data.view;
            $timeout(function() {
                shrink_row()
            })
        }
    };
    $scope.searchThing = function() {
        var data = angular.copy($scope.search);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', data, function(r) {
            $scope.renderList(r)
        })
    }
    $scope.toggleSearchButtons = function(item, val) {
        $scope.search[item] = val;
        if ($scope.search.from_w == undefined && $scope.search.to_w == undefined && $scope.search.view == '3') {
            return !1
        }
        var data = angular.copy($scope.search);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', data, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.action = function(item, action) {
        var params = angular.copy($scope.search);
        for (x in item[action]) {
            params[x] = item[action][x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, $scope.renderList)
    }
    $scope.getUnapproved = function() {
        $scope.search.users_id = undefined;
        $scope.search.from_w = undefined;
        $scope.search.to_w = undefined;
        $scope.expanded_users = [];
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php?do=timetracker-timesheets&view=0', $scope.renderList)
    }
    $scope.getApproved = function() {
        $scope.search.users_id = undefined;
        $scope.search.from_w = undefined;
        $scope.search.to_w = undefined;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php?do=timetracker-timesheets&view=1', $scope.renderList)
    }
    $scope.listMore = function(event, user_id) {
        var _this = event.target;
        angular.element(_this).addClass('hidden');
        angular.element(_this).next().removeClass('hidden');
        angular.element('.table_orders.unapproved').find("tr.subline[data-subid='" + user_id + "']").removeClass('hidden');
        var key_user = $scope.expanded_users.indexOf(user_id);
        $scope.expanded_users.splice(key_user, 1)
    }
    $scope.listLess = function(event, user_id) {
        var _this = event.target;
        angular.element(_this).addClass('hidden');
        angular.element(_this).prev().removeClass('hidden');
        angular.element('.table_orders.unapproved').find("tr.subline[data-subid='" + user_id + "']").addClass('hidden');
        var key_user = $scope.expanded_users.indexOf(user_id);
        if (key_user == -1) {
            $scope.expanded_users.push(user_id)
        }
    }
    $scope.approve_partial = function(time) {
        var data = {
            'start': time.START_W,
            'user_id': time.USER_ID,
            'partial_sub': time.partial_sub
        };
        openModal("approve_partial", data, "md")
    }
    $scope.reject_time = function(time) {
        var data = angular.copy(time);
        openModal("reject_time", data, "md")
    }
    $scope.show_info = function(time) {
        var data = angular.copy(time);
        openModal("show_time_info", data, "lg")
    }
    $scope.userCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Users'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.users_id = (value == undefined || value == '') ? '' : this.options[value].id;
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.viewCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Periods'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value != '' || value != undefined) {
                if (this.options[value].id < 3) {
                    $scope.search.from_w = undefined;
                    $scope.search.to_w = undefined;
                    $scope.searchThing()
                }
            }
        },
        maxItems: 1
    };
    $scope.statusCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Statuses'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value != '' || value != undefined) {
                $scope.searchThing()
            }
        },
        maxItems: 1
    };
    $scope.delete_unsubmitted = function(item) {
        var obj = {};
        obj.do = 'timetracker-timesheets-time-delete_empty_row';
        obj.user_id = item.USER_ID;
        obj.week_start = item.START_W;
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.renderList(r)
        })
    }
    $scope.saveForAction = function() {
        for (x in $scope.item.project_row) {
            for (y in $scope.item.project_row[x].time_row) {
                if ($scope.item.project_row[x].time_row[y].VIEW_LINK) {
                    $scope.item.project_row[x].time_row[y].check_add_action = $scope.listObj.check_add_all
                }
            }
        }
    }
    $scope.approveBulk = function() {
        var bulkApprove = [];
        for (x in $scope.item.project_row) {
            for (y in $scope.item.project_row[x].time_row) {
                if ($scope.item.project_row[x].time_row[y].check_add_action) {
                    var tmp_line = angular.copy($scope.item.project_row[x].time_row[y].APPROVE_LINK);
                    delete tmp_line.list;
                    bulkApprove.push(tmp_line)
                }
            }
        }
        if (bulkApprove.length) {
            approbeBulkLine(bulkApprove)
        }
    }

    function approbeBulkLine(array) {
        if (array.length) {
            //angular.element('.loading_wrap').removeClass('hidden');
            var single_el = angular.copy(array[0]);
            helper.doRequest('post', 'index.php', single_el, function(r) {
                array.shift();
                approbeBulkLine(array)
            })
        } else {
            $scope.listObj.check_add_all = !1;
            $scope.searchThing()
        }
    }

    $scope.addTask = function(id) {
        if (!$scope.item.can_do) {
            return !1
        }
        openModal('add_task', id, 'md')
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'approve_partial':
                var iTemplate = 'ApprovePartial';
                var ctas = 'time';
                break;
            case 'reject_time':
                var iTemplate = 'RejectTime';
                var ctas = 'note';
                break;
            case 'show_time_info':
                var iTemplate = 'ShowTimeInfo';
                var ctas = 'info';
                break;
            case 'add_task':
                var iTemplate = 'TimeTaskEdit';
                var ctas = 'vm';
                break
        }
        var params = {
            template: 'tTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'approve_partial':
                params.params = {
                    'do': 'timetracker-approve_timesheet',
                    'user_id': item.user_id,
                    'start': item.start,
                    'partial_sub': item.partial_sub
                };
                params.callback = function() {
                    $scope.searchThing()
                }
                break;
            case 'reject_time':
                params.item = item;
                params.callback = function() {
                    $scope.searchThing()
                }
                break;
            case 'show_time_info':
                params.params = {
                    'do': 'timetracker-timesheet',
                    'start': item.WEEK,
                    'user_id': item.USER_ID
                };
                params.callback = function() {
                    $scope.searchThing()
                }
                break;
            case 'add_task':
                params.params = {
                    'do': 'timetracker-tasks',
                    'xget': 'task',
                    'task_id': item
                };
                params.callback = function(r) {
                    if(r && r.data){
                        $scope.showAlerts(r)
                    }
                }
                break
        }
        modalFc.open(params)
    }
    list.init($scope, 'timetracker-timesheets', $scope.renderList)
}]).controller('ApprovePartialModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var time = this;
    time.obj = data;
    time.showAlerts = function(d) {
        helper.showAlerts(time, d)
    }
    time.cancel = function() {
        $uibModalInstance.close()
    }
    time.ok = function() {
        var data = angular.copy(time.obj);
        data.do = 'timetracker--time-approve';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            time.showAlerts(r)
        })
    }
}]).controller('RejectTimeModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var note = this;
    note.obj = data;
    note.showAlerts = function(d) {
        helper.showAlerts(note, d)
    }
    note.cancel = function() {
        $uibModalInstance.close()
    }
    note.ok = function() {
        var obj = angular.copy(note.obj);
        for (x in note.obj.ARCHIVE_LINK) {
            obj[x] = note.obj.ARCHIVE_LINK[x]
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            note.showAlerts(r)
        })
    }
}]).controller('ShowTimeInfoModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, helper, $uibModalInstance, data, modalFc) {
    var info = this;
    info.obj = data;
    info.activeType = 2;
    info.types = [{
        name: 'Weekly',
        id: 2,
        active: 'active'
    }, {
        name: 'Monthly',
        id: 3,
        active: ''
    }];
    info.subtotal = [{
        'total_h_wo_0': 0,
        'total_h_0': '',
        'total_d_0': ''
    }, {
        'total_h_wo_1': 0,
        'total_h_1': '',
        'total_d_1': ''
    }, {
        'total_h_wo_2': 0,
        'total_h_2': '',
        'total_d_2': ''
    }, {
        'total_h_wo_3': 0,
        'total_h_3': '',
        'total_d_3': ''
    }, {
        'total_h_wo_4': 0,
        'total_h_4': '',
        'total_d_4': ''
    }, {
        'total_h_wo_5': 0,
        'total_h_5': '',
        'total_d_5': ''
    }, {
        'total_h_wo_6': 0,
        'total_h_6': '',
        'total_d_6': ''
    }];
    info.timesheet = {
        start: 0
    };
    info.auto = {
        project: [],
        task: [],
        customer: [],
    };
    info.auto.project = data.projects_list;
    info.auto.customer = data.customers_list;
    info.auto.task = data.tasks_list;
    init_calculus();

    function init_calculus() {
        for (var i = 0; i < 7; i++) {
            var subtotal_h = 0;
            var subtotal_d = 0;
            for (x in info.obj.task_row) {
                if (info.obj.task_row[x].project_type == '0') {
                    subtotal_h += parseFloat(info.obj.task_row[x].task_day_row[i].HOURS)
                } else {
                    subtotal_d += parseFloat(info.obj.task_row[x].task_day_row[i].HOURS)
                }
            }
            if (subtotal_h > 0) {
                info.subtotal[i]['total_h_wo_' + i] = subtotal_h;
                info.subtotal[i]['total_h_' + i] = corect_val(subtotal_h)
            } else {
                info.subtotal[i]['total_h_wo_' + i] = 0;
                info.subtotal[i]['total_h_' + i] = ''
            }
            if (subtotal_d > 0) {
                info.subtotal[i]['total_d_' + i] = subtotal_d
            } else {
                info.subtotal[i]['total_d_' + i] = ''
            }
        }
        var total_h = 0;
        var total_d = 0;
        for (x in info.obj.task_row) {
            var line_h = 0;
            var line_d = 0;
            if (info.obj.task_row[x].project_type == '0') {
                for (y in info.obj.task_row[x].task_day_row) {
                    if (info.obj.task_row[x].task_day_row[y].HOURS != '') {
                        line_h += parseFloat(info.obj.task_row[x].task_day_row[y].HOURS)
                    }
                }
            } else {
                for (y in info.obj.task_row[x].task_day_row) {
                    if (info.obj.task_row[x].task_day_row[y].HOURS != '') {
                        line_d += parseFloat(info.obj.task_row[x].task_day_row[y].HOURS)
                    }
                }
            }
            total_h += line_h;
            total_d += line_d;
            if (line_h > 0) {
                info.obj.task_row[x].line_h_wo = line_h;
                info.obj.task_row[x].line_h = corect_val(line_h)
            } else {
                info.obj.task_row[x].line_h_wo = 0;
                info.obj.task_row[x].line_h = ''
            }
            if (line_d > 0) {
                info.obj.task_row[x].line_d = line_d
            } else {
                info.obj.task_row[x].line_d = ''
            }
        }
        if (total_h > 0) {
            info.obj.total_h_wo = total_h;
            info.obj.total_h = corect_val(total_h)
        } else {
            info.obj.total_h_wo = 0;
            info.obj.total_h = ''
        }
        if (total_d > 0) {
            info.obj.total_d = total_d
        } else {
            info.obj.total_d = ''
        }
    }
    info.init_calculus = init_calculus;
    info.showAlerts = function(d) {
        helper.showAlerts(info, d)
    }
    info.cancel = function() {
        $uibModalInstance.close()
    }
    info.renderPage = function(d) {
        if (d.data) {
            info.obj = d.data;
            if (info.obj.tab == 1 && info.activeType < 3) {
                info.auto.project = d.data.projects_list;
                info.auto.customer = d.data.customers_list;
                info.auto.task = d.data.tasks_list;
                if (info.activeType == 2) {
                    init_calculus()
                }
            }
        }
    }
    info.getTimesheet = function() {
        info.activeType = 2;
        helper.doRequest('get', 'index.php?do=timetracker-timesheet&start=' + info.obj.start + '&user_id=' + info.obj.user_id, info.renderPage)
    }
    info.getExpense = function() {
        helper.doRequest('get', 'index.php?do=timetracker-timesheet_expense&start=' + info.obj.start + '&user_id=' + info.obj.user_id, info.renderPage)
    }
    info.customerCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Users'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 1
    };
    info.typeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Overview'),
        create: !1,
        onChange(value) {
            var type_r = '';
            switch (value) {
                case '1':
                    type_r = 'Day';
                    break;
                case '2':
                    type_r = 'Week';
                    break;
                case '3':
                    type_r = 'Month';
                    break
            }
            info.timesheet.start = 0;
           // angular.element('.loading_alt').removeClass('hidden');
            helper.doRequest('get', 'index.php', {
                'do': 'timetracker-timesheet',
                'xget': type_r,
                'start': info.obj.start,
                'user_id': info.obj.user_id
            }, function(r) {
                //angular.element('.loading_alt').addClass('hidden');
                info.renderPage(r)
            })
        },
        maxItems: 1
    };
    info.projectAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select project'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value == undefined || value == '') {
                var data = {
                    'do': 'timetracker-timesheet',
                    xget: 'tasks_list'
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    info.auto.task = r.data
                })
            } else if (value != undefined) {
                var data = {
                    'do': 'timetracker-timesheet',
                    xget: 'tasks_list',
                    project_id: this.options[value].id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    info.auto.task = r.data
                })
            }
        },
        maxItems: 1
    };
    info.taskAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select task'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 100
    };
    info.changeDayData = function(cell, increment) {
        if ((cell.HOURSB == '1' && increment > 0) || (cell.HOURSB == '0' && increment < 0)) {
            return !1
        } else {
            if (isNaN(cell.HOURSB) || cell.HOURSB == '') {
                cell.HOURSB = '0';
                cell.HOURS = '0'
            }
            cell.HOURSB = parseFloat(cell.HOURSB) + increment;
            cell.HOURS = parseFloat(cell.HOURS) + increment
        }
        init_calculus();
        info.saveTimesheet(cell)
    }
    info.changeHourData = function(cell) {
        if (cell.IS_BILLED || cell.IS_APPROVED || cell.IS_SUBMITED || cell.IS_CLOSED) {
            return !1
        }
        cell.HOURS = isNaN(cell.HOURSB) ? convert_value(cell.HOURSB) : cell.HOURSB;
        cell.HOURSB = corect_val(cell.HOURSB);
        init_calculus();
        info.saveTimesheet(cell)
    }
    info.changeHourDataKey = function(event, cell) {
        if (event.keyCode == 13) {
            if (cell.IS_BILLED || cell.IS_APPROVED || cell.IS_SUBMITED || cell.IS_CLOSED) {
                return !1
            } else {
                info.changeHourData(cell)
            }
        }
    }
    info.showNote = function(cell) {
        openModal("show_note", cell, "md")
    }
    info.saveTimesheet = function(cell) {
        var data = angular.copy(cell);
        data.do = 'timetracker--time-add_hour_simple';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            info.showAlerts(r)
        })
    }
    info.deleteEntry = function(index) {
        var data = angular.copy(info.obj.task_row[index]);
        data.do = 'timetracker-timesheet-time-delete_time';
        data.start_date = info.obj.start;
        data.user_id = info.obj.user_id;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            info.showAlerts(r);
            if (r.success) {
                info.renderPage(r)
            }
        })
    }
    info.submitTime = function() {
        var data = angular.copy(info.obj);
        openModal("submit_time", data, "md")
    }
    info.addTask = function(type) {
        var data = {
            'activeType': info.activeType,
            'auto_project': angular.copy(info.auto.project),
            'auto_customer': angular.copy(info.auto.customer),
            'auto_task': angular.copy(info.auto.task),
            'item': angular.copy(info.obj),
            't_type': type
        };
        openModal('select_task', data, 'md')
    }
    info.unlock_time = function(task) {
        if (!task.approved && !task.billed && info.obj.can_unlock) {
            var obj = {
                "error": !1,
                "notice": {
                    "notice": helper.getLanguage("Entries unlocked")
                },
                "success": !1
            };
            info.showAlerts(obj);
            for (x in task.task_day_row) {
                task.task_day_row[x].IS_APPROVED = !1;
                task.task_day_row[x].IS_SUBMITED = !1;
                task.task_day_row[x].unlocked_manually = !0
            }
        }
    }
    info.showReciept = function(exp) {
        openModal("show_reciept", exp.IMG_HREF, "md")
    }
    info.deleteReciept = function(exp) {
        info.timesheet.start = 0;
        var data = {};
        data.id = exp.E_ID;
        data.do = 'timetracker-timesheet_expense-time-delete_image';
        data.start_date = info.obj.start;
        data.user_id = info.obj.user_id;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            info.renderPage(r);
            info.showAlerts(r)
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'show_note':
                var iTemplate = 'TimesheetNoteProject';
                var ctas = 'note';
                break;
            case 'submit_time':
                var iTemplate = 'SubmitTimeProject';
                var ctas = 'time';
                break;
            case 'select_task':
                var iTemplate = 'SelectTaskProject';
                var ctas = 'tsk';
                break;
            case 'show_reciept':
                var iTemplate = 'ShowRecieptProject';
                var ctas = 'rec';
                break
        }
        var params = {
            template: 'tTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'show_note':
                params.params = {
                    'do': 'timetracker-xcomment',
                    'task_time_id': item.TASK_DAY_ID,
                    'unlocked_manually': item.unlocked_manually
                };
                params.callback = function() {
                    info.timesheet.start = 0;
                    //angular.element('.loading_alt').removeClass('hidden');
                    helper.doRequest('get', 'index.php', {
                        'do': 'timetracker-timesheet',
                        'xget': 'Week',
                        'start': info.obj.start,
                        'user_id': info.obj.user_id
                    }, function(r) {
                        //angular.element('.loading_alt').addClass('hidden');
                        info.renderPage(r)
                    })
                }
                break;
            case 'submit_time':
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        info.renderPage(d)
                    }
                }
                break;
            case 'select_task':
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        info.timesheet.start = 0;
                        info.renderPage(d);
                        info.showAlerts(d)
                    }
                }
                break;
            case 'show_reciept':
                params.item = item;
                break
        }
        modalFc.open(params)
    }
}]).controller('TimesheetNoteProjectModalCtrl', ['$scope', '$timeout', '$compile', 'helper', '$uibModalInstance', 'data', function($scope, $timeout, $compile, helper, $uibModalInstance, data) {
    var note = this;
    note.obj = data;
    if(note.obj.type == '0'){
        for(let x in note.obj.rows){
            if(note.obj.rows[x].break && note.obj.rows[x].break!='0:00'){
                note.obj.rows[x].break=convert_to_time(note.obj.rows[x].break);
            }else{
                note.obj.rows[x].break=undefined;
            }
            if(note.obj.rows[x].end_time && note.obj.rows[x].end_time!='0:00'){
                note.obj.rows[x].end_time=convert_to_time(note.obj.rows[x].end_time);
            }else{
                note.obj.rows[x].end_time=undefined;
            }
            if(note.obj.rows[x].start_time && note.obj.rows[x].start_time!='0:00'){
                note.obj.rows[x].start_time=convert_to_time(note.obj.rows[x].start_time);
            }else{
                note.obj.rows[x].start_time=undefined;
            }
        }
    }
    
    note.search_day = {
        search: '',
        'do': 'timetracker-xcomment',
        view: 0,
        xget: '',
        offset: 1
    };

    function calculate_hours() {
        for (x in note.obj.rows) {
            if (!note.obj.rows[x].is_total_h) {
                let start_time = note.obj.rows[x].start_time == undefined ? 0 : parseFloat(convert_value(note.obj.rows[x].start_time.getHours()+':'+note.obj.rows[x].start_time.getMinutes()));
                let end_time = note.obj.rows[x].end_time == undefined ? 0 : parseFloat(convert_value(note.obj.rows[x].end_time.getHours()+':'+note.obj.rows[x].end_time.getMinutes()));
                let break_time = note.obj.rows[x].break == undefined ? 0 : parseFloat(convert_value(note.obj.rows[x].break.getHours()+':'+note.obj.rows[x].break.getMinutes()));
                var line_h = end_time - start_time - break_time;
                if (line_h > 0) {
                    note.obj.rows[x].line_h_wo = line_h;
                    note.obj.rows[x].line_h = corect_val(line_h)
                } else {
                    note.obj.rows[x].line_h_wo = 0;
                    note.obj.rows[x].line_h = ''
                }
            }
        }
    }
    if (note.obj.type == '0') {
        $timeout(function() {
            angular.element('.modal-dialog').removeClass('modal-md').addClass('modal-lg');
            calculate_hours()
        })
    }
    note.calculate_hours = calculate_hours;
    note.showAlerts = function(d) {
        helper.showAlerts(note, d)
    }
    note.orderBY = function(p) {
        if (p == note.ord && note.reverse) {
            note.ord = '';
            note.reverse = !1
        } else {
            if (p == note.ord) {
                note.reverse = !note.reverse
            } else {
                note.reverse = !1
            }
            note.ord = p
        }
        var params = {
            view: note.activeView,
            offset: note.search_day.offset,
            desc: note.reverse,
            order_by: note.ord,
            task_time_id: note.obj.task_time_id
        };
        for (x in note.search_day) {
            params[x] = note.search_day[x]
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            note.obj = r.data
            if(note.obj.type == '0'){
                for(let x in note.obj.rows){
                    if(note.obj.rows[x].break && note.obj.rows[x].break!='0:00'){
                        note.obj.rows[x].break=convert_to_time(note.obj.rows[x].break);
                    }else{
                        note.obj.rows[x].break=undefined;
                    }
                    if(note.obj.rows[x].end_time && note.obj.rows[x].end_time!='0:00'){
                        note.obj.rows[x].end_time=convert_to_time(note.obj.rows[x].end_time);
                    }else{
                        note.obj.rows[x].end_time=undefined;
                    }
                    if(note.obj.rows[x].start_time && note.obj.rows[x].start_time!='0:00'){
                        note.obj.rows[x].start_time=convert_to_time(note.obj.rows[x].start_time);
                    }else{
                        note.obj.rows[x].start_time=undefined;
                    }
                }
            }
        })
    };
    note.cancel = function() {
        $uibModalInstance.close()
    }
    note.addTimeRow = function() {
        //angular.element('.loading_alt').removeClass('hidden');
        var data = angular.copy(note.obj);
        data.do = 'timetracker-xcomment-time-addSubHoursPlus';
        helper.doRequest('post', 'index.php', data, function(r) {
            note.obj = r.data
            if(note.obj.type == '0'){
                for(let x in note.obj.rows){
                    if(note.obj.rows[x].break && note.obj.rows[x].break!='0:00'){
                        note.obj.rows[x].break=convert_to_time(note.obj.rows[x].break);
                    }else{
                        note.obj.rows[x].break=undefined;
                    }
                    if(note.obj.rows[x].end_time && note.obj.rows[x].end_time!='0:00'){
                        note.obj.rows[x].end_time=convert_to_time(note.obj.rows[x].end_time);
                    }else{
                        note.obj.rows[x].end_time=undefined;
                    }
                    if(note.obj.rows[x].start_time && note.obj.rows[x].start_time!='0:00'){
                        note.obj.rows[x].start_time=convert_to_time(note.obj.rows[x].start_time);
                    }else{
                        note.obj.rows[x].start_time=undefined;
                    }
                }
            }
        })
    }
    note.deleteEntry = function(index) {
        if (note.obj.disabled) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Cannot delete submitted data")
                },
                "notice": !1,
                "success": !1
            };
            note.showAlerts(obj);
            return !1
        }
        if (note.obj.rows[index].id == '0') {
            note.obj.rows.splice(index, 1)
        } else {
            helper.doRequest('post', 'index.php', {
                'do': 'timetracker--time-deleteSubHours',
                'id': note.obj.rows[index].id
            }, function(r) {
                note.showAlerts(r);
                if (r.success) {
                    note.obj.rows.splice(index, 1)
                }
            })
        }
    }
    note.changeTotalHoursLine = function(index) {
        note.obj.rows[index].is_total_h = !0;
        note.obj.rows[index].line_h = corect_val(note.obj.rows[index].line_h);
        note.obj.rows[index].line_h_wo = convert_value(note.obj.rows[index].line_h);
        note.obj.rows[index].start_time = '0:00';
        note.obj.rows[index].end_time = '0:00';
        note.obj.rows[index].break = '0:00';
        var data = {};
        data.do = 'timetracker--time-updateDayTotalHours';
        data.task_time_id = note.obj.task_time_id;
        data.id = note.obj.rows[index].id;
        data.value = note.obj.rows[index].line_h_wo;
        helper.doRequest('post', 'index.php', data, function(r) {
            note.showAlerts(r)
        })
    }
    note.savepickerData = function(index) {
        note.obj.rows[index].line_h = '0:00';
        note.obj.rows[index].line_h_wo = '0';
        note.obj.rows[index].is_total_h = !1;
        //note.obj.rows[index].start_time = corect_val(note.obj.rows[index].start_time);
        //note.obj.rows[index].end_time = corect_val(note.obj.rows[index].end_time);
        //note.obj.rows[index].break = corect_val(note.obj.rows[index].break);
        var start_time = note.obj.rows[index].start_time == undefined ? 0 : parseFloat(convert_value(note.obj.rows[index].start_time.getHours()+':'+note.obj.rows[index].start_time.getMinutes()));
        var end_time = note.obj.rows[index].end_time == undefined ? 0 : parseFloat(convert_value(note.obj.rows[index].end_time.getHours()+':'+note.obj.rows[index].end_time.getMinutes()));
        var break_time = note.obj.rows[index].break == undefined ? 0 : parseFloat(convert_value(note.obj.rows[index].break.getHours()+':'+note.obj.rows[index].break.getMinutes()));
        if (start_time == 0 || end_time == 0) {
            return !1
        } else {
            var data = {};
            data.do = 'timetracker--time-updateDayHours';
            data.start_time = start_time;
            data.end_time = end_time;
            data.break = break_time;
            data.id = note.obj.rows[index].id;
            data.task_time_id = note.obj.task_time_id;
            helper.doRequest('post', 'index.php', data, function(r) {
                note.showAlerts(r);
                note.calculate_hours()
            })
        }
    }
    note.showTime = function(event, index, model, align) {
        var _this = event.target;
        angular.element('.table_orders').find('timepick').remove();
        var compiledHTML = $compile('<timepick nindex="' + index + '" nmodel="' + model + '" nalign="' + align + '" "></timepick>')($scope);
        angular.element(_this).parent().after(compiledHTML)
    }
    note.updateNote = function(index) {
        var data = {};
        data.do = 'timetracker--time-updateDayComment';
        data.id = note.obj.rows[index].id;
        data.notes = note.obj.rows[index].comment;
        helper.doRequest('post', 'index.php', data, function(r) {
            note.showAlerts(r)
        })
    }
    note.ok = function() {
        var data = angular.copy(note.obj);
        data.do = data.do_next;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            note.showAlerts(r);
            if (r.success) {
                note.obj = r.data
                if(note.obj.type == '0'){
                    for(let x in note.obj.rows){
                        if(note.obj.rows[x].break && note.obj.rows[x].break!='0:00'){
                            note.obj.rows[x].break=convert_to_time(note.obj.rows[x].break);
                        }else{
                            note.obj.rows[x].break=undefined;
                        }
                        if(note.obj.rows[x].end_time && note.obj.rows[x].end_time!='0:00'){
                            note.obj.rows[x].end_time=convert_to_time(note.obj.rows[x].end_time);
                        }else{
                            note.obj.rows[x].end_time=undefined;
                        }
                        if(note.obj.rows[x].start_time && note.obj.rows[x].start_time!='0:00'){
                            note.obj.rows[x].start_time=convert_to_time(note.obj.rows[x].start_time);
                        }else{
                            note.obj.rows[x].start_time=undefined;
                        }
                    }
                }
            }
        })
    }
    note.saveTimeLogs = function() {
        var data = {};
        data.rows = note.obj.rows;
        for (x in note.obj.rows) {
            data.rows[x].line_h_wo = '0';
            data.rows[x].is_total_h = !1;
            data.rows[x].start_time_i = note.obj.rows[x].start_time == undefined ? 0 : parseFloat(convert_value(corect_val(note.obj.rows[x].start_time.getHours()+':'+note.obj.rows[x].start_time.getMinutes())));
            data.rows[x].end_time_i = note.obj.rows[x].end_time == undefined ? 0 : parseFloat(convert_value(corect_val(note.obj.rows[x].end_time.getHours()+':'+note.obj.rows[x].end_time.getMinutes())));
            data.rows[x].break_i = note.obj.rows[x].break == undefined ? 0 : parseFloat(convert_value(corect_val(note.obj.rows[x].break.getHours()+':'+note.obj.rows[x].break.getMinutes())));
            data.rows[x].line_h_i = parseFloat(convert_value(corect_val(note.obj.rows[x].line_h)))
        }
        data.task_time_id = note.obj.task_time_id;
        data.do = 'misc--time-saveTimeLogs';
        helper.doRequest('post', 'index.php', data, function(r) {
            note.showAlerts(r)
        })
    }
    note.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        menubar: !1,
        autoresize_bottom_margin: 0,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        statusbar: !1,
        toolbar: ["styleselect | bold italic | bullist numlist "]
    }
}]).controller('SubmitTimeProjectModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var time = this;
    time.obj = data;
    time.obj.partial_full_radio = '1';
    time.showAlerts = function(d) {
        helper.showAlerts(time, d)
    }
    time.cancel = function() {
        $uibModalInstance.close()
    }
    time.showPartialTime = function() {
        time.showPartial = !0;
        time.obj.partial_end = time.obj.partial_submit[parseInt(time.obj.TODAY_nr) - 1].id.toString()
    }
    time.ok = function() {
        var data = angular.copy(time.obj);
        data.do = 'timetracker-timesheet-time-submit_hour';
        if (time.obj.partial_full_radio == '2') {
            data.week_e = time.obj.partial_end
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            time.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('SelectTaskProjectModalCtrl', ['$scope', 'helper', '$timeout', '$uibModalInstance', 'data', 'modalFc', function($scope, helper, $timeout, $uibModalInstance, data, modalFc) {
    var tsk = this,
        typePromise;
    tsk.obj = data;
    tsk.projectAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select project'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value == undefined || value == '') {
                var obj = {
                    'do': 'timetracker-timesheet',
                    xget: 'tasks_list'
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    tsk.obj.auto_task = r.data
                })
            } else if (value != undefined) {
                var obj = {
                    'do': 'timetracker-timesheet',
                    xget: 'tasks_list',
                    project_id: this.options[value].id
                };
                if (tsk.obj.activeType == 1) {
                    obj.tasks_ids = tsk.obj.item.tasks_ids
                }
                helper.doRequest('get', 'index.php', obj, function(r) {
                    tsk.obj.auto_task = r.data
                })
            }
        },
        onType(str) {
            var selectize = angular.element('#p_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'timetracker-timesheet',
                    xget: 'projects_list',
                    term: str,
                    user_id: tsk.obj.item.user_id
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    if (r.data.length) {
                        tsk.obj.auto_project = r.data;
                        selectize.open()
                    }
                })
            }, 300)
        },
        maxItems: 1
    };
    tsk.customerAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select customer'),
        create: !1,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-8">' + '<strong class="text-ellipsis">' + item.symbol + '  ' + escape(item.top) + '</strong>' + '<small class="text-ellipsis"><span class="glyphicon glyphicon-map-marker"></span>' + escape(item.bottom) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-muted text-right">' + '<small class="text-ellipsis"> ' + escape(item.right) + '</small>' + '</div>' + '</div>' + '</div>';
                if (item.id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Create a new Customer') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.top) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined || value == '') {
                var obj = {
                    'do': 'timetracker-timesheet',
                    xget: 'tasks_list'
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    tsk.obj.auto_task = r.data
                })
            } else if (value != undefined) {
                if (value == '99999999999') {
                    tsk.obj.item.c_id = '';
                    openModal('add_customer', '', 'md');
                    return !1
                } else {
                    var obj = {
                        'do': 'timetracker-timesheet',
                        xget: 'tasks_list',
                        c_id: this.options[value].id
                    };
                    helper.doRequest('get', 'index.php', obj, function(r) {
                        tsk.obj.auto_task = r.data
                    })
                }
            }
        },
        onType(str) {
            var selectize = angular.element('#c_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'timetracker-timesheet',
                    xget: 'cc_list',
                    term: str
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    if (r.data.length) {
                        tsk.obj.auto_customer = r.data;
                        selectize.open()
                    }
                })
            }, 300)
        },
        maxItems: 1
    };
    tsk.taskAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select task'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<strong class="text-ellipsis">' + escape(item.name) + '</strong>' + '</div>' + '</div>' + '</div>';
                if (item.id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Create a new Task') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onChange(value) {
            if (Array.isArray(value) && value[value.length - 1] == '99999999999') {
                openModal('add_task', '', 'md');
                var selectize = angular.element('#task_id')[0].selectize;
                selectize.removeItem('99999999999');
                return !1
            }
        },
        maxItems: 100
    };
    tsk.showAlerts = function(d) {
        helper.showAlerts(tsk, d)
    }
    tsk.cancel = function() {
        $uibModalInstance.close()
    }
    tsk.ok = function() {
        if (tsk.obj.t_type == '1') {
            if (!tsk.obj.item.c_id || tsk.obj.item.tasks_id == undefined || tsk.obj.item.tasks_id[0] == '0') {
                var obj = {
                    "error": {
                        "error": helper.getLanguage("Select customer and task")
                    },
                    "notice": !1,
                    "success": !1
                };
                tsk.showAlerts(obj);
                return !1
            }
        } else {
            if (!tsk.obj.item.project_id || tsk.obj.item.tasks_id == undefined || tsk.obj.item.tasks_id[0] == '0') {
                var obj = {
                    "error": {
                        "error": helper.getLanguage("Select project and task")
                    },
                    "notice": !1,
                    "success": !1
                };
                tsk.showAlerts(obj);
                return !1
            }
        }
        if (tsk.obj.activeType == 1) {
            var tasks_ids = angular.copy(tsk.obj.item.tasks_id);
            multiTime(tasks_ids, tsk.obj.t_type)
        } else {
            var data = angular.copy(tsk.obj.item);
            data.do = 'timetracker-timesheet-time-add_tasks';
            data.xget = 'Week';
            //angular.element('.loading_alt').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                //angular.element('.loading_alt').addClass('hidden');
                if (r.success) {
                    $uibModalInstance.close(r)
                } else {
                    tsk.showAlerts(r)
                }
            })
        }
    }

    function multiTime(array, type) {
        if (array.length) {
            //angular.element('.loading_alt').removeClass('hidden');
            var data = {};
            data.do = 'timetracker--time-addSubHoursPlus';
            data.date = tsk.obj.item.date;
            if (type == '1') {
                data.c_id = tsk.obj.item.c_id
            } else {
                data.project_id = tsk.obj.item.project_id
            }
            data.task_id = array[0];
            data.user_id = tsk.obj.item.user_id;
            helper.doRequest('post', 'index.php', data, function(r) {
                array.shift();
                multiTime(array, type)
            })
        } else {
            helper.doRequest('get', 'index.php', {
                'do': 'timetracker-timesheet',
                'xget': 'Day',
                'start': tsk.obj.item.start,
                'user_id': tsk.obj.item.user_id
            }, function(r) {
                //angular.element('.loading_alt').addClass('hidden');
                $uibModalInstance.close(r)
            })
        }
    }

    function openModal(type, item, size) {
        var params = {
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'add_customer':
                params.template = 'miscTemplate/AddCustomerModal';
                params.ctrl = 'AddCustomerModalCtrl';
                params.ctrlAs = 'vmMod';
                params.params = {
                    'do': 'misc-cc_new'
                };
                params.callback = function(r) {
                    if (r && r.data.buyer_id) {
                        var obj = {
                            'do': 'timetracker-timesheet',
                            xget: 'cc_list',
                            term: r.data.buyer_name
                        };
                        helper.doRequest('get', 'index.php', obj, function(o) {
                            if (o.data.length) {
                                var selectize = angular.element('#c_list')[0].selectize;
                                tsk.obj.auto_customer = o.data;
                                $timeout(function() {
                                    tsk.obj.item.c_id = r.data.buyer_id;
                                    selectize.close();
                                    var obj1 = {
                                        'do': 'timetracker-timesheet',
                                        xget: 'tasks_list',
                                        c_id: r.data.buyer_id
                                    };
                                    helper.doRequest('get', 'index.php', obj1, function(p) {
                                        tsk.obj.auto_task = p.data
                                    })
                                })
                            }
                        })
                    }
                }
                break;
            case 'add_task':
                params.template = 'miscTemplate/TimeTaskAddModal';
                params.ctrl = 'TimeTaskAddModalCtrl';
                params.ctrlAs = 'vm';
                params.callback = function(r) {
                    if (r.data.task_id) {
                        var obj = {
                            'do': 'timetracker-timesheet',
                            xget: 'tasks_list',
                            c_id: tsk.obj.item.c_id
                        };
                        helper.doRequest('get', 'index.php', obj, function(o) {
                            if (o.data.length) {
                                tsk.obj.auto_task = o.data
                            }
                        })
                    }
                }
                break
        }
        modalFc.open(params)
    }
}]).controller('ShowRecieptProjectModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var rec = this;
    rec.img = data;
    rec.showAlerts = function(d) {
        helper.showAlerts(rec, d)
    }
    rec.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('ProjectAddStepsModalCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'data', function($scope, $timeout, helper, $uibModalInstance, data) {
    var add = this;
    add.obj = data;
    add.obj.initial_start = !1;
    add.step = 0;
    add.close_btn = helper.getLanguage('Cancel');
    add.ok_btn = helper.getLanguage('Next Step');
    add.showAlerts = function(d) {
        helper.showAlerts(add, d)
    }
    add.close_modal = function() {
        $uibModalInstance.dismiss()
    }
    add.showTimeOptions = function() {
        add.obj.showTxt.type_time = !0;
        add.obj.showTxt.type_fixed = !1
    }
    add.showFixedOptions = function() {
        add.obj.showTxt.type_time = !1;
        add.obj.showTxt.type_fixed = !0
    }
    add.obj.showHourly = function() {
        add.obj.showTxt.hourly = !0;
        add.obj.showTxt.daily = !1
    }
    add.obj.showDaily = function() {
        add.obj.showTxt.hourly = !1;
        add.obj.showTxt.daily = !0
    }
    add.obj.showFixedDates = function(id) {
        if (id == '5') {
            add.obj.showTxt.fixed_fee = !1
        } else {
            add.obj.showTxt.fixed_fee = !0
        }
    }
    add.format = 'dd/MM/yyyy';
    add.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    add.datePickOpen = {
        start_date: !1,
        end_date: !1,
    }
    add.cancel = function() {
        if (add.step == 0) {
            $uibModalInstance.dismiss()
        } else {
            add.step--;
            if (add.step == 0) {
                add.close_btn = helper.getLanguage('Cancel')
            } else {
                if (add.step == 3) {
                    add.ok_btn = helper.getLanguage("Next Step")
                }
            }
        }
    }
    add.set_type = function(type) {
        add.obj.item.project_type = type;
        if (type == '1') {
            add.showTimeOptions()
        } else {
            add.showFixedOptions()
        }
        add.obj.item.budget_type = '1';
        add.obj.item.project_rate = '0';
        add.obj.item.billable_type = undefined;
        add.step = 2
    }
    add.ok = function() {
        if (add.step == 0) {
            add.setName();
            return !1
        }
        if (!add.obj.item.billable_type) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Select at least one Time & Materials choice")
                },
                "notice": !1,
                "success": !1
            };
            add.showAlerts(obj)
        } else if (add.obj.item.billable_type == '6' && (!add.obj.item.start_date || !add.obj.item.end_date)) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Select both start date and end date")
                },
                "notice": !1,
                "success": !1
            };
            add.showAlerts(obj)
        } else if (!add.obj.item.quote_id) {
            $uibModalInstance.close()
        } else {
            if (add.step == 4) {
                var t_nr = 0;
                var t_nr_line = 0;
                var art_nr = 0;
                var art_nr_line = 0;
                for (x in add.obj.item.quote_group_data) {
                    t_nr_line++;
                    if (add.obj.item.quote_group_data[x].checked) {
                        t_nr++
                    }
                }
                for (x in add.obj.item.quote_group_articles) {
                    art_nr_line++;
                    if (add.obj.item.quote_group_articles[x].checked) {
                        art_nr++
                    }
                }
                if (t_nr == 0 && art_nr == 0) {
                    var obj = {
                        "error": {
                            "error": helper.getLanguage("Select at least one task list or article list")
                        },
                        "notice": !1,
                        "success": !1
                    };
                    add.showAlerts(obj)
                } else {
                    $uibModalInstance.close()
                }
            } else {
                add.step = 4;
                add.ok_btn = helper.getLanguage("Start")
            }
        }
    }
    add.set_time_type = function(type) {
        add.obj.item.project_rate = type;
        add.step = 3;
        if (type == '1') {
            add.obj.showDaily();
            add.obj.item.task_rate = helper.getLanguage('Days')
        } else {
            add.obj.showHourly();
            add.obj.item.task_rate = helper.getLanguage('Hours')
        }
        if (!add.obj.item.quote_id) {
            add.ok_btn = helper.getLanguage("Start")
        } else {
            add.ok_btn = helper.getLanguage("Next Step")
        }
    }
    add.set_billable_type = function(type) {
        add.obj.item.billable_type = type;
        if (type == '5' || type == '6') {
            add.step = 3;
            add.obj.item.task_rate = helper.getLanguage('Hours');
            if (!add.obj.item.quote_id) {
                add.ok_btn = helper.getLanguage("Start")
            } else {
                add.ok_btn = helper.getLanguage("Next Step")
            }
            add.obj.showFixedDates(type);
            $timeout(function() {
                angular.element('.budget_type').children('option:nth-child(3)').addClass('hidden')
            })
        } else {
            $timeout(function() {
                angular.element('.budget_type').children('option:nth-child(3)').removeClass('hidden')
            })
        }
    }
    add.setName = function() {
        var err_set = !1;
        if (add.obj.item.name == null) {
            err_set = !0
        } else if (!add.obj.item.name.trim()) {
            err_set = !0
        }
        if (err_set) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Project Name required")
                },
                "notice": !1,
                "success": !1
            };
            add.showAlerts(obj);
            return !1
        }
        add.step = 1;
        add.close_btn = helper.getLanguage("Previous Step")
    }
    add.setNamePress = function(event) {
        if (event.keyCode == 13) {
            add.setName()
        }
    }
}]).controller('ProjectSettingsCtrl', ['$scope', 'helper', '$uibModal', 'settingsFc', 'data', 'selectizeFc', function($scope, helper, $uibModal, settingsFc, data, selectizeFc) {
    var vm = this;
    vm.app = 'project';
    vm.ctrlpag = 'settings';
    vm.tmpl = 'pTemplate';
    settingsFc.init(vm);
    vm.logos = {};
    vm.settings = {
        client: !1,
        project: !1,
        task: !1,
        comment: !1
    };
    vm.customLabel = {};
    vm.custom = {
        pdf_active: !1,
        pdf_note: !1,
        pdf_condition: !1,
        pdf_stamp: !1
    };
    vm.custom.layouts = {
        clayout: [],
        selectedOption: '0'
    };
    vm.message = {};
    vm.email_default = {};
    vm.weblink = {};
    vm.creditnote = {};
    vm.timesheets = {};
    vm.chargevats = {};
    vm.exportsettings = {};
    vm.vats = {};
    vm.sepa_active = {};
    vm.convention = {};
    vm.expenses = {};
    vm.expenses_add = {};
    vm.timesheet = {};
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 850,
        height: 300,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        //toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.salesCfg = selectizeFc.configure({
        maxOptions: 100,
        placeholder: helper.getLanguage('Select Internal Company'),
        onChange(value) {
            var data = {
                'do': 'project-settings-project-add_internal',
                'xget': 'internal',
                customer_id: value
            };
            helper.doRequest('post', 'index.php', data, function(r) {
                vm.internal = r.data.internal;
                vm.showAlerts(vm, r)
            })
        }
    });
    vm.deleteexpense = function(vm, Q_ID) {
        var get = 'expenses';
        var data = {
            'do': 'project-settings-project-delete_expense',
            'xget': get,
            'Q_ID': Q_ID
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.expenses = r.data.expenses;
            vm.showAlerts(vm, r)
        })
    }
    vm.save_conv = function() {
        var get = 'name_convention';
        var data = angular.copy(vm.name_convention);
        data.do = 'project-settings-project-project_naming';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.name_convention = r.data.name_convention;
            vm.showAlerts(vm, r)
        })
    }
    vm.updateHtml = function() {
        var get = 'emailMessage';
        var data = {
            'do': 'project-settings-project-use_html',
            'xget': get,
            'use_html': vm.message.USE_HTML,
            'lang_code': vm.message.LANG_ID
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.message = r.data.message;
            vm.showAlerts(vm, r)
        })
    }
    vm.saveMessage = function() {
        var get = 'emailMessage';
        var data = angular.copy(vm.message);
        data.do = 'project-settings-project-default_message';
        data.xget = 'emailMessage';
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.message = r.data.message;
            vm.showAlerts(vm, r)
        })
    }
    vm.ChangeLang = function() {
        var get = 'emailMessage';
        var data = {
            'do': 'project-settings',
            'xget': get,
            'lang_code': vm.message.LANG_ID
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            vm.message = r.data.message;
            vm.showAlerts(vm, r)
        })
    }
    vm.deleteaddress = function() {
        var get = 'emailMessage';
        var data = {
            'do': 'project-settings',
            'xget': get,
            'lang_code': vm.message.LANG_ID
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            vm.message = r.data.message;
            vm.showAlerts(vm, r)
        })
    }
    vm.updatetimesheet = function() {
        var get = 'timesheet';
        var data = angular.copy(vm.timesheet);
        data.do = 'project-settings-project-setcnst';
        data.xget = get;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.timesheet = r.data.timesheet;
            vm.showAlerts(vm, r)
        })
    }
    vm.updateLayout = function() {
        var get = 'PDFlayout';
        var data = {
            'do': 'project--project-settings',
            'xget': get,
            client: vm.settings.client,
            'project': vm.settings.project,
            'task': vm.settings.task,
            'comment': vm.settings.comment
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.Changecodelabes = function(vm, r) {
        var get = 'emailMessage';
        var data = {
            'do': 'invoice-settings',
            'xget': get,
            detail_id: r.detail_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            vm.message_labels = {
                detail_id: r.data.message_labels.detail_id,
                detail: r.data.message_labels.detail,
                selected_detail: r.data.message_labels.selected_detail
            };
            vm.showAlerts(vm, r)
        })
    }
    vm.AddField = function(vm, id, table, name) {
        var get = 'business';
        var data = {
            'do': 'project-settings-project-add_field',
            'xget': get,
            'name': name,
            'table': table
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            vm.showAlerts(vm, r)
        })
    }
    vm.DeleteField = function(vm, field_id, table) {
        var get = 'business';
        var data = {
            'do': 'project-settings-project-DeleteField',
            'xget': get,
            'field_id': field_id,
            'table': table
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            vm.showAlerts(vm, r)
        })
    }
    vm.updatefield = function(vm, id, table, value) {
        var get = 'business';
        var data = {
            'do': 'project-settings-project-UpdateField',
            'xget': get,
            'id': id,
            'table': table,
            'value': value
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            vm.showAlerts(vm, r)
        })
    }
    vm.AddFieldInternal = function(item) {
        var internal = {
            field_id: '',
            label: helper.getLanguage('Internal Companies') + ' ' + (item.small.length + 1),
            value: ''
        };
        item.small.push(internal)
    }
    vm.DeleteFieldInternal = function(vm, field_id, table) {
        var get = 'internal';
        var data = {
            'do': 'project-settings-project-delete_internal',
            'xget': get,
            'customer_id': field_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.internal = r.data.internal;
            vm.showAlerts(vm, r)
        })
    }
    vm.load = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'project-settings',
            'xget': h
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
            if (k == 'logos') {
                vm.default_logo = r.data.default_logo
            }
        })
    }
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            vm.selected = selectedItem
        }, function() {
            if (r.load) {
                vm.load(r.load[0], r.load[1])
            }
        })
    };
    vm.modal_small = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: (size ? size : 'md'),
            resolve: {
                obj: function() {
                    var i = angular.copy(r);
                    return i
                }
            }
        });
        modalInstance.result.then(function(r) {
            console.log('e', r);
            if (r && r.data) {
                for (x in r.data) {
                    vm[x] = r.data[x]
                }
            }
        }, function() {
            if (r.load) {
                vm.load(r.load[0], r.load[1])
            }
        })
    };
    vm.PageActive = function() {
        var get = 'general';
        var data = {
            'do': 'invoice--invoice-PageActive',
            'xget': get,
            'page': vm.general_condition.page
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.articlefields.text = vm.articlefields.text.substring(0, startPos) + myValue + vm.articlefields.text.substring(endPos, vm.articlefields.text.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.articlefields.text += myValue;
            _this.focus()
        }
    }
    vm.clickys = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.message.html_content = vm.message.html_content.substring(0, startPos) + myValue + vm.message.html_content.substring(endPos, vm.message.html_content.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.message.html_content += myValue;
            _this.focus()
        }
    }
    vm.renderPage = function(r) {
        vm.expenses = r.data.expenses;
        vm.expenses_add = r.data.expenses_add;
        vm.name_convention = r.data.name_convention;
       /* helper.doRequest('get', 'index.php?do=project-settings&xget=PDFlayout', function(r) {
            vm.settings = {
                client: r.data.client,
                project: r.data.project,
                task: r.data.task,
                comment: r.data.comment,
                selected_header: r.data.selected_header,
                selected_body: r.data.selected_body,
                selected_footer: r.data.selected_footer
            };
            vm.layout_header = r.data.layout_header;
            vm.layout_body = r.data.layout_body;
            vm.layout_footer = r.data.layout_footer;
            vm.custom_layout = r.data.custom_layout
        })*/
    };
    vm.renderPage(data)
}]).controller('ProjectexpensCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this,
        do_next = obj.Q_ID ? 'project-settings-project-update_expense' : 'project-settings-project-add_expense',
        do_xget = obj.Q_ID ? 'expenses' : 'expenses';
    vm.obj = obj;
    if (obj.Q_ID) {
        vm.oldValue = angular.copy(vm.obj);
        vm.obj.page_title = 'Edit expense category'
    } else {
        vm.obj.VALUE = '0,00';
        vm.obj.page_title = 'Add expense category'
    }
    vm.save_me = function(formObj) {
        var data = angular.copy(vm.obj);
        data.do = do_next;
        data.xget = do_xget;
        helper.doRequest('post', 'index.php', data, function(r) {
            if(r.error){
                helper.showAlerts(vm,r,formObj)
            }else{
                $uibModalInstance.close(r)
            }         
        })
    }
    vm.cancel = function() {
        vm.obj.VALUE = vm.oldValue
        $uibModalInstance.dismiss('dismiss')
    }
}]).controller('ProjectlabelCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'timetracker';
    vm.ctrlpag = 'labelCtrl';
    vm.tmpl = 'tTemplate';
    settingsFc.init(vm);
    vm.labels = {};
    helper.doRequest('get', 'index.php', obj, function(r) {
        vm.labels = r.data.labels
    });
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('ProjectCustomPdfCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'timetracker';
    vm.ctrlpag = 'custom_pdf';
    vm.tmpl = 'tTemplate';
    settingsFc.init(vm);
    vm.codelabels = {};
    vm.selectlabels = {};
    vm.showtiny = !0;
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 1110,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
       // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
    toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
    };
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.codelabels.variable_data = vm.codelabels.variable_data.substring(0, startPos) + myValue + vm.codelabels.variable_data.substring(endPos, vm.codelabels.variable_data.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.codelabels.variable_data += myValue;
            _this.focus()
        }
    }
    vm.closeModal = closeModal;
    vm.cancel = cancel;

    function closeModal() {
        $uibModalInstance.close()
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    }
    helper.doRequest('get', 'index.php?do=timetracker-custom_pdf&layout=' + obj.id + '&header=' + obj.header + '&footer=' + obj.footer, function(r) {
        vm.codelabels = {
            variable_data: r.data.codelabels.variable_data,
            layout: r.data.codelabels.layout,
            header: r.data.codelabels.header,
            footer: r.data.codelabels.footer
        };
        vm.selectlabels = {
            make_id: r.data.selectlabels.make_id,
            make: r.data.selectlabels.make,
            selected_make: r.data.selectlabels.selected_make
        };
        vm.selectdetails = {
            detail_id: r.data.selectdetails.detail_id,
            detail: r.data.selectdetails.detail
        }
    })
}]).controller('ArticleDeliveriesModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, helper, $uibModalInstance, data, modalFc) {
    var art = this;
    art.obj = data;
    art.showAlerts = function(d) {
        helper.showAlerts(art, d)
    }
    art.cancel = function() {
        $uibModalInstance.close()
    }
    art.deleteDelivery = function(item) {
        var obj = {};
        obj.do = 'project-article_deliveries-project-delete_delivery';
        obj.delivery_id = item.delivery_id;
        obj.project_id = art.obj.project_id;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            art.showAlerts(r);
            if (r.success) {
                art.obj = r.data
            }
        })
    }
    art.showDelivery = function(item) {
        var obj = {
            'project_id': art.obj.project_id,
            'delivery_id': item.delivery_id
        };
        openModal("article_delivery", obj, "lg")
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'article_delivery':
                var iTemplate = 'ArticleDelivery';
                var ctas = 'art';
                break
        }
        var params = {
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'article_delivery':
                params.template = 'pTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'project-article_delivery',
                    'project_id': item.project_id,
                    'delivery_id': item.delivery_id
                };
                break
        }
        modalFc.open(params)
    }
}]).controller('ProjectDownpaymentModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.data=data;

    vm.showTxt={
        'btn_create':false
    };

    vm.downpayment_opts=[{'id':'1','name':helper.getLanguage('Excl. VAT')},{'id':'2','name':helper.getLanguage('Incl. VAT')}];

    vm.selectfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        onChange(value){
            vm.setDownType('1');
        },
        maxItems: 1
    };

    vm.downpayment_opt_id='1';

    vm.vat = 0;

    var vat_intra = !1;
    for (x in vm.data.vat_regim_dd) {
        if (vm.data.vat_regime_id == vm.data.vat_regim_dd[x].id) {
            vm.vat = parseInt(vm.data.vat_regim_dd[x].value);
            if (vm.data.vat_regim_dd[x].regime_type == '2') {
                vat_intra = !0;
            }
            break;
        }
    }
    if (vat_intra) {
        vm.vat = 0;
    }  

    vm.cancel=function() {
        $uibModalInstance.close();
    }

    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d);
    }

    vm.setDownType = function(type) {
        var task_total = 0;
        for (x in vm.data.tasks.tasks) {
            if(vm.data.tasks.tasks[x].BILLABLE_CHECKED){
                task_total += parseFloat(helper.return_value(vm.data.tasks.tasks[x].task_budget), 10)
            }           
        }

        for (x in vm.data.purchases.purchases) {
            if (vm.data.purchases.purchases[x].billable) {
                task_total += (parseFloat(helper.return_value(vm.data.purchases.purchases[x].sell_price), 10))
            }
        }

        for (x in vm.data.articles.articles) {
            task_total += (parseFloat(helper.return_value(vm.data.articles.articles[x].quantity), 10) * parseFloat(helper.return_value(vm.data.articles.articles[x].price), 10))
        }

        if(vm.vat && vm.vat>0 && vm.downpayment_opt_id == '2'){
            task_total+=(task_total*vm.vat/100);
        }

        if (type == '1') {
            vm.data.downpayment_percent = helper.displayNr(parseFloat(helper.return_value(vm.data.downpayment_fixed), 10) * 100 / task_total)
        } else {
            vm.data.downpayment_fixed = helper.displayNr(parseFloat(helper.return_value(vm.data.downpayment_percent), 10) / 100 * task_total)
        }

        vm.data.downpayment_type = type;
        if (parseFloat(helper.return_value(vm.data.downpayment_fixed), 10) > task_total) {
            vm.showTxt.btn_create=false;
            var obj = {
                "error": {
                    "error": helper.getLanguage("Downpayment amount greater than project value")
                },
                "notice": !1,
                "success": !1
            };
            vm.showAlerts(obj);         
        }else if(parseFloat(helper.return_value(vm.data.downpayment_fixed), 10)<=0){
            //
        }else{
            vm.showTxt.btn_create=true;
        }
    }

    vm.createDownPayment = function() {
        var obj = {};
        obj.downpayment = '1';
        obj.downpayment_type = vm.data.downpayment_type;
        obj.downpayment_fixed = vm.vat && vm.vat>0 && vm.downpayment_opt_id == '2' ? helper.displayNr((parseFloat(helper.return_value(vm.data.downpayment_fixed),10)*100)/(100+vm.vat)) : vm.data.downpayment_fixed;
        obj.downpayment_percent = vm.data.downpayment_percent;
        obj.project_id = vm.data.project_id;
        obj.do = 'project-project-project-downpaymentInvoice';
        obj.xview='1';
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $uibModalInstance.close(r);
            }else{
                vm.showAlerts(r);
            }
        })
    }

    
}])