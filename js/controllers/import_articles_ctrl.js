angular.module('akti').controller('ImportArticlesCtrl', ['$scope', '$timeout', 'helper', '$uibModal', 'data', 'selectizeFc', 'modalFc', function($scope, $timeout, helper, $uibModal, data, selectizeFc, modalFc) {
    var vm = this;
    vm.temp_service = [];
    vm.catalog = data.data.catalog;
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select file'),
        labelField: 'name',
        searchField: ['name'],
    });

    function showAlerts(d) {
        helper.showAlerts(view, d)
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.backup_data = function(vm, r, file_id, step) {
        var get = 'import';
        var data = {
            'do': 'article-import_articles-article-backup_data',
            'xget': get,
            'file_id': file_id,
            'step': step
        };
        vm.modal(data, 'atemplate/makingFile', 'makingArticleCtrl', 'sm')
    }
    vm.delete_import_company = function(vm, r, company_import_log_id) {
        var get = 'import';
        var data = {
            'do': 'customers-import_accounts-customers-delete_import_company',
            'xget': get,
            'company_import_log_id': company_import_log_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.catalog = r.data.catalog;
            vm.showAlerts(vm, r)
        })
    }
    vm.hidecontact = function() {
        if (vm.indi == 1) {
            vm.hideconts = 0;
            vm.hidecont = 0;
            vm.namecomp = helper.getLanguage('Individual');
            vm.changefield = 1
        } else {
            vm.hideconts = 1;
            vm.hidecont = 1;
            vm.namecomp = helper.getLanguage('Company');
            vm.changefield = 0
        }
    }
    vm.hideconts = 0;
    vm.putcontact = function() {
        if (vm.language_multi == 1) {
            vm.hideconts = 1
        } else {
            vm.hideconts = 0
        }
    }
    vm.save_imports = function() {
        var get = 'import';
        var total = vm.total_rows;
        var k = 0;
        var times = Math.ceil(total / 25);
        var width = 100 / times;
        var executed = 0;
        var params = {
            'xget': get,
            'do': 'article-import_accounts-article-save_imports',
            'is_customer': 1,
            'total_rows': k + 25,
            'xls_name': vm.xls_name,
            'timestamp': vm.timestamp,
            'total': total,
            'k': 0,
            'service': vm.service,
            'language_multi': vm.language_multi
        };
        if(vm.stock_location_id){
            params.stock_location_id=vm.stock_location_id;
        }
        for (x in vm.fields) {
            params[x] = vm.fields[x]
        }
        for (x in vm.fieldsC) {
            params[x] = vm.fieldsC[x]
        }
        var data = {
            'params': params,
            'get': get,
            'k': k,
            'times': times,
            'width': width,
            'executed': executed
        };
        vm.openModal('ImportArtCtrl', data, 'md')
    }
    vm.load = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'article-import_accounts',
            'xget': h
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(vm, r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
        })
    }
    vm.modal_history_article = function(r, template, ctrl, size) {
        console.log('sddsdsd');
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data)
        }, function() {})
    };
    vm.openModal = function(type, item, size) {
        var params = {
            template: 'atemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'ImportArtCtrl':
                params.template = 'atemplate/import_progres';
                params.item = item;
                params.ctrl = type;
                params.callback = function(data) {
                    vm.catalog = data.catalog;
                    $timeout(function() {
                        angular.element('.history').click()
                    })
                }
                break
        }
        modalFc.open(params)
    }
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'sm',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            for (x in data.data) {
                vm[x] = data.data[x]
            }
            if (data.data.step == 3) {
                vm.catalog.class3 = 'text-primary';
                vm.catalog.class2 = 'text-muted';
                vm.catalog.step = ''
            } else {
                vm.catalog.class3 = 'text-muted'
            }
        }, function() {})
    };
    vm.cancel = function() {
        $uibModal.dismiss('cancel')
    };
    vm.upload = function(vm, e) {
        angular.element(e.target).parents('.upload-box').find('.newUploadFile2').click()
    }
    angular.element('.newUploadFile2').on('click', function(e) {
        e.stopPropagation()
    }).on('change', function(e) {
        e.preventDefault();
        var _this = $(this),
            p = _this.parents('.upload-box'),
            x = p.find('.newUploadFile2');
        if (x[0].files.length == 0) {
            return !1
        }
        if (x[0].files[0].size > 99999999) {
            alert("Error: file is to big.");
            return !1
        }
        p.find('.tmpImgHolder img').attr('src', '');
        var tmppath = URL.createObjectURL(x[0].files[0]);
        p.find('.tmpImgHolder img').attr('src', tmppath);
        p.find('.btn').addClass('hidden');
        p.find('.nameOfTheFile').text(x[0].files[0].name);
        var formData = new FormData();
        formData.append("Filedata", x[0].files[0]);
        formData.append("do", 'article-import_articles-article-uploadify');
        formData.append("xget", 'import');
        p.find('.upload-progress-bar').css({
            width: '0%'
        });
        p.find('.upload-file').removeClass('hidden');
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "index.php", !0);
        oReq.upload.addEventListener("progress", function(e) {
            var pc = parseInt(e.loaded / e.total * 100);
            p.find('.upload-progress-bar').css({
                width: pc + '%'
            })
        });
        oReq.onload = function(oEvent) {
            var res = JSON.parse(oEvent.target.response);
            setTimeout(function() {
                p.find('.upload-file').addClass('hidden')
            }, 1000);
            setTimeout(function() {
                p.find('.btn').removeClass('hidden')
            }, 1001);
            if (oReq.status == 200) {
                if (res.success) {
                    var params = {};
                    params['do'] = 'article-import_articles-article-import_article';
                    params.xls_name = res.data.xls_name;
                    params.ajax = 1;
                    vm.modal(params, 'atemplate/waitingModal', 'waitingModalCtrl', 'sm')
                } else {
                    alert(res.error)
                }
            } else {
                alert("Error " + oReq.status + " occurred when trying to upload your file.")
            }
        };
        oReq.send(formData)
    });
    vm.updateType = function() {
        if (!vm.catalog.adv_product) {
            if (vm.service) {
                vm.temp_service = [];
                for (x in vm.match_field_company) {
                    if (vm.match_field_company[x].id == '7') {
                        vm.temp_service.push(vm.match_field_company[x]);
                        vm.match_field_company.splice(x, 1)
                    }
                }
                for (x in vm.match_field_company) {
                    if (vm.match_field_company[x].id == '8') {
                        vm.temp_service.push(vm.match_field_company[x]);
                        vm.match_field_company.splice(x, 1)
                    }
                }
            } else {
                for (x in vm.temp_service) {
                    vm.match_field_company.push(vm.temp_service[x])
                }
            }
        }
    }

    vm.stockLocationCfg={
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: true,
        maxItems: 1
    };

}]).controller('waitingModalCtrl', ['$scope', 'helper', 'obj', '$uibModalInstance', function($scope, helper, obj, $uibModalInstance) {
    console.log(obj);
    helper.doRequest('post', 'index.php', obj, function(r) {
        $uibModalInstance.close(r)
    })
}]).controller('historyArticleCtrl', ['$scope', 'helper', 'list', 'obj', '$uibModalInstance', function($scope, helper, list, obj, $uibModalInstance) {
    var vm = this;
    vm.history = obj;
    vm.max_rows = 0;
    vm.id = vm.history[0];
    vm.field = vm.history[1];
    vm.search = {
        search: '',
        'do': 'article-import_articles',
        xget: 'history_import',
        showAdvanced: !1,
        showList: !1,
        offset: 1,
        import_id: vm.id,
        field: vm.field
    };
    vm.list = [];
    vm.renderList = function(d) {
        if (d.data) {
            for (x in d.data) {
                vm[x] = d.data[x]
            }
            vm.showAlerts(d)
        }
    }
    vm.searchThing = function() {
        list.search(vm.search, vm.renderList, vm)
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    if (vm.field == 'companies_add') {
        vm.title = 'Companies added'
    }
    if (vm.field == 'companies_update') {
        vm.title = 'Companies updated'
    }
    if (vm.field == 'contacts_add') {
        vm.title = 'Contacts added'
    }
    if (vm.field == 'contacts_update') {
        vm.title = 'Contacts updated'
    }
    var get = 'history_import';
    var data = {
        'do': 'article-import_articles',
        'xget': get,
        'import_id': vm.id,
        'field': vm.field
    };
    //angular.element('.loading_wrap').removeClass('hidden');
    helper.doRequest('get', 'index.php', data, vm.renderList)
}]).controller('makingArticleCtrl', ['$scope', 'helper', 'obj', '$uibModalInstance', function($scope, helper, obj, $uibModalInstance) {
    var vm = this;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    helper.doRequest('post', 'index.php', obj, function(r) {
        vm.catalog = r.data.catalog;
        vm.showAlerts(vm, r);
        $uibModalInstance.close(r)
    })
}]).controller('ImportArtCtrl', ['$scope', 'helper', 'data', '$uibModalInstance', function($scope, helper, data, $uibModalInstance) {
    var vm = this;
    for (x in data) {
        vm[x] = data[x]
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.save_import = function(params, total, width, times, executed) {
        helper.doRequest('post', 'index.php', params, function(r) {
            if (r.error) {
                vm.showAlerts(vm, r);
                $uibModalInstance.close('closed');
                return !1
            }
            executed++;
            angular.element('.import-progressbar-value').css({
                'width': executed * width + '%'
            });
            params.k = executed * 25;
            params.total_rows = params.k + 25;
            var imported = params.k > total ? total : params.k;
            angular.element('#imported').text(imported);
            if (executed < times) {
                return vm.save_import(params, total, width, times, executed)
            } else {
                var data_get = {
                    'do': 'article-import_articles',
                    'xget': 'import',
                    'step': '1',
                    'class3': 'text_muted',
                    'class1': 'text-primary'
                };
                helper.doRequest('get', 'index.php', data_get, function(r) {
                    vm.catalog = r.data.catalog;
                    vm.showAlerts(vm, r);
                    $uibModalInstance.close(vm)
                })
            }
        })
    }
    vm.save_import(vm.params, vm.total, vm.width, vm.times, vm.executed)
}]).controller('ImportArticlesPricesCtrl', ['$scope', '$timeout', 'helper', '$uibModal', 'data', 'selectizeFc', 'modalFc', function($scope, $timeout, helper, $uibModal, data, selectizeFc, modalFc) {
    var vm = this;
    vm.catalog = data.data.catalog;
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select file'),
        labelField: 'name',
        searchField: ['name'],
    });

    function showAlerts(d) {
        helper.showAlerts(view, d)
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.backup_data = function() {
        var data = {
            'do': 'article-import_articles-article-backup_data',
            'xget': 'import',
            'file_id': vm.catalog.file_id
        };
        vm.modal(data, 'atemplate/makingFile', 'makingArticleCtrl', 'sm')
    }
    vm.save_imports = function() {
        var get = 'import';
        var total = vm.total_rows;
        var k = 0;
        var times = Math.ceil(total / 25);
        var width = 100 / times;
        var executed = 0;
        var params = {
            'xget': get,
            'do': 'article--article-save_imports_prices',
            'total_rows': k + 25,
            'xls_name': vm.xls_name,
            'timestamp': vm.timestamp,
            'total': total,
            'k': 0
        };
        for (x in vm.fields) {
            params[x] = vm.fields[x]
        }
        for (x in vm.fieldsC) {
            params[x] = vm.fieldsC[x]
        }
        var data = {
            'params': params,
            'get': get,
            'k': k,
            'times': times,
            'width': width,
            'executed': executed
        };
        vm.openModal('ImportArtCtrl', data, 'md')
    }
    vm.modal_history_article = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data)
        }, function() {})
    };
    vm.openModal = function(type, item, size) {
        var params = {
            template: 'atemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'ImportArtCtrl':
                params.template = 'atemplate/import_progres';
                params.item = item;
                params.ctrl = type;
                params.callback = function(data) {
                    vm.catalog = data.catalog;
                    $timeout(function() {
                        angular.element('.history').click()
                    })
                }
                break
        }
        modalFc.open(params)
    }
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'sm',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            for (x in data.data) {
                vm[x] = data.data[x]
            }
            if (data.data.step == 3) {
                vm.catalog.class3 = 'text-primary';
                vm.catalog.class2 = 'text-muted';
                vm.catalog.step = ''
            } else {
                vm.catalog.class3 = 'text-muted'
            }
        }, function() {})
    };
    vm.cancel = function() {
        $uibModal.dismiss('cancel')
    };
    vm.upload = function(vm, e) {
        angular.element(e.target).parents('.upload-box').find('.newUploadFile2').click()
    }
    angular.element('.newUploadFile2').on('click', function(e) {
        e.stopPropagation()
    }).on('change', function(e) {
        e.preventDefault();
        var _this = $(this),
            p = _this.parents('.upload-box'),
            x = p.find('.newUploadFile2');
        if (x[0].files.length == 0) {
            return !1
        }
        if (x[0].files[0].size > 99999999) {
            alert("Error: file is to big.");
            return !1
        }
        p.find('.tmpImgHolder img').attr('src', '');
        var tmppath = URL.createObjectURL(x[0].files[0]);
        p.find('.tmpImgHolder img').attr('src', tmppath);
        p.find('.btn').addClass('hidden');
        p.find('.nameOfTheFile').text(x[0].files[0].name);
        var formData = new FormData();
        formData.append("Filedata", x[0].files[0]);
        formData.append("do", 'article-import_articles-article-uploadify');
        formData.append("xget", 'import');
        p.find('.upload-progress-bar').css({
            width: '0%'
        });
        p.find('.upload-file').removeClass('hidden');
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "index.php", !0);
        oReq.upload.addEventListener("progress", function(e) {
            var pc = parseInt(e.loaded / e.total * 100);
            p.find('.upload-progress-bar').css({
                width: pc + '%'
            })
        });
        oReq.onload = function(oEvent) {
            var res = JSON.parse(oEvent.target.response);
            setTimeout(function() {
                p.find('.upload-file').addClass('hidden')
            }, 1000);
            setTimeout(function() {
                p.find('.btn').removeClass('hidden')
            }, 1001);
            if (oReq.status == 200) {
                if (res.success) {
                    var params = {};
                    params['do'] = 'article-import_articles-article-import_article_prices';
                    params.xls_name = res.data.xls_name;
                    params.ajax = 1;
                    vm.modal(params, 'atemplate/waitingModal', 'waitingModalCtrl', 'sm')
                } else {
                    alert(res.error)
                }
            } else {
                alert("Error " + oReq.status + " occurred when trying to upload your file.")
            }
        };
        oReq.send(formData)
    });

}])