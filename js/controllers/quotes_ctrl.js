angular.module('akti').controller('quotesCtrl', ['$scope', '$rootScope', '$confirm', 'helper', 'list', 'selectizeFc', 'data', 'modalFc', function($scope, $rootScope, $confirm, helper, list, selectizeFc, data, modalFc) {
    console.log('quotes');
    $rootScope.opened_lists.quotes = true;
    // $scope.views = [{
    //     name: helper.getLanguage('All Statuses'),
    //     id: 0,
    //     active: 'active'
    // }, {
    //     name: helper.getLanguage('My quotes'),
    //     id: 5,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Draft'),
    //     id: 1,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Sent'),
    //     id: 2,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Accepted'),
    //     id: 3,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Rejected'),
    //     id: 4,
    //     active: ''
    // }, {
    //     name: helper.getLanguage('Pending'),
    //     id: 6,
    //     active: ''
    // }];
    $scope.activeView = 0;
    $scope.list = [];
    $scope.nav = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'quote-quotes',
        view: 0,
        xget: '',
        offset: 1,
        archived: 0
    };
    $scope.listObj = {
        check_add_all: !1,
        markAll: !1
    };
    $scope.ord = '';
    $scope.reverse = !1;
    $scope.show_load = !0;
    $scope.renderList = function(d) {
        if (d.data) {
            for (x in d.data) {
                $scope[x] = d.data[x]
            }
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['quote_id']);
                count ++;
            }
            //console.log(count);
            sessionStorage.setItem('quote.view_total', count);
            sessionStorage.setItem('quote.view_list', JSON.stringify(ids));
            //console.log(ids);
            helper.showAlerts($scope, d);
            $scope.initCheckAll();
        }
    };
    $scope.archived = function(value) {
        $scope.search.archived = value;
        list.filter($scope, value, $scope.renderList, true)
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    }
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
        $scope.checkSearch()
    };
    $scope.checkSearch = function() {
        var data = angular.copy($scope.search),
            is_search = !1;
        data = helper.clearData(data, ['do', 'xget', 'showAdvanced', 'showList', 'offset']);
        for (x in data) {
            if (data[x]) {
                is_search = !0
            }
        }
        // vm.search.showList = is_search
    }
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    }
    $scope.deleteBulk = function() {
        var obj = angular.copy($scope.search);
        obj.do = 'quote-quotes-quote-deleteBulk';
        helper.doRequest('post', 'index.php', obj, function(r) {
            helper.showAlerts($scope, r);
            if (r.success) {
                $scope.renderList(r)
            }
        })
    }
    $scope.actionItem = function(item, action, $index, lists, initalItem) {
        list.action($scope, item, action, function(d) {
            if (d.success) {
                lists[$index - 1].status = !0;
                initalItem.quote_version = lists[$index - 1].quote_version;
                lists.splice($index, 1)
            }
            helper.showAlerts($scope, d)
        })
    }
    $scope.createExports = function($event) {
        return list.exportFnc($scope, $event)
    }
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.saveForPDF = function(p) {
        if(p){
            if(p.check_add_to_product){
              var elem_counter=0;
              for(x in $scope.list){
                if($scope.list[x].check_add_to_product){
                  elem_counter++;
                }
              }
              if(elem_counter==$scope.list.length && $scope.list.length>0){
                $scope.listObj.check_add_all=true;
              }
            }else{
              $scope.listObj.check_add_all=false;
            }
        }      
        list.saveForPDF($scope, 'quote--quote-saveAddToPdf', p)
    }
    $scope.showStats = function(item, $index) {
        $scope.expandWidth=angular.element('.quotesTable thead tr th:eq(0)').outerWidth();
        $scope.checkColWidth=angular.element('.quotesTable thead tr th.col-select').outerWidth();
        $scope.lastColWidth=angular.element('.quotesTable thead tr th.table_actions').outerWidth();
        var add = !1;
        if (!$scope.listStats[$index]) {
            $scope.listStats[$index] = [];
            add = !0
        }
        if (!item.isopen) {
            item.isopen = !0
        } else {
            item.isopen = !1
        }
        if (add == !0) {
            helper.doRequest('get', 'index.php?do=quote-quotes&xget=quote_list&quote_id=' + item.id, function(r) {
                if (r.data) {
                    $scope.listStats[$index] = r.data;
                    for(i=0;i<$scope.columns.length;i++){
                        var elWidth=angular.element('.quotesTable thead tr th:eq('+(i+1)+')').outerWidth();
                        $scope.columns[i].columnWidth=elWidth+"px";
                    }
                }
            })
        }else{
            for(i=0;i<$scope.columns.length;i++){
                var elWidth=angular.element('.quotesTable thead tr th:eq('+(i+1)+')').outerWidth();
                $scope.columns[i].columnWidth=elWidth+"px";
            }
        }
        angular.element('.quotesTable tbody tr.collapseTr').eq($index).find('.fa-chevron-right').toggleClass('fa-chevron-down')
    }
    $scope.filterCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('All Statuses'),
        onChange(value) {
            $scope.toggleSearchButtons('view', value)
        }
    });
    // $scope.filterCfg = {
    //     valueField: 'id',
    //     labelField: 'name',
    //     searchField: ['name'],
    //     delimiter: '|',
    //     placeholder: 'All Statuses',
    //     create: !1,
    //     onItemAdd(value, $item) {
    //         if (value == undefined) {
    //             value = 0
    //         }
    //         $scope.filters(value)
    //     },
    //     maxItems: 1
    // };
    $scope.greenpost = function(item) {
        if ($scope.post_active) {
            var data = {};
            for (x in $scope.search) {
                data[x] = $scope.search[x]
            }
            data.do = 'quote-quotes-quote-postIt';
            data.id = item.id;
            data.version_id = item.version_id;
            data.related = 'list';
            data.no_status = !0;
            data.post_library = $scope.post_library;
            openModal("resend_post", data, "md")
        } else {
            var data = {};
            data.clicked_item = item;
            for (x in $scope.search) {
                data[x] = $scope.search[x]
            }
            openModal("post_register", data, "md")
        }
    }
    $scope.resendPost = function(item) {
        var data = {};
        for (x in $scope.search) {
            data[x] = $scope.search[x]
        }
        data.do = 'quote-quotes-quote-postIt';
        data.id = item.id;
        data.version_id = item.version_id;
        data.related = 'list';
        data.post_library = $scope.post_library;
        openModal("resend_post", data, "md")
    }

    $scope.initCheckAll=function(){
        var total_checked=0;
        for(x in $scope.list){
            if($scope.list[x].check_add_to_product){
                total_checked++;
            }
        }
        if($scope.list.length > 0 && $scope.list.length == total_checked){
            $scope.listObj.check_add_all=true;
        }else{
            $scope.listObj.check_add_all=false;
        }
    }

    $scope.columnSettings = function() {
        openModal('column_set', {}, 'lg')
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'post_register':
                var iTemplate = 'PostRegister';
                var ctas = 'post';
                break;
            case 'resend_post':
                var iTemplate = 'ResendPostGreen';
                var ctas = 'post';
                break;
             case 'column_set':
                var iTemplate = 'ColumnSettings';
                var ctas = 'vm';
                break;   
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'post_register':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.item.do = 'quote-quotes-quote-postRegister';
                params.callback = function(data) {
                    if (data && data.data) {
                        $scope.renderList(data);
                        $scope.greenpost(data.clicked_item)
                    }
                    $scope.showAlerts(data)
                }
                break;
            case 'resend_post':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.request_type = 'post';
                params.params = {
                    'do': 'quote--quote-postQuoteData',
                    'quote_id': item.id,
                    'version_id': item.version_id,
                    'related': 'status',
                    'old_obj': item
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.renderList(d);
                        $scope.showAlerts(d)
                    }
                }
                break;
            case 'column_set':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.params = {
                    'do': 'misc-column_set',
                    'list': 'quotes'
                };

                params.callback = function(d) {
                    if (d) {
                        helper.doRequest('get', 'index.php', {
                            'do': 'quote-quotes',
                            'xget': 'quotes_cols'
                        }, function(o) {
                            $scope.columns = o.data;
                            $scope.searchThing()
                        })
                    }
                }
                break;
        }
        modalFc.open(params)
    }
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    }
    $scope.searchAdvanced = function() {
        $scope.searchThing()
    }
    $scope.resetAdvanced = function() {
        $scope.search.your_reference_search = undefined;
        $scope.search.showAdvanced = !1;
        $scope.searchThing()
    }
    list.init($scope, 'quote-quotes', $scope.renderList, ($rootScope.reset_lists.quotes ? true : false));
    $rootScope.reset_lists.quotes = false;
    $scope.show_load = !0;
    helper.doRequest('get', 'index.php', {
        'do': 'misc-post_library'
    }, function(r) {
        $scope.show_load = !1;
        $scope.post_library = r.data.post_library
    })
}]).controller('QuoteCtrl', ['$scope', 'helper', function($scope, helper) {
    var vm = this;
    vm.serial_number = '';
    vm.title_text='';
    vm.isAddPage = !0
}]).controller('quoteSettingsCtrl', ['$scope', 'helper', '$uibModal', 'settingsFc', 'data', 'selectizeFc', function($scope, helper, $uibModal, settingsFc, data, selectizeFc) {
    var vm = this;
    vm.app = 'quote';
    vm.ctrlpag = 'settings';
    vm.tmpl = 'qTemplate';
    settingsFc.init(vm);
    vm.logos = {};
    vm.layout = {};
    vm.layout_header = {};
    vm.layout_body = {};
    vm.layout_footer = {};
    vm.settings = {
        use_page_numbering: !1,
        show_stamp: !1
    };
    vm.custom = {
        pdf_active: !1,
        pdf_note: !1,
        pdf_condition: !1,
        pdf_stamp: !1
    };
    vm.custom.layouts = {
        clayout: [],
        selectedOption: '0'
    };
    vm.convention = {
        reference: !1
    };
    vm.general_condition = {};
    vm.customLabel = {};
    vm.message = {};
    vm.email_default = {};
    vm.combo = !0;
    vm.defaultEmail = {};
    vm.weblink = {};
    vm.bigloop = {};
    vm.articlefields = {};
    vm.packing = {};
    vm.atachment = {};
    vm.var = '#5d97b9';
    vm.listset = {};
    vm.notes_save_btn_txt=helper.getLanguage('Save'); 

    vm.checkFormNotes = function(formObj) {   
        if (formObj.$invalid ) {         
            return !0
        } else {
            vm.notes_save_btn_txt = helper.getLanguage('Save');  
             return !1      
        }
    }

    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 850,
        height: 300,
        menubar: !1,
        relative_urls: 1,
        selector: 'textarea',
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.updateLayout = function() {
        var get = 'PDFlayout';
        var data = {
            'do': 'quote-settings-quote-settings',
            'xget': get,
            show_bank: vm.settings.show_bank,
            'show_stamp': vm.settings.show_stamp,
            'show_page': vm.settings.show_page,
            'show_paid': vm.settings.show_paid,
            'use_page_numbering': vm.settings.use_page_numbering
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.load = function(h, k, i, j) {
        vm.alerts = [];
        var d = {
            'do': 'quote-settings',
            'xget': h,
            'wname': j
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
            if (k == 'logos') {
                vm.default_logo = r.data.default_logo
            }
            if (k == 'weblink') {
                angular.element('#weblink a[href="#quote_weblink_settings"]').tab('show')
            }
        })
    }
    vm.modal = function(r, template, ctrl, size) {
       // console.log(r, template, ctrl, size);
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'lg',
            resolve: {
                obj: function() {
                    //console.log(r);
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            vm.selected = selectedItem
        }, function() {
            if (r.load) {
                vm.load(r.load[0], r.load[1])
            }
        })
    };
    vm.clickys = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.message.text = vm.message.text.substring(0, startPos) + myValue + vm.message.text.substring(endPos, vm.message.text.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.message.text += myValue;
            _this.focus()
        }
    }
    vm.PDFactive = function() {
        var get = 'CustomizePDF';
        var data = {
            'do': 'quote--quote-PdfAcitve',
            'xget': get,
            'pdf_active': vm.custom.pdf_active,
            'pdf_note': vm.custom.pdf_note,
            'pdf_condition': vm.custom.pdf_condition,
            'pdf_stamp': vm.custom.pdf_stamp
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.PageActive = function() {
        var get = 'general';
        var data = {
            'do': 'quote--quote-PageActive',
            'xget': get,
            'page': vm.general_condition.page
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.DeleteField = function(vm, field_id, table) {
        var get = 'categorisation';
        var data = {
            'do': 'quote-settings-quote-DeleteField',
            'xget': get,
            'field_id': field_id,
            'table': table
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            vm.showAlerts(vm, r)
        })
    }
    vm.updatefield = function(vm, id, table, value) {
        var get = 'categorisation';
        var data = {
            'do': 'quote-settings-quote-UpdateField',
            'xget': get,
            'id': id,
            'table': table,
            'value': value
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            vm.showAlerts(vm, r)
        })
    }
    vm.AddField = function(vm, id, table, name) {
        var get = 'categorisation';
        var data = {
            'do': 'quote-settings-quote-add_field',
            'xget': get,
            'name': name,
            'table': table
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.bigloop = r.data.bigloop;
            vm.showAlerts(vm, r)
        })
    }
    vm.savepacking = function(vm, r) {
        var get = 'packingsettings';
        var data = {
            'do': 'quote-settings-quote-savepacking',
            'xget': get,
            'pack': vm.packing.allowpack,
            'sale': vm.packing.allowsale
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.packing = r.data.packing
            vm.showAlerts(vm, r)
        })
    }
    vm.Changecodelabes = function(vm, r) {
        var get = 'emailMessage';
        var data = {
            'do': 'quote-settings',
            'xget': get,
            detail_id: r.detail_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            vm.message_labels = {
                detail_id: r.data.message_labels.detail_id,
                detail: r.data.message_labels.detail,
                selected_detail: r.data.message_labels.selected_detail
            };
            vm.showAlerts(vm, r)
        })
    }
    vm.weblinkchangecodelabes = function(vm, r) {
        var get = 'weblinkEmailMessage';
        var data = {
            'do': 'quote-settings',
            'xget': get,
            detail_id: r.detail_id,
            'wname': vm.message.name
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            vm.message_labels = {
                detail_id: r.data.message_labels.detail_id,
                detail: r.data.message_labels.detail,
                selected_detail: r.data.message_labels.selected_detail
            };
            vm.showAlerts(vm, r)
        })
    }
    vm.setact = function(vm, id) {
        var get = 'listsettings';
        var data = {
            'do': 'quote-settings-quote-setact',
            'xget': get,
            'id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.listset = r.data.listset;
            vm.showAlerts(vm, r)
        })
    }
    vm.saveterm = function() {
        var data = angular.copy(vm.quoteTerm);
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.quoteTerm = r.data.quoteTerm;
            vm.showAlerts(vm, r)
        })
    }
    vm.clickyArticle = function(scope, myValue) {
        if (!scope.articlefields.ADV_PRODUCT) {
            return !1
        }
        vm.clicky(scope, myValue)
    }
    vm.renderList = function(r) {
        helper.doRequest('get', 'index.php?do=quote-settings&xget=PDFlayout', function(r) {
            vm.settings = {
                use_page_numbering: r.data.use_page_numbering,
                show_stamp: r.data.show_stamp,
                font_pdf: r.data.font_pdf,
                layout_header: r.data.layout_header,
                layout_footer: r.data.layout_footer,
                selected_header: r.data.selected_header,
                selected_body: r.data.selected_body,
                selected_footer: r.data.selected_footer,
                lang_id: r.data.lang_id,
                multiple_identity: r.data.multiple_identity,
                identity: r.data.identity
            };
            vm.layout_header = r.data.layout_header;
            vm.layout_body = r.data.layout_body;
            vm.layout_footer = r.data.layout_footer;
            vm.custom_layout = r.data.custom_layout;
            vm.has_custom_layout = r.data.has_custom_layout;
            vm.use_custom_layout = r.data.use_custom_layout
        });
        helper.doRequest('get', 'index.php?do=quote-settings&xget=CustomizePDF', function(r) {
            vm.custom = {
                pdf_active: r.data.pdf_active,
                pdf_note: r.data.pdf_note,
                pdf_condition: r.data.pdf_condition,
                pdf_stamp: r.data.pdf_stamp,
                layouts: {
                    clayout: [],
                    selectedOption: ""
                },
                header: r.data.header,
                footer: r.data.footer,
                lang_id: r.data.lang_id,
                layout: r.data.layout,
                layout_footer: r.data.layout_footer
            };
            vm.custom.layouts.clayout = r.data.layouts;
            vm.custom.layouts.selectedOption = '0'
        });
        helper.doRequest('get', 'index.php?do=quote-settings&xget=convention', function(r) {
            vm.convention = {
                account_number: r.data.account_number,
                account_digits: r.data.account_digits,
                example: r.data.example,
                reference: r.data.reference,
                del: r.data.del,
                quote_weight:r.data.quote_weight
            }
        })
    };
    vm.updatePdfSetting=function(item){
        var tmp_item=angular.copy(item);
        tmp_item.do='quote--quote-updatePdfSettings';
        helper.doRequest('post','index.php',tmp_item);
    }

    vm.removeActiveTabs=function(array){
        for(var x in array){
            angular.element('#'+array[x]).removeClass('active in');
        }
    }

    vm.renderList(data)
}]).controller('custom_pdfQuoteCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'quote';
    vm.ctrlpag = 'custom_pdf';
    vm.tmpl = 'qTemplate';
    settingsFc.init(vm);
    vm.codelabels = {};
    vm.selectlabels = {};
    vm.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 1100,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload_image_pdf').trigger('click');
                $('#upload_image_pdf').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    vm.closeModal = closeModal;
    vm.cancel = cancel;
    vm.clicky = function(myValue) {
        var _this = angular.element('#altId')[0];
        if (document.selection) {
            _this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            _this.focus()
        } else if (_this.selectionStart || _this.selectionStart == '0') {
            var startPos = _this.selectionStart;
            var endPos = _this.selectionEnd;
            var scrollTop = _this.scrollTop;
            vm.codelabels.variable_data = vm.codelabels.variable_data.substring(0, startPos) + myValue + vm.codelabels.variable_data.substring(endPos, vm.codelabels.variable_data.length);
            _this.focus();
            _this.selectionStart = startPos + myValue.length;
            _this.selectionEnd = startPos + myValue.length;
            _this.scrollTop = scrollTop
        } else {
            vm.codelabels.variable_data += myValue;
            _this.focus()
        }
    }

    function closeModal() {
        $uibModalInstance.close()
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    }
    helper.doRequest('get', 'index.php?do=quote-custom_pdf&layout=' + obj.id + '&header=' + obj.header + '&footer=' + obj.footer+ '&identity_id=' + obj.identity_id, function(r) {
        vm.codelabels = {
            variable_data: r.data.codelabels.variable_data,
            layout: r.data.codelabels.layout,
            header: r.data.codelabels.header,
            footer: r.data.codelabels.footer,
            identity_id: r.data.codelabels.identity_id
        };
        vm.selectlabels = {
            make_id: r.data.selectlabels.make_id,
            make: r.data.selectlabels.make,
            selected_make: r.data.selectlabels.selected_make
        };
        vm.selectdetails = {
            detail_id: r.data.selectdetails.detail_id,
            detail: r.data.selectdetails.detail
        }
    })
}]).controller('labelCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', function($scope, helper, $uibModalInstance, settingsFc, obj) {
    var vm = this;
    vm.app = 'quote';
    vm.ctrlpag = 'label';
    vm.tmpl = 'qTemplate';
    settingsFc.init(vm);
    vm.labels = {};
    helper.doRequest('get', 'index.php', obj, function(r) {
        vm.labels = r.data.labels
    });
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('QuoteEditCtrl', ['$scope', '$rootScope','$stateParams', '$state', '$timeout', '$window', 'editItem', 'helper', 'data','$confirm', 'selectizeFc', 'modalFc', function($scope, $rootScope, $stateParams, $state, $timeout, $window, editItem, helper, data, $confirm, selectizeFc, modalFc) {
    console.log('quoteEdit');
    var edit = this,
        typePromise;
    edit.app = 'quote';
    edit.openContent = !1;
    edit.ctrlpag = 'nquote';
    edit.tmpl = 'qTemplate';
    $scope.vm.isAddPage = !1;
    $scope.vm.times = 0;
    $scope.vm.quote_group = {};
    edit.item_id = $stateParams.quote_id;
    edit.duplicate_quote_id = $stateParams.duplicate_quote_id;
    edit.quote_id = edit.item_id;
    edit.item = {};
    edit.oldObj = {};
    edit.item.validity_date = new Date();
    edit.removedCustomer = !1;
    //edit.item.add_customer = (edit.item_id != 'tmp' ? true : !1);
    edit.item.add_customer = !1;
    edit.quote_group_active = 0;
    edit.showBulkChangeVariants=false;
    edit.originalArticleList = undefined;
    edit.is_add_article = !1;
    edit.from_customer=$stateParams.from_customer;

    edit.currencyCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            edit.changeCurrency()
        }
    });
    edit.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onChange(value) {
            edit.changeDiscount()
        }
    });
    edit.vatregcfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        onDropdownOpen($dropdown) {
            var _this = this;
            edit.item.old_vat_regime_id=_this.$input[0].attributes.value.value;
        },
        onChange(value) {
            if (value) {
                var selectize = angular.element('#vat_regime_select')[0].selectize;
                selectize.blur();
                var _this = this;
                $confirm({
                        ok: helper.getLanguage('Yes'),
                        cancel: helper.getLanguage('No'),
                        text: helper.getLanguage('Apply VAT Regime on all lines')
                    }).then(function() {
                        if (_this.options[value].no_vat) {
                            edit.item.show_vat = !1;
                            edit.item.block_vat = !1;
                            edit.item.vat_regular = !1;
                            edit.item.vat = helper.displayNr(0);                     
                        } else {
                            if (_this.options[value].regime_type == '2') {
                                edit.item.vat = helper.displayNr(0);
                                edit.item.block_vat = !0;
                            } else if (_this.options[value].regime_type == '3') {
                                edit.item.vat = helper.displayNr(_this.options[value].value);
                                edit.item.block_vat = _this.options[value].regime_id == '4' ? !0 : !1;
                            } else {
                                edit.item.vat = helper.displayNr(_this.options[value].value);
                                edit.item.block_vat = !1
                            }
                            if (_this.options[value].regime_type == '1') {
                                edit.item.vat_regular = !0
                            } else {
                                edit.item.vat_regular = !1
                            }
                            edit.item.show_vat = !0
                        }
                        edit.vatApplyAll();
                        edit.changeLang();
                        var data = {
                            'do': 'quote-nquote',
                            xget: 'articles_list',
                            search: '',
                            buyer_id: edit.item.buyer_id,
                            lang_id: edit.item.email_language,
                            cat_id: edit.item.cat_id,
                            remove_vat: (edit.item.show_vat ? '0' : '1'),
                            vat_regime_id: edit.item.vat_regime_id
                        };
                        edit.requestAuto(edit, data, edit.renderArticleListAuto)
                    }, function(e) {
                        if(e == 'cancel'){
                            if (_this.options[value].no_vat) {
                                edit.item.show_vat = !1;
                                edit.item.block_vat = !1;
                                edit.item.vat_regular = !1;
                                edit.item.vat = helper.displayNr(0);
                                edit.vatApplyAll()
                            } else {
                                if (_this.options[value].regime_type == '2') {
                                    edit.item.vat = helper.displayNr(0);
                                    edit.item.block_vat = !0;
                                    edit.vatApplyAll()
                                } else if (_this.options[value].regime_type == '3') {
                                    edit.item.vat = helper.displayNr(_this.options[value].value);
                                    edit.item.block_vat = _this.options[value].regime_id == '4' ? !0 : !1;
                                    edit.vatApplyAll()
                                } else {
                                    edit.item.vat = helper.displayNr(_this.options[value].value);
                                    edit.item.block_vat = !1
                                }
                                if (_this.options[value].regime_type == '1') {
                                    edit.item.vat_regular = !0
                                } else {
                                    edit.item.vat_regular = !1
                                }
                                edit.item.show_vat = !0
                            }
                            edit.changeLang();
                            var data = {
                                'do': 'quote-nquote',
                                xget: 'articles_list',
                                search: '',
                                buyer_id: edit.item.buyer_id,
                                lang_id: edit.item.email_language,
                                cat_id: edit.item.cat_id,
                                remove_vat: (edit.item.show_vat ? '0' : '1'),
                                vat_regime_id: edit.item.vat_regime_id
                            };
                            edit.requestAuto(edit, data, edit.renderArticleListAuto)
                        }else if(e == 'dismiss'){
                            selectize.addItem(edit.item.old_vat_regime_id, true);
                        }                 
                    });            
            } else {
                edit.item.block_vat = !1;
                edit.item.vat_regular = !0;
                var data = {
                    'do': 'quote-nquote',
                    xget: 'articles_list',
                    search: '',
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id
                };
                edit.requestAuto(edit, data, edit.renderArticleListAuto)
            }      
        }
    });

    edit.selectCategoryCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        maxItems: 1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value) {
            $confirm({
                    text: helper.getLanguage('Recalculate prices')
                }).then(function() {
                    var data = {};
                    for (x in edit.item) {
                        data[x] = edit.item[x]
                    }
                    data['do'] = edit.quote_id != 'tmp' ? 'quote-nquote-quote-updatePriceCategory' : ''
                    data.changeCatPrices = '1';
                    data.item_id = edit.quote_id;

                    helper.doRequest("post", 'index.php', data, function(r) {
                        if (r.success) {
                           edit.renderPage(r);
                        } 
                    })

                }, function() {
                   // console.log('test');
                   
                })
            }
        }
    };

    edit.linevatcfg = {
        valueField: 'vat',
        labelField: 'name',
        searchField: ['vat'],
        delimiter: '|',
        placeholder: helper.getLanguage('VAT'),
        create: !1,
        maxOptions: 100,
        onDelete(values) {
            return !1
        },
        closeAfterSelect: !0,
        maxItems: 1
    };
    edit.selectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    edit.showTxt = {
        quote_settings: !1,
        addBtn: !0,
        address: !1,
        addArticleBtn: !1,
        notes: !1,
    }
    edit.contactAddObj = {};
    edit.auto = {
        options: {
            contacts: [],
            addresses: [],
            cc: [],
            articlesList: []
        }
    };
    edit.format = 'dd/MM/yyyy';
    edit.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    edit.datePickOpen = {
        validity_date: !1,
        date: !1
    }
    edit.cancel = function() {
        if (isNaN(edit.quote_id)) {
            $state.go('quotes')
        } else if(!isNaN(edit.duplicate_quote_id)) {
            $state.go('quote.view', {
                quote_id: edit.duplicate_quote_id,
                version_id:edit.item.version_id
            })
        } else {
            $state.go('quote.view', {
                quote_id: edit.quote_id
            });
        }
    }
    edit.articlesListAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code', 'supplier_reference', 'family'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.family) {
                    item.family = "/ " + item.family
                }
                if(item.allow_stock && item.is_service == 0){
                    var stock_display = '';
                    if (item.hide_stock == 0) {
                        stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                    }
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + stock_display + '</div>' + '</div>';
                }else{
                    var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-right">' + (item.base_price) + '</div>' + '</div>' + '</div>'
                }            
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span>' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'quote-nquote',
                    xget: 'articles_list',
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id
                };
                edit.requestAuto(edit, data, edit.renderArticleListAuto)
            }, 300)
        },
        onChange(value) {
            if (value == undefined) {} else if (value == '99999999999') {
                edit.openModal(edit, 'AddAnArticle', {
                    'do': 'quote-addAnArticle',
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id
                }, 'md');
                return !1
            } else {
                var collection = edit.auto.options.articles_list;
                for (x in collection) {
                    if (collection[x].article_id == value) {
                        editItem.addQuoteArticle(edit, collection[x]);
                        edit.selected_article_id = '';
                        edit.showTxt.addArticleBtn = !1;
                        var data = {
                            'do': 'quote-nquote',
                            xget: 'articles_list',
                            buyer_id: edit.item.buyer_id,
                            lang_id: edit.item.email_language,
                            cat_id: edit.item.cat_id,
                            remove_vat: (edit.item.show_vat ? '0' : '1'),
                            vat_regime_id: edit.item.vat_regime_id
                        };
                        edit.requestAuto(edit, data, edit.renderArticleListAuto);
                        if($rootScope.edited_data.indexOf('quote_edit') === -1){
                            $rootScope.edited_data.push('quote_edit');
                        }                    
                        break
                    }
                }
            }
        },
        onItemAdd(value, $item) {
            if (value == '99999999999') {
                edit.selected_article_id = ''
            }
        },
        onBlur() {
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'quote-nquote',
                    xget: 'articles_list',
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id
                };
                edit.showTxt.addArticleBtn = !1;
                if (edit.originalArticleList === undefined) {
                    edit.requestAuto(edit, data, edit.renderArticleListAuto)
                } else {
                    edit.auto.options.articles_list = edit.originalArticleList
                }
            }, 300)
        },
        maxItems: 1
    };

    edit.parentVariantCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code', 'supplier_reference', 'family'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        maxOptions: 5,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.family) {
                    item.family = "/ " + item.family
                }
                var stock_display = '';
                if (item.hide_stock == 0) {
                    stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + stock_display + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span>' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onFocus(){
            var selectize=this.$input[0].selectize;
            var index=this.$input[0].attributes['selectindex'].value;
            var parentVariant=this.$input[0].attributes['selectarticle'].value;
            var tmp_data={
                'do':'misc-articleVariants',
                'parentVariant':parentVariant,
                buyer_id: edit.item.buyer_id,
                lang_id: edit.item.email_language,
                cat_id: edit.item.cat_id,
                remove_vat: (edit.item.show_vat ? '0' : '1'),
                vat_regime_id: edit.item.vat_regime_id,
                app:edit.app
            };
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get','index.php',tmp_data,function(r){
                edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts=r.data.lines;
                if(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts.length){
                    selectize.open();
                }
            });
        },
        onType(str) {
            var selectize=this.$input[0].selectize;
            var index=this.$input[0].attributes['selectindex'].value;
            var parentVariant=this.$input[0].attributes['selectarticle'].value;         
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do':'misc-articleVariants',
                    'parentVariant':parentVariant,
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id,
                    app:edit.app
                };
                helper.doRequest('get','index.php',tmp_data,function(r){
                    edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts=r.data.lines;
                    if(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts.length){
                        selectize.open();
                    }
                });
            }, 300)
        },
        onChange(value) {

            var index=this.$input[0].attributes['selectindex'].value;
            if(value != undefined){
                var collection = edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts;
                for(x in collection){
                    if(collection[x].article_id == value){
                        edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].has_variants_done='1';
                        collection[x].is_variant_for_line = edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].tr_id;
                        editItem.addQuoteArticle(edit, collection[x], false, parseInt(index)+1);
                        break; 
                    }
                }
            }
        },
        onBlur() {
            var index=this.$input[0].attributes['selectindex'].value;
            var parentVariant=this.$input[0].attributes['selectarticle'].value;
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {                
                var tmp_data={
                    'do':'misc-articleVariants',
                    'parentVariant':parentVariant,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id,
                    app:edit.app
                };
                helper.doRequest('get','index.php',tmp_data,function(r){
                    edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts=r.data.lines;
                });
            }, 300)
        },
        maxItems: 1
    };

    edit.childVariantCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code', 'supplier_reference', 'family'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        maxOptions: 5,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.family) {
                    item.family = "/ " + item.family
                }
                var stock_display = '';
                if (item.hide_stock == 0) {
                    stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + stock_display + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span>' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onType(str) {
            var selectize=this.$input[0].selectize;
            var index=this.$input[0].attributes['selectindex'].value;
            var parentVariant=this.$input[0].attributes['selectarticle'].value;         
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do':'misc-articleVariants',
                    'parentVariant':parentVariant,
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id,
                    app:edit.app
                };
                helper.doRequest('get','index.php',tmp_data,function(r){
                    edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts=r.data.lines;
                    if(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts.length){
                        selectize.open();
                    }
                });
            }, 300)
        },
        onChange(value) {
            var index=this.$input[0].attributes['selectindex'].value;
            if(value != undefined){
                var tmp_data = angular.copy(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts);
                for(x in tmp_data){
                    if(tmp_data[x].article_id == value){
                        var tmp_selected_item=angular.copy(tmp_data[x]);
                        tmp_selected_item.quantity=parseFloat(helper.return_value(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].quantity,10));
                        edit.removeLine(index,edit.item.quote_group[edit.quote_group_active].table[0].quote_line);
                        editItem.addQuoteArticle(edit, tmp_selected_item, false, index);
                        break; 
                    }
                }
            }
        },
        onBlur() {
            var index=this.$input[0].attributes['selectindex'].value;
            edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].showVariants=false;
        },
        maxItems: 1
    };

    edit.renderPage = renderPage;
    edit.openDate = openDate;
    edit.save_date = save_date;
    edit.removeDeliveryAddr = removeDeliveryAddr;
    edit.save = save;
    edit.setVisibility = setVisibility;
    edit.removeLine = removeLine;
    edit.addArticle = addArticle;
    edit.calc = calc;
    edit.changeDiscount = changeDiscount;
    edit.vatApplyAll = vatApplyAll;
    edit.applyLineDiscount = applyLineDiscount;
    edit.showhide = showhide;
    edit.changeLang = changeLang;
    edit.delaySave = delaySave;
    edit.changeCurrency = changeCurrency;
    editItem.init(edit);

    function renderPage(d) {
        if (d && d.data) {
            if (d.data.redirect) {
                $state.go(d.data.redirect);
                return !1
            }
            edit.item = d.data;
            //console.log(edit.item);
            /*for (x in edit.item.quote_group) {
                edit.item.quote_group[x].show_AC = !0;
                if (edit.item.quote_group[x].table[0]) {
                    if (edit.item.quote_group[x].table[0].quote_line[0]) {
                        edit.item.quote_group[x].show_block_total = edit.item.quote_group[x].table[0].quote_line[0].show_block_total == '1' ? true : !1;
                        edit.item.quote_group[x].show_AC = edit.item.quote_group[x].table[0].quote_line[0].hide_ACv == '1' ? !1 : !0
                    }
                }
            }*/
            edit.checkVariants();
            if(((edit.item_id == 'tmp' && edit.old_buyer_id != d.data.buyer_id) || (edit.item_id != 'tmp' && edit.old_buyer_id !=undefined && edit.old_buyer_id != d.data.buyer_id)) && d.data.customer_credit_limit){
                if (d.data.customer_credit_limit) {
                    var msg = "<b>" + helper.getLanguage("") + "</b><span>" + helper.getLanguage("This client is over his credit limit of") + " " + d.data.customer_credit_limit + "</span>";
                    var cont = {
                        "e_message": msg,
                        'e_title': helper.getLanguage('Credit warning')
                    };
                    openModal("stock_info", cont, "md")
                }
            }
            /*if (edit.item_id == 'tmp' && d.data.quote_id) {
                edit.item_id = d.data.quote_id;
                edit.quote_id = edit.item_id;
                if (d.data.customer_credit_limit) {
                    var msg = "<b>" + helper.getLanguage("Warning") + ":</br> <span>" + helper.getLanguage("This client is over his credit limit of") + " " + d.data.customer_credit_limit + "</span></b>";
                    var cont = {
                        "e_message": msg,
                        'e_title': helper.getLanguage('Credit warning')
                    };
                    openModal("stock_info", cont, "md")
                }
                $state.go('quote.edit', {
                    quote_id: edit.item_id
                })
            }*/
            if (edit.item_id != 'tmp' && !edit.duplicate_quote_id) {
                $scope.vm.serial_number = edit.item.serial_number;
                if(edit.item.status_title){
                    $scope.vm.status_title = edit.item.status_title;
                }

                $scope.vm.status_nr = edit.item.status_nr;
                $scope.vm.version_code = edit.item.CURRENT_VERSION;
                $scope.vm.adv_quotes = edit.item.ADV_QUOTE;
                $scope.vm.title_text=helper.getLanguage('Quote');
                helper.updateTitle($state.current.name,edit.item.serial_number+' '+edit.item.CURRENT_VERSION);
            }
            if(edit.duplicate_quote_id){
                $scope.vm.times += 1;
                if ($scope.vm.times > 1) {
                    edit.item.quote_group = $scope.vm.quote_group;
                }
                $scope.vm.serial_number='';
                $scope.vm.status_nr='';
                $scope.vm.version_code='';
                $scope.vm.status_title='';
                if(edit.old_buyer_id != undefined ) {
                    for(i in edit.item.quote_group) {
                        if (edit.item.quote_group[i].table[0]) {
                            for (x in edit.item.quote_group[i].table[0].quote_line) {
                                if (edit.item.quote_group[i].table[0].quote_line[x].line_type == 1) {
                                    for (y in d.data.temp) {
                                        if (edit.item.quote_group[i].table[0].quote_line[x].line_type == d.data.temp[y].line_type) {
                                            if (edit.item.quote_group[i].table[0].quote_line[x].article_id == d.data.temp[y].article_id) {
                                                edit.item.quote_group[i].table[0].quote_line[x].price = d.data.temp[y].price;
                                                if (parseInt(edit.item.quote_group[i].table[0].quote_line[x].line_vat) != 0) {
                                                    edit.item.quote_group[i].table[0].quote_line[x].price_vat = helper.displayNr(parseFloat(edit.item.quote_group[i].table[0].quote_line[x].price) + parseFloat(parseFloat(edit.item.quote_group[i].table[0].quote_line[x].price) * parseFloat(edit.item.quote_group[i].table[0].quote_line[x].line_vat) / 100), helper.settings.ARTICLE_PRICE_COMMA_DIGITS);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $scope.vm.quote_group = edit.item.quote_group;
                edit.calcQuoteTotal(edit);
            }
            if (edit.item.validity_date) {
                edit.item.validity_date = new Date(edit.item.validity_date)
            }
            if (edit.item.quote_date) {
                edit.item.quote_date = new Date(edit.item.quote_date)
            }
            edit.auto.options.contacts = d.data.contacts;
            edit.auto.options.addresses = d.data.addresses;
            edit.auto.options.secondaryAddresses = d.data.secondaryAddresses;
            edit.auto.options.cc = d.data.cc;
            edit.auto.options.articles_list = d.data.articles_list && d.data.articles_list.lines ? d.data.articles_list.lines : [];
            if (edit.originalArticleList === undefined) {
                edit.originalArticleList = edit.auto.options.articles_list
            }
            edit.contactAddObj = {
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                language_dd: edit.item.language_dd,
                language_id: edit.item.language_id,
                gender_dd: edit.item.gender_dd,
                gender_id: edit.item.gender_id,
                title_dd: edit.item.title_dd,
                title_id: edit.item.title_id,
                add_contact: !0,
                buyer_id: edit.item.buyer_id,
                article: edit.item.tr_id,
                'do': 'quote-nquote-quote-tryAddC',
                identity_id: edit.item.identity_id,
                contact_only: !0,
                item_id: edit.item_id
            }
            helper.showAlerts(edit, d)
        }
        if (edit.duplicate_quote_id) {
            //angular.element('.loading_wrap').removeClass('hidden');
            /*helper.Loading++;
            var data_dup = angular.copy(edit.item);
            data_dup.do = 'quote-nquote-quote-add';
            helper.doRequest('post', 'index.php', data_dup, function(dot) {
                $state.go('quote.edit', {
                    'quote_id': dot.data.quote_id,
                    duplicate_quote_id: '',
                    version_id: ''
                }, {
                    reload: !0
                })
            })*/ //commented as for akti-4267 since automatic creation not needed
        }
        if(edit.item.vat_regime_changed && angular.element('#vat_regime_select')[0]){
            var vat_regime_selectize=angular.element('#vat_regime_select')[0].selectize;
            vat_regime_selectize.addItem(edit.item.vat_regime_changed);
        }
        if(edit.item.buyer_changed || edit.item.buyer_changed_save){
            edit.applyLineDiscount(true);
        }
    }

    function openDate(p) {
        edit.datePickOpen[p] = !0
    }

    function save_date() {
        var data = angular.copy(edit.item);
        data.do = 'quote-nquote-quote-update';
        helper.doRequest('post', 'index.php', data, function(d) {
            if (reload) {
                edit.renderPage(d)
            }
            helper.showAlerts(edit, d)
        })
    }

    function removeDeliveryAddr() {
        edit.item.delivery_address = '';
        edit.item.delivery_address_id = ''
    }

    function save(showLoading, reload) {
        var data = angular.copy(edit.item);
        data = helper.clearData(data, ['APPLY_DISCOUNT_LIST', 'CURRENCY_TYPE_LIST', 'SOURCE_DD', 'STAGE_DD', 'TYPE_DD', 'accmanager', 'addresses', 'authors', 'articles_list', 'authors', 'cc', 'contacts', 'country_dd', 'language_dd', 'main_comp_info', 'sort_order']);
        if (edit.quote_id == 'tmp') {
            data.do = 'quote-nquote-quote-add'
        } else {
            data.do = 'quote-nquote-quote-update'
        }
        if (edit.duplicate_quote_id) {
            data.do = 'quote-nquote-quote-add'
        }
        if (showLoading) {
            //helper.Loading++;
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                if (r && r.success) {
                    if (edit.quote_id == 'tmp') {
                        /*if (edit.quote_id == 'tmp' && edit.quote_id != r.data.quote_id && r.data.customer_credit_limit) {
                            var msg = "<b class='text-danger'>" + helper.getLanguage("Warning") + ":</br> <span>" + helper.getLanguage("This client is over his credit limit of") + " " + r.data.customer_credit_limit + "</span></b>";
                            var cont = {
                                "e_message": msg
                            };
                            openModal("stock_info", cont, "md")
                        }*/
                        $state.go('quote.view', {
                            quote_id: r.data.quote_id
                        })
                    } else {
                        $state.go('quote.view', {
                            quote_id: r.data.quote_id,
                            version_id:r.data.version_id
                        })
                    }
                }else{
                    helper.showAlerts(edit, r)
                }              
            })
        } else {
            helper.doRequest('post', 'index.php', data, function(d) {
                if (reload) {
                    edit.renderPage(d)
                }
                if (edit.is_add_article) {
                    edit.renderPage(d);
                    edit.is_add_article = !1
                }
                //helper.showAlerts(edit, d)
            })
        }
    }

     function setVisibility(obj, i) {
       obj[i].visible = !obj[i].visible;
    }

    function removeLine($index, obj, isChapter) {
        if($rootScope.edited_data.indexOf('quote_edit') === -1){
            $rootScope.edited_data.push('quote_edit');
        }

        if(isChapter){
            obj.splice($index, 1);         
            if($index>0){
                /*edit.quote_group_active=$index-1;
                edit.item.quote_group[$index - 1].active = !0*/
                edit.quote_group_active=0;
                edit.item.quote_group[0].active = !0
            }else{
                edit.quote_group_active = 0;
                if(edit.item.quote_group.length){
                    edit.item.quote_group[0].active = !0    
                }
            }
            edit.calcQuoteTotal(edit);
            edit.checkVariants();
            return false;
        }
          
        var line_id = obj[$index].line_id;   
        if (line_id) {
            if ($index > -1) {
                if (!isChapter) {                    
                    if(obj[$index].has_variants == true || obj[$index].has_variants == '1'){
                       // removeVariant(obj[$index].article_id, obj);
                       removeVariant(line_id, obj);
                    }else{
                        removeTaxes(line_id, obj);
                        removeComponent(line_id, obj);
                    }
                }
               
                obj.splice($index, 1);
                edit.calcQuoteTotal(edit);
                if (isChapter) {
                    edit.quote_group_active = 0;
                    edit.item.quote_group[0].active = !0
                }
                edit.checkVariants();
            }
        } else {
            var tr_id = obj[$index].tr_id;
            if(obj[$index].has_variants == true || obj[$index].has_variants == '1'){
                //removeVariant(obj[$index].article_id, obj);
                removeVariant(tr_id, obj);
            }
            removeTaxes(tr_id, obj);
            removeComponent(tr_id, obj);
            obj.splice($index, 1);
            edit.calcQuoteTotal(edit);
            if (isChapter) {
                edit.quote_group_active = 0;
                edit.item.quote_group[$index - 1].active = !0
            }
            edit.checkVariants();
        }
    }

    function removeVariant(id, obj) {
        
        for (x in obj) { /*console.log(id, obj,obj[x].is_variant_for);*/
           // if (obj[x].is_variant_for == id) {
            if (obj[x].is_variant_for_line == id) {
                obj.splice(x, 1);
                break;
            }
        }
    }

    function removeComponent(id, obj) {
        for (x in obj) {
            if (obj[x].component_for == id) {
                obj.splice(x, 1);
                removeComponent(id, obj)
            }
        }
    }

    edit.showChildVariants=function(item,index){
        var tmp_data = {
            'do':'misc-articleVariants',
            'parentVariant':item.is_variant_for,
            'is_variant_for_line':item.is_variant_for_line,
            buyer_id: edit.item.buyer_id,
            lang_id: edit.item.email_language,
            cat_id: edit.item.cat_id,
            remove_vat: (edit.item.show_vat ? '0' : '1'),
            vat_regime_id: edit.item.vat_regime_id,
            app:edit.app
        };
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get','index.php',tmp_data,function(r){
            item.showVariants=true;
            edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts=r.data.lines;
        });
    }

    edit.changeBulkVariants=function(){
        openModal('bulk_change_variants',{},'md');
    }

    edit.checkVariants=function(){
        edit.showBulkChangeVariants=false;
        if(edit.item.quote_group.length){
            if(edit.item.quote_group[edit.quote_group_active].table[0]){
                for(x in edit.item.quote_group[edit.quote_group_active].table[0].quote_line){
                    if(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[x].is_variant_for>0){
                        edit.showBulkChangeVariants=true;
                    }              
                }
            }
        }            
    }

    edit.bulkVariantsApply=function(from,to,ind){
        if(ind < edit.item.quote_group[edit.quote_group_active].table[0].quote_line.length){
            for(y=ind; y < edit.item.quote_group[edit.quote_group_active].table[0].quote_line.length; y++){
                if(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[y].is_variant_for > 0 && edit.item.quote_group[edit.quote_group_active].table[0].quote_line[y].variant_type == from){
                    var new_index=y;
                    //angular.element('.loading_wrap').removeClass('hidden');
                    var tmp_data = {
                        'do':'misc-articleVariants',
                        'parentVariant':edit.item.quote_group[edit.quote_group_active].table[0].quote_line[y].is_variant_for,
                        'is_variant_for_line':edit.item.quote_group[edit.quote_group_active].table[0].quote_line[y].is_variant_for_line,
                        buyer_id: edit.item.buyer_id,
                        lang_id: edit.item.email_language,
                        cat_id: edit.item.cat_id,
                        remove_vat: (edit.item.show_vat ? '0' : '1'),
                        vat_regime_id: edit.item.vat_regime_id,
                        variant_type_search:to,
                        app:edit.app
                    };
                    helper.doRequest('get','index.php',tmp_data,function(o){
                        if(o.data.lines.length){
                            var tmp_item = o.data.lines[0];
                            tmp_item.quantity=parseFloat(helper.return_value(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[y].quantity,10));
                            edit.removeLine(y,edit.item.quote_group[edit.quote_group_active].table[0].quote_line);
                            editItem.addQuoteArticle(edit, tmp_item, false, new_index);                      
                        }
                        var incremented_index=parseInt(new_index)+1;
                        $timeout(function() {
                            edit.bulkVariantsApply(from,to,incremented_index);
                        }, 1000);
                    });
                    break;
                }
            }
        }   
    }

    function removeTaxes(id, obj) {
        //console.log(id, obj);
        for (x in obj) {
            if (obj[x].for_article == id) {
                obj.splice(x, 1);
                removeTaxes(id, obj)
            }
        }
    }

    function delaySave(showLoading, reload) {
        angular.element('.keep-open-on-click').on('click', function(e) {
            console.log('ccc');
            e.stopPropagation()
        });
        if (edit.typePromise) {
            console.log('ccc1');
            $timeout.cancel(edit.typePromise)
        }
        edit.typePromise = $timeout(function() {
            console.log('ccc2');
            //edit.save(showLoading, reload)
        }, 2000)
    }

    function addArticle() {
        edit.showTxt.addArticleBtn = !0;
        $timeout(function() {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.open()
        })
    }
    edit.changePurchasePrice = function(index) {
        var temp = {};
        temp.purchase_price = edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].purchase_price;
        temp.index = index;
        openModal("edit_purchase_price", temp, "md")
    }

    edit.updatePdfChapterTotal=function(val){
        if(val){
            var chapterSubtotalExists = false;
            for (x in edit.item.quote_group[edit.quote_group_active].table[0].quote_line) {
                if (parseInt(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[x].line_type) == 8) {
                    chapterSubtotalExists = true;
                    break;
                }
            }
            if (!chapterSubtotalExists) {
                editItem.addQuoteArticle(edit, '', 8);
            }
        }
    }

    edit.contactUpdated=function(language){
        //if(edit.quote_id && !isNaN(edit.quote_id)){
            if(language && language>0 && language != edit.item.email_language){
                var selectize = angular.element('#email_language')[0].selectize;
                selectize.addItem(language);
            }
        //}
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'edit_purchase_price':
                var iTemplate = 'PurchasePrice';
                var ctas = 'pprice';
                break;
            case 'create_install':
                var iTemplate = 'CreateInstall';
                var ctas = 'vm';
                break;
            case 'install_data':
                var iTemplate = 'InstallData';
                var ctas = 'vm';
                break;
            case 'create_deal':
                var iTemplate = 'CreateDeal';
                var ctas = 'show';
                break;
            case 'stock_info':
                var iTemplate = 'modal';
                var ctas = 'vmMod';
                break;
            case 'bulk_change_variants':
                var iTemplate = 'BulkChangeVariants';
                var ctas = 'vmMod';
                break;
        }
        var params = {
            template: 'qTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'edit_purchase_price':
                params.params = {
                    'do': 'quote-' + type,
                    purchase_price: item.purchase_price,
                    index: item.index
                };
                params.callback = function(data) {
                    if (data) {
                        edit.item.quote_group[edit.quote_group_active].table[0].quote_line[data.index].purchase_price = data.purchase_price
                    }
                }
                break;
            case 'create_install':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'installation-installation',
                    'installation_id': 'tmp',
                    'buyer_id': edit.item.buyer_id,
                    'main_address_id': edit.item.main_address_id
                };
                params.callback = function(d) {
                    if (d) {
                        var data = {
                            'do': 'quote-nquote',
                            xget: 'installations',
                            buyer_id: edit.item.buyer_id
                        };
                        helper.doRequest('get', 'index.php', data, function(r) {
                            edit.item.installations = r.data;
                            editItem.init(edit)
                        })
                    }
                    edit.item.installation_id = edit.item.old_installation_id
                }
                break;
            case 'install_data':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                var sr_n = '';
                for (x in edit.item.installations) {
                    if (edit.item.installations[x].installation_id == item) {
                        sr_n = edit.item.installations[x].serial_number;
                        break
                    }
                }
                params.params = {
                    'do': 'misc-s3_link',
                    's3_folder': 'installation',
                    'id': edit.item.installation_id
                };
                break;
            case 'create_deal':
                params.template = 'cTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.params = {
                    'do': 'customers-deal_edit',
                    'opportunity_id': 'tmp',
                    'stage_id': edit.item.stage_id,
                    'board_id': edit.item.board_id,
                    'user_id': edit.item.user_id,
                    'buyer_id': edit.item.buyer_id,
                    'contact_id': edit.item.contact_id,
                    'not_redirect_deal': 0,
                    'amount':edit.item.GRAND_TOTAL,
                    'subject':edit.item.subject
                };
                params.callback = function(d) {
                    if (d) {
                        var data = {
                            'do': 'quote-nquote',
                            xget: 'deals',
                            buyer_id: edit.item.buyer_id
                        };
                        helper.doRequest('get', 'index.php', data, function(r) {
                            edit.item.deals = r.data;
                            edit.item.deal_id=d.deal_id;
                            edit.item.old_deal_id=d.deal_id;
                            editItem.init(edit)
                        })
                    }else{
                        edit.item.deal_id = edit.item.old_deal_id
                    }               
                }
                break;
            case 'stock_info':
                params.template = 'miscTemplate/' + iTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                break;
            case 'bulk_change_variants':
                params.template = 'miscTemplate/' + iTemplate+'Modal';
                params.ctrl = 'BulkChangeVariantsModalCtrl';
                params.params = {'do':'misc-addArticleList','xget':'variant_types','title':edit.item.quote_group[edit.quote_group_active].title};
                params.callback=function(d){
                    if(d){
                        edit.bulkVariantsApply(d.from,d.to,0);
                    }
                }
                break;
        }
        modalFc.open(params)
    }

    function calc(text, $index, id) {
        edit.calcQuoteTotal(edit, text, $index, id)
    }

    function vatApplyAll() {
        var collections = edit.item.quote_group;
        for (x in collections) {
            var table = collections[x].table;
            if (table.length) {
                for (y in table) {
                    var lines = table[y].quote_line;
                    if (lines.length) {
                        for (i in lines) {
                            lines[i].line_vat = edit.item.vat;
                            lines[i].price_vat = parseFloat(helper.return_value(lines[i].price), 10) + (parseFloat(helper.return_value(lines[i].price), 10) * parseFloat(helper.return_value(edit.item.vat), 10)) / 100;
                            lines[i].price_vat = helper.displayNr(lines[i].price_vat)
                        }
                    }
                }
            }
        }
        edit.item.vat_lines.vat_value = parseFloat(helper.return_value(lines[i].net_amount), 10) + (parseFloat(helper.return_value(lines[i].net_amount), 10) * parseFloat(helper.return_value(edit.item.vat), 10)) / 100;
        edit.calcQuoteTotal(edit)
    }

    function changeDiscount() {
        if (edit.item.apply_discount_line == !0 && edit.item.apply_discount_global == !1) {
            edit.item.apply_discount = 1
        } else if (edit.item.apply_discount_global == !0 && edit.item.apply_discount_line == !1) {
            edit.item.apply_discount = 2
        } else if (edit.item.apply_discount_line == !0 && edit.item.apply_discount_global == !0) {
            edit.item.apply_discount = 3
        } else if (edit.item.apply_discount_line == !1 && edit.item.apply_discount_global == !1) {
            edit.item.apply_discount = 0
        }
        var collection = edit.item.quote_group;
        if (edit.item.apply_discount == 0 || edit.item.apply_discount == 2) {
            if (edit.item.VIEW_DISCOUNT === !0) {
                edit.item.colum++;
                edit.item.hcolum++;
                for (x in collection) {
                    for (y in collection[x].table) {
                        for (z in collection[x].table[y].quote_line) {
                            collection[x].table[y].quote_line[z].colum++
                        }
                    }
                }
                edit.item.VIEW_DISCOUNT = !1
            }
        } else {
            if (edit.item.VIEW_DISCOUNT === !1) {
                edit.item.colum--;
                edit.item.hcolum--;
                for (x in collection) {
                    for (y in collection[x].table) {
                        for (z in collection[x].table[y].quote_line) {
                            collection[x].table[y].quote_line[z].colum--
                        }
                    }
                }
                edit.item.VIEW_DISCOUNT = !0
            }
        }
        if (edit.item.apply_discount > 1) {
            edit.item.hide_global_discount = !0
        } else {
            edit.item.hide_global_discount = !1
        }
        if (edit.item.apply_discount == 1) {
            edit.applyLineDiscount(!0)
        } else if (edit.item.apply_discount == 0) {
            edit.applyLineDiscount(!0)
        }
        edit.calcQuoteTotal(edit)
    }

    function applyLineDiscount(both) {
        var collection = edit.item.quote_group;
        for (x in collection) {
            var table = collection[x].table;
            if (table.length) {
                for (y in table) {
                    var lines = table[y].quote_line;
                    if (lines.length) {
                        for (i in lines) {
                            if (both == !0) {
                                lines[i].line_discount = edit.item.discount_line_gen
                            } else {
                                lines[i].line_discount = edit.item.discount
                            }
                            if (edit.item.apply_discount == 0) {
                                lines[i].line_discount = 0
                            }
                        }
                    }
                }
            }
        }
        edit.calcQuoteTotal(edit)
    }

    function showhide(t, m) {
        var collection = edit.item.quote_group;
        for (x in collection) {
            var table = collection[x].table;
            if (table.length) {
                for (y in table) {
                    var lines = table[y].quote_line;
                    if (lines.length) {
                        for (i in lines) {
                            lines[i][t] = m
                        }
                    }
                }
            }
        }
    }

    function changeCurrency() {
        var data = {
            'do': 'quote-nquote',
            quote_id: edit.item_id,
            buyer_id: edit.item.buyer_id,
            'currency_type': edit.item.currency_type,
            'change_currency': '1'
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            if (r && r.data) {
                edit.item.currency_type = r.data.currency_type;
                edit.item.currency_val = r.data.currency_val;
                edit.item.currency_txt = r.data.currency_txt;
                edit.item.currency_rate = r.data.currency_rate;
                edit.item.total_currency_hide = r.data.total_currency_hide;
                edit.item.total_default_currency = helper.displayNr(helper.return_value(edit.item.total) * helper.return_value(r.data.currency_rate))
            }
        });
        //edit.calcQuoteTotal(edit)
    }

    function changeLang() {
        var data = {
            'do': 'quote-nquote',
            'lang_id': edit.item.email_language,
            'xget': 'notes',
            'vat_regime_id': edit.item.vat_regime_id
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            if (r && r.data) {
                for (x in r.data) {
                    edit.item[x] = r.data[x]
                }
            }
        })
    }
    edit.addLine = function(content, title) {
        editItem.addInvoiceLine(edit.item, content, title)
    }
    edit.tinymceOptions = {
        selector: "",
        resize: "both",
        width: 850,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        selector: 'textarea',
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };

    edit.isFreeTextFocused=undefined;
    angular.element($window).scroll(function() {
        $timeout(function(){
            if(edit.isFreeTextFocused === false){
                angular.element('.mce-tinymce-inline').css({'display':'none'});
                edit.isFreeTextFocused=undefined;
            }
        },50)
    });

    edit.tinymceOptionsLines = {
        selector: "",
        resize: "both",
        auto_resize: !0,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code autoresize"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        image_advtab: !0,
        autoresize_bottom_margin: 0,
        setup: function(editor) {
            editor.on('click', function(e) {
                e.stopPropagation();
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                angular.element(editor.editorContainer).parent().removeClass('wysiwyg-hide');
                edit.isFreeTextFocused=true;
            }).on('blur', function(e) {
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                edit.isFreeTextFocused=false;
                edit.delaySave()
            })
        },
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
        content_css: [helper.base_url + 'css/tinymce_styles.css']
    };
    /*angular.element('.content').on('click', function() {
        angular.element('.wysiwyg-holder').addClass('wysiwyg-hide')
    });*/
    edit.delaySave=function(showLoading, reload) {
        angular.element('.keep-open-on-click').on('click', function(e) {
            console.log('ccc');
            e.stopPropagation()
        });
        if (edit.typePromise) {
            console.log('ccc1');
            $timeout.cancel(edit.typePromise)
        }
        edit.typePromise = $timeout(function() {
            console.log('ccc2');
            //edit.save(showLoading, reload)
        }, 2000)
    }
    edit.showGroup = function($event, $index) {
        var collection = edit.item.quote_group;
        for (x in collection) {
            collection[x].active = !1;
            if (x == $index) {
                collection[x].active = !0
            }
        }
        if ($index !== undefined) {
            edit.quote_group_active = $index;
        }
        edit.checkVariants();
    }
    edit.dragControlListeners = {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            var collection = edit.item.quote_group;
            var next_idx_1 = event.dest.index +1;
            var next_idx_2 =event.source.index +1;
         
           if(collection[edit.quote_group_active].table[0].quote_line[event.source.index].component_for){
            //not working 
                /*console.log(event.source.index, event.dest.index, collection[edit.quote_group_active].table[0].quote_line[event.source.index], collection[edit.quote_group_active].table[0].quote_line[event.dest.index]);
                console.log(event.source, event.dest);
                event.dest.sortableScope.insertItem(event.source.index, collection[edit.quote_group_active].table[0].quote_line[event.dest.index]);
                 event.source.sortableScope.removeItem(event.dest.index);*/
           }
            if(collection[edit.quote_group_active].table[0].quote_line[event.dest.index].is_combined){

                var lines = collection[edit.quote_group_active].table[0].quote_line;
                var line_id = 0;
                if(collection[edit.quote_group_active].table[0].quote_line[event.dest.index].line_id){
                    line_id = collection[edit.quote_group_active].table[0].quote_line[event.dest.index].line_id;                 
                }else{
                    line_id = collection[edit.quote_group_active].table[0].quote_line[event.dest.index].tr_id
                }
                
                for(x in lines){
                    if(lines[x].component_for == line_id){
                       //console.log(event.source.index, event.dest.index);
                       if(event.source.index < event.dest.index){
                            event.dest.sortableScope.insertItem(next_idx_1, collection[edit.quote_group_active].table[0].quote_line[event.source.index]);
                            event.source.sortableScope.removeItem(event.source.index);
                       }else{
                        //console.log(next_idx_1, next_idx_2);
                              
                           event.dest.sortableScope.insertItem(next_idx_1, collection[edit.quote_group_active].table[0].quote_line[next_idx_2]);
                           event.source.sortableScope.removeItem(next_idx_2+1); 
                           next_idx_2 = next_idx_2+1;
                           next_idx_1 = next_idx_1+1;
                       }
                       
                    }
                }
            }
            edit.delaySave();
            $timeout(function() {
                angular.element('.table_orders').find('textarea[id^="ui-tinymce"]').each(function() {
                    var id = angular.element(this).attr('id');
                    if(id){
                        $scope.$broadcast('$tinymce:refresh', id)
                    }              
                })
            })
        },
        dragEnd: function(event) {
            if (event && event.dest.index == event.source.index) {
                var collection = edit.item.quote_group;
                if (collection[edit.quote_group_active].table[0].quote_line[event.dest.index].line_type == 3) {
                    $timeout(function() {
                        var id = angular.element('.table_orders').find('li').eq(event.dest.index).find('textarea').attr('id');
                        if(id){
                            $scope.$broadcast('$tinymce:refresh', id)
                        }                     
                    })
                }
            }
        }
    }
    edit.dragChaptersControlListeners = {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            if(event.source.index == edit.quote_group_active){
                edit.quote_group_active=event.dest.index;
            }
            edit.delaySave()
        }
    };
    edit.orderChapter = function($index, next) {
        if ($index == 0 && next === undefined) {
            return !1
        }
        if (next && $index == edit.item.quote_group.length - 1) {
            return !1
        }
        var ind = next ? $index + 1 : $index - 1,
            oldKey = angular.copy(edit.item.quote_group[ind]),
            newKey = angular.copy(edit.item.quote_group[$index]);
        edit.item.quote_group[ind] = newKey;
        edit.item.quote_group[$index] = oldKey;
        edit.delaySave()
    }
    edit.installAutoCfg = {
        valueField: 'installation_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Installation'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.name) + ' </span>' + '</div>' + '</div>' + '</div>';
                if (item.installation_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Add Installation') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined || value == '') {
                edit.item.old_installation_id = value
            } else if (value == '99999999999') {
                openModal('create_install', {}, 'lg')
            } else {
                edit.item.old_installation_id = value
            }
        },
        onType(str) {
            var selectize = angular.element('#installation_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'quote-nquote',
                    xget: 'installations',
                    search: str,
                    quote_id: edit.item.quote_id,
                    version_id: edit.item.version_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.installations = r.data;
                    if (edit.item.installations.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        maxItems: 1
    };
    edit.viewInstall = function(id) {
        var url = $state.href('installation.view', {
            'installation_id': id
        });
        $window.open(url, '_blank')
    }
    edit.viewDataInstall = function(id) {
        openModal("install_data", id, "lg")
    }
    edit.dealAutoCfg = {
        valueField: 'deal_id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Deal'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<span>' + escape(item.name) + ' </span>' + '</div>' + '</div>' + '</div>';
                if (item.deal_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Add Deal') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined || value == '') {
                edit.item.old_deal_id = value
            } else if (value == '99999999999') {
                openModal('create_deal', {}, 'lg')
            } else {
                edit.item.old_deal_id = value
            }
        },
        onType(str) {
            var selectize = angular.element('#deal_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'quote-nquote',
                    xget: 'deals',
                    search: str,
                    quote_id: edit.item.quote_id,
                    version_id: edit.item.version_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    edit.item.deals = r.data;
                    if (edit.item.deals.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        maxItems: 1
    };
    edit.renderPage(data)
}]).controller('PurchasePriceModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var pprice = this;
    pprice.obj = data;
    pprice.ok = function() {
        $uibModalInstance.close(pprice.obj)
    };
    pprice.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('QuoteViewCtrl', ['$scope', '$rootScope','$stateParams', '$state', '$timeout', '$cacheFactory', '$confirm', 'helper', 'editItem', 'modalFc', 'selectizeFc', 'data', 'Lightbox', function($scope, $rootScope, $stateParams, $state, $timeout, $cacheFactory, $confirm, helper, editItem, modalFc, selectizeFc, data, Lightbox) {
    if($rootScope.edited_data.indexOf('quote_edit') !== -1){
        $rootScope.edited_data.splice($rootScope.edited_data.indexOf('quote_edit'), 1);
    }

    var view = this,
        pdfDoc = null,
        pageNum = 1,
        pageRendering = !1,
        pageNumPending = null,
        scale = 1.5,
        canvas = document.getElementById('the-canvas'),
        ctx = canvas.getContext('2d')
        pdfLoaded=false,
        dropLoaded=false;
        PDFJS.workerSrc = 'js/libraries/pdf.worker.js';
    view.quote_id = $stateParams.quote_id;
    view.item = {};
    view.openModal = openModal;
    view.dropbox_click = !1;
    view.showTxt = {
        sendEmail: !1,
        drop: !1,
        markAsReady: !1,
        viewNotes: !1,
        viewMarkAsSent: !1,
        showTxt: !1,
        sendPost: !1,
        source: !1,
        type: !1,
        segment: !1,
    };
    view.show_load = !0;
    view.email = {};
    view.auto = {
        options: {
            recipients: []
        }
    };
    view.contactForEmailAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select recipient'),
        create: !0,
        createOnBlur:true,
        maxItems: 100
    });
    view.stationaryCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select E-Stationary'),
        create: !1,
        maxOptions: 100,
        closeAfterSelect: !0,
        maxItems: 1
    };
    view.selectfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    view.format = 'dd/MM/yyyy';
    view.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1,
    };
    view.datePickOpen = {
        validity_date: !1,
        date: !1
    }
    view.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        height: 300,
        relative_urls: 1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    view.renderPage = renderPag;
    view.showAlerts = showAlerts;
    view.sendEmails = sendEmails;
    view.markQuote = markQuote;
    view.markQuoteAsSent = markQuoteAsSent;
    view.showPDFsettings = showPDFsettings;
    view.showPreview = showPreview;
    view.showWeblinkLog = showWeblinkLog;
    view.lost = lost;
    view.is_admin = data.data.is_admin;
    view.id_quote = data.data.id_quote;
    editItem.init(view);
    old_add = {};
    view.salesCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: view.is_admin,
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'quote_id': view.id_quote,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                view[realModel] = o.data.lines;
                view.item[realModel] = o.data.inserted_id
            })
        }
    }, !1, !0, view);
    view.extraCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 200,
        maxItems: 1,
        create: !1,
        onDropdownOpen($dropdown) {
            var _this = this,
                model_id = _this.$input[0].attributes.model.value;
            if (view.item[model_id] != undefined && view.item[model_id] != '0') {
                old_add[model_id] = view.item[model_id]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                view.item[realModel] = o.data.lines;
                view.item[realModel + '_id'] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            var opts = _this.$input[0].attributes.opts.value;
            var extra_id = undefined;
            if (item == '0') {
                view.openModal('selectEdit', {
                    table: table,
                    model: model,
                    extra_id: extra_id,
                    opts: opts
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                view.item[model] = undefined;
                return !1
            } else {
                view.showTxt.viewLost = !1;
                view.markQuote('status', {
                    status_customer: 1,
                    lost_id: view.item.lost_id
                })
            }
        }
    }, !1, !0, view.item);
    view.openModal = function(type, item, size, txt) {
        var params = {
            template: 'atemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            txt: txt,
            backdrop: 'static'
        };
        switch (type) {
            case 'selectEdit':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.params = {
                    'do': 'misc-' + type,
                    'table': item.table
                };
                params.callback = function(data) {
                    if (data.data) {
                        view.item[item.opts] = data.data.lines;
                        view.item[item.opts].push({
                            'id': '0',
                            'name': ''
                        });
                        console.log(view.item[item.opts]);
                        if (old_add[item.model]) {
                            view.item[item.model] = old_add[item.model]
                        }
                    } else {
                        view.item[item.opts] = data.lines;
                        if (old_add[item.model]) {
                            view.item[item.model] = old_add[item.model]
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }

    function renderPag(d) {
        if (d.data) {
            if (!d.error) {
                var old_drop=undefined;
                if(view.item.dropbox){
                    old_drop=view.item.dropbox;
                }
                view.item = d.data; 
                if(old_drop != undefined){
                    view.item.dropbox=old_drop;
                }               
                view.auto.options.recipients = d.data.recipients;
                view.item.email.recipients = d.data.email.recipients;
                view.item.email.sendgrid_selected = d.data.sendgrid_selected;
                view.item.save_disabled = d.data.email.save_disabled;

                if (view.item.sent_date) {
                    view.item.sent_date = new Date(view.item.sent_date)
                }
                if (view.item.quote_version_date) {
                    view.item.quote_version_date = new Date(view.item.quote_version_date)
                }
                view.dateOptions.minDate = view.item.quote_version_date;
                $scope.vm.serial_number = view.item.serial_number;
                $scope.vm.status_title = view.item.status_title;
                $scope.vm.status_nr = view.item.status_nr;
                helper.updateTitle($state.current.name,view.item.serial_number);
                if (view.item.version_code == 'A') {
                    $scope.vm.version_code = ''
                } else {
                    $scope.vm.version_code = '[' + view.item.version_code + ']'
                }
                $scope.vm.adv_quotes = view.item.ADV_QUOTE;
                view.showTxt.viewLost=false;
                if(!pdfLoaded){
                    pdfLoaded=true;
                    angular.element('#quotePreview center').show();
                    angular.element('#bxWrapper').addClass('hide');
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                    PDFJS.externalLinkTarget = PDFJS.LinkTarget.BLANK;
                    PDFJS.getDocument(view.item.PDF_LINK2).then(function(pdfDoc_) {
                        pdfDoc = pdfDoc_;
                        document.getElementById('page_count').textContent = '/ ' + pdfDoc.numPages;
                        renderPage(pageNum)
                    })
                } 
                if(!dropLoaded){
                    dropLoaded=true;
                    view.showDrop();
                } 
                if(d.data.status_customer && d.data.status_customer == '2'){
                    view.showTxt.viewLost=false;
                }             
            }
            if(d.data.status_customer && d.data.status_customer == '2'){
                
            }else{
                view.showAlerts(d)
            }
        }
    }
    view.myFunction = function() {
        if (view.item.email.recipients.length) {
            view.item.save_disabled = !1
        } else {
            view.item.save_disabled = !0
        }
    }

    function showPreview(exp) {
        var params = {
            template: 'miscTemplate/modal',
            ctrl: 'viewRecepitCtrl',
            size: 'md',
            ctrlAs: 'vmMod',
            request_type: 'post',
            params: {
                'do': 'quote-nquote',
                quote_id: view.quote_id,
                version_id: view.item.version_id,
                message: exp.e_message,
                use_html: exp.use_html,
                xget: 'email_preview'
            }
        }
        modalFc.open(params)
    };

    function sendEmails(r) {
        var d = angular.copy(r);
        view.showTxt.sendEmail = !1;
        //console.log(d.recipients,d.recipients.length);
        if (d.recipients.length) {
            /*if (view.item.dropbox) {
                d.dropbox_files = view.item.dropbox.dropbox_files
            }*/
            if (Array.isArray(d.recipients)) {
                d.email = d.recipients[0]
            } else {
                d.email = d.recipients
            }
            if (d.email) {
                view.item.email.alerts.push({
                    type: 'email',
                    msg: d.email + ' - '
                });
                d.do = 'quote--quote-sendNewEmail';
                d.xview = 1;
                helper.doRequest('post', 'index.php', d, function(r) {
                    if (r.data.mark_as_sent == '1') {
                        view.item.selectedSent = 'selected';
                        view.item.selectedDraft = !1;
                        view.item.external_url = r.data.external_url;
                        view.item.web_url = r.data.web_url;
                        view.item.versions=r.data.versions;
                        $scope.vm.status_title = helper.getLanguage('Sent');
                        $scope.vm.status_nr = '5';
                    }
                    if (r.success && r.success.success) {
                        view.item.email.alerts[view.item.email.alerts.length - 1].type = 'success';
                        view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.success.success
                    }
                    if (r.notice && r.notice.notice) {
                        view.item.email.alerts[view.item.email.alerts.length - 1].type = 'info';
                        view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.notice.notice
                    }
                    if (r.error && r.error.error) {
                        view.item.email.alerts[view.item.email.alerts.length - 1].type = 'danger';
                        view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.error.error
                    }
                    if (Array.isArray(d.recipients)) {
                        d.recipients.shift();
                    }else{
                        d.recipients='';
                    }
                        d.logging_id = r.data.logging_id;
                        /*d.copy = !1;
                        d.copy_acc = !1;*/
                        if( d.new_email == d.user_loged_email){
                            d.copy = !1;
                        }
                        if(d.new_email == d.user_acc_email){
                            d.copy_acc = !1;
                        }
                        view.sendEmails(d);
                   

                })
            } else {
                if (Array.isArray(d.recipients)) {
                    d.recipients.shift();
                    view.sendEmails(d)
                }
            }
        } else {
             //console.log(d.user_loged_email,d.copy);
            if (d.user_loged_email && d.copy) {
                d.email = d.user_loged_email;
                d.copy = !1;
                d.mark_as_sent = 0;
                /*if (view.item.dropbox) {
                    d.dropbox_files = view.item.dropbox.dropbox_files
                }*/
                if (d.email) {
                    view.item.email.alerts.push({
                        type: 'email',
                        msg: d.email + ' - '
                    });
                    d.do = 'quote--quote-sendNewEmail';
                    d.xview = 1;
                    helper.doRequest('post', 'index.php', d, function(r) {
                        if (r.data.mark_as_sent == '1') {
                            view.item.selectedSent = 'selected';
                            view.item.selectedDraft = !1;
                            view.item.external_url = r.data.external_url;
                            view.item.web_url = r.data.web_url;
                            $scope.vm.status_title = helper.getLanguage('Sent');
                            $scope.vm.status_nr = '5';
                        }
                        if (r.success && r.success.success) {
                            view.item.email.alerts[view.item.email.alerts.length - 1].type = 'success';
                            view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.success.success
                        }
                        if (r.notice && r.notice.notice) {
                            view.item.email.alerts[view.item.email.alerts.length - 1].type = 'info';
                            view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.notice.notice
                        }
                        if (r.error && r.error.error) {
                            view.item.email.alerts[view.item.email.alerts.length - 1].type = 'danger';
                            view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.error.error
                        }
                        d.logging_id = r.data.logging_id;
                        d.user_loged_email = null;
                        d.user_mail_sent =1;
                        view.sendEmails(d)
                    })
                }
            } else if (d.user_acc_email && d.copy_acc) {

                if(d.user_mail_sent && d.user_acc_email == d.user_loged_email){
                  
                }else{
                    d.email = d.user_acc_email;
                    d.copy_acc = !1;
                    d.mark_as_sent = 0;
                    /*if (view.item.dropbox) {
                        d.dropbox_files = view.item.dropbox.dropbox_files
                    }*/
                    if (d.email) {
                        view.item.email.alerts.push({
                            type: 'email',
                            msg: d.email + ' - '
                        });
                        d.do = 'quote--quote-sendNewEmail';
                        d.xview = 1;
                        helper.doRequest('post', 'index.php', d, function(r) {
                            if (r.data.mark_as_sent == '1') {
                                view.item.selectedSent = 'selected';
                                view.item.selectedDraft = !1;
                                view.item.external_url = r.data.external_url;
                                view.item.web_url = r.data.web_url;
                                $scope.vm.status_title = helper.getLanguage('Sent');
                                $scope.vm.status_nr = '5';
                            }
                            if (r.success && r.success.success) {
                                view.item.email.alerts[view.item.email.alerts.length - 1].type = 'success';
                                view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.success.success
                            }
                            if (r.notice && r.notice.notice) {
                                view.item.email.alerts[view.item.email.alerts.length - 1].type = 'info';
                                view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.notice.notice
                            }
                            if (r.error && r.error.error) {
                                view.item.email.alerts[view.item.email.alerts.length - 1].type = 'danger';
                                view.item.email.alerts[view.item.email.alerts.length - 1].msg += ' ' + r.error.error
                            }
                            d.logging_id = r.data.logging_id
                        })
                    }
                }
            }else{
                helper.refreshLogging();
            }
        }
          
    }

    function showAlerts(d) {
        helper.showAlerts(view, d)
    }

    function markQuote(func, obj) {
        var data = {
            'do': 'quote-nquote-quote-' + func,
            quote_id: view.quote_id,
            xview: 1,
            version_id: view.item.version_id
        };
        if (obj) {
            angular.extend(data, obj)
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, view.renderPage)
    }

    function markQuoteAsSent() {
        view.showTxt.viewMarkAsSent = !1;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'quote-nquote-quote-mark_sent',
            quote_id: view.quote_id,
            xview: 1,
            sent_date: view.item.sent_date,
            version_id: view.item.version_id
        }, view.renderPage)
    }

    function lost() {
        view.showTxt.viewLost = !1;
        if (view.item.lost_dd != '0') {
            view.markQuote('status', {
                status_customer: 1,
                lost_id: view.item.lost_dd
            })
        } else {
            view.showTxt.viewLost = !1
        }
    }

    function showWeblinkLog() {
        var params = {
            template: 'qTemplate/weblink_log',
            ctrl: 'weblinkLogQCtrl',
            ctrlAs: 'vm',
            size: 'md',
            params: {
                'do': 'quote-nquote',
                quote_id: view.quote_id,
                xget: 'weblinkLog'
            }
        };
        modalFc.open(params)
    };

    function showPDFsettings(i) {
        var params = {
            template: 'qTemplate/pdf_settings',
            ctrl: 'pdfSettingsQCtrl',
            ctrlAs: 'vm',
            size: 'md',
            params: {
                'do': 'quote-nquote',
                quote_id: view.quote_id,
                xget: 'pdfSettings',
                version_id: view.item.version_id
            },
            callback: function(data) {
                pdfLoaded=false;
                view.renderPage(data)
            }
        };
        modalFc.open(params)
    };
    view.addVersion = function() {
        helper.Loading++;
        //angular.element('.loading_wrap').removeClass('hidden');
        var params = {
            do: 'quote-nquote-quote_version-next',
            quote_id: view.quote_id,
            xview: 1,
            current_version_id: view.item.version_id,
            apply_discount: view.item.apply_discount,
            discount: view.item.discount,
            discount_line_gen: view.item.discount_line_gen,
            subject:view.item.subject
        }
        helper.doRequest('post', 'index.php', params, function(d) {
            $state.go('quote.edit', {
                'quote_id': view.quote_id
            })
        })
    }
    view.showDrop = function() {
        view.show_load = !0;
        var data_drop = angular.copy(view.item.drop_info);
        data_drop.do = 'misc-dropbox_new';
        helper.doRequest('post', 'index.php', data_drop, function(r) {
            view.item.dropbox = r.data;
            view.show_load = !1
        })
    }
    view.deleteDropFile = function(index) {
        var data = angular.copy(view.item.dropbox.dropbox_files[index].delete_file_link);
        view.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            view.show_load = !1;
            view.showAlerts(r);
            if (r.success) {
                view.item.dropbox.nr--;
                view.item.dropbox.dropbox_files.splice(index, 1)
            }
        })
    }
    view.deleteDropImage = function(index) {
        var data = angular.copy(view.item.dropbox.dropbox_images[index].delete_file_link);
        view.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            view.show_load = !1;
            view.showAlerts(r);
            if (r.success) {
                view.item.dropbox.nr--;
                view.item.dropbox.dropbox_images.splice(index, 1)
            }
        })
    }
    view.openLightboxModal = function(index) {
        Lightbox.openModal(view.item.dropbox.dropbox_images, index)
    };
    view.uploadDoc = function(type) {
        var obj = angular.copy(view.item.drop_info);
        obj.type = type;
        openModal('uploadDoc', obj, 'lg')
    }
    view.projectGo = function() {
        var cacheQuote = $cacheFactory.get('QuoteToProject');
        if (cacheQuote != undefined) {
            cacheQuote.destroy()
        }
        var cache = $cacheFactory('QuoteToProject');
        cache.put('data', view.item.quote_to_project_link);
        $state.go('project.edit', {
            project_id: "tmp"
        })
    }
    view.serviceGo = function() {
        var cacheQuote = $cacheFactory.get('QuoteToService');
        if (cacheQuote != undefined) {
            cacheQuote.destroy()
        }
        var cache = $cacheFactory('QuoteToService');
        cache.put('data', view.item.quote_to_service_link);
        $state.go('service.edit', {
            service_id: "tmp"
        })
    }

    function renderPage(num) {
        pageRendering = !0;
        pdfDoc.getPage(num).then(function(page) {
            var viewport = page.getViewport(scale);
            canvas.height = viewport.height;
            canvas.width = viewport.width;
            var renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };
            var renderTask = page.render(renderContext);
            renderTask.promise.then(function() {
                pageRendering = !1;
                if (pageNumPending !== null) {
                    renderPage(pageNumPending);
                    pageNumPending = null
                }
            });
            setupAnnotations(page, viewport, canvas, angular.element('#annotation-layer'));
        });
        document.getElementById('page_num').value = pageNum;
        $('#quotePreview center').hide();
        $('#bxWrapper').removeClass('hide')
    }

    function setupAnnotations(page, viewport, canvas, annotationLayerDiv){
        annotationLayerDiv.empty();
        var promise = page.getAnnotations().then(function (annotationsData) {
            // Canvas offset
            var canvas_offset = $(canvas).position();

            // Canvas height
            var canvas_height = $(canvas).get(0).height;

            // Canvas width
            var canvas_width = $(canvas).get(0).width;

            // CSS for annotation layer
            annotationLayerDiv.css({ left: canvas_offset.left + 'px', top: canvas_offset.top + 'px', height: canvas_height + 'px', width: canvas_width + 'px' });

            PDFJS.AnnotationLayer.render({
                viewport: viewport.clone({ dontFlip: true }),
                div: annotationLayerDiv[0],
                annotations: annotationsData,
                page: page
            });
        });
        return promise;
    }

    function queueRenderPage(num) {
        if (pageRendering) {
            pageNumPending = num
        } else {
            renderPage(num)
        }
    }

    function onPrevPage(e) {
        e.preventDefault();
        if (pageNum <= 1) {
            return
        }
        pageNum--;
        queueRenderPage(pageNum)
    }
    document.getElementById('prev').addEventListener('click', onPrevPage);

    function onNextPage(e) {
        e.preventDefault();
        if (pageNum >= pdfDoc.numPages) {
            return
        }
        pageNum++;
        queueRenderPage(pageNum)
    }
    document.getElementById('next').addEventListener('click', onNextPage);
    view.greenpost = function() {
        if (view.item.post_active) {
            view.showTxt.sendPost = !view.showTxt.sendPost
        } else {
            var data = {};
            data.quote_id = view.item.id;
            data.version_id = view.item.version_id;
            openModal("post_register", data, "md")
        }
    }
    view.sendPost = function() {
        var data = {};
        data.do = 'quote-nquote-quote-postIt';
        data.id = view.item.id;
        data.version_id = view.item.version_id;
        data.related = 'view';
        data.xview = 1;
        if (view.item.stationary_id) {
            data.stationary_id = view.item.stationary_id
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            view.showTxt.sendPost = !1;
            if (r.success) {
                view.renderPage(r)
            }
            if (r.error) {
                view.showAlerts(r)
            }
        })
    }
    view.resendGreenPost = function() {
        var data = {};
        data.do = 'quote-nquote-quote-postIt';
        data.id = view.item.id;
        data.version_id = view.item.version_id;
        data.related = 'view';
        data.xview = 1;
        data.post_library = angular.copy(view.item.post_library);
        openModal("resend_post", data, "md")
    }
    view.setSegment = function() {
        var obj = {
            'do': 'quote--quote-updateSegment',
            'id': view.item.id,
            'segment_id': view.item.segment_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                view.showTxt.segment = !1;
                view.item.segment = r.data.segment
            }
        })
    }
    view.setSource = function() {
        var obj = {
            'do': 'quote--quote-updateSource',
            'id': view.item.id,
            'source_id': view.item.source_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                view.showTxt.source = !1;
                view.item.source = r.data.source
            }
        })
    }
    view.setType = function() {
        var obj = {
            'do': 'quote--quote-updateType',
            'id': view.item.id,
            'type_id': view.item.type_id
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                view.showTxt.type = !1;
                view.item.xtype = r.data.xtype
            }
        })
    }

    view.createOrder=function(){
        var order_obj={ 'c_quote_id': view.quote_id,
                    'order_id': 'tmp', 
                    buyer_id: view.item.buyer_id, 
                    contact_id: view.item.contact_id, 
                    a_quote_id: view.item.quote_nr+(view.item.versions.length>1 ? ' ['+view.item.version_code+']' : ''), 
                    currency_type: view.item.currency_type, 
                    languages: view.item.email_language, 
                    vat_regime_id:view.item.vat_regime_id, 
                    identity_id:view.item.identity_id, 
                    source_id:view.item.source_id, 
                    type_id:view.item.type_id, 
                    segment_id:view.item.segment_id,
                    version_id:view.item.version_id };
        if(view.item.has_free_text){
            $confirm({
                    title:helper.getLanguage('Copy free text'),
                    text: helper.getLanguage('Do you want to copy free text from quote to the order?'),
                    ok:helper.getLanguage('Yes'),
                    cancel:helper.getLanguage('No'),
                }).then(function() {
                    order_obj.copy_free_text='1';
                    $state.go('order.edit', order_obj); 

                }, function(e) {
                    if(e == 'cancel'){
                        $state.go('order.edit', order_obj);
                    }              
                })
        }else{
            $state.go('order.edit', order_obj);
        }
    }

    view.createPOrder=function(){
       //create FacQ PO
        var order_obj={ 'quote_id': view.item.id,
                    'p_order_id': '', 
                    buyer_id: view.item.buyer_id, 
                    contact_id: view.item.contact_id, 
                    a_quote_id: view.item.quote_nr+(view.item.versions.length>1 ? ' ['+view.item.version_code+']' : ''), 
                    currency_type: view.item.currency_type, 
                    languages: view.item.email_language, 
                    vat_regime_id:view.item.vat_regime_id, 
                    identity_id:view.item.identity_id, 
                    source_id:view.item.source_id, 
                    type_id:view.item.type_id, 
                    segment_id:view.item.segment_id};
 
        /*var cacheQuote = $cacheFactory.get('QuoteToPurchase');
        if (cacheQuote != undefined) {
            cacheQuote.destroy()
        }
        var cache = $cacheFactory('QuoteToPurchase');
        cache.put('data', view.item.quote_to_purchase_link);
        $state.go('purchase_order.bulkfacq', order_obj);*/

         $state.go('purchase_order.edit', order_obj); 
 
    }

    view.createInvoice=function(){
        var inv_obj={ 'c_quote_id': view.quote_id, 'buyer_id': view.item.buyer_id, 'a_quote_id':view.item.serial_number+(view.item.versions.length>1 ? ' ['+view.item.version_code+']' : ''),'your_ref': view.item.own_reference };
        if(view.item.has_free_text){
            $confirm({
                    title:helper.getLanguage('Copy free text'),
                    text: helper.getLanguage('Do you want to copy free text from quote to the invoice?'),
                    ok:helper.getLanguage('Yes'),
                    cancel:helper.getLanguage('No'),
                }).then(function() {
                    inv_obj.copy_free_text='1';
                    $state.go('invoice.edit', inv_obj); 

                }, function() {
                   $state.go('invoice.edit', inv_obj);                 
                })
        }else{
            $state.go('invoice.edit', inv_obj);
        }
    }

    view.createProformaInvoice=function(){
        var inv_obj={ 'c_quote_id': view.quote_id, 'buyer_id': view.item.buyer_id, 'a_quote_id':view.item.serial_number+(view.item.versions.length>1 ? ' ['+view.item.version_code+']' : ''), 'type':1 };
        if(view.item.has_free_text){
            $confirm({
                    title:helper.getLanguage('Copy free text'),
                    text: helper.getLanguage('Do you want to copy free text from quote to the invoice?'),
                    ok:helper.getLanguage('Yes'),
                    cancel:helper.getLanguage('No'),
                }).then(function() {
                    inv_obj.copy_free_text='1';
                    $state.go('invoice.edit', inv_obj); 

                }, function() {
                   $state.go('invoice.edit', inv_obj);                 
                })
        }else{
            $state.go('invoice.edit', inv_obj);
        }
    }

    view.statusQuote=function(status){
        var tmp_obj={'id':view.quote_id,'quote_id':view.quote_id,'xview':'1'};
        tmp_obj.do=status=='1' ? 'quote-nquote-quote-archive': 'quote-nquote-quote-activate'; 
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post','index.php',tmp_obj,function(r){
            if(r.success){
                view.renderPage(r);
                helper.refreshLogging();
            }else{
                view.showAlerts(r);
            }
        });
    }

    view.deleteQuote=function(){
        var tmp_obj={'id':view.quote_id};
        tmp_obj.do='quote-quotes-quote-delete'; 
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post','index.php',tmp_obj,function(r){
            if(r.success){
                helper.setItem('quote-quotes',{});
                $state.go("quotes");
            }else{
                view.showAlerts(r);
            }
        });
    }

     view.PVCheck = function() {
        var obj = {
            'do': 'quote--quote-PVCheck',
            'quote_id':view.quote_id,
            'version_id':view.item.version_id,
        };
         angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                angular.element('.loading_wrap').addClass('hidden');
                view.item.margin = r.data.amount_margin;
                view.item.total_margin = r.data.total_margin;
                view.item.margin_percent_val = r.data.margin_percent_val;
                view.item.margin_percent = r.data.margin_percent;
            }
        })
    }

    view.sendEmail=function(){
        var params={
            'do': 'quote-nquote',
            quote_id: view.quote_id,
            version_id:view.item.version_id,
            message: view.item.email.e_message,
            use_html: view.item.use_html,
            xget: 'email_preview'
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post','index.php',params,function(r){
            var viewEmail=angular.copy(view.item.email);
            viewEmail.e_message=r.data.e_message;
            var obj={"app":"quote",
                "recipients":view.auto.options.recipients,
                "use_html":view.item.use_html,
                "is_file":view.item.is_file,
                "dropbox":view.item.dropbox,
                "email":viewEmail
            };
            openModal("send_email", angular.copy(obj), "lg");
        });
    }

    view.showYourReference = function() {
        view.showTxt.edit_own_reference=true;
        $timeout(function(){
            angular.element('#inp_yr').focus();
        });


    }
    view.showSubject = function() {
        view.showTxt.edit_subject=true
        $timeout(function(){
            angular.element('#subj_edit').focus();
        });


    }


    view.setSubject = function() {
        var obj = {
            'do': 'quote-nquote-quote_version-updateSubject',
            'quote_id': view.quote_id,
            'version_id':view.item.version_id,
            'subject': view.item.subject,
            'xview':'1'
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                pdfLoaded=false;
                view.showTxt.edit_subject = !1;
                view.renderPage(r);
            }
        })
    }

    view.setYourReference = function() {
        var obj = {
            'do': 'quote-nquote-quote_version-updateOwnReference',
            'quote_id': view.quote_id,
            'version_id':view.item.version_id,
            'own_reference': view.item.own_reference,
            'xview':'1'
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                pdfLoaded=false;
                view.showTxt.edit_own_reference = !1;
                view.renderPage(r);
            }
        })
    }

    view.createContract=function(){
        var contract_obj={ 
                    'contract_id':'tmp',
                    'quote_id': view.quote_id,
                    'version_id':view.item.version_id };
        $state.go('contract.edit', contract_obj);
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'post_register':
                var iTemplate = 'PostRegister';
                var ctas = 'post';
                break;
            case 'resend_post':
                var iTemplate = 'ResendPostGreen';
                var ctas = 'post';
                break;
            case 'uploadDoc':
                var iTemplate = 'AmazonUpload';
                break;
            case 'send_email':
                var iTemplate = 'SendEmail';
                var ctas = 'vm';
                break;
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'post_register':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.item.do = 'quote-nquote-quote-postRegister';
                params.callback = function(data) {
                    if (data && data.data) {
                        view.renderPage(data);
                        view.greenpost()
                    }
                    view.showAlerts(data)
                }
                break;
            case 'resend_post':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.request_type = 'post';
                params.params = {
                    'do': 'quote--quote-postQuoteData',
                    'quote_id': view.item.id,
                    'version_id': view.item.version_id,
                    'related': 'status',
                    'old_obj': item
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        view.renderPage(d);
                        view.showAlerts(d)
                    }
                }
                break;
            case 'uploadDoc':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        view.showDrop()
                    }
                }
                break;
            case 'send_email':
                params.template ='miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        view.sendEmails(d);
                    }
                }
                break;
        }
        modalFc.open(params)
    }
    view.renderPage(data)
}]).controller('pdfSettingsQCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.ok = function() {
        var data = {
            'do': vm.obj.do_next,
            logo: vm.obj.default_logo,
            email_language: vm.obj.language_id,
            pdf_layout: vm.obj.pdf_type_id,
            quote_id: vm.obj.quote_id,
            version_id: vm.obj.version_id,
            identity_id: vm.obj.multiple_identity_id,
            xview: 1
        };
        helper.doRequest('post', 'index.php', data, vm.closeModal)
    };
    vm.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('weblinkLogQCtrl', ['$scope', 'helper', '$uibModalInstance', 'list', 'data', function($scope, helper, $uibModalInstance, list, data) {
    var vm = this;
    vm.obj = data;
    console.log(vm.obj);
    vm.search = {
        search: '',
        'do': 'quote-nquote',
        xget: 'weblinkLog',
        offset: 1,
        quote_id: vm.obj.quote_id
    };
    vm.renderList = function(d) {
        if (d.data) {
            for (x in d.data) {
                vm.obj[x] = d.data[x]
            }
            helper.showAlerts(vm, d)
        }
    };
    vm.searchThing = function() {
        console.log('fdfd');
        list.search(vm.search, vm.renderList, vm)
    }
    vm.closeModal = function(d) {
        if (d) {
            $uibModalInstance.close(d)
        } else {
            $uibModalInstance.close()
        }
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('quotesTemplatesCtrl', ['$scope', 'helper', 'list', 'data', function($scope, helper, list, data) {
    console.log('quotes');
    var vm = this
    vm.list = [];
    vm.search = {
        search: '',
        'do': 'quote-quotes',
        xget: 'quotesTemplates',
        offset: 1,
        'lang': ''
    };
    vm.ord = '';
    vm.reverse = !1;
    vm.renderList = function(d) {
        if (d.data) {
            for (x in d.data) {
                vm[x] = d.data[x]
            }
            helper.showAlerts(vm, d)
        }
    };
    vm.toggleSearchButtons = function(item, val) {
        list.tSearch(vm, item, val, vm.renderList)
    };
    vm.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search(vm.search, vm.renderList, vm)
    }
    vm.orderBY = function(p) {
        list.orderBy(vm, p, vm.renderList)
    };
    vm.action = function(item, action) {
        list.action(vm, item, action, vm.renderList)
    }
    vm.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Languages'),
        onChange(value) {
            if (value) {
                vm.toggleSearchButtons(vm.lang, this.options[value].id)
            } else {
                vm.toggleSearchButtons(vm.lang, '')
            }
        },
        maxItems: 1
    };
    vm.renderList(data)
}]).controller('QuoteEditTemplateCtrl', ['$scope', '$stateParams', '$state', '$timeout', 'helper', 'editItem', 'data', function($scope, $stateParams, $state, $timeout, helper, editItem, data) {
    console.log('template edit');
    var edit = this,
        typePromise;
    edit.app = 'quote';
    edit.ctrlpag = 'nquote';
    edit.app_mod = 'template';
    edit.tmpl = 'qTemplate';
    $scope.vm.isAddPage = !1;
    $scope.vm.title_text=helper.getLanguage('Model quote');
    $scope.vm.status_title = '';
    edit.item_id = $stateParams.template_id;
    edit.duplicate_quote_id = $stateParams.duplicate_quote_id;
    edit.quote_id = edit.item_id;
    edit.item = {};
    edit.oldObj = {};
    edit.item.validity_date = new Date();
    edit.removedCustomer = !1;
    edit.item.add_customer = (edit.item_id != 'tmp' ? true : !1);
    edit.quote_group_active = 0;
    edit.originalArticleList = undefined;
    edit.removeComponent = removeComponent;
    edit.showTxt = {
        addBtn: !0,
        addArticleBtn: !1,
    }
    edit.auto = {
        options: {
            articlesList: []
        }
    };
    editItem.init(edit);
    edit.renderPage = function(d) {
        if (d && d.data) {
            /*if (d.data.redirect) {
                $state.go(d.data.redirect);
                return !1
            }*/
            edit.item = d.data;
            /*for (x in edit.item.quote_group) {
                edit.item.quote_group[x].show_AC = !0;
                if (edit.item.quote_group[x].table[0]) {
                    if (edit.item.quote_group[x].table[0].quote_line[0]) {
                        edit.item.quote_group[x].show_block_total = edit.item.quote_group[x].table[0].quote_line[0].show_block_total == '1' ? true : !1;
                        edit.item.quote_group[x].show_AC = edit.item.quote_group[x].table[0].quote_line[0].hide_ACv == '1' ? !1 : !0
                    }
                }
            }*/
            edit.checkVariants();
            edit.auto.options.articles_list = d.data.articles_list && d.data.articles_list.lines ? d.data.articles_list.lines : [];
            if (edit.originalArticleList === undefined) {
                edit.originalArticleList = edit.auto.options.articles_list
            }
            if (edit.item.validity_date) {
                edit.item.validity_date = new Date(edit.item.validity_date)
            }
            if (edit.item.quote_date) {
                edit.item.quote_date = new Date(edit.item.quote_date)
            }
            $scope.vm.serial_number = edit.item.template_name;
            
            edit.contactAddObj = {
                country_dd: edit.item.country_dd,
                country_id: edit.item.main_country_id,
                add_contact: !0,
                buyer_id: edit.item.buyer_id,
                article: edit.item.tr_id,
                'do': 'quote-nquote-quote-tryAddC',
                identity_id: edit.item.identity_id,
                contact_only: !0,
                item_id: edit.item_id
            }
            //helper.showAlerts(edit, d)
        }
    }
    edit.save = function(showLoading,formObj) {
        var data = angular.copy(edit.item),
            add = !1;
        data = helper.clearData(data, ['APPLY_DISCOUNT_LIST', 'CURRENCY_TYPE_LIST', 'SOURCE_DD', 'STAGE_DD', 'TYPE_DD', 'accmanager', 'addresses', 'authors', 'articles_list', 'authors', 'cc', 'contacts', 'country_dd', 'language_dd', 'main_comp_info', 'sort_order']);
        data.do = 'quote-nquote-quote-update';
        if (edit.item_id == 'tmp') {
            data.do = 'quote-nquote-quote-add';
            add = !0
        } else {
            if (data.is_template) {
                data.quote_id = edit.item_id
            }
        }
        data.template = 'template';
        if (showLoading) {
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                if (r && r.success) {
                    if(add == !0){                   
                        edit.item_id=r.data.quote_id;
                        edit.quote_id=r.data.quote_id;
                        /*$state.go('quote.editTemplate', {
                            template_id: r.data.quote_id
                        })*/
                    }
                    edit.renderPage(r);                
                }else{
                    helper.showAlerts(edit, r, formObj)
                }             
            })
        } else {
            helper.doRequest('post', 'index.php', data, function(d) {
                if (d && d.success) {
                    if(add == !0){
                        edit.item_id=d.data.quote_id;
                        edit.quote_id=d.data.quote_id;
                        /*$state.go('quote.editTemplate', {
                            template_id: d.data.quote_id
                        })*/
                    } 
                    edit.renderPage(d);          
                }else{
                    helper.showAlerts(edit, d, formObj)
                }                
            })
        }
    }
    /*if (edit.item_id == 'tmp') {
        edit.save()
    }*/

   edit.removeLine = function($index, obj, isChapter) {
        edit.changedData();
        if(isChapter){
            obj.splice($index, 1);            
            if($index>0){
                /*edit.quote_group_active=$index-1;
                edit.item.quote_group[$index - 1].active = !0*/
                edit.quote_group_active=0;
                edit.item.quote_group[0].active = !0
            }else{
                edit.quote_group_active = 0;
                if(edit.item.quote_group.length){
                    edit.item.quote_group[0].active = !0    
                }
            }
            edit.calcQuoteTotal(edit);
            edit.checkVariants();
            return false;
        }
          
        var line_id = obj[$index].line_id;      
        if (line_id) {
            if ($index > -1) {
                if (!isChapter) {                    
                    if(obj[$index].has_variants == true || obj[$index].has_variants == '1'){
                        //removeVariant(obj[$index].article_id, obj);
                         removeVariant(line_id, obj);
                    }else{
                        //removeTaxes(line_id, obj);
                        removeComponent(line_id, obj);
                    }
                }
                obj.splice($index, 1);
                edit.calcQuoteTotal(edit);
                if (isChapter) {
                    edit.quote_group_active = 0;
                    edit.item.quote_group[0].active = !0
                }
                edit.checkVariants();
            }
        } else {
            var tr_id = obj[$index].tr_id;
            if(obj[$index].has_variants == true || obj[$index].has_variants == '1'){
                //removeVariant(obj[$index].article_id, obj);
                removeVariant(tr_id, obj);
            }
            removeComponent(tr_id, obj);
            obj.splice($index, 1);
            edit.calcQuoteTotal(edit);
            if (isChapter) {
                edit.quote_group_active = 0;
                edit.item.quote_group[$index - 1].active = !0
            }
            edit.checkVariants();
        }
    }
/*    edit.removeLine = function($index, obj, isChapter) {

        if(isChapter){
            obj.splice($index, 1);
            edit.calcQuoteTotal(edit);
            if($index>0){
                edit.quote_group_active=$index-1;
                edit.item.quote_group[$index - 1].active = !0
            }else{
                edit.quote_group_active = 0;
                if(edit.item.quote_group.length){
                    edit.item.quote_group[0].active = !0    
                }
            }
            edit.checkVariants();
            return false;
        }

        if ($index > -1) {
            var tr_id = obj[$index].tr_id;
            obj.splice($index, 1);
            edit.delaySave();
            if (isChapter) {
                edit.quote_group_active = 0;
                edit.item.quote_group[0].active = !0
            }
            if (!isChapter) {                    
                if(obj[$index].has_variants == true || obj[$index].has_variants == '1'){
                    removeVariant(obj[$index].article_id, obj);
                }
                 removeComponent(tr_id, obj);
            }
            obj.splice($index, 1);
            edit.calcQuoteTotal(edit);
            if (isChapter) {
                edit.quote_group_active = 0;
                edit.item.quote_group[0].active = !0
            }
            edit.checkVariants();
        }
    };*/
    edit.delaySave = function(showLoading, reload) {
        /*angular.element('.keep-open-on-click').on('click', function(e) {
            e.stopPropagation()
        });*/
        edit.changedData();
        if (edit.typePromise) {
            $timeout.cancel(edit.typePromise)
        }
        edit.typePromise = $timeout(function() {        
            //edit.save(showLoading, reload)
        }, 2000)
    };
    edit.linevatcfg = {
        valueField: 'vat',
        labelField: 'name',
        searchField: ['vat'],
        delimiter: '|',
        placeholder: helper.getLanguage('VAT'),
        create: !1,
        maxOptions: 100,
        onDelete(values) {
            return !1
        },
        closeAfterSelect: !0,
        maxItems: 1
    };
    edit.tinymceOptions = {
        selector: "",
        resize: "both",
        width: 850,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        selector: 'textarea',
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        //toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    edit.tinymceOptionsLines = {
        selector: "",
        resize: "both",
        auto_resize: !0,
        relative_urls: !1,
        selector: 'textarea',
        menubar: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code autoresize"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        autoresize_bottom_margin: 0,
        setup: function(editor) {
            editor.on('click', function(e) {
                e.stopPropagation();
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                angular.element(editor.editorContainer).parent().removeClass('wysiwyg-hide')
            }).on('blur', function(e) {
                angular.element('.wysiwyg-holder').addClass('wysiwyg-hide');
                edit.delaySave()
            })
        },
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
        content_css: [helper.base_url + 'css/tinymce_styles.css']
    };
    /*angular.element('.content').on('click', function() {
        angular.element('.wysiwyg-holder').addClass('wysiwyg-hide')
    });*/
    edit.addArticle = function() {
        edit.showTxt.addArticleBtn = !0;
        $timeout(function() {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.open()
        })
    }
    edit.showGroup = function($event, $index) {
        var collection = edit.item.quote_group;
        for (x in collection) {
            collection[x].active = !1;
            if (x == $index) {
                collection[x].active = !0
            }
        }
        if ($index !== undefined) {
            edit.quote_group_active = $index;
        }
    }
    edit.calc = function(text, $index, id) {
        edit.changedData();
        edit.calcQuoteTotal(edit, text, $index, id)
    }
    edit.dragControlListeners = {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            var collection = edit.item.quote_group;
            var next_idx_1 = event.dest.index +1;
            var next_idx_2 =event.source.index +1;
         
           if(collection[edit.quote_group_active].table[0].quote_line[event.source.index].component_for){
            //not working 
                /*console.log(event.source.index, event.dest.index, collection[edit.quote_group_active].table[0].quote_line[event.source.index], collection[edit.quote_group_active].table[0].quote_line[event.dest.index]);
                console.log(event.source, event.dest);
                event.dest.sortableScope.insertItem(event.source.index, collection[edit.quote_group_active].table[0].quote_line[event.dest.index]);
                 event.source.sortableScope.removeItem(event.dest.index);*/
           }
            if(collection[edit.quote_group_active].table[0].quote_line[event.dest.index].is_combined){

                var lines = collection[edit.quote_group_active].table[0].quote_line;
                var line_id = 0;
                if(collection[edit.quote_group_active].table[0].quote_line[event.dest.index].line_id){
                    line_id = collection[edit.quote_group_active].table[0].quote_line[event.dest.index].line_id;                 
                }else{
                    line_id = collection[edit.quote_group_active].table[0].quote_line[event.dest.index].tr_id
                }
                
                for(x in lines){
                    if(lines[x].component_for == line_id){
                       //console.log(event.source.index, event.dest.index);
                       if(event.source.index < event.dest.index){
                            event.dest.sortableScope.insertItem(next_idx_1, collection[edit.quote_group_active].table[0].quote_line[event.source.index]);
                            event.source.sortableScope.removeItem(event.source.index);
                       }else{
                        //console.log(next_idx_1, next_idx_2);
                              
                           event.dest.sortableScope.insertItem(next_idx_1, collection[edit.quote_group_active].table[0].quote_line[next_idx_2]);
                           event.source.sortableScope.removeItem(next_idx_2+1); 
                           next_idx_2 = next_idx_2+1;
                           next_idx_1 = next_idx_1+1;
                       }
                       
                    }
                }
            }
            edit.delaySave();
            $timeout(function() {
                angular.element('.table_orders').find('textarea[id^="ui-tinymce"]').each(function() {
                    var id = angular.element(this).attr('id');
                    if(id){
                        $scope.$broadcast('$tinymce:refresh', id)
                    }           
                })
            })
        },
        dragEnd: function(event) {
            if (event && event.dest.index == event.source.index) {
                var collection = edit.item.quote_group;
                if (collection[edit.quote_group_active].table[0].quote_line[event.dest.index].line_type == 3) {
                    $timeout(function() {
                        var id = angular.element('.table_orders').find('li').eq(event.dest.index).find('textarea').attr('id');
                        if(id){
                            $scope.$broadcast('$tinymce:refresh', id)
                        }                   
                    })
                }
            }
        }
    }
    edit.dragChaptersControlListeners = {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            if(event.source.index == edit.quote_group_active){
                edit.quote_group_active=event.dest.index;
            }
            edit.delaySave()
        }
    };
    edit.orderChapter = function($index, next) {
        edit.changedData();
        if ($index == 0 && next === undefined) {
            return !1
        }
        if (next && $index == edit.item.quote_group.length - 1) {
            return !1
        }
        var ind = next ? $index + 1 : $index - 1,
            oldKey = angular.copy(edit.item.quote_group[ind]),
            newKey = angular.copy(edit.item.quote_group[$index]);
        edit.item.quote_group[ind] = newKey;
        edit.item.quote_group[$index] = oldKey;
        edit.delaySave()
    }
    edit.articlesListAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                var stock_display = '';
                if (item.hide_stock == 0) {
                    stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;</small>' + '</div>' + stock_display + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span>' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'quote-nquote',
                    xget: 'articles_list',
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id
                };
                edit.requestAuto(edit, data, edit.renderArticleListAuto)
            }, 300)
        },
        onChange(value) {
            if (value == undefined) {} else if (value == '99999999999') {
                edit.openModal(edit, 'AddAnArticle', {
                    'do': 'quote-addAnArticle',
                    customer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: edit.item.remove_vat
                }, 'md');
                return !1
            } else {
                var collection = edit.auto.options.articles_list;
                for (x in collection) {
                    if (collection[x].article_id == value) {
                        editItem.addQuoteArticle(edit, collection[x]);
                        edit.selected_article_id = '';
                        edit.showTxt.addArticleBtn = !1;
                        var data = {
                            'do': 'quote-nquote',
                            xget: 'articles_list',
                            buyer_id: edit.item.buyer_id,
                            lang_id: edit.item.email_language,
                            cat_id: edit.item.cat_id
                        };
                        edit.requestAuto(edit, data, edit.renderArticleListAuto);
                        break
                    }
                }
            }
        },
        onItemAdd(value, $item) {
            if (value == '99999999999') {
                edit.selected_article_id = ''
            }
        },
        onBlur() {
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'quote-nquote',
                    xget: 'articles_list',
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id
                };
                edit.showTxt.addArticleBtn = !1;
                if (edit.originalArticleList === undefined) {
                    edit.requestAuto(edit, data, edit.renderArticleListAuto)
                } else {
                    edit.auto.options.articles_list = edit.originalArticleList
                }
            }, 300)
        },
        maxItems: 1
    };
    edit.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Languages'),
        maxItems: 1
    };

    edit.parentVariantCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code', 'supplier_reference', 'family'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        maxOptions: 5,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.family) {
                    item.family = "/ " + item.family
                }
                var stock_display = '';
                if (item.hide_stock == 0) {
                    stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + stock_display + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span>' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onFocus(){
            var selectize=this.$input[0].selectize;
            var index=this.$input[0].attributes['selectindex'].value;
            var parentVariant=this.$input[0].attributes['selectarticle'].value;
            var tmp_data={
                'do':'misc-articleVariants',
                'parentVariant':parentVariant,
                buyer_id: edit.item.buyer_id,
                lang_id: edit.item.email_language,
                cat_id: edit.item.cat_id,
                remove_vat: (edit.item.show_vat ? '0' : '1'),
                vat_regime_id: edit.item.vat_regime_id,
                app:edit.app
            };
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get','index.php',tmp_data,function(r){
                edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts=r.data.lines;
                if(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts.length){
                    selectize.open();
                }
            });
        },
        onType(str) {
            var selectize=this.$input[0].selectize;
            var index=this.$input[0].attributes['selectindex'].value;
            var parentVariant=this.$input[0].attributes['selectarticle'].value;         
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do':'misc-articleVariants',
                    'parentVariant':parentVariant,
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id,
                    app:edit.app
                };
                helper.doRequest('get','index.php',tmp_data,function(r){
                    edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts=r.data.lines;
                    if(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts.length){
                        selectize.open();
                    }
                });
            }, 300)
        },
        onChange(value) {
            var index=this.$input[0].attributes['selectindex'].value;
            if(value != undefined){
                var collection = edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts;
                for(x in collection){
                    if(collection[x].article_id == value){
                        edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].has_variants_done='1';
                        collection[x].is_variant_for_line = edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].tr_id;
                        editItem.addQuoteArticle(edit, collection[x], false, parseInt(index)+1);
                        break; 
                    }
                }
            }
        },
        onBlur() {
            var index=this.$input[0].attributes['selectindex'].value;
            var parentVariant=this.$input[0].attributes['selectarticle'].value;
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {                
                var tmp_data={
                    'do':'misc-articleVariants',
                    'parentVariant':parentVariant,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id,
                    app:edit.app
                };
                helper.doRequest('get','index.php',tmp_data,function(r){
                    edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts=r.data.lines;
                });
            }, 300)
        },
        maxItems: 1
    };

    edit.childVariantCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code', 'supplier_reference', 'family'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select item'),
        maxOptions: 5,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.family) {
                    item.family = "/ " + item.family
                }
                var stock_display = '';
                if (item.hide_stock == 0) {
                    stock_display = '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">Stock <strong>' + escape(item.stock2) + '</strong></small>' + '</div>'
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.family) + '&nbsp;</small>' + '</div>' + stock_display + '</div>' + '</div>';
                if (item.article_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span>' + helper.getLanguage('Create a new item') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onType(str) {
            var selectize=this.$input[0].selectize;
            var index=this.$input[0].attributes['selectindex'].value;
            var parentVariant=this.$input[0].attributes['selectarticle'].value;         
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var tmp_data = {
                    'do':'misc-articleVariants',
                    'parentVariant':parentVariant,
                    search: str,
                    buyer_id: edit.item.buyer_id,
                    lang_id: edit.item.email_language,
                    cat_id: edit.item.cat_id,
                    remove_vat: (edit.item.show_vat ? '0' : '1'),
                    vat_regime_id: edit.item.vat_regime_id,
                    app:edit.app
                };
                helper.doRequest('get','index.php',tmp_data,function(r){
                    edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts=r.data.lines;
                    if(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts.length){
                        selectize.open();
                    }
                });
            }, 300)
        },
        onChange(value) {
            var index=this.$input[0].attributes['selectindex'].value;
            if(value != undefined){
                var tmp_data = angular.copy(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts);
                for(x in tmp_data){
                    if(tmp_data[x].article_id == value){
                        var tmp_selected_item=angular.copy(tmp_data[x]);
                        tmp_selected_item.quantity=parseFloat(helper.return_value(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].quantity,10));
                        edit.removeLine(index,edit.item.quote_group[edit.quote_group_active].table[0].quote_line);
                        editItem.addQuoteArticle(edit, tmp_selected_item, false, index);
                        break; 
                    }
                }
            }
        },
        onBlur() {
            var index=this.$input[0].attributes['selectindex'].value;
            edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].showVariants=false;
        },
        maxItems: 1
    };

    function removeVariant(id, obj) {
        for (x in obj) {
            //if (obj[x].is_variant_for == id) {
            if (obj[x].is_variant_for_line == id) {
                obj.splice(x, 1);
                break;
            }
        }
    }

    function removeComponent(id, obj) {
        for (x in obj ) {
            if (obj[x].component_for == id) {
                obj.splice(x, 1);
                edit.removeComponent(id, obj);
            }
        }
    }

    edit.showChildVariants=function(item,index){
        var tmp_data = {
            'do':'misc-articleVariants',
            'parentVariant':item.is_variant_for,
            buyer_id: edit.item.buyer_id,
            lang_id: edit.item.email_language,
            cat_id: edit.item.cat_id,
            remove_vat: (edit.item.show_vat ? '0' : '1'),
            vat_regime_id: edit.item.vat_regime_id,
            app:edit.app
        };
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get','index.php',tmp_data,function(r){
            item.showVariants=true;
            edit.item.quote_group[edit.quote_group_active].table[0].quote_line[index].variantsOpts=r.data.lines;
        });
    }

    edit.changeBulkVariants=function(){
        openModal('bulk_change_variants',{},'md');
    }

    edit.checkVariants=function(){
        edit.showBulkChangeVariants=false;
        if(edit.item.quote_group.length){
            if(edit.item.quote_group[edit.quote_group_active].table[0]){
                for(x in edit.item.quote_group[edit.quote_group_active].table[0].quote_line){
                    if(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[x].is_variant_for>0){
                        edit.showBulkChangeVariants=true;
                    }              
                }
            }
        }            
    }

    edit.bulkVariantsApply=function(from,to,ind){
        if(ind < edit.item.quote_group[edit.quote_group_active].table[0].quote_line.length){
            for(y=ind; y < edit.item.quote_group[edit.quote_group_active].table[0].quote_line.length; y++){
                if(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[y].is_variant_for > 0 && edit.item.quote_group[edit.quote_group_active].table[0].quote_line[y].variant_type == from){
                    var new_index=y;
                    //angular.element('.loading_wrap').removeClass('hidden');
                    var tmp_data = {
                        'do':'misc-articleVariants',
                        'parentVariant':edit.item.quote_group[edit.quote_group_active].table[0].quote_line[y].is_variant_for,
                        buyer_id: edit.item.buyer_id,
                        lang_id: edit.item.email_language,
                        cat_id: edit.item.cat_id,
                        remove_vat: (edit.item.show_vat ? '0' : '1'),
                        vat_regime_id: edit.item.vat_regime_id,
                        variant_type_search:to,
                        app:edit.app
                    };
                    helper.doRequest('get','index.php',tmp_data,function(o){
                        if(o.data.lines.length){
                            var tmp_item = o.data.lines[0];
                            tmp_item.quantity=parseFloat(helper.return_value(edit.item.quote_group[edit.quote_group_active].table[0].quote_line[y].quantity,10));
                            edit.removeLine(y,edit.item.quote_group[edit.quote_group_active].table[0].quote_line);
                            editItem.addQuoteArticle(edit, tmp_item, false, new_index);                      
                        }
                        var incremented_index=parseInt(new_index)+1;
                        $timeout(function() {
                            edit.bulkVariantsApply(from,to,incremented_index);
                        }, 1000);
                    });
                    break;
                }
            }
        }   
    }

    edit.changedData=function(){
        helper.changedData();
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'bulk_change_variants':
                var iTemplate = 'BulkChangeVariants';
                var ctas = 'vmMod';
                break;
        }
        var params = {
            template: 'qTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'bulk_change_variants':
                params.template = 'miscTemplate/' + iTemplate+'Modal';
                params.ctrl = 'BulkChangeVariantsModalCtrl';
                params.params = {'do':'misc-addArticleList','xget':'variant_types','title':edit.item.quote_group[edit.quote_group_active].title};
                params.callback=function(d){
                    if(d){
                        edit.bulkVariantsApply(d.from,d.to,0);
                    }
                }
                break;
        }
        modalFc.open(params)
    }

    edit.renderPage(data)
}]).controller('quotesTemplatesModalCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'data', 'list', function($scope, $timeout, helper, $uibModalInstance, data, list) {
    var vmMod = this;
    vmMod.search = {
        search: '',
        'do': 'quote-quotes',
        xget: 'quotesTemplates',
        offset: 1,
        'lang': ''
    };
    for (x in data) {
        vmMod[x] = data[x];
        if (vmMod.lang) {
            vmMod.search.lang = vmMod.lang
        }
        if (vmMod.list.length < 10) {
            $timeout(function() {
                angular.element('.modal-body').addClass('modal-body_alt')
            })
        } else {
            $timeout(function() {
                angular.element('.modal-body').removeClass('modal-body_alt')
            })
        }
    }
    vmMod.templates = [];
    vmMod.ok = closeModal;
    vmMod.cancel = cancel;

    function closeModal() {
        var tmp_ar=[];
        for(x in vmMod.templates){
            if(vmMod.templates[x] && vmMod.templates[x]!=undefined){
                tmp_ar.push(vmMod.templates[x]);  
            }                    
        }
        $uibModalInstance.close({'refresh_prices':vmMod.refresh_prices,'list':tmp_ar})
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    }
    vmMod.renderList = function(d) {
        if (d.data) {
            for (x in d.data) {
                vmMod[x] = d.data[x];
                if (vmMod.lang) {
                    vmMod.search.lang = vmMod.lang
                }
                if (vmMod.list.length < 10) {
                    $timeout(function() {
                        angular.element('.modal-body').addClass('modal-body_alt')
                    })
                } else {
                    $timeout(function() {
                        angular.element('.modal-body').removeClass('modal-body_alt')
                    })
                }
            }
        }
    };
    vmMod.searchThing = function() {
        list.search(vmMod.search, vmMod.renderList, vmMod)
    }
    vmMod.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Languages'),
        onChange(value) {
            if (value) {
                vmMod.search.lang = this.options[value].id
            } else {
                vmMod.search.lang = ''
            }
            vmMod.searchThing()
        },
        maxItems: 1
    }
}]).controller('ProgressFacqBasketModalCtrl', ['$scope','editItem', 'helper', '$uibModalInstance', '$timeout', 'data', function($scope,editItem, helper, $uibModalInstance, $timeout, data) {
    var vmMod = this;
    vmMod.start = !0;
    vmMod.value = 0;
    var i = 0;
    var increment = 0;
    vmMod.showAlerts = function(d) {
        helper.showAlerts(vmMod, d)
    }
    var originalScope = editItem.getScope();

    /*vmMod.ok = function() {
        angular.element('.loading_alt').removeClass('hidden');
        vmMod.start = !1;
        postIt();
    };*/


    vmMod.cancel = function() {
        /*$uibModalInstance.close({'items':items});*/
        $uibModalInstance.close();
    };

    function getBasket() {

    var tmp_data = {
            'do': 'quote--quote-get_facq_basket',
            'lang': originalScope.item.email_language,
        };

    angular.element('.loading_alt').removeClass('hidden');

     helper.doRequest('get','index.php',tmp_data,function(o){
            if(o.data.items.length){
                vmMod.obj = o.data.items;
                vmMod.len = vmMod.obj.length;
                vmMod.lang = o.data.lang;
                angular.element('.loading_alt').removeClass('hidden');
                postIt(vmMod.obj, vmMod.lang);
                
            }else{
                 var obj = {
                    "error": {
                        "error": helper.getLanguage("There are currently no items in your Facq webshop basket")
                    },
                    "notice": !1,
                    "success": !1
                };
                vmMod.showAlerts(obj);
            }
          });        
    }

    function postIt(params, lang) {


          if (params.length) {
           
             angular.element('.loading_alt').removeClass('hidden');
            var internal_name = (lang=='FR')? params[0].LabelFR : params[0].LabelNL; 

            //console.log(params[0].LabelFR, params[0].LabelNL, lang, internal_name);
             var item ={
                line_type       :3,
                article_id      :0,
                code            :params[0].ProductID,
                quoteformat     :internal_name,
                packing         :1,
                sale_unit       :1,
                purchase_price  :params[0].NetPriceTaxExcluded,
                vat             :21,
                vat_value       :params[0].PublicPriceTaxExcluded*21/100,
                price           :params[0].PublicPriceTaxExcluded,
                price_vat       :params[0].PublicPriceTaxExcluded*1 + (params[0].PublicPriceTaxExcluded*21/100),
                quantity        :params[0].Quantity,
                line_discount   :0,
                show_vat        :1,
                stock           :0,
                threshold_value :0,
                colum            :'',
                has_variants    :'',
                has_variants_done :'',
                is_variant_for  :'',
                is_variant_for_line  :'',
                variant_type    :'',
                is_combined     :'',
                has_taxes       :'',

            }
           
           if(params[0].ProductID!='000000'){
               /* if(params[0].ProductID!='990000'){*/
                    item.line_type=1;
                    var data = {};
                    data.do = 'quote--quote-update_facq_article';
                    data.item_code = params[0].ProductID;
                    data.internal_name = internal_name;
                    data.nameFR = params[0].LabelFR;
                    data.nameNL = params[0].LabelNL;
                    data.price = params[0].PublicPriceTaxExcluded;
                    data.purchase_price = params[0].NetPriceTaxExcluded;

                  
                    helper.doRequest("post", "index.php", data, function(o) {
                        angular.element('.loading_alt').removeClass('hidden');
                        if(o.data){
                            item.article_id=o.data.article_id;          
                        }                        
                    });
                 /*}else{
                    item.line_type=2;
                 }*/
            }else{
                item.quoteformat='<p>'+item.quoteformat+'</p>';
            }



            i++;
            var percent = i * 100 / vmMod.len;
            vmMod.value = parseInt(percent.toFixed(0));
            vmMod.step = i;

            var obj = {
                        "error": !1,
                        "notice": !1,
                        "success": {
                            "success": helper.getLanguage("Line") + ": " + params[0].ProductID + ' - ' + internal_name
                        }
                    };
            vmMod.showAlerts(obj);

            
            $timeout(function() {
               // console.log(item.quoteformat);
                editItem.addFacqLine(originalScope, item);   
                params.shift();
                postIt(params, vmMod.lang);
             },500);

            
        }else{
            $timeout(function() {
               deleteBasket();
             },1500);
            
            angular.element('.loading_alt').addClass('hidden')
        }

           
        
    }

    function deleteBasket() {
        console.log('basket deleted');
        return false; 
        var data = {};
        data.do = 'quote--quote-delete_facq_basket';
      
        helper.doRequest("post", "index.php", data, function(o) {
            if(o.data){
                    
            }                        
        });
                
                 
        
    }

    getBasket();

}])