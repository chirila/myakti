app.controller('ReportCatalogueCtrl', ['$scope', '$timeout', '$compile', 'helper', 'list', 'modalFc', 'data', function($scope, $timeout, $compile, helper, list, modalFc, data) {
    console.log("Report Catalogue");
    $scope.item = {};
    $scope.show_load = !0;
    $scope.periods = [{
        'id': '0',
        'value': helper.getLanguage('Weekly')
    }, {
        'id': '1',
        'value': helper.getLanguage('Daily')
    }, {
        'id': '2',
        'value': helper.getLanguage('Monthly')
    }, {
        'id': '3',
        'value': helper.getLanguage('Yearly')
    }, {
        'id': '4',
        'value': helper.getLanguage('Custom')
    }];
    $scope.search = {
        'do': 'report-xarticle_report',
        'start_date': new Date(),
        'period_id': '0',
        'customer_id': undefined,
        'article_category_id': undefined,
        'article_id': undefined,
        'from': undefined,
        'to': undefined,
        'offset_article': 1,
        'offset_customer': 1
    };
    var typePromise;
    $scope.chart_options = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_amount.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.chart_article = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_article.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.chart_family = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_family.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        from: !1,
        to: !1,
        start_date: !1,
    }
    $scope.renderList = function(d) {
        if (d && d.data) {
            for (x in d.data) {
                $scope.item[x] = d.data[x]
            }
            if ($scope.item.start_date) {
                $scope.search.start_date = new Date($scope.item.start_date)
            }
        }
        $scope.show_load = !1;
    }
    $scope.periodCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Period'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.start_date = new Date();
            $scope.search.from = undefined;
            $scope.search.top = undefined;
            if (value && this.options[value].id != 4) {
                $scope.search.offset_article = 1;
                $scope.search.offset_customer = 1;
                $scope.searchThing()
            }
        },
        maxItems: 1
    };
    $scope.customerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Customers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_article = 1;
            $scope.search.offset_customer = 1;
            $scope.searchThing()
        },
        onType(str) {
            var selectize = angular.element('#customer_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'report-xarticle_report',
                    xget: 'cc',
                    term: str
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data;
                    if ($scope.item.customers.list.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (!$scope.item.customers.list.length) {
                var obj = {
                    'do': 'report-xarticle_report',
                    xget: 'cc'
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.familyCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Families'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_article = 1;
            $scope.search.offset_customer = 1;
            $scope.searchThing()
        },
        onType(str) {
            var selectize = angular.element('#family_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'report-xarticle_report',
                    xget: 'families',
                    term: str
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.families = r.data;
                    if ($scope.item.families.list.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (!$scope.item.families.list.length) {
                var obj = {
                    'do': 'report-xarticle_report',
                    xget: 'families'
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.families = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.articleCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Articles'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_article = 1;
            $scope.search.offset_customer = 1;
            $scope.searchThing()
        },
        onType(str) {
            var selectize = angular.element('#articles_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var cat_id = 0;
                for (x in $scope.item.customers.list) {
                    if ($scope.item.customers.list[x].id == $scope.search.customer_id) {
                        cat_id = $scope.item.customers.list[x].cat_id;
                        break
                    }
                }
                var obj = {
                    'do': 'report-xarticle_report',
                    xget: 'articles_list',
                    search: str,
                    customer_id: $scope.search.customer_id,
                    cat_id: cat_id
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.articles = r.data;
                    if ($scope.item.articles.lines.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (!$scope.item.articles.lines.length) {
                var obj = {
                    'do': 'report-xarticle_report',
                    xget: 'articles_list'
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.articles = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.searchThing = function(val) {
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        if (val) {
            $scope.search[val] = undefined
        }
        var obj = angular.copy($scope.search);
        obj.start_date = $scope.search.start_date.toUTCString();
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.toggleSearchButtons = function(item, val) {
        $scope.search[item] = val;
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.search.offset_article = 1;
        $scope.search.offset_customer = 1;
        var params = {};
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', params, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.listMore = function(index) {
        var new_line = '<tr class="versionInfo"><td colspan="8" class="no_border">' + '<table class="table dispatchTable">' + '<thead><th>' + '<div class="row">' + '<div class="col-sm-6">' + helper.getLanguage('Customer') + '</div>' + '<div class="col-sm-2 text-center">' + helper.getLanguage('Quantity') + '</div>' + '<div class="col-sm-2 text-center">' + helper.getLanguage('Margin') + '</div>' + '<div class="col-sm-2 text-center">' + helper.getLanguage('Value') + '</div>' + '</div>' + '</th></thead>' + '<tbody>' + '<tr ng-repeat="subart in item.article_tab.article_row[' + index + '].article_collaps">' + '<td>' + '<div class="row">' + '<div class="col-sm-6 text-muted"><a ng-click="toggleSearchButtons(\'customer_id\',subart.CUSTOMER_ID)"">{{subart.CUSTOMER_N}}</a></div>' + '<div class="col-sm-2 text-center text-muted">{{subart.CUSTOMER_QUANTITY}}</div>' + '<div class="col-sm-2 text-center text-muted"><span ng-bind-html="subart.CUSTOMER_TOTAL_MARGIN"></span> (<span>{{subart.CUSTOMER_TOTAL_MARGIN_PERCENT}}</span> %)</div>' + '<div class="col-sm-2 text-center text-muted"><span ng-bind-html="subart.TOTAL_CUSTOMER"></span></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>';
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').addClass('hidden');
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').removeClass('hidden');
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.orderTable.products tbody tr.collapseTr').eq(index).next())($scope)
    }
    $scope.listLess = function(index) {
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').removeClass('hidden');
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').addClass('hidden')
    }
    $scope.get_more = function(tab) {
        var obj = angular.copy($scope.search);
        obj.xget = tab;
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            if (tab == 'customersTab') {
                $scope.item.customer_tab = r.data
            } else if (tab == 'familiesTab') {
                $scope.item.family_tab = r.data
            } else {
                $scope.item.supply_tab = r.data
            }
        })
    }
    $scope.listMoreCustomer = function(index) {
        var new_line = '<tr class="versionInfo"><td colspan="8" class="no_border">' + '<table class="table dispatchTable">' + '<thead><th>' + '<div class="row">' + '<div class="col-sm-8">' + helper.getLanguage('Article') + '</div>' + '<div class="col-sm-2 text-center">' + helper.getLanguage('Quantity') + '</div>' + '<div class="col-sm-2 text-center">' + helper.getLanguage('Value') + '</div>' + '</div>' + '</th></thead>' + '<tbody>' + '<tr ng-repeat="subart in item.customer_tab.other_row[' + index + '].other_collaps">' + '<td>' + '<div class="row">' + '<div class="col-sm-8 text-muted">{{subart.ARTICLE_N}}</div>' + '<div class="col-sm-2 text-center text-muted">{{subart.TOTAL_ART_Q}}</div>' + '<div class="col-sm-2 text-center text-muted"><span ng-bind-html="subart.TOTAL_ART_AMOUNT"></span></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>';
        angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').addClass('hidden');
        angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').removeClass('hidden');
        angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).next())($scope)
    }
    $scope.listLessCustomer = function(index) {
        angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').removeClass('hidden');
        angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').addClass('hidden')
    }
    $scope.navigate = function(i) {
        switch ($scope.search.period_id) {
            case '1':
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + i);
                break;
            case '2':
                $scope.search.start_date = $scope.search.start_date.addMonths(i);
                break;
            case '3':
                $scope.search.start_date.setFullYear($scope.search.start_date.getFullYear() + i);
                break;
            default:
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + (i * 7))
        }
        $scope.searchThing()
    }
    $scope.orderBY = function(p,tab) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        $scope.search.desc = $scope.reverse;
        $scope.search.order_by = $scope.ord;
        if(!tab){
            list.orderBy($scope, p, $scope.renderList)
        }else{
            $scope.get_more(tab);
        }
    };

    $scope.renderList(data)
}]).controller('ReportCatalogueInvoicesCtrl', ['$scope', '$timeout', '$compile', 'helper', 'list', 'modalFc', 'data', function($scope, $timeout, $compile, helper, list, modalFc, data) {
    console.log("Report Catalogue Invoices");
    $scope.item = {};
    $scope.show_load = !0;
    $scope.periods = [{
        'id': '0',
        'value': helper.getLanguage('Weekly')
    }, {
        'id': '1',
        'value': helper.getLanguage('Daily')
    }, {
        'id': '2',
        'value': helper.getLanguage('Monthly')
    }, {
        'id': '3',
        'value': helper.getLanguage('Yearly')
    }, {
        'id': '4',
        'value': helper.getLanguage('Custom')
    }];
    $scope.search = {
        'do': 'report-xarticle_report_invoices',
        'start_date': new Date(),
        'period_id': '0',
        'supplier_id': undefined,
        'customer_id': undefined,
        'article_category_id': undefined,
        'article_id': undefined,
        'from': undefined,
        'to': undefined,
        'manager_id': undefined,
        'offset_article': 1,
        'offset_customer': 1
    };
    var typePromise;
    $scope.chart_options = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_amount.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.chart_article = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_article.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.chart_family = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_family.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        from: !1,
        to: !1,
        start_date: !1,
    }
    $scope.renderList = function(d) {
        if (d && d.data) {
            for (x in d.data) {
                $scope.item[x] = d.data[x]
            }
            if ($scope.item.start_date) {
                $scope.search.start_date = new Date($scope.item.start_date)
            }
        }
        $scope.show_load = !1;
    }
    $scope.periodCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Period'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.start_date = new Date();
            $scope.search.from = undefined;
            $scope.search.top = undefined;
            if (value && this.options[value].id != 4) {
                $scope.search.offset_article = 1;
                $scope.search.offset_customer = 1;
                $scope.searchThing()
            }
        },
        maxItems: 1
    };
    $scope.customerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Customers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_article = 1;
            $scope.search.offset_customer = 1;
            $scope.searchThing()
        },
        onType(str) {
            var selectize = angular.element('#customer_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'report-xarticle_report_invoices',
                    xget: 'cc',
                    term: str
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data;
                    if ($scope.item.customers.list.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (!$scope.item.customers.list.length) {
                var obj = {
                    'do': 'report-xarticle_report_invoices',
                    xget: 'cc'
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.supplierCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Suppliers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_article = 1;
            $scope.search.offset_customer = 1;
            $scope.searchThing()
        },
        onType(str) {
            var selectize = angular.element('#supplier_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'report-xarticle_report_invoices',
                    xget: 'suppliers_list',
                    term: str
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.suppliers = r.data;
                    if ($scope.item.suppliers.list.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (!$scope.item.suppliers.list.length) {
                var obj = {
                    'do': 'report-xarticle_report_invoices',
                    xget: 'suppliers_list'
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.suppliers = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.familyCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Families'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_article = 1;
            $scope.search.offset_customer = 1;
            $scope.searchThing()
        },
        onType(str) {
            var selectize = angular.element('#family_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'report-xarticle_report_invoices',
                    xget: 'families',
                    term: str
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.families = r.data;
                    if ($scope.item.families.list.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (!$scope.item.families.list.length) {
                var obj = {
                    'do': 'report-xarticle_report_invoices',
                    xget: 'families'
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.families = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.articleCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Articles'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_article = 1;
            $scope.search.offset_customer = 1;
            $scope.searchThing()
        },
        onType(str) {
            var selectize = angular.element('#articles_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var cat_id = 0;
                for (x in $scope.item.customers.list) {
                    if ($scope.item.customers.list[x].id == $scope.search.customer_id) {
                        cat_id = $scope.item.customers.list[x].cat_id;
                        break
                    }
                }
                var obj = {
                    'do': 'report-xarticle_report_invoices',
                    xget: 'articles_list',
                    search: str,
                    customer_id: $scope.search.customer_id,
                    cat_id: cat_id
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.articles = r.data;
                    if ($scope.item.articles.lines.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (!$scope.item.articles.lines.length) {
                var obj = {
                    'do': 'report-xarticle_report_invoices',
                    xget: 'articles_list'
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.articles = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.managerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Managers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_article = 1;
            $scope.search.offset_customer = 1;
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.searchThing = function(val) {
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        if (val) {
            $scope.search[val] = undefined
        }
        var obj = angular.copy($scope.search);
        obj.start_date = $scope.search.start_date.toUTCString();
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.toggleSearchButtons = function(item, val) {
        $scope.search[item] = val;
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        $scope.search.offset_article = 1;
        $scope.search.offset_customer = 1;
        var params = {};
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', params, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.listMore = function(index) {
        var new_line = '<tr class="versionInfo"><td colspan="8" class="no_border">' + '<table class="table dispatchTable">' + '<thead><th>' + '<div class="row">' + '<div class="col-sm-6">' + helper.getLanguage('Customer') + '</div>' + '<div class="col-sm-2 text-center">' + helper.getLanguage('Quantity') + '</div>' + '<div class="col-sm-2 text-center">' + helper.getLanguage('Margin') + '</div>' + '<div class="col-sm-2 text-center">' + helper.getLanguage('Value') + '</div>' + '</div>' + '</th></thead>' + '<tbody>' + '<tr ng-repeat="subart in item.article_tab.article_row[' + index + '].article_collaps">' + '<td>' + '<div class="row">' + '<div class="col-sm-6 text-muted"><a ng-click="toggleSearchButtons(\'customer_id\',subart.CUSTOMER_ID)"">{{subart.CUSTOMER_N}}</a></div>' + '<div class="col-sm-2 text-center text-muted">{{subart.CUSTOMER_QUANTITY}}</div>' + '<div class="col-sm-2 text-center text-muted"><span ng-bind-html="subart.CUSTOMER_TOTAL_MARGIN"></span> (<span>{{subart.CUSTOMER_TOTAL_MARGIN_PERCENT}}</span> %)</div>' + '<div class="col-sm-2 text-center text-muted"><span ng-bind-html="subart.TOTAL_CUSTOMER"></span></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>';
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').addClass('hidden');
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').removeClass('hidden');
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.orderTable.products tbody tr.collapseTr').eq(index).next())($scope)
    }
    $scope.listLess = function(index) {
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').removeClass('hidden');
        angular.element('.orderTable.products tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').addClass('hidden')
    }
    $scope.get_more = function(tab) {
        var obj = angular.copy($scope.search);
        obj.xget = tab;
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            if (tab == 'customersTab') {
                $scope.item.customer_tab = r.data
            } else if (tab == 'familiesTab') {
                $scope.item.family_tab = r.data
            } else {
                $scope.item.supply_tab = r.data
            }
        })
    }
    $scope.listMoreCustomer = function(index) {
        var new_line = '<tr class="versionInfo"><td colspan="8" class="no_border">' + '<table class="table dispatchTable">' + '<thead><th>' + '<div class="row">' + '<div class="col-sm-8">' + helper.getLanguage('Article') + '</div>' + '<div class="col-sm-2 text-center">' + helper.getLanguage('Quantity') + '</div>' + '<div class="col-sm-2 text-center">' + helper.getLanguage('Value') + '</div>' + '</div>' + '</th></thead>' + '<tbody>' + '<tr ng-repeat="subart in item.customer_tab.other_row[' + index + '].other_collaps">' + '<td>' + '<div class="row">' + '<div class="col-sm-8 text-muted">{{subart.ARTICLE_N}}</div>' + '<div class="col-sm-2 text-center text-muted">{{subart.TOTAL_ART_Q}}</div>' + '<div class="col-sm-2 text-center text-muted"><span ng-bind-html="subart.TOTAL_ART_AMOUNT"></span></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>';
        angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').addClass('hidden');
        angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').removeClass('hidden');
        angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).next())($scope)
    }
    $scope.listLessCustomer = function(index) {
        angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').removeClass('hidden');
        angular.element('.orderTable.customers tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').addClass('hidden')
    }
    $scope.navigate = function(i) {
        switch ($scope.search.period_id) {
            case '1':
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + i);
                break;
            case '2':
                $scope.search.start_date = $scope.search.start_date.addMonths(i);
                break;
            case '3':
                $scope.search.start_date.setFullYear($scope.search.start_date.getFullYear() + i);
                break;
            default:
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + (i * 7))
        }
        $scope.searchThing()
    }

    $scope.orderBY = function(p,tab) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        $scope.search.desc = $scope.reverse;
        $scope.search.order_by = $scope.ord;
        if(!tab){
            list.orderBy($scope, p, $scope.renderList)
        }else{
            $scope.get_more(tab);
        }
    };

    $scope.renderList(data)
}]).controller('ReportQuoteCtrl', ['$scope', '$state', '$cacheFactory', '$timeout', 'helper', 'data', function($scope, $state, $cacheFactory, $timeout, helper, data) {
    console.log("Report Quote");
    $scope.item = {};
    $scope.show_load = !0;
    $scope.periods = [{
        'id': '0',
        'value': helper.getLanguage('Weekly')
    }, {
        'id': '1',
        'value': helper.getLanguage('Daily')
    }, {
        'id': '2',
        'value': helper.getLanguage('Monthly')
    }, {
        'id': '3',
        'value': helper.getLanguage('Yearly')
    }, {
        'id': '4',
        'value': helper.getLanguage('Custom')
    }];
    $scope.search = {
        'do': 'report-xquote_report',
        'start_date': new Date(),
        'period_id': '0',
        'customer_id': undefined,
        'author_id': undefined,
        'from': undefined,
        'to': undefined,
        'offset_quote': 1,
        'quote_tab': '1',
        'type_id': undefined,
        'source_id': undefined
    };
    var typePromise;
    var cacheReport = $cacheFactory.get('ReportDigestToQuote');
    if (cacheReport != undefined) {
        var cachedDataReport = cacheReport.get('data')
    }
    if (cachedDataReport != undefined) {
        for (x in cachedDataReport) {
            $scope.search[x] = cachedDataReport[x]
        }
        cacheReport.remove('data')
    }
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        from: !1,
        to: !1,
        start_date: !1,
    }
    $scope.renderList = function(d) {
        if (d && d.data) {
            for (x in d.data) {
                $scope.item[x] = d.data[x]
            }
            if ($scope.item.start_date) {
                $scope.search.start_date = new Date($scope.item.start_date)
            }
        }
        $scope.show_load = !1;
    }
    $scope.periodCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Period'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.start_date = new Date();
            $scope.search.from = undefined;
            $scope.search.top = undefined;
            if (value && this.options[value].id != 4) {
                $scope.search.offset_quote = 1;
                $scope.searchThing()
            }
        },
        maxItems: 1
    };
    $scope.customerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Customers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_quote = 1;
            $scope.searchThing()
        },
        onType(str) {
            var selectize = angular.element('#customer_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'report-xquote_report',
                    xget: 'cc',
                    term: str
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data;
                    if ($scope.item.customers.list.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (!$scope.item.customers.list.length) {
                var obj = {
                    'do': 'report-xquote_report',
                    xget: 'cc'
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.authorCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Authors'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_quote = 1;
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.managerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Managers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_quote = 1;
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.selectSourceCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Source'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    $scope.selectTypeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Type'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    $scope.searchThing = function(val) {
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        if (val) {
            $scope.search[val] = undefined
        }
        var obj = angular.copy($scope.search);
        obj.start_date = $scope.search.start_date.toUTCString();
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.toggleSearchButtons = function(item, val) {
        $scope.search[item] = val;
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        $scope.search.offset_article = 1;
        $scope.search.offset_customer = 1;
        var params = {};
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', params, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.get_more = function(tab, quote_tab) {
        if (quote_tab) {
            if (quote_tab != $scope.search.quote_tab) {
                $scope.search.quote_tab = quote_tab;
                $scope.search.offset_quote = 1;
                $scope.ord = '';
                $scope.reverse = !1
            }
        }
        var obj = angular.copy($scope.search);
        obj.xget = tab;
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        if($scope.ord){
            obj.order_by = $scope.ord;
        }
        if($scope.reverse){
            obj.desc = true;
        }else{
            obj.desc = false;
        }

        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            if (tab == 'authorsTab') {
                $scope.item.author_tab = r.data
            } else if (tab == 'customersTab') {
                $scope.item.customer_tab = r.data
            }else {
                $scope.item.quote_tab = r.data
            }
        })
    }
    $scope.navigate = function(i) {
        switch ($scope.search.period_id) {
            case '1':
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + i);
                break;
            case '2':
                $scope.search.start_date = $scope.search.start_date.addMonths(i);
                break;
            case '3':
                $scope.search.start_date.setFullYear($scope.search.start_date.getFullYear() + i);
                break;
            default:
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + (i * 7))
        }
        $scope.searchThing()
    }
    $scope.orderBy = function(p,tab, q) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        $scope.search.offset_quote = 1;
        var obj = angular.copy($scope.search);
        obj.xget = tab;
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        obj.desc = $scope.reverse;
        obj.order_by = $scope.ord;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            if (tab == 'authorsTab') {
                $scope.item.author_tab = r.data
            } else if (tab == 'customersTab') {
                $scope.item.customer_tab = r.data
            }else {
                $scope.item.quote_tab = r.data
            }
        })
    }
    $scope.digest = function() {
        var cacheReport = $cacheFactory.get('ReportQuoteToDigest');
        if (cacheReport != undefined) {
            cacheReport.destroy()
        }
        var obj = angular.copy($scope.search);
        var cache = $cacheFactory('ReportQuoteToDigest');
        cache.put('data', obj);
        $state.go('report_qdigest')
    }
    $scope.resetAdvanced = function() {
        $scope.search.showAdvanced = !1;
        $scope.search.source_id = undefined;
        $scope.search.type_id = undefined;
        $scope.searchThing()
    }
    $scope.renderList(data)
}]).controller('ReportQuoteDigestCtrl', ['$scope', '$state', '$cacheFactory', 'helper', 'data', function($scope, $state, $cacheFactory, helper, data) {
    console.log("Report Quote Digest");
    $scope.item = {};
    $scope.search = {};
    var cacheReport = $cacheFactory.get('ReportQuoteToDigest');
    if (cacheReport != undefined) {
        var cachedDataReport = cacheReport.get('data')
    }
    if (cachedDataReport != undefined) {
        for (x in cachedDataReport) {
            $scope.search[x] = cachedDataReport[x]
        }
        cacheReport.remove('data')
    }
    $scope.renderList = function(d) {
        if (d && d.data) {
            for (x in d.data) {
                $scope.item[x] = d.data[x]
            }
        }
    }
    $scope.toReports = function() {
        var cacheReport = $cacheFactory.get('ReportDigestToQuote');
        if (cacheReport != undefined) {
            cacheReport.destroy()
        }
        var obj = angular.copy($scope.search);
        var cache = $cacheFactory('ReportDigestToQuote');
        cache.put('data', obj);
        $state.go('report_quotes')
    }
    $scope.renderList(data)
}]).controller('ReportOrderCtrl', ['$scope', '$timeout', 'helper', 'data', function($scope, $timeout, helper, data) {
    console.log("Report Order");
    $scope.item = {};
    $scope.show_load = !0;
    $scope.periods = [{
        'id': '0',
        'value': helper.getLanguage('Weekly')
    }, {
        'id': '1',
        'value': helper.getLanguage('Daily')
    }, {
        'id': '2',
        'value': helper.getLanguage('Monthly')
    }, {
        'id': '3',
        'value': helper.getLanguage('Yearly')
    }, {
        'id': '4',
        'value': helper.getLanguage('Custom')
    }];
    $scope.search = {
        'do': 'report-xorder_report',
        'start_date': new Date(),
        'period_id': '0',
        'customer_id': undefined,
        'author_id': undefined,
        'manager_id': undefined,
        'identity_id': undefined,
        'c_type': undefined,
        'from': undefined,
        'to': undefined,
        'offset_customer': 1,
        'offset_order': 1,
        'offset_article': 1,
        'offset_manager': '1',
        'order_tab': '1'
    };
    var typePromise;
    $scope.chart_options = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_order.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.chart_customer = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_customer.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.chart_manager = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_manager.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        from: !1,
        to: !1,
        start_date: !1,
    }
    $scope.renderList = function(d) {
        if (d && d.data) {
            for (x in d.data) {
                $scope.item[x] = d.data[x]
            }
            if ($scope.item.start_date) {
                $scope.search.start_date = new Date($scope.item.start_date)
            }
            $scope.ord = '';
            $scope.reverse = !1
        }
        $scope.show_load = !1;
    }
    $scope.periodCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Period'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.start_date = new Date();
            $scope.search.from = undefined;
            $scope.search.top = undefined;
            if (value && this.options[value].id != 4) {
                $scope.search.offset_customer = 1;
                $scope.search.offset_order = 1;
                $scope.search.offset_article = 1;
                $scope.searchThing()
            }
        },
        maxItems: 1
    };
    $scope.customerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Customers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_customer = 1;
            $scope.search.offset_order = 1;
            $scope.search.offset_article = 1;
            $scope.search.offset_manager = 1;
            $scope.searchThing()
        },
        onType(str) {
            var selectize = angular.element('#customer_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'report-xorder_report',
                    xget: 'cc',
                    term: str
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data;
                    if ($scope.item.customers.list.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (!$scope.item.customers.list.length) {
                var obj = {
                    'do': 'report-xorder_report',
                    xget: 'cc'
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.authorCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Authors'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_customer = 1;
            $scope.search.offset_order = 1;
            $scope.search.offset_article = 1;
            $scope.search.offset_manager = 1;
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.managerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Managers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_customer = 1;
            $scope.search.offset_order = 1;
            $scope.search.offset_article = 1;
            $scope.search.offset_manager = 1;
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.identityCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Bussiness Units'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 1
    };
    $scope.relationTypeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Relation Types'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 1
    };
    $scope.searchThing = function(val) {
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        if (val) {
            $scope.search[val] = undefined
        }
        var obj = angular.copy($scope.search);
        obj.start_date = $scope.search.start_date.toUTCString();
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.toggleSearchButtons = function(item, val) {
        $scope.search[item] = val;
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        $scope.search.offset_customer = 1;
        $scope.search.offset_order = 1;
        $scope.search.offset_article = 1;
        $scope.search.offset_manager = 1;
        var params = {};
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', params, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.get_more = function(tab, order_tab) {
        if (order_tab) {
            if (order_tab != $scope.search.order_tab) {
                $scope.search.order_tab = order_tab;
                $scope.search.offset_order = 1;
                $scope.ord = '';
                $scope.reverse = !1
            }
        }
        var obj = angular.copy($scope.search);
        obj.xget = tab;
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        if($scope.ord){
            obj.order_by = $scope.ord;
        }
        if($scope.reverse){
            obj.desc = true;
        }else{
            obj.desc = false;
        }
        if (order_tab && order_tab == '1') {
            obj.delivered = '1'
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            /*$scope.ord = '';
            $scope.reverse = !1;*/
            if (tab == 'customersTab') {
                $scope.item.customer_tab = r.data
            } else if (tab == 'ordersTab') {
                $scope.item.order_tab = r.data
            } else if (tab == 'articlesTab') {
                $scope.item.article_tab = r.data
            } else if (tab == 'taxesTab') {
                $scope.item.tax_tab = r.data
            } else {
                $scope.item.manager_tab = r.data
            }
        })
    }
    $scope.navigate = function(i) {
        switch ($scope.search.period_id) {
            case '1':
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + i);
                break;
            case '2':
                $scope.search.start_date = $scope.search.start_date.addMonths(i);
                break;
            case '3':
                $scope.search.start_date.setFullYear($scope.search.start_date.getFullYear() + i);
                break;
            default:
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + (i * 7))
        }
        $scope.searchThing()
    }
    $scope.orderBy = function(p, tab, del) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        $scope.search.offset_customer = 1;
        $scope.search.offset_order = 1;
        $scope.search.offset_article = 1;
        $scope.search.offset_manager = 1;
        var obj = angular.copy($scope.search);
        obj.xget = tab;
        if(del=='1'){
            obj.delivered = 1;
        }else{
            obj.delivered = 0;
        }
       
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        obj.desc = $scope.reverse;
        obj.order_by = $scope.ord;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            if (tab == 'customersTab') {
                $scope.item.customer_tab = r.data
            } else if(tab == 'ordersTab') {
                $scope.item.order_tab = r.data
            }else if(tab == 'articlesTab') {
                $scope.item.article_tab = r.data
            }else if(tab == 'taxesTab') {
                $scope.item.tax_tab = r.data
            }else if(tab == 'managersTab') {
                $scope.item.manager_tab = r.data
            }
        })
    }
    $scope.resetAdvanced = function() {
        $scope.search.showAdvanced = !1;
        $scope.search.source_id = undefined;
        $scope.search.type_id = undefined;
        $scope.search.identity_id = undefined;
        $scope.search.c_type = undefined;
        $scope.searchThing()
    }

    $scope.renderList(data)
}]).controller('ReportPOrderCtrl', ['$scope', '$timeout', 'helper', 'data', function($scope, $timeout, helper, data) {
    console.log("Report Purchase Order");
    $scope.item = {};
    $scope.show_load = !0;
    $scope.periods = [{
        'id': '0',
        'value': helper.getLanguage('Weekly')
    }, {
        'id': '1',
        'value': helper.getLanguage('Daily')
    }, {
        'id': '2',
        'value': helper.getLanguage('Monthly')
    }, {
        'id': '3',
        'value': helper.getLanguage('Yearly')
    }, {
        'id': '4',
        'value': helper.getLanguage('Custom')
    }];
    $scope.search = {
        'do': 'report-xporder_report',
        'start_date': new Date(),
        'period_id': '0',
        'customer_id': undefined,
        'author_id': undefined,
        'manager_id': undefined,
        'from': undefined,
        'to': undefined,
        'offset_customer': 1,
        'offset_order': 1,
        'offset_article': 1,
        'offset_manager': '1',
        'order_tab': '1'
    };
    var typePromise;
    $scope.chart_options = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_order.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.chart_customer = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_customer.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.chart_manager = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_manager.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        from: !1,
        to: !1,
        start_date: !1,
    }
    $scope.renderList = function(d) {
        if (d && d.data) {
            for (x in d.data) {
                $scope.item[x] = d.data[x]
            }
            if ($scope.item.start_date) {
                $scope.search.start_date = new Date($scope.item.start_date)
            }
            $scope.ord = '';
            $scope.reverse = !1
        }
        $scope.show_load = !1;
    }
    $scope.periodCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Period'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.start_date = new Date();
            $scope.search.from = undefined;
            $scope.search.top = undefined;
            if (value && this.options[value].id != 4) {
                $scope.search.offset_customer = 1;
                $scope.search.offset_order = 1;
                $scope.search.offset_article = 1;
                $scope.searchThing()
            }
        },
        maxItems: 1
    };
    $scope.customerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Suppliers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_customer = 1;
            $scope.search.offset_order = 1;
            $scope.search.offset_article = 1;
            $scope.search.offset_manager = 1;
            $scope.searchThing()
        },
        onType(str) {
            var selectize = angular.element('#customer_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'report-xporder_report',
                    xget: 'cc',
                    term: str
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data;
                    if ($scope.item.customers.list.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (!$scope.item.customers.list.length) {
                var obj = {
                    'do': 'report-xporder_report',
                    xget: 'cc'
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.authorCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Authors'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_customer = 1;
            $scope.search.offset_order = 1;
            $scope.search.offset_article = 1;
            $scope.search.offset_manager = 1;
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.managerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Managers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_customer = 1;
            $scope.search.offset_order = 1;
            $scope.search.offset_article = 1;
            $scope.search.offset_manager = 1;
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.searchThing = function(val) {
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        if (val) {
            $scope.search[val] = undefined
        }
        var obj = angular.copy($scope.search);
        obj.start_date = $scope.search.start_date.toUTCString();
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.toggleSearchButtons = function(item, val) {
        $scope.search[item] = val;
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        $scope.search.offset_customer = 1;
        $scope.search.offset_order = 1;
        $scope.search.offset_article = 1;
        $scope.search.offset_manager = 1;
        var params = {};
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', params, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.get_more = function(tab, order_tab) {
        if (order_tab) {
            if (order_tab != $scope.search.order_tab) {
                $scope.search.order_tab = order_tab;
                $scope.search.offset_order = 1;
                $scope.ord = '';
                $scope.reverse = !1
            }
        }
        var obj = angular.copy($scope.search);
        obj.xget = tab;
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        if (order_tab && order_tab == '1') {
            obj.received = '1'
        }
        if($scope.ord){
            obj.order_by = $scope.ord;
        }
        if($scope.reverse){
            obj.desc = true;
        }else{
            obj.desc = false;
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            /*$scope.ord = '';
            $scope.reverse = !1;*/
            if (tab == 'customersTab') {
                $scope.item.customer_tab = r.data
            } else if (tab == 'pordersTab') {
                $scope.item.order_tab = r.data
            } else if (tab == 'articlesTab') {
                $scope.item.article_tab = r.data
            } else if (tab == 'taxesTab') {
                $scope.item.tax_tab = r.data
            } else {
                $scope.item.manager_tab = r.data
            }
        })
    }
    $scope.navigate = function(i) {
        switch ($scope.search.period_id) {
            case '1':
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + i);
                break;
            case '2':
                $scope.search.start_date = $scope.search.start_date.addMonths(i);
                break;
            case '3':
                $scope.search.start_date.setFullYear($scope.search.start_date.getFullYear() + i);
                break;
            default:
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + (i * 7))
        }
        $scope.searchThing()
    }
    $scope.orderBy = function(p, tab, rec) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        $scope.search.offset_customer = 1;
        $scope.search.offset_order = 1;
        $scope.search.offset_article = 1;
        $scope.search.offset_manager = 1;
        var obj = angular.copy($scope.search);
        obj.xget = tab;
        if (rec == '1') {
            obj.received = 1
        }else{
             obj.received = 0;
        }
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        obj.desc = $scope.reverse;
        obj.order_by = $scope.ord;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            if (tab == 'customersTab') {
                $scope.item.customer_tab = r.data
            } else if(tab == 'pordersTab') {
                $scope.item.order_tab = r.data
            }else if(tab == 'articlesTab') {
                $scope.item.article_tab = r.data
            }else if(tab == 'taxesTab') {
                $scope.item.tax_tab = r.data
            }
        })
    }
    $scope.renderList(data)
}]).controller('ReportProjectCtrl', ['$scope', '$state', '$cacheFactory', '$timeout', '$compile', 'helper', 'list', 'modalFc', 'data', function($scope, $state, $cacheFactory, $timeout, $compile, helper, list, modalFc, data) {
    console.log("Report Project");
    $scope.item = {};
    $scope.show_load = !0;
    $scope.periods = [{
        'id': '0',
        'value': helper.getLanguage('Weekly')
    }, {
        'id': '1',
        'value': helper.getLanguage('Daily')
    }, {
        'id': '2',
        'value': helper.getLanguage('Monthly')
    }, {
        'id': '3',
        'value': helper.getLanguage('Yearly')
    }, {
        'id': '4',
        'value': helper.getLanguage('Custom')
    }];
    $scope.search = {
        'do': 'report-xproject_report',
        'start_date': new Date(),
        'period_id': '0',
        'customer_id': undefined,
        'user_id': undefined,
        'bunit_id': undefined,
        'from': undefined,
        'to': undefined,
        'internal': !1,
        'project_id': undefined,
        'task_id': undefined,
        'project_name': undefined,
        'task_name': undefined
    };
    var typePromise;
    $scope.chart_options = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_hours.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.chart_customer = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_customer.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.chart_user = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_user.data_show[tooltipItem.index]
                }
            }
        }
    };
    var cacheReport = $cacheFactory.get('ReportDetailToProject');
    if (cacheReport != undefined) {
        var cachedDataReport = cacheReport.get('data')
    }
    if (cachedDataReport != undefined) {
        for (x in cachedDataReport) {
            $scope.search[x] = cachedDataReport[x]
        }
        cacheReport.remove('data')
    }
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        from: !1,
        to: !1,
        start_date: !1,
    }
    $scope.resetAditional = function() {
        $scope.search.project_id = undefined;
        $scope.search.task_id = undefined;
        $scope.search.task_name = undefined;
        $scope.search.project_name = undefined
    }
    $scope.renderList = function(d) {
        if (d && d.data) {
            for (x in d.data) {
                $scope.item[x] = d.data[x]
            }
            if ($scope.item.start_date) {
                $scope.search.start_date = new Date($scope.item.start_date)
            }
            $timeout(function() {
                angular.element('#reportsNav li').removeClass('active');
                angular.element('.tab-content div').removeClass('in active');
                angular.element('#reportsNav li:first-child').addClass('active');
                if ($scope.search.customer_id || $scope.search.task_id) {
                    angular.element('#reportsNav li a[href=#projects]').parent().triggerHandler('click');
                    angular.element('#projects').addClass('in active')
                } else {
                    if ($scope.item.is_bunit) {
                        angular.element('#bunits').addClass('in active')
                    } else {
                        angular.element('#customers').addClass('in active')
                    }
                }
            })
        }
        $scope.show_load = !1;
    }
    $scope.periodCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Period'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.start_date = new Date();
            $scope.search.from = undefined;
            $scope.search.top = undefined;
            if (value && this.options[value].id != 4) {
                $scope.searchThing()
            }
        },
        maxItems: 1
    };
    $scope.customerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Customers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.searchThing()
        },
        onType(str) {
            var selectize = angular.element('#customer_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'report-xproject_report',
                    xget: 'cc',
                    term: str
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data;
                    if ($scope.item.customers.list.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (!$scope.item.customers.list.length) {
                var obj = {
                    'do': 'report-xproject_report',
                    xget: 'cc'
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.userCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Users'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.bunitCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Business Units'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.searchThing = function(val) {
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        $scope.resetAditional();
        if (val) {
            $scope.search[val] = undefined;
            if (val == 'project_id') {
                $scope.search.customer_id = undefined
            }
        }
        var obj = angular.copy($scope.search);
        obj.start_date = $scope.search.start_date.toUTCString();
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.toggleSearchButtons = function(item, val, val2) {
        $scope.resetAditional();
        $scope.search[item] = val;
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        if (item == 'project_id') {
            $scope.search.project_name = val2.PROJECT_N;
            $scope.search.customer_id = val2.CUSTOMER_ID
        } else if (item == 'task_id') {
            $scope.search.task_name = val2.TASK_N
        }
        var params = {};
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', params, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.listMore = function(index, tab, table) {
        $scope.temp_obj = {};
        var obj = {};
        obj.do = 'report-xproject_report';
        obj.xget = 'moreUserReport';
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        if (tab == 'task_tab') {
            obj.project_id = $scope.item[tab].other_row[index].PROJECT_ID;
            obj.task_id = $scope.item[tab].other_row[index].TASK_ID;
            obj.user_id = $scope.search.user_id
        } else {
            obj.project_id = $scope.search.project_id;
            obj.user_id = $scope.item[tab].other_row[index].USER_ID
        }
        obj.customer_id = $scope.search.customer_id;
        obj.internal = $scope.search.internal;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            $scope.temp_obj = r.data;
            $col = tab == 'task_tab' ? 'col-sm-4' : 'col-sm-5';
            $col1 = tab == 'task_tab' ? 'col-sm-2' : 'col-sm-1';
            var new_line = '<tr class="versionInfo"><td colspan="8" class="no_border">' + '<table class="table dispatchTable">' + '<tbody>' + '<tr ng-repeat="subart in temp_obj.other_row">' + '<td>' + '<div class="row">' + '<div class="text-muted ' + $col + '"><div class="row"><span>{{subart.STAFF_N}}</span></div></div>' + '<div class="text-muted ' + $col1 + '"><div class="row"><p><a href="#" ng-click="detailed(\'user_id2\',subart.USER_ID,subart)"><span>{{subart.TOTAL_HOUR}}</span></a></p><p><a href="#" ng-click="detailed(\'user_id2\',subart.USER_ID,subart)"><span>{{subart.TOTAL_DAY}}</span></a></p></div></div>' + '<div class="col-sm-2 text-muted"><div class="row"><p><span>{{subart.INTERNAL}}</span></p><p><span ng-bind-html="subart.INTERNAL_COST"></span></p></div></div>' + '<div class="col-sm-2 text-muted"><div class="row"><p><span>{{subart.BILLABLE_H2}}</span></p><p><span>{{subart.BILLABLE_H2_DAY}}</span></p></div></div>' + '<div class="col-sm-2 text-muted"><div class="row"><span ng-bind-html="subart.BILLALBE_A"></span></div></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>';
            angular.element('.orderTable.' + table + ' tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').addClass('hidden');
            angular.element('.orderTable.' + table + ' tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').removeClass('hidden');
            angular.element('.orderTable.' + table + ' tbody tr.collapseTr').eq(index).after(new_line);
            $compile(angular.element('.orderTable.' + table + ' tbody tr.collapseTr').eq(index).next())($scope)
        })
    }
    $scope.listLess = function(index, table) {
        angular.element('.orderTable.' + table + ' tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable.' + table + ' tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').removeClass('hidden');
        angular.element('.orderTable.' + table + ' tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').addClass('hidden')
    }
    $scope.listMoreExp = function(index) {
        $scope.temp_obj = {};
        var obj = {};
        obj.do = 'report-xproject_report';
        obj.xget = 'moreExpReport';
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        obj.project_id = $scope.item.expense_tab.other_row[index].PROJECT_ID;
        obj.task_id = $scope.item.expense_tab.other_row[index].TASK_ID;
        obj.user_id = $scope.search.user_id;
        obj.bunit_id = $scope.search.bunit_id;
        obj.customer_id = $scope.search.customer_id;
        obj.internal = $scope.search.internal;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            $scope.temp_obj = r.data;
            var new_line = '<tr class="versionInfo"><td colspan="8" class="no_border">' + '<table class="table dispatchTable">' + '<tbody>' + '<tr ng-repeat="subart in temp_obj.other_row">' + '<td>' + '<div class="row">' + '<div class="col-sm-8 text-muted"><div class="row"><a href="#" ng-click="toggleSearchButtons(\'user_id\',subart.USER_ID)"><span>{{subart.user_name}}</span></a></div></div>' + '<div class="col-sm-2 text-muted"><div class="row"><span ng-bind-html="subart.amount_internal"></span></div></div>' + '<div class="col-sm-2 text-muted"><div class="row"><span ng-bind-html="subart.amount"></span></div></div>' + '</div>' + '</td>' + '</tr>' + '</tbody>' + '</table>' + '</td></tr>';
            angular.element('.orderTable.expense tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').addClass('hidden');
            angular.element('.orderTable.expense tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').removeClass('hidden');
            angular.element('.orderTable.expense tbody tr.collapseTr').eq(index).after(new_line);
            $compile(angular.element('.orderTable.expense tbody tr.collapseTr').eq(index).next())($scope)
        })
    }
    $scope.listLessExp = function(index) {
        angular.element('.orderTable.expense tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable.expense tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').removeClass('hidden');
        angular.element('.orderTable.expense tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').addClass('hidden')
    }
    $scope.get_more = function(tab) {
        var obj = angular.copy($scope.search);
        obj.xget = tab;
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            if (tab == 'projectsTab') {
                $scope.item.project_tab = r.data
            } else if (tab == 'tasksTab') {
                $scope.item.task_tab = r.data
            } else if (tab == 'staffTab') {
                $scope.item.staff_tab = r.data
            } else if (tab == 'productsTab') {
                $scope.item.article_tab = r.data
            } else if (tab == 'bunitsTab') {
                $scope.item.bunit_tab = r.data
            } else if (tab == 'customersTab') {
                $scope.item.customer_tab = r.data
            } else {
                $scope.item.expense_tab = r.data
            }
        })
    }
    $scope.navigate = function(i) {
        switch ($scope.search.period_id) {
            case '1':
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + i);
                break;
            case '2':
                $scope.search.start_date = $scope.search.start_date.addMonths(i);
                break;
            case '3':
                $scope.search.start_date.setFullYear($scope.search.start_date.getFullYear() + i);
                break;
            default:
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + (i * 7))
        }
        $scope.searchThing()
    }
    $scope.detailed = function(item, val, val2) {
        var obj = angular.copy($scope.search);
        if (item) {
            obj[item] = val;
            if (val2) {
                if (item == 'project_id2') {
                    obj.customer_id2 = val2.CUSTOMER_ID
                } else if (item == 'task_id2') {
                    obj.project_id2 = val2.PROJECT_ID;
                    obj.customer_id2 = val2.CUSTOMER_ID
                } else if (item == 'user_id2') {
                    obj.project_id2 = val2.PROJECT_ID;
                    obj.task_id2 = val2.TASK_ID;
                    if (val2.CUSTOMER_ID) {
                        obj.customer_id2 = val2.CUSTOMER_ID
                    }
                }
            }
        }
        var cacheReport = $cacheFactory.get('ReportProjectToDetail');
        if (cacheReport != undefined) {
            cacheReport.destroy()
        }
        var cache = $cacheFactory('ReportProjectToDetail');
        cache.put('data', obj);
        $state.go('report_pdetail')
    }

        $scope.orderBy = function(p, tab) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        $scope.search.offset_customer = 1;
        $scope.search.offset_order = 1;
        $scope.search.offset_article = 1;
        $scope.search.offset_manager = 1;
        var obj = angular.copy($scope.search);
        obj.xget = tab;

        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        obj.desc = $scope.reverse;
        obj.order_by = $scope.ord;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            if (tab == 'projectsTab') {
                $scope.item.project_tab = r.data
            } else if (tab == 'tasksTab') {
                $scope.item.task_tab = r.data
            } else if (tab == 'staffTab') {
                $scope.item.staff_tab = r.data
            } else if (tab == 'productsTab') {
                $scope.item.article_tab = r.data
            } else if (tab == 'bunitsTab') {
                $scope.item.bunit_tab = r.data
            } else if (tab == 'customersTab') {
                $scope.item.customer_tab = r.data
            } else {
                $scope.item.expense_tab = r.data
            }
        })
    }
    $scope.renderList(data)
}]).controller('ReportProjectDetailedCtrl', ['$scope', '$state', '$cacheFactory', 'helper', 'data', function($scope, $state, $cacheFactory, helper, data) {
    console.log("Report Project Detailed");
    $scope.item = {};
    $scope.search = {};
    var cacheReport = $cacheFactory.get('ReportProjectToDetail');
    if (cacheReport != undefined) {
        var cachedDataReport = cacheReport.get('data')
    }
    if (cachedDataReport != undefined) {
        for (x in cachedDataReport) {
            if (x == 'customer_id2' || x == 'bunit2' || x == 'project_id2' || x == 'task_id2' || x == 'user_id2') {
                continue
            }
            $scope.search[x] = cachedDataReport[x]
        }
        cacheReport.remove('data')
    }
    $scope.renderList = function(d) {
        if (d && d.data) {
            for (x in d.data) {
                $scope.item[x] = d.data[x]
            }
        }
    }
    $scope.toReports = function() {
        var cacheReport = $cacheFactory.get('ReportDetailToProject');
        if (cacheReport != undefined) {
            cacheReport.destroy()
        }
        var obj = angular.copy($scope.search);
        var cache = $cacheFactory('ReportDetailToProject');
        cache.put('data', obj);
        $state.go('report_projects')
    }
    $scope.renderList(data)
}]).controller('ReportInvoiceCtrl', ['$scope', '$timeout', 'helper', 'data','list', function($scope, $timeout, helper, data, list) {
    console.log("Report Invoice");
    $scope.item = {};
    $scope.show_load = !0;
    $scope.periods = [{
        'id': '0',
        'value': helper.getLanguage('Weekly')
    }, {
        'id': '1',
        'value': helper.getLanguage('Daily')
    }, {
        'id': '2',
        'value': helper.getLanguage('Monthly')
    }, {
        'id': '5',
        'value': helper.getLanguage('Monthly - first invoice')
    }, {
        'id': '3',
        'value': helper.getLanguage('Yearly')
    }, {
        'id': '4',
        'value': helper.getLanguage('Custom')
    }];
    $scope.search = {
        'do': 'report-xinvoice_report',
        'start_date': new Date(),
        'period_id': '0',
        'customer_id': undefined,
        'manager_id': undefined,
        'identity_id': undefined,
        'c_type': undefined,
        'from': undefined,
        'to': undefined,
        'offset_customer': 1,
        'offset_invoice': 1,
        'invoice_tab': 1,
        'offset_manager': 1,
        'type_id': undefined,
        'source_id': undefined
    };
    var typePromise;
    $scope.chart_options = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_invoice.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.chart_customer = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_customer.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.chart_manager = {
        legend: {
            display: !1
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ": " + $scope.item.chart_manager.data_show[tooltipItem.index]
                }
            }
        }
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        from: !1,
        to: !1,
        start_date: !1,
    }
    $scope.renderList = function(d) {
        if (d && d.data) {
            for (x in d.data) {
                $scope.item[x] = d.data[x]
            }
            if ($scope.item.start_date) {
                $scope.search.start_date = new Date($scope.item.start_date)
            }
        }
        $scope.show_load = !1;
    }
    $scope.periodCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Period'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.start_date = new Date();
            $scope.search.from = undefined;
            $scope.search.top = undefined;
            if (value && this.options[value].id != 4) {
                $scope.search.offset_customer = 1;
                $scope.search.offset_invoice = 1;
                $scope.search.offset_manager = 1;
                $scope.searchThing()
            }
        },
        maxItems: 1
    };
    $scope.customerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Customers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_customer = 1;
            $scope.search.offset_invoice = 1;
            $scope.search.offset_manager = 1;
            $scope.searchThing()
        },
        onType(str) {
            var selectize = angular.element('#customer_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'report-xinvoice_report',
                    xget: 'cc',
                    term: str
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data;
                    if ($scope.item.customers.list.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (!$scope.item.customers.list.length) {
                var obj = {
                    'do': 'report-xinvoice_report',
                    xget: 'cc'
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.managerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Managers'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_customer = 1;
            $scope.search.offset_invoice = 1;
            $scope.search.offset_manager = 1;
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.identityCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Bussiness Units'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 1
    };
    $scope.relationTypeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Relation Types'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 1
    };
    $scope.selectSourceCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Source'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    $scope.selectTypeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Type'),
        closeAfterSelect: !0,
        create: !1,
        maxItems: 1
    };
    $scope.searchThing = function(val) {
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        if (val) {
            $scope.search[val] = undefined
        }
        var obj = angular.copy($scope.search);
        obj.start_date = $scope.search.start_date.toUTCString();
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.toggleSearchButtons = function(item, val) {
        $scope.search[item] = val;
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        $scope.search.offset_customer = 1;
        $scope.search.offset_invoice = 1;
        $scope.search.offset_manager = 1;
        var params = {};
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', params, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.get_more = function(tab, invoice_tab) {
        if (invoice_tab) {
            if (invoice_tab != $scope.search.invoice_tab) {
                $scope.search.invoice_tab = invoice_tab;
                $scope.search.offset_customer = 1;
                $scope.search.offset_invoice = 1;
                $scope.search.offset_manager = 1
            }
        }
        var obj = angular.copy($scope.search);
        obj.xget = tab;
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        if($scope.ord){
            obj.order_by = $scope.ord;
        }
        if($scope.reverse){
            obj.desc = true;
        }else{
            obj.desc = false;
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            if (tab == 'customersTab') {
                $scope.item.customer_tab = r.data
            } else if (tab == 'invoicesTab') {
                $scope.item.invoice_tab = r.data
            } else {
                $scope.item.manager_tab = r.data
            }
        })
    }
    $scope.navigate = function(i) {
        switch ($scope.search.period_id) {
            case '1':
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + i);
                break;
            case '2':
                $scope.search.start_date = $scope.search.start_date.addMonths(i);
                break;
            case '3':
                $scope.search.start_date.setFullYear($scope.search.start_date.getFullYear() + i);
                break;
            case '5':
                $scope.search.start_date = $scope.search.start_date.addMonths(i);
                break;
            default:
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + (i * 7))
        }
        $scope.searchThing()
    }
    $scope.resetAdvanced = function() {
        $scope.search.showAdvanced = !1;
        $scope.search.source_id = undefined;
        $scope.search.type_id = undefined;
        $scope.search.identity_id = undefined;
        $scope.search.c_type = undefined;
        $scope.searchThing()
    }
    $scope.searchAdvanced = function() {
        $scope.search.offset_customer = 1;
        $scope.search.offset_invoice = 1;
        $scope.search.offset_manager = 1;
        $scope.searchThing()
    }

    $scope.orderBY = function(p,tab,inv) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        $scope.search.desc = $scope.reverse;
        $scope.search.order_by = $scope.ord;
        if(!tab){
            list.orderBy($scope, p, $scope.renderList)
        }else{
            $scope.get_more(tab,inv);
        }
    };

    $scope.renderList(data)
}]).controller('ReportClientHistoryCtrl', ['$scope', '$timeout', 'helper', 'data', function($scope, $timeout, helper, data) {
    console.log("Report Client History");
    $scope.item = {};
    $scope.show_load = !0;
    $scope.periods = [{
        'id': '0',
        'value': helper.getLanguage('Weekly')
    }, {
        'id': '1',
        'value': helper.getLanguage('Daily')
    }, {
        'id': '2',
        'value': helper.getLanguage('Monthly')
    }, {
        'id': '3',
        'value': helper.getLanguage('Yearly')
    }, {
        'id': '4',
        'value': helper.getLanguage('Custom')
    }];
    $scope.search = {
        'do': 'report-xclient_history_report',
        'start_date': new Date(),
        'period_id': '0',
        'customer_id': undefined,
        'search': undefined,
        'from': undefined,
        'to': undefined,
        'offset_customer': 1,
        'offset_invoice': 1,
        'invoice_tab': 1,
        'offset_manager': 1
    };
    var typePromise;
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        from: !1,
        to: !1,
        start_date: !1,
    }
    $scope.searched = false;
    $scope.renderList = function(d) {
            if (d && d.data) {
                if ($scope.item.start_date) {
                    $scope.search.start_date = new Date($scope.item.start_date)
                }
           
                for (x in d.data) {
                    $scope.item[x] = d.data[x]
                    /*if(!$scope.searched && x=='invoice_tab'){
                        $scope.item[x] = [];
                    } */                  
                }             
            }  
            $scope.show_load = !1;
       
    }
    $scope.periodCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Period'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.start_date = new Date();
            $scope.search.from = undefined;
            $scope.search.top = undefined;
            if (value && this.options[value].id != 4) {
                $scope.search.offset_customer = 1;
                $scope.search.offset_invoice = 1;
                $scope.search.offset_manager = 1;
                $scope.searchThing()
            }
        },
        maxItems: 1
    };
    $scope.customerCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('No customer'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            $scope.search.offset_customer = 1;
            $scope.search.offset_invoice = 1;
            $scope.search.offset_manager = 1;
            $scope.searchThing()
        },
        onType(str) {
            var selectize = angular.element('#customer_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'report-xinvoice_report',
                    xget: 'cc',
                    term: str
                };
                angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data;
                    if ($scope.item.customers.list.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (!$scope.item.customers.list.length) {
                var obj = {
                    'do': 'report-xinvoice_report',
                    xget: 'cc'
                };
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', obj, function(r) {
                    $scope.item.customers = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.searchThing = function(val) {
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        if (val) {
            $scope.search[val] = undefined
        }
        $scope.searched = true;
        var obj = angular.copy($scope.search);
        obj.start_date = $scope.search.start_date.toUTCString();
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.toggleSearchButtons = function(item, val) {
        $scope.search[item] = val;
        if ($scope.search.period_id == '4' && $scope.search.to != undefined && $scope.search.from == undefined) {
            return !1
        }
        $scope.show_load = !0;
        $scope.search.offset_customer = 1;
        $scope.search.offset_invoice = 1;
        $scope.search.offset_manager = 1;
        var params = {};
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', params, function(r) {
            $scope.renderList(r);
            $timeout(function() {
                angular.element('#reportsNav li:first-child a').tab('show').triggerHandler('click')
            })
        })
    };
    $scope.get_more = function(tab, invoice_tab) {
        if (invoice_tab) {
            if (invoice_tab != $scope.search.invoice_tab) {
                $scope.search.invoice_tab = invoice_tab;
                $scope.search.offset_customer = 1;
                $scope.search.offset_invoice = 1;
                $scope.search.offset_manager = 1
            }
        }
        var obj = angular.copy($scope.search);
        obj.xget = tab;
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        obj.desc = $scope.reverse;
        obj.order_by = $scope.ord;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            if (tab == 'customersTab') {
                $scope.item.customer_tab = r.data
            } else if (tab == 'invoicesTab') {
                $scope.item.invoice_tab = r.data
            } else {
                $scope.item.manager_tab = r.data
            }
        })
    }
    $scope.orderBY = function(p) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        var obj = angular.copy($scope.search);
        obj.xget = 'invoicesTab';
        obj.time_start = $scope.item.time_start;
        obj.time_end = $scope.item.time_end;
        obj.desc = $scope.reverse;
        obj.order_by = $scope.ord;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            $scope.item.invoice_tab = r.data
        })
    };
    $scope.navigate = function(i) {
        switch ($scope.search.period_id) {
            case '1':
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + i);
                break;
            case '2':
                $scope.search.start_date = $scope.search.start_date.addMonths(i);
                break;
            case '3':
                $scope.search.start_date.setFullYear($scope.search.start_date.getFullYear() + i);
                break;
            default:
                $scope.search.start_date.setDate($scope.search.start_date.getDate() + (i * 7))
        }
        $scope.searchThing()
    }
    $scope.renderList(data)
}])