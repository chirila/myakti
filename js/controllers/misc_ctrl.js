angular.module('akti').controller('EditCustomerModalCtrl', ['$scope', '$timeout', '$uibModalInstance', 'data', 'modalFc', 'helper', 'selectizeFc', function($scope, $timeout, $uibModalInstance, data, modalFc, helper, selectizeFc) {
    var vmMod = this,
        triedToSave = !1,
        typePromise;
    var old_vmMod = {};
    vmMod.auto = {
        options: {
            c_type: data.c_type,
            accountManager: data.accountManager,
            sales: data.sales
        }
    }
    for (x in data) {
        vmMod[x] = data[x]
    }
    for (x in data.extra_dd) {
        vmMod.extra_dd[x] = data.extra_dd[x]
    }
    if(vmMod.obj.financial_tab){
        $timeout(function() {
            vmMod.activeteTab('add_contact');
        });
    }
    vmMod.customerTypeCfg = selectizeFc.configure({
        labelField: 'name',
        placeholder: helper.getLanguage('Select type'),
        searchField: ['name'],
        maxOptions: 50,
        maxItems: 50,
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value;
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit"
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                vmMod[realModel][(vmMod[realModel].length - 1)].id = o.data.inserted_id;
                vmMod.obj[realModel][(vmMod.obj[realModel].length - 1)] = o.data.inserted_id
            })
        }
    }, !1, !0, vmMod);
    vmMod.salesCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        onDropdownOpen($dropdown) {
            var _this = this,
                model = _this.$input[0].attributes.model.value;
            if (vmMod.obj[model] != undefined && vmMod.obj[model] != '0') {
                old_vmMod[model] = vmMod.obj[model]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                vmMod[realModel] = o.data.lines;
                vmMod.obj[realModel] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            if (vmMod.obj[model] != undefined && vmMod.obj[model] != '0') {
                old_vmMod[model] = vmMod.obj[model]
            }
            if (item == '0') {
                vmMod.openModal('selectEdit', {
                    table: table,
                    model: model
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                vmMod.obj[model] = undefined;
                return !1
            }
        }
    }, !1, !0, vmMod);
    vmMod.extraCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        onDropdownOpen($dropdown) {
            var _this = this,
                extra_id = _this.$input[0].attributes.extra.value;
            if (vmMod.obj.extra[extra_id] != undefined && vmMod.obj.extra[extra_id] != '0') {
                old_vmMod['extra_' + extra_id] = vmMod.obj.extra[extra_id]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                vmMod[realModel][extra_id] = o.data.lines;
                vmMod.obj[realModel][extra_id] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = _this.$input[0].attributes.extra.value;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            var extra_id = _this.$input[0].attributes.extra.value;
            if (vmMod.obj.extra[extra_id] != undefined && vmMod.obj.extra[extra_id] != '0') {
                old_vmMod['extra_' + extra_id] = vmMod.obj.extra[extra_id]
            }
            if (item == '0') {
                vmMod.openModal('selectEdit', {
                    table: table,
                    model: model,
                    extra_id: extra_id
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                vmMod.obj.extra[extra_id] = undefined;
                return !1
            }
        }
    }, !1, !0, vmMod);
    vmMod.ok = ok;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;
    vmMod.activeteTab = activeteTab;
    vmMod.checkVat = checkVat;
    vmMod.reset = reset;
    vmMod.openModal = openModal;
    vmMod.renderPage = renderPage;

    function ok(formObj) {
        if (vmMod.obj.add_customer === !0) {
            vmMod.obj.do = data.obj.do
        } else {
            vmMod.obj.do = "misc-EditCustomer-misc-update_financial"
        }
        formObj.$invalid = !0;
        helper.doRequest('post', 'index.php', vmMod.obj, function(d) {
            triedToSave = !0;
            if (d.success) {
                for (x in d.data) {
                    vmMod[x] = d.data[x]
                }
                vmMod.obj.update_invoice_language = !0;
                vmMod.showAlerts(d)
            } else if (d.error) {
                vmMod.showAlerts(d, formObj)
            }
        })
    };

    function cancel() {
        $uibModalInstance.dismiss(vmMod.obj)
    };

    function showAlerts(d, formObj) {
        helper.showAlerts(vmMod, d, formObj)
    }

    function openModal(type, item, size, obj, extra_id) {
        var params = {
            template: 'miscTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        }
        switch (type) {
            case 'selectEdit':
                params.params = {
                    'do': 'misc-selectEdit',
                    'table': item.table,
                    'extra_id': item.extra_id
                };
                params.callback = function(data) {
                    if (data.data) {
                        if (item.extra_id) {
                            vmMod.extra[item.extra_id] = data.data.lines;
                            if (old_vmMod['extra_' + item.extra_id]) {
                                vmMod.obj.extra[item.extra_id] = old_vmMod['extra_' + item.extra_id]
                            }
                        } else {
                            vmMod[item.table] = data.data.lines;
                            if (old_vmMod[item.model]) {
                                vmMod.obj[item.model] = old_vmMod[item.model]
                            }
                        }
                    } else {
                        if (item.extra_id) {
                            vmMod.extra[item.extra_id] = data.lines;
                            if (old_vmMod['extra_' + item.extra_id]) {
                                vmMod.obj.extra[item.extra_id] = old_vmMod['extra_' + item.extra_id]
                            }
                        } else {
                            vmMod[item.table] = data.lines;
                            if (old_vmMod[item.model]) {
                                vmMod.obj[item.model] = old_vmMod[item.model]
                            }
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }

    function activeteTab(i) {
        vmMod.obj.add_customer = !1;
        vmMod.obj.add_contact = !1;
        vmMod.obj[i] = !0
    }

    function checkVat() {
        vmMod.reset();
        vmMod.checking = !0;
        vmMod.showViesUnavailable=false;
        if (!vmMod.obj.btw_nr) {
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            vmMod.checking = !1;
            return
        }
        if (typePromise) {
            $timeout.cancel(typePromise)
        }
        typePromise = $timeout(function() {
            if (vmMod.obj.btw_nr) {
                var data = {
                    'do': 'misc--misc-check_vies_vat_number',
                    value: vmMod.obj.btw_nr
                };
                helper.doRequest('post', 'index.php', data, vmMod.renderPage)
            } else {
                vmMod.reset()
            }
        }, 300)
    }

    function reset() {
        vmMod.checking = !1;
        vmMod.isOk = !1;
        vmMod.isFalse = !1;
        vmMod.isTrends = !1
    }

    function renderPage(d) {
        vmMod.reset();
        if ((d.error || d.notice) && vmMod.obj.btw_nr) {
            vmMod.isFalse = !0;
            if(d.notice){
                vmMod.showViesUnavailable=true;
            }
        }
        if (d.success && vmMod.obj.btw_nr) {
            if (d.data.trends_ok) {
                vmMod.isTrends = !0
            } else {
                vmMod.isOk = !0
            }
        }
    }
}]).controller('EditContactModalCtrl', ['$scope', '$timeout', '$uibModalInstance', 'data', 'modalFc', 'helper', 'selectizeFc', function($scope, $timeout, $uibModalInstance, data, modalFc, helper, selectizeFc) {
    var vmMod = this,
        typePromise;
    var old_vmMod = {};
    vmMod.obj = data.contact;
    vmMod.obj.birthdate = data.contact.birthdate ? new Date(data.contact.birthdate) : '';
    vmMod.customer_contact_language = data.customer_contact_language;
    vmMod.is_admin = data.is_admin;
    vmMod.e_title = data.e_title;
    vmMod.customer_contact_title = data.customer_contact_title;
    vmMod.customer_contact_dep = data.customer_contact_dep;
    vmMod.customer_contact_job_title = data.customer_contact_job_title;
    vmMod.extra = [];
    for (x in data.extra) {
        vmMod.extra[x] = data.extra[x]
    }
    vmMod.auto = {
        options: {
            function: data.position_dd,
            company: data.customer_dd
        }
    }
    vmMod.salesCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        onDropdownOpen($dropdown) {
            var _this = this,
                model = _this.$input[0].attributes.model.value;
            if (vmMod.obj[model] != undefined && vmMod.obj[model] != '0') {
                old_vmMod[model] = vmMod.obj[model]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                vmMod[realModel] = o.data.lines;
                vmMod.obj[realModel] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            if (vmMod.obj[model] != undefined && vmMod.obj[model] != '0') {
                old_vmMod[model] = vmMod.obj[model]
            }
            if (item == '0') {
                vmMod.openModal('selectEdit', {
                    table: table,
                    model: model
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                vmMod.obj[model] = undefined;
                return !1
            }
        }
    }, !1, !0, vmMod);
    vmMod.extraCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        onDropdownOpen($dropdown) {
            var _this = this,
                extra_id = _this.$input[0].attributes.extra.value;
            if (vmMod.obj.extra[extra_id] != undefined && vmMod.obj.extra[extra_id] != '0') {
                old_vmMod['extra_' + extra_id] = vmMod.obj.extra[extra_id]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                vmMod[realModel][extra_id] = o.data.lines;
                vmMod.obj[realModel][extra_id] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = _this.$input[0].attributes.extra.value;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            if (vmMod.obj.extra[extra_id] != undefined && vmMod.obj.extra[extra_id] != '0') {
                old_vmMod['extra_' + extra_id] = vmMod.obj.extra[extra_id]
            }
            if (item == '0') {
                vmMod.openModal('selectEdit', {
                    table: table,
                    model: model,
                    extra_id: extra_id
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                vmMod.obj.extra[extra_id] = undefined;
                return !1
            }
        }
    }, !1, !0, vmMod);
    vmMod.customerCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: '',
        create: !1,
        maxOptions: 1,
        maxItems: 50
    };
    vmMod.ok = ok;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;
    vmMod.activeteTab = activeteTab;
    vmMod.openModal = openModal;
    vmMod.openDate = openDate;
    vmMod.format = 'dd/MM/yyyy';
    vmMod.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    vmMod.datePickOpen = {
        stop_date: !1
    }

    function openDate(p) {
        vmMod.datePickOpen[p] = !0
    };

    function ok(formObj) {
        vmMod.obj.do = data.do_next;
        formObj.$invalid = !0;
        helper.doRequest('post', 'index.php', vmMod.obj, function(d) {
            triedToSave = !0;
            vmMod.showAlerts(d, formObj)
        })
    };

    function cancel() {
        $uibModalInstance.dismiss(vmMod.obj)
    };

    function showAlerts(d, formObj) {
        helper.showAlerts(vmMod, d, formObj)
    }

    function openModal(type, item, size, obj, extra_id) {
        var params = {
            template: 'miscTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        }
        switch (type) {
            case 'selectEdit':
                params.params = {
                    'do': 'misc-selectEdit',
                    'table': item.table,
                    'extra_id': item.extra_id
                };
                params.callback = function(data) {
                    if (data.data) {
                        if (item.extra_id) {
                            vmMod.extra[item.extra_id] = data.data.lines;
                            if (old_vmMod['extra_' + item.extra_id]) {
                                vmMod.obj.extra[item.extra_id] = old_vmMod['extra_' + item.extra_id]
                            }
                        } else {
                            vmMod[item.table] = data.data.lines;
                            if (old_vmMod[item.model]) {
                                vmMod.obj[item.model] = old_vmMod[item.model]
                            }
                        }
                    } else {
                        if (item.extra_id) {
                            vmMod.extra[item.extra_id] = data.lines;
                            if (old_vmMod['extra_' + item.extra_id]) {
                                vmMod.obj.extra[item.extra_id] = old_vmMod['extra_' + item.extra_id]
                            }
                        } else {
                            vmMod[item.table] = data.lines;
                            if (old_vmMod[item.model]) {
                                vmMod.obj[item.model] = old_vmMod[item.model]
                            }
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }

    function activeteTab(i) {
        vmMod.obj.add_customer = !1;
        vmMod.obj.add_contact = !1;
        vmMod.obj[i] = !0
    }
}]).controller('selectEditModalCtrl', ['$scope', '$uibModalInstance', '$timeout', '$confirm', 'helper', 'modalFc', 'data', function($scope, $uibModalInstance, $timeout, $confirm, helper, modalFc, data) {
    var vmMod = this;
    vmMod.obj = data;
    $timeout(function() {
        modalFc.setHeight(angular.element('.modal.in'))
    })
    vmMod.ok = ok;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;
    vmMod.addLine = addLine;
    vmMod.removeLine = removeLine;
    vmMod.openModal = openModal;

    function openModal(type, item, size, table, extra_id) {
        if (type == 'AddEntry') {
            var params = {
                template: 'miscTemplate/' + type,
                ctrl: type + 'ModalCtrl',
                size: size,
                backdrop: 'static'
            }
        }
        switch (type) {
            case 'AddEntry':
                params.params = {
                    'do': 'misc-AddEntry',
                    'table': table,
                    'id': item,
                    'extra_id': extra_id
                };
                params.callback = function(data) {
                    vmMod.obj.lines = data.data.lines
                }
                break
        }
        modalFc.open(params)
    }

    function ok() {
        var data = angular.copy(vmMod.obj);
        data.do = 'misc--misc-select_edit';
        helper.doRequest('post', 'index.php', data, function(d) {
            if (d.success) {
                $uibModalInstance.close(d)
            } else {
                vmMod.showAlerts(d)
            }
        })
    };

    function cancel() {
        $uibModalInstance.close(vmMod.obj)
    };

    function showAlerts(d) {
        helper.showAlerts(vmMod, d)
    }

    function addLine() {
        vmMod.obj.lines.push({
            number: '',
            name_select: "",
            id: ""
        });
        modalFc.setHeight(angular.element('.modal.in'))
    }

    function removeLine($i) {
        if ($i > -1) {
            var data = {
                'id': vmMod.obj.lines[$i].id,
                'table': vmMod.obj.table
            };
            data.do = 'misc--misc-delete_select';
            $confirm({
                ok: helper.getLanguage('Yes'),
                cancel: helper.getLanguage('No'),
                text: helper.getLanguage('Are you sure, you wanna delete this entry from the list? This entry will be deleted on all accounts using it!')
            }).then(function() {
                helper.doRequest('post', 'index.php', data, function(d) {
                    vmMod.obj.lines.splice($i, 1);
                    modalFc.setHeight(angular.element('.modal.in'))
                })
            }, function() {
                return !1
            })
        }
    }
}]).controller('AddEntryModalCtrl', ['$scope', '$uibModalInstance', '$confirm', '$timeout', 'helper', 'modalFc', 'data', function($scope, $uibModalInstance, $confirm, $timeout, helper, modalFc, data) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;
    vmMod.ok = ok;
    if (vmMod.obj.id) {
        vmMod.obj.edit_id = !0
    } else {
        vmMod.obj.edit_id = !1
    }

    function ok() {
        var data = angular.copy(vmMod.obj);
        data.do = 'misc--misc-Add_Entry';
        if (vmMod.obj.edit_id == !0) {
            $confirm({
                ok: helper.getLanguage('Yes'),
                cancel: helper.getLanguage('No'),
                text: helper.getLanguage('Are you sure, you wanna to change this Entry?The entry will be changed on all the accounts using it!')
            }).then(function() {
                helper.doRequest('post', 'index.php', data, function(d) {
                    if (d.success) {
                        $uibModalInstance.close(d)
                    } else {
                        vmMod.showAlerts(d)
                    }
                })
            }, function() {
                return !1
            })
        } else {
            helper.doRequest('post', 'index.php', data, function(d) {
                if (d.success) {
                    $uibModalInstance.close(d)
                } else {
                    vmMod.showAlerts(d)
                }
            })
        }
    };

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    };

    function showAlerts(d) {
        helper.showAlerts(vmMod, d)
    }
}]).controller('ChangeIdentityModalCtrl', ['$scope', '$uibModalInstance', 'data', 'helper', 'modalFc', '$timeout', function($scope, $uibModalInstance, data, helper, modalFc, $timeout) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.index = 0;
    for (x in vmMod.obj.lines) {
        if (vmMod.obj.lines[x].identity_id == vmMod.obj.identity_id) {
            vmMod.index = x
        }
    }
    $timeout(function() {
        modalFc.setHeight(angular.element('.modal.in'))
    })
    vmMod.ok = ok;
    vmMod.cancel = cancel;
    vmMod.setIndex = setIndex;

    function ok() {
        $uibModalInstance.close(vmMod.obj.lines[vmMod.index])
    };

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    };

    function setIndex(i) {
        vmMod.index = i
    }
}]).controller('AddAddressModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'editItem', function($scope, helper, $uibModalInstance, data, editItem) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.obj.delivery = !0;
    if (vmMod.obj.app == 'invoice') {
        vmMod.obj.invoice = !0;
        vmMod.obj.billing = !0
    }
    if (vmMod.obj.fromCrm) {
        vmMod.obj.delivery = !1
    }
    if (vmMod.obj.app == 'project') {
        vmMod.obj.delivery = !1;
        vmMod.obj.primary = !0
    }
    if (vmMod.obj.app == 'maintenance') {
        vmMod.obj.delivery = !1;
        if (vmMod.obj.app_serv == 'primary') {
            vmMod.obj.primary = !0;
            vmMod.obj.site = !0
        } else {
            vmMod.obj.site = !0
        }
    }
    if (vmMod.obj.app == 'quote') {
        vmMod.obj.delivery = !1;
        if (vmMod.obj.app_serv == 'primary') {
            vmMod.obj.primary = !0
        } else {
            vmMod.obj.delivery = !0;
            vmMod.obj.site = !0
        }
    }
    if (vmMod.obj.app == 'contract') {
        vmMod.obj.billing = !0
    }
    if (vmMod.obj.app == 'installation') {
        vmMod.obj.delivery = !1;
        vmMod.obj.site = !0
    }
    if (vmMod.obj.app == 'customers') {
        vmMod.obj.delivery = !0;
        if (vmMod.obj.app_serv == 'primary') {
            vmMod.obj.primary = !0;
            vmMod.obj.delivery = !1
        } else {
            vmMod.obj.delivery = !0
        }
    }
    vmMod.cfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 1
    };
    vmMod.ok = ok;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;

    function ok() {
        helper.doRequest('post', 'index.php', vmMod.obj, function(d) {
            if (d.success) {
                $uibModalInstance.close(d)
            } else {
                vmMod.showAlerts(d)
            }
        })
    };

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    };

    function showAlerts(d) {
        helper.showAlerts(vmMod, d)
    }
    vmMod.openManageAddressesModal = function() {
        editItem.openModal(vmMod, 'ManageAddresses', vmMod.obj.customer_id, 'lg')
    }
}]).controller('AddCustomerModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'selectizeFc', 'data', '$timeout', 'modalFc', function($scope, helper, $uibModalInstance, selectizeFc, data, $timeout,modalFc) {
    var vmMod = this,
        typePromise;
    vmMod.obj = data;
    var old_vmMod = {};
    if (data.buyer_data) {
        vmMod.obj.btw_nr = data.buyer_data.btw_nr;
        vmMod.obj.name = data.buyer_data.name;
        vmMod.obj.address = data.buyer_data.address;
        vmMod.obj.zip = data.buyer_data.zip;
        vmMod.obj.city = data.buyer_data.city;
        vmMod.obj.country_id = data.buyer_data.country_id
    }
    vmMod.checking = !1;
    vmMod.isOk = !1;
    vmMod.isFalse = !1;
    vmMod.isTrends = !1;
    vmMod.ok = ok;
    vmMod.cancel = cancel;
    vmMod.checkVat = checkVat;
    vmMod.renderPage = renderPage;
    vmMod.reset = reset;
    vmMod.parseData = parseData;
    vmMod.showAlerts = showAlerts;
    vmMod.activeteTab = activeteTab;
    vmMod.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    vmMod.format = 'dd/MM/yyyy';
    vmMod.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    if (!vmMod.form_type) {
        vmMod.form_type = 'add_customer'
    }
    vmMod.obj.related_fields = {};
    var related_for = 'null';
    if (vmMod.obj.add_customer) {
        related_for = 'add_customer'
    } else if (vmMod.obj.add_contact) {
        related_for = 'add_contact'
    }
    if (data.app == 'maintenance' && vmMod.obj.add_customer) {
        vmMod.form_type = 'add_individual';
        //related_for = 'add_individual';
        vmMod.obj.add_individual = !0;
        vmMod.obj.add_customer = !1
    }
    var related_fields_data = {
        'do': 'misc--misc-get_related_fields',
        related_for: related_for
    };   
    if(vmMod.obj.add_customer || vmMod.obj.add_individual){
        helper.doRequest('get', 'index.php', {'do': 'misc--misc-get_customer_other_fields',related_for: related_for} , function(r) {
            angular.element('.modal-dialog').addClass('modal-lg');
            for(var x in r.data){
                vmMod.obj[x]=r.data[x];
            }
        });
    }else{
        helper.doRequest('get', 'index.php', related_fields_data, function(r) {
            vmMod.obj.related_fields = r.data.related_fields
        });
    }

    if(vmMod.obj.add_contact && vmMod.obj.buyer_id){
        for(let x in vmMod.obj.customer_dd){
            if(vmMod.obj.customer_dd[x].id == vmMod.obj.buyer_id){
                vmMod.tmp_customer_addresses=vmMod.obj.customer_dd[x].addresses;
                break;
            }
        }
    }

    vmMod.customerTypeCfg = selectizeFc.configure({
        labelField: 'name',
        placeholder: helper.getLanguage('Select'),
        searchField: ['name'],
        maxOptions: 50,
        maxItems: 50,
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value;
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit"
            }
            helper.doRequest('post', 'index.php', d, function(o) {
                vmMod[realModel][(vmMod[realModel].length - 1)].id = o.data.inserted_id;
                vmMod.obj[realModel][(vmMod.obj[realModel].length - 1)] = o.data.inserted_id
            })
        }
    }, !1, !0, vmMod);
    vmMod.salesCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select'),
        maxOptions: 200,
        maxItems: 1,
        onDropdownOpen($dropdown) {
            var _this = this,
                model = _this.$input[0].attributes.model.value;
            if (vmMod.obj[model] != undefined && vmMod.obj[model] != '0') {
                old_vmMod[model] = vmMod.obj[model]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            helper.doRequest('post', 'index.php', d, function(o) {
                vmMod[realModel] = o.data.lines;
                vmMod.obj[realModel] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            if (vmMod.obj[model] != undefined && vmMod.obj[model] != '0') {
                old_vmMod[model] = vmMod.obj[model]
            }
            if (item == '0') {
                openModal('selectEdit', {
                    table: table,
                    model: model
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                vmMod.obj[model] = undefined;
                return !1
            }
        }
    }, !1, !0, vmMod);
    vmMod.extraCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select'),
        maxOptions: 50,
        maxItems: 1,
        onDropdownOpen($dropdown) {
            var _this = this,
                extra_id = _this.$input[0].attributes.extra.value;
            if (vmMod.obj.extra[extra_id] != undefined && vmMod.obj.extra[extra_id] != '0') {
                old_vmMod['extra_' + extra_id] = vmMod.obj.extra[extra_id]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                vmMod[realModel][extra_id] = o.data.lines;
                vmMod.obj[realModel][extra_id] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = _this.$input[0].attributes.extra.value;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            var extra_id = _this.$input[0].attributes.extra.value;
            if (vmMod.obj.extra[extra_id] != undefined && vmMod.obj.extra[extra_id] != '0') {
                old_vmMod['extra_' + extra_id] = vmMod.obj.extra[extra_id]
            }
            if (item == '0') {
                openModal('selectEdit', {
                    table: table,
                    model: model,
                    extra_id: extra_id
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                vmMod.obj.extra[extra_id] = undefined;
                return !1
            }
        }
    }, !1, !0, vmMod);

    vmMod.addressCfg = {
        valueField: 'address_id',
        labelField: 'address',
        searchField: ['address'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<strong class="text-ellipsis">' + item.symbol + '  ' + escape(item.top) + '</strong>' + '<small class="text-ellipsis"><span class="glyphicon glyphicon-map-marker"></span>' + escape(item.bottom) + ' - ' + escape(item.right) + '&nbsp;</small>' + '</div>' + '</div>' + '</div>';
                if (item.address_id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Create a new location') + '</span></div>'
                }
                return html;
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.top) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined || value == '') {
                vmMod.obj.old_address_id = value
            } else if (value == '99999999999') {
                openModal('CustomerAddress', {}, 'md')
            } else {
                vmMod.obj.old_address_id = value
            }
        },
        maxItems: 1
    };

    vmMod.compCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select Account'),
        maxOptions: 51,
        onType(str) {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'customers-Contact',
                    'xget': 'customers',
                    'term': str
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    vmMod.obj.customer_dd = r.data;
                    if (vmMod.obj.customer_dd.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value){
            vmMod.tmp_customer_addresses=[];
            if(value){
                angular.element('.modal-dialog').removeClass('modal-md').addClass('modal-lg');
                vmMod.tmp_customer_addresses=this.options[value].addresses;
            }else{
                angular.element('.modal-dialog').removeClass('modal-lg').addClass('modal-md');
                let selectize_dropdown=angular.element('#custandcont').closest('div').find('.selectize-dropdown');
                if(selectize_dropdown){
                    $timeout(function() {
                        selectize_dropdown.css('width','100%');
                    });                   
                }
            }
        },
        maxItems: 1
    });

    function ok(formObj) {
        var obj = angular.copy(vmMod.obj);
        if (vmMod.obj.is_zen) {
            obj.add_contact = !0
        }
        if (vmMod.obj.do_next && !vmMod.obj.do) {
            obj.do = vmMod.obj.do_next
        }
        angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(d) {
            angular.element('.loading_alt').remove();
            if (d.success) {
                $uibModalInstance.close(d)
            } else {
                vmMod.showAlerts(d, formObj)
            }
        })
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    };

    function checkVat() {
        vmMod.reset();
        vmMod.checking = !0;
        vmMod.showViesUnavailable=false;
        if (!vmMod.obj.btw_nr) {
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            vmMod.checking = !1;
            return
        }
        if (typePromise) {
            $timeout.cancel(typePromise)
        }
        typePromise = $timeout(function() {
            if (vmMod.obj.btw_nr) {
                var data = {
                    'do': 'misc--misc-check_vies_vat_number',
                    value: vmMod.obj.btw_nr
                };
                helper.doRequest('post', 'index.php', data, vmMod.renderPage)
            } else {
                vmMod.reset()
            }
        }, 300)
    }

    function renderPage(d) {
        vmMod.reset();
        if ((d.error || d.notice) && vmMod.obj.btw_nr) {
            vmMod.isFalse = !0;
            if(d.notice){
                vmMod.showViesUnavailable=true;
            }
        }
        if (d.success && vmMod.obj.btw_nr) {
            if (d.data.trends_ok) {
                vmMod.isTrends = !0
            } else {
                vmMod.isOk = !0
            }
        }
        vmMod.parseData(d)
    }

    function reset() {
        vmMod.checking = !1;
        vmMod.isOk = !1;
        vmMod.isFalse = !1;
        vmMod.isTrends = !1
    }

    function parseData(d) {
        vmMod.obj.name = d.data.comp_name;
        vmMod.obj.address = d.data.comp_address;
        vmMod.obj.city = d.data.comp_city;
        vmMod.obj.zip = d.data.comp_zip;
        vmMod.obj.country_id = d.data.comp_country ? d.data.comp_country : 0;
        vmMod.obj.customer_legal_type_id=d.data.customer_legal_type_id ? d.data.customer_legal_type_id : 0;
        vmMod.obj.legal_type=d.data.customer_legal_type_id ? d.data.customer_legal_type_id : 0;
    }

    function showAlerts(d, formObj) {
        helper.showAlerts(vmMod, d, formObj)
    }

    function activeteTab(i) {
        vmMod.form_type = i;
        vmMod.obj.add_customer = !1;
        vmMod.obj.add_individual = !1;
        vmMod.obj.add_contact = !1;
        vmMod.obj[i] = !0
    }

    function openModal(type, item, size, obj, extra_id) {
        if (type == 'CustomerAddress') {
            var params = {
                template: 'cTemplate/' + type + 'Modal',
                ctrl: type + 'ModalCtrl',
                size: size,
                backdrop: 'static'
            }
        } else {
            var params = {
                template: 'miscTemplate/' + type + 'Modal',
                ctrl: type + 'ModalCtrl',
                size: size,
                backdrop: 'static'
            }
        }
        switch (type) {
            case 'selectEdit':
                params.params = {
                    'do': 'misc-selectEdit',
                    'table': item.table,
                    'extra_id': item.extra_id
                };
                params.callback = function(data) {
                    if (data.data) {
                        if (item.extra_id) {
                            vmMod.obj.extra_data[item.extra_id] = data.data.lines;
                            if (old_vmMod['extra_' + item.extra_id]) {
                                vmMod.obj.extra[item.extra_id] = old_vmMod['extra_' + item.extra_id]
                            }
                        } else {
                            vmMod.obj[item.table] = data.data.lines;
                            if (old_vmMod[item.model]) {
                                vmMod.obj[item.model] = old_vmMod[item.model]
                            }
                        }
                    } else {
                        if (item.extra_id) {
                            vmMod.obj.extra_data[item.extra_id] = data.lines;
                            if (old_vmMod['extra_' + item.extra_id]) {
                                vmMod.obj.extra[item.extra_id] = old_vmMod['extra_' + item.extra_id]
                            }
                        } else {
                            vmMod.obj[item.table] = data.lines;
                            if (old_vmMod[item.model]) {
                                vmMod.obj[item.model] = old_vmMod[item.model]
                            }
                        }
                    }
                }
                break
            case 'CustomerAddress':
                params.params = {
                    'do': 'customers-ViewCustomer',
                    'address_id': 'tmp',
                    'customer_id': vmMod.obj.buyer_id,
                    'xget': 'customerAddress'
                };
                params.callback = function(data) {
                    var data = {
                        'do': 'customers-Contact',
                        'customer_id': vmMod.obj.buyer_id,
                        'from_contact': !0,
                        'xget': 'customers'
                    };
                    helper.doRequest('get', 'index.php', data, function(r) {
                        vmMod.tmp_customer_addresses = r.data[0].addresses
                        vmMod.obj.accountRelatedObj[0].model_value = r.data[0].model_value;
                    })
                }
                break
        }
        modalFc.open(params)
    }

}]).controller('viewRecepitCtrl', ['$scope', '$sce', 'helper', '$uibModalInstance', 'data', function($scope, $sce, helper, $uibModalInstance, data) {
    var vmMod = this;
    vmMod.title = data.e_title ? data.e_title : '';
    vmMod.to_trusted = function(someHTML) {
        return $sce.trustAsHtml(someHTML)
    }
    vmMod.text = data.e_message;
    vmMod.ok = function() {
        $uibModalInstance.close()
    };
    vmMod.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('LinktoAccountCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'modalFc', 'selectizeFc', 'data', function($scope, $timeout, helper, $uibModalInstance, modalFc, selectizeFc, data)  {

    var vmMod = this;
    vmMod.data = data;
    vmMod.newCustomer = {};
    vmMod.data.obj.accounts = data.accounts;
    vmMod.funcCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: 'Select function',
        onChange(value){
            var name = vmMod.title_dd.filter(function(item) {
                return item.id === value;
              });
            vmMod.newCustomer.position_txt = name[0].name;
        },
    });
    vmMod.funcDfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: 'Select Department',
    });
    vmMod.addressCfg = selectizeFc.configure({
        labelField: 'top',
        searchField: ['top'],
        placeholder: helper.getLanguage('Select Address'),
    });
    vmMod.custCofg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select Account'),
        maxOptions: 50,
        onType(str) {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.close();
            typePromise = $timeout(function() {
                var data = {
                    'do': 'customers-contact',
                    'xget': 'customers',
                    'search': str
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    vmMod.data.customers_dd = r.data;
                    if (vmMod.data.customers_dd) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value){
            vmMod.data.customer_id = value;
            var customer = vmMod.data.customers_dd.filter(function(item) {
                return item.id === value;
              });
            vmMod.newCustomer.name = customer[0].name;
            vmMod.newCustomer.type = customer[0].type;
            vmMod.newCustomer.type_name = customer[0].type_name;
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'customers-deal_edit',
                    'xget': 'addresses',
                    'buyer_id': value,
                    'contact_id': vmMod.data.contact_id
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    vmMod.data.addresses = r.data;
                })
            }, 300)
        },
        maxItems: 1
    });
    vmMod.salesCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: !1,
        onDropdownOpen($dropdown) {
            var _this = this,
                model = _this.$input[0].attributes.model.value;
            if (model == 'position' || model == 'department') {
                if (vmMod.data[model] != undefined && vmMod.data[model] != '0') {
                    old_ls[model] = vmMod.data[model]
                }
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined,
                index = _this.$input[0].attributes.ind.value;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
           //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                vmMod[realModel] = o.data.lines;
                vmMod.accounts[index][realModel] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            var ind = undefined;
            if (model == 'position' || model == 'department') {
                if (vmMod.data[model] != undefined && vmMod.data[model] != '0') {
                    old_ls[model] = vmMod.data[model]
                }
            }
            if (item == '0') {
                vmMod.openModal('selectEdit', {
                    table: table,
                    model: model
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                if (ind != undefined) {
                    vmMod.accounts[ind][model] = undefined
                } else {
                    vmMod.data[model] = undefined
                }
                return !1
            }
        }
    }, !1, !0, vmMod);

    vmMod.openModal = function(type, item, size, obj, extra_id) {
        if (type == 'selectEdit') {
            var params = {
                template: 'miscTemplate/' + type + 'Modal',
                ctrl: type + 'ModalCtrl',
                size: size,
            }
        }
        switch (type) {
            case 'selectEdit':
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-selectEdit',
                    'table': item.table,
                    'extra_id': item.extra_id
                };
                params.callback = function(data) {
                    if (data.data) {
                        if (item.extra_id) {
                            vmMod.data.extra[item.extra_id] = data.data.lines;
                            if (old_ls['extra_' + item.extra_id]) {
                                vmMod.data.obj.extra[item.extra_id] = old_ls['extra_' + item.extra_id]
                            }
                        } else {
                            vmMod.data[item.table] = data.data.lines;
                            if (item.ind) {
                                if (old_ls[item.model + '_' + item.ind]) {
                                    vmMod.data.accounts[item.ind][item.model] = old_ls[item.model + '_' + item.ind]
                                }
                            }
                        }
                    } else {
                        if (item.extra_id) {
                            vmMod.data.extra[item.extra_id] = data.lines;
                            if (old_ls['extra_' + item.extra_id]) {
                                vmMod.data.obj.extra[item.extra_id] = old_ls['extra_' + item.extra_id]
                            }
                        } else {
                            vmMod.data[item.table] = data.lines;
                            if (item.ind) {
                                if (old_ls[item.model + '_' + item.ind]) {
                                    vmMod.data.accounts[item.ind][item.model] = old_ls[item.model + '_' + item.ind]
                                }
                            } else {
                                if (old_ls[item.model]) {
                                    vmMod.data.obj[item.model] = old_ls[item.model]
                                }
                            }
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }
    vmMod.ok = function() {
        vmMod.newCustomer.contact_id = vmMod.data.contact_id;
        vmMod.data.update_contact_customer = true;
        vmMod.newCustomer.primary = 0;
        vmMod.data.accounts.push(vmMod.newCustomer);
        vmMod.data.do = "customers-Contact-customers-addContactToCustomer";
        helper.doRequest('post', 'index.php', vmMod.data, function(d) {
            if (d.success) {
                $uibModalInstance.close(d)
            }
        })
        vmMod.newCustomer = {};
    };
    vmMod.cancel = function() {
        vmMod.newCustomer = {};
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('EditInfoCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'modalFc', 'selectizeFc', 'data', function($scope, $timeout, helper, $uibModalInstance, modalFc, selectizeFc, data)  {

    var vmMod = this;
    var old_ls = {};
    vmMod.data = data.obj;
    vmMod.lang_dd = data.lang_dd;
    vmMod.customer_contact_title = data.customer_contact_title;
    vmMod.extra = data.extra;
    vmMod.default_show = data.default_show;
    vmMod.title = data.e_title ? data.e_title : '';
    vmMod.to_trusted = function(someHTML) {
        return $sce.trustAsHtml(someHTML)
    }

    vmMod.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select gender'),
    });
    vmMod.cfgLng = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select language'),
    });
    vmMod.funcCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        placeholder: 'Select Salutation',
    });
    vmMod.extraCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        placeholder: 'Select value',
    });
    vmMod.salesCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: !1,
        onDropdownOpen($dropdown) {
            var _this = this,
                model = _this.$input[0].attributes.model.value;
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined,
                index = _this.$input[0].attributes.ind.value;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
           //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                vmMod[realModel] = o.data.lines;
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            var ind = undefined;
            if (model == 'position' || model == 'department') {
                if (vmMod.data[model] != undefined && vmMod.data[model] != '0') {
                    old_ls[model] = vmMod.data[model]
                }
            }
            if (item == '0') {
                vmMod.openModal('selectEdit', {
                    table: table,
                    model: model
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                if (ind != undefined) {
                    vmMod.accounts[ind][model] = undefined
                } else {
                    vmMod.data[model] = undefined
                }
                return !1
            }
        }
    }, !1, !0, vmMod);


    vmMod.extraCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 1000,
        maxItems: 1,
        create: !1,
        onDropdownOpen($dropdown) {
            var _this = this,
                extra_id = _this.$input[0].attributes.extra.value;
            if (vmMod.extra[extra_id] != undefined && vmMod.extra[extra_id] != '0') {
                old_ls['extra_' + extra_id] = vmMod.extra[extra_id]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                vmMod[realModel][extra_id] = o.data.lines;
                vmMod.data[realModel][extra_id] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = _this.$input[0].attributes.extra.value;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            if (vmMod.extra[extra_id] != undefined && vmMod.extra[extra_id] != '0') {
                old_ls['extra_' + extra_id] = vmMod.extra[extra_id]
            }
            if (item == '0') {
                vmMod.openModalExtra('selectEdit', {
                    table: table,
                    model: model,
                    extra_id: extra_id
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                vmMod.extra[extra_id] = undefined;
                return !1
            }
        }
    }, !1, !0, vmMod);

    vmMod.openModalExtra = function(type, item, size, obj, extra_id) {
        if (type == 'selectEdit') {
            var params = {
                template: 'miscTemplate/' + type + 'Modal',
                ctrl: type + 'ModalCtrl',
                size: size
            }
        }
        switch (type) {
            case 'selectEdit':
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-selectEdit',
                    'table': item.table,
                    'extra_id': item.extra_id
                };
                params.callback = function(data) {
                    if (data.data) {
                        if (item.extra_id) {
                            vmMod.extra[item.extra_id] = data.data.lines;
                            if (old_ls['extra_' + item.extra_id]) {
                                vmMod.data.extra[item.extra_id] = old_ls['extra_' + item.extra_id]
                            }
                        } else {
                            vmMod[item.table] = data.data.lines;
                            if (item.ind) {
                                if (old_ls[item.model + '_' + item.ind]) {
                                    vmMod.data.accounts[item.ind][item.model] = old_ls[item.model + '_' + item.ind]
                                }
                            } else {
                                if (old_ls[item.model]) {
                                    vmMod.data[item.model] = old_ls[item.model]
                                }
                            }
                        }
                    } else {
                        if (item.extra_id) {
                            vmMod.extra[item.extra_id] = data.lines;
                            if (old_ls['extra_' + item.extra_id]) {
                                vmMod.data.extra[item.extra_id] = old_ls['extra_' + item.extra_id]
                            }
                        } else {
                            ls[item.table] = data.lines;
                            if (item.ind) {
                                if (old_ls[item.model + '_' + item.ind]) {
                                    vmMod.data.accounts[item.ind][item.model] = old_ls[item.model + '_' + item.ind]
                                }
                            } else {
                                if (old_ls[item.model]) {
                                    vmMod.data[item.model] = old_ls[item.model]
                                }
                            }
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }

    vmMod.openModal = function(type, item, size, obj, extra_id) {
        if (type == 'selectEdit') {
            var params = {
                template: 'miscTemplate/' + type + 'Modal',
                ctrl: type + 'ModalCtrl',
                size: size,
            }
        }
        switch (type) {
            case 'selectEdit':
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-selectEdit',
                    'table': item.table,
                    'extra_id': item.extra_id
                };
                params.callback = function(data) {
                    vmMod[item.table] = data.data.lines;
                }
                break
        }
        modalFc.open(params)
    }

    vmMod.ok = function() {
        angular.forEach(vmMod.data.extra_ids, function(value, key) {
            vmMod.data.extra[key] = value;
        });
        vmMod.data.do = "customers-Contact-customers-contact_update";
        helper.doRequest('post', 'index.php', vmMod.data, function(d) {

                })
        $uibModalInstance.close()
    };
    vmMod.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('ContactInfoCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'modalFc', 'selectizeFc', 'data', function($scope, $timeout, helper, $uibModalInstance, modalFc, selectizeFc, data)  {

    var vmMod = this;
    vmMod.data = data;
    vmMod.data.accounts = JSON.parse(vmMod.data.accounts);
    vmMod.data.address = vmMod.data.addresses.filter(function(item) {
        return item.address_id === vmMod.data.customer_address_id;
      });
    vmMod.data.title_name = vmMod.data.customer_contact_title.filter(function(item) {
        return item.id === vmMod.data.title;
    });
    
    vmMod.openModal = function(type, item, size) {
        if (type == 'ContactEditInfo') {
            var params = {
                template: 'miscTemplate/' + type + 'Modal',
                ctrl: type + 'Ctrl',
                size: size,
                item: item
            }
            params.callback = function(data) {
                var newData = {};
                var newAddress = {};
                newData = data.data.accounts.filter(function(item) {
                    return item.customer_id === vmMod.data.customer_id;
                });
                newAddress = JSON.stringify(data.data.accounts);
                vmMod.data = newData[0];
                vmMod.data.customer_contact_title = data.data.customer_contact_title;
                vmMod.data.customer_contact_job_title=data.data.customer_contact_job_title;
                vmMod.data.customer_contact_dep=data.data.customer_contact_dep;
                vmMod.data.accounts = JSON.parse(newAddress);
                vmMod.data.address = vmMod.data.addresses.filter(function(item) {
                    return item.address_id === vmMod.data.customer_address_id;
                  });
                vmMod.data.title_name = vmMod.data.customer_contact_title.filter(function(item) {
                    return item.id === vmMod.data.title;
                });
            }
        }
        modalFc.open(params);
    }

    vmMod.cancel = function() {
        $uibModalInstance.close();
    }

}]).controller('ContactEditInfoCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'modalFc', 'selectizeFc', 'data', function($scope, $timeout, helper, $uibModalInstance, modalFc, selectizeFc, data)  {

    var vmMod = this;
    vmMod.data = data;

    vmMod.addressCfg = selectizeFc.configure({
        labelField: 'top',
        searchField: ['top'],
        placeholder: 'Select Address',
    });

    vmMod.salesCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: !1,
        onDropdownOpen($dropdown) {
            var _this = this,
                model = _this.$input[0].attributes.model.value;
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined,
                index = _this.$input[0].attributes.ind.value;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
           //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                vmMod[realModel] = o.data.lines;
                vmMod.accounts[index][realModel] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            var ind = undefined;
            if (item == '0') {
                vmMod.openModal('selectEdit', {
                    table: table,
                    model: model
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                if (ind != undefined) {
                    vmMod.accounts[ind][model] = undefined
                } else {
                    vmMod.data[model] = undefined
                }
                return !1
            }
        }
    }, !1, !0, vmMod);

    vmMod.openModal = function(type, item, size, obj, extra_id) {
        if (type == 'selectEdit') {
            var params = {
                template: 'miscTemplate/' + type + 'Modal',
                ctrl: type + 'ModalCtrl',
                size: size,
            }
        }
        switch (type) {
            case 'selectEdit':
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-selectEdit',
                    'table': item.table,
                    'extra_id': item.extra_id
                };
                params.callback = function(data) {
                    if (data.data) {
                        if (item.extra_id) {
                            vmMod.data.extra[item.extra_id] = data.data.lines;
                            if (old_ls['extra_' + item.extra_id]) {
                                vmMod.data.obj.extra[item.extra_id] = old_ls['extra_' + item.extra_id]
                            }
                        } else {
                            vmMod.data[item.table] = data.data.lines;
                            if (item.ind) {
                                if (old_ls[item.model + '_' + item.ind]) {
                                    vmMod.data.accounts[item.ind][item.model] = old_ls[item.model + '_' + item.ind]
                                }
                            }
                        }
                    } else {
                        if (item.extra_id) {
                            vmMod.data.extra[item.extra_id] = data.lines;
                            if (old_ls['extra_' + item.extra_id]) {
                                vmMod.data.obj.extra[item.extra_id] = old_ls['extra_' + item.extra_id]
                            }
                        } else {
                            vmMod.data[item.table] = data.lines;
                            if (item.ind) {
                                if (old_ls[item.model + '_' + item.ind]) {
                                    vmMod.data.accounts[item.ind][item.model] = old_ls[item.model + '_' + item.ind]
                                }
                            } 
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }
    vmMod.ok = function(contact) {
        vmMod.data.do = "customers-Contact-customers-updateContactAccounts";
        vmMod.data.accounts = vmMod.data.accounts.filter(function(item) {
            if(item.customer_id === vmMod.data.customer_id)
            {
                item.phone = vmMod.data.phone;
                item.email = vmMod.data.email;
                item.fax = vmMod.data.fax;
                item.customer_address_id = vmMod.data.customer_address_id;
                item.title = vmMod.data.title;
                item.department = vmMod.data.department;
                item.position = vmMod.data.position;
                item.s_email = vmMod.data.s_email;
            }
            return vmMod.data.accounts;
        });
        helper.doRequest('post', 'index.php', vmMod.data, function(d) {
                    if (d) {
                        $uibModalInstance.close(d)
                    }
        })
    };
    vmMod.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('EditPersonalInfoCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'modalFc', 'selectizeFc', 'data', function($scope, $timeout, helper, $uibModalInstance, modalFc, selectizeFc, data)  {

    var vmMod = this;
    vmMod.data = data.obj;
    vmMod.lang_dd = data.lang_dd;
    vmMod.title_dd = data.customer_contact_title;
    vmMod.extra = data.extra;
    vmMod.default_show = data.default_show;

    vmMod.title = data.e_title ? data.e_title : '';
    vmMod.to_trusted = function(someHTML) {
        return $sce.trustAsHtml(someHTML)
    }

    vmMod.text = data.e_message;
    vmMod.ok = function(contact) {
        vmMod.data.do = "customers-Contact-customers-contact_update";
        helper.doRequest('post', 'index.php', vmMod.data, function(d) {
                })
        $uibModalInstance.close()
    };
    vmMod.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('EditAvatarInfoCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'modalFc', 'selectizeFc', 'data',  'Upload', function($scope, $timeout, helper, $uibModalInstance, modalFc, selectizeFc, data, Upload)  {

    var vmMod = this;
    vmMod.data = data.obj;
    vmMod.lang_dd = data.lang_dd;
    vmMod.title_dd = data.customer_contact_title;
    vmMod.extra = data.extra;
    vmMod.default_show = data.default_show;

    vmMod.title = data.e_title ? data.e_title : '';
    vmMod.to_trusted = function(someHTML) {
        return $sce.trustAsHtml(someHTML)
    }

    vmMod.uploadFiles = function(files) {
        if (files && files.length) {
            Upload.upload({
                url: 'index.php',
                data: {
                    file: files,
                    'do': 'customers-Contact-customers-upload_photo',
                    'contact_id': vmMod.data.contact_id
                }
            }).then(function(r) {
                vmMod.data.exist_image = !0;
                vmMod.view_upload = !1;
                vmMod.data.artPhoto = r.data.name
            }, function() {})
        }
    }
    vmMod.remove_photo = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        var data = {
            'do': 'customers-Contact-customers-remove_photo',
            'contact_id': vmMod.data.contact_id
        };
        helper.doRequest('get', 'index.php', data, function(o) {
            vmMod.data.artPhoto =  vmMod.data.default_image;
            vmMod.data.exist_image=false;
        })
    };

    vmMod.text = data.e_message;
    vmMod.ok = function(contact) {
        vmMod.data.do = "customers-Contact-customers-contact_update";
        helper.doRequest('post', 'index.php', vmMod.data, function(d) {
                })
        $uibModalInstance.close()
    };
    vmMod.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('labelCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'obj', 'modalFc', function($scope, $timeout, helper, $uibModalInstance, obj, modalFc) {
    var vm = this;
    vm.label = {};
    vm.do_next = '';
    vm.cancel = cancel;
    vm.showAlerts = showAlerts;
    vm.ok = ok;

    function ok() {
        var d = angular.copy(vm.label);
        d.do = vm.do_next;
        helper.doRequest('post', 'index.php', d, function(r) {
            vm.showAlerts(r)
        })
    };

    function showAlerts(d) {
        helper.showAlerts(vm, d)
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    };
    helper.doRequest('get', 'index.php', obj, function(r) {
        vm.label = r.data.labels;
        vm.do_next = r.data.do_next;
        modalFc.setHeight(angular.element('.modal.in'))
    })
}]).controller('AddAnArticleModalCtrl', ['$scope', '$uibModalInstance', 'data', 'helper', '$timeout', '$uibModal', 'selectizeFc', 'modalFc', 'editItem',function($scope, $uibModalInstance, data, helper, $timeout, $uibModal, selectizeFc, modalFc, editItem) {
    var vmMod = this,
        typePromise;
    vmMod.item = data;
    vmMod.tmpl = 'otemplate';
    vmMod.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    var old_vmMod = {};
    vmMod.setPrice = function(t) {
        var vat = 0
        for (x in add.item.vat_dd) {
            if (add.item.vat_dd[x].id == add.item.vat_id) {
                vat = helper.return_value(add.item.vat_dd[x].name.split(' ')[0])
            }
        }
        switch (t) {
            case 'price':
            case 'vat_id':
                add.item.total_price = helper.displayNr(parseFloat(helper.return_value(add.item.price)) + (parseFloat(helper.return_value(add.item.price)) * parseFloat(vat) / 100));
                break;
            case 'total_price':
                add.item.price = helper.displayNr(parseFloat(helper.return_value(add.item.total_price)) * 100 / (100 + parseFloat(vat)));
                break
        }
    }
    vmMod.salesCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: vmMod.item.is_admin,
        onDropdownOpen($dropdown) {
            var _this = this,
                model = _this.$input[0].attributes.model.value;
            if (vmMod.item[model] != undefined && vmMod.item[model] != '0') {
                old_vmMod[model] = vmMod.item[model]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                vmMod.item[realModel] = o.data.lines;
                vmMod.item[realModel + '_id'] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            if (vmMod.item[model] != undefined && vmMod.item[model] != '0') {
                old_vmMod[model] = vmMod.item[model]
            }
            if (item == '0') {
                vmMod.openModal('selectEdit', table, {
                    'do': 'misc-selectEdit'
                }, 'md');
                vmMod.item[model] = undefined;
                return !1
            }
        }
    }, !1, !0, vmMod.item);
    vmMod.customerAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select supplier'),
        maxOptions: 6,
        onType(str) {
            var selectize = angular.element('#inputSupplier')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'misc-accounts',
                    term: str,
                    supplier: 1,
                    noAdd: !0
                };
                vmMod.requestAuto(data, vmMod.renderCustomerAuto)
            }, 300)
        },
        onChange(value){
            if (value == '99999999999') {
                var obj = {
                    country_dd: [],
                    country_id: undefined,
                    add_customer: !0,
                    article: undefined,
                    'do': 'misc--misc-tryAddC',
                    identity_id: undefined,
                    item_id: undefined,
                    after_save:"update_data"
                }
                editItem.openModal(vmMod, 'AddCustomer', obj, 'md');
            }            
        },
        onItemAdd(value, $item) {
            if (value == undefined) {
                vmMod.item.supplier_name = ''
            } else if (value == '99999999999') {
                vmMod.item.supplier_name = '';   
                vmMod.item.supplier_id = '';             
            } else {
                vmMod.item.supplier_name = this.options[value].value;
            }
        },
        maxItems: 1
    }, helper.getLanguage('Create a new supplier'));

    vmMod.updateData=function(buyer_id){
        var data = {
            'do': 'misc-accounts',
            term: undefined,
            supplier: 1,
            noAdd: !0,
            buyer_id:buyer_id
        };
        vmMod.item.supplier_id=buyer_id;
        vmMod.requestAuto(data, vmMod.renderCustomerAuto)
    }
    if (vmMod.item.service_checked) {
        vmMod.form_type = 'service'
    }
    if (!vmMod.form_type) {
        vmMod.form_type = 'article'
    }
    vmMod.ok = ok;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;
    vmMod.setPrice = setPrice;
    vmMod.requestAuto = requestAuto;
    vmMod.renderCustomerAuto = renderCustomerAuto;
    vmMod.openModal = openModal;

    function ok(formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(vmMod.item);
        data.do = vmMod.item.do_next;
        if (formObj.$name == 'addAnService') {
            data.do = vmMod.item.do_next_service
        }
        helper.doRequest('post', 'index.php', data, function(d) {
            if (d.success) {
                $uibModalInstance.close(d)
            } else {
                vmMod.showAlerts(d, formObj)
            }
        })
    };

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    };

    function showAlerts(d, formObj) {
        helper.showAlerts(vmMod, d, formObj)
    }

    function setPrice(t) {
        var vat = 0
        for (x in vmMod.item.vat_dd) {
            if (vmMod.item.vat_dd[x].id == vmMod.item.vat_id) {
                vat = helper.return_value(vmMod.item.vat_dd[x].name.split(' ')[0])
            }
        }
        switch (t) {
            case 'price':
            case 'vat_id':
                vmMod.item.total_price = helper.displayNr(parseFloat(helper.return_value(vmMod.item.price)) + (parseFloat(helper.return_value(vmMod.item.price)) * parseFloat(vat) / 100));
                break;
            case 'total_price':
                vmMod.item.price = helper.displayNr(parseFloat(helper.return_value(vmMod.item.total_price)) * 100 / (100 + parseFloat(vat)));
                break
        }
    }

    function requestAuto(data, renderer) {
        helper.doRequest('get', 'index.php', data, renderer)
    }

    function renderCustomerAuto(d) {
        if (d.data) {
            vmMod.item.supplier = d.data;
            var selectize = angular.element('#inputSupplier')[0].selectize;
            selectize.open()
        }
    }

    function openModal(type, item, size) {
        var params = {
            template: vmMod.tmpl + '/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'selectEdit':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.params = {
                    'do': 'misc-' + type,
                    'table': item
                };
                params.callback = function(data) {
                    if (data) {
                        switch (data.data.table) {
                            case 'pim_article_categories':
                                vmMod.item.article_category = data.data.lines;
                                break;
                            case 'pim_article_ledger_accounts':
                                vmMod.item.ledger_account = data.data.lines;
                                break;
                            case 'pim_article_brands':
                                vmMod.item.article_brand = data.data.lines;
                                break
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }
}]).controller('MyAccountCtrl', ['$scope', '$state', '$compile', 'data', '$timeout', 'helper', 'modalFc', 'UserService', '$rootScope', 'selectizeFc', function($scope, $state, $compile, data, $timeout, helper, modalFc, UserService, $rootScope, selectizeFc) {
    var ls = this,
        typePromise;
    ls.inputType = 'password';
    ls.user = {};
    for (x in data) {
        ls.user[x] = data[x]
    }
    ls.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    ls.user.notification_email_sent = !1;
    ls.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        width: 850,
        height: 300,
        menubar: !1,
        relative_urls: !1,
        selector: 'textarea',
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        // toolbar: ["insertfile undo redo | styleselect | bold italic strikethrough underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image", "forecolor backcolor | formatselect | fontsizeselect |  table tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | code"],
        toolbar: ["insertfile fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image table | code"],
        image_advtab: !0,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        })
                    };
                    reader.readAsDataURL(file)
                })
            }
        },
    };
    ls.hideShowPassword = function(tip) {
        if (tip == 'password')
            ls.inputType = 'text';
        else ls.inputType = 'password'
    };
    ls.genPass = function(nr) {
        var pass = helper.generatePassword(nr);
        ls.user.password = pass;
        ls.user.password1 = pass
    }
    ls.save = function(formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(ls.user);
        data.do = data.do_next;
        helper.doRequest('post', 'index.php', data, function(r) {
            helper.showAlerts(ls, r, formObj);
            if (r && r.success) {
                UserService.name = r.data.first_name + ' ' + r.data.last_name;
                ls.user.notification_email_sent = r.in.notification_email_sent;
                helper.removeCachedTemplates();
                angular.element('.page.ng-scope').find('header-dir,footer-dir,header-module').remove();
                angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                angular.element('.content').prepend('<header-module></header-module>');
                $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                $compile(angular.element('.content').find('header-module'))($rootScope);
                $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                if (r.in.language_change) {
                    angular.element('#beamerOverlay').remove();
                    angular.element('#beamerSelector').remove();
                    helper.doRequest('get', 'index.php?do=misc-lang', function(r) {
                        LANG = r.data;
                        $state.reload();
                        helper.updateTitle($state.current.name,'');
                    }, function(r) {
                        $state.reload()
                    })
                }
            }
        })
    }
    ls.closeNotificationAlert = function(index) {
        ls.user.notification_email_sent = !1
    }
    ls.update_signature = function(formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(ls.user.sign);
        data.do = data.do_next;
        helper.doRequest('post', 'index.php', data, function(r) {
            helper.showAlerts(ls, r, formObj);
            if (r && r.success) {
                for (x in r.data.sign) {
                    ls.user[x] = r.data.sign[x]
                }
            }
        })
    }
    ls.upload = function(e) {
        angular.element(e.target).parents('.upload-box').find('.newUploadFile2').click()
    }
    angular.element('.newUploadFile2').on('click', function(e) {
        e.stopPropagation()
    }).on('change', function(e) {
        e.preventDefault();
        var _this = $(this),
            p = _this.parents('.upload-box'),
            x = p.find('.newUploadFile2');
        if (x[0].files.length == 0) {
            return !1
        }
        if (x[0].files[0].size > 61440) {
            alert("Error: file is to big.");
            return !1
        }
        p.find('.img-thumbnail').attr('src', '');
        var tmppath = URL.createObjectURL(x[0].files[0]);
        p.find('.img-thumbnail').attr('src', tmppath);
        p.find('.btn').addClass('hidden');
        p.find('.nameOfTheFile').text(x[0].files[0].name);
        var formData = new FormData();
        formData.append("Filedata", x[0].files[0]);
        formData.append("do", 'misc-myaccount-user-upload');
        p.find('.upload-progress-bar').css({
            width: '0%'
        });
        p.find('.upload-file').removeClass('hidden');
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "index.php", !0);
        oReq.upload.addEventListener("progress", function(e) {
            var pc = parseInt(e.loaded / e.total * 100);
            p.find('.upload-progress-bar').css({
                width: pc + '%'
            })
        });
        oReq.onload = function(oEvent) {
            var res = JSON.parse(oEvent.target.response);
            p.find('.upload-file').addClass('hidden');
            p.find('.btn').removeClass('hidden');
            if (oReq.status == 200) {
                if (res.success) {
                    $timeout(function() {
                        for (x in res.data) {
                            ls.user[x] = res.data[x]
                        }
                    })
                } else {
                    alert(res.error)
                }
            } else {
                alert("Error " + oReq.status + " occurred when trying to upload your file.")
            }
        };
        oReq.send(formData)
    });
    ls.openModal = function(type, item, size) {
        var params = {
            template: 'miscTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        }
        switch (type) {
            case 'GoogleCalendar':
                params.item = item;
                params.callbackExit = function(data) {
                    item.outgoing = data
                }
                break;
            case 'NylasCalendar':
                params.item = item;
                params.callback = function() {
                    //angular.element('.loading_wrap').removeClass("hidden");
                    helper.doRequest('get', 'index.php?do=misc-myaccount', function(r) {
                        for (x in r.data) {
                            ls.user[x] = r.data[x]
                        }
                    })
                }
                break;
            case 'SelectAvatar':
                params.params = {
                    'do': 'misc-avatars'
                };
                params.ctrlAs = 'vm';
                params.callback = function(data) {
                    if (data && data.data) {
                        for (x in data.data) {
                            ls.user[x] = data.data[x]
                        }
                        helper.showAlerts(ls, data);
                        helper.removeCachedTemplates();
                        angular.element('.page.ng-scope').find('header-dir').remove();
                        angular.element('.page.ng-scope').prepend('<header-dir></header-dir>');
                        $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                        $state.reload()
                    }
                }
                break;
            case 'ChangePass':
                params.ctrlAs = 'vm';
                params.callback = function(d) {
                    if (d) {
                        helper.showAlerts(ls, d)
                    }
                }
                break;
            case 'EnableTfa':
                params.ctrlAs = 'vm';
                params.backdrop = 'static';
                params.item = item;
                break
        }
        modalFc.open(params)
    }
    ls.moduleAction = function(item) {
        helper.doRequest('post', 'index.php', {
            'do': 'misc-myaccount-user-setModuleAccess',
            module: item.key,
            val: item.checked
        }, function(r) {
            helper.showAlerts(ls, r);
            helper.main_menu[item.key] = item.checked;
            if (r.success) {
                helper.removeCachedTemplates();
                angular.element('.page.ng-scope').find('header-dir').remove();
                angular.element('.page.ng-scope').prepend('<header-dir></header-dir>');
                $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope)
            }
            if (helper.main_menu[item.key] == !0) {
                item.succes_add = !0
            } else {
                item.succes_add = !1
            }
        })
    }
    ls.changePass = function() {
        ls.openModal('ChangePass', '', 'md')
    }
    ls.setFactor = function() {
        var obj = {
            'do': 'misc--user-setFactor',
            'value': ls.user.two_factor
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r && r.data && r.data.qr) {
                ls.openModal("EnableTfa", r.data, 'md')
            }
        })
    }
    ls.chooseAvatar = function() {
        ls.openModal('SelectAvatar', '', 'lg')
    }
    ls.removeAvatar = function() {
        helper.doRequest('post', 'index.php', {
            'do': 'misc-myaccount-user-removeAvatar',
            'url': ls.user.avatar
        }, function(r) {
            helper.showAlerts(ls, r);
            if (r) {
                for (x in r.data) {
                    ls.user[x] = r.data[x]
                }
                helper.showAlerts(ls, r);
                helper.removeCachedTemplates();
                angular.element('.page.ng-scope').find('header-dir').remove();
                angular.element('.page.ng-scope').prepend('<header-dir></header-dir>');
                $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                $state.reload()
            }
        })
    }
    ls.sendAvatar = function(e) {
        angular.element('.uploadFileAvatar').on('click', function(e) {
            e.stopPropagation()
        }).on('change', function(e) {
            e.preventDefault();
            var _this = $(this),
                p = _this.parent(),
                x = p.find('.uploadFileAvatar');
            var formData = new FormData();
            if (x[0].files[0] != undefined) {
                formData.append("Filedata", x[0].files[0])
            }
            formData.append("do", 'misc-myaccount-user-attachCustomAvatar');
            //angular.element('.loading_wrap').removeClass('hidden');
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "index.php", !0);
            oReq.onload = function(oEvent) {
                var res = JSON.parse(oEvent.target.response);
                //angular.element('.loading_wrap').addClass('hidden');
                if (oReq.status == 200) {
                    if (res.success) {
                        //angular.element('.loading_wrap').removeClass('hidden');
                        helper.doRequest('get', 'index.php', {
                            'do': 'misc-myaccount'
                        }, function(r) {
                            for (x in r.data) {
                                ls.user[x] = r.data[x]
                            }
                            helper.showAlerts(ls, res);
                            helper.removeCachedTemplates();
                            angular.element('.page.ng-scope').find('header-dir').remove();
                            angular.element('.page.ng-scope').prepend('<header-dir></header-dir>');
                            $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                            $state.reload()
                        })
                    } else {
                        helper.showAlerts(ls, res)
                    }
                } else {
                    alert("Error " + oReq.status + " occurred when trying to upload your file.")
                }
                angular.element('.uploadFileAvatar').unbind()
            };
            oReq.send(formData)
        });
        angular.element(e.target).parents('.row').find('.uploadFileAvatar').click()
    }
}]).controller('Board', ['$scope', '$timeout', '$compile', '$rootScope', '$state', '$uibModal', 'helper', 'selectizeFc', 'UserService', 'Upload', 'modalFc', function($scope, $timeout, $compile, $rootScope, $state, $uibModal, helper, selectizeFc, UserService, Upload, modalFc) {
    var vm = this,
        triedToSave = !1,
        typePromise;
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    vm.database = {};
    vm.annual_id = '';
    vm.step = 1;
    vm.address = '';
    vm.zip = '';
    vm.country_id = '';
    vm.city = '';
    vm.btw_nr = '';
    vm.company = '';
    vm.company_id = '';
    vm.activity_id = '';
    vm.ACCOUNT_BIC_CODE = '';
    vm.ACCOUNT_IBAN = '';
    vm.emails = '';
    vm.logos = 'images/akti-picture.png';
    vm.com_active = '';
    vm.con_active = '';
    vm.pro_active = '';
    vm.int_active = '';
    vm.cas_active = '';
    vm.web_active = '';
    vm.acc_active = '';
    vm.sto_active = '';
    vm.catplus_active = '';
    vm.valid_check = !0;
    vm.slideCurrentNumber=0;
    vm.hideLogoText=false;

    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.check_email = function(email) {
        if (typePromise) {
            $timeout.cancel(typePromise)
        }
        typePromise = $timeout(function() {
            if (email) {
                helper.doRequest('post', 'index.php', {
                    'do': 'misc--user-check_email',
                    emails: email
                }, function(r) {
                    vm.showAlerts(vm, r)
                })
            } else {
                vm.valid_check = !0
            }
        }, 300)
    }
    vm.Step = function(step) {
        vm.step = 0;
        vm.step1 = 1
    }
    vm.reset = function() {
        vm.checking = !1;
        vm.isOk = !1;
        vm.isFalse = !1;
        vm.isTrends = !1
    }
    vm.checkStockOption = function() {

        vm.stockManageOption = !vm.stockManageOption;
    }
    vm.checkServiceOption = function() {

        vm.serviceOption = !vm.serviceOption;
    }
    vm.renderPage = function(d) {
        vm.reset();
        if (d.error && vm.btw_nr) {
            vm.isFalse = !0
        }
        if (d.success && vm.btw_nr) {
            if (d.data.trends_ok) {
                vm.isTrends = !0
            } else {
                vm.isOk = !0;
                vm.vies_ok = !0
            }
            vm.parseData(d)
        }
    }
    vm.modaluser = function(r, template, ctrl, size) {
        var add_data = {
            annual_id: r.annual_id,
            city: r.city,
            country_id: r.country_id,
            btw_nr: r.btw_nr,
            address: r.address,
            zip: r.zip,
            company: r.company
        };
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl,
            controllerAs: 'vm',
            size: size,
            resolve: {
                obj: function() {
                    //angular.element('.loading_wrap').removeClass("hidden");
                    return helper.doRequest('post', 'index.php?do=misc-board', add_data, function(r) {
                        console.log(r)
                    })
                }
            }
        });
        modalInstance.result.then(function(data) {}, function() {})
    }
    vm.parseData = function(d) {
        vm.name = d.data.comp_name;
        vm.address = d.data.comp_address;
        vm.city = d.data.comp_city;
        vm.zip = d.data.comp_zip;
        vm.company = d.data.comp_name;
        vm.country_id = d.data.comp_country ? d.data.comp_country : 26;

    }
    vm.checkVatAdd = function() {
        vm.reset();
        vm.checking = !0;
        if (!vm.btw_nr) {
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            vm.checking = !1;
            return
        }
        if (typePromise) {
            $timeout.cancel(typePromise)
        }
        typePromise = $timeout(function() {
            if (vm.btw_nr) {
                var data = {
                    'do': 'misc--misc-check_vies_vat_number',
                    value: vm.btw_nr
                };
                helper.doRequest('post', 'index.php', data, vm.renderPage)
            } else {
                vm.reset()
            }
        }, 300)
    }
    vm.uploadFiles = function(files) {
        console.log(files);
        if (files && files.length) {
            Upload.upload({
                url: 'index.php',
                data: {
                    file: files,
                    'do': 'misc-board-user-upload_board1'
                }
            }).then(function(r) {
                vm.logos = r.data.data.logos;
                vm.hideLogoText=true;
            }, function() {})
        }
    }
    vm.upload = function(vm, e) {
        angular.element(e.target).parents('.upload-box1').find('.newUploadFile21').click()
    }
    angular.element('.onboarding_body').on('click', '.newUploadFile21', function(e) {
        e.stopPropagation()
    }).on('change', '.newUploadFile21', function(e) {
        e.preventDefault();
        var _this = $(this),
            p = _this.parents('.upload-box1'),
            x = p.find('.newUploadFile21');
        if (x[0].files.length == 0) {
            return !1
        }
        if (x[0].files[0].size > 256000) {
            var msg = "<b>" + helper.getLanguage("The logo does not meet the requierements.") + "</b>";
            var title = helper.getLanguage("Upload logo");
            var cont = {
                "e_message": msg,
                "e_title": title
            };
            openModal("alert_info", cont, "md")
            return !1
        }
        p.find('.img-thumbnail').attr('src', '');
        var tmppath = URL.createObjectURL(x[0].files[0]);
        p.find('.img-thumbnail').attr('src', tmppath);
        p.find('.btn').addClass('hidden');
        p.find('.nameOfTheFile').text(x[0].files[0].name);
        var formData = new FormData();
        formData.append("Filedata", x[0].files[0]);
        formData.append("do", 'misc-board-user-upload_board');
        p.find('.upload-progress-bar').css({
            width: '0%'
        });
        p.find('.upload-file').removeClass('hidden');
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "index.php", !0);
        oReq.upload.addEventListener("progress", function(e) {
            var pc = parseInt(e.loaded / e.total * 100);
            p.find('.upload-progress-bar').css({
                width: pc + '%'
            })
        });
        oReq.onload = function(oEvent) {
            var res = JSON.parse(oEvent.target.response);
            p.find('.upload-file').addClass('hidden');
            p.find('.btn').removeClass('hidden');
            if (oReq.status == 200) {
                if (res.success) {
                    $timeout(function() {
                        vm.account_address = res.data.account_address;
                        vm.hideLogoText=true;
                    })
                } else {
                    alert(res.error);
                    vm.hideLogoText=false;
                }
            } else {
                vm.hideLogoText=false;
                alert("Error  " + oReq.status + "  occurred  when  trying  to  upload  your  file.")
            }
        };
        oReq.send(formData)
    });
    vm.removeLogo=function(){
        var tmp_obj={'do':'misc-board-user-removeLogo'};
        helper.doRequest('post','index.php',tmp_obj, function(r) {
            if (r.success) {
                vm.hideLogoText=false;   
                vm.logos=r.data.logos; 
                angular.element('.drop_drag').find('.img-thumbnail').attr('src', vm.logos);       
            } else {
                vm.showAlerts(vm, r)
            }
        })
    }

    vm.finalStep = function(r) {
        //console.log(r);
        var add_data = {
            annual_id: r.annual_id,
            city: r.city,
            country_id: r.country_id,
            btw_nr: r.btw_nr,
            address: r.address,
            zip: r.zip,
            company: r.company,
            emails: r.emails,
            ACCOUNT_BIC_CODE: r.ACCOUNT_BIC_CODE,
            ACCOUNT_IBAN: r.ACCOUNT_IBAN,
            activity_id: r.activity_id,
            company_id: r.company_id,
            crm: r.crm_active,
            catalogue: r.cat_active,
            invoice: r.inv_active,
            quote: r.quo_active,
            order: r.com_active,
            contract: r.con_active,
            intervention: r.int_active,
            project: r.pro_active,
            cash: r.cas_active,
            webshop: r.web_active,
            accounting: r.acc_active,
            version: r.version,
            stock: r.sto_active,
            catalogueplus: r.catplus_active,
            stockManageOption: r.stockManageOption,
            serviceOption: r.serviceOption,
            email: r.email,
            first_name: r.name,
            last_name: r.lastname
           /* firstname: r.firstname,
            lastname: r.lastname,
            username: r.username,
            new_password: r.new_password,*/
        };
        //angular.element('.loading_wrap').removeClass("hidden");
        helper.doRequest('post', 'index.php?do=misc-isLoged-user-start_aplication', add_data, function(o) {
            if (!o.error) {
                $rootScope.haveGeneralSettings = !1;
              /*  if (vm.easyinvoice) {*/
                    //angular.element('.loading_wrap').removeClass("hidden");
                    UserService.name = o.data.name ? o.data.name : '';
                    UserService.default_pag = o.data.default_pag;
                    angular.element('.board-modal').remove();
                    angular.element('.page.ng-scope').removeClass("onboard");
                    angular.element('.page.ng-scope').find('.content.onboard').removeClass("onboard");
                    
                    helper.Loading=100;
                    angular.element('.loading_wrap').removeClass("hidden");
                    helper.removeCachedTemplates();
                    var get_anyway = 1; //to refresh the credentials in session
                    helper.getSettings(get_anyway).then(function(r) {
                        //console.log('waiting for the settings');
                        $rootScope.haveGeneralSettings=!0;
                        
                        angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                        angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                        $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                        $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                        helper.settings.WIZZARD_COMPLETE = o.data.WIZZARD_COMPLETE;
                        
                        helper.Loading=0;
                        helper.doRequest('get', 'index.php?do=misc-lang', function(d) {
                            LANG = d.data;
                            //angular.element('.loading_wrap').removeClass("hidden");
                            $state.go(UserService.default_pag)
                        }, function(d) {
                            //angular.element('.loading_wrap').removeClass("hidden");
                            $state.go(UserService.default_pag)
                        })
                        /*} else {
                            //vm.modal(o, 'miscTemplate/readymodal', 'ReadyCtrl', 'md')
                        }*/
                        if (o.data.showGdpr) {
                            openModal("GDPRPolicy", {})
                        }
                    })
                    

                
            } else {
                vm.showAlerts(vm, o)
            }
        })
    }
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl,
            controllerAs: 'vm',
            size: 'size',
            resolve: {
                obj: function() {
                    //angular.element('.loading_wrap').removeClass("hidden");
                    UserService.name = r.data.name ? r.data.name : '';
                    helper.removeCachedTemplates();
                    angular.element('.page.ng-scope').find('header-dir,footer-dir').remove();
                    angular.element('.page.ng-scope').prepend('<header-dir></header-dir>').append('<footer-dir></footer-dir>');
                    $compile(angular.element('.page.ng-scope').find('header-dir'))($rootScope);
                    $compile(angular.element('.page.ng-scope').find('footer-dir'))($rootScope);
                    helper.settings.WIZZARD_COMPLETE = r.data.WIZZARD_COMPLETE;
                    UserService.default_pag = r.data.default_pag;
                    return helper.doRequest('get', 'index.php?do=misc-lang', function(d) {
                        LANG = d.data;
                        //angular.element('.loading_wrap').removeClass("hidden");
                        $state.go(UserService.default_pag);
                        return {}
                    }, function(r) {
                        //angular.element('.loading_wrap').removeClass("hidden");
                        $state.go(UserService.default_pag);
                        return {}
                    })
                }
            }
        });
        modalInstance.result.then(function(data) {}, function() {})
    };
    helper.doRequest('get', 'index.php?do=misc-board', function(r) {
        vm.country = r.data.country;
        vm.annual = r.data.annual;
        vm.name = r.data.name;
        vm.firstname = r.data.firstname;
        vm.lastname = r.data.lastname;
        vm.username = r.data.username;
        vm.do = r.data.do_next;
        vm.word = r.data.word;
        vm.version = r.data.version;
        vm.crm_active = r.data.crm_active;
        vm.inv_active = r.data.inv_active;
        vm.quo_active = r.data.quo_active;
        vm.cat_active = r.data.cat_active;
        vm.activity = r.data.activity;
        vm.company_size = r.data.company_size;
        vm.company = r.data.ACCOUNT_COMPANY;
        vm.easyinvoice = r.data.easyinvoice;
        vm.placeholder1 = r.data.placeholder1;
        vm.placeholder2 = r.data.placeholder2;
        vm.placeholder3 = r.data.placeholder3;
        vm.is_facq_subscription = r.data.is_facq_subscription;
        vm.is_easy_invoice = r.data.is_easy_invoice;
        vm.service_terms_url=r.data.service_terms_url;
        vm.privay_policy_url=r.data.privay_policy_url;
        if (vm.easyinvoice) {
            vm.Step(1)
        }
    });

    vm.setSlide=function(event,toSlide,formobj){
        event.preventDefault();
        event.stopPropagation();
        var goToSlide=false;       
        if(vm.slideCurrentNumber == toSlide){
            return false;
        }
        switch(vm.slideCurrentNumber){
            case 0:
                if(toSlide==1 && vm.company && vm.address && vm.zip && vm.city && vm.country_id){
                    goToSlide=true;
                }
                break;
            case 1:
                if(toSlide == 0 || toSlide == 2){
                    goToSlide=true;
                }
                break;
            case 2:
                if(toSlide==3 && formobj && !formobj.email1.$invalid && !formobj.email2.$invalid && !formobj.email3.$invalid){
                    goToSlide=true;
                }else if(toSlide==0 || toSlide==1){
                    goToSlide=true;
                }
                break;
            case 3:
                if(toSlide>=0 && toSlide<3){
                    goToSlide=true;
                }              
                break;
        }
        if(goToSlide){
            angular.element("#myCarousel").carousel(toSlide);
            vm.slideCurrentNumber=toSlide;
        }
    }

    vm.setBillSlide=function(event,toSlide){
        event.preventDefault();
        event.stopPropagation();
        var goToSlide=false;       
        if(vm.slideCurrentNumber == toSlide){
            return false;
        }
        switch(vm.slideCurrentNumber){
            case 0:
                if(toSlide==1 && vm.name && vm.lastname && vm.accept_terms){
                    goToSlide=true;
                }
                break;
            case 1:
                if(toSlide == 0){
                    goToSlide=true;
                }else if(toSlide==2 && vm.company && vm.address && vm.zip && vm.city && vm.country_id){
                    goToSlide=true;
                }
                break;
            case 2:
                if(toSlide == 0 || toSlide == 1){
                    goToSlide=true;
                }
                break;
        }
        if(goToSlide){
            angular.element("#myCarousel").carousel(toSlide);
            vm.slideCurrentNumber=toSlide;
        }
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'GDPRPolicy':
                var iTemplate = 'GDPRPolicy';
                var ctas = 'vm';
                break
        }
        var params = {
            template: 'miscTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'GDPRPolicy':
                params.params = {
                    'do': 'misc-policy_info'
                };
                break
        }
        modalFc.open(params)
    }
}]).controller('ReadyCtrl', ['$scope', 'helper', '$state', '$compile', 'obj', '$uibModalInstance', 'UserService', function($scope, helper, $state, $compile, obj, $uibModalInstance, UserService) {
    var vm = this;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.close()
    };
    vm.show_page = function(status) {
        vm.status = status;
        if (vm.status == 'customer') {
            $state.go('customer', {
                'customer_id': "tmp"
            });
            vm.cancel()
        } else if (vm.status == 'article') {
            $state.go('article.add', {
                'article_id': "tmp"
            });
            vm.cancel()
        } else if (vm.status == 'quote') {
            $state.go('quote.edit', {
                'quote_id': "tmp"
            });
            vm.cancel()
        } else if (vm.status == 'invoice') {
            $state.go('invoice.edit', {
                invoice_id: "tmp"
            });
            vm.cancel()
        }
    }
}]).controller('ReadyUserCtrl', ['$scope', 'helper', '$state', '$compile', 'obj', '$uibModalInstance', 'UserService', function($scope, helper, $state, $compile, obj, $uibModalInstance, UserService) {
    var vm = this;
    vm.user = !1;
    vm.group = !1;
    vm.emails = '';
    vm.activeruser = function(active_user) {
        if (active_user == 0) {
            vm.user = !1
        } else {
            vm.user = !0
        }
    };
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.close()
    };
    vm.show_page = function(status) {
        vm.status = status;
        if (vm.status == 'customer') {
            $state.go('customer', {
                'customer_id': "tmp"
            });
            vm.cancel()
        } else if (vm.status == 'article') {
            $state.go('article.add', {
                'article_id': "tmp"
            });
            vm.cancel()
        } else if (vm.status == 'quote') {
            $state.go('quote.edit', {
                'quote_id': "tmp"
            });
            vm.cancel()
        } else if (vm.status == 'invoice') {
            $state.go('invoice.edit', {
                invoice_id: "tmp"
            });
            vm.cancel()
        }
    }
    vm.ok = function() {
        var data = angular.copy(vm);
        data.do = 'misc--user-add_user_board';
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.add_user = function() {
        var data = angular.copy(vm);
        data.do = 'misc--user-add_user';
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    };
    vm.add_group = function(crm, article, order, po_order, quote, project, intervention, contract, invoice, webshop, timesheet, group_id, name) {
        var get = 'group';
        if (crm == 1) {
            var crm = 1
        }
        if (article == 1) {
            var article = 12
        }
        if (order == 1) {
            var order = 6
        }
        if (po_order == 1) {
            var po_order = 14
        }
        if (quote == 1) {
            var quote = 5
        }
        if (project == 1) {
            var project = 3
        }
        if (intervention == 1) {
            var intervention = 13
        }
        if (contract == 1) {
            var contract = 11
        }
        if (invoice == 1) {
            var invoice = 4
        }
        if (webshop == 1) {
            var webshop = 9
        }
        if (timesheet == 1) {
            var timesheet = 8
        }
        var credential = {
            '1': crm,
            '12': article,
            '6': order,
            '14': po_order,
            '5': quote,
            '3': project,
            '13': intervention,
            '11': contract,
            '4': invoice,
            '9': webshop,
            '8': timesheet
        };
        var data = {
            'do': 'misc--user-add_group',
            'credential': credential,
            'group_id': group_id,
            'name': name
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
}]).controller('HeaderModalCtrl', ['$scope', '$uibModalInstance', '$timeout', 'helper', 'modalFc', 'data', function($scope, $uibModalInstance, $timeout, helper, modalFc, data) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    };

    function showAlerts(d) {
        helper.showAlerts(vmMod, d)
    }
}]).controller('InviteModalCtrl', ['$scope', '$uibModalInstance', '$timeout', 'helper', 'modalFc', 'data', function($scope, $uibModalInstance, $timeout, helper, modalFc, data) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.cancel = cancel;
    vmMod.showAlerts = showAlerts;

    function cancel() {
        $uibModalInstance.dismiss('cancel')
    };

    function showAlerts(d) {
        helper.showAlerts(vmMod, d)
    }

    var data = {
            'do': 'misc--misc-getReferralLink'
        };
     helper.doRequest('get', 'index.php', data, function(r) {
       
        if(r.data){
           vmMod.referral_link = r.data.referral_link;
        }
    })

}]).controller('DbQuery', ['$scope', '$timeout', 'helper', function($scope, $timeout, helper) {
    var vm = this;
    vm.database = {};

    function showAlerts(d) {
        helper.showAlerts(view, d)
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.executeQuery = function(dbs, i) {
        var n = dbs.databases.indexOf(dbs.db_base);
        if (dbs.bases === !0 && n == -1) {
            dbs.databases.push(dbs.db_base)
        }
        if (dbs.databases[i]) {
            if (dbs.users === !0) {
                var data = {
                    'do': 'misc--user-do_query',
                    'users_only': dbs.users,
                    'query': dbs.QUERY
                };
                helper.doRequest('post', 'index.php', data, function(r) {
                    vm.showAlerts(vm, r)
                })
            } else {
                var data = {
                    'do': 'misc--user-do_query',
                    'database': dbs.databases[i],
                    'query': dbs.QUERY
                };
                helper.doRequest('post', 'index.php', data, function(r) {
                    vm.showAlerts(vm, r);
                    i++;
                    vm.executeQuery(dbs, i)
                }, function(r) {
                    vm.executeQuery(dbs, i)
                })
            }
        } else {
            console.log('done')
        }
    }
    helper.doRequest('get', 'index.php?do=misc-db_query&xget=base', function(r) {
        vm.database = r.data.database
    })
}]).controller('GoogleCalendarModalCtrl', ['$scope', 'data', 'helper', '$uibModalInstance', function($scope, data, helper, $uibModalInstance) {
    var vmMod = this;
    vmMod.item = data;
    vmMod.save = function() {
        helper.doRequest('post', 'index.php', {
            'do': 'misc-myaccount-user-calendar_sync',
            'name': 'outgoing',
            'value': vmMod.item.outgoing
        })
    }
    vmMod.cancel = function() {
        $uibModalInstance.dismiss(vmMod.item.outgoing)
    }
}]).controller('NylasCalendarModalCtrl', ['$scope', '$window', 'data', 'helper', '$uibModalInstance', function($scope, $window, data, helper, $uibModalInstance) {
    var vmMod = this;
    vmMod.item = data;
    vmMod.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vmMod.cancel = function() {
        $uibModalInstance.close()
    };
    vmMod.ok = function(formObj) {
        if (vmMod.item.nylas_user_active) {
            //angular.element('.loading_alt').removeClass('hidden');
            helper.doRequest("post", 'index.php', {
                'do': 'misc--user-nylas_deactivate'
            }, function(r) {
                //angular.element('.loading_alt').addClass('hidden');
                vmMod.showAlerts(vmMod, r);
                if (r.success) {
                    vmMod.item.nylas_user_active = !1;
                    vmMod.item.nylas_title = helper.getLanguage('Connect')
                }
            })
        } else {
            if (vmMod.item.nylas_email == '' || vmMod.item.nylas_email === null) {
                var obj = {
                    "error": {
                        "email": helper.getLanguage("This field is required")
                    },
                    "notice": !1,
                    "success": !1
                };
                vmMod.showAlerts(vmMod, obj, formObj)
            } else {
                $uibModalInstance.close();
                $window.open("https://api.nylas.com/oauth/authorize?client_id=" + vmMod.item.nylas_app_id + "&response_type=code&scope=email&login_hint=" + vmMod.item.nylas_email + "&redirect_uri=" + vmMod.item.nylas_authorize_url + "&state=authorize", "_blank")
            }
        }
    }
}]).controller('viesModalCtrl', ['$scope', '$uibModalInstance', '$cacheFactory', '$state', 'data', 'helper', 'modalFc', function($scope, $uibModalInstance, $cacheFactory, $state, data, helper, modalFc) {
    var vmMod = this;
    vmMod.item = {};
    for (x in data) {
        vmMod.item[x] = data[x]
    }
    vmMod.item.do = 'customers-EditCustomer-customers-address_add';
    vmMod.item.field = 'customer_id';
    vmMod.item.title = helper.getLanguage("Add Address");
    vmMod.item.fromCrm = !0;
    vmMod.openModal = function(type, item, size, obj) {
        var params = {
            template: 'miscTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        }
        switch (type) {
            case 'AddAddress':
                params.item = item;
                params.callback = function(data) {
                    vmMod.cancel()
                }
                break
        }
        modalFc.open(params)
    }
    vmMod.cancel = function() {
        $uibModalInstance.dismiss(data)
    };
    vmMod.updateDetails = function() {
        vmMod.cancel();
        var cacheCustomer = $cacheFactory.get('ViewToCustomer');
        if (cacheCustomer != undefined) {
            cacheCustomer.destroy()
        }
        var data = angular.copy(vmMod.item);
        var cache = $cacheFactory('ViewToCustomer');
        cache.put('data', data);
        $state.go('customer', {
            customer_id: vmMod.item.customer_id
        }, {
            reload: !0
        })
    }
}]).controller('timesheetCtrl', ['$scope', '$window', '$timeout', '$compile', 'helper', 'modalFc', function($scope, $window, $timeout, $compile, helper, modalFc) {
    console.log('General timesheet');
    $scope.activeType = 2;
    $scope.types = [{
        name: helper.getLanguage('Daily'),
        id: 1,
        active: ''
    }, {
        name: helper.getLanguage('Weekly'),
        id: 2,
        active: 'active'
    }, {
        name: helper.getLanguage('Monthly'),
        id: 3,
        active: ''
    }];
    $scope.item = {};
    $scope.subtotal = [{
        'total_h_wo_0': 0,
        'total_h_0': '',
        'total_d_0': ''
    }, {
        'total_h_wo_1': 0,
        'total_h_1': '',
        'total_d_1': ''
    }, {
        'total_h_wo_2': 0,
        'total_h_2': '',
        'total_d_2': ''
    }, {
        'total_h_wo_3': 0,
        'total_h_3': '',
        'total_d_3': ''
    }, {
        'total_h_wo_4': 0,
        'total_h_4': '',
        'total_d_4': ''
    }, {
        'total_h_wo_5': 0,
        'total_h_5': '',
        'total_d_5': ''
    }, {
        'total_h_wo_6': 0,
        'total_h_6': '',
        'total_d_6': ''
    }];
    $scope.timesheet = {
        start: 0
    };
    $scope.search_day = {
        search: '',
        'do': 'misc-timesheet',
        view: 0,
        xget: 'Day',
        offset: 1
    };

    function init_calculus() {
        for (var i = 0; i < 7; i++) {
            var subtotal_h = 0;
            var subtotal_d = 0;
            for (x in $scope.item.task_row) {
                if ($scope.item.task_row[x].project_type == '0') {
                    subtotal_h += parseFloat($scope.item.task_row[x].task_day_row[i].HOURS)
                } else {
                    subtotal_d += parseFloat($scope.item.task_row[x].task_day_row[i].HOURS)
                }
            }
            if (subtotal_h > 0) {
                $scope.subtotal[i]['total_h_wo_' + i] = subtotal_h;
                $scope.subtotal[i]['total_h_' + i] = corect_val(subtotal_h)
            } else {
                $scope.subtotal[i]['total_h_wo_' + i] = 0;
                $scope.subtotal[i]['total_h_' + i] = ''
            }
            if (subtotal_d > 0) {
                $scope.subtotal[i]['total_d_' + i] = subtotal_d
            } else {
                $scope.subtotal[i]['total_d_' + i] = ''
            }
        }
        var total_h = 0;
        var total_d = 0;
        for (x in $scope.item.task_row) {
            var line_h = 0;
            var line_d = 0;
            if ($scope.item.task_row[x].project_type == '0') {
                for (y in $scope.item.task_row[x].task_day_row) {
                    if ($scope.item.task_row[x].task_day_row[y].HOURS != '') {
                        line_h += parseFloat($scope.item.task_row[x].task_day_row[y].HOURS)
                    }
                }
            } else {
                for (y in $scope.item.task_row[x].task_day_row) {
                    if ($scope.item.task_row[x].task_day_row[y].HOURS != '') {
                        line_d += parseFloat($scope.item.task_row[x].task_day_row[y].HOURS)
                    }
                }
            }
            total_h += line_h;
            total_d += line_d;
            if (line_h > 0) {
                $scope.item.task_row[x].line_h_wo = line_h;
                $scope.item.task_row[x].line_h = corect_val(line_h)
            } else {
                $scope.item.task_row[x].line_h_wo = 0;
                $scope.item.task_row[x].line_h = ''
            }
            if (line_d > 0) {
                $scope.item.task_row[x].line_d = line_d
            } else {
                $scope.item.task_row[x].line_d = ''
            }
        }
        if (total_h > 0) {
            $scope.item.total_h_wo = total_h;
            $scope.item.total_h = corect_val(total_h)
        } else {
            $scope.item.total_h_wo = 0;
            $scope.item.total_h = ''
        }
        if (total_d > 0) {
            $scope.item.total_d = total_d
        } else {
            $scope.item.total_d = ''
        }
    }

    function init_day_calculus() {
        var total_h = 0;
        for (x in $scope.item.rows) {
            if (!$scope.item.rows[x].is_total_h) {
                var line_h = parseFloat(convert_value($scope.item.rows[x].end_time)) - parseFloat(convert_value($scope.item.rows[x].start_time)) - parseFloat(convert_value($scope.item.rows[x].break));
                if (line_h > 0) {
                    $scope.item.rows[x].line_h_wo = line_h;
                    $scope.item.rows[x].line_h = corect_val(line_h)
                } else {
                    $scope.item.rows[x].line_h_wo = 0;
                    $scope.item.rows[x].line_h = ''
                }
                total_h += line_h
            } else {
                total_h += parseFloat($scope.item.rows[x].line_h_wo, 10)
            }
        }
        if (total_h > 0) {
            $scope.item.total_h_wo = total_h;
            $scope.item.total_h = corect_val(total_h)
        } else {
            $scope.item.total_h_wo = 0;
            $scope.item.total_h = ''
        }
    }
    $scope.auto = {
        project: [],
        task: [],
        customer: [],
    };
    $scope.init_calculus = init_calculus;
    $scope.init_day_calculus = init_day_calculus;
    $scope.renderPage = function(d) {
        if (d.data) {
            $scope.item = d.data;
            if ($scope.item.tab == 1 && $scope.activeType < 3) {
                $scope.auto.project = d.data.projects_list;
                $scope.auto.customer = d.data.customers_list;
                $scope.auto.task = d.data.tasks_list;
                if ($scope.activeType == 2) {
                    init_calculus()
                } else if ($scope.activeType == 1) {
                    init_day_calculus()
                }
            }
            if (d.data.start_date) {
                $scope.item.start_date = new Date(d.data.start_date)
            } else {
                $scope.item.start_date = new Date()
            }
        }
    }
    $scope.getTimesheet = function() {
        $scope.activeType = 2;
        helper.doRequest('get', 'index.php?do=misc-timesheet', $scope.renderPage)
    }
    $scope.getExpense = function() {
        helper.doRequest('get', 'index.php?do=misc-timesheet_expense', $scope.renderPage)
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.orderBY = function(p) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        var params = {
            view: $scope.activeView,
            offset: $scope.search_day.offset,
            desc: $scope.reverse,
            order_by: $scope.ord,
            start_date: $scope.item.start_date.toUTCString(),
            user_id: $scope.item.user_id
        };
        for (x in $scope.search_day) {
            params[x] = $scope.search_day[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderPage(r)
        })
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1
    }
    $scope.customerCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Users'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value != undefined) {
                var type_r = '';
                switch ($scope.activeType) {
                    case '1':
                        type_r = 'Day';
                        break;
                    case '2':
                        type_r = 'Week';
                        break;
                    case '3':
                        type_r = 'Month';
                        break
                }
                $scope.timesheet.start = 0;
                if ($scope.item.tab == 1) {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php', {
                        'do': 'misc-timesheet',
                        'xget': type_r,
                        'start_date': $scope.item.start_date.toUTCString(),
                        'user_id': this.options[value].id
                    }, function(r) {
                        $scope.renderPage(r)
                    })
                } else {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php', {
                        'do': 'misc-timesheet_expense',
                        'start_date': $scope.item.start_date.toUTCString(),
                        'user_id': this.options[value].id
                    }, function(r) {
                        $scope.renderPage(r)
                    })
                }
            }
        },
        maxItems: 1
    };
    $scope.typeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Overview'),
        create: !1,
        onChange(value) {
            var type_r = '';
            switch (value) {
                case '1':
                    type_r = 'Day';
                    break;
                case '2':
                    type_r = 'Week';
                    break;
                case '3':
                    type_r = 'Month';
                    break
            }
            $scope.timesheet.start = 0;
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', {
                'do': 'misc-timesheet',
                'xget': type_r,
                'start_date': $scope.item.start_date.toUTCString(),
                'user_id': $scope.item.user_id
            }, function(r) {
                $scope.renderPage(r)
            })
        },
        maxItems: 1
    };
    $scope.projectAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select project'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value == undefined || value == '') {
                var data = {
                    'do': 'misc-timesheet',
                    xget: 'tasks_list'
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    $scope.auto.task = r.data
                })
            } else if (value != undefined) {
                var data = {
                    'do': 'misc-timesheet',
                    xget: 'tasks_list',
                    project_id: this.options[value].id
                };
                if ($scope.activeType == 1) {
                    data.tasks_ids = $scope.item.tasks_ids
                }
                helper.doRequest('get', 'index.php', data, function(r) {
                    $scope.auto.task = r.data
                })
            }
        },
        maxItems: 1
    };
    $scope.taskAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select task'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 100
    };
    $scope.navigate = function(i) {
        $scope.item.start_date.setDate($scope.item.start_date.getDate() + (i * 7));
        $scope.timesheet.start = $scope.timesheet.start + i;
        //angular.element('.loading_wrap').removeClass('hidden');
        if ($scope.item.tab == 1) {
            helper.doRequest('get', 'index.php', {
                'do': 'misc-timesheet',
                'xget': 'Week',
                'start': $scope.timesheet.start,
                'start_date': $scope.item.start_date.toUTCString(),
                'user_id': $scope.item.user_id
            }, function(r) {
                $scope.renderPage(r)
            })
        } else {
            helper.doRequest('get', 'index.php', {
                'do': 'misc-timesheet_expense',
                'start': $scope.timesheet.start,
                'start_date': $scope.item.start_date.toUTCString(),
                'user_id': $scope.item.user_id
            }, function(r) {
                $scope.renderPage(r)
            })
        }
    }
    $scope.navigate_month = function(i) {
        $scope.item.start_date = $scope.item.start_date.addMonths(i);
        $scope.timesheet.start = $scope.timesheet + i;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'misc-timesheet',
            'xget': 'Month',
            'start': $scope.timesheet.start,
            'start_date': $scope.item.start_date.toUTCString(),
            'user_id': $scope.item.user_id
        }, function(r) {
            $scope.renderPage(r)
        })
    }
    $scope.navigate_day = function(i) {
        $scope.item.start_date.setDate($scope.item.start_date.getDate() + i);
        $scope.timesheet.start = $scope.item.start + i;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'misc-timesheet',
            'xget': 'Day',
            'start': $scope.timesheet.start,
            'start_date': $scope.item.start_date.toUTCString(),
            'user_id': $scope.item.user_id
        }, function(r) {
            $scope.renderPage(r)
        })
    }
    $scope.changeDayData = function(cell, increment) {
        if ((cell.HOURSB == '1' && increment > 0) || (cell.HOURSB == '0' && increment < 0)) {
            return !1
        } else {
            if (isNaN(cell.HOURSB) || cell.HOURSB == '') {
                cell.HOURSB = '0';
                cell.HOURS = '0'
            }
            cell.HOURSB = parseFloat(cell.HOURSB) + increment;
            cell.HOURS = parseFloat(cell.HOURS) + increment
        }
        init_calculus();
        $scope.saveTimesheet(cell)
    }
    $scope.changeHourData = function(cell) {
        if (cell.IS_BILLED || cell.IS_APPROVED || cell.IS_SUBMITED || cell.IS_CLOSED) {
            return !1
        }
        cell.HOURS = isNaN(cell.HOURSB) ? convert_value(cell.HOURSB) : cell.HOURSB;
        cell.HOURSB = corect_val(cell.HOURSB);
        init_calculus();
        $scope.saveTimesheet(cell)
    }
    $scope.changeHourDataKey = function(event, cell) {
        if (event.keyCode == 13) {
            if (cell.IS_BILLED || cell.IS_APPROVED || cell.IS_SUBMITED || cell.IS_CLOSED) {
                return !1
            } else {
                $scope.changeHourData(cell)
            }
        }
    }
    $scope.showNote = function(cell) {
        openModal("show_note", cell, "md")
    }
    $scope.saveTimesheet = function(cell) {
        var data = angular.copy(cell);
        data.do = 'misc--time-add_hour_simple';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.showAlerts(r);
            cell.success_add = !0;
            $timeout(function() {
                cell.success_add = !1
            }, 1500)
        })
    }
    $scope.deleteEntry = function(index) {
        var data = angular.copy($scope.item.task_row[index]);
        data.do = 'misc-timesheet-time-delete_time';
        data.xget = 'Week';
        data.start_date = $scope.item.start_date.toUTCString();
        data.user_id = $scope.item.user_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.renderPage(r)
            }
        })
    }
    $scope.changeDate = function() {
        var type_r = '';
        switch ($scope.activeType) {
            case '1':
                type_r = 'Day';
                break;
            case '2':
                type_r = 'Week';
                break
        }
        $scope.timesheet.start = 0;
        //angular.element('.loading_wrap').removeClass('hidden');
        if ($scope.item.tab == 1) {
            helper.doRequest('get', 'index.php', {
                'do': 'misc-timesheet',
                'xget': type_r,
                'start_date': $scope.item.start_date.toUTCString(),
                'user_id': $scope.item.user_id
            }, function(r) {
                $scope.renderPage(r)
            })
        } else {
            helper.doRequest('get', 'index.php', {
                'do': 'misc-timesheet_expense',
                'start_date': $scope.item.start_date.toUTCString(),
                'user_id': $scope.item.user_id
            }, function(r) {
                $scope.renderPage(r)
            })
        }
    }
    $scope.submitTime = function() {
        var data = angular.copy($scope.item);
        openModal("submit_time", data, "md")
    }
    $scope.printModal = function(item) {
        openModal("print_time", item, "md")
    }
    $scope.addTask = function(type) {
        var data = {
            'activeType': $scope.activeType,
            'auto_project': angular.copy($scope.auto.project),
            'auto_customer': angular.copy($scope.auto.customer),
            'auto_task': angular.copy($scope.auto.task),
            'item': angular.copy($scope.item),
            't_type': type
        };
        openModal('select_task', data, 'md')
    }
    $scope.showTime = function(event, index, model, align) {
        var _this = event.target;
        angular.element('.planning_groups_week').find('timepicker').remove();
        var compiledHTML = $compile('<timepicker nindex="' + index + '" nmodel="' + model + '" nalign="' + align + '" "></timepicker>')($scope);
        angular.element(_this).parent().after(compiledHTML)
    }
    $scope.deleteTimeEntry = function(index) {
        if (!$scope.item.DISPLAY_SAVE) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Cannot delete submitted data")
                },
                "notice": !1,
                "success": !1
            };
            $scope.showAlerts(obj);
            return !1
        }
        if ($scope.item.rows[index].id == '0') {
            $scope.item.rows.splice(index, 1);
            init_day_calculus()
        } else {
            helper.doRequest('post', 'index.php', {
                'do': 'misc--time-deleteSubHours',
                'id': $scope.item.rows[index].id
            }, function(r) {
                $scope.showAlerts(r);
                if (r.success) {
                    $scope.item.rows.splice(index, 1);
                    init_day_calculus()
                }
            })
        }
    }
    $scope.updateNote = function(index) {
        var data = {};
        data.do = 'misc--time-updateDayComment';
        data.id = $scope.item.rows[index].id;
        data.notes = $scope.item.rows[index].comment;
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.showAlerts(r)
        })
    }
    $scope.changeTotalHoursLine = function(index) {
        $scope.item.rows[index].is_total_h = !0;
        $scope.item.rows[index].line_h = corect_val($scope.item.rows[index].line_h);
        $scope.item.rows[index].line_h_wo = convert_value($scope.item.rows[index].line_h);
        $scope.item.rows[index].start_time = '0:00';
        $scope.item.rows[index].end_time = '0:00';
        $scope.item.rows[index].break = '0:00';
        init_day_calculus();
        var data = {};
        data.do = 'misc--time-updateDayTotalHours';
        data.task_time_id = $scope.item.rows[index].task_time_id;
        data.id = $scope.item.rows[index].id;
        data.value = $scope.item.rows[index].line_h_wo;
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.showAlerts(r)
        })
    }
    $scope.savepickerData = function(index) {
        $scope.item.rows[index].line_h = '0:00';
        $scope.item.rows[index].line_h_wo = '0';
        $scope.item.rows[index].is_total_h = !1;
        $scope.item.rows[index].start_time = corect_val($scope.item.rows[index].start_time);
        $scope.item.rows[index].end_time = corect_val($scope.item.rows[index].end_time);
        $scope.item.rows[index].break = corect_val($scope.item.rows[index].break);
        var start_time = parseFloat(convert_value($scope.item.rows[index].start_time));
        var end_time = parseFloat(convert_value($scope.item.rows[index].end_time));
        var break_time = parseFloat(convert_value($scope.item.rows[index].break));
        if (start_time == 0 || end_time == 0) {
            return !1
        } else {
            init_day_calculus();
            var data = {};
            data.do = 'misc--time-updateDayHours';
            data.start_time = start_time;
            data.end_time = end_time;
            data.break = break_time;
            data.id = $scope.item.rows[index].id;
            data.task_time_id = $scope.item.rows[index].task_time_id;
            helper.doRequest('post', 'index.php', data, function(r) {
                $scope.showAlerts(r)
            })
        }
    }
    $scope.addExpense = function(type, id) {
        var data = {
            'type': type,
            'id': id
        };
        openModal('time_expense', data, "md")
    }
    $scope.showReciept = function(exp) {
        openModal("show_reciept", exp.IMG_HREF, "md")
    }
    $scope.deleteReciept = function(exp) {
        $scope.timesheet.start = 0;
        var data = {};
        data.id = exp.E_ID;
        data.do = 'misc-timesheet_expense-time-delete_image';
        data.start_date = $scope.item.start_date.toUTCString();
        data.user_id = $scope.item.user_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.renderPage(r);
            $scope.showAlerts(r)
        })
    }
    $scope.deleteExpense = function(exp) {
        $scope.timesheet.start = 0;
        var data = {};
        data.id = exp.E_ID;
        data.do = 'misc-timesheet_expense-time-delete_expense';
        data.start_date = $scope.item.start_date.toUTCString();
        data.user_id = $scope.item.user_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.renderPage(r);
            $scope.showAlerts(r)
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'show_note':
                var iTemplate = 'TimesheetNote';
                var ctas = 'note';
                break;
            case 'submit_time':
                var iTemplate = 'SubmitTime';
                var ctas = 'time';
                break;
            case 'print_time':
                var iTemplate = 'PrintTime';
                var ctas = 'time';
                break;
            case 'select_task':
                var iTemplate = 'SelectTask';
                var ctas = 'tsk';
                break;
            case 'time_expense':
                var iTemplate = 'TimeExpense';
                var ctas = 'time';
                break;
            case 'show_reciept':
                var iTemplate = 'ShowReciept';
                var ctas = 'rec';
                break
        }
        var params = {
            template: 'miscTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'show_note':
                params.params = {
                    'do': 'misc-xcomment',
                    'task_time_id': item.TASK_DAY_ID
                };
                params.callback = function() {
                    $scope.timesheet.start = 0;
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php', {
                        'do': 'misc-timesheet',
                        'xget': 'Week',
                        'start_date': $scope.item.start_date.toUTCString(),
                        'user_id': $scope.item.user_id
                    }, function(r) {
                        $scope.renderPage(r)
                    })
                }
                break;
            case 'submit_time':
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.renderPage(d)
                    }
                }
                break;
            case 'print_time':
                params.params = {
                    'do': 'misc-select_customer',
                    'user_id': $scope.item.user_id,
                    'time_start': $scope.item.week_s,
                    'time_end': $scope.item.week_e
                };
                params.callback = function(data) {
                    if (data) {
                        var new_link = item.pdf_link_loop + '&customer_id=' + data.customer_id + '&logo=' + data.logo + '&t_type=' + data.t_type;
                        $window.open(new_link, '_blank')
                    }
                }
                break;
            case 'select_task':
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.timesheet.start = 0;
                        $scope.renderPage(d);
                        $scope.showAlerts(d)
                    }
                }
                break;
            case 'time_expense':
                params.request_type = 'post';
                params.params = {
                    'do': 'misc-timesheet_expense',
                    'xget': 'ExpenseData',
                    'id': item.id,
                    'type': item.type,
                    'user_id': $scope.item.user_id,
                    'SELECT_DAY': $scope.item.SELECT_DAY,
                    'TODAY_nr': $scope.item.TODAY_nr
                };
                params.callback = function() {
                    $scope.timesheet.start = 0;
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php', {
                        'do': 'misc-timesheet_expense',
                        'start_date': $scope.item.start_date.toUTCString(),
                        'user_id': $scope.item.user_id
                    }, function(r) {
                        $scope.renderPage(r)
                    })
                }
                break;
            case 'show_reciept':
                params.item = item;
                break
        }
        modalFc.open(params)
    }
    helper.doRequest('get', 'index.php?do=misc-timesheet', $scope.renderPage)
}]).controller('EditActivityModalCtrl', ['data', 'helper', '$uibModalInstance', 'selectizeFc', function(data, helper, $uibModalInstance, selectizeFc) {
    var vmMod = this,
        currentTime = new Date(),
        current = undefined;
    vmMod.obj = data;

    vmMod.hours = [];
    vmMod.format = vmMod.obj.account_date_format;
    vmMod.dateOptions = {
        dateFormat: ''
    }
    vmMod.showTxt={};
    for (var i = 0; i <= 23; i++) {
        for (var j = 0; j <= 3; j++) {
            var min = j * 15,
                text = (i < 10 ? '0' + i : i) + ':' + (min < 10 ? '0' + min : min);
            vmMod.hours.push({
                'id': i + '-' + min,
                'value': text
            });
            if (i <= currentTime.getHours() && min <= currentTime.getMinutes()) {
                current = i + '-' + min
            }
        }
    }
    vmMod.status = [{
        'id': '0',
        'value': helper.getLanguage('Scheduled')
    }, {
        'id': '1',
        'value': helper.getLanguage('Completed')
    }];
    vmMod.typeOfCall = [{
        'id': '0',
        'value': helper.getLanguage('outgoing')
    }, {
        'id': '1',
        'value': helper.getLanguage('incoming')
    }];
    vmMod.obj.date = new Date(vmMod.obj.day_tmpstmp * 1000);
    vmMod.obj.dateHours = new Date(
            vmMod.obj.date.getFullYear(),
            vmMod.obj.date.getMonth(),
            vmMod.obj.date.getDate(),
            vmMod.obj.date.getHours(),
            vmMod.obj.date.getMinutes());
    if(vmMod.obj.end_d_meet){
       vmMod.obj.end_d_meet = new Date(vmMod.obj.end_d_meet * 1000); 
       vmMod.obj.dateEHours=new Date(
            vmMod.obj.end_d_meet.getFullYear(),
            vmMod.obj.end_d_meet.getMonth(),
            vmMod.obj.end_d_meet.getDate(),
            vmMod.obj.end_d_meet.getHours(),
            vmMod.obj.end_d_meet.getMinutes());
    }
    if (vmMod.obj.reminder_assign) {
        vmMod.obj.reminder_event = new Date(vmMod.obj.reminder_tmpstmp * 1000);
        vmMod.obj.reminderHours = vmMod.obj.reminder_time.replace(/:/, '-').replace(/00/, '0')
    }
    vmMod.open={
        'date': false,
        'reminder_date':false
    };
    vmMod.tinymceOptions_new = {
        selector: ".variable_assign_new",
        resize: "height",
        menubar: !1,
        autoresize_bottom_margin: 0,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        toolbar: [" bold italic | bullist numlist "]
    };
    vmMod.timeAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Time'),
        onChange(value) {
            vmMod.changeTime(vmMod.obj.date, vmMod.obj.dateHours, !0);
            vmMod.changeTime(vmMod.obj.reminder_event, vmMod.obj.reminderHours)
        }
    });
    vmMod.statusAutoCfg = selectizeFc.configure({});
    vmMod.dealAutoCfg= selectizeFc.configure({
        placeholder: helper.getLanguage('Search deal')
    });
    vmMod.userAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Assign to'),
        onChange(value){
            if(value){
                vmMod.obj.to_user=this.options[value].value;
            }
        }
    });

    vmMod.accountAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select Account'),
        onType(str) {
            var _this = this;
            helper.doRequest('post', 'index.php', {
                'do': 'misc-myActivities',
                xget: 'customers',
                term: str
            }, function(r) {
                if (r && r.data) {
                    vmMod.accounts = r.data;
                    _this.open()
                }
            })
        },
        onFocus() {
            var _this = this;
            helper.doRequest('post', 'index.php', {
                'do': 'misc-myActivities',
                xget: 'customers'
            }, function(r) {
                if (r && r.data) {
                    vmMod.accounts = r.data;
                    _this.open()
                }
            })
        }
    });
    vmMod.contactAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select contact'),
        maxItems: 1000,
        onType(str) {
            var _this = this;
            helper.doRequest('post', 'index.php', {
                'do': 'misc-myActivities',
                xget: 'contacts',
                'customer_id': vmMod.obj.customer_id,
                term: str
            }, function(r) {
                if (r && r.data) {
                    vmMod.contacts = r.data;
                    _this.open()
                }
            })
        },
        onFocus() {
            var _this = this;
            helper.doRequest('post', 'index.php', {
                'do': 'misc-myActivities',
                xget: 'contacts',
                'customer_id': vmMod.obj.customer_id
            }, function(r) {
                if (r && r.data) {
                    vmMod.contacts = r.data;
                    _this.open()
                }
            })
        },
    });

    vmMod.reminderAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        onChange(value){
            if(value == '1' && vmMod.obj.task_type == '6'){
                this.addItem('2');
            }else if(parseInt(value) > 1){
                vmMod.obj.hd_events = !0;
            }else{
                vmMod.obj.hd_events = !1;
                vmMod.obj.reminder_event=undefined;
            }
        },
        onDelete(value){
            return false;
        }
    });

    vmMod.assignAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select')
    });

    vmMod.showAlerts=function(d, formObj) {
        helper.showAlerts(vmMod, d, formObj)
    }

    vmMod.changeTime = function(item, t, check) {
        if (item && t) {
            item.setHours(t.getHours(),t.getMinutes());
        }
    }
    vmMod.changeDuration = function() {
        if (vmMod.obj.duration_meet) {
            var d = vmMod.obj.duration_meet.split('-'),
                currentH = angular.copy(vmMod.obj.dateHours).split('-');
            vmMod.obj.end_d_meet.setHours(vmMod.obj.date.getHours() + parseInt(d[0]), vmMod.obj.date.getMinutes() + parseInt(d[1]));
            var minutes = parseInt(currentH[1]) + parseInt(d[1]);
            var h = parseInt(currentH[0]) + parseInt(d[0]);
            if (minutes >= 60) {
                minutes -= 60;
                h++
            }
            vmMod.obj.dateEHours = (h) + '-' + (minutes)
        } else {
            vmMod.obj.end_d_meet = new Date();
            vmMod.obj.dateEHours = current
        }
    }
    vmMod.checkAllDay = function() {
        var new_date = angular.copy(vmMod.obj.date),
            day = new_date.getDate();
        vmMod.obj.end_d_meet = new_date
    }
    vmMod.save = function(formObj) {
        var data = angular.copy(vmMod.obj);
        if (vmMod.obj.opportunity_id) {
            data.do = 'customers-deal_edit-deals-UpdateActivity'
        } else if(vmMod.obj.from_tasks){
            data.do = 'misc--customers-UpdateActivity'
        } else {
            data.do = 'misc-activities-customers-UpdateActivity';
            data.xget = 'Activity'
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if(r.error){
                vmMod.showAlerts(r,formObj);
            }else{
                $uibModalInstance.close(r)
            }         
        })
    }
    vmMod.cancel = function() {
        $uibModalInstance.dismiss('close')
    }

    vmMod.delete=function(){
        var data = {
            customer_id: vmMod.obj.customer_id,
            do: 'misc--customers-delete_activity',
            id: vmMod.obj.log_id
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.success) {
                $uibModalInstance.close({})
            }
        })
    }

    if(vmMod.obj.customer_id){
        helper.doRequest('post', 'index.php', {
            'do': 'misc-myActivities',
            xget: 'customers',
            customer_id: vmMod.obj.customer_id
        }, function(r) {
            if (r && r.data) {
                vmMod.accounts = r.data;
            }
        })
    }

    if(vmMod.obj.contact_ids && Array.isArray(vmMod.obj.contact_ids) && vmMod.obj.contact_ids.length){
        helper.doRequest('post', 'index.php', {
            'do': 'misc-myActivities',
            xget: 'contacts',
            'customer_id': vmMod.obj.customer_id,
            contact_id: vmMod.obj.contact_ids
        }, function(r) {
            if (r && r.data) {
                vmMod.contacts = r.data;
            }
        })
    }


}]).controller('EditDealModalCtrl', ['data', 'helper', '$uibModalInstance', 'selectizeFc', function(data, helper, $uibModalInstance, selectizeFc) {
    var vmMod = this,
        currentTime = new Date(),
        current = undefined;
    vmMod.obj = data;
    console.log(vmMod.obj);
    vmMod.obj.cancelEdit = cancelEdit;
    vmMod.hours = [];
    vmMod.format = vmMod.obj.account_date_format;
    vmMod.dateOptions = {
        dateFormat: ''
    }
    vmMod.userAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Assign to'),
        onChange(value){
            if(value){
                vmMod.obj.to_user=this.options[value].value;
            }
        }
    });

    vmMod.boardCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        closeAfterSelect: !0,
        create: !1,
        onChange(value) {
            if (value == '' || value == undefined) {
                vmMod.obj.item.stage_id = undefined;
                vmMod.obj.item.stage_dd = []
            } else {
                var obj = {
                    'do': 'customers-deal_edit',
                    'xget': 'StagesList',
                    'board_id': value
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    vmMod.obj.item.stage_id = undefined;
                    vmMod.obj.item.stage_dd = r.data
                })
            }
        },
        maxItems: 1
    }

    function cancelEdit() {
        if (Object.keys(edit.oldObj).length == 0) {
            return edit.removeCust(edit)
        }
        edit.showTxt.address = !0;
        for (x in edit.oldObj) {
            if (edit.item[x]) {
                if (x == 'delivery_address') {
                    edit.item[x] = edit.oldObj[x].trim()
                } else {
                    edit.item[x] = edit.oldObj[x]
                }
            }
        }
        edit.item.buyer_id = edit.oldObj.buyer_id
    }
    vmMod.setExpCloseDate = function() {
        var initDate = new Date(vmMod.obj.item.exp_close_date_js).getTime() / 1000;
        vmMod.obj.item.exp_close_date_js = new Date((initDate + 7200) * 1000);
        vmMod.obj.item.exp_close_date2 = initDate
    }

    vmMod.showAlerts=function(d, formObj) {
        helper.showAlerts(vmMod, d, formObj)
    }

    vmMod.cancel = function() {
        $uibModalInstance.dismiss('close');
    }

    vmMod.save = function(scope, field) {
        if (scope.app == 'customers' && field) {
            scope.showTxt.address = !0;
        }
        scope.item.add_customer = !1;
        var data = {};
        for (x in scope.item) {
            data[x] = scope.item[x]
        }
        data = helper.clearData(data, ['APPLY_DISCOUNT_LIST', 'CURRENCY_TYPE_LIST', 'SOURCE_DD', 'STAGE_DD', 'TYPE_DD', 'accmanager', 'addresses', 'authors', 'articles_list', 'authors', 'cc', 'contacts', 'country_dd', 'language_dd', 'main_comp_info', 'sort_order']);
        data.article = scope.item.tr_id;
        data.sameAddress = scope.item.sameAddress
        data['do'] = scope.item_id != 'tmp' ? scope.app + '-' + scope.ctrlpag + '-' + scope.app + '-updateCustomerDataDeal' : data.do_request
        data.isAdd = scope.item_id != 'tmp' ? !1 : !0;
        data.buyer_id = scope.item.buyer_id;
        data.customer_id = scope.item.buyer_id;
        data.contact_id = scope.item.contact_id;
        data.item_id = scope.item_id;
        data.identity_id = scope.item.identity_id;
        data.contact_id = scope.item.contact_id;
        data.delivery_address = scope.item.delivery_address;
        data.delivery_address_id = scope.item.delivery_address_id;
        data.delivery_address_txt = scope.item.delivery_address_txt;
        data.field = field;
        helper.doRequest('post', 'index.php', data, function(r) {
            vmMod.obj.renderPage(r);
            if (r && r.success) {
                $uibModalInstance.dismiss('close');
            }
        })
    }


    vmMod.delete=function(){
        var data = {
            customer_id: vmMod.obj.customer_id,
            do: 'misc--customers-delete_activity',
            id: vmMod.obj.log_id
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.success) {
                $uibModalInstance.close({})
            }
        })
    }
}]).controller('MyActivitiesCtrl', ['$scope', '$stateParams', 'helper', 'data', 'selectizeFc', 'modalFc', function($scope, $stateParams, helper, data, selectizeFc, modalFc) {
    var ls = this,
        currentTime = new Date(),
        current = undefined;
    ls.showMoreField = !1;
    ls.showActivityForm = !1;
    ls.showActivityiny = !1;
    ls.activityTxt = helper.getLanguage('Type');
    ls.activity = {
        hd_events: !1
    }
    ls.activityNumber = 5;
    ls.hideSwitch = !1;
    ls.hours = [];
    for (var i = 0; i <= 23; i++) {
        for (var j = 0; j <= 3; j++) {
            var min = j * 15,
                text = (i < 10 ? '0' + i : i) + ':' + (min < 10 ? '0' + min : min);
            ls.hours.push({
                'id': i + '-' + min,
                'value': text
            });
            if (i <= currentTime.getHours() && min <= currentTime.getMinutes()) {
                current = i + '-' + min
            }
        }
    }
    for (x in $stateParams) {
        ls[x] = $stateParams[x]
    }
    ls.activityParams = {
        'do': 'misc-activities',
        'xget': 'Activity',
        filter_type: 'user_timeline',
        'offset': 0
    };
    for (x in data.data) {
        ls[x] = data.data[x];
        if (x == 'obj' && ls[x].birthdate) {
            ls[x].birthdate = new Date(ls[x].birthdate)
        }
    }
    ls.showAlerts = function(d, formObj) {
        helper.showAlerts(ls, d, formObj)
    };
    ls.renderPage = function(d, formObj) {
        if (d && d.data) {
            for (x in d.data) {
                ls[x] = d.data[x];
                if (x == 'obj' && ls[x].birthdate) {
                    ls[x].birthdate = new Date(ls[x].birthdate)
                }
            }
            ls.showAlerts(d, formObj)
        }
    }
    ls.renderActivities = function(r) {
        if (r.data) {
            for (x in r.data) {
                ls[x] = r.data[x]
            }
            ls.showAlerts(r)
        }
    }
    helper.doRequest('get', 'index.php', ls.activityParams, ls.renderActivities);
    ls.removeActivity = function(item, $index) {
        var data = {
            customer_id: ls.customer_id,
            do: 'misc--customers-delete_activity',
            id: item.log_id
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.success) {
                ls.activities.item.splice($index, 1)
            }
        })
    }
    ls.changeActivityStatus = function(item) {
        var data = angular.copy(ls.activityParams);
        data.do = 'misc-activities-customers-update_status';
        data.log_code = item.log_code;
        data.status_change = item.color == 'green_status' ? 0 : 1;
        helper.doRequest('post', 'index.php', data, ls.renderActivities)
    }
    ls.showActivityF = function(show, item, type) {
        ls.showActivityForm = show;
        ls.activityTxt = helper.getLanguage(item);
        ls.activity.task_type = type;
        ls.activity.type_of_call = undefined;
        ls.activity.hd_events = !1;
        ls.hideSwitch = !1;
        ls.format = ls.account_date_format;
        if (show == !1) {
            ls.activity.comment = '';
            ls.activity.log_comment = '';
            ls.activity.contact_ids = [];
            ls.activity.customer_id = '';
            ls.activity.duration_meet = '';
            ls.showAddActivity = !1
        }
        if (type == '5') {
            ls.activity.type_of_call = 0
        }
        if (type == '6') {
            ls.hideSwitch = !0;
            ls.activity.hd_events = !0
        }
        ls.activity.reminderHours = current;
        ls.activity.dateHours = current;
        ls.activity.dateEHours = current;
        ls.activity.date = new Date();
        ls.activity.reminder_event = new Date();
        ls.activity.end_d_meet = new Date();
        ls.changeTime(ls.activity.date, ls.activity.dateHours);
        ls.changeTime(ls.activity.reminder_event, ls.activity.reminderHours);
        ls.changeTime(ls.activity.end_d_meet, ls.activity.dateEHours)
    }
    ls.changeDuration = function() {
        if (ls.activity.duration_meet) {
            var d = ls.activity.duration_meet.split('-'),
                currentH = angular.copy(ls.activity.dateHours).split('-');
            ls.activity.end_d_meet.setHours(ls.activity.date.getHours() + parseInt(d[0]), ls.activity.date.getMinutes() + parseInt(d[1]));
            var minutes = parseInt(currentH[1]) + parseInt(d[1]);
            var h = parseInt(currentH[0]) + parseInt(d[0]);
            if (minutes >= 60) {
                minutes -= 60;
                h++
            }
            ls.activity.dateEHours = (h) + '-' + (minutes)
        } else {
            ls.activity.end_d_meet = new Date();
            ls.activity.dateEHours = current
        }
    }
    ls.checkAllDay = function() {
        var new_date = angular.copy(ls.activity.date),
            day = new_date.getDate();
        ls.activity.end_d_meet = new_date
    }
    ls.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        menubar: !1,
        autoresize_bottom_margin: 0,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        statusbar: !1,
        toolbar: ["styleselect | bold italic | bullist numlist "]
    };
    ls.timeAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Time'),
    });
    ls.customerAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('All Customers'),
        onChange(value) {
            if (value === undefined) {
                ls.customer2_id = '0'
            }
            ls.toggleSearchButtons('customer2_id', value)
        }
    });
    ls.accountAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select Account'),
        onType(str) {
            var _this = this;
            helper.doRequest('post', 'index.php', {
                'do': 'misc-myActivities',
                xget: 'customers',
                term: str
            }, function(r) {
                if (r && r.data) {
                    ls.accounts = r.data;
                    _this.open()
                }
            })
        },
        onFocus() {
            var _this = this;
            helper.doRequest('post', 'index.php', {
                'do': 'misc-myActivities',
                xget: 'customers'
            }, function(r) {
                if (r && r.data) {
                    ls.accounts = r.data;
                    _this.open()
                }
            })
        }
    });
    ls.contactAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select contact'),
        maxItems: 1000,
        onType(str) {
            var _this = this;
            helper.doRequest('post', 'index.php', {
                'do': 'misc-myActivities',
                xget: 'contacts',
                'customer_id': ls.activity.customer_id,
                term: str
            }, function(r) {
                if (r && r.data) {
                    ls.contacts = r.data;
                    _this.open()
                }
            })
        },
        onFocus() {
            var _this = this;
            helper.doRequest('post', 'index.php', {
                'do': 'misc-myActivities',
                xget: 'contacts',
                'customer_id': ls.activity.customer_id
            }, function(r) {
                if (r && r.data) {
                    ls.contacts = r.data;
                    _this.open()
                }
            })
        },
    });
    ls.userAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('User'),
        onChange(value) {
            ls.toggleSearchButtons('user_id', value)
        }
    });
    ls.assignAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Assign to')
    });
    ls.populateContactAcitivityList = function(item) {
        ls.contactsActitivty = [];
        for (x in item) {
            ls.contactsActitivty.push({
                'id': item[x].contact_id,
                'value': (item[x].con_firstname + ' ' + item[x].con_lastname)
            })
        }
    }
    ls.activitiesType = [{
        id: '2',
        value: helper.getLanguage('Note'),
        custom_class: 'fa fa-sticky-note'
    }, {
        id: '5',
        value: helper.getLanguage('Call'),
        custom_class: 'fa fa-phone'
    }, {
        id: '4',
        value: helper.getLanguage('Meeting'),
        custom_class: 'fa fa-calendar'
    }, {
        id: '6',
        value: helper.getLanguage('Task'),
        custom_class: 'akti-icon o_akti_checklist'
    }];
    ls.activityCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: ls.activityTxt,
        create: !1,
        maxItems: 1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline activities_type_dd">' + '<div class="row">' + '<div class="col-sm-2">' + '<a href="#"><span class="' + item.custom_class + '"></span></a>' + '</div>' + '<div class="col-sm-9">' + '<span>' + escape(item.value) + '&nbsp;</span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            ls.toggleSearchButtons('activity_id', value)
        }
    };
    ls.addActivity = function() {
        var data = angular.copy(ls.activity);
        angular.merge(data, ls.activityParams);
        data.user_id = ls.activities.user_id;
        data.do = 'misc-activities-customers-add_activity';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                ls.showAlerts(r)
            } else {
                ls.renderActivities(r);
                ls.showActivityF(!1, 'Type', '');
                ls.showActivityiny = !1
            }
        })
    }
    ls.changeTime = function(item, t, check) {
        if (item && t) {
            var d = t.split('-');
            item.setHours(d[0], d[1]);
            if (check) {
                ls.changeDuration();
                ls.checkAllDay()
            }
        }
    }
    ls.getMoreActivities = function() {
        ls.activityParams.offset++;
        helper.doRequest('get', 'index.php', ls.activityParams, function(r) {
            if (r && r.data) {
                for (x in r.data.activities.item) {
                    ls.activities.item.push(r.data.activities.item[x])
                }
                ls.activities.max_rows = r.data.activities.max_rows;
                ls.activityNumber += 5
            }
        })
    }
    ls.getActivities = function() {
        ls.activityNumber = 5;
        ls.activityParams.offset = 0;
        helper.doRequest('get', 'index.php', ls.activityParams, ls.renderActivities)
    }
    ls.toggleSearchButtons = function(item, value) {
        ls.activityParams[item] = value;
        ls.getActivities()
    }
    ls.openModal = function(type, item, size) {
        var params = {
            template: 'miscTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'NewActivities':
                var newData = {};
                newData.user_id=ls.activities.user_id;
                newData.reminder_dd=ls.reminder_dd;
                newData.filter_type = 'user_timeline';
                newData.offset = ls.activityParams.offset;
                newData.account_date_format = ls.account_date_format;
                newData.user_auto=ls.user_auto;
                params.item = newData;
                params.callback = function(r) {
                    ls.renderActivities(r)
                }
                break;
            case 'EditActivity':
                var newData = angular.copy(item);
                newData.reminder_dd=ls.reminder_dd;
                newData.allow_delete=true;
                newData.filter_type = 'user_timeline';
                newData.offset = ls.activityParams.offset;
                newData.account_date_format = ls.account_date_format;
                newData.user_auto=ls.user_auto;
                params.item = newData;
                params.callback = function(r) {
                    //ls.renderActivities(r)
                    ls.getActivities();
                }
                break
        }
        modalFc.open(params)
    }
    ls.digestData = {};
    ls.digest_periods = [{
        'id': '0',
        'value': helper.getLanguage('Weekly')
    }, {
        'id': '1',
        'value': helper.getLanguage('Monthly')
    }, {
        'id': '2',
        'value': helper.getLanguage('Custom')
    }];
    ls.digest_search = {
        'do': 'misc-my_activity_table',
        'start_date': new Date(),
        'period_id': '0',
        'from': undefined,
        'to': undefined
    };
    ls.digest_format = helper.settings.PICK_DATE_FORMAT;
    ls.digest_dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    ls.digestDatePickOpen = {
        from: !1,
        to: !1,
        start_date: !1,
    }
    ls.searchDigest = function(val) {
        if (ls.digest_search.period_id == '2' && ls.digest_search.to != undefined && ls.digest_search.from == undefined) {
            return !1
        }
        if (val) {
            ls.digest_search[val] = undefined
        }
        var obj = angular.copy(ls.digest_search);
        obj.start_date = ls.digest_search.start_date.toUTCString();
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', obj, function(r) {
            ls.renderDigest(r)
        })
    };
    ls.renderDigest = function(d) {
        if (d && d.data) {
            for (x in d.data) {
                ls.digestData[x] = d.data[x]
            }
            if (ls.digestData.start_date) {
                ls.digest_search.start_date = new Date(ls.digestData.start_date)
            }
        }
    }
    ls.digestPeriodCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Period'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            ls.digest_search.start_date = new Date();
            ls.digest_search.from = undefined;
            ls.digest_search.top = undefined;
            if (value && this.options[value].id != 2) {
                ls.searchDigest()
            }
        },
        maxItems: 1
    };
    ls.navigateDigest = function(i) {
        switch (ls.digest_search.period_id) {
            case '1':
                ls.digest_search.start_date = ls.digest_search.start_date.addMonths(i);
                break;
            default:
                ls.digest_search.start_date.setDate(ls.digest_search.start_date.getDate() + (i * 7))
        }
        ls.searchDigest()
    }
}]).controller('TimesheetNoteModalCtrl', ['$scope', '$timeout', '$compile', 'helper', '$uibModalInstance', 'data', function($scope, $timeout, $compile, helper, $uibModalInstance, data) {
    var note = this;
    note.obj = data;

    if(note.obj.type == '0'){
        for(let x in note.obj.rows){
            if(note.obj.rows[x].break && note.obj.rows[x].break!='0:00'){
                note.obj.rows[x].break=convert_to_time(note.obj.rows[x].break);
            }else{
                note.obj.rows[x].break=undefined;
            }
            if(note.obj.rows[x].end_time && note.obj.rows[x].end_time!='0:00'){
                note.obj.rows[x].end_time=convert_to_time(note.obj.rows[x].end_time);
            }else{
                note.obj.rows[x].end_time=undefined;
            }
            if(note.obj.rows[x].start_time && note.obj.rows[x].start_time!='0:00'){
                note.obj.rows[x].start_time=convert_to_time(note.obj.rows[x].start_time);
            }else{
                note.obj.rows[x].start_time=undefined;
            }
        }
    }

    note.search_day = {
        search: '',
        'do': 'misc-xcomment',
        view: 0,
        xget: '',
        offset: 1
    };

    function calculate_hours() {
        for (x in note.obj.rows) {
            if (!note.obj.rows[x].is_total_h) {
                let start_time = note.obj.rows[x].start_time == undefined ? 0 : parseFloat(convert_value(note.obj.rows[x].start_time.getHours()+':'+note.obj.rows[x].start_time.getMinutes()));
                let end_time = note.obj.rows[x].end_time == undefined ? 0 : parseFloat(convert_value(note.obj.rows[x].end_time.getHours()+':'+note.obj.rows[x].end_time.getMinutes()));
                let break_time = note.obj.rows[x].break == undefined ? 0 : parseFloat(convert_value(note.obj.rows[x].break.getHours()+':'+note.obj.rows[x].break.getMinutes()));
                var line_h = end_time - start_time - break_time;
                if (line_h > 0) {
                    note.obj.rows[x].line_h_wo = line_h;
                    note.obj.rows[x].line_h = corect_val(line_h)
                } else {
                    note.obj.rows[x].line_h_wo = 0;
                    note.obj.rows[x].line_h = ''
                }
            }
        }
    }
    if (note.obj.type == '0') {
        $timeout(function() {
            angular.element('.modal-dialog').removeClass('modal-md').addClass('modal-lg');
            calculate_hours()
        })
    }
    note.calculate_hours = calculate_hours;
    note.showAlerts = function(d) {
        helper.showAlerts(note, d)
    }
    note.orderBY = function(p) {
        if (p == note.ord && note.reverse) {
            note.ord = '';
            note.reverse = !1
        } else {
            if (p == note.ord) {
                note.reverse = !note.reverse
            } else {
                note.reverse = !1
            }
            note.ord = p
        }
        var params = {
            view: note.activeView,
            offset: note.search_day.offset,
            desc: note.reverse,
            order_by: note.ord,
            task_time_id: note.obj.task_time_id
        };
        for (x in note.search_day) {
            params[x] = note.search_day[x]
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            note.obj = r.data
            for(let x in note.obj.rows){
                if(note.obj.rows[x].break && note.obj.rows[x].break!='0:00'){
                    note.obj.rows[x].break=convert_to_time(note.obj.rows[x].break);
                }else{
                    note.obj.rows[x].break=undefined;
                }
                if(note.obj.rows[x].end_time && note.obj.rows[x].end_time!='0:00'){
                    note.obj.rows[x].end_time=convert_to_time(note.obj.rows[x].end_time);
                }else{
                    note.obj.rows[x].end_time=undefined;
                }
                if(note.obj.rows[x].start_time && note.obj.rows[x].start_time!='0:00'){
                    note.obj.rows[x].start_time=convert_to_time(note.obj.rows[x].start_time);
                }else{
                    note.obj.rows[x].start_time=undefined;
                }
            }
        })
    };
    note.cancel = function() {
        $uibModalInstance.close()
    }
    note.addTimeRow = function() {
        if(tinyMCE && tinyMCE.activeEditor){
            var data = {};
            data.rows = note.obj.rows;
            for (x in note.obj.rows) {
                data.rows[x].line_h_wo = '0';
                data.rows[x].is_total_h = !1;
                data.rows[x].start_time_i = note.obj.rows[x].start_time == undefined ? 0 : parseFloat(convert_value(corect_val(note.obj.rows[x].start_time.getHours()+':'+note.obj.rows[x].start_time.getMinutes())));
                data.rows[x].end_time_i = note.obj.rows[x].end_time == undefined ? 0 : parseFloat(convert_value(corect_val(note.obj.rows[x].end_time.getHours()+':'+note.obj.rows[x].end_time.getMinutes())));
                data.rows[x].break_i = note.obj.rows[x].break == undefined ? 0 : parseFloat(convert_value(corect_val(note.obj.rows[x].break.getHours()+':'+note.obj.rows[x].break.getMinutes())));
                data.rows[x].line_h_i = parseFloat(convert_value(corect_val(note.obj.rows[x].line_h)))
            }
            data.task_time_id = note.obj.task_time_id;
            data.do = 'misc--time-saveTimeLogs';
            helper.doRequest('post', 'index.php', data, function(r) {
                var tmp_data = angular.copy(note.obj);
                tmp_data.do = 'misc-xcomment-time-addSubHoursPlus';
                helper.doRequest('post', 'index.php', tmp_data, function(o) {
                    note.obj = o.data;
                    for(let x in note.obj.rows){
                        if(note.obj.rows[x].break && note.obj.rows[x].break!='0:00'){
                            note.obj.rows[x].break=convert_to_time(note.obj.rows[x].break);
                        }else{
                            note.obj.rows[x].break=undefined;
                        }
                        if(note.obj.rows[x].end_time && note.obj.rows[x].end_time!='0:00'){
                            note.obj.rows[x].end_time=convert_to_time(note.obj.rows[x].end_time);
                        }else{
                            note.obj.rows[x].end_time=undefined;
                        }
                        if(note.obj.rows[x].start_time && note.obj.rows[x].start_time!='0:00'){
                            note.obj.rows[x].start_time=convert_to_time(note.obj.rows[x].start_time);
                        }else{
                            note.obj.rows[x].start_time=undefined;
                        }
                    }
                    note.showEditor(note.obj.rows.length-1);
                })
            })
        }else{
            //angular.element('.loading_alt').removeClass('hidden');
            var data = angular.copy(note.obj);
            data.do = 'misc-xcomment-time-addSubHoursPlus';
            helper.doRequest('post', 'index.php', data, function(r) {
                note.obj = r.data;
                for(let x in note.obj.rows){
                    if(note.obj.rows[x].break && note.obj.rows[x].break!='0:00'){
                        note.obj.rows[x].break=convert_to_time(note.obj.rows[x].break);
                    }else{
                        note.obj.rows[x].break=undefined;
                    }
                    if(note.obj.rows[x].end_time && note.obj.rows[x].end_time!='0:00'){
                        note.obj.rows[x].end_time=convert_to_time(note.obj.rows[x].end_time);
                    }else{
                        note.obj.rows[x].end_time=undefined;
                    }
                    if(note.obj.rows[x].start_time && note.obj.rows[x].start_time!='0:00'){
                        note.obj.rows[x].start_time=convert_to_time(note.obj.rows[x].start_time);
                    }else{
                        note.obj.rows[x].start_time=undefined;
                    }
                }
                note.showEditor(note.obj.rows.length-1);
            })
        }      
    }
    note.deleteEntry = function(index) {
        if (note.obj.disabled) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Cannot delete submitted data")
                },
                "notice": !1,
                "success": !1
            };
            note.showAlerts(obj);
            return !1
        }
        if (note.obj.rows[index].id == '0') {
            note.obj.rows.splice(index, 1)
        } else {
            helper.doRequest('post', 'index.php', {
                'do': 'misc--time-deleteSubHours',
                'id': note.obj.rows[index].id
            }, function(r) {
                note.showAlerts(r);
                if (r.success) {
                    note.obj.rows.splice(index, 1)
                }
            })
        }
    }
    note.changeTotalHoursLine = function(index) {
        note.obj.rows[index].is_total_h = !0;
        note.obj.rows[index].line_h = corect_val(note.obj.rows[index].line_h);
        note.obj.rows[index].line_h_wo = convert_value(note.obj.rows[index].line_h);
        note.obj.rows[index].start_time = undefined;
        note.obj.rows[index].end_time = undefined;
        note.obj.rows[index].break = undefined;
    }
    note.savepickerData = function(index) {
        note.obj.rows[index].line_h = '0:00';
        note.obj.rows[index].line_h_wo = '0';
        note.obj.rows[index].is_total_h = !1;
        //note.obj.rows[index].start_time = corect_val(note.obj.rows[index].start_time);
        //note.obj.rows[index].end_time = corect_val(note.obj.rows[index].end_time);
        //note.obj.rows[index].break = corect_val(note.obj.rows[index].break);
        var start_time = note.obj.rows[index].start_time == undefined ? 0 : parseFloat(convert_value(note.obj.rows[index].start_time.getHours()+':'+note.obj.rows[index].start_time.getMinutes()));
        var end_time = note.obj.rows[index].end_time == undefined ? 0 : parseFloat(convert_value(note.obj.rows[index].end_time.getHours()+':'+note.obj.rows[index].end_time.getMinutes()));
        var break_time = note.obj.rows[index].break == undefined ? 0 : parseFloat(convert_value(note.obj.rows[index].break.getHours()+':'+note.obj.rows[index].break.getMinutes()));
        if (start_time == 0 || end_time == 0) {
            return !1
        } else {
            note.calculate_hours()
        }
    }
    note.showTime = function(event, index, model, align) {
        var _this = event.target;
        angular.element('.table_orders').find('timepick').remove();
        var compiledHTML = $compile('<timepick nindex="' + index + '" nmodel="' + model + '" nalign="' + align + '" "></timepick>')($scope);
        angular.element(_this).parent().after(compiledHTML)
    }
    note.updateNote = function(index) {
        var data = {};
        data.do = 'misc--time-updateDayComment';
        data.id = note.obj.rows[index].id;
        data.notes = note.obj.rows[index].comment;
        helper.doRequest('post', 'index.php', data, function(r) {
            note.showAlerts(r)
        })
    }
    note.ok = function() {
        var data = angular.copy(note.obj);
        data.do = data.do_next;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            note.showAlerts(r);
            if (r.success) {
                note.obj = r.data;
                for(let x in note.obj.rows){
                    if(note.obj.rows[x].break && note.obj.rows[x].break!='0:00'){
                        note.obj.rows[x].break=convert_to_time(note.obj.rows[x].break);
                    }else{
                        note.obj.rows[x].break=undefined;
                    }
                    if(note.obj.rows[x].end_time && note.obj.rows[x].end_time!='0:00'){
                        note.obj.rows[x].end_time=convert_to_time(note.obj.rows[x].end_time);
                    }else{
                        note.obj.rows[x].end_time=undefined;
                    }
                    if(note.obj.rows[x].start_time && note.obj.rows[x].start_time!='0:00'){
                        note.obj.rows[x].start_time=convert_to_time(note.obj.rows[x].start_time);
                    }else{
                        note.obj.rows[x].start_time=undefined;
                    }
                }
            }
        })
    }
    note.saveTimeLogs = function() {
        var data = {};
        data.rows = note.obj.rows;
        for (x in note.obj.rows) {
            data.rows[x].line_h_wo = '0';
            data.rows[x].is_total_h = !1;
            data.rows[x].start_time_i = note.obj.rows[x].start_time == undefined ? 0 : parseFloat(convert_value(corect_val(note.obj.rows[x].start_time.getHours()+':'+note.obj.rows[x].start_time.getMinutes())));
            data.rows[x].end_time_i = note.obj.rows[x].end_time == undefined ? 0 : parseFloat(convert_value(corect_val(note.obj.rows[x].end_time.getHours()+':'+note.obj.rows[x].end_time.getMinutes())));
            data.rows[x].break_i = note.obj.rows[x].break == undefined ? 0 : parseFloat(convert_value(corect_val(note.obj.rows[x].break.getHours()+':'+note.obj.rows[x].break.getMinutes())));
            data.rows[x].line_h_i = parseFloat(convert_value(corect_val(note.obj.rows[x].line_h)))
        }
        data.task_time_id = note.obj.task_time_id;
        data.do = 'misc--time-saveTimeLogs';
        helper.doRequest('post', 'index.php', data, function(r) {
            note.showAlerts(r)
        })
    }
    note.tinymceOptions = {
        selector: ".variable_assign",
        resize: "height",
        menubar: !1,
        autoresize_bottom_margin: 0,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        toolbar: ["styleselect | bold italic | bullist numlist "]
    }

    note.showEditor=function(index){
        if(note.obj.disabled){
            note.obj.rows[index].showEditor = false
        }else{
            note.obj.rows[index].showEditor = true
            $timeout(function() {
                tinyMCE.editors[index].selection.select(tinyMCE.editors[index].getBody(), true);
                tinyMCE.editors[index].selection.collapse(false);
                tinyMCE.editors[index].focus()
            },700)       
        }
    }

    if(!note.obj.disabled && note.obj.type=='0'){
        if(!note.obj.rows.length){
            note.addTimeRow()
        }else if(note.obj.rows.length == 1){
            note.showEditor(0);
        }        
    }

}]).controller('SubmitTimeModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var time = this;
    time.obj = data;
    time.obj.partial_full_radio = '1';
    time.showAlerts = function(d) {
        helper.showAlerts(time, d)
    }
    time.cancel = function() {
        $uibModalInstance.close()
    }
    time.showPartialTime = function() {
        time.showPartial = !0;
        time.obj.partial_end = time.obj.partial_submit[parseInt(time.obj.TODAY_nr) - 1].id.toString()
    }
    time.ok = function() {
        var data = angular.copy(time.obj);
        data.do = 'misc-timesheet-time-submit_hour';
        if (time.obj.partial_full_radio == '2') {
            data.week_e = time.obj.partial_end
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            time.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('PrintTimeModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var time = this;
    time.obj = data;
    time.showAlerts = function(d) {
        helper.showAlerts(time, d)
    }
    time.cancel = function() {
        $uibModalInstance.close()
    }
    time.ok = function() {
        var data = {
            'customer_id': time.obj.customer_id,
            'logo': time.obj.default_logo,
            't_type': time.obj.pdf_layout
        };
        $uibModalInstance.close(data)
    }
}]).controller('SelectTaskModalCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, $timeout, helper, $uibModalInstance, data, modalFc) {
    var tsk = this,
        typePromise;
    tsk.obj = data;
    tsk.title=tsk.obj.t_type=='2' ? helper.getLanguage('Add Service') : helper.getLanguage('Add task');
    tsk.projectAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select project'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value == undefined || value == '') {
                var obj = {
                    'do': 'misc-timesheet',
                    xget: 'tasks_list'
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    tsk.obj.auto_task = r.data
                })
            } else if (value != undefined) {
                var obj = {
                    'do': 'misc-timesheet',
                    xget: 'tasks_list',
                    project_id: this.options[value].id
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    tsk.obj.auto_task = r.data
                })
            }
        },
        onType(str) {
            var selectize = angular.element('#p_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'misc-timesheet',
                    xget: 'projects_list',
                    term: str,
                    user_id: tsk.obj.item.user_id
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    if (r.data.length) {
                        tsk.obj.auto_project = r.data;
                        selectize.open()
                    }
                })
            }, 300)
        },
        maxItems: 1
    };
    tsk.customerAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select customer'),
        create: !1,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-8">' + '<strong class="text-ellipsis">' + item.symbol + '  ' + escape(item.top) + '</strong>' + '<small class="text-ellipsis"><span class="glyphicon glyphicon-map-marker"></span>' + escape(item.bottom) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-muted text-right">' + '<small class="text-ellipsis"> ' + escape(item.right) + '</small>' + '</div>' + '</div>' + '</div>';
                if (item.id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Create a new Customer') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.top) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined || value == '') {
                var obj = {
                    'do': 'misc-timesheet',
                    xget: 'tasks_list'
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    tsk.obj.auto_task = r.data
                })
            } else if (value != undefined) {
                if (value == '99999999999') {
                    tsk.obj.item.c_id = '';
                    openModal('add_customer', '', 'md');
                    return !1
                } else {
                    if(tsk.obj.t_type=='2'){
                        var obj = {
                            'do': 'misc-timesheet',
                            xget: 'articles_list',
                            'only_service': !0,
                            'buyer_id':tsk.obj.item.c_id
                        };
                        helper.doRequest('get', 'index.php', obj, function(r) {
                            tsk.obj.auto_service = r.data.lines;
                        })
                    }else{
                        var obj = {
                            'do': 'misc-timesheet',
                            xget: 'tasks_list',
                            c_id: this.options[value].id
                        };
                        helper.doRequest('get', 'index.php', obj, function(r) {
                            tsk.obj.auto_task = r.data
                        })
                    }                  
                }
            }
        },
        onType(str) {
            var selectize = angular.element('#c_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'misc-timesheet',
                    xget: 'cc_list',
                    term: str
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    if (r.data.length) {
                        tsk.obj.auto_customer = r.data;
                        selectize.open()
                    }
                })
            }, 300)
        },
        maxItems: 1
    };
    tsk.taskAutoCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select task'),
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-12">' + '<strong class="text-ellipsis">' + escape(item.name) + '</strong>' + '</div>' + '</div>' + '</div>';
                if (item.id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> <span class="sel_text">' + helper.getLanguage('Create a new Task') + '</span></div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.name) + '</div>'
            }
        },
        onChange(value) {
            if (Array.isArray(value) && value[value.length - 1] == '99999999999') {
                openModal('add_task', '', 'md');
                var selectize = angular.element('#task_id')[0].selectize;
                selectize.removeItem('99999999999');
                return !1
            }
        },
        maxItems: 100
    };
    tsk.serviceAutoCfg={
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Service'),
        create: !1,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7 text-left">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + ' </small>' + '</div>' + '<div class="col-sm-4 text-right">' + (helper.displayNr(item.price)) + '</div>' + '</div>' + '</div>';
                return html
            }
        },
        onType(str) {
            var selectize = angular.element('#services_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'misc-timesheet',
                    xget: 'articles_list',
                    search: str,
                    'only_service': !0,
                    'buyer_id':tsk.obj.item.c_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    tsk.obj.auto_service = r.data.lines;
                    if (tsk.obj.auto_service.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value) {
            if (value == undefined || value == '') {
                var data = {
                    'do': 'misc-timesheet',
                    xget: 'articles_list',
                    'only_service': !0,
                    'buyer_id':tsk.obj.item.c_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    tsk.obj.auto_service = r.data.lines;
                })
            }
        },
        maxItems: 1
    };
    tsk.showAlerts = function(d) {
        helper.showAlerts(tsk, d)
    }
    tsk.cancel = function() {
        $uibModalInstance.close()
    }
    tsk.ok = function() {
        if (tsk.obj.t_type == '1') {
            if (!tsk.obj.item.c_id || tsk.obj.item.tasks_id == undefined || tsk.obj.item.tasks_id[0] == '0') {
                var obj = {
                    "error": {
                        "error": helper.getLanguage("Select customer and task")
                    },
                    "notice": !1,
                    "success": !1
                };
                tsk.showAlerts(obj);
                return !1
            }
        } else if (tsk.obj.t_type == '2') {
            if (!tsk.obj.item.c_id || tsk.obj.item.tasks_id == undefined) {
                var obj = {
                    "error": {
                        "error": helper.getLanguage("Select customer and service")
                    },
                    "notice": !1,
                    "success": !1
                };
                tsk.showAlerts(obj);
                return !1
            }
        }else {
            if (!tsk.obj.item.project_id || tsk.obj.item.tasks_id == undefined || tsk.obj.item.tasks_id[0] == '0') {
                var obj = {
                    "error": {
                        "error": helper.getLanguage("Select project and task")
                    },
                    "notice": !1,
                    "success": !1
                };
                tsk.showAlerts(obj);
                return !1
            }
        }
        if (tsk.obj.activeType == 1) {
            var tasks_ids = tsk.obj.t_type=='2' ? angular.copy([tsk.obj.item.tasks_id]) : angular.copy(tsk.obj.item.tasks_id);
            multiTime(tasks_ids, tsk.obj.t_type)
        } else {
            var data = angular.copy(tsk.obj.item);
            if(tsk.obj.t_type == '2'){
                data.tasks_id=[data.tasks_id];
            }
            data.task_type= tsk.obj.t_type;
            data.do = 'misc-timesheet-time-add_tasks';
            data.xget = 'Week';
            //angular.element('.loading_alt').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                //angular.element('.loading_alt').addClass('hidden');
                if (r.success) {
                    $uibModalInstance.close(r)
                } else {
                    tsk.showAlerts(r)
                }
            })
        }
    }

    function multiTime(array, type) {
        if (array.length) {
            //angular.element('.loading_alt').removeClass('hidden');
            var data = {};
            data.do = 'misc--time-addSubHoursPlus';
            data.date = tsk.obj.item.date;
            if (type == '1' || type == '2') {
                data.c_id = tsk.obj.item.c_id;              
            } else {
                data.project_id = tsk.obj.item.project_id
            }
            data.task_type=type;
            data.task_id = array[0];
            data.user_id = tsk.obj.item.user_id;
            data.day_row = tsk.obj.item.day_row;
            helper.doRequest('post', 'index.php', data, function(r) {
                array.shift();
                multiTime(array, type)
            })
        } else {
            helper.doRequest('get', 'index.php', {
                'do': 'misc-timesheet',
                'xget': 'Day',
                'start_date': tsk.obj.item.start_date.toUTCString(),
                'user_id': tsk.obj.item.user_id
            }, function(r) {
                //angular.element('.loading_alt').addClass('hidden');
                $uibModalInstance.close(r)
            })
        }
    }

    function openModal(type, item, size) {
        var params = {
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'add_customer':
                params.template = 'miscTemplate/AddCustomerModal';
                params.ctrl = 'AddCustomerModalCtrl';
                params.ctrlAs = 'vmMod';
                params.params = {
                    'do': 'misc-cc_new'
                };
                params.callback = function(r) {
                    if (r && r.data.buyer_id) {
                        var obj = {
                            'do': 'misc-timesheet',
                            xget: 'cc_list',
                            term: r.data.buyer_name
                        };
                        helper.doRequest('get', 'index.php', obj, function(o) {
                            if (o.data.length) {
                                var selectize = angular.element('#c_list')[0].selectize;
                                tsk.obj.auto_customer = o.data;
                                $timeout(function() {
                                    tsk.obj.item.c_id = r.data.buyer_id;
                                    selectize.close();
                                    var obj1 = {
                                        'do': 'misc-timesheet',
                                        xget: 'tasks_list',
                                        c_id: r.data.buyer_id
                                    };
                                    helper.doRequest('get', 'index.php', obj1, function(p) {
                                        tsk.obj.auto_task = p.data
                                    })
                                })
                            }
                        })
                    }
                }
                break;
            case 'add_task':
                params.template = 'miscTemplate/TimeTaskAddModal';
                params.ctrl = 'TimeTaskAddModalCtrl';
                params.ctrlAs = 'vm';
                params.callback = function(r) {
                    if (r.data.task_id) {
                        var obj = {
                            'do': 'misc-timesheet',
                            xget: 'tasks_list',
                            c_id: tsk.obj.item.c_id
                        };
                        helper.doRequest('get', 'index.php', obj, function(o) {
                            if (o.data.length) {
                                tsk.obj.auto_task = o.data
                            }
                        })
                    }
                }
                break
        }
        modalFc.open(params)
    }
}]).controller('TimeExpenseModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var time = this;
    time.obj = data;
    time.showAlerts = function(d, formObj) {
        helper.showAlerts(time, d, formObj)
    }
    time.cancel = function() {
        $uibModalInstance.close()
    }
    time.ok = function(formObj) {
        helper.uploadSingleItem(time, time.obj, 'loading_alt', formObj)
    }
    time.projectCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select project'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value == undefined || value == '') {
                var obj = {
                    'do': 'misc-timesheet_expense',
                    xget: 'categories'
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    time.obj.categories_list = r.data
                })
            } else if (value != undefined) {
                var obj = {
                    'do': 'misc-timesheet_expense',
                    xget: 'categories',
                    project_id: this.options[value].id
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    time.obj.categories_list = r.data
                })
            }
        },
        maxItems: 1
    };
    time.companyCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select company'),
        create: !1,
        closeAfterSelect: !0,
        maxItems: 1
    };
    time.catCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select category'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value == undefined || value == '') {
                time.obj.billable = !1
            } else if (value != undefined) {
                time.obj.billable = this.options[value].billable_expense
            }
        },
        maxItems: 1
    }
}]).controller('ShowRecieptModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var rec = this;
    rec.img = data;
    rec.showAlerts = function(d) {
        helper.showAlerts(rec, d)
    }
    rec.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('calendarCtrl', ['$scope', '$compile', '$timeout', '$uibModal', 'helper', 'modalFc', 'data', function($scope, $compile, $timeout, $uibModal, helper, modalFc, data) {
    console.log('General calendar');
    $scope.activeType = 3;
    $scope.activeView = 0;
    $scope.activeActivity = 0;
    $scope.types = [{
        name: helper.getLanguage('Weekly'),
        id: 2,
        active: ''
    }, {
        name: helper.getLanguage('Monthly'),
        id: 3,
        active: 'active'
    }];
    $scope.views = [{
        name: helper.getLanguage('Open'),
        id: 7,
        active: ''
    },{
        name: helper.getLanguage('Not Started'),
        id: 2,
        active: ''
    }, {
        name: helper.getLanguage('In progress'),
        id: 3,
        active: ''
    }, {
        name: helper.getLanguage('On hold'),
        id: 4,
        active: ''
    }, {
        name: helper.getLanguage('Late'),
        id: 5,
        active: ''
    }, {
        name: helper.getLanguage('Completed'),
        id: 6,
        active: ''
    }, {
        name: helper.getLanguage('Archived'),
        id: -1
    },{
        name: helper.getLanguage('All Statuses'),
        id: 0,
        active: ''
    }];
    $scope.viewsActivities = [{
        name: helper.getLanguage('All Activities'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Note'),
        id: 8,
        active: ''
    }, {
        name: helper.getLanguage('Call'),
        id: 9,
        active: ''
    }, {
        name: helper.getLanguage('E-mail'),
        id: 10,
        active: ''
    }, {
        name: helper.getLanguage('Meeting'),
        id: 11,
        active: ''
    }, {
        name: helper.getLanguage('Task'),
        id: 12,
        active: ''
    }];
    $scope.item = {};
    $scope.search_list = {
        search: '',
        'do': 'misc-time_calendar',
        xget: 'list',
        offset: 1
    };
    $scope.search_task = {
        view: '7',
        activity: '0',
        search: '',
        'do': 'misc-time_calendar',
        xget: 'tasks',
        offset: 1,
        start_d: undefined,
        stop_d: undefined,
        user_done: undefined
    };
    $scope.calendar = {
        start: 0
    };
    $scope.listStats = [];
    $scope.renderPage = function(d) {
        delete $scope.scroll_config;
        delete $scope.scroll_config_in;
        if (d.data) {
            $scope.item = d.data;
            if (d.data.start_date) {
                $scope.item.start_date = new Date(d.data.start_date)
            } else {
                $scope.item.start_date = new Date()
            }
            if ($scope.item.task_nr > 0) {
                $('.header_task_number').html('<ins>' + $scope.item.task_nr + '</ins>')
            } else {
                $('.header_task_number').addClass('hide')
            }
            if ($scope.activeType == 2 && $scope.item.tab == 1) {
                $timeout(function() {
                    $scope.scroll_config = {
                        autoHideScrollbar: !1,
                        theme: 'minimal-dark',
                        scrollInertia: 50,
                        setTop: '-234px'
                    }
                })
            }
            if ($scope.item.tab == 3) {
                $scope.search_task.user_done = d.data.user_done;
                $scope.search_task.user_id=d.data.user_id;
            }
            if(d.data.showGdpr){
                $scope.openModal("GDPRPolicy", {})
            }
        }
    }
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_d: !1,
        stop_d: !1,
    }
    $scope.get_calendar = function() {
        $scope.activeType = 3;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php?do=misc-time_calendar&view=0', $scope.renderPage)
    }
    $scope.get_list = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php?do=misc-time_calendar&xget=list', $scope.renderPage)
    }
    $scope.ord='reminder_date';
    $scope.get_tasks = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        $scope.ord='reminder_date';
        $scope.reverse=!1;;
        helper.doRequest('get', 'index.php?do=misc-time_calendar&view=7&xget=tasks&order_by=reminder_date', $scope.renderPage)
    }
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.orderBY = function(p) {
        if (p == $scope.ord && $scope.reverse) {
            $scope.ord = '';
            $scope.reverse = !1
        } else {
            if (p == $scope.ord) {
                $scope.reverse = !$scope.reverse
            } else {
                $scope.reverse = !1
            }
            $scope.ord = p
        }
        if ($scope.item.tab == 2) {
            var params = {
                view: '1',
                offset: $scope.search_list.offset,
                desc: $scope.reverse,
                order_by: $scope.ord,
                start_date: $scope.item.start_date.toUTCString()
            };
            for (x in $scope.search_list) {
                params[x] = $scope.search_list[x]
            }
        } else if ($scope.item.tab == 3) {
            var params = {
                offset: $scope.search_task.offset,
                desc: $scope.reverse,
                order_by: $scope.ord,
                start_date: $scope.item.start_date.toUTCString()
            };
            for (x in $scope.search_task) {
                params[x] = $scope.search_task[x]
            }
        }
        params.user_id = $scope.item.user_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderPage(r)
        })
    };
    $scope.searchThing = function() {
        var data = {};
        if ($scope.item.tab == 2) {
            data = angular.copy($scope.search_list)
        } else if ($scope.item.tab == 3) {
            data = angular.copy($scope.search_task);
            data.order_by = $scope.ord;
            data.desc=$scope.reverse;
        }
        data.user_id = $scope.item.user_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.renderPage(r)
        })
    };
    $scope.toggleSearchButtons = function(item, val) {
        if ($scope.item.tab == 2) {
            $scope.search_list[item] = val;
            var params = {
                view: '1',
                offset: $scope.search_list.offset
            };
            for (x in $scope.search_list) {
                params[x] = $scope.search_list[x]
            }
        } else if ($scope.item.tab == 3) {
            $scope.search_task[item] = val;
            var params = {
                offset: $scope.search_task.offset
            };
            for (x in $scope.search_task) {
                params[x] = $scope.search_task[x]
            }
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderPage(r)
        })
    };
    $scope.format = 'dd/MM/yyyy';
    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    $scope.datePickOpen = {
        start_date: !1
    }
    $scope.customerCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Users'),
        create: !1,
        closeAfterSelect: !0,
        onChange(value) {
            if (value != undefined) {
                var type_r = '';
                switch ($scope.activeType) {
                    case '2':
                        type_r = 'Week';
                        break;
                    case '3':
                        type_r = 'Month';
                        break
                }
                $scope.calendar.start = 0;
                if ($scope.item.tab == 1) {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php', {
                        'do': 'misc-time_calendar',
                        'xget': type_r,
                        'start_date': $scope.item.start_date.toUTCString(),
                        'user_id': this.options[value].id
                    }, function(r) {
                        $scope.renderPage(r)
                    })
                } else if ($scope.item.tab == 3) {
                    $scope.searchThing()
                } else {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php', {
                        'do': 'misc-time_calendar',
                        'xget': 'list',
                        'user_id': this.options[value].id
                    }, function(r) {
                        $scope.renderPage(r)
                    })
                }
            } else {
                if ($scope.item.tab == 3) {
                    $scope.searchThing()
                }
            }
        },
        maxItems: 1
    };
    $scope.typeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Overview'),
        create: !1,
        onChange(value) {
            var type_r = '';
            switch (value) {
                case '2':
                    type_r = 'Week';
                    break;
                case '3':
                    type_r = 'Month';
                    break
            }
            $scope.calendar.start = 0;
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', {
                'do': 'misc-time_calendar',
                'xget': type_r,
                'start_date': $scope.item.start_date.toUTCString(),
                'user_id': $scope.item.user_id
            }, function(r) {
                $scope.renderPage(r)
            })
        },
        maxItems: 1
    };
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Statuses'),
        create: !1,
        onChange(value) {
            if (value == undefined) {
                value = 0
            }
            $scope.search_task.archived = 0;
            if (value == -1) {
                $scope.search_task.archived = 1
            }
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.activitiesCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Activities'),
        create: !1,
        onChange(value) {
            $scope.searchThing()
        },
        maxItems: 1
    };
    $scope.navigate = function(i) {
        $scope.item.start_date.setDate($scope.item.start_date.getDate() + (i * 7));
        $scope.calendar.start = $scope.calendar.start + i;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'misc-time_calendar',
            'xget': 'Week',
            'start': $scope.calendar.start,
            'start_date': $scope.item.start_date.toUTCString(),
            'user_id': $scope.item.user_id
        }, function(r) {
            $scope.renderPage(r)
        })
    }
    $scope.navigate_month = function(i) {
        $scope.item.start_date = $scope.item.start_date.addMonths(i);
        $scope.calendar.start = $scope.calendar.start + i;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'misc-time_calendar',
            'xget': 'Month',
            'start': $scope.calendar.start,
            'start_date': $scope.item.start_date.toUTCString(),
            'user_id': $scope.item.user_id
        }, function(r) {
            $scope.renderPage(r)
        })
    }
    $scope.sync_data = function() {
        openModal('sync_data', '', 'md')
    }
    $scope.share_data = function() {
        openModal('share_data', '', 'md')
    }
    $scope.showMore = function(event, item) {
        if (item.data_nr == 0) {
            return !1
        }
        var _this = event.target;
        $scope.listStats = [];
        delete $scope.scroll_config_in;
        if (angular.element(_this).parents('.planning_details').hasClass('active')) {
            angular.element(_this).parents('.planning_details').removeClass('active');
            angular.element(_this).parents('.planning_details').removeClass('is_top_class');
            angular.element(_this).parents('.planning_details').find('.cell_c').empty()
        } else {
            angular.element(_this).parents('.planning_groups').find('.planning_details').removeClass('active');
            angular.element(_this).parents('.planning_groups').find('.planning_details').removeClass('is_top_class');
            angular.element(_this).parents('.planning_groups').find('.planning_details .cell_c').empty();
            var data = {};
            data.do = 'misc-time_calendar';
            data.xget = 'Day';
            data.start_date = item.data[0].tmsmp;
            data.selected_user_id = $scope.item.user_id;
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', data, function(r) {
                $scope.listStats = r.data;
                if ($scope.listStats.is_top) {
                    angular.element(_this).parents('.planning_details').addClass('is_top_class')
                }
                var new_content = '<div ng-repeat="nitem in listStats.user_data[0].all_day" class="entry {{nitem.phour}} {{nitem.pwidth}} {{nitem.pduration}} {{nitem.colors}}">' + '<p class="text-ellipsis"><span ng-bind-html="nitem.subject" title="{{nitem.type}}" ng-click="showEventInfo(nitem)"></span> <a href="#" ng-show="nitem.is_nylas && listStats.nylas_active && item.user_id == item.logged_user" confirm="' + helper.getLanguage("Delete action will be triggered also in the email attached") + "! " + helper.getLanguage("Are you sure, you wanna delete this entry?") + '" ng-click="deleteNylasEvent(nitem)"><span class="glyphicon glyphicon-trash"></span></a></p></div>' + '<div class="planning_details_body day-preview" ng-scrollbars ng-scrollbars-config="scroll_config_in">' + '<div class="planning_cell_alt" ng-repeat="nitem2 in listStats.days_nr">{{nitem2.day}}</div>' + '<div ng-repeat="nitem3 in listStats.user_data[0].data" class="entry {{nitem3.phour}} {{nitem3.pduration}} {{nitem3.pwidth}} {{nitem3.colors}}">' + '<small><span ng-click="showEventInfo(nitem3)">{{nitem3.start_time}}</span> <span ng-if="nitem3.end_time" ng-click="showEventInfo(nitem3)">- {{nitem3.end_time}}</span> <a href="#" ng-show="nitem3.is_nylas && listStats.nylas_active && item.user_id == item.logged_user" confirm="' + helper.getLanguage("Delete action will be triggered also in the email attached") + "! " + helper.getLanguage("Are you sure, you wanna delete this entry?") + '" ng-click="deleteNylasEvent(nitem3)"><span class="glyphicon glyphicon-trash"></span></a></small>' + '<p class="text-ellipsis" ng-click="showEventInfo(nitem3)"><span ng-bind-html="nitem3.subject" title="{{nitem3.type}}"></span></p>' + '</div>' + '</div>';
                angular.element(_this).parents('.planning_details').find('.cell_c').append(new_content);
                $compile(angular.element(_this).parents('.planning_details').find('a').nextAll())($scope);
                angular.element(_this).parents('.planning_details').addClass('active');
                $timeout(function() {
                    $scope.scroll_config_in = {
                        autoHideScrollbar: !1,
                        theme: 'minimal-dark',
                        axis: 'y',
                        scrollInertia: 50,
                        setTop: '-234px',
                        setHeight: '273px'
                    }
                })
            })
        }
    }
    $scope.changeDate = function() {
        var type_r = '';
        switch ($scope.activeType) {
            case '2':
                type_r = 'Week';
                break
        }
        $scope.calendar.start = 0;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'misc-time_calendar',
            'xget': type_r,
            'start_date': $scope.item.start_date.toUTCString(),
            'user_id': $scope.item.user_id
        }, function(r) {
            $scope.renderPage(r)
        })
    }
    $scope.showEventInfo = function(item) {
        if (item.is_service) {
            $scope.modal(item, 'template/viewIntervention', 'viewInterventionCtrl')
        } else if (!item.is_nylas && !item.is_log) {
            $scope.showListInfo(item)
        } else if (!item.is_nylas && item.is_log) {
            openModal("message_view", item.id, "md")
        } else {
            openModal("nylas_view", item.id, "md")
        }
    }
    $scope.showListInfo = function(item) {
        if (item.is_service) {
            $scope.modal(item, 'template/viewIntervention', 'viewInterventionCtrl')
        } else {
            openModal("meeting_view", item.id, "md")
        }
    }
    $scope.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl,
            size: size ? size : 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem
        }, function() {})
    };
    $scope.setAsDone = function(item) {
        var data = {};
        data.id = item.id;
        data.done = item.done;
        data.do = 'misc--time-markDoneLog';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.showAlerts(r)
        })
    }
    $scope.markAsRead = function(item) {
        var data = {};
        data.id = item.id;
        data.read = item.read;
        for (x in $scope.search_task) {
            data[x] = $scope.search_task[x]
        }
        data.order_by=$scope.ord;
        data.desc=$scope.reverse;
        data.do = 'misc-time_calendar-time-markReadLog';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.showAlerts(r);
            $scope.renderPage(r)
        })
    }
    $scope.deleteNylasEvent = function(item) {
        var type_r = '';
        switch ($scope.activeType) {
            case '2':
                type_r = 'Week';
                break;
            case '3':
                type_r = 'Month';
                break
        }
        $scope.calendar.start = 0;
        var data = {
            'do': 'misc-time_calendar-time-deleteNylasEvent',
            'xget': type_r,
            'start_date': $scope.item.start_date.toUTCString(),
            'user_id': $scope.item.user_id
        };
        data.event_id = item.event_id;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.renderPage(r)
            }
        })
    }
    $scope.action = function(link) {
        var data = {};
        for (x in $scope.search_task) {
            data[x] = $scope.search_task[x]
        }
        for (x in link) {
            data[x] = link[x]
        }
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.showAlerts(r);
            $scope.renderPage(r)
        })
    }

    $scope.editActivityData=function(item){
        openModal('EditActivity', angular.copy(item), 'md');
    }

    $scope.addNewActivity=function(){
        openModal('NewActivities', {}, 'md');
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'meeting_view':
                var iTemplate = 'MeetingView';
                var ctas = 'vm';
                break;
            case 'message_view':
                var iTemplate = 'MessageView';
                var ctas = 'vm';
                break;
            case 'nylas_view':
                var iTemplate = 'NylasView';
                var ctas = 'vm';
                break;
            case 'sync_data':
                var iTemplate = 'SyncMail';
                var ctas = 'vm';
                break;
            case 'share_data':
                var iTemplate = 'ShareCalendar';
                var ctas = 'vm';
                break
            case 'GDPRPolicy':
                var iTemplate = 'GDPRPolicy';
                var ctas = 'vm';
                break
            case 'NewActivities':
                var iTemplate = 'NewActivities';
                var ctas = 'vmMod';
                break;
            case 'EditActivity':
                var iTemplate = 'EditActivity';
                var ctas = 'vmMod';
                break;
        }
        var params = {
            template: 'miscTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'meeting_view':
                params.params = {
                    'do': 'misc-meeting_view',
                    'log_id': item
                };
                break;
            case 'message_view':
                params.params = {
                    'do': 'misc-message_view',
                    'log_id': item
                };
                params.callback = function() {
                    if ($scope.item.tab == 1) {
                        var type_r = '';
                        switch ($scope.activeType) {
                            case '2':
                                type_r = 'Week';
                                break;
                            case '3':
                                type_r = 'Month';
                                break
                        }
                        //angular.element('.loading_wrap').removeClass('hidden');
                        var data = {
                            'do': 'misc-time_calendar',
                            'xget': type_r,
                            'type_s': $scope.activeType,
                            'start_time': $scope.item.start_t,
                            'start_date': $scope.item.start_date.toUTCString(),
                            'user_id': $scope.item.user_id
                        };
                        helper.doRequest('post', 'index.php', data, function(r) {
                            $scope.renderPage(r);
                            $scope.showAlerts(r)
                        })
                    } else {
                        $scope.search_task.user_id = $scope.item.user_id;
                        var obj=angular.copy($scope.search_task);
                        obj.order_by=$scope.ord;
                        obj.desc=$scope.reverse;
                        helper.doRequest('get', 'index.php', obj, function(r) {
                            $scope.renderPage(r)
                        })
                    }
                }
                break;
            case 'nylas_view':
                params.params = {
                    'do': 'misc-nylas_view',
                    'log_id': item
                };
                break;
            case 'sync_data':
                params.params = {
                    'do': 'misc-time_calendar',
                    'xget': 'calendars',
                    'type_s': $scope.activeType,
                    'start_time': $scope.item.start_t,
                    'start_date': $scope.item.start_date.toUTCString(),
                    'user_id': $scope.item.user_id
                };
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.renderPage(d);
                        $scope.showAlerts(d)
                    }
                }
                break;
            case 'share_data':
                params.params = {
                    'do': 'misc-share_users'
                };
                break
            case 'GDPRPolicy':
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-policy_info'
                };
                break
            case 'NewActivities':
                item.filter_type = 'user_timeline';
                item.account_date_format = $scope.item.account_date_format;
                item.from_tasks=true;
                item.return_data=true;
                params.initial_item = item;
                params.params={'do':'misc-time_calendar','xget':'task','activity_id':item.log_id,'log_id':item.log_code};
                params.callback = function(r) {
                    if(r && r.data){
                        $scope.searchThing();
                    }                 
                }
                break;
            case 'EditActivity':
                item.filter_type = 'user_timeline';
                item.account_date_format = $scope.item.account_date_format;
                item.from_tasks=true;
                item.return_data=true;
                params.initial_item = item;
                params.params={'do':'misc-time_calendar','xget':'task','activity_id':item.log_id,'log_id':item.log_code};
                params.callback = function(r) {
                    if(r && r.data){
                        $scope.searchThing();
                    }                 
                }
                break;
        }
        modalFc.open(params)
    }
    $scope.renderPage(data)
}]).controller('MeetingViewModalCtrl', ['$scope', '$state', 'helper', '$uibModalInstance', 'data', function($scope, $state, helper, $uibModalInstance, data) {
    var vm = this;
    vm.item = data;
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.viewCustomer = function(c_id) {
        $uibModalInstance.close();
        $state.go('customerView', {
            'customer_id': c_id
        })
    }
    vm.viewContact = function(c_id) {
        $uibModalInstance.close();
        $state.go('contactView', {
            'contact_id': c_id
        })
    }
    vm.viewIntervention = function(service_id) {
        $uibModalInstance.close();
        $state.go('service.view', {
            'service_id': service_id
        })
    }
}]).controller('MessageViewModalCtrl', ['$scope', '$state', 'helper', '$uibModalInstance', 'data', function($scope, $state, helper, $uibModalInstance, data) {
    var vm = this;
    vm.item = data;
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d)
    }
    vm.viewCustomer = function(c_id) {
        $uibModalInstance.close();
        $state.go('customerView', {
            'customer_id': c_id
        })
    }
    vm.viewContact = function(c_id) {
        $uibModalInstance.close();
        $state.go('contactView', {
            'contact_id': c_id
        })
    }
    vm.showItemInfo = function(url, param, id) {
        $uibModalInstance.close();
        var add_on = {};
        add_on[param] = id;
        $state.go(url, add_on)
    }
    vm.changeStatus = function() {
        var data = {
            'do': 'misc--time-update_status',
            'status': vm.item.status,
            'log_id': vm.item.log_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
}]).controller('NylasViewModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.item = data;
    vm.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('SyncMailModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.item = data;
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d)
    }
    vm.ok = function() {
        if (vm.item.calendar_id == '0') {
            var obj = {
                "error": {
                    "error": helper.getLanguage("Select calendar for sync")
                },
                "notice": !1,
                "success": !1
            };
            vm.showAlerts(obj);
            return !1
        }
        var data = angular.copy(vm.item);
        data.do = 'misc-time_calendar-time-calendar_sync';
        var type_r = '';
        switch (vm.item.type_s) {
            case '2':
                type_r = 'Week';
                break;
            case '3':
                type_r = 'Month';
                break
        }
        data.xget = type_r;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            vm.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('ShareCalendarModalCtrl', ['$scope', '$state', 'helper', '$uibModalInstance', 'data', function($scope, $state, helper, $uibModalInstance, data) {
    var vm = this;
    vm.item = data;
    vm.search = {
        search: '',
        'do': 'misc-share_users',
        xget: '',
        offset: 1
    };
    vm.searchThing = function() {
        var data = angular.copy(vm.search);
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            vm.item = r.data
        })
    };
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d)
    }
    vm.share = function(type, index) {
        var data = {};
        data.do = 'misc--user-set_rights';
        if (type == '1') {
            data.right = !vm.item.list[index].read ? '0' : type;
            if (vm.item.list[index].read) {
                vm.item.list[index].read_write = !1
            }
        } else {
            data.right = !vm.item.list[index].read_write ? '0' : type;
            if (vm.item.list[index].read_write) {
                vm.item.list[index].read = !1
            }
        }
        data.to_user_id = vm.item.list[index].user_id;
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r)
        })
    }
    vm.view_user = function(user_id) {
        $uibModalInstance.close();
        $state.go('user', {
            'user_id': user_id
        })
    }
}]).controller('MyUsersCtrl', ['$scope', '$stateParams', '$timeout', 'helper', '$uibModal', 'settingsFc', 'list', 'data', 'orderByFilter', function($scope, $stateParams, $timeout, helper, $uibModal, settingsFc, list, data, orderBy) {
    var vm = this;
    vm.app = 'settings';
    vm.ctrlpag = 'SettingsCtrl';
    vm.tmpl = 'miscTemplate';
    vm.logos = {};
    vm.account_address = {};
    vm.regional = {};
    vm.financial = {};
    vm.vat = {};
    vm.language = {};
    vm.users = {
        views: [{
            name: helper.getLanguage('All Statuses'),
            id: 0,
            active: 'active'
        }, {
            name: helper.getLanguage('Active'),
            id: 1,
            active: ''
        }, {
            name: helper.getLanguage('Inactive'),
            id: 2,
            active: ''
        }],
        activeView: 1,
        search: {
            search: '',
            'do': 'settings-settings',
            view: 1,
            xget: 'Users'
        },
        list: [],
        max_rows: 0
    };
    vm.groups = {};
    vm.integrated = {};
    vm.integrate = {};
    vm.user_access = {};
    vm.multiple = {};
    vm.appsType = undefined;
    vm.appsActive = !1;
    vm.propertyName = '';
    vm.reverse = !0;
    vm.ord = '';
    vm.friends = orderBy(vm.users.list, vm.propertyName, vm.reverse);
    vm.renderList = function(d) {
        if (d.data) {
            vm.users = d.data.users;
            helper.showAlerts(vm, d)
        }
    };

    function showAlerts(d) {
        helper.showAlerts(view, d)
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.toggleSearchButtons = function(item, val) {
        console.log('h');
        list.tSearch(vm.users, item, val, vm.renderList)
    };
    vm.sortBy = function(propertyName) {
        vm.reverse = (propertyName !== null && vm.propertyName === propertyName) ? !vm.reverse : !1;
        vm.propertyName = propertyName;
        vm.users.list = orderBy(vm.users.list, vm.propertyName, vm.reverse)
    };
    vm.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Statuses'),
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            vm.filters(value)
        },
        maxItems: 1
    };
    vm.filters = function(p) {
        list.filter(vm.users, p, vm.renderList)
    }
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'sm',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.vat = data.data.vat;
            vm.language = data.data.language
        }, function() {})
    };
    vm.modal_group = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.groups = data.data.groups
        }, function() {})
    };
    vm.modal_app = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    var get_data = {
                        'do': 'settings-settings',
                        'xget': r
                    };
                    return helper.doRequest('get', 'index.php', get_data).then(function(r) {
                        return r.data
                    })
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.integrated = data.data.integrated;
            vm.integrate = data.data.integrate
        }, function() {})
    };
    vm.load = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'settings-settings',
            'xget': h
        };
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
            if (k == 'logos') {
                vm.default_logo = r.data.default_logo
            }
        })
    }
    vm.upload = function(vm, e) {
        angular.element(e.target).parents('.upload-box').find('.newUploadFile2').click()
    }
    angular.element('.newUploadFile2').on('click', function(e) {
        e.stopPropagation()
    }).on('change', function(e) {
        e.preventDefault();
        var _this = $(this),
            p = _this.parents('.upload-box'),
            x = p.find('.newUploadFile2');
        if (x[0].files.length == 0) {
            return !1
        }
        if (x[0].files[0].size > 61440) {
            alert("Error: file is to big.");
            return !1
        }
        p.find('.img-thumbnail').attr('src', '');
        var tmppath = URL.createObjectURL(x[0].files[0]);
        p.find('.img-thumbnail').attr('src', tmppath);
        p.find('.btn').addClass('hidden');
        p.find('.nameOfTheFile').text(x[0].files[0].name);
        var formData = new FormData();
        formData.append("Filedata", x[0].files[0]);
        formData.append("do", 'settings-settings-settings-upload');
        formData.append("xget", 'company');
        p.find('.upload-progress-bar').css({
            width: '0%'
        });
        p.find('.upload-file').removeClass('hidden');
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "index.php", !0);
        oReq.upload.addEventListener("progress", function(e) {
            var pc = parseInt(e.loaded / e.total * 100);
            p.find('.upload-progress-bar').css({
                width: pc + '%'
            })
        });
        oReq.onload = function(oEvent) {
            var res = JSON.parse(oEvent.target.response);
            p.find('.upload-file').addClass('hidden');
            p.find('.btn').removeClass('hidden');
            if (oReq.status == 200) {
                if (res.success) {
                    $timeout(function() {
                        vm.account_address = res.data.account_address
                    })
                } else {
                    alert(res.error)
                }
            } else {
                alert("Error " + oReq.status + " occurred when trying to upload your file.")
            }
        };
        oReq.send(formData)
    });
    vm.CompanyInfo = function(vm, r, formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(vm.account_address);
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.account_address = r.data.account_address;
            vm.showAlerts(vm, r, formObj)
        })
    }
    vm.RegionalSet = function(vm, r, formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(vm.regional);
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.regional = r.data.regional;
            vm.showAlerts(vm, r, formObj)
        })
    }
    vm.deletevat = function(vm, r, ID, VALUE) {
        var get = 'Vat';
        var data = {
            'do': 'settings-settings-settings-deletevat',
            'xget': get,
            'ID': ID,
            'VALUE': VALUE
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.vat = r.data.vat
            vm.showAlerts(vm, r)
        })
    }
    vm.copydate = function(vm, r, ACCOUNT_DELIVERY_COUNTRY_ID, ACCOUNT_DELIVERY_ZIP, ACCOUNT_DELIVERY_ADDRESS, ACCOUNT_DELIVERY_CITY) {
        vm.account_address.ACCOUNT_BILLING_ADDRESS = angular.copy(ACCOUNT_DELIVERY_ADDRESS);
        vm.account_address.ACCOUNT_BILLING_ZIP = angular.copy(ACCOUNT_DELIVERY_ZIP);
        vm.account_address.ACCOUNT_BILLING_COUNTRY_ID = angular.copy(ACCOUNT_DELIVERY_COUNTRY_ID);
        vm.account_address.ACCOUNT_BILLING_CITY = angular.copy(ACCOUNT_DELIVERY_CITY)
    }
    vm.defaultvat = function(vm, r, VALUE) {
        var get = 'Vat';
        var data = {
            'do': 'settings-settings-settings-defaultvat',
            'xget': get,
            'VALUE': VALUE
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.vat = r.data.vat
            vm.showAlerts(vm, r)
        })
    }
    vm.activate_lang = function(vm) {
        var get = 'Language';
        var data = {
            'do': 'settings-settings-settings-activate_lang',
            'xget': get,
            'VALUE': vm.language.PUBLISH_LANG_ID
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.language = r.data.language
            vm.showAlerts(vm, r)
        })
    }
    vm.dragControlListeners = {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {
            var get = 'Language';
            var data = {
                'do': 'settings-settings-settings-change_language_name',
                'xget': get,
                'sort': vm.language.public_lang.public_lang,
                'sort_custom': vm.language.public_lang.extra_lang,
                'sort_custom_Extra': vm.language.public_lang.custom_lang
            };
            helper.doRequest('post', 'index.php', data, function(r) {
                vm.language = r.data.language
                vm.showAlerts(vm, r)
            })
        }
    }
    vm.deactivate_lang = function(vm, r, ID, check) {
        var get = 'Language';
        if (check == !1) {
            var data = {
                'do': 'settings-settings-settings-deactivate_lang',
                'xget': get,
                'ID': ID,
                'check': check
            }
        } else {
            var data = {
                'do': 'settings-settings-settings-activate_language',
                'xget': get,
                'ID': ID,
                'check': check
            }
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.language = r.data.language
            vm.showAlerts(vm, r)
        })
    }
    vm.deactivate_extra_lang = function(vm, r, ID, check) {
        var get = 'Language';
        if (check == !1) {
            var data = {
                'do': 'settings-settings-settings-deactivate_custom_lang',
                'xget': get,
                'ID': ID,
                'check': check
            }
        } else {
            var data = {
                'do': 'settings-settings-settings-activate_lang_custom',
                'xget': get,
                'ID': ID,
                'check': check
            }
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.language = r.data.language
            vm.showAlerts(vm, r)
        })
    }
    vm.change_main_user = function(vm, r, id) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-change_main_user',
            'xget': get,
            'user_id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            vm.showAlerts(vm, r)
        })
    }
    vm.deactivate_user = function(vm, r, id) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-deactivate',
            'xget': get,
            'user_id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            vm.showAlerts(vm, r)
        })
    }
    vm.activate = function(vm, r, id, check) {
        var get = 'Users';
        if (check == !0) {
            var data = {
                'do': 'settings-settings-settings-activate2',
                'xget': get,
                'user_id': id
            }
        } else {
            var data = {
                'do': 'settings-settings-settings-deactivate2',
                'xget': get,
                'user_id': id
            }
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            vm.showAlerts(vm, r)
        })
    }
    vm.delete = function(vm, r, id) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-delete',
            'xget': get,
            'user_id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            vm.showAlerts(vm, r)
        })
    }
    vm.activate2 = function(vm, r, id) {
        var get = 'Users';
        var data = {
            'do': 'settings-settings-settings-activate',
            'xget': get,
            'user_id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.users.list = r.data.users.list
            vm.showAlerts(vm, r)
        })
    }
    vm.delete_group = function(vm, r, group_id) {
        var get = 'group';
        var data = {
            'do': 'settings-settings-settings-group_delete',
            'xget': get,
            'group_id': group_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.groups = r.data.groups
            vm.showAlerts(vm, r)
        })
    }
    vm.user_accees_if_acc_manag = function(vm, r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.user_access = r.data.user_access;
            vm.showAlerts(vm, r)
        })
    }
    vm.multiple_id_delete = function(vm, r, identity_id) {
        var get = 'identity';
        var data = {
            'do': 'settings-settings-settings-multiple_id_delete',
            'xget': get,
            'identity_id': identity_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.multiple = r.data.multiple
            vm.showAlerts(vm, r)
        })
    }
    vm.filter_apps = function(type) {
        if (vm.appsType != type) {
            vm.appsType = type
        } else {
            vm.appsType = undefined
        }
    }
    vm.filter_apps2 = function(type) {
        vm.appsActive = type
    }
    vm.financial_settings = function(vm, r, formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(vm.financial);
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.financial = r.data.financial;
            vm.showAlerts(vm, r, formObj)
        })
    }
    vm.credential = function(vm, r, crm, article, order, po_order, quote, project, intervention, contract, invoice, webshop, timesheet, user_id, crm_admin, article_admin, order_admin, po_order_admin, quote_admin, project_admin, maintenance_admin, contract_admin, invoice_admin, project_manage, intervention_manage, is_accountant) {
        var get = 'accountantAccess';
        if (crm == !0) {
            var crm = 1
        } else {
            var crm = ''
        }
        if (article == !0) {
            var article = 12
        } else {
            var article = ''
        }
        if (order == !0) {
            var order = 6
        } else {
            var article = ''
        }
        if (po_order == !0) {
            var po_order = 14
        } else {
            var po_order = ''
        }
        if (quote == !0) {
            var quote = 5
        } else {
            var quote = ''
        }
        if (project == !0) {
            var project = 3
        } else {
            var project = ''
        }
        if (intervention == !0) {
            var intervention = 13
        } else {
            var intervention = ''
        }
        if (contract == !0) {
            var contract = 11
        } else {
            var contract = ''
        }
        if (invoice == !0) {
            var invoice = 4
        } else {
            var invoice = ''
        }
        if (webshop == !0) {
            var webshop = 9
        } else {
            var webshop = ''
        }
        if (timesheet == !0) {
            var timesheet = 8
        } else {
            var timesheet = ''
        }
        var credential = {
            '1': crm,
            '12': article,
            '6': order,
            '14': po_order,
            '5': quote,
            '3': project,
            '13': intervention,
            '11': contract,
            '4': invoice,
            '9': webshop,
            '8': timesheet
        };
        var data = {
            'do': 'settings-settings-settings-credentials',
            'xget': get,
            credential: credential,
            user_id: user_id,
            crm_admin: crm_admin,
            article_admin: article_admin,
            order_admin: order_admin,
            po_order_admin: po_order_admin,
            quote_admin: quote_admin,
            project_admin: project_admin,
            maintenance_admin: maintenance_admin,
            contract_admin: contract_admin,
            invoice_admin: invoice_admin,
            project_manage: project_manage,
            intervention_manage: intervention_manage,
            is_accountant: is_accountant
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.accountantAccess = r.data.accountantAccess;
            vm.showAlerts(vm, r)
        })
    }
    vm.UpdateUser = function(vm, r, formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(vm.accountantAccess);
        data.do = 'settings-settings-settings-UpdateAccountant';
        data.xget = 'accountantAccess';
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.accountantAccess = r.data.accountantAccess;
            vm.showAlerts(vm, r, formObj)
        })
    }
    helper.doRequest('get', 'index.php?do=settings-settings&xget=Users', function(r) {
        vm.users = r.data.users
    })
}]).controller('FeaturesCtrl', ['$scope', '$state', '$compile', 'data', '$timeout', 'helper', 'modalFc', 'UserService', '$rootScope', 'selectizeFc', function($scope, $state, $compile, data, $timeout, helper, modalFc, UserService, $rootScope, selectizeFc) {
    var ls = this,
        typePromise
}]).controller('CreateInstallModalCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'data', 'modalFc', 'editItem', function($scope, $timeout, helper, $uibModalInstance, data, modalFc, editItem) {
    var vm = this,
        typePromise;
    vm.app = 'installation';
    vm.ctrlpag = 'installation';
    vm.tmpl = 'insTemplate';
    vm.item_id = 'tmp';
    vm.installation_id = 'tmp';
    vm.isAddPage = !0;
    vm.item = data;
    var old_item = {};
    vm.format = 'dd/MM/yyyy';
    vm.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    vm.datePickOpen = {};
    vm.oldObj = {};
    vm.showTxt = {
        settings: !1,
        address: !0,
        addBtn: !0,
    };
    vm.auto = {
        options: {
            contacts: [],
            addresses: [],
            cc: [],
            articles: [],
            services: [],
        }
    };
    vm.auto.options.addresses = data.addresses;
    vm.auto.options.cc = data.cc;
    if (data.articles_list) {
        vm.auto.options.articles = data.articles_list.lines
    }
    if (data.services_list) {
        vm.auto.options.services = data.services_list.lines
    }
    vm.contactAddObj = {
        country_dd: vm.item.country_dd,
        country_id: vm.item.main_country_id,
        add_contact: !0,
        buyer_id: vm.item.buyer_id,
        'do': 'installation-installation-installation-tryAddC',
        contact_only: !0,
        item_id: vm.item_id
    }
    vm.item.add_customer = (vm.item_id != 'tmp' ? true : !1);
    vm.showEditFnc = showEditFnc;
    vm.showCCSelectize = showCCSelectize;
    vm.cancelEdit = cancelEdit;
    vm.removeDeliveryAddr = removeDeliveryAddr;
    editItem.init(vm);
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.renderPage = function(d) {
        if (d && d.data) {
            vm.item = d.data;
            vm.auto.options.addresses = d.data.addresses;
            vm.auto.options.cc = d.data.cc;
            if (d.data.articles_list) {
                vm.auto.options.articles = d.data.articles_list.lines
            }
            if (d.data.services_list) {
                vm.auto.options.services = d.data.services_list.lines
            }
            vm.contactAddObj = {
                country_dd: vm.item.country_dd,
                country_id: vm.item.main_country_id,
                add_contact: !0,
                buyer_id: vm.item.buyer_id,
                'do': 'installation-installation-installation-tryAddC',
                contact_only: !0,
                item_id: vm.item_id
            }
        }
    }

    function showEditFnc() {
        vm.showTxt.address = !1;
        vm.oldObj = {
            buyer_id: angular.copy(vm.item.buyer_id),
            contact_id: angular.copy(vm.item.contact_id),
            contact_name: angular.copy(vm.item.contact_name),
            BUYER_NAME: angular.copy(vm.item.buyer_name),
            delivery_address: (vm.item.sameAddress ? !1 : angular.copy(vm.item.delivery_address)),
            delivery_address_id: (vm.item.sameAddress ? !1 : angular.copy(vm.item.delivery_address_id)),
            delivery_address_txt: (vm.item.sameAddress ? !1 : angular.copy(vm.item.delivery_address_txt)),
            view_delivery: (vm.item.sameAddress ? !1 : angular.copy(vm.item.view_delivery)),
            sameAddress: angular.copy(vm.item.sameAddress),
        }
    }

    function showCCSelectize() {
        vm.showTxt.addBtn = !1;
        $timeout(function() {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.open()
        })
    }

    function removeDeliveryAddr() {
        vm.item.delivery_address = '';
        vm.item.delivery_address_id = ''
    }

    function cancelEdit() {
        if (Object.keys(vm.oldObj).length == 0) {
            return vm.removeCust(vm)
        }
        vm.showTxt.address = !0;
        for (x in vm.oldObj) {
            if (vm.item[x]) {
                if (x == 'delivery_address') {
                    vm.item[x] = vm.oldObj[x].trim()
                } else {
                    vm.item[x] = vm.oldObj[x]
                }
            }
        }
        vm.item.buyer_id = vm.oldObj.buyer_id
    }
    vm.showAlerts = function(d, formObj) {
        helper.showAlerts(vm, d, formObj)
    }
    vm.fieldCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Option'),
        closeAfterSelect: !0,
        onDropdownOpen($dropdown) {
            var _this = this,
                field_index = _this.$input[0].attributes.fieldid.value,
                pos = _this.$input[0].attributes.position.value;
            if (vm.item.template[pos][field_index].name_final != undefined && vm.item.template[pos][field_index].name_final != '0') {
                old_item[vm.item.template[pos][field_index].field_id] = vm.item.template[pos][field_index].name_final
            }
        },
        render: {
            option: function(item, escape) {
                var html = '<div  class="option " >' + item.name + '</div>';
                if (item.id == '0') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Manage the list') + '</div>'
                }
                return html
            }
        },
        onChange(item) {
            var _this = this;
            var field_index = _this.$input[0].attributes.fieldid.value;
            var pos = _this.$input[0].attributes.position.value;
            if (vm.item.template[pos][field_index].name_final != undefined && vm.item.template[pos][field_index].name_final != '0') {
                old_item[vm.item.template[pos][field_index].field_id] = vm.item.template[pos][field_index].name_final
            }
            if (item == '0') {
                var obj = {
                    'pos': pos,
                    'field_index': field_index
                };
                openModal('selectEdit', obj, 'md');
                vm.item.template[pos][field_index].name_final = undefined;
                return !1
            }
        },
        maxItems: 1
    };
    vm.articleAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Article'),
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                if (item.supplier_reference) {
                    item.supplier_reference = "/ " + item.supplier_reference
                }
                if (item.name2) {
                    item.name2 = "/ " + item.name2
                }
                if (item.ALLOW_STOCK == !0) {
                    var step = '<br><small class="text-muted">' + helper.getLanguage('Stock') + ' <strong>' + escape(item.stock2) + '</strong></small>'
                } else {
                    var step = ''
                }
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;' + escape(item.supplier_reference) + '&nbsp;' + escape(item.name2) + '&nbsp; </small>' + '</div>' + '<div class="col-sm-3 pull-right text-right">' + (helper.displayNr(item.price)) + step + '</div>' + '</div>' + '</div>';
                return html
            }
        },
        onType(str) {
            var selectize = angular.element('#articles_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'installation-installation',
                    xget: 'articles_list',
                    search: str,
                    cat_id: vm.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    vm.auto.options.articles = r.data.lines;
                    if (vm.auto.options.articles.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value) {
            if (value == undefined || value == '') {
                var data = {
                    'do': 'installation-installation',
                    xget: 'articles_list',
                    cat_id: vm.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    vm.auto.options.articles = r.data.lines
                })
            }
        },
        onBlur() {
            if (!vm.auto.options.articles.length) {
                var data = {
                    'do': 'installation-installation',
                    xget: 'articles_list',
                    cat_id: vm.item.cat_id
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    vm.auto.options.articles = r.data.lines
                })
            }
        },
        maxItems: 1
    };
    vm.serviceAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select Service'),
        create: !1,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp; </small>' + '</div>' + '<div class="col-sm-3 pull-right text-right">' + (helper.displayNr(item.price)) + '</div>' + '</div>' + '</div>';
                return html
            }
        },
        onType(str) {
            var selectize = angular.element('#services_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'installation-installation',
                    xget: 'articles_list',
                    search: str,
                    cat_id: vm.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    vm.auto.options.services = r.data.lines;
                    if (vm.auto.options.services.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value) {
            if (value == undefined || value == '') {
                var data = {
                    'do': 'installation-installation',
                    xget: 'articles_list',
                    cat_id: vm.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    vm.auto.options.services = r.data.lines
                })
            }
        },
        onBlur() {
            if (!vm.auto.options.services.length) {
                var data = {
                    'do': 'installation-installation',
                    xget: 'articles_list',
                    cat_id: vm.item.cat_id,
                    'only_service': !0
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    vm.auto.options.services = r.data.lines
                })
            }
        },
        maxItems: 1
    };
    vm.saveData = function(formObj) {
        var obj = angular.copy(vm.item);
        obj.do = 'installation-installation-ifields-addInstallation';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            vm.showAlerts(r, formObj);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
    vm.selectTemplate = function() {
        openModal("select_template", vm, "lg")
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'select_template':
                var iTemplate = 'InstallationAddTemplate';
                var ctas = 'add';
                break;
            case 'selectEdit':
                var iTemplate = 'selectEdit';
                var ctas = 'vmMod';
                break
        }
        var params = {
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'select_template':
                params.template = 'insTemplate/' + iTemplate + 'Modal';
                params.ctrl = iTemplate + 'ModalCtrl';
                params.item = item;
                params.callback = function() {
                    if (vm.item.template_id && vm.item.name) {
                        angular.element('#template_id').removeClass('hidden');
                        angular.element('#template_name').removeClass('hidden');
                        angular.element('#blank_template').remove()
                    }
                }
                break;
            case 'selectEdit':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.ctrl = 'selectInstallFieldModalCtrl';
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-selectInstallField',
                    'field_id': vm.item.template[item.pos][item.field_index].field_id
                };
                params.callback = function(data) {
                    if (data.data) {
                        vm.item.template[item.pos][item.field_index].no_fields = data.data.lines;
                        if (old_item[vm.item.template[item.pos][item.field_index].field_id]) {
                            vm.item.template[item.pos][item.field_index].name_final = old_item[vm.item.template[item.pos][item.field_index].field_id]
                        }
                    } else {
                        vm.item.template[item.pos][item.field_index].no_fields = data.lines;
                        if (old_item[vm.item.template[item.pos][item.field_index].field_id]) {
                            vm.item.template[item.pos][item.field_index].name_final = old_item[vm.item.template[item.pos][item.field_index].field_id]
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }
}]).controller('InstallDataModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'Lightbox', function($scope, helper, $uibModalInstance, data, Lightbox) {
    var vm = this,
        typePromise;
    vm.item = {
        'dropbox': {}
    };
    vm.item.dropbox = data;
    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d)
    }
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.deleteDropFile = function(index) {
        var data = angular.copy(vm.item.dropbox.dropbox_files[index].delete_file_link);
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r);
            if (r.success) {
                vm.item.dropbox.nr--;
                vm.item.dropbox.dropbox_files.splice(index, 1)
            }
        })
    }
    vm.deleteDropImage = function(index) {
        var data = angular.copy(vm.item.dropbox.dropbox_images[index].delete_file_link);
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(r);
            if (r.success) {
                vm.item.dropbox.nr--;
                vm.item.dropbox.dropbox_images.splice(index, 1)
            }
        })
    }
    vm.openLightboxModal = function(index) {
        Lightbox.openModal(vm.item.dropbox.dropbox_images, index)
    }
}]).controller('SelectAvatarModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.attachAvatar = function(url) {
        helper.doRequest('post', 'index.php', {
            'do': 'misc-myaccount-user-attachAvatar',
            'avatar': url
        }, function(r) {
            $uibModalInstance.close(r)
        })
    }
}]).controller('ChangePassModalCtrl', ['$scope', 'helper', '$uibModalInstance', function($scope, helper, $uibModalInstance) {
    var vm = this;
    vm.obj = {};
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.showAlerts = function(d, formobj) {
        helper.showAlerts(vm, d, formobj)
    }
    vm.ok = function(formobj) {
        var obj = angular.copy(vm.obj);
        obj.do = 'misc--user-changePassword';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.error) {
                vm.showAlerts(r, formobj)
            }
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('EnableTfaModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('TimeTaskAddModalCtrl', ['$scope', 'helper', '$uibModalInstance', function($scope, helper, $uibModalInstance) {
    var vm = this;
    vm.obj = {};
    vm.cancel = function() {
        $uibModalInstance.dismiss()
    }
    vm.showAlerts = function(d, formobj) {
        helper.showAlerts(vm, d, formobj)
    }
    vm.ok = function(formobj) {
        var obj = angular.copy(vm.obj);
        obj.do = 'misc--misc-default_task_add';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.error) {
                vm.showAlerts(r, formobj)
            }
            if (r.success || r.notice) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('AmazonUploadModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    $scope.obj = data;
    $scope.start = !1;
    $scope.uploaded = !1;
    $scope.$watch('files', function() {
        $scope.uploadFiles($scope.files)
    });
    $scope.cancel = function() {
        $uibModalInstance.close($scope.uploaded)
    }
    $scope.showAlerts = function(d, formobj) {
        helper.showAlerts($scope, d, formobj)
    }
    $scope.done = function () {
        var signature = $scope.accept();
        var blob = $scope.base64ToBlob(signature.dataUrl,'image/png');
        var files = new File([blob], "signature"+$scope.obj.id+".png", {
            type: "'image/png'"
        });
        var formData = new FormData();
        for (y in $scope.obj) {
            formData.append(y, $scope.obj[y])
        }
        formData.append("do", 'misc--misc-uploadS3');
        formData.append("Filedata", files);
        formData.append("signature",1);
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "index.php", !0);
        oReq.onload = function(oEvent) {
            var r = JSON.parse(oEvent.target.response);
            if (r.error) {
                var msg = {
                    'error': {
                        'error': r.filename + ': ' + r.error
                    },
                    'notice': !1,
                    'success': !1
                }
            }
            if (r.success) {
                var msg = {
                    'error': !1,
                    'notice': !1,
                    'success': {
                        'success': r.filename + ': ' + r.success
                    }
                };
                $scope.uploaded = !0
            }
            $scope.showAlerts(msg);
            /*if (var i == files.length - 1) {
                $scope.start = !1
            }*/
        };
        oReq.send(formData)
        if (signature.isEmpty) {
            $uibModalInstance.dismiss();
        } else {
            $uibModalInstance.close(signature.dataUrl);
        }
    }
    $scope.base64ToBlob = function (base64, type) {
        var parts = base64.split(';base64,');
        var buff = atob(parts[1]);
        var bytes = new Uint8Array(buff.length);

        for (var i = 0; i < bytes.length; i++) {
            bytes[i] = buff.charCodeAt(i);
        }

        return new Blob([bytes], {type: type});
    }
    $scope.uploadFiles = function(files) {
        if (files && files.length) {
            $scope.start = !0;
            $scope.value = 0;
            var j = 0;
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                var is_error = !1;
                if (file.size == 0) {
                    is_error = !0;
                    var msg_err = {
                        'error': {
                            'error': file.name + ': ' + helper.getLanguage('No size')
                        },
                        'notice': !1,
                        'success': !1
                    }
                } /*else if ($scope.obj.type == '2' && file.type.search("image") == -1) {
                    is_error = !0;
                    var msg_err = {
                        'error': {
                            'error': file.name + ': ' + helper.getLanguage('Invalid file type.')
                        },
                        'notice': !1,
                        'success': !1
                    }
                } else if ($scope.obj.type == '1' && file.type.search("image") != -1) {
                    is_error = !0;
                    var msg_err = {
                        'error': {
                            'error': file.name + ': ' + helper.getLanguage('Invalid file type.')
                        },
                        'notice': !1,
                        'success': !1
                    }
                }*/
                if (is_error) {
                    $scope.showAlerts(msg_err);
                    if (i == files.length - 1) {
                        $scope.start = !1
                    }
                    j++;
                    $scope.value = j * 100 / files.length;
                    continue
                }
                if (!file.$error) {
                    var formData = new FormData();
                    for (y in $scope.obj) {
                        formData.append(y, $scope.obj[y])
                    }
                    if ($scope.obj.drop_folder) {
                        formData.append("do", 'misc--misc-dropboxUpload')
                    } else {
                        formData.append("do", 'misc--misc-uploadS3')
                    }
                    formData.append("Filedata", file);
                    var oReq = new XMLHttpRequest();
                    oReq.open("POST", "index.php", !0);
                    oReq.onload = function(oEvent) {
                        j++;
                        $scope.value = j * 100 / files.length;
                        var r = JSON.parse(oEvent.target.response);
                        if (r.error) {
                            var msg = {
                                'error': {
                                    'error': r.filename + ': ' + r.error
                                },
                                'notice': !1,
                                'success': !1
                            }
                        }
                        if (r.success) {
                            var msg = {
                                'error': !1,
                                'notice': !1,
                                'success': {
                                    'success': r.filename + ': ' + r.success
                                }
                            };
                            $scope.uploaded = !0
                        }
                        $scope.showAlerts(msg);
                        if (i == files.length - 1) {
                            $scope.start = !1
                        }
                    };
                    oReq.send(formData)
                }
            }
        }
    }

    $scope.uploadLink=function(){
        var obj = angular.copy($scope.obj);
        obj.do = $scope.obj.drop_folder ? 'misc--misc-upload_dropbox_url_link' : 'misc--misc-upload_s3_url_link';
        $scope.value = 0;
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.value=100;
            if (r.error) {
                var msg = {
                    'error': {
                        'error': $scope.obj.url_link + ': ' + r.error.url_link
                    },
                    'notice': !1,
                    'success': !1
                }
            }
            if (r.success) {
                var msg = {
                    'error': !1,
                    'notice': !1,
                    'success': {
                        'success': $scope.obj.url_link + ': ' + r.success.success
                    }
                };
                $scope.uploaded = !0
            }
            $scope.showAlerts(msg);
        })
    }

}]).controller('ToInstallDataModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.item = data;
    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d)
    }
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.ok = function() {
        var images_selected = [];
        var is_error = !0;
        for (x in vm.item.images) {
            if (vm.item.images[x].checked) {
                is_error = !1;
                var tmp_image = {};
                tmp_image.id = vm.item.images[x].id;
                tmp_image.item = vm.item.images[x].item;
                tmp_image.name = vm.item.images[x].name;
                images_selected.push(tmp_image)
            }
        }
        if (is_error) {
            var err_msg = {
                'error': {
                    'error': helper.getLanguage('Select at least 1 image')
                },
                'success': !1,
                'notice': !1
            };
            vm.showAlerts(err_msg);
            return !1
        }
        var tmp_ob = {};
        tmp_ob.do = 'misc--misc-copyToS3';
        tmp_ob.images = images_selected;
        tmp_ob.target_id = vm.item.target_id;
        tmp_ob.target_folder = vm.item.target_folder;
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', tmp_ob, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            vm.showAlerts(r)
        })
    }
}]).controller('GDPRPolicyModalCtrl', ['$scope', '$state', 'helper', '$uibModalInstance', 'data', function($scope, $state, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.learn_more = function() {
        $uibModalInstance.close();
        $state.go('settings', {
            tab: 'legal_info'
        })
    }
    vm.ok = function() {
        var obj = {
            'do': 'misc--misc-update_policy',
            'type': '1'
        };
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                $uibModalInstance.close()
            }
        })
    }
}]).controller('ColumnSettingsModalCtrl', ['$scope', '$state', 'helper', '$uibModalInstance', 'data', function($scope, $state, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.cancel = function() {
        $uibModalInstance.close()
    }
    vm.ok = function() {
        if (!vm.obj.selected.length) {
            return !1
        }
        var tmp_obj = {
            'do': 'misc--misc-updateColumnSettings',
            'list_name': vm.obj.list_name
        };
        tmp_obj.selected = angular.copy(vm.obj.selected);
        helper.doRequest('post', 'index.php', tmp_obj, function(r) {
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                helper.showAlerts(vm, r)
            }
        })
    }
}]).controller('addArticleVariantsModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    console.log('Add Article Variants Modal Ctrl');

    $scope.obj = data;
    $scope.max_rows = data.max_rows;
    $scope.lr = data.lr;
    $scope.search = {
        search: '',
        'do': 'misc-addArticleVariantList',
        'parent_article_id':$scope.obj.parent_article_id,
        offset: 1
    };
    $scope.ok = function(item) {
        helper.doRequest('post','index.php',{'do':'misc--misc-addArticleVariant','parent_article_id':$scope.search.parent_article_id,'article_id':item.article_id},function(r){
            if(r.success){
                item.succes_add = true;
            }
        });      
    };

    $scope.renderList = function(d) {
        if (d.data) {
            $scope.obj = d.data;
            $scope.lr = d.data.lr;
            $scope.offset = d.data.offset;
            $scope.max_rows = d.data.max_rows
        }
    };
    $scope.searchThing = function() {
        helper.searchIt($scope.search, $scope.renderList)
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('BulkChangeVariantsModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    console.log('Bulk Change Variants Modal Ctrl');

    var vmMod=this;
    vmMod.obj = {};
    vmMod.obj.from=angular.copy(data.list);
    vmMod.obj.to=angular.copy(data.list);
    vmMod.obj.title=data.title;

    vmMod.variantTypeCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        maxItems: 1
    };

    vmMod.ok = function() {
        if(vmMod.obj.from_id && vmMod.obj.item.to_id && vmMod.obj.from_id != vmMod.obj.item.to_id){
            $uibModalInstance.close({'from':vmMod.obj.from_id,'to':vmMod.obj.item.to_id});    
        }
    };

    vmMod.cancel = function() {
        $uibModalInstance.close();
    }
}]).controller('SendEmailModalCtrl', ['$scope', '$timeout' ,'helper', '$uibModalInstance', 'data', 'selectizeFc',function($scope, $timeout, helper, $uibModalInstance, data, selectizeFc) {
    console.log('SendEmailModalCtrl');

    $timeout(function(){
        angular.element('.modal-dialog')/*.css({'width':'1190px'})*/;
    });

    var vm=this;
    vm.obj = data;
    vm.email = {
        recipients:[]
    };

    if(vm.obj.dropbox && vm.obj.dropbox.dropbox_images && vm.obj.dropbox.dropbox_images.length>0){
        for (x in vm.obj.dropbox.dropbox_images) {   
          vm.obj.dropbox.dropbox_files.push(angular.copy(vm.obj.dropbox.dropbox_images[x]));
        }
    }
     
    /*console.log(vm.obj.dropbox.dropbox_files, vm.obj.dropbox.dropbox_images);*/

    vm.save_btn=vm.obj.app=='invoice' && vm.obj.app_mod && vm.obj.app_mod=='recinvoice' ? helper.getLanguage('Save') : helper.getLanguage('Send');
    vm.tiny_toolbar=vm.obj.app=='invoice' && vm.obj.app_mod && vm.obj.app_mod=='recinvoice' ? ["fontsizeselect | bold italic strikethrough underline | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image code"] : ["fontsizeselect | bold italic strikethrough underline | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"];

    vm.format = 'dd/MM/yyyy';
    vm.datePickOpen = {
        sent_date: !1,
        invoice_date: !1,
        payment_date: !1
    };
    vm.dateOptions = {
        dateFormat: '',
        startingDay: 1
    };

    vm.contactForEmailAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select recipient'),
        create: !0,
        createOnBlur:true,
        maxItems: 100
    });

    vm.tinymceOptions = {
        selector: ".mail_send",
        resize: "height",
        height: 284,
        menubar: !1,
        relative_urls: !1,
        remove_script_host : !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        toolbar: vm.tiny_toolbar,
    };

    vm.sendEmails=function(){
        if(vm.obj.app == 'invoice' && vm.obj.app_mod && vm.obj.app_mod=='recinvoice'){
            var emailCopy=angular.copy(vm.obj); 
        }else{
            var emailCopy=angular.copy(vm.obj.email); 
        }   
        if(vm.obj.app == 'maintenance'){
            emailCopy.attach_calendar=vm.obj.attach_calendar;
        }   
        emailCopy.dropbox_files= vm.obj.dropbox.dropbox_files;

        $uibModalInstance.close(emailCopy);
    }

    vm.cancel = function() {
        $uibModalInstance.close();
    }
}]).controller('EditCustomerNotesModalCtrl', ['$scope', '$timeout','helper', '$uibModalInstance', 'data', function($scope, $timeout, helper, $uibModalInstance, data) {
    console.log('Edit Customer Notes Modal Ctrl');

    var vmMod=this;
    vmMod.obj = data;

    $timeout(function(){
        angular.element('.modal-open .modal').removeClass('hidden');
    },500);

    vmMod.notesTinymceOpts = {
        selector: ".variable_assign",
        resize: "height",
        height: 192,
        menubar: !1,
        relative_urls: !1,
        remove_script_host : !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        toolbar: ["bold italic | bullist numlist "]
    };

    vmMod.editNotes = function() {
        if (helper.searchPromise) {
                $timeout.cancel(helper.searchPromise)
            }
            helper.searchPromise = $timeout(function() {
                var tmp_obj = {
                    'do': 'misc--misc-updateCustomerNotes',
                    'buyer_id': vmMod.obj.buyer_id,
                    'notes': vmMod.obj.customer_notes
                };
                helper.doRequest('post', 'index.php', tmp_obj);
            }, 500); 
    };

    vmMod.cancel = function() {
        $uibModalInstance.close();
    }
}]).controller('scanbarModalCtrl', ['$scope', '$uibModalInstance', 'editItem', 'data', 'helper', 'modalFc', '$timeout', function($scope, $uibModalInstance, editItem, data, helper, modalFc, $timeout) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.start = !0;
    vmMod.value = 0;
    var i = 0;
    var increment = 0;
    vmMod.showAlerts = function(d) {
        helper.showAlerts(vmMod, d)
    }

    $timeout(function() {
        modalFc.setHeight(angular.element('.modal.in'))
    })
    vmMod.ok = ok;
    vmMod.cancel = cancel;
    var originalScope = editItem.getScope();

  function ok() {
         $uibModalInstance.close(vmMod.obj);
       
  }

    function cancel() {
        $uibModalInstance.dismiss('cancel');
    };


}]).controller('scanbarWarningModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {

    var vmMod = this;
    vmMod.obj = data;

    vmMod.ok = function() {
        $uibModalInstance.close()
    };
    vmMod.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
}]).controller('dashboardCtrl', ['$scope', 'helper', 'modalFc',function($scope, helper, modalFc) {
    console.log('dash');
    var vm = this;

    vm.kpis={
        revenue_this_year:"-",
        revenue_this_year_with_vat:"-",
        revenue_this_month:"-",
        revenue_this_month_with_vat:"-",
        outstanding:"-",
        outstanding_with_vat:"-",
        overdue:"-",
        overdue_with_vat:"-"    
    };

    vm.search={'vat_included':false};

    vm.show_load = !0;

    vm.renderPage=function(d,loadKpis) {
        if (d.data) {
            for (x in d.data) {
                vm[x] = d.data[x]
            }
            vm.show_load = !1;
        }
        if(d.data.showGdpr){
            vm.openModal("GDPRPolicy", {})
        }
        if(loadKpis){
            vm.loadKpis();
        }
    }

    vm.loadKpis=function(){
        helper.doRequest('get', 'index.php', {
            'do': 'misc-dashboard',
            'xget':"KPI"
        }, function(r) {
            if(r.data){
                vm.kpis.revenue_this_year=r.data.amount_to_be_inv_year;
                vm.kpis.revenue_this_year_with_vat=r.data.amount_to_be_inv_year_with_btw;
                vm.kpis.revenue_this_month=r.data.amount_to_be_inv_month;
                vm.kpis.revenue_this_month_with_vat=r.data.amount_to_be_inv_month_with_btw;
                vm.kpis.outstanding=r.data.amount_to_be_inv;
                vm.kpis.outstanding_with_vat=r.data.amount_to_be_inv_with_btw;
                vm.kpis.overdue=r.data.amount_to_be_inv_late;
                vm.kpis.overdue_with_vat=r.data.amount_to_be_inv_late_with_btw; 
            }
        });
    }

    vm.openModal = function(type, item, size) {
        var params = {
            template: 'cTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'GDPRPolicy':
                var iTemplate = 'GDPRPolicy';
                var ctas = 'vm';
                var params = {
                    template: 'miscTemplate/' + iTemplate + 'Modal',
                    ctrl: iTemplate + 'ModalCtrl',
                    ctrlAs: ctas,
                    size: size
                };
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-policy_info'
                };
                break; 
        }
        modalFc.open(params)
    }
   

    helper.doRequest('get', 'index.php', {
        'do': 'misc-dashboard'
    }, function(r) {
        vm.renderPage(r,true);
    });

}]).controller('NewActivitiesModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'selectizeFc',function($scope, helper, $uibModalInstance, data, selectizeFc) {
    var vmMod = this,
        currentTime = new Date(),
        current = undefined;

    vmMod.obj = data;

    vmMod.activity = {
        hd_events: !0,
        task_type: '6',
        date:new Date(),
        dateHours:new Date(
            currentTime.getFullYear(),
            currentTime.getMonth(),
            currentTime.getDate(),
            currentTime.getHours(),
            currentTime.getMinutes()),
        dateEHours:new Date(
            currentTime.getFullYear(),
            currentTime.getMonth(),
            currentTime.getDate(),
            currentTime.getHours(),
            currentTime.getMinutes()),
        reminder_event:new Date(),
        reminderHours:new Date(
            currentTime.getFullYear(),
            currentTime.getMonth(),
            currentTime.getDate(),
            currentTime.getHours(),
            currentTime.getMinutes()),
        end_d_meet:new Date(),
        reminder_id:0
    }
    if(vmMod.obj.opportunity_id){
        vmMod.activity.opportunity_id=vmMod.obj.opportunity_id;
    }
    if(vmMod.obj.customer_id){
        vmMod.activity.customer_id=vmMod.obj.customer_id;
    }
    if(vmMod.obj.contact_id){
        vmMod.activity.contact_id=vmMod.obj.contact_id;
    }

    vmMod.hideSwitch = !1;

    vmMod.showAlerts=function(d, formObj) {
        helper.showAlerts(vmMod, d, formObj)
    }

    vmMod.ok = function(formObj) {
        var tmp_data = angular.copy(vmMod.activity);
        angular.merge(tmp_data, vmMod.activityParams);
        if(vmMod.obj.filter_type && vmMod.obj.filter_type != tmp_data.filter_type){
            tmp_data.filter_type=vmMod.obj.filter_type;
        }
        tmp_data.user_id = vmMod.obj.user_id;
        tmp_data.do = 'misc-activities-customers-add_activity';  
        helper.doRequest('post', 'index.php', tmp_data, function(r) {
            if (r.error) {
                vmMod.showAlerts(r,formObj)
            } else {
                $uibModalInstance.close(r)
            }
        })   
    };
    vmMod.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    }
    vmMod.tinymceOptions_new = {
        selector: ".variable_assign_new",
        resize: "height",
        menubar: !1,
        autoresize_bottom_margin: 0,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        toolbar: [" bold italic | bullist numlist "]
    };

    vmMod.showActivityF = function(type) {
        if(vmMod.obj.from_tasks){
            return false;
        }
        vmMod.activity.task_type = type;
        vmMod.activity.type_of_call = undefined;
        vmMod.activity.hd_events = !1;
        vmMod.hideSwitch = !1;

        vmMod.activity.comment = '';
        vmMod.activity.log_comment = '';
        vmMod.activity.duration_meet = '';
        vmMod.activity.reminder_id=0;

        if (type == '5') {
            vmMod.activity.type_of_call = 0
        }
        if (type == '6') {
            vmMod.hideSwitch = !0;
            vmMod.activity.hd_events = !0;
            vmMod.activity.reminder_id=0;
        }
        vmMod.activity.reminderHours = new Date(
            currentTime.getFullYear(),
            currentTime.getMonth(),
            currentTime.getDate(),
            currentTime.getHours(),
            currentTime.getMinutes());
        vmMod.activity.dateHours = new Date(
            currentTime.getFullYear(),
            currentTime.getMonth(),
            currentTime.getDate(),
            currentTime.getHours(),
            currentTime.getMinutes());
        vmMod.activity.dateEHours = new Date(
            currentTime.getFullYear(),
            currentTime.getMonth(),
            currentTime.getDate(),
            currentTime.getHours(),
            currentTime.getMinutes());
        vmMod.activity.date = new Date();
        vmMod.activity.reminder_event = new Date();
        vmMod.activity.end_d_meet = new Date();
        vmMod.changeTime(vmMod.activity.date, vmMod.activity.dateHours, true);
        vmMod.changeTime(vmMod.activity.reminder_event, vmMod.activity.reminderHours, true);
        vmMod.changeTime(vmMod.activity.end_d_meet, vmMod.activity.dateEHours, true)
    }

    vmMod.accountAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select Account'),
        onType(str) {
            var _this = this;
            helper.doRequest('post', 'index.php', {
                'do': 'misc-myActivities',
                xget: 'customers',
                'contact_id': vmMod.obj.contact_id,
                term: str
            }, function(r) {
                if (r && r.data) {
                    vmMod.accounts = r.data;
                    _this.open()
                }
            })
        },
        onFocus() {
            var _this = this;
            helper.doRequest('post', 'index.php', {
                'do': 'misc-myActivities',
                'contact_id': vmMod.obj.contact_id,
                xget: 'customers'
            }, function(r) {
                if (r && r.data) {
                    vmMod.accounts = r.data;
                    _this.open()
                }
            })
        }
    });
    vmMod.contactAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select contact'),
        maxItems: 1000,
        onType(str) {
            var _this = this;
            helper.doRequest('post', 'index.php', {
                'do': 'misc-myActivities',
                xget: 'contacts',
                'customer_id': vmMod.activity.customer_id,
                term: str
            }, function(r) {
                if (r && r.data) {
                    vmMod.contacts = r.data;
                    _this.open()
                }
            })
        },
        onFocus() {
            var _this = this;
            helper.doRequest('post', 'index.php', {
                'do': 'misc-myActivities',
                xget: 'contacts',
                'customer_id': vmMod.activity.customer_id
            }, function(r) {
                if (r && r.data) {
                    vmMod.contacts = r.data;
                    _this.open()
                }
            })
        },
    });

    vmMod.reminderAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        onChange(value){
            if(value == '1' && vmMod.activity.task_type == '6'){
                this.addItem('2');
            }else if(parseInt(value) > 1){
                vmMod.activity.hd_events = !0;
            }else{
                vmMod.activity.hd_events = !1;
                vmMod.activity.reminder_event=undefined;
            }
        },
        onDelete(value){
            return false;
        }
    });

    vmMod.assignAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select')
    });

    vmMod.userAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('User'),
        onChange(value) {
            vmMod.toggleSearchButtons('user_id', value)
        }
    });

    vmMod.activityParams = {
        'do': 'misc-activities',
        'xget': 'Activity',
        filter_type: 'user_timeline',
        'offset': 0
    };

    vmMod.toggleSearchButtons = function(item, value) {
        vmMod.activityParams[item] = value;
    }

    vmMod.changeTime = function(item, t, check) {
        if (item && t) {
            item.setHours(t.getHours(),t.getMinutes());
        }
    }

    vmMod.changeTime(vmMod.activity.date,vmMod.activity.dateHours,true);
    vmMod.changeTime(vmMod.activity.reminder_event, vmMod.activity.reminderHours,true);  
    
    if(vmMod.obj.customer_id){
        helper.doRequest('post', 'index.php', {
            'do': 'misc-myActivities',
            xget: 'customers',
            customer_id: vmMod.obj.customer_id
        }, function(r) {
            if (r && r.data) {
                vmMod.accounts = r.data;
            }
        })
    }

    if(vmMod.obj.contact_ids && Array.isArray(vmMod.obj.contact_ids) && vmMod.obj.contact_ids.length){
        vmMod.activity.contact_ids=vmMod.obj.contact_ids;
        helper.doRequest('post', 'index.php', {
            'do': 'misc-myActivities',
            xget: 'contacts',
            'customer_id': vmMod.obj.customer_id,
            contact_id: vmMod.obj.contact_ids
        }, function(r) {
            if (r && r.data) {
                vmMod.contacts = r.data;
            }
        })
    }


    
}]);