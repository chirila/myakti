angular.module('customer').controller('ContactsCtrl', ['$scope', '$state','helper', 'list', 'data', 'modalFc','editItem',function($scope, $state, helper, list, data, modalFc, editItem) {
    console.log('Contacts list');
    var vm = this;
    vm.ord = '';
    vm.reverse = !1;
    vm.max_rows = 0;
    vm.search = {
        search: '',
        'do': 'customers-contacts',
        xget: '',
        showAdvanced: !1,
        showList: !1,
        offset: 1
    };
    vm.app="general";
    vm.list = [];
    vm.nav = [];
    vm.nav_contacts = [];
    vm.listStats = [];
    vm.renderList = function(d) {
        if (d.data) {
            for (x in d.data) {
                vm[x] = d.data[x]
            }
            var ids = [];
            var count = 0;
            for(y in vm.nav_contacts){
                ids.push(vm.nav_contacts[y]['contact_id']);
                count ++;
            }
            //console.log(count);
            sessionStorage.setItem('contactView_total', count);
            sessionStorage.setItem('contactView_list', JSON.stringify(ids));
            //console.log(ids);
            vm.listStats = [];
            vm.showAlerts(d)
        }
    }
    vm.showMore = function(item) {
        item.showMore = item.showMore ? !1 : !0
    }
    vm.toggleSearchButtons = function(item, val) {
        list.tSearch(vm, item, val, vm.renderList)
    };
    vm.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        angular.element('.mCSB_container').css({
            'height': 'auto'
        });
        list.search(vm.search, vm.renderList, vm)
    }
    vm.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    }
    vm.action = function(item, action) {
        list.action(vm, item, action, vm.renderList)
    };
    vm.orderBY = function(p) {
        list.orderBy(vm, p, vm.renderList)
    };
    vm.showAlerts = function(d, formObj) {
        helper.showAlerts(vm, d, formObj)
    };
    vm.showArchivedList = function(v, i) {
        vm.search.archived = v;
        vm.search.view = i;
        vm.searchThing()
    }
    vm.saveView = function(i) {
        vm.view = i;
        helper.doRequest('post', 'index.php', {
            'do': 'customers--customers-save_view_list',
            'view': i
        })
    }
    vm.alloCall = function(number, item) {
        if (vm.devices.length == 1) {
            var obj = {
                'do': 'misc--customers-alloCall',
                'device_id': vm.devices[0].id,
                'number': number,
                'customer_id': item.customer_id
            };
            obj.contact_ids = [item.contact_id];
            helper.doRequest('post', 'index.php', obj, function(r) {
                vm.showAlerts(r)
            })
        } else {
            var obj = {
                'do': 'misc--customers-alloCall',
                'number': number,
                'device_dd': vm.devices,
                'customer_id': item.customer_id
            };
            obj.contact_ids = [item.contact_id];
            vm.openModal('allo_call', obj, "md")
        }
    }
    vm.columnSettings = function() {
        vm.openModal('column_set', {}, 'lg')
    }
    vm.openModal = function(type, item, size) {
        var params = {
            template: 'miscTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'allo_call':
                params.backdrop = 'static';
                params.template = 'miscTemplate/AlloCallModal';
                params.ctrl = 'AlloCallModalCtrl';
                params.ctrlAs = 'vmMod';
                params.size = size;
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        vm.showAlerts(d)
                    }
                }
                break;
                case 'LinktoAccount':
                    params.backdrop = 'static';
                    params.template = 'miscTemplate/LinktoAccountModal';
                    params.ctrl = 'LinktoAccountCtrl';
                    params.ctrlAs = 'vmMod';
                    params.size = size;
                    params.item = item;
                    params.callback = function(d) {
                        if (d) {
                            vm.showAlerts(d)
                        }
                    }
                    break;
                    case 'EditInfo':
                        params.backdrop = 'static';
                        params.template = 'miscTemplate/EditInfoModal';
                        params.ctrl = 'EditInfoCtrl';
                        params.ctrlAs = 'vmMod';
                        params.size = size;
                        params.item = item;
                        params.callback = function(d) {
                            if (d) {
                                vm.showAlerts(d)
                            }
                        }
                        break;
                        case 'ContactInfo':
                            params.backdrop = 'static';
                            params.template = 'miscTemplate/ContactinfoModal';
                            params.ctrl = 'ContactInfoCtrl';
                            params.ctrlAs = 'vmMod';
                            params.size = size;
                            params.item = item;
                            params.callback = function(d) {
                                if (d) {
                                    vm.showAlerts(d)
                                }
                            }
                            break;
                            case 'ContactEditInfo':
                                params.backdrop = 'static';
                                params.template = 'miscTemplate/ContactEditInfoModal';
                                params.ctrl = 'ContactEditInfoCtrl';
                                params.ctrlAs = 'vmMod';
                                params.size = size;
                                params.item = item;
                                params.callback = function(d) {
                                    if (d) {
                                        vm.showAlerts(d)
                                    }
                                } 
                         break; 
                         case 'EditPersonalInfo':
                            params.backdrop = 'static';
                            params.template = 'miscTemplate/EditPersonalInfoModal';
                            params.ctrl = 'EditPersonalInfoCtrl';
                            params.ctrlAs = 'vmMod';
                            params.size = size;
                            params.item = item;
                            params.callback = function(d) {
                                if (d) {
                                    vm.showAlerts(d)
                                }
                            } 
                     break;                
            case 'column_set':
                params.template = 'miscTemplate/ColumnSettingsModal';
                params.ctrl = 'ColumnSettingsModalCtrl';
                params.params = {
                    'do': 'misc-column_set',
                    'list': 'contacts'
                };
                params.ctrlAs = 'vm';
                params.callback = function(d) {
                    if (d) {
                        vm.searchThing()
                    }
                }
                break;
            case 'SmartListAdd':
                var iTemplate = 'SmartListAdd';
                var ctas = 'ls';
                var params = {
                    template: 'cTemplate/' + iTemplate + 'Modal',
                    ctrl: iTemplate + 'ModalCtrl',
                    size: size,
                    backdrop: 'static'
                };
                params.callback = function(data) {
                    $state.go(data.state, {
                        contact_list_id: data.contact_list_id
                    })
                }
                break 
        }
        modalFc.open(params)
    }
    vm.scroll_config_vertical = {
        autoHideScrollbar: !1,
        theme: 'minimal-dark',
        axis: 'y',
        scrollInertia: 50,
        advanced: {
            autoExpandHorizontalScroll: !0
        }
    };
    vm.scrollUp = function(event, index) {
        var _this = angular.element(event.target);
        var container = _this.closest('.mCSB_container');
        var scrolled = _this.closest('tr').offset();
        if (container.hasClass('mCS_no_scrollbar_y')) {
            angular.element('.mCSB_container').css({
                'height': '100%'
            })
        }
        if (scrolled.top > 550) {
            _this.closest('.btn-group').addClass('dropup')
        } else {
            _this.closest('.btn-group').removeClass('dropup')
        }
    };

    vm.createCustomer=function(){
        var main_obj = {
            country_dd: [],
            country_id: 0,
            add_customer: !0,
            'do': 'misc--misc-tryAddC',
        };
        editItem.openModal(vm, 'AddCustomer', main_obj, 'md');
    };

    vm.createContact=function(){
        var main_obj = {
            country_dd: [],
            country_id: 0,
            add_contact: !0,
            contact_only: !0,
            'do': 'misc--misc-tryAddC',
        };
        editItem.openModal(vm, 'AddCustomer', main_obj, 'md');
    }

    list.init(vm, 'customers-contacts', vm.renderList)
}]).controller('ContactCtrl', ['$scope', '$stateParams', '$state', '$injector', 'helper', 'selectizeFc', 'data', '$timeout', 'modalFc', function($scope, $stateParams, $state, $injector, helper, selectizeFc, data, $timeout, modalFc) {
    var ls = this,
        typePromise;
    var old_ls = {};
    for (x in $stateParams) {
        ls[x] = $stateParams[x]
    }
    ls.contact_id = $stateParams.contact_id;
    for (x in data) {
        ls[x] = data[x];
        if (x == 'obj' && ls[x].birthdate) {
            ls[x].birthdate = new Date(ls[x].birthdate)
        }
        if (data.viewMod) {
            ls.contact_id = data.contact_id
        }
    }
    if(ls.contact_id == 'tmp' && ls.customer_id){
        ls.tmp_customer_addresses=ls.customer_dd[0].addresses;
    }
    helper.updateTitle($state.current.name,(ls.obj.firstname && ls.obj.lastname ? ls.obj.firstname+' '+ls.obj.lastname : (ls.obj.firstname && !ls.obj.lastname ? ls.obj.firstname : ls.obj.lastname)));
    ls.showAlerts = function(d, formObj) {
        helper.showAlerts(ls, d, formObj)
    };
    ls.renderPage = function(d, formObj) {
        if (d && d.data) {
            for (x in d.data) {
                ls[x] = d.data[x];
                if (x == 'obj' && ls[x].birthdate) {
                    ls[x].birthdate = new Date(ls[x].birthdate)
                }
            }
            ls.showAlerts(d, formObj)
        }
    }
    ls.cancels = function() {
        $state.go('contacts')
    }
    ls.compCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select Account'),
        maxOptions: 50,
        onType(str) {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'customers-Contact',
                    'xget': 'customers',
                    'term': str,
                    'contact_id': ls.contact_id
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    ls.customer_dd = r.data;
                    if (ls.customer_dd.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value){
            ls.tmp_customer_addresses=[];
            if(value){
                ls.tmp_customer_addresses=this.options[value].addresses;
            }
        },
        maxItems: 1
    });
    ls.lngCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: !1,
        onDropdownOpen($dropdown) {
            var _this = this,
                model = _this.$input[0].attributes.model.value;
            if (ls.obj[model] != undefined && ls.obj[model] != '0') {
                old_ls[model] = ls.obj[model]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                ls[realModel] = o.data.lines;
                ls.obj[realModel] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            if (ls.obj[model] != undefined && ls.obj[model] != '0') {
                old_ls[model] = ls.obj[model]
            }
            if (item == '0') {
                ls.openModal('selectEdit', {
                    table: table,
                    model: model
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                ls.obj[model] = undefined;
                return !1
            }
        }
    }, !1, !0, ls);
    
    
    ls.salesCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: !1,
        onDropdownOpen($dropdown) {
            var _this = this,
                model = _this.$input[0].attributes.model.value;
            if (model == 'position' || model == 'department') {
                var ind = _this.$input[0].attributes.ind.value;
                if (ls.accounts[ind][model] != undefined && ls.accounts[ind][model] != '0') {
                    old_ls[model + '_' + ind] = ls.accounts[ind][model]
                }
            } else {
                if (ls.obj[model] != undefined && ls.obj[model] != '0') {
                    old_ls[model] = ls.obj[model]
                }
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined,
                index = _this.$input[0].attributes.ind.value;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
           //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                ls[realModel] = o.data.lines;
                ls.accounts[index][realModel] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            var ind = undefined;
            if (model == 'position' || model == 'department') {
                ind = _this.$input[0].attributes.ind.value
            }
            if (model == 'position' || model == 'department') {
                if (ls.accounts[ind][model] != undefined && ls.accounts[ind][model] != '0') {
                    old_ls[model + '_' + ind] = ls.accounts[ind][model]
                }
            } else {
                if (ls.obj[model] != undefined && ls.obj[model] != '0') {
                    old_ls[model] = ls.obj[model]
                }
            }
            if (item == '0') {
                ls.openModal('selectEdit', {
                    table: table,
                    model: model,
                    ind: ind
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                if (ind != undefined) {
                    ls.accounts[ind][model] = undefined
                } else {
                    ls.obj[model] = undefined
                }
                return !1
            }
        }
    }, !1, !0, ls);
    ls.extraCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 1000,
        maxItems: 1,
        create: !1,
        onDropdownOpen($dropdown) {
            var _this = this,
                extra_id = _this.$input[0].attributes.extra.value;
            if (ls.obj.extra[extra_id] != undefined && ls.obj.extra[extra_id] != '0') {
                old_ls['extra_' + extra_id] = ls.obj.extra[extra_id]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            //angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                ls[realModel][extra_id] = o.data.lines;
                ls.obj[realModel][extra_id] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = _this.$input[0].attributes.extra.value;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            if (ls.obj.extra[extra_id] != undefined && ls.obj.extra[extra_id] != '0') {
                old_ls['extra_' + extra_id] = ls.obj.extra[extra_id]
            }
            if (item == '0') {
                ls.openModal('selectEdit', {
                    table: table,
                    model: model,
                    extra_id: extra_id
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                ls.obj.extra[extra_id] = undefined;
                return !1
            }
        }
    }, !1, !0, ls);
    ls.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select gender'),
    });
    ls.cfgLng = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select language'),
    });
    ls.funcCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: 'Select function',
        maxOptions: 50,
        maxItems: 50
    });
    ls.addressCfg = {
        valueField: 'address_id',
        labelField: 'address',
        searchField: ['address'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select'),
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-8">' + '<strong class="text-ellipsis">' + item.symbol + '  ' + escape(item.top) + '</strong>' + '<small class="text-ellipsis"><span class="glyphicon glyphicon-map-marker"></span>' + escape(item.bottom) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-muted text-right">' + '<small class="text-ellipsis"> ' + escape(item.right) + '</small>' + '</div>' + '</div>' + '</div>';
                return html;
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.top) + '</div>'
            }
        },
        maxItems: 1
    };
    ls.openModal = function(type, item, size, obj, extra_id) {
        if (type == 'selectEdit') {
            var params = {
                template: 'miscTemplate/' + type + 'Modal',
                ctrl: type + 'ModalCtrl',
                size: size
            }
        }
        switch (type) {
            case 'selectEdit':
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-selectEdit',
                    'table': item.table,
                    'extra_id': item.extra_id
                };
                params.callback = function(data) {
                    if (data.data) {
                        if (item.extra_id) {
                            ls.extra[item.extra_id] = data.data.lines;
                            if (old_ls['extra_' + item.extra_id]) {
                                ls.obj.extra[item.extra_id] = old_ls['extra_' + item.extra_id]
                            }
                        } else {
                            ls[item.table] = data.data.lines;
                            if (item.ind) {
                                if (old_ls[item.model + '_' + item.ind]) {
                                    ls.accounts[item.ind][item.model] = old_ls[item.model + '_' + item.ind]
                                }
                            } else {
                                if (old_ls[item.model]) {
                                    ls.obj[item.model] = old_ls[item.model]
                                }
                            }
                        }
                    } else {
                        if (item.extra_id) {
                            ls.extra[item.extra_id] = data.lines;
                            if (old_ls['extra_' + item.extra_id]) {
                                ls.obj.extra[item.extra_id] = old_ls['extra_' + item.extra_id]
                            }
                        } else {
                            ls[item.table] = data.lines;
                            if (item.ind) {
                                if (old_ls[item.model + '_' + item.ind]) {
                                    ls.accounts[item.ind][item.model] = old_ls[item.model + '_' + item.ind]
                                }
                            } else {
                                if (old_ls[item.model]) {
                                    ls.obj[item.model] = old_ls[item.model]
                                }
                            }
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }
    ls.ok = function(formObj) {
        var params = angular.copy(ls.obj);
        params.accounts = ls.accounts;
        if (ls.contact_id == 'tmp') {
            params.email = ls.obj.email
        }
        formObj.$invalid = !0;
        helper.doRequest('post', 'index.php', params, function(d) {
            if (ls.contact_id && d.success) {
                if($stateParams.customer_id){
                    $state.go('customerView', {
                        customer_id: $stateParams.customer_id
                    });
                }else{
                    $state.go('contactView', {
                        contact_id: d.data.obj.contact_id
                    });
                }
            } else if (d.success) {
                ls.showAlerts(d)
            } else if (d.error) {
                ls.showAlerts(d, formObj)
            }
        })
    };
    ls.collapse = function(item) {
        for (x in ls.accounts) {
            ls.accounts[x].collapse = !1
        }
        item.collapse = !0
    }
    ls.action = function(item, action, $event) {
        helper.doRequest('post', 'index.php', {
            'do': 'customers-Contact-customers-' + action,
            'contact_id': ls.contact_id,
            'customer_id': item,
            'xget': 'contactAccounts'
        }, function(d) {
            ls.accounts = d.data;
            ls.showAlerts(d);
            ls.showLinkAccount = !1;
            ls.obj.customer_id = undefined;
            helper.doRequest('get', 'index.php', {
                'do': 'customers-Contact',
                'xget': 'customers',
                'contact_id': ls.contact_id
            }, function(r) {
                ls.customer_dd = r.data;
            })
        })
    }
    ls.cancel = function() {
        $scope.$close('cancel')
    }
    
}]).controller('ContactViewCtrl', ['$scope', '$rootScope','$stateParams', '$state', '$timeout', 'helper', 'data', 'selectizeFc', 'modalFc', function($scope, $rootScope, $stateParams, $state, $timeout, helper, data, selectizeFc, modalFc) {
    var ls = this,
        typePromise, currentTime = new Date(),
        current = undefined;
    if(!data.item_exists){
        ls.item_exists = false;
        return 0;
    }; 
    ls.showMoreField = !1;
    ls.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    ls.showActivityForm = !1;
    ls.showActivityiny = !1;
    ls.activityTxt = helper.getLanguage('Type');
    ls.activity = {
        hd_events: !1
    }
    ls.checkString = checkString;
    ls.activityNumber = 5;
    ls.activityLoaded = !1;
    ls.hideSwitch = !1;
    ls.inputType = 'password';
    ls.hours = [];
    ls.inputType = 'password';
    for (var i = 0; i <= 23; i++) {
        for (var j = 0; j <= 3; j++) {
            var min = j * 15,
                text = (i < 10 ? '0' + i : i) + ':' + (min < 10 ? '0' + min : min);
            ls.hours.push({
                'id': i + '-' + min,
                'value': text
            });
            if (i <= currentTime.getHours() && min <= currentTime.getMinutes()) {
                current = i + '-' + min
            }
        }
    }
    for (x in $stateParams) {
        ls[x] = $stateParams[x]
    }
    ls.contact_id = $stateParams.contact_id;
    ls.activityParams = {
        'do': 'misc-activities',
        'contact_id': ls.contact_id,
        'xget': 'Activity',
        filter_type: 'customer'
    };
    for (x in data) {
        ls[x] = data[x];
        if (x == 'obj' && ls[x].birthdate) {
            ls[x].birthdate = new Date(ls[x].birthdate)
        }
        if (data.viewMod) {
            ls.contact_id = data.contact_id
        }
    }
    ls.compCfg = selectizeFc.configure({
        labelField: 'value',
        searchField: ['value'],
        placeholder: helper.getLanguage('Select Account'),
        maxOptions: 50,
        onType(str) {
            var selectize = angular.element('#custandcont')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var obj = {
                    'do': 'customers-Contact',
                    'xget': 'customers',
                    'term': str,
                    'contact_id': ls.contact_id
                };
                helper.doRequest('get', 'index.php', obj, function(r) {
                    ls.customer_dd = r.data;
                    if (ls.customer_dd.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onChange(value){
            ls.tmp_customer_addresses=[];
            if(value){
                ls.tmp_customer_addresses=this.options[value].addresses;
            }
        },
        maxItems: 1
    });
    ls.activitiesType = [{
        id: '2',
        value: helper.getLanguage('Note'),
        custom_class: 'fa fa-sticky-note'
    }, {
        id: '5',
        value: helper.getLanguage('Call'),
        custom_class: 'fa fa-phone'
    }, {
        id: '4',
        value: helper.getLanguage('Meeting'),
        custom_class: 'fa fa-calendar'
    }, {
        id: '6',
        value: helper.getLanguage('Task'),
        custom_class: 'akti-icon o_akti_checklist'
    }];
    ls.activityCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Type'),
        create: !1,
        maxItems: 1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline activities_type_dd">' + '<div class="row">' + '<div class="col-sm-2">' + '<a href="#"><span class="' + item.custom_class + '"></span></a>' + '</div>' + '<div class="col-sm-9">' + '<span>' + escape(item.value) + '&nbsp;</span>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            ls.activityParams['activity_id'] = value;
            helper.doRequest('get', 'index.php', ls.activityParams, ls.renderActivities);
        }
    };
    helper.updateTitle($state.current.name,(ls.obj.firstname && ls.obj.lastname ? ls.obj.firstname+' '+ls.obj.lastname : (ls.obj.firstname && !ls.obj.lastname ? ls.obj.firstname : ls.obj.lastname)));
    ls.showAlerts = function(d, formObj) {
        helper.showAlerts(ls, d, formObj)
    };
    ls.hideShowPassword = function(tip) {
        if (tip == 'password')
            ls.inputType = 'text';
        else ls.inputType = 'password'
    };
   
    ls.genPass = function(nr) {
        var pass = helper.generatePassword(nr);
        ls.webshop.password = pass
    }

    ls.action = function(customer_id, contact_id, action, $event) {
        helper.doRequest('post', 'index.php', {
            'do': 'customers-Contact-customers-' + action,
            'contact_id': contact_id,
            'customer_id': customer_id,
        }, function(d) {
            ls.accounts = d.data.accounts;
            ls.account = d.data.account;
            ls.showAlerts(d);
            ls.showLinkAccount = !1;
            ls.obj.customer_id = undefined;
            if(action === 'archive_contact'){
                $state.go('contacts');
            }
            else
            {
                helper.doRequest('get', 'index.php', {
                    'do': 'customers-Contact',
                    'xget': 'customers',
                    'contact_id': ls.contact_id
                }, function(r) {
                    ls.customers_dd = r.data;
                })
            }

        })
    }
    ls.cancel = function() {
        $scope.$close('cancel')
    }

    ls.editNotes = function() {
        if (typePromise) {
            $timeout.cancel(typePromise);
        }
        typePromise = $timeout(function() {
            ls.obj.do = 'customers--customers-contact_update'
            helper.doRequest('post', 'index.php', ls.obj);
        }, 500);       
    }

    ls.renderPage = function(d, formObj) {
        if (d && d.data) {
            for (x in d.data) {
                ls[x] = d.data[x];
                if (x == 'obj' && ls[x].birthdate) {
                    ls[x].birthdate = new Date(ls[x].birthdate)
                }
            }
            ls.showAlerts(d, formObj)
        }
    }
    ls.whebshop_save = function(formObj) {
        console.log(formObj);
        formObj.$invalid = !0;
        var data = angular.copy(ls.webshop);
        helper.doRequest('post', 'index.php', data, function(d) {
            ls.showAlerts(d, formObj)
        })
    }
    ls.showMore = function(item) {
        item.showMore = item.showMore ? !1 : !0
    }
    ls.renderActivities = function(r) {
        if (r.data) {
            for (x in r.data) {
                ls[x] = r.data[x]
            }
            ls.activity.location_meet = ls.activities.location_meet;
            ls.showAlerts(r);
            ls.activityLoaded = !0
        }
    }
    if (ls.ADV_CRM) {
        helper.doRequest('get', 'index.php', ls.activityParams, ls.renderActivities)
    }
    ls.removeActivity = function(item, $index) {
        var data = {
            contact_id: ls.contact_id,
            do: 'misc--customers-delete_activity',
            id: item.log_id
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r && r.success) {
                ls.activities.item.splice($index, 1)
            }
        })
    }
    ls.changeActivityStatus = function(item) {
        var data = angular.copy(ls.activityParams);
        data.do = 'misc-activities-customers-update_status';
        data.log_code = item.log_code;
        data.status_change = item.color == 'green_status' ? 0 : 1;
        helper.doRequest('post', 'index.php', data, ls.renderActivities)
    }
    ls.showActivityF = function(show, item, type) {
        if (type == '7' && !ls.obj.zen_id) {
            var msg = helper.getLanguage('Contact needs to be synced first');
            var cont = {
                "e_message": msg
            };
            ls.openModal("stock_info", cont, "md");
            return !1
        }
        ls.showActivityForm = !0;
        ls.activityTxt = helper.getLanguage(item);
        ls.activity.task_type = type;
        ls.activity.type_of_call = undefined;
        ls.activity.hd_events = !1;
        ls.hideSwitch = !1;
        if (show == !1) {
            ls.activity.comment = '';
            ls.activity.log_comment = '';
            ls.activity.customer_id = '';
            ls.activity.duration_meet = '';
            ls.showAddActivity = !1
        }
        if (type == '5') {
            ls.activity.type_of_call = 0
        }
        if (type == '6') {
            ls.hideSwitch = !0;
            ls.activity.hd_events = !0
        }
        if (type == '7') {
            ls.hideSwitch = !0
        }
        ls.activity.reminderHours = current;
        ls.activity.dateHours = current;
        ls.activity.dateEHours = current;
        ls.activity.date = new Date();
        ls.activity.reminder_event = new Date();
        ls.activity.end_d_meet = new Date();
        ls.changeTime(ls.activity.date, ls.activity.dateHours);
        ls.changeTime(ls.activity.reminder_event, ls.activity.reminderHours);
        ls.changeTime(ls.activity.end_d_meet, ls.activity.dateEHours)
    }
    ls.changeDuration = function() {
        if (ls.activity.duration_meet) {
            var d = ls.activity.duration_meet.split('-'),
                currentH = angular.copy(ls.activity.dateHours).split('-');
            ls.activity.end_d_meet.setHours(ls.activity.date.getHours() + parseInt(d[0]), ls.activity.date.getMinutes() + parseInt(d[1]));
            var minutes = parseInt(currentH[1]) + parseInt(d[1]);
            var h = parseInt(currentH[0]) + parseInt(d[0]);
            if (minutes >= 60) {
                minutes -= 60;
                h++
            }
            ls.activity.dateEHours = (h) + '-' + (minutes)
        } else {
            ls.activity.end_d_meet = new Date();
            ls.activity.dateEHours = current
        }
    }
    ls.checkAllDay = function() {
        var new_date = angular.copy(ls.activity.date),
            day = new_date.getDate();
        ls.activity.end_d_meet = new_date;
        ls.activity.end_d_meet.setDate(day + 1)
    }
    ls.tinymceOptions = {
        selector: ".variable_assign",
        resize: "both",
        menubar: !1,
        autoresize_bottom_margin: 0,
        relative_urls: !1,
        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample code"],
        statusbar: !1,
        toolbar: ["styleselect | bold italic | bullist numlist "]
    };
    ls.contactAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select customer'),
        valueField: 'customer_id',
        labelField: 'name',
        searchField: ['name']
    });
    ls.timeAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Time'),
    });
    ls.assignAutoCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Assign to')
    });
    ls.populateContactAcitivityList = function(item) {
        ls.contactsActitivty = [];
        for (x in item) {
            ls.contactsActitivty.push({
                'id': item[x].contact_id,
                'value': (item[x].con_firstname + ' ' + item[x].con_lastname)
            })
        }
    }
    
    ls.addActivity = function() {
        var data = angular.copy(ls.activity);
        angular.merge(data, ls.activityParams);
        data.user_id = ls.activities.user_id;
        data.do = 'misc-activities-customers-add_activity';
        data.contact_ids = [ls.contact_id];
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                ls.showAlerts(r)
            } else {
                ls.renderActivities(r);
                var taskName = '';
                switch (data.task_type) {
                    case '1':
                        taskName = 'E-mail';
                        break;
                    case '2':
                        taskName = 'Note';
                        break;
                    case '4':
                        taskName = 'Meeting';
                        break;
                    case '5':
                        taskName = 'Call';
                        break;
                    case '6':
                        taskName = 'Task';
                        break;
                    case '7':
                        taskName = 'Zendesk';
                        break
                }
                ls.showActivityF(!1, taskName, data.task_type);
                ls.showActivityiny = !1
            }
        })
    }
    ls.changeTime = function(item, t, check) {
        if (item && t) {
            var d = t.split('-');
            item.setHours(d[0], d[1]);
            if (check) {
                ls.changeDuration();
                ls.checkAllDay()
            }
        }
    }
    ls.viewContent = function(item) {
        var cont = {
            "e_message": item.comments
        };
        ls.openModal("stock_info", cont, "lg")
    }
    ls.alloCall = function(number, is_customer, item) {
        if (ls.devices.length == 1) {
            var obj = {
                'do': 'misc--customers-alloCall',
                'device_id': ls.devices[0].id,
                'number': number
            };
            obj.contact_ids = [ls.contact_id];
            if (is_customer) {
                obj.customer_id = item.customer_id
            }
            helper.doRequest('post', 'index.php', obj, function(r) {
                ls.showAlerts(r);
                if (r.success) {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php', ls.activityParams, ls.renderActivities)
                }
            })
        } else {
            var obj = {
                'do': 'misc--customers-alloCall',
                'number': number,
                'device_dd': ls.devices
            };
            obj.contact_ids = [ls.contact_id];
            if (is_customer) {
                obj.customer_id = item.customer_id
            }
            ls.openModal('allo_call', obj, "md")
        }
    }
    function checkString(string) {
        
        if(!string)
        {
            string = '-'
        }

        return string;
    }

    ls.showActivityF(!0, 'Note', '2');
    ls.openModal = function(type, item, size) {
    
        var params = {
            template: 'miscTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'NewActivities':
                params.template = 'miscTemplate/' + type + 'Modal/';
                var newData = {};
                newData.user_id=ls.activities.user_id;
                newData.contact_ids = [ls.contact_id];
                newData.contact_id=ls.contact_id;
                newData.reminder_dd=ls.reminder_dd;
                newData.disable_contact=true;
                newData.account_date_format = ls.account_date_format;
                newData.user_auto=ls.activities.user_auto;
                params.item = newData;
                params.callback = function(r) {
                    ls.renderActivities(r)
                }
                break;
            case 'EditActivity':
                var newData = angular.copy(item);
                newData.contact_id = ls.contact_id;
                newData.reminder_dd=ls.reminder_dd;
                newData.account_date_format = ls.account_date_format;
                newData.user_auto=ls.activities.user_auto;
                newData.allow_delete=true;
                newData.allow_delete=true;
                params.item = newData;
                params.callback = function(r) {
                    //ls.renderActivities(r)
                    helper.doRequest('get', 'index.php', ls.activityParams, ls.renderActivities);
                }
                break;
            case 'stock_info':
                params.template = 'miscTemplate/modal';
                params.ctrl = 'viewRecepitCtrl';
                params.ctrlAs = 'vmMod';
                params.item = item;
                break;
            case 'allo_call':
                params.template = 'miscTemplate/AlloCallModal';
                params.ctrl = 'AlloCallModalCtrl';
                params.ctrlAs = 'vmMod';
                params.size = size;
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        ls.showAlerts(d);
                        if (d.success) {
                            //angular.element('.loading_wrap').removeClass('hidden');
                            helper.doRequest('get', 'index.php', ls.activityParams, ls.renderActivities)
                        }
                    }
                }
                break;
                case 'LinktoAccount':
                    params.template = 'miscTemplate/LinktoAccountModal';
                    params.ctrl = 'LinktoAccountCtrl';
                    params.ctrlAs = 'vmMod';
                    params.item = item;
                    params.callback = function(d) {
                        if (d) {
                            if (d.success) {
                                helper.doRequest('get', 'index.php', {
                                    'do': 'customers-Contact',
                                    'viewMod': 'true',
                                    'contact_id': ls.contact_id
                                }, function(r) {
                                    ls.customers_dd = r.data.customers_dd;
                                    ls.accounts = r.data.accounts;
                                })
                            }
                        }
                    }
            break;
            case 'EditInfo':
                params.template = 'miscTemplate/EditInfoModal';
                params.ctrl = 'EditInfoCtrl';
                params.ctrlAs = 'vmMod';
                params.item = item;
                params.callback = function(d) {
                    helper.doRequest('get', 'index.php', {
                        'do': 'customers-Contact',
                        'viewMod': 'true',
                        'contact_id': ls.contact_id
                    }, function(r) {
                        ls.obj = r.data.obj;
                    })
               
                }
             break;
        case 'ContactInfo':
            params.template = 'miscTemplate/ContactinfoModal';
            params.ctrl = 'ContactInfoCtrl';
            params.ctrlAs = 'vmMod';
            var newData = {};
            newData = item;
            newData.accounts=JSON.stringify(ls.accounts);
            newData.firstname = ls.obj.firstname;
            newData.lastname=ls.obj.lastname;
            newData.customer_contact_job_title=ls.customer_contact_job_title;
            newData.customer_contact_dep=ls.customer_contact_dep;
            newData.account_date_format = ls.account_date_format;
            newData.customer_contact_title=ls.customer_contact_title;
            params.item = newData;
            params.callback = function(d) {
                helper.doRequest('get', 'index.php', {
                    'do': 'customers-Contact',
                    'viewMod': 'true',
                    'contact_id': ls.contact_id
                }, function(r) {
                    ls.accounts = r.data.accounts;
                    ls.account = r.data.account;
                })
           
            }
         break;
         case 'ContactEditInfo':
            params.template = 'miscTemplate/ContactEditInfoModal';
            params.ctrl = 'ContactEditInfoCtrl';
            params.ctrlAs = 'vmMod';
            params.item = item;
         break;
         case 'EditPersonalInfo':
            params.template = 'miscTemplate/EditPersonalInfoModal';
            params.ctrl = 'EditPersonalInfoCtrl';
            params.ctrlAs = 'vmMod';
            params.item = item;
         break;
         case 'EditAvatarInfo':
            params.template = 'miscTemplate/EditAvatarInfoModal';
            params.ctrl = 'EditAvatarInfoCtrl';
            params.ctrlAs = 'vmMod';
            params.item = item;
         break;
            case 'ZenComment':
                params.template = 'miscTemplate/ZenCommentModal';
                params.ctrlAs = 'ls';
                //angular.element('.loading_wrap').removeClass('hidden');
                params.params = {
                    'do': 'misc-zen_comments',
                    'ticket_id': item
                };
                break
        }   
        modalFc.open(params)
    }
}])