angular.module('akti').controller('ImportAccountsCtrl', ['$scope', '$timeout', '$state', '$stateParams', 'helper', '$uibModal', 'data', 'selectizeFc', 'modalFc', function($scope, $timeout, $state, $stateParams, helper, $uibModal, data, selectizeFc, modalFc) {
    var vm = this,
        typePromise;
    vm.crm = data.data.crm;
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select file'),
        labelField: 'name',
        searchField: ['name'],
    });
    vm.nylas_search = {
        search: '',
        'do': 'customers-nylas_contacts',
        view: 0,
        xget: 'list',
        offset: 1
    };
    vm.showZenF = {
        'account_fields': !1,
        'contact_fields': !1
    };
    vm.cfgZenF = selectizeFc.configure({
        placeholder: helper.getLanguage('Select field'),
        labelField: 'name',
        searchField: ['name'],
    });

    function showAlerts(d) {
        helper.showAlerts(view, d)
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    if ($stateParams.tab) {
        $timeout(function() {
            angular.element('.nav-tabs a[href="#' + $stateParams.tab + '"]').tab('show').triggerHandler('click')
        })
    }
    vm.backup_data = function(vm, r, file_id, step) {
        var get = 'import';
        var data = {
            'do': 'customers-import_accounts-customers-backup_data',
            'xget': get,
            'file_id': file_id,
            'step': step
        };
        vm.modal(data, 'cTemplate/makingFile', 'makingFileCtrl', 'sm')
    }
    vm.delete_import_company = function(vm, r, company_import_log_id) {
        var get = 'import';
        var data = {
            'do': 'customers-import_accounts-customers-delete_import_company',
            'xget': get,
            'company_import_log_id': company_import_log_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.crm = r.data.crm;
            vm.showAlerts(vm, r)
        })
    }
    vm.hidecontact = function() {
        if (vm.indi == 1) {
            vm.hideconts = 0;
            vm.hidecont = 0;
            vm.namecomp = helper.getLanguage('Individual');
            vm.changefield = 1
        } else {
            vm.hideconts = 1;
            vm.hidecont = 1;
            vm.namecomp = helper.getLanguage('Company');
            vm.changefield = 0
        }
    }
    vm.putcontact = function() {
        if (vm.indicontact == 1) {
            vm.hideconts = 1
        } else {
            vm.hideconts = 0
        }
    }
    vm.multipleAddresses = function() {
        if (vm.multiple_addresses) {
            vm.delete_existent_addresses = 0
        } else {
            vm.delete_existent_addresses = 1
        }
    }
    vm.save_imports = function() {
        var is_c_id = !1;
        var is_name = !1;
        var get = 'import';
        var total = vm.total_rows;
        var k = 0;
        var times = Math.ceil(total / 25);
        var width = 100 / times;
        var executed = 0;
        var params = {
            'xget': get,
            'do': 'customers-import_accounts-customers-save_imports',
            'is_customer': 1,
            'total_rows': k + 25,
            'xls_name': vm.xls_name,
            'timestamp': vm.timestamp,
            'total': total,
            'k': 0,
            'indi': vm.indi,
            'indicontact': vm.indicontact,
            'delete_existent_addresses': vm.delete_existent_addresses
        };
        for (x in vm.fields) {
            params[x] = vm.fields[x];
            if (vm.indi) {
                for (y in vm.match_field_individual) {
                    if (vm.match_field_individual[y].id == params[x]) {
                        if (vm.match_field_individual[y].field_name == 'customer_id') {
                            is_c_id = !0;
                            break
                        } else if (vm.match_field_individual[y].field_name == 'firstname' || vm.match_field_individual[y].field_name == 'lastname') {
                            is_name = !0;
                            break
                        }
                    }
                }
            } else {
                for (y in vm.match_field_company) {
                    if (vm.match_field_company[y].id == params[x]) {
                        if (vm.match_field_company[y].field_name == 'customer_id') {
                            is_c_id = !0;
                            break
                        } else if (vm.match_field_company[y].field_name == 'name') {
                            is_name = !0;
                            break
                        }
                    }
                }
            }
        }
        for (x in vm.fieldsC) {
            params[x] = vm.fieldsC[x]
        }
        if (!is_c_id && !is_name) {
            var cont = {
                "e_message": '<p class="text-danger text-left"><b>' + helper.getLanguage('Matching is impossible') + '</b></p>',
                 "e_title":  helper.getLanguage('Import')
            };
            vm.openModal("stock_info", cont, "sm");
            return !1
        }
        var data = {
            'params': params,
            'get': get,
            'k': k,
            'times': times,
            'width': width,
            'executed': executed
        };
        vm.openModal('ImportAccountFileCtrl', data, 'md')
    }
    vm.load = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'customers-import_accounts',
            'xget': h
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(vm, r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
        })
    }
    vm.modal_history = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data)
        }, function() {})
    };
    vm.searchThing = function() {
        var data = angular.copy(vm.nylas_search);
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.nylas_data = r.data
        })
    };
    vm.get_contacts = function() {
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'customers-nylas_contacts'
        }, function(r) {
            vm.nylas_data = r.data
        })
    }
    vm.get_zendesk = function(view) {
        var old_step = vm.zendesk_data && vm.zendesk_data.step ? vm.zendesk_data.step : 0;
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'customers-zendesk_contacts',
            'view': view
        }, function(r) {
            vm.zendesk_data = r.data;
            vm.showZenF.account_fields = !1;
            vm.showZenF.contact_fields = !1;
            if (old_step > 0) {
                vm.zendesk_data.step = old_step
            }
        })
    }
    vm.sync_contacts = function() {
       // angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', {
            'do': 'customers-nylas_contacts',
            'xget': 'contacts'
        }, function(r) {
            vm.showAlerts(vm, r);
            if (!r.error && r.data.list.length) {
                delete vm.scroll_config;
                vm.nylas_data.step = 3;
                vm.nylas_data.list = r.data.list;
                vm.nylas_data.index = 0;
                vm.nylas_data.country_dd = r.data.country_dd;
                vm.nylas_data.country_id = r.data.country_id;
                $timeout(function() {
                    vm.scroll_config = {
                        autoHideScrollbar: !1,
                        theme: 'minimal-dark',
                        axis: 'y',
                        scrollInertia: 50
                    }
                })
            }
        })
    }
    vm.backup_data_nylas = function(vm, r, file_id) {
        var data = {
            'do': 'customers-nylas_contacts-customers-backup_data',
            'xget': 'init',
            'file_id': file_id
        };
        vm.modal_nylas(data, 'cTemplate/makingFile', 'makingFileCtrl', 'sm')
    }
    vm.filterCust = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Customers'),
        create: !1,
        onDropdownOpen($dropdown) {
            var element_data = angular.element($dropdown.closest('tr'));
            vm.nylas_data.index = element_data[0].rowIndex
        },
        onDropdownClose($dropdown) {
            if (vm.nylas_data.list[vm.nylas_data.index - 1].customer_id == '99999999999') {
                vm.nylas_data.list[vm.nylas_data.index - 1].customer_id = undefined
            }
        },
        render: {
            option: function(item, escape) {
                var html = '<div class="row">' + '<div class="col-sm-12">' + '<p class="text-ellipsis">' + escape(item.name) + ' </p>' + '</div>' + '</div>';
                if (item.id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create new account') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined) {
                //angular.element('.loading_wrap').removeClass('hidden');
                helper.doRequest('get', 'index.php', {
                    'do': 'customers-nylas_contacts',
                    'xget': 'cc'
                }, function(r) {
                    vm.nylas_data.list[vm.nylas_data.index - 1].customer_dd = r.data;
                    selectize.open()
                })
            } else if (value == '99999999999') {
                var data = {
                    country_dd: sync.obj.country_dd,
                    country_id: sync.obj.country_id,
                    add_customer: !0,
                    'do': 'customers-nylas_contacts-customers-tryAddC',
                    'xget': 'created'
                };
                vm.openModal('add_account', data, 'md');
                return !1
            }
        },
        onType(value) {
            var element_data = angular.element('#contacts_list tbody tr:nth-child(' + vm.nylas_data.index + ') selectize');
            var selectize = element_data[0].selectize;
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                if (value != undefined) {
                    //angular.element('.loading_wrap').removeClass('hidden');
                    helper.doRequest('get', 'index.php', {
                        'do': 'customers-nylas_contacts',
                        'xget': 'cc',
                        'term': value
                    }, function(r) {
                        vm.nylas_data.list[vm.nylas_data.index - 1].customer_dd = r.data;
                        if (vm.nylas_data.list[vm.nylas_data.index - 1].customer_dd.length) {
                            selectize.open()
                        }
                    })
                }
            }, 300)
        },
        maxItems: 1
    };
    vm.deleteContact = function(index) {
        vm.nylas_data.list.splice(index, 1)
    }
    vm.checkAll = function() {
        for (x in vm.nylas_data.list) {
            vm.nylas_data.list[x].individual = vm.nylas_data.markAll;
            vm.nylas_data.list[x].disabled = vm.nylas_data.markAll
        }
        if (vm.nylas_data.markAll) {
            var data = {
                "country_dd": vm.nylas_data.country_dd,
                'country_id': undefined,
                'multiple': !0
            };
            vm.openModal("set_country", data, "md")
        }
    }
    vm.checkSync = function() {
        for (x in vm.nylas_data.list) {
            vm.nylas_data.list[x].sync = vm.nylas_data.markSync
        }
    }
    vm.setSync = function(index) {
        var nr_list = parseInt(vm.nylas_data.list.length);
        var temp_nr = 0;
        for (x in vm.nylas_data.list) {
            if (vm.nylas_data.list[x].sync) {
                temp_nr++
            }
        }
        if (nr_list == temp_nr) {
            vm.nylas_data.markSync = !0
        } else {
            vm.nylas_data.markSync = !1
        }
    }
    vm.setIndividual = function(index) {
        vm.nylas_data.list[index].disabled = !vm.nylas_data.list[index].disabled;
        var nr_list = parseInt(vm.nylas_data.list.length);
        var temp_nr = 0;
        for (x in vm.nylas_data.list) {
            if (vm.nylas_data.list[x].individual) {
                temp_nr++
            }
        }
        if (vm.nylas_data.list[index].individual) {
            if (nr_list == temp_nr) {
                vm.nylas_data.markAll = !0
            }
            var data = {
                "country_dd": vm.nylas_data.country_dd,
                'country_id': vm.nylas_data.list[index].country_id,
                'multiple': !1,
                'index': index
            };
            vm.openModal("set_country", data, "md")
        } else {
            if (nr_list != temp_nr) {
                vm.nylas_data.markAll = !1
            }
        }
    }
    vm.saveContacts = function(formObj) {
        var data = angular.copy(vm.nylas_data);
        data.do = 'customers--customers-addEmailContacts';
        //angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                var err_nr = 0;
                for (x in r.error) {
                    err_nr++
                }
                vm.showAlerts(vm, r, formObj);
                var msg = "<b>" + helper.getLanguage("You need to solve") + " <span class='text-danger'>" + err_nr + "</span> " + helper.getLanguage("errors") + "</b>";
                var cont = {
                    "e_message": msg
                };
                vm.openModal("stock_info", cont, "md")
            } else {
                $state.go('import_accounts', {
                    'tab': 'import_history'
                })
            }
        })
    }
    vm.ZenSyncNew = function(type) {
        var obj = {};
        if (type == 'import_account') {
            obj.nr = vm.zendesk_data.accounts_available;
            obj.type = type;
            obj.do_next = 'customers--zendesk-importAccount';
            obj.custom_fields = [];
            obj.custom_fields_id = [];
            for (x in vm.zendesk_data.account_custom_fields) {
                if (vm.zendesk_data.account_custom_fields[x].custom_field_id) {
                    for (y in vm.zendesk_data.account_fields) {
                        if (vm.zendesk_data.account_custom_fields[x].custom_field_id == vm.zendesk_data.account_fields[y].id) {
                            obj.custom_fields.push(vm.zendesk_data.account_custom_fields[x].name);
                            obj.custom_fields_id.push(vm.zendesk_data.account_fields[y].id);
                            break
                        }
                    }
                }
            }
            vm.openModal("sync_incoming_new", obj, "md")
        } else if (type == 'sync_one') {
            obj.nr = vm.zendesk_data.contacts_available;
            obj.type = type;
            obj.do_next = 'customers--zendesk-importMatchedContacts';
            vm.openModal("sync_incoming_new", obj, "md")
        } else if (type == 'sync_contact_change') {
            obj.nr = vm.zendesk_data.contacts_changed;
            obj.type = type;
            obj.do_next = 'customers--zendesk-updateChangedContacts';
            vm.openModal("sync_incoming_new", obj, "md")
        } else if (type == 'sync_membership') {
            obj.nr = vm.zendesk_data.accounts_imported;
            obj.type = type;
            obj.do_next = 'customers--zendesk-syncMembership';
            vm.openModal("sync_membership", obj, "md")
        } else {
            obj.nr = vm.zendesk_data.contacts_available;
            obj.type = type;
            obj.do_next = 'customers--zendesk-importContact';
            obj.custom_fields = [];
            obj.custom_fields_id = [];
            for (x in vm.zendesk_data.contact_custom_fields) {
                if (vm.zendesk_data.contact_custom_fields[x].custom_field_id) {
                    for (y in vm.zendesk_data.contact_fields) {
                        if (vm.zendesk_data.contact_custom_fields[x].custom_field_id == vm.zendesk_data.contact_fields[y].id) {
                            obj.custom_fields.push(vm.zendesk_data.contact_custom_fields[x].name);
                            obj.custom_fields_id.push(vm.zendesk_data.contact_fields[y].id);
                            break
                        }
                    }
                }
            }
            vm.openModal("sync_incoming_new", obj, "md")
        }
    }
    vm.setExportContacts = function() {
        vm.openModal('out_sync', '', 'lg')
    }
    vm.applySync = function(model) {
        if (vm.zendesk_data[model]) {
            vm.showAutoZen = !0
        } else {
            vm.showAutoZen = !1
        }
        helper.doRequest('post', 'index.php', {
            'do': 'customers--zendesk-applySync',
            'name': model,
            'value': vm.zendesk_data[model]
        })
    }
    vm.applyCustomAuto = function() {
        var obj = {
            'master': 'contact',
            'do': 'customers--zendesk-autoSync'
        };
        obj.custom_fields = [];
        obj.custom_fields_id = [];
        for (x in vm.zendesk_data.contact_custom_fields_matched) {
            if (vm.zendesk_data.contact_custom_fields_matched[x].custom_field_id) {
                for (y in vm.zendesk_data.contact_fields) {
                    if (vm.zendesk_data.contact_custom_fields_matched[x].custom_field_id == vm.zendesk_data.contact_fields[y].id) {
                        obj.custom_fields.push(vm.zendesk_data.contact_custom_fields_matched[x].name);
                        obj.custom_fields_id.push(vm.zendesk_data.contact_fields[y].id);
                        break
                    }
                }
            }
        }
        helper.doRequest('post', 'index.php', obj, function(r) {
            if (r.success) {
                vm.showAutoZen = !1
            }
        })
    }
    vm.openModal = function(type, item, size) {
        switch (type) {
            case 'sync_contacts':
                var cTemplate = 'SyncEmailContacts';
                var ctas = 'sync';
                break;
            case 'add_account':
                var cTemplate = 'AddCustomer';
                var ctas = 'vmMod';
                break;
            case 'set_country':
                var cTemplate = 'SetCountry';
                var ctas = 'vm';
                break;
            case 'stock_info':
                var cTemplate = 'modal';
                var ctas = 'vmMod';
                break;
            case 'progress_cash':
                var cTemplate = 'ProgressCash';
                var ctas = 'prog';
                break;
            case 'sync_incoming':
                var cTemplate = 'SyncIncomming';
                var ctas = 'sync';
                break;
            case 'out_sync':
                var cTemplate = 'ZenSyncContacts';
                var ctas = 'sync';
                break;
            case 'sync_incoming_new':
                var cTemplate = 'SyncIncomming';
                var ctas = 'sync';
                break;
            case 'sync_membership':
                var cTemplate = 'SyncIncomming';
                var ctas = 'sync';
                break
        }
        var params = {
            template: 'cTemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'ImportAccountFileCtrl':
                params.template = 'cTemplate/import_progres';
                params.item = item;
                params.ctrl = type;
                params.callback = function(data) {
                    if (data && data.data) {
                        vm.crm = data.data.crm;
                        vm.step = 1;
                        vm.class3 = 'text-muted';
                        $timeout(function() {
                            angular.element('.history').click()
                        })
                    }
                }
                break;
            case 'sync_contacts':
                params.template = 'cTemplate/' + cTemplate + 'Modal';
                params.ctrl = cTemplate + 'ModalCtrl';
                params.ctrlAs = ctas;
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        vm.nylas_data = d.data;
                        vm.showAlerts(d)
                    }
                }
                break;
            case 'add_account':
                params.template = 'miscTemplate/' + cTemplate + 'Modal';
                params.ctrl = cTemplate + 'ModalCtrl';
                params.ctrlAs = ctas;
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        vm.nylas_data.list[vm.nylas_data.index - 1].customer_dd = d.data.customer_dd;
                        vm.nylas_data.list[vm.nylas_data.index - 1].customer_id = d.data.customer_id
                    }
                }
                break;
            case 'set_country':
                params.template = 'cTemplate/' + cTemplate + 'Modal';
                params.ctrl = cTemplate + 'ModalCtrl';
                params.ctrlAs = ctas;
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(d) {
                    if (d.multiple) {
                        for (x in vm.nylas_data.list) {
                            vm.nylas_data.list[x].country_id = d.country_id
                        }
                    } else {
                        vm.nylas_data.list[d.index].country_id = d.country_id
                    }
                }
                params.callbackExit = function(d) {
                    if (d.multiple) {
                        vm.nylas_data.markAll = !1;
                        for (x in vm.nylas_data.list) {
                            vm.nylas_data.list[x].disabled = !1;
                            vm.nylas_data.list[x].individual = !1
                        }
                    } else {
                        vm.nylas_data.list[d.index].disabled = !1;
                        vm.nylas_data.list[d.index].individual = !1;
                        var nr_list = parseInt(vm.nylas_data.list.length);
                        var temp_nr = 0;
                        for (x in vm.nylas_data.list) {
                            if (vm.nylas_data.list[x].individual) {
                                temp_nr++
                            }
                        }
                        if (nr_list != temp_nr) {
                            vm.nylas_data.markAll = !1
                        }
                    }
                }
                break;
            case 'stock_info':
                params.template = 'miscTemplate/' + cTemplate;
                params.ctrl = 'viewRecepitCtrl';
                params.item = item;
                break;
            case 'progress_cash':
                params.template = 'miscTemplate/ProgressCashModal';
                params.ctrl = 'ProgressCashModalCtrl';
                params.ctrlAs = ctas;
                params.item = item;
                params.callback = function() {
                    vm.get_zendesk(vm.zendesk_data.view)
                }
                break;
            case 'sync_incoming':
                params.template = 'miscTemplate/SyncIncommingModal';
                params.ctrl = 'SyncIncommingModalCtrl';
                params.ctrlAs = ctas;
                params.item = item;
                params.callback = function() {
                    vm.get_zendesk(vm.zendesk_data.view)
                }
                break;
            case 'out_sync':
                params.template = 'cTemplate/ZenSyncContactsModal';
                params.ctrl = 'ZenSyncContactsModalCtrl';
                params.ctrlAs = ctas;
                params.params = {
                    'do': 'customers-zendesk_contacts',
                    'xget': 'contacts'
                };
                params.callback = function() {
                    vm.get_zendesk(vm.zendesk_data.view)
                }
                break;
            case 'add_customer':
                params.template = 'miscTemplate/AddCustomerModal';
                params.ctrl = 'AddCustomerModalCtrl';
                params.size = size;
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        vm.get_zendesk(vm.zendesk_data.view)
                    }
                }
                break;
            case 'sync_incoming_new':
                params.template = 'miscTemplate/SyncIncommingModal';
                params.ctrl = 'SyncIncommingNewModalCtrl';
                params.ctrlAs = ctas;
                params.backdrop = 'static';
                params.item = item;
                params.callback = function() {
                    vm.get_zendesk(vm.zendesk_data.view)
                }
                break;
            case 'sync_membership':
                params.template = 'miscTemplate/SyncIncommingModal';
                params.ctrl = 'SyncMembershipModalCtrl';
                params.ctrlAs = ctas;
                params.backdrop = 'static';
                params.item = item;
                params.callback = function() {
                    vm.get_zendesk(vm.zendesk_data.view)
                }
                break
        }
        modalFc.open(params)
    }
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'sm',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            for (x in data.data) {
                vm[x] = data.data[x]
            }
            if (data.data.step == 3) {
                vm.crm.class3 = 'text-primary';
                vm.crm.class2 = 'text-muted';
                vm.crm.step = ''
            } else {
                vm.crm.class3 = 'text-muted'
            }
        }, function() {})
    };
    vm.modal_nylas = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'sm',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            if (data && data.data) {
                vm.nylas_data = data.data
            }
        }, function() {})
    };
    vm.cancel = function() {
        $uibModal.dismiss('cancel')
    };
    vm.upload = function(vm, e) {
        angular.element(e.target).parents('.upload-box').find('.newUploadFile2').click()
    }
    angular.element('.newUploadFile2').on('click', function(e) {
        e.stopPropagation()
    }).on('change', function(e) {
        e.preventDefault();
        console.log('hereme');
        var _this = $(this),
            p = _this.parents('.upload-box'),
            x = p.find('.newUploadFile2');
        if (x[0].files.length == 0) {
            return !1
        }
        if (x[0].files[0].size > 99999999) {
            alert("Error: file is to big.");
            return !1
        }
        p.find('.tmpImgHolder img').attr('src', '');
        var tmppath = URL.createObjectURL(x[0].files[0]);
        p.find('.tmpImgHolder img').attr('src', tmppath);
        p.find('.btn').addClass('hidden');
        p.find('.nameOfTheFile').text(x[0].files[0].name);
        var formData = new FormData();
        formData.append("Filedata", x[0].files[0]);
        formData.append("do", 'customers-import_accounts-customers-uploadify');
        formData.append("xget", 'import');
        p.find('.upload-progress-bar').css({
            width: '0%'
        });
        p.find('.upload-file').removeClass('hidden');
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "index.php", !0);
        oReq.upload.addEventListener("progress", function(e) {
            var pc = parseInt(e.loaded / e.total * 100);
            p.find('.upload-progress-bar').css({
                width: pc + '%'
            })
        });
        oReq.onload = function(oEvent) {
            var res = JSON.parse(oEvent.target.response);
            setTimeout(function() {
                p.find('.upload-file').addClass('hidden')
            }, 1000);
            setTimeout(function() {
                p.find('.btn').removeClass('hidden')
            }, 1001);
            if (oReq.status == 200) {
                if (res.success) {
                    var params = {};
                    params['do'] = 'customers-import_accounts-customers-import_company';
                    params.xls_name = res.data.xls_name;
                    params.ajax = 1;
                    vm.modal(params, 'cTemplate/waitingModal', 'waitingAccountModalCtrl', 'sm')
                } else {
                    alert(res.error)
                }
            } else {
                alert("Error " + oReq.status + " occurred when trying to upload your file.")
            }
        };
        oReq.send(formData)
    })
}]).controller('waitingAccountModalCtrl', ['$scope', 'helper', 'obj', '$uibModalInstance', function($scope, helper, obj, $uibModalInstance) {
    console.log(obj);
    helper.doRequest('post', 'index.php', obj, function(r) {
        $uibModalInstance.close(r)
    })
}]).controller('historyCtrl', ['$scope', 'helper', 'list', 'obj', '$uibModalInstance', function($scope, helper, list, obj, $uibModalInstance) {
    var vm = this;
    vm.history = obj;
    vm.max_rows = 0;
    vm.id = vm.history[0];
    vm.field = vm.history[1];
    vm.search = {
        search: '',
        'do': 'customers-import_accounts',
        xget: 'history_import',
        showAdvanced: !1,
        showList: !1,
        offset: 1,
        import_id: vm.id,
        field: vm.field
    };
    vm.list = [];
    vm.renderList = function(d) {
        if (d.data) {
            for (x in d.data) {
                vm[x] = d.data[x]
            }
            vm.showAlerts(d)
        }
    }
    vm.searchThing = function() {
        list.search(vm.search, vm.renderList, vm)
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel')
    };
    if (vm.field == 'companies_add') {
        vm.title = 'Companies added'
    }
    if (vm.field == 'companies_update') {
        vm.title = 'Companies updated'
    }
    if (vm.field == 'contacts_add') {
        vm.title = 'Contacts added'
    }
    if (vm.field == 'contacts_update') {
        vm.title = 'Contacts updated'
    }
    var get = 'history_import';
    var data = {
        'do': 'customers-import_accounts',
        'xget': get,
        'import_id': vm.id,
        'field': vm.field
    };
    //angular.element('.loading_wrap').removeClass('hidden');
    helper.doRequest('get', 'index.php', data, vm.renderList)
}]).controller('makingFileCtrl', ['$scope', 'helper', 'obj', '$uibModalInstance', function($scope, helper, obj, $uibModalInstance) {
    var vm = this;
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    helper.doRequest('post', 'index.php', obj, function(r) {
        vm.crm = r.data.crm;
        vm.showAlerts(vm, r);
        $uibModalInstance.close(r)
    })
}]).controller('ImportAccountFileCtrl', ['$scope', 'helper', 'data', '$uibModalInstance', function($scope, helper, data, $uibModalInstance) {
    var vm = this;
    for (x in data) {
        vm[x] = data[x]
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.save_import = function(params, total, width, times, executed) {
        console.log(executed);
        helper.doRequest('post', 'index.php', params, function(r) {
            if (r.error) {
                vm.showAlerts(vm, r);
                $uibModalInstance.close('closed');
                return !1
            }
            executed++;
            angular.element('.import-progressbar-value').css({
                'width': executed * width + '%'
            });
            params.k = executed * 25;
            params.total_rows = params.k + 25;
            var imported = params.k > total ? total : params.k;
            angular.element('#imported').text(imported);
            if (executed < times) {
                return vm.save_import(params, total, width, times, executed)
            } else {
                var data_get = {
                    'do': 'customers-import_accounts',
                    'xget': 'import',
                    'step': '1',
                    'class3': 'text_muted',
                    'class1': 'text-primary'
                };
                helper.doRequest('get', 'index.php', data_get, function(r) {
                    vm.showAlerts(vm, r);
                    $uibModalInstance.close(r)
                })
            }
        })
    }
    vm.save_import(vm.params, vm.total, vm.width, vm.times, vm.executed)
}]).controller('SyncEmailContactsModalCtrl', ['$scope', '$timeout', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, $timeout, helper, $uibModalInstance, data, modalFc) {
    var sync = this,
        typePromise;
    sync.obj = data;
    sync.index = 0;
    sync.markAll = !1;
    sync.filterCust = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Customers'),
        create: !1,
        onDropdownOpen($dropdown) {
            var element_data = angular.element($dropdown.closest('tr'));
            sync.index = element_data[0].rowIndex
        },
        onDropdownClose($dropdown) {
            if (sync.obj.list[sync.index - 1].customer_id == '99999999999') {
                sync.obj.list[sync.index - 1].customer_id = undefined
            }
        },
        render: {
            option: function(item, escape) {
                var html = '<div class="row">' + '<div class="col-sm-12">' + '<p class="text-ellipsis">' + escape(item.name) + ' </p>' + '</div>' + '</div>';
                if (item.id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Create new account') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined) {
                //angular.element('.loading_alt').removeClass('hidden');
                helper.doRequest('get', 'index.php', {
                    'do': 'customers-nylas_contacts',
                    'xget': 'cc'
                }, function(r) {
                    //angular.element('.loading_alt').addClass('hidden');
                    sync.obj.list[sync.index - 1].customer_dd = r.data;
                    selectize.open()
                })
            } else if (value == '99999999999') {
                var data = {
                    country_dd: sync.obj.country_dd,
                    country_id: sync.obj.country_id,
                    add_customer: !0,
                    'do': 'customers-nylas_contacts-customers-tryAddC',
                    'xget': 'created'
                };
                openModal('add_account', data, 'md');
                return !1
            }
        },
        onType(value) {
            var element_data = angular.element('.modal-body table tbody tr:nth-child(' + sync.index + ') selectize');
            var selectize = element_data[0].selectize;
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                if (value != undefined) {
                    //angular.element('.loading_alt').removeClass('hidden');
                    helper.doRequest('get', 'index.php', {
                        'do': 'customers-nylas_contacts',
                        'xget': 'cc',
                        'term': value
                    }, function(r) {
                        //angular.element('.loading_alt').addClass('hidden');
                        sync.obj.list[sync.index - 1].customer_dd = r.data;
                        if (sync.obj.list[sync.index - 1].customer_dd.length) {
                            selectize.open()
                        }
                    })
                }
            }, 300)
        },
        maxItems: 1
    };
    sync.showAlerts = function(d, formObj) {
        helper.showAlerts(sync, d, formObj)
    }
    sync.cancel = function() {
        $uibModalInstance.close()
    };
    sync.deleteContact = function(index) {
        sync.obj.list.splice(index, 1)
    }
    sync.ok = function(formObj) {
        var data = angular.copy(sync.obj);
        data.do = 'customers-nylas_contacts-customers-addEmailContacts';
        data.xget = 'list';
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            if (r.error) {
                sync.showAlerts(r, formObj)
            } else {
                $uibModalInstance.close(r)
            }
        })
    }
    sync.checkAll = function() {
        for (x in sync.obj.list) {
            sync.obj.list[x].individual = sync.markAll;
            sync.obj.list[x].disabled = sync.markAll
        }
        if (sync.markAll) {
            var data = {
                "country_dd": sync.obj.country_dd,
                'country_id': undefined,
                'multiple': !0
            };
            openModal("set_country", data, "md")
        }
    }
    sync.setIndividual = function(index) {
        sync.obj.list[index].disabled = !sync.obj.list[index].disabled;
        var nr_list = parseInt(sync.obj.list.length);
        var temp_nr = 0;
        for (x in sync.obj.list) {
            if (sync.obj.list[x].individual) {
                temp_nr++
            }
        }
        if (sync.obj.list[index].individual) {
            if (nr_list == temp_nr) {
                sync.markAll = !0
            }
            var data = {
                "country_dd": sync.obj.country_dd,
                'country_id': sync.obj.list[index].country_id,
                'multiple': !1,
                'index': index
            };
            openModal("set_country", data, "md")
        } else {
            if (nr_list != temp_nr) {
                sync.markAll = !1
            }
        }
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'add_account':
                var iTemplate = 'AddCustomer';
                var ctas = 'vmMod';
                break;
            case 'set_country':
                var iTemplate = 'SetCountry';
                var ctas = 'vm';
                break
        }
        var params = {
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'add_account':
                params.template = 'miscTemplate/' + iTemplate + 'Modal';
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        sync.obj.list[sync.index - 1].customer_dd = d.data.customer_dd;
                        sync.obj.list[sync.index - 1].customer_id = d.data.customer_id
                    }
                }
                break;
            case 'set_country':
                params.template = 'cTemplate/' + iTemplate + 'Modal';
                params.backdrop = 'static';
                params.item = item;
                params.callback = function(d) {
                    if (d.multiple) {
                        for (x in sync.obj.list) {
                            sync.obj.list[x].country_id = d.country_id
                        }
                    } else {
                        sync.obj.list[d.index].country_id = d.country_id
                    }
                }
                params.callbackExit = function(d) {
                    if (d.multiple) {
                        sync.markAll = !1;
                        for (x in sync.obj.list) {
                            sync.obj.list[x].disabled = !1;
                            sync.obj.list[x].individual = !1
                        }
                    } else {
                        sync.obj.list[d.index].disabled = !1;
                        sync.obj.list[d.index].individual = !1;
                        var nr_list = parseInt(sync.obj.list.length);
                        var temp_nr = 0;
                        for (x in sync.obj.list) {
                            if (sync.obj.list[x].individual) {
                                temp_nr++
                            }
                        }
                        if (nr_list != temp_nr) {
                            sync.markAll = !1
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }
}]).controller('SetCountryModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var vm = this;
    vm.obj = data;
    vm.showAlerts = function(d) {
        helper.showAlerts(vm, d)
    }
    vm.cancel = function() {
        $uibModalInstance.dismiss(vm.obj)
    };
    vm.ok = function() {
        if (vm.obj.country_id == '' || vm.obj.country_id == undefined) {
            var obj = {
                "error": {
                    "error": helper.getLanguage("No data to sync")
                },
                "notice": !1,
                "success": !1
            };
            vm.showAlerts(obj);
            return !1
        }
        $uibModalInstance.close(vm.obj)
    }
}]).controller('ZenSyncContactsModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'modalFc', function($scope, helper, $uibModalInstance, data, modalFc) {
    var sync = this;
    sync.obj = data;
    sync.search = {
        search: '',
        'do': 'customers-zendesk_contacts',
        'xget': 'contacts',
        view: 0,
        offset: 1
    };
    sync.renderList = function(d) {
        if (d.data) {
            sync.obj = d.data
        }
    };
    sync.toggleSearchButtons = function(item, val) {
        sync.search[item] = val;
        var params = {
            offset: sync.search.offset
        };
        for (x in sync.search) {
            params[x] = sync.search[x]
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            sync.renderList(r)
        })
    };
    sync.searchThing = function() {
        var params = {
            offset: sync.search.offset
        };
        for (x in sync.search) {
            params[x] = sync.search[x]
        }
        //angular.element('.loading_alt').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            //angular.element('.loading_alt').addClass('hidden');
            sync.renderList(r)
        })
    };
    sync.selectAll = function(type) {
        var bool = !1;
        if (type == 1) {
            bool = !0
        }
        for (x in sync.obj.list) {
            sync.obj.list[x].checked = bool
        }
    }
    sync.showAlerts = function(d) {
        helper.showAlerts(sync, d)
    }
    sync.cancel = function() {
        $uibModalInstance.close()
    }
    sync.ok = function() {
        var new_data = {
            'list': []
        };
        for (x in sync.obj.list) {
            if (sync.obj.list[x].checked) {
                new_data.list.push(sync.obj.list[x])
            }
        }
        new_data.do_next = sync.obj.do_next;
        new_data.type = 1;
        openModal("sync_incoming", new_data, "md")
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'sync_incoming':
                var iTemplate = 'SyncIncomming';
                var ctas = 'sync';
                break
        }
        var params = {
            template: 'miscTemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'sync_incoming':
                params.item = item;
                params.callback = function() {
                    sync.searchThing()
                }
                break
        }
        modalFc.open(params)
    }
}]).controller('SyncIncommingNewModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var sync = this;
    sync.obj = data;
    sync.value = 0;
    sync.showAlerts = function(d) {
        helper.showAlerts(sync, d)
    }
    sync.cancel = function() {
        $uibModalInstance.close()
    }

    function syncIt(i, next, link) {
        if (next) {
            var prm = {};
            prm.do = sync.obj.do_next;
            prm.nr = i;
            prm.custom_fields = angular.copy(sync.obj.custom_fields);
            prm.custom_fields_id = angular.copy(sync.obj.custom_fields_id);
            if (link) {
                prm.link = link
            }
            helper.doRequest('post', 'index.php', prm, function(o) {
                var percent = i * 100 / sync.obj.nr;
                sync.value = parseInt(percent.toFixed(0));
                if (o.error) {
                    sync.showAlerts(o)
                }
                if (o.data.next) {
                    syncIt(o.data.nr, o.data.next, o.data.link)
                } else {
                    syncIt(o.data.nr, o.data.next)
                }
            })
        } else {
            sync.value = 100;
            if (sync.obj.type == 'import_account' || sync.obj.type == 'import_contact' || sync.obj.type == 'sync_one' || sync.obj.type == 'sync_contact_change') {
                helper.doRequest('post', 'index.php', {
                    'do': sync.obj.do_next + '_time'
                })
            }
        }
    }
    syncIt(0, !0)
}]).controller('SyncMembershipModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', function($scope, helper, $uibModalInstance, data) {
    var sync = this;
    sync.obj = data;
    sync.value = 0;
    sync.showAlerts = function(d) {
        helper.showAlerts(sync, d)
    }
    sync.cancel = function() {
        $uibModalInstance.close()
    }

    function syncIt(i, next) {
        if (i < sync.obj.nr) {
            var prm = {};
            prm.do = sync.obj.do_next;
            prm.nr = i;
            helper.doRequest('post', 'index.php', prm, function(o) {
                var percent = i * 100 / sync.obj.nr;
                sync.value = parseInt(percent.toFixed(0));
                if (o.error) {
                    sync.showAlerts(o)
                }
                i++;
                syncIt(i, !0)
            })
        } else {
            sync.value = 100
        }
    }
    syncIt(0, !0)
}])