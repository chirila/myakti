app.controller('ServicesCtrl', ['$scope', '$rootScope', '$cacheFactory', 'helper', 'list', 'modalFc', '$compile', 'data', function($scope, $rootScope, $cacheFactory, helper, list, modalFc, $compile, data) {
    $rootScope.opened_lists.aservices = true;
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    $scope.product_type = [{
        name: helper.getLanguage('All Services Type'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Archived'),
        id: -1
    }];
    $scope.activeView = 0;
    $scope.product_fam = [];
    $scope.list = [];
    $scope.nav = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'article-aservices',
        view: 0,
        xget: '',
        offset: 1,
        name: ''
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.listStats = [];
            $scope.lid = d.data.lid;
            $scope.type = d.data.pdf_type;
            $scope.adv_product = d.data.adv_product;
            $scope.product_fam = d.data.families;
            $scope.lr = d.data.lr;
            $scope.code_width=d.data.code_width;
            $scope.cust_width=d.data.cust_width;
            $scope.fam_width=d.data.fam_width;
            $scope.nav = d.data.nav;
            $scope.minimum_selected=d.data.minimum_selected;
            $scope.all_pages_selected=d.data.all_pages_selected;
            var ids = [];
            var count = 0;
            for(y in $scope.nav){
                ids.push($scope.nav[y]['article_id']);
                count ++;
            }
            sessionStorage.setItem('service.add_total', count);
            sessionStorage.setItem('service.add_list', JSON.stringify(ids));
        }
    };
    $scope.discount_act = function(item) {
        if (item.block_discount) {
            var do_next = 'article-add-service-block_discount'
        } else {
            var do_next = 'article-add-service-unblock_discount'
        }
        var data = {
            do: do_next,
            id_name: 'article_id',
            name: 'block_discount',
            table: 'pim_articles',
            article_id: item.article_id,
            price: item.base_price
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(d) {})
    }
    $scope.markCheckNew=function(i){
        list.markCheckNew($scope, 'article--service-saveAddToArchived', i);
    };
    $scope.checkAllPage=function(){
        $scope.listObj.check_add_all=true;
        $scope.markCheckNew();
    };
    $scope.checkAllPages=function(){
        list.markCheckNewAll($scope, 'article--service-saveAddToArchivedAll','1');
    };
    $scope.uncheckAllPages=function(){
        list.markCheckNewAll($scope, 'article--service-saveAddToArchivedAll','0');
    };
    $scope.archiveBulkServices = function() {
        var obj = angular.copy($scope.search);
        obj.do = 'article-aservices-service-archiveBulkServices';
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.renderList(r)
            }
        })
    };
    $scope.restoreBulkServices = function() {
        var obj = angular.copy($scope.search);
        obj.do = 'article-aservices-service-restoreBulkServices';
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.renderList(r)
            }
        })
    };
    $scope.archived = function(value) {
        if (value == 0) {
            $scope.search.archived = 0
        }
        if (value == 1) {
            $scope.search.archived = 1
        }
        list.filter($scope, value, $scope.renderList, true)
    };
    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
    };
    $scope.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search($scope.search, $scope.renderList, $scope)
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.listMore = function(index) {
        var new_line = '<tr class="versionInfo"><td colspan="7" class="no_border">' + '<h4 class="title_underline">' + $scope.list[index].name + ' (#' + $scope.list[index].code + ')</h4>' + '<div class="row">' + '<div class="col-xs-4 col-sm-4">' + '<p>' + helper.getLanguage('VAT') + ' <span class="text-muted">' + $scope.list[index].vat_value + ' %</span></p>' + '<p>' + helper.getLanguage('Default Price') + ' <span class="text-muted">' + $scope.list[index].base_price + '</span></p>' + '<p>' + helper.getLanguage('Description') + '<br>' + '<span class="text-muted">' + $scope.list[index].description + '</span></p>' + '</div>' + '<div class="col-xs-4 col-sm-4">' + '<p>' + helper.getLanguage('EAN Code') + ' <span class="text-muted">' + $scope.list[index].ean_code + '</span></p>' + '<p>' + helper.getLanguage('Category') + ' <span class="text-muted">' + $scope.list[index].category_name + '</span></p>' + '</div>' + '</div>'
        '</td></tr>';
        angular.element('.orderTable tbody tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.orderTable tbody tr.collapseTr').eq(index).next())($scope);
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').addClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').removeClass('hidden')
    }
    $scope.listLess = function(index) {
        angular.element('.orderTable tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').removeClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').addClass('hidden')
    }
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Services Type',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.filterCfgFam = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Families'),
        create: !1,
        onChange(value) {
            $scope.toggleSearchButtons('article_category_id', value)
        },
        maxItems: 1
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    list.init($scope, 'article-aservices', $scope.renderList, ($rootScope.reset_lists.aservices ? true : false))
    $rootScope.reset_lists.aservices = false;
}]).controller('ArticlesCtrl', ['$scope', '$rootScope', '$cacheFactory', '$timeout', 'helper', 'list', 'selectizeFc', 'modalFc', '$compile', 'data', function($scope, $rootScope, $cacheFactory, $timeout, helper, list, selectizeFc, modalFc, $compile, data) {
    var typePromise;
    $rootScope.opened_lists.articles = true;
    $scope.ord = '';
    $scope.archiveList = 1;
    $scope.reverse = !1;
    $scope.max_rows = 0;
    $scope.code_width = 0;
    $scope.product_type = [{
        name: helper.getLanguage('Supplier'),
        id: 0,
        active: 'active'
    }, {
        name: helper.getLanguage('Archived'),
        id: -1
    }];
    $scope.activeView = 0;
    $scope.product_fam = [];
    $scope.list = [];
    $scope.nav = [];
    $scope.listStats = [];
    $scope.search = {
        search: '',
        'do': 'article-articles',
        view: 0,
        xget: '',
        offset: 1,
        supplier_reference: '',
        name: '',
        active: 1,
        supplier_id:undefined,
        showAdvanced: !1,
        brand_id:undefined
    };
    $scope.listObj = {
        check_add_all: !1
    };
    $scope.renderList = function(d) {
        if (d.data.query) {
            $scope.list = d.data.query;
            $scope.max_rows = d.data.max_rows;
            $scope.allow_stock = d.data.allow_stock;
            $scope.adv_product = d.data.adv_product;
            $scope.code_width = d.data.code_width;
            $scope.listStats = [];
            $scope.lid = d.data.lid;
            $scope.type = d.data.pdf_type;
            $scope.product_fam = d.data.families;
            $scope.lr = d.data.lr;
            $scope.columns = d.data.columns;
            $scope.view_disabled = d.data.view_disabled;
            $scope.suppliers=d.data.suppliers;
            $scope.brands=d.data.brands;
            $scope.minimum_selected=d.data.minimum_selected;
            $scope.all_pages_selected=d.data.all_pages_selected;
            //$scope.nav = d.data.nav;
            //var ids = [];
            //var count = 0;
            //for(y of $scope.nav){
              //  ids.push($scope.nav[y]);
               // count ++;
            //}
            //console.log(count);
            //sessionStorage.setItem('article.add_total', count);
            //sessionStorage.setItem('article.add_list', JSON.stringify(ids));
            //console.log(ids);
            // $scope.initCheckAll();
        }
    };

    $scope.markCheckNew=function(i){
        list.markCheckNew($scope, 'article--article-saveAddToArchived', i);
    }
    $scope.checkAllPage=function(){
        $scope.listObj.check_add_all=true;
        $scope.markCheckNew();
    }
    $scope.checkAllPages=function(){
        list.markCheckNewAll($scope, 'article--article-saveAddToArchivedAll','1');
    }
    $scope.uncheckAllPages=function(){
        list.markCheckNewAll($scope, 'article--article-saveAddToArchivedAll','0');
    }

    $scope.initCheckAll=function(){
        var total_checked=0;
        for(x in $scope.list){
            if($scope.list[x].check_add_to_product){
                total_checked++;
            }
        }
        if($scope.list.length > 0 && $scope.list.length == total_checked){
            $scope.listObj.check_add_all=true;
        }else{
            $scope.listObj.check_add_all=false;
        }
    }

    $scope.filters = function(p) {
        list.filter($scope, p, $scope.renderList)
    };
    $scope.action = function(item, action) {
        list.action($scope, item, action, $scope.renderList)
    };
    $scope.toggleSearchButtons = function(item, val) {
        list.tSearch($scope, item, val, $scope.renderList)
        $scope.checkSearch()
    };
    $scope.checkSearch = function() {
        var data = angular.copy($scope.search),
            is_search = !1;
        data = helper.clearData(data, ['do', 'xget', 'showAdvanced', 'showList', 'offset']);
        for (x in data) {
            if (data[x]) {
                is_search = !0
            }
        }
        // vm.search.showList = is_search
    }
    $scope.searchThing = function(reset_list) {
        angular.element('.loading_wrap').removeClass('hidden');
        var params=angular.copy($scope.search);
        if(reset_list){
            params.reset_list='1';
        }
        list.search(params, $scope.renderList, $scope)
    };
    $scope.resetAdvanced = function() {
        $scope.search.commercial_name = undefined;
        $scope.search.commercial_name2 = undefined;
        $scope.search.brand_id=undefined;
        $scope.search.ean_code=undefined;
        $scope.search.showAdvanced = !1;
        $scope.searchThing();
    };
    $scope.orderBY = function(p) {
        list.orderBy($scope, p, $scope.renderList)
    };
    $scope.archived = function(value) {
        if (value == 0) {
            $scope.search.archived = 0
        }
        if (value == 1) {
            $scope.search.archived = 1
        }
        list.filter($scope, value, $scope.renderList, true)
    };
    $scope.createExports = function($event) {
        return list.exportFnc($scope, $event)
    };
    $scope.updateStock = function(item) {
        item.loadRequestStock = !0;
        var data = {
            do: 'article-add-article-update_stock',
            method_1: !0,
            article_id: item.article_id,
            stock: item.stock
        };
        helper.doRequest('post', 'index.php', data, function(d) {
            item.loadRequestStock = !1;
            if (d && d.success) {
                item.loadSuccessStock = !0
            } else {
                item.loadErrorStock = !0
            }
        })
    }
    $scope.updatePrice = function(item) {
        item.loadRequest = !0;
        var data = {
            do: 'article-add-article-update_price_list',
            article_id: item.article_id,
            price: item.base_price
        };
        helper.doRequest('post', 'index.php', data, function(d) {
            item.loadRequest = !1;
            if (d && d.success) {
                item.total_price = d.data.total_price_currency;
                item.loadSuccess = !0
            } else {
                item.loadError = !0
            }
        })
    }
    $scope.discount_act = function(item) {
        if (item.block_discount) {
            var do_next = 'article-add-article-block_discount'
        } else {
            var do_next = 'article-add-article-unblock_discount'
        }
        var data = {
            do: do_next,
            id_name: 'article_id',
            name: 'block_discount',
            table: 'pim_articles',
            article_id: item.article_id,
            price: item.base_price
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(d) {})
    }
    $scope.columnSettings = function() {
        $scope.openModal('column_set', {}, 'lg')
    }
    $scope.openModal = function(type, item, size) {
        var params = {
            template: 'atemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'setStock':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'article_id': item.article_id
                };
                params.ctrlAs = 'setStock';
                params.callback = function(data) {
                    if (data) {
                        item.stock = data.data.new_stock
                    }
                }
                break;
            case 'viewAntStock':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'article_id': item.article_id
                };
                params.ctrlAs = 'viewAntStock';
                params.callback = function(data) {
                    if (data) {
                        item.stock = data.data.new_stock
                    }
                }
                break;
            case 'viewStock':
                params.template = 'atemplate/setStockModal';
                params.params = {
                    'do': 'article-setStock',
                    'article_id': item.article_id,
                    'view_mode': !0
                };
                params.ctrlAs = 'setStock';
                params.ctrl = 'setStockModalCtrl';
                break;
            case 'column_set':
                params.template = 'miscTemplate/ColumnSettingsModal';
                params.ctrl = 'ColumnSettingsModalCtrl';
                params.params = {
                    'do': 'misc-column_set',
                    'list': 'articles'
                };
                params.ctrlAs = 'vm';
                params.callback = function(d) {
                    if (d) {
                        $scope.searchThing()
                    }
                }
                break
        }
        modalFc.open(params)
    }
    $scope.listMore = function(index) {
        var new_line = '<tr class="versionInfo"><td colspan="9" class="no_border">' + '<h4 class="title_underline">' + $scope.list[index].name + ' (#' + $scope.list[index].code + ')</h4>' + '<div class="row">' + '<div class="col-sm-4">' + '<p>' + helper.getLanguage('VAT') + ' <span class="text-muted">' + $scope.list[index].vat_value + ' %</span></p>' + '<p>' + helper.getLanguage('Default Price') + ' <span class="text-muted">' + $scope.list[index].base_price + '</span></p>' + '<p>' + helper.getLanguage('Description') + '<br>' + '<span class="text-muted">' + $scope.list[index].description + '</span></p>' + '</div>' + '<div class="col-sm-4">' + '<p>' + helper.getLanguage('EAN Code') + ' <span class="text-muted">' + $scope.list[index].ean_code + '</span></p>' + '<p>' + helper.getLanguage('Category') + ' <span class="text-muted">' + $scope.list[index].category_name + '</span></p>' + '</div>' + '</div>'
        '</td></tr>';
        angular.element('.orderTable tbody tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.orderTable tbody tr.collapseTr').eq(index).next())($scope);
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').addClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').removeClass('hidden')
    }
    $scope.listLess = function(index) {
        angular.element('.orderTable tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').removeClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').addClass('hidden')
    }
    $scope.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Product Type',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            $scope.search.archived = 0;
            if (value == -1) {
                $scope.search.archived = 1
            }
            $scope.search.is_picture = 0;
            if (value == 1) {
                $scope.search.is_picture = 1
            }
            if (value == 2) {
                $scope.search.is_picture = 2
            }
            $scope.filters(value)
        },
        maxItems: 1
    };
    $scope.filterCfgFam = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Families'),
        create: !1,
        onChange(value) {
            $scope.toggleSearchButtons('article_category_id', value)
        },
        maxItems: 1
    };
    $scope.saveForArchive = function(p) {
        if(p){
            if(p.check_add_to_product){
              var elem_counter=0;
              for(x in $scope.list){
                if($scope.list[x].check_add_to_product){
                  elem_counter++;
                }
              }
              if(elem_counter==$scope.list.length && $scope.list.length>0){
                $scope.listObj.check_add_all=true;
              }
            }else{
              $scope.listObj.check_add_all=false;
            }
        } 
        list.saveForPDF($scope, 'article--article-saveAddToArchived', p)
    }
    $scope.archiveBulkArticles = function() {
        var obj = angular.copy($scope.search);
        obj.do = 'article-articles-article-archiveBulkArticles';
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.renderList(r)
            }
        })
    }

    $scope.restoreBulkArticles = function() {
        var obj = angular.copy($scope.search);
        obj.do = 'article-articles-article-restoreBulkArticles';
        helper.doRequest('post', 'index.php', obj, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.renderList(r)
            }
        })
    }

    $scope.supplierCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: helper.getLanguage('All Suppliers'),
        onChange(value) {
            $scope.search.offset=1;
            $scope.searchThing();
        },
        onType(str) {
            var selectize = angular.element('#supplier_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'article-articles',
                    xget: 'cc',
                    term: str
                };
                helper.doRequest('get','index.php',data,function(r){
                    $scope.suppliers=r.data;
                    if($scope.suppliers.length){
                        selectize.open();
                    }
                });
            }, 300)
        },
        onBlur(){
            if ($scope.search.supplier_id == undefined || $scope.search.supplier_id == '') {
                var data = {
                    'do': 'article-articles',
                    xget: 'cc'
                };
                helper.doRequest('get','index.php',data,function(r){
                    $scope.suppliers=r.data;
                });
            }
        },
        maxItems: 1
    };
    $scope.brandCfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Brand'),
        onChange(value) {
            $scope.toggleSearchButtons('brand_id', value)
        }
    });
    // $scope.brandCfg = {
    //     valueField: 'id',
    //     labelField: 'name',
    //     searchField: ['name'],
    //     delimiter: '|',
    //     placeholder: helper.getLanguage('Brand'),
    //     onType(str) {
    //         var selectize = angular.element('#brands_list')[0].selectize;
    //         selectize.close();
    //         if (typePromise) {
    //             $timeout.cancel(typePromise)
    //         }
    //         typePromise = $timeout(function() {
    //             var data = {
    //                 'do': 'article-articles',
    //                 xget: 'brands',
    //                 term: str
    //             };
    //             helper.doRequest('get','index.php',data,function(r){
    //                 $scope.brands=r.data;
    //                 if($scope.brands.length){
    //                     selectize.open();
    //                 }
    //             });
    //         }, 300)
    //     },
    //     onBlur(){
    //         if ($scope.search.brand_id == undefined || $scope.search.brand_id == '') {
    //             var data = {
    //                 'do': 'article-articles',
    //                 xget: 'brands'
    //             };
    //             helper.doRequest('get','index.php',data,function(r){
    //                 $scope.brands=r.data;
    //             });
    //         }
    //     },
    //     maxItems: 1
    // };

    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    list.init($scope, 'article-articles', $scope.renderList, ($rootScope.reset_lists.articles ? true : false))
    $rootScope.reset_lists.articles = false;
}]).controller('ArticleCtrl', ['$scope', 'helper', function($scope, helper) {}]).controller('ArticleAddCtrl', ['$scope', '$cacheFactory', '$templateCache', '$stateParams', '$state', 'selectizeFc', '$timeout', 'helper', 'modalFc', 'list', 'data', 'Upload', 'editItem', 'Lightbox',function($scope, $cacheFactory, $templateCache, $stateParams, $state, selectizeFc, $timeout, helper, modalFc, list, data, Upload, editItem,Lightbox) {
    var add = this,
        typePromise;
    add.app = 'article';
    add.ctrlpag = 'add';
    add.tmpl = 'atemplate';
    add.duplicate_article_id = $stateParams.duplicate_article_id;
    add.item = {};
    add.upgrade = upgrade;
    add.cancel = cancel;
    add.setPrice = setPrice;
    add.revertPrice = revertPrice;
    add.showAlerts = showAlerts;
    add.openModal = openModal;
    add.editComposed = editComposed;
    add.saveComposed = saveComposed;
    add.deleteComposed = deleteComposed;
    add.deleteSerial = deleteSerial;
    add.deletePrice = deletePrice;
    add.saveOrderComposed = saveOrderComposed;
    add.article_id = $stateParams.article_id;
    add.addArticle = addArticle;
    add.removeArticleTax = removeArticleTax;
    add.changeSupplierRef = changeSupplierRef;
    add.getSupplierRef = getSupplierRef;
    add.addSupplierRef = addSupplierRef;
    add.removeReference = removeReference;
    add.setVisibility = setVisibility;
    add.item.showAlerts = showAlerts;
    add.show_custom = !1;
    old_add = {};
    add.item_exists = true;
    add.addCustomPrice = addCustomPrice;
    add.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    add.search = {
        search: '',
        'do': 'article-add',
        xget: 'articleCustom',
        showAdvanced: !1,
        showList: !1,
        offset: 1,
        article_id: add.article_id
    };
    add.search_sn = {
        search: '',
        'do': 'article-add',
        xget: 'articleSerial',
        showAdvanced: !1,
        showList: !1,
        offset: 1,
        article_id: add.article_id
    };
    add.search_batch = {
        search: '',
        'do': 'article-add',
        xget: 'articleBatch',
        showAdvanced: !1,
        showList: !1,
        offset: 1,
        article_id: add.article_id,
        status_id:0
    };
    add.batch_statuses = [
        {
            name: helper.getLanguage('All Statuses'),
            id: 0,
        }, {
            name: helper.getLanguage('Not available'),
            id: 1,
            active: ''
        }, {
            name: helper.getLanguage('In stock'),
            id: 2,
            active: ''
        },
        {
            name: helper.getLanguage('Sold'),
            id: 3,
            active: ''
        }
    ]; 
    add.checkSearch = checkSearch;
    if ($stateParams.form_type) {
        add.form_type = $stateParams.form_type
    }
    add.showTxt = {
        addArticleBtn: !1,
    }
    if (!add.form_type) {
        add.form_type = 'article'
    }
    for (x in data) {
        if(add.article_id != 'tmp' && x == 'ean_code'){
            //do nothing
        }else{
            add.item[x] = data[x]
        }
        
    }
    if (add.article_id != 'tmp') {
        add.item.article_id = add.article_id
    }
    add.searchThing = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        list.search(add.search, add.renderList, add)
    };
    add.renderList = function(d) {
        if (d.data.query) {
            add.list = d.data.query;
            add.max_rows = d.data.max_rows;
            add.lr = d.data.lr
        }
    };
    add.articlesListAutoCfg = {
        valueField: 'article_id',
        labelField: 'name',
        searchField: ['name', 'code'],
        delimiter: '|',
        placeholder: 'Select article',
        create: !1,
        maxOptions: 6,
        closeAfterSelect: !0,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-1">' + '<i class="fa ' + (item.is_service == 0 ? 'akti-icon o_akti_articles' : 'fa fa-list-alt') + '" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-7">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.code) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-right">' + (item.base_price) + '<br><small class="text-muted">' + helper.getLanguage('Stock') + ' <strong>' + escape(item.stock2) + '</strong></small>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'article-add',
                    xget: 'articles_list',
                    search: str
                };
                add.requestAuto(add,data, add.renderArticleListAuto)
            }, 300)
        },
        onChange(value) {
            if (value == undefined) {} else if (value == '99999999999') {
                return !1
            } else {
                for (x in add.composed.articles_list.lines) {
                    if (add.composed.articles_list.lines[x].article_id == value) {
                        var data = {
                            do: 'article-add-article-addCombinedArticle',
                            article_id: add.article_id,
                            parent_article_id: add.article_id,
                            value: add.composed.articles_list.lines[x].article_id,
                            use_combined: add.composed.use_combined,
                            xget: 'articleCombine'
                        }
                        helper.doRequest('post', 'index.php', data, function(r) {
                            if (r.success) {
                                add.showAlerts(r);
                                add.composed.articles = r.data.composed.articles;
                                add.composed.potential_stock = r.data.composed.potential_stock
                            }
                        });
                        add.composed.selected_article_id = '';
                        add.showTxt.addArticleBtn = !1;
                        var data = {
                            'do': 'article-add',
                            xget: 'articles_list',
                            article_id: add.article_id
                        };
                        add.requestAuto(add,data, add.renderArticleListAuto);
                        return !1
                    }
                }
            }
        },
        onItemAdd(value, $item) {
            if (value == '99999999999') {
                add.selected_article_id = ''
            }
        },
        onBlur() {
            $timeout(function() {
                add.showTxt.addArticleBtn = !1
            })
        },
        maxItems: 1
    };

    function addArticle() {
        add.showTxt.addArticleBtn = !0;
        $timeout(function() {
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.open()
        })
    }
    add.save_button_text = helper.getLanguage('Save');
    add.checkInput = function() {
        add.show_disabled = !1;
        add.save_button_text = helper.getLanguage('Save')
    }
    add.checkForm = function(formObj) {
       // console.log(add.show_disabled );
        if (add.show_disabled ) {
            add.save_button_text = helper.getLanguage('Saved')
        } else {
            add.save_button_text = helper.getLanguage('Save')
        }
        return !1
        
    } 
    add.save = function(formObj) {
        if(!add.show_disabled){
           formObj.$invalid = !0;
            add.article_id = $stateParams.article_id;
            add.save_button_text = helper.getLanguage('Saved')
            add.show_disabled =!0;
            var data = angular.copy(add.item);
            if (add.article_id == 'tmp') {
                data.do = 'article-add-article-add';
                if (formObj.$name == 'addAnService') {
                    data.do = 'article-add-service-add'
                }
            } else if (add.duplicate_article_id != 'tmp' && !add.article_id) {
                data.do = 'article-add-article-duplicate';
                if (formObj.$name == 'addAnService') {
                    data.do = 'article-add-service-duplicate'
                }
            } else {
                data.do = 'article-add-article-update';
                data.article_id = add.article_id;
                if (formObj.$name == 'addAnService') {
                    data.do = 'article-add-service-update'
                }
            }
            helper.doRequest('post', 'index.php', data, function(r) {
                if (r && r.data) {
                    if (add.article_id == 'tmp' && r.data.article_id && r.success) {
                        $state.go('article.add', {
                            article_id: r.data.article_id
                        });
                        return
                    }
                    if (add.duplicate_article_id != '' && r.data.article_id && r.success) {
                        $state.go('article.add', {
                            article_id: r.data.article_id
                        });
                        add.showAlerts(r, formObj);
                        return
                    }
                    if(r.success){
                        for (x in r.data) {
                            add.item[x] = r.data[x]
                        }
                    }        
                }
                add.showAlerts(r, formObj)
            }) 
        }
        
    }
    if (add.article_id != 'tmp') {

        helper.doRequest('get', 'index.php?do=article-add', {
            article_id: add.article_id
        }, function(r) {        
            add.item_exists = true;    
            if(!r.data.item_exists){
                add.item_exists = false;
                    console.log(add.item_exists);
                return 0;
            }; 
            for (x in r.data) {
                add.item[x] = r.data[x]
            }
            add.add_mode = !1;
            helper.updateTitle($state.current.name,add.item.internal_name);
            
        })
    } else {
        add.add_mode = !0
    }

    if (add.duplicate_article_id != 'tmp' && !add.article_id) {
        helper.doRequest('get', 'index.php?do=article-add', {
            duplicate_article_id: add.duplicate_article_id
        }, function(r) {
            for (x in r.data) {
                add.item[x] = r.data[x]
            }
            add.add_mode = !1
        })
    }
    add.customerAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select supplier'),
        create: !1,
        maxOptions: 6,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-8">' + '<strong class="text-ellipsis">' + escape(item.value) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.contact_name) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-muted text-right">' + '<small class="text-ellipsis"><span class="glyphicon glyphicon-map-marker"></span> ' + escape(item.country) + '</small>' + '</div>' + '</div>' + '</div>';
                if (item.id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> ' + helper.getLanguage('Add Supplier') + '</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined) {
                var selectize = angular.element('#inputSupplier')[0].selectize;
                selectize.close();
                if (typePromise) {
                    $timeout.cancel(typePromise)
                }
                typePromise = $timeout(function() {
                    var data = {
                        'do': 'article-accounts',
                        term: value,
                        supplier: 1,
                        noAdd: 'true'
                    };
                    add.requestAuto(add,data, add.renderCustomerAuto)
                }, 300)
            } else if (value == '99999999999') {
                var obj = {
                    country_dd: add.item.country_dd,
                    country_id: add.item.main_country_id,
                    add_customer: !0,
                    'do': add.app + '-article-' + add.app + '-tryAddSC',
                    item_id: add.item.article_id,
                }
                add.openModal('AddCustomer', obj, 'md');
                return !1
            }
        },
        onType(str) {
            var selectize = angular.element('#inputSupplier')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'article-accounts',
                    term: str,
                    supplier: 1,
                    noAdd: 'true'
                };
                add.requestAuto(add,data, add.renderCustomerAuto)
            }, 300)
        },
        onItemAdd(value, $item) {
            if (value == undefined) {
                add.item.supplier_name = ''
            } else if (value == '99999999999') {
                add.item.supplier_name = '';
                add.item.supplier_id='';
            } else {
                add.item.supplier_name = $item.html()
            }
        },
        maxItems: 1
    };
    add.ncustomerAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select customer'),
        create: !1,
        maxOptions: 6,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-8">' + '<strong class="text-ellipsis">' + escape(item.value) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.contact_name) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-muted text-right">' + '<small class="text-ellipsis"><span class="glyphicon glyphicon-map-marker"></span> ' + escape(item.country) + '</small>' + '</div>' + '</div>' + '</div>';
                if (item.id == '99999999999') {
                    html = '<div class="selectize-add_btn"><span class="glyphicon glyphicon-plus-sign"></span> Add Customer</div>'
                }
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onType(str) {
            var selectize = angular.element('#inputAccount')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'article-accounts',
                    term: str,
                    account: 1,
                    noAdd: !1
                };
                add.requestAuto(add,data, add.nrenderCustomerAuto)
            }, 300)
        },
        onItemAdd(value, $item) {
            if (value == undefined) {
                add.item.account_name = ''
            } else if (value == '99999999999') {
                add.item.account_name = ''
            } else {
                add.item.account_name = $item.html()
            }
        },
        maxItems: 1
    };
    add.supAutoCfg = {
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        delimiter: '|',
        placeholder: helper.getLanguage('Select supplier'),
        create: !1,
        maxOptions: 6,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-8">' + '<strong class="text-ellipsis">' + escape(item.value) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.contact_name) + '&nbsp;</small>' + '</div>' + '<div class="col-sm-4 text-muted text-right">' + '<small class="text-ellipsis"><span class="glyphicon glyphicon-map-marker"></span> ' + escape(item.country) + '</small>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.value) + '</div>'
            }
        },
        onChange(value) {
            if (value == undefined || value == '') {
                var selectize = angular.element('#supplier_list')[0].selectize;
                selectize.close();
                if (typePromise) {
                    $timeout.cancel(typePromise)
                }
                typePromise = $timeout(function() {
                    var data = {
                        'do': 'article-accounts',
                        term: value,
                        supplier: 1,
                        noAdd: !1
                    };
                    helper.doRequest('get', 'index.php', data, function(r) {
                        add.item.supplier_list = r.data
                    })
                }, 300)
            }
        },
        onType(str) {
            var selectize = angular.element('#supplier_list')[0].selectize;
            selectize.close();
            if (typePromise) {
                $timeout.cancel(typePromise)
            }
            typePromise = $timeout(function() {
                var data = {
                    'do': 'article-accounts',
                    term: str,
                    supplier: 1,
                    noAdd: !1
                };
                helper.doRequest('get', 'index.php', data, function(r) {
                    add.item.supplier_list = r.data;
                    if (add.item.supplier_list.length) {
                        selectize.open()
                    }
                })
            }, 300)
        },
        onBlur() {
            if (add.supplierRef.supplier_id == undefined || add.supplierRef.supplier_id == '') {
                $timeout(function() {
                    var data = {
                        'do': 'article-accounts',
                        supplier: 1,
                        noAdd: !1
                    };
                    helper.doRequest('get', 'index.php', data, function(r) {
                        add.item.supplier_list = r.data
                    })
                })
            }
        },
        maxItems: 1
    };
    add.requestAuto = requestAuto;
    add.renderCustomerAuto = renderCustomerAuto;
    add.nrenderCustomerAuto = nrenderCustomerAuto;
    add.renderArticleListAuto = renderArticleListAuto;
    add.salesCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: add.item.is_admin,
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                add.item[realModel] = o.data.lines;
                add.item[realModel + '_id'] = o.data.inserted_id
            })
        }
    }, !1, !0, add.item);
    add.extraCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 200,
        maxItems: 1,
        create: !1,
        onDropdownOpen($dropdown) {
            var _this = this,
                model_id = _this.$input[0].attributes.model.value;
            if (add.item[model_id] != undefined && add.item[model_id] != '0') {
                old_add[model_id] = add.item[model_id]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                add.item[realModel] = o.data.lines;
                add.item[realModel + '_id'] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            var opts = _this.$input[0].attributes.opts.value;
            var extra_id = undefined;
            if (item == '0') {
                add.openModal('selectEdit', {
                    table: table,
                    model: model,
                    extra_id: extra_id,
                    opts: opts,
                    upper: 1
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                add.item[model] = undefined;
                return !1
            }
        }
    }, !1, !0, add.item);
    add.ledgerCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: !1,
        render: {
            option: function(item, escape) {
                var html = '<div class="option option_multiline">' + '<div class="row">' + '<div class="col-sm-8">' + '<strong class="text-ellipsis">' + escape(item.name) + ' </strong>' + '<small class="text-ellipsis">' + escape(item.description) + '&nbsp;</small>' + '</div>' + '</div>' + '</div>';
                return html
            },
            item: function(data, escape) {
                return '<div class="item">' + '<span>' + escape(data.name) + '&nbsp;</span>' + '<span>' + escape(data.description) + '</span>'  + '</div>';
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                add.item[realModel] = o.data.lines;
                add.item[realModel + '_id'] = o.data.inserted_id
            })
        },
        onChange(value) {
      
                add.save_button_text = helper.getLanguage('Save');
                add.show_disabled = false;

        },
    };
    add.variantTypeCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            var nindex = _this.$input[0].attributes.nindex.value;
            var masterscope = _this.$input[0].attributes.masterscope.value;
            var haslist=_this.$input[0].attributes.haslist.value;
            var opts=_this.$input[0].attributes.opts.value;
            if(haslist){
                if (add[masterscope][haslist][nindex][model] != undefined && add[masterscope][haslist][nindex][model] != '0') {
                    old_add[masterscope+"_"+haslist+"_"+model+"_"+nindex] = add[masterscope][haslist][nindex][model];
                }
            }else{
                if (add[masterscope][nindex][model] != undefined && add[masterscope][nindex][model] != '0') {
                    old_add[masterscope+"_"+model+"_"+nindex] = add[masterscope][nindex][model];
                }
            }
            if (item == '0') {
                openModal('selectEditMulti', {
                    table: table,
                    model: model,
                    opts:opts,
                    haslist:haslist,
                    masterscope:masterscope,
                    nindex:nindex
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                if(haslist){
                    add[masterscope][haslist][nindex][model] = undefined;
                }else{
                    add[masterscope][nindex][model] = undefined;
                }               
                return !1
            }else{
                if(masterscope == 'articleVariants'){
                    helper.doRequest('post','index.php',{'do':'article--article-setArticleVariantType','id':add[masterscope][haslist][nindex].id,'type_id':add[masterscope][haslist][nindex][model]});
                }
            }
        }
    }, !1, !0, add.item);
    add.uploadFiles = function(files) {
        if (files && files.length) {
            Upload.upload({
                url: 'index.php',
                data: {
                    file: files,
                    'do': 'article-add-article-upload_photo1',
                    'article_id': add.article_id
                }
            }).then(function(r) {
                add.item.exist_image = !0;
                add.view_upload = !1;
                add.item.artPhoto = r.data.name
            }, function() {})
        }
    }
    add.upload = function(setBatchFile, e) {
        angular.element(e.target).parents('.profile_card').find('.newUploadFile2').click();
        angular.element('.newUploadFile2').off();
        angular.element('.newUploadFile2').on('click', function(e) {
            e.stopPropagation()
        }).on('change', function(e) {
            e.preventDefault();
            var _this = $(this),
                p = _this.parents('.profile_card'),
                x = p.find('.newUploadFile2');
            if (x[0].files.length == 0) {
                return !1
            }
            p.find('.nameOfTheFile').text(x[0].files[0].name);
            var formData = new FormData();
            formData.append("Filedata", x[0].files[0]);
            formData.append("do", 'article-add-article-upload_photo');
            formData.append("article_id", add.article_id);
            p.find('.upload-progress-bar').css({
                width: '0%'
            });
            p.find('.upload-file').removeClass('hidden');
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "index.php", !0);
            oReq.upload.addEventListener("progress", function(e) {
                var pc = parseInt(e.loaded / e.total * 100);
                p.find('.upload-progress-bar').css({
                    width: pc + '%'
                })
            });
            oReq.onload = function(oEvent) {
                var res = JSON.parse(oEvent.target.response);
                if (oReq.status == 200) {
                    if (res.success) {
                        $timeout(function() {
                            add.item.exist_image = !0;
                            add.view_upload = !1
                            p.find('.upload-file').addClass('hidden');
                           if (add.article != 'tmp') {
                                add.item.artPhoto = res.name
                            }
                        })
                    } else {}
                } else {
                    alert("Error " + oReq.status + " occurred when trying to upload your file.")
                }
            };
            oReq.send(formData);
           angular.element('.loading_wrap').addClass('hidden')
        })
    }
   /* add.upload = function(edit, e) {
        angular.element(e.target).parents('.profile_card').find('.newUploadFile2').click()
    }
    angular.element('.newUploadFile2').on('click', function(e) {
        e.stopPropagation()
    }).on('change', function(e) {
        e.preventDefault();
        var _this = $(this),
            p = _this.parents('.profile_card'),
            x = p.find('.newUploadFile2');
        if (x[0].files.length == 0) {
            return !1
        }
        var formData = new FormData();
        formData.append("Filedata", x[0].files[0]);
        formData.append("do", 'article-add-article-upload_photo');
        formData.append("article_id", add.article_id);
        angular.element('.loading_wrap').removeClass('hidden');
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "index.php", !0);
        oReq.onload = function(oEvent) {
            var res = JSON.parse(oEvent.target.response);
            if (oReq.status == 200) {
                if (res.success) {
                    $timeout(function() {
                        add.exist_image = !0;
                        add.view_upload = !1
                    });
                    if (add.article != 'tmp') {
                        add.item.artPhoto = res.name
                    }
                } else {}
            } else {
                alert("Error " + oReq.status + " occurred when trying to upload your file.")
            }
        };
        oReq.send(formData);
        angular.element('.loading_wrap').addClass('hidden')
    });*/
    add.remove_photo = function() {
        angular.element('.loading_wrap').removeClass('hidden');
        var data = {
            'do': 'article-add-article-remove_photo',
            'article_id': add.item.article_id
        };
        helper.doRequest('get', 'index.php', data, function(o) {
            add.item.artPhoto = add.item.default_image;
            add.item.exist_image=false;
        })
    };
    add.load = function(h, k) {
        add.alerts = [];
        var d = {
            'do': 'article-add',
            'xget': h,
            'article_id': add.article_id
        };
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('get', 'index.php', d, function(r) {
            add.showAlerts(r);
            add[k] = r.data[k];
            if(k == 'translation' && add.item.drop_info){
                add.showDrop();
            }
        })
    }
    add.updateCustomPrice = function(item) {
        item.loadRequest = !0;
        var custom_price = helper.return_value(item.custom_price);
        var data = {
            do: 'article-add-article-update_custom_price',
            customer_id: item.delete_link.customer_id,
            article_id: item.delete_link.article_id,
            custom_price: custom_price
        };
        helper.doRequest('post', 'index.php', data, function(d) {
            item.loadRequest = !1;
            if (d && d.success) {
                item.loadSuccess = !0
            } else {
                item.loadError = !0
            }
        })
    }
    add.action = function(item, action) {
        list.action(add, item, action, add.renderList)
    };
    add.toggleSearchButtons = function(item, val) {
        list.tSearch(add, item, val, add.renderList)
    };

    function checkSearch() {
        var data = angular.copy(add.search),
            is_search = !1;
        data = helper.clearData(data, ['do', 'xget', 'showAdvanced', 'showList', 'offset']);
        for (x in data) {
            if (data[x]) {
                is_search = !0
            }
        }
        add.search.showList = is_search
    }
    add.renderList = function(d) {
        if (d && d.data) {
            add.custom = d.data.custom
        }
    }

    function addCustomPrice(item) {
        /*var data = {
            do: 'article-add-article-update_custom_price',
            article_id: item.article_id,
            customer_id: item.custom.customer_id,
            custom_price: item.custom.custom_price,
            xget: 'articleCustom'
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.custom.article_id = '';
                add.custom.customer_id = '';
                add.custom.custom_price = ''
            }
        });
        list.action(add, item, 'add', add.renderList)*/
        openModal("addArticleCustomPrices",{article_id:add.item.article_id,base_price:add.item.price}, 'lg');
    }
    add.check_use_batch_no = function(item) {
        angular.element('.loading_wrap').removeClass('hidden');
        var data = {
            do: 'article-add-article-use_batch_no',
            article_id: item.article_id,
            use_batch_no: add.batch.use_batch_no
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.showAlerts(r)
            }
        })
    }
    add.check_use_serial_no = function(item) {
        angular.element('.loading_wrap').removeClass('hidden');
        var data = {
            do: 'article-add-article-use_serial_no',
            article_id: item.article_id,
            use_serial_no: add.serial.use_serial_no
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.item.stock = r.data.stock;
                if (r.data.use_serial_no) {
                    add.item.use_serial_no = !0
                } else {
                    add.item.use_serial_no = !1
                }
                add.showAlerts(r)
            }
        })
    }
    add.check_use_combined = function(item) {
        angular.element('.loading_wrap').removeClass('hidden');
        var data = {
            do: 'article-add-article-use_combined',
            article_id: item.article_id,
            use_combined: add.composed.use_combined
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.showAlerts(r)
            }
        })
    }

    function requestAuto(data, renderer) {
        helper.doRequest('get', 'index.php', data, renderer)
    }

    function renderCustomerAuto(scope,d) {
        if (d.data) {
            add.item.supplier = d.data;
            var selectize = angular.element('#inputSupplier')[0].selectize;
            selectize.open()
        }
    }

    function nrenderCustomerAuto(scope,d) {
        if (d.data) {
            add.custom.cc = d.data;
            var selectize = angular.element('#inputAccount')[0].selectize;
            selectize.open()
        }
    }

    function renderArticleListAuto(scope,d) {
        if (d.data) {
            add.composed.articles_list = d.data;
            var selectize = angular.element('#article_list_id')[0].selectize;
            selectize.open()
        }
    }

    function upgrade() {
        var data = {
            'do': 'article--article-upgrade'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $templateCache.remove("atemplate/add/articleTemplate");
            $state.reload()
        })
    }

    function getSupplierRef(supplier_id, tabSupplierRef) {
        var data = {
            do: 'article--article-get_supplier_ref',
            article_id: add.item.article_id,
            supplier_id: supplier_id,
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                if (tabSupplierRef) {
                    add.supplierRef.supplier_reference = r.data.supplier_reference;
                    add.supplierRef.purchasing_price = r.data.purchasing_price
                } else {
                    add.item.supplier_reference = r.data.supplier_reference
                }
            }
        })
    }

    function changeSupplierRef(item) {
        var data = {
            do: 'article--article-set_supplier_ref',
            article_id: add.item.article_id,
            supplier_id: add.item.supplier_id,
            supplier_reference: add.item.supplier_reference,
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.item.supplier_reference = r.data.supplier_reference;
                add.item.purchasing_price = r.data.purchasing_price
            }
        })
    }

    function addSupplierRef(item) {
        var data = {
            do: 'article-add-article-set_supplier_ref',
            article_id: item.article_id,
            supplier_id: item.supplierRef.supplier_id,
            supplier_reference: item.supplierRef.supplier_reference,
            purchasing_price: item.supplierRef.purchasing_price,
            xget: 'supplierRef'
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.supplierRef.references = r.data.supplierRef.references;
                add.supplierRef.supplier_reference = '';
                add.supplierRef.supplier_id = '';
                add.supplierRef.purchasing_price = '';
                var obj = {
                    'do': 'article-accounts',
                    supplier: 1,
                    noAdd: !1
                };
                helper.doRequest('get', 'index.php', obj, function(o) {
                    add.item.supplier_list = o.data
                })
            }
        })
    }

    function removeReference(item, supplier_id) {
        var data = {
            do: 'article-add-article-remove_article_reference',
            article_id: item.article_id,
            supplier_id: supplier_id,
            xget: 'supplierRef'
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.supplierRef.references = r.data.supplierRef.references
            }
        })
    }

    function cancel() {
        if (add.form_type == 'service') {
            $state.go('aservices')
        } else {
            $state.go('articles')
        }
    }

    function showAlerts(d, formObj) {
        helper.showAlerts(add, d, formObj)
    }

    add.checkUseVariants=function(){
        helper.doRequest('post','index.php',{'do':'article--article-setArticleHasVariants','has_variants':add.item.has_variants,'article_id':add.item.article_id},function(r){
            add.item.hide_stock=r.data.hide_stock;
        });
    }

    add.showVariants=function(){
        openModal('addArticleVariants',{},'lg','');
    }

    add.removeArticleVariant=function(item,index){
        helper.doRequest('post','index.php',{'do':'article--article-deleteArticleVariant','id':item.id},function(r){
            if(r.success){
                add.articleVariants.list.splice(index,1);
            }
        });
    }

    add.applySelection=function(event){
        if(add.item.allow_variants && add.item.has_variants){
            event.preventDefault();
        }       
    }

    add.showDrop = function() {
        add.show_load = !0;
        var data_drop = angular.copy(add.item.drop_info);
        data_drop.do = 'misc-dropbox_new';
        helper.doRequest('post', 'index.php', data_drop, function(r) {
            add.item.dropbox = r.data;
            add.show_load = !1
        })
    }
    add.deleteDropFile = function(index) {
        var data = angular.copy(add.item.dropbox.dropbox_files[index].delete_file_link);
        add.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            add.show_load = !1;
            add.showAlerts(r);
            if (r.success) {
                add.item.dropbox.nr--;
                add.item.dropbox.dropbox_files.splice(index, 1)
            }
        })
    }
    add.deleteDropImage = function(index) {
        var data = angular.copy(add.item.dropbox.dropbox_images[index].delete_file_link);
        add.show_load = !0;
        helper.doRequest('post', 'index.php', data, function(r) {
            add.show_load = !1;
            add.showAlerts(r);
            if (r.success) {
                add.item.dropbox.nr--;
                add.item.dropbox.dropbox_images.splice(index, 1)
            }
        })
    }
    add.openLightboxModal = function(index) {
        Lightbox.openModal(add.item.dropbox.dropbox_images, index)
    };
    add.uploadDoc = function(type) {
        var obj = angular.copy(add.item.drop_info);
        obj.type = type;
        openModal('uploadDoc', obj, 'lg','')
    }

    function openModal(type, item, size, txt) {
        var params = {
            template: 'atemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            txt: txt,
            backdrop: 'static'
        };
        switch (type) {
            case 'selectEdit':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.params = {
                    'do': 'misc-' + type,
                    'table': item.table,
                    'upper': item.upper
                };
                params.callback = function(data) {
                    if (data.data) {
                        add.item[item.opts] = data.data.lines;
                        if (old_add[item.model]) {
                            add.item[item.model] = old_add[item.model]
                        }
                    } else {
                        add.item[item.opts] = data.lines;
                        if (old_add[item.model]) {
                            add.item[item.model] = old_add[item.model]
                        }
                    }
                }
                break;
            case 'selectEditMulti':
                params.template = 'miscTemplate/selectEditModal';
                params.ctrl='selectEditModalCtrl';
                params.params = {
                    'do': 'misc-selectEdit',
                    'table': item.table
                };
                params.callback = function(data) {
                    if (data.data) {
                        add.item[item.opts] = data.data.lines;
                        if(item.haslist){
                            if(old_add[item.masterscope+"_"+item.haslist+"_"+item.model+"_"+item.nindex]){
                                add[item.masterscope][item.haslist][item.nindex][item.model]=old_add[item.masterscope+"_"+item.haslist+"_"+item.model+"_"+item.nindex];
                            }
                        }else{
                            if(old_add[item.masterscope+"_"+item.model+"_"+item.nindex]){
                                add[masterscope][nindex][model]=old_add[item.masterscope+"_"+item.model+"_"+item.nindex];
                            }
                        }
                        if (old_add[item.model]) {
                            add.item[item.model] = old_add[item.model]
                        }
                    } else {
                        add.item[item.opts] = data.lines;
                        if(item.haslist){
                            if(old_add[item.masterscope+"_"+item.haslist+"_"+item.model+"_"+item.nindex]){
                                add[item.masterscope][item.haslist][item.nindex][item.model]=old_add[item.masterscope+"_"+item.haslist+"_"+item.model+"_"+item.nindex];
                            }
                        }else{
                            if(old_add[item.masterscope+"_"+item.model+"_"+item.nindex]){
                                add[masterscope][nindex][model]=old_add[item.masterscope+"_"+item.model+"_"+item.nindex];
                            }
                        }
                    }
                }
                break;
            case 'setCategPrice':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'article_id': item.article_id,
                    'category_id': item.si_sup_cat_id
                };
                params.ctrlAs = 'setCategPrice';
                params.callback = function(data) {
                    if (data) {
                        add.item.price_categories = data.data.price_categories;
                        add.showAlerts(data)
                    }
                }
                break;
            case 'setPriceVol':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'article_id': item.article_id,
                    'article_price_id': item.article_price_id
                };
                params.ctrlAs = 'setPriceVol';
                params.callback = function(data) {
                    if (data) {
                        add.item.price_categories_vol = data.data.price_categories_vol;
                        add.showAlerts(data)
                    }
                }
                break;
            case 'setTreshold':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'article_id': item
                };
                params.ctrlAs = 'setTresh';
                params.callback = function(data) {
                    if (data) {
                        add.showAlerts(data)
                    }
                }
                break;
            case 'setParts':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'article_id': item
                };
                params.ctrlAs = 'setParts';
                params.callback = function(data) {
                    if (data) {
                        add.composed = data.data.composed;
                        add.showAlerts(data)
                    }
                }
                break;
            case 'setSerialNo':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'article_id': item
                };
                params.ctrlAs = 'setSerialNo';
                params.callback = function(data) {
                    if (data) {
                        add.item.stock = data.data.serial.new_stock;
                        add.serial = data.data.serial;
                        add.showAlerts(data)
                    }
                }
                break;
            case 'setBatchNo':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'article_id': item
                };
                params.ctrlAs = 'setBatchNo';
                params.callback = function(data) {
                    if (data) {
                        add.batch = data.data.batch;
                        add.showAlerts(data)
                    }
                }
                break;
            case 'editSerialNo':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'id': item
                };
                params.ctrlAs = 'editSerialNo';
                params.callback = function(data) {
                    if (data) {
                        add.serial = data.data.serial;
                        add.showAlerts(data)
                    }
                }
                break;
            case 'editBatchNo':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'id': item
                };
                params.ctrlAs = 'editBatchNo';
                params.callback = function(data) {
                    if (data) {
                        add.batch = data.data.batch;
                        add.showAlerts(data)
                    }
                }
                break;
            case 'viewBatchTrans':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'id': item
                };
                params.ctrlAs = 'viewBatchTrans';
                params.callback = function(data) {
                    if (data) {
                        add.trans = data.data.trans;
                        add.showAlerts(data)
                    }
                }
                break;
            case 'viewBatchTransIn':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'id': item
                };
                params.ctrlAs = 'viewBatchTransIn';
                params.callback = function(data) {
                    if (data) {
                        add.batch = data.data.batch;
                        add.showAlerts(data)
                    }
                }
                break;
            case 'setBatchDispatchStock':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'id': item
                };
                params.ctrlAs = 'setBatchDispatchStock';
                params.callback = function(data) {
                    if (data) {
                        add.batch = data.data.batch;
                        add.showAlerts(data)
                    }
                }
                break;
            case 'setBatchFile':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'id': item
                };
                params.ctrlAs = 'setBatchFile';
                params.callback = function(data) {
                    if (data) {
                        add.batch_file = data.data.batch_file;
                        add.showAlerts(data)
                    }
                }
                angular.element('.loading_wrap').removeClass('hidden');
                break;
            case 'setTrans':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'article_id': item,
                    'txt': txt
                };
                params.ctrlAs = 'setTrans';
                params.callback = function(data) {
                    if (data) {
                        add.translation = data.data.translation;
                        add.showAlerts(data)
                    }
                }
                break;
            case 'setStock':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'article_id': item,
                    'from_stock_tab': !0
                };
                params.ctrlAs = 'setStock';
                params.callback = function(data) {
                    if (data) {
                        add.item.stock = data.in.new_stock;
                        add.customer_stocks = data.data.customer_stocks;
                        add.showAlerts(data)
                    }
                }
                break;
            case 'viewAntStock':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'article_id': item
                };
                params.ctrlAs = 'viewAntStock';
                params.callback = function(data) {
                    if (data) {}
                }
                break;
            case 'setTaxes':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'article_id': item
                };
                params.ctrlAs = 'setTaxes';
                params.callback = function(d) {
                    if (d && d.data) {
                        add.item.taxes = d.data;
                        add.showAlerts(d)
                    }
                }
                break;
            case 'AddCustomer':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.item = item;
                params.callback = function(data) {
                    add.item.supplier = data.data.supplier;
                    add.item.supplier_id = data.data.supplier_id;
                    add.item.supplier_name = data.data.supplier_name;
                    var selectize = angular.element('#inputSupplier')[0].selectize;
                    selectize.close()
                }
                break;
            case 'addArticleVariants':
                params.params = {
                    'do': 'misc-addArticleVariantList',
                    'parent_article_id':add.item.article_id,
                    lang_id: 1
                };
                params.template = 'miscTemplate/addArticleVariantsModal';
                params.callbackExit = function(r) {
                    add.load('articleVariants', 'articleVariants');
                }
                break;
            case 'addArticleCustomPrices':
                params.template = 'atemplate/AddCustomPricesModal';
                params.ctrl="addArticleCustomPricesModalCtrl";
                params.initial_item = item;
                params.params = {
                    'do': 'article-addArticleCustomPrices',
                    'article_id':add.item.article_id
                };
                params.callback=function(){
                    add.load('articleCustom', 'custom');
                }
                break;
             case 'uploadDoc':
                params.template = 'miscTemplate/AmazonUploadModal';
                params.ctrl="AmazonUploadModalCtrl";
                params.item = item;
                params.callback = function(d) {
                    if (d) {
                        add.showDrop();
                    }
                }
                break;
        }
        modalFc.open(params)
    }

    editItem.init(add,{'renameFunction':'openModal'});
    add.addArticleCombined=function(item,index){
        editItem.openModalRenamed(add, 'addArticle', item,'lg');
    }

    add.dragControlListeners = {
        accept: function(sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id
        },
        itemMoved: function(eventObj) {
            var moveSuccess, moveFailure;
            moveSuccess = function() {};
            moveFailure = function() {
                eventObj.dest.sortableScope.removeItem(eventObj.dest.index);
                eventObj.source.itemScope.sortableScope.insertItem(eventObj.source.index, eventObj.source.itemScope.item)
            }
        },
        orderChanged: function(event) {           
            var collection = add.composed.articles;
            add.saveOrderComposed(collection[event.source.index].sort_order, collection[event.dest.index].sort_order );
        },
    }

    function saveOrderComposed(source, dest) {

        var order_arr = [];
        for (x in add.composed.articles) {
           order_arr[x] = add.composed.articles[x].article_id;
        }

       var data = {
            do: 'article-add-article-update_combined_order',
            parent_article_id: add.item.article_id,
            order_arr :order_arr,
            xget: 'articleCombine',
        }
         helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
               // console.log('success');
            }
        })
    }

    function editComposed(item) {
        item.edit_q = !0
    }

    function saveComposed(item, i) {
        item.edit_q = !1;
        var data = {
            do: 'article-add-article-update_combined_quantity',
            method_1: '1',
            article_id: item.article_id,
            parent_article_id: add.item.article_id,
            update_value: item.quantity,
            xget: 'articleCombine',
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.composed.articles[i].quantity = r.data.composed.articles[i].quantity;
                add.composed.potential_stock = r.data.composed.potential_stock
            }
        })
    }

    function setVisibility(item, i) {
        item.edit_q = !1;
     /*   console.log(item.visible, i);*/

        var data = {
            do: 'article-add-article-set_article_visibility',
            article_id: item.article_id,
            parent_article_id: add.item.article_id,
            visible: !item.visible,
            xget: 'articleCombine',
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.composed.articles[i].visible = r.data.composed.articles[i].visible;
                add.composed.articles[i].text_visible = r.data.composed.articles[i].text_visible;
            }
        })
    }

    function deleteSerial(item) {
        var data = {
            do: 'article-add-article-delete_serial_number',
            id: item.id,
            article_id: add.item.article_id,
            xget: 'articleSerial'
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.item.stock = r.data.serial.new_stock;
                add.serial.items = r.data.serial.items
            }
        })
    }

    function deleteComposed(item) {
        var data = {
            do: 'article-add-article-deleteCombinedArticle',
            pim_articles_combined_id: item.pim_articles_combined_id,
            parent_article_id: add.item.article_id,
            xget: 'articleCombine'
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.composed.articles = r.data.composed.articles;
                add.composed.potential_stock = r.data.composed.potential_stock
            }
        })
    }

    function deletePrice(item) {
        var data = {
            do: 'article-add-article-delete_quantity_price',
            article_price_id: item.article_price_id,
            article_id: item.article_id,
            xget: 'articlePriceVol'
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.item.price_categories_vol = r.data.price_categories_vol
            }
        })
    }

    function revertPrice(item) {
        var data = {
            do: 'article-add-article_price_category-revert_price_category',
            category_id: item.si_sup_cat_id,
            article_id: item.article_id,
            article_category_id: item.article_category_id,
            price: add.item.price,
            purchase_price: add.item.purchase_price,
            xget: 'articlePrice',
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.item.price_categories = r.data.price_categories
            }
        })
    }

    function setPrice(t) {
        var vat = 0;
        var digits_number = add.item.ARTICLE_PRICE_COMMA_DIGITS;
        add.show_disabled = !1;
        add.save_button_text = helper.getLanguage('Save');

        for (x in add.item.vat_dd) {
            if (add.item.vat_dd[x].id == add.item.vat_id) {
                vat = helper.return_value(add.item.vat_dd[x].name.split(' ')[0])
            }
        }
        switch (t) {
            case 'price':
            case 'vat_id':
                add.item.total_price = helper.displayNr(parseFloat(helper.return_value(add.item.price)) + (parseFloat(helper.return_value(add.item.price)) * parseFloat(vat) / 100), digits_number);
                break;
            case 'total_price':
                add.item.price = helper.displayNr(parseFloat(helper.return_value(add.item.total_price)) * 100 / (100 + parseFloat(vat)), digits_number);
                break
        }
    }

    function removeArticleTax(item, article_id) {
        var data = {
            do: 'article-add-article-remove_article_tax',
            article_id: article_id,
            tax_id: item.tax_id,
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                add.item.taxes = r.data.taxes
            }
        })
    }

    add.batchStatusCfg={
        valueField:'id',
        labelField: 'name',
        searchField: ['name'],
        maxItems: 1,
        onChange(value){
            var params={};
            for (x in add.search_batch) {
                params[x] = add.search_batch[x]
            }
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('get', 'index.php', params, function(r) {
                add.batch = r.data.batch;
            });
        }
    };

}]).controller('setCategPriceModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', function($scope, helper, $uibModalInstance, data, list) {
    var setCategPrice = this;
    setCategPrice.showAlerts = showAlerts;
    setCategPrice.obj = data;
    setCategPrice.ok = function(formObj) {
        var data = angular.copy(setCategPrice.obj);
        if (data.category_type == '4') {
            data.price_type = '3';
            data.price_value_type = '2'
        }
        data.do = 'article-add-article_price_category-update_for_article';
        data.xget = 'articlePrice';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                setCategPrice.showAlerts(r, formObj)
            } else {
                setCategPrice.showAlerts(r);
                $uibModalInstance.close(r)
            }
        })
    };
    setCategPrice.cancel = function() {
        $uibModalInstance.close()
    };

    function showAlerts(d) {
        helper.showAlerts(setCategPrice, d)
    }
}]).controller('setPriceVolModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', function($scope, helper, $uibModalInstance, data, list) {
    var setPriceVol = this;
    setPriceVol.showAlerts = showAlerts;
    setPriceVol.setPrice = setPrice;
    setPriceVol.obj = data;
    setPriceVol.ok = function(formObj) {
        formObj.$invalid = !0;
        var data = angular.copy(setPriceVol.obj);
        if (!data.article_price_id) {
            data.do = 'article-add-article-quantity_price'
        } else {
            data.do = 'article-add-article-quantity_price_update'
        }
        data.xget = 'articlePriceVol';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                setPriceVol.showAlerts(r, formObj)
            } else {
                setPriceVol.showAlerts(r);
                $uibModalInstance.close(r)
            }
        })
    };
    setPriceVol.cancel = function() {
        $uibModalInstance.close()
    };

    function showAlerts(d, formObj) {
        helper.showAlerts(setPriceVol, d, formObj)
    }

    function setPrice(t, base_price) {
        switch (t) {
            case 'percent':
                setPriceVol.obj.price_q = helper.displayNr(base_price - (parseFloat(helper.return_value(setPriceVol.obj.percent)) * base_price / 100));
                break;
            case 'price_q':
                setPriceVol.obj.percent = helper.displayNr(100 - (parseFloat(helper.return_value(setPriceVol.obj.price_q)) * 100) / base_price);
                break
        }
    }
}]).controller('viewAntStockModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', function($scope, helper, $uibModalInstance, data, list) {
    var viewAntStock = this;
    viewAntStock.list_order = data.query_order;
    viewAntStock.list_project = data.query_project;
    viewAntStock.list_intervention = data.query_intervention;
    viewAntStock.list_po_order = data.query_po_order;
    viewAntStock.article_id = data.article_id;
    viewAntStock.is_order_data = data.is_order_data;
    viewAntStock.is_project_data = data.is_project_data;
    viewAntStock.is_intervention_data = data.is_intervention_data;
    viewAntStock.is_po_order_data = data.is_po_order_data;
    viewAntStock.item_code = data.item_code;
    viewAntStock.internal_name = data.internal_name;
    viewAntStock.obj = data;
    viewAntStock.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('setStockModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', 'selectizeFc', 'modalFc', '$compile', function($scope, helper, $uibModalInstance, data, list, selectizeFc, modalFc, $compile) {
    var setStock = this;
    setStock.item = data;
    setStock.showAlerts = showAlerts;
    setStock.openModal = openModal;
    setStock.listMore = listMore;
    setStock.listLess = listLess;
    setStock.checkStockValue = checkStockValue;
    old_add = {};
    setStock.showTxt = {};
    for (x in setStock.item.query) {
        setStock.showTxt['stock_reason_' + x] = !1
    }

    function checkStockValue(item, $index) {
        var new_stock_val = helper.return_value(item.stock);
        var old_stock_val = helper.return_value(item.old_stock);
        if (new_stock_val == old_stock_val) {
            setStock.showTxt['stock_reason_' + $index] = !1
        } else if (item.require_reason_stock_modif) {
            setStock.showTxt['stock_reason_' + $index] = !0
        }
    }

    function listMore(index) {
        var new_line = '<tr class="versionInfo"><td colspan="3" class="no_border">' + '<h4 class="title_underline">' + helper.getLanguage('Edit Reason List') + '</h4>';
        if (setStock.item.query[index].edit_reasons) {
            var i = 1;
            new_line += '<div class="row">' + '<div class="col-md-1">' + '<p><strong>' + helper.getLanguage('No.') + '</strong></p>' + '</div>' + '<div class="col-md-3">' + '<p><strong>' + helper.getLanguage('Reason') + '</strong></p>' + '</div>' + '<div class="col-md-5">' + '<p><strong>' + helper.getLanguage('User Name') + '</strong></p>' + '</div>' + '<div class="col-md-1">' + '<p><strong>' + helper.getLanguage('Date') + '</strong></p>' + '</div>' + '</div>';
            for (x in setStock.item.query[index].edit_reasons) {
                new_line += '<div class="row">' + '<div class="col-md-1">' + '<p class="text-center">' + i + '.' + '</p>' + '</div>' + '<div class="col-md-3">' + '<p>' + setStock.item.query[index].edit_reasons[x].edit_reason + '</p>' + '</div>' + '<div class="col-md-5">' + '<p>' + setStock.item.query[index].edit_reasons[x].username + '</p>' + '</div>' + '<div class="col-md-1">' + '<p>' + setStock.item.query[index].edit_reasons[x].date + '</p>' + '</div>' + '</div>';
                i++
            }
        }
        new_line += '</td></tr>';
        angular.element('.orderTable tbody tr.collapseTr').eq(index).after(new_line);
        $compile(angular.element('.orderTable tbody tr.collapseTr').eq(index).next())($scope);
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').addClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').removeClass('hidden')
    }

    function listLess(index) {
        angular.element('.orderTable tbody tr.collapseTr').eq(index).next().remove();
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('span.glyphicon-plus').removeClass('hidden');
        angular.element('.orderTable tbody tr.collapseTr').eq(index).find('span.glyphicon-minus').addClass('hidden')
    }
    setStock.extraCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 200,
        maxItems: 1,
        create: !1,
        onDropdownOpen($dropdown) {
            var _this = this,
                model_id = _this.$input[0].attributes.model.value;
            if (setStock.item[model_id] != undefined && setStock.item[model_id] != '0') {
                old_add[model_id] = setStock.item[model_id]
            }
        },
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                setStock.item[realModel] = o.data.lines;
                setStock.item[realModel + '_id'] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            var opts = _this.$input[0].attributes.opts.value;
            var index = _this.$input[0].attributes.index.value;
            var extra_id = undefined;
            if (item == '0') {
                setStock.openModal('selectEdit', {
                    table: table,
                    model: model,
                    extra_id: extra_id,
                    opts: opts
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                setStock.item.query[index][model] = undefined;
                return !1
            }
        }
    }, !1, !0, setStock.item);

    function openModal(type, item, size, txt) {
        var params = {
            template: 'atemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            txt: txt,
            backdrop: 'static'
        };
        switch (type) {
            case 'selectEdit':
                params.template = 'miscTemplate/' + type + 'Modal';
                params.params = {
                    'do': 'misc-' + type,
                    'table': item.table
                };
                params.callback = function(data) {
                    if (data.data) {
                        setStock.item[item.opts] = data.data.lines;
                        if (old_add[item.model]) {
                            setStock.item[item.model] = old_add[item.model]
                        }
                    } else {
                        setStock.item[item.opts] = data.lines;
                        if (old_add[item.model]) {
                            setStock.item[item.model] = old_add[item.model]
                        }
                    }
                }
                break
        }
        modalFc.open(params)
    }
    setStock.obj = data;
    setStock.ok = function(formObj) {
        setStock.alertsField = [];
        for (var i = 0; i < setStock.obj.query.length; i++) {
            if (formObj['edit_reason_stock_id_' + i] != undefined) {
                formObj['edit_reason_stock_id_' + i].$invalid = !1
            }
        }
        var data = angular.copy(setStock.obj);
        if (setStock.from_stock_tab) {
            data.do = 'article-add-article-set_dispatch_stock';
            data.xget = 'articleDispatch'
        } else {
            data.do = 'article--article-set_dispatch_stock'
        }
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                setStock.showAlerts(r, formObj)
            } else {
                setStock.showAlerts(r);
                $uibModalInstance.close(r)
            }
        })
    };
    setStock.cancel = function() {
        $uibModalInstance.close()
    };

    function showAlerts(d, formObj) {
        helper.showAlerts(setStock, d, formObj)
    }
}]).controller('setTransModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', function($scope, helper, $uibModalInstance, data, list) {
    var setTrans = this;
    setTrans.list = data.translation;
    setTrans.article_id = data.article_id;
    setTrans.showAlerts = showAlerts;
    setTrans.obj = data;
    setTrans.ok = function(formObj) {
        var data = angular.copy(setTrans.obj);
        data.do = 'article--article-update_lang_fields';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                setTrans.showAlerts(r, formObj)
            } else {
                angular.forEach(r.data.translation, function(value, key) {
                    value.name = value.name.replace(new RegExp("\\\\", "g"), "");
                    value.name2 = value.name2.replace(new RegExp("\\\\", "g"), "");
                    value.description = value.description.replace(new RegExp("\\\\", "g"), "")
                });
                setTrans.showAlerts(r);
                $uibModalInstance.close(r)
            }
        })
    };
    setTrans.cancel = function() {
        $uibModalInstance.close()
    };

    function showAlerts(d) {
        helper.showAlerts(setTrans, d)
    }
}]).controller('setTresholdModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', function($scope, helper, $uibModalInstance, data, list) {
    var setTresh = this;
    setTresh.list = data.query;
    setTresh.article_id = data.article_id;
    setTresh.is_data = data.is_data;
    setTresh.showAlerts = showAlerts;
    setTresh.obj = data;
    setTresh.ok = function(formObj) {
        var data = angular.copy(setTresh.obj);
        data.do = 'article--article-set_threshold_value';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                setTresh.showAlerts(r, formObj)
            } else {
                setTresh.showAlerts(r);
                $uibModalInstance.close(r)
            }
        })
    };
    setTresh.cancel = function() {
        $uibModalInstance.close()
    };

    function showAlerts(d) {
        helper.showAlerts(setTresh, d)
    }
}]).controller('editSerialNoModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', function($scope, helper, $uibModalInstance, data, list) {
    var editSerialNo = this;
    editSerialNo.obj = data;
    editSerialNo.id = data.id;
    editSerialNo.format = 'dd/MM/yyyy';
    editSerialNo.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    editSerialNo.datePickOpen = {
        date2: !1,
        date: !1
    }
    editSerialNo.locationFilterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Locations',
        create: !1,
        onChange(value) {
            
        },
        maxItems: 1
    };
    editSerialNo.openDate = openDate;
    editSerialNo.showAlerts = showAlerts;

    function openDate(p) {
        editSerialNo.datePickOpen[p] = !0
    }
    
    if (editSerialNo.obj.date_in) {
        editSerialNo.obj.date_in = new Date(editSerialNo.obj.date_in)
    }
    if (editSerialNo.obj.date_out) {
        editSerialNo.obj.date_out = new Date(editSerialNo.obj.date_out)
    }
    editSerialNo.ok = function(formObj) {
        var data = angular.copy(editSerialNo.obj);
        data.do = 'article-add-article-update_serial_number';
        data.xget = 'articleSerial';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                editSerialNo.showAlerts(r, formObj)
            } else {
                editSerialNo.showAlerts(r);
                $uibModalInstance.close(r)
            }
        })
    };
    editSerialNo.cancel = function() {
        $uibModalInstance.close()
    };

    function showAlerts(d) {
        helper.showAlerts(editSerialNo, d)
    }
}]).controller('viewBatchTransModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', function($scope, helper, $uibModalInstance, data, list) {
    var viewBatchTrans = this;
    viewBatchTrans.list = data.query;
    viewBatchTrans.id = data.id;
    viewBatchTrans.is_data = data.is_data;
    viewBatchTrans.obj = data;
    viewBatchTrans.cancel = function() {
        $uibModalInstance.close()
    }
}]).controller('viewBatchTransInModalCtrl', ['$scope', 'helper', '$uibModalInstance','modalFc', 'data', 'list', function($scope, helper, $uibModalInstance, modalFc, data, list) {
    var viewBatchTransIn = this;
    viewBatchTransIn.list = data.query;
    viewBatchTransIn.id = data.id;
    viewBatchTransIn.is_data = data.is_data;
    viewBatchTransIn.obj = data;
    viewBatchTransIn.openModal = openModal;

    function openModal(type, item, size, txt) {
        var params = {
            template: 'atemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            txt: txt,
            backdrop: 'static'
        };
        switch (type) {
            
            case 'editBatchNo':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'id': item
                };
                params.ctrlAs = 'editBatchNo';
                params.callback = function(data) {
                    if (data && data.data) {
                        viewBatchTransIn.list = data.data.query;
                        viewBatchTransIn.id = data.data.id;
                        viewBatchTransIn.is_data = data.data.is_data;
                    }
                }
                break;         
        }
        modalFc.open(params)
    }

    viewBatchTransIn.cancel = function() {
         var data = angular.copy(viewBatchTransIn.obj);
        data.do = 'article-add';
        data.xget = 'articleBatch';
        //data.article_id =viewBatchTransIn.obj.article_id;
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r);
        })
        
    }
}]).controller('setBatchDispatchStockModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', function($scope, helper, $uibModalInstance, data, list) {
    var setBatchDispatchStock = this;
    setBatchDispatchStock.list = data.query;
    setBatchDispatchStock.id = data.id;
    setBatchDispatchStock.is_data = data.is_data;
    setBatchDispatchStock.obj = data;
    setBatchDispatchStock.save_disabled=true;
    setBatchDispatchStock.cancel = function() {
        $uibModalInstance.close()
    }

    setBatchDispatchStock.ok = function(formObj) {
        var data = angular.copy(setBatchDispatchStock.obj);
            data.do = 'article--article-set_batch_dispatch_stock';
         

        helper.doRequest('post', 'index.php', data, function(r) {

            $uibModalInstance.close(r)

        })
    };
    setBatchDispatchStock.checkStockValue = function(item, $index) {
        var sum = 0;
        for (x in setBatchDispatchStock.list) {
             sum = sum*1 +  helper.return_value(setBatchDispatchStock.list[x].stock)*1 ;
        }
        if(sum == helper.return_value(setBatchDispatchStock.obj.total_in_stock)*1){
            //console.log(setBatchDispatchStock.save_disabled);
            setBatchDispatchStock.save_disabled=false;
        }else{
            setBatchDispatchStock.save_disabled=true;
        }
        //console.log(sum, helper.return_value(setBatchDispatchStock.obj.total_in_stock)*1);
    }
}]).controller('setBatchFileModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', '$timeout', function($scope, helper, $uibModalInstance, data, list, $timeout) {
    var setBatchFile = this;
    setBatchFile.list = data.query;
    setBatchFile.id = data.id;
    setBatchFile.is_data = data.is_data;
    setBatchFile.obj = data;
    setBatchFile.delete = function(setBatchFile, r, id, path) {
        angular.element('.loading_alt').removeClass('hidden');
        var data = {
            'do': 'article-setBatchFile-article-deleteDropboxFile',
            'path': path,
            'id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            setBatchFile.list = r.data.query;
            angular.element('.loading_alt').addClass('hidden')
        })
    }
    setBatchFile.upload = function(setBatchFile, e) {
        angular.element(e.target).parents('.batch_file_form').find('.newUploadFile21').click();
        angular.element('.newUploadFile21').off();
        angular.element('.newUploadFile21').on('click', function(e) {
            e.stopPropagation()
        }).on('change', function(e) {
            e.preventDefault();
            var _this = $(this),
                p = _this.parents('.batch_file_form'),
                x = p.find('.newUploadFile21');
            if (x[0].files.length == 0) {
                return !1
            }
            p.find('.nameOfTheFile').text(x[0].files[0].name);
            var formData = new FormData();
            formData.append("Filedata", x[0].files[0]);
            formData.append("do", 'article-setBatchFile-article-uploadDropboxFile');
            formData.append("id", setBatchFile.id);
            p.find('.upload-progress-bar').css({
                width: '0%'
            });
            p.find('.upload-file').removeClass('hidden');
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "index.php", !0);
            oReq.upload.addEventListener("progress", function(e) {
                var pc = parseInt(e.loaded / e.total * 100);
                p.find('.upload-progress-bar').css({
                    width: pc + '%'
                })
            });
            oReq.onload = function(oEvent) {
                var res = JSON.parse(oEvent.target.response);
                if (oReq.status == 200) {
                    if (res.success) {
                        $timeout(function() {
                            p.find('.upload-file').addClass('hidden');
                            angular.element('.loading_alt').removeClass('hidden');
                            var data = {
                                'do': 'article-setBatchFile',
                                'id': setBatchFile.id
                            };
                            helper.doRequest('post', 'index.php', data, function(r) {
                                setBatchFile.list = r.data.query;
                                angular.element('.loading_alt').addClass('hidden')
                            })
                        })
                    } else {}
                } else {
                    alert("Error " + oReq.status + " occurred when trying to upload your file.")
                }
            };
            oReq.send(formData);
           angular.element('.loading_wrap').addClass('hidden')
        })
    }
    setBatchFile.cancel = function() {
        $uibModalInstance.close()
    };

    function showAlerts(d) {
        helper.showAlerts(setBatchFile, d)
    }
}]).controller('editBatchNoModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', function($scope, helper, $uibModalInstance, data, list) {
    var editBatchNo = this;
    editBatchNo.id = data.id;
    editBatchNo.format = 'dd/MM/yyyy';
    editBatchNo.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    editBatchNo.datePickOpen = {
        date2: !1,
        date3: !1,
        date: !1
    }
    editBatchNo.openDate = openDate;
    editBatchNo.showAlerts = showAlerts;

    function openDate(p) {
        editBatchNo.datePickOpen[p] = !0
    }
    editBatchNo.obj = data;
    if (editBatchNo.obj.date_in) {
        editBatchNo.obj.date_in = new Date(editBatchNo.obj.date_in)
    }
    if (editBatchNo.obj.date_out) {
        editBatchNo.obj.date_out = new Date(editBatchNo.obj.date_out)
    }
    if (editBatchNo.obj.date_exp) {
        editBatchNo.obj.date_exp = new Date(editBatchNo.obj.date_exp)
    }
    editBatchNo.ok = function(formObj) {
        var data = angular.copy(editBatchNo.obj);
        data.do = 'article-viewBatchTransIn-article-update_batch_number';
        //data.xget = 'articleBatch';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                editBatchNo.showAlerts(r, formObj)
            } else {
                editBatchNo.showAlerts(r);
                $uibModalInstance.close(r)
            }
        })
    };
    editBatchNo.cancel = function() {
        $uibModalInstance.close()
    };

    function showAlerts(d) {
        helper.showAlerts(editBatchNo, d)
    }
}]).controller('setSerialNoModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', function($scope, helper, $uibModalInstance, data, list) {
    var setSerialNo = this;
    setSerialNo.article_id = data.article_id;
    setSerialNo.showAlerts = showAlerts;
    setSerialNo.format = 'dd/MM/yyyy';
    setSerialNo.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    setSerialNo.datePickOpen = {
        date: !1
    }
    setSerialNo.openDate = openDate;
    setSerialNo.obj = data;
    setSerialNo.ok = function(formObj) {
        var data = angular.copy(setSerialNo.obj);
        data.do = 'article-add-article-add_serial_number';
        data.xget = 'articleSerial';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                setSerialNo.showAlerts(r, formObj)
            } else {
                setSerialNo.showAlerts(r);
                $uibModalInstance.close(r)
            }
        })
    };
    setSerialNo.cancel = function() {
        $uibModalInstance.close()
    };

    function openDate(p) {
        setSerialNo.datePickOpen[p] = !0
    }

    function showAlerts(d) {
        helper.showAlerts(setSerialNo, d)
    }
}]).controller('setBatchNoModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', function($scope, helper, $uibModalInstance, data, list) {
    var setBatchNo = this;
    setBatchNo.article_id = data.article_id;
    setBatchNo.showAlerts = showAlerts;
    setBatchNo.format = 'dd/MM/yyyy';
    setBatchNo.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1
    };
    setBatchNo.datePickOpen = {
        date2: !1,
        date3: !1,
        date: !1
    }
    setBatchNo.openDate = openDate;
    setBatchNo.obj = data;
    setBatchNo.ok = function(formObj) {
        var data = angular.copy(setBatchNo.obj);
        data.do = 'article-add-article-add_batch';
        data.xget = 'articleBatch';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                setBatchNo.showAlerts(r, formObj)
            } else {
                setBatchNo.showAlerts(r);
                $uibModalInstance.close(r)
            }
        })
    };

    function openDate(p) {
        setBatchNo.datePickOpen[p] = !0
    }
    setBatchNo.cancel = function() {
        $uibModalInstance.close()
    };

    function showAlerts(d) {
        helper.showAlerts(setBatchNo, d)
    }
}]).controller('setPartsModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', function($scope, helper, $uibModalInstance, data, list) {
    var setParts = this;
    setParts.article_id = data.article_id;
    setParts.showAlerts = showAlerts;
    setParts.obj = data;
    setParts.ok = function(formObj) {
        var data = angular.copy(setParts.obj);
        data.do = 'article-add-article-compose_combined';
        data.xget = 'articleCombine';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                setParts.showAlerts(r, formObj)
            } else {
                setParts.showAlerts(r);
                $uibModalInstance.close(r)
            }
        })
    };
    setParts.cancel = function() {
        $uibModalInstance.close()
    };

    function showAlerts(d) {
        helper.showAlerts(setParts, d)
    }
}]).controller('setTaxesModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', 'modalFc', '$templateCache', '$state', function($scope, helper, $uibModalInstance, data, list, modalFc, $templateCache, $state) {
    var setTaxes = this;
    setTaxes.list = data.query;
    setTaxes.article_id = data.article_id;
    setTaxes.is_data = data.is_data;
    setTaxes.showAlerts = showAlerts;
    setTaxes.obj=data;

    setTaxes.max_rows = data.max_rows;
    setTaxes.lr = data.lr;
    setTaxes.search = {
        search: '',
        'do': 'article-setTaxes',
        offset: 1
    };

    setTaxes.renderList = function(d) {
        if (d.data) {
            setTaxes.list = d.data.query;
            setTaxes.lr = d.data.lr;
            setTaxes.max_rows = d.data.max_rows
        }
    };

    setTaxes.searchThing = function() {
        helper.searchIt(setTaxes.search, setTaxes.renderList)
    }

    setTaxes.deleteTax = function(item, $index) {
        var tmp_data=angular.copy(setTaxes.search);
        tmp_data.do='article-setTaxes-article-delete_tax';
        tmp_data.tax_id=item;
        helper.doRequest('post', 'index.php', tmp_data, function(r) {
            if (r && r.success) {
                setTaxes.renderList(r);
            }
        })
    }
    setTaxes.action = function(item, $action) {
        var tmp_data=angular.copy(setTaxes.search);
        tmp_data.do='article-setTaxes-article-' + $action;
        tmp_data.article_id=setTaxes.article_id;
        tmp_data.tax_id=item;
        helper.doRequest('post', 'index.php', tmp_data, function(r) {
            if (r && r.success) {
                setTaxes.renderList(r);
            }
        })
    }
    setTaxes.ok = function(formObj) {
        var data = angular.copy(setTaxes.obj);
        data.article_id = setTaxes.article_id;
        data.do = 'article-add';
        data.xget = 'articleTaxes';
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                setTaxes.showAlerts(r, formObj)
            } else {
                $uibModalInstance.close(r)
            }
        })
    };
    setTaxes.cancel = function() {
        $uibModalInstance.close()
    };
    setTaxes.openModal = function(type, item, size) {
        var params = {
            template: 'atemplate/' + type + 'Modal',
            ctrl: type + 'ModalCtrl',
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'editTaxes':
                params.template = 'atemplate/' + type + 'Modal';
                params.params = {
                    'do': 'article-' + type,
                    'tax_id': item
                };
                params.ctrlAs = 'editTaxes';
                params.callback = function(d) {
                    if (d) {
                        setTaxes.renderList(d);
                    }
                }
                break
        }
        modalFc.open(params)
    }

    function showAlerts(d) {
        helper.showAlerts(setTaxes, d)
    }
}]).controller('editTaxesModalCtrl', ['$scope', 'helper', '$uibModalInstance', 'data', 'list', 'modalFc', 'selectizeFc', '$templateCache', '$state', function($scope, helper, $uibModalInstance, data, list, modalFc, selectizeFc, $templateCache, $state) {
    var editTaxes = this;
    if (data.item) {
        editTaxes.name = data.item.code;
        editTaxes.value = data.item.amount;
        editTaxes.description = data.item.description;
        editTaxes.type_id = data.item.type_id;
        editTaxes.tax_id = data.item.tax_id;
        editTaxes.tax_vat_id = data.item.tax_vat_id;
        editTaxes.apply_to_id = data.item.apply_to_id
    }
    editTaxes.showAlerts = showAlerts;
    editTaxes.tax_type_dd = data.tax_type_dd;
    editTaxes.vat_dd = data.vat_dd;
    editTaxes.apply_to_dd = data.apply_to_dd;
    editTaxes.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select type'),
        labelField: 'name',
        searchField: ['name'],
    });
    editTaxes.taxTypeCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: editTaxes.is_admin,
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                editTaxes[realModel] = o.data.lines;
                editTaxes.obj[realModel] = o.data.inserted_id
            })
        },
        onChange(item) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            var table = _this.$input[0].attributes.table.value;
            var model = _this.$input[0].attributes.model.value;
            if (item == '0') {
                editTaxes.openModal('selectEdit', {
                    table: table,
                    model: model
                }, {
                    'do': 'misc-selectEdit'
                }, 'md');
                editTaxes.obj[model] = undefined;
                return !1
            }
        }
    }, !1, !0, editTaxes);
    editTaxes.openModal = function(type, item, size, obj, extra_id) {
        if (type == 'selectEdit') {
            var params = {
                template: 'miscTemplate/' + type + 'Modal',
                ctrl: type + 'ModalCtrl',
                size: size
            }
        }
        switch (type) {
            case 'selectEdit':
                params.backdrop = 'static';
                params.params = {
                    'do': 'misc-selectEdit',
                    'table': item.table,
                    'extra_id': item.extra_id
                };
                params.callback = function(data) {
                    editTaxes[item.table] = data.data.lines
                }
                break
        }
        modalFc.open(params)
    }
    editTaxes.obj = data;
    editTaxes.ok = function(formObj) {
        var data = angular.copy(editTaxes.obj);
        data.do = data.do_next;
        data.code = editTaxes.name;
        data.amount = editTaxes.value;
        data.description = editTaxes.description;
        data.type_id = editTaxes.type_id;
        data.tax_id = editTaxes.tax_id;
        data.tax_vat_id = editTaxes.tax_vat_id;
        data.apply_to = editTaxes.apply_to_id - 1;
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.error) {
                editTaxes.showAlerts(r, formObj)
            } else {
                editTaxes.showAlerts(r);
                $uibModalInstance.close(r)
            }
        })
    };
    editTaxes.cancel = function() {
        $uibModalInstance.close()
    };

    function showAlerts(d) {
        helper.showAlerts(editTaxes, d)
    }
}]).controller('articlesSettingsCtrl', ['$scope', '$rootScope','$timeout','helper', '$uibModal', 'settingsFc', 'list', 'selectizeFc', 'data', 'modalFc', function($scope, $rootScope,$timeout,helper, $uibModal, settingsFc, list, selectizeFc, data, modalFc) {
    var vm = this;
    vm.app = 'articles';
    vm.ctrlpag = 'settings';
    vm.tmpl = 'atemplate';
    vm.data_article = {};
    vm.setarticles = {};
    vm.pricescateg = {};
    vm.volume = {};
    vm.add_volume = {};
    vm.taxes = {};
    vm.add_tax = {};
    vm.cfg = selectizeFc.configure({
        placeholder: helper.getLanguage('Select'),
        labelField: 'name',
        searchField: ['name'],
    });
    vm.price_search = {
        search: {
            search: '',
            'do': 'article-settings',
            xget: 'pricecateg'
        },
        pricelist: [],
    };
    vm.tax_search = {
        search: {
            search: '',
            'do': 'article-settings',
            xget: 'tax'
        },
        taxlist: [],
    };
    vm.ledge = {
        search: {
            search: '',
            'do': 'article-settings',
            xget: 'ledger',
            offset: 1
        },
        ledgelist: [],
    };
    vm.numbers = {};
    vm.ledge = {};
    vm.listObj = {
        check_add_all: 1
    };
    vm.renderList = function(d) {
        if (d.data) {
            vm.price_search = d.data.price_search;
            helper.showAlerts(vm, d)
        }
    };
    vm.renderListtax = function(d) {
        if (d.data) {
            vm.tax_search = d.data.tax_search;
            helper.showAlerts(vm, d)
        }
    };
    vm.renderListledge = function(r) {
        if (r && r.data) {
            for (x in r.data) {
                vm[x] = r.data[x]
            }
        }
    }

    function showAlerts(d) {
        helper.showAlerts(view, d)
    }
    vm.toggleSearchButtons = function(item, val) {
        console.log('h');
        pricelist.tSearch(vm.price_search, item, val, vm.renderList)
    };
    vm.filterCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            vm.price_search.search.archived = 0;
            if (value == -1) {
                vm.price_search.search.archived = 1
            }
            vm.filters(value)
        },
        maxItems: 1
    };
    vm.filters = function(p) {
        pricelist.filter(vm.price_search, p, vm.renderList)
    }
    vm.toggleSearchButtonstax = function(item, val) {
        console.log('h');
        list.tSearch(vm.tax_search, item, val, vm.renderListtax)
    };
    vm.filterCfgtax = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        delimiter: '|',
        placeholder: 'All Statuses',
        create: !1,
        onItemAdd(value, $item) {
            if (value == undefined) {
                value = 0
            }
            vm.tax_search.search.archived = 0;
            if (value == -1) {
                vm.tax_search.search.archived = 1
            }
            vm.filters(value)
        },
        maxItems: 1
    };
    vm.filterstax = function(p) {
        list.filter(vm.tax_search, p, vm.renderListtax)
    }
    vm.searchThing = function() {
        list.search(vm.ledge.search, vm.renderListledge, vm)
    }
    vm.load = function(h, k, i) {
        vm.alerts = [];
        var d = {
            'do': 'article-settings',
            'xget': h
        };
        helper.doRequest('get', 'index.php', d, function(r) {
            vm.showAlerts(r);
            vm[k] = r.data[k];
            if (i) {
                vm[i] = r.data[i]
            }
        })
    }
    vm.modal = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'lg',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.vat = data.data.vat;
            vm.language = data.data.language
        }, function() {})
    };
    vm.modal_small = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'lg',
            resolve: {
                obj: function() {
                    var i = angular.copy(r);
                    return i
                }
            }
        });
        modalInstance.result.then(function(r) {
            if (r && r.data) {
                for (x in r.data) {
                    vm[x] = r.data[x]
                }
            }
        }, function() {
            if (r.load) {
                vm.load(r.load[0], r.load[1])
            }
        })
    };
    vm.modal_tax = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.vat = data.data.vat;
            vm.language = data.data.language
        }, function() {})
    };
    vm.backup = function(vm, r) {
        var get = 'article_data';
        var data = {
            'do': 'article-settings-article-get_backup_tables',
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.data_article = r.data.data_article;
            vm.showAlerts(vm, r)
        })
    }
    vm.restoreData = function(vm, r) {
        var get = 'restore';
        var data = {
            'do': 'article-settings-article-backup_data',
            'xget': 'restore',
            'file_id': vm.restore.file_id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.restore = r.data.restore;
            vm.showAlerts(vm, r)
        })
    }
    vm.eraseData = function(vm, r) {
        var get = 'restore';
        var data = {
            'do': 'article-settings-article-erase_data',
            'xget': 'restore'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.restore = r.data.restore;
            vm.showAlerts(vm, r)
        })
    }
    vm.importData = function(vm, r) {
        var data_restore = {
            'do': 'article-settings-article-restore_data',
            'xget': 'restore',
            'backup_id': vm.restore.file2_id
        };
        vm.modal_tax(data_restore, 'atemplate/restoredata', 'RestoreCtrl', 'sm')
    }
    vm.article_settings = function() {
        var get = 'setarticle';
        var data = {
            'do': 'article-settings-article-article_settings',
            'xget': get,
            'ALLOW_ARTICLE_PACKING': vm.setarticles.ALLOW_ARTICLE_PACKING,
            'ALLOW_ARTICLE_SALE_UNIT': vm.setarticles.ALLOW_ARTICLE_SALE_UNIT,
            'ALLOW_QUOTE_PACKING': vm.setarticles.ALLOW_QUOTE_PACKING,
            'ALLOW_QUOTE_SALE_UNIT': vm.setarticles.ALLOW_QUOTE_SALE_UNIT,
            'ARTICLE_PRICE_COMMA_DIGITS': vm.setarticles.ARTICLE_PRICE_COMMA_DIGITS,
            'SHOW_TOTAL_MARGIN_QUOTE': vm.setarticles.SHOW_TOTAL_MARGIN_QUOTE,
            'SHOW_TOTAL_MARGIN_ORDER': vm.setarticles.SHOW_TOTAL_MARGIN_ORDER,
            'ALLOW_ARTICLE_VARIANTS' : vm.setarticles.ALLOW_ARTICLE_VARIANTS,
            'SHOW_ARTICLES_PDF' : vm.setarticles.SHOW_ARTICLES_PDF,
            'WAC': vm.setarticles.WAC,
            'AAC': vm.setarticles.AAC,
            'AUTO_AAC': vm.setarticles.AUTO_AAC
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.setarticles = r.data.setarticles;
            vm.showAlerts(vm, r)
        })
    }
    vm.update_default_settings = function(vm, r, SHOW_TOTAL_MARGIN_QUOTE) {
        var get = 'setarticle';
        var data = {
            'do': 'article-settings-article-update_default_settings',
            'xget': get,
            'SHOW_TOTAL_MARGIN_QUOTE': SHOW_TOTAL_MARGIN_QUOTE
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.setarticles = r.data.setarticles;
            vm.showAlerts(vm, r)
        })
    }
    vm.delete_price = function(vm, r, id) {
        var get = 'pricecateg';
        var data = {
            'do': 'article-settings-article-delete_price_category',
            'xget': get,
            'id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.price_search = r.data.price_search;
            vm.showAlerts(vm, r)
        })
    }
    vm.use_fam_price = function(vm, r) {
        helper.doRequest('post', 'index.php', r, function(r) {
            vm.pricescateg = r.data.pricescateg;
            vm.price_search = r.data.price_search;
            vm.showAlerts(vm, r)
        })
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.loadme = function() {
        var get = 'pricecateg';
        var data = {
            'do': 'article-settings',
            'xget': get
        };
        helper.doRequest('get', 'index.php', data, function(r) {
            vm.volume = r.data.volume;
            vm.add_volume = r.data.add_volume
        })
    }
    vm.delete_volume = function(vm, r, id) {
        var get = 'pricecateg';
        var data = {
            'do': 'article-settings-article-delete_volume',
            'xget': get,
            'id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.volume = r.data.volume;
            vm.showAlerts(vm, r)
        })
    }
    vm.delete_tax = function(vm, r, id) {
        var get = 'tax';
        var data = {
            'do': 'article-settings-article-delete_tax',
            'xget': get,
            'tax_id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.tax_search = r.data.tax_search;
            vm.showAlerts(vm, r)
        })
    }
    vm.showart = function(vm, r, SHOW_ART_S_N_ORDER_PDF) {
        var get = 'serial';
        var data = {
            'do': 'article-settings-article-show_art_serial_nr_del',
            'xget': get,
            'SHOW_ART_S_N_ORDER_PDF': SHOW_ART_S_N_ORDER_PDF
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.numbers = r.data.numbers;
            vm.showAlerts(vm, r)
        })
    }
    vm.showinv = function(vm, r, SHOW_ART_S_N_INVOICE_PDF) {
        var get = 'serial';
        var data = {
            'do': 'article-settings-article-show_art_serial_nr_inv',
            'xget': get,
            'SHOW_ART_S_N_INVOICE_PDF': SHOW_ART_S_N_INVOICE_PDF
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.numbers = r.data.numbers;
            vm.showAlerts(vm, r)
        })
    }
    vm.showbo = function(vm, r, SHOW_ART_B_N_ORDER_PDF) {
        var get = 'serial';
        var data = {
            'do': 'article-settings-article-show_art_batch_nr_del',
            'xget': get,
            'SHOW_ART_B_N_ORDER_PDF': SHOW_ART_B_N_ORDER_PDF
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.numbers = r.data.numbers;
            vm.showAlerts(vm, r)
        })
    }
    vm.showbi = function(vm, r, SHOW_ART_B_N_INVOICE_PDF) {
        var get = 'serial';
        var data = {
            'do': 'article-settings-article-show_art_batch_nr_inv',
            'xget': get,
            'SHOW_ART_B_N_INVOICE_PDF': SHOW_ART_B_N_INVOICE_PDF
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.numbers = r.data.numbers;
            vm.showAlerts(vm, r)
        })
    }
    vm.showbu = function(vm, r, UNIQUE_BATCH_NR) {
        var get = 'serial';
        var data = {
            'do': 'article-settings-article-unique_batch_nr',
            'xget': get,
            'UNIQUE_BATCH_NR': UNIQUE_BATCH_NR
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.numbers = r.data.numbers;
            vm.showAlerts(vm, r)
        })
    }
    vm.showbe = function(vm, r, SHOW_ART_B_ED_ORDER_PDF) {
        var get = 'serial';
        var data = {
            'do': 'article-settings-article-show_art_batch_ed_del',
            'xget': get,
            'SHOW_ART_B_ED_ORDER_PDF': SHOW_ART_B_ED_ORDER_PDF
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.numbers = r.data.numbers;
            vm.showAlerts(vm, r)
        })
    }
    vm.saveAddToArticle = function(vm, r, id, val) {
        var get = 'ledger',
            item = [];
        var check_me = '1';
        for (x in vm.ledge.ledgelist) {
            item.push(vm.ledge.ledgelist[x].fam_id);
            vm.ledge.ledgelist[x].check_add_to_article = val
        }
        var data = {
            'do': 'article-settings-article-saveAddToArticle',
            'xget': get,
            'id': id,
            'val': val,
            'check_all': check_me,
            item: item
        };
        helper.doRequest('post', 'index.php', data)
    }
    vm.applyledger = function(vm, r) {
        var get = 'ledger';
        var data = {
            'do': 'article-settings-article-apply_ledger_to_articles',
            'xget': get
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.ledge = r.data.ledge;
            vm.showAlerts(vm, r)
        })
    }
    vm.saveAddToArticleone = function(vm, r, id, val) {
        var get = 'ledger',
            item = [];
        for (x in vm.ledge.ledgelist) {
            item.push(vm.ledge.ledgelist[x].fam_id)
        }
        var data = {
            'do': 'article-settings-article-saveAddToArticle',
            'xget': get,
            'id': id,
            'val': val
        };
        helper.doRequest('post', 'index.php', data)
    }
    vm.add_fam = function(item) {
        item.loadRequest = !0;
        var data = {
            'do': 'article-settings-article-add_fam',
            xget: 'ledger',
            fam_id: item.fam_id,
            ledger_val: item.ledger_account_id
        };
        helper.doRequest('post', 'index.php', data, function(d) {
            item.loadRequest = !1;
            if (d && d.success) {
                item.loadSuccess = !0
            } else {
                item.loadError = !0
            }
        })
    }
    vm.renderList = function(r) {
        vm.setarticles = r.data.setarticles
    };

    vm.ledgerCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: !1,
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                item[realModel] = o.data.lines;
                item[realModel + '_id'] = o.data.inserted_id
            })
        }
    }, !1, !0, vm.item);

    vm.addCategory = function() {
        vm.openModal('add_category', {}, 'md')
    }

    vm.editCategory = function(id) {
        vm.openModal('add_category', {'id':id}, 'md')
    }

    vm.upload_category = function(e, fam_id) {
        angular.element('.newUploadFile2').on('click', function(e) {
            e.stopPropagation()
        }).on('change', function(e) {
            e.preventDefault();
            var _this = $(this),
                p = _this.parent(),
                x = p.find('.newUploadFile2'),
                row = _this.parents('tr')[0].rowIndex;
            var formData = new FormData();
            if (x[0].files[0] != undefined) {
                formData.append("Filedata", x[0].files[0])
            }
            formData.append("do", 'article--article-uploadCategoryPhoto');
            formData.append("category_id", fam_id);
            angular.element('.loading_wrap').removeClass('hidden');
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "index.php", !0);
            oReq.onload = function(oEvent) {
                var res = JSON.parse(oEvent.target.response);
                angular.element('.loading_wrap').addClass('hidden');
                if (oReq.status == 200) {
                    if (res.success) {
                        angular.element('.loading_wrap').removeClass('hidden');
                        helper.doRequest('get', 'index.php', {
                            'do': 'article-settings',
                            'xget': 'ledger'
                        }, function(r) {
                            vm.ledge = r.data.ledge;
                            vm.showAlerts(res)
                        })
                    } else {
                        vm.showAlerts(res)
                    }
                } else {
                    alert("Error " + oReq.status + " occurred when trying to upload your file.")
                }
            };
            oReq.send(formData)
        });
        angular.element(e.target).parent().find('.newUploadFile2').click()
    }

    vm.viewImage = function(item) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'miscTemplate/modal',
            controller: 'viewRecepitCtrl as vmMod',
            resolve: {
                data: function() {
                    var obj = {
                        e_message: '<center><img src="' + item.photo_link + '" class="img-responsive" ></center>'
                    }
                    return obj
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {}, function() {})
    };
    vm.removeImage = function(item) {
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'article-settings-article-removeCategoryPhoto',
            'xget': 'ledger',
            'category_id': item.fam_id
        }, function(r) {
            vm.showAlerts(r);
            if (r.success) {
                vm.ledge = r.data.ledge;
            }
        })
    }

    vm.deleteCategory = function(item) {
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'article-settings-article-deleteCategory',
            'xget': 'ledger',
            'category_id': item.fam_id
        }, function(r) {
            vm.showAlerts(r);
            if (r.success) {
                vm.ledge = r.data.ledge;
            }
            if(r.error){
                vm.showAlerts(vm, r)
            }
        })
    }

    vm.openModal = function(type, item, size) {
        var params = {
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'AddEntry':
                params.template = 'miscTemplate/' + type;
                params.ctrl = 'LedgerAccountsCtrl';
                params.params = {
                    'do': 'article-ledgers',
                    'id': item
                };
                params.callback = function(d) {
                    if (d) {
                        vm.ledge_new = d.data.ledge_new;
                        //vm.showAlerts(vm, d)
                    }
                }
                break;
            case 'add_category':
                params.template = 'atemplate/AddCategoryModal';
                params.ctrl = 'AddCategoryModalCtrl';
                params.ctrlAs='add';
                if(item.id){
                    params.params={'do':'article-settings','xget':'category','id':item.id};
                }else{
                    params.params={'do':'article-settings','xget':'category'};
                }                
                params.callback = function(d) {
                    if (d && d.data) {
                        vm.ledge = d.data.ledge;
                        vm.showAlerts(d)
                    }
                }
                break
        }
        modalFc.open(params)
    }
    vm.removeLine = function(id) {
        helper.doRequest('post', 'index.php', {
            'do': 'article-settings-article-deleteLedger',
            'xget': 'ledger_new',
            'id': id
        }, function(r) {
            vm.showAlerts(vm, r);
            if (r.success) {
                vm.ledge_new = r.data.ledge_new
            }
        })
    }
    vm.setDefaultCategory = function(vm, r, id) {
        var get = 'pricecateg';
        var data = {
            'do': 'article-settings-article-set_default_price_category',
            'xget': get,
            'id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.price_search.pricelist = r.data.price_search.pricelist;
            vm.showAlerts(vm, r)
        })
    }
    vm.saveLedgers = function() {
        var obj = {};
        obj.list = angular.copy(vm.ledge_new);
        obj.do = 'article--article-saveLedgers';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            if(!r.success){
                vm.showAlerts(vm, r)
            }
        })
    }
    vm.dragControlListenerLedgers = {
        orderChanged: function(event) {
            vm.saveLedgers();
        }
    }

    vm.saveBarcodesData=function(formObj){
        var obj = angular.copy(vm.barcodes);
        obj.do = 'article--article-saveBarcodes';
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', obj, function(r) {
            if(!r.success){
                vm.showAlerts(vm, r, formObj)
            }
        })
    }
    if(!$rootScope.show_settings.not_easy_invoice_diy){
        $timeout(function() {
            angular.element('.tabs-settings a[href="#article_families"]').tab('show').triggerHandler('click')
        })
    }

    vm.renderList(data)
}]).controller('PriceSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', 'list', function($scope, helper, $uibModalInstance, settingsFc, obj, list) {
    var vm = this,
        do_next = obj.id ? 'article-settings-article-update_category' : 'article-settings-article-add_price_category',
        do_xget = obj.id ? obj.xget : 'pricecateg';
    vm.obj = obj.id ? obj : obj[0];
    vm.search = {
        search: '',
        'do': 'article-settings',
        xget: 'family',
        offset: 1,
        category_id: obj.id
    };
    vm.pricescateg = {};
    vm.price_search = {};
    vm.renderList = function(r) {
        if (r && r.data) {
            for (x in r.data) {
                vm[x] = r.data[x]
            }
        }
        vm.setModalMaxHeight(angular.element('.modal.in'))
    }
    if (obj.id) {
        helper.doRequest('get', 'index.php', vm.search, vm.renderList)
    }
    vm.searchThing = function() {
        list.search(vm.search, vm.renderList, vm)
    }
    vm.save_me = function(vm) {
        var data = angular.copy(vm);
        data.do = do_next;
        data.xget = do_xget;
        helper.doRequest('post', 'index.php', data, function(r) {
            $uibModalInstance.close(r)
        })
    }
    vm.setModalMaxHeight = function(element) {
        var current_modal = angular.element(element),
            modal_content = current_modal.find('.modal-content'),
            borderWidth = modal_content.outerHeight() - modal_content.innerHeight(),
            dialogMargin = angular.element(window).width() < 768 ? 20 : 60,
            contentHeight = angular.element(window).height() - (dialogMargin + borderWidth),
            headerHeight = current_modal.find('.modal-header').outerHeight() || 0,
            footerHeight = current_modal.find('.modal-footer').outerHeight() || 0,
            maxHeight = contentHeight - (headerHeight + footerHeight);
        current_modal.find('.modal-body').css({
            'max-height': maxHeight,
            'overflow-y': 'auto'
        })
    }
    vm.updatePrice = function(item) {
        item.loadRequest = !0;
        var get = 'family';
        var data = {
            do: 'article-settings-article-update_fam_price_list',
            xget: get,
            fam_value: item.fam_price,
            fam_id: item.fam_id,
            category_id: item.category_id
        };
        helper.doRequest('post', 'index.php', data, function(d) {
            item.loadRequest = !1;
            if (d && d.success) {
                item.loadSuccess = !0
            } else {
                item.loadError = !0
            }
        })
    }

    function showAlerts(d) {
        helper.showAlerts(view, d)
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.update_category = function(vm, id, name, type, price_type, price_value, price_value_type) {
        var get = 'pricecateg';
        var data = {
            'do': 'article-settings-article-update_category',
            'xget': get,
            'category_id': id,
            'name': name,
            'type': type,
            'price_type_id': price_type,
            'price_value': price_value,
            'price_value_type_id': price_value_type
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r)
        })
    }
    vm.cancel = function() {
        vm.obj.VALUE = vm.oldValue
        $uibModalInstance.dismiss('dismiss')
    }
}]).controller('VolumeSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', 'list', function($scope, helper, $uibModalInstance, settingsFc, obj, list) {
    var vm = this,
        do_next = obj.id ? 'article-settings-article-update_category' : 'article-settings-article-add_volume',
        do_xget = obj.id ? obj.xget : 'pricecateg';
    vm.obj = obj.id ? obj : obj[0];
    if (obj.id) {
        helper.doRequest('get', 'index.php', vm.search, vm.renderList)
    }
    vm.add_volume = function(from_q, to_q, percent) {
        var get = 'pricecateg';
        var data = {
            'do': 'article-settings-article-add_volume',
            'xget': get,
            'from_q': from_q,
            'to_q': to_q,
            'percent': percent
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r)
            }
        })
    }
    vm.update_volume = function(id, from_q, to_q, percent) {
        var get = 'pricecateg';
        var data = {
            'do': 'article-settings-article-update_volume',
            'xget': get,
            'id': id,
            'from_q': from_q,
            'to_q': to_q,
            'percent': percent
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r)
            }
        })
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        vm.obj.VALUE = vm.oldValue
        $uibModalInstance.dismiss('dismiss')
    }
}]).controller('TaxSetCtrl', ['$scope', 'helper', '$uibModalInstance', '$uibModal', 'settingsFc', 'obj', 'list', function($scope, helper, $uibModalInstance, $uibModal, settingsFc, obj, list) {
    var vm = this,
        do_next = obj.id ? 'article-settings-article-update_tax' : 'article-settings-article-add_tax',
        do_xget = obj.id ? obj.xget : 'tax';
    vm.obj = obj.id ? obj : obj[0];
    vm.add_tax = function(code, type_id, amount, description) {
        var get = 'tax';
        var data = {
            'do': 'article-settings-article-add_tax',
            'xget': get,
            'code': code,
            'type_id': type_id,
            'amount': amount,
            'description': description
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                vm.tax_search = r.data.tax_search;
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r)
            }
        })
    }
    vm.update_volume = function(id, from_q, to_q, percent) {
        var get = 'pricecateg';
        var data = {
            'do': 'article-settings-article-update_volume',
            'xget': get,
            'id': id,
            'from_q': from_q,
            'to_q': to_q,
            'percent': percent
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r)
            }
        })
    }
    vm.update_tax = function(tax_id, code, type_id, amount, description) {
        var get = 'tax';
        var data = {
            'do': 'article-settings-article-update_tax',
            'xget': get,
            'tax_id': tax_id,
            'code': code,
            'type_id': type_id,
            'amount': amount,
            'description': description
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                vm.tax_search = r.data.tax_search;
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r)
            }
        })
    }
    vm.modal_type = function(r, template, ctrl, size) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: template,
            controller: ctrl + ' as vm',
            size: 'md',
            resolve: {
                obj: function() {
                    return r
                }
            }
        });
        modalInstance.result.then(function(data) {
            vm.showAlerts(vm, data);
            vm.vat = data.data.vat;
            vm.language = data.data.language
        }, function() {})
    };
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    vm.cancel = function() {
        vm.obj.VALUE = vm.oldValue
        $uibModalInstance.dismiss('dismiss')
    }
}]).controller('TypeSetCtrl', ['$scope', 'helper', '$uibModalInstance', 'settingsFc', 'obj', 'list', function($scope, helper, $uibModalInstance, settingsFc, obj, list) {
    var vm = this,
        do_next = obj.id ? 'article-settings-article-update_tax' : 'article-settings-article-add_tax',
        do_xget = obj.id ? obj.xget : 'tax';
    vm.obj = obj.id ? obj : obj[0];
    vm.manage = {};
    vm.add_tax = function(code, type_id, amount, description) {
        var get = 'tax';
        var data = {
            'do': 'article-settings-article-add_tax',
            'xget': get,
            'code': code,
            'type_id': type_id,
            'amount': amount,
            'description': description
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            if (r.success) {
                vm.tax_search = r.data.tax_search;
                $uibModalInstance.close(r)
            } else {
                vm.showAlerts(vm, r)
            }
        })
    }
    vm.delete_manage_tax = function(vm, r, id) {
        var get = 'tax';
        var data = {
            'do': 'article-settings-article-delete_select',
            'xget': get,
            'id': id
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            vm.showAlerts(vm, r)
        })
    }
    vm.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    helper.doRequest('get', 'index.php?do=article-settings&xget=tax', function(r) {
        vm.manage = r.data.manage
    });
    vm.cancel = function() {
        vm.obj.VALUE = vm.oldValue
        $uibModalInstance.dismiss('dismiss')
    }
}]).controller('RestoreCtrl', ['$scope', 'helper', 'obj', '$uibModalInstance', function($scope, helper, obj, $uibModalInstance) {
    var ls = this;
    ls.showAlerts = function(scope, d, formObj) {
        helper.showAlerts(scope, d, formObj)
    }
    helper.doRequest('post', 'index.php', obj, function(r) {
        ls = r.data;
        $uibModalInstance.close(r);
        helper.showAlerts(ls, r)
    })
}]).controller('ArticleFamiliesCtrl', ['$scope', 'helper', 'list', 'data', 'modalFc', '$uibModal', 'selectizeFc', function($scope, helper, list, data, modalFc, $uibModal, selectizeFc) {
    console.log("Article Families");
    $scope.ledge = [];
    $scope.search = {
        search: '',
        'do': 'article-settings',
        view: 0,
        xget: 'ledger'
    };
    $scope.listObj = {
        'check_add_all': !1
    };
    $scope.renderList = function(d) {
        if (d && d.data) {
            $scope.ledge = d.data.ledge
        }
    };
    $scope.showAlerts = function(d) {
        helper.showAlerts($scope, d)
    }
    $scope.toggleSearchButtons = function(item, val) {
        $scope.search[item] = val;
        for (x in $scope.search) {
            params[x] = $scope.search[x]
        }
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', params, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.searchThing = function() {
        var data = angular.copy($scope.search);
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.renderList(r)
        })
    };
    $scope.saveAddToArticle = function() {
        var item = [];
        for (x in $scope.ledge.ledgelist) {
            $scope.ledge.ledgelist[x].check_add_to_article = $scope.listObj.check_add_all;
            item.push($scope.ledge.ledgelist[x].fam_id)
        }
        var data = {
            'do': 'article-settings-article-saveAddToArticle',
            'xget': 'ledger',
            'val': $scope.listObj.check_add_all,
            'check_all': '1',
            item: item
        };
        helper.doRequest('post', 'index.php', data)
    }
    $scope.applyledger = function() {
        var data = {
            'do': 'article-settings-article-apply_ledger_to_articles',
            'xget': 'ledger'
        };
        helper.doRequest('post', 'index.php', data, function(r) {
            $scope.ledge = r.data.ledge;
            $scope.showAlerts(r)
        })
    }
    $scope.saveAddToArticleone = function(obj) {
        var item = [];
        for (x in $scope.ledge.ledgelist) {
            item.push($scope.ledge.ledgelist[x].fam_id)
        }
        var data = {
            'do': 'article-settings-article-saveAddToArticle',
            'xget': 'ledger',
            'id': obj.fam_id,
            'val': obj.check_add_to_article
        };
        helper.doRequest('post', 'index.php', data)
    }
    $scope.add_fam = function(item) {
        item.loadRequest = !0;
        var data = {
            'do': 'article-settings-article-add_fam',
            xget: 'ledger',
            fam_id: item.fam_id,
            ledger_val: item.ledger_account_id
        };
        helper.doRequest('post', 'index.php', data, function(d) {
            item.loadRequest = !1;
            if (d && d.success) {
                item.loadSuccess = !0
            } else {
                item.loadError = !0
            }
        })
    }
    $scope.addCategory = function() {
        openModal('add_category', {}, 'md')
    }
    $scope.ledgerCfg = selectizeFc.configure({
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1,
        create: !1,
        onOptionAdd: function(value, data) {
            var _this = this,
                lines = [],
                realModel = _this.$input[0].attributes.model.value,
                extra_id = undefined;
            if (_this.$input[0].attributes.extra) {
                extra_id = _this.$input[0].attributes.extra.value
            }
            for (x in _this.options) {
                lines.push(_this.options[x])
            }
            var d = {
                'table': _this.$input[0].attributes.table.value,
                'lines': lines,
                'do': "misc--misc-select_edit",
                'extra_id': extra_id
            }
            angular.element('.loading_wrap').removeClass('hidden');
            helper.doRequest('post', 'index.php', d, function(o) {
                item[realModel] = o.data.lines;
                item[realModel + '_id'] = o.data.inserted_id
            })
        }
    }, !1, !0, $scope.item);
    $scope.upload = function(e, fam_id) {
        angular.element('.newUploadFile2').on('click', function(e) {
            e.stopPropagation()
        }).on('change', function(e) {
            e.preventDefault();
            var _this = $(this),
                p = _this.parent(),
                x = p.find('.newUploadFile2'),
                row = _this.parents('tr')[0].rowIndex;
            var formData = new FormData();
            if (x[0].files[0] != undefined) {
                formData.append("Filedata", x[0].files[0])
            }
            formData.append("do", 'article--article-uploadCategoryPhoto');
            formData.append("category_id", fam_id);
            angular.element('.loading_wrap').removeClass('hidden');
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "index.php", !0);
            oReq.onload = function(oEvent) {
                var res = JSON.parse(oEvent.target.response);
                angular.element('.loading_wrap').addClass('hidden');
                if (oReq.status == 200) {
                    if (res.success) {
                        angular.element('.loading_wrap').removeClass('hidden');
                        helper.doRequest('get', 'index.php', {
                            'do': 'article-settings',
                            'xget': 'ledger'
                        }, function(r) {
                            $scope.renderList(r);
                            $scope.showAlerts(res)
                        })
                    } else {
                        $scope.showAlerts(res)
                    }
                } else {
                    alert("Error " + oReq.status + " occurred when trying to upload your file.")
                }
            };
            oReq.send(formData)
        });
        angular.element(e.target).parent().find('.newUploadFile2').click()
    }
    $scope.viewImage = function(item) {
        var modalInstance = $uibModal.open({
            animation: !0,
            templateUrl: 'miscTemplate/modal',
            controller: 'viewRecepitCtrl as vmMod',
            resolve: {
                data: function() {
                    var obj = {
                        e_message: '<center><img src="' + item.photo_link + '" class="img-responsive" ></center>'
                    }
                    return obj
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {}, function() {})
    };
    $scope.removeImage = function(item) {
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'article-settings-article-removeCategoryPhoto',
            'xget': 'ledger',
            'category_id': item.fam_id
        }, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.renderList(r)
            }
        })
    }
    $scope.deleteCategory = function(item) {
        angular.element('.loading_wrap').removeClass('hidden');
        helper.doRequest('post', 'index.php', {
            'do': 'article-settings-article-deleteCategory',
            'xget': 'ledger',
            'category_id': item.fam_id
        }, function(r) {
            $scope.showAlerts(r);
            if (r.success) {
                $scope.renderList(r)
            }
        })
    }

    function openModal(type, item, size) {
        switch (type) {
            case 'add_category':
                var iTemplate = 'AddCategory';
                var ctas = 'add';
                break
        }
        var params = {
            template: 'atemplate/' + iTemplate + 'Modal',
            ctrl: iTemplate + 'ModalCtrl',
            ctrlAs: ctas,
            size: size,
            backdrop: 'static'
        };
        switch (type) {
            case 'add_category':
                params.item = item;
                params.callback = function(d) {
                    if (d && d.data) {
                        $scope.renderList(d);
                        $scope.showAlerts(d)
                    }
                }
                break
        }
        modalFc.open(params)
    }
    $scope.renderList(data)
}]).controller('AddCategoryModalCtrl', ['$scope', 'helper', 'data', '$uibModalInstance', function($scope, helper, data, $uibModalInstance) {
    var add = this;
    add.obj = data;
    add.showAlerts = function(d, formObj) {
        helper.showAlerts(add, d, formObj)
    }
    add.cancel = function() {
        $uibModalInstance.close()
    }
    add.ok = function(formObj) {
        var obj = angular.copy(add.obj);
        obj.do = obj.do_next;
        obj.xget = 'ledger';
        helper.doRequest('post', 'index.php', obj, function(r) {
            add.showAlerts(r, formObj);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }

    add.ledgerCfg = {
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        placeholder: helper.getLanguage('Select type'),
        maxOptions: 50,
        maxItems: 1
    };


}]).controller('LedgerAccountsCtrl', ['$scope', 'helper', 'data', '$uibModalInstance', function($scope, helper, data, $uibModalInstance) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.showAlerts = function(d) {
        helper.showAlerts(vmMod, d)
    }
    vmMod.cancel = function() {
        $uibModalInstance.close()
    }
    vmMod.ok = function() {
        var obj = angular.copy(vmMod.obj);
        obj.do = 'article-settings-article-editLedger';
        obj.xget = 'ledger_new';
        helper.doRequest('post', 'index.php', obj, function(r) {
            vmMod.showAlerts(r);
            if (r.success) {
                $uibModalInstance.close(r)
            }
        })
    }
}]).controller('addArticleCustomPricesModalCtrl', ['$scope', '$timeout', 'helper', 'data', 'modalFc', '$uibModalInstance', '$window', '$state', 'list', function($scope, $timeout, helper, data, modalFc, $uibModalInstance, $window, $state, list) {
    var vmMod = this;
    vmMod.obj = data;
    vmMod.search = {
        search: '',
        'do': 'article-addArticleCustomPrices',
        article_id: vmMod.obj.article_id,
        offset: 1
    };
    vmMod.ok = function(item) {
        var custom_price = helper.return_value(item.custom_price);
        var data = {
            do: 'article-addArticleCustomPrices-article-update_custom_price',
            customer_id: item.customer_id,
            article_id: vmMod.obj.article_id,
            custom_price: custom_price
        };
        helper.doRequest('post', 'index.php', data, function(d) {
            item.loadRequest = !1;
            if (d && d.success) {
                item.loadSuccess = !0
            } else {
                item.loadError = !0
            }
        });
        item.succes_add = !0
    };
    vmMod.cancel = function() {
        $uibModalInstance.close()
    };
    vmMod.searchThing = function() {
        list.search(vmMod.search, vmMod.renderList, vmMod)
    }
    vmMod.renderList = function(r) {
        if (r && r.data) {
            for (x in r.data) {
                vmMod.obj[x] = r.data[x]
            }
        }
        vmMod.setModalMaxHeight(angular.element('.modal.in'))
    }
    vmMod.setModalMaxHeight = function(element) {
        var current_modal = angular.element(element),
            modal_content = current_modal.find('.modal-content'),
            borderWidth = modal_content.outerHeight() - modal_content.innerHeight(),
            dialogMargin = angular.element(window).width() < 768 ? 20 : 60,
            contentHeight = angular.element(window).height() - (dialogMargin + borderWidth),
            headerHeight = current_modal.find('.modal-header').outerHeight() || 0,
            footerHeight = current_modal.find('.modal-footer').outerHeight() || 0,
            maxHeight = contentHeight - (headerHeight + footerHeight);
        current_modal.find('.modal-body').css({
            'max-height': maxHeight,
            'overflow-y': 'auto'
        })
    }
}])