(function($) {
    'use strict';
    $(function() {
        var browser_window = $(window),
            header = $('.header');

        function add_header_glow() {
            var scrolled = browser_window.scrollTop(),
                current_width = browser_window.width();
            if (scrolled > 40) {
                header.addClass('header_glow')
            } else {
                header.removeClass('header_glow')
            }
        }
        browser_window.scroll(function() {
            add_header_glow()
        });
        browser_window.on('resize', function() {
            add_header_glow()
        });
        $('.user_options-toggle>a').on('touchstart click', function(e) {
            e.preventDefault();
            $('.page').toggleClass('showing-menu');
            $(this).parent().toggleClass('user_options-toggle-show')
        });
        $('.navigation-link>a').on('touchstart click', function(e) {
            var current_width = browser_window.width(),
                link_parent = $(this).parent();
            if (current_width < 1200) {
                e.preventDefault();
                e.stopPropagation();
                if (!link_parent.hasClass('navigation-current')) {
                    $('#navigation-menu>li').removeClass('navigation-current');
                    link_parent.addClass('navigation-current')
                }
            }
        })
    })
})(jQuery)