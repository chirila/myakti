<?php
define('INSTALLPATH',__DIR__.'/');
//define('ENVIRONMENT','admin');
define('LAYOUT',__DIR__.'/layout/');
define('BASEPATH', INSTALLPATH.'core/');
define('APPSPATH', INSTALLPATH.'apps/');

define('AT_CACHE', 'cache/');

/************************************************************************
* @Author: Tinu Coman
***********************************************************************/
ini_set('display_errors', 1);
ini_set('error_reporting', 1);
error_reporting(E_ALL ^ E_NOTICE);
ini_set('error_log','cron_project.log');
set_time_limit(0);
include_once(__DIR__."/core/startup.php");
global $config,$database_config;
$db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
); 
$db_users= new sqldb($db_config);
$response=file_get_contents("php://input");
$db_users->query("INSERT INTO nylas_log SET response='".addslashes(serialize($response))."', date='".time()."' ");
	
	header('X-PHP-Response-Code: 200', true, 200);
	exit();
?>