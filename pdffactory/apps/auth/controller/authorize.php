<?php
	global $config,$database_config;

	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db = new sqldb($database_users);

	// Autoloading
	require_once('libraries/php_oauth2/src/OAuth2/Autoloader.php');
	OAuth2_Autoloader::register();

	// create your storage
	$dsn="mysql:dbname=".$config['db_users'].';host='.$config['db_host'];
	$storage = new OAuth2_Storage_Pdo(array('dsn' => $dsn, 'username' => $config['db_user'], 'password' => $config['db_pass']));

	// create your server
	$server = new OAuth2_Server($storage);

	// Add the "Authorization Code" grant type (this is required for authorization flows)
	$server->addGrantType(new OAuth2_GrantType_AuthorizationCode($storage));

	$v = new validation($in);
	$v->field('responseType','Response Type','required');
	$v->field('clientId','Client Id','required');
	$v->field('redirectUri','Redirect Uri','required');
	$v->field('username','Username','required','Please fill the username');
	$v->field('password','Password','required','Please fill the password');

	if(!$v->run()){
		http_response_code(400);
		json_out();
	}

	$user_data=$db->query("SELECT password, user_id, database_name
                FROM users
    			WHERE username = '".$in['username']."'");
	if($user_data->move_next()){
		$check_if_db_exists = $db->query("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '".$user_data->f('database_name')."'")->getAll();
		if(empty($check_if_db_exists)){
    		msg::error ( "Wrong username or password. Try again.",'error');
    		http_response_code(400);
    		json_out();
    	}else if($user_data->f('database_name')=='445e3208_dd94_fdd4_c53c135a2422'){
    		msg::error ( "Please go to cozie.akti.com",'error');
    		http_response_code(400);
    		json_out();
    	}
    	if(md5($in['password']) == $user_data->f('password')){
    		$in['user_id']=$user_data->f('user_id');
    	}else{
    		msg::error("Wrong username or password. Try again.",'error');
    		http_response_code(400);
			json_out();
    	}
	}else{
		msg::error("Wrong username or password. Try again.",'error');
		http_response_code(400);
		json_out();
	}

	$in['response_type']=$in['responseType'];
	$in['client_id']=$in['clientId'];
	$in['redirect_uri']=$in['redirectUri'];
	$_GET=$in;
	$_POST=array();

	$request = OAuth2_Request::createFromGlobals();
	$response = new OAuth2_Response();

	// validate the authorize request
	$validateResponse=$server->validateAuthorizeRequest($request, $response);
	if (!$validateResponse) {
		$error_rsp=json_decode($response->getResponseBody());
		msg::error($error_rsp->error_description,'error');
		http_response_code(400);
		json_out();
	}

	// return uri with authorization code if the user has authorized your client
	$server->handleAuthorizeRequest($request, $response, true, $in['user_id']);
	$code_full = substr($response->getHttpHeader('Location'), strpos($response->getHttpHeader('Location'), 'code=')+0);
	  	/*$code_array=explode("&",$code_full);
	  	$final_data=array();
	  	foreach($code_array as $key=>$value){
			$tmp_equal=strpos($value,"=");
			if($tmp_equal!==false){
				$final_data[substr($value,0,$tmp_equal)]=substr($value,$tmp_equal+1,strlen($value)-$tmp_equal-1);
			}				
	  	}*/
	$final_data=array('redirectUri'=>$in['redirect_uri'].'?'.$code_full);
	json_out($final_data);

?>