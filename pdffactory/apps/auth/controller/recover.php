<?php

switch (true) {
	case  !empty($_SESSION['l']):
		$info_lang_code = $_SESSION['l'];
		break;
	case  !empty($_COOKIE["l"]):
		$info_lang_code = $_COOKIE["l"];
		break;
	default:
		$info_lang_code = LANGUAGE_TR;
		break;
}

$allowd = array('nl','en','fr');
if(!in_array($info_lang_code, $allowd)){
	$info_lang_code = 'en';
}

switch ($info_lang_code) {
	case 'en':

		$banner_link = 'https://akti.com/lp/service-premium/';
		$banner_img = 'en';
		break;
	case 'fr':
		$banner_link = 'https://akti.com/fr_BE/lp/service-premium/';
		$banner_img = 'fr';
		break;

	case 'nl':
		$banner_link = 'https://akti.com/nl_BE/lp/service-premium/';
		$banner_img = 'nl';
		break;
	default:
		$banner_link = 'https://akti.com/lp/service-premium/';
		$banner_img = 'en';
		break;
}

$response=array(
	'username'				=> $in['username'],
	'password'				=> '',
	'password2'				=> '',
	'banner'				=> $banner_link,
	'lang'					=> $banner_img,
	'crypt'					=> $in['crypt']
	);

json_out($response);
