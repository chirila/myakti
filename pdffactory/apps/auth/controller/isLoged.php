<?php
global $database_config;
$response = array('isLoggedIn'=>false);
if($_SESSION['u_id']){
	$response['isLoggedIn'] = true;
	$response['name'] = get_user_name($_SESSION['u_id']);
	$response['group'] = $_SESSION['group'];
	$response['adminSetting'] = array();
	if(isset($_SESSION['admin_sett'])){
		foreach ($_SESSION['admin_sett'] as $key => $value) {
			array_push($response['adminSetting'],$value);
		}
	}
	$db_config = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);

	$db_users = new sqldb($db_config);
	//$db=new sqldb();

	$is_new_subscription = $db->field("SELECT value FROM settings WHERE constant_name='NEW_SUBSCRIPTION' ");

	$response['must_pay']=false;
	$user_info = $db_users->query("SELECT * FROM user_info WHERE user_id='".$_SESSION['u_id']."' ");
	if(time()>$user_info->f('end_date')){
		$response['must_pay']=true;
		if(is_null($is_new_subscription)){
			$response['pag']='subscription';
		}else{
			$response['pag']='subscription_new';
		}
	}
}
json_out($response);