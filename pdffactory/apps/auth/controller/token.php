<?php
	global $config,$database_config;

	$database_users = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
	);
	$db = new sqldb($database_users);

	// Autoloading
	require_once('libraries/php_oauth2/src/OAuth2/Autoloader.php');
	OAuth2_Autoloader::register();

	// create your storage
	$dsn="mysql:dbname=".$config['db_users'].';host='.$config['db_host'];
	$storage = new OAuth2_Storage_Pdo(array('dsn' => $dsn, 'username' => $config['db_user'], 'password' => $config['db_pass']));

	// Pass a storage object or array of storage objects to the OAuth2 server class
	$server = new OAuth2_Server($storage);

	$in['grant_type']=$in['grantType'];
	unset($in['grantType']);
	$in['client_id']=$in['clientId'];
	$in['client_secret']=$in['clientSecret'];

	if($in['grant_type']=='password'){
		$v = new validation($in);
		$v->field('username','Username','required','Please fill the username');
		$v->field('password','Password','required','Please fill the password');	

		if(!$v->run()){
			http_response_code(400);
			json_out();
		}	

		$user_data=$db->query("SELECT password, user_id, database_name
                FROM users
    			WHERE username = '".$in['username']."'");
		if($user_data->move_next()){
			$check_if_db_exists = $db->query("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '".$user_data->f('database_name')."'")->getAll();
    		if(empty($check_if_db_exists)){
	    		msg::error ( "Wrong username or password. Try again.",'error');
	    		http_response_code(400);
	    		json_out();
	    	}else if($user_data->f('database_name')=='445e3208_dd94_fdd4_c53c135a2422'){
	    		msg::error ( "Please go to cozie.akti.com",'error');
	    		http_response_code(400);
	    		json_out();
	    	}
	    	if(md5($in['password']) == $user_data->f('password')){
	    		$in['user_id']=$user_data->f('user_id');
	    		$user_storage = new OAuth2_Storage_Memory(array('user_credentials' => array($in['username']=>$in['password'])));
	    			$server->addGrantType(new OAuth2_GrantType_UserCredentials($user_storage));
	    	}else{
	    		msg::error("Wrong username or password. Try again.",'error');
	    		http_response_code(400);
				json_out();
	    	}
		}else{
			msg::error("Wrong username or password. Try again.",'error');
			http_response_code(400);
			json_out();
		}		
	}else if($in['grant_type']=='authorization_code'){
		$v = new validation($in);	
		$v->field('code','Code','required');
		$v->field('redirectUri','Redirect Uri','required');
		if(!$v->run()){
			http_response_code(400);
			json_out();
		}
		$in['redirect_uri']=$in['redirectUri'];
		unset($in['redirectUri']);
		$user_data=$db->query("SELECT users.user_id
                FROM oauth_authorization_codes
                INNER JOIN users ON oauth_authorization_codes.user_id=users.user_id
    			WHERE oauth_authorization_codes.authorization_code = '".$in['code']."'");
		if($user_data->move_next()){
			$in['user_id']=$user_data->f('user_id');
			$server->addGrantType(new OAuth2_GrantType_AuthorizationCode($storage));
		}else{
			msg::error("Wrong authorization code",'error');
			http_response_code(400);
			json_out();
		}
	}else if($in['grant_type']=='refresh_token'){
		$v = new validation($in);
		$v->field('refreshToken','Refresh Token','required');
		if(!$v->run()){
			http_response_code(400);
			json_out();
		}

		$in['refresh_token']=$in['refreshToken'];
		$server->addGrantType(new OAuth2_GrantType_RefreshToken($storage, array('always_issue_new_refresh_token' => true)));

		$user_data=$db->query("SELECT user_id
                FROM oauth_refresh_tokens
    			WHERE refresh_token = '".$in['refresh_token']."'");
		if($user_data->move_next()){
			$in['user_id']=$user_data->f('user_id');
		}else{
			msg::error ( "Wrong Refresh Token",'error');
			http_response_code(400);
	    	json_out();
		}
	}else{
		msg::error('Invalid grant type','error');
		http_response_code(400);
		json_out();
	}	

	$_GET=array();
	$_POST=$in;

	// Handle a request for an OAuth2.0 Access Token and send the response to the client
	$server_response=$server->handleTokenRequest(OAuth2_Request::createFromGlobals(), new OAuth2_Response());
	$server_status=$server_response->getStatusCode();
	if(!$server_status || $server_status>300){
		$error_rsp=json_decode($server_response->getResponseBody());
		msg::error($error_rsp->error_description,'error');
		http_response_code($server_status);
		json_out();
	}else{
		$data_rsp=json_decode($server_response->getResponseBody());
		$final_data=array();
		foreach($data_rsp as $key=>$value){
			$dash_pos=strpos($key,"_");
			if($dash_pos!==false){
				$final_data[substr($key,0,$dash_pos).ucfirst(substr($key,$dash_pos+1,strlen($key)-$dash_pos))]=$value;
			}else{
				$final_data[$key]=$value;
			}
		}
		if($in['user_id']){
			if($final_data['accessToken']){
				$db->query("UPDATE oauth_access_tokens SET user_id='".$in['user_id']."',expires='".date('Y-m-d H:i:s', time()+604800)."' WHERE access_token='".$final_data['accessToken']."' ");
				if($final_data['refreshToken']){
					$db->query("UPDATE oauth_access_tokens SET refresh_token='".$final_data['refreshToken']."' WHERE access_token='".$final_data['accessToken']."' ");
				}
				$db->query("INSERT INTO user_activity SET
                                user_id='".$in['user_id']."',
                                HTTP_USER_AGENT='".$_SERVER['HTTP_USER_AGENT']."',
                                HTTP_REFERER='".$_SERVER['HTTP_REFERER']."',
                                REMOTE_ADDR='".$_SERVER['REMOTE_ADDR']."',
                                REMOTE_PORT='".$_SERVER['REMOTE_PORT']."',
                                REQUEST_METHOD='".$_SERVER['REQUEST_METHOD']."',
                                REQUEST_TIME_FLOAT='".$_SERVER['REQUEST_TIME_FLOAT']."',
                                QUERY_STRING='".addslashes($_SERVER['QUERY_STRING'])."',
                                REQUEST_URI='".addslashes($_SERVER['REQUEST_URI'])."',
                                access_token='".$final_data['accessToken']."' ");
			}
			if($final_data['refreshToken']){
				$db->query("UPDATE oauth_refresh_tokens SET user_id='".$in['user_id']."',expires='".date('Y-m-d H:i:s', time()+1209600)."' WHERE refresh_token='".$final_data['refreshToken']."' ");
			}		
		}		
		json_out($final_data);
	}
	

?>