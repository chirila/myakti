<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
global $apps;
global $p_access;

perm::controller('all', 'admin', in_array($apps['invoice'], $p_access) );
perm::controller('all', 'user',in_array($apps['invoice'], $p_access));
if($_SESSION['admin_sett'] && !in_array(ark::$app, $_SESSION['admin_sett'])){
	perm::controller(array('invoice_note','xdefault_message','xdefault_format','xlanguage','xlabel'), 'user', false);
}

/*if(defined('WIZZARD_COMPLETE') && WIZZARD_COMPLETE=='0'){
	// echo "string";
	perm::controller('all', 'admin',false);
	perm::controller('all', 'user',false);
}*/

// var_dump(perm::$ctrl);