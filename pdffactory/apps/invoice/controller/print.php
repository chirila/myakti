<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

$db=new sqldb();
$v=new validation($in);
$v->field("objectId","Object Id","required:exist[tblinvoice.id]");
if(!$v->run()){
	http_response_code(400);
	json_out();
}
global $config;
if(!$in['print'] && !$in['inline_show'] && !$in['save_as']){
	ark::loadLibraries(array('aws'));
	$aws = new awsWrap(DATABASE_NAME);

	$link =  $aws->getLink($config['awsBucket'].DATABASE_NAME.'/invoice/invoice_'.$in['objectId'].'.pdf');
	if(file_get_contents($link)){
		json_out($link);
	}
}

$logo_src="";
if(!empty($_FILES)){
	$fileParts = pathinfo($_FILES['logo']['name']);
	$logo_data=file_get_contents($_FILES['logo']['tmp_name']);
	if($logo_data){
		$logo_src = 'data: '.$fileParts['extension'].';base64,'.$logo_data;
	}
}

$inv = $db->query("SELECT * FROM tblinvoice WHERE id = '".$in['objectId']."' ");
$in['use_custom'] = 0;
if($inv->f('pdf_layout') && $inv->f('use_custom_template')==0){
  	$in['logo'] = $inv->f('pdf_logo');
  	$in['type']=$inv->f('pdf_layout');
  	$in['logo']=$inv->f('pdf_logo');
  	$in['template_type'] = $inv->f('pdf_layout');
}elseif($inv->f('pdf_layout') && $inv->f('use_custom_template')==1){
  	$in['custom_type']=$inv->f('pdf_layout');
  	unset($in['type']);
  	$in['logo']=$inv->f('pdf_logo');
  	$in['template_type'] = $inv->f('pdf_layout');
  	$in['use_custom'] = 1;
}else{
	$in['type']= ACCOUNT_INVOICE_BODY_PDF_FORMAT;
	$in['template_type']=ACCOUNT_INVOICE_BODY_PDF_FORMAT;
}
#if we are using a customer pdf template
if(defined('USE_CUSTOME_INVOICE_PDF') && USE_CUSTOME_INVOICE_PDF == 1 && $inv->f('pdf_layout') == 0){
  $in['custom_type']=ACCOUNT_INVOICE_PDF_FORMAT;
  unset($in['type']);
}
$in['id'] = $in['objectId'];
$in['lid'] = $inv->f('email_language');
$in['type_pdf'] =  $inv->f('type');

require_once(INSTALLPATH.'libraries/wkhtml_pdf/Pdf.php');
$pdfOptions=array(
	'encoding' => 'UTF-8',
	'title'		=> 'Invoice',
	'orientation'=>'Portrait',
	'minimum-font-size'	=> '16',
);
$pdf = new Pdf($pdfOptions);

if(file_exists(__DIR__.'/../view/'.DATABASE_NAME)){
	$html=file_get_contents(__DIR__.'/../view/'.DATABASE_NAME.'/print.html');
}else{
	$html = include('invoice_print_body.php');
	//var_dump($content2);exit;
	//$html=file_get_contents(__DIR__.'/../view/default/print.html');
}

if($in['print']){
	echo $html;exit;
}
//var_dump($content2);exit;
$pdf->addPage($html,array(
	'header-html'	=> '<!DOCTYPE html><html><body></body></html>',
	'header-spacing'	=> 16,
	'footer-html'	=> '<!DOCTYPE html><html><head><script>
  function subst() {
      var vars = {};
      var query_strings_from_url = document.location.search.substring(1).split(\'&\');
      for (var query_string in query_strings_from_url) {
          if (query_strings_from_url.hasOwnProperty(query_string)) {
              var temp_var = query_strings_from_url[query_string].split(\'=\', 2);
              vars[temp_var[0]] = decodeURI(temp_var[1]);
          }
      }
      var css_selector_classes = [\'page\', \'topage\'];
      for (var css_class in css_selector_classes) {
          if (css_selector_classes.hasOwnProperty(css_class)) {
              var element = document.getElementsByClassName(css_selector_classes[css_class]);
              for (var j = 0; j < element.length; ++j) {
                  element[j].textContent = vars[css_selector_classes[css_class]];
              }
          }
      }
  }
  </script></head><body onload="subst()">'.$content2.'<div>
  	<p style="text-align:right">Page <span class="page"></span> of <span class="topage"></span></p>
  </div></body></html>',
	'footer-spacing'	=> 0,
));

if($in['save_as'] == 'F'){
	$pdfPath = INSTALLPATH.'/upload/'.DATABASE_NAME.'/invoice_'.unique_id(32).'.pdf';
	if (!$pdf->saveAs($pdfPath)) {
	    $error = $pdf->getError();
	    // ... handle error here
	}else{
		ark::loadLibraries(array('aws'));
		$a = new awsWrap(DATABASE_NAME);
		
		$pdfFile = 'invoice/invoice_'.$in['objectId'].".pdf";
		$e = $a->uploadFile($pdfPath,$pdfFile);
		unlink($pdfPath);
	}
}else if($in['inline_show']){
	if (!$pdf->send()) {
	    $error = $pdf->getError();
	    //var_dump($error);
	    //var_dump('eroare generare pdf');
	    // ... handle error here
	}
}

exit;

?>