<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

global $apps;
global $p_access;

perm::model('all','all', 'admin',in_array($apps['maintenance'], $p_access));
perm::model('all','all', 'user',in_array($apps['maintenance'], $p_access));
if(isset($_SESSION['team_int']) && $_SESSION['team_int']){
    perm::model('all','all','user',true);
}
/*if(defined('WIZZARD_COMPLETE') && WIZZARD_COMPLETE=='0'){
    // echo "string";
    perm::model('all','all', 'admin',false);
    perm::model('all','all', 'user',false);
}*/