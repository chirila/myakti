<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

// perm::controller('all', 'user');

global $apps;
global $p_access;


perm::controller('all', 'admin',in_array($apps['maintenance'], $p_access));
perm::controller('all', 'user',in_array($apps['maintenance'], $p_access));
if(isset($_SESSION['admin_sett']) && !in_array(ark::$app, $_SESSION['admin_sett'])){
    perm::controller(array('project_settings','default_expenses','default_expenses_list','project_naming_conv','timesheet_messages','default_expense'), 'user', false);
}
if(isset($_SESSION['team_int']) && $_SESSION['team_int']){
    perm::controller(array('planning','template','services','service','article_deliveries','article_delivery','select_serial_no','print','print_body','pdf_settings'), 'user', true);
}
/*if(defined('WIZZARD_COMPLETE') && WIZZARD_COMPLETE=='0'){
    // echo "string";
    perm::controller('all', 'admin',false);
    perm::controller('all', 'user',false);
}*/
