<?php
function json_out($out = array(), $strict = false){
    // unset($_SESSION['articles_id']);
    global $in,$config;

    // $json = new Services_JSON();
    if(is_array($out)){
        unset($out['do']);
    }

    $status = parse_error_code(http_response_code());
    //$out = array_merge($out,$status);
    $tmp=array();
    $tmp['data']=$out;
    $tmp['message']=array();
    if (class_exists('msg')) {      
        $error = array_values(msg::get_errors());
        if($error){
            $tmp['error'] = true;
            $tmp['message']['error']= msg::get_errors();
        }/*else{
            $tmp['error'] = false;
        }*/   
        $notice=array_values(msg::get_notices());
        if($notice){
            $tmp['message']['notice']=msg::get_notices(); 
            $tmp['notice']=true;
        }/*else{
            $tmp['notice']=false;
        }*/
        $success = array_values(msg::get_success());
        //$success = implode("\n", $success);
        if($success && !$error && !$notice){
            $tmp['message']['success'] = msg::get_success();
            $tmp['success']=true;
        }/*else{
            $tmp['success']=false;
        }*/
    }
    $tmp['status_code']=$status['status_code'];
    if(array_key_exists('rows_per_page',$out)){
        $tmp['rows_per_page']=$out['rows_per_page'];
        unset($tmp['data']['rows_per_page']);
    }
    if(array_key_exists('offset',$out)){
        $tmp['offset']=$out['offset'];
        unset($tmp['data']['offset']);
    }
    if(array_key_exists('rows',$out)){
        $tmp['rows']=$out['rows'];
        unset($tmp['data']['rows']);
    }

    if($_SESSION['l']){
        //$out['lang'] = $_SESSION['l'];
        $tmp['lang'] = $_SESSION['l'];
    }
    header("Content-type: application/json; charset=utf-8");
    echo json_encode($tmp); //fix UTF-8 issues
    exit();
    
}

// for php version < 5.5
if( !function_exists( 'array_column' ) ){
    function array_column( array $input, $column_key, $index_key = null ) {

        $result = array();
        foreach( $input as $k => $v )
            $result[ $index_key ? $v[ $index_key ] : $k ] = $v[ $column_key ];

        return $result;
    }
}


if (!function_exists('getallheaders')) 
{ 
    function getallheaders() 
    { 
           $headers = ''; 
       foreach ($_SERVER as $name => $value) 
       { 
           if (substr($name, 0, 5) == 'HTTP_') 
           { 
               $headers[str_replace(' ', '-', strtolower(str_replace('_', ' ', substr($name, 5))))] = $value; 
           } 
       } 
       return $headers; 
    } 
}
if (!function_exists('http_response_code')) {
    function http_response_code($code = NULL) {

        if ($code !== NULL) {

            switch ($code) {
                case 100: $text = 'Continue'; break;
                case 101: $text = 'Switching Protocols'; break;
                case 200: $text = 'OK'; break;
                case 201: $text = 'Created'; break;
                case 202: $text = 'Accepted'; break;
                case 203: $text = 'Non-Authoritative Information'; break;
                case 204: $text = 'No Content'; break;
                case 205: $text = 'Reset Content'; break;
                case 206: $text = 'Partial Content'; break;
                case 300: $text = 'Multiple Choices'; break;
                case 301: $text = 'Moved Permanently'; break;
                case 302: $text = 'Moved Temporarily'; break;
                case 303: $text = 'See Other'; break;
                case 304: $text = 'Not Modified'; break;
                case 305: $text = 'Use Proxy'; break;
                case 400: $text = 'Bad Request'; break;
                case 401: $text = 'Unauthorized'; break;
                case 402: $text = 'Payment Required'; break;
                case 403: $text = 'Forbidden'; break;
                case 404: $text = 'Not Found'; break;
                case 405: $text = 'Method Not Allowed'; break;
                case 406: $text = 'Not Acceptable'; break;
                case 407: $text = 'Proxy Authentication Required'; break;
                case 408: $text = 'Request Time-out'; break;
                case 409: $text = 'Conflict'; break;
                case 410: $text = 'Gone'; break;
                case 411: $text = 'Length Required'; break;
                case 412: $text = 'Precondition Failed'; break;
                case 413: $text = 'Request Entity Too Large'; break;
                case 414: $text = 'Request-URI Too Large'; break;
                case 415: $text = 'Unsupported Media Type'; break;
                case 500: $text = 'Internal Server Error'; break;
                case 501: $text = 'Not Implemented'; break;
                case 502: $text = 'Bad Gateway'; break;
                case 503: $text = 'Service Unavailable'; break;
                case 504: $text = 'Gateway Time-out'; break;
                case 505: $text = 'HTTP Version not supported'; break;
                default:
                    exit('Unknown http status code "' . htmlentities($code) . '"');
                break;
            }

            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

            header($protocol . ' ' . $code . ' ' . $text);

            $GLOBALS['http_response_code'] = $code;

        } else {

            $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);

        }

        return $code;

    }
}

function parse_error_code($code){

    switch ($code) {
        case 100: $text = 'Continue'; break;
        case 101: $text = 'Switching Protocols'; break;
        case 200: $text = 'OK'; break;
        case 201: $text = 'Created'; break;
        case 202: $text = 'Accepted'; break;
        case 203: $text = 'Non-Authoritative Information'; break;
        case 204: $text = 'No Content'; break;
        case 205: $text = 'Reset Content'; break;
        case 206: $text = 'Partial Content'; break;
        case 300: $text = 'Multiple Choices'; break;
        case 301: $text = 'Moved Permanently'; break;
        case 302: $text = 'Moved Temporarily'; break;
        case 303: $text = 'See Other'; break;
        case 304: $text = 'Not Modified'; break;
        case 305: $text = 'Use Proxy'; break;
        case 400: $text = 'Bad Request'; break;
        case 401: $text = 'Unauthorized'; break;
        case 402: $text = 'Payment Required'; break;
        case 403: $text = 'Forbidden'; break;
        case 404: $text = 'Not Found'; break;
        case 405: $text = 'Method Not Allowed'; break;
        case 406: $text = 'Not Acceptable'; break;
        case 407: $text = 'Proxy Authentication Required'; break;
        case 408: $text = 'Request Time-out'; break;
        case 409: $text = 'Conflict'; break;
        case 410: $text = 'Gone'; break;
        case 411: $text = 'Length Required'; break;
        case 412: $text = 'Precondition Failed'; break;
        case 413: $text = 'Request Entity Too Large'; break;
        case 414: $text = 'Request-URI Too Large'; break;
        case 415: $text = 'Unsupported Media Type'; break;
        case 500: $text = 'Internal Server Error'; break;
        case 501: $text = 'Not Implemented'; break;
        case 502: $text = 'Bad Gateway'; break;
        case 503: $text = 'Service Unavailable'; break;
        case 504: $text = 'Gateway Time-out'; break;
        case 505: $text = 'HTTP Version not supported'; break;
        default:
            $text = 'Unknown error';
        break;
    }
    $out = array('status_code'=>$code);
    if($code >= 400){
        // $out['error'] = $text;
    }
    return $out;
}

function build_article_tax_type_dd($selected){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT name, id FROM pim_article_tax_type ORDER BY sort_order");
    while($db->move_next()){
        $array[$db->f('id')] = $db->f('name');
    }
    return build_simple_dropdown($array,$selected);
}
function backupcrm($selected){
    $db = new sqldb();
    $array = array();
    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    $tz=array('1'=> $file1, '2'=> $file2, '3'=> $file3);
    return build_simple_dropdown($tz,$selected);
}
function backupcrm2($selected){
    $db = new sqldb();
    $array = array();
    $backup1 = $db->field("SELECT backup_time FROM backup_data WHERE  backup_id='1'");
    $backup2 = $db->field("SELECT backup_time FROM backup_data WHERE  backup_id='2'");
    $backup3 = $db->field("SELECT backup_time FROM backup_data WHERE  backup_id='3'");

    $date1  = date('d/m/Y H:i:s', $backup1);
    $date2  = date('d/m/Y H:i:s', $backup2);
    $date3  = date('d/m/Y H:i:s', $backup3);

    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    $tz=array('1'=> $file1.'    '.$date1, '2'=> $file2.'    '.$date2, '3'=> $file3.'    '.$date3);
    return build_simple_dropdown($tz,$selected);
}
function backupcrm3($selected){
    $db = new sqldb();
    $array = array();
    $backup1 = $db->field("SELECT backup_time FROM backup_data WHERE  backup_id='1'");
    $backup2 = $db->field("SELECT backup_time FROM backup_data WHERE  backup_id='2'");
    $backup3 = $db->field("SELECT backup_time FROM backup_data WHERE  backup_id='3'");

    $date1  = date('d/m/Y H:i:s', $backup1);
    $date2  = date('d/m/Y H:i:s', $backup2);
    $date3  = date('d/m/Y H:i:s', $backup3);

    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    $tz=array('1'=> $file1.'    '.$date1, '2'=> $file2.'    '.$date2, '3'=> $file3.'    '.$date3);
    return build_simple_dropdown($tz,$selected);
}
function backuparticle2($selected){
    $db = new sqldb();
    $array = array();
    $backup1 = $db->field("SELECT backup_time FROM backup_data_article WHERE  backup_id='1'");
    $backup2 = $db->field("SELECT backup_time FROM backup_data_article WHERE  backup_id='2'");
    $backup3 = $db->field("SELECT backup_time FROM backup_data_article WHERE  backup_id='3'");

    $date1  = date('d/m/Y H:i:s', $backup1);
    $date2  = date('d/m/Y H:i:s', $backup2);
    $date3  = date('d/m/Y H:i:s', $backup3);

    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    $tz=array('1'=> $file1.'    '.$date1, '2'=> $file2.'    '.$date2, '3'=> $file3.'    '.$date3);
    return build_simple_dropdown($tz,$selected);
}
function backuparticle3($selected){
    $db = new sqldb();
    $array = array();
    $backup1 = $db->field("SELECT backup_time FROM backup_data_article WHERE  backup_id='1'");
    $backup2 = $db->field("SELECT backup_time FROM backup_data_article WHERE  backup_id='2'");
    $backup3 = $db->field("SELECT backup_time FROM backup_data_article WHERE  backup_id='3'");

    $date1  = date('d/m/Y H:i:s', $backup1);
    $date2  = date('d/m/Y H:i:s', $backup2);
    $date3  = date('d/m/Y H:i:s', $backup3);

    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    $tz=array('1'=> $file1.'    '.$date1, '2'=> $file2.'    '.$date2, '3'=> $file3.'    '.$date3);
    return build_simple_dropdown($tz,$selected);
}
function backupinvoice2($selected){
    $db = new sqldb();
    $array = array();
    $backup1 = $db->field("SELECT backup_time FROM backup_data_invoice WHERE  backup_id='1'");
    $backup2 = $db->field("SELECT backup_time FROM backup_data_invoice WHERE  backup_id='2'");
    $backup3 = $db->field("SELECT backup_time FROM backup_data_invoice WHERE  backup_id='3'");

    $date1  = date('d/m/Y H:i:s', $backup1);
    $date2  = date('d/m/Y H:i:s', $backup2);
    $date3  = date('d/m/Y H:i:s', $backup3);

    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    $tz=array('1'=> $file1.'    '.$date1, '2'=> $file2.'    '.$date2, '3'=> $file3.'    '.$date3);
    return build_simple_dropdown($tz,$selected);
}
function backupinvoice3($selected){
    $db = new sqldb();
    $array = array();
    $backup1 = $db->field("SELECT backup_time FROM backup_data_invoice WHERE  backup_id='1'");
    $backup2 = $db->field("SELECT backup_time FROM backup_data_invoice WHERE  backup_id='2'");
    $backup3 = $db->field("SELECT backup_time FROM backup_data_invoice WHERE  backup_id='3'");

    $date1  = date('d/m/Y H:i:s', $backup1);
    $date2  = date('d/m/Y H:i:s', $backup2);
    $date3  = date('d/m/Y H:i:s', $backup3);

    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    $tz=array('1'=> $file1.'    '.$date1, '2'=> $file2.'    '.$date2, '3'=> $file3.'    '.$date3);
    return build_simple_dropdown($tz,$selected);
}
function backupcatalog($selected){
    $file1 ='backup1';
    $file2 ='backup2';
    $file3 ='backup3';
    $tz=array('1'=> $file1, '2'=> $file2, '3'=> $file3);
    return build_simple_dropdown($tz,$selected);
}

function build_time_zone_dropdown($selected=0){
    /*$tz=array(
    '-12' => '(GMT-12:00) International Date Line West',
    '-11' => '(GMT-11:00) Midway Island, Samoa',
    '-10' => '(GMT-10:00) Hawaii',
    '-9' => '(GMT-09:00) Alaska',
    '-8' => '(GMT-08:00) Pacific Time (US & Canada); Tijuana',
    '-7' => '(GMT-07:00) Mountain Time (US & Canada), Chihuahua',
    '-6' => '(GMT-06:00) Saskatchewan, Guadalajara, Mexico City',
    '-5' => '(GMT-05:00) Eastern Time (US & Canada), Bogota, Lima, Quito',
    '-4' => '(GMT-04:00) Santiago,Caracas, La Paz,Atlantic Time (Canada)',
    '-3.3' => '(GMT-03:30) Newfoundland',
    '-3' => '(GMT-03:00) Greenland, Buenos Aires, Georgetown, Brasilia',
    '-2' => '(GMT-02:00) Mid-Atlantic',
    '-1' => '(GMT-01:00) Cape Verde Is., Azores',
    '0' => '(GMT) Casablanca, Monrovia, Greenwich Mean Time : Dublin',
    '1' => '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm',
    '2' => '(GMT+02:00) Athens, Beirut, Istanbul, Minsk ',
    '3' => '(GMT+03:00) Kuwait, Riyadh, Moscow, St. Petersburg',
    '3.3' => '(GMT+03:30) Tehran',
    '4' => '(GMT+04:00) Abu Dhabi, Muscat, Baku, Tbilisi, Yerevan',
    '4.3' => '(GMT+04:30) Kabul',
    '5' => '(GMT+05:00) Ekaterinburg, Islamabad, Karachi, Tashkent',
    '5.3' => '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi',
    '6' => '(GMT+06:00) Almaty, Novosibirsk, Astana, Dhaka, Sri Jayawardenepura',
    '7' => '(GMT+07:00) Bangkok, Hanoi, Jakarta, Krasnoyarsk',
    '8' => '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi',
    '9' => '(GMT+09:00) Osaka, Sapporo, Tokyo, Seoul, Yakutsk',
    '9.3' => '(GMT+09:30) Adelaide, Darwin',
    '10' => '(GMT+10:00) Sydney,Brisbane, Canberra, Melbourne',
    '11' => '(GMT+11:00) Magadan, Solomon Is., New Caledonia',
    '12' => '(GMT+12:00) Auckland, Wellington, Fiji, Kamchatka, Marshall Is.'
    );*/
    $tz = array(
        '-12' => '(GMT-12:00) International Date Line West',
'-11' => '(GMT-11:00) Midway Island, Samoa',
'-10' => '(GMT-10:00) Hawaii',
'-9' => '(GMT-09:00) Alaska',
'-8' => '(GMT-08:00) Pacific Time (US & Canada), Tijuana, Baja California',
'-7' => '(GMT-07:00) Arizona, Chihuahua, La Paz, Mazatlan, Mountain Time (US & Canada)',
'-6' => '(GMT-06:00) Central America, Central Time (US & Canada), Guadalajara, Mexico City, Monterrey, Saskatchewan',
'-5' => '(GMT-05:00) Bogota, Lima, Quito, Rio Branco, Eastern Time (US & Canada), Indiana (East)',
'-4' => '(GMT-04:00) Atlantic Time (Canada), Caracas, La Paz, Manaus, Santiago',
'-3.5' => '(GMT-03:30) Newfoundland',
'-3' => '(GMT-03:00) Brasilia, Buenos Aires, Georgetown, Greenland, Montevideo',
'-2' => '(GMT-02:00) Mid-Atlantic',
'-1' => '(GMT-01:00) Cape Verde Is., Azores',
'0' => '(GMT+00:00) Casablanca, Monrovia, Reykjavik, Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London',
'1' => '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna, Belgrade, Bratislava, Budapest, Ljubljana, Prague, Brussels, Copenhagen, Madrid, Paris, Sarajevo, Skopje, Warsaw, Zagreb, West Central Africa',
'2' => '(GMT+02:00) Amman, Athens, Bucharest, Istanbul, Beirut, Cairo, Harare, Pretoria, Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius, Jerusalem, Minsk, Windhoek',
'3' => '(GMT+03:00) Kuwait, Riyadh, Baghdad, Moscow, St. Petersburg, Volgograd, Nairobi, Tbilisi',
'3.5' => '(GMT+03:30) Tehran',
'4' => '(GMT+04:00) Abu Dhabi, Muscat, Baku, Yerevan',
'4.5' => '(GMT+04:30) Kabul',
'5' => '(GMT+05:00) Yekaterinburg, Islamabad, Karachi, Tashkent',
'5.5' => '(GMT+05:30) Sri Jayawardenapura, Chennai, Kolkata, Mumbai, New Delhi',
'5.75' => '(GMT+05:45) Kathmandu',
'6' => '(GMT+06:00) Almaty, Novosibirsk, Astana, Dhaka',
'6.5' => '(GMT+06:30) Yangon (Rangoon)',
'7' => '(GMT+07:00) Bangkok, Hanoi, Jakarta, Krasnoyarsk',
'8' => '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi, Kuala Lumpur, Singapore, Irkutsk, Ulaan Bataar, Perth, Taipei',
'9' => '(GMT+09:00) Osaka, Sapporo, Tokyo, Seoul, Yakutsk',
'9.5' => '(GMT+09:30) Adelaide, Darwin',
'10' => '(GMT+10:00) Brisbane, Canberra, Melbourne, Sydney, Hobart, Guam, Port Moresby, Vladivostok',
'11' => '(GMT+11:00) Magadan, Solomon Is., New Caledonia',
'12' => '(GMT+12:00) Auckland, Wellington, Fiji, Kamchatka, Marshall Is.',
'13' => '(GMT+13:00) Nuku\'alofa,'
);

    return build_simple_dropdown($tz,$selected);
}
function build_price_type($selected){
    return build_simple_dropdown(array('1' =>  gm('Use Base Price'),'2' =>  gm('Use Purchase Price')),$selected);
}
function build_price_category_type($selected){
    return build_simple_dropdown(array('1' => gm('Discount'),'2' => gm('Mark-up'),'3' => gm('Profit Margin')),$selected);
}
function build_price_value_type_list($selected){
    return build_simple_dropdown(array('1' => '%','2' => gm('fixed value')),$selected);
}
function get_price_category_type($selected){

    $opt=array(
    1   => 'Discount',
    2   => 'Mark-up',
    3   => 'Profit Margin'
    );
    $out_str=$opt[$selected];
    return $out_str;
}
function get_price_type($selected){

    $opt=array(
    1   => gm('Use Base Price'),
    2   => gm('Use Purchase Price')
    );
    $out_str=$opt[$selected];
    return $out_str;
}
function get_price_value_type($selected){
    $currency = get_currency(ACCOUNT_CURRENCY_TYPE);
    $opt=array(
    1   => '%',
    2   => $currency
    );
    $out_str=$opt[$selected];
    return $out_str;
}
function build_language_dd_user($selected){

    $db=new sqldb;
    $array = array();
    $db->query("SELECT * FROM lang ORDER BY name");
    while($db->move_next()){
        $array[$db->f('lang_id')] = $db->f('name');
    }
    return build_simple_dropdown($array,$selected);
}
function build_group_list($selected = ''){
    $db=new sqldb;
    $db->query("select * from groups WHERE group_id <> 2");
     $array = array();
    $no_timesheets = false;
    if (ACCOUNT_TYPE=='goods')
    {
        $no_timesheets = true;
    }
    if(!$selected){
        if($no_timesheets)
        {
            $selected='1';
        }else
        {
            $selected = '2';
        }
    }
    $out_str="";
    while($db->move_next()){
        if($no_timesheets==false || $db->f('group_id')!=2)
        {
            $array[$db->f('group_id')] = $db->f('name');
        }
    }
    return build_simple_dropdown($array,$selected);
}
function build_month_list($selected=0)
{

    $opt=array(
        array('id' => "1" , 'name' => gm("January")),
        array('id' => "2" , 'name' => gm("February-EU")),
        array('id' => "3" , 'name' => gm("March")),
        array('id' => "4" , 'name' => gm("April")),
        array('id' => "5" , 'name' => gm("May")),
        array('id' => "6" , 'name' => gm("June")),
        array('id' => "7" , 'name' => gm("July")),
        array('id' => "8" , 'name' => gm("August")),
        array('id' => "9" , 'name' => gm("September")),
        array('id' => "10" , 'name' => gm("October")),
        array('id' => "11" , 'name' => gm("November")),
        array('id' => "12" , 'name' => gm("December")),

    );
    /*while (list ($key, $val) = each ($opt))
    {
        $out_str.="<option value=\"".$key."\" ";//options values
        $out_str.=($key==$selected?" SELECTED ":"");//if selected
        $out_str.=">".$val."</option>";//options names
    }*/
    return $opt;
}

function display_number($number,$decimals=2){
    if(!$number){
        $number = 0;
    }
    $separator = ACCOUNT_NUMBER_FORMAT;
    $out = number_format(floatval($number),$decimals,substr($separator, -1),substr($separator, 0, 1));
    return $out;
}
function build_publish_lang_dd($selected,$setting = ''){
    $db = new sqldb();
    $array = array();
    $filter = " AND active='1' ";
    if($setting){
        $filter = "";
    }
    // $db->query("SELECT * FROM pim_lang WHERE language!='German' {$filter} ORDER BY sort_order");
    $db->query("SELECT * FROM pim_lang WHERE 1 = 1 {$filter} ORDER BY sort_order");
    while($db->move_next()){
        $array[$db->f('lang_id')] = gm($db->f('language'));
    }
    return build_simple_dropdown($array,$selected);
}
function updateUsersInfo()
{
    $used_const = array('ACCOUNT_DELIVERY_ADDRESS','ACCOUNT_DELIVERY_ZIP','ACCOUNT_DELIVERY_CITY','ACCOUNT_DELIVERY_STATE_ID','ACCOUNT_BILLING_ADDRESS','ACCOUNT_DELIVERY_COUNTRY_ID','ACCOUNT_BILLING_ZIP','ACCOUNT_BILLING_CITY','ACCOUNT_BILLING_STATE_ID','ACCOUNT_BILLING_COUNTRY_ID','ACCOUNT_VAT_NUMBER','ACCOUNT_VAT','ACCOUNT_CURRENCY_TYPE','ACCOUNT_COMPANY','ACCOUNT_BANK_NAME','ACCOUNT_BIC_CODE','ACCOUNT_IBAN','ACCOUNT_PHONE','ACCOUNT_FAX','ACCOUNT_TYPE');
    $db = new sqldb();
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $dbs = new sqldb($database_users);
    $const = $db->query("SELECT * FROM settings");
    while ($const->next()) {
        if(in_array($const->f('constant_name'), $used_const) ){
            $value = $const->f('value');
            if($const->f('type')  != 1){
                $value = $const->f('long_value');
            }
            $dbs->query("update users set ".$const->f('constant_name')."='".addslashes(utf8_decode($value))."' where database_name='".DATABASE_NAME."'");
        }
    }
}

function get_country_name($country_id){
    $name = '';
    if($country_id){
        $db=new sqldb();
        $db->query("SELECT country.* FROM country WHERE country_id='".$country_id."'");
        $db->move_next();
        $name=addslashes($db->f('name'));
    }
    return $name;
}
function get_country_by_code($code){
    $id = '';
    if($code){
        $db=new sqldb();
        $id = $db->field("SELECT country_id FROM country WHERE code='".$code."'");
        // $db->move_next();
        // $name=addslashes($db->f('name'));
    }
    return $id;
}

function get_country_code($country_id)
{
    $db=new sqldb;
    $db->query("SELECT code FROM country WHERE country_id='".$country_id."'");
    $db->move_next();
    $name=$db->f('code');
    return $name;
}

function number_as_hour($number){
    $negative = false;
    $min = round($number * 60);
    if($min < 0){
        $min = $min*(-1);
        $negative = true;
    }
    if( $min >= "60" ){
        $n = floor( $min / 60 );
        $min = $min - ( 60 * $n );
        if(strlen($min) == 1){
            $min = "0" . $min;
        }
        if($negative){
            $n = $n*(-1);
        }
        $out = $n . ":" . $min;
    }else{
        if(strlen($min) == 1){
            $min = "0" . $min;
        }
        $out = "0:". $min;
    }
    return $out;
}

function place_currency($out,$currency = '',$position='',$currency_font_face=''){

    if(!$currency){
        $currency = html_entity_decode(get_currency(ACCOUNT_CURRENCY_TYPE),ENT_COMPAT,"UTF-8");
    }
    if($currency_font_face){
        $currency = '<font face="'.$currency_font_face.'">'.$currency.'</font>';
    }
    if($position==1){
        $out = $out.' '.$currency;
    }else{
        if(ACCOUNT_CURRENCY_FORMAT == 0){
            $out = $out.' '.$currency;
        }
        else {
            $out = $currency.' '.$out;
        }
    }
    return $out;

}

function get_currency($selected)
{
    return currency::get_currency($selected);
}

function build_language_dd_new($selected='',$select_text=false) {
    // var_dump($select_text);
    $db_1 = new sqldb();
    $select = array();
    $country_codes = array();
    $db_1->query("SELECT * FROM pim_lang WHERE active='1' ORDER BY sort_order");
    while($db_1->next()) {
        $row = array('id'=>$db_1->f('lang_id'),'name'=>gm($db_1->f('language')));
        // $select[$db_1->f('lang_id')] = gm($db_1->f('language'));
            array_push($select, $row);
        switch ($db_1->f('code')) {
            case 'du':
                $country_codes[$db_1->f('lang_id')] = 'nl';
                break;
            case 'en':
                $country_codes[$db_1->f('lang_id')] = 'gb';
                break;
            default:
                $country_codes[$db_1->f('lang_id')] = $db_1->f('code');
                break;
        }
    }
    $db_1->query("SELECT * FROM pim_custom_lang WHERE active='1' ORDER BY sort_order");
    while($db_1->next()) {
        $row = array('id'=>$db_1->f('lang_id'),'name'=>gm($db_1->f('language')));
        // $select[$db_1->f('lang_id')] = gm($db_1->f('language'));
        array_push($select, $row);
        switch ($db_1->f('code')) {
            case 'cs':
                $country_codes[$db_1->f('lang_id')] = 'cz';
                break;
            case 'sv':
                $country_codes[$db_1->f('lang_id')] = 'se';
                break;
            case 'da':
                $country_codes[$db_1->f('lang_id')] = 'dk';
                break;
            default:
                $country_codes[$db_1->f('lang_id')] = $db_1->f('code');
                break;
        }
    }
    
    if($select_text){
        if($select_text=='country_code'){
            return strtoupper($country_codes[$selected]);
        }else{

            return $select[$selected]['name'];
        }
    }else{
        return $select;
    }
}
function get_custom_right($user_cred,$group){
    $db = new sqldb();
    // global $database_config;

    // $database_users = array(
    //     'hostname' => $database_config['mysql']['hostname'],
    //     'username' => $database_config['mysql']['username'],
    //     'password' => $database_config['mysql']['password'],
    //     'database' => $database_config['user_db'],
    // );
    // $dbs = new sqldb($database_users);
    // $user_cred = $dbs->field("SELECT credentials FROM users WHERE user_id='".$user."'");
    $user_cred = explode(';',$user_cred);
    $rezult = count($user_cred);
    $img = "";
    if($rezult == 1 && $user_cred[0] == 8){
        $img = 'time';
    }else{
            $img = 'power';
            if($group['group_id'] == 1){
                $img = 'admin';
            }
    }
    $out[2] = $img;
    // $group = $db->query("SELECT credentials, name FROM groups WHERE group_id='".$group."' ");
    // $group->next();
    $out[0] = $group['name'];
    $cred = explode(';',$group['credentials']);
    $result = array_diff($user_cred, $cred);
    $text = "";
    $i=0;
    if(!empty($result)){
        foreach ($user_cred as $key){
            if($i != 0 && !empty($key)){
                $text .=', ';
            }
            switch ($key){
                case '1':
                    $text .='CRM';
                    break;
                case '2':
                    $text .='Reports';
                    break;
                case '3':
                    $text .='Projects';
                    break;
                case '4':
                    $text .='Billing';
                    break;
                case '5':
                    $text .='Quotes';
                    break;
                case '6':
                    $text .='Orders';
                    break;
                case '7':
                    $text .='Manage';
                    break;
                case '8':
                    $text .='Timesheet only';
                    break;
                case '11':
                    $text .='Contracts';
                    break;
                case '12':
                    $text .='Articles';
                    break;
                case '13':
                    $text .='Interventions';
                    break;
            }
            $i++;
        }
    }

    $out[1] = $text;

    return $out;
}
function get_user_name($id){
    if(!$id){
        return '';
    }
    global $database_config;

    $db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
    );

    $db = new sqldb($db_config);

    $db->query("SELECT first_name,last_name FROM users WHERE user_id='".$id."'");
    $db->move_next();
    return htmlspecialchars_decode(stripslashes($db->f('first_name').' '.$db->f('last_name')));
}

function split_lines($str, $l=40){
    $str_len = strlen($str);
    $new_str ='';
    if($str_len>$l){
        $i=0;
        $strs = str_split($str,$l);
        while($i<count($strs)){
            $new_str .= $strs[$i]."<br/>";
            $i++;
        }
    }else{
        $new_str = $str;
    }
    return $new_str;
}
function get_group_name($id){
    $db = new sqldb();
    return $db->field("SELECT name FROM groups WHERE group_id='".$id."' ");
}
function pick_date_format($format = ACCOUNT_DATE_FORMAT){
    switch ($format){
        case 'm/d/Y':
            return 'MM/dd/yyyy';
            break;
        case 'd/m/Y':
            return 'dd/MM/yyyy';
            break;
        case 'Y-m-d':
            return 'yyyy-MM-dd';
            break;
        case 'd.m.Y':
            return 'dd.MM.yyyy';
            break;
        case 'Y.m.d':
            return 'yyyy.MM.dd';
            break;
        case 'Y/m/d':
            return 'yyyy/MM/dd';
            break;
    }
}

function return_value($number){
    $separator = ACCOUNT_NUMBER_FORMAT;
    $out=str_replace(substr($separator, 0, -1), '', $number);
    // var_dump(substr($separator, 0, -1));
    $out=str_replace(substr($separator, -1), '.', $out);
    if(!is_numeric($out)){
        $out =0;
    }
    return (float)$out;

}
function build_currency_list($selected=''){
    $currency = currency::get_currency($selected,'arrays');
    $array = array();
    foreach ($currency as $key => $value) {
        array_push($array, array('id'=>$key,'name'=>$value));
    }
    return $array;
}
function build_currency_name_list($selected){
    return currency::get_currency($selected,'code');
}
function get_sys_message($name,$lang='',$useHtml=true){
    $_db=new sqldb();
    $filter = ' ';
    if(!empty($lang) && is_numeric($lang)){
        if($lang>=1000) {
            $code = $_db->field("SELECT code FROM pim_custom_lang WHERE lang_id = '".$lang."' ");
            $filter = " AND lang_code = '".$code."' ";
        } else {
            $code = $_db->field("SELECT code FROM pim_lang WHERE lang_id='".$lang."' ");
            $filter = " AND lang_code='".$code."' ";
        }
    }else if(!empty($lang)){
        $filter = " AND lang_code='".$lang."' ";
    }

    $_db->query("SELECT * FROM sys_message WHERE name='".$name."' ".$filter);
    $_db->move_next();
    $msg['text']=$_db->f('text');
    $msg['from_email']=$_db->f('from_email');
    $msg['from_name']=$_db->f('from_name');
    $msg['subject']=$_db->f('subject');
    $msg['footnote']=$_db->f('footnote');
    $msg['use_html']=$_db->f('use_html');
    if($msg['use_html'] == 1 && $useHtml === true && $_db->f('html_content')){
        $msg['text']=$_db->f('html_content');
    }
    return $msg;
}

function generate_chars(){
    $num_chars = rand(20,25); //max length of random chars
    $i = 0;
    $my_keys = "123456789bcdfghjklmnpqrstvwxyz"; //keys to be chosen from
    $keys_length = strlen($my_keys);
    $url  = "";
    while($i<$num_chars)
    {
        $rand_num = mt_rand(1, $keys_length-1);
        $url .= $my_keys[$rand_num];
        $i++;
    }
    return $url;
}
function isUnique($chars){
    global $database_config;

    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb($database_users);
    global $link;
    $q = $db->query("SELECT * FROM `urls` WHERE `url_code`='".$chars."'");
    if($q->next()){
        return false;
    }else{
        return true;
    }
}
function get_commission_type_list($selected)
{
    return currency::get_currency($selected);
}

function get_signature(){
    global $database_config,$config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $dbs = new sqldb($database_users);
    if($_SESSION['u_id']){
        $dbs->query("SELECT * FROM users where user_id='".$_SESSION['u_id']."'");
        $dbs->move_next();
        if($dbs->gf('signature')){
            $info.= "<img src=".$config['site_url'].'../pim/'.$dbs->f('signature')." style='float:left;margin-right:15px;'>";
        }
        $info.="<br/>".$dbs->f('title') .", <strong>".$dbs->f('first_name').' '.$dbs->f('last_name')."</strong>";
        $info.="<br/>".$dbs->f('mobile');
        $info.="<br/>".$dbs->f('phone');
        $info.="<br/>".$dbs->f('fax');
        return $info;
    }
    return '';
}
function get_signature_new(){
    global $database_config,$config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $dbs = new sqldb($database_users);
    if($_SESSION['u_id']){
        $text=$dbs->field("SELECT text_signature FROM users where user_id='".$_SESSION['u_id']."'");
/*        $dbs->move_next();*/
/*        if($dbs->gf('signature')){
            $info.= "<img src=".$config['site_url'].'../pim/'.$dbs->f('signature')." style='float:left;margin-right:15px;'>";
        }
        $info.="<br/>".$dbs->f('title') .", <strong>".$dbs->f('first_name').' '.$dbs->f('last_name')."</strong>";
        $info.="<br/>".$dbs->f('mobile');
        $info.="<br/>".$dbs->f('phone');
        $info.="<br/>".$dbs->f('fax');*/
        $info=$text;
        return $info;
    }
    return '';
}
function build_identity_dd(){
    $db= new sqldb();
    $db->query("SELECT * FROM multiple_identity ");
    $out_str=array();

    while($db->move_next()){
        array_push($out_str, array(
            'id'=>$db->f('identity_id'),
            'name'=>$db->f('identity_name')
        ));
    }
    return $out_str;
}
function generate_service_number($database=''){
    if($database){
        global $database_config,$cfg;
        $db_conf = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database,
        );
        $db = new sqldb($db_conf);
    }else{
        $db = new sqldb();
    }

    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_SERVICE_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_SERVICE_DIGIT_NR' ");
    $prefix_lenght=strlen($invoice_start)+1;
    $db->query("SELECT service_id,serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM servicing_support
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");
    if(!$digit_nr){
        $digit_nr = 0;
    }
    if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
        $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}
//Build Country DD
function build_country_list($selected ='',$view_mode=false,$country_ids=''){
    $db=new sqldb;
    if(!$selected)
    {
        $selected = MAIN_COUNTRY;
    }

    $filter = " 1=1  ";
    if($view_mode === true){

        $filter.= " AND country_id='".$selected."' ";

    }
     $filter_country_ids = " ";
    if($country_ids){
        $filter_country_ids = " AND country_id in (".$country_ids.") ";
    }
    $db->query("SELECT * FROM country WHERE ".$filter.$filter_country_ids." ORDER BY name");
    if($view_mode === true && $db->move_next()){
        return $db->f('name');
    }
    $out_str=array();
    while($db->move_next()){
        array_push($out_str, array(
            'id'=>$db->f('country_id'),
            'name'=>$db->f('name')
        ));
    }
    return $out_str;
}

function build_user_dd_service($selected, $access){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $array = array();
    $db = new sqldb($db_config);
    $db2 = new sqldb();
    $array = array();

    $db->query("SELECT user_id, first_name, last_name,group_id FROM users WHERE database_name='".DATABASE_NAME."' AND user_id='".$_SESSION['u_id']."'  ");
    $db->move_next();
    $array[$db->f('user_id')] = utf8_encode($db->f('first_name'))." ".utf8_encode($db->f('last_name'));
    $filter = ' 1=1 ';
    $group_id = $db->f('group_id');
    if($group_id != 2){
        if($access > 1 ){
            $filter = " user_role > '".$_SESSION['access_level']."' OR user_id='".$selected."' ";
        }
        $is_manager = $db2->field("SELECT service_id FROM servicing_support_users WHERE pr_m='1' AND user_id='".$_SESSION['u_id']."' ");

        if($_SESSION['group'] != 'admin' && !empty($is_manager)){
            $u = '';
            $users = $db2->query("SELECT user_id FROM  `servicing_support_users` WHERE service_id IN
                                    ( SELECT service_id FROM `servicing_support_users` WHERE pr_m='1' AND user_id ='".$_SESSION['u_id']."' )
                                  GROUP BY user_id ");
            while ($users->next()) {
                $u .= $users->f('user_id').',';
            }
            $filter =" user_id IN ( ".rtrim($u,',')." ) ";
        }
        $db->query("SELECT user_id, first_name, last_name FROM users WHERE database_name='".DATABASE_NAME."' AND $filter AND active='1' AND credentials like '%13%'  ");
        while ($db->move_next()) {
            $array[$db->f('user_id')] = utf8_encode($db->f('first_name'))." ".utf8_encode($db->f('last_name'));
        }
    }
    asort($array);
    $new_array = array();
    foreach ($array as $key => $value) {
        array_push($new_array, array('id'=>$key,'value'=>$value));
    }

    // $out[0] = $array[$selected];
    // $out[1] = $array;
    return $new_array;
}

function getItemPerm($perm){
    if(!is_array($perm)){
        return false;
    }
    #we check if the user has access to the module
    $module_access=false;
    if($_SESSION['group']=='admin') {
        $module_access=true;
    } else {
        if(in_array($perm['module'], $_SESSION['admin_sett'])) {
            $module_access=true;
        } else {
            $module_access=false;
        }
    }
    $permission = $module_access;
    if($permission === false) {
        #the user does not have admin access for module
        #we check to see if he has access to the item
        if(!is_array($perm['item'])){
            $perm['item'] = (array)$perm['item'];
        }
        foreach ($perm['item'] as $key => $value) {
            if($_SESSION['u_id'] == $value) {
                $permission = true;
                break;
            }
        }
    }
    return $permission;

}

function display_number_var_dec($number){
    if(!$number){
        $number = 0;
    }
    $separator = ACCOUNT_NUMBER_FORMAT;
    $decimals_after_comma = ARTICLE_PRICE_COMMA_DIGITS;
    $out = number_format(floatval($number),$decimals_after_comma,substr($separator, -1),substr($separator, 0, 1));
    return $out;
}

function remove_zero_decimals($number,$decimals=2){
    $separator = ACCOUNT_NUMBER_FORMAT;
    if(!$number){
        $number = '0.00';
    }
    if(substr($number, -2,2)== '00'){
        $out = intval($number);
    }else{
        $pos = strpos($number, '.');
        if($pos === false){
            return $number;
        }
        $dec_part = substr($number, -2,2);
        $int_part = intval($number);
        // console::log($int_part,substr($separator, -1),$dec_part);
        if( substr($number,0,1) == '-' && $int_part == 0 ){
            $minus_sign = '-';
        }
        $out = $minus_sign.$int_part.substr($separator, -1).$dec_part;

    }
    return $out;
}

function build_pdf_language_dd($selected='',$active){

    $db = new sqldb();
    $array = array();
    $filter="1=1";
    if($active){
        $filter.=" and active=1";
    }

    $db->query("SELECT * FROM pim_lang where  ".$filter." ORDER BY sort_order ");
    while($db->move_next()){
        $row = array('id'=>$db->f('lang_id'),'name'=>gm($db->f('language')));
        array_push($array, $row);
    }
    $lng = $db->query("SELECT * FROM pim_lang where  ".$filter." ORDER BY sort_order ");
    while($lng->next()){
        $l = $db->query("SELECT * FROM label_language WHERE lang_code='".$lng->f('lang_id')."'");
        if($l->next())
        {
            $row = array('id'=>$lng->f('label_language_id'),'name'=>gm($lng->f('language')));
            array_push($array, $row);
        }
    }
    return $array;

}

function build_simple_dropdown($opt, $selected=0,$view=0){
    $out_str = array();
    if($view==0){
        if($opt)

        while (list ($key, $val) = each ($opt))
        {
            array_push($out_str, array('id'=>$key,'value'=>$val));
        }
    }elseif($view==1){
        if($opt)
            while (list ($key, $val) = each ($opt))
            {
                $out_str.="<option value=\"".$key."\" ";//options values
                $out_str.=($key==$selected?" SELECTED ":"");//if selected
                $out_str.=">".$val."</option>";//options names            
            }
    }
    else{
        $out_str = '';
        if($opt)
        while (list ($key, $val) = each ($opt))
        {
            if($key==$selected){
                  $out_str=  $val;
            }

        }
    }
    return $out_str;
}

function wrap($string, $width=75, $break="\n", $cut=true){
  if($cut) {
    // Match anything 1 to $width chars long followed by whitespace or EOS,
    // otherwise match anything $width chars long
    $search = '/(.{1,'.$width.'})(?:\s|$)|(.{'.$width.'})/uS';
    $replace = '$1$2'.$break;
  } else {
    // Anchor the beginning of the pattern with a lookahead
    // to avoid crazy backtracking when words are longer than $width
    $pattern = '/(?=\s)(.{1,'.$width.'})(?:\s|$)/uS';
    $replace = '$1'.$break;
  }
  return preg_replace($search, $replace, $string);
}

function wrap2($string){
    $string = utf8_decode($string);
    $string2='';
    $q = strlen($string);
    for ($i=0; $i <= $q; $i++) {
    $char = substr( $string, $i, 1 );
        $string2 .= $char.' &nbsp;';
    }
    return utf8_encode($string2);
}
/* o functie care s-a uitat de mult ce trebuia sa faca, dar nu o putem scoate ca este peste tot in proiect */
function insert_message_log($pag,$message,$field_name,$field_value,$type = 0,$user_id = '', $track_log=false, $customer_id=0, $the_time='0'){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    if(!$the_time)
    {
        $the_time=time();
    }
    $db = new sqldb();
    $db_users = new sqldb($db_config);
    //$customer_id = '';
    $message = stripslashes($message);
    $id = $db->insert("INSERT INTO logging SET pag='".$pag."', message='".addslashes($message)."', field_name='".$field_name."', field_value='".$field_value."', date='".$the_time."', type='".$type."', user_id='".$user_id."', reminder_date='".$the_time."' ");
    if($track_log==true)
    {
        $users = $db_users->query("SELECT user_id FROM users WHERE database_name='".DATABASE_NAME."'")->getAll();
        foreach ($users as $key => $value) {
            $db->query("INSERT INTO logging_tracked SET
                                log_id='".$id."',
                                customer_id='".$customer_id."',
                                user_id='".$value['user_id']."'");
        }
        $db->query("UPDATE logging_tracked SET seen='1' WHERE user_id='".$_SESSION['u_id']."' AND log_id='".$id."'");
    }
    return $id;
}
/* end of CMS MENU functions */
function build_dispach_location_dd($selected = '',$article_ids,$own=''){
    $db=new sqldb();
    //first we select all our address
    $array=array();
    $db->query("SELECT  dispatch_stock_address.* FROM  dispatch_stock_address order by is_default DESC");

    while($db->move_next()){
       array_push($array, array('id'=>'0-'.$db->f('address_id'),'name'=>$db->f('naming').' / '.$db->f('address').' '.$db->f('zip').' '.$db->f('city').' '.$db->f('to_country')));
    }
    
if(!$own){
    //we selct customer addreses
    $db->query("SELECT  dispatch_stock.*,pim_stock_disp.to_naming,pim_stock_disp.to_address,pim_stock_disp.to_zip,pim_stock_disp.to_city,pim_stock_disp.to_country
                 FROM  dispatch_stock
                    INNER JOIN pim_stock_disp ON pim_stock_disp.to_customer_id=dispatch_stock.customer_id and pim_stock_disp.to_address_id=dispatch_stock.address_id
                 WHERE dispatch_stock.customer_id!=0
                 GROUp BY dispatch_stock.customer_id,dispatch_stock.address_id");


     while($db->move_next()){
       array_push($array, array('id'=>$db->f('customer_id').'-'.$db->f('address_id'),'name'=>$db->f('to_naming').' / '.$db->f('to_address').' '.$db->f('to_zip').' '.$db->f('to_city').' '.$db->f('to_country')));
    }
}
    return $array;
}

function generate_purchase_price($article_id,$first=0){

    $db = new sqldb();
    $db2 = new sqldb();
    $wac_act_date=WAC_ACT_DATE;
    $price=0;
    $total_stock_value=0;
    $total_quantity_purchase=0;
    $wac=0;
    $stocks_value_entry=array();
    $stock_exit=0;

    $stocks_value=array();
    if($first){
        $wac=$db->field("SELECT pim_p_order_articles.price FROM pim_p_order_articles WHERE article_id='".$article_id."' ORDER BY order_articles_id DESC LIMIT 1");
        if($wac){
            $db->query("UPDATE pim_article_prices SET purchase_price='".$wac."' WHERE base_price=1 AND article_id='".$article_id."'");
        }
        return true;
    }
    //first we select all  entries from purchase order
    $db->query("SELECT pim_p_order_deliveries.date,pim_p_order_deliveries.delivery_id,pim_p_order_articles.article_id,pim_p_order_articles.order_articles_id,pim_p_order_articles.price,pim_p_orders_delivery.quantity,pim_p_order_deliveries.date
                FROM pim_p_order_articles
                INNER JOIN pim_p_orders_delivery ON pim_p_orders_delivery.order_articles_id= pim_p_order_articles.order_articles_id
                INNER JOIN  pim_p_order_deliveries ON  pim_p_order_deliveries.delivery_id=pim_p_orders_delivery.delivery_id
                WHERE pim_p_order_articles.article_id='".$article_id."' AND pim_p_order_deliveries.date > '".$wac_act_date."'
                ORDER BY pim_p_order_deliveries.date ASC
              ");

    while($db->move_next()) {
        $stocks_value_entry[$db->f('delivery_id')] = $db->f('quantity');
    }

   //the magic now we selelct all exit form order
    $db->query("SELECT pim_order_deliveries.date,pim_order_deliveries.delivery_id,pim_order_articles.article_id,pim_order_articles.order_articles_id,pim_order_articles.price,sum(pim_orders_delivery.quantity) as qty,pim_order_deliveries.date
                FROM pim_order_articles
                INNER JOIN pim_orders_delivery ON pim_orders_delivery.order_articles_id= pim_order_articles.order_articles_id
                INNER JOIN  pim_order_deliveries ON  pim_order_deliveries.delivery_id=pim_orders_delivery.delivery_id
                WHERE pim_order_articles.article_id='".$article_id."' AND pim_order_deliveries.date > '".$wac_act_date."'
                 ORDER BY pim_order_deliveries.date ASC
              ");
    $db->move_next();
    $returned = $db2->field("SELECT sum(quantity) as qty_p FROM  pim_articles_return WHERE article_id='".$article_id."'");
    $stock_exit=$db->f('qty')-$returned;
    $db->query("SELECT sum(quantity) FROM  project_articles WHERE delivered=1 AND article_id='".$article_id."'");
    $db->move_next();
    $stock_exit=$db->f('qty_p')+$stock_exit;
    $i=0;
    //we have to take  entries

    foreach ($stocks_value_entry as $key => $q) {
        if(($q-$stock_exit)>=0){
            $stocks_value_entry[$key]=$q-$stock_exit;
            break;
        }else{
            $stocks_value_entry[$key]=0;
            $stock_exit= $stock_exit-$q;
        }
        $i++;
    }

      //calculate wac
    foreach ($stocks_value_entry as $key => $q) {
        $price=$db->field("SELECT pim_p_order_articles.price
                              FROM pim_p_order_articles
                              INNER JOIN pim_p_order_deliveries ON pim_p_order_deliveries.p_order_id=pim_p_order_articles.p_order_id
                              WHERE pim_p_order_articles.article_id='".$article_id."' AND  pim_p_order_deliveries.delivery_id='".$key."'");

        $total_stock_value+=$price*$q;
        $total_quantity_purchase+=$q;
    }

    $wac=@($total_stock_value/$total_quantity_purchase);

    $db2->query("UPDATE pim_article_prices SET purchase_price='".$wac."' WHERE base_price=1 AND article_id='".$article_id."'");
}

function build_language_admin_dd($selected){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT * FROM pim_lang WHERE active='1'  ORDER BY sort_order");
    while($db->move_next()){
        array_push($array,array('id'=>$db->f('lang_id'), 'name' => gm($db->f('language'))));
    }
    return $array;
}

function build_pdf_type_dd($selected,$invoice = false,$quote = false,$timesheet = false,$order = false, $p_order= false){
    $db= new sqldb();

    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_users = new sqldb($db_config);

    $out_str=array();
    $opt=array(
    "Layout 1"      => '1',
    "Layout 2"      => '2',
    "Layout 3"      => '3',
    "Layout 4"      => '4',
    );
    if($timesheet){
        $opt['Layout 5'] = '5';
    }
    if($order){
        $opt['Layout 5'] = '5';

    }if($p_order){
        $opt['Layout 5'] = '5';

    }
    if($invoice){
        $opt['Layout 5'] = '5';
        $opt['Layout 6'] = '6';

        $main_user_id = $db_users->field("SELECT user_id FROM users WHERE main_user_id='0' AND database_name='".DATABASE_NAME."' ");
        $custom_pdf = $db_users->query("SELECT * FROM user_meta WHERE name='custom_pdf_template' AND user_id='".$main_user_id."' ");
        $i=0;
        while($custom_pdf->next()){
            $i++;
            $count = 6+$i;
            $opt['Custom Layout '.$i] = $count;
        }

    }if ($quote) {
        $opt['Layout 5'] = '5';

        // if is custom quote template
        $main_user_id = $db_users->field("SELECT user_id FROM users WHERE main_user_id='0' AND database_name='".DATABASE_NAME."' ");
        $custom_pdf = $db_users->query("SELECT * FROM user_meta WHERE name='custom_pdf_quote_template' AND user_id='".$main_user_id."' ");
        $i=0;
        while($custom_pdf->next()){
            $i++;
            $count = 5+$i;
            $opt['Custom Layout '.$i] = $count;
        }

    } else {
        # nothing to see here
    }

    foreach ($opt as $key=>$val)
    {
        array_push($out_str, array('id'=>$val,'name'=>$key));
    }
    return $out_str;
}

function generate_order_serial_number($database) {
    global $database_config;
    $db_used = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database,
    );
    $db= new sqldb($db_used);
    $account_order_start=$db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_START' ");
    $account_digit_nr=$db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DIGIT_NR' ");
    $account_order_digit_nr=$db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_ORDER_DIGIT_NR' ");

    $prefix_lenght=strlen($account_order_start)+1;
    $left = $account_digit_nr ? $account_order_digit_nr : '';
    if(!$left){
        $left=1;
    }
    $db->query("SELECT pim_orders.order_id,pim_orders.serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM pim_orders
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($account_order_start)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");

    if($db->move_next() && $db->f('invoice_prefix')==$account_order_start){
        $next_serial=$account_order_start.str_pad($db->f('invoice_number')+1,$left,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $order_nr = $account_order_start ? $account_order_start : '';
        $next_serial= $order_nr.str_pad(1,$left,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}

function build_vat_dd(){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT value, vat_id FROM vats ORDER BY value");
    while($db->move_next()){
        array_push($array,array('id'=>$db->f('vat_id'),'name' => display_number($db->f('value')).' %' ));
    }
    return $array;
}
function build_vat_regime_dd($selected=''){
    $db = new sqldb();
    $array = array(
        array('id' => "1" , 'name' => gm("Regular")),
        array('id' => "2" , 'name' => gm("Intra-EU")),
        array('id' => "3" , 'name' => gm("Export")),
        array('id' => "4" , 'name' => gm("Contracting Partner")),
    );
    if(isset($selected)){
        foreach ($array as $key => $value) {
            if($value['id']==$selected){
                return $value['name'];
            }
        }
        return '';
    }
    return $array;    
}

function build_payment_type_dd($selected='',$txt = false){
    $db = new sqldb();
    $array = array();
    if($txt){
        if($selected){
            return $db->field("SELECT name FROM customer_payment_type WHERE id='".$selected."' ORDER BY sort_order");
        }
        return '';
    }
    $db->query("SELECT name, id FROM customer_payment_type ORDER BY sort_order");
    while($db->move_next()){
        array_push( $array , array('id'=>$db->f('id'), 'name' => $db->f('name')));
    }
    return $array;
}

function build_apply_discount_list($selected,$text = false){
    if($text === true){
        switch ($selected) {
            case '1':
                $t = gm('Apply at line level');
                break;
            case '2':
                $t = gm('Apply at global level');
                break;
            case '3':
                $t = gm('Apply at both levels');
                break;
            default:
                $t = gm('No Discount');
                break;
        }
        return $t;
    }
    $array = array( array('id'=>'0', 'name' => gm('No Discount')),
                           array('id'=>'1', 'name' => gm('Apply at line level')),
                           array('id'=>'2', 'name' => gm('Apply at global level')),
                           array('id'=>'3', 'name' => gm('Apply at both levels')));
    return $array;
}

function gfn($value){
    return htmlspecialchars(html_entity_decode($value));
}

function get_article_calc_price($article_id,$price_type=1){
   $db = new sqldb();

   $db->query("SELECT pim_article_prices.price,pim_article_prices.total_price,pim_article_prices.purchase_price FROM pim_article_prices
                                 WHERE pim_article_prices.article_id='".$article_id."' and base_price=1

                                 LIMIT 1
                                 ");
    if($price_type==1){
       return $db->f('price');
    }
    if($price_type==2){
       return $db->f('total_price');
    }
     if($price_type==3){
       return $db->f('purchase_price');
    }

}

function return_value2($number){
    $separator = ACCOUNT_NUMBER_FORMAT;
    $out=str_replace(substr($separator, -1), '.', $number);

    return $out;
}

function doManageLog($action='',$db,$hub=array()){
    if(!$db){
        $db = DATABASE_NAME;
    }
    global $database_config,$cfg;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $dbs = new sqldb($database_users);
    $dbs->query("INSERT INTO users_manage_logs SET database_name='".$db."', action='".$action."', dates='".date('Y-m-d H:i:s')."' ");

    $database_2 = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $db,
    );
    $db2 = new sqldb($database_2);
    $hub_vid = $db2->field("SELECT value FROM settings WHERE constant_name='HUBSPOT_VID' ");

    if(!empty($hub) && !empty($hub_vid) ){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        if($hub[0]['property'] == 'first_module_usage'){
            $host_contact = 'https://api.hubapi.com//contacts/v1/contact/vid/'.$hub_vid.'/profile?hapikey='.$cfg['hubspot_api_key'];
            curl_setopt($ch, CURLOPT_URL, $host_contact);
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            $result = json_decode($result);
            $hub[0]['value'] .= ';'.$result->properties->first_module_usage->value;
        }

        $host_update = 'https://api.hubapi.com/contacts/v1/contact/vid/'.$hub_vid.'/profile?hapikey='.$cfg['hubspot_api_key'];

        $post = array('properties'=>$hub);
        $post = json_encode($post);


        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        curl_setopt($ch, CURLOPT_URL, $host_update);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        $result = json_decode($result);
        if($result){
            $dbs->query("INSERT INTO cron SET time=NOW(), db='hubspot', extra='".addslashes(serialize($result))."' ");
        }

    }
}

function generate_invoice_number($database){
    global $database_config,$cfg;
    $db_conf = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database,
    );
    $db = new sqldb($db_conf);

    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INVOICE_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_DIGIT_NR' ");
    $invoice_starting_nr= $db->field("SELECT value FROM settings WHERE constant_name='INVOICE_STARTING_NUMBER'");
    $prefix_lenght=strlen($invoice_start)+1;
    $db->query("SELECT tblinvoice.id,tblinvoice.serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM tblinvoice
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."' AND type!='2'
                    ORDER BY invoice_number DESC
                    LIMIT 1");
    if(!$digit_nr){
        $digit_nr = 0;
    }
    // console::log($db->f('invoice_prefix'),$invoice_start,$db->f('invoice_number'),$digit_nr);
    if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
        if($invoice_starting_nr && $invoice_starting_nr>$db->f('invoice_number')){
            $next_serial=$invoice_start.str_pad($invoice_starting_nr,$digit_nr,"0",STR_PAD_LEFT);
        }else{
            $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        }     
        return $next_serial;
    }else{
        if($invoice_starting_nr){
            $next_serial=$invoice_start.str_pad($invoice_starting_nr,$digit_nr,"0",STR_PAD_LEFT);
        }else{
            $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        }       
        return $next_serial;
    }
}

function generate_credit_invoice_number(){
    $db = new sqldb();

    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CREDIT_INVOICE_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_CREDIT_DIGIT_NR' ");
    $credit_starting_nr= $db->field("SELECT value FROM settings WHERE constant_name='CREDIT_STARTING_NUMBER'");
    $prefix_lenght=strlen($invoice_start)+1;

    $db->query("SELECT tblinvoice.id,tblinvoice.serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM tblinvoice
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."' AND type=2
                    ORDER BY invoice_number DESC
                    LIMIT 1");

    if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
        if($credit_starting_nr && $credit_starting_nr>$db->f('invoice_number')){
            $next_serial=$invoice_start.str_pad($credit_starting_nr+1,$digit_nr,"0",STR_PAD_LEFT);
        }else{
            $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        }
        return $next_serial;
    }else{
        if($credit_starting_nr){
            $next_serial=$invoice_start.str_pad($credit_starting_nr,$digit_nr,"0",STR_PAD_LEFT);
        }else{
            $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        } 
        return $next_serial;
    }
}


/**
 * Get the account company name
 *
 * @return void
 * @author
 **/
function get_company_byUser($db,$id){
    $name = $db->field("SELECT ACCOUNT_COMPANY FROM users WHERE user_id='".$id."' ");
    return $name;
}

function build_article_brand_dd($selected='',$view=0){
    $db = new sqldb();
    $e = $db->query("SELECT id,name FROM pim_article_brands ORDER BY sort_order")->getAll();
    array_unshift($e, array('id'=>'','name'=>gm('Select brand')));
    return $e;
}
function build_article_ledger_account_dd($selected='',$view=0){
    $db = new sqldb();
    $e = $db->query("SELECT id,name FROM  pim_article_ledger_accounts   ORDER BY sort_order")->getAll();
    array_unshift($e, array('id'=>'','name'=>gm('Select Ledger Account')));
    return $e;
}
function build_article_category_dd($selected='',$view=0){
    $db = new sqldb();
    $e=$db->query("SELECT id,name FROM pim_article_categories ORDER BY sort_order")->getAll();
    array_unshift($e, array('id'=>'','name'=>gm('Select article category')));
    return $e;
}

/**
 * undocumented function
 *
 * @return void
 * @author
 **/
function update_articles($art,$db){
    global $database_config;

    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $db,
    );
    $db = new sqldb($database_users);
    if(!is_array($art)){
        $art = array($art);
    }
    foreach ($art as $key => $value) {
        $price = $db->field("SELECT price FROM pim_article_prices WHERE article_id='".$value."' AND base_price='1' ");
        $article_category = $db->field("SELECT name FROM pim_article_categories WHERE id=(SELECT article_category_id FROM pim_articles WHERE article_id='".$value."') ");
        $photo = $db->field("SELECT parent_id FROM pim_article_photo WHERE parent_id='".$value."' ");
        $update = $db->query("UPDATE pim_articles SET price='".$price."',article_category='".addslashes($article_category)."',parent_id='".$photo."' WHERE article_id='".$value."' ");
    }
}
function insert_product_attribute_value($item_id, $attribute_array, $in,$apply_to){
    if($apply_to==1) //product
    {
        $product_id=$item_id;
        $article_id=0;
    }else{
        $product_id=0;
        $article_id=$item_id;
    }

    $db=new sqldb();

    switch($attribute_array['type'])
    {

        //text_field
        case 1:
            {
                if($in[$attribute_array['field_name']])
                {
                    $db->query("INSERT INTO pim_product_attribute_value ( product_id, article_id, product_attribute_id, product_attribute_set_id, product_attribute_option_id, value, short_text, long_text )
                                                           VALUES
                                                          (
                                                           '".$product_id."',
                                                           '".$article_id."',
                                                           '".$attribute_array['product_attribute_id']."',
                                                           '".$attribute_array['product_attribute_set_id']."',
                                                           '',
                                                           '".$in[$attribute_array['field_name']]."',
                                                           '".$in[$attribute_array['field_name']]."',
                                                           ''
                                                           )
                           ");
                }
                return true;
            }break;
            //text_area
        case 2:
            {  if($in[$attribute_array['field_name']])
            {
                $db->query("insert into pim_product_attribute_value (
                                                            product_id,
                                                            article_id,
                                                            product_attribute_id,
                                                            product_attribute_set_id,
                                                            product_attribute_option_id,
                                                            value,
                                                            short_text,
                                                            long_text
                                                           )
                                                           values
                                                          (
                                                           '".$product_id."',
                                                           '".$article_id."',
                                                           '".$attribute_array['product_attribute_id']."',
                                                           '".$attribute_array['product_attribute_set_id']."',
                                                           '',
                                                           '',
                                                           '',
                                                           '".$in[$attribute_array['field_name']]."'
                                                           )
                           ");
            }

            }break;
            //select_box
        case 3:
            {
                if($in[$attribute_array['field_name']])
                {
                    $product_attribute_option_id=$in[$attribute_array['field_name']];
                    $db->query("select value from pim_product_attribute_option where product_attribute_option_id='".$product_attribute_option_id."'");
                    $db->move_next();
                    $value=$db->f('value');
                    $db->query("insert into pim_product_attribute_value (
                                                            product_id,
                                                            article_id,
                                                            product_attribute_id,
                                                            product_attribute_set_id,
                                                            product_attribute_option_id,
                                                            value,
                                                            short_text,
                                                            long_text
                                                           )
                                                           values
                                                          (
                                                           '".$product_id."',
                                                           '".$article_id."',
                                                           '".$attribute_array['product_attribute_id']."',
                                                            '".$attribute_array['product_attribute_set_id']."',
                                                           '".$product_attribute_option_id."',
                                                           '".$value."',
                                                           '',
                                                           ''
                                                           )
                           ");
                }


            }break;
            //radio_buttons
        case 4:
            {
                if($in[$attribute_array['field_name']])
                {
                    $product_attribute_option_id=$in[$attribute_array['field_name']];
                    $db->query("select value from pim_product_attribute_option where product_attribute_option_id='".$product_attribute_option_id."'");
                    $db->move_next();
                    $value=$db->f('value');
                    $db->query("insert into pim_product_attribute_value (
                                                            product_id,
                                                            article_id,
                                                            product_attribute_id,
                                                            product_attribute_set_id,
                                                            product_attribute_option_id,
                                                            value,
                                                            short_text,
                                                            long_text
                                                           )
                                                           values
                                                          (
                                                           '".$product_id."',
                                                           '".$article_id."',
                                                           '".$attribute_array['product_attribute_id']."',
                                                           '".$attribute_array['product_attribute_set_id']."',
                                                           '".$product_attribute_option_id."',
                                                           '".$value."',
                                                           '',
                                                           ''
                                                           )
                           ");
                }
                return true;

            }break;
            //single_checbox
        case 5:
            {
                if($in[$attribute_array['field_name']])
                {
                    $db->query("insert into pim_product_attribute_value (
                                                            product_id,
                                                            article_id,
                                                            product_attribute_id,
                                                            product_attribute_set_id,
                                                            product_attribute_option_id,
                                                            value,
                                                            short_text,
                                                            long_text
                                                           )
                                                           values
                                                          (
                                                           '".$product_id."',
                                                           '".$article_id."',
                                                           '".$attribute_array['product_attribute_id']."',
                                                           '".$attribute_array['product_attribute_set_id']."',
                                                           '',
                                                           '1',
                                                           '',

                                                           ''
                                                           )
                           ");
                }

            }break;
            //checboxes_block
        case 6:
            {
                if($in[$attribute_array['field_name']])
                {
                    foreach ($in[$attribute_array['field_name']] as $product_attribute_option_id => $value )
                    {
                        $db->query("insert into pim_product_attribute_value (
                                                            product_id,
                                                            article_id,
                                                            product_attribute_id,
                                                            product_attribute_set_id,
                                                            product_attribute_option_id,
                                                            value,
                                                            short_text,
                                                            long_text
                                                           )
                                                           values
                                                          (
                                                           '".$product_id."',
                                                           '".$article_id."',
                                                           '".$attribute_array['product_attribute_id']."',
                                                           '".$attribute_array['product_attribute_set_id']."',
                                                           '".$product_attribute_option_id."',
                                                           '".$value."',
                                                           '',
                                                           ''
                                                           )
                           ");
                    }
                }
                return true;

            }
            break;


    }

}

function build_some_dd($t,$e){
    switch ($t) {
         case 'tblquote_lost_reason':
            return build_data_qq();
            break;
        case 'customer_payment_type':
            return build_payment_type_dd();
            break;
        case 'pim_article_brands':
            return build_article_brand_dd();
            break;
        case 'pim_article_ledger_accounts':
            return build_article_ledger_account_dd();
            break;
        case 'pim_article_categories':
            return build_article_category_dd();
            break;
        case 'customer_sector':
            return build_s_type_dd();
            break;
        case 'customer_legal_type':
            return build_l_type_dd();
            break; 
        case 'customer_type':
            return getCustomerType();
            break;
        case 'customer_contact_language':
            return build_language_dd();
            break;
        case 'sales_rep':
            return getSales();
            break;
        case 'customer_lead_source':
            return build_l_source_dd();
            break;
        case 'customer_activity':
            return build_c_activity_dd();
            break;
        case 'customer_various1':
            return build_various_dd('','1');
            break;
        case 'customer_various2':
            return build_various_dd('','2');
            break;
        case 'customer_extrafield_dd':
            return build_extra_field_dd('',$e);
            break;
        case 'customer_contact_title':
            return build_contact_title_type_dd('',$e);
            break;
        case 'customer_contact_title':
            return build_contact_title_type_dd('',$e);
            break;
        case 'customer_contact_job_title':
            return build_contact_job_title_type_dd();
            break;
        case 'customer_contact_dep':
            return build_contact_dep_type_dd();
            break;
        case 'contact_extrafield_dd':
            return build_extra_contact_field_dd('',$e);
            break;
    }
}

function build_extra_field_dd($selected='',$field_id){

    $db = new sqldb();
    $array = array();
    $db->query("SELECT * FROM customer_extrafield_dd WHERE extra_field_id='".$field_id."' ORDER BY name ASC");
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('id'),'name'=>$db->f('name')));
    }    
    // return build_simple_dropdown($array,$selected,'1');
    return $array;

}

function get_extra_dd_value($field_id){
    $db = new sqldb();
    return $db->field("SELECT name FROM customer_extrafield_dd WHERE id='".$field_id."' ");
}

function build_language_dd($selected='',$view_mode=false){

    $db = new sqldb();
    $array = array();
    $filter = "";
    if($view_mode === true){
        $filter = " WHERE id='".$selected."' ";
    }
    $db->query("SELECT * FROM customer_contact_language ".$filter." ORDER BY name ASC");
    if($view_mode === true && $db->move_next()){
        return $db->f('name');
    }
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('id'),'name'=>$db->f('name')));
    }
    return $array;
    // return build_simple_dropdown($array,$selected);
}

function build_l_source_dd($selected='',$join = false){

    $db = new sqldb();
    $array = array();
    $filter = '';
    if($join){
        $filter = " INNER JOIN customers ON customers.lead_source = customer_lead_source.id WHERE customers.active='1' ";
    }
    $db->query("SELECT customer_lead_source.* FROM customer_lead_source ".$filter." ORDER BY customer_lead_source.name ASC");
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('id'),'name'=>$db->f('name')));
    }
    return $array;

}

function build_c_activity_dd($selected=''){

    $db = new sqldb();
    $array = array();
    $db->query("SELECT * FROM customer_activity ORDER BY name ASC");
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('id'),'name'=>$db->f('name')));
    }
    return $array;

}

function build_various_dd($selected='',$type = '1'){

    $db = new sqldb();
    $array = array();
    $db->query("SELECT * FROM customer_various".$type." ORDER BY name ASC");
    while($db->move_next()){
        array_push($array, array('id'=>$db->f('id'),'name'=>$db->f('name')));
    }
    return $array;

}

function build_l_type_dd($selected){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT name, id FROM customer_legal_type ORDER BY name ASC");
    while($db->move_next()){
        array_push($array, array('id'=>$db->f('id'),'name'=>$db->f('name')));
    }
    return $array;
}

function build_s_type_dd($selected = ''){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT name, id FROM customer_sector ORDER BY name ASC");
    while($db->move_next()){
        array_push($array, array('id'=>$db->f('id'),'name'=>$db->f('name')));
    }
    return $array;
}

function getCustomerType($term='')
{
    $q = strtolower($term);

    $filter = ' 1=1 ';

    if($q){
        if(strrchr($q, ", ")){
            $q = substr(strrchr($q, ", "), 2);
        }
      $filter .=" AND name LIKE '".$q."%'";
    }

    $db = new sqldb();

    $customer_type = $db->query("SELECT * FROM customer_type WHERE  ".$filter." ORDER BY name ASC")->getAll();
    
    return $customer_type;
}
function getAccountManager($term='')
{
    $q = strtolower($term);

    global $database_config;

    $database_users = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
    );

    $db_users = new sqldb($database_users);

    $filter = '';

    if($q){
        $q = trim($q);
      $filter .=" AND users.first_name LIKE '".$q."%'";
    }

    $u = $db_users->query("SELECT CONCAT_WS(' ',users.first_name, users.last_name) AS name, users.user_id AS id FROM users LEFT JOIN user_meta ON users.user_id = user_meta.user_id AND name='active' WHERE  database_name='".DATABASE_NAME."' AND users.user_type!=2 AND users.active='1' $filter  AND ( user_meta.value!='1' OR user_meta.value IS NULL ) ORDER BY name ASC")->getAll();
    $users =array();
    foreach ($u as $key => $value) {
        array_push($users, array('id'=>$value['id'],'name'=>utf8_encode($value['name'])));
    }
    
    return $users;
}
function getSales($term='')
{
    $q = strtolower($term);

    $filter = '1=1';

    if($q){
      $filter .=" AND name LIKE '".$q."%'";
    }

    $db = new sqldb();

    $s = $db->query("SELECT * FROM sales_rep WHERE  ".$filter." ORDER BY name ASC ")->getAll();
    $users =array();
    foreach ($s as $key => $value) {
        array_push($users, array('id'=>$value['id'],'name'=>utf8_encode($value['name'])));
    }
    
    return $users;
}

function set_first_letter($table,$value,$field,$id)
{
    $db = new sqldb();
    $value = addslashes(substr(strtoupper($value), 0,1));
    $db->query("UPDATE ".$table." SET first_letter='".$value."' WHERE ".$field."='".$id."' ");
}
function build_cat_dd(){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT name, category_id FROM pim_article_price_category ORDER BY category_id");
    while($db->move_next()){
        // $array[$db->f('category_id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('category_id'),'name'=> $db->f('name') ) );
    }
    return $array;
}

function build_contact_title_type_dd($selected,$view_mode=false){
    $db = new sqldb();
    $array = array();
   
    $db->query("SELECT name, id FROM customer_contact_title ORDER BY name ASC");
    if($view_mode === true ){
        while($db->move_next()){
            $array[$db->f('id')]=$db->f('name');
        }
        return $array;
    }

    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
           array_push($array, array('id'=>$db->f('id'),'name'=> $db->f('name') ) );
    }
    return $array;
}

function build_contact_function($selected,$view_mode=false){
    $db= new sqldb();
    $array = array();
    
    $db->query("SELECT name, id FROM customer_contact_job_title ORDER BY name ASC ");
    if($view_mode === true ){
        while($db->move_next()){
            $array[$db->f('id')]=$db->f('name');
        }
        return $array;
    }
    
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
           array_push($array, array('id'=>$db->f('id'),'name'=> $db->f('name') ) );
    }
    return $array;
}
function build_contact_customers($selected,$view_mode=false){
    $db= new sqldb();
    $array = array();
    $filter = "";
    if($view_mode == true){
        $filter = " WHERE customer_id='".$selected."' AND customers.active=1 ";
    }
    $db->query("SELECT customers.name,customers.customer_id FROM customers ".$filter." ORDER BY customers.name ");
    if($view_mode === true && $db->move_next()){
        return $db->f('name');
    }
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
           array_push($array, array('id'=>$db->f('customer_id'),'name'=> $db->f('name') ) );
    }
    return $array;
}

function build_contact_dep_type_dd($selected,$view_mode=false){
    $db = new sqldb();
    $array = array();
    // $filter = "";
    // if($view_mode == true){
    //     $filter = " WHERE id='".$selected."' ";
    // }
    $db->query("SELECT name, id FROM customer_contact_dep ORDER BY name ASC");
    if($view_mode === true ){
        while($db->move_next()){
            $array[$db->f('id')]=$db->f('name');
        }
        return $array;
    }
    while($db->move_next()){
        array_push($array, array('id'=>$db->f('id'),'name'=> $db->f('name') ) );
    }
    return $array;
}

function build_extra_contact_field_dd($selected='',$field_id){

    $db = new sqldb();
    $array = array();
    $db->query("SELECT * FROM contact_extrafield_dd WHERE extra_field_id='".$field_id."' ORDER BY name ASC");
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('id'),'name'=>$db->f('name')));
    }    
    // return build_simple_dropdown($array,$selected,'1');
    return $array;

}

function build_contact_job_title_type_dd($view_mode=false){
    $db = new sqldb();
    $array = array();
    $filter = "";
    $values = explode(',', $selected);
    $selected = '';
    foreach ($values as $key) {
        if(is_numeric($key)){
            $selected .=$key.',';
        }
    }
    $selected = rtrim($key,',');
    if($view_mode === true && $selected && $selected != "undefined"){

        $filter = " WHERE id IN (".$selected.") ";
    }
    $db->query("SELECT name, id FROM customer_contact_job_title ".$filter." ORDER BY sort_order");
    if($view_mode === true && $selected){
        $name = '';
        while ($db->next()) {
            $name .= $db->f('name').',';
        }
        return rtrim($name,',');
    }
    if($view_mode === true && !$selected){
        $name = '';
        return $name;
    }
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('id'),'name'=>$db->f('name')));
    }
    return $array;
    // return build_simple_dropdown($array,$selected);
}

function build_customer_dd(){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT name, customer_id FROM customers ");
    while($db->move_next()){
        // $array[$db->f('id')] = $db->f('name');
        array_push($array, array('id'=>$db->f('customer_id'),'name'=>$db->f('name')));
    }
    return $array;
}

function get_state_name($state_id)
{
    $db=new sqldb();
    $db->query("SELECT state.*  FROM state WHERE state_id='".$state_id."'");
    $db->move_next();
    $name=$db->f('name');
    return $name;
}

function unique_id($l = 4) {
    return substr(md5(uniqid(mt_rand(), true)), 0, $l);
}


function build_data_qq($term='')
{

    $q = strtolower($term);

    $filter = '1=1';

    if($q){
      $filter .=" AND name LIKE '".$q."%'";
    }

    $db = new sqldb();
    $s = $db->query("SELECT * FROM tblquote_lost_reason WHERE  ".$filter." ORDER BY name ASC ")->getAll();
    $users =array();
    foreach ($s as $key => $value) {
        array_push($users, array('id'=>$value['id'],'name'=>utf8_encode($value['name'])));
    }
    
    return $users;
}
function build_data_dd($table_name,$selected='',$field='name',$opt = array()){
    $data = get_data($table_name,'',$field,$opt);
    $array= array();
    if(is_array($data)){
        foreach ($data as $key => $value) {
            array_push($array, array('id'=>$key,'name'=>$value));
        }
    }
    return $array;
}

function get_data($table_name='', $selected = '', $field ='name', $opt = array()){
    if($table_name == ''){
        return array();
    }
    $dbu = new sqldb();
    $id = isset($opt['id']) ? $opt['id'] : $table_name.'_id';
    $cond = isset($opt['cond']) ? $opt['cond'] : '';
    $selfield = isset($opt['field']) ? $opt['field'] : $field;

    $dbu->query('SELECT '.$id.','.$field.' FROM '.$table_name.' '.$cond);
    $ret_val = array();
    while ($dbu->move_next()){
        $ret_val[$dbu->f($id)] = $dbu->f($selfield);
    }
    return $selected!='' ? $ret_val[$selected] : $ret_val;
}

function get_customer_vat($customer_id,$daba='')
{
    $db=new sqldb();
    if(!empty($daba)){
        global $database_config;
        $db_config = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $daba,
        );
        $db=new sqldb($db_config);
    }
    $vat_value='';
    if($customer_id){
        $db->query("SELECT customers.customer_id,customers.vat_id,vats.value
                    FROM customers
                    INNER JOIN vats ON vats.vat_id=customers.vat_id
                    WHERE customers.customer_id='".$customer_id."'");
        $db->move_next();
        $vat_value=$db->f('value');
    }
    if(!$vat_value){
        $vat_value = $db->field("SELECT value FROM vats WHERE value='".ACCOUNT_VAT."' ");
    }
    return display_number($vat_value);
}

function get_custom_width($all_custom_widths,$can_be_hidden_custom_widths,$custom_line_1,$custom_line_2,$custom_line_3=false,$custom_line_4=false,$custom_line_5=false,$custom_line_6=false){
    // $all_custom_widths - array with all the columns widths that can be shown on the same time (except the variable one)
    // $can_be_hidden_custom_widths - array with all the columns widths that can be hidden
    // $custom_line_1 , $custom_line_2, $custom_line_3, $custom_line_4 - the state of the column (shown = true , hidden = false)
    $total_width = 100;
    $total_fixed_width = 0;
    foreach ($all_custom_widths as $key => $value) {
        $total_fixed_width += $value;
    }
    $fixed_diff = $total_width - $total_fixed_width;
    $variable_diff = 0;
    for ($i=1; $i <= 6; $i++) {
        if(func_get_arg($i+1) == false){
            $variable_diff += $can_be_hidden_custom_widths['custom_line_'.$i];
        }
    }
    $custom_width = $fixed_diff+$variable_diff;
    return $custom_width;
}

function place_currency_new($out,$currency = '',$position='',$currency_font_face=''){


    if(!$currency){
        $currency = get_currency(ACCOUNT_CURRENCY_TYPE);
    }
    if($currency_font_face){
        $currency = '<font face="'.$currency_font_face.'">'.$currency.'</font>';
    }
    if($position==1){
        $out = $out.' '.$currency;
    }else{
        if(ACCOUNT_CURRENCY_FORMAT == 0){
            $out = $out.' '.$currency;
        }
        else {
            $out = $out.' '.$currency;
        }
    }
    return $out;


}

function generate_quote_number($database)
{
    global $database_config;

    $db_used = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database,
    );
    $db= new sqldb($db_used);
    $prefix_lenght=strlen(ACCOUNT_QUOTE_START)+1;
    $left = ACCOUNT_QUOTE_DIGIT_NR;
    $db->query("SELECT tblquote.id,tblquote.serial_number, CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS quote_number,LEFT( serial_number, $prefix_lenght-1 ) AS quote_prefix
                    FROM tblquote
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes(ACCOUNT_QUOTE_START)."'
                    ORDER BY quote_number DESC
                    LIMIT 1");

    if($db->move_next() && $db->f('quote_prefix')==ACCOUNT_QUOTE_START){
        $next_serial=ACCOUNT_QUOTE_START.str_pad($db->f('quote_number')+1,$left,"0",STR_PAD_LEFT);
    }else{
        $next_serial=ACCOUNT_QUOTE_START.str_pad(1,$left,"0",STR_PAD_LEFT);
    }
    return $next_serial;

}

function build_user_dd($selected=''){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $array = array();
    $db = new sqldb($db_config);
    $db2 = new sqldb($db_config);

    $db->query("SELECT users.*, user_meta.value, user_meta.name FROM users LEFT JOIN user_meta ON users.user_id=user_meta.user_id AND user_meta.name='active' WHERE database_name='".DATABASE_NAME."' AND active='1' ");
    while ($db->next()) {
        $active = $db2->field("SELECT name FROM  user_meta WHERE user_id='".$db->f('user_id')."' AND name='active' AND ( user_meta.value =  '1' OR user_meta.value IS NULL )");
        if(!$active){
           $array[$db->f('user_id')] = utf8_encode($db->f('last_name').' '.$db->f('first_name'));
        }
    }
    if(!$selected){
        $selected = 0;
    }
    return build_simple_dropdown($array,$selected);
}
function time_ago($timestamp,$now){
    $dif = $now - $timestamp;
    if($dif < 60 ){
        $out = gm('a few seconds ago');
    }else if($dif < 3600){
        $time = floor($dif/60);
        $out = $time.' '.gm('minutes ago');
        if($time == 1){
            $out = $time.' '.gm('minute ago');
        }
    }else if($dif < 86400){
        $time = floor($dif/3600);
        $out = $time.' '.gm('hours ago');
        if($time == 1){
            $out = $time.' '.gm('hours ago');
        }
    }/*else if($dif < 2592000){
        $time = floor($dif/86400);
        $out = $time.' '.gm('days ago');
        if($time == 1){
            $out = $time.' '.gm('days ago');
        }
    }*/else{
        /*$time = floor($dif/2592000);
        $out = $time.' '.gm('months ago');
        if($time == 1){
            $out = $time.' '.gm('months ago');
        }*/
        $out = date(ACCOUNT_DATE_FORMAT,$timestamp);
    }
    return $out;
}

function get_last_invoice_number($only_serial = false,$dbs='')
{
    if($dbs){
        global $database_config;
        $database_users = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $dbs,
        );
        $db = new sqldb($database_users);
    }else{
        $db=new sqldb();
    }

    $prefix_lenght=strlen(ACCOUNT_INVOICE_START)+1;

    $db->query("SELECT tblinvoice.id,tblinvoice.serial_number FROM tblinvoice WHERE f_archived='0' AND type !='1' AND type !='2' ORDER BY tblinvoice.id DESC LIMIT 1");
    if($db->move_next()){
        $last_serial='('.gm("Last Used").': '.$db->f('serial_number').')';
    }else{
        $last_serial='';
    }
    if($only_serial===true){
        $last_serial=str_replace(ACCOUNT_INVOICE_START, '', $db->f('serial_number'));
    }
    return $last_serial;
}

function build_sepa_mandate_dd($selected, $buyer)
{
    $db= new sqldb();

    $array=array();
    $db->query("SELECT * FROM mandate WHERE buyer_id='".$buyer."'");

    while($db->move_next()){
       array_push($array, array('id'=>$db->f('mandate_id'),'name'=>$db->f('sepa_number')));
    }

    return $array;
}

function update_log_message($id,$message = '',$replace = false)
{
    if(!$message){
        return $id;
    }
    $db = new sqldb();
    if($replace){
        $oldMessage = '';
    }else{
        $oldMessage = $db->field("SELECT message FROM logging WHERE log_id='".$id."' ").',';
    }
    $db->query("UPDATE logging SET message='".addslashes($oldMessage.$message)."' WHERE log_id='".$id."' ");
    return $id;
}

function get_last_credit_invoice_number()
{
    $db=new sqldb();

    $prefix_lenght=strlen(ACCOUNT_CREDIT_INVOICE_START)+1;

    $db->query("SELECT tblinvoice.id,tblinvoice.serial_number FROM tblinvoice WHERE f_archived='0' AND type ='2' ORDER BY tblinvoice.id DESC LIMIT 1");
    if($db->move_next()){
        $last_serial='('.gm("Last Used").': '.$db->f('serial_number').')';
    }else{
        $last_serial='';
    }

    return $last_serial;
}

function get_article_label($article_id,$language_id=DEFAULT_LANG_ID,$module){

    $db=new sqldb();
    if($module=='invoice'){
        $fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='INVOICE_FIELD_LABEL'");
    }
    if($module=='order'){
        $fieldFormat = $db->field("SELECT long_value FROM settings WHERE constant_name='ORDER_FIELD_LABEL'");
    }

    $table = 'pim_articles INNER JOIN pim_article_prices ON pim_articles.article_id = pim_article_prices.article_id
                           INNER JOIN pim_articles_lang ON pim_articles.article_id = pim_articles_lang.item_id
                           LEFT JOIN pim_article_brands ON pim_article_brands.id = pim_articles.article_brand_id';
    $columns = 'pim_articles.*,pim_articles.sale_unit,pim_articles.packing,pim_article_prices.base_price,
                pim_article_prices.price, pim_articles.internal_name,
                pim_article_brands.name AS article_brand,
                pim_articles_lang.description AS description,
                pim_articles_lang.name2 AS item_name2,
                pim_articles_lang.name AS item_name,
                pim_articles_lang.lang_id,
                pim_articles.vat_id,
                pim_articles.block_discount';

    $filter.="pim_articles_lang.lang_id='".$language_id."'";

    $article = $db->query("SELECT $columns FROM $table WHERE $filter AND pim_articles.active='1' and pim_articles.article_id='".$article_id."'");


    while($article->next()){

    $article->move_next();
    $values = $article->next_array();


        $tags = array_map(function($field){
            return '/\[\!'.strtoupper($field).'\!\]/';
        },array_keys($values));

        $label = preg_replace($tags, $values, $fieldFormat);

    return gfn($label);
    }
}

function generate_ogm($db='')
{
    if(!$db){
        $dbs = new sqldb();
    }else{
        global $database_config;
        $database_users = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $db,
        );
        $dbs = new sqldb($database_users);
    }
    $result = '';
    $o = $dbs->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PAYMENT_INS_OPTION' ");
    $i = $dbs->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PAYMENT_INS_OPTION_NR' ");
    if($o){
        switch ($o) {
            case 1:
                $nr = time();
                // $first_part = substr($nr,0,4);$second_part = substr($nr,4,4); $last_part = substr($nr,8,2);
                $first_part = substr($nr,0,3);$second_part = substr($nr,3,4); $last_part = substr($nr,7,3);
                $calc =$first_part.$second_part.$last_part;
                $calc = $calc%97;
                if($calc < 10){
                    $calc = '0'.$calc;
                }
                // $s = strpos($calc,'.');
                // $calc = substr($calc,$s+1,2);
                $result = $first_part.'/'.$second_part.'/'.$last_part.$calc;
                break;
            case 2:
                $nr = $i;$nr2=get_last_invoice_number(true,$db);$nr3='';$calc=0;
                if(strlen($nr) > 3){
                    $nr2 = substr($nr,3).$nr2;
                    $nr= substr($nr,0,3);
                }else{
                    $l = 3-strlen($nr);
                    $nr.=substr($nr2,0,$l);
                    $nr2 = substr($nr2,$l);
                }
                if(strlen($nr2) > 4){
                    $nr3 = substr($nr2,4);
                    $nr2 = substr($nr2,0,4);
                }else{
                    $l = 4-strlen($nr2);
                    for($i=1;$i<=$l;$i++){
                        $nr2 .= '0';
                    }
                }
                if(strlen($nr3) < 3){
                    $l = 3-strlen($nr3);
                    for($i=1;$i<=$l;$i++){
                        $nr3 .= '0';
                    }
                }
                $calc= $nr.$nr2.$nr3;
                $calc = $calc%97;
                if($calc < 10){
                    $calc = '0'.$calc;
                }
                console::log($calc);
                // $s = strpos($calc,'.');
                // $calc = substr($calc,$s+1,2);
                $result = $nr.'/'.$nr2.'/'.$nr3.$calc;

                break;
        }
    }
    return $result;
}
function build_language_ecom_dd($selected){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT * FROM pim_lang WHERE active='1' and front_active='1' ORDER BY sort_order");
    while($db->move_next()){
        $array[$db->f('lang_id')] = gm($db->f('language'));

    }
    return build_simple_dropdown($array,$selected);
}

function build_acc_manager(){
    global $database_config;
    $database_users = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
    );
    $users = array();
    $db_users = new sqldb($database_users);
    $db_users->query("SELECT CONCAT_WS(' ',first_name, last_name) as name FROM users WHERE database_name='".DATABASE_NAME."' order by first_name ");
    while ($db_users->move_next()) {
        $users[] = array('id'=>utf8_encode($db_users->f('name')), 'value'=>utf8_encode($db_users->f('name')) );
    }
    // ksort($users);
    return $users;
}

function build_c_type_name_dd(){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT name, id FROM customer_type ORDER BY sort_order");
    while($db->move_next()){
        $array[] = array('id'=>$db->f('name'), 'value'=>$db->f('name') );
    }
    return $array;    
}

// country exists
function company_country_list($view_mode=false)
{
    $db=new sqldb();
     
    $filter = " WHERE 1=1 ";
    if($view_mode === true){
        $filter = " WHERE country_id='".$selected."' ";
    }
    $db->query("SELECT country.* FROM country
                INNER JOIN customer_addresses ON country.country_id=customer_addresses.country_id
                INNER JOIN customers ON customers.customer_id = customer_addresses.customer_id
                ".$filter." AND customers.active='1' AND front_register='0' GROUP by country.country_id ORDER BY name");
    if($view_mode === true && $db->move_next()){
        return $db->f('name');
    }
    $array="";
    while($db->move_next()){
        $array[] = array('id'=>$db->f('name'),'value'=>$db->f('name'));            
    }
    return $array;

}

function get_contact_nr($customer){
    $db=new sqldb();
    $nr = $db->field("SELECT COUNT(contact_id) FROM customer_contacts WHERE customer_id='".$customer."' and active='1' ");
    return $nr;

}

function build_frequency_list($selected){
    $array=array();
    $texts=array(1=>gm('Weekly'),2=>gm('Monthly'),3=>gm('Quarterly'),4=>gm('Yearly'),5=>gm('Custom'));
    foreach($texts as $key => $val){
        array_push($array, array('id'=>$key,'name'=>$val));
    }
    
    return $array;
}
function generate_customer_number($database)
{
    global $database_config,$cfg;
    $db_conf = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database,
    );
    $db = new sqldb($db_conf);

    //$company_start = 0;

    $company_start=$db->field("SELECT value FROM settings WHERE constant_name='FIRST_REFERENCE_C' ");

    if($company_start){
        $db->query("SELECT customers.customer_id,customers.our_reference
                    FROM customers
                    WHERE our_reference='".addslashes($company_start)."'
                    ORDER BY our_reference DESC
                    LIMIT 1
                   ");
        if(!$db->move_next()){
            return $company_start;
        }else{
            $customer_main_id=$db->field("SELECT customers.customer_id
                    FROM customers
                    WHERE our_reference='".addslashes($company_start)."'

                   ");
            $last_customer_id=$db->field("SELECT customers.customer_id
                    FROM customers
                    ORDER BY customer_id DESC
                    LIMIT 1

                   ");

             return ($last_customer_id-$customer_main_id)+$company_start+1;
        }
    }

    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_COMPANY_DIGIT_NR' ");

    $prefix_lenght=strlen($company_start)+1;
    $db->query("SELECT customers.customer_id,customers.our_reference,CAST(SUBSTRING(our_reference,$prefix_lenght)AS UNSIGNED) AS company_number,LEFT( our_reference, $prefix_lenght-1 ) AS company_prefix
                    FROM customers
                    WHERE SUBSTRING(our_reference,1,$prefix_lenght-1)='".addslashes($company_start)."'
                    ORDER BY company_number DESC
                    LIMIT 1");

    if(!$digit_nr){
        $digit_nr = 0;
    }

    // console::log($db->f('invoice_prefix'),$invoice_start,$db->f('invoice_number'),$digit_nr);
    if($db->move_next() && $db->f('company_prefix')==$company_start){
        $next_serial=str_pad($db->f('company_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial+1;
    }else{
        $next_serial=str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}

function update_q_price($article_id,$price,$vat_id){
       $db = new sqldb();
       $db2 = new sqldb();
      $vat_value   = $db->field("SELECT value from vats WHERE vat_id='".$vat_id."'");
      $db->query("SELECT * FROM  pim_article_prices WHERE article_id='".$article_id."' AND base_price=0 and percent!='0.00'");
        while ($db->move_next()) {
               $total_price=(return_value($price)*$db->f('percent')/100)  + return_value($price);
               $total_with_vat = $total_price+($total_price*$vat_value/100);
               $db2->query("UPDATE pim_article_prices SET
                                                            price           = '".$total_price."',
                                            total_price     = '".$total_with_vat."'
                          WHERE article_price_id='".$db->f('article_price_id')."'

                            ");
        }

}
function generate_serial_number($database)
{
    global $database_config;

    $db_used = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database,
    );
    $db= new sqldb($db_used);
    $prefix_lenght=strlen(ACCOUNT_P_ORDER_START)+1;
    $left = defined('ACCOUNT_DIGIT_NR') ? ACCOUNT_P_ORDER_DIGIT_NR : '';
    if(!$left){
        $left=1;
    }
    $db->query("SELECT pim_p_orders.p_order_id,pim_p_orders.serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM pim_p_orders
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes(ACCOUNT_P_ORDER_START)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");

    if($db->move_next() && $db->f('invoice_prefix')==ACCOUNT_P_ORDER_START){
        $next_serial=ACCOUNT_P_ORDER_START.str_pad($db->f('invoice_number')+1,$left,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $order_nr = defined('ACCOUNT_P_ORDER_START') ? ACCOUNT_P_ORDER_START : '';
        $next_serial= $order_nr.str_pad(1,$left,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}
function generate_aac_price($article_id,$first=0){
 
   $db = new sqldb();
   $db2 = new sqldb();

   //we select al entries for these article
   $db->query("SELECT pim_p_orders.add_cost,pim_p_orders.amount,pim_p_order_articles.article_id,pim_p_order_articles.quantity,pim_p_order_articles.price
               FROM pim_p_order_articles
               INNER JOIN pim_p_orders on pim_p_orders.p_order_id=pim_p_order_articles.p_order_id
               WHERE article_id='".$article_id."'

    ");
    
 
  
   while($db->move_next()) {
       
       $ratio=($db->f("quantity")*$db->f("price")/($db->f("amount")- $db->f("add_cost")))*100;
       $distributed_cost+=$db->f("add_cost")*$ratio/100;
       $total_q+=$db->f("quantity");
      

      
       //$db2->query("UPDATE  pim_articles SET aac_price='".$aac_price."' WHERE  article_id='".$article_id."'");
       
   }
   /*$aac_price=$distributed_cost/$total_q;*/
    $aac_price=number_format($aac_price, 2, '.', '');
   $db2->query("UPDATE  pim_articles SET aac_price='".$aac_price."' WHERE  article_id='".$article_id."'");


}

function check_if_activity_exists($customer_id='',$contact_id=''){
    $db = new sqldb();
    $now = time();
    if(!$in['user_id'])
    {
        $in['user_id'] = $_SESSION['u_id'];
    }
    $limit = "";
    $filter_c = '';
    $filter_c2 = '';
    $filter_l = '';
    $filter_q = '';
    if($customer_id){
        $in['customer_id'] = $customer_id;
    }
    if($contact_id){
        $in['contact_id'] = $contact_id;
    }
    if($in['customer_id'])
    {
        $filter_c = "AND customer_id='".$in['customer_id']."'";
        $filter_c2 = " AND customer_id='".$in['customer_id']."'";
    }
    if($in['contact_id2']){
        $in['contact_id'] = $in['contact_id2'];
    }
    if($in['contact_id']){
        $filter_c .= " AND contact_id='".$in['contact_id']."' ";
    }
    $i=0;
    $comments = $db->query("SELECT customer_contact_activity.* FROM customer_contact_activity WHERE email_to='{$in['user_id']}' {$filter_c} ORDER BY date DESC {$limit} ");
    // console::log("SELECT customer_contact_activity.* FROM customer_contact_activity WHERE email_to='{$in['user_id']}' {$filter_c} ORDER BY date DESC {$limit} ");
    $in['has_activities'] = false;
    if($comments->next()){
        // console::log('first');
        $in['has_activities'] = true;
    }else{
        if($in['contact_id']){
            $comments2 = $db->query("SELECT customer_contact_activity.*, customer_contact_activity_contacts.contact_id AS contact_id2
                FROM customer_contact_activity
                LEFT JOIN customer_contact_activity_contacts
                ON customer_contact_activity.customer_contact_activity_id = customer_contact_activity_contacts.activity_id
                WHERE email_to='{$in['user_id']}' {$filter_c2} AND customer_contact_activity_contacts.contact_id = '".$in['contact_id']."' ORDER BY date DESC {$limit} ");
            if($comments2->next()){
                // console::log('second');
                $in['has_activities'] = true;
            }
        }
    }
    return $in['has_activities'];
}
function payment_method_dd($selected){
    $array = array(
        array('id' => "0" , 'name' => gm("Select")),
        array('id' => "1" , 'name' => gm("Cash")),
        array('id' => "2" , 'name' => gm("Bank transfer")),
        array('id' => "3" , 'name' => gm("Credit Card")),
        array('id' => "4" , 'name' => gm("Cheque")),
    );
    return $array;    
}

function get_vat_id($value){
    $db= new sqldb();
    $vat = $db->field("SELECT vat_id FROM vats WHERE value='".number_format($value, 2, '.', '')."'");
    return $vat;
}
function get_article_vat($id){
    $db= new sqldb();
    $vat_id = $db->field("SELECT vat_id FROM pim_articles WHERE article_id='".$id."' ");
    return get_vat_val($vat_id);
}

function get_vat_val($id){
    $db= new sqldb();
    $vat = $db->field("SELECT value FROM vats WHERE vat_id='".$id."'");
    return $vat;
}

function generate_binvoice_number()
{
    $db = new sqldb();

    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PURCHASE_INVOICE_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PURCHASE_DIGIT_NR' ");
    $prefix_lenght=strlen($invoice_start)+1;
    $db->query("SELECT tblinvoice_incomming.invoice_id,tblinvoice_incomming.booking_number,CAST(SUBSTRING(booking_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( booking_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM tblinvoice_incomming
                    WHERE SUBSTRING(booking_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");
    if(!$digit_nr){
        $digit_nr = 0;
    }
    // console::log($db->f('invoice_prefix'),$invoice_start,$db->f('invoice_number'),$digit_nr);
    if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
        $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}
function generate_binvoice_credit_number()
{
    $db = new sqldb();

    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PURCHASE_CREDIT_INVOICE_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PURCHASE_CREDIT_DIGIT_NR' ");
    $prefix_lenght=strlen($invoice_start)+1;
    $db->query("SELECT tblinvoice_incomming.invoice_id,tblinvoice_incomming.booking_number,CAST(SUBSTRING(booking_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( booking_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM tblinvoice_incomming
                    WHERE SUBSTRING(booking_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");
    if(!$digit_nr){
        $digit_nr = 0;
    }
    // console::log($db->f('invoice_prefix'),$invoice_start,$db->f('invoice_number'),$digit_nr);
    if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
        $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}
function build_incinv_type_dd(){
    
    $array = array(
        array('id' => "0" , 'name' => gm("Invoice")),
        array('id' => "1" , 'name' => gm("Proforma")),
        array('id' => "2" , 'name' => gm("Credit Invoice")),
    );
    return $array;    
}
function twoDecNumber($number,$separator = '.'){
    $number = str_replace(',', '', $number);
    return number_format($number,2,'.','');
    return substr($number, 0, strpos($number,$separator)+3);
}
function build_coda_dd($selected){
    $array = array(
        array('id'=>'00','name'=>'0%'),
        array('id'=>'01','name'=>'6%'),
        array('id'=>'02','name'=>'12%'),
        array('id'=>'03','name'=>'21%'),
        array('id'=>'45','name'=>'0% Contractor'),
        array('id'=>'44','name'=>'0% ICD Services B2B'),
        array('id'=>'NA','name'=>'0% Sundries excluded from VTA'),
        array('id'=>'00/44','name'=>'0% Clause 44'),
        array('id'=>'46/GO','name'=>'0% ICD Goods'),
        array('id'=>'47/TO','name'=>'0% ICD Manufacturing cost'),
        array('id'=>'47/AS','name'=>'0% ICD Assembly'),
        array('id'=>'47/DI','name'=>'0% ICD Distance'),
        array('id'=>'47/SE','name'=>'0% ICD Services'),
        array('id'=>'46/TR','name'=>'0% ICD Triangle a-B-c'),
        array('id'=>'47/EX','name'=>'0% Export non E.U.'),
        array('id'=>'47/EI','name'=>'0% Indirect export'),
        array('id'=>'47/EE','name'=>'0% Export via E.U.'),
        array('id'=>'03/SE','name'=>'0% Standard exchange'));
    return $array;
}
function build_yuki_dd($selected){
    $array = array(
        array('id'=>'100','name'=>'0% VAT exempt'),
        array('id'=>'1','name'=>'High VAT'),
        array('id'=>'2','name'=>'Low VAT'),
        array('id'=>'4','name'=>'VAT 0%'),
        array('id'=>'6','name'=>'0% Exports outside the EU'),
        array('id'=>'7','name'=>'0% Exports within the EU'),
        array('id'=>'8','name'=>'Country Specific Installation / distance sales within the EU'),
        array('id'=>'17','name'=>'0% VAT shifted'),
        array('id'=>'19','name'=>'0% EU exports of goods ( delivery in 2010 )'),
        array('id'=>'20','name'=>'0% EU export services ( services in 2010 )'));
    return $array;
}
function build_match_field_dd_contacts($selected){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb;
    $array = array();
    $db->query("SELECT label_name, import_label_id,custom_field,field_name,group_name FROM import_labels WHERE type = 'contact' ORDER BY import_label_id ASC");
    // $array['9998'] = gm('Contact');
     $out_str=array();
    while($db->move_next()){
        if($db->f('field_name')=='firstname'){
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm('First Name Contact'),
            ));
        }elseif($db->f('field_name')=='lastname'){
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm('Last Name Contact'),
            ));
        }
        elseif($db->f('custom_field')==1 || $db->f('field_name')=='various1' || $db->f('field_name')=='various2')
        {
            /*$array[$db->f('import_label_id')] = $db->f('label_name');*/
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name'))
            ));
        }else
        {
            /*$array[$db->f('import_label_id')] = gm($db->f('label_name'));*/
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name'))
            ));
        }
    }
    
    return $out_str;
}
function build_match_field_dd_company($selected){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb;
    $array2 = array();
    $db->query("SELECT label_name, import_label_id,custom_field,group_name FROM import_labels WHERE type = 'customer' AND field_name!='firstname' AND label_name!='Last Name'  ORDER BY import_label_id ASC");
    $out_str=array();
    while($db->move_next()){
        if($db->f('custom_field')==1)
        {
            /*$array2[$db->f('import_label_id')] = $db->f('label_name');*/
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name'))
            ));
        }else
        {
            /*$array2[$db->f('import_label_id')] = gm($db->f('label_name'));*/
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name'))
            ));
        }
    }


      return $out_str;
}
function build_match_field_dd_article($selected){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $lng_arr= array('1'=>array('13','14','15'),'2'=>array('16','17','18'),'3'=>array('19','20','21'),'4'=>array('22','23','24'));
    $db = new sqldb;
    $db2 = new sqldb;
    $array2 = array();
    $db->query("SELECT label_name, import_label_id,custom_field,group_name,is_language_field FROM import_labels_articles  ORDER BY import_label_id ASC");
    $default_lang = $db2->query("SELECT lang_id FROM pim_lang WHERE  active='1'")->getAll();
    $default_lang2=array();
    foreach ($default_lang as $key => $value) {
        $default_lang2[]=$value['lang_id'];
    }
    $out_str=array();
    while($db->move_next()){
        if(in_array($db->f('is_language_field'), $default_lang2) && $db->f('is_language_field')!=0 && in_array($db->f('import_label_id'), $lng_arr[$db->f('is_language_field')]) ){
            
            // if(in_array($db->f('import_label_id'), $lng_arr[$default_lang->f('lang_id')])){
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name'))
            ));
            
        }elseif ($db->f('is_language_field')==0) {
           
            array_push($out_str, array(
                'id'=>$db->f('import_label_id'),
                'group'=>$db->f('group_name'),
                'name'=>gm($db->f('label_name'))
            ));
        }

        /*if($default_lang->f('lang_id')==1){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==2){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==3){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==2){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12  && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==3){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12  && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==2 && $default_lang->f('lang_id')==3){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==2 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==3 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==2 && $default_lang->f('lang_id')==3){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=22 && $db->f('import_label_id')!=23 && $db->f('import_label_id')!=24){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==2 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=19 && $db->f('import_label_id')!=20 && $db->f('import_label_id')!=21){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==3 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=16 && $db->f('import_label_id')!=17 && $db->f('import_label_id')!=18){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==2 && $default_lang->f('lang_id')==3 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 && $db->f('import_label_id')!=13 && $db->f('import_label_id')!=14 && $db->f('import_label_id')!=15){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }elseif($default_lang->f('lang_id')==1 && $default_lang->f('lang_id')==2 && $default_lang->f('lang_id')==3 && $default_lang->f('lang_id')==4){
            if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12){
                if($db->f('custom_field')==1)
                {
                    
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
                }else
                {
                    array_push($out_str, array(
                        'id'=>$db->f('import_label_id'),
                        'group'=>$db->f('group_name'),
                        'name'=>gm($db->f('label_name'))
                    ));
            
                }
            }
        }*/


  
    }


      return $out_str;
}
function build_match_field_dd_article_language($selected){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb;
    $array2 = array();
    $db->query("SELECT label_name, import_label_id,custom_field,group_name FROM import_labels_articles  ORDER BY import_label_id ASC");
    $out_str=array();
    while($db->move_next()){
        if($db->f('import_label_id')!=10 && $db->f('import_label_id')!=11 && $db->f('import_label_id')!=12 )
        {
            if($db->f('custom_field')==1)
            {
                /*$array2[$db->f('import_label_id')] = $db->f('label_name');*/
                array_push($out_str, array(
                    'id'=>$db->f('import_label_id'),
                    'group'=>$db->f('group_name'),
                    'name'=>gm($db->f('label_name'))
                ));
            }else
            {
                /*$array2[$db->f('import_label_id')] = gm($db->f('label_name'));*/
                array_push($out_str, array(
                    'id'=>$db->f('import_label_id'),
                    'group'=>$db->f('group_name'),
                    'name'=>gm($db->f('label_name'))
                ));
            }
        }
    }


      return $out_str;
}
function build_match_field_dd_individual($selected){
    
    $db = new sqldb;
    $array2 = array();
    $db->query("SELECT label_name, import_label_id,custom_field,group_name FROM import_labels WHERE type = 'customer' ORDER BY import_label_id ASC");
    $out_str=array();
    while($db->move_next()){
        if($db->f('custom_field')==1)
        {
            if($db->f('label_name')!='Name'){
                array_push($out_str, array(
                    'id'=>$db->f('import_label_id'),
                    'group'=>$db->f('group_name'),
                    'name'=>gm($db->f('label_name'))
                ));
            }
        }else
        {
            if($db->f('label_name')!='Name'){
                array_push($out_str, array(
                    'id'=>$db->f('import_label_id'),
                    'group'=>$db->f('group_name'),
                    'name'=>gm($db->f('label_name'))
                ));
            }
        }
    }
     return $out_str;
}

function build_simple_dropdown_sorted($opt, $selected=0,$view=0)
{
    $out_str = '';
    if($view==1){
    if($opt)
    while (list ($key, $val) = each ($opt))
    {   if($key=='9998'||$key=='9999'){
            $out_str.="<option value=\"".$key."\" ";//options values
            $out_str.=($key==$selected?" SELECTED ":"");//if selected
            $out_str.=">".$val."</option>";//options names
        }else{
            $out_str.="<option value=\"".$key."\" ";//options values
            $out_str.=($key==$selected?" SELECTED ":"");//if selected
            $out_str.=">".$val."</option>";//options names
        }
    }
  }else{
    if($opt)
        while (list ($key, $val) = each ($opt))
        {
            array_push($out_str, array('id'=>$key,'value'=>$val));
        }
  }
    return $out_str;
}
function build_match_field_dd_invoice($selected){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb;
    $array2 = array();
    $db->query("SELECT label_name, import_label_id,custom_field,group_name FROM import_labels_invoice  ORDER BY import_label_id ASC");
    $out_str=array();
    while($db->move_next()){
            if($db->f('custom_field')==1)
            {
                /*$array2[$db->f('import_label_id')] = $db->f('label_name');*/
                array_push($out_str, array(
                    'id'=>$db->f('import_label_id'),
                    'group'=>$db->f('group_name'),
                    'name'=>gm($db->f('label_name'))
                ));
            }else
            {
                /*$array2[$db->f('import_label_id')] = gm($db->f('label_name'));*/
                array_push($out_str, array(
                    'id'=>$db->f('import_label_id'),
                    'group'=>$db->f('group_name'),
                    'name'=>gm($db->f('label_name'))
                ));
            }
        
    }


      return $out_str;
}
function build_match_field_dd_invoice_line($selected){
    global $database_config;
    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db = new sqldb;
    $array2 = array();
    $db->query("SELECT name,field_id,field FROM tblinvoice_fields WHERE type=2 ");
    $out_str=array();
    while($db->move_next()){
            array_push($out_str, array(
                'id'=>$db->f('field_id'),
                'name'=>$db->f('name')
            ));

    }


      return $out_str;
}

function build_budget_type_list($selected,$hide='',$dd = true){
    $array = array('1' => gm('No budget'),'2' => gm('Total project hours'),'3' => gm('Total project fees'),'4' => gm('Hours per task'),'5' => gm('Hours per person'));
    if($dd === false){

        return $array[$selected];
    }
    if($hide!=''){
        unset($array[$hide]);
    }
    $new_array=array();
    foreach($array as $key => $value){
        array_push($new_array,array('id'=>$key,'value'=>$value));
    } 
    return $new_array;
}
function build_budget_type_list_daily($selected,$hide='',$dd = true){
    $array = array('1' => gm('No budget'),'2' => gm('Total project days'),'3' => gm('Total project fees'),'4' => gm('Days per task'),'5' => gm('Days per staff'));
    if($dd === false){

        return $array[$selected];
    }
    if($hide!=''){
        unset($array[$hide]);
    }
    $new_array=array();
    foreach($array as $key => $value){
        array_push($new_array,array('id'=>$key,'value'=>$value));
    } 
    return $new_array;
}
function generate_project_number($database)
{
    global $database_config;
        $db_config = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database,
        );

    $db = new sqldb($db_config);
    $invoice_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROJECT_START' ");
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_PROJECT_DIGIT_NR' ");
    $prefix_lenght=strlen($invoice_start)+1;
    $db->query("SELECT serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM projects
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($invoice_start)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");

    if($db->move_next() && $db->f('invoice_prefix')==$invoice_start){
        $next_serial=$invoice_start.str_pad($db->f('invoice_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $next_serial=$invoice_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}
function build_timesheet_week_from_project($project_id){
    $start = microtime(true);
    $db = new sqldb();
    $weeks = array();
    $dates  = $db->query("SELECT `date` FROM `task_time` WHERE `project_id`='".$project_id."' AND approved='1' AND billed='0' ");
    while ($dates->next()) {
        $week  = date('W',$dates->f('date'));
        if(!array_key_exists($week, $weeks)){
            $now                = $dates->f('date');
            $today_of_week      = date("N", $now);
            $month              = date('n',$now);
            $year               = date('Y',$now);
            $week_start             = mktime(0,0,0,$month,(date('j',$now)-$today_of_week+1),$year);
            $week_end               = $week_start + 604799;
            $weeks[$week] = array($week_start,$week_end);
        }
    }
    // console::log($weeks);
    return $weeks;

}
function build_custom_hours_list($selected=1)
{
    $array=array('1'=>gm('This Month'),'2'=>gm('Last Month'),'3'=>gm('Custom'));

    return build_simple_dropdown($array,$selected,$hide);
}
function build_task_list($selected,$customer_id)
{
    $db=new sqldb;
    $i = 0;
    $is_contact = $db->field("SELECT is_contact FROM projects WHERE project_id='".$selected."' ");
    if(!$is_contact){
        $projects = $db->query("SELECT projects.project_id, customers.is_admin AS is_admin
                FROM   projects
                INNER  JOIN customers ON customers.customer_id=projects.customer_id
                WHERE  projects.customer_id='".$customer_id."' AND project_id='".$selected."' AND customers.is_admin!=1 ");
    }else{
        $projects = $db->query("SELECT projects.project_id FROM projects
                INNER JOIN customer_contacts ON projects.customer_id = customer_contacts.contact_id
                WHERE  projects.customer_id='".$customer_id."' AND project_id='".$selected."' ");
    }

    $array=array();
    while ($projects->next()) {
        $tasks = $db->query("SELECT task_id, task_name FROM tasks WHERE project_id='".$projects->f('project_id')."' AND closed='1' AND billable='1' ");
        while ($tasks->next()) {
            array_push($array,array('id'=>$tasks->f('task_id'),'value'=>$tasks->f('task_name')));
            //$out_str .= "<input type=\"checkbox\" name=\"tasks_id[]\" value=\"".$tasks->f('task_id')."\" style=\"width: auto; vertical-align: middle;\"><label style=\"font-weight: normal\">".$tasks->f('task_name')."</label><br />";
            $i++;
        }
    }

    if($i == 0){

        msg::notice(gm('No tasks to be invoiced'),"notice");

    }

    return $array;
}
function build_purchase_list($selected,$customer_id)
{
    $db=new sqldb;
    $i = 0;
    $projects = $db->query("SELECT project_purchase.project_purchase_id, project_purchase.description, customers.is_admin AS is_admin
                FROM project_purchase
                INNER JOIN projects ON project_purchase.project_id=projects.project_id
                LEFT JOIN customers ON customers.customer_id=project_purchase.supplier_id AND projects.customer_id='".$customer_id."' AND customers.is_admin!=1
                WHERE project_purchase.delivered='1' AND project_purchase.billable='1' AND project_purchase.invoiced != '1' AND project_purchase.project_id='".$selected."'");
    $array=array();
    while ($projects->next()) {
        array_push($array,array('id'=>$projects->f('project_purchase_id'),'value'=>$projects->f('description')));
        //$out_str .= "<div><input type=\"checkbox\" name=\"project_purchases_id[]\" value=\"".$projects->f('project_purchase_id')."\" style=\"width: auto; float:left; vertical-align: middle;\"><label style=\"font-weight: normal\">".$projects->f('description')."</label></div><br />";
    $i++;
    }
    if($i == 0){

        msg::notice(gm('No purchase to be invoiced'),"notice");

    }

    return $array;
}

function build_article_list($selected,$customer_id)
{
    $db=new sqldb;
    $i = 0;
    $projects = $db->query("SELECT project_articles.a_id,project_articles.name FROM project_articles
            INNER JOIN projects ON project_articles.project_id=projects.project_id
            WHERE project_articles.project_id='".$selected."' AND projects.invoice_method>0 AND project_articles.delivered='1' AND project_articles.billed='0' ");

    $array=array();
    while ($projects->next()) {
        array_push($array,array('id'=>$projects->f('a_id'),'value'=>$projects->f('name')));
        //$out_str .= "<input type=\"checkbox\" name=\"project_a_id[]\" value=\"".$projects->f('a_id')."\" style=\"width: auto; float:left; vertical-align: middle;\"><label style=\"font-weight: normal\">".$projects->f('name')."</label><br />";
    $i++;
    }
    if($i == 0){

        msg::notice(gm('No article to be invoiced'),"notice");

    }

    return $array;
}
function projectHourReport($p)
{
    if(!is_object($p)){
        return;
    }
    if(get_class($p) != 'sqldbResult'){
        return;
    }
    $pbt = $p->f('billable_type');
    $pId = $p->f('project_id');
    if(!$pId){
        return;
    }
    $db = new sqldb();
    global $database_config;

    $database_users = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $dbs = new sqldb($database_users);
    $result = array('user_cost'=>0,'teoreticalA'=>0,'invoicedA'=>0);
    $ph = $db->query("SELECT SUM( hours ) as h , user_id FROM  `task_time` WHERE project_id='".$pId."' GROUP BY user_id ");
    while ($ph->next()) {
        if($ph->f('h')){
            //$uCost = $dbs->field("SELECT h_cost FROM users WHERE user_id='".$ph->f('user_id')."' ");
            if($p->f('status_rate')==0){
                $uCost = $db->field("SELECT p_hourly_cost FROM project_user WHERE user_id='".$ph->f('user_id')."' AND project_id='".$pId."'");
            }else{
                $uCost = $db->field("SELECT p_daily_cost FROM project_user WHERE user_id='".$ph->f('user_id')."' AND project_id='".$pId."'");
            }
            $result['user_cost'] += $ph->f('h') * $uCost;
        }
    }

    $result['total_purchases_cost'] = $db->field("SELECT SUM(unit_price) FROM `project_purchase` WHERE project_id='".$pId."' ");
    $result['teo_puchases_in'] = $db->field("SELECT SUM(unit_price+ (unit_price*margin/100) ) FROM `project_purchase` WHERE project_id='".$pId."' AND billable='1' ");
    $result['puchases_in'] = $db->field("SELECT SUM(unit_price+ (unit_price*margin/100) ) FROM `project_purchase` WHERE project_id='".$pId."' AND billable='1' AND invoiced='1' ");
    $result['exp_can_bill'] = 0;
    $result['exp_bill'] = 0;
    $result['exp_cost'] = 0;
    $exp_can_bill = $db->query("SELECT SUM( amount ) AS am, expense.unit_price, expense.expense_id
                                        FROM  `project_expenses`
                                        INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
                                        LEFT JOIN project_expences ON project_expenses.project_id = project_expences.project_id AND project_expenses.expense_id = project_expences.expense_id
                                        WHERE  `project_expenses`.`project_id` ='".$pId."' AND  `project_expences`.`project_id` IS NOT NULL
                                        GROUP BY project_expenses.expense_id");
    while ($exp_can_bill->next()) {
        $result['exp_can_bill'] += $exp_can_bill->f('unit_price') ? $exp_can_bill->f('unit_price')*$exp_can_bill->f('am') : $exp_can_bill->f('am');
    }
    $exp_bill = $db->query("SELECT SUM( amount ) AS am, expense.unit_price, expense.expense_id
                                        FROM  `project_expenses`
                                        INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
                                        LEFT JOIN project_expences ON project_expenses.project_id = project_expences.project_id AND project_expenses.expense_id = project_expences.expense_id
                                        WHERE  `project_expenses`.`project_id` ='".$pId."' AND billed='1' AND  `project_expences`.`project_id` IS NOT NULL
                                        GROUP BY project_expenses.expense_id");
    while ($exp_bill->next()) {
        $result['exp_bill'] += $exp_bill->f('unit_price') ? $exp_bill->f('unit_price')*$exp_bill->f('am') : $exp_bill->f('am');
    }
    $exp_cost = $db->query("SELECT SUM( amount ) AS am, expense.unit_price, expense.expense_id
                                        FROM  `project_expenses`
                                        INNER JOIN expense ON project_expenses.expense_id=expense.expense_id
                                        LEFT JOIN project_expences ON project_expenses.project_id = project_expences.project_id AND project_expenses.expense_id = project_expences.expense_id
                                        WHERE  `project_expenses`.`project_id` ='".$pId."'
                                        GROUP BY project_expenses.expense_id");
    while ($exp_cost->next()) {
        $result['exp_cost'] += $exp_cost->f('unit_price') ? $exp_cost->f('unit_price')*$exp_cost->f('am') : $exp_cost->f('am');
    }

    switch ($pbt) {
        case 1:
        case 7:
            $t = $db->query("SELECT SUM( hours ) as h, task_id FROM task_time WHERE project_id='".$pId."' AND billable='1' GROUP BY task_id ");
            while ($t->next()) {
                if($t->f('h')){
                    if($p->f('status_rate')==0){
                        $tCost = $db->field("SELECT t_h_rate FROM tasks WHERE task_id='".$t->f('task_id')."' AND project_id='".$pId."' ");
                    }else{
                        $tCost = $db->field("SELECT t_daily_rate FROM tasks WHERE task_id='".$t->f('task_id')."' AND project_id='".$pId."' ");
                    }
                    $result['teoreticalA'] += $tCost * $t->f('h');
                }
            }
            $t = $db->query("SELECT SUM( hours ) as h, task_id FROM task_time WHERE project_id='".$pId."' AND billable='1' AND billed='1' GROUP BY task_id ");
            while ($t->next()) {
                if($t->f('h')){
                    if($p->f('status_rate')==0){
                        $tCost = $db->field("SELECT t_h_rate FROM tasks WHERE task_id='".$t->f('task_id')."' AND project_id='".$pId."' ");
                    }else{
                        $tCost = $db->field("SELECT t_daily_rate FROM tasks WHERE task_id='".$t->f('task_id')."' AND project_id='".$pId."' ");
                    }
                    $result['invoicedA'] += $tCost * $t->f('h');
                }
            }
            break;
        case 2:
            $t = $db->query("SELECT SUM( hours ) as h, user_id FROM task_time WHERE project_id='".$pId."' AND billable='1' GROUP BY user_id ");
            while ($t->next()) {
                if($t->f('h')){
                    if($p->f('status_rate')==0){
                        $tCost = $db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$t->f('user_id')."' AND project_id='".$pId."' ");
                    }else{
                        $tCost = $db->field("SELECT p_daily_rate FROM project_user WHERE user_id='".$t->f('user_id')."' AND project_id='".$pId."' ");
                    }
                    $result['teoreticalA'] += $tCost * $t->f('h');
                }
            }
            $t = $db->query("SELECT SUM( hours ) as h, user_id FROM task_time WHERE project_id='".$pId."' AND billable='1' AND billed='1' GROUP BY user_id ");
            while ($t->next()) {
                if($t->f('h')){
                    if($p->f('status_rate')==0){
                        $tCost = $db->field("SELECT p_h_rate FROM project_user WHERE user_id='".$t->f('user_id')."' AND project_id='".$pId."' ");
                    }else{
                        $tCost = $db->field("SELECT p_daily_rate FROM project_user WHERE user_id='".$t->f('user_id')."' AND project_id='".$pId."' ");
                    }
                    $result['invoicedA'] += $tCost * $t->f('h');
                }
            }
            break;
        case 3:
            $t = $db->field("SELECT SUM( hours ) as h FROM task_time WHERE project_id='".$pId."' AND billable='1' GROUP BY user_id ");
            $tCost = $db->field("SELECT pr_h_rate FROM projects WHERE project_id='".$pId."' ");
            $result['teoreticalA'] += $tCost * $t;
            $t = $db->field("SELECT SUM( hours ) as h FROM task_time WHERE project_id='".$pId."' AND billable='1' AND billed='1' GROUP BY user_id ");
            $tCost = $db->field("SELECT pr_h_rate FROM projects WHERE project_id='".$pId."' ");
            $result['invoicedA'] += $tCost * $t;
            break;
        case 5:
            $t = $db->field("SELECT SUM( task_budget ) as h FROM tasks WHERE project_id='".$pId."' AND billable='1' ");
            $result['teoreticalA'] += $t;
            $t = $db->field("SELECT SUM( task_budget ) as h FROM tasks WHERE project_id='".$pId."' AND billable='1' AND closed='2' ");
            $result['invoicedA'] += $t;
            break;
        case 6:
            $rate = $p->f('retainer_budget');
            $startDate = new DateTime(date(DATE_W3C,$p->f('start_date')));
            $now = new DateTime();
            $diff = $startDate->diff($now)->format('%m');
            $result['teoreticalA'] += $rate * $diff;
            $next = new DateTime(date(DATE_W3C,$p->f('next_date')));
            $diff = $startDate->diff($next)->format('%m');
            $result['invoicedA'] += $rate * $diff;
        default:
            # I don't know what I'm doing here
            break;
    }

    return $result;
}
function get_billable_amount($customer_id, $start, $end, $project = 0, $task = 0,$user = 0 ){
    if($project == 0){
        $and = ' AND 1=1 ';
    }
    else {
        if($task == 0){
            $and_task = ' AND 1=1 ';
        }
        else {
            $and_task = " AND task_id='".$task."'";
        }

        if($user == 0){
            $and_user = ' AND 1=1 ';
        }
        else{
            $and_user = " AND user_id='".$user."'";
        }
        $and = " AND project_id='".$project."' ";
    }
    $filter_date = " date BETWEEN '".$start."' AND '".$end."' ";
    if(!$start && !$end){
        $filter_date = " 1=1 ";
    }

    $db = new sqldb();
    $db2 = new sqldb();
    $db3 = new sqldb();
    $db4 = new sqldb();
    $total_amount = 0;
    // select all project for the customer
    $db->query("SELECT project_id, billable_type, pr_h_rate, status_rate FROM projects WHERE customer_id='".$customer_id."' $and ");
    while($db->move_next()){
        // select all blillable tasks for the customers projects
        $db2->query("SELECT task_id, t_h_rate, t_daily_rate, task_budget FROM tasks WHERE project_id='".$db->f('project_id')."' AND billable='1' $and_task ");
        while($db2->move_next()){
            // sums the hours for the users for the customers projects tasks
            $db3->query("SELECT user_id, SUM(hours) AS total_h FROM task_time WHERE customer_id='".$customer_id."'  AND project_id='".$db->f('project_id')."' AND task_id='".$db2->f('task_id')."' AND $filter_date $and_user GROUP BY user_id ");
            while ($db3->move_next()) {
                $total_h = $db3->f('total_h');
                //console::log($total_h,$db->f('billable_type'));
                // gets the rate for the specific taks
                switch ($db->f('billable_type')){
                    case '1':
                    case '7':
                        if($db->f('status_rate')==0){
                            $db4->query("SELECT t_h_rate FROM tasks WHERE task_id='".$db2->f('task_id')."' AND project_id='".$db->f('project_id')."' ");
                            $db4->move_next();
                            $rate = $db4->f('t_h_rate');
                        }else{
                            $db4->query("SELECT t_daily_rate FROM tasks WHERE task_id='".$db2->f('task_id')."' AND project_id='".$db->f('project_id')."' ");
                            $db4->move_next();
                            $rate = $db4->f('t_daily_rate');
                        }
                        break;
                    case '2':
                        if($db->f('status_rate')==0){
                            $db4->query("SELECT p_h_rate FROM project_user WHERE user_id='".$db3->f('user_id')."' AND project_id='".$db->f('project_id')."' ");
                            $db4->move_next();
                            $rate = $db4->f('p_h_rate');
                        }else{
                            $db4->query("SELECT p_daily_rate FROM project_user WHERE user_id='".$db3->f('user_id')."' AND project_id='".$db->f('project_id')."' ");
                            $db4->move_next();
                            $rate = $db4->f('p_daily_rate');
                        }
                        break;
                    case '3':
                        $rate = $db->f('pr_h_rate');
                        break;
                    case '4':
                        $rate = 0;
                        break;
                    case '5':
                        if($user == 0){
                            if($db->f('status_rate')==0){
                                $db4->query("SELECT p_h_rate FROM project_user WHERE user_id='".$db3->f('user_id')."' AND project_id='".$db->f('project_id')."' ");
                                $db4->move_next();
                                $rate = $db4->f('p_h_rate');
                            }else{
                                uery("SELECT p_daily_rate FROM project_user WHERE user_id='".$db3->f('user_id')."' AND project_id='".$db->f('project_id')."' ");
                                $db4->move_next();
                                $rate = $db4->f('p_daily_rate');
                            }
                        }else{
                            $rate = 1;
                            $total_h = $db2->f('task_budget');
                        }
                        break;
                }
                $total_amount += $total_h*$rate;
            }
        }
    }
    return $total_amount;
}
function billableAmountToInvoice($filter,$filter_task){
    $db = new sqldb();
    $total_amount = 0;
    $array = array();
    $tasks = $db->query("SELECT task_time.user_id, task_time.task_id, task_time.project_status_rate, task_time.customer_id, task_time.project_id, task_time.approved, tasks.t_h_rate, tasks.t_daily_rate, project_user.p_h_rate, project_user.p_daily_rate, tasks.task_budget,
                                projects.billable_type, projects.pr_h_rate,tasks.task_name as t_name, projects.company_name as c_name, projects.name as p_name,task_time.hours
                        FROM task_time
                        INNER JOIN projects on task_time.project_id = projects.project_id
                        INNER JOIN project_user ON task_time.project_id=project_user.project_id AND task_time.user_id=project_user.user_id
                        INNER JOIN tasks ON task_time.task_id = tasks.task_id AND task_time.project_id = tasks.project_id
                        WHERE $filter AND projects.invoice_method>0 AND projects.active!='0' ");


    while ($tasks->next()) {

        $total_h = 0;
        $total_h_approved = 0;
        $total_h = $tasks->f('hours');
        if($tasks->f('approved') == 1){
            $total_h_approved = $tasks->f('hours');
        }
        if($tasks->f('billable_type') == '5'){
            $total_h = 0;
            $total_h_approved = 0;
        }
        switch ($tasks->f('billable_type')) {
            case '1':
            case '7':
                if($tasks->f('project_status_rate')==0){
                    $rate = $tasks->f('t_h_rate');
                }else{
                    $rate = $tasks->f('t_daily_rate');
                }
                break;
            case '2':
                if($tasks->f('project_status_rate')==0){
                    $rate = $tasks->f('p_h_rate');
                }else{
                    $rate = $tasks->f('p_daily_rate');
                }
                break;
            case '3':
                $rate = $tasks->f('pr_h_rate');
                break;
            case '4':
                $rate = 0;
                break;
            case '5':
                # here we only invoice the hours not the bugdet of the tasks
                $rate = 0;
                $total_h= 0;
                // $rate = 1;
                // $total_h = $tasks->f('task_budget');
                break;
            default:
                $rate = 0;
                break;
        }
        $total_amount += $total_h*$rate;
        #creating the data for the fake invoices
        if(!array_key_exists($tasks->f('customer_id'), $array)){
            $array[$tasks->f('customer_id')] = array();
        }
        if(!array_key_exists($tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id'), $array[$tasks->f('customer_id')])) {
            $detail = array('name'          => $tasks->f('c_name'),
                            'project_id'        => $tasks->f('project_id'),
                            'description'       => $tasks->f('p_name').": ".$tasks->f('t_name')." ",
                            'quantity'          => $total_h,
                            'unit_price'        => $rate,
                            'project_name'      => $tasks->f('p_name'),
                            'task_name'         => $tasks->f('t_name'),
                            'task_approved'     => $total_h_approved ? 1 : $tasks->f('approved'),
                            'quantity_approved' => $total_h_approved,
                            );
            $array[$tasks->f('customer_id')][$tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id')] = $detail;
        }else{
            $array[$tasks->f('customer_id')][$tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id')]['quantity']+=$total_h;
            $array[$tasks->f('customer_id')][$tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id')]['quantity_approved']+=$total_h_approved;
        }
        // array_push($array[$tasks->f('customer_id')], $detail);
    }

    $tasks_budget = $db->query("SELECT tasks.*,projects.customer_id,projects.company_name as c_name,projects.name as p_name FROM tasks
        LEFT JOIN projects ON tasks.project_id=projects.project_id
        WHERE projects.billable_type='5' AND tasks.billable='1' AND tasks.closed='1' AND projects.active>'0' AND projects.stage>'0' $filter_task ");
    while($tasks_budget->next()){
        $total_amount += $tasks_budget->f('task_budget');
        if(!array_key_exists($tasks_budget->f('customer_id'), $array)){
            $array[$tasks_budget->f('customer_id')] = array();
        }
        if(!array_key_exists($tasks_budget->f('project_id').'-'.$tasks->f('task_id').'-0', $array[$tasks_budget->f('customer_id')])) {
            $detail = array('name'          => $tasks_budget->f('c_name'),
                            'project_id'        => $tasks_budget->f('project_id'),
                            'description'       => $tasks_budget->f('p_name').": ".$tasks_budget->f('task_name')." ",
                            'quantity'          => 1,
                            'unit_price'        => $tasks_budget->f('task_budget'),
                            'project_name'      => $tasks_budget->f('p_name'),
                            'task_name'         => $tasks_budget->f('task_name'),
                            'task_approved'         => 1,
                            'quantity_approved' => 1,
                            );
            $array[$tasks_budget->f('customer_id')][$tasks_budget->f('project_id').'-'.$tasks_budget->f('task_id').'-0'] = $detail;
        }else{
            $array[$tasks_budget->f('customer_id')][$tasks_budget->f('project_id').'-'.$tasks_budget->f('task_id').'-0']['quantity']=1;
            $array[$tasks_budget->f('customer_id')][$tasks_budget->f('project_id').'-'.$tasks_budget->f('task_id').'-0']['quantity_approved']=1;
        }
    }


    $result = array('total' => $total_amount, 'details' => $array);
    return $result;
}
function get_label_txt($label,$language_id)
{
    $db=new sqldb();
    if($language_id<=4) {
        $db->query("SELECT * FROM label_language WHERE label_language_id = '".$language_id."' ");
    } else {
        $db->query("SELECT * FROM label_language WHERE lang_code ='".$language_id."'");
    }
    $db->move_next();
    if($db->f($label)){
        $label_val=$db->f($label);
    }else{
        $label_val='';
    }
    return $label_val;
}
function build_user_dd_timesheet($selected, $access){
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $array = array();
    $db = new sqldb($db_config);
    $db2 = new sqldb();
    $array = array();

    $db->query("SELECT user_id, first_name, last_name,group_id FROM users WHERE database_name='".DATABASE_NAME."' AND user_id='".$_SESSION['u_id']."'  ");
    $db->move_next();
    $array[$db->f('user_id')] = utf8_encode($db->f('first_name'))." ".utf8_encode($db->f('last_name'));
    $filter = ' 1=1 ';
    $group_id = $db->f('group_id');
    if($group_id != 2){
        if($access > 1  ){
            $filter = " user_role > '".$_SESSION['access_level']."' OR user_id='".$selected."' ";
        }
        $is_manager = $db2->field("SELECT project_id FROM project_user WHERE manager='1' AND user_id='".$_SESSION['u_id']."' ");

        if($_SESSION['group'] != 'admin' && !empty($is_manager)){
            $u = '';
            $users = $db2->query("SELECT user_id FROM  `project_user` WHERE project_id IN
                                    ( SELECT project_id FROM project_user WHERE manager =1 AND user_id ='".$_SESSION['u_id']."' )
                                  GROUP BY user_id ");
            while ($users->next()) {
                $u .= $users->f('user_id').',';
            }
            $filter =" user_id IN ( ".rtrim($u,',')." ) ";
        }
        $db->query("SELECT user_id, first_name, last_name FROM users WHERE database_name='".DATABASE_NAME."' AND $filter AND active='1' ");
        while ($db->move_next()) {
            $array[$db->f('user_id')] = utf8_encode($db->f('first_name'))." ".utf8_encode($db->f('last_name'));
        }
    }

    asort($array);
    $out=array();
    foreach($array as $key => $value){
        array_push($out, array('id'=>$key,'name'=>$value));
    }
    return $out;
}
function build_how_many_dropdown($selected=0){
    $sell=array(
    '0' => gm('Personal'),
    '1' => gm('Startup'),
    '2' => gm('Small'),
    '3' => gm('Business'),
    '4' => gm('Entreprise'),
    );
    $out=array();
    foreach($sell as $key => $value){
        array_push($out, array('id'=>$key,'name'=>$value));
    }
    return $out;
}
function build_period_dropdown($selected=1){
    $sell=array(
    '1' => gm('Monthly'),
    '2' => gm('Yearly'),
    );
    $out=array();
    foreach($sell as $key => $value){
        array_push($out, array('id'=>$key,'name'=>$value));
    }
    return $out;
}
function build_you_sell_dropdown($selected=1){
    $sell=array(
    // '1' => gm('Free'),
    '5' => gm('Services'),
    '4' => gm('Products'),
    '3' => gm('Products & Services'),
    );
    $out=array();
    foreach($sell as $key => $value){
        array_push($out, array('id'=>$key,'name'=>$value));
    }
    return $out;
}
function send_attention_mail($user_id)
{
    global $database_config;
    $mail = new PHPMailer();
    $mail->WordWrap = 50;
    $database_2 = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $db_user= new sqldb($database_2);
    $fromMail='support@akti.com';
    $user_info = $db_user->query("SELECT * FROM users WHERE user_id='".$user_id."' ");
    if($user_info->next()){
        $email = $user_info->f('email');
        switch ($user_info->f('lang_id')) {
            case '1':
                $body='Hallo,

Uw kredietkaart voor uw <a href="app.akti.com">Akti</a> abonnement kon niet verwerkt worden.
We blijven je kaart opnieuw te proberen in de komende
dagen. Wij begrijpen dat creditcards verlopen, dus geven we je een paar extra dagen om alles in orde te brengen.

Als uw kaart is vervallen, kunt u inloggen op uw<a href="app.akti.com">Akti</a> account
en werk deze zo spoedig mogelijk bij.

Met vriendelijke groet,

Het Akti Team';

                $subject = 'Akti – Maandelijkse betaling geweigerd';
                break;
            case '2':
                $body='Hi,

Your credit card for your <a href="app.akti.com">Akti</a> subscription failed to
process recently. We\'ll continue to try your card again over the next few
days. We understand that credit cards expire, so we\'re
glad to give you  a few extra days to straighten things out.

If your card has expired, please login to your <a href="app.akti.com">Akti</a> account
and update it as soon as possible.

Regards,

The Akti Team';
                $subject = 'Akti - Recurring Payment Declined';
                break;
            case '3':
                $body='Bonjour,

Votre carte de crédit pour votre abonnement <a href="app.akti.com">Akti</a> n’a pas pu être traitée.
Nous allons réessayer de traiter votre paiement les prochains jours.
Nous comprenons que les cartes de crédit viennent à échéance, et nous vous donnons donc quelques jours supplémentaires pour arranger cela.

Si votre carte a expiré, veuillez vous connecter à votre compte <a href="app.akti.com">Akti</a> et mettez-le à jour dès que possible.

Cordialement,

L\'équipe Akti';
                $subject = 'Akti – Paiement mensuel refusé';
                break;

            default:
                $body='Hi,

Your credit card for your <a href="app.akti.com">Akti</a> subscription failed to
process recently. We\'ll continue to try your card again over the next few
days. We understand that credit cards expire, so we\'re
glad to give you  a few extra days to straighten things out.

If your card has expired, please login to your <a href="app.akti.com">Akti</a> account
and update it as soon as possible.

Regards,

The Akti Team';
                $subject = 'Akti - Recurring Payment Declined';
                break;
        }
        $xxx = '<table style="background:#f6f6f6; width:100%; margin:0; padding:0; " border="0" cellspacing="0" cellpadding="50" >
                <tbody>
                    <tr ><td style="border-collapse:collapse; " >
                        <table style="margin:0 auto; padding:0; background:#FFFFFF;" width="600" border="0" cellspacing="0" cellpadding="0" >
                            <tbody>
                                <tr style="background:#FFF;"><td style="padding: 0px;"><img src="http://www.akti.com/images/logo_newsletter.png"></td></tr>
                                <tr style=""><td style="width=100%; padding:20px; border-bottom:1px solid #dcdcdc; border-right:1px solid #dcdcdc; border-left:1px solid #dcdcdc; border-collapse: collapse;">'.nl2br($body).'
                                </td></tr>
                            </tbody>
                        </table>
                    </tr></td>
                </tbody>
            </table>';
        $body = $xxx;
        $body = str_replace("\n", '', $body);
        $body = str_replace("\t", '', $body);

        $mail->Subject = $subject;

        $mail->SetFrom($fromMail, $fromMail);

        $mail->MsgHTML(nl2br($body));

        $mail->AddAddress($email);

        $mail->Send();
    }
    return true;

}

function build_s_n_status_dd($selected){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT serial_number_status.* FROM serial_number_status ");
    while($db->move_next()){
        $array[$db->f('id')] = gm($db->f('name'));
    }
    return build_simple_dropdown($array,$selected);

}
function build_batch_nr_status_dd($selected){
    $db = new sqldb();
    $array = array();
    $db->query("SELECT batch_number_status.* FROM batch_number_status ");
    while($db->move_next()){
        $array[$db->f('id')] = $db->f('name');
    }
    return build_simple_dropdown($array,$selected);
}
function getpo_label_txt($label,$language_id)
{
    $db=new sqldb();
    if($language_id<=4) {
        $db->query("SELECT * FROM label_language_p_order WHERE label_language_id='".$language_id."'");
    } else {
        $db->query("SELECT * FROM label_language_p_order WHERE lang_code = '".$language_id."'");
    }

    $db->move_next();
    $label_val = $db->f($label) ? $db->f($label) : '';
    return $label_val;
}

function get_customer_name($id){
    $db = new sqldb();
    return $db->field("SELECT name FROM customers WHERE customer_id='".$id."' ");
    // $db->next();
    // return $db->f('name');
}
function get_contact_first_and_name($id){
    $db = new sqldb();
    return $db->field("SELECT CONCAT_WS(' ',lastname, firstname) FROM customer_contacts WHERE contact_id='".$id."' ");
    // $db->next();
    // return $db->f('firstname').' '.$db->f('lastname');
}

function strtotimeActivity($str){
    $tmstmp = strtotime($str);
    $offset = 2; // we set the timezone to europe/bucharest;
    $hour = 3600;
    $tmstmp = $tmstmp-2*$hour;
    if(defined('ACCOUNT_TIME_ZONE')){
        // console::log('erere',ACCOUNT_TIME_ZONE);
        $tmstmp = $tmstmp + (ACCOUNT_TIME_ZONE*$hour);
    }
    return $tmstmp;
}
function build_timesheet_customer_dd($selected='',$time_start,$time_end,$user_id)
{
    $db=new sqldb();
    $array = array();
    $db->query("SELECT task_time.*, projects.company_name AS c_name, projects.customer_id
            FROM task_time
            INNER JOIN projects ON task_time.project_id=projects.project_id
            WHERE task_time.user_id='".$user_id."' AND task_time.date BETWEEN '".$time_start."' AND '".$time_end."' AND hours!='0'
            GROUP BY c_name
            ORDER BY task_time.date
           ");
    while($db->move_next()){
        array_push($array,array('id'=>$db->f('customer_id'), 'name'=>$db->f('c_name')));
    }
    return $array;
}
function gettime_label_txt($label,$language_id)
{
    $db=new sqldb();
    $db->query("SELECT * FROM label_language_time WHERE label_language_id='".$language_id."'");
    $db->move_next();
    if($db->f($label)){
        $label_val=$db->f($label);
    }else{
        $label_val='';
    }
    return $label_val;
}
function build_days_of_the_week($start_week,$selected){

    $k = 0;
    $out = array();
    for ($i = 0; $i<7; $i++){
        $out[$start_week+$k] = date(ACCOUNT_DATE_FORMAT,$start_week+$k);
        $k += 86400;
    }

    return build_simple_dropdown($out,$selected);

}

function updateCustomerPrimaryAddressCoords($address,$customer_id){
    $db = new sqldb();
    if($address->f('address') )
    {
        $country_n = get_country_name($address->f('country_id') );
        if(!$address->f('zip') && $address->f('city'))
        {
            $address = $address->f('address').', '.$address->f('city').', '.$country_n;
        }elseif(!$address->f('city') && $address->f('zip'))
        {
            $address = $address->f('address').', '.$address->f('zip').', '.$country_n;
        }elseif(!$address->f('zip') && !$address->f('city'))
        {
            $address = $address->f('address').', '.$country_n;
        }else
        {
            $address = $address->f('address').', '.$address->f('zip').' '.$address->f('city').', '.$country_n;
        }
        $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
        $output= json_decode($geocode);
        $lat = $output->results[0]->geometry->location->lat;
        $long = $output->results[0]->geometry->location->lng;
        $db->query("INSERT INTO addresses_coord SET location_lat='".$lat."', location_lng='".$long."', customer_id='".$customer_id."'");
        return array('latitude'=>$lat,'longitude'=>$long);
    }
    return array();
   

}
function build_user_dd_calendar($selected,$user_ids='0')
{
    global $database_config;
    $db_config = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database_config['user_db'],
    );
    $array = array();
    $db = new sqldb($db_config);
    $db2 = new sqldb();
    $array = array();

    $db->query("SELECT user_id, first_name, last_name FROM users WHERE database_name='".DATABASE_NAME."' AND user_id='".$_SESSION['u_id']."' ");
    $db->move_next();
    $array[$db->f('user_id')] = utf8_encode($db->f('first_name'))." ".utf8_encode($db->f('last_name'));
    $db->query("SELECT user_id, first_name, last_name FROM users WHERE user_id IN (".$user_ids.")");
    while ($db->move_next()) {
        $array[$db->f('user_id')] = utf8_encode($db->f('first_name'))." ".utf8_encode($db->f('last_name'));
    }
    asort($array);
    $out=array();
    foreach($array as $key => $value){
        array_push($out, array('id'=>$key,'name'=>$value));
    }
    return $out;

}
function geto_label_txt($label,$language_id)
{
    $db=new sqldb();
    if($language_id<=4) {
        $db->query("SELECT * FROM label_language_order WHERE label_language_id='".$language_id."'");
    } else {
        $db->query("SELECT * FROM label_language_order WHERE lang_code = '".$language_id."' ");
    }

    $db->move_next();
    $label_val = $db->f($label) ? $db->f($label) : '';
    return $label_val;
}
function generate_stock_disp_serial_number($database)
{
    global $database_config;

    $db_used = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database,
    );
    $db= new sqldb($db_used);
    $prefix_lenght=strlen(ACCOUNT_STOCK_DISP_START)+1;
    $left = defined('ACCOUNT_DIGIT_NR') ? ACCOUNT_STOCK_DISP_DIGIT_NR : '';
    if(!$left){
        $left=1;
    }
    $db->query("SELECT  pim_stock_disp.stock_disp_id,pim_stock_disp.serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS invoice_number,LEFT( serial_number, $prefix_lenght-1 ) AS invoice_prefix
                    FROM pim_stock_disp
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes(ACCOUNT_STOCK_DISP_START)."'
                    ORDER BY invoice_number DESC
                    LIMIT 1");

    if($db->move_next() && $db->f('invoice_prefix')==ACCOUNT_STOCK_DISP_START){
        $next_serial=ACCOUNT_STOCK_DISP_START.str_pad($db->f('invoice_number')+1,$left,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $order_nr = defined('ACCOUNT_STOCK_DISP_START') ? ACCOUNT_STOCK_DISP_START : '';
        $next_serial= $order_nr.str_pad(1,$left,"0",STR_PAD_LEFT);
        return $next_serial;
    }

}

function build_pdf_language_code_dd($selected='',$active){

    $db = new sqldb();
    $array = array();
    $filter="1=1";
    if($active){
        $filter.=" and active=1";
    }
    $db->query("SELECT * FROM pim_lang where  ".$filter." ");
    while($db->move_next()){
        $array[$db->f('code')] = $db->f('language');
        /*array_push($array,array('id'=>$db->f('code'), 'name'=>$db->f('language')));*/
    }
    return build_simple_dropdown($array,$selected,0);

}

function generate_sepa_number($database)
{
    global $database_config,$cfg;
    $db_conf = array(
        'hostname' => $database_config['mysql']['hostname'],
        'username' => $database_config['mysql']['username'],
        'password' => $database_config['mysql']['password'],
        'database' => $database,
    );
    $db = new sqldb($db_conf);
    $sepa_id=$db->field("SELECT sepa_settings_id FROM sepa_settings ");
    $last_number=$db->field("SELECT value FROM settings WHERE constant_name='SEPA_START_NUMBER' ");
    $last_number=$last_number+1;

    if(!$sepa_id){
        $sepa_number='334-'.$last_number;
    }else{
        $sepa_data = $db->query("SELECT sepa_invoice_start,sepa_inc_part FROM sepa_settings WHERE sepa_settings_id='".$sepa_id."' ");
        $sepa_number=$sepa_data->f('sepa_invoice_start').''.str_pad($last_number, $sepa_data->f('sepa_inc_part'), "0", STR_PAD_LEFT );
    }
    return $sepa_number;
}
function correct_val($number){
    $value=explode(":",$number);
    $nr_string1=(int)$value[0];
    $nr_string2=((int)$value[1])*100/60;
    $final_string=$nr_string1.'.'.$nr_string2;
    return (float)$final_string;
}
function get_contact_name($id){
    $db = new sqldb();
    $db->query("SELECT lastname, firstname FROM customer_contacts WHERE contact_id='".$id."' ");
    $db->next();
    return $db->f('lastname').' '.$db->f('firstname');
}
function get_not_paid_invoice($invoice_id,$with_vat=0){

   $db = new sqldb();
   $db->query("SELECT sum(amount) as amount,sum(amount_vat) as amount_vat
               FROM tblinvoice
               WHERE id='".$invoice_id."' and not_paid=1");
    return array($db->f('amount')-$db->f('amount_paid'), $db->f('amount_vat')-$db->f('amount_paid'));
}
function billableAmount($filter){
    $db = new sqldb();
    $total_amount = 0;
    $array = array();
    $tasks = $db->query("SELECT task_time.user_id, task_time.task_id, task_time.project_status_rate, task_time.customer_id, task_time.project_id, task_time.approved, tasks.t_h_rate, tasks.t_daily_rate, project_user.p_h_rate, project_user.p_daily_rate, tasks.task_budget,
                                projects.billable_type, projects.pr_h_rate,tasks.task_name as t_name, projects.company_name as c_name, projects.name as p_name,task_time.hours
                        FROM task_time
                        INNER JOIN projects on task_time.project_id = projects.project_id
                        INNER JOIN project_user ON task_time.project_id=project_user.project_id AND task_time.user_id=project_user.user_id
                        INNER JOIN tasks ON task_time.task_id = tasks.task_id AND task_time.project_id = tasks.project_id
                        WHERE $filter AND projects.invoice_method>0 AND projects.active!='0' ");

    while ($tasks->next()) {
        $total_h = 0;
        $total_h_approved = 0;
        $total_h = $tasks->f('hours');
        if($tasks->f('approved') == 1){
            $total_h_approved = $tasks->f('hours');
        }
        if($tasks->f('billable_type') == '5'){
            $total_h = 0;
            $total_h_approved = 0;
        }
        switch ($tasks->f('billable_type')) {
            case '1':
            case '7':
                if($tasks->f('project_status_rate')==0){
                    $rate = $tasks->f('t_h_rate');
                }else{
                    $rate = $tasks->f('t_daily_rate');
                }
                break;
            case '2':
                if($tasks->f('project_status_rate')==0){
                    $rate = $tasks->f('p_h_rate');
                }else{
                    $rate = $tasks->f('p_daily_rate');
                }
                break;
            case '3':
                $rate = $tasks->f('pr_h_rate');
                break;
            case '4':
                $rate = 0;
                break;
            case '5':
                # here we only invoice the hours not the bugdet of the tasks
                $rate = 0;
                $total_h= 0;
                // $rate = 1;
                // $total_h = $tasks->f('task_budget');
                break;
            default:
                $rate = 0;
                break;
        }
        $total_amount += $total_h*$rate;
        #creating the data for the fake invoices
        if(!array_key_exists($tasks->f('customer_id'), $array)){
            $array[$tasks->f('customer_id')] = array();
        }
        if(!array_key_exists($tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id'), $array[$tasks->f('customer_id')])) {
            $detail = array('name'          => $tasks->f('c_name'),
                            'project_id'        => $tasks->f('project_id'),
                            'description'       => $tasks->f('p_name').": ".$tasks->f('t_name')." ",
                            'quantity'          => $total_h,
                            'unit_price'        => $rate,
                            'project_name'      => $tasks->f('p_name'),
                            'task_name'         => $tasks->f('t_name'),
                            'task_approved'     => $total_h_approved ? 1 : $tasks->f('approved'),
                            'quantity_approved' => $total_h_approved,
                            );
            $array[$tasks->f('customer_id')][$tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id')] = $detail;
        }else{
            $array[$tasks->f('customer_id')][$tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id')]['quantity']+=$total_h;
            $array[$tasks->f('customer_id')][$tasks->f('project_id').'-'.$tasks->f('task_id').'-'.$tasks->f('user_id')]['quantity_approved']+=$total_h_approved;
        }
        // array_push($array[$tasks->f('customer_id')], $detail);
    }
    $result = array('total' => $total_amount, 'details' => $array);
    return $result;
}
function calculate_distance($lat1, $lon1, $lat2, $lon2, $unit='K')
{
  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}


/**
 * Used to log actions and allow tracebility between crm,quotes,projects,interventions,orders, p orders,invoices
 * It accepts 2 parameters: 1 associative array parameter with the REQUIRED following keys:
 * - target_id (INT - ex. invoice_id number)
 * - target_type (INT - values: 1-invoice,2-quote,3-project,4-order,5-intervention,6-p order,7-proforma,8-credit invoice,9-contracts,10-purchase invoice)
 * - target_buyer_id (INT - destination buyer id)
 * - lines: array containing multiple associative arrays with the REQUIRED following keys:
 *          - origin_id (INT - the origin module id, ex. invoice_id number)
 *          - origin_type (INT - same values as target_type)
 * AND 2 parameter trace_id if needed to update the lines
 * @return void
 * @author 
 **/
function addTracking($data,$trace_id=0,$database=null){
    
    $db = new sqldb();
    
    if(!$trace_id){
        if(!empty($data['lines'])){
            switch($data['lines'][0]['origin_type']){
                case '1':
                case '7':
                case '8':
                    $table="tblinvoice";
                    $table_id="id";
                    $customer_id="buyer_id";
                    break;
                case '2':
                    $table="tblquote";
                    $table_id="id";
                    $customer_id="buyer_id";
                    break;
                case '3':
                    $table="projects";
                    $table_id="project_id";
                    $customer_id="customer_id";
                    break;
                case '4':
                    $table="pim_orders";
                    $table_id="order_id";
                    $customer_id="customer_id";
                    break;
                case '5':
                    $table="servicing_support";
                    $table_id="service_id";
                    $customer_id="customer_id";
                    break;
                case '6':
                    $table="pim_p_orders";
                    $table_id="p_order_id";
                    $customer_id="customer_id";
                    break;
                case '9':
                    $table="contracts";
                    $table_id="contract_id";
                    $customer_id="customer_id";
                    break;
                case '10':
                    $table="tblinvoice_incomming";
                    $table_id="invoice_id";
                    $customer_id="supplier_id";
                    break;
                case '11':
                    $table='tblopportunity';
                    $table_id="opportunity_id";
                    $customer_id="buyer_id";
                    break;
                case '12':
                    $table='installations';
                    $table_id="id";
                    $customer_id="customer_id";
                    break;
            }
            $origin_buyer_id=$db->field("SELECT ".$customer_id." FROM ".$table." WHERE ".$table_id."='".$data['lines'][0]['origin_id']."' ");
        }else{
            $origin_buyer_id=$data['target_buyer_id'];
        }    
        $trace_id=$db->insert("INSERT INTO tracking SET
                                origin_buyer_id  ='".$origin_buyer_id."',
                                target_id        ='".$data['target_id']."',             
                                target_type      ='".$data['target_type']."',
                                target_buyer_id  ='".$data['target_buyer_id']."',
                                creation_date    ='".time()."',
                                creation_user_id ='".$_SESSION['u_id']."',
                                archived         ='0' ");
        switch($data['target_type']){
            case '1':
            case '7':
            case '8':
                $table_target="tblinvoice";
                $table_target_id="id";
                break;
            case '2':
                $table_target="tblquote";
                $table_target_id="id";
                break;
            case '3':
                $table_target="projects";
                $table_target_id="project_id";
                break;
            case '4':
                $table_target="pim_orders";
                $table_target_id="order_id";
                break;
            case '5':
                $table_target="servicing_support";
                $table_target_id="service_id";
                break;
            case '6':
                $table_target="pim_p_orders";
                $table_target_id="p_order_id";
                break;
            case '9':
                $table_target="contracts";
                $table_target_id="contract_id";
                break;
             case '10':
                $table_target="tblinvoice_incomming";
                $table_target_id="invoice_id";
                break;
            case '11':
                $table_target='tblopportunity';
                $table_target_id="opportunity_id";
                break;
            case '12':
                $table_target='installations';
                $table_target_id="id";
                break;
        }
        $db->query("UPDATE ".$table_target." SET trace_id='".$trace_id."' WHERE ".$table_target_id."='".$data['target_id']."' ");
    }
    foreach($data['lines'] as $key=>$value){
        $db->query("INSERT INTO tracking_line SET
                            origin_id             ='".$value['origin_id']."',
                            origin_type           ='".$value['origin_type']."',
                            trace_id              ='".$trace_id."' ");
    }
}


//Set email language as account / contact language
//Use logged in user as default language if are not set
function get_email_language($emailMessageData){ 
    
    $db = new sqldb();

    $email_language = DEFAULT_LANG_ID;

    if($emailMessageData['param'] == 'add'){
        if($emailMessageData['buyer_id']){
        $buyerLang = $db->field("SELECT language FROM customers WHERE customer_id='".$emailMessageData['buyer_id']."' ");
            if($buyerLang !='0'){
                $email_language = $buyerLang;
            }
        }

        if($emailMessageData['contact_id']){
            $contactLang = $db->field("SELECT language FROM customer_contacts WHERE contact_id='".$emailMessageData['contact_id']."' ");
            if($contactLang !='0'){
                $email_language = $contactLang;
            }
        }

        if($email_language == '0'){
            $default_email_language = $db->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");
            $email_language = $default_email_language;
        }    
    } else if($emailMessageData['param'] == 'edit' || $emailMessageData['param'] == 'update_customer_data'){

        if($emailMessageData['email_language']){

            //Get the document language
            $email_language = $emailMessageData['email_language'];

            if(($emailMessageData['item_id'] && $emailMessageData['item_id'] != 'tmp') && $emailMessageData['buyer_id']){
                $currentBuyerId = $db->field("SELECT ".$emailMessageData['table_buyer_label']." FROM ".$emailMessageData['table']." WHERE ".$emailMessageData['table_label']." = ".$emailMessageData['item_id']);

                $buyerLang = $db->field("SELECT language FROM customers WHERE customer_id='".$emailMessageData['buyer_id']."' ");

                if(($emailMessageData['buyer_id'] != $currentBuyerId) && $buyerLang !='0'){
                    $email_language = $buyerLang;
                }
            } else if($emailMessageData['buyer_id']){
                $buyerLang = $db->field("SELECT language FROM customers WHERE customer_id='".$emailMessageData['buyer_id']."' ");

                if($buyerLang !='0'){
                    $email_language = $buyerLang;
                }
            }
            
            if($email_language == '0'){
                $default_email_language = $db->field("SELECT lang_id FROM pim_lang WHERE pdf_default=1");
                $email_language = $default_email_language;
            }
                
        } else {
            if($emailMessageData['item_id']=='tmp' || !$emailMessageData['item_id']){
                $buyer_lang = $db->field("SELECT language FROM customers WHERE customer_id = '".$emailMessageData['buyer_id']."' ");
                if($buyer_lang){
                    $email_language=$buyer_lang;
                }
            }else{
                $itemLang = $db->field("SELECT email_language 
                            FROM ".$emailMessageData['table']
                            ." WHERE ".$emailMessageData['table_label']
                            ." = ".$emailMessageData['item_id']);
                if($itemLang){
                    $email_language = $itemLang;
                }
            }
        }
    }

    return $email_language;
}

function get_email_message_default_notes($data){
    $db = new sqldb();

    $terms = '';
    if($data['email_language'] != 1){      
        $terms = '_'.$data['email_language'];
    }

    if($data['default_name_note'] != 'contract_note'){
        $note = $db->field("SELECT value FROM default_data
                                               WHERE default_name = '".$data['default_name_note']."'
                                               AND type = '".$data['default_type_note'].$terms."'  ");    
    } else {
        $note = $db->field("SELECT value FROM default_data
                                               WHERE default_main_id='0'
                                               AND type = '".$data['default_type_note'].$terms."'  ");
    }
    
    $free_text_content = $db->field("SELECT value FROM default_data
                                               WHERE default_name = '".$data['default_name_free_text_content']."'
                                               AND type = '".$data['default_type_free_text_content'].$terms."'  ");
    
    return array('notes' => $note,'free_text_content' => $free_text_content);

}    

function get_categorisation_segment(){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT * FROM tblquote_segment ORDER BY sort_order ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('id'),'name'=>stripslashes($inf->f('name'))));
    }
    return $res;
}
function get_categorisation_type(){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT * FROM tblquote_type ORDER BY sort_order ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('id'),'name'=>stripslashes($inf->f('name'))));
    }
    return $res;
}
function get_categorisation_source(){
    $db=new sqldb();
    $res=array();
    $inf=$db->query("SELECT * FROM tblquote_source ORDER BY sort_order ASC");
    while($inf->next()){
        array_push($res, array('id'=>$inf->f('id'),'name'=>stripslashes($inf->f('name'))));
    }
    return $res;
}
function generate_installation_number($database=''){
    if($database){
        global $database_config,$cfg;
        $db_conf = array(
            'hostname' => $database_config['mysql']['hostname'],
            'username' => $database_config['mysql']['username'],
            'password' => $database_config['mysql']['password'],
            'database' => $database,
        );
        $db = new sqldb($db_conf);
    }else{
        $db = new sqldb();
    }

    $installation_start = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INSTALLATION_START' ");
    if(!$installation_start){
        $installation_start='inst-';
    }
    $digit_nr = $db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_INSTALLATION_DIGIT_NR' ");
    $prefix_lenght=strlen($installation_start)+1;
    $db->query("SELECT id,serial_number,CAST(SUBSTRING(serial_number,$prefix_lenght)AS UNSIGNED) AS installation_number,LEFT( serial_number, $prefix_lenght-1 ) AS installation_prefix
                    FROM installations
                    WHERE SUBSTRING(serial_number,1,$prefix_lenght-1)='".addslashes($installation_start)."'
                    ORDER BY installation_number DESC
                    LIMIT 1");
    if(!$digit_nr){
        $digit_nr = 0;
    }
    if($db->move_next() && $db->f('installation_prefix')==$installation_start){
        $next_serial=$installation_start.str_pad($db->f('installation_number')+1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }else{
        $next_serial=$installation_start.str_pad(1,$digit_nr,"0",STR_PAD_LEFT);
        return $next_serial;
    }
}