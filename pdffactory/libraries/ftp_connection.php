<?php

class ftp{

	private $db;
	public $is_connect = false;
	private $ftp_conn = false;


	function __construct( $host = null, $ftp_username = null, $ftp_userpass=null ) {
		// $ftp_server = "ftp.example.com";
		$this->ftp_conn = ftp_connect($host,21,10);
		if(!$this->ftp_conn){
			console::log('Could not connect to '.$host.' !');
			return false;
		}else{
			$this->is_connect= ftp_login($this->ftp_conn, $ftp_username, $ftp_userpass);
			ftp_pasv($this->ftp_conn, true);
		}

	}
	// returns content of folder
	public function getContent($path = '' ){
		if(!$path){
			$path='.';
		}
		if($this->is_connect){
			$file_list = ftp_nlist($this->ftp_conn, $path);
			return $file_list;
		}
		return $this->not_connected();
	}

	// write_a file upstairs
	public function writeFile($server_file, $local_file ){
		if(!$server_file){
			return 'no server file!';
		}
		if(!$local_file){
			return 'no local file!';
		}
		if($this->is_connect){
			$file = ftp_put($this->ftp_conn, $server_file, $local_file,  FTP_BINARY);
			return $file;
		}
		return $this->not_connected();
	}

	// returns timestamp of last update
	public function getLastUpdateTime($path ){

		if($this->is_connect){
			$timestamp = ftp_mdtm($this->ftp_conn, $path);
			return $timestamp;
		}
		return $this->not_connected();
	}

	// returns content of folder
	public function getfileContent($server_file){
		if($this->is_connect){
			ob_start();
			$result = ftp_get($this->ftp_conn, "php://output", $server_file, FTP_BINARY);
			$data = ob_get_contents();
			ob_end_clean();


			if ($data){
				return $data;
			} elseif($result) {
			   echo "Memory problem!";
			}else{
				echo "Connection problem!";
			}
		}
		return $this->not_connected();
	}
	public function getfile($server_file, $local_file){
		if($this->is_connect){
			$result=ftp_get($this->ftp_conn, $local_file, $server_file, FTP_BINARY);
				if ($result)
				  {
				  	return true;
				  }
				else
				  {
				  	echo "Error downloading $server_file.";
				  }

		}else{
			return $this->not_connected();
		}

	}

	public function close(){
		ftp_close ( $this->ftp_conn );
	}

	private function not_connected(){
		console::log('Could not connect');
		return null;
	}
}
?>