<?php

class tips{

	private $db;

	private $html = '<div id="sticky%d" class="atip">%s</div>';

	public $text = '';

	function __construct() {
		global $database_config;
		$db_config = array(
		'hostname' => $database_config['mysql']['hostname'],
		'username' => $database_config['mysql']['username'],
		'password' => $database_config['mysql']['password'],
		'database' => $database_config['user_db'],
		);

		$this->db = new sqldb($db_config);
	}

	public function getTip($controller){
		$tips = $this->db->query("SELECT * FROM tips_page WHERE controller='".$controller."' AND lang='".$_SESSION['l']."' ");
		while ($tips->next()) {
			if($tips->f('text')){
				$this->text .= sprintf($this->html,$tips->f('order'),utf8_encode($tips->f('text')));
			}
		}
		return $this->text;
	}

}

?>