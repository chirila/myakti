<?php
use Aws\Common\Aws;
use Aws\S3\Model\ClearBucket;

class awsWrap
{
	public $client = array();
	private $awsC;
	private $bucket = '';
	private $store = '';
	protected $append = '';
	/**
	*
	*/
	function __construct($bucket='',$client = 'S3') {
		global $config;
		$this->store = $bucket;
		if(!$bucket && $client == 'S3'){
			console::error('You must provide a bucket');
			return;
		}
		$this->append = $config['awsBucket'];
		ark::loadLibraries(array('aws/aws-autoloader'));
		$this->awsC = Aws::factory(__DIR__.'/../config/aws.php');
		$this->client[$client] = $this->awsC->get($client);
		if($client == 'S3'){
			// $this->bucket = $this->append.$bucket;
			$this->bucket ='thinkwebs3';
			// if(!$this->checkIfBucketExists($this->bucket)){
			// 	$this->createBucket($this->bucket);
			// }
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function listBuckets(){
		return $this->client['S3']->listBuckets(array());
	}

	/**
	 * undocumented function
	 *
	 * @param $b - bucket name
	 * @return void
	 * @author
	 **/
	function createBucket($b){
		$this->client['S3']->createBucket(array('Bucket' => $b));
		$this->client['S3']->waitUntil('BucketExists', array('Bucket' => $b));
	}

	/**
	 * undocumented function
	 *
	 * @param $b - bucket name
	 * @return void
	 * @author
	 **/
	function checkIfBucketExists($b){
		$result = $this->listBuckets();
		$exists = false;
		foreach ($result['Buckets'] as $bucket) {
		  if($bucket['Name'] == $b){
		  	$exists = true;
		  	break;
		  }
		}
		return $exists;
	}

	function addClient($client){
		if(!$client){ return; }
		$this->client[$client] = $this->awsC->get($client);
	}

	/**
	*	@param path - the path to the file name
	*	@param fName - the name of the file in amazon
	*/
	function uploadFile($path,$fName){
		$fsize = filesize($path);
		$finfo = finfo_open(FILEINFO_MIME);
		$mtype = finfo_file($finfo, $path);
		finfo_close($finfo);
		try{
			$this->client['S3']->putObject(array(
		    'Bucket' 				=> $this->bucket,
		    'Key'    				=> $this->append.$this->store.'/'.$fName,
		    'ContentLength' => $fsize,
		    'ContentType'	  => $mtype,
		    'Body'   				=> fopen($path, 'r+')
			));
			return true;
		} catch( Exception $e){
			if($e->message == 'The specified bucket does not exist'){
				$this->createBucket($this->bucket);
				return $this->uploadFile($path,$fName);
			}
			msg::$error = $e->message;
			return false;
		}
	}

	/**
	 * delete a bucket and all of it's content
	 *
	 * @return void
	 * @author
	 **/
	function deleteB($b){
		$b = $this->append.$b;
		$clear = new ClearBucket($this->client['S3'], $b);
		$clear->clear();
		$this->client['S3']->deleteBucket(array('Bucket' => $b));
		$this->client['S3']->waitUntil('BucketNotExists', array('Bucket' => $b));
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function getBucketContent(){
		try {
			$iterator = $this->client['S3']->getIterator('ListObjects', array(
		    'Bucket' => $this->bucket
			));
			$array = array();
			foreach ($iterator as $object) {
			  // console::log($object);
				array_push($array, $object['Key']);
			}
			return $array;
		} catch (Exception $e) {
			if($e->message == 'The specified bucket does not exist'){
				$this->createBucket($this->bucket);
			}
			msg::$error = $e->message;
			return false;
		}
	}

	/**
	 *
	 * check if an object exists
	 * the function can be extended to take the bucket as a param also
	 *
	 * @return Boolean
	 * @author PM
	 **/
	function doesObjectExist($o){
		try{
			$exist = $this->client['S3']->doesObjectExist($this->bucket, $o);
			return $exist;
		} catch(Exception $e){
			if($e->message == 'The specified bucket does not exist'){
				$this->createBucket($this->bucket);
			}
			msg::$error = $e->message;
			return false;
		}
	}
	/**
	 * get signed url for an object
	 * the function can be extended to take the bucket as a param also
	 * the expire will be set to 1 year (will have to see how to deal when the link expires (done maybe) )
	 *
	 * @return void
	 * @author
	 **/

	function getLink($o){
		console::log($this->bucket,$o);
		$expire = time() + (60*60*24*365);
		try{
			$db = new sqldb();
			$link = $db->query("SELECT * FROM s3_links WHERE bucket='".$this->bucket."' AND item='".$o."' ");
			if($link->next()){
				if(defined('AWS_FACTORY_KEY') && strpos($link->f('link'), AWS_FACTORY_KEY) === false){
					$signedUrl = $this->client['S3']->getObjectUrl($this->bucket, $o, $expire);
					$db->query("UPDATE s3_links SET link='".$signedUrl."', expire='".$expire."' WHERE id='".$link->f('id')."' ");
					return $signedUrl;
				}
				if($link->f('expire') > (time()+60*60*24*3)){
					return $link->f('link');
				}else{
					$signedUrl = $this->client['S3']->getObjectUrl($this->bucket, $o, $expire);
					$db->query("UPDATE s3_links SET link='".$signedUrl."', expire='".$expire."' WHERE id='".$link->f('id')."' ");
					return $signedUrl;
				}
			}
			$signedUrl = $this->client['S3']->getObjectUrl($this->bucket, $o, $expire);
			$db->query("INSERT INTO s3_links SET
				link='".$signedUrl."',
				expire='".$expire."',
				bucket='".$this->bucket."',
				item='".$o."' ");
			return $signedUrl;
		} catch(Exception $e){
			if($e->message == 'The specified bucket does not exist'){
				$this->createBucket($this->bucket);
			}
			msg::$error = $e->message;
			return '';
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function getItem($f,$target){
		try {
			$this->client['S3']->getObject(array(
			    'Bucket' => $this->bucket,
			    'Key'    => $this->append.$this->store.'/'.$f,
			    'SaveAs' => $target,
			));
			return true;
		} catch (Exception $e) {
			if($e->message == 'The specified bucket does not exist'){
				$this->createBucket($this->bucket);
			}
			return false;
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function deleteItems($i){
		$files = array();
		$Bfiles = $this->getBucketContent();
		if($Bfiles){
			foreach ($Bfiles as $key => $value) {
				if(strpos($value,$i) !== false ){
					$f =  array('Key'=>$value);
					array_push($files, $f);
				}
			}
		}
		if(!empty($files)){
			$db = new sqldb();
			try {
				$ddd = $this->client['S3']->deleteObjects(array(
				    'Bucket' 		 => $this->bucket,
				    'Objects'    => $files
				));
				$db->query("DELETE FROM s3_links WHERE item LIKE '".$i."%' ");
				return true;
			} catch (Exception $e) {
				if($e->message == 'The specified bucket does not exist'){
					$this->createBucket($this->bucket);
				}
				$db->query("DELETE FROM s3_links WHERE item LIKE '".$i."%' ");
				return false;
			}
		}

	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function deleteItem($i)
	{
		$files = array(array('Key'=>$i));
		$db = new sqldb();
		try {
			$ddd = $this->client['S3']->deleteObjects(array(
			    'Bucket' 		 => $this->bucket,
			    'Objects'    => $files
			));
			$db->query("DELETE FROM s3_links WHERE item = '".$i."%' ");
			return true;
		} catch (Exception $e) {
			if($e->message == 'The specified bucket does not exist'){
				$this->createBucket($this->bucket);
			}
			$db->query("DELETE FROM s3_links WHERE item = '".$i."%' ");
			return false;
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function copyItem($source,$target)
	{
		console::log($source);
		try {
			$this->client['S3']->CopyObject(array(
			    'Bucket' => $this->bucket,
			    'Key'    => $target,
			    'CopySource' => $this->bucket.'/'.$source,
			));
			return true;
		} catch (Exception $e) {
			var_dump($e);
			if($e->message == 'The specified bucket does not exist'){
				$this->createBucket($this->bucket);
			}
			return false;
		}
	}

}

?>