<?php

class gCalendar{

	var $db;							# holds the database connection
	var $db_users;
	var $conected = false;
	var $api;
	var $client;
	var $calendar;
	var $calendar_active = false;

	/**
	 * [__construct description]
	 */
	function __construct(){
		$this->db = new sqldb;
		global $database_config;
		$db_config = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($db_config);
		$calendar = $this->db->field("SELECT active FROM apps WHERE name='Google Calendar' AND main_app_id='0' AND type='main' ");
		if($calendar){
			$this->calendar_active = $this->db_users->field("SELECT value FROM user_meta WHERE user_id='".$_SESSION['u_id']."' AND name='google_cal_active' ");
			if($this->calendar_active){
				$this->conected = true;
				$this->api = GOOGLE_CALENDAR;
				ark::loadLibraries(array('google-api-php-client/src/Google_Client','google-api-php-client/src/contrib/Google_CalendarService'));
				$this->client = new Google_Client();
				$this->client->setClientId('276047236640-ivltoj4ovpdeei5dhak1r7h3gnmlru0t.apps.googleusercontent.com');
				$this->client->setClientSecret('-K13Jw5RwQlxF7Tp0X1jXy3q');
				$this->client->setRedirectUri('https://app.akti.com/pim/admin/index.php?do=misc-maccount');
				$this->client->setDeveloperKey('AIzaSyAhqFkkJLyO--XNMTTXS6LPy11czGJd4Cg');
				$this->cal = new Google_CalendarService($this->client);
				/*if($this->client->isAccessTokenExpired()){
					$this->client->authenticate();
				}*/
				$this->client->setAccessToken($this->api);
			}
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function add_event(&$in)
	{
		if($this->conected){
			$attendees = array();
			if($in['customer_id']){
				$cemail = $this->db->field("SELECT c_email FROM customers WHERE customer_id='".$in['customer_id']."' ");
				if(filter_var($cemail,FILTER_VALIDATE_EMAIL)){
					$att = new Google_EventAttendee();
					$att->setEmail($cemail);
					$att->setDisplayName($in['customer_name']);
					array_push($attendees, $att);
				}
			}
			if($in['contact_ids']){
				foreach ($in['contact_ids'] as $key ) {
					$cont = $this->db->query("SELECT CONCAT_WS(' ',firstname, lastname)as name, email FROM customer_contacts WHERE contact_id='".$key."' ");
					if(filter_var($cont->f('email'),FILTER_VALIDATE_EMAIL)){
						$att = new Google_EventAttendee();
						$att->setEmail($cont->f('email'));
						$att->setDisplayName($cont->f('name'));
						array_push($attendees, $att);
					}
				}
			}

			$calList = $this->cal->calendarList->listCalendarList();
			$end_data = new Google_EventDateTime();
			$end_data->setDateTime(date("c",$in['end_date']));
			$start_data = new Google_EventDateTime();
			$start_data->setDateTime(date("c",$in['start_date']));
			
			$post = new Google_Event();
			$post->setEnd($end_data);
			$post->setstart($start_data);
			$post->setSummary('[Akti] '.$in['subject']);
			$post->setLocation($in['location']);
			$post->setDescription($in['comment']);

			if(!empty($attendees)){
				$post->setAttendees($attendees);
			}
			$test2 = $this->cal->events->insert($calList->items[0]->id,$post);
			$this->db->query("UPDATE customer_meetings SET google_calendar_id='".$test2->getId()."'  WHERE customer_meeting_id='".$in['meeting_id']."' ");
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function update_event(&$in)
	{
		if($this->conected){
			$evend_id = $this->db->field("SELECT google_calendar_id FROM customer_meetings WHERE customer_meeting_id='".$in['meeting_id']."' ");
			if($evend_id){
				$attendees = array();
				if($in['customer_id']){
					$cemail = $this->db->field("SELECT c_email FROM customers WHERE customer_id='".$in['customer_id']."' ");
					if(filter_var($cemail,FILTER_VALIDATE_EMAIL)){
						$att = new Google_EventAttendee();
						$att->setEmail($cemail);
						$att->setDisplayName($in['customer_name']);
						array_push($attendees, $att);
					}
				}
				if($in['contact_ids']){
					foreach ($in['contact_ids'] as $key ) {
						$cont = $this->db->query("SELECT CONCAT_WS(' ',firstname, lastname)as name, email FROM customer_contacts WHERE contact_id='".$key."' ");
						if(filter_var($cont->f('email'),FILTER_VALIDATE_EMAIL)){
							$att = new Google_EventAttendee();
							$att->setEmail($cont->f('email'));
							$att->setDisplayName($cont->f('name'));
							array_push($attendees, $att);
						}
					}
				}

				$calList = $this->cal->calendarList->listCalendarList();
				$end_data = new Google_EventDateTime();
				$end_data->setDateTime(date("c",$in['end_date']));
				$start_data = new Google_EventDateTime();
				$start_data->setDateTime(date("c",$in['start_date']));
				$post = new Google_Event();
				$post->setEnd($end_data);
				$post->setstart($start_data);
				$post->setSummary('[Akti] '.$in['subject']);
				$post->setLocation($in['location']);
				$post->setDescription($in['comment']);
				if(!empty($attendees)){
					$post->setAttendees($attendees);
				}
				$e = $this->cal->events->get($calList->items[0]->id,$evend_id);
				$post->setSequence($e->sequence);
				$test2 = $this->cal->events->update($calList->items[0]->id,$evend_id,$post);
			}
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function delete_event(&$in)
	{
		if($this->calendar_active){
			$evend_id = $this->db->field("SELECT google_calendar_id FROM customer_meetings WHERE customer_meeting_id='".$in['log_id']."' ");
			if($evend_id){
				$calList = $this->cal->calendarList->listCalendarList();
				$test2 = $this->cal->events->delete($calList->items[0]->id,$evend_id);
			}
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function sendToGoogle(&$in)
	{
		$time = time();
		$meeting = $this->db->query("SELECT * FROM customer_meetings WHERE google_calendar_id='' AND start_date>='".$time."' ");
		while ( $meeting->next() ) {
			$in['start_date'] = $meeting->f('start_date');
			$in['end_date'] = $meeting->f('end_date');
			$in['subject'] = $meeting->f('subject');
			$in['location'] = $meeting->f('location');
			$in['comment'] = $meeting->f('message');
			$in['customer_id'] = $meeting->f('customer_id');
			$in['customer_name'] = $meeting->f('customer_name');
			#contacts
			$in['contact_ids'] = array();
			$contacts = $this->db->query("SELECT * FROM customer_contact_activity_contacts WHERE activity_id='".$in['meeting_id']."' ");
			while ($contacts->next()) {
				array_push($in['contact_ids'], $contacts->f('contact_id'));
			}

			$this->add_event($in);
		}
	}

}

?>