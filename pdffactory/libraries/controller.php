<?php

class Controller{

	public $in = array();
	protected $out = '';

	function __construct($in,$db = null)
	{
		$this->in = $in;
		$this->db = $db ? $db : new sqldb();
		global $database_config;
		$this->database_users = array(
			'hostname' => $database_config['mysql']['hostname'],
			'username' => $database_config['mysql']['username'],
			'password' => $database_config['mysql']['password'],
			'database' => $database_config['user_db'],
		);
		$this->db_users = new sqldb($this->database_users);
	}

	public function output($value=array())
	{
		if(!empty($value)){
			json_out($value);
		}
		if(!empty($this->out)){
			json_out($this->out);
		}
	}

}

?>