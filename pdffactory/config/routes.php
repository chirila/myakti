<?php   if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * --------------------------------------------------------------------------
 * Website Routes
 * --------------------------------------------------------------------------
  */
$routes = array(
    'quote/' => array(
        'method' => array("GET","POST"),
        'do' => 'quote-print',
        'params' => ''
    ),
    'order/' => array(
        'method' => array("GET","POST"),
        'do' => 'order-print',
        'params' => ''
    ),
    'po_order/' => array(
        'method' => array("GET","POST"),
        'do' => 'po_order-print',
        'params' => ''
    ),
    'stock/' => array(
        'method' => array("GET","POST"),
        'do' => 'stock-print',
        'params' => ''
    ),
    'project/' => array(
        'method' => array("GET","POST"),
        'do' => 'project-print',
        'params' => ''
    ),
    'timetracker/' => array(
        'method' => array("GET","POST"),
        'do' => 'timetracker-print',
        'params' => ''
    ),
    'maintenance/' => array(
        'method' => array("GET","POST"),
        'do' => 'maintenance-print',
        'params' => ''
    ),
    'contract/' => array(
        'method' => array("GET","POST"),
        'do' => 'contract-print',
        'params' => ''
    ),
    'invoice/' => array(
        'method' => array("GET","POST"),
        'do' => 'invoice-print',
        'params' => ''
    ),

);

if(INSTALLPATH == '../'){
    $base = $config['install_path'].ENVIRONMENT.'/';
} else {
    $base = $config['install_path'];
}

$router = new arkRouter($routes,$base);

/*
$router->add('myaccount', 'GET', 'home-my_account', 'user_id');
$router->add('user', 'GET', 'home-user', '/user_id/test/');
*/

$router->run();