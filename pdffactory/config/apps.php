<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * --------------------------------------------------------------------------
 * Website Apps
 * --------------------------------------------------------------------------
 * Every app in the apps folder needs to be registered here, so the framework know where to look for it
 */

define("USE_APPS", true);
define("USE_ENVIRONMENT", false);

$apps = array();

$apps['auth'] = 1; # for auth we don't care
$apps['maintenance'] = 13;
$apps['misc'] = 10;
$apps['order'] = 6;
$apps['invoice'] = 4;
$apps['quote'] = 5;
$apps['article'] = 12;
$apps['customers'] = 1;
$apps['settings'] = 7;
$apps['project'] = 3;
$apps['po_order'] = 14;
$apps['cashregister'] = 15;
$apps['stock'] = 16;
$apps['installation'] = 17;
$apps['contract'] = 11;
$apps['report'] = 18;
$apps['timetracker'] = 19;

// $default_app = 'misc';
// $default_controller = 'home';

// $default_controller['front'] = 'home';
$auth_app = 'auth';
$auth_controller = 'login';

/*
$auth_app = 'auth';
$auth_controller = 'login';

$default_app['front'] = 'home';
$default_controller['front'] = 'home';

$default_app['admin'] = 'home';

*/

# google maps api key for project CRM maps

// AIzaSyCd5onUZQ4N_hZHPY5lbHb_2m8HWSs0sXY