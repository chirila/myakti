<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
	
/*
$msg['required']			= "The %s field is required.";
$msg['min_length']			= "The %s field must be at least %s characters in length.";
$msg['max_length']			= "The %s field can not exceed %s characters in length.";
$msg['length']		= "The %s field must be exactly %s characters in length.";
$msg['email']		= "The %s field must contain a valid email address.";
$msg['emails']		= "The %s field must contain all valid email addresses.";
$msg['url']			= "The %s field must contain a valid URL.";
$msg['ip']			= "The %s field must contain a valid IP.";
$msg['alpha']				= "The %s field may only contain alphabetical characters.";
$msg['alpha_numeric']		= "The %s field may only contain alpha-numeric characters.";
$msg['alpha_numeric_dash']			= "The %s field may only contain alpha-numeric characters, underscores, and dashes.";
$msg['numeric']			= "The %s field must contain only numbers.";
$msg['integer']			= "The %s field must contain an integer.";
$msg['decimal']			= "The %s field must contain a decimal number.";
$msg['less_than']			= "The %s field must contain a number less than %s.";
$msg['greater_than']		= "The %s field must contain a number greater than %s.";
$msg['match']			= "The %s field does not match the %s field.";
$msg['unique'] 			= "The %s field must contain a unique value.";
$msg['exist'] 			= "The %s does not exist.";

*/
$msg['required'] = "This field is required.";
$msg['min_length'] = "This field must be at least %s characters in length.";
$msg['max_length'] = "This field can not exceed %s characters in length.";
$msg['length'] = "This field must be exactly %s characters in length.";
$msg['email'] = "This field must contain a valid email address.";
$msg['emails'] = "This field must contain all valid email addresses.";
$msg['url'] = "This field must contain a valid URL.";
$msg['ip'] = "This field must contain a valid IP.";
$msg['alpha'] = "This field may only contain alphabetical characters.";
$msg['alpha_numeric'] = "This field may only contain alpha-numeric characters.";
$msg['alpha_numeric_dash'] = "This field may only contain alpha-numeric characters, underscores, and dashes.";
$msg['numeric'] = "This field must contain only numbers.";
$msg['integer'] = "This field must contain an integer.";
$msg['decimal'] = "This field must contain a decimal number.";
$msg['less_than'] = "This field must contain a number less than %s.";
$msg['greater_than'] = "This field must contain a number greater than %s.";
$msg['match'] = "This field does not match the '<strong>%s</strong>' field.";
$msg['unique'] = "This value is already in use.";
$msg['exist'] = "This value does not exist.";
$msg['exist2'] = "This value does not exist.";
$msg['empty_condition'] ="";
$msg['dateISO8601'] = "Value does not comply to ISO8601 format";
if(!class_exists('validation')){
class validation {
	private $result;	
	private $fields;
	private $in;
	
	function validation($in=false) {
		if(!$in){
			global $in;
		}
		global $msg;
		$this->result = true;
		$this->fields = array();
		$this->in = $in;
		$this->msg = $msg;
	}

	public function f($field, $label, $rules, $error_message=false){
		return $this->field($field, $label, $rules, $error_message);
	}

	public function field($field, $label, $rules, $error_message=false,$database_config="") {
		$this->fields[$field]['label'] = $label;
		if($error_message){
			$this->fields[$field]['error_message'] = $error_message;
		}
		$rules = explode(':', $rules);
		foreach($rules as $k => $v){
			list($rule, $condition) = explode('[',$v);
			$this->fields[$field]['rules'][]=$rule;
			if($condition){
				$condition = str_replace(']','',$condition);
				$this->fields[$field][$rule] = $condition;
			}
		}
		if($database_config){
			$this->fields[$field]['datab'] = $database_config;			
		}
	}

	public function run() {
		foreach($this->fields as $field => $params){
			$valid = true;
			foreach($this->fields[$field]['rules'] as $k => $rule){
				if($valid == true){
					$function = 'validate_'.$rule;
					if(method_exists($this, $function)){
						$valid = $this->$function($field,$params);
					} elseif(function_exists($rule)){
						$rule($this->in[$field]);
					}
				}
				
			}
		}
		return $this->result;
	}
	
	private function get_message($field, $params, $rule){
		if($params['error_message']){
			$error_message = $params['error_message'];
		}else{
			if($params[$rule]){
				$error_message = sprintf($this->msg[$rule], $params[$rule]);
			} else {
				//$error_message = sprintf($this->msg[$rule],$params['label']);
				$error_message = sprintf($this->msg[$rule],'');
			}
		}
		
		return $error_message;
	}
	
	private function validate_required($field, $params){
		if(is_array($this->in[$field])){
			foreach($this->in[$field] as $key => $value ){
				if(!$this->in[$field][$key]){
					$this->result = false;
					$error_message = $this->get_message($field, $params, 'required');
					msg::set($field,$error_message);
					return false;
				}
			}
		}
		else if(!$this->in[$field]){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'required');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}
	
	private function validate_min_length($field, $params){
		if(!$this->in[$field]){ return true; }
		if(strlen($this->in[$field]) < $params['min_length']){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'min_length');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}
	
	private function validate_max_length($field, $params){
		if(!$this->in[$field]){ return true; }
		if(strlen($this->in[$field]) > $params['max_length']){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'max_length');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}
	
	private function validate_length($field, $params){
		if(!$this->in[$field]){ return true; }
		if(strlen($this->in[$field]) != $params['length']){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'length');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}

	private function validate_email($field, $params){
		if(!$this->in[$field]){ return true; }
		if(!filter_var($this->in[$field], FILTER_VALIDATE_EMAIL)){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'email');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}

	private function validate_emails($field, $params){
		if(!$this->in[$field]){ return true; }
		$field_content = str_replace(' ', '', $this->in[$field]);
		$email_addresses = explode(',', $field_content);
		if(count($email_addresses) > 0){
			foreach($email_addresses as $k => $email){
				if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
					$this->result = false;
					$error_message = $this->get_message($field, $params, 'emails');
					msg::set($field,$error_message);
					return false;
				}
			}
			return true;
		}
	}

	private function validate_url($field, $params){
		if(!$this->in[$field]){ return true; }
		if(!filter_var($this->in[$field], FILTER_VALIDATE_URL)){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'url');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}

	private function validate_ip($field, $params){
		if(!$this->in[$field]){ return true; }
		if(!filter_var($this->in[$field], FILTER_VALIDATE_IP)){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'ip');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}

	private function validate_alpha($field, $params){
		if(!$this->in[$field]){ return true; }
		if(!preg_match("/^([a-z])+$/i", $this->in[$field])){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'alpha');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}

	private function validate_alpha_numeric($field, $params){
		if(!$this->in[$field]){ return true; }
		if(!preg_match("/^([a-z0-9])+$/i", $this->in[$field])){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'alpha_numeric');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}

	private function validate_alpha_numeric_dash($field, $params){
		if(!$this->in[$field]){ return true; }
		if(!preg_match("/^([-a-z0-9_-])+$/i", $this->in[$field])){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'alpha_numeric_dash');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}

	/*private function validate_numeric($field, $params){
		if(!$this->in[$field]){ return true; }
		if(is_array($this->in[$field])){
			foreach ($this->in[$field] as $key){
				if(!is_numeric($key)){
					$this->result = false;
					$error_message = $this->get_message($field, $params, 'numeric');
					msg::set($field,$error_message);
				}
			}
		}
		else if(!is_numeric($this->in[$field])){
			
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'numeric');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}*/
	private function validate_numeric($field, $params){
		if(!$this->in[$field]){ return true; }
		if(is_array($this->in[$field])){
			foreach ($this->in[$field] as $key){
				if(!is_numeric(return_value($key))){
					$this->result = false;
					$error_message = $this->get_message($field, $params, 'numeric');
					msg::set($field,$error_message);
				}
			}
		}
		else if(!is_numeric(return_value($this->in[$field]))){

			$this->result = false;
			$error_message = $this->get_message($field, $params, 'numeric');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}

	
	private function validate_integer($field, $params){
		if(!$this->in[$field]){ return true; }
		if(!is_numeric($this->in[$field]) || intval($this->in[$field]) != $this->in[$field]){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'integer');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}

	private function validate_decimal($field, $params){
		if(!$this->in[$field]){ return true; }
		if(!is_numeric($this->in[$field]) || intval($this->in[$field]) == $this->in[$field]){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'decimal');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}
	
	private function validate_less_than($field, $params){
		if(!$this->in[$field]){ return true; }
		if($this->in[$field] > $params['less_than']){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'less_than');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}

	private function validate_greater_than($field, $params){
		if(!$this->in[$field]){ return true; }
		if( $this->in[$field] < $params['greater_than']){
			$this->result = false;
			
			$error_message = $this->get_message($field, $params, 'greater_than');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}

	private function validate_match($field, $params){
		if(!$this->in[$field]){ return true; }
		if($this->in[$field] != $this->in[$params['match']]){
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'match');
			msg::set($field,$error_message);
			return false;
		}
		return true;
	}

	private function validate_unique($field, $params){
        if(!$this->in[$field]){  return true; }

        $db = new sqldb($params['datab']);
        list($table, $column, $condition) = explode('.',$params['unique'],3);
        if($condition){
            $condition = " and ".trim($condition);
        }else{
            $condition = "";
        }
        $dbval = $db->field("select `".$column."` from `".$table."` where `".$column."` = '".$this->in[$field]."'".$condition);
		if($dbval){
            $this->result = false;
            $error_message = $this->get_message($field, $params, 'unique');
            msg::set($field,$error_message);
            return false;
        }
       
        return true;
    }
  
  private function validate_exist($field, $params){
        if(!$this->in[$field]){  return true; }
        $db = new sqldb($params['datab']);
        list($table, $column, $condition) = explode('.',$params['exist'],3);
        if($condition){
            $condition = " and ".trim($condition);
        }else{
            $condition = "";
        }
        $dbval = $db->field("select `".$column."` from `".$table."` where `".$column."` = '".$this->in[$field]."'".$condition);
        if(!$dbval){
            $this->result = false;
            $error_message = $this->get_message($field, $params, 'exist');
            msg::set($field,$error_message);
            return false;
        }
       
        return true;
    }
  
  private function validate_exist2($field, $params){
        if(!$this->in[$field]){  return true; }
        $db = new sqldb($params['datab']);
        list($table, $column, $condition) = explode('.',$params['exist2'],3);
        if($condition){
            $condition = " and ".trim($condition);
        }else{
            $condition = "";
        }
        $dbval = $db->field("select `".$column."` from `".$table."` where `".$column."` = '".$this->in[$field]."'".$condition);
        if($dbval){
            $this->result = false;
            $error_message = $this->get_message($field, $params, 'exist2');
            msg::set($field,$error_message);
            return false;
        }
       
        return true;
    }
   
	private function validate_empty_condition($field, $params){
		$this->result = false;
		$error_message = $this->get_message($field, $params, '');
		msg::set($field,$error_message);
		return false;
	}

	private function validate_dateISO8601($field, $params){
		
	    if (preg_match('/^'.
            '(\d{4})-(\d{2})-(\d{2})T'. // YYYY-MM-DDT ex: 2014-01-01T
            '(\d{2}):(\d{2}):(\d{2})'.  // HH-MM-SS  ex: 17:00:00
            '(Z|((-|\+)\d{2}:\d{2}))'.  // Z or +01:00 or -01:00
            '$/', $this->in[$field], $parts) == true)
		{
			try {
				new \DateTime($this->in[$field]);
				return true;
			}
			catch ( \Exception $e)
			{
				$this->result = false;
				$error_message = $this->get_message($field, $params, 'dateISO8601');
	    			msg::set($field,$error_message);
				return false;
			}
		} else {
			$this->result = false;
			$error_message = $this->get_message($field, $params, 'dateISO8601');
	    		msg::set($field,$error_message);
			return false;
		}
	}
	
}
}