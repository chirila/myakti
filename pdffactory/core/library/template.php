<?php	if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * DooHoo
 * An open-source PHP framework.
 * 
 * @package DooHoo
 * @author Bodi Zsolt <zsolt@medeeaweb.com>
 * @copyright Copyright (c) Medeeaweb Works
 * @since Version 1.0
 */

/**
 * Template Class
 * 
 * This class is used to parse the "template tags" and "template commands" into
 * php code which then is executed.
 * 
 * @package DooHoo
 * @subpackage Core
 * @author Bodi Zsolt <zsolt@medeeaweb.com>
 * @example 
 * 
 * $foo = array(array('a'=>1),array('a'=>2),array('a'=>3),array('a'=>4),array('a'=>5),array('a'=>6));
 * $empty_foo = array();
 * $check_for_me = true
 * $i_dont_exists = false;
 * 
 * This will itereate over $foo and echo out the value of "a"
 * 
 * {d:repeat items="foo"}
 * {d:a}
 * {d:endrepeat}
 * {d:unless_repeat items="empty_foo"}
 * This will be executed because $empty_foo is empty
 * {d:endunless_repeat}
 * 
 * {d:exists value="check_for_me"}
 * this will exist
 * {d:endexists}
 * 
 * {d:unless_exists value="i_dont_exists"}
 * this will be executed if the value is falsy aka false,empty string or zero like for $i_dont_exists
 * {d:endunless_exists}
 * 
 * For form handling use
 * For normal input/textarea {d:=name_of_field}
 * For select button {d:options from="place_to_take_Value_from" id="key_of_the_select"}
 * 
 * Widgets are {d:widget app="name-of-app" name="name-of-widget"}
 * 
 * For messages {d:messages} from="name-of-message-sender"}
 * OR  {d:messages} from="name-of-message-sender" field="name-of-field"} for only the messages which refer to one field
 * 
 */
class dhTemplate{
	
	protected $_input = array();
	protected $_message = null;
	protected $_widget = null;
	protected $_error = null;
	protected $_debug = false;
	protected $_hasErrorHandler = false;
	
	public $tags = array(
		//control structures
			'!{d:repeat items="([\w]+)"}!',
			'!{d:endrepeat}!',
			'!{d:unless_repeat items="([\w]+)"}!',
			'!{d:endunless_repeat}!',
			'!{d:exists value="([\w]+)"}!',
			'!{d:endexists}!',
			'!{d:unless_exists value="([\w]+)"}!',
			'!{d:endunless_exists}!',
			'!{d:cycle values=(.*?)}!',
		//pagination helper
			'!{d:pagination for="([\w]+)"( showlast="(true|false)"){0,1}( isdropdown="(true|false)"){0,1}}(.*?){d:end_pagination}!is',
		//format input
			'!{d:=([\w]+)\|format=([a-z0-9A-z/_\-\:]+)}!',	
			'!{d:([\w]+)\|format=([a-z0-9A-z/_\-\:]+)}!',	
		//template vars	
			'!{d:([\w]+)}!',
		//form handling	
			'!{d:=([\w]+)}!',
			'!{d:options from="([\w]+)"( id="([\w]+)"){0,1}}!',
			'!{d:check field="([\w]+)"( always="(true|false)"){0,1}}!',
		//widget handling
			'!{d:widget app="([\w]+)" name="([\w]+)"}!'	,
		//messaging
			'!{d:messages from="([\w]+)"( field="([\w]+)"){0,1}}!',
		),		
		$replace = array(
		//control structures
			 '<?php if((isset($\\1) && is_array($\\1) && !empty($\\1)) || (is_object($\\1) && ($\\1 instanceof Iterator) )):
				foreach($\\1 as $\\1_context):
			 extract($\\1_context,EXTR_REFS);?>',
			 '<?php endforeach;endif;?>',
			 '<?php if((!isset($\\1) || !is_array($\\1) || empty($\\1)) && !((is_object($\\1) && ($\\1 instanceof Iterator)) && $\\1->rewind())): ?>',
			 '<?php endif;?>',
			 '<?php if(isset($\\1) && !empty($\\1)): ?>',
			 '<?php endif;?>',
			 '<?php if(!isset($\\1) || empty($\\1)): ?>',
			 '<?php endif;?>',
			 '<?php $this->cycle("\\1");?>',
		//pagination helper 
			 '<?php $this->pagination($\\1,\'\\3\',\'\\5\',<<<EOT
\\6
EOT
); ?>',			 
 		//format input
			'<?php $this->format(\'\\1\',\'\\2\');?>',	
			'<?php if(isset($\\1)){ echo date("\\2",$\\1);}?>',
		 //template vars
			 '<?php if(isset($\\1)){ echo $\\1;}?>',
		 //form handling
			 '<?php $this->input(\'\\1\');?>',
			 '<?php $this->buildSelect(\'\\1\',\'\\3\'); ?>',
			 '<?php $this->checkField(\'\\1\',\'\\3\'); ?>',
		 //widget handling
			 '<?php $this->requestWidget(\'$1\',\'$2\'); ?>',
		 //messaging
			 '<?php $this->requesMessage(\'\\1\',\'\\3\'); ?>',
		 );
	
	/**
	 * Parses a template file looking for template commands and template commands. Optionaly it can echo the result 
	 * instead of returning it by setting the $print param to true
	 *
	 * @param string $file
	 * @param array $data
	 * @param bool $print
	 * @return string
	 */
	public function parse($file, $data, $print = false){
		try {
			$template = $this->_loadTemplate($file);
		}catch (Exception  $e){
			$this->_handleExceptions($e);
		}
		if(!is_array($data)){
			$data = array();
		}
		$template = preg_replace($this->tags,$this->replace,$template);
		/*header('Content-type:text/plain');
		echo $template;
		exit();*/
		$error = true;
		ob_start();
		extract($data);
		$error = eval('?>'.$template); 
		$ret_val = ob_get_clean();
		if($this->getDebug() && $error === false){
			$this->handleParserError($ret_val,$template,$file);
		}
		if($print){
			echo $ret_val;
		}
		return $ret_val;
	}
	
	/**
	 * Clears the output buffer
	 */
	public function clear(){
		if(ob_get_level() > 1){
			ob_end_clean();
		}
	}
	
	/**
	 * Loads the tempalte file
	 *
	 * @param string $path_to_template
	 * @return string
	 */
	protected function _loadTemplate($path_to_template){
		if(!$path_to_template){
			throw new Exception('No template file specified');		
		}
		$template = '';
		$template = file_get_contents($path_to_template);
		if($template === false){
			throw new Exception('Failed to load Template file:'.$path_to_template);
		}
		return $template;
	}
	
	/**
	 * Handles exceptions thrown by the loading of the tempalte file, or by the registering of new commands/modifiers
	 *
	 * @param Exception $e
	 */
	protected function _handleExceptions(Exception $e){
		showError($e->getMessage());
		exit();
	}
	
	public function input($field){
		if(!isset($this->_input[$field])){
			return;
		}
		echo $this->_input[$field];
	}
	
	public function format($field,$format){
		if(!isset($this->_input[$field])){
			return;
		}
		echo date($format,$this->_input[$field]);
	}
	
	public function buildSelect($options,$key = ''){
		if(empty($key)){
			$key = $options.'_id';
		}
		if(!isset($this->_input[$options]) || empty($this->_input[$options])){
			return;
		}
		
		if(isset($this->_input[$key])){
			$selected = $this->_input[$key];
		}else{
			$selected = '';
		}
		
		$out_str = '';
		foreach ($this->_input[$options] as $key => $val){
			$out_str .= '<option value="'.$key.'" ';
			$out_str .= ($key == $selected ? ' selected="selected" ' : '');
			$out_str .= '>'.$val.'</option>';
		}
		echo $out_str;
	}
	
	public function checkField($field, $always = false){
		$always = (bool)$always;
		//we have the following posibilities
		//1. not set in the input
		//2. set in the input
		//2a. set in the input to a truthy value(1,true)
		//2b set in the input as a false value
		//3 not set in the input but has always set to true
		$ret_val = 'checked="checked"';
		if(!isset($this->_input[$field]) || empty($this->_input[$field])){
			$ret_val = '';
		}elseif(!$this->_input[$field]){
			$ret_val = '';
		}
		if(!isset($this->_input[$field]) && $always){
			$ret_val = 'checked="checked"';
		}
		echo $ret_val;	
	}
	
	
	public function requestWidget($app, $widget){
		if(!is_object($this->_widget)){
			return false;
		}
		
		$this->_widget->handle($app,$widget);
	}
	
	public function requesMessage($model, $field = false){
		if($field == ''){
			$field = false;
		}
		echo $this->_message->getMessage($model,$field);
	}
	
	public function cycle($params){
		$params = explode(',',$params);
		static $cycles = array();
		$key = join('@-@',$params);
		$i = 0;
		if(!isset($cycles[$key])){
			$cycles[$key] = array('used_index' => 0,'data'=>$params);
		}
		$ret_val = $cycles[$key]['data'][$cycles[$key]['used_index']];
		$cycles[$key]['used_index'] = $cycles[$key]['used_index']+1 < count($cycles[$key]['data']) ? ++$cycles[$key]['used_index'] : 0;
		echo $ret_val;
	}
	
	public function pagination($objectName,$showLast = false,$isDropDown = false,$templates = ''){
		//if we have both isdropdown and showlast we return nothing to let the dude know something is wrong
		if($showLast && $isDropDown){
			return false;
		}
		
		if(!isset($objectName)){
			return false;
 		}
 		$max = $currentPage = $rowPerPage = 0;

 		if(is_object($objectName) && ($objectName instanceof Paginate)){
			//if an object then use the standard methods :P
			$max = $objectName->getTotalRecords();
			if(!$max){
				return '';
			}
			$currentPage = $objectName->getCurrentPage();
			$rowPerPage = $objectName->getRowPerPage();
			if(!$rowPerPage){
				return '';
			}
			$max = ceil($max / $rowPerPage);
		}elseif (is_array($objectName) && !empty($objectName)){
			$max = count($objectName);
			$currentPage = 0;
		}else{
			return '';//do not know what to do :)
		}
		$isDropDown = $isDropDown;
		//we extract the prev and next tempaltes since those are required in both
		$prev = preg_split('!{#:prev}(.*?){#:end_prev}!',$templates,null,PREG_SPLIT_DELIM_CAPTURE);
		$prev = next($prev);
		$next = preg_split('!{#:next}(.*?){#:end_next}!',$templates,null,PREG_SPLIT_DELIM_CAPTURE);
		$next = next($next);
		if($isDropDown =='true'){
			//if it's a dropdown we get the item, and select templates
			$item = preg_split('!{#:item}(.*?){#:end_item}!',$templates,null,PREG_SPLIT_DELIM_CAPTURE);
			$item = next($item);
			$select = preg_split('!{#:select}(.*?){#:end_select}!',$templates,null,PREG_SPLIT_DELIM_CAPTURE);
			$select = next($select);
			//now that we have all the items we need the object to paginate
			if($currentPage - 1 >= 0){
				echo str_replace('{#:offset}',($currentPage - 1),$prev);
			}
			$out = '';
			for($i = 0; $i < $max;$i++){
				$option = str_replace('{#:offset}',$i,$item); 
				$option = str_replace('{#:page}',($i+1),$option); 
				if($i  == $currentPage){
					$option = str_replace('{#:selected}','selected="selected"',$option);
				}else{
					$option = str_replace('{#:selected}','',$option);
				}
				$out.= $option;
			}
			echo str_replace('{#:items}',$out,$select);
			if($currentPage + 1 < $max){
				echo str_replace('{#:offset}',($currentPage+1),$next);
			}
			return ;//done !
		}
		if($currentPage - 1 >= 0){
			echo str_replace('{#:offset}',($currentPage - 1),$prev);
		}
		//otherwise we need to get the here template and normal template
		$here = preg_split('!{#:here}(.*?){#:end_here}!',$templates,null,PREG_SPLIT_DELIM_CAPTURE);
		$here = next($here);
		$normal = preg_split('!{#:normal}(.*?){#:end_normal}!',$templates,null,PREG_SPLIT_DELIM_CAPTURE);
		$normal = next($normal);
		$out = '';

		for($i = 0; $i < $max;$i++){
			$option = '';
			if($i == $currentPage){
				$option = str_replace('{#:page}',($i+1),$here);
				$option = str_replace('{#:offset}',$i,$option);
			}else{
				$option = str_replace('{#:page}',($i+1),$normal);
				$option = str_replace('{#:offset}',$i,$option);
			}
			$out.= $option;
		}
		echo $out;
		if($currentPage + 1 < $max){
			echo str_replace('{#:offset}',($currentPage+1),$next);
		}
		
		if($showLast == 'true' && ($currentPage + 1 < $max)){
			//use the last template
			$last = preg_split('!{#:last}(.*?){#:end_last}!',$templates,null,PREG_SPLIT_DELIM_CAPTURE);
			$last = next($last);
			echo str_replace('{#:offset}',($max-1),$last);
		}
	}
	
	
	public function handleParserError($content, $template = '',$file = ''){
		$matches = array();
		//echo $content;
		preg_match('!<b>Parse error</b>:  syntax error, unexpected ([\w$]+) in <b>(.+)</b> on line <b>([\w]+)</b>!',$content,$matches);
		$type = $matches[1];
		$line = end($matches);
		if(!$this->_hasErrorHandler){
			echo '<h1>Error: unexpected '.$type.' on line '.$line.' in '.$file.'</h1>';
			highlight_string($template);
		}else{
			$title ='<h1>Error: unexpected '.$type.' on line '.$line.' in '.$file.'</h1>';
			$this->_error->showError(highlight_string($template,true),$title);
		}
		exit();
	}
	
	
	
	public function setInput($arr){
		$this->_input = $arr;
	}
	
	public function setWidgetHandler($widgetHandler){
		$this->_widget = $widgetHandler;
	}
	
	public function setMessageContainer($container){
		$this->_message = $container;
	}
	
	public function setErrorHandler($err){
		$this->_hasErrorHandler = true;
		$this->_error = $err;
	}
	
	public function setDebug($debug){
		$this->_debug = $debug;
	}
	
	public function getDebug(){
		return $this->_debug;
	}
}
/* End of file template.php */