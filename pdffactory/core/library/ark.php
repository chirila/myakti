<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ark
{

    // for debugging purposes only
    public static $dolist = array();
    public static $starttime = array();
    public static $dotime = array();

    // layout
    public static $layout;

    // currently executed app / controller / model / method
    public static $app;
    public static $controller;
    public static $model;
    public static $method;

    // model view controller filepaths
    public static $apppath;
    public static $modelpath;
    public static $controllerpath;
    public static $viewpath;

    /*
    app mode:
    1. no environment & no apps used.
    2. environment used. No apps
    3. apps used. No Environment
    4. apps used. Environment Used
    */
    public static $mode;

    private static $_loadedLibs = array();

    /**
     * @param string $do
     * @return bool|mixed|string
     */
    public static function run($do = '')
    {
        global $in, $default_app, $default_controller;

        $start = console::getMicroTime();
        

        self::get_mode();
        if ($do == '') {
            if (empty($in['do'])) {
                switch (self::$mode) {
                    case 1: {
                        $dctrl = $default_controller;
                        $dapp = false;
                    }
                        break;
                    case 2: {
                        $dctrl = $default_controller[ENVIRONMENT];
                        $dapp = false;
                    }
                        break;
                    case 3: {
                        $dctrl = $default_controller;
                        $dapp = $default_app;
                    }
                        break;
                    case 4: {
                        $dctrl = $default_controller[ENVIRONMENT];
                        $dapp = $default_app[ENVIRONMENT];
                    }
                        break;
                }

                if ($dapp) {
                    $in['do'] = $dapp . '-' . $dctrl;
                } else {
                    $in['do'] = $dctrl;
                }
            }
            $do = $in['do'];
        }

        self::get_acmm($do);

        // if (DEBUG == true) {
            self::debug($do);
            self::$starttime[self::$controller] = console::getMicroTime();
        // }


        if (self::$model) {
            if (perm::get('model')) {
                $mod = self::load_model(self::$model, $in);
                if (self::$method) {
                    $met = self::$method;
                    if (method_exists($mod, $met)) {
                        console::benchmark('model start');
                        $mod->$met($in);
                        console::end('model start');
                    } else {
                        // method does not exist
                        msg::error('Method does not exist','d');
                    }

                }
            } else {
                http_response_code(401);
                json_out();
                // no permissions are set for this method
                msg::error("You don't have permissions to run this action",'d');
            }

        }

        if (self::$controller) {
            global $auth_app, $auth_controller;
            if (defined('USE_APPS') && USE_APPS == true) {
                $auth = $auth_app . '-' . $auth_controller;
            } else {
                $auth = $auth_controller;
            }
            if (perm::get('controller')) {
                $page = self::load_controller(self::$controller, $in);
            } elseif ($do == $in['do']) {
                http_response_code(401);
                json_out();
                msg::notice ( "You don't have permission to access this page",'notice');
                // return '';
                if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                    json_out($in); #deny access
                }
                global $config;
                // return login form
                // $in['do'] = $auth;
                // self::$dotime[] = console::getEndMicroTime($start);
                $requestMethod = $_SERVER['REQUEST_METHOD'];
                if(isset($requestMethod) && $requestMethod == 'OPTIONS'){
                    return '';
                }else{
                    header('Location: '.$config['site_url']);
                    exit();
                }
                // return ark::run();
            } else {
                http_response_code(401);
                json_out();
                // msg::notice ( "You don't have permission to access this page",'notice');
                // json_out($in); #deny accessS
                // // msg::$notice = "You don't have permission to access this page";
                // self::$dotime[] = console::getEndMicroTime($start);
                return '';
            }
        } else {
            return '';
        }
        if ($do == $in['do']) {
            self::$layout = self::get_layout($in['do']);
        }

        // self::$dotime[] = console::getEndMicroTime($start);
        return $page;
    }

    /*
    get app mode
     1. no environment & no apps used.
    2. environment used. No apps
    3. apps used. No Environment
    4. apps used. Environment Used
    */
    public static function get_mode()
    {

        if ((defined('USE_APPS') && USE_APPS == true)) {
            if (defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true) {
                // mode 4. apps used. Environment Used
                self::$mode = 4;
                return 4;
            } else {
                // mode 3. apps used. No Environment
                self::$mode = 3;
                return 3;
            }
        } else {
            if (defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true) {
                // mode 2. environment used. No apps
                self::$mode = 2;
                return 2;
            } else {
                // mode 1. no environment & no apps used.
                self::$mode = 1;
                return 1;
            }
        }

    }

    public static function loadLibraries($libraries = array(), $libPath = '')
    {
        $libraryPath = INSTALLPATH . 'libraries/';
        if ($libPath) {
            $libraryPath = $libPath;
        }
        if (empty($libraries)) {
            include(INSTALLPATH . 'config/startup.php');
            if (!isset($load)) {
                return false;
            }
        } else {
            $load = array('libraries' => $libraries);
        }
        foreach ($load['libraries'] as $library) {
            if (isset(self::$_loadedLibs[$library])) {
                continue;
            }
            if (!file_exists($libraryPath . $library . '.php')) {
                console::error('Could not load library ' . $library);
            } else {
                require($libraryPath . $library . '.php');
                self::$_loadedLibs[$library] = true;
            }
        }

    }

    private static function get_layout($do)
    {
        if(self::$layout){
            return self::$layout;
        }

        if (defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true) {
            $layout_path = self::$apppath . ENVIRONMENT . '/layout/';
        } else {
            $layout_path = self::$apppath . 'layout/';
        }

        /*if (defined('USE_APPS') && USE_APPS == true) {
            $act = $act[1];
        } else {
            $act = $act[0];
        }

        if (file_exists($layout_path . $act . '.html')) {
            return $layout_path . $act . '.html';
        }*/

        if (file_exists($layout_path . 'template.html')) {
            return $layout_path . 'template.html';
        }
        if (defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true) {
            if (file_exists(LAYOUT . ENVIRONMENT . '_template.html')) {
                return LAYOUT . ENVIRONMENT . '_template.html';
            }
        }else{
            if (file_exists(LAYOUT . 'template.html')) {
                return LAYOUT . 'template.html';
            }
        }
    }

    public static function set_layout($file)
    {

        if (defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true) {
            $layout_path = self::$apppath . ENVIRONMENT . '/layout/';
        } else {
            $layout_path = self::$apppath . 'layout/';
        }

        if (file_exists($layout_path . $file)) {
            self::$layout = $layout_path . $file;
        }

        if (file_exists($layout_path . $file)) {
            self::$layout = $layout_path . $file;
        }

        if (file_exists(LAYOUT . ENVIRONMENT . '_template.html')) {
            self::$layout = LAYOUT . ENVIRONMENT . '_template.html';
        }
    }

    private static function get_acmm($do)
    {
        global $apps;

        $actions = explode('-', $do);
        if ($actions[0] == 'this') {
            global $in;
            $act = explode('-', $in['do']);
            $actions[0] = $act[0];
        }
        // if the first parameter is a registered app
        if (defined('USE_APPS') && USE_APPS == true && $apps[$actions[0]]) {
            self::$app = $actions[0];
            if (strlen($actions[1]) > 1) {
                self::$controller = $actions[1];
            } else {
                self::$controller = false;
            }
            self::$model = null;
            self::$method = null;
            if (!empty($actions[2])) {
                self::$model = $actions[2];
            }
            if (!empty($actions[3])) {
                self::$method = $actions[3];
            }
        } else {
            // we are in No Apps mode
            self::$app = '';
            if (strlen($actions[0]) > 1) {
                self::$controller = $actions[0];
            } else {
                self::$controller = false;
            }
            self::$model = null;
            self::$method = null;
            if (!empty($actions[1])) {
                self::$model = $actions[1];
            }
            if (!empty($actions[2])) {
                self::$method = $actions[2];
            }
        }

        if (defined('USE_APPS') && USE_APPS == true) {
            self::$apppath = APPSPATH . self::$app . '/';
        } else {
            self::$apppath = APPSPATH;
        }

        if (defined('USE_ENVIRONMENT') && USE_ENVIRONMENT == true) {
            self::$modelpath = self::$apppath . ENVIRONMENT . '/model/';
            self::$controllerpath = self::$apppath . ENVIRONMENT . '/controller/';
            self::$viewpath = self::$apppath . ENVIRONMENT . '/view/';
        } else {
            self::$modelpath = self::$apppath . 'model/';
            self::$controllerpath = self::$apppath . 'controller/';
            self::$viewpath = self::$apppath . 'view/';
        }
    }

    private static function load_model($m, &$in)
    {
        if (!empty($in['skip_action'])) {
            return false;
        }

        if (file_exists(self::$modelpath . $m . '.php')) {
            include_once(self::$modelpath . $m . '.php');
        } else {
            // error can not find file
            console::error('Cannot find model file');
            return false;
        }
        if (class_exists($m)) {
            $m = new $m($in);
            return $m;
        }
        console::error('Model class does not exist');
        // model class does not exist
        return false;
    }

    private static function load_controller($c, &$in)
    {
        global $config;

        if (file_exists(self::$controllerpath . $c . '.php')) {
            $view = new at();
            $db = new sqldb();
            $page_tip = new tips();
            $c = include(self::$controllerpath . $c . '.php');
        } else {
            if (ENVIRONMENT == "admin") {
                global $default_app;
                if (defined('USE_APPS') && USE_APPS == true) {
                    $home = $default_app['admin'] . '-error';
                } else {
                    $home = 'error';
                }
                $in['do'] = $home;
                $in['error'] = '404';
                // error can not find file
                // return false;
                # error can not find file => redirect the user to ther 404 page
                console::error('Page '.$c.' not found');
                return ark::run();
            }
            console::error('Page not found');
            return false;
        }
        return $c;
    }

    private static function debug($do)
    {
        $entry = array(
            'do' => str_replace('this', 'this(' . self::$app . ')', $do),
            'app' => self::$app,
            'apppath' => self::$apppath,
            'controller' => self::$controller,
            'controllerpath' => self::$controllerpath . self::$controller . '.php',
            'model' => self::$model,
            'modelpath' => self::$modelpath . self::$model . '.php',
            'method' => self::$method
        );
        array_push(self::$dolist, $entry);
    }
}
