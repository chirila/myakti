<?php   if(!defined('BASEPATH')) exit('No direct script access allowed');

require(INSTALLPATH.'libraries/gen_lib.php');

date_default_timezone_set('Europe/Bucharest');
session_start();

$request_method = $_SERVER['REQUEST_METHOD'];
$allowd_methods = array("GET","POST","PUT","DELETE","OPTIONS");

if(!in_array($request_method,$allowd_methods)){
    http_response_code(401);
    exit();
    // block unwanted request methods
}

// build the $in array
if(is_array($_GET)){
    $in = cleanInputs($_GET);
    unset($_GET);
}

if(is_array($_POST)){
    $in = array_merge($in,cleanInputs($_POST));
    unset($_POST);
}

$json_vars = json_decode( @file_get_contents( "php://input" ), true );

if($json_vars){
    $in = array_merge($in,cleanInputs($json_vars));
}

$header = getallheaders();

if(isset($header['Token'])){
    $in['token'] =$header['Token'];
}else if(isset($header['token'])){
    $in['token'] =$header['token'];
}

require(BASEPATH.'common.php');

if(isset($in['lang_id'])){
    $_SESSION['lang_id'] = $in['lang_id'];
}

function cleanInputs($data){
    $clean_input = array();
    if(is_array($data)){
        foreach($data as $k => $v){
            $clean_input[$k] = cleanInputs($v);
        }
    }elseif(is_object($data)){
        $data = (array)$data;
        foreach($data as $k => $v){
            $clean_input[$k] = cleanInputs($v);
        }
    }
    else{
        if(get_magic_quotes_gpc()){
            $data = trim(stripslashes($data));
        }
        $clean_input = addslashes(trim($data));
    }
    return $clean_input;
}
define("LOG_QUERYS", 'NO');

$_db=new sqldb();
$_db->query("SET names utf8");
$_db->query("SET sql_mode= ''");
$_db->query("SELECT constant_name, value, type, long_value FROM settings");
while($_db->move_next())
{
    if($_db->f('constant_name') && $_db->f('type')  == 1)
    {
        if(!defined($_db->f('constant_name'))){
            if($_db->f('value')==''){
                define($_db->f('constant_name'),'0');
            }else{
                define($_db->f('constant_name'),$_db->f('value'));
            }           
        }
    }else{
        if(!defined($_db->f('constant_name'))){
            define($_db->f('constant_name'),$_db->f('long_value'));
        }
    }
}

### apps ###
$_db->query("SELECT * FROM apps WHERE type='main' ");
while ($_db->move_next()) {
    if($_db->f('name') && $_db->f('api')){
        define(str_replace(' ',"_",strtoupper($_db->f('name'))),$_db->f('api'));
        define(str_replace(' ',"_",strtoupper($_db->f('name')))."_ACTIVE",$_db->f('active'));
    }
}
$def_lang = $_db->field("SELECT lang_id FROM pim_lang WHERE default_lang=1");
if(!$def_lang){
    $user_lang = $_SESSION['l'];
    if ($user_lang=='nl')
    {
        $user_lang='du';
    }
    switch ($user_lang) {
        case 'en':
            $user_lang_id=1;
            break;
        case 'du':
            $user_lang_id=3;
            break;
        case 'fr':
            $user_lang_id=2;
            break;
        default: 
            $user_lang_id=2;
            break;
    }
    $def_lang = $user_lang_id;
}

// $_db->move_next();
define('DEFAULT_LANG_ID',$def_lang);

# load the language translations 
switch (true) {
    case isset($_SESSION['l']):
        $language = $_SESSION['l'];
        break;
    case isset($_COOKIE["l"]):
        $language = $_COOKIE["l"];
        break;
    default:
        $language = LANGUAGE_TR;
        break;
}
$allowed_langs = array('en','fr','nl','de');
if(!in_array($language, $allowed_langs)){
    $language = 'en';
}
$lang = array();
$db_config = array(
    'hostname' => $database_config['mysql']['hostname'],
    'username' => $database_config['mysql']['username'],
    'password' => $database_config['mysql']['password'],
    'database' => $database_config['user_db'],
);
$db_users = new sqldb($db_config);
$languageLabels = $db_users->query("SELECT id, ".strtolower($language)." FROM langs ")->getAll();
foreach ($languageLabels as $key => $value) {
    $lang[$value['id']] = $value[strtolower($language)];
}
//JWT start
ark::loadLibraries(array('jwt/JWT'));
$JWT_PSWD=$db_users->field("SELECT value FROM settings WHERE constant_name='JWT_PSWD'");
$AWS_FACTORY_DATA=$db_users->field("SELECT long_value FROM settings WHERE constant_name='AWS_FACTORY'");
try{
    $AWS_FACTORY=JWT::decode($AWS_FACTORY_DATA, $JWT_PSWD, array('HS256'));
    define('AWS_FACTORY_KEY',$AWS_FACTORY->data->key);
    define('AWS_FACTORY_SECRET',$AWS_FACTORY->data->secret);
}catch(ExpiredException $e){
    //
}catch(Exception $e){
    //    
}
//JWT end

$p_access = array();
$user_plan = 0;
$user_start=0;
// $show_trial_warning='';

if (!empty($_SESSION['u_id']))
{
    $user_plan = $db_users->field("SELECT plan FROM users WHERE user_id='".$_SESSION['u_id']."' ");
    $p_access = $db_users->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' ");
    $p_access = explode(';',$p_access);
    array_push($p_access,'10');
    #timesheet must be moved back to projects
    // $db_users = new sqldb($database_users);
    $user = $db_users->query("SELECT * FROM users WHERE user_id=".$_SESSION['u_id']);
    if($user->f('user_role') ==1 && $user->f('active')==0 && $user_plan > 1){
        $_SESSION['is_trial']=1;
        $didit = explode('-',$in['do']);
        $didit = $didit[0].'-'.$didit[1];
        $pricing_pages_array = array('settings-users','auth-login','settings-change_billing','settings-pricing_plan','settings-subscription');
        // if( ($didit != 'settings-users') && ($didit != 'auth-login') && ($didit != 'settings-change_billing') ){
        /*if( !in_array($didit, $pricing_pages_array) ){
            if(defined('PAYMILL_CLIENT_ID')){
                $in['do'] = 'settings-subscription';
            }else{
                $in['do'] = 'settings-pricing_plan';
            }
            header('Location: index.php?do='.$in['do']);
            exit();
        }*/
    }
    $trial = $db_users->query("SELECT is_trial,end_date,start_date FROM user_info WHERE user_id=".$_SESSION['u_id']);
    $trial->next();
    $user_start = $trial->f('start_date');
    if ($trial->f('is_trial') == 1)
    {

        if($trial->f('is_trial') && $trial->f('end_date')+7*24*60*60 < time() && $user_plan > 1 && $user->f('payment_type') == 0 && $user->f('u_type') == 0){
            $_SESSION['is_trial']=1;
            $didit = explode('-',$in['do']);
            $didit = $didit[0].'-'.$didit[1];
            $pricing_pages_array = array('settings-users','auth-login','settings-change_billing','settings-pricing_plan','settings-subscription');
            // if( ($didit != 'settings-users') && ($didit != 'auth-login') && ($didit != 'settings-change_billing') ){
            /*if( !in_array($didit, $pricing_pages_array) ){
                if(defined('PAYMILL_CLIENT_ID')){
                    $in['do'] = 'settings-subscription';
                }else{
                    $in['do'] = 'settings-pricing_plan';
                }
                header('Location: index.php?do='.$in['do']);
                exit();
            }*/
        }else{
            $_SESSION['is_trial']=0;
        }
    }
    else
    {
        if(time()>$trial->f('start_date')+24*60*60)
        {
            // $show_trial_warning = '<div class="trial_warning">'.gm("Trial period ends in").': '.ceil(($trial->f('end_date') - time())/(24*60*60)).' '.gm("days").'. <a href="index.php?do=settings-subscription" ><b>'.gm('Purchase now').'.</b></a> <a href="#" id="close_trial_warning">'.gm("Close").'</a></div>';
        }
        if($trial->f('end_date')<time())
        {
            $_SESSION['is_trial']=1;
            // header('Location: ../../index.php');
            // header("Location: index.php?do=settings-pricing_plan");
            $didit = explode('-',$in['do']);
            $didit = $didit[0].'-'.$didit[1];
            $pricing_pages_array = array('settings-users','auth-login','settings-change_billing','settings-pricing_plan','settings-subscription');
            // if( ($didit != 'settings-users') && ($didit != 'auth-login') && ($didit != 'settings-change_billing') ){
            /*if( !in_array($didit, $pricing_pages_array) ){
                if(defined('PAYMILL_CLIENT_ID')){
                    $in['do'] = 'settings-subscription';
                }else{
                    $in['do'] = 'settings-pricing_plan';
                }
                header('Location: index.php?do='.$in['do']);
                exit();
            }*/
        }
    }

    $acc_time_zone = $db_users->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_TIME_ZONE'");
    $acc_time_zone = $acc_time_zone*60*60;
    $_SESSION['user_timezone_offset'] = $acc_time_zone-7200;

    $acc_type = $_db->field("SELECT value FROM settings WHERE constant_name='ACCOUNT_TYPE'");
    $_SESSION['acc_type'] = $acc_type;

}
// console::log('user_plan:'.$user_plan);
if(defined('NEW_SUBSCRIPTION') && NEW_SUBSCRIPTION==1){
    $access_array_init=explode(';',$db_users->field("SELECT credentials FROM users WHERE user_id='".$_SESSION['u_id']."' "));
    $access_array=array();
    foreach($access_array_init as $key=>$value){
        $access_array[$key]=(int)$value;
    }
    if(defined('TACTILL_ACTIVE') && TACTILL_ACTIVE==1){
        array_push($access_array,15);
    }
    array_push($access_array,'100');
    perm::$allow_apps = $access_array;
}else{
    switch ($user_plan) {
        case '1':
             $access_array = array(1,3,4,7,8,10,100,18,19);
            if($user_start > 1380624720){ # from this time we have a different type o free account (one that does not include projects)
                $access_array = array(1,2,4,7,8,10,100,18);
            }
            # with the new payment system
            if(defined('PAYMILL_CLIENT_ID')){
                $access_array = array();
            }
            // $_SESSION['group'] = 'free';
            break;
        case '2':
            $access_array = array(1,3,4,5,7,8,10,13,17,100,18,19);
            # with the new payment system
            if(defined('PAYMILL_CLIENT_ID')){
                if(defined('HOW_MANY') && HOW_MANY == 0){
                    $access_array = array(1,2,3,4,7,10,100,18,19);
                }else{
                    $access_array = array(1,2,3,4,5,7,10,13,17,100,18,19);
                }
            }
            // $_SESSION['group'] = 'services';
            break;
        case '5':
                /*if(defined('HOW_MANY') && HOW_MANY == 0){
                    $access_array = array(1,2,3,4,7,10,100);
                }else{*/
                    $access_array = array(1,2,3,4,5,7,10,13,17,100,18,19);
                // }

            // $_SESSION['group'] = 'services';
            break;
        case '3':
            $access_array = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,100,18,19);
            if(defined('PAYMILL_CLIENT_ID')){
                if(defined('HOW_MANY') && HOW_MANY == 0){
                    $access_array = array(1,2,3,4,5,6,7,10,12,13,14,16,17,100,18,19);
                }else{
                    $access_array = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,100,18,19);
                }
                if(defined('NEW_PLAN_WEBSHOP') && NEW_PLAN_WEBSHOP > 0){
                    array_push($access_array, 9);
                }
            }
            // $_SESSION['group'] = 'products';
            break;
        case '4':
            $access_array = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,100,18,19);
            if(defined('PAYMILL_CLIENT_ID')){
                if(defined('HOW_MANY') && HOW_MANY == 0){
                    $access_array = array(1,2,4,5,6,7,8,10,12,14,16,100,18);
                }else{
                    $access_array = array(1,2,4,5,6,7,8,10,11,12,14,16,100,18);
                }
                if(defined('NEW_PLAN_WEBSHOP') && NEW_PLAN_WEBSHOP > 0){
                    array_push($access_array, 9);
                }
            }
            break;
        default:
            $access_array = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,100,18,19);
            // $_SESSION['group'] = 'admin';
            break;
    }
    if(defined('TACTILL_ACTIVE') && TACTILL_ACTIVE==1){
        array_push($access_array,15);
    }
#access 100 for loging
array_push($p_access,'100');
$p_access = array_intersect($p_access, $access_array);
perm::$allow_apps = $p_access;
}
/* End of file startup.php */